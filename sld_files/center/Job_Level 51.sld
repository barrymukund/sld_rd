<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#217" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3620" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475585938" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.563302124023" Y="-3.512524414063" />
                  <Point X="0.557721069336" Y="-3.497141357422" />
                  <Point X="0.542362915039" Y="-3.467376708984" />
                  <Point X="0.381131591797" Y="-3.235073242188" />
                  <Point X="0.378635253906" Y="-3.231476318359" />
                  <Point X="0.35675100708" Y="-3.209020751953" />
                  <Point X="0.330495697021" Y="-3.189777099609" />
                  <Point X="0.302495056152" Y="-3.175668945312" />
                  <Point X="0.052999134064" Y="-3.098234619141" />
                  <Point X="0.049135917664" Y="-3.097035644531" />
                  <Point X="0.020973333359" Y="-3.092766357422" />
                  <Point X="-0.008670423508" Y="-3.092767333984" />
                  <Point X="-0.036826633453" Y="-3.097036621094" />
                  <Point X="-0.286322418213" Y="-3.174470703125" />
                  <Point X="-0.290185760498" Y="-3.175669921875" />
                  <Point X="-0.318190185547" Y="-3.189780761719" />
                  <Point X="-0.344443572998" Y="-3.209024902344" />
                  <Point X="-0.366324005127" Y="-3.231477783203" />
                  <Point X="-0.527555297852" Y="-3.463781005859" />
                  <Point X="-0.534243225098" Y="-3.474989501953" />
                  <Point X="-0.543959289551" Y="-3.494153808594" />
                  <Point X="-0.550989929199" Y="-3.512524414063" />
                  <Point X="-0.557097961426" Y="-3.535319824219" />
                  <Point X="-0.916584594727" Y="-4.876942382812" />
                  <Point X="-1.079045410156" Y="-4.845408203125" />
                  <Point X="-1.079330810547" Y="-4.845352539063" />
                  <Point X="-1.24641784668" Y="-4.802362792969" />
                  <Point X="-1.214962890625" Y="-4.563438476562" />
                  <Point X="-1.214201049805" Y="-4.5479296875" />
                  <Point X="-1.216508544922" Y="-4.516224121094" />
                  <Point X="-1.27611315918" Y="-4.216572265625" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938476562" Y="-4.182965820312" />
                  <Point X="-1.304010864258" Y="-4.155127929687" />
                  <Point X="-1.323645141602" Y="-4.131203613281" />
                  <Point X="-1.553349243164" Y="-3.929758056641" />
                  <Point X="-1.556906005859" Y="-3.926638916016" />
                  <Point X="-1.583189941406" Y="-3.910294677734" />
                  <Point X="-1.612887573242" Y="-3.897993896484" />
                  <Point X="-1.643028320312" Y="-3.890966308594" />
                  <Point X="-1.947896728516" Y="-3.870984130859" />
                  <Point X="-1.952617431641" Y="-3.870674804688" />
                  <Point X="-1.983420410156" Y="-3.873708984375" />
                  <Point X="-2.01446887207" Y="-3.882029052734" />
                  <Point X="-2.042658325195" Y="-3.894802001953" />
                  <Point X="-2.296690917969" Y="-4.064541259766" />
                  <Point X="-2.315639404297" Y="-4.079608398438" />
                  <Point X="-2.330957275391" Y="-4.095212402344" />
                  <Point X="-2.338531982422" Y="-4.103930664062" />
                  <Point X="-2.480149169922" Y="-4.288489746094" />
                  <Point X="-2.801288085938" Y="-4.0896484375" />
                  <Point X="-2.801712158203" Y="-4.089385986328" />
                  <Point X="-3.104722167969" Y="-3.856078125" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.463888671875" Y="-2.484505371094" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-2.658844238281" Y="-2.472548339844" />
                  <Point X="-3.818024169922" Y="-3.141801269531" />
                  <Point X="-4.082526367188" Y="-2.794300292969" />
                  <Point X="-4.082856445313" Y="-2.793866699219" />
                  <Point X="-4.306142089844" Y="-2.419450195312" />
                  <Point X="-3.105954589844" Y="-1.498513427734" />
                  <Point X="-3.084577148438" Y="-1.475593261719" />
                  <Point X="-3.066612304688" Y="-1.448462036133" />
                  <Point X="-3.053856689453" Y="-1.419833007812" />
                  <Point X="-3.04626953125" Y="-1.390539306641" />
                  <Point X="-3.046150634766" Y="-1.390080078125" />
                  <Point X="-3.043346679688" Y="-1.359643432617" />
                  <Point X="-3.045557861328" Y="-1.327975952148" />
                  <Point X="-3.052560546875" Y="-1.298233764648" />
                  <Point X="-3.068642578125" Y="-1.272252929688" />
                  <Point X="-3.089473876953" Y="-1.248299194336" />
                  <Point X="-3.112971923828" Y="-1.228767211914" />
                  <Point X="-3.13905078125" Y="-1.213418212891" />
                  <Point X="-3.139454589844" Y="-1.213180664062" />
                  <Point X="-3.168717285156" Y="-1.201956665039" />
                  <Point X="-3.20060546875" Y="-1.195474853516" />
                  <Point X="-3.231928710938" Y="-1.194383544922" />
                  <Point X="-3.256748535156" Y="-1.197651245117" />
                  <Point X="-4.732102050781" Y="-1.391885131836" />
                  <Point X="-4.833946289062" Y="-0.993168395996" />
                  <Point X="-4.834076660156" Y="-0.992657470703" />
                  <Point X="-4.892423828125" Y="-0.584698303223" />
                  <Point X="-3.532875976562" Y="-0.220408447266" />
                  <Point X="-3.517489257812" Y="-0.214825744629" />
                  <Point X="-3.487724853516" Y="-0.199466827393" />
                  <Point X="-3.460395019531" Y="-0.180498336792" />
                  <Point X="-3.459980224609" Y="-0.180210388184" />
                  <Point X="-3.437514648438" Y="-0.158318115234" />
                  <Point X="-3.418274414062" Y="-0.132065811157" />
                  <Point X="-3.404168457031" Y="-0.1040679245" />
                  <Point X="-3.395058349609" Y="-0.074715454102" />
                  <Point X="-3.394895751953" Y="-0.074190254211" />
                  <Point X="-3.390642333984" Y="-0.046064556122" />
                  <Point X="-3.390647949219" Y="-0.016441291809" />
                  <Point X="-3.394917236328" Y="0.01169989109" />
                  <Point X="-3.404027099609" Y="0.041052360535" />
                  <Point X="-3.404164550781" Y="0.041495285034" />
                  <Point X="-3.418278808594" Y="0.069514579773" />
                  <Point X="-3.437517578125" Y="0.095762023926" />
                  <Point X="-3.459972167969" Y="0.117644889832" />
                  <Point X="-3.487302001953" Y="0.136613235474" />
                  <Point X="-3.501063720703" Y="0.144547775269" />
                  <Point X="-3.532876464844" Y="0.157848403931" />
                  <Point X="-3.555500976562" Y="0.163910461426" />
                  <Point X="-4.89181640625" Y="0.521975097656" />
                  <Point X="-4.824570800781" Y="0.976414001465" />
                  <Point X="-4.82448828125" Y="0.976971862793" />
                  <Point X="-4.70355078125" Y="1.423267822266" />
                  <Point X="-3.765666259766" Y="1.29979296875" />
                  <Point X="-3.744986328125" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228271484" />
                  <Point X="-3.703137451172" Y="1.305263549805" />
                  <Point X="-3.642647949219" Y="1.32433581543" />
                  <Point X="-3.641711425781" Y="1.324631103516" />
                  <Point X="-3.622776367188" Y="1.332963134766" />
                  <Point X="-3.604030517578" Y="1.343786743164" />
                  <Point X="-3.587348144531" Y="1.35601965332" />
                  <Point X="-3.573709228516" Y="1.371573730469" />
                  <Point X="-3.561294677734" Y="1.389306274414" />
                  <Point X="-3.551349609375" Y="1.407435424805" />
                  <Point X="-3.527077880859" Y="1.466032348633" />
                  <Point X="-3.526702148438" Y="1.466939697266" />
                  <Point X="-3.520913085938" Y="1.486802734375" />
                  <Point X="-3.517156494141" Y="1.508113647461" />
                  <Point X="-3.515804443359" Y="1.528749755859" />
                  <Point X="-3.518950195312" Y="1.549189453125" />
                  <Point X="-3.524550048828" Y="1.570092163086" />
                  <Point X="-3.532047851562" Y="1.589374145508" />
                  <Point X="-3.561334228516" Y="1.64563293457" />
                  <Point X="-3.561787597656" Y="1.64650390625" />
                  <Point X="-3.57327734375" Y="1.663700195313" />
                  <Point X="-3.587191894531" Y="1.680284057617" />
                  <Point X="-3.602135009766" Y="1.694589477539" />
                  <Point X="-3.615112548828" Y="1.704547851562" />
                  <Point X="-4.351859863281" Y="2.269874023438" />
                  <Point X="-4.081475097656" Y="2.733108398438" />
                  <Point X="-4.081153564453" Y="2.733659179688" />
                  <Point X="-3.750504638672" Y="3.158661621094" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187719970703" Y="2.836338867188" />
                  <Point X="-3.167073486328" Y="2.829830078125" />
                  <Point X="-3.146790527344" Y="2.825796142578" />
                  <Point X="-3.062545654297" Y="2.818425537109" />
                  <Point X="-3.061241210938" Y="2.818311523438" />
                  <Point X="-3.040555419922" Y="2.818763427734" />
                  <Point X="-3.019098876953" Y="2.821589599609" />
                  <Point X="-2.999008789062" Y="2.826506835938" />
                  <Point X="-2.980458984375" Y="2.835655273438" />
                  <Point X="-2.962206787109" Y="2.847283935547" />
                  <Point X="-2.946077392578" Y="2.860229736328" />
                  <Point X="-2.886302490234" Y="2.920004638672" />
                  <Point X="-2.885375976562" Y="2.920930419922" />
                  <Point X="-2.872417724609" Y="2.937069580078" />
                  <Point X="-2.860785400391" Y="2.955323486328" />
                  <Point X="-2.851633789062" Y="2.973875488281" />
                  <Point X="-2.84671484375" Y="2.993968994141" />
                  <Point X="-2.843887695312" Y="3.015428710938" />
                  <Point X="-2.843435546875" Y="3.036116943359" />
                  <Point X="-2.850806152344" Y="3.120362060547" />
                  <Point X="-2.850920166016" Y="3.121666503906" />
                  <Point X="-2.854954101562" Y="3.141949462891" />
                  <Point X="-2.861462890625" Y="3.162595947266" />
                  <Point X="-2.869795166016" Y="3.181533447266" />
                  <Point X="-2.875545898438" Y="3.191493896484" />
                  <Point X="-3.183332763672" Y="3.724596435547" />
                  <Point X="-2.701180175781" Y="4.094258056641" />
                  <Point X="-2.700623535156" Y="4.094684814453" />
                  <Point X="-2.167036621094" Y="4.391134277344" />
                  <Point X="-2.043195556641" Y="4.229741210938" />
                  <Point X="-2.028893554688" Y="4.214800292969" />
                  <Point X="-2.012315673828" Y="4.200889160156" />
                  <Point X="-1.995115478516" Y="4.189396484375" />
                  <Point X="-1.90135144043" Y="4.140585449219" />
                  <Point X="-1.899899658203" Y="4.139829589844" />
                  <Point X="-1.880625244141" Y="4.132333496094" />
                  <Point X="-1.859716308594" Y="4.126729980469" />
                  <Point X="-1.839268554688" Y="4.12358203125" />
                  <Point X="-1.818624145508" Y="4.124935546875" />
                  <Point X="-1.797306396484" Y="4.1286953125" />
                  <Point X="-1.777451538086" Y="4.134482910156" />
                  <Point X="-1.679789794922" Y="4.174936035156" />
                  <Point X="-1.678277587891" Y="4.1755625" />
                  <Point X="-1.660142089844" Y="4.185512207031" />
                  <Point X="-1.642414672852" Y="4.197925292969" />
                  <Point X="-1.626865112305" Y="4.211562011719" />
                  <Point X="-1.614635253906" Y="4.228240722656" />
                  <Point X="-1.603813964844" Y="4.246981933594" />
                  <Point X="-1.595481079102" Y="4.265918457031" />
                  <Point X="-1.563694213867" Y="4.366733886719" />
                  <Point X="-1.563201904297" Y="4.368294921875" />
                  <Point X="-1.559166748047" Y="4.388578613281" />
                  <Point X="-1.557279418945" Y="4.410143554688" />
                  <Point X="-1.55773046875" Y="4.430826171875" />
                  <Point X="-1.558384155273" Y="4.435792480469" />
                  <Point X="-1.584201904297" Y="4.631897460938" />
                  <Point X="-0.950351867676" Y="4.809606933594" />
                  <Point X="-0.949631225586" Y="4.809809082031" />
                  <Point X="-0.294710754395" Y="4.886457519531" />
                  <Point X="-0.133903274536" Y="4.286315429688" />
                  <Point X="-0.121765968323" Y="4.256526855469" />
                  <Point X="-0.10966418457" Y="4.236766113281" />
                  <Point X="-0.093138450623" Y="4.220522949219" />
                  <Point X="-0.067946853638" Y="4.201192382812" />
                  <Point X="-0.040650951385" Y="4.186602539062" />
                  <Point X="-0.010113798141" Y="4.181560546875" />
                  <Point X="0.022425815582" Y="4.181560546875" />
                  <Point X="0.052962985992" Y="4.186602539062" />
                  <Point X="0.080258872986" Y="4.201192382812" />
                  <Point X="0.105450462341" Y="4.220522949219" />
                  <Point X="0.123929679871" Y="4.239309570312" />
                  <Point X="0.136537139893" Y="4.262450195312" />
                  <Point X="0.146318511963" Y="4.288459472656" />
                  <Point X="0.149161849976" Y="4.2973125" />
                  <Point X="0.307419250488" Y="4.8879375" />
                  <Point X="0.843405944824" Y="4.831805175781" />
                  <Point X="0.844040588379" Y="4.831738769531" />
                  <Point X="1.478889038086" Y="4.678466796875" />
                  <Point X="1.481028320312" Y="4.677950195312" />
                  <Point X="1.89423046875" Y="4.528079101562" />
                  <Point X="1.894642089844" Y="4.5279296875" />
                  <Point X="2.294214355469" Y="4.341062988281" />
                  <Point X="2.294572509766" Y="4.340895507812" />
                  <Point X="2.680596679688" Y="4.115997070312" />
                  <Point X="2.680980957031" Y="4.115772949219" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075683594" Y="2.539930175781" />
                  <Point X="2.133076660156" Y="2.516055908203" />
                  <Point X="2.111934326172" Y="2.436993896484" />
                  <Point X="2.109032714844" Y="2.420283447266" />
                  <Point X="2.107367919922" Y="2.400157226562" />
                  <Point X="2.107727783203" Y="2.380952636719" />
                  <Point X="2.115970458984" Y="2.312595947266" />
                  <Point X="2.116095947266" Y="2.311555664062" />
                  <Point X="2.121437988281" Y="2.289621582031" />
                  <Point X="2.129705566406" Y="2.2675234375" />
                  <Point X="2.140071289062" Y="2.247470947266" />
                  <Point X="2.182374023438" Y="2.185127685547" />
                  <Point X="2.193075439453" Y="2.172036621094" />
                  <Point X="2.221598632812" Y="2.145592773438" />
                  <Point X="2.283941894531" Y="2.103290039063" />
                  <Point X="2.284927246094" Y="2.102621582031" />
                  <Point X="2.30496484375" Y="2.092265625" />
                  <Point X="2.327044677734" Y="2.084004394531" />
                  <Point X="2.348962158203" Y="2.078663818359" />
                  <Point X="2.417328369141" Y="2.070419921875" />
                  <Point X="2.434446777344" Y="2.069910400391" />
                  <Point X="2.473205566406" Y="2.074170898438" />
                  <Point X="2.552267578125" Y="2.095313232422" />
                  <Point X="2.563040527344" Y="2.098896240234" />
                  <Point X="2.588534179688" Y="2.110145263672" />
                  <Point X="2.611290039062" Y="2.123283447266" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.123043945313" Y="2.689779296875" />
                  <Point X="4.123273925781" Y="2.689459716797" />
                  <Point X="4.26219921875" Y="2.459884277344" />
                  <Point X="3.230783935547" Y="1.668451416016" />
                  <Point X="3.221424316406" Y="1.660241210938" />
                  <Point X="3.203974609375" Y="1.641628662109" />
                  <Point X="3.147073486328" Y="1.567396728516" />
                  <Point X="3.138066894531" Y="1.553202270508" />
                  <Point X="3.128715087891" Y="1.535098388672" />
                  <Point X="3.121629394531" Y="1.517084350586" />
                  <Point X="3.10043359375" Y="1.441293334961" />
                  <Point X="3.10010546875" Y="1.440119750977" />
                  <Point X="3.096651855469" Y="1.417814941406" />
                  <Point X="3.095837158203" Y="1.394241943359" />
                  <Point X="3.097740478516" Y="1.371763549805" />
                  <Point X="3.115139892578" Y="1.287436401367" />
                  <Point X="3.119968261719" Y="1.27136730957" />
                  <Point X="3.127437255859" Y="1.252685058594" />
                  <Point X="3.136284912109" Y="1.235736816406" />
                  <Point X="3.183609619141" Y="1.163805053711" />
                  <Point X="3.184342529297" Y="1.16269140625" />
                  <Point X="3.198899169922" Y="1.145443603516" />
                  <Point X="3.216140380859" Y="1.129357666016" />
                  <Point X="3.234346923828" Y="1.116034912109" />
                  <Point X="3.302927001953" Y="1.077430175781" />
                  <Point X="3.318426757812" Y="1.070450439453" />
                  <Point X="3.337462402344" Y="1.063855102539" />
                  <Point X="3.356116210938" Y="1.059438842773" />
                  <Point X="3.448841064453" Y="1.047183959961" />
                  <Point X="3.460022949219" Y="1.046373657227" />
                  <Point X="3.488203857422" Y="1.04698449707" />
                  <Point X="3.509820556641" Y="1.049830444336" />
                  <Point X="4.776839355469" Y="1.216636474609" />
                  <Point X="4.845838867188" Y="0.933206542969" />
                  <Point X="4.845937011719" Y="0.932803588867" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="3.716579833984" Y="0.32958984375" />
                  <Point X="3.704791259766" Y="0.325586517334" />
                  <Point X="3.681546386719" Y="0.315068359375" />
                  <Point X="3.590447021484" Y="0.262411346436" />
                  <Point X="3.576804443359" Y="0.252836868286" />
                  <Point X="3.561078125" Y="0.239596954346" />
                  <Point X="3.547528076172" Y="0.225572875977" />
                  <Point X="3.492868408203" Y="0.155923706055" />
                  <Point X="3.492022216797" Y="0.15484538269" />
                  <Point X="3.480297607422" Y="0.135564208984" />
                  <Point X="3.47052734375" Y="0.114107566833" />
                  <Point X="3.463681396484" Y="0.092607521057" />
                  <Point X="3.445461425781" Y="-0.002530004501" />
                  <Point X="3.443775146484" Y="-0.019077587128" />
                  <Point X="3.443492919922" Y="-0.03936680603" />
                  <Point X="3.445179199219" Y="-0.058557060242" />
                  <Point X="3.463399169922" Y="-0.153694580078" />
                  <Point X="3.463681396484" Y="-0.155167556763" />
                  <Point X="3.47052734375" Y="-0.176667602539" />
                  <Point X="3.480297607422" Y="-0.198124237061" />
                  <Point X="3.492022216797" Y="-0.217405426025" />
                  <Point X="3.546681884766" Y="-0.287054595947" />
                  <Point X="3.558234375" Y="-0.299349029541" />
                  <Point X="3.573396240234" Y="-0.312851837158" />
                  <Point X="3.589036376953" Y="-0.324155822754" />
                  <Point X="3.680135742188" Y="-0.376812988281" />
                  <Point X="3.690031982422" Y="-0.381787445068" />
                  <Point X="3.716580322266" Y="-0.392150024414" />
                  <Point X="3.736403808594" Y="-0.397461639404" />
                  <Point X="4.89147265625" Y="-0.706961425781" />
                  <Point X="4.855077636719" Y="-0.948363037109" />
                  <Point X="4.8550234375" Y="-0.948722595215" />
                  <Point X="4.801173828125" Y="-1.18469909668" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.408034912109" Y="-1.002710266113" />
                  <Point X="3.374658935547" Y="-1.005508666992" />
                  <Point X="3.195863037109" Y="-1.044370849609" />
                  <Point X="3.193094726563" Y="-1.044972290039" />
                  <Point X="3.163975097656" Y="-1.056596557617" />
                  <Point X="3.136148681641" Y="-1.07348840332" />
                  <Point X="3.112397949219" Y="-1.093959716797" />
                  <Point X="3.004327148438" Y="-1.223935180664" />
                  <Point X="3.002653808594" Y="-1.225947631836" />
                  <Point X="2.987932861328" Y="-1.250330444336" />
                  <Point X="2.976588867188" Y="-1.277718017578" />
                  <Point X="2.969757324219" Y="-1.30536706543" />
                  <Point X="2.954268066406" Y="-1.473691162109" />
                  <Point X="2.954028320312" Y="-1.476297363281" />
                  <Point X="2.956347167969" Y="-1.50756640625" />
                  <Point X="2.964078125" Y="-1.539184448242" />
                  <Point X="2.976449462891" Y="-1.567995361328" />
                  <Point X="3.075397705078" Y="-1.721902832031" />
                  <Point X="3.083852539062" Y="-1.733131225586" />
                  <Point X="3.110628173828" Y="-1.760909301758" />
                  <Point X="3.129024414062" Y="-1.775025268555" />
                  <Point X="4.213122558594" Y="-2.6068828125" />
                  <Point X="4.124962402344" Y="-2.7495390625" />
                  <Point X="4.124807617188" Y="-2.749789550781" />
                  <Point X="4.028981445312" Y="-2.8859453125" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786130615234" Y="-2.170011962891" />
                  <Point X="2.754223388672" Y="-2.159824951172" />
                  <Point X="2.541427978516" Y="-2.121394287109" />
                  <Point X="2.538133056641" Y="-2.120799316406" />
                  <Point X="2.506779052734" Y="-2.120395263672" />
                  <Point X="2.474606933594" Y="-2.125353515625" />
                  <Point X="2.444832763672" Y="-2.135177001953" />
                  <Point X="2.268052001953" Y="-2.228215332031" />
                  <Point X="2.265314697266" Y="-2.229656005859" />
                  <Point X="2.242381591797" Y="-2.246551269531" />
                  <Point X="2.221421142578" Y="-2.267513183594" />
                  <Point X="2.204530517578" Y="-2.290441650391" />
                  <Point X="2.111491943359" Y="-2.467222412109" />
                  <Point X="2.110051513672" Y="-2.469959716797" />
                  <Point X="2.100227783203" Y="-2.499736816406" />
                  <Point X="2.095270996094" Y="-2.531906005859" />
                  <Point X="2.095675292969" Y="-2.563257080078" />
                  <Point X="2.134105957031" Y="-2.776052490234" />
                  <Point X="2.1375078125" Y="-2.789327636719" />
                  <Point X="2.144005859375" Y="-2.808737548828" />
                  <Point X="2.151819335938" Y="-2.826078857422" />
                  <Point X="2.163640869141" Y="-2.846554199219" />
                  <Point X="2.861283447266" Y="-4.054906494141" />
                  <Point X="2.782037841797" Y="-4.111509765625" />
                  <Point X="2.78184765625" Y="-4.111645507812" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747504882812" Y="-2.922180419922" />
                  <Point X="1.721925415039" Y="-2.900558105469" />
                  <Point X="1.512051635742" Y="-2.765628662109" />
                  <Point X="1.508802001953" Y="-2.763539306641" />
                  <Point X="1.479991455078" Y="-2.75116796875" />
                  <Point X="1.448367797852" Y="-2.743435546875" />
                  <Point X="1.417098388672" Y="-2.741116699219" />
                  <Point X="1.187565795898" Y="-2.762238525391" />
                  <Point X="1.18401159668" Y="-2.762565673828" />
                  <Point X="1.156362304688" Y="-2.769397460938" />
                  <Point X="1.128979248047" Y="-2.780739746094" />
                  <Point X="1.104596923828" Y="-2.795460205078" />
                  <Point X="0.92735760498" Y="-2.942828857422" />
                  <Point X="0.924613220215" Y="-2.945110595703" />
                  <Point X="0.90414276123" Y="-2.968860107422" />
                  <Point X="0.887249084473" Y="-2.996688232422" />
                  <Point X="0.875624084473" Y="-3.025809326172" />
                  <Point X="0.822630554199" Y="-3.269621826172" />
                  <Point X="0.820717224121" Y="-3.282854492188" />
                  <Point X="0.81918371582" Y="-3.303775390625" />
                  <Point X="0.819742126465" Y="-3.323120117188" />
                  <Point X="0.823092285156" Y="-3.348566650391" />
                  <Point X="1.022065185547" Y="-4.859915527344" />
                  <Point X="0.975854003906" Y="-4.870044921875" />
                  <Point X="0.975690124512" Y="-4.870081054688" />
                  <Point X="0.929315429688" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058420410156" Y="-4.752638671875" />
                  <Point X="-1.141246337891" Y="-4.731328613281" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077270508" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759033203" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.182938720703" Y="-4.198038574219" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149501953125" />
                  <Point X="-1.205666259766" Y="-4.135465332031" />
                  <Point X="-1.221738647461" Y="-4.107627441406" />
                  <Point X="-1.230575073242" Y="-4.094860351562" />
                  <Point X="-1.250209350586" Y="-4.070936035156" />
                  <Point X="-1.261007202148" Y="-4.059778808594" />
                  <Point X="-1.490711303711" Y="-3.858333251953" />
                  <Point X="-1.506739868164" Y="-3.845964599609" />
                  <Point X="-1.533023803711" Y="-3.829620361328" />
                  <Point X="-1.5468359375" Y="-3.822525634766" />
                  <Point X="-1.576533569336" Y="-3.810224853516" />
                  <Point X="-1.591316040039" Y="-3.805475341797" />
                  <Point X="-1.621456787109" Y="-3.798447753906" />
                  <Point X="-1.636815063477" Y="-3.796169677734" />
                  <Point X="-1.94168359375" Y="-3.7761875" />
                  <Point X="-1.961930175781" Y="-3.776132324219" />
                  <Point X="-1.992733154297" Y="-3.779166503906" />
                  <Point X="-2.008010131836" Y="-3.781946533203" />
                  <Point X="-2.03905847168" Y="-3.790266601562" />
                  <Point X="-2.053677246094" Y="-3.795497558594" />
                  <Point X="-2.081866699219" Y="-3.808270507812" />
                  <Point X="-2.0954375" Y="-3.8158125" />
                  <Point X="-2.349470214844" Y="-3.985551757812" />
                  <Point X="-2.355817382812" Y="-3.99018359375" />
                  <Point X="-2.374765869141" Y="-4.005250732422" />
                  <Point X="-2.383433349609" Y="-4.013057617188" />
                  <Point X="-2.398751220703" Y="-4.028661621094" />
                  <Point X="-2.413900634766" Y="-4.046098388672" />
                  <Point X="-2.503202880859" Y="-4.162479003906" />
                  <Point X="-2.747588134766" Y="-4.011161865234" />
                  <Point X="-2.980863037109" Y="-3.831548095703" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396713623047" Y="-2.417330322266" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-2.706344238281" Y="-2.390275878906" />
                  <Point X="-3.793089111328" Y="-3.017708496094" />
                  <Point X="-4.004013183594" Y="-2.740598144531" />
                  <Point X="-4.181264648438" Y="-2.443373535156" />
                  <Point X="-3.048122314453" Y="-1.573881958008" />
                  <Point X="-3.036482177734" Y="-1.563309814453" />
                  <Point X="-3.015104736328" Y="-1.540389648438" />
                  <Point X="-3.005367431641" Y="-1.528041503906" />
                  <Point X="-2.987402587891" Y="-1.500910400391" />
                  <Point X="-2.979835693359" Y="-1.48712512207" />
                  <Point X="-2.967080078125" Y="-1.45849609375" />
                  <Point X="-2.961891357422" Y="-1.44365234375" />
                  <Point X="-2.954304199219" Y="-1.414358520508" />
                  <Point X="-2.951551269531" Y="-1.398795043945" />
                  <Point X="-2.948747314453" Y="-1.368358398438" />
                  <Point X="-2.948577392578" Y="-1.353026000977" />
                  <Point X="-2.950788574219" Y="-1.321358642578" />
                  <Point X="-2.953086425781" Y="-1.306203857422" />
                  <Point X="-2.960089111328" Y="-1.276461669922" />
                  <Point X="-2.971783447266" Y="-1.248233032227" />
                  <Point X="-2.987865478516" Y="-1.222252319336" />
                  <Point X="-2.996958007812" Y="-1.209912597656" />
                  <Point X="-3.017789306641" Y="-1.185958862305" />
                  <Point X="-3.028747558594" Y="-1.1752421875" />
                  <Point X="-3.052245605469" Y="-1.155710083008" />
                  <Point X="-3.06478515625" Y="-1.146895141602" />
                  <Point X="-3.090864013672" Y="-1.131546142578" />
                  <Point X="-3.105433105469" Y="-1.124481445312" />
                  <Point X="-3.134695800781" Y="-1.113257446289" />
                  <Point X="-3.149793945312" Y="-1.108860473633" />
                  <Point X="-3.181682128906" Y="-1.102378540039" />
                  <Point X="-3.197297607422" Y="-1.100532348633" />
                  <Point X="-3.228620849609" Y="-1.099441162109" />
                  <Point X="-3.244329101562" Y="-1.100196411133" />
                  <Point X="-3.269148925781" Y="-1.103463989258" />
                  <Point X="-4.660920898438" Y="-1.286694213867" />
                  <Point X="-4.740761230469" Y="-0.974121276855" />
                  <Point X="-4.786451660156" Y="-0.654654418945" />
                  <Point X="-3.508288085938" Y="-0.312171417236" />
                  <Point X="-3.500474365234" Y="-0.309712005615" />
                  <Point X="-3.47392578125" Y="-0.299248535156" />
                  <Point X="-3.444161376953" Y="-0.283889678955" />
                  <Point X="-3.433557617188" Y="-0.277511077881" />
                  <Point X="-3.406227783203" Y="-0.258542633057" />
                  <Point X="-3.393678710938" Y="-0.248248123169" />
                  <Point X="-3.371213134766" Y="-0.226355819702" />
                  <Point X="-3.360890136719" Y="-0.214475952148" />
                  <Point X="-3.341649902344" Y="-0.188223648071" />
                  <Point X="-3.333433837891" Y="-0.174810287476" />
                  <Point X="-3.319327880859" Y="-0.146812408447" />
                  <Point X="-3.313437988281" Y="-0.132227890015" />
                  <Point X="-3.304327880859" Y="-0.102875457764" />
                  <Point X="-3.300963867188" Y="-0.088395431519" />
                  <Point X="-3.296710449219" Y="-0.060269733429" />
                  <Point X="-3.295642333984" Y="-0.046046524048" />
                  <Point X="-3.295647949219" Y="-0.016423303604" />
                  <Point X="-3.29672265625" Y="-0.002191916943" />
                  <Point X="-3.300991943359" Y="0.025949234009" />
                  <Point X="-3.304186523438" Y="0.039859149933" />
                  <Point X="-3.313296386719" Y="0.069211578369" />
                  <Point X="-3.319321044922" Y="0.084233787537" />
                  <Point X="-3.333435302734" Y="0.112253067017" />
                  <Point X="-3.341657226562" Y="0.125676391602" />
                  <Point X="-3.360895996094" Y="0.151923782349" />
                  <Point X="-3.371214111328" Y="0.163797714233" />
                  <Point X="-3.393668701172" Y="0.185680664062" />
                  <Point X="-3.405805175781" Y="0.195689376831" />
                  <Point X="-3.433135009766" Y="0.214657669067" />
                  <Point X="-3.439850341797" Y="0.218913635254" />
                  <Point X="-3.464418945312" Y="0.232195755005" />
                  <Point X="-3.496231689453" Y="0.245496322632" />
                  <Point X="-3.508289306641" Y="0.249611541748" />
                  <Point X="-3.530913818359" Y="0.255673568726" />
                  <Point X="-4.785446289062" Y="0.591824584961" />
                  <Point X="-4.731331054688" Y="0.957529724121" />
                  <Point X="-4.6335859375" Y="1.318237182617" />
                  <Point X="-3.778066162109" Y="1.205605712891" />
                  <Point X="-3.767739013672" Y="1.204815551758" />
                  <Point X="-3.747059082031" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.204703125" />
                  <Point X="-3.715144042969" Y="1.20658984375" />
                  <Point X="-3.704891357422" Y="1.208053588867" />
                  <Point X="-3.684604248047" Y="1.212088867188" />
                  <Point X="-3.6745703125" Y="1.214660522461" />
                  <Point X="-3.614080810547" Y="1.233732666016" />
                  <Point X="-3.603448974609" Y="1.237677246094" />
                  <Point X="-3.584513916016" Y="1.246009399414" />
                  <Point X="-3.575274169922" Y="1.250692016602" />
                  <Point X="-3.556528320312" Y="1.261515625" />
                  <Point X="-3.547853515625" Y="1.267176391602" />
                  <Point X="-3.531171142578" Y="1.279409423828" />
                  <Point X="-3.515919677734" Y="1.293386108398" />
                  <Point X="-3.502280761719" Y="1.308940185547" />
                  <Point X="-3.495885742188" Y="1.317089599609" />
                  <Point X="-3.483471191406" Y="1.334822021484" />
                  <Point X="-3.47800390625" Y="1.343615600586" />
                  <Point X="-3.468058837891" Y="1.361744873047" />
                  <Point X="-3.463581054688" Y="1.371080444336" />
                  <Point X="-3.439309326172" Y="1.429677368164" />
                  <Point X="-3.435496826172" Y="1.440358032227" />
                  <Point X="-3.429707763672" Y="1.460221069336" />
                  <Point X="-3.42735546875" Y="1.470310668945" />
                  <Point X="-3.423598876953" Y="1.491621826172" />
                  <Point X="-3.422359863281" Y="1.501902709961" />
                  <Point X="-3.4210078125" Y="1.522538696289" />
                  <Point X="-3.421909912109" Y="1.543200439453" />
                  <Point X="-3.425055664062" Y="1.563640136719" />
                  <Point X="-3.427186035156" Y="1.573773071289" />
                  <Point X="-3.432785888672" Y="1.59467590332" />
                  <Point X="-3.436008544922" Y="1.604521728516" />
                  <Point X="-3.443506347656" Y="1.623803588867" />
                  <Point X="-3.447781738281" Y="1.633240112305" />
                  <Point X="-3.477068115234" Y="1.689498901367" />
                  <Point X="-3.482797119141" Y="1.699281494141" />
                  <Point X="-3.494286865234" Y="1.716477905273" />
                  <Point X="-3.500500976562" Y="1.724762451172" />
                  <Point X="-3.514415527344" Y="1.741346435547" />
                  <Point X="-3.521497070312" Y="1.748907470703" />
                  <Point X="-3.536440185547" Y="1.763212890625" />
                  <Point X="-3.557278808594" Y="1.779915405273" />
                  <Point X="-4.227614257812" Y="2.294281982422" />
                  <Point X="-4.00229296875" Y="2.680312011719" />
                  <Point X="-3.726338378906" Y="3.035012451172" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244916259766" Y="2.757715576172" />
                  <Point X="-3.225979248047" Y="2.749383544922" />
                  <Point X="-3.216282958984" Y="2.745734375" />
                  <Point X="-3.195636474609" Y="2.739225585938" />
                  <Point X="-3.185604492188" Y="2.736655029297" />
                  <Point X="-3.165321533203" Y="2.73262109375" />
                  <Point X="-3.155070556641" Y="2.731157714844" />
                  <Point X="-3.070825683594" Y="2.723787109375" />
                  <Point X="-3.059166259766" Y="2.723334228516" />
                  <Point X="-3.03848046875" Y="2.723786132812" />
                  <Point X="-3.028149658203" Y="2.724576904297" />
                  <Point X="-3.006693115234" Y="2.727403076172" />
                  <Point X="-2.996513427734" Y="2.729313476562" />
                  <Point X="-2.976423339844" Y="2.734230712891" />
                  <Point X="-2.956988769531" Y="2.741305175781" />
                  <Point X="-2.938438964844" Y="2.750453613281" />
                  <Point X="-2.929413330078" Y="2.755534423828" />
                  <Point X="-2.911161132812" Y="2.767163085938" />
                  <Point X="-2.902742431641" Y="2.773196289062" />
                  <Point X="-2.886613037109" Y="2.786142089844" />
                  <Point X="-2.87890234375" Y="2.7930546875" />
                  <Point X="-2.819153808594" Y="2.852803222656" />
                  <Point X="-2.811298583984" Y="2.861453125" />
                  <Point X="-2.798340332031" Y="2.877592285156" />
                  <Point X="-2.792302001953" Y="2.886015869141" />
                  <Point X="-2.780669677734" Y="2.904269775391" />
                  <Point X="-2.775587646484" Y="2.913295898438" />
                  <Point X="-2.766436035156" Y="2.931847900391" />
                  <Point X="-2.759358398438" Y="2.951286132812" />
                  <Point X="-2.754439453125" Y="2.971379638672" />
                  <Point X="-2.752528564453" Y="2.981560791016" />
                  <Point X="-2.749701416016" Y="3.003020507812" />
                  <Point X="-2.748910400391" Y="3.013353027344" />
                  <Point X="-2.748458251953" Y="3.034041259766" />
                  <Point X="-2.748797119141" Y="3.044396972656" />
                  <Point X="-2.756167724609" Y="3.128642089844" />
                  <Point X="-2.757745117188" Y="3.140197509766" />
                  <Point X="-2.761779052734" Y="3.16048046875" />
                  <Point X="-2.764349609375" Y="3.170512451172" />
                  <Point X="-2.770858398438" Y="3.191158935547" />
                  <Point X="-2.774507568359" Y="3.200855224609" />
                  <Point X="-2.78283984375" Y="3.219792724609" />
                  <Point X="-2.793273681641" Y="3.238994384766" />
                  <Point X="-3.059386962891" Y="3.699916503906" />
                  <Point X="-2.648372070312" Y="4.015037353516" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.118564208984" Y="4.171908691406" />
                  <Point X="-2.111822021484" Y="4.164049316406" />
                  <Point X="-2.097520019531" Y="4.149108398438" />
                  <Point X="-2.089959960938" Y="4.14202734375" />
                  <Point X="-2.073382080078" Y="4.128116210938" />
                  <Point X="-2.065094482422" Y="4.121899414062" />
                  <Point X="-2.047894287109" Y="4.110406738281" />
                  <Point X="-2.038982055664" Y="4.105130859375" />
                  <Point X="-1.945217895508" Y="4.056319580078" />
                  <Point X="-1.934333984375" Y="4.051289794922" />
                  <Point X="-1.915059570312" Y="4.043793701172" />
                  <Point X="-1.905217041016" Y="4.040571533203" />
                  <Point X="-1.884308105469" Y="4.034968017578" />
                  <Point X="-1.874171386719" Y="4.032836181641" />
                  <Point X="-1.853723632812" Y="4.029688232422" />
                  <Point X="-1.833053222656" Y="4.028785644531" />
                  <Point X="-1.812408935547" Y="4.030139160156" />
                  <Point X="-1.802124023438" Y="4.031379394531" />
                  <Point X="-1.780806152344" Y="4.035139160156" />
                  <Point X="-1.770720825195" Y="4.037491210938" />
                  <Point X="-1.750865966797" Y="4.043278808594" />
                  <Point X="-1.741096435547" Y="4.046714355469" />
                  <Point X="-1.643434692383" Y="4.087167480469" />
                  <Point X="-1.632582885742" Y="4.092273925781" />
                  <Point X="-1.614447387695" Y="4.102223632812" />
                  <Point X="-1.605651733398" Y="4.107693359375" />
                  <Point X="-1.587924316406" Y="4.120106445313" />
                  <Point X="-1.579776489258" Y="4.126500976563" />
                  <Point X="-1.564226806641" Y="4.140137695312" />
                  <Point X="-1.55025402832" Y="4.155386230469" />
                  <Point X="-1.538024169922" Y="4.172064941406" />
                  <Point X="-1.532364868164" Y="4.180737304688" />
                  <Point X="-1.521543579102" Y="4.199478515625" />
                  <Point X="-1.516860473633" Y="4.20871875" />
                  <Point X="-1.508527587891" Y="4.227655273437" />
                  <Point X="-1.504877807617" Y="4.2373515625" />
                  <Point X="-1.473090942383" Y="4.338166992188" />
                  <Point X="-1.470027709961" Y="4.349759277344" />
                  <Point X="-1.465992675781" Y="4.37004296875" />
                  <Point X="-1.464528442383" Y="4.380295898438" />
                  <Point X="-1.462641113281" Y="4.401860839844" />
                  <Point X="-1.462302001953" Y="4.41221484375" />
                  <Point X="-1.462753051758" Y="4.432897460938" />
                  <Point X="-1.464196533203" Y="4.448189941406" />
                  <Point X="-1.479266113281" Y="4.562654296875" />
                  <Point X="-0.931171508789" Y="4.716321289062" />
                  <Point X="-0.365221984863" Y="4.782556640625" />
                  <Point X="-0.225666290283" Y="4.261727539062" />
                  <Point X="-0.221880706787" Y="4.250469238281" />
                  <Point X="-0.209743423462" Y="4.220680664062" />
                  <Point X="-0.20278074646" Y="4.206912109375" />
                  <Point X="-0.190678970337" Y="4.187151367188" />
                  <Point X="-0.176257644653" Y="4.169014160156" />
                  <Point X="-0.159731811523" Y="4.152770996094" />
                  <Point X="-0.150971542358" Y="4.145154785156" />
                  <Point X="-0.125779960632" Y="4.12582421875" />
                  <Point X="-0.112729232788" Y="4.117409667969" />
                  <Point X="-0.085433296204" Y="4.102819824219" />
                  <Point X="-0.056126941681" Y="4.092871582031" />
                  <Point X="-0.025589693069" Y="4.087829589844" />
                  <Point X="-0.010113739014" Y="4.086560546875" />
                  <Point X="0.022425760269" Y="4.086560546875" />
                  <Point X="0.037901714325" Y="4.087829589844" />
                  <Point X="0.068438964844" Y="4.092871582031" />
                  <Point X="0.09774546814" Y="4.102819824219" />
                  <Point X="0.125041252136" Y="4.117409667969" />
                  <Point X="0.13809197998" Y="4.12582421875" />
                  <Point X="0.163283554077" Y="4.145154785156" />
                  <Point X="0.173177383423" Y="4.153904296875" />
                  <Point X="0.191656707764" Y="4.172690917969" />
                  <Point X="0.207352035522" Y="4.193859375" />
                  <Point X="0.219959411621" Y="4.217" />
                  <Point X="0.225456985474" Y="4.229009765625" />
                  <Point X="0.235238449097" Y="4.255019042969" />
                  <Point X="0.240924758911" Y="4.272724609375" />
                  <Point X="0.378190246582" Y="4.785006347656" />
                  <Point X="0.827876281738" Y="4.737912109375" />
                  <Point X="1.453597045898" Y="4.58684375" />
                  <Point X="1.858252685547" Y="4.440072265625" />
                  <Point X="2.250449462891" Y="4.256655273438" />
                  <Point X="2.629435302734" Y="4.035856933594" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062372314453" Y="2.593103515625" />
                  <Point X="2.053181152344" Y="2.5734375" />
                  <Point X="2.044182006836" Y="2.549563232422" />
                  <Point X="2.041301391602" Y="2.540597900391" />
                  <Point X="2.020159057617" Y="2.461535888672" />
                  <Point X="2.018334960938" Y="2.453246582031" />
                  <Point X="2.014356079102" Y="2.428114990234" />
                  <Point X="2.01269140625" Y="2.407988769531" />
                  <Point X="2.012384643555" Y="2.398377441406" />
                  <Point X="2.013411010742" Y="2.369579589844" />
                  <Point X="2.021653564453" Y="2.301222900391" />
                  <Point X="2.023794067383" Y="2.289075439453" />
                  <Point X="2.029135986328" Y="2.267141357422" />
                  <Point X="2.032461303711" Y="2.256332763672" />
                  <Point X="2.040728881836" Y="2.234234619141" />
                  <Point X="2.045314208984" Y="2.223898925781" />
                  <Point X="2.055679931641" Y="2.203846435547" />
                  <Point X="2.061460205078" Y="2.194129638672" />
                  <Point X="2.103762939453" Y="2.131786376953" />
                  <Point X="2.108822021484" Y="2.125001953125" />
                  <Point X="2.128487548828" Y="2.102370117188" />
                  <Point X="2.157010742188" Y="2.075926269531" />
                  <Point X="2.168257324219" Y="2.066981689453" />
                  <Point X="2.230600585938" Y="2.024678955078" />
                  <Point X="2.241309814453" Y="2.0182265625" />
                  <Point X="2.261347412109" Y="2.007870605469" />
                  <Point X="2.271674316406" Y="2.003289550781" />
                  <Point X="2.293754150391" Y="1.995028320312" />
                  <Point X="2.304554199219" Y="1.991704956055" />
                  <Point X="2.326471679688" Y="1.986364379883" />
                  <Point X="2.337589111328" Y="1.984347167969" />
                  <Point X="2.405955322266" Y="1.976103149414" />
                  <Point X="2.414501953125" Y="1.975462036133" />
                  <Point X="2.444826904297" Y="1.975479248047" />
                  <Point X="2.483585693359" Y="1.979739746094" />
                  <Point X="2.497747558594" Y="1.982395751953" />
                  <Point X="2.576809570312" Y="2.003537841797" />
                  <Point X="2.582249267578" Y="2.005168334961" />
                  <Point X="2.601391601563" Y="2.011981323242" />
                  <Point X="2.626885253906" Y="2.023230224609" />
                  <Point X="2.636034423828" Y="2.027872924805" />
                  <Point X="2.658790283203" Y="2.041011108398" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043954833984" Y="2.637040527344" />
                  <Point X="4.136885253906" Y="2.483472412109" />
                  <Point X="3.172951660156" Y="1.743819946289" />
                  <Point X="3.168137207031" Y="1.739868530273" />
                  <Point X="3.152119140625" Y="1.725216552734" />
                  <Point X="3.134669433594" Y="1.706604003906" />
                  <Point X="3.128577148438" Y="1.699423217773" />
                  <Point X="3.071676025391" Y="1.62519128418" />
                  <Point X="3.066858642578" Y="1.618294311523" />
                  <Point X="3.053662841797" Y="1.596802368164" />
                  <Point X="3.044311035156" Y="1.578698486328" />
                  <Point X="3.040308349609" Y="1.569872558594" />
                  <Point X="3.030139648438" Y="1.542670410156" />
                  <Point X="3.008943847656" Y="1.466879272461" />
                  <Point X="3.006224121094" Y="1.454656005859" />
                  <Point X="3.002770507812" Y="1.432351196289" />
                  <Point X="3.001708496094" Y="1.421096191406" />
                  <Point X="3.000893798828" Y="1.39752331543" />
                  <Point X="3.00117578125" Y="1.38622668457" />
                  <Point X="3.003079101562" Y="1.363748291016" />
                  <Point X="3.004700439453" Y="1.35256640625" />
                  <Point X="3.022099853516" Y="1.268239257813" />
                  <Point X="3.024158203125" Y="1.260098632812" />
                  <Point X="3.031756591797" Y="1.236101074219" />
                  <Point X="3.039225585938" Y="1.217418701172" />
                  <Point X="3.043222167969" Y="1.208721435547" />
                  <Point X="3.056920898438" Y="1.183522338867" />
                  <Point X="3.104245605469" Y="1.111590576172" />
                  <Point X="3.111742675781" Y="1.101419311523" />
                  <Point X="3.126299316406" Y="1.084171508789" />
                  <Point X="3.134091552734" Y="1.075981567383" />
                  <Point X="3.151332763672" Y="1.059895629883" />
                  <Point X="3.160039550781" Y="1.05269152832" />
                  <Point X="3.17824609375" Y="1.039368774414" />
                  <Point X="3.18774609375" Y="1.03324987793" />
                  <Point X="3.256326171875" Y="0.994645202637" />
                  <Point X="3.263919921875" Y="0.990807800293" />
                  <Point X="3.287325683594" Y="0.980685668945" />
                  <Point X="3.306361328125" Y="0.974090209961" />
                  <Point X="3.315576416016" Y="0.971410522461" />
                  <Point X="3.343668945312" Y="0.965257873535" />
                  <Point X="3.436393798828" Y="0.953002868652" />
                  <Point X="3.441974853516" Y="0.952432434082" />
                  <Point X="3.462081542969" Y="0.951395935059" />
                  <Point X="3.490262451172" Y="0.952006774902" />
                  <Point X="3.500604003906" Y="0.952797180176" />
                  <Point X="3.522220703125" Y="0.955643127441" />
                  <Point X="4.704703613281" Y="1.111319824219" />
                  <Point X="4.752684570312" Y="0.914228149414" />
                  <Point X="4.78387109375" Y="0.713920959473" />
                  <Point X="3.691991943359" Y="0.421352722168" />
                  <Point X="3.686031738281" Y="0.419544433594" />
                  <Point X="3.665627197266" Y="0.412138122559" />
                  <Point X="3.642382324219" Y="0.401619934082" />
                  <Point X="3.634005126953" Y="0.397316986084" />
                  <Point X="3.542905761719" Y="0.34466003418" />
                  <Point X="3.535873779297" Y="0.340172241211" />
                  <Point X="3.515620361328" Y="0.325511016846" />
                  <Point X="3.499894042969" Y="0.312270965576" />
                  <Point X="3.492758300781" Y="0.30560760498" />
                  <Point X="3.472794189453" Y="0.284222991943" />
                  <Point X="3.418134521484" Y="0.21457383728" />
                  <Point X="3.410851318359" Y="0.204204269409" />
                  <Point X="3.399126708984" Y="0.184922973633" />
                  <Point X="3.393838867188" Y="0.174933135986" />
                  <Point X="3.384068603516" Y="0.153476455688" />
                  <Point X="3.380005615234" Y="0.142931060791" />
                  <Point X="3.373159667969" Y="0.121431129456" />
                  <Point X="3.370376953125" Y="0.1104764328" />
                  <Point X="3.352156982422" Y="0.015338897705" />
                  <Point X="3.350950927734" Y="0.007101027012" />
                  <Point X="3.348784423828" Y="-0.01775630188" />
                  <Point X="3.348502197266" Y="-0.038045410156" />
                  <Point X="3.348857666016" Y="-0.047682563782" />
                  <Point X="3.351874755859" Y="-0.076425933838" />
                  <Point X="3.370094726562" Y="-0.171563461304" />
                  <Point X="3.373159667969" Y="-0.183991165161" />
                  <Point X="3.380005615234" Y="-0.205491104126" />
                  <Point X="3.384068847656" Y="-0.216036499023" />
                  <Point X="3.393839111328" Y="-0.237493026733" />
                  <Point X="3.399126708984" Y="-0.247483016968" />
                  <Point X="3.410851318359" Y="-0.266764312744" />
                  <Point X="3.417288330078" Y="-0.276055480957" />
                  <Point X="3.471947998047" Y="-0.345704772949" />
                  <Point X="3.477450195312" Y="-0.352108184814" />
                  <Point X="3.495052978516" Y="-0.370293395996" />
                  <Point X="3.51021484375" Y="-0.383796234131" />
                  <Point X="3.517747802734" Y="-0.389846954346" />
                  <Point X="3.541495117188" Y="-0.406404449463" />
                  <Point X="3.632594482422" Y="-0.459061553955" />
                  <Point X="3.655489013672" Y="-0.470284790039" />
                  <Point X="3.682037353516" Y="-0.480647369385" />
                  <Point X="3.691992919922" Y="-0.483913085938" />
                  <Point X="3.71181640625" Y="-0.489224700928" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.761614257812" Y="-0.931048400879" />
                  <Point X="4.727801757812" Y="-1.079219726563" />
                  <Point X="3.436781982422" Y="-0.909253662109" />
                  <Point X="3.428624267578" Y="-0.908535827637" />
                  <Point X="3.400097412109" Y="-0.908042419434" />
                  <Point X="3.366721435547" Y="-0.910840820312" />
                  <Point X="3.354481445312" Y="-0.912676147461" />
                  <Point X="3.175685546875" Y="-0.951538391113" />
                  <Point X="3.157874267578" Y="-0.956742431641" />
                  <Point X="3.128754638672" Y="-0.968366638184" />
                  <Point X="3.114677978516" Y="-0.975388183594" />
                  <Point X="3.0868515625" Y="-0.992280090332" />
                  <Point X="3.074125488281" Y="-1.00152935791" />
                  <Point X="3.050374755859" Y="-1.022000671387" />
                  <Point X="3.039350097656" Y="-1.03322253418" />
                  <Point X="2.931279296875" Y="-1.163197998047" />
                  <Point X="2.921326660156" Y="-1.176847045898" />
                  <Point X="2.906605712891" Y="-1.201229736328" />
                  <Point X="2.9001640625" Y="-1.213976318359" />
                  <Point X="2.888820068359" Y="-1.241363891602" />
                  <Point X="2.884362304688" Y="-1.254930664062" />
                  <Point X="2.877530761719" Y="-1.282579711914" />
                  <Point X="2.875156982422" Y="-1.296661865234" />
                  <Point X="2.859667724609" Y="-1.464985961914" />
                  <Point X="2.859288574219" Y="-1.483323120117" />
                  <Point X="2.861607421875" Y="-1.514592285156" />
                  <Point X="2.864065673828" Y="-1.530130371094" />
                  <Point X="2.871796630859" Y="-1.561748291016" />
                  <Point X="2.876785400391" Y="-1.576667724609" />
                  <Point X="2.889156738281" Y="-1.605478759766" />
                  <Point X="2.896539306641" Y="-1.619370239258" />
                  <Point X="2.995487548828" Y="-1.77327746582" />
                  <Point X="2.999506591797" Y="-1.779047851562" />
                  <Point X="3.015454589844" Y="-1.799060913086" />
                  <Point X="3.042230224609" Y="-1.826838989258" />
                  <Point X="3.052795654297" Y="-1.836277832031" />
                  <Point X="3.071191894531" Y="-1.850393798828" />
                  <Point X="4.087170898438" Y="-2.629981445312" />
                  <Point X="4.045485839844" Y="-2.697433837891" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841192138672" Y="-2.090885253906" />
                  <Point X="2.815024414062" Y="-2.079512451172" />
                  <Point X="2.7831171875" Y="-2.069325439453" />
                  <Point X="2.771107177734" Y="-2.066337402344" />
                  <Point X="2.558311767578" Y="-2.027906616211" />
                  <Point X="2.539357177734" Y="-2.025807250977" />
                  <Point X="2.508003173828" Y="-2.025403198242" />
                  <Point X="2.492308837891" Y="-2.02650378418" />
                  <Point X="2.46013671875" Y="-2.031462158203" />
                  <Point X="2.444841552734" Y="-2.035136962891" />
                  <Point X="2.415067382812" Y="-2.044960449219" />
                  <Point X="2.400588378906" Y="-2.051108886719" />
                  <Point X="2.223807617188" Y="-2.144147216797" />
                  <Point X="2.208966796875" Y="-2.153171142578" />
                  <Point X="2.186033691406" Y="-2.17006640625" />
                  <Point X="2.175204101562" Y="-2.179378417969" />
                  <Point X="2.154243652344" Y="-2.200340332031" />
                  <Point X="2.144934326172" Y="-2.21116796875" />
                  <Point X="2.128043701172" Y="-2.234096435547" />
                  <Point X="2.120462402344" Y="-2.246197265625" />
                  <Point X="2.027423950195" Y="-2.422978027344" />
                  <Point X="2.019834350586" Y="-2.440196289062" />
                  <Point X="2.010010620117" Y="-2.469973388672" />
                  <Point X="2.00633581543" Y="-2.48526953125" />
                  <Point X="2.00137902832" Y="-2.517438720703" />
                  <Point X="2.000278930664" Y="-2.533131103516" />
                  <Point X="2.000683227539" Y="-2.564482177734" />
                  <Point X="2.00218762207" Y="-2.580140869141" />
                  <Point X="2.040618286133" Y="-2.792936279297" />
                  <Point X="2.042079467773" Y="-2.799635009766" />
                  <Point X="2.047422119141" Y="-2.819486572266" />
                  <Point X="2.053920166016" Y="-2.838896484375" />
                  <Point X="2.057391845703" Y="-2.847763183594" />
                  <Point X="2.069547119141" Y="-2.873579101562" />
                  <Point X="2.081368652344" Y="-2.894054443359" />
                  <Point X="2.735893554688" Y="-4.027724609375" />
                  <Point X="2.723754394531" Y="-4.036083740234" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828654541016" Y="-2.870147705078" />
                  <Point X="1.808833251953" Y="-2.849628173828" />
                  <Point X="1.783253662109" Y="-2.828005859375" />
                  <Point X="1.773300292969" Y="-2.820647949219" />
                  <Point X="1.563426513672" Y="-2.685718505859" />
                  <Point X="1.546285644531" Y="-2.676246826172" />
                  <Point X="1.517475097656" Y="-2.663875488281" />
                  <Point X="1.502555541992" Y="-2.658886474609" />
                  <Point X="1.470931884766" Y="-2.651154052734" />
                  <Point X="1.455393432617" Y="-2.648695800781" />
                  <Point X="1.424124023438" Y="-2.646376953125" />
                  <Point X="1.408393188477" Y="-2.646516357422" />
                  <Point X="1.178860595703" Y="-2.667638183594" />
                  <Point X="1.161223632812" Y="-2.670339355469" />
                  <Point X="1.133574462891" Y="-2.677171142578" />
                  <Point X="1.1200078125" Y="-2.681628662109" />
                  <Point X="1.092624755859" Y="-2.692970947266" />
                  <Point X="1.07987902832" Y="-2.699412109375" />
                  <Point X="1.055496704102" Y="-2.714132568359" />
                  <Point X="1.043859863281" Y="-2.722412109375" />
                  <Point X="0.866620605469" Y="-2.869780761719" />
                  <Point X="0.852654418945" Y="-2.883086914062" />
                  <Point X="0.832183959961" Y="-2.906836425781" />
                  <Point X="0.82293548584" Y="-2.919561279297" />
                  <Point X="0.806041809082" Y="-2.947389404297" />
                  <Point X="0.799019348145" Y="-2.961467285156" />
                  <Point X="0.787394348145" Y="-2.990588378906" />
                  <Point X="0.782791687012" Y="-3.005631835938" />
                  <Point X="0.729798095703" Y="-3.249444335938" />
                  <Point X="0.728608337402" Y="-3.256027099609" />
                  <Point X="0.725971496582" Y="-3.275909667969" />
                  <Point X="0.724437866211" Y="-3.296830566406" />
                  <Point X="0.724223266602" Y="-3.306516601562" />
                  <Point X="0.725554931641" Y="-3.335520263672" />
                  <Point X="0.728905029297" Y="-3.360966796875" />
                  <Point X="0.833091369629" Y="-4.152340332031" />
                  <Point X="0.655065002441" Y="-3.487936523438" />
                  <Point X="0.652606323242" Y="-3.480124267578" />
                  <Point X="0.642144897461" Y="-3.453579833984" />
                  <Point X="0.626786804199" Y="-3.423815185547" />
                  <Point X="0.620407348633" Y="-3.413209716797" />
                  <Point X="0.459175994873" Y="-3.18090625" />
                  <Point X="0.446670288086" Y="-3.165172119141" />
                  <Point X="0.424786010742" Y="-3.142716552734" />
                  <Point X="0.41291104126" Y="-3.132397949219" />
                  <Point X="0.386655761719" Y="-3.113154296875" />
                  <Point X="0.373242095947" Y="-3.1049375" />
                  <Point X="0.345241546631" Y="-3.090829345703" />
                  <Point X="0.330654510498" Y="-3.084938232422" />
                  <Point X="0.081158554077" Y="-3.00750390625" />
                  <Point X="0.063374637604" Y="-3.003108886719" />
                  <Point X="0.035212085724" Y="-2.998839599609" />
                  <Point X="0.020970146179" Y="-2.997766357422" />
                  <Point X="-0.008673583031" Y="-2.997767333984" />
                  <Point X="-0.022912399292" Y="-2.998840820312" />
                  <Point X="-0.051068561554" Y="-3.003110107422" />
                  <Point X="-0.064986053467" Y="-3.006305908203" />
                  <Point X="-0.314481842041" Y="-3.083739990234" />
                  <Point X="-0.332934112549" Y="-3.090831298828" />
                  <Point X="-0.360938537598" Y="-3.104942138672" />
                  <Point X="-0.374353851318" Y="-3.113160644531" />
                  <Point X="-0.400607330322" Y="-3.132404785156" />
                  <Point X="-0.412480377197" Y="-3.14272265625" />
                  <Point X="-0.434360931396" Y="-3.165175537109" />
                  <Point X="-0.44436831665" Y="-3.177310791016" />
                  <Point X="-0.605599609375" Y="-3.409614013672" />
                  <Point X="-0.618975708008" Y="-3.43203125" />
                  <Point X="-0.628691772461" Y="-3.451195556641" />
                  <Point X="-0.632683654785" Y="-3.460197998047" />
                  <Point X="-0.642752807617" Y="-3.487936523438" />
                  <Point X="-0.648860961914" Y="-3.510731933594" />
                  <Point X="-0.985425170898" Y="-4.766807128906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.663115466326" Y="-3.957057616754" />
                  <Point X="2.689540935077" Y="-3.947439532702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.602476538121" Y="-3.878031493273" />
                  <Point X="2.64130816329" Y="-3.863897937563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.541837609916" Y="-3.799005369792" />
                  <Point X="2.593075391502" Y="-3.780356342425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.481198681711" Y="-3.719979246311" />
                  <Point X="2.544842619715" Y="-3.696814747286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420559753506" Y="-3.64095312283" />
                  <Point X="2.496609847927" Y="-3.613273152148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.824621481296" Y="-4.120730261256" />
                  <Point X="0.828732817932" Y="-4.119233857097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.359920825301" Y="-3.56192699935" />
                  <Point X="2.44837707614" Y="-3.52973155701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.135094090041" Y="-4.732911508466" />
                  <Point X="-0.959187492302" Y="-4.668886742877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.799939762799" Y="-4.028616783734" />
                  <Point X="0.816031759569" Y="-4.0227597759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.299281897096" Y="-3.482900875869" />
                  <Point X="2.400144304352" Y="-3.446189961871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.127795315762" Y="-4.629158083496" />
                  <Point X="-0.92917133191" Y="-4.556864865562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.775258044302" Y="-3.936503306213" />
                  <Point X="0.803330701206" Y="-3.926285694703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.238642968891" Y="-3.403874752388" />
                  <Point X="2.351911532565" Y="-3.362648366733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.120586675775" Y="-4.525437464726" />
                  <Point X="-0.899155171517" Y="-4.444842988247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.750576325804" Y="-3.844389828691" />
                  <Point X="0.790629642843" Y="-3.829811613505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.178004040686" Y="-3.324848628907" />
                  <Point X="2.303678760778" Y="-3.279106771594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.896082371038" Y="-2.699519256522" />
                  <Point X="4.087159650395" Y="-2.629972814391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.13675385947" Y="-4.430224949978" />
                  <Point X="-0.869139011125" Y="-4.332821110932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725894607307" Y="-3.752276351169" />
                  <Point X="0.77792858448" Y="-3.733337532308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.117365112481" Y="-3.245822505426" />
                  <Point X="2.25544598899" Y="-3.195565176456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.788683408749" Y="-2.637512393601" />
                  <Point X="3.99779592964" Y="-2.561401660384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.155505728228" Y="-4.335953183657" />
                  <Point X="-0.839122850732" Y="-4.220799233617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.70121288881" Y="-3.660162873648" />
                  <Point X="0.765227526117" Y="-3.63686345111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.056726184276" Y="-3.166796381946" />
                  <Point X="2.207213217203" Y="-3.112023581317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.68128444646" Y="-2.57550553068" />
                  <Point X="3.908432208884" Y="-2.492830506377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.174257596987" Y="-4.241681417337" />
                  <Point X="-0.80910669034" Y="-4.108777356302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.676531170313" Y="-3.568049396126" />
                  <Point X="0.752526467755" Y="-3.540389369913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.996087256071" Y="-3.087770258465" />
                  <Point X="2.158980445415" Y="-3.028481986179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573885484172" Y="-2.513498667758" />
                  <Point X="3.819068488129" Y="-2.42425935237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.198994894715" Y="-4.149588169001" />
                  <Point X="-0.779090529948" Y="-3.996755478987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.651067779598" Y="-3.476220424024" />
                  <Point X="0.739825409392" Y="-3.443915288715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.935448327866" Y="-3.008744134984" />
                  <Point X="2.110747673628" Y="-2.944940391041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.466486521883" Y="-2.451491804837" />
                  <Point X="3.729704767374" Y="-2.355688198363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.252931992009" Y="-4.068122778554" />
                  <Point X="-0.749074369555" Y="-3.884733601672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.605487680123" Y="-3.391713335123" />
                  <Point X="0.727124352659" Y="-3.347441206925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.874809399661" Y="-2.929718011503" />
                  <Point X="2.06362231306" Y="-2.860995731181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.359087559594" Y="-2.389484941916" />
                  <Point X="3.640341046618" Y="-2.287117044356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.333382297415" Y="-3.996307406674" />
                  <Point X="-0.719058209163" Y="-3.772711724357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.549471449587" Y="-3.311004687289" />
                  <Point X="0.730759472023" Y="-3.245021243293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.810981843595" Y="-2.851852453652" />
                  <Point X="2.036438612303" Y="-2.769792900729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.251688597305" Y="-2.327478078995" />
                  <Point X="3.550977325863" Y="-2.218545890349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.414849715229" Y="-3.924862233436" />
                  <Point X="-0.68904204877" Y="-3.660689847042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.493455219051" Y="-3.230296039454" />
                  <Point X="0.754620962993" Y="-3.135239482449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.717506610361" Y="-2.784777767805" />
                  <Point X="2.019306728183" Y="-2.67493150822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.144289635016" Y="-2.265471216074" />
                  <Point X="3.461613605108" Y="-2.149974736342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.496837001441" Y="-3.85360627682" />
                  <Point X="-0.659025888378" Y="-3.548667969727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.433041784161" Y="-3.151187843119" />
                  <Point X="0.778482453964" Y="-3.025457721605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.617100297851" Y="-2.720225788506" />
                  <Point X="2.00218062244" Y="-2.580068012554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.036890672728" Y="-2.203464353153" />
                  <Point X="3.372249884352" Y="-2.081403582335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.547634168737" Y="-4.134968279582" />
                  <Point X="-2.456695212524" Y="-4.101869206385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.622589861233" Y="-3.798279686273" />
                  <Point X="-0.619574264946" Y="-3.433211864719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.33380448262" Y="-3.086210378623" />
                  <Point X="0.834722650934" Y="-2.903891075554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.505323877397" Y="-2.659812190059" />
                  <Point X="2.008392111921" Y="-2.476710326887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.929491710439" Y="-2.141457490232" />
                  <Point X="3.282886163597" Y="-2.012832428328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.65046427697" Y="-4.071298489779" />
                  <Point X="-2.271214191904" Y="-3.933262747472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.855223236881" Y="-3.78185442212" />
                  <Point X="-0.528907044594" Y="-3.299114806902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.184319977868" Y="-3.039521400452" />
                  <Point X="1.034742464121" Y="-2.729992928905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.215264582668" Y="-2.664288251127" />
                  <Point X="2.062767461503" Y="-2.355822429777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.818168535949" Y="-2.080878923745" />
                  <Point X="3.193522442842" Y="-1.944261274321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75253556465" Y="-4.007352511883" />
                  <Point X="-0.431990618416" Y="-3.162743224177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.02122651756" Y="-2.997785677022" />
                  <Point X="2.131193688538" Y="-2.229820431508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.643618622635" Y="-2.0433130082" />
                  <Point X="3.104158722086" Y="-1.875690120314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.841691264384" Y="-3.938705644416" />
                  <Point X="3.021033249423" Y="-1.804848429688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.930846964119" Y="-3.87005877695" />
                  <Point X="2.964108997669" Y="-1.724470274549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948422069179" Y="-3.77535870367" />
                  <Point X="2.911437960003" Y="-1.642544076082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.53051864716" Y="-1.053246899082" />
                  <Point X="4.752135685213" Y="-0.972584893824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.874525166308" Y="-3.647365542236" />
                  <Point X="2.870478003175" Y="-1.556355392779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.326539151856" Y="-1.026392475387" />
                  <Point X="4.771665549316" Y="-0.864379716226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.800628263437" Y="-3.519372380801" />
                  <Point X="2.860219256404" Y="-1.458992382859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.122559656553" Y="-0.999538051693" />
                  <Point X="4.755955202596" Y="-0.769000926417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.726731360566" Y="-3.391379219366" />
                  <Point X="2.869844616083" Y="-1.354392150057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.91858016125" Y="-0.972683627999" />
                  <Point X="4.595971394331" Y="-0.726133382205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.652834457696" Y="-3.263386057932" />
                  <Point X="2.886944720021" Y="-1.247071332835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.714600665946" Y="-0.945829204305" />
                  <Point X="4.435987586065" Y="-0.683265837993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.578937554825" Y="-3.135392896497" />
                  <Point X="2.97105431788" Y="-1.115361054413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.510621170643" Y="-0.918974780611" />
                  <Point X="4.2760037778" Y="-0.640398293781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.505040651954" Y="-3.007399735062" />
                  <Point X="4.116019969534" Y="-0.597530749569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.431143749083" Y="-2.879406573628" />
                  <Point X="3.956036161269" Y="-0.554663205357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.357246846212" Y="-2.751413412193" />
                  <Point X="3.796052353003" Y="-0.511795661145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.313377776108" Y="-2.634349488085" />
                  <Point X="3.645625639524" Y="-0.465449618904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.321193768942" Y="-2.536097388442" />
                  <Point X="3.53758324207" Y="-0.403676947231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.821091830335" Y="-2.980918748838" />
                  <Point X="-3.572909802695" Y="-2.890587878097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.365969825054" Y="-2.45129765169" />
                  <Point X="3.460206225943" Y="-0.330742989533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.881348813341" Y="-2.901753608674" />
                  <Point X="-3.099122208882" Y="-2.617046408199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.448704865738" Y="-2.380313855444" />
                  <Point X="3.401314532046" Y="-0.251080924771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.941605796348" Y="-2.82258846851" />
                  <Point X="3.368265668529" Y="-0.162012838983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.001862779354" Y="-2.743423328345" />
                  <Point X="3.350909802879" Y="-0.067232969084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.051782647849" Y="-2.660495786191" />
                  <Point X="3.356064017244" Y="0.035739899911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.101319961127" Y="-2.577429005324" />
                  <Point X="3.381179600418" Y="0.145978112988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.150857274404" Y="-2.494362224458" />
                  <Point X="3.468616267287" Y="0.278899345497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.328845461112" Y="0.591997166696" />
                  <Point X="4.777433348101" Y="0.755269805012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.084475034167" Y="-2.369104176542" />
                  <Point X="4.762537345339" Y="0.850944991783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.833836629996" Y="-2.176782369475" />
                  <Point X="4.745030256678" Y="0.945669821007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.583198225826" Y="-1.984460562407" />
                  <Point X="4.722421982173" Y="1.038537970424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.332559821655" Y="-1.79213875534" />
                  <Point X="4.610582236481" Y="1.098928520369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.081921417484" Y="-1.599816948273" />
                  <Point X="4.175415979424" Y="1.041637844229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.966449326138" Y="-1.456691655749" />
                  <Point X="3.740249722367" Y="0.984347168088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948845419071" Y="-1.349187469185" />
                  <Point X="3.392360110132" Y="0.95882259281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.968829245604" Y="-1.255364098824" />
                  <Point X="3.239320836927" Y="1.004217741074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.028126431966" Y="-1.17584962125" />
                  <Point X="3.141005597438" Y="1.069530808711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.134316732883" Y="-1.113402841567" />
                  <Point X="3.080010352228" Y="1.148427243408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.45029362752" Y="-1.127312137545" />
                  <Point X="3.033201426016" Y="1.232487075954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.885460050585" Y="-1.184602874108" />
                  <Point X="3.010334415611" Y="1.325261053205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.320626473651" Y="-1.241893610671" />
                  <Point X="3.001915864102" Y="1.423293839425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.666071805198" Y="-1.266528540535" />
                  <Point X="3.027646627194" Y="1.533755959681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.689698393773" Y="-1.174031027128" />
                  <Point X="3.098984925718" Y="1.660817865292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.713324982348" Y="-1.081533513722" />
                  <Point X="3.284556270549" Y="1.829457199529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.736951570923" Y="-0.989036000315" />
                  <Point X="3.535195026998" Y="2.021779134815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.752288699667" Y="-0.893521370272" />
                  <Point X="3.785833783446" Y="2.2141010701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.766032240438" Y="-0.797426721641" />
                  <Point X="4.036472539894" Y="2.406423005386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.779775781209" Y="-0.701332073009" />
                  <Point X="-4.275027121912" Y="-0.517618585239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.333509802778" Y="-0.174934306029" />
                  <Point X="4.106835453591" Y="2.533129899953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.296738266172" Y="-0.060453672851" />
                  <Point X="2.326722884984" Y="1.986318799722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.968896058675" Y="2.22005072019" />
                  <Point X="4.056700041245" Y="2.615978990562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.303778130223" Y="0.038080914567" />
                  <Point X="2.203981247076" Y="2.042741385404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.442684298975" Y="2.49359242539" />
                  <Point X="4.000963051593" Y="2.696789273766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.341514257119" Y="0.125442976006" />
                  <Point X="2.11925159582" Y="2.112999202772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.916472539275" Y="2.76713413059" />
                  <Point X="3.943316651078" Y="2.776904588252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.41284710295" Y="0.200576831783" />
                  <Point X="2.062041304922" Y="2.193273248177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.535704589211" Y="0.256957252112" />
                  <Point X="2.025723856535" Y="2.281151666365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.695688405579" Y="0.299824793375" />
                  <Point X="2.013122943308" Y="2.377662197411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.855672221948" Y="0.342692334638" />
                  <Point X="2.026020082202" Y="2.483453260461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.015656038316" Y="0.3855598759" />
                  <Point X="2.065492864389" Y="2.598917066626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.175639854684" Y="0.428427417163" />
                  <Point X="2.139389709347" Y="2.726910206982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.335623671053" Y="0.471294958426" />
                  <Point X="2.213286554304" Y="2.854903347338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.495607487421" Y="0.514162499689" />
                  <Point X="2.287183399262" Y="2.982896487694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.655591303789" Y="0.557030040952" />
                  <Point X="2.361080244219" Y="3.11088962805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.78246862011" Y="0.611947362793" />
                  <Point X="2.434977089177" Y="3.238882768405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.766657225324" Y="0.718799128242" />
                  <Point X="2.508873934134" Y="3.366875908761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.750845830537" Y="0.825650893692" />
                  <Point X="-3.69455340428" Y="1.210109895531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.562004496228" Y="1.258353752646" />
                  <Point X="2.582770779092" Y="3.494869049117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.735034435751" Y="0.932502659142" />
                  <Point X="-3.929804330573" Y="1.225582449162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.451780746563" Y="1.399568805019" />
                  <Point X="2.656667624049" Y="3.622862189473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708056734287" Y="1.043418627849" />
                  <Point X="-4.133783779125" Y="1.252436889872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421724152039" Y="1.511605399154" />
                  <Point X="2.730564469007" Y="3.750855329829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.677663684142" Y="1.155577681815" />
                  <Point X="-4.337763227678" Y="1.279291330582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.437024155439" Y="1.607133541717" />
                  <Point X="2.804461313964" Y="3.878848470185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.647270633998" Y="1.267736735782" />
                  <Point X="-4.541742676231" Y="1.306145771292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.479074713083" Y="1.692925278786" />
                  <Point X="2.740693947284" Y="3.956735935181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.544960184873" Y="1.770041816569" />
                  <Point X="2.646660386204" Y="4.023607406311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.633951564999" Y="1.838748491482" />
                  <Point X="2.542196483443" Y="4.086682543536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.723315238492" Y="1.907319662691" />
                  <Point X="2.435393470317" Y="4.148906314214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.812678911984" Y="1.9758908339" />
                  <Point X="2.328590457191" Y="4.211130084891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.902042585477" Y="2.044462005109" />
                  <Point X="2.217826169764" Y="4.271912069633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.99140625897" Y="2.113033176319" />
                  <Point X="2.096262297196" Y="4.328763326841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.080769932463" Y="2.181604347528" />
                  <Point X="1.974698424628" Y="4.38561458405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.170133605956" Y="2.250175518737" />
                  <Point X="1.852395307699" Y="4.442196778315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.200881939335" Y="2.340080929019" />
                  <Point X="-3.131962062977" Y="2.729135946829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.834584504967" Y="2.837372526283" />
                  <Point X="1.713273196827" Y="4.492657359414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.125954970137" Y="2.468449003936" />
                  <Point X="-3.278972214232" Y="2.776725516022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75544452155" Y="2.967274012972" />
                  <Point X="1.574151085956" Y="4.543117940514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.051028000938" Y="2.596817078853" />
                  <Point X="-3.386371232659" Y="2.83873235851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75103501351" Y="3.069975831032" />
                  <Point X="0.051799910765" Y="4.09012431503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.121360654639" Y="4.115442355273" />
                  <Point X="1.431309351361" Y="4.592224689296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.963937720341" Y="2.729612237069" />
                  <Point X="-3.493770251086" Y="2.900739200999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.763345102699" Y="3.166592213371" />
                  <Point X="-0.126454572859" Y="4.126341877251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.236317940106" Y="4.25838027378" />
                  <Point X="1.264317791407" Y="4.632541620484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.854215307867" Y="2.870644817627" />
                  <Point X="-3.601169269513" Y="2.962746043487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.801766722562" Y="3.253704775774" />
                  <Point X="-0.199148125093" Y="4.2009804764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.267179862564" Y="4.370709983313" />
                  <Point X="1.097326231453" Y="4.672858551672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.744492895393" Y="3.011677398185" />
                  <Point X="-3.70856828794" Y="3.024752885976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.849999452432" Y="3.337246386169" />
                  <Point X="-0.23316087932" Y="4.289697734662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.297196037659" Y="4.482731865979" />
                  <Point X="0.930334671499" Y="4.713175482861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.898232182301" Y="3.420787996565" />
                  <Point X="-0.25784260318" Y="4.381811210231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.327212212753" Y="4.594753748645" />
                  <Point X="0.744520843174" Y="4.746641668621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.94646491217" Y="3.50432960696" />
                  <Point X="-0.282524327041" Y="4.473924685801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.357228387848" Y="4.706775631311" />
                  <Point X="0.52882316902" Y="4.769231024013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.994697642039" Y="3.587871217355" />
                  <Point X="-0.307206050901" Y="4.56603816137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.042930371908" Y="3.671412827751" />
                  <Point X="-1.961755110352" Y="4.064928440982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.507552978038" Y="4.230244497485" />
                  <Point X="-0.331887774762" Y="4.65815163694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.894002563238" Y="3.826715005547" />
                  <Point X="-2.070940854819" Y="4.126284968375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.47141932076" Y="4.344492961574" />
                  <Point X="-0.356569498622" Y="4.75026511251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.637011801447" Y="4.021348881705" />
                  <Point X="-2.141440771896" Y="4.201721985426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.464200149955" Y="4.448217413249" />
                  <Point X="-0.631104875878" Y="4.751439295321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.47690125361" Y="4.544691477961" />
                  <Point X="-1.254115836457" Y="4.625778738433" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000163818359" Y="0.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.471539093018" Y="-3.537112304688" />
                  <Point X="0.46431842041" Y="-3.521543701172" />
                  <Point X="0.303087188721" Y="-3.289240234375" />
                  <Point X="0.300590789795" Y="-3.285643310547" />
                  <Point X="0.274335540771" Y="-3.266399658203" />
                  <Point X="0.024839710236" Y="-3.188965332031" />
                  <Point X="0.020976411819" Y="-3.187766357422" />
                  <Point X="-0.008667250633" Y="-3.187767333984" />
                  <Point X="-0.25816305542" Y="-3.265201416016" />
                  <Point X="-0.262026367188" Y="-3.266400634766" />
                  <Point X="-0.288279663086" Y="-3.285644775391" />
                  <Point X="-0.449510925293" Y="-3.517947998047" />
                  <Point X="-0.459227050781" Y="-3.537112304688" />
                  <Point X="-0.465335113525" Y="-3.559907714844" />
                  <Point X="-0.847744018555" Y="-4.987077148438" />
                  <Point X="-1.097147216797" Y="-4.938667480469" />
                  <Point X="-1.100236083984" Y="-4.938067871094" />
                  <Point X="-1.351589477539" Y="-4.873396972656" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.369287841797" Y="-4.235105957031" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386283081055" Y="-4.202628417969" />
                  <Point X="-1.615987182617" Y="-4.001182861328" />
                  <Point X="-1.619543945312" Y="-3.998063720703" />
                  <Point X="-1.649241577148" Y="-3.985762939453" />
                  <Point X="-1.954109985352" Y="-3.965780761719" />
                  <Point X="-1.958830688477" Y="-3.965471435547" />
                  <Point X="-1.98987902832" Y="-3.973791503906" />
                  <Point X="-2.243911865234" Y="-4.143530761719" />
                  <Point X="-2.247845458984" Y="-4.146159179688" />
                  <Point X="-2.263163330078" Y="-4.161763183594" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.851299316406" Y="-4.170418945312" />
                  <Point X="-2.855833496094" Y="-4.167611816406" />
                  <Point X="-3.228581298828" Y="-3.880608398438" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531063720703" Y="-2.551680419922" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-2.611344238281" Y="-2.554820800781" />
                  <Point X="-3.842959228516" Y="-3.265894042969" />
                  <Point X="-4.158119628906" Y="-2.851838623047" />
                  <Point X="-4.161701171875" Y="-2.847133056641" />
                  <Point X="-4.43101953125" Y="-2.395526855469" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013671875" />
                  <Point X="-3.138234863281" Y="-1.366719970703" />
                  <Point X="-3.138115966797" Y="-1.366260742188" />
                  <Point X="-3.140327148438" Y="-1.334593261719" />
                  <Point X="-3.161158447266" Y="-1.310639526367" />
                  <Point X="-3.187237304688" Y="-1.295290649414" />
                  <Point X="-3.187641113281" Y="-1.295052978516" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.244349121094" Y="-1.291838500977" />
                  <Point X="-4.803283691406" Y="-1.497076171875" />
                  <Point X="-4.925991210938" Y="-1.016679321289" />
                  <Point X="-4.927392089844" Y="-1.011195617676" />
                  <Point X="-4.998395996094" Y="-0.514742126465" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541892089844" Y="-0.121422485352" />
                  <Point X="-3.514562255859" Y="-0.102454040527" />
                  <Point X="-3.514139160156" Y="-0.102160453796" />
                  <Point X="-3.494898925781" Y="-0.075908073425" />
                  <Point X="-3.485788818359" Y="-0.04655556488" />
                  <Point X="-3.485642333984" Y="-0.046082633972" />
                  <Point X="-3.485647949219" Y="-0.016459354401" />
                  <Point X="-3.4947578125" Y="0.012893157005" />
                  <Point X="-3.494900390625" Y="0.013352852821" />
                  <Point X="-3.514139160156" Y="0.0396003685" />
                  <Point X="-3.541468994141" Y="0.058568660736" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-3.580088378906" Y="0.072147476196" />
                  <Point X="-4.998186523438" Y="0.452125762939" />
                  <Point X="-4.918547363281" Y="0.99032019043" />
                  <Point X="-4.917645019531" Y="0.996418029785" />
                  <Point X="-4.773516113281" Y="1.528298706055" />
                  <Point X="-3.753266357422" Y="1.393980224609" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.671215087891" Y="1.414938964844" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651532714844" Y="1.426057861328" />
                  <Point X="-3.639118164062" Y="1.443790405273" />
                  <Point X="-3.614846435547" Y="1.502387329102" />
                  <Point X="-3.614470703125" Y="1.503294677734" />
                  <Point X="-3.610714111328" Y="1.52460559082" />
                  <Point X="-3.616313964844" Y="1.545508056641" />
                  <Point X="-3.645600341797" Y="1.601766723633" />
                  <Point X="-3.646053710938" Y="1.602637817383" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-3.672945800781" Y="1.629179931641" />
                  <Point X="-4.47610546875" Y="2.245466064453" />
                  <Point X="-4.163521484375" Y="2.780997802734" />
                  <Point X="-4.160014160156" Y="2.787006835938" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138510498047" Y="2.920434570312" />
                  <Point X="-3.054265625" Y="2.913063964844" />
                  <Point X="-3.052961181641" Y="2.912949951172" />
                  <Point X="-3.031504638672" Y="2.915776123047" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.953459716797" Y="2.987197509766" />
                  <Point X="-2.952533447266" Y="2.988123291016" />
                  <Point X="-2.940901123047" Y="3.006377197266" />
                  <Point X="-2.938073974609" Y="3.027836914062" />
                  <Point X="-2.945444580078" Y="3.11208203125" />
                  <Point X="-2.94555859375" Y="3.113386474609" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-2.957818115234" Y="3.143993408203" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.758982177734" Y="4.169649902344" />
                  <Point X="-2.752873779297" Y="4.174333007812" />
                  <Point X="-2.141548339844" Y="4.513971679688" />
                  <Point X="-1.967827026367" Y="4.287573242188" />
                  <Point X="-1.951249145508" Y="4.273662109375" />
                  <Point X="-1.857484985352" Y="4.224851074219" />
                  <Point X="-1.856033203125" Y="4.224095214844" />
                  <Point X="-1.835124389648" Y="4.218491699219" />
                  <Point X="-1.813806640625" Y="4.222251464844" />
                  <Point X="-1.716144897461" Y="4.262704589844" />
                  <Point X="-1.7146328125" Y="4.263331054688" />
                  <Point X="-1.696905639648" Y="4.275744140625" />
                  <Point X="-1.686084350586" Y="4.294485351562" />
                  <Point X="-1.654297363281" Y="4.39530078125" />
                  <Point X="-1.653805053711" Y="4.396861816406" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.652571411133" Y="4.423393066406" />
                  <Point X="-1.689137573242" Y="4.701140625" />
                  <Point X="-0.975997741699" Y="4.901080078125" />
                  <Point X="-0.96809185791" Y="4.903296875" />
                  <Point X="-0.224199539185" Y="4.990358398438" />
                  <Point X="-0.042140289307" Y="4.310903320312" />
                  <Point X="-0.035305416107" Y="4.295891113281" />
                  <Point X="-0.01011379528" Y="4.276560546875" />
                  <Point X="0.022425775528" Y="4.276560546875" />
                  <Point X="0.047617332458" Y="4.295891113281" />
                  <Point X="0.057398834229" Y="4.321900390625" />
                  <Point X="0.236648300171" Y="4.990868652344" />
                  <Point X="0.853300964355" Y="4.926288574219" />
                  <Point X="0.860205993652" Y="4.925565429688" />
                  <Point X="1.501184448242" Y="4.770813476562" />
                  <Point X="1.508457519531" Y="4.769057617188" />
                  <Point X="1.926622680664" Y="4.617386230469" />
                  <Point X="1.931032226562" Y="4.615786621094" />
                  <Point X="2.334459228516" Y="4.4271171875" />
                  <Point X="2.338696289063" Y="4.425135742188" />
                  <Point X="2.728419677734" Y="4.19808203125" />
                  <Point X="2.732525146484" Y="4.195690429687" />
                  <Point X="3.068740234375" Y="3.956592529297" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224851806641" Y="2.491513916016" />
                  <Point X="2.203709472656" Y="2.412451904297" />
                  <Point X="2.202044677734" Y="2.392325683594" />
                  <Point X="2.210287353516" Y="2.323968994141" />
                  <Point X="2.210414794922" Y="2.322910400391" />
                  <Point X="2.218682373047" Y="2.300812255859" />
                  <Point X="2.260985107422" Y="2.238468994141" />
                  <Point X="2.274939941406" Y="2.224203857422" />
                  <Point X="2.337283203125" Y="2.181901123047" />
                  <Point X="2.338255371094" Y="2.181241699219" />
                  <Point X="2.360335205078" Y="2.17298046875" />
                  <Point X="2.428701416016" Y="2.164736572266" />
                  <Point X="2.448663574219" Y="2.165946044922" />
                  <Point X="2.527725585938" Y="2.187088378906" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="2.563789794922" Y="2.205555664062" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.20015625" Y="2.745264892578" />
                  <Point X="4.202594726563" Y="2.741876220703" />
                  <Point X="4.387513671875" Y="2.436296142578" />
                  <Point X="3.288616210938" Y="1.593082885742" />
                  <Point X="3.279372070312" Y="1.583834106445" />
                  <Point X="3.222470947266" Y="1.509602172852" />
                  <Point X="3.213119140625" Y="1.491498291016" />
                  <Point X="3.191923339844" Y="1.415707275391" />
                  <Point X="3.191595214844" Y="1.414533691406" />
                  <Point X="3.190780517578" Y="1.390960693359" />
                  <Point X="3.208179931641" Y="1.306633544922" />
                  <Point X="3.215648925781" Y="1.287951293945" />
                  <Point X="3.262973632812" Y="1.21601953125" />
                  <Point X="3.263706542969" Y="1.214905761719" />
                  <Point X="3.280947753906" Y="1.198819946289" />
                  <Point X="3.349527832031" Y="1.160215209961" />
                  <Point X="3.349532958984" Y="1.160212280273" />
                  <Point X="3.368563476562" Y="1.153619873047" />
                  <Point X="3.461288330078" Y="1.141365112305" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.497420410156" Y="1.144017700195" />
                  <Point X="4.848975097656" Y="1.321953125" />
                  <Point X="4.938143066406" Y="0.95567755127" />
                  <Point X="4.939188964844" Y="0.951381958008" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="3.741167724609" Y="0.237826904297" />
                  <Point X="3.729087646484" Y="0.232819702148" />
                  <Point X="3.63798828125" Y="0.180162658691" />
                  <Point X="3.622261962891" Y="0.166922775269" />
                  <Point X="3.567602294922" Y="0.097273582458" />
                  <Point X="3.566756103516" Y="0.096195236206" />
                  <Point X="3.556985839844" Y="0.074738624573" />
                  <Point X="3.538765869141" Y="-0.020398862839" />
                  <Point X="3.538483642578" Y="-0.04068813324" />
                  <Point X="3.556703613281" Y="-0.135825622559" />
                  <Point X="3.556985839844" Y="-0.137298706055" />
                  <Point X="3.566756103516" Y="-0.158755325317" />
                  <Point X="3.621415771484" Y="-0.228404510498" />
                  <Point X="3.636577636719" Y="-0.24190725708" />
                  <Point X="3.727677001953" Y="-0.294564453125" />
                  <Point X="3.741167724609" Y="-0.300386993408" />
                  <Point X="3.760991210938" Y="-0.305698608398" />
                  <Point X="4.998068359375" Y="-0.637172424316" />
                  <Point X="4.949016113281" Y="-0.962525817871" />
                  <Point X="4.948432617188" Y="-0.966395874023" />
                  <Point X="4.874545898438" Y="-1.290178466797" />
                  <Point X="3.411982177734" Y="-1.097628173828" />
                  <Point X="3.394836425781" Y="-1.098341186523" />
                  <Point X="3.216040527344" Y="-1.13720324707" />
                  <Point X="3.213272216797" Y="-1.137804931641" />
                  <Point X="3.185445800781" Y="-1.154696777344" />
                  <Point X="3.077375" Y="-1.284672363281" />
                  <Point X="3.075701660156" Y="-1.286684814453" />
                  <Point X="3.064357666016" Y="-1.314072265625" />
                  <Point X="3.048868408203" Y="-1.482396362305" />
                  <Point X="3.048628662109" Y="-1.485002563477" />
                  <Point X="3.056359619141" Y="-1.516620605469" />
                  <Point X="3.155307861328" Y="-1.670528076172" />
                  <Point X="3.168460693359" Y="-1.685540771484" />
                  <Point X="3.186856933594" Y="-1.699656738281" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.205775878906" Y="-2.799480957031" />
                  <Point X="4.204131347656" Y="-2.802142089844" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737339599609" Y="-2.2533125" />
                  <Point X="2.524544189453" Y="-2.214881835938" />
                  <Point X="2.521249267578" Y="-2.214286865234" />
                  <Point X="2.489077148438" Y="-2.219245117188" />
                  <Point X="2.312296386719" Y="-2.312283447266" />
                  <Point X="2.309559082031" Y="-2.313724121094" />
                  <Point X="2.288598632812" Y="-2.334686035156" />
                  <Point X="2.195560058594" Y="-2.511466796875" />
                  <Point X="2.194119628906" Y="-2.514204101562" />
                  <Point X="2.189162841797" Y="-2.546373291016" />
                  <Point X="2.227593505859" Y="-2.759168701172" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.245913085938" Y="-2.799053955078" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.837255126953" Y="-4.188814941406" />
                  <Point X="2.835299316406" Y="-4.190211914062" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670550537109" Y="-2.980468261719" />
                  <Point X="1.460676757812" Y="-2.845538818359" />
                  <Point X="1.457427124023" Y="-2.843449462891" />
                  <Point X="1.425803588867" Y="-2.835717041016" />
                  <Point X="1.196270996094" Y="-2.856838867188" />
                  <Point X="1.192716796875" Y="-2.857166015625" />
                  <Point X="1.165333862305" Y="-2.868508300781" />
                  <Point X="0.988094543457" Y="-3.015876953125" />
                  <Point X="0.985350219727" Y="-3.018158691406" />
                  <Point X="0.968456604004" Y="-3.045986816406" />
                  <Point X="0.91546295166" Y="-3.289799316406" />
                  <Point X="0.913929443359" Y="-3.310720214844" />
                  <Point X="0.917279541016" Y="-3.336166748047" />
                  <Point X="1.127642089844" Y="-4.934028320312" />
                  <Point X="0.99619519043" Y="-4.962841796875" />
                  <Point X="0.994345153809" Y="-4.963247070312" />
                  <Point X="0.860200378418" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#216" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.190297965249" Y="5.0653261847" Z="2.5" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.5" />
                  <Point X="-0.205301466071" Y="5.074911947802" Z="2.5" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.5" />
                  <Point X="-0.995652588278" Y="4.980503814645" Z="2.5" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.5" />
                  <Point X="-1.7082996835" Y="4.448146597217" Z="2.5" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.5" />
                  <Point X="-1.708138087686" Y="4.441619523077" Z="2.5" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.5" />
                  <Point X="-1.741437351441" Y="4.340177718811" Z="2.5" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.5" />
                  <Point X="-1.840550907092" Y="4.300480373425" Z="2.5" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.5" />
                  <Point X="-2.131240526435" Y="4.605929519272" Z="2.5" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.5" />
                  <Point X="-2.144235126693" Y="4.604377896997" Z="2.5" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.5" />
                  <Point X="-2.795559906076" Y="4.240609982665" Z="2.5" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.5" />
                  <Point X="-3.007275393598" Y="3.150273425835" Z="2.5" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.5" />
                  <Point X="-3.001410560372" Y="3.139008452092" Z="2.5" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.5" />
                  <Point X="-2.994965925961" Y="3.053837641926" Z="2.5" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.5" />
                  <Point X="-3.056068013515" Y="2.994154138801" Z="2.5" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.5" />
                  <Point X="-3.783585922462" Y="3.372918555147" Z="2.5" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.5" />
                  <Point X="-3.799861084417" Y="3.370552673632" Z="2.5" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.5" />
                  <Point X="-4.212859457823" Y="2.837483301517" Z="2.5" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.5" />
                  <Point X="-3.709539753124" Y="1.62079207813" Z="2.5" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.5" />
                  <Point X="-3.696108816359" Y="1.609963006463" Z="2.5" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.5" />
                  <Point X="-3.667198258572" Y="1.552797058877" Z="2.5" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.5" />
                  <Point X="-3.692406534916" Y="1.493904793318" Z="2.5" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.5" />
                  <Point X="-4.800278107478" Y="1.612723037554" Z="2.5" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.5" />
                  <Point X="-4.818879692203" Y="1.606061209653" Z="2.5" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.5" />
                  <Point X="-4.97416491715" Y="1.028918518768" Z="2.5" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.5" />
                  <Point X="-3.59918522931" Y="0.055131246957" Z="2.5" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.5" />
                  <Point X="-3.576137559381" Y="0.04877532354" Z="2.5" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.5" />
                  <Point X="-3.548666798223" Y="0.029352454173" Z="2.5" />
                  <Point X="-3.539556741714" Y="0" Z="2.5" />
                  <Point X="-3.539697802912" Y="-0.000454496888" Z="2.5" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.5" />
                  <Point X="-3.549231035742" Y="-0.030100659594" Z="2.5" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.5" />
                  <Point X="-5.037702559463" Y="-0.440580809799" Z="2.5" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.5" />
                  <Point X="-5.059142810836" Y="-0.454923121477" Z="2.5" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.5" />
                  <Point X="-4.980591677739" Y="-0.997774967371" Z="2.5" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.5" />
                  <Point X="-3.243978762234" Y="-1.310130905777" Z="2.5" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.5" />
                  <Point X="-3.218755081009" Y="-1.307100971095" Z="2.5" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.5" />
                  <Point X="-3.192793784061" Y="-1.3229033917" Z="2.5" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.5" />
                  <Point X="-4.483039756779" Y="-2.336415960968" Z="2.5" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.5" />
                  <Point X="-4.498424610343" Y="-2.359161273806" Z="2.5" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.5" />
                  <Point X="-4.204062047001" Y="-2.850840780243" Z="2.5" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.5" />
                  <Point X="-2.5925002819" Y="-2.566842129031" Z="2.5" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.5" />
                  <Point X="-2.572574967876" Y="-2.555755501255" Z="2.5" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.5" />
                  <Point X="-3.288574884137" Y="-3.842578100592" Z="2.5" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.5" />
                  <Point X="-3.293682734016" Y="-3.867046023603" Z="2.5" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.5" />
                  <Point X="-2.88377617712" Y="-4.181650216827" Z="2.5" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.5" />
                  <Point X="-2.229651486684" Y="-4.160921210869" Z="2.5" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.5" />
                  <Point X="-2.222288802539" Y="-4.153823911689" Z="2.5" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.5" />
                  <Point X="-1.963535376207" Y="-3.984393773903" Z="2.5" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.5" />
                  <Point X="-1.655110083492" Y="-4.007495138604" Z="2.5" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.5" />
                  <Point X="-1.42448305568" Y="-4.213580351293" Z="2.5" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.5" />
                  <Point X="-1.412363783299" Y="-4.873918343503" Z="2.5" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.5" />
                  <Point X="-1.408590254488" Y="-4.880663324148" Z="2.5" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.5" />
                  <Point X="-1.112831492951" Y="-4.956470692524" Z="2.5" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.5" />
                  <Point X="-0.423194850433" Y="-3.541568709474" Z="2.5" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.5" />
                  <Point X="-0.414590257354" Y="-3.515176065645" Z="2.5" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.5" />
                  <Point X="-0.249495855884" Y="-3.281673674848" Z="2.5" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.5" />
                  <Point X="0.003863223476" Y="-3.20543837044" Z="2.5" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.5" />
                  <Point X="0.255855602078" Y="-3.286469690123" Z="2.5" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.5" />
                  <Point X="0.811560009968" Y="-4.990967320342" Z="2.5" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.5" />
                  <Point X="0.820417939353" Y="-5.013263401872" Z="2.5" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.5" />
                  <Point X="1.000740206392" Y="-4.980402952187" Z="2.5" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.5" />
                  <Point X="0.960695882631" Y="-3.298360218998" Z="2.5" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.5" />
                  <Point X="0.958166344025" Y="-3.269138473286" Z="2.5" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.5" />
                  <Point X="1.013904411061" Y="-3.023044139116" Z="2.5" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.5" />
                  <Point X="1.194697861121" Y="-2.875348387402" Z="2.5" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.5" />
                  <Point X="1.427480166139" Y="-2.856315852334" Z="2.5" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.5" />
                  <Point X="2.646422138372" Y="-4.306288336414" Z="2.5" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.5" />
                  <Point X="2.665023494066" Y="-4.324723779245" Z="2.5" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.5" />
                  <Point X="2.860159098462" Y="-4.198222944594" Z="2.5" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.5" />
                  <Point X="2.283058313457" Y="-2.742773908323" Z="2.5" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.5" />
                  <Point X="2.270641830715" Y="-2.719003671479" Z="2.5" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.5" />
                  <Point X="2.233651731628" Y="-2.503470888155" Z="2.5" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.5" />
                  <Point X="2.329427490217" Y="-2.325249578339" Z="2.5" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.5" />
                  <Point X="2.509503132034" Y="-2.232806178776" Z="2.5" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.5" />
                  <Point X="4.044639688783" Y="-3.034691186818" Z="2.5" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.5" />
                  <Point X="4.067777389652" Y="-3.04272967923" Z="2.5" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.5" />
                  <Point X="4.242154101566" Y="-2.794484614797" Z="2.5" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.5" />
                  <Point X="3.211139034035" Y="-1.628708938924" Z="2.5" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.5" />
                  <Point X="3.191210698985" Y="-1.612209908391" Z="2.5" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.5" />
                  <Point X="3.092502341785" Y="-1.455696150305" Z="2.5" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.5" />
                  <Point X="3.109664836429" Y="-1.285359675513" Z="2.5" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.5" />
                  <Point X="3.220504024901" Y="-1.154782438881" Z="2.5" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.5" />
                  <Point X="4.884017200855" Y="-1.311387155841" Z="2.5" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.5" />
                  <Point X="4.908294155754" Y="-1.308772156914" Z="2.5" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.5" />
                  <Point X="4.992301071731" Y="-0.938700788063" Z="2.5" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.5" />
                  <Point X="3.76777607314" Y="-0.226122293549" Z="2.5" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.5" />
                  <Point X="3.746542105864" Y="-0.219995288379" Z="2.5" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.5" />
                  <Point X="3.654596358413" Y="-0.166259750576" Z="2.5" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.5" />
                  <Point X="3.59965460495" Y="-0.095137412217" Z="2.5" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.5" />
                  <Point X="3.581716845474" Y="0.001473118956" Z="2.5" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.5" />
                  <Point X="3.600783079987" Y="0.097688987999" Z="2.5" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.5" />
                  <Point X="3.656853308488" Y="0.168153555863" Z="2.5" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.5" />
                  <Point X="5.028191744166" Y="0.56384963922" Z="2.5" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.5" />
                  <Point X="5.047010239293" Y="0.57561546551" Z="2.5" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.5" />
                  <Point X="4.980567842339" Y="0.998787150061" Z="2.5" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.5" />
                  <Point X="3.48473797933" Y="1.224869958804" Z="2.5" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.5" />
                  <Point X="3.461685648805" Y="1.222213837023" Z="2.5" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.5" />
                  <Point X="3.367898715572" Y="1.235066338931" Z="2.5" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.5" />
                  <Point X="3.298585769598" Y="1.274784828066" Z="2.5" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.5" />
                  <Point X="3.25099157923" Y="1.348022134343" Z="2.5" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.5" />
                  <Point X="3.233920230973" Y="1.433522760962" Z="2.5" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.5" />
                  <Point X="3.255997049412" Y="1.510463119403" Z="2.5" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.5" />
                  <Point X="4.430014318854" Y="2.441888449425" Z="2.5" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.5" />
                  <Point X="4.444123092444" Y="2.460430835126" Z="2.5" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.5" />
                  <Point X="4.234586620218" Y="2.805746901938" Z="2.5" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.5" />
                  <Point X="2.532633804763" Y="2.28013688321" Z="2.5" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.5" />
                  <Point X="2.508653713849" Y="2.266671399139" Z="2.5" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.5" />
                  <Point X="2.428533064189" Y="2.245656821337" Z="2.5" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.5" />
                  <Point X="2.359201455744" Y="2.254555545308" Z="2.5" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.5" />
                  <Point X="2.296203054927" Y="2.297823404638" Z="2.5" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.5" />
                  <Point X="2.253772881421" Y="2.361225379643" Z="2.5" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.5" />
                  <Point X="2.245856543518" Y="2.430815862033" Z="2.5" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.5" />
                  <Point X="3.115488469649" Y="3.979505277192" Z="2.5" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.5" />
                  <Point X="3.122906615617" Y="4.006328888027" Z="2.5" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.5" />
                  <Point X="2.747433167234" Y="4.272565293294" Z="2.5" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.5" />
                  <Point X="2.349484631596" Y="4.503689454734" Z="2.5" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.5" />
                  <Point X="1.937515857713" Y="4.695669622845" Z="2.5" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.5" />
                  <Point X="1.506762649233" Y="4.850697268768" Z="2.5" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.5" />
                  <Point X="0.852352950287" Y="5.007296780804" Z="2.5" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.5" />
                  <Point X="0.002946596304" Y="4.366121321251" Z="2.5" />
                  <Point X="0" Y="4.355124473572" Z="2.5" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>