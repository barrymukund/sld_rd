<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#183" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2481" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476318359" Y="0.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.823722473145" Y="-4.484426757812" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.435922729492" Y="-3.314016845703" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.137784973145" Y="-3.124549072266" />
                  <Point X="0.049136188507" Y="-3.097035888672" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.008664840698" Y="-3.092766601562" />
                  <Point X="-0.036824237823" Y="-3.097035888672" />
                  <Point X="-0.201534591675" Y="-3.148155761719" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323425293" Y="-3.2314765625" />
                  <Point X="-0.472764007568" Y="-3.384836669922" />
                  <Point X="-0.530051086426" Y="-3.467376953125" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.691150817871" Y="-4.035611816406" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-0.978741577148" Y="-4.864876953125" />
                  <Point X="-1.079341308594" Y="-4.845350585938" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.246311523438" Y="-4.801553710938" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.255858154297" Y="-4.31840234375" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938110352" Y="-4.182966308594" />
                  <Point X="-1.304010131836" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204101562" />
                  <Point X="-1.47528918457" Y="-3.998215332031" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.844293334961" Y="-3.877774658203" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.21036328125" Y="-4.006858886719" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102539062" Y="-4.099461914062" />
                  <Point X="-2.413791992188" Y="-4.20201171875" />
                  <Point X="-2.480148925781" Y="-4.288490234375" />
                  <Point X="-2.654258056641" Y="-4.180686035156" />
                  <Point X="-2.801708007812" Y="-4.089388671875" />
                  <Point X="-3.062044677734" Y="-3.888937988281" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.907723144531" Y="-3.514866210938" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653076172" />
                  <Point X="-2.406588134766" Y="-2.616126220703" />
                  <Point X="-2.405575683594" Y="-2.585191894531" />
                  <Point X="-2.414560546875" Y="-2.555573974609" />
                  <Point X="-2.428778076172" Y="-2.526744873047" />
                  <Point X="-2.446805175781" Y="-2.501588623047" />
                  <Point X="-2.464153808594" Y="-2.484239990234" />
                  <Point X="-2.489312744141" Y="-2.466211425781" />
                  <Point X="-2.518141601562" Y="-2.451995117188" />
                  <Point X="-2.547758544922" Y="-2.443011230469" />
                  <Point X="-2.578691650391" Y="-2.444024169922" />
                  <Point X="-2.610217529297" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.090336181641" Y="-2.721670410156" />
                  <Point X="-3.818024169922" Y="-3.141801269531" />
                  <Point X="-3.966365722656" Y="-2.946910888672" />
                  <Point X="-4.082860351562" Y="-2.793861083984" />
                  <Point X="-4.269498535156" Y="-2.480896484375" />
                  <Point X="-4.306142578125" Y="-2.419450195312" />
                  <Point X="-3.952730957031" Y="-2.148267822266" />
                  <Point X="-3.105954589844" Y="-1.498513427734" />
                  <Point X="-3.084576904297" Y="-1.475592895508" />
                  <Point X="-3.066612060547" Y="-1.448461425781" />
                  <Point X="-3.053856689453" Y="-1.419832519531" />
                  <Point X="-3.046152099609" Y="-1.390085449219" />
                  <Point X="-3.04334765625" Y="-1.35965612793" />
                  <Point X="-3.045556640625" Y="-1.327985473633" />
                  <Point X="-3.052557861328" Y="-1.298240600586" />
                  <Point X="-3.068640136719" Y="-1.272257202148" />
                  <Point X="-3.08947265625" Y="-1.24830078125" />
                  <Point X="-3.11297265625" Y="-1.228766723633" />
                  <Point X="-3.139455322266" Y="-1.213180297852" />
                  <Point X="-3.168718261719" Y="-1.201956542969" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.801466796875" Y="-1.269364868164" />
                  <Point X="-4.732102050781" Y="-1.391885253906" />
                  <Point X="-4.788515136719" Y="-1.171030395508" />
                  <Point X="-4.834077636719" Y="-0.992654663086" />
                  <Point X="-4.883457519531" Y="-0.647395751953" />
                  <Point X="-4.892424804688" Y="-0.584698303223" />
                  <Point X="-4.497481933594" Y="-0.478873657227" />
                  <Point X="-3.532875976562" Y="-0.220408447266" />
                  <Point X="-3.517493652344" Y="-0.214827560425" />
                  <Point X="-3.487728759766" Y="-0.199469406128" />
                  <Point X="-3.469686523438" Y="-0.186947158813" />
                  <Point X="-3.459975830078" Y="-0.180207504272" />
                  <Point X="-3.437520751953" Y="-0.158323883057" />
                  <Point X="-3.418276611328" Y="-0.132068237305" />
                  <Point X="-3.404168212891" Y="-0.10406716156" />
                  <Point X="-3.398154052734" Y="-0.084689292908" />
                  <Point X="-3.394917236328" Y="-0.07426007843" />
                  <Point X="-3.390647949219" Y="-0.046100532532" />
                  <Point X="-3.390647949219" Y="-0.016459506989" />
                  <Point X="-3.394917236328" Y="0.011700042725" />
                  <Point X="-3.400931396484" Y="0.031077758789" />
                  <Point X="-3.404168212891" Y="0.041506973267" />
                  <Point X="-3.418276611328" Y="0.069508209229" />
                  <Point X="-3.437520751953" Y="0.095763694763" />
                  <Point X="-3.459975830078" Y="0.117647315979" />
                  <Point X="-3.478018066406" Y="0.130169723511" />
                  <Point X="-3.486260009766" Y="0.135287185669" />
                  <Point X="-3.511538574219" Y="0.149247039795" />
                  <Point X="-3.532875976562" Y="0.157848251343" />
                  <Point X="-4.052036376953" Y="0.296956878662" />
                  <Point X="-4.89181640625" Y="0.521975280762" />
                  <Point X="-4.853851074219" Y="0.778540039062" />
                  <Point X="-4.824488769531" Y="0.976970458984" />
                  <Point X="-4.725087402344" Y="1.343791870117" />
                  <Point X="-4.70355078125" Y="1.423267700195" />
                  <Point X="-4.457119140625" Y="1.390824462891" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341674805" />
                  <Point X="-3.723424072266" Y="1.301228271484" />
                  <Point X="-3.703137451172" Y="1.305263549805" />
                  <Point X="-3.663204101562" Y="1.317854492188" />
                  <Point X="-3.641711425781" Y="1.324631103516" />
                  <Point X="-3.622778564453" Y="1.332961791992" />
                  <Point X="-3.604034423828" Y="1.343783691406" />
                  <Point X="-3.587353027344" Y="1.356015014648" />
                  <Point X="-3.573714599609" Y="1.371566894531" />
                  <Point X="-3.561300292969" Y="1.389296630859" />
                  <Point X="-3.5513515625" Y="1.407431030273" />
                  <Point X="-3.535328125" Y="1.446115112305" />
                  <Point X="-3.526704101563" Y="1.466935302734" />
                  <Point X="-3.520916015625" Y="1.48679296875" />
                  <Point X="-3.517157470703" Y="1.508108154297" />
                  <Point X="-3.515804443359" Y="1.528748657227" />
                  <Point X="-3.518951171875" Y="1.549192626953" />
                  <Point X="-3.524552978516" Y="1.570099243164" />
                  <Point X="-3.532049804688" Y="1.589377685547" />
                  <Point X="-3.551383789062" Y="1.626518066406" />
                  <Point X="-3.561789550781" Y="1.646507446289" />
                  <Point X="-3.573281738281" Y="1.663706665039" />
                  <Point X="-3.587194335938" Y="1.680286987305" />
                  <Point X="-3.602135986328" Y="1.694590087891" />
                  <Point X="-3.899927734375" Y="1.92309387207" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.195247558594" Y="2.538189453125" />
                  <Point X="-4.081152832031" Y="2.733661132812" />
                  <Point X="-3.817847412109" Y="3.072102294922" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.631328125" Y="3.089854980469" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.091178466797" Y="2.820930664063" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999015136719" Y="2.826504394531" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.906600585938" Y="2.899706298828" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406982422" Y="2.937083740234" />
                  <Point X="-2.860777587891" Y="2.955337890625" />
                  <Point X="-2.85162890625" Y="2.973889648438" />
                  <Point X="-2.846712158203" Y="2.993981689453" />
                  <Point X="-2.843886962891" Y="3.015440429688" />
                  <Point X="-2.843435791016" Y="3.036120849609" />
                  <Point X="-2.848301757812" Y="3.091737060547" />
                  <Point X="-2.850920410156" Y="3.121670410156" />
                  <Point X="-2.854955566406" Y="3.141957275391" />
                  <Point X="-2.861464111328" Y="3.162599853516" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.001755615234" Y="3.410095214844" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-2.899336425781" Y="3.942333740234" />
                  <Point X="-2.700625732422" Y="4.09468359375" />
                  <Point X="-2.285928466797" Y="4.325080566406" />
                  <Point X="-2.167036865234" Y="4.391134277344" />
                  <Point X="-2.04319543457" Y="4.229740722656" />
                  <Point X="-2.028892700195" Y="4.214799804688" />
                  <Point X="-2.012312866211" Y="4.200887207031" />
                  <Point X="-1.99511340332" Y="4.18939453125" />
                  <Point X="-1.933212890625" Y="4.157170898438" />
                  <Point X="-1.899897094727" Y="4.139827636719" />
                  <Point X="-1.8806171875" Y="4.132330566406" />
                  <Point X="-1.859710571289" Y="4.126729003906" />
                  <Point X="-1.839267700195" Y="4.123582519531" />
                  <Point X="-1.818628417969" Y="4.124935058594" />
                  <Point X="-1.797312988281" Y="4.128693359375" />
                  <Point X="-1.777453491211" Y="4.134481933594" />
                  <Point X="-1.712979858398" Y="4.161187988281" />
                  <Point X="-1.678279541016" Y="4.175561523437" />
                  <Point X="-1.660144287109" Y="4.185511230469" />
                  <Point X="-1.642414794922" Y="4.19792578125" />
                  <Point X="-1.626863525391" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.574495361328" Y="4.332477050781" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.572732666016" Y="4.544781738281" />
                  <Point X="-1.584201904297" Y="4.6318984375" />
                  <Point X="-1.206878540039" Y="4.737687011719" />
                  <Point X="-0.949637573242" Y="4.809808105469" />
                  <Point X="-0.4469012146" Y="4.868645996094" />
                  <Point X="-0.294710784912" Y="4.886457519531" />
                  <Point X="-0.259532806396" Y="4.755171386719" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.0821145401" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155911446" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426460266" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.213830307007" Y="4.538658691406" />
                  <Point X="0.307419342041" Y="4.8879375" />
                  <Point X="0.619423095703" Y="4.855262207031" />
                  <Point X="0.844041320801" Y="4.831738769531" />
                  <Point X="1.259970703125" Y="4.731320800781" />
                  <Point X="1.481025634766" Y="4.677950683594" />
                  <Point X="1.751239013672" Y="4.579942871094" />
                  <Point X="1.894645019531" Y="4.527928222656" />
                  <Point X="2.156430175781" Y="4.405500488281" />
                  <Point X="2.29457421875" Y="4.34089453125" />
                  <Point X="2.547495849609" Y="4.193541992188" />
                  <Point X="2.680977539062" Y="4.115775390625" />
                  <Point X="2.919493896484" Y="3.946156005859" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.707730712891" Y="3.521305664062" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075927734" Y="2.539931152344" />
                  <Point X="2.133076904297" Y="2.516056884766" />
                  <Point X="2.119119384766" Y="2.463862304688" />
                  <Point X="2.111607177734" Y="2.435770507813" />
                  <Point X="2.108619384766" Y="2.417934570312" />
                  <Point X="2.107728027344" Y="2.380953125" />
                  <Point X="2.113170410156" Y="2.335819580078" />
                  <Point X="2.116099365234" Y="2.311528320312" />
                  <Point X="2.121442138672" Y="2.289605224609" />
                  <Point X="2.129708496094" Y="2.267515869141" />
                  <Point X="2.140071044922" Y="2.247470947266" />
                  <Point X="2.167998046875" Y="2.206313476562" />
                  <Point X="2.183028808594" Y="2.184162353516" />
                  <Point X="2.194465576172" Y="2.170327636719" />
                  <Point X="2.221599121094" Y="2.145592529297" />
                  <Point X="2.262756347656" Y="2.117665527344" />
                  <Point X="2.284907714844" Y="2.102635009766" />
                  <Point X="2.304952392578" Y="2.092272216797" />
                  <Point X="2.327040283203" Y="2.084006347656" />
                  <Point X="2.348963623047" Y="2.078663574219" />
                  <Point X="2.394097167969" Y="2.073221191406" />
                  <Point X="2.418388427734" Y="2.070291992188" />
                  <Point X="2.436467285156" Y="2.069845703125" />
                  <Point X="2.473206542969" Y="2.074171142578" />
                  <Point X="2.525401123047" Y="2.088128662109" />
                  <Point X="2.553492919922" Y="2.095640869141" />
                  <Point X="2.565284179688" Y="2.099638427734" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.110709472656" Y="2.411623046875" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.044093994141" Y="2.799501708984" />
                  <Point X="4.123270019531" Y="2.689465087891" />
                  <Point X="4.256241210938" Y="2.469729248047" />
                  <Point X="4.262198730469" Y="2.459884033203" />
                  <Point X="3.9682734375" Y="2.234346923828" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221424560547" Y="1.660241455078" />
                  <Point X="3.203973632812" Y="1.641627685547" />
                  <Point X="3.166408935547" Y="1.592621948242" />
                  <Point X="3.14619140625" Y="1.566246459961" />
                  <Point X="3.13660546875" Y="1.550911376953" />
                  <Point X="3.121629882812" Y="1.517086181641" />
                  <Point X="3.107636962891" Y="1.467051025391" />
                  <Point X="3.100105957031" Y="1.440121459961" />
                  <Point X="3.09665234375" Y="1.417821899414" />
                  <Point X="3.095836425781" Y="1.394251708984" />
                  <Point X="3.097739501953" Y="1.371767944336" />
                  <Point X="3.109226074219" Y="1.316097412109" />
                  <Point X="3.115408447266" Y="1.286135253906" />
                  <Point X="3.120680419922" Y="1.268976196289" />
                  <Point X="3.136282714844" Y="1.235740356445" />
                  <Point X="3.167525146484" Y="1.188253051758" />
                  <Point X="3.184340332031" Y="1.162694946289" />
                  <Point X="3.198893798828" Y="1.145450195312" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234347167969" Y="1.116034790039" />
                  <Point X="3.279621826172" Y="1.090548950195" />
                  <Point X="3.303989257812" Y="1.076832519531" />
                  <Point X="3.320521728516" Y="1.069501464844" />
                  <Point X="3.356118408203" Y="1.059438598633" />
                  <Point X="3.417332763672" Y="1.051348388672" />
                  <Point X="3.450279052734" Y="1.046994018555" />
                  <Point X="3.462702392578" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="3.984235351562" Y="1.112288330078" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.8119296875" Y="1.07249621582" />
                  <Point X="4.845936035156" Y="0.932809448242" />
                  <Point X="4.887838378906" Y="0.663677185059" />
                  <Point X="4.890864746094" Y="0.644238586426" />
                  <Point X="4.561758789062" Y="0.556054931641" />
                  <Point X="3.716579833984" Y="0.32958984375" />
                  <Point X="3.704791259766" Y="0.325586517334" />
                  <Point X="3.681545654297" Y="0.315068054199" />
                  <Point X="3.621404296875" Y="0.280305175781" />
                  <Point X="3.589035644531" Y="0.261595489502" />
                  <Point X="3.574311035156" Y="0.251096282959" />
                  <Point X="3.547530761719" Y="0.225576522827" />
                  <Point X="3.511446044922" Y="0.179596176147" />
                  <Point X="3.492024902344" Y="0.154848876953" />
                  <Point X="3.48030078125" Y="0.135568603516" />
                  <Point X="3.470527099609" Y="0.114104988098" />
                  <Point X="3.463680908203" Y="0.092604179382" />
                  <Point X="3.451652587891" Y="0.029797254562" />
                  <Point X="3.445178710938" Y="-0.004006166458" />
                  <Point X="3.443483154297" Y="-0.021875085831" />
                  <Point X="3.445178710938" Y="-0.058553871155" />
                  <Point X="3.45720703125" Y="-0.121360946655" />
                  <Point X="3.463680908203" Y="-0.155164215088" />
                  <Point X="3.470527099609" Y="-0.176665023804" />
                  <Point X="3.48030078125" Y="-0.198128646851" />
                  <Point X="3.492024902344" Y="-0.217409057617" />
                  <Point X="3.528109619141" Y="-0.263389556885" />
                  <Point X="3.547530761719" Y="-0.28813671875" />
                  <Point X="3.559999023438" Y="-0.301236083984" />
                  <Point X="3.589035644531" Y="-0.324155517578" />
                  <Point X="3.649177001953" Y="-0.358918395996" />
                  <Point X="3.681545654297" Y="-0.377628082275" />
                  <Point X="3.692710205078" Y="-0.383138977051" />
                  <Point X="3.716579833984" Y="-0.392150024414" />
                  <Point X="4.171463378906" Y="-0.514035583496" />
                  <Point X="4.891472167969" Y="-0.706961425781" />
                  <Point X="4.874010253906" Y="-0.822784423828" />
                  <Point X="4.855022460938" Y="-0.948725524902" />
                  <Point X="4.801338378906" Y="-1.183976806641" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="4.404551757812" Y="-1.132482910156" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.408035644531" Y="-1.002710266113" />
                  <Point X="3.374658447266" Y="-1.005508666992" />
                  <Point X="3.256622314453" Y="-1.031164306641" />
                  <Point X="3.193094238281" Y="-1.044972290039" />
                  <Point X="3.163973632812" Y="-1.056597167969" />
                  <Point X="3.136147216797" Y="-1.073489501953" />
                  <Point X="3.112397460938" Y="-1.093959960937" />
                  <Point X="3.041052001953" Y="-1.179766235352" />
                  <Point X="3.002653320312" Y="-1.225948120117" />
                  <Point X="2.987932617188" Y="-1.250330688477" />
                  <Point X="2.976589355469" Y="-1.277715942383" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.959531982422" Y="-1.41648828125" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347167969" Y="-1.507564086914" />
                  <Point X="2.964078613281" Y="-1.539185058594" />
                  <Point X="2.976450195312" Y="-1.567996704102" />
                  <Point X="3.041773193359" Y="-1.669602172852" />
                  <Point X="3.076930419922" Y="-1.724287231445" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909057617" />
                  <Point X="3.532763183594" Y="-2.084824707031" />
                  <Point X="4.213122558594" Y="-2.6068828125" />
                  <Point X="4.178334960938" Y="-2.663174560547" />
                  <Point X="4.124810546875" Y="-2.749785400391" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.673927978516" Y="-2.680954833984" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.613742919922" Y="-2.134454345703" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.328127441406" Y="-2.196598144531" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384521484" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508544922" />
                  <Point X="2.204531738281" Y="-2.290439208984" />
                  <Point X="2.143110351562" Y="-2.407145019531" />
                  <Point X="2.110052734375" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733154297" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.121046386719" Y="-2.703739990234" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.423083251953" Y="-3.295921630859" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.845364257812" Y="-4.066276855469" />
                  <Point X="2.781847412109" Y="-4.111645507812" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.424705566406" Y="-3.802410400391" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503417969" Y="-2.922178955078" />
                  <Point X="1.721924072266" Y="-2.900557373047" />
                  <Point X="1.58337121582" Y="-2.81148046875" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368896484" Y="-2.743435546875" />
                  <Point X="1.417099731445" Y="-2.741116699219" />
                  <Point X="1.265568237305" Y="-2.755060791016" />
                  <Point X="1.184012817383" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="0.987587219238" Y="-2.89275" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141479492" Y="-2.968861572266" />
                  <Point X="0.887249084473" Y="-2.996688232422" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.840639404297" Y="-3.186767333984" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.896616210938" Y="-3.907037109375" />
                  <Point X="1.022065246582" Y="-4.859915039062" />
                  <Point X="0.975677429199" Y="-4.870083496094" />
                  <Point X="0.929315551758" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058435058594" Y="-4.752635742188" />
                  <Point X="-1.141246459961" Y="-4.731328613281" />
                  <Point X="-1.120774658203" Y="-4.575830078125" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.16268359375" Y="-4.299868652344" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812487793" Y="-4.178469238281" />
                  <Point X="-1.199026855469" Y="-4.149502929688" />
                  <Point X="-1.205665771484" Y="-4.135466308594" />
                  <Point X="-1.221737792969" Y="-4.10762890625" />
                  <Point X="-1.230573852539" Y="-4.094861816406" />
                  <Point X="-1.250208251953" Y="-4.070937011719" />
                  <Point X="-1.261006713867" Y="-4.059779296875" />
                  <Point X="-1.412651367188" Y="-3.926790527344" />
                  <Point X="-1.494267700195" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833374023" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621454956055" Y="-3.798447998047" />
                  <Point X="-1.636814086914" Y="-3.796169677734" />
                  <Point X="-1.838080078125" Y="-3.782978027344" />
                  <Point X="-1.946403320312" Y="-3.775878173828" />
                  <Point X="-1.961928100586" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.263142578125" Y="-3.927869384766" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442382812" Y="-4.010131347656" />
                  <Point X="-2.402759277344" Y="-4.032771728516" />
                  <Point X="-2.410470947266" Y="-4.041629394531" />
                  <Point X="-2.489160400391" Y="-4.144179199219" />
                  <Point X="-2.503202636719" Y="-4.162479492188" />
                  <Point X="-2.604246826172" Y="-4.099915527344" />
                  <Point X="-2.747584228516" Y="-4.011164550781" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.825450683594" Y="-3.562366210938" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083007812" />
                  <Point X="-2.323947998047" Y="-2.681116455078" />
                  <Point X="-2.319684570312" Y="-2.666186523438" />
                  <Point X="-2.313413574219" Y="-2.634659667969" />
                  <Point X="-2.311638916016" Y="-2.619233886719" />
                  <Point X="-2.310626464844" Y="-2.588299560547" />
                  <Point X="-2.314666748047" Y="-2.557613769531" />
                  <Point X="-2.323651611328" Y="-2.527995849609" />
                  <Point X="-2.329358398438" Y="-2.513555175781" />
                  <Point X="-2.343575927734" Y="-2.484726074219" />
                  <Point X="-2.351558105469" Y="-2.471408691406" />
                  <Point X="-2.369585205078" Y="-2.446252441406" />
                  <Point X="-2.379630126953" Y="-2.434413574219" />
                  <Point X="-2.396978759766" Y="-2.417064941406" />
                  <Point X="-2.408818603516" Y="-2.40701953125" />
                  <Point X="-2.433977539062" Y="-2.388990966797" />
                  <Point X="-2.447296630859" Y="-2.3810078125" />
                  <Point X="-2.476125488281" Y="-2.366791503906" />
                  <Point X="-2.490565429688" Y="-2.361085449219" />
                  <Point X="-2.520182373047" Y="-2.3521015625" />
                  <Point X="-2.550867675781" Y="-2.348062011719" />
                  <Point X="-2.58180078125" Y="-2.349074951172" />
                  <Point X="-2.597225585938" Y="-2.350849609375" />
                  <Point X="-2.628751464844" Y="-2.357120605469" />
                  <Point X="-2.643681396484" Y="-2.361384033203" />
                  <Point X="-2.672647460938" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.137836181641" Y="-2.639397949219" />
                  <Point X="-3.793089355469" Y="-3.017708496094" />
                  <Point X="-3.890772460938" Y="-2.889372802734" />
                  <Point X="-4.004016113281" Y="-2.740593994141" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-3.894898681641" Y="-2.223636474609" />
                  <Point X="-3.048122314453" Y="-1.573881958008" />
                  <Point X="-3.036481933594" Y="-1.563309692383" />
                  <Point X="-3.015104248047" Y="-1.540389282227" />
                  <Point X="-3.005366943359" Y="-1.528040893555" />
                  <Point X="-2.987402099609" Y="-1.500909545898" />
                  <Point X="-2.979835449219" Y="-1.487124023438" />
                  <Point X="-2.967080078125" Y="-1.458495239258" />
                  <Point X="-2.961891357422" Y="-1.443651977539" />
                  <Point X="-2.954186767578" Y="-1.413904785156" />
                  <Point X="-2.951552978516" Y="-1.398803955078" />
                  <Point X="-2.948748535156" Y="-1.368374511719" />
                  <Point X="-2.948577880859" Y="-1.353046020508" />
                  <Point X="-2.950786865234" Y="-1.321375366211" />
                  <Point X="-2.953083740234" Y="-1.306219604492" />
                  <Point X="-2.960084960938" Y="-1.276474731445" />
                  <Point X="-2.971779052734" Y="-1.248242919922" />
                  <Point X="-2.987861328125" Y="-1.222259521484" />
                  <Point X="-2.996953857422" Y="-1.209918823242" />
                  <Point X="-3.017786376953" Y="-1.185962402344" />
                  <Point X="-3.028745605469" Y="-1.175244384766" />
                  <Point X="-3.052245605469" Y="-1.155710449219" />
                  <Point X="-3.064786376953" Y="-1.146894287109" />
                  <Point X="-3.091269042969" Y="-1.131307739258" />
                  <Point X="-3.105434814453" Y="-1.124480834961" />
                  <Point X="-3.134697753906" Y="-1.113257080078" />
                  <Point X="-3.149794921875" Y="-1.108860351562" />
                  <Point X="-3.181682617188" Y="-1.102378662109" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-3.813866699219" Y="-1.175177612305" />
                  <Point X="-4.660920410156" Y="-1.286694335938" />
                  <Point X="-4.696470214844" Y="-1.14751940918" />
                  <Point X="-4.740762207031" Y="-0.974118103027" />
                  <Point X="-4.786452636719" Y="-0.65465435791" />
                  <Point X="-4.472894042969" Y="-0.570636657715" />
                  <Point X="-3.508288085938" Y="-0.312171447754" />
                  <Point X="-3.500475341797" Y="-0.309712493896" />
                  <Point X="-3.473932373047" Y="-0.299251647949" />
                  <Point X="-3.444167480469" Y="-0.2838934021" />
                  <Point X="-3.433561767578" Y="-0.277513916016" />
                  <Point X="-3.41551953125" Y="-0.264991699219" />
                  <Point X="-3.393671875" Y="-0.248242782593" />
                  <Point X="-3.371216796875" Y="-0.226359100342" />
                  <Point X="-3.3608984375" Y="-0.214484436035" />
                  <Point X="-3.341654296875" Y="-0.188228866577" />
                  <Point X="-3.333437255859" Y="-0.174814743042" />
                  <Point X="-3.319328857422" Y="-0.146813751221" />
                  <Point X="-3.3134375" Y="-0.1322265625" />
                  <Point X="-3.307423339844" Y="-0.112848655701" />
                  <Point X="-3.300990478516" Y="-0.088500358582" />
                  <Point X="-3.296721191406" Y="-0.060340778351" />
                  <Point X="-3.295647949219" Y="-0.046100475311" />
                  <Point X="-3.295647949219" Y="-0.016459568024" />
                  <Point X="-3.296721191406" Y="-0.002219263792" />
                  <Point X="-3.300990478516" Y="0.025940319061" />
                  <Point X="-3.304186767578" Y="0.039859596252" />
                  <Point X="-3.310200927734" Y="0.059237350464" />
                  <Point X="-3.319328613281" Y="0.084253257751" />
                  <Point X="-3.333437011719" Y="0.112254562378" />
                  <Point X="-3.341654296875" Y="0.125668968201" />
                  <Point X="-3.3608984375" Y="0.151924545288" />
                  <Point X="-3.371216796875" Y="0.163798919678" />
                  <Point X="-3.393671875" Y="0.185682601929" />
                  <Point X="-3.405808349609" Y="0.195691467285" />
                  <Point X="-3.423850585938" Y="0.20821383667" />
                  <Point X="-3.440334716797" Y="0.218448898315" />
                  <Point X="-3.46561328125" Y="0.232408752441" />
                  <Point X="-3.476020751953" Y="0.23735774231" />
                  <Point X="-3.497358154297" Y="0.245958847046" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.027448486328" Y="0.38871975708" />
                  <Point X="-4.785446289062" Y="0.591824707031" />
                  <Point X="-4.759874511719" Y="0.764633789062" />
                  <Point X="-4.731331542969" Y="0.957526611328" />
                  <Point X="-4.633586425781" Y="1.318236938477" />
                  <Point X="-4.469519042969" Y="1.296637207031" />
                  <Point X="-3.778066162109" Y="1.20560546875" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747057861328" Y="1.204364257812" />
                  <Point X="-3.736705322266" Y="1.20470324707" />
                  <Point X="-3.715143554688" Y="1.20658984375" />
                  <Point X="-3.704890380859" Y="1.208053588867" />
                  <Point X="-3.684603759766" Y="1.212088989258" />
                  <Point X="-3.6745703125" Y="1.214660400391" />
                  <Point X="-3.634636962891" Y="1.227251464844" />
                  <Point X="-3.613144287109" Y="1.234028076172" />
                  <Point X="-3.603450439453" Y="1.237676513672" />
                  <Point X="-3.584517578125" Y="1.246007202148" />
                  <Point X="-3.575278564453" Y="1.250689331055" />
                  <Point X="-3.556534423828" Y="1.261511230469" />
                  <Point X="-3.547859863281" Y="1.267171508789" />
                  <Point X="-3.531178466797" Y="1.279402832031" />
                  <Point X="-3.515927734375" Y="1.293377807617" />
                  <Point X="-3.502289306641" Y="1.30892956543" />
                  <Point X="-3.495894775391" Y="1.317077636719" />
                  <Point X="-3.48348046875" Y="1.334807373047" />
                  <Point X="-3.478010986328" Y="1.343603027344" />
                  <Point X="-3.468062255859" Y="1.361737548828" />
                  <Point X="-3.463583007813" Y="1.371076171875" />
                  <Point X="-3.447559570312" Y="1.409760253906" />
                  <Point X="-3.438935546875" Y="1.430580444336" />
                  <Point X="-3.435499511719" Y="1.440351074219" />
                  <Point X="-3.429711425781" Y="1.460208740234" />
                  <Point X="-3.427359375" Y="1.470295898438" />
                  <Point X="-3.423600830078" Y="1.491611083984" />
                  <Point X="-3.422360839844" Y="1.501894042969" />
                  <Point X="-3.4210078125" Y="1.522534545898" />
                  <Point X="-3.42191015625" Y="1.543200805664" />
                  <Point X="-3.425056884766" Y="1.563644775391" />
                  <Point X="-3.427187988281" Y="1.573780029297" />
                  <Point X="-3.432789794922" Y="1.594686645508" />
                  <Point X="-3.436011962891" Y="1.604530273438" />
                  <Point X="-3.443508789062" Y="1.62380871582" />
                  <Point X="-3.447783691406" Y="1.633243774414" />
                  <Point X="-3.467117675781" Y="1.670384033203" />
                  <Point X="-3.4775234375" Y="1.690373413086" />
                  <Point X="-3.482800048828" Y="1.699286743164" />
                  <Point X="-3.494292236328" Y="1.716485961914" />
                  <Point X="-3.500507568359" Y="1.724771606445" />
                  <Point X="-3.514420166016" Y="1.741351928711" />
                  <Point X="-3.521501708984" Y="1.748912475586" />
                  <Point X="-3.536443359375" Y="1.763215698242" />
                  <Point X="-3.544303710938" Y="1.769958618164" />
                  <Point X="-3.842095458984" Y="1.998462402344" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.113201171875" Y="2.490300048828" />
                  <Point X="-4.002291748047" Y="2.680314453125" />
                  <Point X="-3.742866943359" Y="3.013767822266" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.678828125" Y="3.007582519531" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736656982422" />
                  <Point X="-3.165327880859" Y="2.732621582031" />
                  <Point X="-3.155074462891" Y="2.731157958984" />
                  <Point X="-3.099458251953" Y="2.726292236328" />
                  <Point X="-3.069525146484" Y="2.723673339844" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996526611328" Y="2.729310546875" />
                  <Point X="-2.976435302734" Y="2.734226806641" />
                  <Point X="-2.956997802734" Y="2.741301513672" />
                  <Point X="-2.938446533203" Y="2.750449951172" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.87890234375" Y="2.793054443359" />
                  <Point X="-2.839425537109" Y="2.83253125" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265625" Y="2.861489013672" />
                  <Point X="-2.798318847656" Y="2.877619873047" />
                  <Point X="-2.79228515625" Y="2.886039550781" />
                  <Point X="-2.780655761719" Y="2.904293701172" />
                  <Point X="-2.775574707031" Y="2.913320556641" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.7593515625" Y="2.951308349609" />
                  <Point X="-2.754434814453" Y="2.971400390625" />
                  <Point X="-2.752524902344" Y="2.981581298828" />
                  <Point X="-2.749699707031" Y="3.003040039063" />
                  <Point X="-2.748909667969" Y="3.013368408203" />
                  <Point X="-2.748458496094" Y="3.034048828125" />
                  <Point X="-2.748797363281" Y="3.044400878906" />
                  <Point X="-2.753663330078" Y="3.100017089844" />
                  <Point X="-2.756281982422" Y="3.129950439453" />
                  <Point X="-2.757745605469" Y="3.140203369141" />
                  <Point X="-2.761780761719" Y="3.160490234375" />
                  <Point X="-2.764352294922" Y="3.170524169922" />
                  <Point X="-2.770860839844" Y="3.191166748047" />
                  <Point X="-2.774509521484" Y="3.200860839844" />
                  <Point X="-2.782840332031" Y="3.219793945312" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.919483154297" Y="3.457595214844" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.841534423828" Y="3.866941894531" />
                  <Point X="-2.648374023438" Y="4.015036376953" />
                  <Point X="-2.239791015625" Y="4.242036621094" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.118563964844" Y="4.171908203125" />
                  <Point X="-2.1118203125" Y="4.164047363281" />
                  <Point X="-2.097517578125" Y="4.149106445313" />
                  <Point X="-2.089958740234" Y="4.142026367188" />
                  <Point X="-2.07337890625" Y="4.128113769531" />
                  <Point X="-2.065093261719" Y="4.1218984375" />
                  <Point X="-2.047893676758" Y="4.110405761719" />
                  <Point X="-2.038979858398" Y="4.105128417969" />
                  <Point X="-1.977079345703" Y="4.072905029297" />
                  <Point X="-1.943763549805" Y="4.055561767578" />
                  <Point X="-1.934326782227" Y="4.051286132812" />
                  <Point X="-1.915046875" Y="4.0437890625" />
                  <Point X="-1.905203491211" Y="4.040567138672" />
                  <Point X="-1.88429699707" Y="4.034965576172" />
                  <Point X="-1.874162353516" Y="4.032834716797" />
                  <Point X="-1.853719482422" Y="4.029688232422" />
                  <Point X="-1.833055419922" Y="4.028785888672" />
                  <Point X="-1.812416137695" Y="4.030138427734" />
                  <Point X="-1.802132568359" Y="4.031378173828" />
                  <Point X="-1.780817138672" Y="4.035136474609" />
                  <Point X="-1.770728881836" Y="4.037488769531" />
                  <Point X="-1.750869506836" Y="4.04327734375" />
                  <Point X="-1.741098388672" Y="4.046713378906" />
                  <Point X="-1.676624755859" Y="4.073419433594" />
                  <Point X="-1.641924438477" Y="4.08779296875" />
                  <Point X="-1.632584350586" Y="4.092273193359" />
                  <Point X="-1.61444909668" Y="4.10222265625" />
                  <Point X="-1.605653808594" Y="4.107692382812" />
                  <Point X="-1.587924194336" Y="4.120106933594" />
                  <Point X="-1.579776733398" Y="4.126500976563" />
                  <Point X="-1.564225463867" Y="4.140139160156" />
                  <Point X="-1.550250976562" Y="4.155390136719" />
                  <Point X="-1.538019897461" Y="4.172071289062" />
                  <Point X="-1.532359741211" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516856201172" Y="4.208728515625" />
                  <Point X="-1.508525878906" Y="4.227660644531" />
                  <Point X="-1.504877441406" Y="4.237354003906" />
                  <Point X="-1.483892333984" Y="4.303909667969" />
                  <Point X="-1.472598022461" Y="4.33973046875" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991210938" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462753051758" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.478545410156" Y="4.557181640625" />
                  <Point X="-1.479266113281" Y="4.56265625" />
                  <Point X="-1.181232666016" Y="4.646214355469" />
                  <Point X="-0.931181030273" Y="4.716319824219" />
                  <Point X="-0.435858306885" Y="4.774290039063" />
                  <Point X="-0.365221984863" Y="4.782557128906" />
                  <Point X="-0.35129574585" Y="4.730583496094" />
                  <Point X="-0.225666290283" Y="4.261727539062" />
                  <Point X="-0.220435211182" Y="4.247107910156" />
                  <Point X="-0.207661819458" Y="4.218916503906" />
                  <Point X="-0.20011920166" Y="4.205344726562" />
                  <Point X="-0.182260971069" Y="4.178618164062" />
                  <Point X="-0.172608352661" Y="4.166456054688" />
                  <Point X="-0.151451293945" Y="4.1438671875" />
                  <Point X="-0.126898040771" Y="4.125026367188" />
                  <Point X="-0.099602851868" Y="4.110436523438" />
                  <Point X="-0.085356750488" Y="4.104260742188" />
                  <Point X="-0.054918632507" Y="4.093927978516" />
                  <Point X="-0.039857185364" Y="4.090155273438" />
                  <Point X="-0.009319933891" Y="4.08511328125" />
                  <Point X="0.021631828308" Y="4.08511328125" />
                  <Point X="0.052169078827" Y="4.090155273438" />
                  <Point X="0.067230377197" Y="4.093927978516" />
                  <Point X="0.097668495178" Y="4.104260742188" />
                  <Point X="0.111914596558" Y="4.110436523438" />
                  <Point X="0.139209793091" Y="4.125026367188" />
                  <Point X="0.163763183594" Y="4.143866699219" />
                  <Point X="0.184920257568" Y="4.166455566406" />
                  <Point X="0.194572860718" Y="4.178617675781" />
                  <Point X="0.212431091309" Y="4.205344238281" />
                  <Point X="0.219973709106" Y="4.218916503906" />
                  <Point X="0.232747253418" Y="4.247107910156" />
                  <Point X="0.237978179932" Y="4.261727539062" />
                  <Point X="0.305593292236" Y="4.514070800781" />
                  <Point X="0.378190216064" Y="4.785006347656" />
                  <Point X="0.609528198242" Y="4.760778808594" />
                  <Point X="0.82787689209" Y="4.737912109375" />
                  <Point X="1.237675415039" Y="4.638974121094" />
                  <Point X="1.453595703125" Y="4.58684375" />
                  <Point X="1.718846923828" Y="4.490635742188" />
                  <Point X="1.858251831055" Y="4.440072265625" />
                  <Point X="2.116185546875" Y="4.319445800781" />
                  <Point X="2.250454101562" Y="4.25665234375" />
                  <Point X="2.499672851562" Y="4.11145703125" />
                  <Point X="2.629433105469" Y="4.035858398438" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.625458251953" Y="3.568805664062" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053181396484" Y="2.573438476562" />
                  <Point X="2.044182250977" Y="2.549564208984" />
                  <Point X="2.041301635742" Y="2.540598876953" />
                  <Point X="2.027343994141" Y="2.488404296875" />
                  <Point X="2.01983190918" Y="2.4603125" />
                  <Point X="2.017912719727" Y="2.451465820312" />
                  <Point X="2.013646972656" Y="2.420223632812" />
                  <Point X="2.012755615234" Y="2.3832421875" />
                  <Point X="2.013411254883" Y="2.369580078125" />
                  <Point X="2.018853637695" Y="2.324446533203" />
                  <Point X="2.021782592773" Y="2.300155273438" />
                  <Point X="2.02380078125" Y="2.289034667969" />
                  <Point X="2.029143554687" Y="2.267111572266" />
                  <Point X="2.032468139648" Y="2.256309082031" />
                  <Point X="2.04073449707" Y="2.234219726562" />
                  <Point X="2.045318359375" Y="2.223888916016" />
                  <Point X="2.055680908203" Y="2.203843994141" />
                  <Point X="2.061459716797" Y="2.194129882813" />
                  <Point X="2.08938671875" Y="2.152972412109" />
                  <Point X="2.104417480469" Y="2.130821289063" />
                  <Point X="2.109808349609" Y="2.123633056641" />
                  <Point X="2.130465087891" Y="2.100121337891" />
                  <Point X="2.157598632812" Y="2.075386230469" />
                  <Point X="2.168258056641" Y="2.066981445312" />
                  <Point X="2.209415283203" Y="2.039054199219" />
                  <Point X="2.231566650391" Y="2.024023925781" />
                  <Point X="2.241279541016" Y="2.018245361328" />
                  <Point X="2.26132421875" Y="2.00788269043" />
                  <Point X="2.271656005859" Y="2.003298461914" />
                  <Point X="2.293743896484" Y="1.995032470703" />
                  <Point X="2.304546875" Y="1.991707641602" />
                  <Point X="2.326470214844" Y="1.986364868164" />
                  <Point X="2.337590576172" Y="1.984346801758" />
                  <Point X="2.382724121094" Y="1.978904541016" />
                  <Point X="2.407015380859" Y="1.975975219727" />
                  <Point X="2.416043945312" Y="1.975320922852" />
                  <Point X="2.447575195312" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822631836" />
                  <Point X="2.497748535156" Y="1.982395996094" />
                  <Point X="2.549943115234" Y="1.996353393555" />
                  <Point X="2.578034912109" Y="2.003865600586" />
                  <Point X="2.583995117188" Y="2.005670776367" />
                  <Point X="2.604405761719" Y="2.013067749023" />
                  <Point X="2.627655517578" Y="2.02357434082" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.158209472656" Y="2.329350585938" />
                  <Point X="3.940405029297" Y="2.780951416016" />
                  <Point X="3.966981445313" Y="2.744016113281" />
                  <Point X="4.043949462891" Y="2.637048095703" />
                  <Point X="4.136884765625" Y="2.483471923828" />
                  <Point X="3.910441162109" Y="2.309715332031" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168138427734" Y="1.739869262695" />
                  <Point X="3.152119384766" Y="1.725216918945" />
                  <Point X="3.134668457031" Y="1.706603149414" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.09101171875" Y="1.650416748047" />
                  <Point X="3.070794189453" Y="1.624041259766" />
                  <Point X="3.065635253906" Y="1.616602172852" />
                  <Point X="3.04973828125" Y="1.589370483398" />
                  <Point X="3.034762695312" Y="1.555545288086" />
                  <Point X="3.030140380859" Y="1.542672363281" />
                  <Point X="3.016147460938" Y="1.492637207031" />
                  <Point X="3.008616455078" Y="1.465707641602" />
                  <Point X="3.006225097656" Y="1.454661132812" />
                  <Point X="3.002771484375" Y="1.432361572266" />
                  <Point X="3.001709228516" Y="1.421108642578" />
                  <Point X="3.000893310547" Y="1.397538330078" />
                  <Point X="3.001174804688" Y="1.386239379883" />
                  <Point X="3.003077880859" Y="1.363755615234" />
                  <Point X="3.004699462891" Y="1.352570800781" />
                  <Point X="3.016186035156" Y="1.296900268555" />
                  <Point X="3.022368408203" Y="1.266937988281" />
                  <Point X="3.024597900391" Y="1.25823449707" />
                  <Point X="3.034684814453" Y="1.228606201172" />
                  <Point X="3.050287109375" Y="1.195370361328" />
                  <Point X="3.056918701172" Y="1.183525756836" />
                  <Point X="3.088161132812" Y="1.136038574219" />
                  <Point X="3.104976318359" Y="1.11048046875" />
                  <Point X="3.111739257813" Y="1.101424316406" />
                  <Point X="3.126292724609" Y="1.08417956543" />
                  <Point X="3.134083251953" Y="1.075991088867" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034667969" Y="1.052695922852" />
                  <Point X="3.178244384766" Y="1.039370117188" />
                  <Point X="3.18774609375" Y="1.03324987793" />
                  <Point X="3.233020751953" Y="1.007763916016" />
                  <Point X="3.257388183594" Y="0.994047485352" />
                  <Point X="3.265479492188" Y="0.989987854004" />
                  <Point X="3.294678955078" Y="0.97808404541" />
                  <Point X="3.330275634766" Y="0.968021240234" />
                  <Point X="3.343671142578" Y="0.965257629395" />
                  <Point X="3.404885498047" Y="0.957167297363" />
                  <Point X="3.437831787109" Y="0.952812988281" />
                  <Point X="3.444030029297" Y="0.952199768066" />
                  <Point X="3.465716308594" Y="0.951222839355" />
                  <Point X="3.491217529297" Y="0.952032409668" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="3.996635253906" Y="1.018101074219" />
                  <Point X="4.704703613281" Y="1.11132019043" />
                  <Point X="4.719625488281" Y="1.050025146484" />
                  <Point X="4.75268359375" Y="0.914234069824" />
                  <Point X="4.78387109375" Y="0.713921020508" />
                  <Point X="4.537170898438" Y="0.647817810059" />
                  <Point X="3.691991943359" Y="0.421352752686" />
                  <Point X="3.686031738281" Y="0.419544311523" />
                  <Point X="3.665627197266" Y="0.412138122559" />
                  <Point X="3.642381591797" Y="0.401619659424" />
                  <Point X="3.634004394531" Y="0.397316558838" />
                  <Point X="3.573863037109" Y="0.36255368042" />
                  <Point X="3.541494384766" Y="0.343844116211" />
                  <Point X="3.533881835938" Y="0.338945800781" />
                  <Point X="3.508773925781" Y="0.319870513916" />
                  <Point X="3.481993652344" Y="0.294350769043" />
                  <Point X="3.472796875" Y="0.284226715088" />
                  <Point X="3.436712158203" Y="0.238246353149" />
                  <Point X="3.417291015625" Y="0.213499008179" />
                  <Point X="3.410854248047" Y="0.204207855225" />
                  <Point X="3.399130126953" Y="0.184927597046" />
                  <Point X="3.393842529297" Y="0.174938354492" />
                  <Point X="3.384068847656" Y="0.15347467041" />
                  <Point X="3.380005371094" Y="0.142928543091" />
                  <Point X="3.373159179688" Y="0.121427711487" />
                  <Point X="3.370376464844" Y="0.110473167419" />
                  <Point X="3.358348144531" Y="0.047666164398" />
                  <Point X="3.351874267578" Y="0.013862774849" />
                  <Point X="3.350603515625" Y="0.004967991352" />
                  <Point X="3.348584472656" Y="-0.026261991501" />
                  <Point X="3.350280029297" Y="-0.062940788269" />
                  <Point X="3.351874267578" Y="-0.076422821045" />
                  <Point X="3.363902587891" Y="-0.139229812622" />
                  <Point X="3.370376464844" Y="-0.173033203125" />
                  <Point X="3.373159179688" Y="-0.183987762451" />
                  <Point X="3.380005371094" Y="-0.205488586426" />
                  <Point X="3.384068847656" Y="-0.216034713745" />
                  <Point X="3.393842529297" Y="-0.237498245239" />
                  <Point X="3.399129882813" Y="-0.247487335205" />
                  <Point X="3.410854003906" Y="-0.266767883301" />
                  <Point X="3.417290771484" Y="-0.276059051514" />
                  <Point X="3.453375488281" Y="-0.322039550781" />
                  <Point X="3.472796630859" Y="-0.346786773682" />
                  <Point X="3.478718505859" Y="-0.35363381958" />
                  <Point X="3.501139404297" Y="-0.375805236816" />
                  <Point X="3.530176025391" Y="-0.39872467041" />
                  <Point X="3.541494384766" Y="-0.406404022217" />
                  <Point X="3.601635742188" Y="-0.441166931152" />
                  <Point X="3.634004394531" Y="-0.459876617432" />
                  <Point X="3.639496582031" Y="-0.462815338135" />
                  <Point X="3.659157958984" Y="-0.472016723633" />
                  <Point X="3.683027587891" Y="-0.481027740479" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.146875488281" Y="-0.605798583984" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.780071777344" Y="-0.808621887207" />
                  <Point X="4.76161328125" Y="-0.931051208496" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="4.416951660156" Y="-1.038295776367" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535705566" />
                  <Point X="3.400098632812" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354480957031" Y="-0.912676208496" />
                  <Point X="3.236444824219" Y="-0.938331787109" />
                  <Point X="3.172916748047" Y="-0.952139770508" />
                  <Point X="3.157873046875" Y="-0.956742614746" />
                  <Point X="3.128752441406" Y="-0.968367431641" />
                  <Point X="3.114675537109" Y="-0.975389465332" />
                  <Point X="3.086849121094" Y="-0.992281738281" />
                  <Point X="3.074124023438" Y="-1.001530273438" />
                  <Point X="3.050374267578" Y="-1.022000854492" />
                  <Point X="3.039349609375" Y="-1.03322277832" />
                  <Point X="2.968004150391" Y="-1.119029052734" />
                  <Point X="2.92960546875" Y="-1.1652109375" />
                  <Point X="2.921325927734" Y="-1.17684765625" />
                  <Point X="2.906605224609" Y="-1.201230224609" />
                  <Point X="2.9001640625" Y="-1.213976074219" />
                  <Point X="2.888820800781" Y="-1.241361328125" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577880859" />
                  <Point X="2.875157226562" Y="-1.29666027832" />
                  <Point X="2.864931640625" Y="-1.407783081055" />
                  <Point X="2.859428222656" Y="-1.467590576172" />
                  <Point X="2.859288574219" Y="-1.483320800781" />
                  <Point X="2.861607177734" Y="-1.514589233398" />
                  <Point X="2.864065429688" Y="-1.530127319336" />
                  <Point X="2.871796875" Y="-1.561748291016" />
                  <Point X="2.876785888672" Y="-1.57666809082" />
                  <Point X="2.889157470703" Y="-1.605479614258" />
                  <Point X="2.896540039062" Y="-1.619371582031" />
                  <Point X="2.961863037109" Y="-1.720977050781" />
                  <Point X="2.997020263672" Y="-1.775662109375" />
                  <Point X="3.0017421875" Y="-1.782353881836" />
                  <Point X="3.019793945312" Y="-1.804450317383" />
                  <Point X="3.043489501953" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.83627746582" />
                  <Point X="3.474930908203" Y="-2.160193359375" />
                  <Point X="4.087170898438" Y="-2.629981689453" />
                  <Point X="4.045488037109" Y="-2.697431152344" />
                  <Point X="4.001274658203" Y="-2.760251953125" />
                  <Point X="3.721427978516" Y="-2.598682373047" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.630626708984" Y="-2.040966674805" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136108398" />
                  <Point X="2.415068603516" Y="-2.044959960938" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.283883056641" Y="-2.112530029297" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968261719" Y="-2.153170410156" />
                  <Point X="2.186037597656" Y="-2.170063476562" />
                  <Point X="2.175209472656" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333496094" />
                  <Point X="2.144939453125" Y="-2.211161621094" />
                  <Point X="2.128046386719" Y="-2.234092285156" />
                  <Point X="2.120463623047" Y="-2.246194824219" />
                  <Point X="2.059042236328" Y="-2.362900634766" />
                  <Point X="2.025984741211" Y="-2.425712890625" />
                  <Point X="2.0198359375" Y="-2.440192626953" />
                  <Point X="2.010012207031" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264648438" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.02755871582" Y="-2.720623779297" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.340810791016" Y="-3.343421630859" />
                  <Point X="2.735892822266" Y="-4.027724365234" />
                  <Point X="2.723754394531" Y="-4.036083496094" />
                  <Point X="2.50007421875" Y="-3.744578125" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828654296875" Y="-2.870147216797" />
                  <Point X="1.808830810547" Y="-2.849625732422" />
                  <Point X="1.783251464844" Y="-2.828004150391" />
                  <Point X="1.773298950195" Y="-2.820647216797" />
                  <Point X="1.63474609375" Y="-2.7315703125" />
                  <Point X="1.56017578125" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553710938" Y="-2.658885742188" />
                  <Point X="1.470932861328" Y="-2.651154052734" />
                  <Point X="1.45539453125" Y="-2.648695800781" />
                  <Point X="1.424125488281" Y="-2.646376953125" />
                  <Point X="1.40839453125" Y="-2.646516357422" />
                  <Point X="1.256863037109" Y="-2.660460449219" />
                  <Point X="1.175307617188" Y="-2.667965332031" />
                  <Point X="1.161224975586" Y="-2.670339111328" />
                  <Point X="1.133575317383" Y="-2.677170898438" />
                  <Point X="1.120008300781" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495483398" Y="-2.714133789062" />
                  <Point X="1.043858764648" Y="-2.722413085938" />
                  <Point X="0.926850097656" Y="-2.819701904297" />
                  <Point X="0.863875" Y="-2.872063476562" />
                  <Point X="0.852652954102" Y="-2.883088378906" />
                  <Point X="0.832182373047" Y="-2.906838378906" />
                  <Point X="0.82293359375" Y="-2.919563720703" />
                  <Point X="0.806041259766" Y="-2.947390380859" />
                  <Point X="0.799019287109" Y="-2.961467529297" />
                  <Point X="0.787394592285" Y="-2.990588134766" />
                  <Point X="0.782791870117" Y="-3.005631347656" />
                  <Point X="0.747806945801" Y="-3.16658984375" />
                  <Point X="0.728977600098" Y="-3.253219238281" />
                  <Point X="0.727584838867" Y="-3.261289306641" />
                  <Point X="0.72472442627" Y="-3.289677734375" />
                  <Point X="0.72474230957" Y="-3.323170410156" />
                  <Point X="0.725554931641" Y="-3.335520019531" />
                  <Point X="0.802428955078" Y="-3.919437011719" />
                  <Point X="0.833091247559" Y="-4.15233984375" />
                  <Point X="0.655065063477" Y="-3.487936523438" />
                  <Point X="0.652606201172" Y="-3.480124511719" />
                  <Point X="0.642145080566" Y="-3.453580322266" />
                  <Point X="0.626786865234" Y="-3.423815673828" />
                  <Point X="0.620407531738" Y="-3.413210205078" />
                  <Point X="0.513967224121" Y="-3.259849853516" />
                  <Point X="0.456679901123" Y="-3.177309814453" />
                  <Point X="0.44667074585" Y="-3.165172851562" />
                  <Point X="0.424786773682" Y="-3.142717529297" />
                  <Point X="0.412912261963" Y="-3.132399169922" />
                  <Point X="0.38665713501" Y="-3.113155273438" />
                  <Point X="0.373242706299" Y="-3.104937988281" />
                  <Point X="0.345241577148" Y="-3.090829589844" />
                  <Point X="0.330654663086" Y="-3.084938476562" />
                  <Point X="0.165944519043" Y="-3.033818359375" />
                  <Point X="0.077295715332" Y="-3.006305175781" />
                  <Point X="0.063376434326" Y="-3.003109130859" />
                  <Point X="0.035216854095" Y="-2.99883984375" />
                  <Point X="0.020976697922" Y="-2.997766601562" />
                  <Point X="-0.008664803505" Y="-2.997766601562" />
                  <Point X="-0.022905256271" Y="-2.998840087891" />
                  <Point X="-0.051064540863" Y="-3.003109375" />
                  <Point X="-0.064983673096" Y="-3.006305175781" />
                  <Point X="-0.229693969727" Y="-3.057425048828" />
                  <Point X="-0.31834262085" Y="-3.084938476562" />
                  <Point X="-0.332929382324" Y="-3.090829589844" />
                  <Point X="-0.36093081665" Y="-3.104937988281" />
                  <Point X="-0.374345367432" Y="-3.113155273438" />
                  <Point X="-0.400600494385" Y="-3.132399169922" />
                  <Point X="-0.412475036621" Y="-3.142717529297" />
                  <Point X="-0.434358703613" Y="-3.165172607422" />
                  <Point X="-0.444367736816" Y="-3.177309326172" />
                  <Point X="-0.550808349609" Y="-3.330669433594" />
                  <Point X="-0.608095336914" Y="-3.413209716797" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777770996" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.782913757324" Y="-4.011023925781" />
                  <Point X="-0.985425231934" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.93277066625" Y="-3.748249685001" />
                  <Point X="-2.471791302434" Y="-4.121543413452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.968429355716" Y="-2.787347655672" />
                  <Point X="-3.729380086298" Y="-2.980925937194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.884678539532" Y="-3.66495176258" />
                  <Point X="-2.41393904226" Y="-4.046149091265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.124313523993" Y="-2.538872986414" />
                  <Point X="-3.641254397822" Y="-2.930046553876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.836586412813" Y="-3.581653840159" />
                  <Point X="-2.343083586358" Y="-3.981284549359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.135066006455" Y="-2.407923639039" />
                  <Point X="-3.553128709346" Y="-2.879167170559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.788494310378" Y="-3.498355898075" />
                  <Point X="-2.260373874561" Y="-3.926019394603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.057555886949" Y="-2.348447937466" />
                  <Point X="-3.46500302087" Y="-2.828287787241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.740402215259" Y="-3.415057950065" />
                  <Point X="-2.177663997142" Y="-3.870754373964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.138712428828" Y="-4.712080765248" />
                  <Point X="-1.102693758343" Y="-4.741248109504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.980045767444" Y="-2.288972235893" />
                  <Point X="-3.376877332393" Y="-2.777408403924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.69231012014" Y="-3.331760002056" />
                  <Point X="-2.09491435749" Y="-3.815521552148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.124169359108" Y="-4.601615352142" />
                  <Point X="-0.973791490442" Y="-4.723388949133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.902535647938" Y="-2.229496534319" />
                  <Point X="-3.288751643917" Y="-2.726529020606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.644218025021" Y="-3.248462054046" />
                  <Point X="-1.989273561381" Y="-3.778825623331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.127516865103" Y="-4.476662436475" />
                  <Point X="-0.946876770758" Y="-4.622941900631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.82502548234" Y="-2.170020870071" />
                  <Point X="-3.200625955441" Y="-2.675649637289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.596125929902" Y="-3.165164106036" />
                  <Point X="-1.832758554752" Y="-3.783326817894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.156501245056" Y="-4.330949189618" />
                  <Point X="-0.919962051074" Y="-4.52249485213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.747515311703" Y="-2.110545209903" />
                  <Point X="-3.112500271438" Y="-2.624770250349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.548033834783" Y="-3.081866158027" />
                  <Point X="-1.668507709511" Y="-3.79409237105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.18640523708" Y="-4.184491255588" />
                  <Point X="-0.893047331389" Y="-4.422047803628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.673174962417" Y="-1.238718645823" />
                  <Point X="-4.620501169202" Y="-1.281373042536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.670005141067" Y="-2.051069549735" />
                  <Point X="-3.024374598521" Y="-2.573890854431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.499941739664" Y="-2.998568210017" />
                  <Point X="-0.866132611705" Y="-4.321600755126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.712542434934" Y="-1.084597336392" />
                  <Point X="-4.490654751779" Y="-1.264278439372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.592494970431" Y="-1.991593889566" />
                  <Point X="-2.936248925605" Y="-2.523011458514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.451849644545" Y="-2.915270262008" />
                  <Point X="-0.839217892021" Y="-4.221153706625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.746361381288" Y="-0.934969134854" />
                  <Point X="-4.360808334357" Y="-1.247183836209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.514984799795" Y="-1.932118229398" />
                  <Point X="-2.848123252688" Y="-2.472132062596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.403757549426" Y="-2.831972313998" />
                  <Point X="-0.812303172337" Y="-4.120706658123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.766134837546" Y="-0.796714746935" />
                  <Point X="-4.230961916934" Y="-1.230089233046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.437474629158" Y="-1.87264256923" />
                  <Point X="-2.759997579772" Y="-2.421252666679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.355665454307" Y="-2.748674365988" />
                  <Point X="-0.785388452652" Y="-4.020259609621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.785908293805" Y="-0.658460359017" />
                  <Point X="-4.101115499511" Y="-1.212994629883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.359964458522" Y="-1.813166909061" />
                  <Point X="-2.670505342126" Y="-2.371479893059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.317865618508" Y="-2.657041910716" />
                  <Point X="-0.758473755055" Y="-3.919812543235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.676149845381" Y="-0.625098839299" />
                  <Point X="-3.971269082088" Y="-1.195900026719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.282454287886" Y="-1.753691248893" />
                  <Point X="-2.548001591005" Y="-2.348439315964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.322799659425" Y="-2.530804244402" />
                  <Point X="-0.731559059693" Y="-3.819365475037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.562724572949" Y="-0.594706655115" />
                  <Point X="-3.841422664665" Y="-1.178805423556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.20494411725" Y="-1.694215588724" />
                  <Point X="-0.704644364332" Y="-3.718918406839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.449299301507" Y="-0.56431447013" />
                  <Point X="-3.711576254996" Y="-1.161710814114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.127433946613" Y="-1.634739928556" />
                  <Point X="-0.67772966897" Y="-3.618471338641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.33587403383" Y="-0.533922282096" />
                  <Point X="-3.581729847416" Y="-1.144616202981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.049923775977" Y="-1.575264268388" />
                  <Point X="-0.650814973609" Y="-3.518024270443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.222448766153" Y="-0.503530094061" />
                  <Point X="-3.451883439835" Y="-1.127521591847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.988583816227" Y="-1.50269422963" />
                  <Point X="-0.614979393261" Y="-3.424801192469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.109023498476" Y="-0.473137906027" />
                  <Point X="-3.322037032255" Y="-1.110426980714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.953331152368" Y="-1.408999115191" />
                  <Point X="-0.561407405581" Y="-3.345940773959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.995598230799" Y="-0.442745717993" />
                  <Point X="-3.180797110145" Y="-1.102558655728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.958685292769" Y="-1.282421259023" />
                  <Point X="-0.507091840212" Y="-3.267682492789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.882172963122" Y="-0.412353529959" />
                  <Point X="-0.452776197766" Y="-3.189424274035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.768747695445" Y="-0.381961341925" />
                  <Point X="-0.385714008079" Y="-3.121488005714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818074563965" Y="-4.096296770698" />
                  <Point X="0.826624539519" Y="-4.103220404386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.655322427768" Y="-0.35156915389" />
                  <Point X="-0.290545998774" Y="-3.076311381361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.776243329004" Y="-3.940180345778" />
                  <Point X="0.808610549831" Y="-3.966390804403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.773024293474" Y="0.67577012959" />
                  <Point X="-4.611953259799" Y="0.54533737831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.541897160091" Y="-0.321176965856" />
                  <Point X="-0.181415614306" Y="-3.042441265479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.734412094042" Y="-3.784063920858" />
                  <Point X="0.790596601442" Y="-3.829561237862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.756870980584" Y="0.784931593488" />
                  <Point X="-4.386345445949" Y="0.48488593165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.440044566503" Y="-0.281413411123" />
                  <Point X="-0.072285091983" Y="-3.008571261231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.692580859081" Y="-3.627947495937" />
                  <Point X="0.772582674629" Y="-3.692731688795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.74071797259" Y="0.894093304287" />
                  <Point X="-4.160737632098" Y="0.42443448499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.36537565406" Y="-0.219636945436" />
                  <Point X="0.075309963477" Y="-3.005849221761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.648675952194" Y="-3.470151844601" />
                  <Point X="0.754568747816" Y="-3.555902139727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.719952267957" Y="0.999519726997" />
                  <Point X="-3.935129864063" Y="0.36398307543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.315636926951" Y="-0.137672413721" />
                  <Point X="0.319756725528" Y="-3.081556147877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.480648138731" Y="-3.211843445367" />
                  <Point X="0.736554821003" Y="-3.419072590659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.692787945001" Y="1.099764650755" />
                  <Point X="-3.709522162175" Y="0.303531719435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.295647949219" Y="-0.031617009968" />
                  <Point X="0.724950556531" Y="-3.287433483812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665623622046" Y="1.200009574512" />
                  <Point X="-3.477667184123" Y="0.238021418951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.345641957342" Y="0.131109498326" />
                  <Point X="0.744644564255" Y="-3.181139218057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.63845929909" Y="1.30025449827" />
                  <Point X="0.767237753554" Y="-3.07719266325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.485659948843" Y="1.298762182917" />
                  <Point X="0.793221388398" Y="-2.975991637111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.305396689408" Y="1.275030032415" />
                  <Point X="0.843115913567" Y="-2.894153268177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.125133400071" Y="1.251297857699" />
                  <Point X="0.914654621299" Y="-2.829842012694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.944870110734" Y="1.227565682983" />
                  <Point X="0.989135732586" Y="-2.767913468629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.765766265319" Y="1.204772407441" />
                  <Point X="1.0659129951" Y="-2.707844311166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.640346138831" Y="1.22545135033" />
                  <Point X="1.16894703821" Y="-2.669037475392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.504907405267" Y="-3.750876849616" />
                  <Point X="2.638525041245" Y="-3.859078277784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.544212400625" Y="1.269845942839" />
                  <Point X="1.303959865214" Y="-2.656126548216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.257172797751" Y="-3.428023161219" />
                  <Point X="2.505979918146" Y="-3.629503194661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.480130072421" Y="1.340195255409" />
                  <Point X="1.444766899465" Y="-2.647907677554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.009438190428" Y="-3.10516947298" />
                  <Point X="2.373434795047" Y="-3.399928111538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.439401335554" Y="1.429455933362" />
                  <Point X="2.240889396187" Y="-3.170352805107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421653660087" Y="1.537326307902" />
                  <Point X="2.108343907291" Y="-2.940777425768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.220503656856" Y="2.306464438964" />
                  <Point X="-3.805043015659" Y="1.970031045301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.49793071222" Y="1.721336405579" />
                  <Point X="2.034432079365" Y="-2.758682648889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.172053130482" Y="2.389472135066" />
                  <Point X="2.008573595601" Y="-2.615500702854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.123602604107" Y="2.472479831168" />
                  <Point X="2.005490202924" Y="-2.490761661936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.075151971534" Y="2.555487441272" />
                  <Point X="2.040965961359" Y="-2.397247205923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.026701309929" Y="2.638495027866" />
                  <Point X="2.086075914959" Y="-2.311534367326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.973340821875" Y="2.717526715396" />
                  <Point X="2.133035835646" Y="-2.227319602539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.914995257683" Y="2.792521567865" />
                  <Point X="2.200306108006" Y="-2.159551836245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.85664969349" Y="2.867516420335" />
                  <Point X="2.289527249448" Y="-2.109559533248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.798304129298" Y="2.942511272804" />
                  <Point X="2.381020633612" Y="-2.061407256128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.739958572534" Y="3.017506131288" />
                  <Point X="2.489424403815" Y="-2.026948739616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.323185387457" Y="2.802252019309" />
                  <Point X="2.665460263717" Y="-2.047257609474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.075878771157" Y="2.724229228886" />
                  <Point X="2.940822201415" Y="-2.147999151212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950165524003" Y="2.744670807339" />
                  <Point X="3.466745527123" Y="-2.451641304295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.8699782197" Y="2.801978567409" />
                  <Point X="3.99266855577" Y="-2.755283216823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.803877854011" Y="2.870693705446" />
                  <Point X="4.054391758772" Y="-2.683023522332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.758213782817" Y="2.955957828462" />
                  <Point X="2.901636206794" Y="-1.627298323404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.751261978748" Y="3.072570527285" />
                  <Point X="2.859399256623" Y="-1.470853356784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.783347769918" Y="3.220795247427" />
                  <Point X="2.869615398032" Y="-1.356884066218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.914801687646" Y="3.449486689863" />
                  <Point X="2.886528379863" Y="-1.248337770099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.04734692073" Y="3.679061862051" />
                  <Point X="2.931708441392" Y="-1.162681703785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.988889524868" Y="3.753966155019" />
                  <Point X="2.992450621907" Y="-1.089627592947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.911348045503" Y="3.813416461879" />
                  <Point X="3.054986172293" Y="-1.018025724397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.833806572741" Y="3.872866774086" />
                  <Point X="3.139380510715" Y="-0.964124753383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.756265159634" Y="3.9323171346" />
                  <Point X="3.253821328213" Y="-0.934554941379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.678723746527" Y="3.991767495115" />
                  <Point X="3.374670394735" Y="-0.910174427115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.593885554138" Y="4.045309040273" />
                  <Point X="3.541517579595" Y="-0.923042454638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.504354687455" Y="4.095050532715" />
                  <Point X="3.721780849273" Y="-0.946774613435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.414823820773" Y="4.144792025157" />
                  <Point X="3.902044118951" Y="-0.970506772232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.32529295409" Y="4.1945335176" />
                  <Point X="4.082307388629" Y="-0.994238931029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.235762076741" Y="4.244275001404" />
                  <Point X="4.262570658307" Y="-1.017971089826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.819773534658" Y="4.029656280793" />
                  <Point X="3.370443981627" Y="-0.173298992703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.784694600827" Y="-0.508752529873" />
                  <Point X="4.44283387294" Y="-1.041703204048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.707213454719" Y="4.060749084043" />
                  <Point X="3.348925130804" Y="-0.033631212135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.010302361231" Y="-0.569203933253" />
                  <Point X="4.623096759245" Y="-1.065435052396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.610503031248" Y="4.104676686033" />
                  <Point X="3.363871621417" Y="0.076507517175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.23591011291" Y="-0.629655329568" />
                  <Point X="4.737670511133" Y="-1.035972888539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.539790277518" Y="4.169656785879" />
                  <Point X="3.393689946606" Y="0.1746032723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.461517851204" Y="-0.690106715044" />
                  <Point X="4.761214975003" Y="-0.93279666069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.498278403426" Y="4.258283291811" />
                  <Point X="3.448113394186" Y="0.252774192178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.687125589498" Y="-0.75055810052" />
                  <Point X="4.777760372254" Y="-0.823952700448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468676015727" Y="4.356553909668" />
                  <Point X="3.512611992002" Y="0.322786416263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468172140211" Y="4.47838803808" />
                  <Point X="3.597551387117" Y="0.376246009069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.436191566835" Y="4.574732839148" />
                  <Point X="3.692603375362" Y="0.421516585425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.324058114592" Y="4.606171118694" />
                  <Point X="3.806028618857" Y="0.451908793041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.211924662348" Y="4.63760939824" />
                  <Point X="3.060801629687" Y="1.177623868737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.313934890319" Y="0.972640596007" />
                  <Point X="3.919453862352" Y="0.482301000658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.099791143513" Y="4.669047623861" />
                  <Point X="3.006501422596" Y="1.343837468199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.490373465512" Y="0.952005613736" />
                  <Point X="4.032879105847" Y="0.512693208274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.987657599582" Y="4.70048582916" />
                  <Point X="2.03325897007" Y="2.254195825441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.371551770103" Y="1.980251717429" />
                  <Point X="3.008369503169" Y="1.464566885138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.620809596991" Y="0.968622675872" />
                  <Point X="4.146304349342" Y="0.54308541589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.865715905364" Y="4.723981550961" />
                  <Point X="-0.158098615712" Y="4.150964368188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.124455748136" Y="4.123720911194" />
                  <Point X="2.012987279284" Y="2.392853675726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.514371111865" Y="1.9868410536" />
                  <Point X="3.037986112707" Y="1.562825986376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.750656007909" Y="0.985717284303" />
                  <Point X="4.259729592837" Y="0.573477623507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.733821627785" Y="4.739417829667" />
                  <Point X="-0.248317929104" Y="4.346264686418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.06422240751" Y="4.093174512099" />
                  <Point X="2.030655658998" Y="2.5007882627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.622720030169" Y="2.021343988302" />
                  <Point X="3.087091924543" Y="1.645303042774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.880502418826" Y="1.002811892734" />
                  <Point X="4.373154836332" Y="0.603869831123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.601927350206" Y="4.754854108374" />
                  <Point X="-0.29014915798" Y="4.502381106411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.158020662124" Y="4.139460341931" />
                  <Point X="2.06395502099" Y="2.596065129804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.711680701264" Y="2.071547216027" />
                  <Point X="3.146614262253" Y="1.719344962838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.010348827158" Y="1.019906503259" />
                  <Point X="4.486580079827" Y="0.634262038739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.470033072627" Y="4.770290387081" />
                  <Point X="-0.331980386857" Y="4.658497526404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.217144256802" Y="4.213825157735" />
                  <Point X="2.111973025553" Y="2.679423075163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.799806404363" Y="2.122426587504" />
                  <Point X="3.221419911308" Y="1.7810107014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.140195213589" Y="1.037001131519" />
                  <Point X="4.600005304183" Y="0.664654261855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.250631382574" Y="4.308949976728" />
                  <Point X="2.160065115791" Y="2.762721027124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.887932107462" Y="2.17330595898" />
                  <Point X="3.298930088962" Y="1.840486355886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.27004160002" Y="1.054095759779" />
                  <Point X="4.713430513128" Y="0.695046497449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.277546070711" Y="4.409397050776" />
                  <Point X="2.20815720603" Y="2.846018979086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.97605781056" Y="2.224185330457" />
                  <Point X="3.376440266615" Y="1.899962010372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.399887986451" Y="1.07119038804" />
                  <Point X="4.775617873729" Y="0.766930324527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.304460758847" Y="4.509844124825" />
                  <Point X="2.256249296268" Y="2.929316931048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.064183513659" Y="2.275064701933" />
                  <Point X="3.453950444268" Y="1.959437664859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.529734372881" Y="1.0882850163" />
                  <Point X="4.753839781376" Y="0.906808034748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.331375455218" Y="4.610291192205" />
                  <Point X="2.304341386506" Y="3.01261488301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.152309216758" Y="2.325944073409" />
                  <Point X="3.531460621921" Y="2.018913319345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.659580759312" Y="1.10537964456" />
                  <Point X="4.717584450032" Y="1.058409181949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.35829015195" Y="4.710738259293" />
                  <Point X="2.352433476745" Y="3.095912834972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.240434900614" Y="2.376823460468" />
                  <Point X="3.608970799574" Y="2.078388973831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.423377137114" Y="4.780274016698" />
                  <Point X="2.400525566983" Y="3.179210786934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.328560583089" Y="2.427702848646" />
                  <Point X="3.686480977227" Y="2.137864628317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.596756463248" Y="4.762116365469" />
                  <Point X="2.448617657222" Y="3.262508738896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.416686265563" Y="2.478582236823" />
                  <Point X="3.763991154881" Y="2.197340282803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.770135268729" Y="4.743959135856" />
                  <Point X="2.49670974746" Y="3.345806690857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.504811948038" Y="2.529461625" />
                  <Point X="3.841501332534" Y="2.25681593729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.971328071562" Y="4.703278575288" />
                  <Point X="2.544801837698" Y="3.429104642819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.592937630513" Y="2.580341013178" />
                  <Point X="3.919011503136" Y="2.316291597486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.186409385821" Y="4.651351319922" />
                  <Point X="2.592893927937" Y="3.512402594781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.681063312988" Y="2.631220401355" />
                  <Point X="3.996521617015" Y="2.375767303615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.401491449752" Y="4.599423457484" />
                  <Point X="2.640986005972" Y="3.595700556625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.769188995463" Y="2.682099789532" />
                  <Point X="4.074031730895" Y="2.435243009744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.660782166284" Y="4.51169613404" />
                  <Point X="2.689078058414" Y="3.678998539193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.857314677938" Y="2.732979177709" />
                  <Point X="4.10945477827" Y="2.528800150332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.957510280241" Y="4.393652603918" />
                  <Point X="2.737170110857" Y="3.762296521761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.347385966913" Y="4.20017965668" />
                  <Point X="2.7852621633" Y="3.845594504329" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.731959533691" Y="-4.509014648438" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.357878326416" Y="-3.368183837891" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.109625442505" Y="-3.215279785156" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664908409" Y="-3.187766601562" />
                  <Point X="-0.173375213623" Y="-3.238886474609" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.39471963501" Y="-3.43900390625" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.599387878418" Y="-4.060199707031" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.996843566895" Y="-4.958136230469" />
                  <Point X="-1.100246948242" Y="-4.938065429688" />
                  <Point X="-1.291021606445" Y="-4.88898046875" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.340498779297" Y="-4.789153808594" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.349032714844" Y="-4.336936035156" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282592773" Y="-4.20262890625" />
                  <Point X="-1.537927001953" Y="-4.069639892578" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.850506591797" Y="-3.972571289062" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.157583984375" Y="-4.085848388672" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.338423583984" Y="-4.259844238281" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.704269287109" Y="-4.261456542969" />
                  <Point X="-2.855832275391" Y="-4.167612304688" />
                  <Point X="-3.120001953125" Y="-3.964210449219" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.989995605469" Y="-3.467366210938" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597592773438" />
                  <Point X="-2.513980224609" Y="-2.568763671875" />
                  <Point X="-2.531328857422" Y="-2.551415039062" />
                  <Point X="-2.560157714844" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.042836181641" Y="-2.803942871094" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-4.041959228516" Y="-3.004448974609" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.351091308594" Y="-2.5295546875" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-4.010563232422" Y="-2.072899169922" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013427734" />
                  <Point X="-3.138117431641" Y="-1.366266235352" />
                  <Point X="-3.140326416016" Y="-1.334595581055" />
                  <Point X="-3.161158935547" Y="-1.310639160156" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.789066894531" Y="-1.363552124023" />
                  <Point X="-4.803283691406" Y="-1.497076293945" />
                  <Point X="-4.880560058594" Y="-1.194541259766" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.977500488281" Y="-0.660846069336" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.522069824219" Y="-0.387110656738" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541895996094" Y="-0.121425231934" />
                  <Point X="-3.523853759766" Y="-0.108902862549" />
                  <Point X="-3.514143066406" Y="-0.102163200378" />
                  <Point X="-3.494898925781" Y="-0.075907623291" />
                  <Point X="-3.488884765625" Y="-0.056529830933" />
                  <Point X="-3.485647949219" Y="-0.046100585938" />
                  <Point X="-3.485647949219" Y="-0.016459506989" />
                  <Point X="-3.491662109375" Y="0.002918283463" />
                  <Point X="-3.494898925781" Y="0.013347529411" />
                  <Point X="-3.514143066406" Y="0.039603107452" />
                  <Point X="-3.532185302734" Y="0.052125473022" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.076624267578" Y="0.205193862915" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.947827636719" Y="0.792446289062" />
                  <Point X="-4.917645019531" Y="0.996418212891" />
                  <Point X="-4.816780761719" Y="1.368638793945" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.444719238281" Y="1.48501171875" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.691771240234" Y="1.408457641602" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639120117188" Y="1.443785888672" />
                  <Point X="-3.623096679688" Y="1.482469970703" />
                  <Point X="-3.61447265625" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551171875" />
                  <Point X="-3.635649902344" Y="1.582652099609" />
                  <Point X="-3.646055664062" Y="1.602641479492" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-3.957760009766" Y="1.847725341797" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.277293945313" Y="2.586078857422" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.892827880859" Y="3.130436767578" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.583828125" Y="3.172127441406" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.082898681641" Y="2.915569091797" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-2.973775634766" Y="2.966881347656" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382080078" />
                  <Point X="-2.93807421875" Y="3.027840820312" />
                  <Point X="-2.942940185547" Y="3.08345703125" />
                  <Point X="-2.945558837891" Y="3.113390380859" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.084028076172" Y="3.362595214844" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.957138427734" Y="4.017725585938" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.332065917969" Y="4.408124511719" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.097831787109" Y="4.456999023438" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246948242" Y="4.273660644531" />
                  <Point X="-1.889346435547" Y="4.241437011719" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124145508" Y="4.2184921875" />
                  <Point X="-1.81380859375" Y="4.222250488281" />
                  <Point X="-1.749335083008" Y="4.248956542969" />
                  <Point X="-1.714634765625" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.665098388672" Y="4.361044433594" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.666919921875" Y="4.532381835938" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.232524414062" Y="4.829159667969" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.457944213867" Y="4.963001953125" />
                  <Point X="-0.224199630737" Y="4.990358398438" />
                  <Point X="-0.167769882202" Y="4.779759277344" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282119751" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594028473" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.122067337036" Y="4.563246582031" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.629317993164" Y="4.949745605469" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="1.282265991211" Y="4.823667480469" />
                  <Point X="1.508456176758" Y="4.769057617188" />
                  <Point X="1.783631225586" Y="4.66925" />
                  <Point X="1.931037963867" Y="4.615784179688" />
                  <Point X="2.196674804688" Y="4.491555175781" />
                  <Point X="2.338695800781" Y="4.425136230469" />
                  <Point X="2.595318847656" Y="4.275626953125" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.974550292969" Y="4.023575439453" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.790003173828" Y="3.473805664062" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514892578" />
                  <Point X="2.21089453125" Y="2.4393203125" />
                  <Point X="2.203382324219" Y="2.411228515625" />
                  <Point X="2.202044677734" Y="2.392326171875" />
                  <Point X="2.207487060547" Y="2.347192626953" />
                  <Point X="2.210416015625" Y="2.322901367188" />
                  <Point X="2.218682373047" Y="2.300812011719" />
                  <Point X="2.246609375" Y="2.259654541016" />
                  <Point X="2.261640136719" Y="2.237503417969" />
                  <Point X="2.274940185547" Y="2.224203613281" />
                  <Point X="2.316097412109" Y="2.196276611328" />
                  <Point X="2.338248779297" Y="2.18124609375" />
                  <Point X="2.360336669922" Y="2.172980224609" />
                  <Point X="2.405470214844" Y="2.167537841797" />
                  <Point X="2.429761474609" Y="2.164608642578" />
                  <Point X="2.448664550781" Y="2.165946289062" />
                  <Point X="2.500859130859" Y="2.179903808594" />
                  <Point X="2.528950927734" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.063209472656" Y="2.493895507813" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.121206542969" Y="2.854987304688" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.337518066406" Y="2.518913330078" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="4.026105712891" Y="2.158978271484" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.27937109375" Y="1.583833129883" />
                  <Point X="3.241806396484" Y="1.534827270508" />
                  <Point X="3.221588867188" Y="1.508451782227" />
                  <Point X="3.213119384766" Y="1.4915" />
                  <Point X="3.199126464844" Y="1.44146484375" />
                  <Point X="3.191595458984" Y="1.41453527832" />
                  <Point X="3.190779541016" Y="1.390965087891" />
                  <Point X="3.202266113281" Y="1.335294555664" />
                  <Point X="3.208448486328" Y="1.305332275391" />
                  <Point X="3.215646728516" Y="1.287954833984" />
                  <Point X="3.246889160156" Y="1.240467529297" />
                  <Point X="3.263704345703" Y="1.214909423828" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.32622265625" Y="1.173334106445" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565673828" Y="1.153619628906" />
                  <Point X="3.429780029297" Y="1.145529418945" />
                  <Point X="3.462726318359" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.971835449219" Y="1.206475585938" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.904233886719" Y="1.094967285156" />
                  <Point X="4.939188476562" Y="0.951385559082" />
                  <Point X="4.981707519531" Y="0.678291992188" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.586346679688" Y="0.464291900635" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.668945556641" Y="0.198056564331" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.586180175781" Y="0.120946083069" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735275269" />
                  <Point X="3.54495703125" Y="0.011928283691" />
                  <Point X="3.538483154297" Y="-0.021875152588" />
                  <Point X="3.538483154297" Y="-0.040684940338" />
                  <Point X="3.550511474609" Y="-0.103492088318" />
                  <Point X="3.556985351562" Y="-0.13729536438" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.60284375" Y="-0.204739486694" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.696718261719" Y="-0.276669799805" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.196051269531" Y="-0.422272644043" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.967948730469" Y="-0.836947143555" />
                  <Point X="4.948431640625" Y="-0.966399108887" />
                  <Point X="4.893957519531" Y="-1.205112426758" />
                  <Point X="4.874545410156" Y="-1.290178344727" />
                  <Point X="4.392151855469" Y="-1.226670043945" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.3948359375" Y="-1.098341186523" />
                  <Point X="3.276799804688" Y="-1.123996826172" />
                  <Point X="3.213271728516" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.114099853516" Y="-1.240503417969" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070678711" />
                  <Point X="3.054132324219" Y="-1.425193603516" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.121683349609" Y="-1.618227294922" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.590595458984" Y="-2.009456176758" />
                  <Point X="4.339074707031" Y="-2.583784179688" />
                  <Point X="4.2591484375" Y="-2.713116455078" />
                  <Point X="4.204131835938" Y="-2.802141845703" />
                  <Point X="4.091476074219" Y="-2.962209472656" />
                  <Point X="4.056688232422" Y="-3.011638427734" />
                  <Point X="3.626427978516" Y="-2.763227294922" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.596859130859" Y="-2.227941894531" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.372371826172" Y="-2.280666259766" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.33468359375" />
                  <Point X="2.227178466797" Y="-2.451389404297" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.214533935547" Y="-2.686856201172" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.505355712891" Y="-3.248421630859" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.900581298828" Y="-4.14358203125" />
                  <Point X="2.835296875" Y="-4.190213378906" />
                  <Point X="2.709342041016" Y="-4.271741699219" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.349336914062" Y="-3.860242675781" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549194336" Y="-2.980467529297" />
                  <Point X="1.531996337891" Y="-2.891390625" />
                  <Point X="1.45742578125" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.27427355957" Y="-2.849661132812" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.04832434082" Y="-2.965798095703" />
                  <Point X="0.985349243164" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.933471801758" Y="-3.206944824219" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="0.990803527832" Y="-3.894637207031" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.056106323242" Y="-4.949708984375" />
                  <Point X="0.99434552002" Y="-4.963247070312" />
                  <Point X="0.877987854004" Y="-4.984385253906" />
                  <Point X="0.860200439453" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#182" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.125629508639" Y="4.823980206672" Z="1.65" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.65" />
                  <Point X="-0.469912334385" Y="5.043943110425" Z="1.65" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.65" />
                  <Point X="-1.252178203445" Y="4.908583028187" Z="1.65" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.65" />
                  <Point X="-1.722648312199" Y="4.557135362634" Z="1.65" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.65" />
                  <Point X="-1.718940200075" Y="4.407359683241" Z="1.65" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.65" />
                  <Point X="-1.77462546924" Y="4.326430634745" Z="1.65" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.65" />
                  <Point X="-1.872414581505" Y="4.317067670483" Z="1.65" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.65" />
                  <Point X="-2.064319912689" Y="4.518716837131" Z="1.65" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.65" />
                  <Point X="-2.362504832927" Y="4.483112020102" Z="1.65" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.65" />
                  <Point X="-2.993716423558" Y="4.088685555161" Z="1.65" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.65" />
                  <Point X="-3.133485194254" Y="3.368875200345" Z="1.65" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.65" />
                  <Point X="-2.998905846443" Y="3.11037973065" Z="1.65" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.65" />
                  <Point X="-3.015286797011" Y="3.033516770875" Z="1.65" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.65" />
                  <Point X="-3.084696734957" Y="2.99665885273" Z="1.65" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.65" />
                  <Point X="-3.564984147952" Y="3.246708754492" Z="1.65" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.65" />
                  <Point X="-3.938447569163" Y="3.192419266796" Z="1.65" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.65" />
                  <Point X="-4.326631857437" Y="2.642563931115" Z="1.65" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.65" />
                  <Point X="-3.994353963097" Y="1.839337683475" Z="1.65" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.65" />
                  <Point X="-3.686156501953" Y="1.590844785858" Z="1.65" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.65" />
                  <Point X="-3.675446429225" Y="1.532884212133" Z="1.65" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.65" />
                  <Point X="-3.712962534561" Y="1.487423517907" Z="1.65" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.65" />
                  <Point X="-4.444349026869" Y="1.565864070969" Z="1.65" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.65" />
                  <Point X="-4.871196482541" Y="1.412996219563" Z="1.65" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.65" />
                  <Point X="-5.003445168995" Y="0.831045370337" Z="1.65" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.65" />
                  <Point X="-4.095721209114" Y="0.188177663805" Z="1.65" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.65" />
                  <Point X="-3.566850179384" Y="0.042329351451" Z="1.65" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.65" />
                  <Point X="-3.5455709517" Y="0.019377686936" Z="1.65" />
                  <Point X="-3.539556741714" Y="0" Z="1.65" />
                  <Point X="-3.542793649435" Y="-0.010429264125" Z="1.65" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.65" />
                  <Point X="-3.55851841574" Y="-0.036546631683" Z="1.65" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.65" />
                  <Point X="-4.541166579658" Y="-0.307534392951" Z="1.65" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.65" />
                  <Point X="-5.033152444448" Y="-0.636645021899" Z="1.65" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.65" />
                  <Point X="-4.935160716439" Y="-1.175635487009" Z="1.65" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.65" />
                  <Point X="-3.788696396723" Y="-1.381844329081" Z="1.65" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.65" />
                  <Point X="-3.209892799467" Y="-1.312316924358" Z="1.65" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.65" />
                  <Point X="-3.195372060448" Y="-1.332858219357" Z="1.65" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.65" />
                  <Point X="-4.047157140572" Y="-2.00195148008" Z="1.65" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.65" />
                  <Point X="-4.40019080394" Y="-2.523884372212" Z="1.65" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.65" />
                  <Point X="-4.08790118129" Y="-3.003452242808" Z="1.65" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.65" />
                  <Point X="-3.023992208709" Y="-2.815964105668" Z="1.65" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.65" />
                  <Point X="-2.566769358056" Y="-2.561561111075" Z="1.65" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.65" />
                  <Point X="-3.039452907501" Y="-3.411086173783" Z="1.65" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.65" />
                  <Point X="-3.156661884968" Y="-3.972547513625" Z="1.65" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.65" />
                  <Point X="-2.736747004072" Y="-4.272687029783" Z="1.65" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.65" />
                  <Point X="-2.304911785335" Y="-4.259002307355" Z="1.65" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.65" />
                  <Point X="-2.135961502823" Y="-4.096141769278" Z="1.65" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.65" />
                  <Point X="-1.859932707158" Y="-3.991184258974" Z="1.65" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.65" />
                  <Point X="-1.577050342366" Y="-4.075951787535" Z="1.65" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.65" />
                  <Point X="-1.404227808407" Y="-4.315410260848" Z="1.65" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.65" />
                  <Point X="-1.396226996457" Y="-4.751347331373" Z="1.65" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.65" />
                  <Point X="-1.309636461511" Y="-4.906123274894" Z="1.65" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.65" />
                  <Point X="-1.012526887399" Y="-4.975940169876" Z="1.65" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.65" />
                  <Point X="-0.557247649777" Y="-4.04186060328" Z="1.65" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.65" />
                  <Point X="-0.359799490546" Y="-3.436233016523" Z="1.65" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.65" />
                  <Point X="-0.164710335927" Y="-3.255359378332" Z="1.65" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.65" />
                  <Point X="0.088648743434" Y="-3.231752666957" Z="1.65" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.65" />
                  <Point X="0.310646368887" Y="-3.365412739245" Z="1.65" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.65" />
                  <Point X="0.677507210624" Y="-4.490675426536" Z="1.65" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.65" />
                  <Point X="0.880768635723" Y="-5.002299882919" Z="1.65" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.65" />
                  <Point X="1.060656110338" Y="-4.967269420509" Z="1.65" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.65" />
                  <Point X="1.034219943712" Y="-3.856830841699" Z="1.65" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.65" />
                  <Point X="0.976175044097" Y="-3.186284326887" Z="1.65" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.65" />
                  <Point X="1.074135189454" Y="-2.972964221958" Z="1.65" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.65" />
                  <Point X="1.27269931876" Y="-2.868170622319" Z="1.65" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.65" />
                  <Point X="1.498801003679" Y="-2.902168561089" Z="1.65" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.65" />
                  <Point X="2.303513134109" Y="-3.859400712666" Z="1.65" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.65" />
                  <Point X="2.730355334248" Y="-4.282435738058" Z="1.65" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.65" />
                  <Point X="2.923487119629" Y="-4.152989213659" Z="1.65" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.65" />
                  <Point X="2.54250097529" Y="-3.192141592205" Z="1.65" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.65" />
                  <Point X="2.257582021067" Y="-2.646689940221" Z="1.65" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.65" />
                  <Point X="2.265268748407" Y="-2.443395971216" Z="1.65" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.65" />
                  <Point X="2.389502407156" Y="-2.29363256156" Z="1.65" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.65" />
                  <Point X="2.581816863292" Y="-2.245865988424" Z="1.65" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.65" />
                  <Point X="3.595272004902" Y="-2.775248524985" Z="1.65" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.65" />
                  <Point X="4.126208959577" Y="-2.959706466834" Z="1.65" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.65" />
                  <Point X="4.295525499896" Y="-2.708121650852" Z="1.65" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.65" />
                  <Point X="3.614877568823" Y="-1.938508454239" Z="1.65" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.65" />
                  <Point X="3.157585395189" Y="-1.559907958202" Z="1.65" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.65" />
                  <Point X="3.097765967032" Y="-1.398495076051" Z="1.65" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.65" />
                  <Point X="3.146390215393" Y="-1.241190446003" Z="1.65" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.65" />
                  <Point X="3.281263742898" Y="-1.141576062817" Z="1.65" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.65" />
                  <Point X="4.379469615134" Y="-1.244962211043" Z="1.65" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.65" />
                  <Point X="4.936548841618" Y="-1.184956267733" Z="1.65" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.65" />
                  <Point X="5.011234229298" Y="-0.813121155177" Z="1.65" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.65" />
                  <Point X="4.202836337661" Y="-0.342696334733" Z="1.65" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.65" />
                  <Point X="3.715584039563" Y="-0.202100971147" Z="1.65" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.65" />
                  <Point X="3.636021518632" Y="-0.142591039697" Z="1.65" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.65" />
                  <Point X="3.593462991689" Y="-0.062807115867" Z="1.65" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.65" />
                  <Point X="3.587908458735" Y="0.033803415333" Z="1.65" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.65" />
                  <Point X="3.619357919768" Y="0.121357698878" Z="1.65" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.65" />
                  <Point X="3.68781137479" Y="0.186047873095" Z="1.65" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.65" />
                  <Point X="4.593131479646" Y="0.447275598036" Z="1.65" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.65" />
                  <Point X="5.02495634205" Y="0.717264047407" Z="1.65" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.65" />
                  <Point X="4.946659059787" Y="1.138074233444" Z="1.65" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.65" />
                  <Point X="3.959153121651" Y="1.287327916409" Z="1.65" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.65" />
                  <Point X="3.430175145997" Y="1.22637833383" Z="1.65" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.65" />
                  <Point X="3.344593299115" Y="1.248185269817" Z="1.65" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.65" />
                  <Point X="3.282503516313" Y="1.299229146157" Z="1.65" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.65" />
                  <Point X="3.245078737367" Y="1.376678780796" Z="1.65" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.65" />
                  <Point X="3.241123128566" Y="1.459278597149" Z="1.65" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.65" />
                  <Point X="3.275333603712" Y="1.535689178235" Z="1.65" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.65" />
                  <Point X="4.050387674845" Y="2.150590690147" Z="1.65" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.65" />
                  <Point X="4.374139344331" Y="2.576079715703" Z="1.65" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.65" />
                  <Point X="4.155636071235" Y="2.915470254719" Z="1.65" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.65" />
                  <Point X="3.032053401006" Y="2.568476905092" Z="1.65" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.65" />
                  <Point X="2.481786262754" Y="2.259486692696" Z="1.65" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.65" />
                  <Point X="2.405300332547" Y="2.248458258457" Z="1.65" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.65" />
                  <Point X="2.338015467848" Y="2.268931101696" Z="1.65" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.65" />
                  <Point X="2.281827498539" Y="2.319009392534" Z="1.65" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.65" />
                  <Point X="2.250971444302" Y="2.384458111286" Z="1.65" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.65" />
                  <Point X="2.253041249961" Y="2.457683313128" Z="1.65" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.65" />
                  <Point X="2.827148447767" Y="3.480085680949" Z="1.65" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.65" />
                  <Point X="2.997371403445" Y="4.095602595563" Z="1.65" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.65" />
                  <Point X="2.614332830181" Y="4.350109857857" Z="1.65" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.65" />
                  <Point X="2.211700472314" Y="4.568126497772" Z="1.65" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.65" />
                  <Point X="1.794523883314" Y="4.74753386843" Z="1.65" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.65" />
                  <Point X="1.287844504293" Y="4.903550857606" Z="1.65" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.65" />
                  <Point X="0.628369881515" Y="5.030753790179" Z="1.65" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.65" />
                  <Point X="0.067615052914" Y="4.607467299279" Z="1.65" />
                  <Point X="0" Y="4.355124473572" Z="1.65" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>