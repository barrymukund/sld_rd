<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#209" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3352" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.926233276367" Y="-4.867002929688" />
                  <Point X="0.563302001953" Y="-3.512524414063" />
                  <Point X="0.557721374512" Y="-3.497142333984" />
                  <Point X="0.542363098145" Y="-3.467376953125" />
                  <Point X="0.394023803711" Y="-3.253648193359" />
                  <Point X="0.378635284424" Y="-3.2314765625" />
                  <Point X="0.356751220703" Y="-3.209020996094" />
                  <Point X="0.330495880127" Y="-3.189777099609" />
                  <Point X="0.3024949646" Y="-3.175668945312" />
                  <Point X="0.072948547363" Y="-3.104426025391" />
                  <Point X="0.049135822296" Y="-3.097035644531" />
                  <Point X="0.020975515366" Y="-3.092766357422" />
                  <Point X="-0.008665506363" Y="-3.092766601562" />
                  <Point X="-0.036824142456" Y="-3.097035888672" />
                  <Point X="-0.266370544434" Y="-3.168278564453" />
                  <Point X="-0.290183258057" Y="-3.175669189453" />
                  <Point X="-0.318184509277" Y="-3.189777587891" />
                  <Point X="-0.344439849854" Y="-3.209021484375" />
                  <Point X="-0.366323608398" Y="-3.231476806641" />
                  <Point X="-0.514662780762" Y="-3.445205322266" />
                  <Point X="-0.530051269531" Y="-3.467377197266" />
                  <Point X="-0.538188903809" Y="-3.481573730469" />
                  <Point X="-0.55099017334" Y="-3.512524414063" />
                  <Point X="-0.588639953613" Y="-3.653035644531" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-1.055445678711" Y="-4.84998828125" />
                  <Point X="-1.079339599609" Y="-4.845350585938" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.271347290039" Y="-4.240532226563" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.534981933594" Y="-3.945865966797" />
                  <Point X="-1.556905395508" Y="-3.926639404297" />
                  <Point X="-1.583188720703" Y="-3.910295410156" />
                  <Point X="-1.612886230469" Y="-3.897994384766" />
                  <Point X="-1.64302746582" Y="-3.890966308594" />
                  <Point X="-1.923519042969" Y="-3.872581787109" />
                  <Point X="-1.952616699219" Y="-3.870674804688" />
                  <Point X="-1.983418212891" Y="-3.873708496094" />
                  <Point X="-2.014466796875" Y="-3.882028076172" />
                  <Point X="-2.042657958984" Y="-3.894801513672" />
                  <Point X="-2.276378417969" Y="-4.050968994141" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.356240234375" Y="-4.127008789062" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.766692382812" Y="-4.111069335937" />
                  <Point X="-2.801706787109" Y="-4.089389404297" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-3.098228271484" Y="-3.844830566406" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.462522705078" Y="-2.485871337891" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-2.760371826172" Y="-2.531165283203" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-4.055194824219" Y="-2.830208007813" />
                  <Point X="-4.082858642578" Y="-2.79386328125" />
                  <Point X="-4.306142578125" Y="-2.419449951172" />
                  <Point X="-4.286053222656" Y="-2.404034912109" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.084577148438" Y="-1.475593261719" />
                  <Point X="-3.066612060547" Y="-1.448461547852" />
                  <Point X="-3.053856201172" Y="-1.419832397461" />
                  <Point X="-3.046875732422" Y="-1.392880859375" />
                  <Point X="-3.046151611328" Y="-1.390084960938" />
                  <Point X="-3.043347412109" Y="-1.359655761719" />
                  <Point X="-3.045556396484" Y="-1.327985595703" />
                  <Point X="-3.052557617188" Y="-1.298240722656" />
                  <Point X="-3.068639892578" Y="-1.272257324219" />
                  <Point X="-3.089472167969" Y="-1.248301269531" />
                  <Point X="-3.112972412109" Y="-1.228767211914" />
                  <Point X="-3.136966064453" Y="-1.214645629883" />
                  <Point X="-3.139455078125" Y="-1.213180541992" />
                  <Point X="-3.168717285156" Y="-1.201956665039" />
                  <Point X="-3.20060546875" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.38491796875" Y="-1.214525146484" />
                  <Point X="-4.732103027344" Y="-1.391885375977" />
                  <Point X="-4.823256835938" Y="-1.035019287109" />
                  <Point X="-4.834076171875" Y="-0.992662414551" />
                  <Point X="-4.892424316406" Y="-0.584698242188" />
                  <Point X="-4.877185546875" Y="-0.580614990234" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.517492431641" Y="-0.214827148438" />
                  <Point X="-3.487728271484" Y="-0.199469161987" />
                  <Point X="-3.462583740234" Y="-0.182017501831" />
                  <Point X="-3.459975341797" Y="-0.180207107544" />
                  <Point X="-3.437519287109" Y="-0.158322113037" />
                  <Point X="-3.418275146484" Y="-0.132065719604" />
                  <Point X="-3.40416796875" Y="-0.104065849304" />
                  <Point X="-3.395786376953" Y="-0.07706036377" />
                  <Point X="-3.394916992188" Y="-0.074258918762" />
                  <Point X="-3.390647460938" Y="-0.046099220276" />
                  <Point X="-3.390647460938" Y="-0.016460931778" />
                  <Point X="-3.394916992188" Y="0.011698768616" />
                  <Point X="-3.403298583984" Y="0.03870425415" />
                  <Point X="-3.40416796875" Y="0.041505847931" />
                  <Point X="-3.418275146484" Y="0.069505561829" />
                  <Point X="-3.437519287109" Y="0.095762115479" />
                  <Point X="-3.459975585938" Y="0.117646949768" />
                  <Point X="-3.485120117188" Y="0.135098754883" />
                  <Point X="-3.497016845703" Y="0.142132110596" />
                  <Point X="-3.515193603516" Y="0.151163024902" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-3.672332519531" Y="0.195215484619" />
                  <Point X="-4.89181640625" Y="0.521975158691" />
                  <Point X="-4.831460449219" Y="0.929854797363" />
                  <Point X="-4.824487792969" Y="0.976974609375" />
                  <Point X="-4.703551269531" Y="1.423268066406" />
                  <Point X="-3.765666015625" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341552734" />
                  <Point X="-3.723423828125" Y="1.301228149414" />
                  <Point X="-3.703137451172" Y="1.305263549805" />
                  <Point X="-3.647484619141" Y="1.322810791016" />
                  <Point X="-3.641711425781" Y="1.324631103516" />
                  <Point X="-3.622777832031" Y="1.332961914062" />
                  <Point X="-3.604034179688" Y="1.343783813477" />
                  <Point X="-3.587353759766" Y="1.356014526367" />
                  <Point X="-3.573715820312" Y="1.371565551758" />
                  <Point X="-3.561301513672" Y="1.389294677734" />
                  <Point X="-3.551351806641" Y="1.407430297852" />
                  <Point X="-3.529020996094" Y="1.461341674805" />
                  <Point X="-3.526704345703" Y="1.466934570312" />
                  <Point X="-3.520916503906" Y="1.486791503906" />
                  <Point X="-3.517157714844" Y="1.508106933594" />
                  <Point X="-3.515804443359" Y="1.528747680664" />
                  <Point X="-3.518950927734" Y="1.549192138672" />
                  <Point X="-3.524552734375" Y="1.570098999023" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.558994384766" Y="1.641137817383" />
                  <Point X="-3.561789550781" Y="1.646507202148" />
                  <Point X="-3.573281005859" Y="1.663705688477" />
                  <Point X="-3.587193603516" Y="1.680286132812" />
                  <Point X="-3.602135986328" Y="1.694590332031" />
                  <Point X="-3.682128662109" Y="1.755970825195" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.108245117188" Y="2.687245361328" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.206656494141" Y="2.844670654297" />
                  <Point X="-3.187723632813" Y="2.836340087891" />
                  <Point X="-3.167080810547" Y="2.829831542969" />
                  <Point X="-3.146794189453" Y="2.825796386719" />
                  <Point X="-3.069285400391" Y="2.819015136719" />
                  <Point X="-3.061244873047" Y="2.818311767578" />
                  <Point X="-3.040563232422" Y="2.818763183594" />
                  <Point X="-3.019104736328" Y="2.821588623047" />
                  <Point X="-2.999013427734" Y="2.826505126953" />
                  <Point X="-2.980462402344" Y="2.835653808594" />
                  <Point X="-2.962208496094" Y="2.847282958984" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.891061035156" Y="2.915245605469" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406982422" Y="2.937084228516" />
                  <Point X="-2.860777832031" Y="2.955338134766" />
                  <Point X="-2.851629150391" Y="2.973889160156" />
                  <Point X="-2.846712646484" Y="2.99398046875" />
                  <Point X="-2.843887207031" Y="3.015438964844" />
                  <Point X="-2.843435791016" Y="3.036120605469" />
                  <Point X="-2.850217041016" Y="3.113629394531" />
                  <Point X="-2.850920410156" Y="3.121670166016" />
                  <Point X="-2.854955566406" Y="3.141956787109" />
                  <Point X="-2.861464111328" Y="3.162599609375" />
                  <Point X="-2.869795166016" Y="3.181533203125" />
                  <Point X="-2.905242431641" Y="3.242929443359" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-2.747804931641" Y="4.058511474609" />
                  <Point X="-2.700625732422" Y="4.09468359375" />
                  <Point X="-2.167037109375" Y="4.391134277344" />
                  <Point X="-2.04319543457" Y="4.229740722656" />
                  <Point X="-2.028891723633" Y="4.214799316406" />
                  <Point X="-2.012311523438" Y="4.20088671875" />
                  <Point X="-1.99511315918" Y="4.189395019531" />
                  <Point X="-1.908846313477" Y="4.144486816406" />
                  <Point X="-1.899897216797" Y="4.139828125" />
                  <Point X="-1.880619384766" Y="4.132331054688" />
                  <Point X="-1.859713134766" Y="4.126729003906" />
                  <Point X="-1.839268920898" Y="4.12358203125" />
                  <Point X="-1.818628173828" Y="4.124935058594" />
                  <Point X="-1.797313232422" Y="4.128693359375" />
                  <Point X="-1.777454223633" Y="4.134481445312" />
                  <Point X="-1.687601196289" Y="4.171700195312" />
                  <Point X="-1.678280029297" Y="4.175561035156" />
                  <Point X="-1.660145874023" Y="4.185510253906" />
                  <Point X="-1.642416015625" Y="4.197924804688" />
                  <Point X="-1.626864624023" Y="4.211562988281" />
                  <Point X="-1.614633666992" Y="4.228243652344" />
                  <Point X="-1.603811523438" Y="4.246987792969" />
                  <Point X="-1.59548034668" Y="4.265921386719" />
                  <Point X="-1.566234985352" Y="4.35867578125" />
                  <Point X="-1.563201049805" Y="4.368297851562" />
                  <Point X="-1.559165771484" Y="4.388584472656" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.557730224609" Y="4.430827148438" />
                  <Point X="-1.561760253906" Y="4.4614375" />
                  <Point X="-1.584201782227" Y="4.631897949219" />
                  <Point X="-1.010711975098" Y="4.792684570312" />
                  <Point X="-0.949634643555" Y="4.80980859375" />
                  <Point X="-0.294710876465" Y="4.886458007812" />
                  <Point X="-0.133903366089" Y="4.286315429688" />
                  <Point X="-0.121129745483" Y="4.258124023438" />
                  <Point X="-0.103271453857" Y="4.231397460938" />
                  <Point X="-0.082114509583" Y="4.20880859375" />
                  <Point X="-0.054819351196" Y="4.19421875" />
                  <Point X="-0.024381277084" Y="4.183886230469" />
                  <Point X="0.006155910492" Y="4.178844238281" />
                  <Point X="0.036693107605" Y="4.183886230469" />
                  <Point X="0.06713117981" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583587646" Y="4.231397460938" />
                  <Point X="0.133441726685" Y="4.258124023438" />
                  <Point X="0.146215194702" Y="4.286315429688" />
                  <Point X="0.164377822876" Y="4.354099609375" />
                  <Point X="0.307419281006" Y="4.8879375" />
                  <Point X="0.790704040527" Y="4.837324707031" />
                  <Point X="0.844040710449" Y="4.831738769531" />
                  <Point X="1.427378662109" Y="4.690903320312" />
                  <Point X="1.481025634766" Y="4.677950683594" />
                  <Point X="1.8605859375" Y="4.540281738281" />
                  <Point X="1.894649291992" Y="4.527926757812" />
                  <Point X="2.261794189453" Y="4.356225585938" />
                  <Point X="2.294569824219" Y="4.340896972656" />
                  <Point X="2.649278320312" Y="4.134243652344" />
                  <Point X="2.680979736328" Y="4.115773925781" />
                  <Point X="2.943260253906" Y="3.929254638672" />
                  <Point X="2.928226074219" Y="3.90321484375" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075927734" Y="2.539930908203" />
                  <Point X="2.133077148438" Y="2.516057128906" />
                  <Point X="2.113625488281" Y="2.443316894531" />
                  <Point X="2.111156494141" Y="2.430734619141" />
                  <Point X="2.107800292969" Y="2.404286621094" />
                  <Point X="2.107727783203" Y="2.380954101562" />
                  <Point X="2.1153125" Y="2.318054443359" />
                  <Point X="2.116099121094" Y="2.311529296875" />
                  <Point X="2.12144140625" Y="2.289606933594" />
                  <Point X="2.129708007812" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247470703125" />
                  <Point X="2.178991210938" Y="2.190112304688" />
                  <Point X="2.187013183594" Y="2.179875488281" />
                  <Point X="2.204351074219" Y="2.160625488281" />
                  <Point X="2.221599365234" Y="2.145592041016" />
                  <Point X="2.278957763672" Y="2.106671875" />
                  <Point X="2.284907958984" Y="2.102634521484" />
                  <Point X="2.304953125" Y="2.092271728516" />
                  <Point X="2.327040771484" Y="2.084006103516" />
                  <Point X="2.348963623047" Y="2.078663574219" />
                  <Point X="2.41186328125" Y="2.071078857422" />
                  <Point X="2.425293701172" Y="2.070417724609" />
                  <Point X="2.450721435547" Y="2.070968505859" />
                  <Point X="2.473206054688" Y="2.074171142578" />
                  <Point X="2.545946533203" Y="2.093623046875" />
                  <Point X="2.553392089844" Y="2.0959453125" />
                  <Point X="2.573021484375" Y="2.102964599609" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="2.728800292969" Y="2.191127929688" />
                  <Point X="3.967326660156" Y="2.906191162109" />
                  <Point X="4.104468261719" Y="2.715595703125" />
                  <Point X="4.123271484375" Y="2.689463134766" />
                  <Point X="4.262198730469" Y="2.459884521484" />
                  <Point X="4.258575683594" Y="2.457104248047" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.221424560547" Y="1.660241333008" />
                  <Point X="3.203974121094" Y="1.641628051758" />
                  <Point X="3.151622802734" Y="1.573331665039" />
                  <Point X="3.144781982422" Y="1.563096191406" />
                  <Point X="3.130881103516" Y="1.539059082031" />
                  <Point X="3.121629638672" Y="1.51708605957" />
                  <Point X="3.102128662109" Y="1.447355102539" />
                  <Point X="3.100105712891" Y="1.440121337891" />
                  <Point X="3.09665234375" Y="1.417821899414" />
                  <Point X="3.095836425781" Y="1.394252197266" />
                  <Point X="3.097739501953" Y="1.371768188477" />
                  <Point X="3.113747802734" Y="1.29418371582" />
                  <Point X="3.117076904297" Y="1.28212487793" />
                  <Point X="3.125935546875" Y="1.256698852539" />
                  <Point X="3.136282470703" Y="1.235740234375" />
                  <Point X="3.179823242188" Y="1.169560180664" />
                  <Point X="3.184340087891" Y="1.162694824219" />
                  <Point X="3.19889453125" Y="1.14544934082" />
                  <Point X="3.216139160156" Y="1.12935925293" />
                  <Point X="3.234347900391" Y="1.116034301758" />
                  <Point X="3.297444335938" Y="1.080516357422" />
                  <Point X="3.309155517578" Y="1.074939941406" />
                  <Point X="3.333675537109" Y="1.065258300781" />
                  <Point X="3.356117919922" Y="1.059438598633" />
                  <Point X="3.441428710938" Y="1.048163574219" />
                  <Point X="3.448802001953" Y="1.047480224609" />
                  <Point X="3.470729736328" Y="1.046307250977" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="3.621447265625" Y="1.064526367188" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.837859863281" Y="0.96598223877" />
                  <Point X="4.845936035156" Y="0.932807922363" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="3.716579833984" Y="0.329589904785" />
                  <Point X="3.704790283203" Y="0.325586151123" />
                  <Point X="3.681545898438" Y="0.315067962646" />
                  <Point X="3.597730712891" Y="0.266621337891" />
                  <Point X="3.587870117188" Y="0.260069763184" />
                  <Point X="3.564863037109" Y="0.242623428345" />
                  <Point X="3.547530517578" Y="0.225576309204" />
                  <Point X="3.497241455078" Y="0.161496017456" />
                  <Point X="3.492024658203" Y="0.154848648071" />
                  <Point X="3.480300537109" Y="0.135568084717" />
                  <Point X="3.470527099609" Y="0.114104919434" />
                  <Point X="3.463680908203" Y="0.092604415894" />
                  <Point X="3.446917724609" Y="0.005074180126" />
                  <Point X="3.44540625" Y="-0.006882805347" />
                  <Point X="3.443667236328" Y="-0.034772926331" />
                  <Point X="3.445178710938" Y="-0.058553928375" />
                  <Point X="3.461941894531" Y="-0.14608416748" />
                  <Point X="3.463680908203" Y="-0.15516456604" />
                  <Point X="3.470527099609" Y="-0.17666506958" />
                  <Point X="3.480300537109" Y="-0.198128234863" />
                  <Point X="3.492024902344" Y="-0.217408813477" />
                  <Point X="3.542313964844" Y="-0.281488800049" />
                  <Point X="3.550679443359" Y="-0.290811004639" />
                  <Point X="3.570208496094" Y="-0.309879211426" />
                  <Point X="3.589035888672" Y="-0.324155548096" />
                  <Point X="3.672851074219" Y="-0.372602203369" />
                  <Point X="3.679078125" Y="-0.375899841309" />
                  <Point X="3.699853759766" Y="-0.385933197021" />
                  <Point X="3.716580078125" Y="-0.392150054932" />
                  <Point X="3.838770507813" Y="-0.424890808105" />
                  <Point X="4.891472167969" Y="-0.706961486816" />
                  <Point X="4.859532226562" Y="-0.918815551758" />
                  <Point X="4.855022460938" Y="-0.948725463867" />
                  <Point X="4.801173339844" Y="-1.184698974609" />
                  <Point X="4.790382324219" Y="-1.183278320313" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.40803515625" Y="-1.002710266113" />
                  <Point X="3.374658447266" Y="-1.005508850098" />
                  <Point X="3.210158935547" Y="-1.041263305664" />
                  <Point X="3.193094238281" Y="-1.04497253418" />
                  <Point X="3.163973876953" Y="-1.056597167969" />
                  <Point X="3.136147705078" Y="-1.073489257812" />
                  <Point X="3.112397705078" Y="-1.093960083008" />
                  <Point X="3.012968261719" Y="-1.21354309082" />
                  <Point X="3.002653564453" Y="-1.225948120117" />
                  <Point X="2.987932861328" Y="-1.250330322266" />
                  <Point X="2.976589355469" Y="-1.277716064453" />
                  <Point X="2.969757568359" Y="-1.305365722656" />
                  <Point X="2.955506835938" Y="-1.46023034668" />
                  <Point X="2.954028564453" Y="-1.476295898438" />
                  <Point X="2.956347412109" Y="-1.507564575195" />
                  <Point X="2.964078857422" Y="-1.539185058594" />
                  <Point X="2.976450195312" Y="-1.567996337891" />
                  <Point X="3.067486572266" Y="-1.70959753418" />
                  <Point X="3.072164306641" Y="-1.716232177734" />
                  <Point X="3.093228271484" Y="-1.743550048828" />
                  <Point X="3.110628417969" Y="-1.760909301758" />
                  <Point X="3.224021972656" Y="-1.847919189453" />
                  <Point X="4.213122558594" Y="-2.6068828125" />
                  <Point X="4.137520996094" Y="-2.729217041016" />
                  <Point X="4.124806152344" Y="-2.749791503906" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="4.017562255859" Y="-2.879352294922" />
                  <Point X="2.800954833984" Y="-2.176943359375" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.558444091797" Y="-2.124467529297" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609375" Y="-2.125353027344" />
                  <Point X="2.444833496094" Y="-2.135176757812" />
                  <Point X="2.282187988281" Y="-2.220775878906" />
                  <Point X="2.265315429688" Y="-2.229655761719" />
                  <Point X="2.242384765625" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508789062" />
                  <Point X="2.204531738281" Y="-2.290439453125" />
                  <Point X="2.118932617188" Y="-2.453084960938" />
                  <Point X="2.110052734375" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.131033203125" Y="-2.759038818359" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.224685791016" Y="-2.952287597656" />
                  <Point X="2.861283691406" Y="-4.054906738281" />
                  <Point X="2.796937255859" Y="-4.100868164062" />
                  <Point X="2.781851806641" Y="-4.111643554688" />
                  <Point X="2.701765136719" Y="-4.163481933594" />
                  <Point X="2.6869296875" Y="-4.144147949219" />
                  <Point X="1.758546020508" Y="-2.934254882812" />
                  <Point X="1.747504150391" Y="-2.9221796875" />
                  <Point X="1.721924316406" Y="-2.900557373047" />
                  <Point X="1.528831787109" Y="-2.776416748047" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989624023" Y="-2.751167236328" />
                  <Point X="1.448368041992" Y="-2.743435546875" />
                  <Point X="1.417099365234" Y="-2.741116699219" />
                  <Point X="1.205919799805" Y="-2.760549560547" />
                  <Point X="1.184012451172" Y="-2.762565673828" />
                  <Point X="1.156363037109" Y="-2.769397216797" />
                  <Point X="1.128978149414" Y="-2.780740234375" />
                  <Point X="1.104595825195" Y="-2.7954609375" />
                  <Point X="0.941528625488" Y="-2.931046142578" />
                  <Point X="0.924612304688" Y="-2.945111328125" />
                  <Point X="0.904141540527" Y="-2.968861328125" />
                  <Point X="0.887248962402" Y="-2.996688232422" />
                  <Point X="0.875624267578" Y="-3.025808837891" />
                  <Point X="0.826867919922" Y="-3.250126220703" />
                  <Point X="0.821809997559" Y="-3.273396728516" />
                  <Point X="0.81972454834" Y="-3.289627441406" />
                  <Point X="0.81974230957" Y="-3.323120117188" />
                  <Point X="0.840392028809" Y="-3.479971435547" />
                  <Point X="1.022065307617" Y="-4.859915039062" />
                  <Point X="0.98994744873" Y="-4.866955566406" />
                  <Point X="0.975677734375" Y="-4.870083496094" />
                  <Point X="0.929315429688" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.03734375" Y="-4.756729003906" />
                  <Point X="-1.058434082031" Y="-4.752635253906" />
                  <Point X="-1.141246337891" Y="-4.731328613281" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.178172729492" Y="-4.221998535156" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006958008" Y="-4.059779296875" />
                  <Point X="-1.472344116211" Y="-3.874441162109" />
                  <Point X="-1.494267456055" Y="-3.855214599609" />
                  <Point X="-1.506739013672" Y="-3.845965087891" />
                  <Point X="-1.533022338867" Y="-3.82962109375" />
                  <Point X="-1.546833984375" Y="-3.822526855469" />
                  <Point X="-1.576531494141" Y="-3.810225830078" />
                  <Point X="-1.591313598633" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.917305664063" Y="-3.77778515625" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928344727" Y="-3.776132324219" />
                  <Point X="-1.992729858398" Y="-3.779166015625" />
                  <Point X="-2.008006469727" Y="-3.781945556641" />
                  <Point X="-2.039054931641" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865722656" Y="-3.808269775391" />
                  <Point X="-2.095437255859" Y="-3.815812011719" />
                  <Point X="-2.329157714844" Y="-3.971979492188" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.431608886719" Y="-4.069176513672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.716681396484" Y="-4.030298828125" />
                  <Point X="-2.747581787109" Y="-4.011166015625" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.39534765625" Y="-2.418696289062" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-2.807871826172" Y="-2.448892822266" />
                  <Point X="-3.793089599609" Y="-3.017708496094" />
                  <Point X="-3.9796015625" Y="-2.772669921875" />
                  <Point X="-4.004013427734" Y="-2.740597412109" />
                  <Point X="-4.181265136719" Y="-2.443373046875" />
                  <Point X="-3.048122314453" Y="-1.573882202148" />
                  <Point X="-3.036481933594" Y="-1.563309814453" />
                  <Point X="-3.015104492188" Y="-1.540389404297" />
                  <Point X="-3.005367431641" Y="-1.528041503906" />
                  <Point X="-2.98740234375" Y="-1.500909912109" />
                  <Point X="-2.979835693359" Y="-1.48712512207" />
                  <Point X="-2.967079833984" Y="-1.45849597168" />
                  <Point X="-2.961890625" Y="-1.443651489258" />
                  <Point X="-2.95491015625" Y="-1.416699951172" />
                  <Point X="-2.951552490234" Y="-1.398802856445" />
                  <Point X="-2.948748291016" Y="-1.368373657227" />
                  <Point X="-2.948577636719" Y="-1.353045654297" />
                  <Point X="-2.950786621094" Y="-1.321375488281" />
                  <Point X="-2.953083496094" Y="-1.306219604492" />
                  <Point X="-2.960084716797" Y="-1.276474853516" />
                  <Point X="-2.971778808594" Y="-1.248243041992" />
                  <Point X="-2.987861083984" Y="-1.222259643555" />
                  <Point X="-2.996953857422" Y="-1.209918823242" />
                  <Point X="-3.017786132812" Y="-1.185962768555" />
                  <Point X="-3.028745361328" Y="-1.175244628906" />
                  <Point X="-3.052245605469" Y="-1.155710571289" />
                  <Point X="-3.064786132812" Y="-1.146894897461" />
                  <Point X="-3.088779785156" Y="-1.13277331543" />
                  <Point X="-3.105433349609" Y="-1.124481445312" />
                  <Point X="-3.134695556641" Y="-1.11325769043" />
                  <Point X="-3.149793945312" Y="-1.108860473633" />
                  <Point X="-3.181682128906" Y="-1.102378540039" />
                  <Point X="-3.197298339844" Y="-1.100532348633" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-3.397317871094" Y="-1.120337890625" />
                  <Point X="-4.660921386719" Y="-1.286694335938" />
                  <Point X="-4.731211914062" Y="-1.011508483887" />
                  <Point X="-4.740760742188" Y="-0.974126159668" />
                  <Point X="-4.786452148438" Y="-0.65465435791" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.500476074219" Y="-0.309712615967" />
                  <Point X="-3.473930664063" Y="-0.299250915527" />
                  <Point X="-3.444166503906" Y="-0.283892944336" />
                  <Point X="-3.433561279297" Y="-0.27751361084" />
                  <Point X="-3.408416748047" Y="-0.260061859131" />
                  <Point X="-3.393670898438" Y="-0.248241729736" />
                  <Point X="-3.37121484375" Y="-0.226356719971" />
                  <Point X="-3.360896240234" Y="-0.2144815979" />
                  <Point X="-3.341652099609" Y="-0.188225280762" />
                  <Point X="-3.333435058594" Y="-0.17481072998" />
                  <Point X="-3.319327880859" Y="-0.146810913086" />
                  <Point X="-3.3134375" Y="-0.132225509644" />
                  <Point X="-3.305055908203" Y="-0.10522013855" />
                  <Point X="-3.300990478516" Y="-0.088499908447" />
                  <Point X="-3.296720947266" Y="-0.060340179443" />
                  <Point X="-3.295647460938" Y="-0.046099285126" />
                  <Point X="-3.295647460938" Y="-0.016460905075" />
                  <Point X="-3.296720947266" Y="-0.002219857931" />
                  <Point X="-3.300990478516" Y="0.025939723969" />
                  <Point X="-3.304186523438" Y="0.039858406067" />
                  <Point X="-3.312568115234" Y="0.066863929749" />
                  <Point X="-3.319327880859" Y="0.084251029968" />
                  <Point X="-3.333435058594" Y="0.112250839233" />
                  <Point X="-3.341651855469" Y="0.125664794922" />
                  <Point X="-3.360895996094" Y="0.151921417236" />
                  <Point X="-3.371215332031" Y="0.163797424316" />
                  <Point X="-3.393671630859" Y="0.185682144165" />
                  <Point X="-3.405808349609" Y="0.195691146851" />
                  <Point X="-3.430952880859" Y="0.213143051147" />
                  <Point X="-3.436773193359" Y="0.216876312256" />
                  <Point X="-3.454746826172" Y="0.22721005249" />
                  <Point X="-3.472923583984" Y="0.236240982056" />
                  <Point X="-3.481597167969" Y="0.240024032593" />
                  <Point X="-3.508288330078" Y="0.249611251831" />
                  <Point X="-3.647744873047" Y="0.28697845459" />
                  <Point X="-4.785446289062" Y="0.591824584961" />
                  <Point X="-4.737483886719" Y="0.915948669434" />
                  <Point X="-4.731330566406" Y="0.957532043457" />
                  <Point X="-4.633586425781" Y="1.318237304688" />
                  <Point X="-3.778065917969" Y="1.20560559082" />
                  <Point X="-3.767738769531" Y="1.204815429688" />
                  <Point X="-3.74705859375" Y="1.204364135742" />
                  <Point X="-3.736705322266" Y="1.204703125" />
                  <Point X="-3.715143310547" Y="1.20658984375" />
                  <Point X="-3.704889404297" Y="1.208053710938" />
                  <Point X="-3.684603027344" Y="1.212089111328" />
                  <Point X="-3.674570556641" Y="1.214660522461" />
                  <Point X="-3.618917724609" Y="1.232207641602" />
                  <Point X="-3.603451171875" Y="1.237676147461" />
                  <Point X="-3.584517578125" Y="1.246007080078" />
                  <Point X="-3.575277099609" Y="1.250689941406" />
                  <Point X="-3.556533447266" Y="1.261511962891" />
                  <Point X="-3.547859375" Y="1.267171875" />
                  <Point X="-3.531178955078" Y="1.279402587891" />
                  <Point X="-3.515929199219" Y="1.293376708984" />
                  <Point X="-3.502291259766" Y="1.308927612305" />
                  <Point X="-3.495896972656" Y="1.317075073242" />
                  <Point X="-3.483482666016" Y="1.334804199219" />
                  <Point X="-3.478012695312" Y="1.343600219727" />
                  <Point X="-3.468062988281" Y="1.361735961914" />
                  <Point X="-3.463583251953" Y="1.371075317383" />
                  <Point X="-3.441252441406" Y="1.424986816406" />
                  <Point X="-3.435499755859" Y="1.440350463867" />
                  <Point X="-3.429711914062" Y="1.460207397461" />
                  <Point X="-3.427360107422" Y="1.470293579102" />
                  <Point X="-3.423601318359" Y="1.491609008789" />
                  <Point X="-3.422361328125" Y="1.501891723633" />
                  <Point X="-3.421008056641" Y="1.522532470703" />
                  <Point X="-3.421909912109" Y="1.543198364258" />
                  <Point X="-3.425056396484" Y="1.563642822266" />
                  <Point X="-3.427187744141" Y="1.573779296875" />
                  <Point X="-3.432789550781" Y="1.594686157227" />
                  <Point X="-3.436011962891" Y="1.604530761719" />
                  <Point X="-3.443509033203" Y="1.623809326172" />
                  <Point X="-3.447783691406" Y="1.633243530273" />
                  <Point X="-3.474728271484" Y="1.68500378418" />
                  <Point X="-3.482799560547" Y="1.699285766602" />
                  <Point X="-3.494291015625" Y="1.71648425293" />
                  <Point X="-3.500506591797" Y="1.724770385742" />
                  <Point X="-3.514419189453" Y="1.741350952148" />
                  <Point X="-3.5215" Y="1.748910766602" />
                  <Point X="-3.536442382812" Y="1.76321496582" />
                  <Point X="-3.544303710938" Y="1.769958862305" />
                  <Point X="-3.624296386719" Y="1.831339355469" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.026198730469" Y="2.639355957031" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.726338378906" Y="3.035012695312" />
                  <Point X="-3.254156494141" Y="2.762398193359" />
                  <Point X="-3.244916992188" Y="2.757715820312" />
                  <Point X="-3.225984130859" Y="2.749385253906" />
                  <Point X="-3.216290283203" Y="2.745736816406" />
                  <Point X="-3.195647460938" Y="2.739228271484" />
                  <Point X="-3.185614013672" Y="2.736656738281" />
                  <Point X="-3.165327392578" Y="2.732621582031" />
                  <Point X="-3.15507421875" Y="2.731157958984" />
                  <Point X="-3.077565429688" Y="2.724376708984" />
                  <Point X="-3.059171875" Y="2.723334472656" />
                  <Point X="-3.038490234375" Y="2.723785888672" />
                  <Point X="-3.028161621094" Y="2.724576171875" />
                  <Point X="-3.006703125" Y="2.727401611328" />
                  <Point X="-2.996523681641" Y="2.729311279297" />
                  <Point X="-2.976432373047" Y="2.734227783203" />
                  <Point X="-2.956994873047" Y="2.741302734375" />
                  <Point X="-2.938443847656" Y="2.750451416016" />
                  <Point X="-2.929418457031" Y="2.755531982422" />
                  <Point X="-2.911164550781" Y="2.767161132812" />
                  <Point X="-2.902745849609" Y="2.773193847656" />
                  <Point X="-2.886614746094" Y="2.786140380859" />
                  <Point X="-2.87890234375" Y="2.793054199219" />
                  <Point X="-2.823885986328" Y="2.8480703125" />
                  <Point X="-2.811264648438" Y="2.861490234375" />
                  <Point X="-2.798317871094" Y="2.877621582031" />
                  <Point X="-2.79228515625" Y="2.886040283203" />
                  <Point X="-2.780656005859" Y="2.904294189453" />
                  <Point X="-2.775575439453" Y="2.913319580078" />
                  <Point X="-2.766426757812" Y="2.931870605469" />
                  <Point X="-2.759351806641" Y="2.951308105469" />
                  <Point X="-2.754435302734" Y="2.971399414062" />
                  <Point X="-2.752525634766" Y="2.981578857422" />
                  <Point X="-2.749700195312" Y="3.003037353516" />
                  <Point X="-2.748909912109" Y="3.013365966797" />
                  <Point X="-2.748458496094" Y="3.034047607422" />
                  <Point X="-2.748797363281" Y="3.044400634766" />
                  <Point X="-2.755578613281" Y="3.121909423828" />
                  <Point X="-2.757745605469" Y="3.140203369141" />
                  <Point X="-2.761780761719" Y="3.160489990234" />
                  <Point X="-2.764352294922" Y="3.1705234375" />
                  <Point X="-2.770860839844" Y="3.191166259766" />
                  <Point X="-2.774509765625" Y="3.200860839844" />
                  <Point X="-2.782840820312" Y="3.219794433594" />
                  <Point X="-2.787522949219" Y="3.229033447266" />
                  <Point X="-2.822970214844" Y="3.2904296875" />
                  <Point X="-3.059387451172" Y="3.699916259766" />
                  <Point X="-2.690002929688" Y="3.983119628906" />
                  <Point X="-2.648373779297" Y="4.015036376953" />
                  <Point X="-2.192525390625" Y="4.268296386719" />
                  <Point X="-2.118563964844" Y="4.171908203125" />
                  <Point X="-2.111819091797" Y="4.164045898438" />
                  <Point X="-2.097515380859" Y="4.149104492188" />
                  <Point X="-2.08995703125" Y="4.142025390625" />
                  <Point X="-2.073376708984" Y="4.128112792969" />
                  <Point X="-2.065091064453" Y="4.121897460938" />
                  <Point X="-2.047892578125" Y="4.110405761719" />
                  <Point X="-2.038979736328" Y="4.105129394531" />
                  <Point X="-1.952712890625" Y="4.060220947266" />
                  <Point X="-1.934330200195" Y="4.051287841797" />
                  <Point X="-1.915052246094" Y="4.043790771484" />
                  <Point X="-1.905208129883" Y="4.040568359375" />
                  <Point X="-1.884301879883" Y="4.034966308594" />
                  <Point X="-1.874166259766" Y="4.032834960938" />
                  <Point X="-1.853722045898" Y="4.029687988281" />
                  <Point X="-1.833054931641" Y="4.028785400391" />
                  <Point X="-1.81241418457" Y="4.030138427734" />
                  <Point X="-1.802131835938" Y="4.031378173828" />
                  <Point X="-1.780817016602" Y="4.035136474609" />
                  <Point X="-1.770730712891" Y="4.03748828125" />
                  <Point X="-1.750871704102" Y="4.043276367188" />
                  <Point X="-1.741098999023" Y="4.046713134766" />
                  <Point X="-1.651245849609" Y="4.083931884766" />
                  <Point X="-1.632584350586" Y="4.092272949219" />
                  <Point X="-1.614450195312" Y="4.102222167969" />
                  <Point X="-1.60565625" Y="4.107690917969" />
                  <Point X="-1.587926513672" Y="4.12010546875" />
                  <Point X="-1.579778198242" Y="4.1265" />
                  <Point X="-1.564226806641" Y="4.140138183594" />
                  <Point X="-1.550252807617" Y="4.155388183594" />
                  <Point X="-1.538021850586" Y="4.172068847656" />
                  <Point X="-1.532361694336" Y="4.180743164062" />
                  <Point X="-1.521539550781" Y="4.199487304687" />
                  <Point X="-1.516857299805" Y="4.208726074219" />
                  <Point X="-1.508526123047" Y="4.227659667969" />
                  <Point X="-1.504877197266" Y="4.237354492188" />
                  <Point X="-1.475631835938" Y="4.330108886719" />
                  <Point X="-1.470026489258" Y="4.349764160156" />
                  <Point X="-1.465991210938" Y="4.37005078125" />
                  <Point X="-1.46452734375" Y="4.380304199219" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.412217773438" />
                  <Point X="-1.462752807617" Y="4.4328984375" />
                  <Point X="-1.46354284668" Y="4.443227539062" />
                  <Point X="-1.467572998047" Y="4.473837890625" />
                  <Point X="-1.479265991211" Y="4.562654785156" />
                  <Point X="-0.985066162109" Y="4.701211425781" />
                  <Point X="-0.931174682617" Y="4.716320800781" />
                  <Point X="-0.365221984863" Y="4.782557128906" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.22043510437" Y="4.247107421875" />
                  <Point X="-0.207661560059" Y="4.218916015625" />
                  <Point X="-0.200119094849" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166456054688" />
                  <Point X="-0.151451339722" Y="4.1438671875" />
                  <Point X="-0.126897789001" Y="4.125026367188" />
                  <Point X="-0.099602752686" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918533325" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.067230316162" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914390564" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163762969971" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.1945730896" Y="4.178618164062" />
                  <Point X="0.21243132019" Y="4.205344726562" />
                  <Point X="0.219973648071" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978118896" Y="4.261727539062" />
                  <Point X="0.256140869141" Y="4.32951171875" />
                  <Point X="0.378190246582" Y="4.785006347656" />
                  <Point X="0.780809143066" Y="4.742841308594" />
                  <Point X="0.827875976562" Y="4.737912109375" />
                  <Point X="1.405083496094" Y="4.598556640625" />
                  <Point X="1.453596069336" Y="4.58684375" />
                  <Point X="1.828193725586" Y="4.450974609375" />
                  <Point X="1.858260375977" Y="4.440069335938" />
                  <Point X="2.221549560547" Y="4.270171386719" />
                  <Point X="2.250445068359" Y="4.256657226562" />
                  <Point X="2.601455566406" Y="4.052158447266" />
                  <Point X="2.629435791016" Y="4.035856689453" />
                  <Point X="2.817780029297" Y="3.901916748047" />
                  <Point X="2.065308837891" Y="2.598598144531" />
                  <Point X="2.062372558594" Y="2.593104248047" />
                  <Point X="2.053181152344" Y="2.573438232422" />
                  <Point X="2.044182373047" Y="2.549564453125" />
                  <Point X="2.041301757812" Y="2.540598876953" />
                  <Point X="2.021850219727" Y="2.467858642578" />
                  <Point X="2.016912231445" Y="2.442694091797" />
                  <Point X="2.013556030273" Y="2.41624609375" />
                  <Point X="2.01280090332" Y="2.404581787109" />
                  <Point X="2.012728271484" Y="2.381249267578" />
                  <Point X="2.013410888672" Y="2.369581054688" />
                  <Point X="2.020995727539" Y="2.306681396484" />
                  <Point X="2.023800170898" Y="2.289036865234" />
                  <Point X="2.029142333984" Y="2.267114501953" />
                  <Point X="2.032467285156" Y="2.256311523438" />
                  <Point X="2.040733764648" Y="2.234220947266" />
                  <Point X="2.045318115234" Y="2.223889160156" />
                  <Point X="2.055681152344" Y="2.203843505859" />
                  <Point X="2.061459716797" Y="2.194129638672" />
                  <Point X="2.100379882813" Y="2.136771240234" />
                  <Point X="2.116423828125" Y="2.116297607422" />
                  <Point X="2.13376171875" Y="2.097047607422" />
                  <Point X="2.141931640625" Y="2.089009765625" />
                  <Point X="2.159179931641" Y="2.073976318359" />
                  <Point X="2.168258300781" Y="2.066980712891" />
                  <Point X="2.225616699219" Y="2.028060668945" />
                  <Point X="2.241280761719" Y="2.018244628906" />
                  <Point X="2.261325927734" Y="2.007881835938" />
                  <Point X="2.271657226562" Y="2.003297607422" />
                  <Point X="2.293744873047" Y="1.995032104492" />
                  <Point X="2.304547851562" Y="1.99170715332" />
                  <Point X="2.326470703125" Y="1.986364868164" />
                  <Point X="2.337590576172" Y="1.984346801758" />
                  <Point X="2.400490234375" Y="1.976762084961" />
                  <Point X="2.407192382812" Y="1.976193603516" />
                  <Point X="2.427351074219" Y="1.975439941406" />
                  <Point X="2.452778808594" Y="1.975990844727" />
                  <Point X="2.464117675781" Y="1.976917724609" />
                  <Point X="2.486602294922" Y="1.980120483398" />
                  <Point X="2.497748046875" Y="1.982395996094" />
                  <Point X="2.570488525391" Y="2.001847900391" />
                  <Point X="2.585379638672" Y="2.006492553711" />
                  <Point X="2.605009033203" Y="2.01351184082" />
                  <Point X="2.612927490234" Y="2.016752563477" />
                  <Point X="2.636033935547" Y="2.027872680664" />
                  <Point X="2.776300292969" Y="2.10885546875" />
                  <Point X="3.940405029297" Y="2.780951416016" />
                  <Point X="4.027355712891" Y="2.660109863281" />
                  <Point X="4.043952880859" Y="2.637043457031" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.172948730469" Y="1.743817749023" />
                  <Point X="3.168137451172" Y="1.739868530273" />
                  <Point X="3.152119384766" Y="1.725216674805" />
                  <Point X="3.134668945312" Y="1.706603515625" />
                  <Point X="3.128576660156" Y="1.699422607422" />
                  <Point X="3.076225341797" Y="1.631126220703" />
                  <Point X="3.062543701172" Y="1.610655395508" />
                  <Point X="3.048642822266" Y="1.586618164062" />
                  <Point X="3.043325195312" Y="1.575923339844" />
                  <Point X="3.034073730469" Y="1.553950317383" />
                  <Point X="3.030139892578" Y="1.542672119141" />
                  <Point X="3.010638916016" Y="1.472941162109" />
                  <Point X="3.006224853516" Y="1.45466003418" />
                  <Point X="3.002771484375" Y="1.432360595703" />
                  <Point X="3.001709228516" Y="1.421108520508" />
                  <Point X="3.000893310547" Y="1.397538818359" />
                  <Point X="3.001174804688" Y="1.386239868164" />
                  <Point X="3.003077880859" Y="1.363755859375" />
                  <Point X="3.004699462891" Y="1.352570922852" />
                  <Point X="3.020707763672" Y="1.274986450195" />
                  <Point X="3.027365966797" Y="1.250868774414" />
                  <Point X="3.036224609375" Y="1.225442749023" />
                  <Point X="3.040750732422" Y="1.21464453125" />
                  <Point X="3.05109765625" Y="1.193685913086" />
                  <Point X="3.056918457031" Y="1.183525634766" />
                  <Point X="3.100459228516" Y="1.117345581055" />
                  <Point X="3.111739746094" Y="1.101423339844" />
                  <Point X="3.126294189453" Y="1.084177856445" />
                  <Point X="3.134084960938" Y="1.075989257813" />
                  <Point X="3.151329589844" Y="1.059899169922" />
                  <Point X="3.160036621094" Y="1.052694335938" />
                  <Point X="3.178245361328" Y="1.039369384766" />
                  <Point X="3.187747070312" Y="1.033249267578" />
                  <Point X="3.250843505859" Y="0.997731384277" />
                  <Point X="3.274266357422" Y="0.986578552246" />
                  <Point X="3.298786376953" Y="0.976896789551" />
                  <Point X="3.309829101562" Y="0.973299865723" />
                  <Point X="3.332271484375" Y="0.967480224609" />
                  <Point X="3.343670654297" Y="0.965257568359" />
                  <Point X="3.428981445312" Y="0.95398260498" />
                  <Point X="3.443727539062" Y="0.952615844727" />
                  <Point X="3.465655273438" Y="0.951442932129" />
                  <Point X="3.474408935547" Y="0.951378540039" />
                  <Point X="3.500603515625" Y="0.952797180176" />
                  <Point X="3.633847167969" Y="0.970339111328" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.745555664063" Y="0.943511352539" />
                  <Point X="4.75268359375" Y="0.914232666016" />
                  <Point X="4.78387109375" Y="0.713920959473" />
                  <Point X="3.691991943359" Y="0.421352874756" />
                  <Point X="3.68603125" Y="0.419544281006" />
                  <Point X="3.665625488281" Y="0.412137359619" />
                  <Point X="3.642381103516" Y="0.401619171143" />
                  <Point X="3.634004638672" Y="0.397316558838" />
                  <Point X="3.550189453125" Y="0.348869934082" />
                  <Point X="3.53046875" Y="0.335767059326" />
                  <Point X="3.507461669922" Y="0.318320648193" />
                  <Point X="3.498247802734" Y="0.310353881836" />
                  <Point X="3.480915283203" Y="0.29330682373" />
                  <Point X="3.472796386719" Y="0.284226257324" />
                  <Point X="3.422507324219" Y="0.220146011353" />
                  <Point X="3.410853515625" Y="0.204207092285" />
                  <Point X="3.399129394531" Y="0.184926544189" />
                  <Point X="3.393842285156" Y="0.174937606812" />
                  <Point X="3.384068847656" Y="0.153474517822" />
                  <Point X="3.380005371094" Y="0.142928833008" />
                  <Point X="3.373159179688" Y="0.12142829895" />
                  <Point X="3.370376464844" Y="0.110473457336" />
                  <Point X="3.35361328125" Y="0.022943187714" />
                  <Point X="3.350590332031" Y="-0.000970831215" />
                  <Point X="3.348851318359" Y="-0.028860958099" />
                  <Point X="3.348858642578" Y="-0.040798797607" />
                  <Point X="3.350370117188" Y="-0.064579795837" />
                  <Point X="3.351874267578" Y="-0.076422966003" />
                  <Point X="3.368637451172" Y="-0.163953231812" />
                  <Point X="3.373159179688" Y="-0.183988494873" />
                  <Point X="3.380005371094" Y="-0.205489028931" />
                  <Point X="3.384068847656" Y="-0.216034561157" />
                  <Point X="3.393842285156" Y="-0.237497787476" />
                  <Point X="3.399129882813" Y="-0.247487472534" />
                  <Point X="3.410854248047" Y="-0.266768035889" />
                  <Point X="3.417291015625" Y="-0.276058898926" />
                  <Point X="3.467580078125" Y="-0.340139007568" />
                  <Point X="3.484311035156" Y="-0.358783294678" />
                  <Point X="3.503840087891" Y="-0.377851593018" />
                  <Point X="3.512808349609" Y="-0.385577484131" />
                  <Point X="3.531635742188" Y="-0.399853881836" />
                  <Point X="3.541494628906" Y="-0.406404144287" />
                  <Point X="3.625309814453" Y="-0.454850891113" />
                  <Point X="3.637764404297" Y="-0.46144619751" />
                  <Point X="3.658540039063" Y="-0.47147958374" />
                  <Point X="3.666756347656" Y="-0.474981292725" />
                  <Point X="3.691992431641" Y="-0.483913085938" />
                  <Point X="3.814182861328" Y="-0.516653808594" />
                  <Point X="4.784876953125" Y="-0.776750610352" />
                  <Point X="4.76559375" Y="-0.904653076172" />
                  <Point X="4.761613769531" Y="-0.931050537109" />
                  <Point X="4.727801269531" Y="-1.079219726563" />
                  <Point X="3.436781982422" Y="-0.909253662109" />
                  <Point X="3.428624267578" Y="-0.908535827637" />
                  <Point X="3.400097412109" Y="-0.908042541504" />
                  <Point X="3.366720703125" Y="-0.910841125488" />
                  <Point X="3.354480957031" Y="-0.912676330566" />
                  <Point X="3.189981445312" Y="-0.948430847168" />
                  <Point X="3.157873535156" Y="-0.956742736816" />
                  <Point X="3.128753173828" Y="-0.968367431641" />
                  <Point X="3.114676025391" Y="-0.975389221191" />
                  <Point X="3.086849853516" Y="-0.992281433105" />
                  <Point X="3.074124267578" Y="-1.001530395508" />
                  <Point X="3.050374267578" Y="-1.022001098633" />
                  <Point X="3.039349609375" Y="-1.033223022461" />
                  <Point X="2.939920166016" Y="-1.152806030273" />
                  <Point X="2.921326416016" Y="-1.176847045898" />
                  <Point X="2.906605712891" Y="-1.201229370117" />
                  <Point X="2.900164306641" Y="-1.213975463867" />
                  <Point X="2.888820800781" Y="-1.241361206055" />
                  <Point X="2.884362792969" Y="-1.254928466797" />
                  <Point X="2.877531005859" Y="-1.28257800293" />
                  <Point X="2.875157226562" Y="-1.296660522461" />
                  <Point X="2.860906494141" Y="-1.451525146484" />
                  <Point X="2.859288818359" Y="-1.483321655273" />
                  <Point X="2.861607666016" Y="-1.514590332031" />
                  <Point X="2.864065917969" Y="-1.530128051758" />
                  <Point X="2.871797363281" Y="-1.561748535156" />
                  <Point X="2.876786132812" Y="-1.57666796875" />
                  <Point X="2.889157470703" Y="-1.605479248047" />
                  <Point X="2.896540039062" Y="-1.61937109375" />
                  <Point X="2.987576416016" Y="-1.760972290039" />
                  <Point X="2.996931884766" Y="-1.774241699219" />
                  <Point X="3.017995849609" Y="-1.801559448242" />
                  <Point X="3.026132324219" Y="-1.810804199219" />
                  <Point X="3.043532470703" Y="-1.828163574219" />
                  <Point X="3.052796142578" Y="-1.836277832031" />
                  <Point X="3.166189697266" Y="-1.923287719727" />
                  <Point X="4.087170654297" Y="-2.629981689453" />
                  <Point X="4.056707763672" Y="-2.679275146484" />
                  <Point X="4.045485595703" Y="-2.697434082031" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="2.848454345703" Y="-2.094670654297" />
                  <Point X="2.841192871094" Y="-2.090885742188" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.575327880859" Y="-2.030979858398" />
                  <Point X="2.555018066406" Y="-2.027312011719" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503662109" />
                  <Point X="2.460140869141" Y="-2.031461425781" />
                  <Point X="2.444844726562" Y="-2.035136230469" />
                  <Point X="2.415068847656" Y="-2.044959838867" />
                  <Point X="2.400589111328" Y="-2.051108642578" />
                  <Point X="2.237943603516" Y="-2.136707763672" />
                  <Point X="2.208968505859" Y="-2.153170410156" />
                  <Point X="2.186037841797" Y="-2.170063476562" />
                  <Point X="2.175209716797" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333740234" />
                  <Point X="2.144939453125" Y="-2.211161865234" />
                  <Point X="2.128046386719" Y="-2.234092529297" />
                  <Point X="2.120463623047" Y="-2.246195068359" />
                  <Point X="2.034864624023" Y="-2.408840576172" />
                  <Point X="2.0198359375" Y="-2.440192871094" />
                  <Point X="2.010012207031" Y="-2.46996875" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.037545532227" Y="-2.775922607422" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.142413330078" Y="-2.999787597656" />
                  <Point X="2.735893554688" Y="-4.027724853516" />
                  <Point X="2.723754394531" Y="-4.036084228516" />
                  <Point X="1.833914550781" Y="-2.876422607422" />
                  <Point X="1.828653808594" Y="-2.870146484375" />
                  <Point X="1.80883190918" Y="-2.849626953125" />
                  <Point X="1.783252197266" Y="-2.828004638672" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.580206542969" Y="-2.696506591797" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283813477" Y="-2.676245849609" />
                  <Point X="1.51747265625" Y="-2.663874511719" />
                  <Point X="1.502553222656" Y="-2.658885742188" />
                  <Point X="1.470931518555" Y="-2.651154052734" />
                  <Point X="1.455393798828" Y="-2.648695800781" />
                  <Point X="1.42412512207" Y="-2.646376953125" />
                  <Point X="1.408394165039" Y="-2.646516357422" />
                  <Point X="1.197214599609" Y="-2.66594921875" />
                  <Point X="1.175307250977" Y="-2.667965332031" />
                  <Point X="1.161225341797" Y="-2.670339111328" />
                  <Point X="1.133575805664" Y="-2.677170654297" />
                  <Point X="1.120008544922" Y="-2.681628417969" />
                  <Point X="1.092623657227" Y="-2.692971435547" />
                  <Point X="1.079877319336" Y="-2.699413085938" />
                  <Point X="1.055494995117" Y="-2.714133789062" />
                  <Point X="1.043858764648" Y="-2.722412841797" />
                  <Point X="0.880791625977" Y="-2.857998046875" />
                  <Point X="0.863875244141" Y="-2.872063232422" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182556152" Y="-2.906837890625" />
                  <Point X="0.822933776855" Y="-2.919563476563" />
                  <Point X="0.806041137695" Y="-2.947390380859" />
                  <Point X="0.799019165039" Y="-2.961467529297" />
                  <Point X="0.787394470215" Y="-2.990588134766" />
                  <Point X="0.782791748047" Y="-3.005631347656" />
                  <Point X="0.734035461426" Y="-3.229948730469" />
                  <Point X="0.728977478027" Y="-3.253219238281" />
                  <Point X="0.727584594727" Y="-3.261289794922" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.724742370605" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.746204833984" Y="-3.492371337891" />
                  <Point X="0.83309161377" Y="-4.152341308594" />
                  <Point X="0.655065124512" Y="-3.487937011719" />
                  <Point X="0.652606262207" Y="-3.480124755859" />
                  <Point X="0.642145629883" Y="-3.453581298828" />
                  <Point X="0.626787231445" Y="-3.423815917969" />
                  <Point X="0.620407592773" Y="-3.413209960938" />
                  <Point X="0.472068267822" Y="-3.199481201172" />
                  <Point X="0.4566796875" Y="-3.177309570312" />
                  <Point X="0.446670684814" Y="-3.165172851562" />
                  <Point X="0.424786560059" Y="-3.142717285156" />
                  <Point X="0.412911743164" Y="-3.132398681641" />
                  <Point X="0.386656463623" Y="-3.113154785156" />
                  <Point X="0.373242034912" Y="-3.1049375" />
                  <Point X="0.34524105835" Y="-3.090829345703" />
                  <Point X="0.330654449463" Y="-3.084938232422" />
                  <Point X="0.101108016968" Y="-3.0136953125" />
                  <Point X="0.07729535675" Y="-3.006304931641" />
                  <Point X="0.063375785828" Y="-3.003108886719" />
                  <Point X="0.035215461731" Y="-2.998839599609" />
                  <Point X="0.020974712372" Y="-2.997766357422" />
                  <Point X="-0.008666342735" Y="-2.997766601562" />
                  <Point X="-0.022906200409" Y="-2.998840087891" />
                  <Point X="-0.051064888" Y="-3.003109375" />
                  <Point X="-0.064983573914" Y="-3.006305175781" />
                  <Point X="-0.294529998779" Y="-3.077547851562" />
                  <Point X="-0.318342681885" Y="-3.084938476562" />
                  <Point X="-0.33292956543" Y="-3.090829589844" />
                  <Point X="-0.360930847168" Y="-3.104937988281" />
                  <Point X="-0.374344970703" Y="-3.113155273438" />
                  <Point X="-0.400600402832" Y="-3.132399169922" />
                  <Point X="-0.412475219727" Y="-3.142717773438" />
                  <Point X="-0.434359039307" Y="-3.165173095703" />
                  <Point X="-0.444368041992" Y="-3.177309814453" />
                  <Point X="-0.592707214355" Y="-3.391038330078" />
                  <Point X="-0.60809564209" Y="-3.413210205078" />
                  <Point X="-0.612470947266" Y="-3.420133300781" />
                  <Point X="-0.62597644043" Y="-3.445264648438" />
                  <Point X="-0.638777648926" Y="-3.476215332031" />
                  <Point X="-0.642753173828" Y="-3.487936767578" />
                  <Point X="-0.680402893066" Y="-3.628447998047" />
                  <Point X="-0.985425170898" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.764860529647" Y="0.708827095077" />
                  <Point X="4.607417562227" Y="1.098512113901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.068925913749" Y="2.431325713724" />
                  <Point X="3.930077100623" Y="2.774988585706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.672408450785" Y="0.684054639598" />
                  <Point X="4.510131511174" Y="1.085704159442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.990712783052" Y="2.371310624834" />
                  <Point X="3.846996143646" Y="2.727021789646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.579956371922" Y="0.65928218412" />
                  <Point X="4.41284546012" Y="1.072896204983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.912499652355" Y="2.311295535943" />
                  <Point X="3.763915186668" Y="2.679054993587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.487504293059" Y="0.634509728641" />
                  <Point X="4.315559409066" Y="1.060088250525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.834286521659" Y="2.251280447053" />
                  <Point X="3.680834229691" Y="2.631088197528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.395052214197" Y="0.609737273163" />
                  <Point X="4.218273358012" Y="1.047280296066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.756073390962" Y="2.191265358163" />
                  <Point X="3.597753272714" Y="2.583121401468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.302600135334" Y="0.584964817684" />
                  <Point X="4.120987306959" Y="1.034472341607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.677860260265" Y="2.131250269273" />
                  <Point X="3.514672315737" Y="2.535154605409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.775482835931" Y="-0.839060318214" />
                  <Point X="4.746111374754" Y="-0.766363400789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.210148056471" Y="0.560192362206" />
                  <Point X="4.023701255905" Y="1.021664387149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.599647129568" Y="2.071235180382" />
                  <Point X="3.43159135876" Y="2.487187809349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.743051882346" Y="-1.012390271794" />
                  <Point X="4.631211703017" Y="-0.735576114254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.117695977608" Y="0.535419906727" />
                  <Point X="3.926415204851" Y="1.00885643269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.521433998871" Y="2.011220091492" />
                  <Point X="3.348510401783" Y="2.43922101329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.78234216953" Y="3.840536561761" />
                  <Point X="2.733258018493" Y="3.962024098704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.664209412797" Y="-1.070847692366" />
                  <Point X="4.51631203128" Y="-0.704788827719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.025243898746" Y="0.510647451249" />
                  <Point X="3.829129153797" Y="0.996048478231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.443220868174" Y="1.951205002602" />
                  <Point X="3.265429444806" Y="2.39125421723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.722063809432" Y="3.736131357944" />
                  <Point X="2.592202548754" Y="4.057549257014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.555992430973" Y="-1.056600643781" />
                  <Point X="4.401412359544" Y="-0.674001541185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.932791819883" Y="0.48587499577" />
                  <Point X="3.731843102744" Y="0.983240523773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.365007737477" Y="1.891189913711" />
                  <Point X="3.182348487829" Y="2.343287421171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.661785449333" Y="3.631726154126" />
                  <Point X="2.458199290509" Y="4.13561957937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.44777544915" Y="-1.042353595196" />
                  <Point X="4.286512687807" Y="-0.64321425465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.84033974102" Y="0.461102540292" />
                  <Point X="3.63455705169" Y="0.970432569314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.286794606781" Y="1.831174824821" />
                  <Point X="3.099267530852" Y="2.295320625112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.601507089235" Y="3.527320950309" />
                  <Point X="2.324196032263" Y="4.213689901727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.339558467327" Y="-1.028106546611" />
                  <Point X="4.17161301607" Y="-0.612426968115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.747887662157" Y="0.436330084813" />
                  <Point X="3.537271019438" Y="0.957624568319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.208581476084" Y="1.771159735931" />
                  <Point X="3.016186573875" Y="2.247353829052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.541228729136" Y="3.422915746491" />
                  <Point X="2.193642144541" Y="4.283222732448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.231341485503" Y="-1.013859498026" />
                  <Point X="4.056713344333" Y="-0.58163968158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.656812567649" Y="0.408149473462" />
                  <Point X="3.436565679556" Y="0.953279650687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.132998556235" Y="1.704634646749" />
                  <Point X="2.933105616898" Y="2.199387032993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.480950369038" Y="3.318510542673" />
                  <Point X="2.067311204805" Y="4.342303400124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.12312450368" Y="-0.999612449441" />
                  <Point X="3.941813672597" Y="-0.550852395046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.572979906758" Y="0.362043209879" />
                  <Point X="3.327910583264" Y="0.968611070635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.066260798566" Y="1.616217012939" />
                  <Point X="2.850024659921" Y="2.151420236933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.42067200894" Y="3.214105338856" />
                  <Point X="1.94098026507" Y="4.4013840678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.014907521856" Y="-0.985365400856" />
                  <Point X="3.82691400086" Y="-0.520065108511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.493349427615" Y="0.305536181493" />
                  <Point X="3.202745345542" Y="1.024806524582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.015158188721" Y="1.489101030299" />
                  <Point X="2.766943702884" Y="2.103453441022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.360393648841" Y="3.109700135038" />
                  <Point X="1.816816302488" Y="4.455101278811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.906690540033" Y="-0.971118352271" />
                  <Point X="3.712014373585" Y="-0.489277932022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.424409306129" Y="0.222569589414" />
                  <Point X="2.683862745375" Y="2.055486646279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.300115288743" Y="3.005294931221" />
                  <Point X="1.696762438344" Y="4.498645639214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.798473558209" Y="-0.956871303686" />
                  <Point X="3.58660529674" Y="-0.432478955068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.369367290941" Y="0.105203977149" />
                  <Point X="2.599200116066" Y="2.011434626614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.239836928644" Y="2.900889727403" />
                  <Point X="1.576708574199" Y="4.542189999618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.690256576386" Y="-0.942624255101" />
                  <Point X="3.424825765588" Y="-0.285659944804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.362315112508" Y="-0.130940649167" />
                  <Point X="2.507426046371" Y="1.984984039558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.179558568546" Y="2.796484523585" />
                  <Point X="1.456654710055" Y="4.585734360021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.582039594563" Y="-0.928377206516" />
                  <Point X="2.408537124993" Y="1.976143328366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.119280208448" Y="2.692079319768" />
                  <Point X="1.342952889577" Y="4.613556860653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.473822612739" Y="-0.914130157932" />
                  <Point X="2.299112198983" Y="1.993380143726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.059387977785" Y="2.586718412059" />
                  <Point X="1.229417352251" Y="4.640967796042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.073529236455" Y="-2.652055518641" />
                  <Point X="4.054474290246" Y="-2.604892871785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.36992442449" Y="-0.910572498545" />
                  <Point X="2.166307481086" Y="2.068483974624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.016483094149" Y="2.439312345053" />
                  <Point X="1.115881814924" Y="4.668378731431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.009856158584" Y="-2.748058501127" />
                  <Point X="3.905975983042" Y="-2.490946044313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.275269680033" Y="-0.92989316537" />
                  <Point X="1.002346277598" Y="4.69578966682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.885259683863" Y="-2.693270785006" />
                  <Point X="3.757477675839" Y="-2.376999216842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.181215440566" Y="-0.950700134199" />
                  <Point X="0.888810740272" Y="4.723200602209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.751627113421" Y="-2.61611794716" />
                  <Point X="3.608979368635" Y="-2.26305238937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.093840087706" Y="-0.988037927466" />
                  <Point X="0.778308782946" Y="4.743103163621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.617994542979" Y="-2.538965109313" />
                  <Point X="3.460481061431" Y="-2.149105561899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019352317676" Y="-1.057273607566" />
                  <Point X="0.67132107301" Y="4.754307657519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.484361972537" Y="-2.461812271466" />
                  <Point X="3.311982754228" Y="-2.035158734427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.950397825733" Y="-1.140204631517" />
                  <Point X="0.564333363074" Y="4.765512151416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.350729402095" Y="-2.38465943362" />
                  <Point X="3.163484448415" Y="-1.921211910397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.888814581448" Y="-1.241380133638" />
                  <Point X="0.457345653137" Y="4.776716645314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.217096831653" Y="-2.307506595773" />
                  <Point X="3.00683050039" Y="-1.787079163547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.86282675704" Y="-1.43065739154" />
                  <Point X="0.367561746447" Y="4.74534023197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.083464261211" Y="-2.230353757926" />
                  <Point X="0.326705663714" Y="4.59286320478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.949831690769" Y="-2.153200920079" />
                  <Point X="0.285849580982" Y="4.44038617759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.818147157583" Y="-2.080869643635" />
                  <Point X="0.244993483464" Y="4.287909186996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.704990649952" Y="-2.054396839664" />
                  <Point X="0.18934870247" Y="4.172035472452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.594465160482" Y="-2.03443603415" />
                  <Point X="0.111796194789" Y="4.110385284221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.489005432419" Y="-2.0270134281" />
                  <Point X="0.019545946214" Y="4.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.397035381351" Y="-2.052978944235" />
                  <Point X="-0.091775111894" Y="4.107043188241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.312541105454" Y="-2.097447653217" />
                  <Point X="-0.46255867105" Y="4.771165320526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.228183001481" Y="-2.142253399537" />
                  <Point X="-0.560393335245" Y="4.759715231243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.150806257632" Y="-2.204338618519" />
                  <Point X="-0.658227999441" Y="4.748265141959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.089232341072" Y="-2.305537207571" />
                  <Point X="-0.756062663637" Y="4.736815052675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.643924540796" Y="-3.932047959243" />
                  <Point X="2.558305656725" Y="-3.720133784873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.031415747192" Y="-2.416035496592" />
                  <Point X="-0.853897327832" Y="4.725364963391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.427517289577" Y="-3.650020597209" />
                  <Point X="2.217004571675" Y="-3.128983336653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.007789080607" Y="-2.61115682518" />
                  <Point X="-0.950513442047" Y="4.710898857071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.211110038358" Y="-3.367993235176" />
                  <Point X="-1.042548905001" Y="4.685095241032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.994702787139" Y="-3.085965873143" />
                  <Point X="-1.134584381225" Y="4.659291657839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.790491118767" Y="-2.834123637882" />
                  <Point X="-1.226619857449" Y="4.633488074647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.650754308126" Y="-2.741862275369" />
                  <Point X="-1.318655333673" Y="4.607684491454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.516676900293" Y="-2.663608426344" />
                  <Point X="-1.410690809897" Y="4.581880908262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.407349302298" Y="-2.646612506274" />
                  <Point X="-1.466641791948" Y="4.466765067931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.308561312605" Y="-2.655703032152" />
                  <Point X="-1.492401345728" Y="4.276922820397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.209773322912" Y="-2.664793558031" />
                  <Point X="-1.547355449749" Y="4.159339620358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.114959249715" Y="-2.683719872384" />
                  <Point X="-1.624509487606" Y="4.096703184703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.032086231041" Y="-2.732201333804" />
                  <Point X="-1.711694054078" Y="4.058893178557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.955390290443" Y="-2.795971599961" />
                  <Point X="-1.802995975779" Y="4.031273984208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.878694340293" Y="-2.859741842479" />
                  <Point X="-1.909822181601" Y="4.042078741395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.809406230009" Y="-2.941847132059" />
                  <Point X="-2.037431455283" Y="4.104323396617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.765275610884" Y="-3.086219397272" />
                  <Point X="-2.203645522784" Y="4.2621182695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.729435585374" Y="-3.251111601749" />
                  <Point X="-2.28732328626" Y="4.215628621359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.756720078642" Y="-3.57224247278" />
                  <Point X="-2.371001049735" Y="4.169138973218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.806244825083" Y="-3.948419902058" />
                  <Point X="0.723722502289" Y="-3.744169985797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.547654693954" Y="-3.308386868079" />
                  <Point X="-2.454678813211" Y="4.122649325077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.36037589672" Y="-3.098454959564" />
                  <Point X="-2.538356576686" Y="4.076159676936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.241242110793" Y="-3.05718787266" />
                  <Point X="-2.622034340162" Y="4.029670028795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.124091148389" Y="-3.020828446192" />
                  <Point X="-2.701978096868" Y="3.973938389589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.012312687975" Y="-2.997766428767" />
                  <Point X="-2.780206612408" Y="3.913961379522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.084278724826" Y="-3.012293673234" />
                  <Point X="-2.858435127948" Y="3.853984369455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.175323048063" Y="-3.040550446154" />
                  <Point X="-2.936663643488" Y="3.794007359387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.2663673713" Y="-3.068807219075" />
                  <Point X="-2.752261656269" Y="3.083997044633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.865259956616" Y="3.363677652282" />
                  <Point X="-3.014892159028" Y="3.73403034932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.355363688503" Y="-3.102132984805" />
                  <Point X="-2.781550508476" Y="2.902890117241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.432942073225" Y="-3.163719125116" />
                  <Point X="-2.850934435772" Y="2.821021983086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.49843699508" Y="-3.255212885508" />
                  <Point X="-2.927443821689" Y="2.756789977891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.563198507088" Y="-3.348521898971" />
                  <Point X="-3.017458771263" Y="2.725985415749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.626278061" Y="-3.445993904808" />
                  <Point X="-3.120797802369" Y="2.728159112642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.670282572793" Y="-3.590678296621" />
                  <Point X="-3.233099435667" Y="2.752516028393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.711138639129" Y="-3.743155364392" />
                  <Point X="-3.365533139969" Y="2.826701568418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.751994729927" Y="-3.89563237162" />
                  <Point X="-3.49916571515" Y="2.903854417993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.792850820725" Y="-4.048109378847" />
                  <Point X="-3.632798290331" Y="2.981007267569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.833706911522" Y="-4.200586386074" />
                  <Point X="-3.746571349413" Y="3.009006089933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.87456300232" Y="-4.353063393302" />
                  <Point X="-3.814010018882" Y="2.922323273706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.915419093118" Y="-4.505540400529" />
                  <Point X="-3.881448688352" Y="2.835640457478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.956275183915" Y="-4.658017407756" />
                  <Point X="-3.429158092495" Y="1.462582569308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557409112646" Y="1.78001498322" />
                  <Point X="-3.948887357821" Y="2.748957641251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.017280481891" Y="-4.76062337719" />
                  <Point X="-1.12324437297" Y="-4.498353543443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.3220790878" Y="-4.006220354765" />
                  <Point X="-3.481367377085" Y="1.338205702778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.705907419483" Y="1.893961809785" />
                  <Point X="-4.014892404792" Y="2.658726484827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.130455403716" Y="-4.734104996488" />
                  <Point X="-1.13886995854" Y="-4.713278242466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.48076637272" Y="-3.867054922498" />
                  <Point X="-3.553612062482" Y="1.263418193391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.854405799792" Y="2.007908818198" />
                  <Point X="-4.075441441697" Y="2.554991229614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.609853211458" Y="-3.801153165432" />
                  <Point X="-3.640689518864" Y="1.225343080469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.0029041801" Y="2.12185582661" />
                  <Point X="-4.135990489379" Y="2.451256001076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.716435978661" Y="-3.790950939969" />
                  <Point X="-3.734875911444" Y="1.204863202072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.151402560408" Y="2.235802835022" />
                  <Point X="-4.196539537061" Y="2.347520772539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.821683897435" Y="-3.784052580305" />
                  <Point X="-2.313443075919" Y="-2.566905902592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.352631728947" Y="-2.46991058268" />
                  <Point X="-3.306347427448" Y="-0.109381395424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.438584951793" Y="0.217917962612" />
                  <Point X="-3.840983308296" Y="1.213888844629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.926931790999" Y="-3.777154283038" />
                  <Point X="-2.347899334601" Y="-2.735223050153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.500247254825" Y="-2.358148715662" />
                  <Point X="-3.364615594579" Y="-0.218762001426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.559381985221" Y="0.263301731537" />
                  <Point X="-3.949200301039" Y="1.228135920241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.025556782082" Y="-3.786648244633" />
                  <Point X="-2.408177706878" Y="-2.839628223827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.60502981303" Y="-2.352402163824" />
                  <Point X="-2.966667696485" Y="-1.457316992787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.100061315228" Y="-1.127156200707" />
                  <Point X="-3.441427463017" Y="-0.282245336111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.674281632165" Y="0.294088956709" />
                  <Point X="-4.057417293782" Y="1.242382995852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.111813645042" Y="-3.826754397546" />
                  <Point X="-2.468456079156" Y="-2.9440333975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.694866086019" Y="-2.383648966031" />
                  <Point X="-3.029380759373" Y="-1.555696095738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.213506928088" Y="-1.099967836181" />
                  <Point X="-3.529500921294" Y="-0.31785525784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.789181296265" Y="0.324876224343" />
                  <Point X="-4.165634286525" Y="1.256630071464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.192493823499" Y="-3.880663328959" />
                  <Point X="-2.528734451433" Y="-3.048438571174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.777947046703" Y="-2.431615752916" />
                  <Point X="-3.106420335068" Y="-1.618615835186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.31226191015" Y="-1.109140058814" />
                  <Point X="-3.621952997648" Y="-0.342627719526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.904080960365" Y="0.355663491976" />
                  <Point X="-4.273851279268" Y="1.270877147076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.273174001955" Y="-3.934572260373" />
                  <Point X="-2.589012823711" Y="-3.152843744847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.861028002779" Y="-2.479582551207" />
                  <Point X="-3.184633483762" Y="-1.67863087953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.409547961908" Y="-1.121948011528" />
                  <Point X="-3.714405074003" Y="-0.367400181212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.018980624465" Y="0.38645075961" />
                  <Point X="-4.382068272011" Y="1.285124222688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.353845598816" Y="-3.988502431982" />
                  <Point X="-2.649291195988" Y="-3.257248918521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.94410895626" Y="-2.527549355918" />
                  <Point X="-3.262846632457" Y="-1.738645923875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.50683401741" Y="-1.134755954978" />
                  <Point X="-3.806857150358" Y="-0.392172642898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.133880288566" Y="0.417238027243" />
                  <Point X="-4.490285264754" Y="1.299371298299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.426435785972" Y="-4.062434794508" />
                  <Point X="-2.709569568265" Y="-3.361654092194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.027189909741" Y="-2.57551616063" />
                  <Point X="-3.341059781151" Y="-1.79866096822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.604120072911" Y="-1.147563898429" />
                  <Point X="-3.899309226712" Y="-0.416945104585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.248779952666" Y="0.448025294877" />
                  <Point X="-4.598502257497" Y="1.313618373911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.493555567439" Y="-4.149906886236" />
                  <Point X="-2.769847940543" Y="-3.466059265868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.110270863223" Y="-2.623482965341" />
                  <Point X="-3.419272929846" Y="-1.858676012564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.701406128413" Y="-1.160371841879" />
                  <Point X="-3.991761303067" Y="-0.441717566271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.363679616766" Y="0.47881256251" />
                  <Point X="-4.661383859228" Y="1.215656419235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.620206763645" Y="-4.090033555981" />
                  <Point X="-2.83012631282" Y="-3.570464439541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.193351816704" Y="-2.671449770053" />
                  <Point X="-3.49748607854" Y="-1.918691056909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.798692183914" Y="-1.173179785329" />
                  <Point X="-4.084213379421" Y="-0.466490027957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.478579280866" Y="0.509599830144" />
                  <Point X="-4.702516615633" Y="1.063864183414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.757670616432" Y="-4.00339796157" />
                  <Point X="-2.890404685098" Y="-3.674869613215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.276432770186" Y="-2.719416574764" />
                  <Point X="-3.575699227235" Y="-1.978706101254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.895978239416" Y="-1.185987728779" />
                  <Point X="-4.176665455776" Y="-0.491262489643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.593478944966" Y="0.540387097777" />
                  <Point X="-4.739556550581" Y="0.901941859014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.906398700882" Y="-3.888882415456" />
                  <Point X="-2.950683057375" Y="-3.779274786889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.359513723667" Y="-2.767383379476" />
                  <Point X="-3.653912375929" Y="-2.038721145598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.993264294917" Y="-1.19879567223" />
                  <Point X="-4.269117532131" Y="-0.51603495133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708378609066" Y="0.57117436541" />
                  <Point X="-4.76702328229" Y="0.71632502513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.442594677148" Y="-2.815350184187" />
                  <Point X="-3.732125524624" Y="-2.098736189943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.090550350418" Y="-1.21160361568" />
                  <Point X="-4.361569608485" Y="-0.540807413016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.52567563063" Y="-2.863316988899" />
                  <Point X="-3.810338673318" Y="-2.158751234288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.18783640592" Y="-1.22441155913" />
                  <Point X="-4.45402168484" Y="-0.565579874702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.608756584111" Y="-2.91128379361" />
                  <Point X="-3.888551822013" Y="-2.218766278632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.285122461421" Y="-1.237219502581" />
                  <Point X="-4.546473761195" Y="-0.590352336388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.691837537593" Y="-2.959250598322" />
                  <Point X="-3.966764970707" Y="-2.278781322977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.382408516923" Y="-1.250027446031" />
                  <Point X="-4.638925837549" Y="-0.615124798075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.774918491074" Y="-3.007217403033" />
                  <Point X="-4.044978119401" Y="-2.338796367321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.479694572424" Y="-1.262835389481" />
                  <Point X="-4.731377913904" Y="-0.639897259761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.96370415892" Y="-2.793555858877" />
                  <Point X="-4.123191268096" Y="-2.398811411666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.576980627926" Y="-1.275643332931" />
                  <Point X="-4.763753014665" Y="-0.813365453932" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="0.001626220703" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.834470397949" Y="-4.891590820312" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521543945312" />
                  <Point X="0.315979400635" Y="-3.307815185547" />
                  <Point X="0.300590881348" Y="-3.285643554688" />
                  <Point X="0.274335449219" Y="-3.266399658203" />
                  <Point X="0.0447890625" Y="-3.195156738281" />
                  <Point X="0.020976322174" Y="-3.187766357422" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.238211135864" Y="-3.259009277344" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.436618408203" Y="-3.499372314453" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.49687701416" Y="-3.677623535156" />
                  <Point X="-0.847744018555" Y="-4.987077148438" />
                  <Point X="-1.073547729492" Y="-4.943247558594" />
                  <Point X="-1.100244140625" Y="-4.938065917969" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.364521850586" Y="-4.259065917969" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.597619873047" Y="-4.017290527344" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240966797" Y="-3.985762939453" />
                  <Point X="-1.929732421875" Y="-3.967378417969" />
                  <Point X="-1.958830078125" Y="-3.965471435547" />
                  <Point X="-1.989878662109" Y="-3.973791015625" />
                  <Point X="-2.223599121094" Y="-4.129958496094" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.280871582031" Y="-4.184841308594" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.816703613281" Y="-4.19183984375" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.224782714844" Y="-3.883532714844" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-3.180500732422" Y="-3.797330566406" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.529697753906" Y="-2.553046386719" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-2.712871826172" Y="-2.613437744141" />
                  <Point X="-3.842959228516" Y="-3.265894042969" />
                  <Point X="-4.130788085937" Y="-2.88774609375" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.426211425781" Y="-2.403590087891" />
                  <Point X="-4.431020019531" Y="-2.395526855469" />
                  <Point X="-4.343885253906" Y="-2.328666259766" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145821777344" Y="-1.396013305664" />
                  <Point X="-3.138841308594" Y="-1.369061767578" />
                  <Point X="-3.1381171875" Y="-1.366265869141" />
                  <Point X="-3.140326171875" Y="-1.334595703125" />
                  <Point X="-3.161158447266" Y="-1.310639526367" />
                  <Point X="-3.185152099609" Y="-1.296518066406" />
                  <Point X="-3.187641113281" Y="-1.295052978516" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.372518066406" Y="-1.308712402344" />
                  <Point X="-4.803284179688" Y="-1.497076416016" />
                  <Point X="-4.915301757812" Y="-1.058530273438" />
                  <Point X="-4.927391601562" Y="-1.01119934082" />
                  <Point X="-4.997375" Y="-0.521882507324" />
                  <Point X="-4.998395996094" Y="-0.51474206543" />
                  <Point X="-4.9017734375" Y="-0.488852111816" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541895507813" Y="-0.12142477417" />
                  <Point X="-3.516750976562" Y="-0.103973075867" />
                  <Point X="-3.514142578125" Y="-0.102162742615" />
                  <Point X="-3.4948984375" Y="-0.07590625" />
                  <Point X="-3.486516845703" Y="-0.048900741577" />
                  <Point X="-3.485647460938" Y="-0.046099216461" />
                  <Point X="-3.485647460938" Y="-0.016460876465" />
                  <Point X="-3.494029052734" Y="0.010544633865" />
                  <Point X="-3.4948984375" Y="0.013346159935" />
                  <Point X="-3.514142578125" Y="0.039602649689" />
                  <Point X="-3.539287109375" Y="0.057054344177" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-3.696920410156" Y="0.103452552795" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.925437011719" Y="0.943760925293" />
                  <Point X="-4.91764453125" Y="0.996421569824" />
                  <Point X="-4.776774414062" Y="1.516276611328" />
                  <Point X="-4.773516601562" Y="1.528298706055" />
                  <Point X="-4.716900390625" Y="1.520845214844" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704345703" Y="1.395866699219" />
                  <Point X="-3.676051513672" Y="1.41341394043" />
                  <Point X="-3.670278320312" Y="1.41523425293" />
                  <Point X="-3.651534667969" Y="1.426056030273" />
                  <Point X="-3.639120361328" Y="1.44378515625" />
                  <Point X="-3.616789550781" Y="1.497696655273" />
                  <Point X="-3.614472900391" Y="1.503289428711" />
                  <Point X="-3.610714111328" Y="1.524604858398" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.643260498047" Y="1.597271850586" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221801758" />
                  <Point X="-3.7399609375" Y="1.680602294922" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.190291503906" Y="2.735134765625" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.786850341797" Y="3.266656494141" />
                  <Point X="-3.774671386719" Y="3.282310791016" />
                  <Point X="-3.750994140625" Y="3.268640869141" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514160156" Y="2.920434814453" />
                  <Point X="-3.061005371094" Y="2.913653564453" />
                  <Point X="-3.05296484375" Y="2.912950195312" />
                  <Point X="-3.031506347656" Y="2.915775634766" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.958236083984" Y="2.982420898438" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899658203" Y="3.006382080078" />
                  <Point X="-2.93807421875" Y="3.027840576172" />
                  <Point X="-2.94485546875" Y="3.105349365234" />
                  <Point X="-2.945558837891" Y="3.113390136719" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-2.987514648438" Y="3.195429199219" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.805606933594" Y="4.133903320312" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.165153808594" Y="4.500857421875" />
                  <Point X="-2.141548828125" Y="4.513971679688" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246582031" Y="4.273660644531" />
                  <Point X="-1.864979858398" Y="4.228752441406" />
                  <Point X="-1.856030639648" Y="4.22409375" />
                  <Point X="-1.835124389648" Y="4.218491699219" />
                  <Point X="-1.813809448242" Y="4.22225" />
                  <Point X="-1.723956542969" Y="4.25946875" />
                  <Point X="-1.714635498047" Y="4.263329589844" />
                  <Point X="-1.696905639648" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.656838134766" Y="4.387242675781" />
                  <Point X="-1.653804199219" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.655947631836" Y="4.449037109375" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.036357788086" Y="4.884157714844" />
                  <Point X="-0.968094055176" Y="4.903296386719" />
                  <Point X="-0.255594848633" Y="4.986684082031" />
                  <Point X="-0.224199661255" Y="4.990358398438" />
                  <Point X="-0.217222137451" Y="4.964317871094" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282115936" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.072614936829" Y="4.3786875" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.800598999023" Y="4.931808105469" />
                  <Point X="0.860205383301" Y="4.925565429688" />
                  <Point X="1.449673950195" Y="4.78325" />
                  <Point X="1.508456542969" Y="4.769057617188" />
                  <Point X="1.892978271484" Y="4.629588867188" />
                  <Point X="1.931036621094" Y="4.615784667969" />
                  <Point X="2.302038818359" Y="4.442279785156" />
                  <Point X="2.338695556641" Y="4.425136230469" />
                  <Point X="2.697101318359" Y="4.216328613281" />
                  <Point X="2.732524902344" Y="4.195690429687" />
                  <Point X="3.068740478516" Y="3.956592773438" />
                  <Point X="3.010498535156" Y="3.85571484375" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852294922" Y="2.491515380859" />
                  <Point X="2.205400634766" Y="2.418775146484" />
                  <Point X="2.202044433594" Y="2.392327148438" />
                  <Point X="2.209629150391" Y="2.329427490234" />
                  <Point X="2.210415771484" Y="2.32290234375" />
                  <Point X="2.218682373047" Y="2.300811767578" />
                  <Point X="2.257602539062" Y="2.243453369141" />
                  <Point X="2.274940429688" Y="2.224203369141" />
                  <Point X="2.332298828125" Y="2.185283203125" />
                  <Point X="2.338249023438" Y="2.181245849609" />
                  <Point X="2.360336669922" Y="2.172980224609" />
                  <Point X="2.423236328125" Y="2.165395507813" />
                  <Point X="2.4486640625" Y="2.165946289062" />
                  <Point X="2.521404541016" Y="2.185398193359" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="2.681300292969" Y="2.273400390625" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.181580566406" Y="2.771081298828" />
                  <Point X="4.202592773438" Y="2.741878662109" />
                  <Point X="4.387513183594" Y="2.436296386719" />
                  <Point X="4.316408203125" Y="2.381735595703" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.279371582031" Y="1.583833496094" />
                  <Point X="3.227020263672" Y="1.515537109375" />
                  <Point X="3.213119384766" Y="1.4915" />
                  <Point X="3.193618408203" Y="1.421769042969" />
                  <Point X="3.191595458984" Y="1.41453527832" />
                  <Point X="3.190779541016" Y="1.390965576172" />
                  <Point X="3.206787841797" Y="1.313380981445" />
                  <Point X="3.215646484375" Y="1.287954833984" />
                  <Point X="3.259187255859" Y="1.221774780273" />
                  <Point X="3.263704101562" Y="1.214909423828" />
                  <Point X="3.280948730469" Y="1.198819335938" />
                  <Point X="3.344045166016" Y="1.163301391602" />
                  <Point X="3.368565185547" Y="1.153619628906" />
                  <Point X="3.453875976562" Y="1.142344604492" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.609047363281" Y="1.158713623047" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.9301640625" Y="0.988453308105" />
                  <Point X="4.939188476562" Y="0.951384277344" />
                  <Point X="4.997858398438" Y="0.574556213379" />
                  <Point X="4.919039550781" Y="0.553436767578" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819259644" />
                  <Point X="3.645271728516" Y="0.184372650146" />
                  <Point X="3.622264648438" Y="0.166926269531" />
                  <Point X="3.571975585938" Y="0.102846069336" />
                  <Point X="3.566758789062" Y="0.09619858551" />
                  <Point X="3.556985351562" Y="0.074735427856" />
                  <Point X="3.540222167969" Y="-0.012794874191" />
                  <Point X="3.538483154297" Y="-0.040684940338" />
                  <Point X="3.555246337891" Y="-0.128215240479" />
                  <Point X="3.556985351562" Y="-0.137295516968" />
                  <Point X="3.566758789062" Y="-0.158758682251" />
                  <Point X="3.617047851562" Y="-0.222838729858" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.720392089844" Y="-0.29035357666" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="3.863358154297" Y="-0.3331277771" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.953470703125" Y="-0.932978149414" />
                  <Point X="4.948431640625" Y="-0.966399841309" />
                  <Point X="4.874545410156" Y="-1.290178344727" />
                  <Point X="4.777982421875" Y="-1.277465576172" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.3948359375" Y="-1.098341308594" />
                  <Point X="3.230336425781" Y="-1.134095947266" />
                  <Point X="3.213271728516" Y="-1.137805053711" />
                  <Point X="3.185445556641" Y="-1.154697143555" />
                  <Point X="3.086016113281" Y="-1.274280029297" />
                  <Point X="3.075701416016" Y="-1.286685180664" />
                  <Point X="3.064357910156" Y="-1.314070922852" />
                  <Point X="3.050107177734" Y="-1.468935546875" />
                  <Point X="3.04862890625" Y="-1.485001098633" />
                  <Point X="3.056360351562" Y="-1.516621582031" />
                  <Point X="3.147396728516" Y="-1.65822277832" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.281854248047" Y="-1.77255065918" />
                  <Point X="4.33907421875" Y="-2.583783935547" />
                  <Point X="4.218334472656" Y="-2.779158935547" />
                  <Point X="4.204129394531" Y="-2.802145019531" />
                  <Point X="4.056688232422" Y="-3.011638427734" />
                  <Point X="3.970062011719" Y="-2.961624511719" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.541560302734" Y="-2.217955078125" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077880859" Y="-2.219244873047" />
                  <Point X="2.326432373047" Y="-2.304843994141" />
                  <Point X="2.309559814453" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.203000732422" Y="-2.497329345703" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.224520751953" Y="-2.742155029297" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.306958251953" Y="-2.904787597656" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.852154541016" Y="-4.178172851563" />
                  <Point X="2.835298828125" Y="-4.190212402344" />
                  <Point X="2.679776123047" Y="-4.290879394531" />
                  <Point X="2.611561279297" Y="-4.20198046875" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549560547" Y="-2.980467529297" />
                  <Point X="1.47745703125" Y="-2.856326904297" />
                  <Point X="1.457426025391" Y="-2.843448730469" />
                  <Point X="1.425804443359" Y="-2.835717041016" />
                  <Point X="1.21462512207" Y="-2.855149902344" />
                  <Point X="1.192717651367" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509033203" />
                  <Point X="1.002265625" Y="-3.004094238281" />
                  <Point X="0.985349365234" Y="-3.018159423828" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.919700439453" Y="-3.270303710938" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="0.934579345703" Y="-3.467571533203" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.010288696289" Y="-4.959752441406" />
                  <Point X="0.994347717285" Y="-4.963246582031" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#208" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.175081857811" Y="5.008538895752" Z="2.3" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.3" />
                  <Point X="-0.267562846851" Y="5.067625162536" Z="2.3" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.3" />
                  <Point X="-1.056011556553" Y="4.963581276655" Z="2.3" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.3" />
                  <Point X="-1.711675831429" Y="4.47379101261" Z="2.3" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.3" />
                  <Point X="-1.710679761189" Y="4.433558384292" Z="2.3" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.3" />
                  <Point X="-1.749246320335" Y="4.336943110796" Z="2.3" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.3" />
                  <Point X="-1.848048242248" Y="4.304383266851" Z="2.3" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.3" />
                  <Point X="-2.115494499671" Y="4.58540888818" Z="2.3" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.3" />
                  <Point X="-2.19559270463" Y="4.575844749492" Z="2.3" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.3" />
                  <Point X="-2.842184969013" Y="4.204863058547" Z="2.3" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.3" />
                  <Point X="-3.036971817282" Y="3.201709137485" Z="2.3" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.3" />
                  <Point X="-3.000821215918" Y="3.132272282341" Z="2.3" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.3" />
                  <Point X="-2.999747307384" Y="3.049056260502" Z="2.3" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.3" />
                  <Point X="-3.062804183266" Y="2.994743483255" Z="2.3" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.3" />
                  <Point X="-3.732150210812" Y="3.343222131464" Z="2.3" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.3" />
                  <Point X="-3.832469669063" Y="3.328638930847" Z="2.3" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.3" />
                  <Point X="-4.239629434203" Y="2.791619920246" Z="2.3" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.3" />
                  <Point X="-3.776554861353" Y="1.672214573506" Z="2.3" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.3" />
                  <Point X="-3.693767095323" Y="1.605464601615" Z="2.3" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.3" />
                  <Point X="-3.669139004608" Y="1.548111683173" Z="2.3" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.3" />
                  <Point X="-3.697243240715" Y="1.492379787339" Z="2.3" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.3" />
                  <Point X="-4.716530088512" Y="1.601697398357" Z="2.3" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.3" />
                  <Point X="-4.831189525224" Y="1.560634153161" Z="2.3" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.3" />
                  <Point X="-4.981054388172" Y="0.982360130902" Z="2.3" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.3" />
                  <Point X="-3.716017224558" Y="0.086436286216" Z="2.3" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.3" />
                  <Point X="-3.573952293499" Y="0.047258624225" Z="2.3" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.3" />
                  <Point X="-3.547938363747" Y="0.027005450117" Z="2.3" />
                  <Point X="-3.539556741714" Y="0" Z="2.3" />
                  <Point X="-3.540426237388" Y="-0.002801500944" Z="2.3" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.3" />
                  <Point X="-3.551416301624" Y="-0.031617358909" Z="2.3" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.3" />
                  <Point X="-4.920870564215" Y="-0.40927577054" Z="2.3" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.3" />
                  <Point X="-5.053027430509" Y="-0.497681215694" Z="2.3" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.3" />
                  <Point X="-4.969902039786" Y="-1.039624501403" Z="2.3" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.3" />
                  <Point X="-3.372147617408" Y="-1.327004652437" Z="2.3" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.3" />
                  <Point X="-3.216669838293" Y="-1.308328254216" Z="2.3" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.3" />
                  <Point X="-3.193400437328" Y="-1.32524570409" Z="2.3" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.3" />
                  <Point X="-4.380479141201" Y="-2.257718436053" Z="2.3" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.3" />
                  <Point X="-4.475310773542" Y="-2.397919649902" Z="2.3" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.3" />
                  <Point X="-4.176730078599" Y="-2.88674935967" Z="2.3" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.3" />
                  <Point X="-2.694027794091" Y="-2.62545906471" Z="2.3" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.3" />
                  <Point X="-2.571208942036" Y="-2.557121527095" Z="2.3" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.3" />
                  <Point X="-3.229957948458" Y="-3.741050588401" Z="2.3" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.3" />
                  <Point X="-3.26144253424" Y="-3.891869903608" Z="2.3" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.3" />
                  <Point X="-2.849181077579" Y="-4.203070643405" Z="2.3" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.3" />
                  <Point X="-2.247359792249" Y="-4.183999115925" Z="2.3" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.3" />
                  <Point X="-2.201976496723" Y="-4.140251642886" Z="2.3" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.3" />
                  <Point X="-1.939158277608" Y="-3.985991535096" Z="2.3" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.3" />
                  <Point X="-1.63674308558" Y="-4.023602585412" Z="2.3" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.3" />
                  <Point X="-1.419717115145" Y="-4.237540330011" Z="2.3" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.3" />
                  <Point X="-1.408566892278" Y="-4.845078105354" Z="2.3" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.3" />
                  <Point X="-1.385307009081" Y="-4.886653900794" Z="2.3" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.3" />
                  <Point X="-1.089230409292" Y="-4.961051746018" Z="2.3" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.3" />
                  <Point X="-0.454736685573" Y="-3.659284449193" Z="2.3" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.3" />
                  <Point X="-0.401698312223" Y="-3.496601230557" Z="2.3" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.3" />
                  <Point X="-0.229546321777" Y="-3.275482075668" Z="2.3" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.3" />
                  <Point X="0.023812757584" Y="-3.21162996962" Z="2.3" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.3" />
                  <Point X="0.268747547209" Y="-3.305044525211" Z="2.3" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.3" />
                  <Point X="0.780018174828" Y="-4.873251580623" Z="2.3" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.3" />
                  <Point X="0.834618103204" Y="-5.010683750354" Z="2.3" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.3" />
                  <Point X="1.014838066144" Y="-4.977312709439" Z="2.3" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.3" />
                  <Point X="0.977995661709" Y="-3.429765071398" Z="2.3" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.3" />
                  <Point X="0.962403685218" Y="-3.249643380016" Z="2.3" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.3" />
                  <Point X="1.028076358918" Y="-3.011260629196" Z="2.3" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.3" />
                  <Point X="1.213051145272" Y="-2.8736595015" Z="2.3" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.3" />
                  <Point X="1.444261539678" Y="-2.867104724982" Z="2.3" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.3" />
                  <Point X="2.565737666781" Y="-4.201138307297" Z="2.3" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.3" />
                  <Point X="2.680395691756" Y="-4.314773651907" Z="2.3" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.3" />
                  <Point X="2.875059809325" Y="-4.187579713786" Z="2.3" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.3" />
                  <Point X="2.344103645653" Y="-2.848507481002" Z="2.3" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.3" />
                  <Point X="2.267568934327" Y="-2.701988675889" Z="2.3" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.3" />
                  <Point X="2.241091029694" Y="-2.489335613581" Z="2.3" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.3" />
                  <Point X="2.343562764791" Y="-2.317810280273" Z="2.3" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.3" />
                  <Point X="2.526518127624" Y="-2.235879075164" Z="2.3" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.3" />
                  <Point X="3.938906116105" Y="-2.973645854622" Z="2.3" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.3" />
                  <Point X="4.081525994341" Y="-3.023194805725" Z="2.3" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.3" />
                  <Point X="4.254712077643" Y="-2.774163917398" Z="2.3" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.3" />
                  <Point X="3.306136336338" Y="-1.701602942527" Z="2.3" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.3" />
                  <Point X="3.183298862798" Y="-1.59990356717" Z="2.3" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.3" />
                  <Point X="3.093740841843" Y="-1.44223707401" Z="2.3" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.3" />
                  <Point X="3.118306102067" Y="-1.274966915628" Z="2.3" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.3" />
                  <Point X="3.234800429135" Y="-1.151675056278" Z="2.3" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.3" />
                  <Point X="4.765300121862" Y="-1.295757757065" Z="2.3" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.3" />
                  <Point X="4.914942317134" Y="-1.279639006519" Z="2.3" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.3" />
                  <Point X="4.996755932335" Y="-0.909152639149" Z="2.3" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.3" />
                  <Point X="3.870143194204" Y="-0.25355147971" Z="2.3" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.3" />
                  <Point X="3.73925785497" Y="-0.215784860795" Z="2.3" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.3" />
                  <Point X="3.650225807876" Y="-0.160690642134" Z="2.3" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.3" />
                  <Point X="3.598197754771" Y="-0.087530283664" Z="2.3" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.3" />
                  <Point X="3.583173695653" Y="0.009080247515" Z="2.3" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.3" />
                  <Point X="3.605153630524" Y="0.103258096441" Z="2.3" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.3" />
                  <Point X="3.664137559383" Y="0.172363983447" Z="2.3" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.3" />
                  <Point X="4.925824623103" Y="0.536420453059" Z="2.3" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.3" />
                  <Point X="5.041821087001" Y="0.608944543603" Z="2.3" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.3" />
                  <Point X="4.972589305268" Y="1.031560581445" Z="2.3" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.3" />
                  <Point X="3.596365071641" Y="1.239565948829" Z="2.3" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.3" />
                  <Point X="3.454271412851" Y="1.223193718625" Z="2.3" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.3" />
                  <Point X="3.36241508817" Y="1.238153146198" Z="2.3" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.3" />
                  <Point X="3.294801710001" Y="1.280536432323" Z="2.3" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.3" />
                  <Point X="3.249600322321" Y="1.354764874685" Z="2.3" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.3" />
                  <Point X="3.235615030407" Y="1.439582957712" Z="2.3" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.3" />
                  <Point X="3.260546826895" Y="1.516398662658" Z="2.3" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.3" />
                  <Point X="4.340690402616" Y="2.373347800183" Z="2.3" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.3" />
                  <Point X="4.427656328182" Y="2.487642336438" Z="2.3" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.3" />
                  <Point X="4.216010020457" Y="2.831564161416" Z="2.3" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.3" />
                  <Point X="2.650144297997" Y="2.347981594241" Z="2.3" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.3" />
                  <Point X="2.50233196065" Y="2.264980879976" Z="2.3" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.3" />
                  <Point X="2.423066539097" Y="2.246315983013" Z="2.3" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.3" />
                  <Point X="2.354216517416" Y="2.257938029164" Z="2.3" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.3" />
                  <Point X="2.292820571071" Y="2.302808342966" Z="2.3" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.3" />
                  <Point X="2.253113719746" Y="2.366691904736" Z="2.3" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.3" />
                  <Point X="2.247547062681" Y="2.437137615232" Z="2.3" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.3" />
                  <Point X="3.047643758618" Y="3.861994783958" Z="2.3" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.3" />
                  <Point X="3.093368918636" Y="4.027334466271" Z="2.3" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.3" />
                  <Point X="2.716115440868" Y="4.290811073191" Z="2.3" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.3" />
                  <Point X="2.317064829412" Y="4.518851111919" Z="2.3" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.3" />
                  <Point X="1.903870687266" Y="4.707872974747" Z="2.3" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.3" />
                  <Point X="1.455252497482" Y="4.863133407318" Z="2.3" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.3" />
                  <Point X="0.799651051753" Y="5.012816077128" Z="2.3" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.3" />
                  <Point X="0.018162703742" Y="4.422908610199" Z="2.3" />
                  <Point X="0" Y="4.355124473572" Z="2.3" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>