<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#151" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1409" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.78389453125" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.697556640625" Y="-4.013563720703" />
                  <Point X="25.5633046875" Y="-3.512524414063" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.4874921875" Y="-3.388315917969" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.217583984375" Y="-3.149315673828" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.878263671875" Y="-3.123389404297" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231477783203" />
                  <Point X="24.5788046875" Y="-3.310538574219" />
                  <Point X="24.46994921875" Y="-3.467378173828" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.182681640625" Y="-4.506474609375" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.82578515625" Y="-4.820939453125" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.768876953125" Y="-4.686192871094" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.51622265625" />
                  <Point X="23.763205078125" Y="-4.414240722656" />
                  <Point X="23.722962890625" Y="-4.211931152344" />
                  <Point X="23.712060546875" Y="-4.182965332031" />
                  <Point X="23.69598828125" Y="-4.155127929687" />
                  <Point X="23.67635546875" Y="-4.131204589844" />
                  <Point X="23.5981796875" Y="-4.062645507813" />
                  <Point X="23.443095703125" Y="-3.926639892578" />
                  <Point X="23.4168125" Y="-3.910295898438" />
                  <Point X="23.387115234375" Y="-3.897994628906" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.25321484375" Y="-3.884165527344" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.87088671875" Y="-3.952570068359" />
                  <Point X="22.699376953125" Y="-4.067169921875" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.5198515625" Y="-4.288488769531" />
                  <Point X="22.484125" Y="-4.266367675781" />
                  <Point X="22.198287109375" Y="-4.089384521484" />
                  <Point X="22.066916015625" Y="-3.988233154297" />
                  <Point X="21.89527734375" Y="-3.856077392578" />
                  <Point X="22.326744140625" Y="-3.108756103516" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647655273438" />
                  <Point X="22.593412109375" Y="-2.616129882812" />
                  <Point X="22.59442578125" Y="-2.585197509766" />
                  <Point X="22.585443359375" Y="-2.555581054688" />
                  <Point X="22.571228515625" Y="-2.526752685547" />
                  <Point X="22.553201171875" Y="-2.501592529297" />
                  <Point X="22.535853515625" Y="-2.484243896484" />
                  <Point X="22.5106953125" Y="-2.466215332031" />
                  <Point X="22.481865234375" Y="-2.451997070313" />
                  <Point X="22.452248046875" Y="-2.443011962891" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.5035546875" Y="-2.956138183594" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="21.142962890625" Y="-3.090545410156" />
                  <Point X="20.917142578125" Y="-2.793863037109" />
                  <Point X="20.82295703125" Y="-2.6359296875" />
                  <Point X="20.693857421875" Y="-2.419450195312" />
                  <Point X="21.45751171875" Y="-1.833477416992" />
                  <Point X="21.894044921875" Y="-1.498513305664" />
                  <Point X="21.91693359375" Y="-1.475881958008" />
                  <Point X="21.936525390625" Y="-1.444145874023" />
                  <Point X="21.9458203125" Y="-1.419109130859" />
                  <Point X="21.948724609375" Y="-1.40986706543" />
                  <Point X="21.95384765625" Y="-1.390089355469" />
                  <Point X="21.957962890625" Y="-1.358610717773" />
                  <Point X="21.957265625" Y="-1.335738037109" />
                  <Point X="21.9511171875" Y="-1.313695922852" />
                  <Point X="21.939111328125" Y="-1.284712402344" />
                  <Point X="21.92605859375" Y="-1.262393310547" />
                  <Point X="21.90746875" Y="-1.244420776367" />
                  <Point X="21.886091796875" Y="-1.228767333984" />
                  <Point X="21.87815234375" Y="-1.223542724609" />
                  <Point X="21.860544921875" Y="-1.213179931641" />
                  <Point X="21.831283203125" Y="-1.201956176758" />
                  <Point X="21.799396484375" Y="-1.195474731445" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="20.685859375" Y="-1.336859741211" />
                  <Point X="20.2678984375" Y="-1.391885253906" />
                  <Point X="20.254244140625" Y="-1.338427734375" />
                  <Point X="20.165921875" Y="-0.992648620605" />
                  <Point X="20.141005859375" Y="-0.818428466797" />
                  <Point X="20.107578125" Y="-0.584698303223" />
                  <Point X="20.96984765625" Y="-0.353653564453" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.488314453125" Y="-0.211888778687" />
                  <Point X="21.513181640625" Y="-0.198213821411" />
                  <Point X="21.521568359375" Y="-0.193016983032" />
                  <Point X="21.540021484375" Y="-0.180210723877" />
                  <Point X="21.563978515625" Y="-0.158685028076" />
                  <Point X="21.58508984375" Y="-0.127851852417" />
                  <Point X="21.595615234375" Y="-0.103123527527" />
                  <Point X="21.59893359375" Y="-0.094076515198" />
                  <Point X="21.605083984375" Y="-0.074259216309" />
                  <Point X="21.61052734375" Y="-0.045515129089" />
                  <Point X="21.60946875" Y="-0.011270002365" />
                  <Point X="21.604123046875" Y="0.013539984703" />
                  <Point X="21.601984375" Y="0.021688549042" />
                  <Point X="21.595833984375" Y="0.041505996704" />
                  <Point X="21.582515625" Y="0.07083265686" />
                  <Point X="21.559244140625" Y="0.100434516907" />
                  <Point X="21.538123046875" Y="0.118407867432" />
                  <Point X="21.530724609375" Y="0.124101829529" />
                  <Point X="21.5122734375" Y="0.136908233643" />
                  <Point X="21.498076171875" Y="0.145046478271" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="20.48063671875" Y="0.422177001953" />
                  <Point X="20.108185546875" Y="0.521975402832" />
                  <Point X="20.118591796875" Y="0.592306884766" />
                  <Point X="20.17551171875" Y="0.976969055176" />
                  <Point X="20.225673828125" Y="1.162084228516" />
                  <Point X="20.29644921875" Y="1.423268066406" />
                  <Point X="20.877875" Y="1.346721923828" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228027344" />
                  <Point X="21.296865234375" Y="1.305263793945" />
                  <Point X="21.317451171875" Y="1.311754394531" />
                  <Point X="21.358291015625" Y="1.324631225586" />
                  <Point X="21.37722265625" Y="1.332961791992" />
                  <Point X="21.395966796875" Y="1.343783935547" />
                  <Point X="21.4126484375" Y="1.356015625" />
                  <Point X="21.426287109375" Y="1.371567993164" />
                  <Point X="21.438701171875" Y="1.389298095703" />
                  <Point X="21.448650390625" Y="1.407433959961" />
                  <Point X="21.45691015625" Y="1.427376464844" />
                  <Point X="21.473296875" Y="1.466938232422" />
                  <Point X="21.479083984375" Y="1.486788330078" />
                  <Point X="21.48284375" Y="1.508103759766" />
                  <Point X="21.484197265625" Y="1.528750244141" />
                  <Point X="21.481048828125" Y="1.549200195312" />
                  <Point X="21.4754453125" Y="1.570106811523" />
                  <Point X="21.467951171875" Y="1.589377075195" />
                  <Point X="21.457984375" Y="1.608523925781" />
                  <Point X="21.4382109375" Y="1.646506835938" />
                  <Point X="21.426716796875" Y="1.663707763672" />
                  <Point X="21.4128046875" Y="1.680287231445" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="20.83201171875" Y="2.128783935547" />
                  <Point X="20.648140625" Y="2.269873535156" />
                  <Point X="20.697673828125" Y="2.354735595703" />
                  <Point X="20.918853515625" Y="2.733666992188" />
                  <Point X="21.05171875" Y="2.904447998047" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.574416015625" Y="2.971069335938" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.88187890625" Y="2.823287841797" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.019537109375" Y="2.835653076172" />
                  <Point X="22.037791015625" Y="2.847281982422" />
                  <Point X="22.053921875" Y="2.860228271484" />
                  <Point X="22.0742734375" Y="2.880579345703" />
                  <Point X="22.114646484375" Y="2.920951660156" />
                  <Point X="22.127595703125" Y="2.9370859375" />
                  <Point X="22.139224609375" Y="2.955340576172" />
                  <Point X="22.14837109375" Y="2.973889404297" />
                  <Point X="22.1532890625" Y="2.993978027344" />
                  <Point X="22.156115234375" Y="3.015437255859" />
                  <Point X="22.15656640625" Y="3.036119384766" />
                  <Point X="22.15405859375" Y="3.064791015625" />
                  <Point X="22.14908203125" Y="3.121668945312" />
                  <Point X="22.145044921875" Y="3.141963867188" />
                  <Point X="22.13853515625" Y="3.162605712891" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.879458984375" Y="3.615837890625" />
                  <Point X="21.81666796875" Y="3.724596191406" />
                  <Point X="21.9141640625" Y="3.799346191406" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.508642578125" Y="4.210947265625" />
                  <Point X="22.832962890625" Y="4.391133789062" />
                  <Point X="22.889783203125" Y="4.317083496094" />
                  <Point X="22.9568046875" Y="4.229740234375" />
                  <Point X="22.971111328125" Y="4.214796875" />
                  <Point X="22.98769140625" Y="4.200885253906" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.03680078125" Y="4.172782714844" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.1193828125" Y="4.132331542969" />
                  <Point X="23.1402890625" Y="4.126729492187" />
                  <Point X="23.160732421875" Y="4.123582519531" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.202685546875" Y="4.128692871094" />
                  <Point X="23.222548828125" Y="4.134482421875" />
                  <Point X="23.25578515625" Y="4.148250488281" />
                  <Point X="23.32172265625" Y="4.175562011719" />
                  <Point X="23.339853515625" Y="4.185509277344" />
                  <Point X="23.357583984375" Y="4.197923828125" />
                  <Point X="23.37313671875" Y="4.2115625" />
                  <Point X="23.3853671875" Y="4.228244140625" />
                  <Point X="23.396189453125" Y="4.246988769531" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.41533984375" Y="4.300232421875" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583984375" />
                  <Point X="23.44272265625" Y="4.410145019531" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.5516875" Y="4.669996582031" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.3040546875" Y="4.839499023438" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.80133203125" Y="4.528021972656" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.274697265625" Y="4.765807617188" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.4086171875" Y="4.877339355469" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.053931640625" Y="4.781064941406" />
                  <Point X="26.4810234375" Y="4.677951660156" />
                  <Point X="26.616658203125" Y="4.628756347656" />
                  <Point X="26.894650390625" Y="4.527926757812" />
                  <Point X="27.026751953125" Y="4.466146972656" />
                  <Point X="27.294572265625" Y="4.340895996094" />
                  <Point X="27.4222265625" Y="4.266524902344" />
                  <Point X="27.680982421875" Y="4.1157734375" />
                  <Point X="27.80134375" Y="4.030178222656" />
                  <Point X="27.94326171875" Y="3.929254150391" />
                  <Point X="27.436353515625" Y="3.051263671875" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.539936035156" />
                  <Point X="27.133078125" Y="2.516057128906" />
                  <Point X="27.1258828125" Y="2.489149414062" />
                  <Point X="27.111607421875" Y="2.435770751953" />
                  <Point X="27.108619140625" Y="2.417936523438" />
                  <Point X="27.107728515625" Y="2.380950195312" />
                  <Point X="27.11053515625" Y="2.357682861328" />
                  <Point X="27.116099609375" Y="2.311525390625" />
                  <Point X="27.12144140625" Y="2.289609375" />
                  <Point X="27.12970703125" Y="2.26751953125" />
                  <Point X="27.140072265625" Y="2.247467773438" />
                  <Point X="27.154470703125" Y="2.226250244141" />
                  <Point X="27.18303125" Y="2.184159179688" />
                  <Point X="27.194466796875" Y="2.170328125" />
                  <Point X="27.22159765625" Y="2.145593261719" />
                  <Point X="27.242814453125" Y="2.131196044922" />
                  <Point X="27.28490625" Y="2.102635742188" />
                  <Point X="27.304955078125" Y="2.092271728516" />
                  <Point X="27.32704296875" Y="2.084006103516" />
                  <Point X="27.34896484375" Y="2.078663330078" />
                  <Point X="27.372232421875" Y="2.075857666016" />
                  <Point X="27.418388671875" Y="2.070291748047" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.473205078125" Y="2.074170898438" />
                  <Point X="27.50011328125" Y="2.081366455078" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.580751953125" Y="2.683001953125" />
                  <Point X="28.967326171875" Y="2.906190673828" />
                  <Point X="28.969787109375" Y="2.902770263672" />
                  <Point X="29.12328125" Y="2.689450195312" />
                  <Point X="29.190375" Y="2.578575927734" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.610978515625" Y="1.960184204102" />
                  <Point X="28.23078515625" Y="1.668451171875" />
                  <Point X="28.2214296875" Y="1.660244506836" />
                  <Point X="28.20397265625" Y="1.641627075195" />
                  <Point X="28.184607421875" Y="1.616363647461" />
                  <Point X="28.14619140625" Y="1.566245849609" />
                  <Point X="28.13660546875" Y="1.550909179688" />
                  <Point X="28.1216328125" Y="1.517090209961" />
                  <Point X="28.11441796875" Y="1.491295898438" />
                  <Point X="28.100107421875" Y="1.440125732422" />
                  <Point X="28.09665234375" Y="1.41782421875" />
                  <Point X="28.0958359375" Y="1.394253173828" />
                  <Point X="28.097740234375" Y="1.371766723633" />
                  <Point X="28.103662109375" Y="1.343067260742" />
                  <Point X="28.11541015625" Y="1.286133911133" />
                  <Point X="28.1206796875" Y="1.268978881836" />
                  <Point X="28.13628125" Y="1.235742431641" />
                  <Point X="28.15238671875" Y="1.211261474609" />
                  <Point X="28.184337890625" Y="1.162696899414" />
                  <Point X="28.19889453125" Y="1.14544921875" />
                  <Point X="28.216138671875" Y="1.129359741211" />
                  <Point X="28.23434765625" Y="1.116034057617" />
                  <Point X="28.257689453125" Y="1.102895385742" />
                  <Point X="28.303990234375" Y="1.076831542969" />
                  <Point X="28.320521484375" Y="1.069501342773" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.387677734375" Y="1.055267822266" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.430744140625" Y="1.171072265625" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.780015625" Y="1.203590942383" />
                  <Point X="29.84594140625" Y="0.932787963867" />
                  <Point X="29.867083984375" Y="0.796993347168" />
                  <Point X="29.890865234375" Y="0.644238891602" />
                  <Point X="29.152291015625" Y="0.44633795166" />
                  <Point X="28.716580078125" Y="0.329589874268" />
                  <Point X="28.7047890625" Y="0.325585662842" />
                  <Point X="28.681546875" Y="0.315067810059" />
                  <Point X="28.65054296875" Y="0.297146820068" />
                  <Point X="28.589037109375" Y="0.261595367432" />
                  <Point X="28.5743125" Y="0.25109588623" />
                  <Point X="28.547533203125" Y="0.225577957153" />
                  <Point X="28.5289296875" Y="0.201873748779" />
                  <Point X="28.49202734375" Y="0.154850158691" />
                  <Point X="28.48030078125" Y="0.135567306519" />
                  <Point X="28.47052734375" Y="0.11410369873" />
                  <Point X="28.463681640625" Y="0.092604560852" />
                  <Point X="28.45748046875" Y="0.06022600174" />
                  <Point X="28.4451796875" Y="-0.004005921364" />
                  <Point X="28.443484375" Y="-0.021875597" />
                  <Point X="28.4451796875" Y="-0.058554225922" />
                  <Point X="28.451380859375" Y="-0.090932785034" />
                  <Point X="28.463681640625" Y="-0.155164550781" />
                  <Point X="28.47052734375" Y="-0.17666217041" />
                  <Point X="28.48030078125" Y="-0.198126846313" />
                  <Point X="28.4920234375" Y="-0.217407730103" />
                  <Point X="28.510625" Y="-0.24111177063" />
                  <Point X="28.547529296875" Y="-0.288135375977" />
                  <Point X="28.560001953125" Y="-0.301238830566" />
                  <Point X="28.589037109375" Y="-0.324155517578" />
                  <Point X="28.620041015625" Y="-0.342076507568" />
                  <Point X="28.681546875" Y="-0.377627929688" />
                  <Point X="28.692708984375" Y="-0.383138244629" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="29.580931640625" Y="-0.623752441406" />
                  <Point X="29.89147265625" Y="-0.706961669922" />
                  <Point X="29.85501953125" Y="-0.948747741699" />
                  <Point X="29.82793359375" Y="-1.067443603516" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="28.92968359375" Y="-1.069965209961" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.31380859375" Y="-1.018734985352" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.1639765625" Y="-1.056596313477" />
                  <Point X="28.1361484375" Y="-1.073489013672" />
                  <Point X="28.1123984375" Y="-1.093959472656" />
                  <Point X="28.0756171875" Y="-1.138194580078" />
                  <Point X="28.002654296875" Y="-1.225947387695" />
                  <Point X="27.987931640625" Y="-1.250334838867" />
                  <Point X="27.97658984375" Y="-1.277720092773" />
                  <Point X="27.969759765625" Y="-1.305365722656" />
                  <Point X="27.96448828125" Y="-1.36265234375" />
                  <Point X="27.95403125" Y="-1.476296142578" />
                  <Point X="27.95634765625" Y="-1.507562011719" />
                  <Point X="27.964078125" Y="-1.539182617188" />
                  <Point X="27.976451171875" Y="-1.567996826172" />
                  <Point X="28.010126953125" Y="-1.620376953125" />
                  <Point X="28.076931640625" Y="-1.724287353516" />
                  <Point X="28.0869375" Y="-1.737241943359" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.91275390625" Y="-2.376400634766" />
                  <Point X="29.213123046875" Y="-2.606882568359" />
                  <Point X="29.1248046875" Y="-2.749794677734" />
                  <Point X="29.068783203125" Y="-2.829392822266" />
                  <Point X="29.028982421875" Y="-2.885945068359" />
                  <Point X="28.250994140625" Y="-2.4367734375" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.6818046875" Y="-2.146745849609" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.384669921875" Y="-2.166841308594" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246549072266" />
                  <Point X="27.22142578125" Y="-2.267508300781" />
                  <Point X="27.204533203125" Y="-2.290438964844" />
                  <Point X="27.172869140625" Y="-2.350603759766" />
                  <Point X="27.110052734375" Y="-2.46995703125" />
                  <Point X="27.100228515625" Y="-2.499735107422" />
                  <Point X="27.095271484375" Y="-2.531908691406" />
                  <Point X="27.09567578125" Y="-2.563259521484" />
                  <Point X="27.108755859375" Y="-2.635681152344" />
                  <Point X="27.134703125" Y="-2.779349853516" />
                  <Point X="27.13898828125" Y="-2.795140136719" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.667267578125" Y="-3.718855957031" />
                  <Point X="27.86128515625" Y="-4.054904541016" />
                  <Point X="27.78183984375" Y="-4.111650878906" />
                  <Point X="27.7192109375" Y="-4.152189941406" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="27.10196875" Y="-3.381810058594" />
                  <Point X="26.758548828125" Y="-2.934254882812" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.650498046875" Y="-2.854635742188" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.338982421875" Y="-2.748305175781" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.04427734375" Y="-2.845615722656" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968861083984" />
                  <Point X="25.88725" Y="-2.996687744141" />
                  <Point X="25.875625" Y="-3.025809082031" />
                  <Point X="25.85758984375" Y="-3.108787109375" />
                  <Point X="25.821810546875" Y="-3.273396972656" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.96581640625" Y="-4.432656738281" />
                  <Point X="26.02206640625" Y="-4.859915527344" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94157421875" Y="-4.75263671875" />
                  <Point X="23.858755859375" Y="-4.731328125" />
                  <Point X="23.863064453125" Y="-4.698591796875" />
                  <Point X="23.8792265625" Y="-4.575837402344" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030761719" />
                  <Point X="23.8782421875" Y="-4.509323730469" />
                  <Point X="23.876666015625" Y="-4.4976875" />
                  <Point X="23.85637890625" Y="-4.395705566406" />
                  <Point X="23.81613671875" Y="-4.193395996094" />
                  <Point X="23.811873046875" Y="-4.178466308594" />
                  <Point X="23.800970703125" Y="-4.149500488281" />
                  <Point X="23.79433203125" Y="-4.135464355469" />
                  <Point X="23.778259765625" Y="-4.107626953125" />
                  <Point X="23.76942578125" Y="-4.094861572266" />
                  <Point X="23.74979296875" Y="-4.070938232422" />
                  <Point X="23.738994140625" Y="-4.059780273438" />
                  <Point X="23.660818359375" Y="-3.991221191406" />
                  <Point X="23.505734375" Y="-3.855215576172" />
                  <Point X="23.49326171875" Y="-3.845965820312" />
                  <Point X="23.466978515625" Y="-3.829621826172" />
                  <Point X="23.45316796875" Y="-3.822527587891" />
                  <Point X="23.423470703125" Y="-3.810226318359" />
                  <Point X="23.4086875" Y="-3.805476318359" />
                  <Point X="23.378544921875" Y="-3.798447998047" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.259427734375" Y="-3.789368896484" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.808270019531" />
                  <Point X="22.9045625" Y="-3.8158125" />
                  <Point X="22.818107421875" Y="-3.873580810547" />
                  <Point X="22.64659765625" Y="-3.988180664062" />
                  <Point X="22.64031640625" Y="-3.992759277344" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629150391" />
                  <Point X="22.496798828125" Y="-4.162478027344" />
                  <Point X="22.252408203125" Y="-4.011158447266" />
                  <Point X="22.124873046875" Y="-3.912960693359" />
                  <Point X="22.01913671875" Y="-3.831546875" />
                  <Point X="22.409015625" Y="-3.156256103516" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.7100859375" />
                  <Point X="22.67605078125" Y="-2.681121582031" />
                  <Point X="22.680314453125" Y="-2.666190917969" />
                  <Point X="22.6865859375" Y="-2.634665527344" />
                  <Point X="22.688361328125" Y="-2.619241455078" />
                  <Point X="22.689375" Y="-2.588309082031" />
                  <Point X="22.6853359375" Y="-2.557625" />
                  <Point X="22.676353515625" Y="-2.528008544922" />
                  <Point X="22.6706484375" Y="-2.513567871094" />
                  <Point X="22.65643359375" Y="-2.484739501953" />
                  <Point X="22.648451171875" Y="-2.471421630859" />
                  <Point X="22.630423828125" Y="-2.446261474609" />
                  <Point X="22.62037890625" Y="-2.434419189453" />
                  <Point X="22.60303125" Y="-2.417070556641" />
                  <Point X="22.591189453125" Y="-2.407024169922" />
                  <Point X="22.56603125" Y="-2.388995605469" />
                  <Point X="22.55271484375" Y="-2.381013427734" />
                  <Point X="22.523884765625" Y="-2.366795166016" />
                  <Point X="22.5094453125" Y="-2.361088378906" />
                  <Point X="22.479828125" Y="-2.352103271484" />
                  <Point X="22.449140625" Y="-2.348062744141" />
                  <Point X="22.418205078125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.4560546875" Y="-2.873865722656" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="20.995982421875" Y="-2.740588623047" />
                  <Point X="20.904548828125" Y="-2.587270996094" />
                  <Point X="20.818734375" Y="-2.443373535156" />
                  <Point X="21.51534375" Y="-1.908845825195" />
                  <Point X="21.951876953125" Y="-1.573881835938" />
                  <Point X="21.96083984375" Y="-1.566067260742" />
                  <Point X="21.983728515625" Y="-1.543435791016" />
                  <Point X="21.997771484375" Y="-1.525785522461" />
                  <Point X="22.01736328125" Y="-1.494049438477" />
                  <Point X="22.0255859375" Y="-1.477209716797" />
                  <Point X="22.034880859375" Y="-1.452172973633" />
                  <Point X="22.040689453125" Y="-1.433688842773" />
                  <Point X="22.0458125" Y="-1.413911132812" />
                  <Point X="22.048046875" Y="-1.402404052734" />
                  <Point X="22.052162109375" Y="-1.370925415039" />
                  <Point X="22.05291796875" Y="-1.355716064453" />
                  <Point X="22.052220703125" Y="-1.332843383789" />
                  <Point X="22.048771484375" Y="-1.310213134766" />
                  <Point X="22.042623046875" Y="-1.288171020508" />
                  <Point X="22.038884765625" Y="-1.27733972168" />
                  <Point X="22.02687890625" Y="-1.248356201172" />
                  <Point X="22.0211171875" Y="-1.236753417969" />
                  <Point X="22.008064453125" Y="-1.214434448242" />
                  <Point X="21.99208984375" Y="-1.19409387207" />
                  <Point X="21.9735" Y="-1.17612109375" />
                  <Point X="21.96359375" Y="-1.167772949219" />
                  <Point X="21.942216796875" Y="-1.152119628906" />
                  <Point X="21.926337890625" Y="-1.141670166016" />
                  <Point X="21.90873046875" Y="-1.131307373047" />
                  <Point X="21.89456640625" Y="-1.124480957031" />
                  <Point X="21.8653046875" Y="-1.113257202148" />
                  <Point X="21.85020703125" Y="-1.108859741211" />
                  <Point X="21.8183203125" Y="-1.102378417969" />
                  <Point X="21.802703125" Y="-1.100532348633" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="20.673458984375" Y="-1.242672485352" />
                  <Point X="20.339080078125" Y="-1.286694335938" />
                  <Point X="20.259236328125" Y="-0.97411138916" />
                  <Point X="20.235048828125" Y="-0.804979003906" />
                  <Point X="20.213548828125" Y="-0.654654541016" />
                  <Point X="20.994435546875" Y="-0.445416503906" />
                  <Point X="21.491712890625" Y="-0.312171203613" />
                  <Point X="21.502564453125" Y="-0.308550750732" />
                  <Point X="21.52375390625" Y="-0.300031097412" />
                  <Point X="21.534091796875" Y="-0.295132049561" />
                  <Point X="21.558958984375" Y="-0.281457122803" />
                  <Point X="21.575732421875" Y="-0.271063781738" />
                  <Point X="21.594185546875" Y="-0.258257537842" />
                  <Point X="21.603515625" Y="-0.25087588501" />
                  <Point X="21.62747265625" Y="-0.229350234985" />
                  <Point X="21.642365234375" Y="-0.212355819702" />
                  <Point X="21.6634765625" Y="-0.181522674561" />
                  <Point X="21.672501953125" Y="-0.165057632446" />
                  <Point X="21.68302734375" Y="-0.140329315186" />
                  <Point X="21.6896640625" Y="-0.122235229492" />
                  <Point X="21.695814453125" Y="-0.102418014526" />
                  <Point X="21.69842578125" Y="-0.091935493469" />
                  <Point X="21.703869140625" Y="-0.063191390991" />
                  <Point X="21.705482421875" Y="-0.042579929352" />
                  <Point X="21.704423828125" Y="-0.008334697723" />
                  <Point X="21.702337890625" Y="0.008739985466" />
                  <Point X="21.6969921875" Y="0.033550041199" />
                  <Point X="21.69271484375" Y="0.049847133636" />
                  <Point X="21.686564453125" Y="0.069664505005" />
                  <Point X="21.68233203125" Y="0.080788032532" />
                  <Point X="21.669013671875" Y="0.110114738464" />
                  <Point X="21.65719921875" Y="0.129545837402" />
                  <Point X="21.633927734375" Y="0.159147644043" />
                  <Point X="21.620810546875" Y="0.172784225464" />
                  <Point X="21.599689453125" Y="0.190757644653" />
                  <Point X="21.584892578125" Y="0.202145721436" />
                  <Point X="21.56644140625" Y="0.214952102661" />
                  <Point X="21.559517578125" Y="0.219327407837" />
                  <Point X="21.53438671875" Y="0.23283366394" />
                  <Point X="21.503435546875" Y="0.245635437012" />
                  <Point X="21.491712890625" Y="0.249611236572" />
                  <Point X="20.505224609375" Y="0.513939941406" />
                  <Point X="20.2145546875" Y="0.591825073242" />
                  <Point X="20.26866796875" Y="0.957520690918" />
                  <Point X="20.3173671875" Y="1.137237426758" />
                  <Point X="20.3664140625" Y="1.318237182617" />
                  <Point X="20.865474609375" Y="1.252534667969" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.20470324707" />
                  <Point X="21.28485546875" Y="1.206589477539" />
                  <Point X="21.295109375" Y="1.208053466797" />
                  <Point X="21.3153984375" Y="1.212089233398" />
                  <Point X="21.325431640625" Y="1.214660522461" />
                  <Point X="21.346017578125" Y="1.221151245117" />
                  <Point X="21.386857421875" Y="1.234028076172" />
                  <Point X="21.396552734375" Y="1.237677246094" />
                  <Point X="21.415484375" Y="1.24600793457" />
                  <Point X="21.42472265625" Y="1.250689819336" />
                  <Point X="21.443466796875" Y="1.261511962891" />
                  <Point X="21.452142578125" Y="1.267172119141" />
                  <Point X="21.46882421875" Y="1.279403808594" />
                  <Point X="21.48407421875" Y="1.29337878418" />
                  <Point X="21.497712890625" Y="1.308931152344" />
                  <Point X="21.504107421875" Y="1.317080200195" />
                  <Point X="21.516521484375" Y="1.334810302734" />
                  <Point X="21.521990234375" Y="1.343605834961" />
                  <Point X="21.531939453125" Y="1.361741699219" />
                  <Point X="21.536419921875" Y="1.371081665039" />
                  <Point X="21.5446796875" Y="1.391024169922" />
                  <Point X="21.56106640625" Y="1.4305859375" />
                  <Point X="21.5645" Y="1.440348876953" />
                  <Point X="21.570287109375" Y="1.460198974609" />
                  <Point X="21.572640625" Y="1.470286376953" />
                  <Point X="21.576400390625" Y="1.49160168457" />
                  <Point X="21.577640625" Y="1.501889160156" />
                  <Point X="21.578994140625" Y="1.522535522461" />
                  <Point X="21.578091796875" Y="1.543205932617" />
                  <Point X="21.574943359375" Y="1.563656005859" />
                  <Point X="21.572810546875" Y="1.573794555664" />
                  <Point X="21.56720703125" Y="1.594701171875" />
                  <Point X="21.563986328125" Y="1.604539916992" />
                  <Point X="21.5564921875" Y="1.623810058594" />
                  <Point X="21.55221875" Y="1.633241699219" />
                  <Point X="21.542251953125" Y="1.652388549805" />
                  <Point X="21.522478515625" Y="1.690371459961" />
                  <Point X="21.51719921875" Y="1.699288696289" />
                  <Point X="21.505705078125" Y="1.716489624023" />
                  <Point X="21.499490234375" Y="1.72477331543" />
                  <Point X="21.485578125" Y="1.741352783203" />
                  <Point X="21.478498046875" Y="1.748912841797" />
                  <Point X="21.463556640625" Y="1.763215698242" />
                  <Point X="21.4556953125" Y="1.769958618164" />
                  <Point X="20.88984375" Y="2.20415234375" />
                  <Point X="20.77238671875" Y="2.294280761719" />
                  <Point X="20.779720703125" Y="2.306845947266" />
                  <Point X="20.99771875" Y="2.680325439453" />
                  <Point X="21.12669921875" Y="2.846114013672" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.526916015625" Y="2.888796875" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.873599609375" Y="2.728649414062" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.02356640625" Y="2.734227294922" />
                  <Point X="22.04300390625" Y="2.741302001953" />
                  <Point X="22.0615546875" Y="2.750450439453" />
                  <Point X="22.070580078125" Y="2.755530761719" />
                  <Point X="22.088833984375" Y="2.767159667969" />
                  <Point X="22.09725390625" Y="2.773192626953" />
                  <Point X="22.113384765625" Y="2.786138916016" />
                  <Point X="22.121095703125" Y="2.793052246094" />
                  <Point X="22.141447265625" Y="2.813403320312" />
                  <Point X="22.1818203125" Y="2.853775634766" />
                  <Point X="22.188734375" Y="2.861488769531" />
                  <Point X="22.20168359375" Y="2.877623046875" />
                  <Point X="22.20771875" Y="2.886044189453" />
                  <Point X="22.21934765625" Y="2.904298828125" />
                  <Point X="22.2244296875" Y="2.913326171875" />
                  <Point X="22.233576171875" Y="2.931875" />
                  <Point X="22.240646484375" Y="2.951299316406" />
                  <Point X="22.245564453125" Y="2.971387939453" />
                  <Point X="22.2474765625" Y="2.981573730469" />
                  <Point X="22.250302734375" Y="3.003032958984" />
                  <Point X="22.251091796875" Y="3.013365478516" />
                  <Point X="22.25154296875" Y="3.034047607422" />
                  <Point X="22.251205078125" Y="3.044397216797" />
                  <Point X="22.248697265625" Y="3.073068847656" />
                  <Point X="22.243720703125" Y="3.129946777344" />
                  <Point X="22.242255859375" Y="3.140203369141" />
                  <Point X="22.23821875" Y="3.160498291016" />
                  <Point X="22.235646484375" Y="3.170536621094" />
                  <Point X="22.22913671875" Y="3.191178466797" />
                  <Point X="22.225486328125" Y="3.200874023438" />
                  <Point X="22.21715625" Y="3.219801269531" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="21.96173046875" Y="3.663337890625" />
                  <Point X="21.94061328125" Y="3.699915527344" />
                  <Point X="21.971966796875" Y="3.723954589844" />
                  <Point X="22.3516328125" Y="4.015040527344" />
                  <Point X="22.554779296875" Y="4.127902832031" />
                  <Point X="22.807474609375" Y="4.268295410156" />
                  <Point X="22.8144140625" Y="4.259251464844" />
                  <Point X="22.881435546875" Y="4.171908203125" />
                  <Point X="22.88818359375" Y="4.16404296875" />
                  <Point X="22.902490234375" Y="4.149099609375" />
                  <Point X="22.910048828125" Y="4.142020996094" />
                  <Point X="22.92662890625" Y="4.128109375" />
                  <Point X="22.934912109375" Y="4.121895507812" />
                  <Point X="22.952109375" Y="4.110404785156" />
                  <Point X="22.9610234375" Y="4.105127929688" />
                  <Point X="22.992935546875" Y="4.088516113281" />
                  <Point X="23.056240234375" Y="4.055561035156" />
                  <Point X="23.06567578125" Y="4.051286132812" />
                  <Point X="23.084953125" Y="4.043790039063" />
                  <Point X="23.094794921875" Y="4.040568847656" />
                  <Point X="23.115701171875" Y="4.034966796875" />
                  <Point X="23.1258359375" Y="4.032835449219" />
                  <Point X="23.146279296875" Y="4.029688476562" />
                  <Point X="23.1669453125" Y="4.028785888672" />
                  <Point X="23.187583984375" Y="4.030138427734" />
                  <Point X="23.197865234375" Y="4.031377929688" />
                  <Point X="23.2191796875" Y="4.035135742188" />
                  <Point X="23.22926953125" Y="4.037488037109" />
                  <Point X="23.2491328125" Y="4.043277587891" />
                  <Point X="23.25890625" Y="4.04671484375" />
                  <Point X="23.292142578125" Y="4.060482910156" />
                  <Point X="23.358080078125" Y="4.087794433594" />
                  <Point X="23.36741796875" Y="4.092273681641" />
                  <Point X="23.385548828125" Y="4.102220703125" />
                  <Point X="23.394341796875" Y="4.107688964844" />
                  <Point X="23.412072265625" Y="4.120103515625" />
                  <Point X="23.420220703125" Y="4.126497558594" />
                  <Point X="23.4357734375" Y="4.140136230469" />
                  <Point X="23.449751953125" Y="4.155391113281" />
                  <Point X="23.461982421875" Y="4.172072753906" />
                  <Point X="23.467638671875" Y="4.180744140625" />
                  <Point X="23.4784609375" Y="4.199488769531" />
                  <Point X="23.483140625" Y="4.208722167969" />
                  <Point X="23.49147265625" Y="4.227654785156" />
                  <Point X="23.495125" Y="4.237354003906" />
                  <Point X="23.505943359375" Y="4.271665039063" />
                  <Point X="23.527404296875" Y="4.33973046875" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370050292969" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401863769531" />
                  <Point X="23.53769921875" Y="4.412216796875" />
                  <Point X="23.537248046875" Y="4.4328984375" />
                  <Point X="23.536458984375" Y="4.443227050781" />
                  <Point X="23.520734375" Y="4.562655761719" />
                  <Point X="23.577333984375" Y="4.578523925781" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.31509765625" Y="4.745143066406" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.709568359375" Y="4.503434082031" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.3664609375" Y="4.741219726562" />
                  <Point X="25.378193359375" Y="4.785005859375" />
                  <Point X="25.39872265625" Y="4.782855957031" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.03163671875" Y="4.688718261719" />
                  <Point X="26.453591796875" Y="4.586844726562" />
                  <Point X="26.584265625" Y="4.53944921875" />
                  <Point X="26.858265625" Y="4.440067871094" />
                  <Point X="26.9865078125" Y="4.380092773438" />
                  <Point X="27.2504453125" Y="4.256658203125" />
                  <Point X="27.374404296875" Y="4.184439453125" />
                  <Point X="27.629435546875" Y="4.035857910156" />
                  <Point X="27.746287109375" Y="3.952759033203" />
                  <Point X="27.817783203125" Y="3.901915771484" />
                  <Point X="27.35408203125" Y="3.098763671875" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.06237890625" Y="2.593112060547" />
                  <Point X="27.0531875" Y="2.573447265625" />
                  <Point X="27.044185546875" Y="2.549568359375" />
                  <Point X="27.041302734375" Y="2.540598632812" />
                  <Point X="27.034107421875" Y="2.513690917969" />
                  <Point X="27.01983203125" Y="2.460312255859" />
                  <Point X="27.0179140625" Y="2.451469970703" />
                  <Point X="27.013646484375" Y="2.420223388672" />
                  <Point X="27.012755859375" Y="2.383237060547" />
                  <Point X="27.013412109375" Y="2.369573242188" />
                  <Point X="27.01621875" Y="2.346305908203" />
                  <Point X="27.021783203125" Y="2.3001484375" />
                  <Point X="27.02380078125" Y="2.289028808594" />
                  <Point X="27.029142578125" Y="2.267112792969" />
                  <Point X="27.032466796875" Y="2.25631640625" />
                  <Point X="27.040732421875" Y="2.2342265625" />
                  <Point X="27.045314453125" Y="2.223895507812" />
                  <Point X="27.0556796875" Y="2.20384375" />
                  <Point X="27.061462890625" Y="2.194123046875" />
                  <Point X="27.075861328125" Y="2.172905517578" />
                  <Point X="27.104421875" Y="2.130814453125" />
                  <Point X="27.10981640625" Y="2.123624267578" />
                  <Point X="27.130462890625" Y="2.100124511719" />
                  <Point X="27.15759375" Y="2.075389648438" />
                  <Point X="27.16825390625" Y="2.066983154297" />
                  <Point X="27.189470703125" Y="2.0525859375" />
                  <Point X="27.2315625" Y="2.024025756836" />
                  <Point X="27.24128125" Y="2.018244628906" />
                  <Point X="27.261330078125" Y="2.007880615234" />
                  <Point X="27.27166015625" Y="2.003297485352" />
                  <Point X="27.293748046875" Y="1.995031860352" />
                  <Point X="27.304548828125" Y="1.991707763672" />
                  <Point X="27.326470703125" Y="1.986364990234" />
                  <Point X="27.337591796875" Y="1.984346557617" />
                  <Point X="27.360859375" Y="1.981540893555" />
                  <Point X="27.407015625" Y="1.975974975586" />
                  <Point X="27.416044921875" Y="1.975320678711" />
                  <Point X="27.44757421875" Y="1.975497314453" />
                  <Point X="27.4843125" Y="1.979822509766" />
                  <Point X="27.49774609375" Y="1.982395629883" />
                  <Point X="27.524654296875" Y="1.989591186523" />
                  <Point X="27.578033203125" Y="2.003865356445" />
                  <Point X="27.583994140625" Y="2.005670654297" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.628251953125" Y="2.600729492188" />
                  <Point X="28.940404296875" Y="2.780950683594" />
                  <Point X="29.043962890625" Y="2.637028076172" />
                  <Point X="29.10909765625" Y="2.529392333984" />
                  <Point X="29.136884765625" Y="2.483471923828" />
                  <Point X="28.553146484375" Y="2.035552856445" />
                  <Point X="28.172953125" Y="1.743819702148" />
                  <Point X="28.168138671875" Y="1.739867797852" />
                  <Point X="28.15212890625" Y="1.725225219727" />
                  <Point X="28.134671875" Y="1.706607788086" />
                  <Point X="28.12857421875" Y="1.69942175293" />
                  <Point X="28.109208984375" Y="1.674158325195" />
                  <Point X="28.07079296875" Y="1.624040527344" />
                  <Point X="28.0656328125" Y="1.61659765625" />
                  <Point X="28.04973828125" Y="1.589367797852" />
                  <Point X="28.034765625" Y="1.555549072266" />
                  <Point X="28.03014453125" Y="1.542680175781" />
                  <Point X="28.0229296875" Y="1.516885864258" />
                  <Point X="28.008619140625" Y="1.465715698242" />
                  <Point X="28.0062265625" Y="1.454670166016" />
                  <Point X="28.002771484375" Y="1.432368652344" />
                  <Point X="28.001708984375" Y="1.421112670898" />
                  <Point X="28.000892578125" Y="1.397541625977" />
                  <Point X="28.001173828125" Y="1.386236694336" />
                  <Point X="28.003078125" Y="1.363750244141" />
                  <Point X="28.004701171875" Y="1.352568725586" />
                  <Point X="28.010623046875" Y="1.323869140625" />
                  <Point X="28.02237109375" Y="1.266935913086" />
                  <Point X="28.02459765625" Y="1.258239013672" />
                  <Point X="28.03468359375" Y="1.228610961914" />
                  <Point X="28.05028515625" Y="1.195374633789" />
                  <Point X="28.056916015625" Y="1.183529785156" />
                  <Point X="28.073021484375" Y="1.159048828125" />
                  <Point X="28.10497265625" Y="1.11048425293" />
                  <Point X="28.11173828125" Y="1.101424438477" />
                  <Point X="28.126294921875" Y="1.084176757813" />
                  <Point X="28.1340859375" Y="1.075988769531" />
                  <Point X="28.151330078125" Y="1.059899291992" />
                  <Point X="28.16003515625" Y="1.052695922852" />
                  <Point X="28.178244140625" Y="1.039370239258" />
                  <Point X="28.187748046875" Y="1.033247924805" />
                  <Point X="28.21108984375" Y="1.020109191895" />
                  <Point X="28.257390625" Y="0.994045349121" />
                  <Point X="28.265482421875" Y="0.989986328125" />
                  <Point X="28.2946796875" Y="0.978083557129" />
                  <Point X="28.33027734375" Y="0.968020935059" />
                  <Point X="28.343671875" Y="0.965257751465" />
                  <Point X="28.37523046875" Y="0.961086791992" />
                  <Point X="28.437833984375" Y="0.952812927246" />
                  <Point X="28.444033203125" Y="0.952199707031" />
                  <Point X="28.46571875" Y="0.951222839355" />
                  <Point X="28.49121875" Y="0.952032348633" />
                  <Point X="28.50060546875" Y="0.952797180176" />
                  <Point X="29.44314453125" Y="1.076885131836" />
                  <Point X="29.704703125" Y="1.111319824219" />
                  <Point X="29.752689453125" Y="0.91420916748" />
                  <Point X="29.77321484375" Y="0.782378356934" />
                  <Point X="29.783873046875" Y="0.713921447754" />
                  <Point X="29.127703125" Y="0.538100952148" />
                  <Point X="28.6919921875" Y="0.421352813721" />
                  <Point X="28.68603125" Y="0.419544219971" />
                  <Point X="28.665623046875" Y="0.412135955811" />
                  <Point X="28.642380859375" Y="0.40161807251" />
                  <Point X="28.634005859375" Y="0.397316192627" />
                  <Point X="28.603001953125" Y="0.379395263672" />
                  <Point X="28.54149609375" Y="0.343843780518" />
                  <Point X="28.5338828125" Y="0.338945007324" />
                  <Point X="28.50877734375" Y="0.31987121582" />
                  <Point X="28.481998046875" Y="0.294353271484" />
                  <Point X="28.47280078125" Y="0.284229675293" />
                  <Point X="28.454197265625" Y="0.260525360107" />
                  <Point X="28.417294921875" Y="0.213501831055" />
                  <Point X="28.410857421875" Y="0.204211868286" />
                  <Point X="28.399130859375" Y="0.184928955078" />
                  <Point X="28.393841796875" Y="0.174936141968" />
                  <Point X="28.384068359375" Y="0.153472625732" />
                  <Point X="28.380005859375" Y="0.142927383423" />
                  <Point X="28.37316015625" Y="0.121428352356" />
                  <Point X="28.370376953125" Y="0.110474250793" />
                  <Point X="28.36417578125" Y="0.078095726013" />
                  <Point X="28.351875" Y="0.013863758087" />
                  <Point X="28.350603515625" Y="0.004966600418" />
                  <Point X="28.3485859375" Y="-0.026261882782" />
                  <Point X="28.35028125" Y="-0.062940513611" />
                  <Point X="28.351875" Y="-0.076423873901" />
                  <Point X="28.358076171875" Y="-0.10880255127" />
                  <Point X="28.370376953125" Y="-0.173034225464" />
                  <Point X="28.37316015625" Y="-0.183990097046" />
                  <Point X="28.380005859375" Y="-0.205487808228" />
                  <Point X="28.384068359375" Y="-0.21602947998" />
                  <Point X="28.393841796875" Y="-0.237494033813" />
                  <Point X="28.399126953125" Y="-0.247480300903" />
                  <Point X="28.410849609375" Y="-0.266761138916" />
                  <Point X="28.417287109375" Y="-0.276055847168" />
                  <Point X="28.435888671875" Y="-0.299759857178" />
                  <Point X="28.47279296875" Y="-0.346783538818" />
                  <Point X="28.47871875" Y="-0.353633850098" />
                  <Point X="28.50114453125" Y="-0.375809875488" />
                  <Point X="28.5301796875" Y="-0.398726623535" />
                  <Point X="28.54149609375" Y="-0.406403900146" />
                  <Point X="28.5725" Y="-0.424324981689" />
                  <Point X="28.634005859375" Y="-0.459876312256" />
                  <Point X="28.639494140625" Y="-0.46281338501" />
                  <Point X="28.65915625" Y="-0.472015960693" />
                  <Point X="28.68302734375" Y="-0.481027557373" />
                  <Point X="28.6919921875" Y="-0.483912780762" />
                  <Point X="29.55634375" Y="-0.715515319824" />
                  <Point X="29.78487890625" Y="-0.776750976562" />
                  <Point X="29.76161328125" Y="-0.931061645508" />
                  <Point X="29.735314453125" Y="-1.046308105469" />
                  <Point X="29.7278046875" Y="-1.079219848633" />
                  <Point X="28.942083984375" Y="-0.975778015137" />
                  <Point X="28.436783203125" Y="-0.909253662109" />
                  <Point X="28.428625" Y="-0.908535705566" />
                  <Point X="28.40009765625" Y="-0.908042419434" />
                  <Point X="28.36672265625" Y="-0.910840942383" />
                  <Point X="28.354482421875" Y="-0.912676147461" />
                  <Point X="28.293630859375" Y="-0.925902404785" />
                  <Point X="28.17291796875" Y="-0.952140014648" />
                  <Point X="28.157875" Y="-0.956742248535" />
                  <Point X="28.128755859375" Y="-0.968366027832" />
                  <Point X="28.1146796875" Y="-0.975387695312" />
                  <Point X="28.0868515625" Y="-0.992280334473" />
                  <Point X="28.074125" Y="-1.001529602051" />
                  <Point X="28.050375" Y="-1.022" />
                  <Point X="28.0393515625" Y="-1.033221313477" />
                  <Point X="28.0025703125" Y="-1.077456420898" />
                  <Point X="27.929607421875" Y="-1.165209228516" />
                  <Point X="27.921326171875" Y="-1.176849365234" />
                  <Point X="27.906603515625" Y="-1.201236816406" />
                  <Point X="27.900162109375" Y="-1.213984130859" />
                  <Point X="27.8888203125" Y="-1.241369384766" />
                  <Point X="27.88436328125" Y="-1.254934692383" />
                  <Point X="27.877533203125" Y="-1.282580322266" />
                  <Point X="27.87516015625" Y="-1.296660644531" />
                  <Point X="27.869888671875" Y="-1.353947143555" />
                  <Point X="27.859431640625" Y="-1.467591064453" />
                  <Point X="27.859291015625" Y="-1.483315185547" />
                  <Point X="27.861607421875" Y="-1.514580932617" />
                  <Point X="27.864064453125" Y="-1.530122802734" />
                  <Point X="27.871794921875" Y="-1.561743530273" />
                  <Point X="27.87678515625" Y="-1.576666748047" />
                  <Point X="27.889158203125" Y="-1.605480834961" />
                  <Point X="27.896541015625" Y="-1.619371826172" />
                  <Point X="27.930216796875" Y="-1.671752075195" />
                  <Point X="27.997021484375" Y="-1.775662231445" />
                  <Point X="28.00174609375" Y="-1.782358520508" />
                  <Point X="28.019798828125" Y="-1.804454101562" />
                  <Point X="28.0434921875" Y="-1.828121337891" />
                  <Point X="28.052798828125" Y="-1.836277709961" />
                  <Point X="28.854921875" Y="-2.451769287109" />
                  <Point X="29.087171875" Y="-2.629981445312" />
                  <Point X="29.045486328125" Y="-2.697434570312" />
                  <Point X="29.001275390625" Y="-2.760251953125" />
                  <Point X="28.298494140625" Y="-2.354500976562" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.698689453125" Y="-2.053258300781" />
                  <Point X="27.555021484375" Y="-2.027312011719" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.03513671875" />
                  <Point X="27.415068359375" Y="-2.044960571289" />
                  <Point X="27.40058984375" Y="-2.051109130859" />
                  <Point X="27.34042578125" Y="-2.0827734375" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.208970703125" Y="-2.153169433594" />
                  <Point X="27.1860390625" Y="-2.1700625" />
                  <Point X="27.175208984375" Y="-2.179374267578" />
                  <Point X="27.15425" Y="-2.200333496094" />
                  <Point X="27.144939453125" Y="-2.211162353516" />
                  <Point X="27.128046875" Y="-2.234093017578" />
                  <Point X="27.12046484375" Y="-2.246194824219" />
                  <Point X="27.08880078125" Y="-2.306359619141" />
                  <Point X="27.025984375" Y="-2.425712890625" />
                  <Point X="27.0198359375" Y="-2.440193115234" />
                  <Point X="27.01001171875" Y="-2.469971191406" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517442626953" />
                  <Point X="27.000279296875" Y="-2.533133789062" />
                  <Point X="27.00068359375" Y="-2.564484619141" />
                  <Point X="27.0021875" Y="-2.580144287109" />
                  <Point X="27.015267578125" Y="-2.652565917969" />
                  <Point X="27.04121484375" Y="-2.796234619141" />
                  <Point X="27.04301953125" Y="-2.804230957031" />
                  <Point X="27.05123828125" Y="-2.831540771484" />
                  <Point X="27.064072265625" Y="-2.862479248047" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.58499609375" Y="-3.766355957031" />
                  <Point X="27.735896484375" Y="-4.027722412109" />
                  <Point X="27.7237578125" Y="-4.036082763672" />
                  <Point X="27.177337890625" Y="-3.323977539062" />
                  <Point X="26.83391796875" Y="-2.876422363281" />
                  <Point X="26.82865234375" Y="-2.870141113281" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.701873046875" Y="-2.774725585938" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.33027734375" Y="-2.653704833984" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.983541015625" Y="-2.772567871094" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883087646484" />
                  <Point X="25.83218359375" Y="-2.906837158203" />
                  <Point X="25.822935546875" Y="-2.919562744141" />
                  <Point X="25.80604296875" Y="-2.947389404297" />
                  <Point X="25.79901953125" Y="-2.961467041016" />
                  <Point X="25.78739453125" Y="-2.990588378906" />
                  <Point X="25.78279296875" Y="-3.005632080078" />
                  <Point X="25.7647578125" Y="-3.088610107422" />
                  <Point X="25.728978515625" Y="-3.253219970703" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520263672" />
                  <Point X="25.83308984375" Y="-4.152328125" />
                  <Point X="25.7893203125" Y="-3.988976074219" />
                  <Point X="25.655068359375" Y="-3.487936767578" />
                  <Point X="25.652607421875" Y="-3.480121582031" />
                  <Point X="25.642146484375" Y="-3.453578613281" />
                  <Point X="25.6267890625" Y="-3.423815429688" />
                  <Point X="25.62041015625" Y="-3.413209716797" />
                  <Point X="25.565537109375" Y="-3.334148681641" />
                  <Point X="25.456681640625" Y="-3.177309326172" />
                  <Point X="25.446671875" Y="-3.165172119141" />
                  <Point X="25.424787109375" Y="-3.142716308594" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104936767578" />
                  <Point X="25.3452421875" Y="-3.090829345703" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.245744140625" Y="-3.058585205078" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.850103515625" Y="-3.032658691406" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.6670703125" Y="-3.090829589844" />
                  <Point X="24.6390703125" Y="-3.104937255859" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.5875234375" Y="-3.142718017578" />
                  <Point X="24.565638671875" Y="-3.165175048828" />
                  <Point X="24.555630859375" Y="-3.177311767578" />
                  <Point X="24.500759765625" Y="-3.256372558594" />
                  <Point X="24.391904296875" Y="-3.413212158203" />
                  <Point X="24.38753125" Y="-3.420131347656" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.09091796875" Y="-4.48188671875" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.047816470188" Y="-3.781872037086" />
                  <Point X="22.555005265205" Y="-4.086621810424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.095323669361" Y="-3.699587069454" />
                  <Point X="22.61680519503" Y="-4.012924781779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.142830868535" Y="-3.617302101822" />
                  <Point X="22.700327059176" Y="-3.952279608036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.868880552083" Y="-4.654417383151" />
                  <Point X="24.020307174568" Y="-4.745403677275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.103687418375" Y="-2.882091552365" />
                  <Point X="21.269373804397" Y="-2.981645976835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.190338067709" Y="-3.53501713419" />
                  <Point X="22.787660701373" Y="-3.893924781613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.880332192304" Y="-4.550468050048" />
                  <Point X="24.045886369771" Y="-4.649943035604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.964342796081" Y="-2.68753468362" />
                  <Point X="21.363440304567" Y="-2.927336659622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.237845266882" Y="-3.452732166559" />
                  <Point X="22.874994071147" Y="-3.835569791502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.863052933254" Y="-4.429255451023" />
                  <Point X="24.071465564975" Y="-4.554482393932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.861337825478" Y="-2.514812880485" />
                  <Point X="21.457506805422" Y="-2.873027342819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.285352466056" Y="-3.370447198927" />
                  <Point X="22.976922071523" Y="-3.785984140169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.838014159443" Y="-4.303380465156" />
                  <Point X="24.097044637226" Y="-4.459021678384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.866234824589" Y="-2.406925121667" />
                  <Point X="21.551573349895" Y="-2.818718052226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.33285966523" Y="-3.288162231295" />
                  <Point X="23.155692058865" Y="-3.782569812691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.811083555228" Y="-4.176368752901" />
                  <Point X="24.122623319108" Y="-4.363560728278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.947239878126" Y="-2.344767695544" />
                  <Point X="21.645639894367" Y="-2.764408761632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.380366864403" Y="-3.205877263664" />
                  <Point X="23.362728568311" Y="-3.796139725183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.602977339926" Y="-3.940495750816" />
                  <Point X="24.14820200099" Y="-4.268099778172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.028244931664" Y="-2.282610269422" />
                  <Point X="21.73970643884" Y="-2.710099471038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.427874079752" Y="-3.123592305751" />
                  <Point X="24.173780682872" Y="-4.172638828066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.109249985202" Y="-2.220452843299" />
                  <Point X="21.833772983313" Y="-2.655790180444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.475381319671" Y="-3.041307362602" />
                  <Point X="24.199359364754" Y="-4.07717787796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.190255038739" Y="-2.158295417177" />
                  <Point X="21.927839527785" Y="-2.60148088985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.522888559591" Y="-2.959022419453" />
                  <Point X="24.224938046636" Y="-3.981716927854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.271260092277" Y="-2.096137991054" />
                  <Point X="22.021906072258" Y="-2.547171599256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.570395799511" Y="-2.876737476304" />
                  <Point X="24.250516728518" Y="-3.886255977748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.352265145815" Y="-2.033980564932" />
                  <Point X="22.11597261673" Y="-2.492862308662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.61790303943" Y="-2.794452533155" />
                  <Point X="24.2760954104" Y="-3.790795027642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.433270199352" Y="-1.97182313881" />
                  <Point X="22.210039161203" Y="-2.438553018068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.664439608748" Y="-2.711584352267" />
                  <Point X="24.301674092282" Y="-3.695334077536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.313987953046" Y="-1.188460342692" />
                  <Point X="20.452603113466" Y="-1.271748733788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.51427525289" Y="-1.909665712687" />
                  <Point X="22.304105705676" Y="-2.384243727475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.688493531877" Y="-2.615207234672" />
                  <Point X="24.327252774164" Y="-3.59987312743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.280545664016" Y="-1.057536015468" />
                  <Point X="20.603904374214" Y="-1.251829530245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.595280313764" Y="-1.847508290973" />
                  <Point X="22.429416876799" Y="-2.348708102592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.656695651851" Y="-2.485270968061" />
                  <Point X="24.352831456046" Y="-3.504412177325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.252945430144" Y="-0.930121949123" />
                  <Point X="20.755205653812" Y="-1.231910338029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.676285374736" Y="-1.785350869317" />
                  <Point X="24.390142039179" Y="-3.416000464666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.235605708632" Y="-0.808873020586" />
                  <Point X="20.90650694945" Y="-1.211991155451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.757290435708" Y="-1.723193447662" />
                  <Point X="24.444304135435" Y="-3.337714162615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.218264101866" Y="-0.687622959275" />
                  <Point X="21.057808245088" Y="-1.192071972872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.83829549668" Y="-1.661036026006" />
                  <Point X="24.49858830609" Y="-3.259501210263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.804035010322" Y="-4.043892725075" />
                  <Point X="25.820083523254" Y="-4.05353564449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.306428727977" Y="-0.62976743836" />
                  <Point X="21.209109540727" Y="-1.172152790293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.919300557652" Y="-1.598878604351" />
                  <Point X="24.55287145505" Y="-3.181287644014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.768639883023" Y="-3.91179501424" />
                  <Point X="25.8042390251" Y="-3.933185136786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.4339942315" Y="-0.595586353038" />
                  <Point X="21.360410836365" Y="-1.152233607715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.992704566209" Y="-1.532154009636" />
                  <Point X="24.624878636176" Y="-3.113723750705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.733244694724" Y="-3.779697266753" />
                  <Point X="25.788394526947" Y="-3.812834629082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.561559735024" Y="-0.561405267717" />
                  <Point X="21.511712132003" Y="-1.132314425136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.036336574124" Y="-1.447540592186" />
                  <Point X="24.734255271396" Y="-3.068613690715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.697849506425" Y="-3.647599519266" />
                  <Point X="25.772550028793" Y="-3.692484121377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.689125238548" Y="-0.527224182395" />
                  <Point X="21.663013427641" Y="-1.112395242557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.052637166703" Y="-1.346504803598" />
                  <Point X="24.855883139588" Y="-3.030864914152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.662454318127" Y="-3.515501771779" />
                  <Point X="25.756705530639" Y="-3.572133613673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.816690742071" Y="-0.493043097073" />
                  <Point X="21.837172330915" Y="-1.106210296253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.999823188417" Y="-1.203940791176" />
                  <Point X="24.985928893563" Y="-2.998174113652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.580168161539" Y="-3.355229088059" />
                  <Point X="25.740861032486" Y="-3.451783105969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.944256245595" Y="-0.458862011752" />
                  <Point X="25.297822454115" Y="-3.07474849878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.444353461299" Y="-3.162793210464" />
                  <Point X="25.725296850254" Y="-3.331601029063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.071821714705" Y="-0.424680905752" />
                  <Point X="25.734791566819" Y="-3.226475857601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.199387161501" Y="-0.390499786345" />
                  <Point X="25.756098660939" Y="-3.128448278628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.326952608296" Y="-0.356318666938" />
                  <Point X="25.777405261934" Y="-3.030420403355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.454518055092" Y="-0.32213754753" />
                  <Point X="25.810754392958" Y="-2.939628410131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.56501826963" Y="-0.277702602105" />
                  <Point X="25.871798525206" Y="-2.865477252486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.641412319617" Y="-3.928768492435" />
                  <Point X="27.69860631294" Y="-3.963134110668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.221721984261" Y="0.640261408195" />
                  <Point X="20.372984449514" Y="0.549373749687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.641796142352" Y="-0.213005229501" />
                  <Point X="25.94917658695" Y="-2.801140509829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.483615303027" Y="-3.723124306631" />
                  <Point X="27.600629314198" Y="-3.793433417818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.23678283642" Y="0.742042107979" />
                  <Point X="20.705896813921" Y="0.460169993063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.687048733989" Y="-0.129365556989" />
                  <Point X="26.026554335041" Y="-2.736803578711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.325818286438" Y="-3.517480120827" />
                  <Point X="27.502652629724" Y="-3.623732913799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.25184368858" Y="0.843822807762" />
                  <Point X="21.038808671509" Y="0.370966540967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.705073951762" Y="-0.029366027763" />
                  <Point X="26.119517696067" Y="-2.681831428628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.168021407235" Y="-3.311836017573" />
                  <Point X="27.404676004915" Y="-3.454032445631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.26690454074" Y="0.945603507546" />
                  <Point X="21.371720529096" Y="0.281763088871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.673375251346" Y="0.100510645727" />
                  <Point X="26.266870342458" Y="-2.659539658219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.01022671758" Y="-3.106193229933" />
                  <Point X="27.306699380106" Y="-3.284331977463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.291471234102" Y="1.041672521701" />
                  <Point X="26.430160789381" Y="-2.646824284502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.852432027926" Y="-2.900550442292" />
                  <Point X="27.208722755297" Y="-3.114631509294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.317298537523" Y="1.136984084915" />
                  <Point X="27.110746130489" Y="-2.944931041126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.343125834847" Y="1.232295651792" />
                  <Point X="27.040422239973" Y="-2.791846012003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.38963544353" Y="1.315180032264" />
                  <Point X="27.017969206023" Y="-2.667524695389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.625842241075" Y="1.284082842408" />
                  <Point X="27.000447363192" Y="-2.546166337324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.862049038619" Y="1.252985652552" />
                  <Point X="27.017965624908" Y="-2.445862198167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.098256033997" Y="1.221888343826" />
                  <Point X="27.060317182609" Y="-2.360479408609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.30309116636" Y="1.209641152132" />
                  <Point X="27.104633000027" Y="-2.27627686536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.421741516268" Y="1.249179002173" />
                  <Point X="27.157419460958" Y="-2.197163998216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.500608424257" Y="1.312621155754" />
                  <Point X="27.239710257879" Y="-2.135779124659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.546651905749" Y="1.395785613698" />
                  <Point X="27.33803789858" Y="-2.084030158982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.575944168998" Y="1.489015219005" />
                  <Point X="27.442438598107" Y="-2.035930255192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.562722451026" Y="1.607789801385" />
                  <Point X="27.637268598016" Y="-2.042165756806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.377783642116" Y="1.829742421325" />
                  <Point X="29.00589443687" Y="-2.753688952822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.776730012516" Y="2.30172205001" />
                  <Point X="29.059360392416" Y="-2.674984367232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.824623679063" Y="2.383774804617" />
                  <Point X="28.792121952582" Y="-2.403581140111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.872517420905" Y="2.465827513982" />
                  <Point X="28.126345354774" Y="-1.892712028683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.920411162748" Y="2.547880223346" />
                  <Point X="27.922133400057" Y="-1.659178934423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.96830490459" Y="2.629932932711" />
                  <Point X="27.861405831143" Y="-1.511859957038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.020390356203" Y="2.709467008747" />
                  <Point X="27.865338874479" Y="-1.403392995156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.079147614861" Y="2.784992258673" />
                  <Point X="27.875002902752" Y="-1.29836955643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.137905019135" Y="2.860517421104" />
                  <Point X="27.904552938238" Y="-1.205294836309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.196663041337" Y="2.936042212247" />
                  <Point X="27.96048532435" Y="-1.128072231717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.255421063539" Y="3.011567003389" />
                  <Point X="28.021936473877" Y="-1.054165634727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.917054665896" Y="2.724847600242" />
                  <Point X="28.095080712413" Y="-0.987284954436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.060094894097" Y="2.749730532915" />
                  <Point X="28.208258131934" Y="-0.944458636054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.140370296902" Y="2.812326377429" />
                  <Point X="28.343711719448" Y="-0.915017189761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.206038227909" Y="2.883699276489" />
                  <Point X="28.541520746957" Y="-0.923042671744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.245434642148" Y="2.970857695378" />
                  <Point X="28.777727839993" Y="-0.954140039149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.248082336338" Y="3.080096972943" />
                  <Point X="29.013934808775" Y="-0.985237331895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.223314257651" Y="3.20580930877" />
                  <Point X="29.250141493328" Y="-1.016334453858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.12927350628" Y="3.373144865588" />
                  <Point X="29.48634817788" Y="-1.047431575821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.031296887956" Y="3.54284532986" />
                  <Point X="28.444297128896" Y="-0.310473964735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.765911638722" Y="-0.503719458197" />
                  <Point X="29.722554862433" Y="-1.078528697785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.94664454688" Y="3.704539760657" />
                  <Point X="28.366523553328" Y="-0.15291271324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.098823803176" Y="-0.592923094677" />
                  <Point X="29.749551283368" Y="-0.983919611244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.027687023268" Y="3.766674700862" />
                  <Point X="28.348825670328" Y="-0.031448579569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.43173596763" Y="-0.682126731157" />
                  <Point X="29.768625817307" Y="-0.884550574778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.108729884782" Y="3.82880940966" />
                  <Point X="28.362812583495" Y="0.070977407862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.764647540161" Y="-0.771330011974" />
                  <Point X="29.783947774727" Y="-0.782926762863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.189772746296" Y="3.890944118459" />
                  <Point X="28.389632669495" Y="0.165692447121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.27081560781" Y="3.953078827257" />
                  <Point X="28.442039675121" Y="0.245033314015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.351899665286" Y="4.015188783025" />
                  <Point X="28.506133583438" Y="0.317351981324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.447737730039" Y="4.068433636846" />
                  <Point X="28.595013429607" Y="0.374777754671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.543575794792" Y="4.121678490667" />
                  <Point X="28.698880232667" Y="0.423198455823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.639413170485" Y="4.174923758519" />
                  <Point X="28.826445835692" Y="0.457379481358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.735250454962" Y="4.228169081177" />
                  <Point X="28.954011438718" Y="0.491560506893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.234508182331" Y="4.039014946791" />
                  <Point X="28.102730907314" Y="1.113891626694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.351911638208" Y="0.964168738479" />
                  <Point X="29.081577041743" Y="0.525741532428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.346062311704" Y="4.082816636297" />
                  <Point X="28.020941066075" Y="1.273866094066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.545461250201" Y="0.95870257154" />
                  <Point X="29.209142547687" Y="0.559922616296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.435507294383" Y="4.139902841371" />
                  <Point X="28.00091275803" Y="1.396730488372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.696762458666" Y="0.978621806497" />
                  <Point X="29.336707998646" Y="0.594103733201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.487800997105" Y="4.219311787518" />
                  <Point X="28.017529471299" Y="1.497576332487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.848063667132" Y="0.998541041454" />
                  <Point X="29.464273449604" Y="0.628284850107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.518554740062" Y="4.311663247222" />
                  <Point X="27.074357681154" Y="2.175121290897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.40548577788" Y="1.976159457722" />
                  <Point X="28.049628369885" Y="1.589119541147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.999364875598" Y="1.018460276412" />
                  <Point X="29.591838900563" Y="0.662465967013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.537659958198" Y="4.411013846762" />
                  <Point X="27.019522674195" Y="2.318899659859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.554362441296" Y="1.997535506318" />
                  <Point X="28.103956595509" Y="1.667306022604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.150666084063" Y="1.038379511369" />
                  <Point X="29.719404351522" Y="0.696647083919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.5251154346" Y="4.529381529712" />
                  <Point X="27.015292033451" Y="2.432271858011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.662702024361" Y="2.043268690108" />
                  <Point X="28.16794329624" Y="1.739689106728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.301967292529" Y="1.058298746326" />
                  <Point X="29.774457306446" Y="0.77439810408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.611731215701" Y="4.588167690597" />
                  <Point X="27.038278779806" Y="2.529290200102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.756768498805" Y="2.097578022779" />
                  <Point X="28.248738976682" Y="1.801972336899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.453268513747" Y="1.078217973621" />
                  <Point X="29.755420742421" Y="0.896666598459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.737499492274" Y="4.623428658817" />
                  <Point X="27.076148269016" Y="2.617366088108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.850834973249" Y="2.151887355451" />
                  <Point X="28.329743993434" Y="1.864129785124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.604569912788" Y="1.098137094069" />
                  <Point X="29.725619408651" Y="1.025403219052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.863267768848" Y="4.658689627036" />
                  <Point X="27.123655344401" Y="2.69965113012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.944901447692" Y="2.206196688122" />
                  <Point X="28.410749010186" Y="1.926287233349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.989036045421" Y="4.693950595256" />
                  <Point X="24.798710596583" Y="4.207449043234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.002311495211" Y="4.08511328125" />
                  <Point X="27.171162419785" Y="2.781936172132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.038967922136" Y="2.260506020794" />
                  <Point X="28.491754026938" Y="1.988444681575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.125265643936" Y="4.722925767098" />
                  <Point X="24.751593725182" Y="4.346589888486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.129222366772" Y="4.119687709138" />
                  <Point X="27.218669495169" Y="2.864221214143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.13303439658" Y="2.314815353465" />
                  <Point X="28.572759040792" Y="2.050602131542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.279647308756" Y="4.740994077143" />
                  <Point X="24.716199096212" Y="4.478687299894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.200596739596" Y="4.187631832035" />
                  <Point X="27.266176570554" Y="2.946506256155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.227100871023" Y="2.369124686137" />
                  <Point X="28.65376404557" Y="2.112759586962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.434029315412" Y="4.759062181793" />
                  <Point X="24.680803564429" Y="4.610785253768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.241274361032" Y="4.274020423974" />
                  <Point X="27.313683645938" Y="3.028791298167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.321167345467" Y="2.423434018808" />
                  <Point X="28.734769050349" Y="2.174917042382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.58841142396" Y="4.777130225219" />
                  <Point X="24.645407824527" Y="4.742883332692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.266853203664" Y="4.369481277492" />
                  <Point X="27.361190757711" Y="3.111076318314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.41523381991" Y="2.47774335148" />
                  <Point X="28.815774055127" Y="2.237074497802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.292432046296" Y="4.46494213101" />
                  <Point X="27.408698076278" Y="3.193361214208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.509300294354" Y="2.532052684151" />
                  <Point X="28.896779059905" Y="2.299231953222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.318010888927" Y="4.560402984527" />
                  <Point X="27.456205394844" Y="3.275646110101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.603366768798" Y="2.586362016823" />
                  <Point X="28.977784064684" Y="2.361389408642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.343589731559" Y="4.655863838045" />
                  <Point X="27.50371271341" Y="3.357931005994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.697433270008" Y="2.640671333411" />
                  <Point X="29.058789069462" Y="2.423546864062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.369168550048" Y="4.75132470607" />
                  <Point X="27.551220031976" Y="3.440215901888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.791499780846" Y="2.694980644215" />
                  <Point X="29.133100023985" Y="2.489726510663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.522763651577" Y="4.76986563102" />
                  <Point X="27.598727350543" Y="3.522500797781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.885566291685" Y="2.749289955018" />
                  <Point X="29.022303109632" Y="2.667130185942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.746150982787" Y="4.746471153642" />
                  <Point X="27.646234669109" Y="3.604785693675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.023417898817" Y="4.690702555576" />
                  <Point X="27.693741987675" Y="3.687070589568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.331769068345" Y="4.616256653711" />
                  <Point X="27.741249306242" Y="3.769355485461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.735101739253" Y="4.484740108131" />
                  <Point X="27.788756624808" Y="3.851640381355" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.60579296875" Y="-4.038151611328" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.409447265625" Y="-3.442483154297" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.189423828125" Y="-3.240046142578" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.906423828125" Y="-3.214120117188" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.656849609375" Y="-3.364704589844" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.2744453125" Y="-4.5310625" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.0975625" Y="-4.976460449219" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.80211328125" Y="-4.912942871094" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.674689453125" Y="-4.673793457031" />
                  <Point X="23.6908515625" Y="-4.5510390625" />
                  <Point X="23.690318359375" Y="-4.5347578125" />
                  <Point X="23.67003125" Y="-4.432775878906" />
                  <Point X="23.6297890625" Y="-4.230466308594" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.535541015625" Y="-4.134069824219" />
                  <Point X="23.38045703125" Y="-3.998064208984" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.247001953125" Y="-3.978962158203" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.923666015625" Y="-4.031559326172" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.590744140625" Y="-4.352155761719" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.43411328125" Y="-4.347138183594" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="22.008958984375" Y="-4.063505859375" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.24447265625" Y="-3.061256103516" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597594238281" />
                  <Point X="22.4860234375" Y="-2.568765869141" />
                  <Point X="22.46867578125" Y="-2.551417236328" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.5510546875" Y="-3.038410644531" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.067369140625" Y="-3.148083496094" />
                  <Point X="20.838302734375" Y="-2.847136962891" />
                  <Point X="20.741365234375" Y="-2.684588134766" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.3996796875" Y="-1.758109008789" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.84746484375" Y="-1.41108203125" />
                  <Point X="21.856759765625" Y="-1.386045288086" />
                  <Point X="21.8618828125" Y="-1.366267578125" />
                  <Point X="21.863349609375" Y="-1.350052124023" />
                  <Point X="21.85134375" Y="-1.321068603516" />
                  <Point X="21.829966796875" Y="-1.305415283203" />
                  <Point X="21.812359375" Y="-1.295052490234" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.698259765625" Y="-1.43104699707" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.16219921875" Y="-1.361939697266" />
                  <Point X="20.072607421875" Y="-1.011188293457" />
                  <Point X="20.046962890625" Y="-0.831878295898" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="20.945259765625" Y="-0.261890563965" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.467404296875" Y="-0.114970451355" />
                  <Point X="21.485857421875" Y="-0.102164085388" />
                  <Point X="21.497677734375" Y="-0.09064591217" />
                  <Point X="21.508203125" Y="-0.065917671204" />
                  <Point X="21.514353515625" Y="-0.046100421906" />
                  <Point X="21.516599609375" Y="-0.031280040741" />
                  <Point X="21.51125390625" Y="-0.006470028877" />
                  <Point X="21.505103515625" Y="0.013347373962" />
                  <Point X="21.497677734375" Y="0.028084819794" />
                  <Point X="21.476556640625" Y="0.046058151245" />
                  <Point X="21.45810546875" Y="0.058864665985" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.456048828125" Y="0.330414093018" />
                  <Point X="20.001814453125" Y="0.45212600708" />
                  <Point X="20.024615234375" Y="0.606212890625" />
                  <Point X="20.08235546875" Y="0.996414611816" />
                  <Point X="20.13398046875" Y="1.186930786133" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.890275390625" Y="1.440909179688" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.2888828125" Y="1.402357421875" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056152344" />
                  <Point X="21.360880859375" Y="1.443786132813" />
                  <Point X="21.369140625" Y="1.463728759766" />
                  <Point X="21.38552734375" Y="1.503290405273" />
                  <Point X="21.389287109375" Y="1.524605834961" />
                  <Point X="21.38368359375" Y="1.545512451172" />
                  <Point X="21.373716796875" Y="1.564659301758" />
                  <Point X="21.353943359375" Y="1.602642211914" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.7741796875" Y="2.053415283203" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.615626953125" Y="2.402625244141" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="20.97673828125" Y="2.962781982422" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.621916015625" Y="3.053341796875" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.890158203125" Y="2.917926269531" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404296875" />
                  <Point X="22.007099609375" Y="2.947755371094" />
                  <Point X="22.04747265625" Y="2.988127685547" />
                  <Point X="22.0591015625" Y="3.006382324219" />
                  <Point X="22.061927734375" Y="3.027841552734" />
                  <Point X="22.059419921875" Y="3.056513183594" />
                  <Point X="22.054443359375" Y="3.113391113281" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.7971875" Y="3.568337890625" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="21.856361328125" Y="3.874737792969" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.462505859375" Y="4.293991699219" />
                  <Point X="22.858451171875" Y="4.513971679688" />
                  <Point X="22.96515234375" Y="4.374916015625" />
                  <Point X="23.032173828125" Y="4.287572753906" />
                  <Point X="23.04875390625" Y="4.273661132813" />
                  <Point X="23.080666015625" Y="4.257049316406" />
                  <Point X="23.143970703125" Y="4.224094238281" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.18619140625" Y="4.22225" />
                  <Point X="23.219427734375" Y="4.236018066406" />
                  <Point X="23.285365234375" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.294488769531" />
                  <Point X="23.324736328125" Y="4.328799804688" />
                  <Point X="23.346197265625" Y="4.396865234375" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.319576171875" Y="4.634958984375" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.526041015625" Y="4.761469238281" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.29301171875" Y="4.933854980469" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.893095703125" Y="4.552609863281" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.18293359375" Y="4.790395507812" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.41851171875" Y="4.971822753906" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.0762265625" Y="4.873411621094" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.64905078125" Y="4.718063476562" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="27.06699609375" Y="4.552201171875" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.470048828125" Y="4.348610351562" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.856400390625" Y="4.10759765625" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.518625" Y="3.003763671875" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491515625" />
                  <Point X="27.217658203125" Y="2.464607910156" />
                  <Point X="27.2033828125" Y="2.411229248047" />
                  <Point X="27.202044921875" Y="2.392327148438" />
                  <Point X="27.2048515625" Y="2.369059814453" />
                  <Point X="27.210416015625" Y="2.32290234375" />
                  <Point X="27.218681640625" Y="2.3008125" />
                  <Point X="27.233080078125" Y="2.279594970703" />
                  <Point X="27.261640625" Y="2.23750390625" />
                  <Point X="27.27494140625" Y="2.224203369141" />
                  <Point X="27.296158203125" Y="2.209806152344" />
                  <Point X="27.33825" Y="2.181245849609" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.38360546875" Y="2.170174560547" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.475572265625" Y="2.173141845703" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.533251953125" Y="2.765274414062" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.046900390625" Y="2.958256103516" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.27165234375" Y="2.627759521484" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.668810546875" Y="1.884815673828" />
                  <Point X="28.2886171875" Y="1.593082641602" />
                  <Point X="28.27937109375" Y="1.583832885742" />
                  <Point X="28.260005859375" Y="1.558569213867" />
                  <Point X="28.22158984375" Y="1.508451538086" />
                  <Point X="28.21312109375" Y="1.491500244141" />
                  <Point X="28.20590625" Y="1.465705932617" />
                  <Point X="28.191595703125" Y="1.414535766602" />
                  <Point X="28.190779296875" Y="1.39096472168" />
                  <Point X="28.196701171875" Y="1.362265258789" />
                  <Point X="28.20844921875" Y="1.30533190918" />
                  <Point X="28.215646484375" Y="1.287955078125" />
                  <Point X="28.231751953125" Y="1.263474243164" />
                  <Point X="28.263703125" Y="1.214909667969" />
                  <Point X="28.280947265625" Y="1.19882019043" />
                  <Point X="28.3042890625" Y="1.185681518555" />
                  <Point X="28.35058984375" Y="1.159617675781" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.400125" Y="1.149448852539" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.41834375" Y="1.265259521484" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.8723203125" Y="1.226061279297" />
                  <Point X="29.939193359375" Y="0.951366943359" />
                  <Point X="29.960953125" Y="0.811608337402" />
                  <Point X="29.997859375" Y="0.57455645752" />
                  <Point X="29.17687890625" Y="0.354575042725" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819351196" />
                  <Point X="28.698083984375" Y="0.214898376465" />
                  <Point X="28.636578125" Y="0.179346984863" />
                  <Point X="28.622265625" Y="0.166926086426" />
                  <Point X="28.603662109375" Y="0.143221954346" />
                  <Point X="28.566759765625" Y="0.096198410034" />
                  <Point X="28.556986328125" Y="0.074734802246" />
                  <Point X="28.55078515625" Y="0.042356250763" />
                  <Point X="28.538484375" Y="-0.021875602722" />
                  <Point X="28.538484375" Y="-0.040684474945" />
                  <Point X="28.544685546875" Y="-0.073063026428" />
                  <Point X="28.556986328125" Y="-0.137294876099" />
                  <Point X="28.566759765625" Y="-0.158759552002" />
                  <Point X="28.585361328125" Y="-0.182463531494" />
                  <Point X="28.622265625" Y="-0.229487228394" />
                  <Point X="28.636578125" Y="-0.241907058716" />
                  <Point X="28.66758203125" Y="-0.259828033447" />
                  <Point X="28.729087890625" Y="-0.295379425049" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.60551953125" Y="-0.531989440918" />
                  <Point X="29.998068359375" Y="-0.637172546387" />
                  <Point X="29.98576953125" Y="-0.718754577637" />
                  <Point X="29.948431640625" Y="-0.966412536621" />
                  <Point X="29.920552734375" Y="-1.088579589844" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.917283203125" Y="-1.16415234375" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.333986328125" Y="-1.111567504883" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.1854453125" Y="-1.154697631836" />
                  <Point X="28.1486640625" Y="-1.198932739258" />
                  <Point X="28.075701171875" Y="-1.286685546875" />
                  <Point X="28.064359375" Y="-1.314070800781" />
                  <Point X="28.059087890625" Y="-1.371357421875" />
                  <Point X="28.048630859375" Y="-1.485001220703" />
                  <Point X="28.056361328125" Y="-1.516621826172" />
                  <Point X="28.090037109375" Y="-1.569001953125" />
                  <Point X="28.156841796875" Y="-1.672912353516" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.9705859375" Y="-2.301032226562" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.309380859375" Y="-2.631833740234" />
                  <Point X="29.2041328125" Y="-2.802140625" />
                  <Point X="29.146470703125" Y="-2.884069824219" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.203494140625" Y="-2.519045898438" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.664919921875" Y="-2.240233398438" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.4289140625" Y="-2.250909179688" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334683105469" />
                  <Point X="27.2569375" Y="-2.394847900391" />
                  <Point X="27.19412109375" Y="-2.514201171875" />
                  <Point X="27.1891640625" Y="-2.546374755859" />
                  <Point X="27.202244140625" Y="-2.618796386719" />
                  <Point X="27.22819140625" Y="-2.762465087891" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.7495390625" Y="-3.671355957031" />
                  <Point X="27.98667578125" Y="-4.082087646484" />
                  <Point X="27.96018359375" Y="-4.101009277344" />
                  <Point X="27.83529296875" Y="-4.190216308594" />
                  <Point X="27.77083203125" Y="-4.231940917969" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="27.026599609375" Y="-3.439642578125" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.599123046875" Y="-2.934545898438" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.3476875" Y="-2.842905517578" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.105013671875" Y="-2.918663574219" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045986083984" />
                  <Point X="25.950421875" Y="-3.128964111328" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310719970703" />
                  <Point X="26.06000390625" Y="-4.420256835938" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.1125" Y="-4.937348144531" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.9347890625" Y="-4.97406640625" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#150" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.06476507889" Y="4.59683105088" Z="0.85" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.85" />
                  <Point X="-0.718957857503" Y="5.014795969364" Z="0.85" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.85" />
                  <Point X="-1.493614076544" Y="4.840892876226" Z="0.85" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.85" />
                  <Point X="-1.736152903915" Y="4.659713024202" Z="0.85" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.85" />
                  <Point X="-1.729106894088" Y="4.375115128102" Z="0.85" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.85" />
                  <Point X="-1.805861344817" Y="4.313492202684" Z="0.85" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.85" />
                  <Point X="-1.90240392213" Y="4.332679244185" Z="0.85" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.85" />
                  <Point X="-2.001335805635" Y="4.436634312762" Z="0.85" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.85" />
                  <Point X="-2.567935144678" Y="4.368979430084" Z="0.85" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.85" />
                  <Point X="-3.180216675305" Y="3.945697858686" Z="0.85" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.85" />
                  <Point X="-3.252270888989" Y="3.574618046943" Z="0.85" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.85" />
                  <Point X="-2.996548468626" Y="3.083435051646" Z="0.85" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.85" />
                  <Point X="-3.034412322705" Y="3.014391245181" Z="0.85" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.85" />
                  <Point X="-3.111641413961" Y="2.999016230547" Z="0.85" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.85" />
                  <Point X="-3.359241301354" Y="3.127923059757" Z="0.85" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.85" />
                  <Point X="-4.068881907746" Y="3.024764295656" Z="0.85" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.85" />
                  <Point X="-4.433711762955" Y="2.459110406031" Z="0.85" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.85" />
                  <Point X="-4.262414396013" Y="2.045027664976" Z="0.85" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.85" />
                  <Point X="-3.676789617806" Y="1.572851166465" Z="0.85" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.85" />
                  <Point X="-3.68320941337" Y="1.514142709315" Z="0.85" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.85" />
                  <Point X="-3.732309357755" Y="1.48132349399" Z="0.85" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.85" />
                  <Point X="-4.109356951002" Y="1.521761514183" Z="0.85" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.85" />
                  <Point X="-4.920435814624" Y="1.231287993596" Z="0.85" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.85" />
                  <Point X="-5.031003053085" Y="0.644811818872" Z="0.85" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.85" />
                  <Point X="-4.563049190107" Y="0.313397820839" Z="0.85" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.85" />
                  <Point X="-3.558109115856" Y="0.03626255419" Z="0.85" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.85" />
                  <Point X="-3.542657213796" Y="0.009989670713" Z="0.85" />
                  <Point X="-3.539556741714" Y="0" Z="0.85" />
                  <Point X="-3.545707387339" Y="-0.019817280348" Z="0.85" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.85" />
                  <Point X="-3.567259479267" Y="-0.042613428943" Z="0.85" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.85" />
                  <Point X="-4.073838598666" Y="-0.182314235917" Z="0.85" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.85" />
                  <Point X="-5.008690923142" Y="-0.807677398767" Z="0.85" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.85" />
                  <Point X="-4.892402164627" Y="-1.343033623139" Z="0.85" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.85" />
                  <Point X="-4.301371817419" Y="-1.44933931572" Z="0.85" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.85" />
                  <Point X="-3.201551828604" Y="-1.317226056841" Z="0.85" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.85" />
                  <Point X="-3.197798673519" Y="-1.342227468917" Z="0.85" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.85" />
                  <Point X="-3.63691467826" Y="-1.687161380421" Z="0.85" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.85" />
                  <Point X="-4.307735456737" Y="-2.678917876593" Z="0.85" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.85" />
                  <Point X="-3.97857330768" Y="-3.147086560516" Z="0.85" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.85" />
                  <Point X="-3.430102257471" Y="-3.050431848384" Z="0.85" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.85" />
                  <Point X="-2.561305254696" Y="-2.567025214436" Z="0.85" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.85" />
                  <Point X="-2.804985164784" Y="-3.004976125021" Z="0.85" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.85" />
                  <Point X="-3.027701085864" Y="-4.071843033646" Z="0.85" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.85" />
                  <Point X="-2.598366605909" Y="-4.358368736094" Z="0.85" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.85" />
                  <Point X="-2.375745007594" Y="-4.351313927576" Z="0.85" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.85" />
                  <Point X="-2.05471227956" Y="-4.041852694068" Z="0.85" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.85" />
                  <Point X="-1.762424312759" Y="-3.997575303746" Z="0.85" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.85" />
                  <Point X="-1.503582350718" Y="-4.140381574765" Z="0.85" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.85" />
                  <Point X="-1.385164046266" Y="-4.411250175723" Z="0.85" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.85" />
                  <Point X="-1.38103943237" Y="-4.63598637878" Z="0.85" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.85" />
                  <Point X="-1.216503479886" Y="-4.93008558148" Z="0.85" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.85" />
                  <Point X="-0.918122552761" Y="-4.994264383855" Z="0.85" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.85" />
                  <Point X="-0.683414990336" Y="-4.512723562155" Z="0.85" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.85" />
                  <Point X="-0.30823171002" Y="-3.361933676173" Z="0.85" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.85" />
                  <Point X="-0.084912199497" Y="-3.23059298161" Z="0.85" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.85" />
                  <Point X="0.168446879864" Y="-3.256519063678" Z="0.85" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.85" />
                  <Point X="0.362214149412" Y="-3.439712079595" Z="0.85" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.85" />
                  <Point X="0.551339870065" Y="-4.01981246766" Z="0.85" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.85" />
                  <Point X="0.93756929113" Y="-4.991981276846" Z="0.85" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.85" />
                  <Point X="1.117047549347" Y="-4.954908449519" Z="0.85" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.85" />
                  <Point X="1.103419060024" Y="-4.382450251299" Z="0.85" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.85" />
                  <Point X="0.993124408871" Y="-3.108303953805" Z="0.85" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.85" />
                  <Point X="1.130822980883" Y="-2.925830182279" Z="0.85" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.85" />
                  <Point X="1.346112455361" Y="-2.861415078711" Z="0.85" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.85" />
                  <Point X="1.565926497834" Y="-2.945324051681" Z="0.85" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.85" />
                  <Point X="1.980775247744" Y="-3.438800596198" Z="0.85" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.85" />
                  <Point X="2.791844125008" Y="-4.242635228706" Z="0.85" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.85" />
                  <Point X="2.98308996308" Y="-4.110416290426" Z="0.85" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.85" />
                  <Point X="2.786682304073" Y="-3.615075882917" Z="0.85" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.85" />
                  <Point X="2.245290435516" Y="-2.57862995786" Z="0.85" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.85" />
                  <Point X="2.29502594067" Y="-2.386854872922" Z="0.85" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.85" />
                  <Point X="2.446043505451" Y="-2.263875369297" Z="0.85" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.85" />
                  <Point X="2.649876845653" Y="-2.258157573975" Z="0.85" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.85" />
                  <Point X="3.17233771419" Y="-2.531067196201" Z="0.85" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.85" />
                  <Point X="4.181203378329" Y="-2.881566972815" Z="0.85" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.85" />
                  <Point X="4.345757404207" Y="-2.626838861257" Z="0.85" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.85" />
                  <Point X="3.994866778036" Y="-2.230084468654" Z="0.85" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.85" />
                  <Point X="3.12593805044" Y="-1.510682593318" Z="0.85" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.85" />
                  <Point X="3.102719967264" Y="-1.344658770871" Z="0.85" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.85" />
                  <Point X="3.180955277946" Y="-1.199619406464" Z="0.85" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.85" />
                  <Point X="3.338449359837" Y="-1.129146532404" Z="0.85" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.85" />
                  <Point X="3.904601299162" Y="-1.18244461594" Z="0.85" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.85" />
                  <Point X="4.963141487137" Y="-1.068423666151" Z="0.85" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.85" />
                  <Point X="5.029053671714" Y="-0.69492855952" Z="0.85" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.85" />
                  <Point X="4.612304821916" Y="-0.452413079377" Z="0.85" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.85" />
                  <Point X="3.686447035985" Y="-0.185259260811" Z="0.85" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.85" />
                  <Point X="3.618539316485" Y="-0.120314605928" Z="0.85" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.85" />
                  <Point X="3.587635590974" Y="-0.032378601655" Z="0.85" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.85" />
                  <Point X="3.59373585945" Y="0.064231929569" Z="0.85" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.85" />
                  <Point X="3.636840121915" Y="0.143634132647" Z="0.85" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.85" />
                  <Point X="3.716948378367" Y="0.202889583431" Z="0.85" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.85" />
                  <Point X="4.183662995391" Y="0.337558853392" Z="0.85" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.85" />
                  <Point X="5.004199732881" Y="0.850580359781" Z="0.85" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.85" />
                  <Point X="4.914744911502" Y="1.26916795898" Z="0.85" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.85" />
                  <Point X="4.405661490894" Y="1.346111876508" Z="0.85" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.85" />
                  <Point X="3.400518202178" Y="1.230297860237" Z="0.85" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.85" />
                  <Point X="3.322658789508" Y="1.260532498886" Z="0.85" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.85" />
                  <Point X="3.267367277928" Y="1.322235563184" Z="0.85" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.85" />
                  <Point X="3.239513709731" Y="1.403649742164" Z="0.85" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.85" />
                  <Point X="3.247902326301" Y="1.483519384148" Z="0.85" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.85" />
                  <Point X="3.293532713641" Y="1.559431351253" Z="0.85" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.85" />
                  <Point X="3.693092009895" Y="1.876428093179" Z="0.85" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.85" />
                  <Point X="4.308272287284" Y="2.684925720953" Z="0.85" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.85" />
                  <Point X="4.081329672193" Y="3.01873929263" Z="0.85" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.85" />
                  <Point X="3.502095373941" Y="2.839855749216" Z="0.85" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.85" />
                  <Point X="2.456499249959" Y="2.252724616044" Z="0.85" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.85" />
                  <Point X="2.383434232177" Y="2.251094905158" Z="0.85" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.85" />
                  <Point X="2.318075714535" Y="2.28246103712" Z="0.85" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.85" />
                  <Point X="2.268297563115" Y="2.338949145847" Z="0.85" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.85" />
                  <Point X="2.248334797601" Y="2.406324211655" Z="0.85" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.85" />
                  <Point X="2.259803326613" Y="2.482970325923" Z="0.85" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.85" />
                  <Point X="2.555769603643" Y="3.010043708014" Z="0.85" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.85" />
                  <Point X="2.879220615519" Y="4.179624908539" Z="0.85" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.85" />
                  <Point X="2.48906192472" Y="4.423092977446" Z="0.85" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.85" />
                  <Point X="2.082021263578" Y="4.628773126515" Z="0.85" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.85" />
                  <Point X="1.659943201527" Y="4.79634727604" Z="0.85" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.85" />
                  <Point X="1.08180389729" Y="4.953295411807" Z="0.85" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.85" />
                  <Point X="0.417562287376" Y="5.052830975473" Z="0.85" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.85" />
                  <Point X="0.128479482663" Y="4.834616455071" Z="0.85" />
                  <Point X="0" Y="4.355124473572" Z="0.85" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>