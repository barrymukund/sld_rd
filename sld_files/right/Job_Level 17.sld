<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#149" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1342" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004715820312" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442382812" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.689671875" Y="-3.984135009766" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467376708984" />
                  <Point X="25.49071484375" Y="-3.392959228516" />
                  <Point X="25.37863671875" Y="-3.231476318359" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.222572265625" Y="-3.150863525391" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097036132812" />
                  <Point X="24.883251953125" Y="-3.121841796875" />
                  <Point X="24.70981640625" Y="-3.175669433594" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231477539062" />
                  <Point X="24.58202734375" Y="-3.305894775391" />
                  <Point X="24.46994921875" Y="-3.467377929688" />
                  <Point X="24.4618125" Y="-3.481571533203" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.174796875" Y="-4.535903808594" />
                  <Point X="24.0834140625" Y="-4.876940917969" />
                  <Point X="23.920658203125" Y="-4.845349609375" />
                  <Point X="23.83160546875" Y="-4.822437011719" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.769826171875" Y="-4.678982421875" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.51622265625" />
                  <Point X="23.764396484375" Y="-4.42023046875" />
                  <Point X="23.722962890625" Y="-4.211931152344" />
                  <Point X="23.712060546875" Y="-4.182965332031" />
                  <Point X="23.69598828125" Y="-4.155127929687" />
                  <Point X="23.67635546875" Y="-4.131204589844" />
                  <Point X="23.602771484375" Y="-4.066672363281" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.25930859375" Y="-3.884565185547" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.87596484375" Y="-3.949177001953" />
                  <Point X="22.699376953125" Y="-4.067169921875" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.5198515625" Y="-4.288488769531" />
                  <Point X="22.4927734375" Y="-4.27172265625" />
                  <Point X="22.19828515625" Y="-4.089382568359" />
                  <Point X="22.0749765625" Y="-3.994439453125" />
                  <Point X="21.89527734375" Y="-3.856077392578" />
                  <Point X="22.3413984375" Y="-3.083374267578" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647655273438" />
                  <Point X="22.593412109375" Y="-2.616129882812" />
                  <Point X="22.59442578125" Y="-2.5851953125" />
                  <Point X="22.58544140625" Y="-2.555576904297" />
                  <Point X="22.571224609375" Y="-2.526748046875" />
                  <Point X="22.55319921875" Y="-2.501592041016" />
                  <Point X="22.5358515625" Y="-2.484243408203" />
                  <Point X="22.5106953125" Y="-2.466215576172" />
                  <Point X="22.4818671875" Y="-2.451997558594" />
                  <Point X="22.452248046875" Y="-2.44301171875" />
                  <Point X="22.4213125" Y="-2.444023925781" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.478171875" Y="-2.970792480469" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="21.149794921875" Y="-3.099521972656" />
                  <Point X="20.917138671875" Y="-2.793858398438" />
                  <Point X="20.828734375" Y="-2.645619384766" />
                  <Point X="20.693857421875" Y="-2.419450439453" />
                  <Point X="21.48315234375" Y="-1.813803466797" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.91693359375" Y="-1.475883056641" />
                  <Point X="21.936443359375" Y="-1.444362548828" />
                  <Point X="21.94558984375" Y="-1.419906738281" />
                  <Point X="21.948576171875" Y="-1.410444580078" />
                  <Point X="21.953849609375" Y="-1.390081298828" />
                  <Point X="21.957962890625" Y="-1.358597290039" />
                  <Point X="21.95726171875" Y="-1.335732177734" />
                  <Point X="21.9511171875" Y="-1.313697265625" />
                  <Point X="21.93911328125" Y="-1.284715209961" />
                  <Point X="21.926126953125" Y="-1.262480834961" />
                  <Point X="21.907646484375" Y="-1.244550537109" />
                  <Point X="21.886791015625" Y="-1.229204467773" />
                  <Point X="21.878673828125" Y="-1.223848999023" />
                  <Point X="21.860544921875" Y="-1.213179321289" />
                  <Point X="21.83128125" Y="-1.201955810547" />
                  <Point X="21.79939453125" Y="-1.195474609375" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="20.65381640625" Y="-1.341078369141" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.25691796875" Y="-1.348890869141" />
                  <Point X="20.165923828125" Y="-0.99265045166" />
                  <Point X="20.142533203125" Y="-0.8291171875" />
                  <Point X="20.107576171875" Y="-0.584698425293" />
                  <Point X="20.9990546875" Y="-0.345827331543" />
                  <Point X="21.467125" Y="-0.220408279419" />
                  <Point X="21.48810546875" Y="-0.212003646851" />
                  <Point X="21.5124296875" Y="-0.198706802368" />
                  <Point X="21.52102734375" Y="-0.193393234253" />
                  <Point X="21.540025390625" Y="-0.18020765686" />
                  <Point X="21.563984375" Y="-0.15867817688" />
                  <Point X="21.585" Y="-0.12805947876" />
                  <Point X="21.59534375" Y="-0.103918128967" />
                  <Point X="21.598751953125" Y="-0.094660438538" />
                  <Point X="21.605083984375" Y="-0.074256469727" />
                  <Point X="21.61052734375" Y="-0.045516029358" />
                  <Point X="21.60951171875" Y="-0.011471729279" />
                  <Point X="21.60434765625" Y="0.01275067234" />
                  <Point X="21.602166015625" Y="0.021099294662" />
                  <Point X="21.595833984375" Y="0.041503257751" />
                  <Point X="21.582517578125" Y="0.070830368042" />
                  <Point X="21.559421875" Y="0.100283920288" />
                  <Point X="21.53884765625" Y="0.117878700256" />
                  <Point X="21.53126953125" Y="0.123723999023" />
                  <Point X="21.512271484375" Y="0.136909576416" />
                  <Point X="21.498076171875" Y="0.145047348022" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="20.4514296875" Y="0.430003234863" />
                  <Point X="20.10818359375" Y="0.521975524902" />
                  <Point X="20.116869140625" Y="0.580667175293" />
                  <Point X="20.17551171875" Y="0.976967407227" />
                  <Point X="20.22259765625" Y="1.150726928711" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.898810546875" Y="1.343965576172" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228149414" />
                  <Point X="21.296865234375" Y="1.305263793945" />
                  <Point X="21.3162421875" Y="1.311373535156" />
                  <Point X="21.358291015625" Y="1.324631347656" />
                  <Point X="21.377224609375" Y="1.332962524414" />
                  <Point X="21.39596875" Y="1.343784790039" />
                  <Point X="21.412646484375" Y="1.356014282227" />
                  <Point X="21.426283203125" Y="1.371563232422" />
                  <Point X="21.43869921875" Y="1.389293457031" />
                  <Point X="21.448650390625" Y="1.407431640625" />
                  <Point X="21.45642578125" Y="1.42620300293" />
                  <Point X="21.473296875" Y="1.466936035156" />
                  <Point X="21.479087890625" Y="1.486805664062" />
                  <Point X="21.48284375" Y="1.50812097168" />
                  <Point X="21.484193359375" Y="1.528755249023" />
                  <Point X="21.481048828125" Y="1.549193115234" />
                  <Point X="21.475447265625" Y="1.570099609375" />
                  <Point X="21.467951171875" Y="1.589375366211" />
                  <Point X="21.4585703125" Y="1.607397583008" />
                  <Point X="21.4382109375" Y="1.646505249023" />
                  <Point X="21.426716796875" Y="1.663709228516" />
                  <Point X="21.4128046875" Y="1.680288330078" />
                  <Point X="21.39786328125" Y="1.694590332031" />
                  <Point X="20.8152578125" Y="2.141639648438" />
                  <Point X="20.648140625" Y="2.269873779297" />
                  <Point X="20.69098046875" Y="2.343270019531" />
                  <Point X="20.9188515625" Y="2.733665283203" />
                  <Point X="21.04356640625" Y="2.893969482422" />
                  <Point X="21.24949609375" Y="3.158662353516" />
                  <Point X="21.587275390625" Y="2.963645263672" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.8801953125" Y="2.823435302734" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.019537109375" Y="2.835653076172" />
                  <Point X="22.037791015625" Y="2.847281982422" />
                  <Point X="22.053921875" Y="2.860228271484" />
                  <Point X="22.073078125" Y="2.879384033203" />
                  <Point X="22.114646484375" Y="2.920951660156" />
                  <Point X="22.127595703125" Y="2.937085449219" />
                  <Point X="22.139224609375" Y="2.95533984375" />
                  <Point X="22.14837109375" Y="2.973888671875" />
                  <Point X="22.1532890625" Y="2.993977539062" />
                  <Point X="22.156115234375" Y="3.015436279297" />
                  <Point X="22.15656640625" Y="3.036121337891" />
                  <Point X="22.154205078125" Y="3.063108886719" />
                  <Point X="22.14908203125" Y="3.121670898438" />
                  <Point X="22.145044921875" Y="3.141962158203" />
                  <Point X="22.13853515625" Y="3.162604736328" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.87203515625" Y="3.628697021484" />
                  <Point X="21.81666796875" Y="3.724596435547" />
                  <Point X="21.9025078125" Y="3.790409667969" />
                  <Point X="22.29937890625" Y="4.094685791016" />
                  <Point X="22.495802734375" Y="4.203813964844" />
                  <Point X="22.83296484375" Y="4.391133789062" />
                  <Point X="22.893720703125" Y="4.311954101562" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.03492578125" Y="4.173758300781" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222548828125" Y="4.134482421875" />
                  <Point X="23.253833984375" Y="4.14744140625" />
                  <Point X="23.32172265625" Y="4.175562011719" />
                  <Point X="23.339857421875" Y="4.18551171875" />
                  <Point X="23.3575859375" Y="4.19792578125" />
                  <Point X="23.37313671875" Y="4.2115625" />
                  <Point X="23.3853671875" Y="4.2282421875" />
                  <Point X="23.396189453125" Y="4.246985839844" />
                  <Point X="23.404521484375" Y="4.265923828125" />
                  <Point X="23.414703125" Y="4.298219726563" />
                  <Point X="23.43680078125" Y="4.368300292969" />
                  <Point X="23.4408359375" Y="4.388583007812" />
                  <Point X="23.44272265625" Y="4.410145019531" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.415798828125" Y="4.6318984375" />
                  <Point X="23.53659765625" Y="4.665766113281" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.28848828125" Y="4.837677246094" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.805134765625" Y="4.513825195312" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.278501953125" Y="4.780004394531" />
                  <Point X="25.307421875" Y="4.887937011719" />
                  <Point X="25.39544140625" Y="4.878719238281" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.0410546875" Y="4.784174316406" />
                  <Point X="26.481029296875" Y="4.677950195312" />
                  <Point X="26.608248046875" Y="4.631807617188" />
                  <Point X="26.894646484375" Y="4.527928222656" />
                  <Point X="27.018646484375" Y="4.4699375" />
                  <Point X="27.294576171875" Y="4.340893554688" />
                  <Point X="27.414396484375" Y="4.271086914062" />
                  <Point X="27.680978515625" Y="4.115775878906" />
                  <Point X="27.793958984375" Y="4.0354296875" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.419392578125" Y="3.021885986328" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.539934814453" />
                  <Point X="27.133078125" Y="2.516059082031" />
                  <Point X="27.1263046875" Y="2.490731933594" />
                  <Point X="27.111607421875" Y="2.435772705078" />
                  <Point X="27.108619140625" Y="2.417935791016" />
                  <Point X="27.107728515625" Y="2.380953857422" />
                  <Point X="27.110369140625" Y="2.359053222656" />
                  <Point X="27.116099609375" Y="2.311529052734" />
                  <Point X="27.12144140625" Y="2.289606933594" />
                  <Point X="27.12970703125" Y="2.267518066406" />
                  <Point X="27.140072265625" Y="2.247467773438" />
                  <Point X="27.153625" Y="2.227496337891" />
                  <Point X="27.18303125" Y="2.184159179688" />
                  <Point X="27.194466796875" Y="2.170326660156" />
                  <Point X="27.221599609375" Y="2.145593261719" />
                  <Point X="27.2415703125" Y="2.132041748047" />
                  <Point X="27.284908203125" Y="2.102635742188" />
                  <Point X="27.304953125" Y="2.092272216797" />
                  <Point X="27.327041015625" Y="2.084006347656" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.370865234375" Y="2.076022705078" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.473205078125" Y="2.074170898438" />
                  <Point X="27.498533203125" Y="2.080943847656" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.610130859375" Y="2.699963134766" />
                  <Point X="28.967326171875" Y="2.906190185547" />
                  <Point X="29.12328125" Y="2.689449707031" />
                  <Point X="29.1862578125" Y="2.585379150391" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.5886484375" Y="1.943049194336" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.22142578125" Y="1.660242553711" />
                  <Point X="28.203974609375" Y="1.641628173828" />
                  <Point X="28.18574609375" Y="1.617848388672" />
                  <Point X="28.146193359375" Y="1.566246826172" />
                  <Point X="28.13660546875" Y="1.550909790039" />
                  <Point X="28.1216328125" Y="1.517090087891" />
                  <Point X="28.114841796875" Y="1.492810791016" />
                  <Point X="28.100107421875" Y="1.440125610352" />
                  <Point X="28.09665234375" Y="1.41782434082" />
                  <Point X="28.0958359375" Y="1.394253295898" />
                  <Point X="28.097740234375" Y="1.371766357422" />
                  <Point X="28.103314453125" Y="1.344752563477" />
                  <Point X="28.11541015625" Y="1.286133666992" />
                  <Point X="28.1206796875" Y="1.268979003906" />
                  <Point X="28.136283203125" Y="1.235741088867" />
                  <Point X="28.151443359375" Y="1.212698120117" />
                  <Point X="28.18433984375" Y="1.162695556641" />
                  <Point X="28.198892578125" Y="1.145450195312" />
                  <Point X="28.21613671875" Y="1.129360473633" />
                  <Point X="28.23434765625" Y="1.116034057617" />
                  <Point X="28.256318359375" Y="1.103667114258" />
                  <Point X="28.303990234375" Y="1.076831542969" />
                  <Point X="28.320521484375" Y="1.069501342773" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.38582421875" Y="1.055512817383" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.458650390625" Y="1.174746337891" />
                  <Point X="29.776837890625" Y="1.216636230469" />
                  <Point X="29.77801953125" Y="1.211785522461" />
                  <Point X="29.84594140625" Y="0.932788879395" />
                  <Point X="29.86578515625" Y="0.805326477051" />
                  <Point X="29.890865234375" Y="0.644238647461" />
                  <Point X="29.12669921875" Y="0.439480743408" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.7047890625" Y="0.325585296631" />
                  <Point X="28.681546875" Y="0.31506817627" />
                  <Point X="28.65236328125" Y="0.298199890137" />
                  <Point X="28.589037109375" Y="0.261595794678" />
                  <Point X="28.574314453125" Y="0.25109765625" />
                  <Point X="28.54753125" Y="0.225576385498" />
                  <Point X="28.530021484375" Y="0.203264572144" />
                  <Point X="28.492025390625" Y="0.154848602295" />
                  <Point X="28.48030078125" Y="0.13556803894" />
                  <Point X="28.47052734375" Y="0.11410458374" />
                  <Point X="28.463681640625" Y="0.092602264404" />
                  <Point X="28.457845703125" Y="0.062125495911" />
                  <Point X="28.4451796875" Y="-0.004008045197" />
                  <Point X="28.443484375" Y="-0.021874832153" />
                  <Point X="28.4451796875" Y="-0.058551937103" />
                  <Point X="28.451015625" Y="-0.089028709412" />
                  <Point X="28.463681640625" Y="-0.155162399292" />
                  <Point X="28.47052734375" Y="-0.176664718628" />
                  <Point X="28.48030078125" Y="-0.198128173828" />
                  <Point X="28.492025390625" Y="-0.217408737183" />
                  <Point X="28.50953515625" Y="-0.239720565796" />
                  <Point X="28.54753125" Y="-0.288136383057" />
                  <Point X="28.560001953125" Y="-0.301237548828" />
                  <Point X="28.589037109375" Y="-0.324155761719" />
                  <Point X="28.618220703125" Y="-0.341024230957" />
                  <Point X="28.681546875" Y="-0.37762802124" />
                  <Point X="28.692708984375" Y="-0.383137695312" />
                  <Point X="28.716580078125" Y="-0.392149810791" />
                  <Point X="29.6065234375" Y="-0.630609558105" />
                  <Point X="29.891474609375" Y="-0.706961975098" />
                  <Point X="29.855025390625" Y="-0.948722900391" />
                  <Point X="29.829595703125" Y="-1.060160400391" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="28.90000390625" Y="-1.066057739258" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.3173828125" Y="-1.017958251953" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.1639765625" Y="-1.056597045898" />
                  <Point X="28.136150390625" Y="-1.073489013672" />
                  <Point X="28.1123984375" Y="-1.093960693359" />
                  <Point X="28.077779296875" Y="-1.13559777832" />
                  <Point X="28.002654296875" Y="-1.225948608398" />
                  <Point X="27.987935546875" Y="-1.250329956055" />
                  <Point X="27.976591796875" Y="-1.277716064453" />
                  <Point X="27.969759765625" Y="-1.305364257812" />
                  <Point X="27.964796875" Y="-1.359286132812" />
                  <Point X="27.95403125" Y="-1.476294555664" />
                  <Point X="27.95634765625" Y="-1.507562255859" />
                  <Point X="27.964078125" Y="-1.539182739258" />
                  <Point X="27.976451171875" Y="-1.567996337891" />
                  <Point X="28.0081484375" Y="-1.617300048828" />
                  <Point X="28.076931640625" Y="-1.724286987305" />
                  <Point X="28.0869375" Y="-1.737243286133" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.93650390625" Y="-2.394624267578" />
                  <Point X="29.213125" Y="-2.6068828125" />
                  <Point X="29.124814453125" Y="-2.749781738281" />
                  <Point X="29.072220703125" Y="-2.824509277344" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="28.224560546875" Y="-2.421512207031" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.686056640625" Y="-2.147514160156" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135176757812" />
                  <Point X="27.388203125" Y="-2.164981201172" />
                  <Point X="27.26531640625" Y="-2.229655761719" />
                  <Point X="27.242384765625" Y="-2.246549560547" />
                  <Point X="27.22142578125" Y="-2.267509033203" />
                  <Point X="27.204533203125" Y="-2.290438720703" />
                  <Point X="27.174728515625" Y="-2.347069580078" />
                  <Point X="27.110052734375" Y="-2.469956787109" />
                  <Point X="27.100228515625" Y="-2.499735107422" />
                  <Point X="27.095271484375" Y="-2.531908203125" />
                  <Point X="27.095677734375" Y="-2.563260253906" />
                  <Point X="27.107990234375" Y="-2.631428222656" />
                  <Point X="27.134705078125" Y="-2.779350585938" />
                  <Point X="27.13898828125" Y="-2.795139648438" />
                  <Point X="27.1518203125" Y="-2.826078613281" />
                  <Point X="27.682525390625" Y="-3.745289306641" />
                  <Point X="27.86128515625" Y="-4.054907470703" />
                  <Point X="27.781865234375" Y="-4.111634277344" />
                  <Point X="27.723052734375" Y="-4.149702636719" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="27.081798828125" Y="-3.355522949219" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.654693359375" Y="-2.857333007812" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.3435703125" Y="-2.747883056641" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.0478203125" Y="-2.842669921875" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968861572266" />
                  <Point X="25.88725" Y="-2.996688476562" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.8586484375" Y="-3.103912841797" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627685547" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.970140625" Y="-4.4655078125" />
                  <Point X="26.022064453125" Y="-4.859916015625" />
                  <Point X="25.97569140625" Y="-4.870081054688" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94156640625" Y="-4.752635253906" />
                  <Point X="23.858755859375" Y="-4.731328613281" />
                  <Point X="23.864013671875" Y="-4.691381347656" />
                  <Point X="23.8792265625" Y="-4.575837402344" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030761719" />
                  <Point X="23.8782421875" Y="-4.509323730469" />
                  <Point X="23.876666015625" Y="-4.4976875" />
                  <Point X="23.8575703125" Y="-4.4016953125" />
                  <Point X="23.81613671875" Y="-4.193395996094" />
                  <Point X="23.811873046875" Y="-4.178466308594" />
                  <Point X="23.800970703125" Y="-4.149500488281" />
                  <Point X="23.79433203125" Y="-4.135464355469" />
                  <Point X="23.778259765625" Y="-4.107626953125" />
                  <Point X="23.76942578125" Y="-4.094861572266" />
                  <Point X="23.74979296875" Y="-4.070938232422" />
                  <Point X="23.738994140625" Y="-4.059780273438" />
                  <Point X="23.66541015625" Y="-3.995248046875" />
                  <Point X="23.505734375" Y="-3.855215332031" />
                  <Point X="23.493263671875" Y="-3.845965820312" />
                  <Point X="23.46698046875" Y="-3.829621582031" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.265521484375" Y="-3.789768554688" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.808270019531" />
                  <Point X="22.9045625" Y="-3.8158125" />
                  <Point X="22.823185546875" Y="-3.870187744141" />
                  <Point X="22.64659765625" Y="-3.988180664062" />
                  <Point X="22.64031640625" Y="-3.992759277344" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629150391" />
                  <Point X="22.496798828125" Y="-4.162478027344" />
                  <Point X="22.252408203125" Y="-4.011156982422" />
                  <Point X="22.13293359375" Y="-3.919166748047" />
                  <Point X="22.01913671875" Y="-3.831546630859" />
                  <Point X="22.423669921875" Y="-3.130874267578" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.7100859375" />
                  <Point X="22.67605078125" Y="-2.681121582031" />
                  <Point X="22.680314453125" Y="-2.666190917969" />
                  <Point X="22.6865859375" Y="-2.634665527344" />
                  <Point X="22.688361328125" Y="-2.619241210938" />
                  <Point X="22.689375" Y="-2.588306640625" />
                  <Point X="22.6853359375" Y="-2.557619140625" />
                  <Point X="22.6763515625" Y="-2.528000732422" />
                  <Point X="22.67064453125" Y="-2.513559570312" />
                  <Point X="22.656427734375" Y="-2.484730712891" />
                  <Point X="22.648447265625" Y="-2.471415039063" />
                  <Point X="22.630421875" Y="-2.446259033203" />
                  <Point X="22.620376953125" Y="-2.434418701172" />
                  <Point X="22.603029296875" Y="-2.417070068359" />
                  <Point X="22.591189453125" Y="-2.407024658203" />
                  <Point X="22.566033203125" Y="-2.388996826172" />
                  <Point X="22.552716796875" Y="-2.381014404297" />
                  <Point X="22.523888671875" Y="-2.366796386719" />
                  <Point X="22.509447265625" Y="-2.361089111328" />
                  <Point X="22.479828125" Y="-2.352103271484" />
                  <Point X="22.449140625" Y="-2.3480625" />
                  <Point X="22.418205078125" Y="-2.349074707031" />
                  <Point X="22.402779296875" Y="-2.350849121094" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.430671875" Y="-2.888520019531" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="20.9959765625" Y="-2.740581787109" />
                  <Point X="20.910326171875" Y="-2.596960693359" />
                  <Point X="20.818734375" Y="-2.443374023438" />
                  <Point X="21.540984375" Y="-1.88917199707" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.960837890625" Y="-1.566068725586" />
                  <Point X="21.9837265625" Y="-1.543438354492" />
                  <Point X="21.997712890625" Y="-1.525881347656" />
                  <Point X="22.01722265625" Y="-1.494360839844" />
                  <Point X="22.025423828125" Y="-1.477641113281" />
                  <Point X="22.0345703125" Y="-1.453185424805" />
                  <Point X="22.04054296875" Y="-1.434260864258" />
                  <Point X="22.04581640625" Y="-1.413897583008" />
                  <Point X="22.048048828125" Y="-1.402388183594" />
                  <Point X="22.052162109375" Y="-1.370904174805" />
                  <Point X="22.05291796875" Y="-1.355685424805" />
                  <Point X="22.052216796875" Y="-1.3328203125" />
                  <Point X="22.04876953125" Y="-1.310214599609" />
                  <Point X="22.042625" Y="-1.2881796875" />
                  <Point X="22.03888671875" Y="-1.277344604492" />
                  <Point X="22.0268828125" Y="-1.248362548828" />
                  <Point X="22.021146484375" Y="-1.236802612305" />
                  <Point X="22.00816015625" Y="-1.214568237305" />
                  <Point X="21.992279296875" Y="-1.194298461914" />
                  <Point X="21.973798828125" Y="-1.176368164062" />
                  <Point X="21.96394921875" Y="-1.168033203125" />
                  <Point X="21.94309375" Y="-1.152687133789" />
                  <Point X="21.926859375" Y="-1.141976318359" />
                  <Point X="21.90873046875" Y="-1.131306640625" />
                  <Point X="21.894564453125" Y="-1.124479248047" />
                  <Point X="21.86530078125" Y="-1.113255737305" />
                  <Point X="21.850203125" Y="-1.10885949707" />
                  <Point X="21.81831640625" Y="-1.102378173828" />
                  <Point X="21.802701171875" Y="-1.100532104492" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="20.641416015625" Y="-1.246891235352" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.259240234375" Y="-0.974113220215" />
                  <Point X="20.236576171875" Y="-0.815666015625" />
                  <Point X="20.213548828125" Y="-0.654654541016" />
                  <Point X="21.023642578125" Y="-0.437590270996" />
                  <Point X="21.491712890625" Y="-0.312171295166" />
                  <Point X="21.502453125" Y="-0.308595550537" />
                  <Point X="21.52343359375" Y="-0.300190948486" />
                  <Point X="21.533673828125" Y="-0.295361724854" />
                  <Point X="21.557998046875" Y="-0.282064880371" />
                  <Point X="21.575193359375" Y="-0.271437896729" />
                  <Point X="21.59419140625" Y="-0.258252227783" />
                  <Point X="21.603521484375" Y="-0.250869842529" />
                  <Point X="21.62748046875" Y="-0.229340316772" />
                  <Point X="21.642310546875" Y="-0.212438034058" />
                  <Point X="21.663326171875" Y="-0.181819335938" />
                  <Point X="21.672322265625" Y="-0.165474090576" />
                  <Point X="21.682666015625" Y="-0.141332672119" />
                  <Point X="21.689482421875" Y="-0.122817382812" />
                  <Point X="21.695814453125" Y="-0.102413391113" />
                  <Point X="21.698423828125" Y="-0.091934883118" />
                  <Point X="21.7038671875" Y="-0.063194484711" />
                  <Point X="21.705484375" Y="-0.042683185577" />
                  <Point X="21.70446875" Y="-0.00863888073" />
                  <Point X="21.702423828125" Y="0.008336529732" />
                  <Point X="21.697259765625" Y="0.032558940887" />
                  <Point X="21.692896484375" Y="0.049256278992" />
                  <Point X="21.686564453125" Y="0.069660270691" />
                  <Point X="21.682333984375" Y="0.080780090332" />
                  <Point X="21.669017578125" Y="0.110107246399" />
                  <Point X="21.657275390625" Y="0.129450668335" />
                  <Point X="21.6341796875" Y="0.158904159546" />
                  <Point X="21.621166015625" Y="0.172483093262" />
                  <Point X="21.600591796875" Y="0.190077819824" />
                  <Point X="21.585435546875" Y="0.20176864624" />
                  <Point X="21.5664375" Y="0.214954162598" />
                  <Point X="21.55951953125" Y="0.219327102661" />
                  <Point X="21.534384765625" Y="0.23283543396" />
                  <Point X="21.50343359375" Y="0.245636322021" />
                  <Point X="21.491712890625" Y="0.249611251831" />
                  <Point X="20.476017578125" Y="0.521766235352" />
                  <Point X="20.2145546875" Y="0.591824707031" />
                  <Point X="20.26866796875" Y="0.957521850586" />
                  <Point X="20.314291015625" Y="1.125879638672" />
                  <Point X="20.3664140625" Y="1.318236938477" />
                  <Point X="20.88641015625" Y="1.249778320313" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815551758" />
                  <Point X="21.252943359375" Y="1.204364257812" />
                  <Point X="21.263294921875" Y="1.204703369141" />
                  <Point X="21.28485546875" Y="1.20658984375" />
                  <Point X="21.295109375" Y="1.208053466797" />
                  <Point X="21.3153984375" Y="1.212089111328" />
                  <Point X="21.32543359375" Y="1.214661010742" />
                  <Point X="21.344810546875" Y="1.220770751953" />
                  <Point X="21.386859375" Y="1.234028564453" />
                  <Point X="21.396552734375" Y="1.237677124023" />
                  <Point X="21.415486328125" Y="1.246008300781" />
                  <Point X="21.4247265625" Y="1.250690795898" />
                  <Point X="21.443470703125" Y="1.261513061523" />
                  <Point X="21.452146484375" Y="1.267174438477" />
                  <Point X="21.46882421875" Y="1.279403930664" />
                  <Point X="21.4840703125" Y="1.293374755859" />
                  <Point X="21.49770703125" Y="1.308923706055" />
                  <Point X="21.504099609375" Y="1.317069946289" />
                  <Point X="21.516515625" Y="1.334800292969" />
                  <Point X="21.52198828125" Y="1.343598754883" />
                  <Point X="21.531939453125" Y="1.361736938477" />
                  <Point X="21.53641796875" Y="1.371076538086" />
                  <Point X="21.544193359375" Y="1.389847900391" />
                  <Point X="21.561064453125" Y="1.430581054688" />
                  <Point X="21.564501953125" Y="1.440354248047" />
                  <Point X="21.57029296875" Y="1.460223876953" />
                  <Point X="21.572646484375" Y="1.47032019043" />
                  <Point X="21.57640234375" Y="1.491635498047" />
                  <Point X="21.577640625" Y="1.501920532227" />
                  <Point X="21.578990234375" Y="1.522554931641" />
                  <Point X="21.578087890625" Y="1.543201782227" />
                  <Point X="21.574943359375" Y="1.563639648438" />
                  <Point X="21.5728125" Y="1.573779663086" />
                  <Point X="21.5672109375" Y="1.594686157227" />
                  <Point X="21.56398828125" Y="1.604531982422" />
                  <Point X="21.5564921875" Y="1.623807617188" />
                  <Point X="21.55221875" Y="1.63323815918" />
                  <Point X="21.542837890625" Y="1.651260498047" />
                  <Point X="21.522478515625" Y="1.690368041992" />
                  <Point X="21.517203125" Y="1.699280639648" />
                  <Point X="21.505708984375" Y="1.716484619141" />
                  <Point X="21.499490234375" Y="1.724775634766" />
                  <Point X="21.485578125" Y="1.741354736328" />
                  <Point X="21.47849609375" Y="1.748915893555" />
                  <Point X="21.4635546875" Y="1.763217895508" />
                  <Point X="21.4556953125" Y="1.769958862305" />
                  <Point X="20.87308984375" Y="2.217008300781" />
                  <Point X="20.772384765625" Y="2.294281738281" />
                  <Point X="20.77302734375" Y="2.295381103516" />
                  <Point X="20.99771484375" Y="2.680321533203" />
                  <Point X="21.118546875" Y="2.835635253906" />
                  <Point X="21.273662109375" Y="3.035013427734" />
                  <Point X="21.539775390625" Y="2.881372802734" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.871916015625" Y="2.728796875" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.02356640625" Y="2.734227294922" />
                  <Point X="22.04300390625" Y="2.741302001953" />
                  <Point X="22.0615546875" Y="2.750450439453" />
                  <Point X="22.070580078125" Y="2.755530761719" />
                  <Point X="22.088833984375" Y="2.767159667969" />
                  <Point X="22.09725390625" Y="2.773192626953" />
                  <Point X="22.113384765625" Y="2.786138916016" />
                  <Point X="22.121095703125" Y="2.793052246094" />
                  <Point X="22.140251953125" Y="2.812208007812" />
                  <Point X="22.1818203125" Y="2.853775634766" />
                  <Point X="22.188734375" Y="2.861487548828" />
                  <Point X="22.20168359375" Y="2.877621337891" />
                  <Point X="22.20771875" Y="2.886043212891" />
                  <Point X="22.21934765625" Y="2.904297607422" />
                  <Point X="22.2244296875" Y="2.913325439453" />
                  <Point X="22.233576171875" Y="2.931874267578" />
                  <Point X="22.240646484375" Y="2.951298828125" />
                  <Point X="22.245564453125" Y="2.971387695312" />
                  <Point X="22.2474765625" Y="2.981572998047" />
                  <Point X="22.250302734375" Y="3.003031738281" />
                  <Point X="22.251091796875" Y="3.013364746094" />
                  <Point X="22.25154296875" Y="3.034049804688" />
                  <Point X="22.251205078125" Y="3.044401855469" />
                  <Point X="22.24884375" Y="3.071389404297" />
                  <Point X="22.243720703125" Y="3.129951416016" />
                  <Point X="22.242255859375" Y="3.140208496094" />
                  <Point X="22.23821875" Y="3.160499755859" />
                  <Point X="22.235646484375" Y="3.170533935547" />
                  <Point X="22.22913671875" Y="3.191176513672" />
                  <Point X="22.225486328125" Y="3.200871337891" />
                  <Point X="22.21715625" Y="3.219799560547" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="21.954306640625" Y="3.676197021484" />
                  <Point X="21.94061328125" Y="3.699916015625" />
                  <Point X="21.960310546875" Y="3.715018310547" />
                  <Point X="22.3516328125" Y="4.015040283203" />
                  <Point X="22.541939453125" Y="4.12076953125" />
                  <Point X="22.8074765625" Y="4.268295898437" />
                  <Point X="22.8183515625" Y="4.254122070313" />
                  <Point X="22.881435546875" Y="4.171909179688" />
                  <Point X="22.8881796875" Y="4.164048828125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93491015625" Y="4.121895996094" />
                  <Point X="22.952111328125" Y="4.110403320312" />
                  <Point X="22.9610234375" Y="4.105128417969" />
                  <Point X="22.991060546875" Y="4.0894921875" />
                  <Point X="23.056240234375" Y="4.055561523438" />
                  <Point X="23.06567578125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.043789550781" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.229271484375" Y="4.037488525391" />
                  <Point X="23.2491328125" Y="4.043277587891" />
                  <Point X="23.258904296875" Y="4.046714111328" />
                  <Point X="23.290189453125" Y="4.059673095703" />
                  <Point X="23.358078125" Y="4.087793701172" />
                  <Point X="23.36741796875" Y="4.092274169922" />
                  <Point X="23.385552734375" Y="4.102224121094" />
                  <Point X="23.39434765625" Y="4.107693359375" />
                  <Point X="23.412076171875" Y="4.120107421875" />
                  <Point X="23.420220703125" Y="4.126499023438" />
                  <Point X="23.435771484375" Y="4.140135742188" />
                  <Point X="23.449748046875" Y="4.15538671875" />
                  <Point X="23.461978515625" Y="4.17206640625" />
                  <Point X="23.467638671875" Y="4.180740234375" />
                  <Point X="23.4784609375" Y="4.199483886719" />
                  <Point X="23.483146484375" Y="4.208728515625" />
                  <Point X="23.491478515625" Y="4.227666503906" />
                  <Point X="23.495125" Y="4.237359863281" />
                  <Point X="23.505306640625" Y="4.269655761719" />
                  <Point X="23.527404296875" Y="4.339736328125" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370046386719" />
                  <Point X="23.535474609375" Y="4.380301757812" />
                  <Point X="23.537361328125" Y="4.401863769531" />
                  <Point X="23.53769921875" Y="4.412216796875" />
                  <Point X="23.537248046875" Y="4.4328984375" />
                  <Point X="23.536458984375" Y="4.443227050781" />
                  <Point X="23.520736328125" Y="4.562655273438" />
                  <Point X="23.562244140625" Y="4.57429296875" />
                  <Point X="24.068826171875" Y="4.7163203125" />
                  <Point X="24.29953125" Y="4.743321289062" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.71337109375" Y="4.489237792969" />
                  <Point X="24.77433203125" Y="4.261728027344" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.370265625" Y="4.755416503906" />
                  <Point X="25.378193359375" Y="4.785005859375" />
                  <Point X="25.385546875" Y="4.784235839844" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.018759765625" Y="4.691827636719" />
                  <Point X="26.45359765625" Y="4.58684375" />
                  <Point X="26.57585546875" Y="4.542500488281" />
                  <Point X="26.85826171875" Y="4.440069335938" />
                  <Point X="26.97840234375" Y="4.383883300781" />
                  <Point X="27.25044921875" Y="4.256655761719" />
                  <Point X="27.36657421875" Y="4.189001464844" />
                  <Point X="27.62943359375" Y="4.035859375" />
                  <Point X="27.73890234375" Y="3.958010498047" />
                  <Point X="27.817783203125" Y="3.901915527344" />
                  <Point X="27.33712109375" Y="3.069385986328" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.062376953125" Y="2.593108398438" />
                  <Point X="27.0531875" Y="2.573449951172" />
                  <Point X="27.044185546875" Y="2.54957421875" />
                  <Point X="27.041302734375" Y="2.540603027344" />
                  <Point X="27.034529296875" Y="2.515275878906" />
                  <Point X="27.01983203125" Y="2.460316650391" />
                  <Point X="27.0179140625" Y="2.451469726562" />
                  <Point X="27.013646484375" Y="2.420222900391" />
                  <Point X="27.012755859375" Y="2.383240966797" />
                  <Point X="27.013412109375" Y="2.369581787109" />
                  <Point X="27.016052734375" Y="2.347681152344" />
                  <Point X="27.021783203125" Y="2.300156982422" />
                  <Point X="27.02380078125" Y="2.289038330078" />
                  <Point X="27.029142578125" Y="2.267116210938" />
                  <Point X="27.032466796875" Y="2.256312744141" />
                  <Point X="27.040732421875" Y="2.234223876953" />
                  <Point X="27.04531640625" Y="2.223891601562" />
                  <Point X="27.055681640625" Y="2.203841308594" />
                  <Point X="27.061462890625" Y="2.194123291016" />
                  <Point X="27.075015625" Y="2.174151855469" />
                  <Point X="27.104421875" Y="2.130814697266" />
                  <Point X="27.1098125" Y="2.123628173828" />
                  <Point X="27.130466796875" Y="2.100118896484" />
                  <Point X="27.157599609375" Y="2.075385498047" />
                  <Point X="27.1682578125" Y="2.066982910156" />
                  <Point X="27.188228515625" Y="2.053431396484" />
                  <Point X="27.23156640625" Y="2.024025512695" />
                  <Point X="27.24127734375" Y="2.018247192383" />
                  <Point X="27.261322265625" Y="2.007883789062" />
                  <Point X="27.27165625" Y="2.003298339844" />
                  <Point X="27.293744140625" Y="1.995032470703" />
                  <Point X="27.304548828125" Y="1.991707519531" />
                  <Point X="27.32647265625" Y="1.986364868164" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.3594921875" Y="1.981705932617" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.416044921875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975497314453" />
                  <Point X="27.4843125" Y="1.979822631836" />
                  <Point X="27.49774609375" Y="1.982395507812" />
                  <Point X="27.52307421875" Y="1.989168457031" />
                  <Point X="27.578033203125" Y="2.003865112305" />
                  <Point X="27.583994140625" Y="2.005670654297" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.657630859375" Y="2.617690673828" />
                  <Point X="28.94040625" Y="2.780950927734" />
                  <Point X="29.043955078125" Y="2.637041503906" />
                  <Point X="29.10498046875" Y="2.536195556641" />
                  <Point X="29.136884765625" Y="2.483471679688" />
                  <Point X="28.53081640625" Y="2.018417724609" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.168142578125" Y="1.739872802734" />
                  <Point X="28.152119140625" Y="1.725217285156" />
                  <Point X="28.13466796875" Y="1.706602905273" />
                  <Point X="28.128578125" Y="1.699423828125" />
                  <Point X="28.110349609375" Y="1.675644165039" />
                  <Point X="28.070796875" Y="1.624042602539" />
                  <Point X="28.065638671875" Y="1.616605102539" />
                  <Point X="28.04973828125" Y="1.589367797852" />
                  <Point X="28.034765625" Y="1.555548095703" />
                  <Point X="28.03014453125" Y="1.54267980957" />
                  <Point X="28.023353515625" Y="1.518400512695" />
                  <Point X="28.008619140625" Y="1.465715332031" />
                  <Point X="28.0062265625" Y="1.454670043945" />
                  <Point X="28.002771484375" Y="1.432368896484" />
                  <Point X="28.001708984375" Y="1.421112792969" />
                  <Point X="28.000892578125" Y="1.397541748047" />
                  <Point X="28.001173828125" Y="1.386236938477" />
                  <Point X="28.003078125" Y="1.36375" />
                  <Point X="28.004701171875" Y="1.352567749023" />
                  <Point X="28.010275390625" Y="1.325554077148" />
                  <Point X="28.02237109375" Y="1.266935180664" />
                  <Point X="28.02459765625" Y="1.25823815918" />
                  <Point X="28.03468359375" Y="1.228608520508" />
                  <Point X="28.050287109375" Y="1.195370605469" />
                  <Point X="28.056919921875" Y="1.183526733398" />
                  <Point X="28.072080078125" Y="1.160483642578" />
                  <Point X="28.1049765625" Y="1.110481201172" />
                  <Point X="28.111736328125" Y="1.101427978516" />
                  <Point X="28.1262890625" Y="1.084182617188" />
                  <Point X="28.13408203125" Y="1.075990234375" />
                  <Point X="28.151326171875" Y="1.059900512695" />
                  <Point X="28.16003515625" Y="1.05269519043" />
                  <Point X="28.17824609375" Y="1.039369018555" />
                  <Point X="28.187748046875" Y="1.033247924805" />
                  <Point X="28.20971875" Y="1.02088104248" />
                  <Point X="28.257390625" Y="0.994045349121" />
                  <Point X="28.265482421875" Y="0.989986328125" />
                  <Point X="28.2946796875" Y="0.978083557129" />
                  <Point X="28.33027734375" Y="0.968020935059" />
                  <Point X="28.343671875" Y="0.96525769043" />
                  <Point X="28.373376953125" Y="0.961331848145" />
                  <Point X="28.437833984375" Y="0.952812927246" />
                  <Point X="28.444033203125" Y="0.952199707031" />
                  <Point X="28.46571875" Y="0.951222961426" />
                  <Point X="28.49121875" Y="0.952032348633" />
                  <Point X="28.50060546875" Y="0.952797180176" />
                  <Point X="29.47105078125" Y="1.080559082031" />
                  <Point X="29.704703125" Y="1.111319702148" />
                  <Point X="29.75269140625" Y="0.914204101562" />
                  <Point X="29.771916015625" Y="0.790712585449" />
                  <Point X="29.78387109375" Y="0.713921020508" />
                  <Point X="29.102111328125" Y="0.531243774414" />
                  <Point X="28.6919921875" Y="0.421352722168" />
                  <Point X="28.686029296875" Y="0.419543548584" />
                  <Point X="28.665625" Y="0.412136627197" />
                  <Point X="28.6423828125" Y="0.401619476318" />
                  <Point X="28.634005859375" Y="0.39731729126" />
                  <Point X="28.604822265625" Y="0.380448883057" />
                  <Point X="28.54149609375" Y="0.343844848633" />
                  <Point X="28.5338828125" Y="0.338945343018" />
                  <Point X="28.508779296875" Y="0.319873474121" />
                  <Point X="28.48199609375" Y="0.294352233887" />
                  <Point X="28.472796875" Y="0.284226104736" />
                  <Point X="28.455287109375" Y="0.261914245605" />
                  <Point X="28.417291015625" Y="0.213498260498" />
                  <Point X="28.41085546875" Y="0.204208587646" />
                  <Point X="28.399130859375" Y="0.184928024292" />
                  <Point X="28.393841796875" Y="0.174937149048" />
                  <Point X="28.384068359375" Y="0.153473632812" />
                  <Point X="28.38000390625" Y="0.142924377441" />
                  <Point X="28.373158203125" Y="0.121422058105" />
                  <Point X="28.370376953125" Y="0.11046900177" />
                  <Point X="28.364541015625" Y="0.079992240906" />
                  <Point X="28.351875" Y="0.013858761787" />
                  <Point X="28.350603515625" Y="0.004965910435" />
                  <Point X="28.3485859375" Y="-0.026261247635" />
                  <Point X="28.35028125" Y="-0.062938407898" />
                  <Point X="28.351875" Y="-0.076418655396" />
                  <Point X="28.3577109375" Y="-0.10689541626" />
                  <Point X="28.370376953125" Y="-0.173029190063" />
                  <Point X="28.373158203125" Y="-0.183982254028" />
                  <Point X="28.38000390625" Y="-0.205484558105" />
                  <Point X="28.384068359375" Y="-0.216033813477" />
                  <Point X="28.393841796875" Y="-0.237497344971" />
                  <Point X="28.399130859375" Y="-0.247488220215" />
                  <Point X="28.41085546875" Y="-0.266768768311" />
                  <Point X="28.417291015625" Y="-0.276058441162" />
                  <Point X="28.43480078125" Y="-0.298370147705" />
                  <Point X="28.472796875" Y="-0.346786010742" />
                  <Point X="28.478720703125" Y="-0.353635437012" />
                  <Point X="28.501142578125" Y="-0.37580670166" />
                  <Point X="28.530177734375" Y="-0.398724945068" />
                  <Point X="28.54149609375" Y="-0.406404602051" />
                  <Point X="28.5706796875" Y="-0.423273132324" />
                  <Point X="28.634005859375" Y="-0.45987689209" />
                  <Point X="28.639498046875" Y="-0.462815460205" />
                  <Point X="28.659154296875" Y="-0.472014770508" />
                  <Point X="28.683025390625" Y="-0.481026824951" />
                  <Point X="28.6919921875" Y="-0.483912780762" />
                  <Point X="29.581935546875" Y="-0.722372558594" />
                  <Point X="29.784880859375" Y="-0.776751281738" />
                  <Point X="29.761619140625" Y="-0.931036071777" />
                  <Point X="29.7369765625" Y="-1.039024902344" />
                  <Point X="29.7278046875" Y="-1.079219726563" />
                  <Point X="28.912404296875" Y="-0.971870422363" />
                  <Point X="28.436783203125" Y="-0.909253540039" />
                  <Point X="28.428625" Y="-0.908535522461" />
                  <Point X="28.40009765625" Y="-0.908042541504" />
                  <Point X="28.36672265625" Y="-0.910840820312" />
                  <Point X="28.354482421875" Y="-0.912676147461" />
                  <Point X="28.297205078125" Y="-0.925125854492" />
                  <Point X="28.17291796875" Y="-0.952140014648" />
                  <Point X="28.157875" Y="-0.956743164062" />
                  <Point X="28.128755859375" Y="-0.968367553711" />
                  <Point X="28.1146796875" Y="-0.975388916016" />
                  <Point X="28.086853515625" Y="-0.992280944824" />
                  <Point X="28.07412890625" Y="-1.001528869629" />
                  <Point X="28.050376953125" Y="-1.022000488281" />
                  <Point X="28.039349609375" Y="-1.033224365234" />
                  <Point X="28.00473046875" Y="-1.074861450195" />
                  <Point X="27.92960546875" Y="-1.165212280273" />
                  <Point X="27.92132421875" Y="-1.176851196289" />
                  <Point X="27.90660546875" Y="-1.201232421875" />
                  <Point X="27.90016796875" Y="-1.213974975586" />
                  <Point X="27.88882421875" Y="-1.241360961914" />
                  <Point X="27.884365234375" Y="-1.254926513672" />
                  <Point X="27.877533203125" Y="-1.282574584961" />
                  <Point X="27.87516015625" Y="-1.296657348633" />
                  <Point X="27.870197265625" Y="-1.350579223633" />
                  <Point X="27.859431640625" Y="-1.467587768555" />
                  <Point X="27.859291015625" Y="-1.483313232422" />
                  <Point X="27.861607421875" Y="-1.514580932617" />
                  <Point X="27.86406640625" Y="-1.530123046875" />
                  <Point X="27.871796875" Y="-1.561743530273" />
                  <Point X="27.87678515625" Y="-1.576667358398" />
                  <Point X="27.889158203125" Y="-1.605480957031" />
                  <Point X="27.896541015625" Y="-1.619370605469" />
                  <Point X="27.92823828125" Y="-1.668674316406" />
                  <Point X="27.997021484375" Y="-1.775661254883" />
                  <Point X="28.001744140625" Y="-1.782353271484" />
                  <Point X="28.01980078125" Y="-1.804457397461" />
                  <Point X="28.043494140625" Y="-1.828123291016" />
                  <Point X="28.052798828125" Y="-1.836277709961" />
                  <Point X="28.878671875" Y="-2.469992919922" />
                  <Point X="29.087173828125" Y="-2.629981689453" />
                  <Point X="29.04549609375" Y="-2.697421386719" />
                  <Point X="29.001275390625" Y="-2.760252197266" />
                  <Point X="28.272060546875" Y="-2.339239746094" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.702939453125" Y="-2.054026611328" />
                  <Point X="27.555017578125" Y="-2.027312011719" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.035136352539" />
                  <Point X="27.415068359375" Y="-2.044960083008" />
                  <Point X="27.40058984375" Y="-2.051108642578" />
                  <Point X="27.343958984375" Y="-2.080913085938" />
                  <Point X="27.221072265625" Y="-2.145587646484" />
                  <Point X="27.20896875" Y="-2.153170410156" />
                  <Point X="27.186037109375" Y="-2.170064208984" />
                  <Point X="27.175208984375" Y="-2.179375244141" />
                  <Point X="27.15425" Y="-2.200334716797" />
                  <Point X="27.14494140625" Y="-2.211161621094" />
                  <Point X="27.128048828125" Y="-2.234091308594" />
                  <Point X="27.12046484375" Y="-2.246194091797" />
                  <Point X="27.09066015625" Y="-2.302824951172" />
                  <Point X="27.025984375" Y="-2.425712158203" />
                  <Point X="27.0198359375" Y="-2.440193115234" />
                  <Point X="27.01001171875" Y="-2.469971435547" />
                  <Point X="27.0063359375" Y="-2.485268798828" />
                  <Point X="27.00137890625" Y="-2.517441894531" />
                  <Point X="27.000279296875" Y="-2.533139160156" />
                  <Point X="27.000685546875" Y="-2.564491210938" />
                  <Point X="27.00219140625" Y="-2.580145996094" />
                  <Point X="27.01450390625" Y="-2.648313964844" />
                  <Point X="27.04121875" Y="-2.796236328125" />
                  <Point X="27.04301953125" Y="-2.804222900391" />
                  <Point X="27.051236328125" Y="-2.831534912109" />
                  <Point X="27.064068359375" Y="-2.862473876953" />
                  <Point X="27.069546875" Y="-2.873578613281" />
                  <Point X="27.600251953125" Y="-3.792789306641" />
                  <Point X="27.73589453125" Y="-4.027725341797" />
                  <Point X="27.723755859375" Y="-4.036082519531" />
                  <Point X="27.15716796875" Y="-3.297690673828" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.706068359375" Y="-2.777422851562" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.334865234375" Y="-2.653282714844" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.987083984375" Y="-2.769622070312" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883088378906" />
                  <Point X="25.83218359375" Y="-2.906838378906" />
                  <Point X="25.822935546875" Y="-2.919563720703" />
                  <Point X="25.80604296875" Y="-2.947390625" />
                  <Point X="25.799021484375" Y="-2.961466552734" />
                  <Point X="25.787396484375" Y="-2.990586669922" />
                  <Point X="25.78279296875" Y="-3.005630859375" />
                  <Point X="25.76581640625" Y="-3.083735107422" />
                  <Point X="25.728978515625" Y="-3.25321875" />
                  <Point X="25.7275859375" Y="-3.261287353516" />
                  <Point X="25.724724609375" Y="-3.289677490234" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520019531" />
                  <Point X="25.833087890625" Y="-4.1523203125" />
                  <Point X="25.781435546875" Y="-3.959547363281" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480122314453" />
                  <Point X="25.642146484375" Y="-3.453578857422" />
                  <Point X="25.6267890625" Y="-3.423815429688" />
                  <Point X="25.62041015625" Y="-3.413209228516" />
                  <Point X="25.568759765625" Y="-3.338791748047" />
                  <Point X="25.456681640625" Y="-3.177308837891" />
                  <Point X="25.446671875" Y="-3.165171386719" />
                  <Point X="25.424787109375" Y="-3.142715820312" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104936767578" />
                  <Point X="25.3452421875" Y="-3.090829345703" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.250732421875" Y="-3.060133056641" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.998840087891" />
                  <Point X="24.948935546875" Y="-3.003109619141" />
                  <Point X="24.935015625" Y="-3.006305664062" />
                  <Point X="24.855091796875" Y="-3.031111328125" />
                  <Point X="24.68165625" Y="-3.084938964844" />
                  <Point X="24.6670703125" Y="-3.090829345703" />
                  <Point X="24.6390703125" Y="-3.104936767578" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.587525390625" Y="-3.142717773438" />
                  <Point X="24.565640625" Y="-3.165174560547" />
                  <Point X="24.555630859375" Y="-3.177311279297" />
                  <Point X="24.503982421875" Y="-3.251728515625" />
                  <Point X="24.391904296875" Y="-3.413211669922" />
                  <Point X="24.38753125" Y="-3.420130615234" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213867188" />
                  <Point X="24.35724609375" Y="-3.487936767578" />
                  <Point X="24.083033203125" Y="-4.511315917969" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.411740240315" Y="1.312269648458" />
                  <Point X="20.307006779068" Y="0.567052349381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.505931092801" Y="1.299869217604" />
                  <Point X="20.399458870635" Y="0.542279991731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.231933000846" Y="-0.649728509885" />
                  <Point X="20.222472000625" Y="-0.717047024402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.600121945287" Y="1.28746878675" />
                  <Point X="20.491910959225" Y="0.517507612891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.331620643101" Y="-0.623017249248" />
                  <Point X="20.274100779613" Y="-1.032292344342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.858221233439" Y="2.441337476271" />
                  <Point X="20.831210146167" Y="2.249143603727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.694312797774" Y="1.275068355896" />
                  <Point X="20.584363033469" Y="0.492735131978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.431308285356" Y="-0.596305988612" />
                  <Point X="20.335984725044" Y="-1.274568363484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.98457906191" Y="2.657816972398" />
                  <Point X="20.917805289141" Y="2.182696891388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.78850365026" Y="1.262667925042" />
                  <Point X="20.676815107713" Y="0.467962651065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.530995927611" Y="-0.569594727976" />
                  <Point X="20.431932116972" Y="-1.274470366776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.102627654798" Y="2.815173185242" />
                  <Point X="21.004400412071" Y="2.116250036428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.882694502746" Y="1.250267494188" />
                  <Point X="20.769267181957" Y="0.443190170152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.630683569866" Y="-0.54288346734" />
                  <Point X="20.529674218134" Y="-1.261602350327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.219711992205" Y="2.965668363838" />
                  <Point X="21.090995535" Y="2.049803181469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.976885356756" Y="1.237867074174" />
                  <Point X="20.861719256201" Y="0.418417689239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.730371212121" Y="-0.516172206703" />
                  <Point X="20.627416319296" Y="-1.248734333878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.321509060228" Y="3.007388968711" />
                  <Point X="21.17759065793" Y="1.983356326509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.071076210829" Y="1.225466654606" />
                  <Point X="20.954171330445" Y="0.393645208326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.830058854376" Y="-0.489460946067" />
                  <Point X="20.725158417569" Y="-1.235866337987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.410242714214" Y="2.956158552879" />
                  <Point X="21.26418578086" Y="1.91690947155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.165267064901" Y="1.213066235037" />
                  <Point X="21.046623404689" Y="0.368872727413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.929746496631" Y="-0.462749685431" />
                  <Point X="20.822900515359" Y="-1.222998345532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.4989763682" Y="2.904928137047" />
                  <Point X="21.35078090379" Y="1.85046261659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.260010237324" Y="1.204595764734" />
                  <Point X="21.139075478933" Y="0.3441002465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.029434138413" Y="-0.436038428163" />
                  <Point X="20.920642613149" Y="-1.210130353077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.587710000916" Y="2.853697569868" />
                  <Point X="21.43737602672" Y="1.784015761631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.358838718025" Y="1.225193773262" />
                  <Point X="21.231527553177" Y="0.319327765587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.129121772518" Y="-0.409327225511" />
                  <Point X="21.018384710939" Y="-1.197262360622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.846219113847" Y="-2.422284237403" />
                  <Point X="20.838578520045" Y="-2.476649887204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.676443615527" Y="2.802466873873" />
                  <Point X="21.520595434483" Y="1.693549445183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.461651929714" Y="1.274144616014" />
                  <Point X="21.323979627422" Y="0.294555284674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.228809406624" Y="-0.38261602286" />
                  <Point X="21.116126808729" Y="-1.184394368167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.953748822703" Y="-2.339773773516" />
                  <Point X="20.916215630742" Y="-2.60683631118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.765447083565" Y="2.753156284774" />
                  <Point X="21.416431701666" Y="0.269782803761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.32849704073" Y="-0.355904820208" />
                  <Point X="21.213868906518" Y="-1.171526375712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.061278531558" Y="-2.257263309629" />
                  <Point X="20.993852995489" Y="-2.737020927496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.000939390672" Y="3.746167945856" />
                  <Point X="21.983901510569" Y="3.624937129638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.858126754125" Y="2.730003235797" />
                  <Point X="21.508667530014" Y="0.243471653346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.428184674836" Y="-0.329193617556" />
                  <Point X="21.311611004308" Y="-1.158658383257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.168808240414" Y="-2.174752845742" />
                  <Point X="21.074742634817" Y="-2.844064407722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.108458269345" Y="3.82860134899" />
                  <Point X="22.061054278084" Y="3.49130442485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.953160954908" Y="2.723603539875" />
                  <Point X="21.597438927521" Y="0.192509796621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.528532285038" Y="-0.297786440976" />
                  <Point X="21.409353102098" Y="-1.145790390802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.27633794927" Y="-2.092242381855" />
                  <Point X="21.155723733234" Y="-2.950457122728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.215977148018" Y="3.911034752123" />
                  <Point X="22.138207045599" Y="3.357671720062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.052220736729" Y="2.745847341389" />
                  <Point X="21.67877234961" Y="0.08862399481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.635345130191" Y="-0.220376727369" />
                  <Point X="21.507095199888" Y="-1.132922398348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.383867658125" Y="-2.009731917968" />
                  <Point X="21.245322503002" Y="-2.995531919915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.323496026691" Y="3.993468155256" />
                  <Point X="22.215283348941" Y="3.223494944418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.160297974506" Y="2.832253675989" />
                  <Point X="21.604837297678" Y="-1.120054405893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.491397366981" Y="-1.92722145408" />
                  <Point X="21.349727727803" Y="-2.93525331527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.42846014313" Y="4.057723480544" />
                  <Point X="21.702579395467" Y="-1.107186413438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.598927098329" Y="-1.844710830151" />
                  <Point X="21.454132941097" Y="-2.874974792501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.532518730436" Y="4.115535631252" />
                  <Point X="21.799464059056" Y="-1.100419382338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.706456848926" Y="-1.762200069259" />
                  <Point X="21.558538114689" Y="-2.814696552221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.63657737725" Y="4.173348205384" />
                  <Point X="21.892146610648" Y="-1.12355193171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.813986599523" Y="-1.679689308366" />
                  <Point X="21.662943288282" Y="-2.754418311941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.740636029988" Y="4.231160821666" />
                  <Point X="21.979834400807" Y="-1.182224055348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.92151635012" Y="-1.597178547474" />
                  <Point X="21.767348461875" Y="-2.69414007166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.836476867409" Y="4.230500643658" />
                  <Point X="22.052644218113" Y="-1.34675845656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.040188876447" Y="-1.435382817531" />
                  <Point X="21.871753635467" Y="-2.63386183138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.918928175635" Y="4.134569015021" />
                  <Point X="21.97615880906" Y="-2.5735835911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.007335928678" Y="4.081019693482" />
                  <Point X="22.080563982653" Y="-2.513305350819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.097483261224" Y="4.039848123272" />
                  <Point X="22.184969156245" Y="-2.453027110539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.19212930007" Y="4.030686511664" />
                  <Point X="22.289374329838" Y="-2.392748870259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.109052034932" Y="-3.675808667701" />
                  <Point X="22.080521872215" Y="-3.878811323674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.292257083911" Y="4.060529542412" />
                  <Point X="22.390863474121" Y="-2.353219256635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.235851742745" Y="-3.456185036686" />
                  <Point X="22.167088043083" Y="-3.945464183256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.394870609952" Y="4.108059547954" />
                  <Point X="22.486662534847" Y="-2.354176691274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.362651450557" Y="-3.236561405672" />
                  <Point X="22.253678592784" Y="-4.011943578433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.556086425209" Y="4.572566507838" />
                  <Point X="23.536969600135" Y="4.436543229517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.520115637529" Y="4.316621054291" />
                  <Point X="22.576634753576" Y="-2.396594261036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.489451154325" Y="-3.016937803436" />
                  <Point X="22.341932397247" Y="-4.066588301034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.655955138391" Y="4.600566155069" />
                  <Point X="22.659349057345" Y="-2.490654579154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.616250854341" Y="-2.797314227895" />
                  <Point X="22.430186201709" Y="-4.121233023636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.755823842877" Y="4.628565740421" />
                  <Point X="22.525597967807" Y="-4.124946202748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.855692547363" Y="4.656565325774" />
                  <Point X="22.640081634317" Y="-3.992955759119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.955561251849" Y="4.684564911126" />
                  <Point X="22.74602308446" Y="-3.921746343184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.055429956335" Y="4.712564496478" />
                  <Point X="22.851899235834" Y="-3.851001552137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.153280564677" Y="4.726204581625" />
                  <Point X="22.956126461461" Y="-3.79198947742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.250818531954" Y="4.737620110012" />
                  <Point X="23.054317725954" Y="-3.775925497811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.348356483259" Y="4.749035524747" />
                  <Point X="23.149375713583" Y="-3.782155941526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.445894418627" Y="4.760450826092" />
                  <Point X="23.244433701211" Y="-3.788386385242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.543432353996" Y="4.771866127437" />
                  <Point X="23.339491702787" Y="-3.794616729725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.638772778238" Y="4.767645324649" />
                  <Point X="23.432694643778" Y="-3.814046516117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.701700253327" Y="4.532794404839" />
                  <Point X="23.52096514484" Y="-3.868572436241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.7646278727" Y="4.297944511667" />
                  <Point X="23.606372198049" Y="-3.943472846522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.840165228411" Y="4.152817554642" />
                  <Point X="23.691779231998" Y="-4.018373393849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.928608430012" Y="4.099520462707" />
                  <Point X="23.77569080187" Y="-4.103914720982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.022538262837" Y="4.085262780453" />
                  <Point X="23.841282269316" Y="-4.319810350232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.122829795208" Y="4.116270942533" />
                  <Point X="23.878660993189" Y="-4.736450080881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.240557830023" Y="4.271346266174" />
                  <Point X="23.97150326152" Y="-4.758446186607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.408239383054" Y="4.781859340851" />
                  <Point X="24.123276842871" Y="-4.361124211961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.502781503112" Y="4.771958308644" />
                  <Point X="24.325032923876" Y="-3.608158272636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.597323623171" Y="4.762057276437" />
                  <Point X="24.462700124027" Y="-3.311208415678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.691865743229" Y="4.75215624423" />
                  <Point X="24.581435277696" Y="-3.148967069037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.786407863288" Y="4.742255212023" />
                  <Point X="24.686582327929" Y="-3.083410102174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.879963814089" Y="4.72533722094" />
                  <Point X="24.786891262514" Y="-3.052278116905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.972749148013" Y="4.702936005867" />
                  <Point X="24.88720022199" Y="-3.021145954523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.065534471242" Y="4.680534714686" />
                  <Point X="24.986366951058" Y="-2.998141183811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.158319783948" Y="4.65813334864" />
                  <Point X="25.080991954839" Y="-3.007452467683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.251105096655" Y="4.635731982595" />
                  <Point X="25.172916012147" Y="-3.03598198432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.343890409362" Y="4.613330616549" />
                  <Point X="25.264840050954" Y="-3.064511632594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.436675722069" Y="4.590929250504" />
                  <Point X="25.356292489493" Y="-3.096396891134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.528230771117" Y="4.55977410367" />
                  <Point X="25.443078650032" Y="-3.161484442876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.619511367139" Y="4.526666122085" />
                  <Point X="25.523298835287" Y="-3.273291336352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.710791927309" Y="4.493557885405" />
                  <Point X="25.603077890282" Y="-3.388237034722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.80207248748" Y="4.460449648725" />
                  <Point X="25.674701740132" Y="-3.561210032859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.892867332118" Y="4.423885366449" />
                  <Point X="25.867966429111" Y="-2.868663487251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.762700326799" Y="-3.617670724434" />
                  <Point X="25.737629362269" Y="-3.796059906361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.982884435976" Y="4.381787170974" />
                  <Point X="25.976593564307" Y="-2.778344429208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.809100671648" Y="-3.970118286349" />
                  <Point X="25.800556877617" Y="-4.030910539712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.07290155337" Y="4.339689071815" />
                  <Point X="26.083906389365" Y="-2.697377173731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.162918670764" Y="4.297590972656" />
                  <Point X="26.184087103876" Y="-2.667157521694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.25289866351" Y="4.255228717904" />
                  <Point X="26.281277665186" Y="-2.658213915208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.341571819318" Y="4.203567835167" />
                  <Point X="27.132319063945" Y="2.714657115258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.059602345954" Y="2.197250781752" />
                  <Point X="26.378468234297" Y="-2.649270253217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.430244959142" Y="4.151906838694" />
                  <Point X="27.259118909424" Y="2.934281725822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.140588280529" Y="2.090892477808" />
                  <Point X="26.474030615527" Y="-2.651911749975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.518918092689" Y="4.10024579756" />
                  <Point X="27.385918663968" Y="3.153905689345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.227511079261" Y="2.026777157329" />
                  <Point X="26.565065019827" Y="-2.686771476679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.607591226235" Y="4.048584756426" />
                  <Point X="27.512718273152" Y="3.373528618584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.318053467333" Y="1.988416553249" />
                  <Point X="26.65304893771" Y="-2.743336542083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.695166663082" Y="3.989113197427" />
                  <Point X="27.639517882336" Y="3.593151547823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.412185916464" Y="1.975600560925" />
                  <Point X="26.741032839456" Y="-2.799901722304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.782383428663" Y="3.927089559765" />
                  <Point X="27.766317491521" Y="3.812774477063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.509516870046" Y="1.985543110334" />
                  <Point X="26.827292625727" Y="-2.868734621573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.609651985233" Y="2.015438306319" />
                  <Point X="26.908487789208" Y="-2.973604184497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.713629200913" Y="2.072671467826" />
                  <Point X="27.10161995021" Y="-2.282000624444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.034543457838" Y="-2.759274667354" />
                  <Point X="26.989570571754" Y="-3.07927337932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.818034385202" Y="2.132949784219" />
                  <Point X="27.216305456308" Y="-2.148574017523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.105629346359" Y="-2.936075459239" />
                  <Point X="27.070653354301" Y="-3.184942574143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.922439569492" Y="2.193228100611" />
                  <Point X="27.319974140923" Y="-2.093536168609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.182782093879" Y="-3.069708306304" />
                  <Point X="27.151736136847" Y="-3.290611768966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.026844753782" Y="2.253506417004" />
                  <Point X="27.423107442843" Y="-2.042307765522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.259934841398" Y="-3.203341153368" />
                  <Point X="27.232818923451" Y="-3.396280934924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.131249938071" Y="2.313784733397" />
                  <Point X="28.015538943215" Y="1.490458224052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.001131381072" Y="1.387943092607" />
                  <Point X="27.521392586241" Y="-2.025575802788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.337087588917" Y="-3.336974000433" />
                  <Point X="27.313901710345" Y="-3.501950098809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.235655122361" Y="2.374063049789" />
                  <Point X="28.143116175196" Y="1.715614226991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.066326691381" Y="1.169228658861" />
                  <Point X="27.615545892621" Y="-2.038243388074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.414240336436" Y="-3.470606847498" />
                  <Point X="27.39498449724" Y="-3.607619262694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.34006030665" Y="2.434341366182" />
                  <Point X="28.251482507881" Y="1.804077578748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.147408919906" Y="1.06355552197" />
                  <Point X="27.709104838114" Y="-2.055140070813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.491393083955" Y="-3.604239694563" />
                  <Point X="27.476067284134" Y="-3.71328842658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.44446549094" Y="2.494619682575" />
                  <Point X="28.359012196405" Y="1.886587897965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.235319644241" Y="1.006469657412" />
                  <Point X="27.930212205514" Y="-1.16448257418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.873615955832" Y="-1.567185815567" />
                  <Point X="27.80218907429" Y="-2.075414485854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.568545831474" Y="-3.737872541628" />
                  <Point X="27.557150071029" Y="-3.818957590465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.548870675229" Y="2.554897998967" />
                  <Point X="28.466541884929" Y="1.969098217182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.326018828664" Y="0.969224717341" />
                  <Point X="28.045468200741" Y="-1.026996726178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.950434249692" Y="-1.70319842407" />
                  <Point X="27.891891891485" Y="-2.119748947137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.645698747495" Y="-3.871504189737" />
                  <Point X="27.638232857923" Y="-3.92462675435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.653275859519" Y="2.61517631536" />
                  <Point X="28.574071595737" Y="2.051608694964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.419977594644" Y="0.955172905183" />
                  <Point X="28.150883037022" Y="-0.959534362577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.030618306745" Y="-1.815262383062" />
                  <Point X="27.980625537844" Y="-2.170979417239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.722851781074" Y="-4.005135001386" />
                  <Point X="27.719315644818" Y="-4.030295918235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.757681039886" Y="2.675454603841" />
                  <Point X="28.681601339659" Y="2.134119408361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.515859568203" Y="0.954805426011" />
                  <Point X="28.410224158084" Y="0.203170427235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.362865071496" Y="-0.133806983553" />
                  <Point X="28.250217117794" Y="-0.935338822612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.116706550433" Y="-1.88531587123" />
                  <Point X="28.069359184203" Y="-2.222209887341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.862086220082" Y="2.735732891108" />
                  <Point X="28.789131083581" Y="2.216630121758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.613601670913" Y="0.967673453475" />
                  <Point X="28.524206532529" Y="0.331593992483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.435538698877" Y="-0.299310426434" />
                  <Point X="28.349173595941" Y="-0.91383006493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.203301676443" Y="-1.951762704274" />
                  <Point X="28.158092830562" Y="-2.273440357443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.960458262309" Y="2.753083171139" />
                  <Point X="28.896660827503" Y="2.299140835155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.711343773623" Y="0.980541480939" />
                  <Point X="28.628967688385" Y="0.394405178188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.518766408349" Y="-0.389717673152" />
                  <Point X="28.445587501509" Y="-0.910412651195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.289896802453" Y="-2.018209537318" />
                  <Point X="28.246826476921" Y="-2.324670827545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.040715954371" Y="2.641543152467" />
                  <Point X="29.004190571425" Y="2.381651548552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.809085876334" Y="0.993409508403" />
                  <Point X="28.730124517344" Y="0.431570245409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.607031096514" Y="-0.444284954187" />
                  <Point X="28.53977834499" Y="-0.922813146127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.376491928463" Y="-2.084656370361" />
                  <Point X="28.335560129568" Y="-2.375901252901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.118665540462" Y="2.513580106445" />
                  <Point X="29.111720315347" Y="2.464162261949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.906827979044" Y="1.006277535867" />
                  <Point X="28.829812152707" Y="0.458281457008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.697199299536" Y="-0.485308023241" />
                  <Point X="28.633969188471" Y="-0.935213641058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.463087054473" Y="-2.151103203405" />
                  <Point X="28.424293784715" Y="-2.427131660475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.004570081754" Y="1.019145563331" />
                  <Point X="28.92949978807" Y="0.484992668607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.789651374359" Y="-0.51008050004" />
                  <Point X="28.728160031952" Y="-0.947614135989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.549682180483" Y="-2.217550036449" />
                  <Point X="28.513027439861" Y="-2.47836206805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.102312184464" Y="1.032013590795" />
                  <Point X="29.029187423434" Y="0.511703880206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.882103449181" Y="-0.534852976839" />
                  <Point X="28.822350875433" Y="-0.96001463092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.636277306493" Y="-2.283996869492" />
                  <Point X="28.601761095008" Y="-2.529592475624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.200054287174" Y="1.044881618259" />
                  <Point X="29.128875060714" Y="0.538415105448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.974555524003" Y="-0.559625453637" />
                  <Point X="28.916541719263" Y="-0.972415123371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.722872432503" Y="-2.350443702536" />
                  <Point X="28.690494750154" Y="-2.580822883199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.297796389884" Y="1.057749645723" />
                  <Point X="29.228562703219" Y="0.565126367861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.067007598826" Y="-0.584397930436" />
                  <Point X="29.01073257068" Y="-0.984815561828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.809467558513" Y="-2.41689053558" />
                  <Point X="28.779228405301" Y="-2.632053290774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.395538492594" Y="1.070617673187" />
                  <Point X="29.328250345724" Y="0.591837630274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.159459673648" Y="-0.609170407235" />
                  <Point X="29.104923422098" Y="-0.997216000285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.896062690243" Y="-2.483337327924" />
                  <Point X="28.867962060448" Y="-2.683283698348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.493280590135" Y="1.083485663873" />
                  <Point X="29.427937988229" Y="0.618548892687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.25191174847" Y="-0.633942884033" />
                  <Point X="29.199114273516" Y="-1.009616438742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.982657844734" Y="-2.549783958312" />
                  <Point X="28.956695715594" Y="-2.734514105923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.591022670118" Y="1.096353529624" />
                  <Point X="29.527625630733" Y="0.6452601551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.344363823293" Y="-0.658715360832" />
                  <Point X="29.293305124934" Y="-1.022016877198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.069252999226" Y="-2.6162305887" />
                  <Point X="29.061476523784" Y="-2.671563086601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.6887647501" Y="1.109221395375" />
                  <Point X="29.627313273238" Y="0.671971417513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.436815898115" Y="-0.683487837631" />
                  <Point X="29.387495976352" Y="-1.034417315655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.755108432387" Y="0.898678052825" />
                  <Point X="29.727000915743" Y="0.698682679927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.529267972937" Y="-0.708260314429" />
                  <Point X="29.48168682777" Y="-1.046817754112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.621720057026" Y="-0.733032725297" />
                  <Point X="29.575877679188" Y="-1.059218192569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.714172153381" Y="-0.757805048885" />
                  <Point X="29.670068530605" Y="-1.071618631026" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.597908203125" Y="-4.00872265625" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.412669921875" Y="-3.447126708984" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.194412109375" Y="-3.241593994141" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.911412109375" Y="-3.212572265625" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.660072265625" Y="-3.360061035156" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112060547" />
                  <Point X="24.266560546875" Y="-4.560491699219" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.103462890625" Y="-4.97760546875" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.80793359375" Y="-4.914440429688" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.675638671875" Y="-4.666583007812" />
                  <Point X="23.6908515625" Y="-4.5510390625" />
                  <Point X="23.690318359375" Y="-4.5347578125" />
                  <Point X="23.67122265625" Y="-4.438765625" />
                  <Point X="23.6297890625" Y="-4.230466308594" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.5401328125" Y="-4.138096679688" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.253095703125" Y="-3.979361816406" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.928744140625" Y="-4.028166259766" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.58631640625" Y="-4.357925292969" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.44276171875" Y="-4.352493164063" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="22.01701953125" Y="-4.069711914062" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.259126953125" Y="-3.035874267578" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597594238281" />
                  <Point X="22.486021484375" Y="-2.568765380859" />
                  <Point X="22.468673828125" Y="-2.551416748047" />
                  <Point X="22.439845703125" Y="-2.537198730469" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.525671875" Y="-3.053064941406" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.074201171875" Y="-3.157060546875" />
                  <Point X="20.83830078125" Y="-2.847135742188" />
                  <Point X="20.747142578125" Y="-2.694277832031" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="21.4253203125" Y="-1.738434814453" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.847462890625" Y="-1.411083862305" />
                  <Point X="21.856609375" Y="-1.386628051758" />
                  <Point X="21.8618828125" Y="-1.366264892578" />
                  <Point X="21.86334765625" Y="-1.350049926758" />
                  <Point X="21.85134375" Y="-1.321067871094" />
                  <Point X="21.83048828125" Y="-1.305721801758" />
                  <Point X="21.812359375" Y="-1.295052246094" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.666216796875" Y="-1.43526550293" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.164873046875" Y="-1.372401855469" />
                  <Point X="20.072607421875" Y="-1.011187438965" />
                  <Point X="20.048490234375" Y="-0.842567932129" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="20.974466796875" Y="-0.254064346313" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.466861328125" Y="-0.115348609924" />
                  <Point X="21.485859375" Y="-0.102163024902" />
                  <Point X="21.497677734375" Y="-0.090644767761" />
                  <Point X="21.508021484375" Y="-0.066503471375" />
                  <Point X="21.514353515625" Y="-0.046099510193" />
                  <Point X="21.516599609375" Y="-0.031280040741" />
                  <Point X="21.511435546875" Y="-0.007057653904" />
                  <Point X="21.505103515625" Y="0.013346308708" />
                  <Point X="21.497677734375" Y="0.028084686279" />
                  <Point X="21.477103515625" Y="0.045679382324" />
                  <Point X="21.45810546875" Y="0.058864971161" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.426841796875" Y="0.338240325928" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.022892578125" Y="0.594573120117" />
                  <Point X="20.08235546875" Y="0.996414794922" />
                  <Point X="20.130904296875" Y="1.17557409668" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.9112109375" Y="1.438152832031" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.287673828125" Y="1.401976318359" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056518555" />
                  <Point X="21.3608828125" Y="1.443786743164" />
                  <Point X="21.368658203125" Y="1.462558105469" />
                  <Point X="21.385529296875" Y="1.503291137695" />
                  <Point X="21.38928515625" Y="1.524606445312" />
                  <Point X="21.38368359375" Y="1.545512817383" />
                  <Point X="21.374302734375" Y="1.56353515625" />
                  <Point X="21.353943359375" Y="1.602642700195" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.75742578125" Y="2.066270996094" />
                  <Point X="20.52389453125" Y="2.245466308594" />
                  <Point X="20.60893359375" Y="2.391159423828" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="20.9685859375" Y="2.952303466797" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.634775390625" Y="3.045917724609" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.888474609375" Y="2.918073730469" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404296875" />
                  <Point X="22.005904296875" Y="2.946560058594" />
                  <Point X="22.04747265625" Y="2.988127685547" />
                  <Point X="22.0591015625" Y="3.006382080078" />
                  <Point X="22.061927734375" Y="3.027840820312" />
                  <Point X="22.05956640625" Y="3.054828369141" />
                  <Point X="22.054443359375" Y="3.113390380859" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.789763671875" Y="3.581197021484" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="21.844705078125" Y="3.865801269531" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.449666015625" Y="4.286858398438" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.96908984375" Y="4.369786132812" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.078791015625" Y="4.258024414062" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.217478515625" Y="4.235209472656" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.303095703125" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.294487792969" />
                  <Point X="23.324099609375" Y="4.326783691406" />
                  <Point X="23.346197265625" Y="4.396864257812" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.318732421875" Y="4.641370117188" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.510951171875" Y="4.757238769531" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.2774453125" Y="4.932033203125" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.8968984375" Y="4.538413085938" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.18673828125" Y="4.804592285156" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.4053359375" Y="4.973202636719" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.063349609375" Y="4.876520996094" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.640640625" Y="4.721114746094" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.058890625" Y="4.555991699219" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.46221875" Y="4.353171875" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.849015625" Y="4.112849121094" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.5016640625" Y="2.974385986328" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491515136719" />
                  <Point X="27.218080078125" Y="2.466187988281" />
                  <Point X="27.2033828125" Y="2.411228759766" />
                  <Point X="27.202044921875" Y="2.392325927734" />
                  <Point X="27.204685546875" Y="2.370425292969" />
                  <Point X="27.210416015625" Y="2.322901123047" />
                  <Point X="27.218681640625" Y="2.300812255859" />
                  <Point X="27.232234375" Y="2.280840820312" />
                  <Point X="27.261640625" Y="2.237503662109" />
                  <Point X="27.27494140625" Y="2.224203613281" />
                  <Point X="27.294912109375" Y="2.210652099609" />
                  <Point X="27.33825" Y="2.18124609375" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.38223828125" Y="2.170339355469" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.4739921875" Y="2.172719238281" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.562630859375" Y="2.782235595703" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.042255859375" Y="2.964710449219" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.26753515625" Y="2.634562255859" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.64648046875" Y="1.867680664062" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583832519531" />
                  <Point X="28.261142578125" Y="1.560052734375" />
                  <Point X="28.22158984375" Y="1.508451171875" />
                  <Point X="28.21312109375" Y="1.491500366211" />
                  <Point X="28.206330078125" Y="1.467221069336" />
                  <Point X="28.191595703125" Y="1.414535888672" />
                  <Point X="28.190779296875" Y="1.39096484375" />
                  <Point X="28.196353515625" Y="1.363951049805" />
                  <Point X="28.20844921875" Y="1.30533215332" />
                  <Point X="28.215646484375" Y="1.287955444336" />
                  <Point X="28.230806640625" Y="1.264912475586" />
                  <Point X="28.263703125" Y="1.21491003418" />
                  <Point X="28.280947265625" Y="1.19882019043" />
                  <Point X="28.30291796875" Y="1.18645324707" />
                  <Point X="28.35058984375" Y="1.159617675781" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.398271484375" Y="1.149693847656" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.44625" Y="1.26893359375" />
                  <Point X="29.8489765625" Y="1.321953125" />
                  <Point X="29.87032421875" Y="1.234254394531" />
                  <Point X="29.939193359375" Y="0.951366821289" />
                  <Point X="29.959654296875" Y="0.819940368652" />
                  <Point X="29.997859375" Y="0.574556335449" />
                  <Point X="29.151287109375" Y="0.347717773438" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819198608" />
                  <Point X="28.699904296875" Y="0.215950866699" />
                  <Point X="28.636578125" Y="0.179346832275" />
                  <Point X="28.622265625" Y="0.166926696777" />
                  <Point X="28.604755859375" Y="0.144614883423" />
                  <Point X="28.566759765625" Y="0.096199020386" />
                  <Point X="28.556986328125" Y="0.074735565186" />
                  <Point X="28.551150390625" Y="0.044258766174" />
                  <Point X="28.538484375" Y="-0.02187484169" />
                  <Point X="28.538484375" Y="-0.04068523407" />
                  <Point X="28.5443203125" Y="-0.071162033081" />
                  <Point X="28.556986328125" Y="-0.137295639038" />
                  <Point X="28.566759765625" Y="-0.158759094238" />
                  <Point X="28.58426953125" Y="-0.181070907593" />
                  <Point X="28.622265625" Y="-0.22948677063" />
                  <Point X="28.636578125" Y="-0.241906906128" />
                  <Point X="28.66576171875" Y="-0.258775390625" />
                  <Point X="28.729087890625" Y="-0.295379272461" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.631111328125" Y="-0.538846679688" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.9868828125" Y="-0.711367492676" />
                  <Point X="29.948431640625" Y="-0.966412597656" />
                  <Point X="29.92221484375" Y="-1.081296264648" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.887603515625" Y="-1.160244995117" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.337560546875" Y="-1.110790649414" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.185447265625" Y="-1.154697021484" />
                  <Point X="28.150828125" Y="-1.196334106445" />
                  <Point X="28.075703125" Y="-1.286685058594" />
                  <Point X="28.064359375" Y="-1.314071166992" />
                  <Point X="28.059396484375" Y="-1.367993041992" />
                  <Point X="28.048630859375" Y="-1.485001464844" />
                  <Point X="28.056361328125" Y="-1.516622070312" />
                  <Point X="28.08805859375" Y="-1.56592565918" />
                  <Point X="28.156841796875" Y="-1.672912719727" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.9943359375" Y="-2.319255615234" />
                  <Point X="29.339076171875" Y="-2.583784179688" />
                  <Point X="29.312521484375" Y="-2.626753417969" />
                  <Point X="29.2041328125" Y="-2.802140136719" />
                  <Point X="29.149908203125" Y="-2.879186035156" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.177060546875" Y="-2.503784667969" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.669173828125" Y="-2.241001708984" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.432447265625" Y="-2.249049316406" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334683349609" />
                  <Point X="27.258796875" Y="-2.391314208984" />
                  <Point X="27.19412109375" Y="-2.514201416016" />
                  <Point X="27.1891640625" Y="-2.546374511719" />
                  <Point X="27.2014765625" Y="-2.614542480469" />
                  <Point X="27.22819140625" Y="-2.76246484375" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.764798828125" Y="-3.697789306641" />
                  <Point X="27.98667578125" Y="-4.082089111328" />
                  <Point X="27.963912109375" Y="-4.098348632812" />
                  <Point X="27.835314453125" Y="-4.190202148438" />
                  <Point X="27.774673828125" Y="-4.229453613281" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="27.0064296875" Y="-3.413355224609" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.603318359375" Y="-2.937243164062" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.352275390625" Y="-2.842483398438" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.108556640625" Y="-2.915717773438" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045986328125" />
                  <Point X="25.95148046875" Y="-3.124090576172" />
                  <Point X="25.914642578125" Y="-3.29357421875" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="26.064328125" Y="-4.453107910156" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.1160234375" Y="-4.936575683594" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.938337890625" Y="-4.973421386719" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#148" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.06096105203" Y="4.582634228643" Z="0.8" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.8" />
                  <Point X="-0.734523202698" Y="5.012974273047" Z="0.8" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.8" />
                  <Point X="-1.508703818612" Y="4.836662241728" Z="0.8" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.8" />
                  <Point X="-1.736996940898" Y="4.66612412805" Z="0.8" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.8" />
                  <Point X="-1.729742312464" Y="4.373099843406" Z="0.8" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.8" />
                  <Point X="-1.80781358704" Y="4.31268355068" Z="0.8" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.8" />
                  <Point X="-1.904278255919" Y="4.333654967541" Z="0.8" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.8" />
                  <Point X="-1.997399298944" Y="4.431504154989" Z="0.8" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.8" />
                  <Point X="-2.580774539162" Y="4.361846143208" Z="0.8" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.8" />
                  <Point X="-3.19187294104" Y="3.936761127657" Z="0.8" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.8" />
                  <Point X="-3.25969499491" Y="3.587476974855" Z="0.8" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.8" />
                  <Point X="-2.996401132513" Y="3.081751009208" Z="0.8" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.8" />
                  <Point X="-3.035607668061" Y="3.013195899825" Z="0.8" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.8" />
                  <Point X="-3.113325456399" Y="2.99916356666" Z="0.8" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.8" />
                  <Point X="-3.346382373442" Y="3.120498953836" Z="0.8" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.8" />
                  <Point X="-4.077034053908" Y="3.01428585996" Z="0.8" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.8" />
                  <Point X="-4.44040425705" Y="2.447644560713" Z="0.8" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.8" />
                  <Point X="-4.27916817307" Y="2.05788328882" Z="0.8" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.8" />
                  <Point X="-3.676204187547" Y="1.571726565253" Z="0.8" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.8" />
                  <Point X="-3.683694599879" Y="1.512971365389" Z="0.8" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.8" />
                  <Point X="-3.733518534205" Y="1.480942242495" Z="0.8" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.8" />
                  <Point X="-4.08841994626" Y="1.519005104384" Z="0.8" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.8" />
                  <Point X="-4.923513272879" Y="1.219931229473" Z="0.8" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.8" />
                  <Point X="-5.03272542084" Y="0.633172221906" Z="0.8" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.8" />
                  <Point X="-4.592257188919" Y="0.321224080653" Z="0.8" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.8" />
                  <Point X="-3.557562799386" Y="0.035883379362" Z="0.8" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.8" />
                  <Point X="-3.542475105177" Y="0.009402919699" Z="0.8" />
                  <Point X="-3.539556741714" Y="0" Z="0.8" />
                  <Point X="-3.545889495958" Y="-0.020404031362" Z="0.8" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.8" />
                  <Point X="-3.567805795737" Y="-0.042992603772" Z="0.8" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.8" />
                  <Point X="-4.044630599854" Y="-0.174487976103" Z="0.8" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.8" />
                  <Point X="-5.007162078061" Y="-0.818366922321" Z="0.8" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.8" />
                  <Point X="-4.889729755139" Y="-1.353496006647" Z="0.8" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.8" />
                  <Point X="-4.333414031213" Y="-1.453557752385" Z="0.8" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.8" />
                  <Point X="-3.201030517925" Y="-1.317532877622" Z="0.8" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.8" />
                  <Point X="-3.197950336836" Y="-1.342813047014" Z="0.8" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.8" />
                  <Point X="-3.611274524366" Y="-1.667486999192" Z="0.8" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.8" />
                  <Point X="-4.301956997537" Y="-2.688607470617" Z="0.8" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.8" />
                  <Point X="-3.971740315579" Y="-3.156063705373" Z="0.8" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.8" />
                  <Point X="-3.455484135518" Y="-3.065086082304" Z="0.8" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.8" />
                  <Point X="-2.560963748236" Y="-2.567366720896" Z="0.8" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.8" />
                  <Point X="-2.790330930864" Y="-2.979594246974" Z="0.8" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.8" />
                  <Point X="-3.01964103592" Y="-4.078049003647" Z="0.8" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.8" />
                  <Point X="-2.589717831024" Y="-4.363723842738" Z="0.8" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.8" />
                  <Point X="-2.380172083985" Y="-4.35708340384" Z="0.8" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.8" />
                  <Point X="-2.049634203106" Y="-4.038459626868" Z="0.8" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.8" />
                  <Point X="-1.756330038109" Y="-3.997974744045" Z="0.8" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.8" />
                  <Point X="-1.49899060124" Y="-4.144408436467" Z="0.8" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.8" />
                  <Point X="-1.383972561133" Y="-4.417240170402" Z="0.8" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.8" />
                  <Point X="-1.380090209615" Y="-4.628776319243" Z="0.8" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.8" />
                  <Point X="-1.210682668534" Y="-4.931583225641" Z="0.8" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.8" />
                  <Point X="-0.912222281846" Y="-4.995409647229" Z="0.8" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.8" />
                  <Point X="-0.691300449121" Y="-4.542152497085" Z="0.8" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.8" />
                  <Point X="-0.305008723737" Y="-3.357289967401" Z="0.8" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.8" />
                  <Point X="-0.07992481597" Y="-3.229045081815" Z="0.8" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.8" />
                  <Point X="0.173434263391" Y="-3.258066963473" Z="0.8" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.8" />
                  <Point X="0.365437135695" Y="-3.444355788367" Z="0.8" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.8" />
                  <Point X="0.54345441128" Y="-3.99038353273" Z="0.8" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.8" />
                  <Point X="0.941119332093" Y="-4.991336363966" Z="0.8" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.8" />
                  <Point X="1.120572014285" Y="-4.954135888832" Z="0.8" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.8" />
                  <Point X="1.107744004794" Y="-4.415301464399" Z="0.8" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.8" />
                  <Point X="0.99418374417" Y="-3.103430180487" Z="0.8" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.8" />
                  <Point X="1.134365967848" Y="-2.9228843048" Z="0.8" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.8" />
                  <Point X="1.350700776399" Y="-2.860992857236" Z="0.8" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.8" />
                  <Point X="1.570121841219" Y="-2.948021269843" Z="0.8" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.8" />
                  <Point X="1.960604129846" Y="-3.412513088919" Z="0.8" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.8" />
                  <Point X="2.79568717443" Y="-4.240147696871" Z="0.8" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.8" />
                  <Point X="2.986815140796" Y="-4.107755482724" Z="0.8" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.8" />
                  <Point X="2.801943637122" Y="-3.641509276087" Z="0.8" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.8" />
                  <Point X="2.244522211419" Y="-2.574376208962" Z="0.8" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.8" />
                  <Point X="2.296885765186" Y="-2.383321054278" Z="0.8" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.8" />
                  <Point X="2.449577324094" Y="-2.262015544781" Z="0.8" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.8" />
                  <Point X="2.65413059455" Y="-2.258925798072" Z="0.8" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.8" />
                  <Point X="3.14590432102" Y="-2.515805863152" Z="0.8" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.8" />
                  <Point X="4.184640529501" Y="-2.876683254438" Z="0.8" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.8" />
                  <Point X="4.348896898227" Y="-2.621758686907" Z="0.8" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.8" />
                  <Point X="4.018616103612" Y="-2.248307969555" Z="0.8" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.8" />
                  <Point X="3.123960091393" Y="-1.507606008013" Z="0.8" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.8" />
                  <Point X="3.103029592278" Y="-1.341294001797" Z="0.8" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.8" />
                  <Point X="3.183115594356" Y="-1.197021216493" Z="0.8" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.8" />
                  <Point X="3.342023460896" Y="-1.128369686754" Z="0.8" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.8" />
                  <Point X="3.874922029413" Y="-1.178537266246" Z="0.8" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.8" />
                  <Point X="4.964803527481" Y="-1.061140378552" Z="0.8" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.8" />
                  <Point X="5.030167386865" Y="-0.687541522291" Z="0.8" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.8" />
                  <Point X="4.637896602181" Y="-0.459270375917" Z="0.8" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.8" />
                  <Point X="3.684625973261" Y="-0.184206653915" Z="0.8" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.8" />
                  <Point X="3.617446678851" Y="-0.118922328818" Z="0.8" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.8" />
                  <Point X="3.587271378429" Y="-0.030476819516" Z="0.8" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.8" />
                  <Point X="3.594100071995" Y="0.066133711709" Z="0.8" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.8" />
                  <Point X="3.637932759549" Y="0.145026409757" Z="0.8" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.8" />
                  <Point X="3.718769441091" Y="0.203942190327" Z="0.8" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.8" />
                  <Point X="4.158071215125" Y="0.330701556851" Z="0.8" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.8" />
                  <Point X="5.002902444808" Y="0.858912629305" Z="0.8" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.8" />
                  <Point X="4.912750277234" Y="1.277361316826" Z="0.8" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.8" />
                  <Point X="4.433568263972" Y="1.349785874014" Z="0.8" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.8" />
                  <Point X="3.39866464319" Y="1.230542830637" Z="0.8" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.8" />
                  <Point X="3.321287882657" Y="1.261304200703" Z="0.8" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.8" />
                  <Point X="3.266421263028" Y="1.323673464248" Z="0.8" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.8" />
                  <Point X="3.239165895503" Y="1.405335427249" Z="0.8" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.8" />
                  <Point X="3.24832602616" Y="1.485034433335" Z="0.8" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.8" />
                  <Point X="3.294670158012" Y="1.560915237067" Z="0.8" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.8" />
                  <Point X="3.670761030836" Y="1.859292930868" Z="0.8" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.8" />
                  <Point X="4.304155596219" Y="2.691728596281" Z="0.8" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.8" />
                  <Point X="4.076685522253" Y="3.0251936075" Z="0.8" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.8" />
                  <Point X="3.531472997249" Y="2.856816926974" Z="0.8" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.8" />
                  <Point X="2.454918811659" Y="2.252301986253" Z="0.8" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.8" />
                  <Point X="2.382067600904" Y="2.251259695577" Z="0.8" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.8" />
                  <Point X="2.316829479953" Y="2.283306658084" Z="0.8" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.8" />
                  <Point X="2.26745194215" Y="2.34019538043" Z="0.8" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.8" />
                  <Point X="2.248170007182" Y="2.407690842928" Z="0.8" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.8" />
                  <Point X="2.260225956404" Y="2.484550764223" Z="0.8" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.8" />
                  <Point X="2.538808425886" Y="2.980666084706" Z="0.8" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.8" />
                  <Point X="2.871836191273" Y="4.1848763031" Z="0.8" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.8" />
                  <Point X="2.481232493129" Y="4.42765442242" Z="0.8" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.8" />
                  <Point X="2.073916313032" Y="4.632563540811" Z="0.8" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.8" />
                  <Point X="1.651531908916" Y="4.799398114016" Z="0.8" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.8" />
                  <Point X="1.068926359353" Y="4.956404446444" Z="0.8" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.8" />
                  <Point X="0.404386812742" Y="5.054210799554" Z="0.8" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.8" />
                  <Point X="0.132283509523" Y="4.848813277308" Z="0.8" />
                  <Point X="0" Y="4.355124473572" Z="0.8" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>