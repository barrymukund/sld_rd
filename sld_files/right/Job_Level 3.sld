<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#121" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="404" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004716308594" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.579275390625" Y="-3.572129882812" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.554763671875" Y="-3.491298583984" />
                  <Point X="25.541015625" Y="-3.466324707031" />
                  <Point X="25.535837890625" Y="-3.457971191406" />
                  <Point X="25.37863671875" Y="-3.231476318359" />
                  <Point X="25.357109375" Y="-3.207521972656" />
                  <Point X="25.326244140625" Y="-3.1863984375" />
                  <Point X="25.301404296875" Y="-3.175837890625" />
                  <Point X="25.29239453125" Y="-3.172533935547" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0203984375" Y="-3.091593017578" />
                  <Point X="24.9861015625" Y="-3.09266015625" />
                  <Point X="24.9611796875" Y="-3.098042236328" />
                  <Point X="24.95307421875" Y="-3.100170898438" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.680494140625" Y="-3.188985107422" />
                  <Point X="24.650861328125" Y="-3.212289794922" />
                  <Point X="24.63281640625" Y="-3.233513671875" />
                  <Point X="24.6271484375" Y="-3.240882324219" />
                  <Point X="24.46994921875" Y="-3.467377441406" />
                  <Point X="24.461814453125" Y="-3.481569335938" />
                  <Point X="24.449009765625" Y="-3.5125234375" />
                  <Point X="24.083416015625" Y="-4.876941894531" />
                  <Point X="23.920666015625" Y="-4.8453515625" />
                  <Point X="23.91309375" Y="-4.843403320312" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.783115234375" Y="-4.578045898438" />
                  <Point X="23.7850390625" Y="-4.563442382812" />
                  <Point X="23.78534375" Y="-4.5412421875" />
                  <Point X="23.782396484375" Y="-4.512825195312" />
                  <Point X="23.781078125" Y="-4.504091796875" />
                  <Point X="23.722962890625" Y="-4.211932128906" />
                  <Point X="23.7129609375" Y="-4.181754882812" />
                  <Point X="23.6930546875" Y="-4.14992578125" />
                  <Point X="23.673421875" Y="-4.129177734375" />
                  <Point X="23.667056640625" Y="-4.123047851562" />
                  <Point X="23.443095703125" Y="-3.926639160156" />
                  <Point X="23.416794921875" Y="-3.908789550781" />
                  <Point X="23.3816171875" Y="-3.89565234375" />
                  <Point X="23.353408203125" Y="-3.891144287109" />
                  <Point X="23.34462890625" Y="-3.890157226562" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.015646484375" Y="-3.872525634766" />
                  <Point X="22.97974609375" Y="-3.883517089844" />
                  <Point X="22.954623046875" Y="-3.897112304688" />
                  <Point X="22.947056640625" Y="-3.901673828125" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076821533203" />
                  <Point X="22.6648984375" Y="-4.099461425781" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.198287109375" Y="-4.089384033203" />
                  <Point X="22.187818359375" Y="-4.081322753906" />
                  <Point X="21.89527734375" Y="-3.856077392578" />
                  <Point X="22.546556640625" Y="-2.728028564453" />
                  <Point X="22.57623828125" Y="-2.676620117188" />
                  <Point X="22.587140625" Y="-2.647659912109" />
                  <Point X="22.593412109375" Y="-2.616142089844" />
                  <Point X="22.5943671875" Y="-2.584763916016" />
                  <Point X="22.58504296875" Y="-2.554787597656" />
                  <Point X="22.570134765625" Y="-2.525258300781" />
                  <Point X="22.552505859375" Y="-2.500898925781" />
                  <Point X="22.535849609375" Y="-2.4842421875" />
                  <Point X="22.51069140625" Y="-2.466213134766" />
                  <Point X="22.481861328125" Y="-2.451995849609" />
                  <Point X="22.452244140625" Y="-2.44301171875" />
                  <Point X="22.421310546875" Y="-2.444024414062" />
                  <Point X="22.38978515625" Y="-2.450295166016" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="20.917146484375" Y="-2.793868408203" />
                  <Point X="20.9096328125" Y="-2.781270751953" />
                  <Point X="20.693857421875" Y="-2.41944921875" />
                  <Point X="21.84211328125" Y="-1.538361938477" />
                  <Point X="21.894044921875" Y="-1.498513305664" />
                  <Point X="21.916037109375" Y="-1.474653076172" />
                  <Point X="21.934310546875" Y="-1.446334228516" />
                  <Point X="21.946453125" Y="-1.418642456055" />
                  <Point X="21.953849609375" Y="-1.390081298828" />
                  <Point X="21.95665234375" Y="-1.359650024414" />
                  <Point X="21.95444140625" Y="-1.327973266602" />
                  <Point X="21.94721875" Y="-1.297700927734" />
                  <Point X="21.930599609375" Y="-1.271387329102" />
                  <Point X="21.90869921875" Y="-1.246809448242" />
                  <Point X="21.885958984375" Y="-1.228138427734" />
                  <Point X="21.860546875" Y="-1.213181518555" />
                  <Point X="21.83128515625" Y="-1.20195715332" />
                  <Point X="21.7993984375" Y="-1.195475097656" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.165923828125" Y="-0.992650939941" />
                  <Point X="20.1639375" Y="-0.978768737793" />
                  <Point X="20.107578125" Y="-0.584698303223" />
                  <Point X="21.407966796875" Y="-0.236259490967" />
                  <Point X="21.467125" Y="-0.220408325195" />
                  <Point X="21.483580078125" Y="-0.214322296143" />
                  <Point X="21.513380859375" Y="-0.198701400757" />
                  <Point X="21.54002734375" Y="-0.180207260132" />
                  <Point X="21.56315234375" Y="-0.157397872925" />
                  <Point X="21.582765625" Y="-0.129952362061" />
                  <Point X="21.596205078125" Y="-0.10287310791" />
                  <Point X="21.6050859375" Y="-0.074254554749" />
                  <Point X="21.609345703125" Y="-0.044949760437" />
                  <Point X="21.60897265625" Y="-0.014110663414" />
                  <Point X="21.604712890625" Y="0.012896483421" />
                  <Point X="21.5958359375" Y="0.041501983643" />
                  <Point X="21.580982421875" Y="0.070507759094" />
                  <Point X="21.560615234375" Y="0.09754359436" />
                  <Point X="21.538904296875" Y="0.118425994873" />
                  <Point X="21.5122734375" Y="0.136909362793" />
                  <Point X="21.49807421875" Y="0.145047744751" />
                  <Point X="21.467125" Y="0.157848236084" />
                  <Point X="20.108185546875" Y="0.521975585938" />
                  <Point X="20.175513671875" Y="0.976980895996" />
                  <Point X="20.17951171875" Y="0.991736633301" />
                  <Point X="20.296447265625" Y="1.423267944336" />
                  <Point X="21.1919296875" Y="1.305375854492" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.25502734375" Y="1.299342651367" />
                  <Point X="21.27659765625" Y="1.301232299805" />
                  <Point X="21.2968828125" Y="1.30526953125" />
                  <Point X="21.2993203125" Y="1.306038574219" />
                  <Point X="21.3582890625" Y="1.324630859375" />
                  <Point X="21.37722265625" Y="1.332962036133" />
                  <Point X="21.395966796875" Y="1.343783813477" />
                  <Point X="21.4126484375" Y="1.356015869141" />
                  <Point X="21.426287109375" Y="1.371568847656" />
                  <Point X="21.438701171875" Y="1.389298950195" />
                  <Point X="21.44865234375" Y="1.407440063477" />
                  <Point X="21.449634765625" Y="1.4098125" />
                  <Point X="21.473298828125" Y="1.466944335938" />
                  <Point X="21.479083984375" Y="1.486788330078" />
                  <Point X="21.48284375" Y="1.508103515625" />
                  <Point X="21.484197265625" Y="1.528750244141" />
                  <Point X="21.481048828125" Y="1.549200195312" />
                  <Point X="21.4754453125" Y="1.570106689453" />
                  <Point X="21.467953125" Y="1.589371826172" />
                  <Point X="21.466767578125" Y="1.591649658203" />
                  <Point X="21.438212890625" Y="1.646501708984" />
                  <Point X="21.42671875" Y="1.663706054688" />
                  <Point X="21.412806640625" Y="1.680286132812" />
                  <Point X="21.39786328125" Y="1.694590576172" />
                  <Point X="20.648140625" Y="2.269873046875" />
                  <Point X="20.918849609375" Y="2.733661132812" />
                  <Point X="20.929439453125" Y="2.747274414062" />
                  <Point X="21.24949609375" Y="3.158661376953" />
                  <Point X="21.767298828125" Y="2.859707519531" />
                  <Point X="21.79334375" Y="2.844670654297" />
                  <Point X="21.81227734375" Y="2.836339599609" />
                  <Point X="21.832919921875" Y="2.829831298828" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.8566171875" Y="2.825498046875" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959431640625" Y="2.818762451172" />
                  <Point X="21.980884765625" Y="2.8215859375" />
                  <Point X="22.00097265625" Y="2.826499755859" />
                  <Point X="22.01951953125" Y="2.835643554688" />
                  <Point X="22.037771484375" Y="2.847267333984" />
                  <Point X="22.053892578125" Y="2.860198730469" />
                  <Point X="22.056345703125" Y="2.862650146484" />
                  <Point X="22.1146484375" Y="2.920952636719" />
                  <Point X="22.127599609375" Y="2.937091552734" />
                  <Point X="22.139228515625" Y="2.955348876953" />
                  <Point X="22.148375" Y="2.973899902344" />
                  <Point X="22.1532890625" Y="2.993990966797" />
                  <Point X="22.15611328125" Y="3.015452880859" />
                  <Point X="22.15656640625" Y="3.036099853516" />
                  <Point X="22.156267578125" Y="3.039530761719" />
                  <Point X="22.14908203125" Y="3.121669433594" />
                  <Point X="22.145044921875" Y="3.141961669922" />
                  <Point X="22.13853515625" Y="3.162604736328" />
                  <Point X="22.130205078125" Y="3.181533447266" />
                  <Point X="21.81666796875" Y="3.724596679688" />
                  <Point X="22.299375" Y="4.094683349609" />
                  <Point X="22.31605078125" Y="4.103947753906" />
                  <Point X="22.83296484375" Y="4.391133789062" />
                  <Point X="22.94883203125" Y="4.240132324219" />
                  <Point X="22.9568046875" Y="4.229741699219" />
                  <Point X="22.97110546875" Y="4.214802734375" />
                  <Point X="22.987685546875" Y="4.200889160156" />
                  <Point X="23.004896484375" Y="4.189390625" />
                  <Point X="23.008693359375" Y="4.187414550781" />
                  <Point X="23.10011328125" Y="4.139823730469" />
                  <Point X="23.11938671875" Y="4.132330078125" />
                  <Point X="23.140296875" Y="4.126728027344" />
                  <Point X="23.160744140625" Y="4.123581542969" />
                  <Point X="23.18138671875" Y="4.124936523438" />
                  <Point X="23.20270703125" Y="4.128698242188" />
                  <Point X="23.2225703125" Y="4.134490722656" />
                  <Point X="23.226509765625" Y="4.136123046875" />
                  <Point X="23.32172265625" Y="4.175561523437" />
                  <Point X="23.33985546875" Y="4.185509765625" />
                  <Point X="23.3575859375" Y="4.197924316406" />
                  <Point X="23.373140625" Y="4.211565429688" />
                  <Point X="23.385373046875" Y="4.22825" />
                  <Point X="23.396193359375" Y="4.246994140625" />
                  <Point X="23.404521484375" Y="4.265920898438" />
                  <Point X="23.40580859375" Y="4.270002929688" />
                  <Point X="23.43680078125" Y="4.368297363281" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410143066406" />
                  <Point X="23.442271484375" Y="4.430824707031" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="24.0503671875" Y="4.80980859375" />
                  <Point X="24.07057421875" Y="4.812173339844" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.858392578125" Y="4.315069824219" />
                  <Point X="24.866095703125" Y="4.286317871094" />
                  <Point X="24.8788671875" Y="4.258129882812" />
                  <Point X="24.896724609375" Y="4.231401855469" />
                  <Point X="24.917880859375" Y="4.208811523438" />
                  <Point X="24.94517578125" Y="4.194220703125" />
                  <Point X="24.97561328125" Y="4.183887207031" />
                  <Point X="25.00615234375" Y="4.178844238281" />
                  <Point X="25.036689453125" Y="4.183885253906" />
                  <Point X="25.06712890625" Y="4.194217285156" />
                  <Point X="25.094423828125" Y="4.208806152344" />
                  <Point X="25.11558203125" Y="4.23139453125" />
                  <Point X="25.13344140625" Y="4.25812109375" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.307421875" Y="4.887937988281" />
                  <Point X="25.844041015625" Y="4.831738769531" />
                  <Point X="25.86076953125" Y="4.827700195313" />
                  <Point X="26.481029296875" Y="4.677950683594" />
                  <Point X="26.490490234375" Y="4.674519042969" />
                  <Point X="26.894640625" Y="4.527930664062" />
                  <Point X="26.90517578125" Y="4.523003417969" />
                  <Point X="27.2945703125" Y="4.340896972656" />
                  <Point X="27.304783203125" Y="4.334946777344" />
                  <Point X="27.680986328125" Y="4.115770019531" />
                  <Point X="27.690578125" Y="4.10894921875" />
                  <Point X="27.94326171875" Y="3.929254150391" />
                  <Point X="27.1819375" Y="2.610599609375" />
                  <Point X="27.147583984375" Y="2.551098144531" />
                  <Point X="27.141150390625" Y="2.537603271484" />
                  <Point X="27.132220703125" Y="2.512854980469" />
                  <Point X="27.111607421875" Y="2.43576953125" />
                  <Point X="27.108484375" Y="2.415624511719" />
                  <Point X="27.10748046875" Y="2.393954833984" />
                  <Point X="27.1080625" Y="2.378186279297" />
                  <Point X="27.116099609375" Y="2.311529296875" />
                  <Point X="27.121443359375" Y="2.289601318359" />
                  <Point X="27.1297109375" Y="2.26751171875" />
                  <Point X="27.14007421875" Y="2.247467773438" />
                  <Point X="27.141787109375" Y="2.244943603516" />
                  <Point X="27.183033203125" Y="2.184159179688" />
                  <Point X="27.19625390625" Y="2.168587646484" />
                  <Point X="27.212076171875" Y="2.153575195312" />
                  <Point X="27.224125" Y="2.143879150391" />
                  <Point X="27.28491015625" Y="2.102634521484" />
                  <Point X="27.304953125" Y="2.092272216797" />
                  <Point X="27.327041015625" Y="2.084006347656" />
                  <Point X="27.34895703125" Y="2.078664550781" />
                  <Point X="27.351724609375" Y="2.078330566406" />
                  <Point X="27.418380859375" Y="2.07029296875" />
                  <Point X="27.43914453125" Y="2.070073242188" />
                  <Point X="27.461248046875" Y="2.072267089844" />
                  <Point X="27.47640625" Y="2.075027099609" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.56528125" Y="2.099636962891" />
                  <Point X="27.58853515625" Y="2.110144775391" />
                  <Point X="28.967328125" Y="2.90619140625" />
                  <Point X="29.123279296875" Y="2.689456542969" />
                  <Point X="29.128625" Y="2.680620605469" />
                  <Point X="29.262201171875" Y="2.459884277344" />
                  <Point X="28.276015625" Y="1.703157226563" />
                  <Point X="28.23078515625" Y="1.668451782227" />
                  <Point X="28.219478515625" Y="1.658235473633" />
                  <Point X="28.201671875" Y="1.638622314453" />
                  <Point X="28.146193359375" Y="1.566246582031" />
                  <Point X="28.135478515625" Y="1.54857421875" />
                  <Point X="28.126150390625" Y="1.528554077148" />
                  <Point X="28.120771484375" Y="1.514017944336" />
                  <Point X="28.10010546875" Y="1.440122070312" />
                  <Point X="28.09665234375" Y="1.417815429688" />
                  <Point X="28.095837890625" Y="1.394236450195" />
                  <Point X="28.097744140625" Y="1.371749755859" />
                  <Point X="28.098447265625" Y="1.36834387207" />
                  <Point X="28.11541015625" Y="1.286134765625" />
                  <Point X="28.1217890625" Y="1.266408081055" />
                  <Point X="28.13090234375" Y="1.246118652344" />
                  <Point X="28.138197265625" Y="1.232829223633" />
                  <Point X="28.184337890625" Y="1.162696166992" />
                  <Point X="28.1988984375" Y="1.14544543457" />
                  <Point X="28.2161484375" Y="1.129352661133" />
                  <Point X="28.2343515625" Y="1.116033081055" />
                  <Point X="28.237123046875" Y="1.114473022461" />
                  <Point X="28.30398828125" Y="1.076833496094" />
                  <Point X="28.323388671875" Y="1.068595336914" />
                  <Point X="28.345119140625" Y="1.062101196289" />
                  <Point X="28.359873046875" Y="1.058942382812" />
                  <Point X="28.45028125" Y="1.046993896484" />
                  <Point X="28.462705078125" Y="1.046174926758" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.776837890625" Y="1.216636474609" />
                  <Point X="29.845939453125" Y="0.932789550781" />
                  <Point X="29.847623046875" Y="0.921975769043" />
                  <Point X="29.890865234375" Y="0.644238647461" />
                  <Point X="28.7684140625" Y="0.34347857666" />
                  <Point X="28.716580078125" Y="0.329589904785" />
                  <Point X="28.701982421875" Y="0.324368774414" />
                  <Point X="28.677859375" Y="0.312936248779" />
                  <Point X="28.589037109375" Y="0.26159588623" />
                  <Point X="28.572173828125" Y="0.2491824646" />
                  <Point X="28.5556484375" Y="0.233942108154" />
                  <Point X="28.545318359375" Y="0.222756820679" />
                  <Point X="28.492025390625" Y="0.154848999023" />
                  <Point X="28.480302734375" Y="0.135572387695" />
                  <Point X="28.47053125" Y="0.114116065979" />
                  <Point X="28.463689453125" Y="0.092636360168" />
                  <Point X="28.462943359375" Y="0.08875113678" />
                  <Point X="28.4451796875" Y="-0.004007497787" />
                  <Point X="28.44353515625" Y="-0.024969139099" />
                  <Point X="28.4442734375" Y="-0.047629619598" />
                  <Point X="28.44591796875" Y="-0.062404285431" />
                  <Point X="28.463681640625" Y="-0.155163070679" />
                  <Point X="28.47052734375" Y="-0.176662811279" />
                  <Point X="28.48030078125" Y="-0.198127029419" />
                  <Point X="28.4920234375" Y="-0.217406677246" />
                  <Point X="28.494236328125" Y="-0.220226638794" />
                  <Point X="28.547529296875" Y="-0.288134307861" />
                  <Point X="28.5625390625" Y="-0.30336328125" />
                  <Point X="28.5805390625" Y="-0.317915588379" />
                  <Point X="28.592724609375" Y="-0.326287872314" />
                  <Point X="28.681546875" Y="-0.377628234863" />
                  <Point X="28.692708984375" Y="-0.383138366699" />
                  <Point X="28.716580078125" Y="-0.392150024414" />
                  <Point X="29.891474609375" Y="-0.706961975098" />
                  <Point X="29.855025390625" Y="-0.948720275879" />
                  <Point X="29.852865234375" Y="-0.958186279297" />
                  <Point X="29.80117578125" Y="-1.184698852539" />
                  <Point X="28.48449609375" Y="-1.011354797363" />
                  <Point X="28.424384765625" Y="-1.003440917969" />
                  <Point X="28.403115234375" Y="-1.00304309082" />
                  <Point X="28.37873046875" Y="-1.005329650879" />
                  <Point X="28.367421875" Y="-1.007082214355" />
                  <Point X="28.193095703125" Y="-1.044972290039" />
                  <Point X="28.1627265625" Y="-1.055693725586" />
                  <Point X="28.132505859375" Y="-1.07505859375" />
                  <Point X="28.115630859375" Y="-1.091094970703" />
                  <Point X="28.108025390625" Y="-1.099222167969" />
                  <Point X="28.00265625" Y="-1.225947753906" />
                  <Point X="27.98662890625" Y="-1.250416015625" />
                  <Point X="27.9747578125" Y="-1.281100585938" />
                  <Point X="27.97053515625" Y="-1.302468261719" />
                  <Point X="27.9691328125" Y="-1.312180786133" />
                  <Point X="27.95403125" Y="-1.476295898438" />
                  <Point X="27.955111328125" Y="-1.508485473633" />
                  <Point X="27.96459375" Y="-1.543362670898" />
                  <Point X="27.975109375" Y="-1.56475769043" />
                  <Point X="27.98045703125" Y="-1.574228149414" />
                  <Point X="28.076931640625" Y="-1.724287597656" />
                  <Point X="28.0869375" Y="-1.73724230957" />
                  <Point X="28.110630859375" Y="-1.760909301758" />
                  <Point X="29.213123046875" Y="-2.606881347656" />
                  <Point X="29.124796875" Y="-2.749805175781" />
                  <Point X="29.120337890625" Y="-2.756142822266" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="27.854494140625" Y="-2.207853271484" />
                  <Point X="27.800955078125" Y="-2.176942871094" />
                  <Point X="27.780888671875" Y="-2.168262451172" />
                  <Point X="27.75616015625" Y="-2.160803955078" />
                  <Point X="27.745609375" Y="-2.158269287109" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.505974609375" Y="-2.119082275391" />
                  <Point X="27.470119140625" Y="-2.125611328125" />
                  <Point X="27.4472890625" Y="-2.134548583984" />
                  <Point X="27.43767578125" Y="-2.138943847656" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.24114453125" Y="-2.246129638672" />
                  <Point X="27.21814453125" Y="-2.270374267578" />
                  <Point X="27.205486328125" Y="-2.289600585938" />
                  <Point X="27.200765625" Y="-2.297596435547" />
                  <Point X="27.110052734375" Y="-2.46995703125" />
                  <Point X="27.098734375" Y="-2.500111328125" />
                  <Point X="27.094185546875" Y="-2.536549560547" />
                  <Point X="27.095955078125" Y="-2.561666503906" />
                  <Point X="27.097232421875" Y="-2.571874511719" />
                  <Point X="27.134703125" Y="-2.779349365234" />
                  <Point X="27.13898828125" Y="-2.795140136719" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.86128515625" Y="-4.054905273438" />
                  <Point X="27.781857421875" Y="-4.111639160156" />
                  <Point X="27.776857421875" Y="-4.114875" />
                  <Point X="27.701765625" Y="-4.16348046875" />
                  <Point X="26.79940234375" Y="-2.987498291016" />
                  <Point X="26.758548828125" Y="-2.934255615234" />
                  <Point X="26.7429140625" Y="-2.918217041016" />
                  <Point X="26.721787109375" Y="-2.901133300781" />
                  <Point X="26.713427734375" Y="-2.895093994141" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479744140625" Y="-2.749644287109" />
                  <Point X="26.443607421875" Y="-2.741946777344" />
                  <Point X="26.417857421875" Y="-2.741581787109" />
                  <Point X="26.407806640625" Y="-2.741971923828" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.155380859375" Y="-2.768534667969" />
                  <Point X="26.124455078125" Y="-2.782402099609" />
                  <Point X="26.104447265625" Y="-2.796116210938" />
                  <Point X="26.097421875" Y="-2.801427490234" />
                  <Point X="25.92461328125" Y="-2.945111328125" />
                  <Point X="25.90261328125" Y="-2.968641113281" />
                  <Point X="25.884302734375" Y="-3.001061767578" />
                  <Point X="25.876041015625" Y="-3.026257324219" />
                  <Point X="25.87348046875" Y="-3.035679443359" />
                  <Point X="25.821810546875" Y="-3.273395996094" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="25.975685546875" Y="-4.87008203125" />
                  <Point X="25.97105859375" Y="-4.870922363281" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941572265625" Y="-4.75263671875" />
                  <Point X="23.936765625" Y="-4.751399902344" />
                  <Point X="23.858755859375" Y="-4.731328613281" />
                  <Point X="23.877302734375" Y="-4.590445800781" />
                  <Point X="23.880029296875" Y="-4.56474609375" />
                  <Point X="23.880333984375" Y="-4.542545898438" />
                  <Point X="23.8798359375" Y="-4.531441894531" />
                  <Point X="23.876888671875" Y="-4.503024902344" />
                  <Point X="23.874251953125" Y="-4.485558105469" />
                  <Point X="23.81613671875" Y="-4.1933984375" />
                  <Point X="23.813138671875" Y="-4.182043945313" />
                  <Point X="23.80313671875" Y="-4.151866699219" />
                  <Point X="23.793505859375" Y="-4.131381347656" />
                  <Point X="23.773599609375" Y="-4.099552246094" />
                  <Point X="23.76205859375" Y="-4.084630859375" />
                  <Point X="23.74242578125" Y="-4.0638828125" />
                  <Point X="23.7296953125" Y="-4.051623046875" />
                  <Point X="23.505734375" Y="-3.855214355469" />
                  <Point X="23.496443359375" Y="-3.848032714844" />
                  <Point X="23.470142578125" Y="-3.830183105469" />
                  <Point X="23.45003125" Y="-3.81979296875" />
                  <Point X="23.414853515625" Y="-3.806655761719" />
                  <Point X="23.396609375" Y="-3.801842773438" />
                  <Point X="23.368400390625" Y="-3.797334716797" />
                  <Point X="23.350841796875" Y="-3.795360595703" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0418515625" Y="-3.7758359375" />
                  <Point X="23.010115234375" Y="-3.777686767578" />
                  <Point X="22.9878359375" Y="-3.781687744141" />
                  <Point X="22.951935546875" Y="-3.792679199219" />
                  <Point X="22.934533203125" Y="-3.799966064453" />
                  <Point X="22.90941015625" Y="-3.813561279297" />
                  <Point X="22.89427734375" Y="-3.822684326172" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.640322265625" Y="-3.992755371094" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032771484375" />
                  <Point X="22.589529296875" Y="-4.04162890625" />
                  <Point X="22.496796875" Y="-4.162478515625" />
                  <Point X="22.2524140625" Y="-4.011160888672" />
                  <Point X="22.245779296875" Y="-4.006052734375" />
                  <Point X="22.01913671875" Y="-3.831546142578" />
                  <Point X="22.628828125" Y="-2.775528564453" />
                  <Point X="22.658509765625" Y="-2.724120117188" />
                  <Point X="22.665146484375" Y="-2.710090576172" />
                  <Point X="22.676048828125" Y="-2.681130371094" />
                  <Point X="22.680314453125" Y="-2.666199707031" />
                  <Point X="22.6865859375" Y="-2.634681884766" />
                  <Point X="22.6883671875" Y="-2.619032226562" />
                  <Point X="22.689322265625" Y="-2.587654052734" />
                  <Point X="22.685080078125" Y="-2.556547363281" />
                  <Point X="22.675755859375" Y="-2.526571044922" />
                  <Point X="22.66984765625" Y="-2.511972900391" />
                  <Point X="22.654939453125" Y="-2.482443603516" />
                  <Point X="22.647095703125" Y="-2.469562011719" />
                  <Point X="22.629466796875" Y="-2.445202636719" />
                  <Point X="22.619681640625" Y="-2.433724853516" />
                  <Point X="22.603025390625" Y="-2.417068115234" />
                  <Point X="22.5911875" Y="-2.407023193359" />
                  <Point X="22.566029296875" Y="-2.388994140625" />
                  <Point X="22.552708984375" Y="-2.381010009766" />
                  <Point X="22.52387890625" Y="-2.366792724609" />
                  <Point X="22.5094375" Y="-2.361086425781" />
                  <Point X="22.4798203125" Y="-2.352102294922" />
                  <Point X="22.449134765625" Y="-2.3480625" />
                  <Point X="22.418201171875" Y="-2.349075195312" />
                  <Point X="22.40277734375" Y="-2.350849853516" />
                  <Point X="22.371251953125" Y="-2.357120605469" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372285888672" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="20.995986328125" Y="-2.740596191406" />
                  <Point X="20.99122265625" Y="-2.732607666016" />
                  <Point X="20.818734375" Y="-2.443372314453" />
                  <Point X="21.8999453125" Y="-1.61373046875" />
                  <Point X="21.951876953125" Y="-1.573881835938" />
                  <Point X="21.9638984375" Y="-1.56289831543" />
                  <Point X="21.985890625" Y="-1.539038085938" />
                  <Point X="21.995861328125" Y="-1.526161499023" />
                  <Point X="22.014134765625" Y="-1.497842651367" />
                  <Point X="22.021314453125" Y="-1.48448425293" />
                  <Point X="22.03345703125" Y="-1.456792602539" />
                  <Point X="22.038419921875" Y="-1.442458984375" />
                  <Point X="22.04581640625" Y="-1.413897827148" />
                  <Point X="22.04844921875" Y="-1.398793945312" />
                  <Point X="22.051251953125" Y="-1.368362670898" />
                  <Point X="22.051421875" Y="-1.353035400391" />
                  <Point X="22.0492109375" Y="-1.321358642578" />
                  <Point X="22.04684765625" Y="-1.305926025391" />
                  <Point X="22.039625" Y="-1.275653808594" />
                  <Point X="22.027541015625" Y="-1.246971557617" />
                  <Point X="22.010921875" Y="-1.220657958984" />
                  <Point X="22.00152734375" Y="-1.208186767578" />
                  <Point X="21.979626953125" Y="-1.183608886719" />
                  <Point X="21.968982421875" Y="-1.173387207031" />
                  <Point X="21.9462421875" Y="-1.154716064453" />
                  <Point X="21.934146484375" Y="-1.146266845703" />
                  <Point X="21.908734375" Y="-1.131309936523" />
                  <Point X="21.8945703125" Y="-1.124483032227" />
                  <Point X="21.86530859375" Y="-1.113258789062" />
                  <Point X="21.8502109375" Y="-1.108861206055" />
                  <Point X="21.81832421875" Y="-1.102379150391" />
                  <Point X="21.802705078125" Y="-1.100532592773" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196411133" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.259240234375" Y="-0.974118774414" />
                  <Point X="20.25798046875" Y="-0.965312683105" />
                  <Point X="20.21355078125" Y="-0.654654296875" />
                  <Point X="21.4325546875" Y="-0.328022460938" />
                  <Point X="21.491712890625" Y="-0.312171234131" />
                  <Point X="21.500080078125" Y="-0.309509399414" />
                  <Point X="21.527685546875" Y="-0.298463592529" />
                  <Point X="21.557486328125" Y="-0.28284274292" />
                  <Point X="21.567548828125" Y="-0.276745788574" />
                  <Point X="21.5941953125" Y="-0.258251586914" />
                  <Point X="21.606740234375" Y="-0.247842346191" />
                  <Point X="21.629865234375" Y="-0.225033035278" />
                  <Point X="21.6404453125" Y="-0.212632980347" />
                  <Point X="21.66005859375" Y="-0.185187393188" />
                  <Point X="21.667861328125" Y="-0.172185714722" />
                  <Point X="21.68130078125" Y="-0.145106323242" />
                  <Point X="21.6869375" Y="-0.131028900146" />
                  <Point X="21.695818359375" Y="-0.102410232544" />
                  <Point X="21.69909765625" Y="-0.08792024231" />
                  <Point X="21.703357421875" Y="-0.058615371704" />
                  <Point X="21.704337890625" Y="-0.043800640106" />
                  <Point X="21.70396484375" Y="-0.012961535454" />
                  <Point X="21.7028125" Y="0.000690518022" />
                  <Point X="21.698552734375" Y="0.027697528839" />
                  <Point X="21.6954453125" Y="0.041052635193" />
                  <Point X="21.686568359375" Y="0.069658081055" />
                  <Point X="21.68039453125" Y="0.08480305481" />
                  <Point X="21.665541015625" Y="0.113808746338" />
                  <Point X="21.656861328125" Y="0.127669914246" />
                  <Point X="21.636494140625" Y="0.154705749512" />
                  <Point X="21.626470703125" Y="0.166012542725" />
                  <Point X="21.604759765625" Y="0.186894943237" />
                  <Point X="21.593072265625" Y="0.196470275879" />
                  <Point X="21.56644140625" Y="0.214953613281" />
                  <Point X="21.559513671875" Y="0.219331008911" />
                  <Point X="21.5343828125" Y="0.232835479736" />
                  <Point X="21.50343359375" Y="0.245635925293" />
                  <Point X="21.491712890625" Y="0.24961114502" />
                  <Point X="20.2145546875" Y="0.591825073242" />
                  <Point X="20.268671875" Y="0.957540893555" />
                  <Point X="20.271205078125" Y="0.966892333984" />
                  <Point X="20.366412109375" Y="1.318237182617" />
                  <Point X="21.179529296875" Y="1.211188476562" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.23226953125" Y="1.204815307617" />
                  <Point X="21.2529609375" Y="1.204365112305" />
                  <Point X="21.263318359375" Y="1.204705200195" />
                  <Point X="21.284888671875" Y="1.206594726562" />
                  <Point X="21.295140625" Y="1.208059692383" />
                  <Point X="21.31542578125" Y="1.212096923828" />
                  <Point X="21.327904296875" Y="1.215440795898" />
                  <Point X="21.38685546875" Y="1.234027587891" />
                  <Point X="21.39655078125" Y="1.237676635742" />
                  <Point X="21.415484375" Y="1.2460078125" />
                  <Point X="21.42472265625" Y="1.250689331055" />
                  <Point X="21.443466796875" Y="1.261511108398" />
                  <Point X="21.452142578125" Y="1.267172851562" />
                  <Point X="21.46882421875" Y="1.279404907227" />
                  <Point X="21.484076171875" Y="1.293380371094" />
                  <Point X="21.49771484375" Y="1.308933349609" />
                  <Point X="21.504107421875" Y="1.317081054688" />
                  <Point X="21.516521484375" Y="1.334811157227" />
                  <Point X="21.5219921875" Y="1.343609863281" />
                  <Point X="21.531943359375" Y="1.361750976562" />
                  <Point X="21.53740625" Y="1.373466186523" />
                  <Point X="21.5610703125" Y="1.430598022461" />
                  <Point X="21.564501953125" Y="1.440355712891" />
                  <Point X="21.570287109375" Y="1.460199707031" />
                  <Point X="21.572640625" Y="1.470286132812" />
                  <Point X="21.576400390625" Y="1.491601318359" />
                  <Point X="21.577640625" Y="1.501888916016" />
                  <Point X="21.578994140625" Y="1.522535766602" />
                  <Point X="21.578091796875" Y="1.543206054688" />
                  <Point X="21.574943359375" Y="1.563655883789" />
                  <Point X="21.572810546875" Y="1.573794677734" />
                  <Point X="21.56720703125" Y="1.594701171875" />
                  <Point X="21.563986328125" Y="1.604539794922" />
                  <Point X="21.556494140625" Y="1.623804931641" />
                  <Point X="21.551037109375" Y="1.635509399414" />
                  <Point X="21.522482421875" Y="1.690361450195" />
                  <Point X="21.517205078125" Y="1.699276367188" />
                  <Point X="21.5057109375" Y="1.716480712891" />
                  <Point X="21.499494140625" Y="1.724770263672" />
                  <Point X="21.48558203125" Y="1.741350341797" />
                  <Point X="21.478498046875" Y="1.748912475586" />
                  <Point X="21.4635546875" Y="1.763216796875" />
                  <Point X="21.4556953125" Y="1.769959106445" />
                  <Point X="20.77238671875" Y="2.294280517578" />
                  <Point X="20.997712890625" Y="2.680317382812" />
                  <Point X="21.004423828125" Y="2.688944091797" />
                  <Point X="21.273662109375" Y="3.035011962891" />
                  <Point X="21.719798828125" Y="2.777435058594" />
                  <Point X="21.74584375" Y="2.762398193359" />
                  <Point X="21.75508203125" Y="2.757716308594" />
                  <Point X="21.774015625" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736083984" />
                  <Point X="21.804353515625" Y="2.739227783203" />
                  <Point X="21.814388671875" Y="2.73665625" />
                  <Point X="21.83467578125" Y="2.732621337891" />
                  <Point X="21.848337890625" Y="2.730859619141" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940826171875" Y="2.723334228516" />
                  <Point X="21.961501953125" Y="2.723784912109" />
                  <Point X="21.971828125" Y="2.724574707031" />
                  <Point X="21.99328125" Y="2.727398193359" />
                  <Point X="22.00345703125" Y="2.729306640625" />
                  <Point X="22.023544921875" Y="2.734220458984" />
                  <Point X="22.04298046875" Y="2.741292236328" />
                  <Point X="22.06152734375" Y="2.750436035156" />
                  <Point X="22.07055078125" Y="2.755513427734" />
                  <Point X="22.088802734375" Y="2.767137207031" />
                  <Point X="22.09721484375" Y="2.773162353516" />
                  <Point X="22.1133359375" Y="2.78609375" />
                  <Point X="22.123521484375" Y="2.795474853516" />
                  <Point X="22.18182421875" Y="2.85377734375" />
                  <Point X="22.1887421875" Y="2.861494628906" />
                  <Point X="22.201693359375" Y="2.877633544922" />
                  <Point X="22.2077265625" Y="2.886055175781" />
                  <Point X="22.21935546875" Y="2.9043125" />
                  <Point X="22.224435546875" Y="2.913338378906" />
                  <Point X="22.23358203125" Y="2.931889404297" />
                  <Point X="22.240654296875" Y="2.951329345703" />
                  <Point X="22.245568359375" Y="2.971420410156" />
                  <Point X="22.2474765625" Y="2.981596679688" />
                  <Point X="22.25030078125" Y="3.00305859375" />
                  <Point X="22.25108984375" Y="3.013368408203" />
                  <Point X="22.25154296875" Y="3.034015380859" />
                  <Point X="22.25090625" Y="3.047809814453" />
                  <Point X="22.243720703125" Y="3.129948486328" />
                  <Point X="22.242255859375" Y="3.140206298828" />
                  <Point X="22.23821875" Y="3.160498535156" />
                  <Point X="22.235646484375" Y="3.170532958984" />
                  <Point X="22.22913671875" Y="3.191176025391" />
                  <Point X="22.22548828125" Y="3.200870361328" />
                  <Point X="22.217158203125" Y="3.219799072266" />
                  <Point X="22.2124765625" Y="3.229033447266" />
                  <Point X="21.94061328125" Y="3.699915527344" />
                  <Point X="22.35162890625" Y="4.015037109375" />
                  <Point X="22.3621875" Y="4.020903076172" />
                  <Point X="22.8074765625" Y="4.268295410156" />
                  <Point X="22.873462890625" Y="4.182300292969" />
                  <Point X="22.8881796875" Y="4.164048339844" />
                  <Point X="22.90248046875" Y="4.149109375" />
                  <Point X="22.910037109375" Y="4.14203125" />
                  <Point X="22.9266171875" Y="4.128117675781" />
                  <Point X="22.93491015625" Y="4.121896484375" />
                  <Point X="22.95212109375" Y="4.110397949219" />
                  <Point X="22.9648359375" Y="4.10314453125" />
                  <Point X="23.056255859375" Y="4.055553466797" />
                  <Point X="23.0656875" Y="4.051280761719" />
                  <Point X="23.0849609375" Y="4.043787109375" />
                  <Point X="23.094802734375" Y="4.040566162109" />
                  <Point X="23.115712890625" Y="4.034964111328" />
                  <Point X="23.12584765625" Y="4.032833251953" />
                  <Point X="23.146294921875" Y="4.029686767578" />
                  <Point X="23.166966796875" Y="4.028785644531" />
                  <Point X="23.187609375" Y="4.030140625" />
                  <Point X="23.197892578125" Y="4.031381591797" />
                  <Point X="23.219212890625" Y="4.035143310547" />
                  <Point X="23.229302734375" Y="4.037497070312" />
                  <Point X="23.249166015625" Y="4.043289550781" />
                  <Point X="23.262865234375" Y="4.048354492188" />
                  <Point X="23.358078125" Y="4.08779296875" />
                  <Point X="23.36741796875" Y="4.092272949219" />
                  <Point X="23.38555078125" Y="4.102221191406" />
                  <Point X="23.39434375" Y="4.107689453125" />
                  <Point X="23.41207421875" Y="4.120104003906" />
                  <Point X="23.420224609375" Y="4.126499511719" />
                  <Point X="23.435779296875" Y="4.140140625" />
                  <Point X="23.449755859375" Y="4.15539453125" />
                  <Point X="23.46198828125" Y="4.172079101562" />
                  <Point X="23.4676484375" Y="4.180755371094" />
                  <Point X="23.47846875" Y="4.199499511719" />
                  <Point X="23.4831484375" Y="4.208732421875" />
                  <Point X="23.4914765625" Y="4.227659179688" />
                  <Point X="23.496412109375" Y="4.241435058594" />
                  <Point X="23.527404296875" Y="4.339729492188" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370049804688" />
                  <Point X="23.535474609375" Y="4.380301757812" />
                  <Point X="23.537361328125" Y="4.401861328125" />
                  <Point X="23.53769921875" Y="4.41221484375" />
                  <Point X="23.537248046875" Y="4.432896484375" />
                  <Point X="23.536458984375" Y="4.443225097656" />
                  <Point X="23.520734375" Y="4.562654785156" />
                  <Point X="24.068828125" Y="4.716320800781" />
                  <Point X="24.0816171875" Y="4.717817382812" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.76662890625" Y="4.290481933594" />
                  <Point X="24.77433203125" Y="4.261729980469" />
                  <Point X="24.7795625" Y="4.247111328125" />
                  <Point X="24.792333984375" Y="4.218923339844" />
                  <Point X="24.799875" Y="4.205354003906" />
                  <Point X="24.817732421875" Y="4.178625976563" />
                  <Point X="24.827384765625" Y="4.166463867188" />
                  <Point X="24.848541015625" Y="4.143873535156" />
                  <Point X="24.873095703125" Y="4.125030761719" />
                  <Point X="24.900390625" Y="4.110439941406" />
                  <Point X="24.914634765625" Y="4.104263671875" />
                  <Point X="24.945072265625" Y="4.093930175781" />
                  <Point X="24.960134765625" Y="4.090156494141" />
                  <Point X="24.990673828125" Y="4.085113525391" />
                  <Point X="25.021625" Y="4.085112792969" />
                  <Point X="25.052162109375" Y="4.090153808594" />
                  <Point X="25.067224609375" Y="4.093926269531" />
                  <Point X="25.0976640625" Y="4.104258300781" />
                  <Point X="25.11191015625" Y="4.110434082031" />
                  <Point X="25.139205078125" Y="4.125022949219" />
                  <Point X="25.1637578125" Y="4.143861816406" />
                  <Point X="25.184916015625" Y="4.166450195312" />
                  <Point X="25.1945703125" Y="4.178612792969" />
                  <Point X="25.2124296875" Y="4.205339355469" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.24710546875" />
                  <Point X="25.23798046875" Y="4.261727050781" />
                  <Point X="25.378193359375" Y="4.785006835938" />
                  <Point X="25.827873046875" Y="4.737912597656" />
                  <Point X="25.838474609375" Y="4.735353027344" />
                  <Point X="26.45360546875" Y="4.586841796875" />
                  <Point X="26.45809765625" Y="4.585212402344" />
                  <Point X="26.858251953125" Y="4.440073730469" />
                  <Point X="26.8649296875" Y="4.436950195312" />
                  <Point X="27.25044140625" Y="4.256659667969" />
                  <Point X="27.256958984375" Y="4.252862304688" />
                  <Point X="27.629443359375" Y="4.035851806641" />
                  <Point X="27.6355234375" Y="4.031528320312" />
                  <Point X="27.817783203125" Y="3.901915527344" />
                  <Point X="27.099666015625" Y="2.658099609375" />
                  <Point X="27.0653125" Y="2.598598144531" />
                  <Point X="27.061830078125" Y="2.59198046875" />
                  <Point X="27.0517890625" Y="2.569846435547" />
                  <Point X="27.042859375" Y="2.545098144531" />
                  <Point X="27.0404453125" Y="2.537396484375" />
                  <Point X="27.01983203125" Y="2.460311035156" />
                  <Point X="27.017728515625" Y="2.450323242188" />
                  <Point X="27.01460546875" Y="2.430178222656" />
                  <Point X="27.0135859375" Y="2.420020996094" />
                  <Point X="27.01258203125" Y="2.398351318359" />
                  <Point X="27.012544921875" Y="2.390450683594" />
                  <Point X="27.01374609375" Y="2.366814208984" />
                  <Point X="27.021783203125" Y="2.300157226562" />
                  <Point X="27.02380078125" Y="2.289036376953" />
                  <Point X="27.02914453125" Y="2.267108398438" />
                  <Point X="27.032470703125" Y="2.256301269531" />
                  <Point X="27.04073828125" Y="2.234211669922" />
                  <Point X="27.045322265625" Y="2.223880859375" />
                  <Point X="27.055685546875" Y="2.203836914063" />
                  <Point X="27.063177734375" Y="2.191599609375" />
                  <Point X="27.104423828125" Y="2.130815185547" />
                  <Point X="27.110615234375" Y="2.122673339844" />
                  <Point X="27.1238359375" Y="2.107101806641" />
                  <Point X="27.130865234375" Y="2.099672119141" />
                  <Point X="27.1466875" Y="2.084659667969" />
                  <Point X="27.152517578125" Y="2.079563720703" />
                  <Point X="27.17078515625" Y="2.065267578125" />
                  <Point X="27.2315703125" Y="2.024022827148" />
                  <Point X="27.24128125" Y="2.018245605469" />
                  <Point X="27.26132421875" Y="2.007883422852" />
                  <Point X="27.27165625" Y="2.003298339844" />
                  <Point X="27.293744140625" Y="1.995032592773" />
                  <Point X="27.304544921875" Y="1.991708496094" />
                  <Point X="27.3264609375" Y="1.986366699219" />
                  <Point X="27.34034375" Y="1.984014892578" />
                  <Point X="27.407" Y="1.975977294922" />
                  <Point X="27.417375" Y="1.975298339844" />
                  <Point X="27.438138671875" Y="1.975078613281" />
                  <Point X="27.44852734375" Y="1.975537719727" />
                  <Point X="27.470630859375" Y="1.977731445312" />
                  <Point X="27.478265625" Y="1.978803710938" />
                  <Point X="27.500947265625" Y="1.983251831055" />
                  <Point X="27.578033203125" Y="2.003865356445" />
                  <Point X="27.5839921875" Y="2.005669433594" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572998047" />
                  <Point X="27.63603515625" Y="2.027872314453" />
                  <Point X="28.940408203125" Y="2.780952148438" />
                  <Point X="29.04395703125" Y="2.637041992188" />
                  <Point X="29.04734765625" Y="2.631436767578" />
                  <Point X="29.13688671875" Y="2.483471923828" />
                  <Point X="28.21818359375" Y="1.778525634766" />
                  <Point X="28.172953125" Y="1.7438203125" />
                  <Point X="28.16709375" Y="1.738939575195" />
                  <Point X="28.149142578125" Y="1.722093261719" />
                  <Point X="28.1313359375" Y="1.702480102539" />
                  <Point X="28.126275390625" Y="1.696416992188" />
                  <Point X="28.070796875" Y="1.624041259766" />
                  <Point X="28.064958984375" Y="1.615499755859" />
                  <Point X="28.054244140625" Y="1.597827392578" />
                  <Point X="28.0493671875" Y="1.588696777344" />
                  <Point X="28.0400390625" Y="1.568676635742" />
                  <Point X="28.0370546875" Y="1.561522827148" />
                  <Point X="28.02928125" Y="1.539604248047" />
                  <Point X="28.008615234375" Y="1.465708374023" />
                  <Point X="28.006224609375" Y="1.454655151367" />
                  <Point X="28.002771484375" Y="1.432348510742" />
                  <Point X="28.001708984375" Y="1.421095092773" />
                  <Point X="28.00089453125" Y="1.397515991211" />
                  <Point X="28.001177734375" Y="1.386211914062" />
                  <Point X="28.003083984375" Y="1.363725219727" />
                  <Point X="28.00541015625" Y="1.349137817383" />
                  <Point X="28.02237109375" Y="1.266937011719" />
                  <Point X="28.02501953125" Y="1.256905395508" />
                  <Point X="28.0313984375" Y="1.237178710938" />
                  <Point X="28.03512890625" Y="1.227483642578" />
                  <Point X="28.0442421875" Y="1.207194335938" />
                  <Point X="28.047625" Y="1.200405029297" />
                  <Point X="28.05883203125" Y="1.180615356445" />
                  <Point X="28.10497265625" Y="1.110482299805" />
                  <Point X="28.111740234375" Y="1.101420532227" />
                  <Point X="28.12630078125" Y="1.084169799805" />
                  <Point X="28.13409375" Y="1.075980834961" />
                  <Point X="28.15134375" Y="1.059887939453" />
                  <Point X="28.160048828125" Y="1.052685180664" />
                  <Point X="28.178251953125" Y="1.039365600586" />
                  <Point X="28.1905234375" Y="1.03168737793" />
                  <Point X="28.257388671875" Y="0.994047790527" />
                  <Point X="28.266857421875" Y="0.989390686035" />
                  <Point X="28.2862578125" Y="0.981152526855" />
                  <Point X="28.2961875" Y="0.977573120117" />
                  <Point X="28.31791796875" Y="0.971078857422" />
                  <Point X="28.32523046875" Y="0.969206359863" />
                  <Point X="28.34742578125" Y="0.964761352539" />
                  <Point X="28.437833984375" Y="0.952812805176" />
                  <Point X="28.444033203125" Y="0.952199584961" />
                  <Point X="28.46571875" Y="0.951222839355" />
                  <Point X="28.49121875" Y="0.952032409668" />
                  <Point X="28.50060546875" Y="0.952797180176" />
                  <Point X="29.704701171875" Y="1.111319702148" />
                  <Point X="29.7526875" Y="0.914206481934" />
                  <Point X="29.75375390625" Y="0.90736126709" />
                  <Point X="29.78387109375" Y="0.713920959473" />
                  <Point X="28.743826171875" Y="0.435241577148" />
                  <Point X="28.6919921875" Y="0.421352783203" />
                  <Point X="28.6845859375" Y="0.419040496826" />
                  <Point X="28.661296875" Y="0.410215881348" />
                  <Point X="28.637173828125" Y="0.398783355713" />
                  <Point X="28.630318359375" Y="0.395185028076" />
                  <Point X="28.54149609375" Y="0.34384475708" />
                  <Point X="28.53271875" Y="0.338102539062" />
                  <Point X="28.51585546875" Y="0.325689117432" />
                  <Point X="28.50776953125" Y="0.319017883301" />
                  <Point X="28.491244140625" Y="0.303777648926" />
                  <Point X="28.485857421875" Y="0.29839630127" />
                  <Point X="28.470583984375" Y="0.281406921387" />
                  <Point X="28.417291015625" Y="0.213499038696" />
                  <Point X="28.41085546875" Y="0.204210403442" />
                  <Point X="28.3991328125" Y="0.18493371582" />
                  <Point X="28.393845703125" Y="0.17494581604" />
                  <Point X="28.38407421875" Y="0.153489425659" />
                  <Point X="28.38001171875" Y="0.142948501587" />
                  <Point X="28.373169921875" Y="0.121468772888" />
                  <Point X="28.369638671875" Y="0.106619407654" />
                  <Point X="28.351875" Y="0.013860736847" />
                  <Point X="28.350470703125" Y="0.003422801495" />
                  <Point X="28.348826171875" Y="-0.017538824081" />
                  <Point X="28.3485859375" Y="-0.028062662125" />
                  <Point X="28.34932421875" Y="-0.050723049164" />
                  <Point X="28.349857421875" Y="-0.058138885498" />
                  <Point X="28.35261328125" Y="-0.080272399902" />
                  <Point X="28.370376953125" Y="-0.173031219482" />
                  <Point X="28.37316015625" Y="-0.183986068726" />
                  <Point X="28.380005859375" Y="-0.205485855103" />
                  <Point X="28.384068359375" Y="-0.216030807495" />
                  <Point X="28.393841796875" Y="-0.237494918823" />
                  <Point X="28.39912890625" Y="-0.247482681274" />
                  <Point X="28.4108515625" Y="-0.266762329102" />
                  <Point X="28.4195" Y="-0.278873901367" />
                  <Point X="28.47279296875" Y="-0.346781494141" />
                  <Point X="28.479869140625" Y="-0.354820800781" />
                  <Point X="28.49487890625" Y="-0.370049743652" />
                  <Point X="28.5028125" Y="-0.377239837646" />
                  <Point X="28.5208125" Y="-0.391792236328" />
                  <Point X="28.5267421875" Y="-0.396215423584" />
                  <Point X="28.54518359375" Y="-0.408536682129" />
                  <Point X="28.634005859375" Y="-0.459876983643" />
                  <Point X="28.63949609375" Y="-0.462814239502" />
                  <Point X="28.65915625" Y="-0.472016052246" />
                  <Point X="28.68302734375" Y="-0.481027648926" />
                  <Point X="28.6919921875" Y="-0.483913024902" />
                  <Point X="29.78487890625" Y="-0.776751220703" />
                  <Point X="29.761619140625" Y="-0.931032775879" />
                  <Point X="29.76024609375" Y="-0.937050415039" />
                  <Point X="29.727802734375" Y="-1.079219482422" />
                  <Point X="28.496896484375" Y="-0.917167541504" />
                  <Point X="28.43678515625" Y="-0.909253662109" />
                  <Point X="28.426162109375" Y="-0.908457458496" />
                  <Point X="28.404892578125" Y="-0.908059753418" />
                  <Point X="28.39424609375" Y="-0.908458068848" />
                  <Point X="28.369861328125" Y="-0.910744628906" />
                  <Point X="28.347244140625" Y="-0.914249755859" />
                  <Point X="28.17291796875" Y="-0.952139831543" />
                  <Point X="28.161470703125" Y="-0.955390930176" />
                  <Point X="28.1311015625" Y="-0.966112304688" />
                  <Point X="28.11147265625" Y="-0.975706359863" />
                  <Point X="28.081251953125" Y="-0.995071166992" />
                  <Point X="28.067064453125" Y="-1.006194091797" />
                  <Point X="28.050189453125" Y="-1.022230529785" />
                  <Point X="28.034978515625" Y="-1.038484863281" />
                  <Point X="27.929609375" Y="-1.165210571289" />
                  <Point X="27.9231875" Y="-1.173893310547" />
                  <Point X="27.90716015625" Y="-1.198361694336" />
                  <Point X="27.898029296875" Y="-1.216138671875" />
                  <Point X="27.886158203125" Y="-1.246823242188" />
                  <Point X="27.881560546875" Y="-1.262682861328" />
                  <Point X="27.877337890625" Y="-1.28405078125" />
                  <Point X="27.874533203125" Y="-1.303475830078" />
                  <Point X="27.859431640625" Y="-1.467590942383" />
                  <Point X="27.859083984375" Y="-1.479481689453" />
                  <Point X="27.8601640625" Y="-1.511671264648" />
                  <Point X="27.863439453125" Y="-1.533409301758" />
                  <Point X="27.872921875" Y="-1.568286499023" />
                  <Point X="27.8793359375" Y="-1.585267089844" />
                  <Point X="27.8898515625" Y="-1.606662109375" />
                  <Point X="27.900546875" Y="-1.625603149414" />
                  <Point X="27.997021484375" Y="-1.775662475586" />
                  <Point X="28.00174609375" Y="-1.782358398438" />
                  <Point X="28.019798828125" Y="-1.804454833984" />
                  <Point X="28.0434921875" Y="-1.828121826172" />
                  <Point X="28.052798828125" Y="-1.836277832031" />
                  <Point X="29.087171875" Y="-2.629979980469" />
                  <Point X="29.045484375" Y="-2.697436523438" />
                  <Point X="29.042642578125" Y="-2.701478515625" />
                  <Point X="29.001275390625" Y="-2.760252441406" />
                  <Point X="27.901994140625" Y="-2.125580810547" />
                  <Point X="27.848455078125" Y="-2.094670410156" />
                  <Point X="27.838671875" Y="-2.089751220703" />
                  <Point X="27.81860546875" Y="-2.081070800781" />
                  <Point X="27.808322265625" Y="-2.077309570312" />
                  <Point X="27.78359375" Y="-2.069851074219" />
                  <Point X="27.7624921875" Y="-2.064781738281" />
                  <Point X="27.555017578125" Y="-2.027311889648" />
                  <Point X="27.543201171875" Y="-2.025934692383" />
                  <Point X="27.511041015625" Y="-2.024217407227" />
                  <Point X="27.488955078125" Y="-2.025619140625" />
                  <Point X="27.453099609375" Y="-2.03214831543" />
                  <Point X="27.43548828125" Y="-2.037148193359" />
                  <Point X="27.412658203125" Y="-2.046085571289" />
                  <Point X="27.393431640625" Y="-2.054875976563" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.211814453125" Y="-2.151153564453" />
                  <Point X="27.187642578125" Y="-2.167627197266" />
                  <Point X="27.17222265625" Y="-2.180746826172" />
                  <Point X="27.14922265625" Y="-2.204991455078" />
                  <Point X="27.138796875" Y="-2.218133789062" />
                  <Point X="27.126138671875" Y="-2.237360107422" />
                  <Point X="27.116697265625" Y="-2.253351806641" />
                  <Point X="27.025984375" Y="-2.425712402344" />
                  <Point X="27.021111328125" Y="-2.436573242188" />
                  <Point X="27.00979296875" Y="-2.466727539062" />
                  <Point X="27.004466796875" Y="-2.488343261719" />
                  <Point X="26.99991796875" Y="-2.524781494141" />
                  <Point X="26.999419921875" Y="-2.543225830078" />
                  <Point X="27.001189453125" Y="-2.568342773438" />
                  <Point X="27.003744140625" Y="-2.588758789062" />
                  <Point X="27.04121484375" Y="-2.796233642578" />
                  <Point X="27.04301953125" Y="-2.804229736328" />
                  <Point X="27.05123828125" Y="-2.831540771484" />
                  <Point X="27.064072265625" Y="-2.862479248047" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.735896484375" Y="-4.027723388672" />
                  <Point X="27.723755859375" Y="-4.036082275391" />
                  <Point X="26.874771484375" Y="-2.929666015625" />
                  <Point X="26.83391796875" Y="-2.876423339844" />
                  <Point X="26.82657421875" Y="-2.867942382812" />
                  <Point X="26.810939453125" Y="-2.851903808594" />
                  <Point X="26.8026484375" Y="-2.844346191406" />
                  <Point X="26.781521484375" Y="-2.827262451172" />
                  <Point X="26.764802734375" Y="-2.815183837891" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.549783203125" Y="-2.677832519531" />
                  <Point X="26.5207265625" Y="-2.663938476562" />
                  <Point X="26.49953515625" Y="-2.656728759766" />
                  <Point X="26.4633984375" Y="-2.64903125" />
                  <Point X="26.444953125" Y="-2.646956298828" />
                  <Point X="26.419203125" Y="-2.646591308594" />
                  <Point X="26.3991015625" Y="-2.647371582031" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.164626953125" Y="-2.669564941406" />
                  <Point X="26.135994140625" Y="-2.675533935547" />
                  <Point X="26.11651171875" Y="-2.681850585938" />
                  <Point X="26.0855859375" Y="-2.695718017578" />
                  <Point X="26.070744140625" Y="-2.704042724609" />
                  <Point X="26.050736328125" Y="-2.717756835938" />
                  <Point X="26.036685546875" Y="-2.728379394531" />
                  <Point X="25.863876953125" Y="-2.872063232422" />
                  <Point X="25.855220703125" Y="-2.880229980469" />
                  <Point X="25.833220703125" Y="-2.903759765625" />
                  <Point X="25.81989453125" Y="-2.921923095703" />
                  <Point X="25.801583984375" Y="-2.95434375" />
                  <Point X="25.79403125" Y="-2.971461669922" />
                  <Point X="25.78576953125" Y="-2.996657226562" />
                  <Point X="25.7806484375" Y="-3.015501464844" />
                  <Point X="25.728978515625" Y="-3.253218017578" />
                  <Point X="25.7275859375" Y="-3.261286865234" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520019531" />
                  <Point X="25.833087890625" Y="-4.152318847656" />
                  <Point X="25.6710390625" Y="-3.547542236328" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.6514375" Y="-3.477061523438" />
                  <Point X="25.642896484375" Y="-3.455835449219" />
                  <Point X="25.637986328125" Y="-3.445484619141" />
                  <Point X="25.62423828125" Y="-3.420510742188" />
                  <Point X="25.6138828125" Y="-3.403803710938" />
                  <Point X="25.456681640625" Y="-3.177308837891" />
                  <Point X="25.449296875" Y="-3.167976074219" />
                  <Point X="25.42776953125" Y="-3.144021728516" />
                  <Point X="25.410763671875" Y="-3.129124023438" />
                  <Point X="25.3798984375" Y="-3.108000488281" />
                  <Point X="25.3634140625" Y="-3.098971679688" />
                  <Point X="25.33857421875" Y="-3.088411132812" />
                  <Point X="25.3205546875" Y="-3.081803222656" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.066814453125" Y="-3.0036953125" />
                  <Point X="25.038076171875" Y="-2.998252441406" />
                  <Point X="25.017443359375" Y="-2.996638916016" />
                  <Point X="24.983146484375" Y="-2.997706054688" />
                  <Point X="24.966046875" Y="-2.99980078125" />
                  <Point X="24.941125" Y="-3.005182861328" />
                  <Point X="24.9249140625" Y="-3.009440185547" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.67053515625" Y="-3.089170654297" />
                  <Point X="24.641212890625" Y="-3.102486572266" />
                  <Point X="24.621767578125" Y="-3.114311523438" />
                  <Point X="24.592134765625" Y="-3.137616210938" />
                  <Point X="24.578484375" Y="-3.150754150391" />
                  <Point X="24.560439453125" Y="-3.171978027344" />
                  <Point X="24.549103515625" Y="-3.186715332031" />
                  <Point X="24.391904296875" Y="-3.413210449219" />
                  <Point X="24.387529296875" Y="-3.420134277344" />
                  <Point X="24.374029296875" Y="-3.445255371094" />
                  <Point X="24.361224609375" Y="-3.476209472656" />
                  <Point X="24.35724609375" Y="-3.487935791016" />
                  <Point X="24.01457421875" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.667032480237" Y="-3.962159043309" />
                  <Point X="27.686942341397" Y="-3.942932313879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.945180418009" Y="-2.727865927261" />
                  <Point X="29.064532950014" Y="-2.612608526858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.608825932788" Y="-3.886302911655" />
                  <Point X="27.63798819842" Y="-3.858141239086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.859592538664" Y="-2.678451640457" />
                  <Point X="28.98832732098" Y="-2.554133906247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.550619385339" Y="-3.81044678" />
                  <Point X="27.589034055442" Y="-3.773350164294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.774004659319" Y="-2.629037353653" />
                  <Point X="28.912121691947" Y="-2.495659285635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.49241283789" Y="-3.734590648345" />
                  <Point X="27.540079912464" Y="-3.688559089501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.688416779975" Y="-2.579623066849" />
                  <Point X="28.835916062913" Y="-2.437184665023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.43420629044" Y="-3.658734516691" />
                  <Point X="27.491125769486" Y="-3.603768014708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.60282890063" Y="-2.530208780045" />
                  <Point X="28.75971043388" Y="-2.378710044411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.375999742991" Y="-3.582878385036" />
                  <Point X="27.442171626509" Y="-3.518976939916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.517241021285" Y="-2.480794493242" />
                  <Point X="28.683504804846" Y="-2.3202354238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.317793195542" Y="-3.507022253382" />
                  <Point X="27.393217483531" Y="-3.434185865123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.43165314194" Y="-2.431380206438" />
                  <Point X="28.607299175812" Y="-2.261760803188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.259586648093" Y="-3.431166121727" />
                  <Point X="27.344263340553" Y="-3.34939479033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.346065262596" Y="-2.381965919634" />
                  <Point X="28.531093546779" Y="-2.203286182576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.699022328101" Y="-1.075430468679" />
                  <Point X="29.737046998465" Y="-1.038710471343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.201380100643" Y="-3.355309990073" />
                  <Point X="27.295309197576" Y="-3.264603715538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.260477383251" Y="-2.33255163283" />
                  <Point X="28.454887917745" Y="-2.144811561965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.578671860367" Y="-1.059586023266" />
                  <Point X="29.77010942063" Y="-0.874716920244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.143173553194" Y="-3.279453858418" />
                  <Point X="27.246355054598" Y="-3.179812640745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.174889503906" Y="-2.283137346026" />
                  <Point X="28.378682288712" Y="-2.086336941353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.458321392633" Y="-1.043741577853" />
                  <Point X="29.745675698811" Y="-0.766246749984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.084967005745" Y="-3.203597726764" />
                  <Point X="27.19740091162" Y="-3.095021565952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.089301624561" Y="-2.233723059223" />
                  <Point X="28.302476659678" Y="-2.027862320741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.337970924899" Y="-1.02789713244" />
                  <Point X="29.638621979152" Y="-0.737561784214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.026760458296" Y="-3.127741595109" />
                  <Point X="27.148446768642" Y="-3.01023049116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.003713745216" Y="-2.184308772419" />
                  <Point X="28.226271030644" Y="-1.969387700129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.217620457165" Y="-1.012052687027" />
                  <Point X="29.531568259494" Y="-0.708876818443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.832252045399" Y="-4.149199419709" />
                  <Point X="25.83262925517" Y="-4.148835152468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.968553910846" Y="-3.051885463454" />
                  <Point X="27.099492625665" Y="-2.925439416367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.918125865872" Y="-2.134894485615" />
                  <Point X="28.150065401611" Y="-1.910913079518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.097269989431" Y="-0.996208241614" />
                  <Point X="29.424514539835" Y="-0.680191852672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.804139553084" Y="-4.044281796823" />
                  <Point X="25.817203689155" Y="-4.031665907267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.910347363397" Y="-2.9760293318" />
                  <Point X="27.05373558317" Y="-2.837560937526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.831381134335" Y="-2.086597357987" />
                  <Point X="28.073859772577" Y="-1.852438458906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.976919521697" Y="-0.980363796201" />
                  <Point X="29.317460820176" Y="-0.651506886901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.776027060769" Y="-3.939364173937" />
                  <Point X="25.80177812314" Y="-3.914496662066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.852141162719" Y="-2.900172865272" />
                  <Point X="27.029120270243" Y="-2.729266127761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.724347653735" Y="-2.057892847584" />
                  <Point X="28.005241527037" Y="-1.786636787225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.856569053963" Y="-0.964519350788" />
                  <Point X="29.210407100517" Y="-0.622821921131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.747914568454" Y="-3.834446551051" />
                  <Point X="25.786352557125" Y="-3.797327416865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.786617265024" Y="-2.831383016612" />
                  <Point X="27.008810859995" Y="-2.616813156114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.609136216094" Y="-2.037085698497" />
                  <Point X="27.952139303752" Y="-1.705851467021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.736218586229" Y="-0.948674905375" />
                  <Point X="29.103353380858" Y="-0.59413695536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.719802076139" Y="-3.729528928165" />
                  <Point X="25.77092699111" Y="-3.680158171664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.705869071707" Y="-2.777295099337" />
                  <Point X="27.004381717357" Y="-2.489024788295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.483159448583" Y="-2.026674507622" />
                  <Point X="27.899817885034" Y="-1.624312132613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.615868118495" Y="-0.932830459962" />
                  <Point X="28.9962996612" Y="-0.565451989589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.691689583824" Y="-3.624611305279" />
                  <Point X="25.755501425095" Y="-3.562988926463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.623769069741" Y="-2.724512608501" />
                  <Point X="27.123364561444" Y="-2.242058850219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.225886831243" Y="-2.143054245106" />
                  <Point X="27.862645964914" Y="-1.528143097464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.495517652225" Y="-0.916986013135" />
                  <Point X="28.889245941541" Y="-0.536767023818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.66357724438" Y="-3.519693534767" />
                  <Point X="25.74007585908" Y="-3.445819681263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.540129820218" Y="-2.673216551752" />
                  <Point X="27.866340463584" Y="-1.392509820423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.364336243553" Y="-0.911600885806" />
                  <Point X="28.782192221882" Y="-0.508082058048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.626073661073" Y="-3.423844783035" />
                  <Point X="25.725075716912" Y="-3.328239609028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.43077336736" Y="-2.646755309583" />
                  <Point X="27.889565432632" Y="-1.238016187372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.189282240243" Y="-0.948583030641" />
                  <Point X="28.676141753367" Y="-0.478428263909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.572223754308" Y="-3.343781492376" />
                  <Point X="25.745708380206" Y="-3.176249336544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.282241629153" Y="-2.658125200725" />
                  <Point X="28.586830836715" Y="-0.432609272441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.770344399913" Y="0.710296490371" />
                  <Point X="29.782593707619" Y="0.722125509322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.970699098539" Y="-4.758290333882" />
                  <Point X="24.03296874187" Y="-4.698157238306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.517344735798" Y="-3.264712003376" />
                  <Point X="25.782512407724" Y="-3.008642559156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.1231412044" Y="-2.679701153829" />
                  <Point X="28.505314793977" Y="-0.379262858734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.581068087911" Y="0.65958002168" />
                  <Point X="29.764719499861" Y="0.836930128678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.861207663179" Y="-4.7319594428" />
                  <Point X="24.080708430566" Y="-4.519990015672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.462465717289" Y="-3.185642514377" />
                  <Point X="28.442355185679" Y="-0.307996704586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.391791775909" Y="0.60886355299" />
                  <Point X="29.744178382804" Y="0.94915934366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.878023152184" Y="-4.583655372679" />
                  <Point X="24.128448119263" Y="-4.341822793039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.395237669127" Y="-3.118498344692" />
                  <Point X="28.389187911158" Y="-0.227274203632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.202515463906" Y="0.558147084299" />
                  <Point X="29.718147328166" Y="1.056086987547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.869191584077" Y="-4.460118377716" />
                  <Point X="24.176187807959" Y="-4.163655570405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.302334489978" Y="-3.076148360794" />
                  <Point X="28.360735437992" Y="-0.122684896437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.013239151904" Y="0.507430615608" />
                  <Point X="29.62814809362" Y="1.101241278151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.847154716903" Y="-4.349333591832" />
                  <Point X="24.223927496656" Y="-3.985488347772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.198838989788" Y="-3.044027262424" />
                  <Point X="28.350135006714" Y="-0.000856072783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.823962839902" Y="0.456714146917" />
                  <Point X="29.469802963773" Y="1.080394704859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.825117849728" Y="-4.238548805948" />
                  <Point X="24.271667185352" Y="-3.807321125138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.095343489598" Y="-3.011906164053" />
                  <Point X="28.392605654384" Y="0.172222896076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.613308450775" Y="0.38535310912" />
                  <Point X="29.311457833926" Y="1.059548131567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.795326011582" Y="-4.13525290848" />
                  <Point X="24.319406874049" Y="-3.629153902505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.971858313006" Y="-2.999088871797" />
                  <Point X="29.15311270408" Y="1.038701558276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.737443038686" Y="-4.059084304511" />
                  <Point X="24.375051793339" Y="-3.443352687424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.776768875166" Y="-3.055419010855" />
                  <Point X="28.994767574233" Y="1.017854984984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.66613394156" Y="-3.995881158001" />
                  <Point X="28.836422444386" Y="0.997008411692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.594463044511" Y="-3.933027397615" />
                  <Point X="28.67807731454" Y="0.976161838401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.522792147462" Y="-3.87017363723" />
                  <Point X="28.519732184693" Y="0.955315265109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.441503029439" Y="-3.816608084871" />
                  <Point X="28.387299048907" Y="0.959491613614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.328278958584" Y="-3.793881757987" />
                  <Point X="28.277029598898" Y="0.985071184684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.200213149074" Y="-3.785487931521" />
                  <Point X="28.189322396414" Y="1.032438864921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.072147339564" Y="-3.777094105055" />
                  <Point X="28.117244367874" Y="1.094899462996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.874483632748" Y="-3.835910186767" />
                  <Point X="28.062878616245" Y="1.174464608061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.437545380505" Y="-4.125791011096" />
                  <Point X="28.022289286438" Y="1.267333489036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.354216519234" Y="-4.074195215896" />
                  <Point X="28.001741915523" Y="1.379556664738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.270887657964" Y="-4.022599420696" />
                  <Point X="28.028749602612" Y="1.53770322614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.510015590753" Y="2.002456388585" />
                  <Point X="29.08939355894" Y="2.561955188833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.193192741756" Y="-3.965562987991" />
                  <Point X="29.03839500061" Y="2.644771994669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.117102895587" Y="-3.906976557166" />
                  <Point X="28.982327390257" Y="2.722693673868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.041013049418" Y="-3.848390126341" />
                  <Point X="28.854590911834" Y="2.731405531668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.141918912786" Y="-3.618880925628" />
                  <Point X="28.514512701388" Y="2.535061362431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.314246388143" Y="-3.320400675938" />
                  <Point X="28.174434490943" Y="2.338717193193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.486573863501" Y="-3.021920426248" />
                  <Point X="27.834356280497" Y="2.142373023956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.658772605396" Y="-2.723564493017" />
                  <Point X="27.545030797529" Y="1.995040193935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.686230513158" Y="-2.564983158565" />
                  <Point X="27.390582698172" Y="1.977956939242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.647535760083" Y="-2.470284706107" />
                  <Point X="27.277717700652" Y="2.001030019214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.582634323101" Y="-2.400893754123" />
                  <Point X="27.19233673173" Y="2.050644117091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.492438017974" Y="-2.355929772366" />
                  <Point X="27.119423812482" Y="2.112298470582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.346015354662" Y="-2.365262953558" />
                  <Point X="27.063813878585" Y="2.190662122796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.019368270134" Y="-2.548636835264" />
                  <Point X="27.024777057804" Y="2.285030244311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.679290256091" Y="-2.744980814836" />
                  <Point X="27.012919954527" Y="2.405645513921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.339212242049" Y="-2.941324794409" />
                  <Point X="27.05614863103" Y="2.579456502716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.171514426817" Y="-2.971203150992" />
                  <Point X="27.224211073764" Y="2.873818058277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.113577684432" Y="-2.895086471616" />
                  <Point X="27.39653880438" Y="3.172298554468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.055640942047" Y="-2.818969792239" />
                  <Point X="27.568866534997" Y="3.470779050659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.997704199662" Y="-2.742853112862" />
                  <Point X="27.741194265613" Y="3.76925954685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.947491471251" Y="-2.659277439894" />
                  <Point X="27.774027693341" Y="3.933031960592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.897514527635" Y="-2.575474072198" />
                  <Point X="21.868222777382" Y="-1.638072011804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.014890396111" Y="-1.49643673877" />
                  <Point X="27.69526873411" Y="3.989040858893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.847537584018" Y="-2.491670704501" />
                  <Point X="21.202443124029" Y="-2.148942408395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.049856304843" Y="-1.33060501206" />
                  <Point X="27.615435813197" Y="4.044012644454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.016994819052" Y="-1.230273438865" />
                  <Point X="27.530138231034" Y="4.093707267987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.952855787692" Y="-1.16014624033" />
                  <Point X="27.44484064887" Y="4.14340189152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.864803609441" Y="-1.113111699317" />
                  <Point X="27.359543066706" Y="4.193096515053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.739170235561" Y="-1.102368897068" />
                  <Point X="27.274245484543" Y="4.242791138586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.580825110693" Y="-1.123215465551" />
                  <Point X="27.184017017796" Y="4.287724062228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.422479985826" Y="-1.144062034035" />
                  <Point X="27.091879613757" Y="4.330813546553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.264134860958" Y="-1.164908602518" />
                  <Point X="26.999742209717" Y="4.373903030879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.105789736091" Y="-1.185755171002" />
                  <Point X="26.907604805677" Y="4.416992515205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.947444611223" Y="-1.206601739485" />
                  <Point X="26.812087367221" Y="4.456817938236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.789099486355" Y="-1.227448307969" />
                  <Point X="26.712670034919" Y="4.492877277557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.630754361488" Y="-1.248294876452" />
                  <Point X="26.613252702616" Y="4.528936616878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.47240923662" Y="-1.269141444936" />
                  <Point X="21.45250087416" Y="-0.322677952282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.699668871553" Y="-0.083990591707" />
                  <Point X="26.513835370314" Y="4.564995956198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.334806790774" Y="-1.269957041129" />
                  <Point X="21.263224546634" Y="-0.373394435963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.69474245185" Y="0.043317561232" />
                  <Point X="26.410481006939" Y="4.597253348806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.307747646746" Y="-1.164022211626" />
                  <Point X="21.073948121947" Y="-0.424111013472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.652006867334" Y="0.134113828127" />
                  <Point X="26.301075445011" Y="4.623667166898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.280688502717" Y="-1.058087382124" />
                  <Point X="20.884671697261" Y="-0.474827590981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.585362284138" Y="0.201821443379" />
                  <Point X="26.191669883083" Y="4.650080984989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.255798723028" Y="-0.950057621831" />
                  <Point X="20.695395272574" Y="-0.52554416849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.496433987967" Y="0.24800992715" />
                  <Point X="26.082264321156" Y="4.676494803081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.239203038084" Y="-0.834018347345" />
                  <Point X="20.506118847887" Y="-0.576260745999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.389652819175" Y="0.276958092234" />
                  <Point X="25.972858759228" Y="4.702908621172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.22260735314" Y="-0.717979072859" />
                  <Point X="20.3168424232" Y="-0.626977323508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.282599121473" Y="0.305643079209" />
                  <Point X="25.8634531973" Y="4.729322439264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.175545423772" Y="0.334328066183" />
                  <Point X="25.069535628821" Y="4.094710696408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.235601188368" Y="4.255078343145" />
                  <Point X="25.744619496557" Y="4.746631609534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.06849172607" Y="0.363013053158" />
                  <Point X="24.935377729094" Y="4.097221459736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.284147631382" Y="4.434024639366" />
                  <Point X="25.62124186106" Y="4.759552753019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.961438028369" Y="0.391698040133" />
                  <Point X="24.847775711393" Y="4.144690715739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.33188764816" Y="4.612192178825" />
                  <Point X="25.497864225564" Y="4.772473896505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.854384330667" Y="0.420383027107" />
                  <Point X="24.790980771436" Y="4.221910020903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.747330632966" Y="0.449068014082" />
                  <Point X="24.758118975998" Y="4.322241295076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.640276935264" Y="0.477753001057" />
                  <Point X="24.730006312403" Y="4.427158752558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.533223237563" Y="0.506437988031" />
                  <Point X="21.256053069726" Y="1.204466643046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.578559627925" Y="1.515907606101" />
                  <Point X="24.701893648808" Y="4.532076210041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.426169539861" Y="0.535122975006" />
                  <Point X="21.132647281085" Y="1.217360599357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.555574740444" Y="1.625776899418" />
                  <Point X="24.673780985212" Y="4.636993667523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.31911584216" Y="0.563807961981" />
                  <Point X="21.012296780796" Y="1.233205013332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.508468800731" Y="1.712352763357" />
                  <Point X="24.645668321617" Y="4.741911125006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.215085559584" Y="0.595412627003" />
                  <Point X="20.891946280507" Y="1.249049427307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.442137197395" Y="1.780362619747" />
                  <Point X="24.539446644417" Y="4.77139958484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.237886297068" Y="0.749496584396" />
                  <Point X="20.771595780218" Y="1.264893841282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.365931595529" Y="1.838837266593" />
                  <Point X="24.383828900436" Y="4.753186817463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.260687034553" Y="0.903580541789" />
                  <Point X="20.651245279929" Y="1.280738255257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.289725993662" Y="1.89731191344" />
                  <Point X="24.228211156455" Y="4.734974050086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.300167079248" Y="1.073771518927" />
                  <Point X="20.53089477964" Y="1.296582669232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.213520391796" Y="1.955786560287" />
                  <Point X="22.018278189911" Y="2.732932132365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.238606645251" Y="2.945700848457" />
                  <Point X="24.072593570086" Y="4.716761434913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.348637975936" Y="1.25264486091" />
                  <Point X="20.410544279351" Y="1.312427083207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.137314789929" Y="2.014261207134" />
                  <Point X="21.876795923794" Y="2.728369837288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.247531651815" Y="3.086385168258" />
                  <Point X="23.233792746515" Y="4.038806436439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.519522848177" Y="4.314732788238" />
                  <Point X="23.880785869715" Y="4.66360043289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.061109188063" Y="2.07273585398" />
                  <Point X="21.765623850996" Y="2.753077755661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.226511434211" Y="3.19815172122" />
                  <Point X="23.097976600542" Y="4.039715849982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.534131125078" Y="4.460905378407" />
                  <Point X="23.6880806922" Y="4.609572747263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.984903586196" Y="2.131210500827" />
                  <Point X="21.678675078633" Y="2.801177843354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.17999367555" Y="3.285295584998" />
                  <Point X="23.005162240112" Y="4.08215160512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.90869798433" Y="2.189685147674" />
                  <Point X="21.593087102301" Y="2.850592036498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.131039599191" Y="3.370086724125" />
                  <Point X="22.920938421378" Y="4.132883149944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.832492382464" Y="2.24815979452" />
                  <Point X="21.507499125968" Y="2.900006229642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.082085522833" Y="3.454877863251" />
                  <Point X="22.857242886771" Y="4.203438628315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.809710729479" Y="2.358225349109" />
                  <Point X="21.421911149635" Y="2.949420422785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.033131446475" Y="3.539669002378" />
                  <Point X="22.760782034601" Y="4.242353007312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.986375923761" Y="2.660894485272" />
                  <Point X="21.336323173303" Y="2.998834615929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.984177370117" Y="3.624460141504" />
                  <Point X="22.438758797577" Y="4.063444323238" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626464844" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.48751171875" Y="-3.596717773438" />
                  <Point X="25.471541015625" Y="-3.537112548828" />
                  <Point X="25.45779296875" Y="-3.512138671875" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.28907421875" Y="-3.273825195312" />
                  <Point X="25.264234375" Y="-3.263264648438" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="25.00615625" Y="-3.18551953125" />
                  <Point X="24.981234375" Y="-3.190901611328" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.72323828125" Y="-3.273825439453" />
                  <Point X="24.705193359375" Y="-3.295049316406" />
                  <Point X="24.547994140625" Y="-3.521544433594" />
                  <Point X="24.5407734375" Y="-3.537111083984" />
                  <Point X="24.1561640625" Y="-4.972495605469" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.889423828125" Y="-4.935407226562" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.688927734375" Y="-4.565645996094" />
                  <Point X="23.6908515625" Y="-4.551042480469" />
                  <Point X="23.687904296875" Y="-4.522625488281" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.62405078125" Y="-4.215220703125" />
                  <Point X="23.60441796875" Y="-4.19447265625" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.366625" Y="-3.989461914063" />
                  <Point X="23.338416015625" Y="-3.984953857422" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.024958984375" Y="-3.967068115234" />
                  <Point X="22.9998359375" Y="-3.980663330078" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157293945313" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.14416796875" Y="-4.167610839844" />
                  <Point X="22.129861328125" Y="-4.156595214844" />
                  <Point X="21.771419921875" Y="-3.880608154297" />
                  <Point X="22.46428515625" Y="-2.680528564453" />
                  <Point X="22.493966796875" Y="-2.629120117188" />
                  <Point X="22.50023828125" Y="-2.597602294922" />
                  <Point X="22.485330078125" Y="-2.568072998047" />
                  <Point X="22.468673828125" Y="-2.551416259766" />
                  <Point X="22.43984375" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.170326171875" Y="-3.258224121094" />
                  <Point X="21.157041015625" Y="-3.26589453125" />
                  <Point X="20.83830078125" Y="-2.847134765625" />
                  <Point X="20.828041015625" Y="-2.829932128906" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.78428125" Y="-1.462993408203" />
                  <Point X="21.836212890625" Y="-1.423144897461" />
                  <Point X="21.854486328125" Y="-1.394825927734" />
                  <Point X="21.8618828125" Y="-1.366264770508" />
                  <Point X="21.859671875" Y="-1.334587890625" />
                  <Point X="21.837771484375" Y="-1.310010009766" />
                  <Point X="21.812359375" Y="-1.295053100586" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.217626953125" Y="-1.494323608398" />
                  <Point X="20.19671875" Y="-1.497076171875" />
                  <Point X="20.072607421875" Y="-1.011186279297" />
                  <Point X="20.06989453125" Y="-0.992221252441" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="21.38337890625" Y="-0.144496612549" />
                  <Point X="21.442537109375" Y="-0.128645339966" />
                  <Point X="21.459212890625" Y="-0.120657104492" />
                  <Point X="21.485859375" Y="-0.102162963867" />
                  <Point X="21.50547265625" Y="-0.074717384338" />
                  <Point X="21.514353515625" Y="-0.046098842621" />
                  <Point X="21.51398046875" Y="-0.015259732246" />
                  <Point X="21.505103515625" Y="0.013345760345" />
                  <Point X="21.484736328125" Y="0.040381717682" />
                  <Point X="21.45810546875" Y="0.058865032196" />
                  <Point X="21.442537109375" Y="0.066085227966" />
                  <Point X="20.0179296875" Y="0.447808074951" />
                  <Point X="20.001814453125" Y="0.452126220703" />
                  <Point X="20.08235546875" Y="0.996415527344" />
                  <Point X="20.087818359375" Y="1.016579162598" />
                  <Point X="20.226484375" Y="1.528298706055" />
                  <Point X="21.204330078125" Y="1.399563110352" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268306640625" Y="1.395869873047" />
                  <Point X="21.270744140625" Y="1.396638671875" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056152344" />
                  <Point X="21.360880859375" Y="1.443786376953" />
                  <Point X="21.36186328125" Y="1.446158813477" />
                  <Point X="21.38552734375" Y="1.503290649414" />
                  <Point X="21.389287109375" Y="1.524605712891" />
                  <Point X="21.38368359375" Y="1.545512084961" />
                  <Point X="21.382498046875" Y="1.547789916992" />
                  <Point X="21.353943359375" Y="1.602641845703" />
                  <Point X="21.34003125" Y="1.619221923828" />
                  <Point X="20.52389453125" Y="2.245465576172" />
                  <Point X="20.83998828125" Y="2.787008300781" />
                  <Point X="20.854455078125" Y="2.805605224609" />
                  <Point X="21.225330078125" Y="3.282310791016" />
                  <Point X="21.814798828125" Y="2.941979980469" />
                  <Point X="21.84084375" Y="2.926943115234" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.864896484375" Y="2.920136474609" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.96848828125" Y="2.915773681641" />
                  <Point X="21.986740234375" Y="2.927397460938" />
                  <Point X="21.989169921875" Y="2.929825439453" />
                  <Point X="22.04747265625" Y="2.988127929688" />
                  <Point X="22.0591015625" Y="3.006385253906" />
                  <Point X="22.06192578125" Y="3.027847167969" />
                  <Point X="22.06162890625" Y="3.031251708984" />
                  <Point X="22.054443359375" Y="3.113390380859" />
                  <Point X="22.04793359375" Y="3.134033447266" />
                  <Point X="21.69272265625" Y="3.749277587891" />
                  <Point X="22.247123046875" Y="4.174331054688" />
                  <Point X="22.2699140625" Y="4.186992675781" />
                  <Point X="22.858453125" Y="4.513972167969" />
                  <Point X="23.024201171875" Y="4.29796484375" />
                  <Point X="23.032173828125" Y="4.28757421875" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.05255078125" Y="4.271684570312" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164880859375" Y="4.218491699219" />
                  <Point X="23.186201171875" Y="4.222253417969" />
                  <Point X="23.190154296875" Y="4.223891601563" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744628906" />
                  <Point X="23.31391796875" Y="4.294488769531" />
                  <Point X="23.315205078125" Y="4.298570800781" />
                  <Point X="23.346197265625" Y="4.396865234375" />
                  <Point X="23.348083984375" Y="4.418424804688" />
                  <Point X="23.31086328125" Y="4.701141113281" />
                  <Point X="24.03190625" Y="4.903296386719" />
                  <Point X="24.05953125" Y="4.906529296875" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.95015625" Y="4.339657714844" />
                  <Point X="24.957859375" Y="4.310905761719" />
                  <Point X="24.975716796875" Y="4.284177734375" />
                  <Point X="25.006154296875" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176269531" />
                  <Point X="25.054453125" Y="4.310902832031" />
                  <Point X="25.236650390625" Y="4.990869140625" />
                  <Point X="25.860205078125" Y="4.925565429688" />
                  <Point X="25.8830625" Y="4.920047363281" />
                  <Point X="26.50845703125" Y="4.769058105469" />
                  <Point X="26.5228828125" Y="4.763825683594" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="26.945421875" Y="4.609057128906" />
                  <Point X="27.33869921875" Y="4.425134765625" />
                  <Point X="27.352607421875" Y="4.417031738281" />
                  <Point X="27.73252734375" Y="4.195689941406" />
                  <Point X="27.745634765625" Y="4.186368652344" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.264208984375" Y="2.563099609375" />
                  <Point X="27.22985546875" Y="2.503598144531" />
                  <Point X="27.22399609375" Y="2.488313476562" />
                  <Point X="27.2033828125" Y="2.411228027344" />
                  <Point X="27.20237890625" Y="2.389558349609" />
                  <Point X="27.210416015625" Y="2.322901367188" />
                  <Point X="27.21868359375" Y="2.300811767578" />
                  <Point X="27.220396484375" Y="2.298287597656" />
                  <Point X="27.261642578125" Y="2.237503173828" />
                  <Point X="27.27746484375" Y="2.222490722656" />
                  <Point X="27.33825" Y="2.18124609375" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.36310546875" Y="2.172646240234" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.451865234375" Y="2.166802490234" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417236328" />
                  <Point X="28.973916015625" Y="3.019691894531" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.20990234375" Y="2.729802490234" />
                  <Point X="29.387513671875" Y="2.436296142578" />
                  <Point X="28.33384765625" Y="1.627788696289" />
                  <Point X="28.2886171875" Y="1.593083251953" />
                  <Point X="28.277068359375" Y="1.580827758789" />
                  <Point X="28.22158984375" Y="1.508451904297" />
                  <Point X="28.21226171875" Y="1.488431640625" />
                  <Point X="28.191595703125" Y="1.414535766602" />
                  <Point X="28.19078125" Y="1.39095690918" />
                  <Point X="28.191484375" Y="1.387551025391" />
                  <Point X="28.20844921875" Y="1.305332519531" />
                  <Point X="28.2175625" Y="1.28504309082" />
                  <Point X="28.263703125" Y="1.21491003418" />
                  <Point X="28.280953125" Y="1.198817138672" />
                  <Point X="28.283724609375" Y="1.197256958008" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.3723203125" Y="1.153123413086" />
                  <Point X="28.462728515625" Y="1.141174926758" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.8369453125" Y="1.320369506836" />
                  <Point X="29.848974609375" Y="1.32195324707" />
                  <Point X="29.93919140625" Y="0.951368103027" />
                  <Point X="29.9414921875" Y="0.936592285156" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="28.793001953125" Y="0.251715591431" />
                  <Point X="28.74116796875" Y="0.237826919556" />
                  <Point X="28.725400390625" Y="0.2306875" />
                  <Point X="28.636578125" Y="0.179347045898" />
                  <Point X="28.620052734375" Y="0.164106674194" />
                  <Point X="28.566759765625" Y="0.096198928833" />
                  <Point X="28.55698828125" Y="0.074742622375" />
                  <Point X="28.556248046875" Y="0.070882980347" />
                  <Point X="28.538484375" Y="-0.021875694275" />
                  <Point X="28.53922265625" Y="-0.044536151886" />
                  <Point X="28.556986328125" Y="-0.137294815063" />
                  <Point X="28.566759765625" Y="-0.158759033203" />
                  <Point X="28.56897265625" Y="-0.161578964233" />
                  <Point X="28.622265625" Y="-0.229486709595" />
                  <Point X="28.640265625" Y="-0.2440390625" />
                  <Point X="28.729087890625" Y="-0.295379516602" />
                  <Point X="28.74116796875" Y="-0.300387023926" />
                  <Point X="29.989396484375" Y="-0.634848876953" />
                  <Point X="29.998068359375" Y="-0.637172607422" />
                  <Point X="29.948431640625" Y="-0.96641204834" />
                  <Point X="29.945482421875" Y="-0.979330444336" />
                  <Point X="29.874548828125" Y="-1.290178344727" />
                  <Point X="28.472095703125" Y="-1.105542114258" />
                  <Point X="28.411984375" Y="-1.097628173828" />
                  <Point X="28.387599609375" Y="-1.099914672852" />
                  <Point X="28.2132734375" Y="-1.13780480957" />
                  <Point X="28.197947265625" Y="-1.143923095703" />
                  <Point X="28.181072265625" Y="-1.159959472656" />
                  <Point X="28.075703125" Y="-1.286685180664" />
                  <Point X="28.067955078125" Y="-1.299518066406" />
                  <Point X="28.063732421875" Y="-1.320885742188" />
                  <Point X="28.048630859375" Y="-1.485000976562" />
                  <Point X="28.0498515625" Y="-1.501458251953" />
                  <Point X="28.0603671875" Y="-1.522853271484" />
                  <Point X="28.156841796875" Y="-1.672912475586" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="29.326826171875" Y="-2.574384521484" />
                  <Point X="29.33907421875" Y="-2.583783203125" />
                  <Point X="29.2041328125" Y="-2.802140380859" />
                  <Point X="29.198029296875" Y="-2.810813964844" />
                  <Point X="29.056689453125" Y="-3.011638183594" />
                  <Point X="27.806994140625" Y="-2.290125732422" />
                  <Point X="27.753455078125" Y="-2.259215332031" />
                  <Point X="27.7287265625" Y="-2.251756835938" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.50475" Y="-2.214074462891" />
                  <Point X="27.481919921875" Y="-2.22301171875" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2974921875" Y="-2.322614746094" />
                  <Point X="27.284833984375" Y="-2.341841064453" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.188951171875" Y="-2.529873291016" />
                  <Point X="27.190720703125" Y="-2.554990234375" />
                  <Point X="27.22819140625" Y="-2.762465087891" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.978458984375" Y="-4.067856933594" />
                  <Point X="27.98667578125" Y="-4.082087646484" />
                  <Point X="27.835294921875" Y="-4.19021484375" />
                  <Point X="27.8284765625" Y="-4.194627929688" />
                  <Point X="27.679775390625" Y="-4.29087890625" />
                  <Point X="26.724033203125" Y="-3.045330566406" />
                  <Point X="26.6831796875" Y="-2.992087890625" />
                  <Point X="26.662052734375" Y="-2.975004150391" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.44226171875" Y="-2.836937255859" />
                  <Point X="26.41651171875" Y="-2.836572265625" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.178166015625" Y="-2.860761474609" />
                  <Point X="26.158158203125" Y="-2.874475585938" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.97457421875" Y="-3.030661865234" />
                  <Point X="25.9663125" Y="-3.055857421875" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="26.124876953125" Y="-4.913024902344" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="25.99435546875" Y="-4.963245117188" />
                  <Point X="25.9880390625" Y="-4.964392578125" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#120" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.007704675999" Y="4.383878717325" Z="0.1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.1" />
                  <Point X="-0.952438035427" Y="4.987470524619" Z="0.1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.1" />
                  <Point X="-1.719960207573" Y="4.777433358763" Z="0.1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.1" />
                  <Point X="-1.74881345865" Y="4.755879581922" Z="0.1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.1" />
                  <Point X="-1.738638169725" Y="4.34488585766" Z="0.1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.1" />
                  <Point X="-1.835144978169" Y="4.301362422626" Z="0.1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.1" />
                  <Point X="-1.930518928965" Y="4.34731509453" Z="0.1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.1" />
                  <Point X="-1.942288205271" Y="4.359681946167" Z="0.1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.1" />
                  <Point X="-2.760526061943" Y="4.261980126942" Z="0.1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.1" />
                  <Point X="-3.355060661319" Y="3.811646893241" Z="0.1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.1" />
                  <Point X="-3.363632477803" Y="3.767501965628" Z="0.1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.1" />
                  <Point X="-2.994338426924" Y="3.05817441508" Z="0.1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.1" />
                  <Point X="-3.052342503043" Y="2.996461064843" Z="0.1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.1" />
                  <Point X="-3.136902050527" Y="3.001226272249" Z="0.1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.1" />
                  <Point X="-3.166357382669" Y="3.016561470943" Z="0.1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.1" />
                  <Point X="-4.191164100168" Y="2.867587760212" Z="0.1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.1" />
                  <Point X="-4.534099174379" Y="2.287122726264" Z="0.1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.1" />
                  <Point X="-4.513721051871" Y="2.237862022633" Z="0.1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.1" />
                  <Point X="-3.668008163918" Y="1.555982148284" Z="0.1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.1" />
                  <Point X="-3.690487211005" Y="1.496572550423" Z="0.1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.1" />
                  <Point X="-3.7504470045" Y="1.475604721567" Z="0.1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.1" />
                  <Point X="-3.795301879876" Y="1.480415367196" Z="0.1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.1" />
                  <Point X="-4.966597688451" Y="1.060936531753" Z="0.1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.1" />
                  <Point X="-5.056838569419" Y="0.470217864375" Z="0.1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.1" />
                  <Point X="-5.001169172287" Y="0.430791718058" Z="0.1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.1" />
                  <Point X="-3.5499143688" Y="0.030574931759" Z="0.1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.1" />
                  <Point X="-3.539925584511" Y="0.001188405503" Z="0.1" />
                  <Point X="-3.539556741714" Y="0" Z="0.1" />
                  <Point X="-3.548439016624" Y="-0.028618545557" Z="0.1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.1" />
                  <Point X="-3.575454226323" Y="-0.048301051375" Z="0.1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.1" />
                  <Point X="-3.635718616485" Y="-0.064920338698" Z="0.1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.1" />
                  <Point X="-4.985758246918" Y="-0.968020252081" Z="0.1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.1" />
                  <Point X="-4.852316022304" Y="-1.49996937576" Z="0.1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.1" />
                  <Point X="-4.782005024322" Y="-1.512615865694" Z="0.1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.1" />
                  <Point X="-3.19373216842" Y="-1.321828368544" Z="0.1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.1" />
                  <Point X="-3.200073623273" Y="-1.351011140379" Z="0.1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.1" />
                  <Point X="-3.252312369843" Y="-1.392045661991" Z="0.1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.1" />
                  <Point X="-4.221058568734" Y="-2.824261786951" Z="0.1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.1" />
                  <Point X="-3.87607842617" Y="-3.281743733367" Z="0.1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.1" />
                  <Point X="-3.810830428184" Y="-3.270245357181" Z="0.1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.1" />
                  <Point X="-2.556182657796" Y="-2.572147811336" Z="0.1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.1" />
                  <Point X="-2.585171655987" Y="-2.624247954308" Z="0.1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.1" />
                  <Point X="-2.906800336704" Y="-4.164932583666" Z="0.1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.1" />
                  <Point X="-2.468634982632" Y="-4.43869533576" Z="0.1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.1" />
                  <Point X="-2.442151153462" Y="-4.437856071534" Z="0.1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.1" />
                  <Point X="-1.978541132752" Y="-3.990956686059" Z="0.1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.1" />
                  <Point X="-1.67101019301" Y="-4.003566908221" Z="0.1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.1" />
                  <Point X="-1.434706108548" Y="-4.200784500292" Z="0.1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.1" />
                  <Point X="-1.36729176926" Y="-4.501100095918" Z="0.1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.1" />
                  <Point X="-1.366801091039" Y="-4.527835485724" Z="0.1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.1" />
                  <Point X="-1.129191309612" Y="-4.952550243903" Z="0.1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.1" />
                  <Point X="-0.829618489039" Y="-5.011443334461" Z="0.1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.1" />
                  <Point X="-0.80169687211" Y="-4.954157586102" Z="0.1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.1" />
                  <Point X="-0.259886915777" Y="-3.292278044594" Z="0.1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.1" />
                  <Point X="-0.010101446594" Y="-3.207374484683" Z="0.1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.1" />
                  <Point X="0.243257632767" Y="-3.279737560605" Z="0.1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.1" />
                  <Point X="0.410558943655" Y="-3.509367711174" Z="0.1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.1" />
                  <Point X="0.433057988291" Y="-3.578378443714" Z="0.1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.1" />
                  <Point X="0.990819905574" Y="-4.982307583652" Z="0.1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.1" />
                  <Point X="1.169914523417" Y="-4.943320039215" Z="0.1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.1" />
                  <Point X="1.168293231566" Y="-4.8752184478" Z="0.1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.1" />
                  <Point X="1.009014438347" Y="-3.035197354041" Z="0.1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.1" />
                  <Point X="1.183967785348" Y="-2.881642020081" Z="0.1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.1" />
                  <Point X="1.414937270924" Y="-2.855081756579" Z="0.1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.1" />
                  <Point X="1.628856648605" Y="-2.985782324111" Z="0.1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.1" />
                  <Point X="1.678208479277" Y="-3.044487987009" Z="0.1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.1" />
                  <Point X="2.849489866345" Y="-4.205322251188" Z="0.1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.1" />
                  <Point X="3.038967628816" Y="-4.070504174894" Z="0.1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.1" />
                  <Point X="3.015602299808" Y="-4.01157678046" Z="0.1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.1" />
                  <Point X="2.233767074062" Y="-2.514823724397" Z="0.1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.1" />
                  <Point X="2.322923308416" Y="-2.33384759327" Z="0.1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.1" />
                  <Point X="2.499050785102" Y="-2.235978001551" Z="0.1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.1" />
                  <Point X="2.713683079116" Y="-2.269680935429" Z="0.1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.1" />
                  <Point X="2.775836816647" Y="-2.302147200466" Z="0.1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.1" />
                  <Point X="4.23276064591" Y="-2.808311197171" Z="0.1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.1" />
                  <Point X="4.392849814499" Y="-2.550636246012" Z="0.1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.1" />
                  <Point X="4.351106661673" Y="-2.503436982168" Z="0.1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.1" />
                  <Point X="3.096268664738" Y="-1.46453381374" Z="0.1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.1" />
                  <Point X="3.107364342481" Y="-1.294187234764" Z="0.1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.1" />
                  <Point X="3.213360024091" Y="-1.160646556897" Z="0.1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.1" />
                  <Point X="3.392060875718" Y="-1.117493847642" Z="0.1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.1" />
                  <Point X="3.459412252937" Y="-1.12383437053" Z="0.1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.1" />
                  <Point X="4.98807209231" Y="-0.959174352167" Z="0.1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.1" />
                  <Point X="5.045759398979" Y="-0.584123001091" Z="0.1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.1" />
                  <Point X="4.996181525904" Y="-0.555272527481" Z="0.1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.1" />
                  <Point X="3.659131095131" Y="-0.169470157371" Z="0.1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.1" />
                  <Point X="3.602149751973" Y="-0.09943044927" Z="0.1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.1" />
                  <Point X="3.582172402803" Y="-0.003851869581" Z="0.1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.1" />
                  <Point X="3.599199047621" Y="0.092758661666" Z="0.1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.1" />
                  <Point X="3.653229686427" Y="0.164518289305" Z="0.1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.1" />
                  <Point X="3.744264319222" Y="0.218678686871" Z="0.1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.1" />
                  <Point X="3.799786291402" Y="0.234699405288" Z="0.1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.1" />
                  <Point X="4.984740411784" Y="0.975564402632" Z="0.1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.1" />
                  <Point X="4.884825397486" Y="1.39206832667" Z="0.1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.1" />
                  <Point X="4.82426308706" Y="1.401221839101" Z="0.1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.1" />
                  <Point X="3.372714817348" Y="1.233972416243" Z="0.1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.1" />
                  <Point X="3.302095186752" Y="1.272108026139" Z="0.1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.1" />
                  <Point X="3.253177054441" Y="1.343804079146" Z="0.1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.1" />
                  <Point X="3.234296496322" Y="1.428935018446" Z="0.1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.1" />
                  <Point X="3.254257824177" Y="1.50624512196" Z="0.1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.1" />
                  <Point X="3.310594379199" Y="1.581689638458" Z="0.1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.1" />
                  <Point X="3.358127324005" Y="1.619400658521" Z="0.1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.1" />
                  <Point X="4.246521921302" Y="2.786968850875" Z="0.1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.1" />
                  <Point X="4.011667423091" Y="3.115554015673" Z="0.1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.1" />
                  <Point X="3.942759723567" Y="3.094273415583" Z="0.1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.1" />
                  <Point X="2.432792675463" Y="2.246385169183" Z="0.1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.1" />
                  <Point X="2.362934763081" Y="2.25356676144" Z="0.1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.1" />
                  <Point X="2.299382195803" Y="2.295145351581" Z="0.1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.1" />
                  <Point X="2.255613248654" Y="2.357642664579" Z="0.1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.1" />
                  <Point X="2.245862941319" Y="2.426823680751" Z="0.1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.1" />
                  <Point X="2.266142773475" Y="2.506676900419" Z="0.1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.1" />
                  <Point X="2.301351937277" Y="2.569379358388" Z="0.1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.1" />
                  <Point X="2.768454251838" Y="4.258395826954" Z="0.1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.1" />
                  <Point X="2.37162045085" Y="4.491514652061" Z="0.1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.1" />
                  <Point X="1.960447005389" Y="4.685629340961" Z="0.1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.1" />
                  <Point X="1.533773812352" Y="4.842109845674" Z="0.1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.1" />
                  <Point X="0.888640828225" Y="4.99993093137" Z="0.1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.1" />
                  <Point X="0.219930167871" Y="5.073528336686" Z="0.1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.1" />
                  <Point X="0.185539885554" Y="5.047568788626" Z="0.1" />
                  <Point X="0" Y="4.355124473572" Z="0.1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>