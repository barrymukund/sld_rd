<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#141" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1074" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.78389453125" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.658130859375" Y="-3.866419189453" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.503607421875" Y="-3.411534423828" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.242521484375" Y="-3.157055175781" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097036132812" />
                  <Point X="24.903201171875" Y="-3.115650146484" />
                  <Point X="24.70981640625" Y="-3.175669433594" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231478271484" />
                  <Point X="24.594919921875" Y="-3.287320556641" />
                  <Point X="24.46994921875" Y="-3.467378662109" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.14325390625" Y="-4.653619628906" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.854888671875" Y="-4.828427734375" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.773623046875" Y="-4.650142578125" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.516223632812" />
                  <Point X="23.7691640625" Y="-4.44419140625" />
                  <Point X="23.722962890625" Y="-4.211932128906" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204589844" />
                  <Point X="23.621138671875" Y="-4.082780029297" />
                  <Point X="23.443095703125" Y="-3.926639892578" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.283685546875" Y="-3.886162841797" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894802001953" />
                  <Point X="22.89627734375" Y="-3.935604980469" />
                  <Point X="22.699376953125" Y="-4.067170166016" />
                  <Point X="22.68721484375" Y="-4.076821533203" />
                  <Point X="22.6648984375" Y="-4.099461425781" />
                  <Point X="22.5198515625" Y="-4.288488769531" />
                  <Point X="22.19828515625" Y="-4.089383544922" />
                  <Point X="22.107216796875" Y="-4.019263183594" />
                  <Point X="21.89527734375" Y="-3.856077392578" />
                  <Point X="22.400015625" Y="-2.981846679688" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647653808594" />
                  <Point X="22.593412109375" Y="-2.616125976562" />
                  <Point X="22.594423828125" Y="-2.585190185547" />
                  <Point X="22.585439453125" Y="-2.555570556641" />
                  <Point X="22.571220703125" Y="-2.526740966797" />
                  <Point X="22.5531953125" Y="-2.501588378906" />
                  <Point X="22.5358515625" Y="-2.484243896484" />
                  <Point X="22.510697265625" Y="-2.466216308594" />
                  <Point X="22.4818671875" Y="-2.451997558594" />
                  <Point X="22.452248046875" Y="-2.443011962891" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.37664453125" Y="-3.029409423828" />
                  <Point X="21.1819765625" Y="-3.141801513672" />
                  <Point X="21.177126953125" Y="-3.135430664062" />
                  <Point X="20.917140625" Y="-2.793861083984" />
                  <Point X="20.851849609375" Y="-2.684377685547" />
                  <Point X="20.693857421875" Y="-2.419450195312" />
                  <Point X="21.585712890625" Y="-1.73510559082" />
                  <Point X="21.894044921875" Y="-1.498513305664" />
                  <Point X="21.916935546875" Y="-1.475880004883" />
                  <Point X="21.936087890625" Y="-1.445301391602" />
                  <Point X="21.944625" Y="-1.423191894531" />
                  <Point X="21.947966796875" Y="-1.412791503906" />
                  <Point X="21.95384765625" Y="-1.3900859375" />
                  <Point X="21.957962890625" Y="-1.358610229492" />
                  <Point X="21.957265625" Y="-1.335736816406" />
                  <Point X="21.9511171875" Y="-1.313694091797" />
                  <Point X="21.939111328125" Y="-1.284711669922" />
                  <Point X="21.92643359375" Y="-1.262873901367" />
                  <Point X="21.90844921875" Y="-1.245147827148" />
                  <Point X="21.889677734375" Y="-1.231028686523" />
                  <Point X="21.880759765625" Y="-1.225078491211" />
                  <Point X="21.860546875" Y="-1.213181640625" />
                  <Point X="21.83128515625" Y="-1.20195715332" />
                  <Point X="21.7993984375" Y="-1.195475097656" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="20.5256484375" Y="-1.357952026367" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.267607421875" Y="-1.390740112305" />
                  <Point X="20.165923828125" Y="-0.992650268555" />
                  <Point X="20.1486484375" Y="-0.871875061035" />
                  <Point X="20.107578125" Y="-0.584698425293" />
                  <Point X="21.11588671875" Y="-0.314522155762" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.487166015625" Y="-0.212510360718" />
                  <Point X="21.509302734375" Y="-0.200730514526" />
                  <Point X="21.51883984375" Y="-0.194910263062" />
                  <Point X="21.5400234375" Y="-0.180207992554" />
                  <Point X="21.563978515625" Y="-0.158684127808" />
                  <Point X="21.584595703125" Y="-0.128991195679" />
                  <Point X="21.5942109375" Y="-0.107196975708" />
                  <Point X="21.5980234375" Y="-0.097008346558" />
                  <Point X="21.605083984375" Y="-0.074257392883" />
                  <Point X="21.61052734375" Y="-0.045515735626" />
                  <Point X="21.609705078125" Y="-0.012401903152" />
                  <Point X="21.60526953125" Y="0.009473824501" />
                  <Point X="21.60289453125" Y="0.018753225327" />
                  <Point X="21.595833984375" Y="0.041504177094" />
                  <Point X="21.582517578125" Y="0.070830841064" />
                  <Point X="21.560205078125" Y="0.099606651306" />
                  <Point X="21.541814453125" Y="0.115684440613" />
                  <Point X="21.533453125" Y="0.122207633972" />
                  <Point X="21.51226953125" Y="0.136909896851" />
                  <Point X="21.498076171875" Y="0.14504586792" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="20.33459765625" Y="0.461308410645" />
                  <Point X="20.108185546875" Y="0.521975341797" />
                  <Point X="20.10998046875" Y="0.534108459473" />
                  <Point X="20.17551171875" Y="0.976969299316" />
                  <Point X="20.210287109375" Y="1.105300048828" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.98255859375" Y="1.332939941406" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228027344" />
                  <Point X="21.2968671875" Y="1.305264282227" />
                  <Point X="21.31140625" Y="1.309848876953" />
                  <Point X="21.35829296875" Y="1.324631713867" />
                  <Point X="21.37722265625" Y="1.332962036133" />
                  <Point X="21.395966796875" Y="1.343784179688" />
                  <Point X="21.4126484375" Y="1.35601574707" />
                  <Point X="21.426287109375" Y="1.371567993164" />
                  <Point X="21.438701171875" Y="1.389298095703" />
                  <Point X="21.448650390625" Y="1.407434814453" />
                  <Point X="21.454484375" Y="1.421520751953" />
                  <Point X="21.473296875" Y="1.466938964844" />
                  <Point X="21.479083984375" Y="1.486788696289" />
                  <Point X="21.48284375" Y="1.508104125977" />
                  <Point X="21.484197265625" Y="1.528750488281" />
                  <Point X="21.481048828125" Y="1.549200317383" />
                  <Point X="21.4754453125" Y="1.570107055664" />
                  <Point X="21.467951171875" Y="1.589374145508" />
                  <Point X="21.460912109375" Y="1.602897949219" />
                  <Point X="21.4382109375" Y="1.64650390625" />
                  <Point X="21.426716796875" Y="1.663708984375" />
                  <Point X="21.4128046875" Y="1.680288208008" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="20.748244140625" Y="2.193061767578" />
                  <Point X="20.648140625" Y="2.269873779297" />
                  <Point X="20.6642109375" Y="2.297406738281" />
                  <Point X="20.918853515625" Y="2.733668212891" />
                  <Point X="21.01095703125" Y="2.852056152344" />
                  <Point X="21.24949609375" Y="3.158662353516" />
                  <Point X="21.6387109375" Y="2.933948730469" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.873458984375" Y="2.824024658203" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.019537109375" Y="2.835653076172" />
                  <Point X="22.037791015625" Y="2.847281982422" />
                  <Point X="22.053921875" Y="2.860228027344" />
                  <Point X="22.068296875" Y="2.874602539062" />
                  <Point X="22.114646484375" Y="2.920951416016" />
                  <Point X="22.127595703125" Y="2.9370859375" />
                  <Point X="22.139224609375" Y="2.955340576172" />
                  <Point X="22.14837109375" Y="2.973889160156" />
                  <Point X="22.1532890625" Y="2.993977783203" />
                  <Point X="22.156115234375" Y="3.015436767578" />
                  <Point X="22.15656640625" Y="3.036119873047" />
                  <Point X="22.154794921875" Y="3.056371337891" />
                  <Point X="22.14908203125" Y="3.121669433594" />
                  <Point X="22.145044921875" Y="3.141963378906" />
                  <Point X="22.13853515625" Y="3.16260546875" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.842337890625" Y="3.680132568359" />
                  <Point X="21.81666796875" Y="3.724596191406" />
                  <Point X="21.8558828125" Y="3.754662841797" />
                  <Point X="22.299376953125" Y="4.094684814453" />
                  <Point X="22.4444453125" Y="4.175280761719" />
                  <Point X="22.83296484375" Y="4.391133789062" />
                  <Point X="22.909466796875" Y="4.29143359375" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.02742578125" Y="4.177661621094" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222548828125" Y="4.134482421875" />
                  <Point X="23.246025390625" Y="4.14420703125" />
                  <Point X="23.32172265625" Y="4.175562011719" />
                  <Point X="23.339853515625" Y="4.185508789062" />
                  <Point X="23.357583984375" Y="4.197922851562" />
                  <Point X="23.373140625" Y="4.211563964844" />
                  <Point X="23.385373046875" Y="4.228249511719" />
                  <Point X="23.396193359375" Y="4.246993652344" />
                  <Point X="23.404521484375" Y="4.265923339844" />
                  <Point X="23.412162109375" Y="4.290158203125" />
                  <Point X="23.43680078125" Y="4.368299804688" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.41014453125" />
                  <Point X="23.442271484375" Y="4.430826171875" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.47623828125" Y="4.648843261719" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.226228515625" Y="4.830390136719" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.8203515625" Y="4.457038085938" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183885742188" />
                  <Point X="25.067130859375" Y="4.194217773438" />
                  <Point X="25.094423828125" Y="4.208806640625" />
                  <Point X="25.11558203125" Y="4.23139453125" />
                  <Point X="25.13344140625" Y="4.25812109375" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.293716796875" Y="4.836791015625" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.342740234375" Y="4.884238769531" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="25.98954296875" Y="4.796610351562" />
                  <Point X="26.48102734375" Y="4.677951171875" />
                  <Point X="26.5746015625" Y="4.644010742188" />
                  <Point X="26.894642578125" Y="4.527930175781" />
                  <Point X="26.986228515625" Y="4.485098632812" />
                  <Point X="27.294578125" Y="4.340893066406" />
                  <Point X="27.383080078125" Y="4.28933203125" />
                  <Point X="27.6809765625" Y="4.115775878906" />
                  <Point X="27.764421875" Y="4.056435302734" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.351548828125" Y="2.904375488281" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.139490234375" Y="2.532909912109" />
                  <Point X="27.12940625" Y="2.501822265625" />
                  <Point X="27.12799609375" Y="2.497053955078" />
                  <Point X="27.111607421875" Y="2.435772949219" />
                  <Point X="27.1083984375" Y="2.409496826172" />
                  <Point X="27.10904296875" Y="2.37416015625" />
                  <Point X="27.1097109375" Y="2.364521484375" />
                  <Point X="27.116099609375" Y="2.311530761719" />
                  <Point X="27.12144140625" Y="2.289608642578" />
                  <Point X="27.12970703125" Y="2.267519042969" />
                  <Point X="27.140072265625" Y="2.247467285156" />
                  <Point X="27.1502421875" Y="2.232480957031" />
                  <Point X="27.18303125" Y="2.184158691406" />
                  <Point X="27.20098046875" Y="2.164391113281" />
                  <Point X="27.229267578125" Y="2.140922119141" />
                  <Point X="27.2365859375" Y="2.135423339844" />
                  <Point X="27.284908203125" Y="2.102634765625" />
                  <Point X="27.304953125" Y="2.092272216797" />
                  <Point X="27.327041015625" Y="2.084006347656" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.3653984375" Y="2.076681884766" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.445625" Y="2.070942382812" />
                  <Point X="27.483533203125" Y="2.077362548828" />
                  <Point X="27.4922109375" Y="2.079253417969" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.727640625" Y="2.767807861328" />
                  <Point X="28.967328125" Y="2.906191162109" />
                  <Point X="29.1232734375" Y="2.689464355469" />
                  <Point X="29.169791015625" Y="2.612590820312" />
                  <Point X="29.26219921875" Y="2.459884033203" />
                  <Point X="28.49932421875" Y="1.874508544922" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.221427734375" Y="1.660243896484" />
                  <Point X="28.20397265625" Y="1.641626342773" />
                  <Point X="28.190294921875" Y="1.623782104492" />
                  <Point X="28.14619140625" Y="1.566245239258" />
                  <Point X="28.132953125" Y="1.542637084961" />
                  <Point X="28.119388671875" Y="1.507467041016" />
                  <Point X="28.11653515625" Y="1.498869018555" />
                  <Point X="28.10010546875" Y="1.440123535156" />
                  <Point X="28.09665234375" Y="1.417824707031" />
                  <Point X="28.0958359375" Y="1.39425378418" />
                  <Point X="28.097740234375" Y="1.371763671875" />
                  <Point X="28.101923828125" Y="1.351492553711" />
                  <Point X="28.11541015625" Y="1.286130859375" />
                  <Point X="28.1206796875" Y="1.268979248047" />
                  <Point X="28.13628515625" Y="1.235738525391" />
                  <Point X="28.147662109375" Y="1.218447387695" />
                  <Point X="28.184341796875" Y="1.162693359375" />
                  <Point X="28.198890625" Y="1.145452270508" />
                  <Point X="28.216134765625" Y="1.129361694336" />
                  <Point X="28.23434765625" Y="1.116033935547" />
                  <Point X="28.250833984375" Y="1.10675402832" />
                  <Point X="28.303990234375" Y="1.076831542969" />
                  <Point X="28.3205234375" Y="1.069501098633" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.37841015625" Y="1.056492675781" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.57027734375" Y="1.189442260742" />
                  <Point X="29.776837890625" Y="1.216636474609" />
                  <Point X="29.84594140625" Y="0.932786010742" />
                  <Point X="29.86059765625" Y="0.83865447998" />
                  <Point X="29.890865234375" Y="0.644238891602" />
                  <Point X="29.02433203125" Y="0.412051544189" />
                  <Point X="28.716580078125" Y="0.329589874268" />
                  <Point X="28.7047890625" Y="0.325585662842" />
                  <Point X="28.681546875" Y="0.315067657471" />
                  <Point X="28.6596484375" Y="0.302409698486" />
                  <Point X="28.589037109375" Y="0.261595214844" />
                  <Point X="28.574314453125" Y="0.251097854614" />
                  <Point X="28.547529296875" Y="0.225574920654" />
                  <Point X="28.534390625" Y="0.208832290649" />
                  <Point X="28.4920234375" Y="0.15484727478" />
                  <Point X="28.48030078125" Y="0.13556640625" />
                  <Point X="28.47052734375" Y="0.114101722717" />
                  <Point X="28.46368359375" Y="0.092607444763" />
                  <Point X="28.459302734375" Y="0.069737640381" />
                  <Point X="28.445181640625" Y="-0.004003189087" />
                  <Point X="28.443484375" Y="-0.021876050949" />
                  <Point X="28.4451796875" Y="-0.058550582886" />
                  <Point X="28.44955859375" Y="-0.08142023468" />
                  <Point X="28.463681640625" Y="-0.155161224365" />
                  <Point X="28.47052734375" Y="-0.176664901733" />
                  <Point X="28.48030078125" Y="-0.198128372192" />
                  <Point X="28.4920234375" Y="-0.217407272339" />
                  <Point X="28.505162109375" Y="-0.234150054932" />
                  <Point X="28.547529296875" Y="-0.288135070801" />
                  <Point X="28.560001953125" Y="-0.301237762451" />
                  <Point X="28.58903515625" Y="-0.324155059814" />
                  <Point X="28.61093359375" Y="-0.336813171387" />
                  <Point X="28.681544921875" Y="-0.377627471924" />
                  <Point X="28.692708984375" Y="-0.383138244629" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="29.708890625" Y="-0.658039001465" />
                  <Point X="29.89147265625" Y="-0.706961730957" />
                  <Point X="29.85501953125" Y="-0.948747558594" />
                  <Point X="29.836244140625" Y="-1.03102746582" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="28.781287109375" Y="-1.050428344727" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.3316796875" Y="-1.014850524902" />
                  <Point X="28.193095703125" Y="-1.044972412109" />
                  <Point X="28.163974609375" Y="-1.056597412109" />
                  <Point X="28.1361484375" Y="-1.073489746094" />
                  <Point X="28.1123984375" Y="-1.093960327148" />
                  <Point X="28.086419921875" Y="-1.125204589844" />
                  <Point X="28.002654296875" Y="-1.225948486328" />
                  <Point X="27.987935546875" Y="-1.250329223633" />
                  <Point X="27.976591796875" Y="-1.277714111328" />
                  <Point X="27.969759765625" Y="-1.305366943359" />
                  <Point X="27.966037109375" Y="-1.345829711914" />
                  <Point X="27.95403125" Y="-1.476297241211" />
                  <Point X="27.95634765625" Y="-1.507561035156" />
                  <Point X="27.964078125" Y="-1.539182373047" />
                  <Point X="27.976451171875" Y="-1.56799597168" />
                  <Point X="28.000236328125" Y="-1.604993164063" />
                  <Point X="28.076931640625" Y="-1.724286621094" />
                  <Point X="28.0869375" Y="-1.737242919922" />
                  <Point X="28.110630859375" Y="-1.760909423828" />
                  <Point X="29.0315" Y="-2.467518066406" />
                  <Point X="29.213123046875" Y="-2.6068828125" />
                  <Point X="29.1248203125" Y="-2.749772216797" />
                  <Point X="29.08596875" Y="-2.804975097656" />
                  <Point X="29.028982421875" Y="-2.885945068359" />
                  <Point X="28.118828125" Y="-2.360466796875" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.703072265625" Y="-2.150587158203" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135176513672" />
                  <Point X="27.402337890625" Y="-2.157541503906" />
                  <Point X="27.26531640625" Y="-2.229655517578" />
                  <Point X="27.242384765625" Y="-2.246549560547" />
                  <Point X="27.22142578125" Y="-2.267509033203" />
                  <Point X="27.204533203125" Y="-2.290438720703" />
                  <Point X="27.18216796875" Y="-2.332934326172" />
                  <Point X="27.110052734375" Y="-2.469956787109" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531908935547" />
                  <Point X="27.09567578125" Y="-2.563259033203" />
                  <Point X="27.1049140625" Y="-2.614412109375" />
                  <Point X="27.134703125" Y="-2.779349365234" />
                  <Point X="27.13898828125" Y="-2.795140869141" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.74357421875" Y="-3.851022949219" />
                  <Point X="27.861287109375" Y="-4.054905273438" />
                  <Point X="27.78184765625" Y="-4.111645996094" />
                  <Point X="27.73842578125" Y="-4.139752441406" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="27.00111328125" Y="-3.250372802734" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.671474609375" Y="-2.868121826172" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.361923828125" Y="-2.746194091797" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.0619921875" Y="-2.830886474609" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968861083984" />
                  <Point X="25.88725" Y="-2.996687744141" />
                  <Point X="25.875625" Y="-3.025809570312" />
                  <Point X="25.86288671875" Y="-3.084418701172" />
                  <Point X="25.821810546875" Y="-3.273397460938" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.98744140625" Y="-4.596912597656" />
                  <Point X="26.02206640625" Y="-4.859915527344" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.93555859375" Y="-4.877371582031" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94158203125" Y="-4.752638183594" />
                  <Point X="23.878560546875" Y="-4.736423828125" />
                  <Point X="23.858755859375" Y="-4.731328125" />
                  <Point X="23.867810546875" Y="-4.662541992188" />
                  <Point X="23.8792265625" Y="-4.575837890625" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030761719" />
                  <Point X="23.8782421875" Y="-4.509324707031" />
                  <Point X="23.876666015625" Y="-4.497689941406" />
                  <Point X="23.862337890625" Y="-4.425657714844" />
                  <Point X="23.81613671875" Y="-4.1933984375" />
                  <Point X="23.811875" Y="-4.178469238281" />
                  <Point X="23.80097265625" Y="-4.149501464844" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.769423828125" Y="-4.094860351562" />
                  <Point X="23.749791015625" Y="-4.0709375" />
                  <Point X="23.738994140625" Y="-4.059780273438" />
                  <Point X="23.68377734375" Y="-4.011355712891" />
                  <Point X="23.505734375" Y="-3.855215576172" />
                  <Point X="23.493263671875" Y="-3.845966308594" />
                  <Point X="23.46698046875" Y="-3.829621826172" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.2898984375" Y="-3.791366210938" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.94632421875" Y="-3.795496582031" />
                  <Point X="22.9181328125" Y="-3.808270507812" />
                  <Point X="22.9045625" Y="-3.815812988281" />
                  <Point X="22.843498046875" Y="-3.856615966797" />
                  <Point X="22.64659765625" Y="-3.988181152344" />
                  <Point X="22.64032421875" Y="-3.992754394531" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032771484375" />
                  <Point X="22.589529296875" Y="-4.04162890625" />
                  <Point X="22.496796875" Y="-4.162478027344" />
                  <Point X="22.25240234375" Y="-4.011154541016" />
                  <Point X="22.165173828125" Y="-3.943990966797" />
                  <Point X="22.01913671875" Y="-3.831546875" />
                  <Point X="22.482287109375" Y="-3.029346679688" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.710084472656" />
                  <Point X="22.67605078125" Y="-2.681118652344" />
                  <Point X="22.680314453125" Y="-2.666187988281" />
                  <Point X="22.6865859375" Y="-2.63466015625" />
                  <Point X="22.688361328125" Y="-2.619231201172" />
                  <Point X="22.689373046875" Y="-2.588295410156" />
                  <Point X="22.685333984375" Y="-2.557614990234" />
                  <Point X="22.676349609375" Y="-2.527995361328" />
                  <Point X="22.670640625" Y="-2.513549316406" />
                  <Point X="22.656421875" Y="-2.484719726562" />
                  <Point X="22.648439453125" Y="-2.471402832031" />
                  <Point X="22.6304140625" Y="-2.446250244141" />
                  <Point X="22.62037109375" Y="-2.434414550781" />
                  <Point X="22.60302734375" Y="-2.417070068359" />
                  <Point X="22.59119140625" Y="-2.407026855469" />
                  <Point X="22.566037109375" Y="-2.388999267578" />
                  <Point X="22.55271875" Y="-2.381014892578" />
                  <Point X="22.523888671875" Y="-2.366796142578" />
                  <Point X="22.5094453125" Y="-2.361088867188" />
                  <Point X="22.479826171875" Y="-2.352103271484" />
                  <Point X="22.449140625" Y="-2.348062744141" />
                  <Point X="22.418205078125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.32914453125" Y="-2.947136962891" />
                  <Point X="21.206912109375" Y="-3.017708496094" />
                  <Point X="20.9959765625" Y="-2.740581787109" />
                  <Point X="20.93344140625" Y="-2.635719482422" />
                  <Point X="20.818734375" Y="-2.443373535156" />
                  <Point X="21.643544921875" Y="-1.810473999023" />
                  <Point X="21.951876953125" Y="-1.573881835938" />
                  <Point X="21.96083984375" Y="-1.566067260742" />
                  <Point X="21.98373046875" Y="-1.543433837891" />
                  <Point X="21.997447265625" Y="-1.526307006836" />
                  <Point X="22.016599609375" Y="-1.495728271484" />
                  <Point X="22.0247109375" Y="-1.479521362305" />
                  <Point X="22.033248046875" Y="-1.457411743164" />
                  <Point X="22.039931640625" Y="-1.436610961914" />
                  <Point X="22.0458125" Y="-1.413905395508" />
                  <Point X="22.048046875" Y="-1.402401855469" />
                  <Point X="22.052162109375" Y="-1.370926025391" />
                  <Point X="22.05291796875" Y="-1.355715454102" />
                  <Point X="22.052220703125" Y="-1.332842163086" />
                  <Point X="22.0487734375" Y="-1.310212524414" />
                  <Point X="22.042625" Y="-1.288169799805" />
                  <Point X="22.038884765625" Y="-1.277336669922" />
                  <Point X="22.02687890625" Y="-1.248354248047" />
                  <Point X="22.02126953125" Y="-1.237015136719" />
                  <Point X="22.008591796875" Y="-1.215177246094" />
                  <Point X="21.99312109375" Y="-1.195214599609" />
                  <Point X="21.97513671875" Y="-1.177488525391" />
                  <Point X="21.9655546875" Y="-1.1692265625" />
                  <Point X="21.946783203125" Y="-1.155107421875" />
                  <Point X="21.928947265625" Y="-1.14320703125" />
                  <Point X="21.908734375" Y="-1.131310180664" />
                  <Point X="21.8945703125" Y="-1.124483276367" />
                  <Point X="21.86530859375" Y="-1.113258911133" />
                  <Point X="21.8502109375" Y="-1.108861083984" />
                  <Point X="21.81832421875" Y="-1.102379150391" />
                  <Point X="21.802705078125" Y="-1.100532714844" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="20.513248046875" Y="-1.263764770508" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.259240234375" Y="-0.97411541748" />
                  <Point X="20.24269140625" Y="-0.858423461914" />
                  <Point X="20.213548828125" Y="-0.654654663086" />
                  <Point X="21.140474609375" Y="-0.40628515625" />
                  <Point X="21.491712890625" Y="-0.312171203613" />
                  <Point X="21.50195703125" Y="-0.308792572021" />
                  <Point X="21.521998046875" Y="-0.300894592285" />
                  <Point X="21.531794921875" Y="-0.296375427246" />
                  <Point X="21.553931640625" Y="-0.284595581055" />
                  <Point X="21.573005859375" Y="-0.27295513916" />
                  <Point X="21.594189453125" Y="-0.258252929688" />
                  <Point X="21.603517578125" Y="-0.250873352051" />
                  <Point X="21.62747265625" Y="-0.229349502563" />
                  <Point X="21.64201171875" Y="-0.212866638184" />
                  <Point X="21.66262890625" Y="-0.183173721313" />
                  <Point X="21.67151171875" Y="-0.167337509155" />
                  <Point X="21.681126953125" Y="-0.145543304443" />
                  <Point X="21.68875390625" Y="-0.125165924072" />
                  <Point X="21.695814453125" Y="-0.102415039062" />
                  <Point X="21.698423828125" Y="-0.091935195923" />
                  <Point X="21.7038671875" Y="-0.063193470001" />
                  <Point X="21.705498046875" Y="-0.043157474518" />
                  <Point X="21.70467578125" Y="-0.010043711662" />
                  <Point X="21.702810546875" Y="0.006476311207" />
                  <Point X="21.698375" Y="0.028351959229" />
                  <Point X="21.693625" Y="0.04691078949" />
                  <Point X="21.686564453125" Y="0.069661827087" />
                  <Point X="21.682333984375" Y="0.080781494141" />
                  <Point X="21.669017578125" Y="0.110108192444" />
                  <Point X="21.65759375" Y="0.129043487549" />
                  <Point X="21.63528125" Y="0.157819244385" />
                  <Point X="21.622732421875" Y="0.171128417969" />
                  <Point X="21.604341796875" Y="0.187206298828" />
                  <Point X="21.587619140625" Y="0.200252563477" />
                  <Point X="21.566435546875" Y="0.214954772949" />
                  <Point X="21.559513671875" Y="0.219329193115" />
                  <Point X="21.53438671875" Y="0.232832473755" />
                  <Point X="21.503435546875" Y="0.245634841919" />
                  <Point X="21.491712890625" Y="0.249611236572" />
                  <Point X="20.359185546875" Y="0.553071289062" />
                  <Point X="20.2145546875" Y="0.591825012207" />
                  <Point X="20.26866796875" Y="0.957517211914" />
                  <Point X="20.30198046875" Y="1.080452880859" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="20.970158203125" Y="1.238752685547" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.20470324707" />
                  <Point X="21.28485546875" Y="1.206589477539" />
                  <Point X="21.295109375" Y="1.208053710938" />
                  <Point X="21.315400390625" Y="1.21208984375" />
                  <Point X="21.3399765625" Y="1.219246582031" />
                  <Point X="21.38686328125" Y="1.234029418945" />
                  <Point X="21.39655859375" Y="1.237678955078" />
                  <Point X="21.41548828125" Y="1.246009277344" />
                  <Point X="21.42472265625" Y="1.250690185547" />
                  <Point X="21.443466796875" Y="1.261512207031" />
                  <Point X="21.452140625" Y="1.267172119141" />
                  <Point X="21.468822265625" Y="1.279403564453" />
                  <Point X="21.48407421875" Y="1.293378662109" />
                  <Point X="21.497712890625" Y="1.308930908203" />
                  <Point X="21.504107421875" Y="1.317080200195" />
                  <Point X="21.516521484375" Y="1.334810302734" />
                  <Point X="21.5219921875" Y="1.343607543945" />
                  <Point X="21.53194140625" Y="1.361744140625" />
                  <Point X="21.54225390625" Y="1.385169189453" />
                  <Point X="21.56106640625" Y="1.430587280273" />
                  <Point X="21.5645" Y="1.440349121094" />
                  <Point X="21.570287109375" Y="1.460198852539" />
                  <Point X="21.572640625" Y="1.470286621094" />
                  <Point X="21.576400390625" Y="1.491602050781" />
                  <Point X="21.577640625" Y="1.501889526367" />
                  <Point X="21.578994140625" Y="1.522535888672" />
                  <Point X="21.578091796875" Y="1.543206298828" />
                  <Point X="21.574943359375" Y="1.56365612793" />
                  <Point X="21.572810546875" Y="1.573794555664" />
                  <Point X="21.56720703125" Y="1.594701293945" />
                  <Point X="21.563984375" Y="1.604544921875" />
                  <Point X="21.556490234375" Y="1.623812133789" />
                  <Point X="21.5451796875" Y="1.646759277344" />
                  <Point X="21.522478515625" Y="1.690365234375" />
                  <Point X="21.517205078125" Y="1.699276977539" />
                  <Point X="21.5057109375" Y="1.716481933594" />
                  <Point X="21.499490234375" Y="1.724775024414" />
                  <Point X="21.485578125" Y="1.741354125977" />
                  <Point X="21.478494140625" Y="1.748916015625" />
                  <Point X="21.463552734375" Y="1.763217895508" />
                  <Point X="21.4556953125" Y="1.769958618164" />
                  <Point X="20.806076171875" Y="2.268430175781" />
                  <Point X="20.77238671875" Y="2.294281494141" />
                  <Point X="20.997716796875" Y="2.680324707031" />
                  <Point X="21.0859375" Y="2.793722412109" />
                  <Point X="21.273662109375" Y="3.035013671875" />
                  <Point X="21.5912109375" Y="2.851676269531" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.8651796875" Y="2.729386230469" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.02356640625" Y="2.734227294922" />
                  <Point X="22.04300390625" Y="2.741302001953" />
                  <Point X="22.0615546875" Y="2.750450439453" />
                  <Point X="22.070580078125" Y="2.755530761719" />
                  <Point X="22.088833984375" Y="2.767159667969" />
                  <Point X="22.097251953125" Y="2.773192138672" />
                  <Point X="22.1133828125" Y="2.786138183594" />
                  <Point X="22.121095703125" Y="2.793051757812" />
                  <Point X="22.135470703125" Y="2.807426269531" />
                  <Point X="22.1818203125" Y="2.853775146484" />
                  <Point X="22.188736328125" Y="2.861489013672" />
                  <Point X="22.201685546875" Y="2.877623535156" />
                  <Point X="22.20771875" Y="2.886044189453" />
                  <Point X="22.21934765625" Y="2.904298828125" />
                  <Point X="22.2244296875" Y="2.913325683594" />
                  <Point X="22.233576171875" Y="2.931874267578" />
                  <Point X="22.240646484375" Y="2.951299072266" />
                  <Point X="22.245564453125" Y="2.971387695312" />
                  <Point X="22.2474765625" Y="2.981573242188" />
                  <Point X="22.250302734375" Y="3.003032226562" />
                  <Point X="22.251091796875" Y="3.013364990234" />
                  <Point X="22.25154296875" Y="3.034048095703" />
                  <Point X="22.251205078125" Y="3.0443984375" />
                  <Point X="22.24943359375" Y="3.064649902344" />
                  <Point X="22.243720703125" Y="3.129947998047" />
                  <Point X="22.242255859375" Y="3.140204833984" />
                  <Point X="22.23821875" Y="3.160498779297" />
                  <Point X="22.235646484375" Y="3.170535888672" />
                  <Point X="22.22913671875" Y="3.191177978516" />
                  <Point X="22.225486328125" Y="3.200873291016" />
                  <Point X="22.21715625" Y="3.21980078125" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="21.940611328125" Y="3.699916015625" />
                  <Point X="22.351626953125" Y="4.015036621094" />
                  <Point X="22.49058203125" Y="4.092236328125" />
                  <Point X="22.8074765625" Y="4.268295898437" />
                  <Point X="22.83409765625" Y="4.2336015625" />
                  <Point X="22.881435546875" Y="4.171909179688" />
                  <Point X="22.8881796875" Y="4.164048828125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.934908203125" Y="4.121897460938" />
                  <Point X="22.952107421875" Y="4.110405273438" />
                  <Point X="22.96101953125" Y="4.105129394531" />
                  <Point X="22.98355859375" Y="4.093395996094" />
                  <Point X="23.056236328125" Y="4.0555625" />
                  <Point X="23.065673828125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.0437890625" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.229271484375" Y="4.037488525391" />
                  <Point X="23.2491328125" Y="4.043277587891" />
                  <Point X="23.258904296875" Y="4.046714355469" />
                  <Point X="23.282380859375" Y="4.056438964844" />
                  <Point X="23.358078125" Y="4.087793945312" />
                  <Point X="23.367416015625" Y="4.092272705078" />
                  <Point X="23.385546875" Y="4.102219238281" />
                  <Point X="23.39433984375" Y="4.1076875" />
                  <Point X="23.4120703125" Y="4.1201015625" />
                  <Point X="23.420216796875" Y="4.126494140625" />
                  <Point X="23.4357734375" Y="4.140135253906" />
                  <Point X="23.4497578125" Y="4.155395507812" />
                  <Point X="23.461990234375" Y="4.172081054688" />
                  <Point X="23.4676484375" Y="4.180754882812" />
                  <Point X="23.47846875" Y="4.199499023438" />
                  <Point X="23.483150390625" Y="4.208737304688" />
                  <Point X="23.491478515625" Y="4.227666992188" />
                  <Point X="23.495125" Y="4.237358398438" />
                  <Point X="23.502765625" Y="4.261593261719" />
                  <Point X="23.527404296875" Y="4.339734863281" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370047851562" />
                  <Point X="23.535474609375" Y="4.380302246094" />
                  <Point X="23.537361328125" Y="4.40186328125" />
                  <Point X="23.53769921875" Y="4.412216308594" />
                  <Point X="23.537248046875" Y="4.432897949219" />
                  <Point X="23.536458984375" Y="4.4432265625" />
                  <Point X="23.520734375" Y="4.562655761719" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.237271484375" Y="4.736034179688" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.728587890625" Y="4.432450195312" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.021630859375" Y="4.085113037109" />
                  <Point X="25.05216796875" Y="4.090154541016" />
                  <Point X="25.06723046875" Y="4.093927246094" />
                  <Point X="25.09766796875" Y="4.104259277344" />
                  <Point X="25.1119140625" Y="4.110436035156" />
                  <Point X="25.13920703125" Y="4.125024902344" />
                  <Point X="25.1637578125" Y="4.143861328125" />
                  <Point X="25.184916015625" Y="4.16644921875" />
                  <Point X="25.1945703125" Y="4.178612792969" />
                  <Point X="25.2124296875" Y="4.205339355469" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.24710546875" />
                  <Point X="25.23798046875" Y="4.261727050781" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="25.967248046875" Y="4.704263671875" />
                  <Point X="26.453599609375" Y="4.58684375" />
                  <Point X="26.542208984375" Y="4.554704101562" />
                  <Point X="26.85825" Y="4.440074707031" />
                  <Point X="26.945984375" Y="4.399043945312" />
                  <Point X="27.250455078125" Y="4.256652832031" />
                  <Point X="27.3352578125" Y="4.207246582031" />
                  <Point X="27.62942578125" Y="4.03586328125" />
                  <Point X="27.709365234375" Y="3.979015380859" />
                  <Point X="27.81778125" Y="3.901915039062" />
                  <Point X="27.26927734375" Y="2.951875488281" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.0607890625" Y="2.589721679688" />
                  <Point X="27.049125" Y="2.562221923828" />
                  <Point X="27.039041015625" Y="2.531134277344" />
                  <Point X="27.036220703125" Y="2.52159765625" />
                  <Point X="27.01983203125" Y="2.460316650391" />
                  <Point X="27.01730859375" Y="2.447289306641" />
                  <Point X="27.014099609375" Y="2.421013183594" />
                  <Point X="27.0134140625" Y="2.407764404297" />
                  <Point X="27.01405859375" Y="2.372427734375" />
                  <Point X="27.01539453125" Y="2.353150390625" />
                  <Point X="27.021783203125" Y="2.300159667969" />
                  <Point X="27.02380078125" Y="2.289040039062" />
                  <Point X="27.029142578125" Y="2.267117919922" />
                  <Point X="27.032466796875" Y="2.256315429688" />
                  <Point X="27.040732421875" Y="2.234225830078" />
                  <Point X="27.045314453125" Y="2.223895019531" />
                  <Point X="27.0556796875" Y="2.203843261719" />
                  <Point X="27.0716328125" Y="2.179135986328" />
                  <Point X="27.104421875" Y="2.130813720703" />
                  <Point X="27.11269921875" Y="2.120296386719" />
                  <Point X="27.1306484375" Y="2.100528808594" />
                  <Point X="27.1403203125" Y="2.091278564453" />
                  <Point X="27.168607421875" Y="2.067809570312" />
                  <Point X="27.183244140625" Y="2.056812011719" />
                  <Point X="27.23156640625" Y="2.0240234375" />
                  <Point X="27.24128125" Y="2.018244628906" />
                  <Point X="27.261326171875" Y="2.007882080078" />
                  <Point X="27.27165625" Y="2.003298339844" />
                  <Point X="27.293744140625" Y="1.995032470703" />
                  <Point X="27.304548828125" Y="1.991707519531" />
                  <Point X="27.32647265625" Y="1.986364746094" />
                  <Point X="27.354025390625" Y="1.982365112305" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.42065625" Y="1.975319091797" />
                  <Point X="27.447892578125" Y="1.975969482422" />
                  <Point X="27.46148828125" Y="1.977276245117" />
                  <Point X="27.499396484375" Y="1.983696411133" />
                  <Point X="27.516751953125" Y="1.987478271484" />
                  <Point X="27.578033203125" Y="2.003865356445" />
                  <Point X="27.583994140625" Y="2.005670654297" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.775140625" Y="2.685535400391" />
                  <Point X="28.940408203125" Y="2.780951904297" />
                  <Point X="29.043951171875" Y="2.637051269531" />
                  <Point X="29.088513671875" Y="2.563408203125" />
                  <Point X="29.13688671875" Y="2.483472167969" />
                  <Point X="28.4414921875" Y="1.949877075195" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.168142578125" Y="1.739871337891" />
                  <Point X="28.152123046875" Y="1.725220581055" />
                  <Point X="28.13466796875" Y="1.706603027344" />
                  <Point X="28.12857421875" Y="1.699419799805" />
                  <Point X="28.114896484375" Y="1.681575561523" />
                  <Point X="28.07079296875" Y="1.624038696289" />
                  <Point X="28.063330078125" Y="1.612709838867" />
                  <Point X="28.050091796875" Y="1.58910168457" />
                  <Point X="28.04431640625" Y="1.576822387695" />
                  <Point X="28.030751953125" Y="1.54165234375" />
                  <Point X="28.025044921875" Y="1.524456176758" />
                  <Point X="28.008615234375" Y="1.46571081543" />
                  <Point X="28.006224609375" Y="1.454661621094" />
                  <Point X="28.002771484375" Y="1.432362792969" />
                  <Point X="28.001708984375" Y="1.42111315918" />
                  <Point X="28.000892578125" Y="1.397542236328" />
                  <Point X="28.001173828125" Y="1.386238525391" />
                  <Point X="28.003078125" Y="1.363748413086" />
                  <Point X="28.004701171875" Y="1.352562011719" />
                  <Point X="28.008884765625" Y="1.332290893555" />
                  <Point X="28.02237109375" Y="1.266929199219" />
                  <Point X="28.024599609375" Y="1.258230834961" />
                  <Point X="28.034685546875" Y="1.228607421875" />
                  <Point X="28.050291015625" Y="1.195366821289" />
                  <Point X="28.056923828125" Y="1.183521118164" />
                  <Point X="28.06830078125" Y="1.166229980469" />
                  <Point X="28.10498046875" Y="1.110475952148" />
                  <Point X="28.11173828125" Y="1.101426513672" />
                  <Point X="28.126287109375" Y="1.084185424805" />
                  <Point X="28.134078125" Y="1.075994140625" />
                  <Point X="28.151322265625" Y="1.059903442383" />
                  <Point X="28.160033203125" Y="1.052696289062" />
                  <Point X="28.17824609375" Y="1.039368530273" />
                  <Point X="28.204234375" Y="1.023967895508" />
                  <Point X="28.257390625" Y="0.994045532227" />
                  <Point X="28.265484375" Y="0.989985168457" />
                  <Point X="28.294681640625" Y="0.978083557129" />
                  <Point X="28.33027734375" Y="0.968021240234" />
                  <Point X="28.343671875" Y="0.965257751465" />
                  <Point X="28.365962890625" Y="0.962311584473" />
                  <Point X="28.437833984375" Y="0.952812927246" />
                  <Point X="28.444033203125" Y="0.952199707031" />
                  <Point X="28.46571875" Y="0.951222839355" />
                  <Point X="28.49121875" Y="0.952032348633" />
                  <Point X="28.50060546875" Y="0.952797180176" />
                  <Point X="29.582677734375" Y="1.095255004883" />
                  <Point X="29.704701171875" Y="1.111319824219" />
                  <Point X="29.752689453125" Y="0.914204711914" />
                  <Point X="29.766728515625" Y="0.824039123535" />
                  <Point X="29.783873046875" Y="0.713921386719" />
                  <Point X="28.999744140625" Y="0.503814422607" />
                  <Point X="28.6919921875" Y="0.421352813721" />
                  <Point X="28.68603125" Y="0.419544219971" />
                  <Point X="28.66562109375" Y="0.41213583374" />
                  <Point X="28.64237890625" Y="0.401617797852" />
                  <Point X="28.634005859375" Y="0.397315887451" />
                  <Point X="28.612107421875" Y="0.384657836914" />
                  <Point X="28.54149609375" Y="0.343843475342" />
                  <Point X="28.533884765625" Y="0.338946655273" />
                  <Point X="28.508779296875" Y="0.319874023438" />
                  <Point X="28.481994140625" Y="0.294351043701" />
                  <Point X="28.47279296875" Y="0.284223114014" />
                  <Point X="28.459654296875" Y="0.26748046875" />
                  <Point X="28.417287109375" Y="0.213495452881" />
                  <Point X="28.410849609375" Y="0.20420072937" />
                  <Point X="28.399126953125" Y="0.184919876099" />
                  <Point X="28.393841796875" Y="0.174933609009" />
                  <Point X="28.384068359375" Y="0.153468917847" />
                  <Point X="28.380005859375" Y="0.142923828125" />
                  <Point X="28.373162109375" Y="0.1214296875" />
                  <Point X="28.370380859375" Y="0.11048034668" />
                  <Point X="28.366" Y="0.087610565186" />
                  <Point X="28.35187890625" Y="0.013869703293" />
                  <Point X="28.350607421875" Y="0.004977895737" />
                  <Point X="28.3485859375" Y="-0.026262773514" />
                  <Point X="28.35028125" Y="-0.062937393188" />
                  <Point X="28.351875" Y="-0.076415847778" />
                  <Point X="28.35625390625" Y="-0.099285636902" />
                  <Point X="28.370376953125" Y="-0.173026489258" />
                  <Point X="28.373158203125" Y="-0.183979400635" />
                  <Point X="28.38000390625" Y="-0.205483047485" />
                  <Point X="28.384068359375" Y="-0.216033935547" />
                  <Point X="28.393841796875" Y="-0.237497451782" />
                  <Point X="28.39912890625" Y="-0.24748550415" />
                  <Point X="28.4108515625" Y="-0.266764404297" />
                  <Point X="28.417287109375" Y="-0.276055114746" />
                  <Point X="28.43042578125" Y="-0.292797912598" />
                  <Point X="28.47279296875" Y="-0.346782958984" />
                  <Point X="28.478720703125" Y="-0.353635498047" />
                  <Point X="28.501140625" Y="-0.375806152344" />
                  <Point X="28.530173828125" Y="-0.398723510742" />
                  <Point X="28.5414921875" Y="-0.406402984619" />
                  <Point X="28.563390625" Y="-0.419061065674" />
                  <Point X="28.634001953125" Y="-0.459875427246" />
                  <Point X="28.63949609375" Y="-0.462814422607" />
                  <Point X="28.65915625" Y="-0.472015960693" />
                  <Point X="28.68302734375" Y="-0.481027557373" />
                  <Point X="28.6919921875" Y="-0.483912780762" />
                  <Point X="29.684302734375" Y="-0.749801879883" />
                  <Point X="29.78487890625" Y="-0.776750976562" />
                  <Point X="29.76161328125" Y="-0.931063537598" />
                  <Point X="29.743625" Y="-1.00989276123" />
                  <Point X="29.7278046875" Y="-1.079219970703" />
                  <Point X="28.7936875" Y="-0.956241088867" />
                  <Point X="28.436783203125" Y="-0.909253662109" />
                  <Point X="28.428625" Y="-0.908535705566" />
                  <Point X="28.40009765625" Y="-0.908042419434" />
                  <Point X="28.36672265625" Y="-0.910840942383" />
                  <Point X="28.354482421875" Y="-0.912676147461" />
                  <Point X="28.311501953125" Y="-0.922018005371" />
                  <Point X="28.17291796875" Y="-0.952139831543" />
                  <Point X="28.157875" Y="-0.956742675781" />
                  <Point X="28.12875390625" Y="-0.968367675781" />
                  <Point X="28.11467578125" Y="-0.975389770508" />
                  <Point X="28.086849609375" Y="-0.992282104492" />
                  <Point X="28.074125" Y="-1.001530456543" />
                  <Point X="28.050375" Y="-1.022001037598" />
                  <Point X="28.039349609375" Y="-1.033223388672" />
                  <Point X="28.01337109375" Y="-1.064467651367" />
                  <Point X="27.92960546875" Y="-1.165211547852" />
                  <Point X="27.921326171875" Y="-1.176850097656" />
                  <Point X="27.906607421875" Y="-1.201230834961" />
                  <Point X="27.90016796875" Y="-1.21397265625" />
                  <Point X="27.88882421875" Y="-1.241357666016" />
                  <Point X="27.884365234375" Y="-1.254928100586" />
                  <Point X="27.877533203125" Y="-1.282580932617" />
                  <Point X="27.87516015625" Y="-1.296663452148" />
                  <Point X="27.8714375" Y="-1.337126220703" />
                  <Point X="27.859431640625" Y="-1.46759375" />
                  <Point X="27.859291015625" Y="-1.483316894531" />
                  <Point X="27.861607421875" Y="-1.514580566406" />
                  <Point X="27.864064453125" Y="-1.530121337891" />
                  <Point X="27.871794921875" Y="-1.561742675781" />
                  <Point X="27.87678515625" Y="-1.576666992188" />
                  <Point X="27.889158203125" Y="-1.605480712891" />
                  <Point X="27.896541015625" Y="-1.619369873047" />
                  <Point X="27.920326171875" Y="-1.65636706543" />
                  <Point X="27.997021484375" Y="-1.775660522461" />
                  <Point X="28.001744140625" Y="-1.782352783203" />
                  <Point X="28.01980078125" Y="-1.804456176758" />
                  <Point X="28.043494140625" Y="-1.828122680664" />
                  <Point X="28.052798828125" Y="-1.836277954102" />
                  <Point X="28.97366796875" Y="-2.54288671875" />
                  <Point X="29.087171875" Y="-2.629981445312" />
                  <Point X="29.04548828125" Y="-2.697432373047" />
                  <Point X="29.00828125" Y="-2.750298339844" />
                  <Point X="29.001275390625" Y="-2.760251953125" />
                  <Point X="28.166328125" Y="-2.278194335938" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.719955078125" Y="-2.057099609375" />
                  <Point X="27.555017578125" Y="-2.027312011719" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.444845703125" Y="-2.035136108398" />
                  <Point X="27.4150703125" Y="-2.044959594727" />
                  <Point X="27.40058984375" Y="-2.051108154297" />
                  <Point X="27.35809375" Y="-2.073473144531" />
                  <Point X="27.221072265625" Y="-2.145587158203" />
                  <Point X="27.20896875" Y="-2.153170410156" />
                  <Point X="27.186037109375" Y="-2.170064453125" />
                  <Point X="27.175208984375" Y="-2.179375244141" />
                  <Point X="27.15425" Y="-2.200334716797" />
                  <Point X="27.14494140625" Y="-2.211161621094" />
                  <Point X="27.128048828125" Y="-2.234091308594" />
                  <Point X="27.12046484375" Y="-2.246194091797" />
                  <Point X="27.098099609375" Y="-2.288689697266" />
                  <Point X="27.025984375" Y="-2.425712158203" />
                  <Point X="27.0198359375" Y="-2.440193359375" />
                  <Point X="27.01001171875" Y="-2.469971923828" />
                  <Point X="27.0063359375" Y="-2.485269287109" />
                  <Point X="27.00137890625" Y="-2.517442871094" />
                  <Point X="27.000279296875" Y="-2.533134033203" />
                  <Point X="27.00068359375" Y="-2.564484130859" />
                  <Point X="27.0021875" Y="-2.580143066406" />
                  <Point X="27.01142578125" Y="-2.631296142578" />
                  <Point X="27.04121484375" Y="-2.796233398438" />
                  <Point X="27.04301953125" Y="-2.804228759766" />
                  <Point X="27.05123828125" Y="-2.831542236328" />
                  <Point X="27.064072265625" Y="-2.862479980469" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.661302734375" Y="-3.898522949219" />
                  <Point X="27.735896484375" Y="-4.027722167969" />
                  <Point X="27.7237578125" Y="-4.036083007813" />
                  <Point X="27.076482421875" Y="-3.192540283203" />
                  <Point X="26.83391796875" Y="-2.876422607422" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.722849609375" Y="-2.788211669922" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.35321875" Y="-2.65159375" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="26.001255859375" Y="-2.757838623047" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883087646484" />
                  <Point X="25.83218359375" Y="-2.906837158203" />
                  <Point X="25.822935546875" Y="-2.919562744141" />
                  <Point X="25.80604296875" Y="-2.947389404297" />
                  <Point X="25.79901953125" Y="-2.961467529297" />
                  <Point X="25.78739453125" Y="-2.990589355469" />
                  <Point X="25.78279296875" Y="-3.005633056641" />
                  <Point X="25.7700546875" Y="-3.0642421875" />
                  <Point X="25.728978515625" Y="-3.253220947266" />
                  <Point X="25.7275859375" Y="-3.261287353516" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520263672" />
                  <Point X="25.83308984375" Y="-4.152321777344" />
                  <Point X="25.74989453125" Y="-3.841831542969" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480122314453" />
                  <Point X="25.642146484375" Y="-3.453578613281" />
                  <Point X="25.6267890625" Y="-3.423815429688" />
                  <Point X="25.62041015625" Y="-3.413209716797" />
                  <Point X="25.58165234375" Y="-3.3573671875" />
                  <Point X="25.456681640625" Y="-3.177309326172" />
                  <Point X="25.446671875" Y="-3.165172119141" />
                  <Point X="25.424787109375" Y="-3.142716308594" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104936767578" />
                  <Point X="25.3452421875" Y="-3.090829345703" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.270681640625" Y="-3.066324707031" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.998840087891" />
                  <Point X="24.948935546875" Y="-3.003109619141" />
                  <Point X="24.935015625" Y="-3.006305664062" />
                  <Point X="24.875041015625" Y="-3.024919677734" />
                  <Point X="24.68165625" Y="-3.084938964844" />
                  <Point X="24.6670703125" Y="-3.090829345703" />
                  <Point X="24.6390703125" Y="-3.104936767578" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.5875234375" Y="-3.14271875" />
                  <Point X="24.565638671875" Y="-3.165176269531" />
                  <Point X="24.555630859375" Y="-3.177312744141" />
                  <Point X="24.516875" Y="-3.233155029297" />
                  <Point X="24.391904296875" Y="-3.413213134766" />
                  <Point X="24.38753125" Y="-3.420130615234" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.051490234375" Y="-4.629031738281" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.868678644924" Y="-4.655948828351" />
                  <Point X="24.039467500159" Y="-4.673899460373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.880073813282" Y="-4.561623222249" />
                  <Point X="24.0643622642" Y="-4.580992718951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.870175420738" Y="-4.465059572711" />
                  <Point X="24.089256588808" Y="-4.488085931343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.850768428378" Y="-4.367496529062" />
                  <Point X="24.114150913416" Y="-4.395179143734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.43296753368" Y="-4.122956363707" />
                  <Point X="22.520096437121" Y="-4.132113980473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.831361115322" Y="-4.269933451706" />
                  <Point X="24.139045238024" Y="-4.302272356126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.248341946421" Y="-4.00802814599" />
                  <Point X="22.587924905385" Y="-4.043719753201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.809481840982" Y="-4.17211056075" />
                  <Point X="24.163939562632" Y="-4.209365568518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.104669177495" Y="-3.897404242925" />
                  <Point X="22.690330434955" Y="-3.958959721515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.749118815208" Y="-4.070242864529" />
                  <Point X="24.18883388724" Y="-4.11645878091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.040177802869" Y="-3.795102639755" />
                  <Point X="22.813859539709" Y="-3.876419867044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.627416206545" Y="-3.961928118357" />
                  <Point X="24.213728211848" Y="-4.023551993302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.092173014658" Y="-3.705044270169" />
                  <Point X="22.947787590448" Y="-3.794972985839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.503220894248" Y="-3.853351378476" />
                  <Point X="24.238622536456" Y="-3.930645205694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.818176167415" Y="-4.096662982137" />
                  <Point X="25.825868585772" Y="-4.097471487886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.144168226448" Y="-3.614985900583" />
                  <Point X="24.263516861064" Y="-3.837738418086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.791839154708" Y="-3.998371563999" />
                  <Point X="25.813116105507" Y="-4.000607861641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.196163438237" Y="-3.524927530997" />
                  <Point X="24.288411185672" Y="-3.744831630477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.765502142001" Y="-3.90008014586" />
                  <Point X="25.800363625243" Y="-3.903744235397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.248158650027" Y="-3.434869161411" />
                  <Point X="24.31330551028" Y="-3.651924842869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.73916504147" Y="-3.801788718491" />
                  <Point X="25.787611144978" Y="-3.806880609152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.702303151505" Y="-4.008122848267" />
                  <Point X="27.72601991947" Y="-4.010615581027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.300153861816" Y="-3.344810791825" />
                  <Point X="24.338199834888" Y="-3.559018055261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.712827813184" Y="-3.703497277695" />
                  <Point X="25.774858664714" Y="-3.710016982908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.622575274608" Y="-3.904219824178" />
                  <Point X="27.667306232781" Y="-3.90892123733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.352149073605" Y="-3.254752422239" />
                  <Point X="24.365305042909" Y="-3.466343640863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.686490584897" Y="-3.605205836898" />
                  <Point X="25.762106184449" Y="-3.613153356663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.542847397712" Y="-3.800316800089" />
                  <Point X="27.608592950892" Y="-3.807226936178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.404144285395" Y="-3.164694052653" />
                  <Point X="24.417518445837" Y="-3.37630820409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.660153356611" Y="-3.506914396101" />
                  <Point X="25.749353704185" Y="-3.516289730419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.463119520816" Y="-3.696413776" />
                  <Point X="27.549879715109" Y="-3.705532639873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.145381305021" Y="-2.936869445662" />
                  <Point X="21.31588839512" Y="-2.954790462974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.456139497184" Y="-3.074635683067" />
                  <Point X="24.479309550314" Y="-3.287279424314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.615921293989" Y="-3.406742132427" />
                  <Point X="25.73660122392" Y="-3.419426104174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.38339164392" Y="-3.592510751911" />
                  <Point X="27.491166479326" Y="-3.603838343567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.066351127292" Y="-2.83303975271" />
                  <Point X="21.455858651253" Y="-2.873978643146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.508134678197" Y="-2.984577310247" />
                  <Point X="24.541099544971" Y="-3.198250527891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.544405739235" Y="-3.303702258176" />
                  <Point X="25.724741917962" Y="-3.322656354332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.303663767024" Y="-3.488607727822" />
                  <Point X="27.432453243543" Y="-3.502144047262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.989320294199" Y="-2.729420199347" />
                  <Point X="21.595829021666" Y="-2.79316683533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.560129828077" Y="-2.894518934154" />
                  <Point X="24.627801096737" Y="-3.111839941627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.47289001857" Y="-3.200662366488" />
                  <Point X="25.734427668377" Y="-3.228151081164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.223935890127" Y="-3.384704703733" />
                  <Point X="27.37374000776" Y="-3.400449750956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.928545130682" Y="-2.627509185704" />
                  <Point X="21.735799392078" Y="-2.712355027513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.612124977956" Y="-2.804460558061" />
                  <Point X="24.833201420753" Y="-3.037905099047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.347482805196" Y="-3.091958250671" />
                  <Point X="25.75472674408" Y="-3.134761313434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.144208013231" Y="-3.280801679644" />
                  <Point X="27.315026771977" Y="-3.298755454651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.86776967081" Y="-2.525598140913" />
                  <Point X="21.87576976249" Y="-2.631543219697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.663154170008" Y="-2.714300655709" />
                  <Point X="25.775025468229" Y="-3.041371508754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.064480245592" Y="-3.176898667038" />
                  <Point X="27.256313536194" Y="-3.197061158345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.83988503295" Y="-2.427144060817" />
                  <Point X="22.015740132903" Y="-2.55073141188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.688111708219" Y="-2.621400512118" />
                  <Point X="25.805228157241" Y="-2.949022652727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.984753094457" Y="-3.07299571923" />
                  <Point X="27.197600300411" Y="-3.09536686204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.949375918831" Y="-2.343128730087" />
                  <Point X="22.155710503315" Y="-2.469919604064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.674966530764" Y="-2.524495611736" />
                  <Point X="25.877116957513" Y="-2.861055183545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.905025943323" Y="-2.969092771421" />
                  <Point X="27.138887064627" Y="-2.993672565734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.058866804712" Y="-2.259113399358" />
                  <Point X="22.295680873727" Y="-2.389107796247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.607878119567" Y="-2.421921049023" />
                  <Point X="25.979110242124" Y="-2.776251823168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.823706400868" Y="-2.865022456541" />
                  <Point X="27.080173828844" Y="-2.891978269429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.168357690593" Y="-2.175098068628" />
                  <Point X="26.093224268554" Y="-2.69272240409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.668344139165" Y="-2.753169938277" />
                  <Point X="27.040501780656" Y="-2.792285282584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.277848576474" Y="-2.091082737899" />
                  <Point X="27.022915634956" Y="-2.69491361763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.387339462355" Y="-2.007067407169" />
                  <Point X="27.005329756066" Y="-2.59754198072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.496830348236" Y="-1.92305207644" />
                  <Point X="27.003780421056" Y="-2.50185585249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.898678770214" Y="-2.701017694384" />
                  <Point X="29.033026896168" Y="-2.715138251422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.606321234117" Y="-1.83903674571" />
                  <Point X="27.034485403499" Y="-2.40955978963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.696404664985" Y="-2.584234542682" />
                  <Point X="29.080111400836" Y="-2.62456374572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.715811953276" Y="-1.755021397458" />
                  <Point X="27.082124305495" Y="-2.319043553435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.494130559757" Y="-2.467451390979" />
                  <Point X="28.935865170347" Y="-2.513879569415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.825302586558" Y="-1.671006040179" />
                  <Point X="27.131976508502" Y="-2.228759944549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.291856454528" Y="-2.350668239276" />
                  <Point X="28.791618953661" Y="-2.403195394562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.93479321984" Y="-1.5869906829" />
                  <Point X="27.225801680177" Y="-2.143098080908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.089582026377" Y="-2.233885053633" />
                  <Point X="28.647372736974" Y="-2.292511219709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.014050806222" Y="-1.499797704348" />
                  <Point X="27.377089834177" Y="-2.063475820081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.887307070039" Y="-2.117101812475" />
                  <Point X="28.503126520288" Y="-2.181827044856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.323736057547" Y="-1.226615178771" />
                  <Point X="20.586027716681" Y="-1.254183143021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.047010299409" Y="-1.407738600115" />
                  <Point X="28.358880303602" Y="-2.071142870002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.298663501386" Y="-1.128456660371" />
                  <Point X="20.989493642673" Y="-1.20106583407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.049112183764" Y="-1.312436230504" />
                  <Point X="28.214634086915" Y="-1.960458695149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.273590945225" Y="-1.030298141971" />
                  <Point X="21.392959568665" Y="-1.147948525118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.006462986885" Y="-1.212430332723" />
                  <Point X="28.070387870229" Y="-1.849774520296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.253307933445" Y="-0.932643024971" />
                  <Point X="27.976942823994" Y="-1.744429763613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.239436074468" Y="-0.835661747283" />
                  <Point X="27.911079400101" Y="-1.641983952254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.225566046926" Y="-0.738680662086" />
                  <Point X="27.866924372666" Y="-1.541819785304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.247753928618" Y="-0.645489415865" />
                  <Point X="27.861444453563" Y="-1.445720536039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.503811488929" Y="-0.576878863367" />
                  <Point X="27.87015047652" Y="-1.351112289366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.759869049241" Y="-0.508268310869" />
                  <Point X="27.883846302336" Y="-1.257028492106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.015926609552" Y="-0.439657758371" />
                  <Point X="27.928875321399" Y="-1.166237946161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.271984418493" Y="-0.371047232005" />
                  <Point X="28.001803603513" Y="-1.078379730923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.520181433473" Y="-0.301610502901" />
                  <Point X="28.087533384717" Y="-0.991867007457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.637147769603" Y="-0.218380873653" />
                  <Point X="28.318120663525" Y="-0.9205794205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.333173209472" Y="-1.027265742096" />
                  <Point X="29.730139468379" Y="-1.068988577165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.68763188967" Y="-0.128163681928" />
                  <Point X="29.751426935933" Y="-0.975702693604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.705282959157" Y="-0.034495597529" />
                  <Point X="29.769006480836" Y="-0.882027091669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.688703168032" Y="0.062770295297" />
                  <Point X="29.783183816034" Y="-0.787993903085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.628908364646" Y="0.164578268938" />
                  <Point X="29.268422723729" Y="-0.638367045575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.338744638495" Y="0.290598992035" />
                  <Point X="28.684359247417" Y="-0.481456213992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.752155094697" Y="0.44777532401" />
                  <Point X="28.490809657419" Y="-0.365590045791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.215717105088" Y="0.599680515233" />
                  <Point X="28.407537730416" Y="-0.261314527025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.22963568488" Y="0.693740900107" />
                  <Point X="28.368199363478" Y="-0.161656611493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.243554264673" Y="0.78780128498" />
                  <Point X="28.350438418054" Y="-0.064266574348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.257472844465" Y="0.881861669854" />
                  <Point X="28.355114339577" Y="0.030765253054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.273592533136" Y="0.975690708862" />
                  <Point X="28.374074659298" Y="0.124295729708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.298760171143" Y="1.068568770074" />
                  <Point X="28.41857886872" Y="0.215141435369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.323927813095" Y="1.161446830872" />
                  <Point X="28.491113827535" Y="0.303040990552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.349095455625" Y="1.254324891609" />
                  <Point X="28.613848651109" Y="0.385664327338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.400367328636" Y="1.239355051898" />
                  <Point X="28.830368368761" Y="0.458430474553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.508603915354" Y="1.323502214782" />
                  <Point X="29.086426628245" Y="0.527040953565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.554288556551" Y="1.414223852064" />
                  <Point X="29.342484328213" Y="0.595651491384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.577992419855" Y="1.507255762197" />
                  <Point X="29.598542028182" Y="0.664262029204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.564083909802" Y="1.604240892069" />
                  <Point X="29.779696776529" Y="0.740745184472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.513314725921" Y="1.705100234874" />
                  <Point X="28.252533223559" Y="0.996779828392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.57624928602" Y="0.962755899203" />
                  <Point X="29.764576931068" Y="0.837857630825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.400244423768" Y="1.812507689072" />
                  <Point X="28.10713963435" Y="1.107584596956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.979714777491" Y="1.015873253825" />
                  <Point X="29.74758618857" Y="0.935166716381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.255998387845" Y="1.923191844926" />
                  <Point X="28.043517491879" Y="1.209794840145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.383180268961" Y="1.068990608446" />
                  <Point X="29.723720065053" Y="1.0331984336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.111752351921" Y="2.03387600078" />
                  <Point X="28.013805815664" Y="1.308440949711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.967506315998" Y="2.144560156635" />
                  <Point X="28.001161043659" Y="1.405293255361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.823260280074" Y="2.255244312489" />
                  <Point X="28.017940247865" Y="1.499052976493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.80639201058" Y="2.352540525613" />
                  <Point X="28.051201351193" Y="1.591080380222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.858925290853" Y="2.442542341923" />
                  <Point X="28.113714294136" Y="1.680033291719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.911458571126" Y="2.532544158232" />
                  <Point X="28.202193705408" Y="1.766257017419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.9639918514" Y="2.622545974541" />
                  <Point X="28.311684679062" Y="1.850272338923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.022312371236" Y="2.711939527462" />
                  <Point X="27.171503583875" Y="2.065633487555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.617092157634" Y="2.018800241267" />
                  <Point X="28.421175652716" Y="1.934287660427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.091010048631" Y="2.800242397174" />
                  <Point X="27.077099876321" Y="2.171079003603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.759794293426" Y="2.099324928973" />
                  <Point X="28.530666557731" Y="2.018302989145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.159709536608" Y="2.888545076585" />
                  <Point X="21.609175631346" Y="2.84130428642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.115551416412" Y="2.788082046774" />
                  <Point X="27.028011011817" Y="2.271761737725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.899764484585" Y="2.180136755629" />
                  <Point X="28.640157447108" Y="2.102318319507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.228409024585" Y="2.976847755997" />
                  <Point X="21.40690197173" Y="2.958087391287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.199412369383" Y="2.874791192001" />
                  <Point X="27.014315230653" Y="2.368724508889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.039734675744" Y="2.260948582286" />
                  <Point X="28.749648336485" Y="2.186333649869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.24415055894" Y="2.965612305359" />
                  <Point X="27.020703783328" Y="2.463576331504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.179704866903" Y="2.341760408943" />
                  <Point X="28.859139225862" Y="2.270348980231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.249792880986" Y="3.060542559974" />
                  <Point X="27.047208582495" Y="2.556313851415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.319675058062" Y="2.4225722356" />
                  <Point X="28.968630115239" Y="2.354364310593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.238872266537" Y="3.157213649363" />
                  <Point X="27.093255935448" Y="2.646997366156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.459645249221" Y="2.503384062257" />
                  <Point X="29.078121004616" Y="2.438379640955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.196201684137" Y="3.257221794853" />
                  <Point X="27.145251142642" Y="2.737055736225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.59961544038" Y="2.584195888913" />
                  <Point X="29.108287386721" Y="2.530732312992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.13748827705" Y="3.358916109163" />
                  <Point X="27.197246349836" Y="2.827114106294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.739585631539" Y="2.66500771557" />
                  <Point X="29.046557780106" Y="2.632743642647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.078774869964" Y="3.460610423473" />
                  <Point X="27.249241557031" Y="2.917172476363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.879556540881" Y="2.745819466743" />
                  <Point X="28.972734347848" Y="2.736026084598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.020061462878" Y="3.562304737783" />
                  <Point X="27.301236681567" Y="3.007230855119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.961348055792" Y="3.663999052094" />
                  <Point X="27.353231754283" Y="3.097289239322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.011483564562" Y="3.754252884343" />
                  <Point X="27.405226826999" Y="3.187347623525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.121054768064" Y="3.838259773351" />
                  <Point X="27.457221899715" Y="3.277406007729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.230625971566" Y="3.922266662358" />
                  <Point X="27.509216972431" Y="3.367464391932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.340197175069" Y="4.006273551365" />
                  <Point X="27.561212045147" Y="3.457522776135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.481128740009" Y="4.086984333566" />
                  <Point X="27.613207117863" Y="3.547581160338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.625711646804" Y="4.167311344273" />
                  <Point X="22.916306920661" Y="4.136768550243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.362426369855" Y="4.089879506698" />
                  <Point X="27.665202190579" Y="3.637639544541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.770294486539" Y="4.247638362029" />
                  <Point X="22.827979264411" Y="4.241575447564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.463734455211" Y="4.174754884419" />
                  <Point X="27.717197263295" Y="3.727697928744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.504164196395" Y="4.266028833948" />
                  <Point X="24.879805981031" Y="4.121443056175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.084813677664" Y="4.099895878996" />
                  <Point X="27.769192336011" Y="3.817756312947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.531743013052" Y="4.358653470073" />
                  <Point X="24.78889167878" Y="4.226521820946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.197861065522" Y="4.183537406305" />
                  <Point X="27.807455904418" Y="3.90925793641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.535063176518" Y="4.453827793389" />
                  <Point X="24.757280041309" Y="4.325367624486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.241398881523" Y="4.274484684008" />
                  <Point X="27.649837778235" Y="4.021347555585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.522309641889" Y="4.550691530452" />
                  <Point X="24.730943440341" Y="4.423658999349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.266293288976" Y="4.367391462909" />
                  <Point X="27.45528322877" Y="4.137319349282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.737936818708" Y="4.623551487488" />
                  <Point X="24.704606393141" Y="4.521950421113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.29118769643" Y="4.460298241809" />
                  <Point X="27.255233926971" Y="4.253868664722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.985747924315" Y="4.693028777302" />
                  <Point X="24.678269302109" Y="4.620241847483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.316082103883" Y="4.55320502071" />
                  <Point X="26.993282456742" Y="4.376924160236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.354683115001" Y="4.74977541278" />
                  <Point X="24.651932211078" Y="4.718533273854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.340976511337" Y="4.646111799611" />
                  <Point X="26.677484338096" Y="4.505639166553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.36587091879" Y="4.739018578512" />
                  <Point X="26.175956569784" Y="4.653875145664" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.5663671875" Y="-3.891006835938" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.4255625" Y="-3.465701660156" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.214361328125" Y="-3.247785644531" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.931361328125" Y="-3.206380615234" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.67296484375" Y="-3.341486083984" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.235017578125" Y="-4.678207519531" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.1270625" Y="-4.982186523438" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.831216796875" Y="-4.920431152344" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.679435546875" Y="-4.637743164062" />
                  <Point X="23.6908515625" Y="-4.5510390625" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.675990234375" Y="-4.462725097656" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.5585" Y="-4.154204101562" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.27747265625" Y="-3.980959472656" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.949056640625" Y="-4.014593994141" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157293945313" />
                  <Point X="22.568609375" Y="-4.381002929688" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.477357421875" Y="-4.373913574219" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="22.049259765625" Y="-4.094535644531" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.317744140625" Y="-2.934346679688" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597591796875" />
                  <Point X="22.48601953125" Y="-2.568762207031" />
                  <Point X="22.46867578125" Y="-2.551417724609" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.42414453125" Y="-3.111681884766" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.101533203125" Y="-3.192969238281" />
                  <Point X="20.838302734375" Y="-2.847136962891" />
                  <Point X="20.7702578125" Y="-2.733036132812" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.527880859375" Y="-1.659737060547" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.84746484375" Y="-1.411081542969" />
                  <Point X="21.856001953125" Y="-1.388972045898" />
                  <Point X="21.8618828125" Y="-1.366266479492" />
                  <Point X="21.863349609375" Y="-1.350051513672" />
                  <Point X="21.85134375" Y="-1.321069091797" />
                  <Point X="21.832572265625" Y="-1.306949951172" />
                  <Point X="21.812359375" Y="-1.295053100586" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.538048828125" Y="-1.452139282227" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.1755625" Y="-1.414251464844" />
                  <Point X="20.072607421875" Y="-1.011187316895" />
                  <Point X="20.05460546875" Y="-0.88532598877" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="21.091298828125" Y="-0.222759277344" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.464673828125" Y="-0.116865516663" />
                  <Point X="21.485857421875" Y="-0.10216317749" />
                  <Point X="21.497677734375" Y="-0.090645019531" />
                  <Point X="21.50729296875" Y="-0.068850776672" />
                  <Point X="21.514353515625" Y="-0.046099815369" />
                  <Point X="21.516599609375" Y="-0.031280040741" />
                  <Point X="21.5121640625" Y="-0.009404351234" />
                  <Point X="21.505103515625" Y="0.01334661293" />
                  <Point X="21.497677734375" Y="0.028084936142" />
                  <Point X="21.479287109375" Y="0.044162780762" />
                  <Point X="21.458103515625" Y="0.058865123749" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.310009765625" Y="0.369545410156" />
                  <Point X="20.001814453125" Y="0.452125976563" />
                  <Point X="20.01600390625" Y="0.548014892578" />
                  <Point X="20.08235546875" Y="0.996414916992" />
                  <Point X="20.11859375" Y="1.130146850586" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.994958984375" Y="1.427127197266" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.2828359375" Y="1.400451171875" />
                  <Point X="21.32972265625" Y="1.415234008789" />
                  <Point X="21.348466796875" Y="1.426056152344" />
                  <Point X="21.360880859375" Y="1.443786132813" />
                  <Point X="21.36671484375" Y="1.457872192383" />
                  <Point X="21.38552734375" Y="1.503290527344" />
                  <Point X="21.389287109375" Y="1.524606079102" />
                  <Point X="21.38368359375" Y="1.545512817383" />
                  <Point X="21.37664453125" Y="1.559036621094" />
                  <Point X="21.353943359375" Y="1.602642578125" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.690412109375" Y="2.117693359375" />
                  <Point X="20.52389453125" Y="2.245466308594" />
                  <Point X="20.5821640625" Y="2.345296142578" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="20.9359765625" Y="2.910389648438" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.6862109375" Y="3.016221191406" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.88173828125" Y="2.918663085938" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404296875" />
                  <Point X="22.001123046875" Y="2.941778808594" />
                  <Point X="22.04747265625" Y="2.988127685547" />
                  <Point X="22.0591015625" Y="3.006382324219" />
                  <Point X="22.061927734375" Y="3.027841308594" />
                  <Point X="22.06015625" Y="3.048092773438" />
                  <Point X="22.054443359375" Y="3.113390869141" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.76006640625" Y="3.632632568359" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.798080078125" Y="3.830054199219" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.39830859375" Y="4.258325195312" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.9848359375" Y="4.349265625" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.07129296875" Y="4.261927246094" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.209669921875" Y="4.231975097656" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.29448828125" />
                  <Point X="23.32155859375" Y="4.318723144531" />
                  <Point X="23.346197265625" Y="4.396864746094" />
                  <Point X="23.348083984375" Y="4.41842578125" />
                  <Point X="23.31535546875" Y="4.667013671875" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.450591796875" Y="4.740315917969" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.215185546875" Y="4.92474609375" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.912115234375" Y="4.481625976562" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176269531" />
                  <Point X="25.054453125" Y="4.310902832031" />
                  <Point X="25.201953125" Y="4.86137890625" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.352634765625" Y="4.978722167969" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.011837890625" Y="4.88895703125" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.606994140625" Y="4.733317382812" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="27.02647265625" Y="4.571153320312" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.43090234375" Y="4.371417480469" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.819478515625" Y="4.133854980469" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.4338203125" Y="2.856875488281" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.219771484375" Y="2.472510253906" />
                  <Point X="27.2033828125" Y="2.411229248047" />
                  <Point X="27.20402734375" Y="2.375892578125" />
                  <Point X="27.210416015625" Y="2.322901855469" />
                  <Point X="27.218681640625" Y="2.300812255859" />
                  <Point X="27.2288515625" Y="2.285825927734" />
                  <Point X="27.261640625" Y="2.237503662109" />
                  <Point X="27.289927734375" Y="2.214034667969" />
                  <Point X="27.33825" Y="2.18124609375" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.376771484375" Y="2.170998535156" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.467669921875" Y="2.171028808594" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.680140625" Y="2.850080322266" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.0236796875" Y="2.990527587891" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.251068359375" Y="2.661773925781" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.55715625" Y="1.799140014648" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583832885742" />
                  <Point X="28.265693359375" Y="1.565988647461" />
                  <Point X="28.22158984375" Y="1.508451782227" />
                  <Point X="28.208025390625" Y="1.473281738281" />
                  <Point X="28.191595703125" Y="1.414536254883" />
                  <Point X="28.190779296875" Y="1.390965332031" />
                  <Point X="28.194962890625" Y="1.370694213867" />
                  <Point X="28.20844921875" Y="1.305332519531" />
                  <Point X="28.215646484375" Y="1.287955810547" />
                  <Point X="28.2270234375" Y="1.270664550781" />
                  <Point X="28.263703125" Y="1.214910522461" />
                  <Point X="28.280947265625" Y="1.198819946289" />
                  <Point X="28.29743359375" Y="1.189540039062" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.390857421875" Y="1.150673706055" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.557876953125" Y="1.283629516602" />
                  <Point X="29.848974609375" Y="1.321953125" />
                  <Point X="29.862345703125" Y="1.267027832031" />
                  <Point X="29.939193359375" Y="0.951365905762" />
                  <Point X="29.954466796875" Y="0.853269775391" />
                  <Point X="29.997859375" Y="0.57455645752" />
                  <Point X="29.048919921875" Y="0.320288635254" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819351196" />
                  <Point X="28.707189453125" Y="0.220161453247" />
                  <Point X="28.636578125" Y="0.179346984863" />
                  <Point X="28.622265625" Y="0.166926849365" />
                  <Point X="28.609126953125" Y="0.150184173584" />
                  <Point X="28.566759765625" Y="0.096199172974" />
                  <Point X="28.556986328125" Y="0.07473449707" />
                  <Point X="28.55260546875" Y="0.05186473465" />
                  <Point X="28.538484375" Y="-0.021876060486" />
                  <Point X="28.538484375" Y="-0.04068523407" />
                  <Point X="28.54286328125" Y="-0.063554851532" />
                  <Point X="28.556986328125" Y="-0.137295791626" />
                  <Point X="28.566759765625" Y="-0.158759246826" />
                  <Point X="28.5798984375" Y="-0.175501922607" />
                  <Point X="28.622265625" Y="-0.229486923218" />
                  <Point X="28.636578125" Y="-0.241907058716" />
                  <Point X="28.6584765625" Y="-0.254565109253" />
                  <Point X="28.729087890625" Y="-0.295379425049" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.733478515625" Y="-0.566276000977" />
                  <Point X="29.998068359375" Y="-0.637172546387" />
                  <Point X="29.991337890625" Y="-0.681819396973" />
                  <Point X="29.948431640625" Y="-0.966412658691" />
                  <Point X="29.92886328125" Y="-1.052163208008" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.76888671875" Y="-1.144615600586" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.351857421875" Y="-1.107683105469" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.185447265625" Y="-1.154697265625" />
                  <Point X="28.15946875" Y="-1.18594152832" />
                  <Point X="28.075703125" Y="-1.286685424805" />
                  <Point X="28.064359375" Y="-1.31407043457" />
                  <Point X="28.06063671875" Y="-1.354533203125" />
                  <Point X="28.048630859375" Y="-1.485000732422" />
                  <Point X="28.056361328125" Y="-1.516622070312" />
                  <Point X="28.080146484375" Y="-1.553619262695" />
                  <Point X="28.156841796875" Y="-1.672912719727" />
                  <Point X="28.168462890625" Y="-1.685540771484" />
                  <Point X="29.08933203125" Y="-2.392149658203" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.325078125" Y="-2.606432861328" />
                  <Point X="29.204134765625" Y="-2.802138427734" />
                  <Point X="29.163658203125" Y="-2.859651123047" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.071328125" Y="-2.442739257812" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.686189453125" Y="-2.244074707031" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.44658203125" Y="-2.241609863281" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334683349609" />
                  <Point X="27.266236328125" Y="-2.377178955078" />
                  <Point X="27.19412109375" Y="-2.514201416016" />
                  <Point X="27.1891640625" Y="-2.546375" />
                  <Point X="27.19840234375" Y="-2.597528076172" />
                  <Point X="27.22819140625" Y="-2.762465332031" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.825845703125" Y="-3.803522949219" />
                  <Point X="27.98667578125" Y="-4.082087646484" />
                  <Point X="27.978810546875" Y="-4.087705078125" />
                  <Point X="27.83529296875" Y="-4.190216308594" />
                  <Point X="27.790046875" Y="-4.219503417969" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="26.925744140625" Y="-3.308205078125" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.620099609375" Y="-2.948031982422" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.37062890625" Y="-2.840794433594" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.122728515625" Y="-2.903934326172" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045986083984" />
                  <Point X="25.95571875" Y="-3.104595214844" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310719970703" />
                  <Point X="26.08162890625" Y="-4.584512695312" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.9525390625" Y="-4.970841796875" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#140" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.045744944593" Y="4.525846939695" Z="0.6" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.6" />
                  <Point X="-0.796784583478" Y="5.005687487782" Z="0.6" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.6" />
                  <Point X="-1.569062786887" Y="4.819739703738" Z="0.6" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.6" />
                  <Point X="-1.740373088827" Y="4.691768543442" Z="0.6" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.6" />
                  <Point X="-1.732283985967" Y="4.365038704621" Z="0.6" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.6" />
                  <Point X="-1.815622555934" Y="4.309448942664" Z="0.6" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.6" />
                  <Point X="-1.911775591075" Y="4.337557860966" Z="0.6" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.6" />
                  <Point X="-1.98165327218" Y="4.410983523897" Z="0.6" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.6" />
                  <Point X="-2.632132117099" Y="4.333312995704" Z="0.6" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.6" />
                  <Point X="-3.238498003977" Y="3.901014203538" Z="0.6" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.6" />
                  <Point X="-3.289391418594" Y="3.638912686505" Z="0.6" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.6" />
                  <Point X="-2.995811788059" Y="3.075014839457" Z="0.6" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.6" />
                  <Point X="-3.040389049484" Y="3.008414518402" Z="0.6" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.6" />
                  <Point X="-3.12006162615" Y="2.999752911114" Z="0.6" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.6" />
                  <Point X="-3.294946661792" Y="3.090802530152" Z="0.6" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.6" />
                  <Point X="-4.109642638554" Y="2.972372117175" Z="0.6" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.6" />
                  <Point X="-4.46717423343" Y="2.401781179442" Z="0.6" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.6" />
                  <Point X="-4.346183281299" Y="2.109305784195" Z="0.6" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.6" />
                  <Point X="-3.67386246651" Y="1.567228160404" Z="0.6" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.6" />
                  <Point X="-3.685635345915" Y="1.508285989684" Z="0.6" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.6" />
                  <Point X="-3.738355240003" Y="1.479417236516" Z="0.6" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.6" />
                  <Point X="-4.004671927293" Y="1.507979465187" Z="0.6" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.6" />
                  <Point X="-4.935823105899" Y="1.174504172982" Z="0.6" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.6" />
                  <Point X="-5.039614891863" Y="0.58661383404" Z="0.6" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.6" />
                  <Point X="-4.709089184167" Y="0.352529119912" Z="0.6" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.6" />
                  <Point X="-3.555377533504" Y="0.034366680047" Z="0.6" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.6" />
                  <Point X="-3.541746670701" Y="0.007055915643" Z="0.6" />
                  <Point X="-3.539556741714" Y="0" Z="0.6" />
                  <Point X="-3.546617930434" Y="-0.022751035418" Z="0.6" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.6" />
                  <Point X="-3.569991061619" Y="-0.044509303087" Z="0.6" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.6" />
                  <Point X="-3.927798604606" Y="-0.143182936844" Z="0.6" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.6" />
                  <Point X="-5.001046697734" Y="-0.861125016538" Z="0.6" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.6" />
                  <Point X="-4.879040117186" Y="-1.395345540679" Z="0.6" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.6" />
                  <Point X="-4.461582886387" Y="-1.470431499045" Z="0.6" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.6" />
                  <Point X="-3.198945275209" Y="-1.318760160742" Z="0.6" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.6" />
                  <Point X="-3.198556990103" Y="-1.345155359404" Z="0.6" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.6" />
                  <Point X="-3.508713908788" Y="-1.588789474277" Z="0.6" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.6" />
                  <Point X="-4.278843160736" Y="-2.727365846712" Z="0.6" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.6" />
                  <Point X="-3.944408347177" Y="-3.1919722848" Z="0.6" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.6" />
                  <Point X="-3.557011647708" Y="-3.123703017983" Z="0.6" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.6" />
                  <Point X="-2.559597722396" Y="-2.568732746736" Z="0.6" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.6" />
                  <Point X="-2.731713995185" Y="-2.878066734784" Z="0.6" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.6" />
                  <Point X="-2.987400836144" Y="-4.102872883653" Z="0.6" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.6" />
                  <Point X="-2.555122731483" Y="-4.385144269316" Z="0.6" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.6" />
                  <Point X="-2.39788038955" Y="-4.380161308895" Z="0.6" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.6" />
                  <Point X="-2.029321897291" Y="-4.024887358065" Z="0.6" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.6" />
                  <Point X="-1.73195293951" Y="-3.999572505238" Z="0.6" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.6" />
                  <Point X="-1.480623603328" Y="-4.160515883274" Z="0.6" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.6" />
                  <Point X="-1.379206620598" Y="-4.441200149121" Z="0.6" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.6" />
                  <Point X="-1.376293318593" Y="-4.599936081094" Z="0.6" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.6" />
                  <Point X="-1.187399423128" Y="-4.937573802288" Z="0.6" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.6" />
                  <Point X="-0.888621198187" Y="-4.999990700724" Z="0.6" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.6" />
                  <Point X="-0.722842284261" Y="-4.659868236804" Z="0.6" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.6" />
                  <Point X="-0.292116778606" Y="-3.338715132313" Z="0.6" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.6" />
                  <Point X="-0.059975281863" Y="-3.222853482634" Z="0.6" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.6" />
                  <Point X="0.193383797498" Y="-3.264258562654" Z="0.6" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.6" />
                  <Point X="0.378329080827" Y="-3.462930623455" Z="0.6" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.6" />
                  <Point X="0.511912576141" Y="-3.872667793011" Z="0.6" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.6" />
                  <Point X="0.955319495944" Y="-4.988756712448" Z="0.6" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.6" />
                  <Point X="1.134669874037" Y="-4.951045646084" Z="0.6" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.6" />
                  <Point X="1.125043783872" Y="-4.546706316799" Z="0.6" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.6" />
                  <Point X="0.998421085363" Y="-3.083935087217" Z="0.6" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.6" />
                  <Point X="1.148537915705" Y="-2.91110079488" Z="0.6" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.6" />
                  <Point X="1.369054060549" Y="-2.859303971334" Z="0.6" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.6" />
                  <Point X="1.586903214758" Y="-2.958810142491" Z="0.6" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.6" />
                  <Point X="1.879919658255" Y="-3.307363059802" Z="0.6" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.6" />
                  <Point X="2.81105937212" Y="-4.230197569533" Z="0.6" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.6" />
                  <Point X="3.001715851659" Y="-4.097112251915" Z="0.6" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.6" />
                  <Point X="2.862988969318" Y="-3.747242848765" Z="0.6" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.6" />
                  <Point X="2.241449315031" Y="-2.557361213372" Z="0.6" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.6" />
                  <Point X="2.304325063252" Y="-2.369185779704" Z="0.6" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.6" />
                  <Point X="2.463712598668" Y="-2.254576246715" Z="0.6" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.6" />
                  <Point X="2.67114559014" Y="-2.26199869446" Z="0.6" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.6" />
                  <Point X="3.040170748342" Y="-2.454760530956" Z="0.6" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.6" />
                  <Point X="4.198389134189" Y="-2.857148380933" Z="0.6" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.6" />
                  <Point X="4.361454874305" Y="-2.601437989508" Z="0.6" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.6" />
                  <Point X="4.113613405915" Y="-2.321201973159" Z="0.6" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.6" />
                  <Point X="3.116048255206" Y="-1.495299666792" Z="0.6" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.6" />
                  <Point X="3.104268092336" Y="-1.327834925502" Z="0.6" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.6" />
                  <Point X="3.191756859995" Y="-1.186628456608" Z="0.6" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.6" />
                  <Point X="3.356319865131" Y="-1.12526230415" Z="0.6" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.6" />
                  <Point X="3.75620495042" Y="-1.16290786747" Z="0.6" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.6" />
                  <Point X="4.971451688861" Y="-1.032007228156" Z="0.6" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.6" />
                  <Point X="5.034622247469" Y="-0.657993373377" Z="0.6" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.6" />
                  <Point X="4.740263723245" Y="-0.486699562078" Z="0.6" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.6" />
                  <Point X="3.677341722367" Y="-0.179996226331" Z="0.6" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.6" />
                  <Point X="3.613076128315" Y="-0.113353220375" Z="0.6" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.6" />
                  <Point X="3.58581452825" Y="-0.022869690964" Z="0.6" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.6" />
                  <Point X="3.595556922174" Y="0.073740840268" Z="0.6" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.6" />
                  <Point X="3.642303310086" Y="0.1505955182" Z="0.6" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.6" />
                  <Point X="3.726053691985" Y="0.208152617911" Z="0.6" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.6" />
                  <Point X="4.055704094061" Y="0.30327237069" Z="0.6" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.6" />
                  <Point X="4.997713292515" Y="0.892241707398" Z="0.6" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.6" />
                  <Point X="4.904771740163" Y="1.31013474821" Z="0.6" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.6" />
                  <Point X="4.545195356283" Y="1.364481864039" Z="0.6" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.6" />
                  <Point X="3.391250407235" Y="1.231522712239" Z="0.6" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.6" />
                  <Point X="3.315804255256" Y="1.26439100797" Z="0.6" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.6" />
                  <Point X="3.262637203432" Y="1.329425068505" Z="0.6" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.6" />
                  <Point X="3.237774638594" Y="1.412078167591" Z="0.6" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.6" />
                  <Point X="3.250020825593" Y="1.491094630085" Z="0.6" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.6" />
                  <Point X="3.299219935494" Y="1.566850780321" Z="0.6" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.6" />
                  <Point X="3.581437114599" Y="1.790752281626" Z="0.6" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.6" />
                  <Point X="4.287688831957" Y="2.718940097594" Z="0.6" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.6" />
                  <Point X="4.058108922492" Y="3.051010866978" Z="0.6" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.6" />
                  <Point X="3.648983490483" Y="2.924661638005" Z="0.6" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.6" />
                  <Point X="2.44859705846" Y="2.25061146709" Z="0.6" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.6" />
                  <Point X="2.376601075812" Y="2.251918857252" Z="0.6" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.6" />
                  <Point X="2.311844541624" Y="2.286689141941" Z="0.6" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.6" />
                  <Point X="2.264069458294" Y="2.345180318758" Z="0.6" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.6" />
                  <Point X="2.247510845507" Y="2.41315736802" Z="0.6" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.6" />
                  <Point X="2.261916475567" Y="2.490872517422" Z="0.6" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.6" />
                  <Point X="2.470963714854" Y="2.863155591472" Z="0.6" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.6" />
                  <Point X="2.842298494292" Y="4.205881881344" Z="0.6" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.6" />
                  <Point X="2.449914766763" Y="4.445900202318" Z="0.6" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.6" />
                  <Point X="2.041496510848" Y="4.647725197997" Z="0.6" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.6" />
                  <Point X="1.617886738469" Y="4.811601465918" Z="0.6" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.6" />
                  <Point X="1.017416207602" Y="4.968840584994" Z="0.6" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.6" />
                  <Point X="0.351684914208" Y="5.059730095877" Z="0.6" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.6" />
                  <Point X="0.14749961696" Y="4.905600566256" Z="0.6" />
                  <Point X="0" Y="4.355124473572" Z="0.6" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>