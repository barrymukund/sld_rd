<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#197" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2950" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.878923828125" Y="-4.6904296875" />
                  <Point X="25.5633046875" Y="-3.512525146484" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.41336328125" Y="-3.281510498047" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209021240234" />
                  <Point X="25.33049609375" Y="-3.189777099609" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.102875" Y="-3.113713867188" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766357422" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.7635546875" Y="-3.158990966797" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231477050781" />
                  <Point X="24.50467578125" Y="-3.417343261719" />
                  <Point X="24.46994921875" Y="-3.467377441406" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.364046875" Y="-3.829609375" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.97995703125" Y="-4.856859863281" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.75358203125" Y="-4.802362304688" />
                  <Point X="23.7850390625" Y="-4.563436035156" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.73580078125" Y="-4.276471191406" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.18296484375" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.4925703125" Y="-3.970027099609" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.113046875" Y="-3.874978515625" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.016580078125" Y="-3.873708984375" />
                  <Point X="22.98553125" Y="-3.882029052734" />
                  <Point X="22.957341796875" Y="-3.894802001953" />
                  <Point X="22.75408984375" Y="-4.030610595703" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.664900390625" Y="-4.0994609375" />
                  <Point X="22.61719921875" Y="-4.161624511719" />
                  <Point X="22.519853515625" Y="-4.288489257813" />
                  <Point X="22.285203125" Y="-4.143200195312" />
                  <Point X="22.198287109375" Y="-4.089384033203" />
                  <Point X="21.89527734375" Y="-3.856077636719" />
                  <Point X="21.989697265625" Y="-3.692539306641" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647654296875" />
                  <Point X="22.593412109375" Y="-2.616127197266" />
                  <Point X="22.59442578125" Y="-2.585190917969" />
                  <Point X="22.585439453125" Y="-2.555571289062" />
                  <Point X="22.571220703125" Y="-2.526741699219" />
                  <Point X="22.55319921875" Y="-2.501591552734" />
                  <Point X="22.53586328125" Y="-2.484253662109" />
                  <Point X="22.510716796875" Y="-2.466230224609" />
                  <Point X="22.481884765625" Y="-2.452004882812" />
                  <Point X="22.452263671875" Y="-2.443014160156" />
                  <Point X="22.42132421875" Y="-2.444023681641" />
                  <Point X="22.389791015625" Y="-2.450293701172" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="22.087337890625" Y="-2.619090820312" />
                  <Point X="21.181978515625" Y="-3.141800537109" />
                  <Point X="20.9858046875" Y="-2.884070800781" />
                  <Point X="20.917134765625" Y="-2.793850097656" />
                  <Point X="20.693857421875" Y="-2.419449707031" />
                  <Point X="20.8677890625" Y="-2.285988525391" />
                  <Point X="21.894044921875" Y="-1.498513549805" />
                  <Point X="21.915421875" Y="-1.475592773438" />
                  <Point X="21.93338671875" Y="-1.448461303711" />
                  <Point X="21.94614453125" Y="-1.419829101562" />
                  <Point X="21.952212890625" Y="-1.396395751953" />
                  <Point X="21.953837890625" Y="-1.390126342773" />
                  <Point X="21.95665234375" Y="-1.359667114258" />
                  <Point X="21.9544453125" Y="-1.327993774414" />
                  <Point X="21.9474453125" Y="-1.298245849609" />
                  <Point X="21.93136328125" Y="-1.272259765625" />
                  <Point X="21.910529296875" Y="-1.248301147461" />
                  <Point X="21.887029296875" Y="-1.228766967773" />
                  <Point X="21.8661640625" Y="-1.216486328125" />
                  <Point X="21.860546875" Y="-1.213180419922" />
                  <Point X="21.83128125" Y="-1.201955810547" />
                  <Point X="21.79939453125" Y="-1.195474609375" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="21.422830078125" Y="-1.23983581543" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.192779296875" Y="-1.097793945313" />
                  <Point X="20.16592578125" Y="-0.99265234375" />
                  <Point X="20.107576171875" Y="-0.584698547363" />
                  <Point X="20.298064453125" Y="-0.533657592773" />
                  <Point X="21.467125" Y="-0.220408432007" />
                  <Point X="21.4825078125" Y="-0.214827392578" />
                  <Point X="21.51226953125" Y="-0.199471069336" />
                  <Point X="21.53413671875" Y="-0.184294616699" />
                  <Point X="21.5400234375" Y="-0.180209030151" />
                  <Point X="21.56248046875" Y="-0.158324188232" />
                  <Point X="21.581724609375" Y="-0.132068557739" />
                  <Point X="21.59583203125" Y="-0.104068252563" />
                  <Point X="21.60312109375" Y="-0.080583244324" />
                  <Point X="21.6050859375" Y="-0.074252830505" />
                  <Point X="21.60935546875" Y="-0.04608008194" />
                  <Point X="21.6093515625" Y="-0.016446353912" />
                  <Point X="21.60508203125" Y="0.011704078674" />
                  <Point X="21.597794921875" Y="0.03518012619" />
                  <Point X="21.595849609375" Y="0.04145665741" />
                  <Point X="21.58173046875" Y="0.069495521545" />
                  <Point X="21.562482421875" Y="0.095759658813" />
                  <Point X="21.5400234375" Y="0.117648742676" />
                  <Point X="21.51815625" Y="0.1328253479" />
                  <Point X="21.508279296875" Y="0.138825164795" />
                  <Point X="21.486826171875" Y="0.150130203247" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="21.152421875" Y="0.242172988892" />
                  <Point X="20.10818359375" Y="0.521975524902" />
                  <Point X="20.158205078125" Y="0.860017272949" />
                  <Point X="20.17551171875" Y="0.976967834473" />
                  <Point X="20.29644921875" Y="1.423268066406" />
                  <Point X="20.396322265625" Y="1.410119506836" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341674805" />
                  <Point X="21.276576171875" Y="1.301227783203" />
                  <Point X="21.296865234375" Y="1.305263427734" />
                  <Point X="21.34526171875" Y="1.320523071289" />
                  <Point X="21.358291015625" Y="1.324630859375" />
                  <Point X="21.3772265625" Y="1.332963867188" />
                  <Point X="21.395970703125" Y="1.343787109375" />
                  <Point X="21.4126484375" Y="1.356017578125" />
                  <Point X="21.426287109375" Y="1.371567749023" />
                  <Point X="21.438703125" Y="1.389299926758" />
                  <Point X="21.448650390625" Y="1.407434204102" />
                  <Point X="21.4680703125" Y="1.454317871094" />
                  <Point X="21.473296875" Y="1.466938598633" />
                  <Point X="21.479087890625" Y="1.486807739258" />
                  <Point X="21.48284375" Y="1.508122802734" />
                  <Point X="21.484193359375" Y="1.52875769043" />
                  <Point X="21.481048828125" Y="1.549196166992" />
                  <Point X="21.475447265625" Y="1.57010168457" />
                  <Point X="21.46794921875" Y="1.589379394531" />
                  <Point X="21.444517578125" Y="1.634392211914" />
                  <Point X="21.438208984375" Y="1.646509277344" />
                  <Point X="21.42671484375" Y="1.663713134766" />
                  <Point X="21.412802734375" Y="1.680290893555" />
                  <Point X="21.39786328125" Y="1.69458996582" />
                  <Point X="21.217349609375" Y="1.833104248047" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.8516015625" Y="2.618450195312" />
                  <Point X="20.9188515625" Y="2.733665039062" />
                  <Point X="21.239216796875" Y="3.145451416016" />
                  <Point X="21.24949609375" Y="3.158662597656" />
                  <Point X="21.27866015625" Y="3.141823974609" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.920611328125" Y="2.819899169922" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.00098828125" Y="2.826504882812" />
                  <Point X="22.0195390625" Y="2.835654541016" />
                  <Point X="22.03779296875" Y="2.847284179688" />
                  <Point X="22.053921875" Y="2.860228759766" />
                  <Point X="22.101767578125" Y="2.908072998047" />
                  <Point X="22.114646484375" Y="2.920952148438" />
                  <Point X="22.127595703125" Y="2.937086669922" />
                  <Point X="22.139224609375" Y="2.955341308594" />
                  <Point X="22.14837109375" Y="2.973889892578" />
                  <Point X="22.1532890625" Y="2.993978515625" />
                  <Point X="22.156115234375" Y="3.0154375" />
                  <Point X="22.15656640625" Y="3.036123535156" />
                  <Point X="22.15066796875" Y="3.103528076172" />
                  <Point X="22.14908203125" Y="3.12166796875" />
                  <Point X="22.145046875" Y="3.141953369141" />
                  <Point X="22.138537109375" Y="3.162599853516" />
                  <Point X="22.130205078125" Y="3.181533203125" />
                  <Point X="22.050212890625" Y="3.320083007812" />
                  <Point X="21.81666796875" Y="3.724595703125" />
                  <Point X="22.182259765625" Y="4.004890869141" />
                  <Point X="22.29937890625" Y="4.094686279297" />
                  <Point X="22.803947265625" Y="4.375013671875" />
                  <Point X="22.832962890625" Y="4.391134277344" />
                  <Point X="22.956802734375" Y="4.229743164063" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.07991015625" Y="4.150340820313" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.1193828125" Y="4.132331054688" />
                  <Point X="23.1402890625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.12358203125" />
                  <Point X="23.18137109375" Y="4.124934570312" />
                  <Point X="23.2026875" Y="4.128692871094" />
                  <Point X="23.222548828125" Y="4.134481445312" />
                  <Point X="23.3006875" Y="4.166848144531" />
                  <Point X="23.32172265625" Y="4.175561035156" />
                  <Point X="23.339857421875" Y="4.185509765625" />
                  <Point X="23.3575859375" Y="4.197923339844" />
                  <Point X="23.373134765625" Y="4.211560058594" />
                  <Point X="23.385365234375" Y="4.228239257812" />
                  <Point X="23.3961875" Y="4.246981933594" />
                  <Point X="23.404521484375" Y="4.265919921875" />
                  <Point X="23.429953125" Y="4.346582519531" />
                  <Point X="23.43680078125" Y="4.368296386719" />
                  <Point X="23.4408359375" Y="4.38858203125" />
                  <Point X="23.44272265625" Y="4.410145996094" />
                  <Point X="23.442271484375" Y="4.430828125" />
                  <Point X="23.43317578125" Y="4.499905273438" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.898751953125" Y="4.767301269531" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.662056640625" Y="4.881397949219" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.713837890625" Y="4.854550292969" />
                  <Point X="24.866095703125" Y="4.28631640625" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.187205078125" Y="4.439280273438" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.711654296875" Y="4.845603515625" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.350115234375" Y="4.709557617188" />
                  <Point X="26.48102734375" Y="4.677950195312" />
                  <Point X="26.810119140625" Y="4.558587402344" />
                  <Point X="26.894646484375" Y="4.527928710938" />
                  <Point X="27.213166015625" Y="4.378967285156" />
                  <Point X="27.294576171875" Y="4.340893554688" />
                  <Point X="27.602302734375" Y="4.161612304688" />
                  <Point X="27.680978515625" Y="4.115775878906" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.8264609375" Y="3.72694921875" />
                  <Point X="27.147583984375" Y="2.551098144531" />
                  <Point X="27.14208203125" Y="2.539940429688" />
                  <Point X="27.133078125" Y="2.516059326172" />
                  <Point X="27.116162109375" Y="2.452801513672" />
                  <Point X="27.114189453125" Y="2.443633056641" />
                  <Point X="27.108296875" Y="2.407699951172" />
                  <Point X="27.107728515625" Y="2.380953613281" />
                  <Point X="27.11432421875" Y="2.326253662109" />
                  <Point X="27.116099609375" Y="2.311528808594" />
                  <Point X="27.12144140625" Y="2.289608398438" />
                  <Point X="27.12970703125" Y="2.267518798828" />
                  <Point X="27.1400703125" Y="2.247469726562" />
                  <Point X="27.17391796875" Y="2.197588623047" />
                  <Point X="27.179734375" Y="2.189889892578" />
                  <Point X="27.202146484375" Y="2.163162353516" />
                  <Point X="27.221599609375" Y="2.145592285156" />
                  <Point X="27.27148046875" Y="2.111745849609" />
                  <Point X="27.284908203125" Y="2.102634765625" />
                  <Point X="27.304955078125" Y="2.092271728516" />
                  <Point X="27.32704296875" Y="2.084006103516" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.4036640625" Y="2.072067626953" />
                  <Point X="27.413798828125" Y="2.071392333984" />
                  <Point X="27.44742578125" Y="2.070954101563" />
                  <Point X="27.473205078125" Y="2.074170898438" />
                  <Point X="27.536462890625" Y="2.091086914063" />
                  <Point X="27.541546875" Y="2.092599609375" />
                  <Point X="27.57066015625" Y="2.102155029297" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="27.90506640625" Y="2.292894775391" />
                  <Point X="28.967326171875" Y="2.906190673828" />
                  <Point X="29.076603515625" Y="2.754321044922" />
                  <Point X="29.12328125" Y="2.689450927734" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="29.124591796875" Y="2.35429296875" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.221423828125" Y="1.660239868164" />
                  <Point X="28.203974609375" Y="1.641627319336" />
                  <Point X="28.15844921875" Y="1.58223425293" />
                  <Point X="28.146193359375" Y="1.566245849609" />
                  <Point X="28.13661328125" Y="1.550922485352" />
                  <Point X="28.121630859375" Y="1.51708972168" />
                  <Point X="28.104671875" Y="1.45644909668" />
                  <Point X="28.10010546875" Y="1.440125244141" />
                  <Point X="28.09665234375" Y="1.417826904297" />
                  <Point X="28.0958359375" Y="1.394252075195" />
                  <Point X="28.097740234375" Y="1.371766235352" />
                  <Point X="28.111662109375" Y="1.304296020508" />
                  <Point X="28.11391015625" Y="1.295529663086" />
                  <Point X="28.12485546875" Y="1.259993652344" />
                  <Point X="28.136283203125" Y="1.235742797852" />
                  <Point X="28.1741484375" Y="1.178190063477" />
                  <Point X="28.18433984375" Y="1.162697265625" />
                  <Point X="28.198890625" Y="1.145455200195" />
                  <Point X="28.21613671875" Y="1.129362426758" />
                  <Point X="28.23434765625" Y="1.116034667969" />
                  <Point X="28.28921875" Y="1.085146850586" />
                  <Point X="28.297775390625" Y="1.08088293457" />
                  <Point X="28.330521484375" Y="1.066570922852" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.43030859375" Y="1.049633544922" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.788888671875" Y="1.0865703125" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.825892578125" Y="1.015143371582" />
                  <Point X="29.845939453125" Y="0.932789733887" />
                  <Point X="29.890865234375" Y="0.644238525391" />
                  <Point X="29.74090234375" Y="0.604055908203" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.7047890625" Y="0.325585754395" />
                  <Point X="28.681546875" Y="0.315067901611" />
                  <Point X="28.608658203125" Y="0.272936950684" />
                  <Point X="28.589037109375" Y="0.261595489502" />
                  <Point X="28.5743125" Y="0.25109614563" />
                  <Point X="28.547533203125" Y="0.225577148438" />
                  <Point X="28.50380078125" Y="0.169850814819" />
                  <Point X="28.49202734375" Y="0.154849517822" />
                  <Point X="28.4803046875" Y="0.135571990967" />
                  <Point X="28.470529296875" Y="0.11410610199" />
                  <Point X="28.463681640625" Y="0.092603485107" />
                  <Point X="28.449103515625" Y="0.016483804703" />
                  <Point X="28.4451796875" Y="-0.004007134438" />
                  <Point X="28.443484375" Y="-0.021876350403" />
                  <Point X="28.4451796875" Y="-0.058553001404" />
                  <Point X="28.4597578125" Y="-0.134672531128" />
                  <Point X="28.463681640625" Y="-0.155163619995" />
                  <Point X="28.470529296875" Y="-0.176666244507" />
                  <Point X="28.4803046875" Y="-0.198132125854" />
                  <Point X="28.49202734375" Y="-0.217409500122" />
                  <Point X="28.535759765625" Y="-0.273135986328" />
                  <Point X="28.541662109375" Y="-0.279963531494" />
                  <Point X="28.56774609375" Y="-0.307383850098" />
                  <Point X="28.589037109375" Y="-0.324155456543" />
                  <Point X="28.66192578125" Y="-0.366286560059" />
                  <Point X="28.681546875" Y="-0.37762802124" />
                  <Point X="28.692708984375" Y="-0.383138305664" />
                  <Point X="28.716580078125" Y="-0.392149963379" />
                  <Point X="28.992322265625" Y="-0.466034667969" />
                  <Point X="29.891474609375" Y="-0.706961914062" />
                  <Point X="29.866216796875" Y="-0.874494018555" />
                  <Point X="29.855025390625" Y="-0.94872467041" />
                  <Point X="29.80117578125" Y="-1.18469934082" />
                  <Point X="29.612306640625" Y="-1.159834228516" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.23160546875" Y="-1.036602539062" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.1639765625" Y="-1.056596435547" />
                  <Point X="28.136150390625" Y="-1.073488037109" />
                  <Point X="28.1123984375" Y="-1.093959716797" />
                  <Point X="28.025931640625" Y="-1.197953369141" />
                  <Point X="28.002654296875" Y="-1.225947753906" />
                  <Point X="27.987935546875" Y="-1.250329589844" />
                  <Point X="27.976591796875" Y="-1.277715820312" />
                  <Point X="27.969759765625" Y="-1.305365966797" />
                  <Point X="27.9573671875" Y="-1.440042236328" />
                  <Point X="27.95403125" Y="-1.476296386719" />
                  <Point X="27.956349609375" Y="-1.507566772461" />
                  <Point X="27.96408203125" Y="-1.539188232422" />
                  <Point X="27.976453125" Y="-1.567997192383" />
                  <Point X="28.05562109375" Y="-1.691138793945" />
                  <Point X="28.07693359375" Y="-1.724287841797" />
                  <Point X="28.086935546875" Y="-1.737239746094" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.36651953125" Y="-1.957260131836" />
                  <Point X="29.213123046875" Y="-2.606881835938" />
                  <Point X="29.156357421875" Y="-2.698736328125" />
                  <Point X="29.124814453125" Y="-2.749775878906" />
                  <Point X="29.028982421875" Y="-2.885944580078" />
                  <Point X="28.858962890625" Y="-2.787784179688" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.583966796875" Y="-2.129076904297" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395507812" />
                  <Point X="27.474609375" Y="-2.125353515625" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.303390625" Y="-2.2096171875" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246549804688" />
                  <Point X="27.22142578125" Y="-2.267509277344" />
                  <Point X="27.204533203125" Y="-2.290438720703" />
                  <Point X="27.130091796875" Y="-2.431881347656" />
                  <Point X="27.110052734375" Y="-2.469956787109" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531908203125" />
                  <Point X="27.09567578125" Y="-2.563259033203" />
                  <Point X="27.12642578125" Y="-2.733517089844" />
                  <Point X="27.134703125" Y="-2.779349365234" />
                  <Point X="27.13898828125" Y="-2.795139648438" />
                  <Point X="27.1518203125" Y="-2.826078369141" />
                  <Point X="27.31625390625" Y="-3.110887695312" />
                  <Point X="27.86128515625" Y="-4.054907226563" />
                  <Point X="27.819291015625" Y="-4.084902587891" />
                  <Point X="27.781845703125" Y="-4.111646972656" />
                  <Point X="27.701767578125" Y="-4.163481445312" />
                  <Point X="27.565904296875" Y="-3.986422851562" />
                  <Point X="26.758548828125" Y="-2.934254882812" />
                  <Point X="26.74750390625" Y="-2.922177490234" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.55400390625" Y="-2.792599853516" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479990234375" Y="-2.751167236328" />
                  <Point X="26.448369140625" Y="-2.743435546875" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.233451171875" Y="-2.758016357422" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397705078" />
                  <Point X="26.128978515625" Y="-2.780740966797" />
                  <Point X="26.10459765625" Y="-2.7954609375" />
                  <Point X="25.962787109375" Y="-2.913370849609" />
                  <Point X="25.92461328125" Y="-2.945111328125" />
                  <Point X="25.904142578125" Y="-2.968861083984" />
                  <Point X="25.88725" Y="-2.9966875" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.833224609375" Y="-3.220883300781" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.866341796875" Y="-3.677078613281" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="26.011095703125" Y="-4.8623203125" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.929318359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.99805859375" Y="-4.763600585938" />
                  <Point X="23.94157421875" Y="-4.752636230469" />
                  <Point X="23.85875390625" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575836914062" />
                  <Point X="23.879923828125" Y="-4.568097167969" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688964844" />
                  <Point X="23.828974609375" Y="-4.257937011719" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.811873046875" Y="-4.178467773438" />
                  <Point X="23.800970703125" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135463867188" />
                  <Point X="23.778259765625" Y="-4.107626464844" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.74979296875" Y="-4.070937744141" />
                  <Point X="23.738994140625" Y="-4.059779541016" />
                  <Point X="23.555208984375" Y="-3.898602539062" />
                  <Point X="23.505734375" Y="-3.85521484375" />
                  <Point X="23.49326171875" Y="-3.845965332031" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.119259765625" Y="-3.780181884766" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.007267578125" Y="-3.779166503906" />
                  <Point X="22.991990234375" Y="-3.781946533203" />
                  <Point X="22.96094140625" Y="-3.790266601562" />
                  <Point X="22.946322265625" Y="-3.795497558594" />
                  <Point X="22.9181328125" Y="-3.808270507812" />
                  <Point X="22.9045625" Y="-3.8158125" />
                  <Point X="22.701310546875" Y="-3.95162109375" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.61955859375" Y="-4.010133056641" />
                  <Point X="22.597244140625" Y="-4.032770996094" />
                  <Point X="22.589533203125" Y="-4.041627441406" />
                  <Point X="22.54183203125" Y="-4.103791015625" />
                  <Point X="22.49680078125" Y="-4.162478515625" />
                  <Point X="22.33521484375" Y="-4.062429443359" />
                  <Point X="22.252408203125" Y="-4.011157714844" />
                  <Point X="22.01913671875" Y="-3.831547119141" />
                  <Point X="22.07196875" Y="-3.740039794922" />
                  <Point X="22.658509765625" Y="-2.724120117188" />
                  <Point X="22.6651484375" Y="-2.710084960938" />
                  <Point X="22.67605078125" Y="-2.681119628906" />
                  <Point X="22.680314453125" Y="-2.666188964844" />
                  <Point X="22.6865859375" Y="-2.634661865234" />
                  <Point X="22.688361328125" Y="-2.61923828125" />
                  <Point X="22.689375" Y="-2.588302001953" />
                  <Point X="22.685333984375" Y="-2.557610107422" />
                  <Point X="22.67634765625" Y="-2.527990478516" />
                  <Point X="22.670640625" Y="-2.513550048828" />
                  <Point X="22.656421875" Y="-2.484720458984" />
                  <Point X="22.64844140625" Y="-2.471407958984" />
                  <Point X="22.630419921875" Y="-2.4462578125" />
                  <Point X="22.62037890625" Y="-2.434420166016" />
                  <Point X="22.60304296875" Y="-2.417082275391" />
                  <Point X="22.59120703125" Y="-2.407038574219" />
                  <Point X="22.566060546875" Y="-2.389015136719" />
                  <Point X="22.55275" Y="-2.381035400391" />
                  <Point X="22.52391796875" Y="-2.366810058594" />
                  <Point X="22.5094765625" Y="-2.361100097656" />
                  <Point X="22.47985546875" Y="-2.352109375" />
                  <Point X="22.449166015625" Y="-2.348064697266" />
                  <Point X="22.4182265625" Y="-2.34907421875" />
                  <Point X="22.402796875" Y="-2.35084765625" />
                  <Point X="22.371263671875" Y="-2.357117675781" />
                  <Point X="22.356330078125" Y="-2.361381591797" />
                  <Point X="22.327357421875" Y="-2.37228515625" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="22.039837890625" Y="-2.536818359375" />
                  <Point X="21.2069140625" Y="-3.017707519531" />
                  <Point X="21.0613984375" Y="-2.826532226562" />
                  <Point X="20.99597265625" Y="-2.740572998047" />
                  <Point X="20.818734375" Y="-2.443373779297" />
                  <Point X="20.92562109375" Y="-2.361357421875" />
                  <Point X="21.951876953125" Y="-1.573882324219" />
                  <Point X="21.96351953125" Y="-1.563308227539" />
                  <Point X="21.984896484375" Y="-1.540387451172" />
                  <Point X="21.994630859375" Y="-1.528040771484" />
                  <Point X="22.012595703125" Y="-1.500909301758" />
                  <Point X="22.020162109375" Y="-1.487126464844" />
                  <Point X="22.032919921875" Y="-1.458494262695" />
                  <Point X="22.038111328125" Y="-1.443644897461" />
                  <Point X="22.0441796875" Y="-1.420211547852" />
                  <Point X="22.0441796875" Y="-1.420211425781" />
                  <Point X="22.048435546875" Y="-1.3988671875" />
                  <Point X="22.05125" Y="-1.368407958984" />
                  <Point X="22.051421875" Y="-1.353063476562" />
                  <Point X="22.04921484375" Y="-1.321390136719" />
                  <Point X="22.046919921875" Y="-1.306233642578" />
                  <Point X="22.039919921875" Y="-1.276485717773" />
                  <Point X="22.0282265625" Y="-1.248252441406" />
                  <Point X="22.01214453125" Y="-1.222266357422" />
                  <Point X="22.00305078125" Y="-1.209922119141" />
                  <Point X="21.982216796875" Y="-1.185963623047" />
                  <Point X="21.971255859375" Y="-1.175244995117" />
                  <Point X="21.947755859375" Y="-1.15571081543" />
                  <Point X="21.935216796875" Y="-1.146895263672" />
                  <Point X="21.9143515625" Y="-1.134614501953" />
                  <Point X="21.89456640625" Y="-1.124480712891" />
                  <Point X="21.86530078125" Y="-1.113256103516" />
                  <Point X="21.850203125" Y="-1.108859375" />
                  <Point X="21.81831640625" Y="-1.102378173828" />
                  <Point X="21.802701171875" Y="-1.100532104492" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="21.4104296875" Y="-1.14564855957" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.28482421875" Y="-1.074282470703" />
                  <Point X="20.2592421875" Y="-0.974116394043" />
                  <Point X="20.213548828125" Y="-0.654654663086" />
                  <Point X="20.32265234375" Y="-0.625420532227" />
                  <Point X="21.491712890625" Y="-0.312171478271" />
                  <Point X="21.499525390625" Y="-0.309712524414" />
                  <Point X="21.526068359375" Y="-0.299251556396" />
                  <Point X="21.555830078125" Y="-0.283895233154" />
                  <Point X="21.566435546875" Y="-0.277516479492" />
                  <Point X="21.588302734375" Y="-0.262339996338" />
                  <Point X="21.606326171875" Y="-0.248245361328" />
                  <Point X="21.628783203125" Y="-0.226360488892" />
                  <Point X="21.639103515625" Y="-0.214484771729" />
                  <Point X="21.65834765625" Y="-0.188229202271" />
                  <Point X="21.666564453125" Y="-0.174813598633" />
                  <Point X="21.680671875" Y="-0.146813354492" />
                  <Point X="21.6865625" Y="-0.132228393555" />
                  <Point X="21.6938515625" Y="-0.108743293762" />
                  <Point X="21.699013671875" Y="-0.088487327576" />
                  <Point X="21.703283203125" Y="-0.060314670563" />
                  <Point X="21.70435546875" Y="-0.046067531586" />
                  <Point X="21.7043515625" Y="-0.016433759689" />
                  <Point X="21.70327734375" Y="-0.002200738907" />
                  <Point X="21.6990078125" Y="0.025949626923" />
                  <Point X="21.6958125" Y="0.03986712265" />
                  <Point X="21.688525390625" Y="0.063343151093" />
                  <Point X="21.688525390625" Y="0.063343044281" />
                  <Point X="21.68069921875" Y="0.084183204651" />
                  <Point X="21.666580078125" Y="0.112222099304" />
                  <Point X="21.65835546875" Y="0.125651664734" />
                  <Point X="21.639107421875" Y="0.151915863037" />
                  <Point X="21.6287890625" Y="0.16379246521" />
                  <Point X="21.606330078125" Y="0.185681503296" />
                  <Point X="21.594189453125" Y="0.195693771362" />
                  <Point X="21.572322265625" Y="0.210870407104" />
                  <Point X="21.552568359375" Y="0.222869918823" />
                  <Point X="21.531115234375" Y="0.234174926758" />
                  <Point X="21.521478515625" Y="0.238584716797" />
                  <Point X="21.50177734375" Y="0.246302703857" />
                  <Point X="21.491712890625" Y="0.249611190796" />
                  <Point X="21.177009765625" Y="0.333935974121" />
                  <Point X="20.2145546875" Y="0.591824951172" />
                  <Point X="20.252181640625" Y="0.84611114502" />
                  <Point X="20.26866796875" Y="0.95752166748" />
                  <Point X="20.3664140625" Y="1.318237182617" />
                  <Point X="20.383921875" Y="1.315932250977" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364257812" />
                  <Point X="21.263294921875" Y="1.204703125" />
                  <Point X="21.28485546875" Y="1.206589355469" />
                  <Point X="21.295109375" Y="1.208053100586" />
                  <Point X="21.3153984375" Y="1.212088745117" />
                  <Point X="21.32543359375" Y="1.214660522461" />
                  <Point X="21.373830078125" Y="1.229920166016" />
                  <Point X="21.396556640625" Y="1.237678222656" />
                  <Point X="21.4154921875" Y="1.246011230469" />
                  <Point X="21.42473046875" Y="1.250694091797" />
                  <Point X="21.443474609375" Y="1.261517211914" />
                  <Point X="21.452150390625" Y="1.267178955078" />
                  <Point X="21.468828125" Y="1.279409423828" />
                  <Point X="21.4840703125" Y="1.293375732422" />
                  <Point X="21.497708984375" Y="1.308926025391" />
                  <Point X="21.504107421875" Y="1.317078491211" />
                  <Point X="21.5165234375" Y="1.334810791016" />
                  <Point X="21.52199609375" Y="1.343611450195" />
                  <Point X="21.531943359375" Y="1.361745605469" />
                  <Point X="21.53641796875" Y="1.371079223633" />
                  <Point X="21.555837890625" Y="1.417962890625" />
                  <Point X="21.564501953125" Y="1.440356201172" />
                  <Point X="21.57029296875" Y="1.460225219727" />
                  <Point X="21.572646484375" Y="1.470322021484" />
                  <Point X="21.57640234375" Y="1.491636962891" />
                  <Point X="21.577640625" Y="1.501922607422" />
                  <Point X="21.578990234375" Y="1.522557373047" />
                  <Point X="21.578087890625" Y="1.543203857422" />
                  <Point X="21.574943359375" Y="1.563642333984" />
                  <Point X="21.5728125" Y="1.573783813477" />
                  <Point X="21.5672109375" Y="1.594689331055" />
                  <Point X="21.563986328125" Y="1.604538696289" />
                  <Point X="21.55648828125" Y="1.62381640625" />
                  <Point X="21.55221484375" Y="1.633244750977" />
                  <Point X="21.528783203125" Y="1.678257568359" />
                  <Point X="21.517201171875" Y="1.699284912109" />
                  <Point X="21.50570703125" Y="1.716488769531" />
                  <Point X="21.499486328125" Y="1.724782348633" />
                  <Point X="21.48557421875" Y="1.741360107422" />
                  <Point X="21.478490234375" Y="1.748920898438" />
                  <Point X="21.46355078125" Y="1.763219970703" />
                  <Point X="21.4556953125" Y="1.769958251953" />
                  <Point X="21.275181640625" Y="1.90847253418" />
                  <Point X="20.77238671875" Y="2.294280517578" />
                  <Point X="20.9336484375" Y="2.570560546875" />
                  <Point X="20.997716796875" Y="2.680322998047" />
                  <Point X="21.27366015625" Y="3.035013427734" />
                  <Point X="21.745841796875" Y="2.762399658203" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774015625" Y="2.749385742188" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.91233203125" Y="2.725260742188" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310546875" />
                  <Point X="22.023568359375" Y="2.734227294922" />
                  <Point X="22.04301171875" Y="2.7413046875" />
                  <Point X="22.0615625" Y="2.750454345703" />
                  <Point X="22.070583984375" Y="2.755533691406" />
                  <Point X="22.088837890625" Y="2.767163330078" />
                  <Point X="22.097255859375" Y="2.773194580078" />
                  <Point X="22.113384765625" Y="2.786139160156" />
                  <Point X="22.121095703125" Y="2.793052490234" />
                  <Point X="22.16894140625" Y="2.840896728516" />
                  <Point X="22.188736328125" Y="2.861489746094" />
                  <Point X="22.201685546875" Y="2.877624267578" />
                  <Point X="22.20771875" Y="2.886044921875" />
                  <Point X="22.21934765625" Y="2.904299560547" />
                  <Point X="22.2244296875" Y="2.913326416016" />
                  <Point X="22.233576171875" Y="2.931875" />
                  <Point X="22.240646484375" Y="2.951299804688" />
                  <Point X="22.245564453125" Y="2.971388427734" />
                  <Point X="22.2474765625" Y="2.981573974609" />
                  <Point X="22.250302734375" Y="3.003032958984" />
                  <Point X="22.251091796875" Y="3.013365966797" />
                  <Point X="22.25154296875" Y="3.034052001953" />
                  <Point X="22.251205078125" Y="3.044405029297" />
                  <Point X="22.245306640625" Y="3.111809570312" />
                  <Point X="22.242255859375" Y="3.140202148438" />
                  <Point X="22.238220703125" Y="3.160487548828" />
                  <Point X="22.235650390625" Y="3.170520263672" />
                  <Point X="22.229140625" Y="3.191166748047" />
                  <Point X="22.225490234375" Y="3.200865234375" />
                  <Point X="22.217158203125" Y="3.219798583984" />
                  <Point X="22.2124765625" Y="3.229033447266" />
                  <Point X="22.132484375" Y="3.367583251953" />
                  <Point X="21.94061328125" Y="3.699914550781" />
                  <Point X="22.2400625" Y="3.929499023438" />
                  <Point X="22.351634765625" Y="4.015041748047" />
                  <Point X="22.807474609375" Y="4.268296386719" />
                  <Point X="22.88143359375" Y="4.171910644531" />
                  <Point X="22.888177734375" Y="4.16405078125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93491015625" Y="4.121895996094" />
                  <Point X="22.952111328125" Y="4.110403320312" />
                  <Point X="22.9610234375" Y="4.105128417969" />
                  <Point X="23.036044921875" Y="4.066074707031" />
                  <Point X="23.056240234375" Y="4.055561523438" />
                  <Point X="23.065673828125" Y="4.051286865234" />
                  <Point X="23.084951171875" Y="4.043790283203" />
                  <Point X="23.094794921875" Y="4.040568359375" />
                  <Point X="23.115701171875" Y="4.034966308594" />
                  <Point X="23.1258359375" Y="4.032834960938" />
                  <Point X="23.146279296875" Y="4.029687988281" />
                  <Point X="23.1669453125" Y="4.028785400391" />
                  <Point X="23.187583984375" Y="4.030137939453" />
                  <Point X="23.197865234375" Y="4.031377685547" />
                  <Point X="23.219181640625" Y="4.035135986328" />
                  <Point X="23.22926953125" Y="4.037487548828" />
                  <Point X="23.249130859375" Y="4.043276123047" />
                  <Point X="23.258904296875" Y="4.046713134766" />
                  <Point X="23.33704296875" Y="4.079079833984" />
                  <Point X="23.358078125" Y="4.087792724609" />
                  <Point X="23.367416015625" Y="4.092271240234" />
                  <Point X="23.38555078125" Y="4.102220214844" />
                  <Point X="23.39434765625" Y="4.107689941406" />
                  <Point X="23.412076171875" Y="4.120103515625" />
                  <Point X="23.4202265625" Y="4.126500488281" />
                  <Point X="23.435775390625" Y="4.140137207031" />
                  <Point X="23.44974609375" Y="4.155383300781" />
                  <Point X="23.4619765625" Y="4.1720625" />
                  <Point X="23.467634765625" Y="4.180735351563" />
                  <Point X="23.47845703125" Y="4.199478027344" />
                  <Point X="23.483140625" Y="4.208716796875" />
                  <Point X="23.491474609375" Y="4.227654785156" />
                  <Point X="23.495125" Y="4.237354003906" />
                  <Point X="23.520556640625" Y="4.318016601563" />
                  <Point X="23.527404296875" Y="4.33973046875" />
                  <Point X="23.529974609375" Y="4.349762207031" />
                  <Point X="23.534009765625" Y="4.370047851562" />
                  <Point X="23.535474609375" Y="4.380301757812" />
                  <Point X="23.537361328125" Y="4.401865722656" />
                  <Point X="23.53769921875" Y="4.412217773438" />
                  <Point X="23.537248046875" Y="4.432899902344" />
                  <Point X="23.536458984375" Y="4.443229980469" />
                  <Point X="23.52736328125" Y="4.512307128906" />
                  <Point X="23.520734375" Y="4.562655273438" />
                  <Point X="23.9243984375" Y="4.675828613281" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.63477734375" Y="4.782557128906" />
                  <Point X="24.77433203125" Y="4.26173046875" />
                  <Point X="24.779564453125" Y="4.247107910156" />
                  <Point X="24.79233984375" Y="4.218913085938" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.247104980469" />
                  <Point X="25.23798046875" Y="4.2617265625" />
                  <Point X="25.27896875" Y="4.414691894531" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.701759765625" Y="4.751120117188" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.3278203125" Y="4.6172109375" />
                  <Point X="26.453595703125" Y="4.586843261719" />
                  <Point X="26.7777265625" Y="4.469280273438" />
                  <Point X="26.858263671875" Y="4.440068847656" />
                  <Point X="27.172921875" Y="4.292913085938" />
                  <Point X="27.25044921875" Y="4.256655273438" />
                  <Point X="27.55448046875" Y="4.079527099609" />
                  <Point X="27.6294296875" Y="4.035861572266" />
                  <Point X="27.81778125" Y="3.901915527344" />
                  <Point X="27.744189453125" Y="3.77444921875" />
                  <Point X="27.0653125" Y="2.598598144531" />
                  <Point X="27.06237890625" Y="2.593113037109" />
                  <Point X="27.053189453125" Y="2.573455322266" />
                  <Point X="27.044185546875" Y="2.54957421875" />
                  <Point X="27.041302734375" Y="2.540601318359" />
                  <Point X="27.02438671875" Y="2.477343505859" />
                  <Point X="27.02044140625" Y="2.459006591797" />
                  <Point X="27.014548828125" Y="2.423073486328" />
                  <Point X="27.013318359375" Y="2.409718261719" />
                  <Point X="27.01275" Y="2.382971923828" />
                  <Point X="27.013412109375" Y="2.369580810547" />
                  <Point X="27.0200078125" Y="2.314880859375" />
                  <Point X="27.02380078125" Y="2.289036376953" />
                  <Point X="27.029142578125" Y="2.267115966797" />
                  <Point X="27.032466796875" Y="2.256315185547" />
                  <Point X="27.040732421875" Y="2.234225585938" />
                  <Point X="27.045314453125" Y="2.223896728516" />
                  <Point X="27.055677734375" Y="2.20384765625" />
                  <Point X="27.061458984375" Y="2.194127441406" />
                  <Point X="27.095306640625" Y="2.144246337891" />
                  <Point X="27.106939453125" Y="2.128848876953" />
                  <Point X="27.1293515625" Y="2.102121337891" />
                  <Point X="27.138470703125" Y="2.092661621094" />
                  <Point X="27.157923828125" Y="2.075091552734" />
                  <Point X="27.1682578125" Y="2.066981201172" />
                  <Point X="27.218138671875" Y="2.033134765625" />
                  <Point X="27.241283203125" Y="2.018243774414" />
                  <Point X="27.261330078125" Y="2.007880737305" />
                  <Point X="27.27166015625" Y="2.003297485352" />
                  <Point X="27.293748046875" Y="1.995031860352" />
                  <Point X="27.304548828125" Y="1.991707397461" />
                  <Point X="27.326470703125" Y="1.986365112305" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.392291015625" Y="1.977750854492" />
                  <Point X="27.412560546875" Y="1.976400390625" />
                  <Point X="27.4461875" Y="1.975962158203" />
                  <Point X="27.459189453125" Y="1.976685302734" />
                  <Point X="27.48496875" Y="1.979902099609" />
                  <Point X="27.49774609375" Y="1.982395629883" />
                  <Point X="27.56100390625" Y="1.999311645508" />
                  <Point X="27.571171875" Y="2.002336914062" />
                  <Point X="27.60028515625" Y="2.011892578125" />
                  <Point X="27.609427734375" Y="2.015425170898" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="27.95256640625" Y="2.210622314453" />
                  <Point X="28.940404296875" Y="2.780950683594" />
                  <Point X="28.999490234375" Y="2.698835205078" />
                  <Point X="29.043966796875" Y="2.637024902344" />
                  <Point X="29.13688671875" Y="2.483471435547" />
                  <Point X="29.066759765625" Y="2.429661376953" />
                  <Point X="28.172953125" Y="1.743819824219" />
                  <Point X="28.168138671875" Y="1.739869018555" />
                  <Point X="28.1521171875" Y="1.725214233398" />
                  <Point X="28.13466796875" Y="1.7066015625" />
                  <Point X="28.128576171875" Y="1.699420898438" />
                  <Point X="28.08305078125" Y="1.640027832031" />
                  <Point X="28.070794921875" Y="1.624039428711" />
                  <Point X="28.065640625" Y="1.616607055664" />
                  <Point X="28.04975" Y="1.589389038086" />
                  <Point X="28.034767578125" Y="1.555556274414" />
                  <Point X="28.030140625" Y="1.542676025391" />
                  <Point X="28.013181640625" Y="1.482035400391" />
                  <Point X="28.006224609375" Y="1.454663696289" />
                  <Point X="28.002771484375" Y="1.432365356445" />
                  <Point X="28.001708984375" Y="1.421114868164" />
                  <Point X="28.000892578125" Y="1.397540039063" />
                  <Point X="28.001173828125" Y="1.386235351562" />
                  <Point X="28.003078125" Y="1.363749511719" />
                  <Point X="28.004701171875" Y="1.352568237305" />
                  <Point X="28.018623046875" Y="1.285098022461" />
                  <Point X="28.023119140625" Y="1.267565429688" />
                  <Point X="28.034064453125" Y="1.232029418945" />
                  <Point X="28.038919921875" Y="1.219497802734" />
                  <Point X="28.05034765625" Y="1.195246948242" />
                  <Point X="28.056919921875" Y="1.183527832031" />
                  <Point X="28.09478515625" Y="1.125975097656" />
                  <Point X="28.11173828125" Y="1.101427612305" />
                  <Point X="28.1262890625" Y="1.084185546875" />
                  <Point X="28.134078125" Y="1.075997802734" />
                  <Point X="28.15132421875" Y="1.059905029297" />
                  <Point X="28.16003125" Y="1.052699951172" />
                  <Point X="28.1782421875" Y="1.039372192383" />
                  <Point X="28.18774609375" Y="1.033249755859" />
                  <Point X="28.2426171875" Y="1.002361816406" />
                  <Point X="28.25973046875" Y="0.993833984375" />
                  <Point X="28.2924765625" Y="0.979521911621" />
                  <Point X="28.3050234375" Y="0.97505682373" />
                  <Point X="28.33062109375" Y="0.967924560547" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.417861328125" Y="0.955452575684" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="28.8012890625" Y="0.992383117676" />
                  <Point X="29.704703125" Y="1.111319824219" />
                  <Point X="29.733587890625" Y="0.992672363281" />
                  <Point X="29.7526875" Y="0.914208251953" />
                  <Point X="29.78387109375" Y="0.713920837402" />
                  <Point X="29.716314453125" Y="0.695818908691" />
                  <Point X="28.6919921875" Y="0.42135269165" />
                  <Point X="28.686033203125" Y="0.419544525146" />
                  <Point X="28.665623046875" Y="0.412136138916" />
                  <Point X="28.642380859375" Y="0.401618225098" />
                  <Point X="28.634005859375" Y="0.397316497803" />
                  <Point X="28.5611171875" Y="0.35518560791" />
                  <Point X="28.54149609375" Y="0.343844055176" />
                  <Point X="28.5338828125" Y="0.33894543457" />
                  <Point X="28.508775390625" Y="0.319870147705" />
                  <Point X="28.48199609375" Y="0.294351135254" />
                  <Point X="28.472798828125" Y="0.284226654053" />
                  <Point X="28.42906640625" Y="0.228500350952" />
                  <Point X="28.410857421875" Y="0.204209121704" />
                  <Point X="28.399134765625" Y="0.184931686401" />
                  <Point X="28.39384765625" Y="0.174943939209" />
                  <Point X="28.384072265625" Y="0.153478027344" />
                  <Point X="28.3800078125" Y="0.14293309021" />
                  <Point X="28.37316015625" Y="0.121430480957" />
                  <Point X="28.370376953125" Y="0.110472663879" />
                  <Point X="28.355798828125" Y="0.034353065491" />
                  <Point X="28.351875" Y="0.013862127304" />
                  <Point X="28.350603515625" Y="0.004965560436" />
                  <Point X="28.3485859375" Y="-0.026262935638" />
                  <Point X="28.35028125" Y="-0.062939498901" />
                  <Point X="28.351875" Y="-0.076422271729" />
                  <Point X="28.366453125" Y="-0.152541870117" />
                  <Point X="28.370376953125" Y="-0.173032958984" />
                  <Point X="28.37316015625" Y="-0.183990631104" />
                  <Point X="28.3800078125" Y="-0.205493240356" />
                  <Point X="28.384072265625" Y="-0.21603817749" />
                  <Point X="28.39384765625" Y="-0.237504089355" />
                  <Point X="28.399134765625" Y="-0.247492141724" />
                  <Point X="28.410857421875" Y="-0.26676940918" />
                  <Point X="28.41729296875" Y="-0.276058807373" />
                  <Point X="28.461025390625" Y="-0.331785400391" />
                  <Point X="28.472830078125" Y="-0.345440429688" />
                  <Point X="28.4989140625" Y="-0.372860748291" />
                  <Point X="28.5089609375" Y="-0.382010864258" />
                  <Point X="28.530251953125" Y="-0.398782531738" />
                  <Point X="28.54149609375" Y="-0.406404052734" />
                  <Point X="28.614384765625" Y="-0.448535064697" />
                  <Point X="28.634005859375" Y="-0.459876647949" />
                  <Point X="28.639494140625" Y="-0.462813568115" />
                  <Point X="28.65915625" Y="-0.472015869141" />
                  <Point X="28.68302734375" Y="-0.481027618408" />
                  <Point X="28.6919921875" Y="-0.483912963867" />
                  <Point X="28.967734375" Y="-0.557797607422" />
                  <Point X="29.784880859375" Y="-0.776751220703" />
                  <Point X="29.772279296875" Y="-0.860331542969" />
                  <Point X="29.761619140625" Y="-0.931036437988" />
                  <Point X="29.7278046875" Y="-1.079219970703" />
                  <Point X="29.62470703125" Y="-1.065646972656" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042480469" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.211427734375" Y="-0.943770202637" />
                  <Point X="28.17291796875" Y="-0.952140075684" />
                  <Point X="28.157875" Y="-0.956742492676" />
                  <Point X="28.128755859375" Y="-0.968366271973" />
                  <Point X="28.1146796875" Y="-0.975387939453" />
                  <Point X="28.086853515625" Y="-0.992279541016" />
                  <Point X="28.07412890625" Y="-1.001527893066" />
                  <Point X="28.050376953125" Y="-1.021999511719" />
                  <Point X="28.039349609375" Y="-1.033222900391" />
                  <Point X="27.9528828125" Y="-1.137216552734" />
                  <Point X="27.92960546875" Y="-1.1652109375" />
                  <Point X="27.92132421875" Y="-1.176850952148" />
                  <Point X="27.90660546875" Y="-1.201232788086" />
                  <Point X="27.90016796875" Y="-1.213974731445" />
                  <Point X="27.88882421875" Y="-1.241360839844" />
                  <Point X="27.884365234375" Y="-1.254927734375" />
                  <Point X="27.877533203125" Y="-1.282577758789" />
                  <Point X="27.87516015625" Y="-1.296661132812" />
                  <Point X="27.862767578125" Y="-1.431337402344" />
                  <Point X="27.859431640625" Y="-1.467591430664" />
                  <Point X="27.859291015625" Y="-1.48332019043" />
                  <Point X="27.861609375" Y="-1.514590698242" />
                  <Point X="27.864068359375" Y="-1.530132324219" />
                  <Point X="27.87180078125" Y="-1.56175378418" />
                  <Point X="27.876791015625" Y="-1.576672973633" />
                  <Point X="27.889162109375" Y="-1.605481933594" />
                  <Point X="27.89654296875" Y="-1.619371704102" />
                  <Point X="27.9757109375" Y="-1.742513183594" />
                  <Point X="27.9970234375" Y="-1.775662353516" />
                  <Point X="28.001744140625" Y="-1.782352294922" />
                  <Point X="28.019796875" Y="-1.804451538086" />
                  <Point X="28.0434921875" Y="-1.82812097168" />
                  <Point X="28.052798828125" Y="-1.836277587891" />
                  <Point X="28.3086875" Y="-2.032628662109" />
                  <Point X="29.087171875" Y="-2.62998046875" />
                  <Point X="29.075544921875" Y="-2.648794189453" />
                  <Point X="29.045501953125" Y="-2.697406738281" />
                  <Point X="29.0012734375" Y="-2.760250732422" />
                  <Point X="28.906462890625" Y="-2.70551171875" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.600849609375" Y="-2.035589233398" />
                  <Point X="27.555017578125" Y="-2.027311889648" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403442383" />
                  <Point X="27.492314453125" Y="-2.02650378418" />
                  <Point X="27.460140625" Y="-2.031461914062" />
                  <Point X="27.444845703125" Y="-2.03513659668" />
                  <Point X="27.4150703125" Y="-2.044960083008" />
                  <Point X="27.40058984375" Y="-2.051108886719" />
                  <Point X="27.259146484375" Y="-2.125549072266" />
                  <Point X="27.221072265625" Y="-2.145587890625" />
                  <Point X="27.20896875" Y="-2.153170654297" />
                  <Point X="27.186037109375" Y="-2.170064453125" />
                  <Point X="27.175208984375" Y="-2.179375488281" />
                  <Point X="27.15425" Y="-2.200334960938" />
                  <Point X="27.14494140625" Y="-2.211161376953" />
                  <Point X="27.128048828125" Y="-2.234090820312" />
                  <Point X="27.12046484375" Y="-2.246193847656" />
                  <Point X="27.0460234375" Y="-2.387636474609" />
                  <Point X="27.025984375" Y="-2.425711914062" />
                  <Point X="27.0198359375" Y="-2.440193359375" />
                  <Point X="27.01001171875" Y="-2.469971923828" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517441894531" />
                  <Point X="27.000279296875" Y="-2.533133300781" />
                  <Point X="27.00068359375" Y="-2.564484130859" />
                  <Point X="27.0021875" Y="-2.580143554688" />
                  <Point X="27.0329375" Y="-2.750401611328" />
                  <Point X="27.04121484375" Y="-2.796233886719" />
                  <Point X="27.04301953125" Y="-2.80423046875" />
                  <Point X="27.051236328125" Y="-2.83153515625" />
                  <Point X="27.064068359375" Y="-2.862473876953" />
                  <Point X="27.069546875" Y="-2.873578125" />
                  <Point X="27.23398046875" Y="-3.158387451172" />
                  <Point X="27.73589453125" Y="-4.027724609375" />
                  <Point X="27.723755859375" Y="-4.036082519531" />
                  <Point X="27.6412734375" Y="-3.928590087891" />
                  <Point X="26.83391796875" Y="-2.876422119141" />
                  <Point X="26.828654296875" Y="-2.870143066406" />
                  <Point X="26.808830078125" Y="-2.849624023438" />
                  <Point X="26.783251953125" Y="-2.828003662109" />
                  <Point X="26.77330078125" Y="-2.820646728516" />
                  <Point X="26.60537890625" Y="-2.712689453125" />
                  <Point X="26.56017578125" Y="-2.683627929688" />
                  <Point X="26.546283203125" Y="-2.676245605469" />
                  <Point X="26.51747265625" Y="-2.663874511719" />
                  <Point X="26.5025546875" Y="-2.658885742188" />
                  <Point X="26.47093359375" Y="-2.651154052734" />
                  <Point X="26.45539453125" Y="-2.648695800781" />
                  <Point X="26.424125" Y="-2.646376953125" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.22474609375" Y="-2.663416015625" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670339355469" />
                  <Point X="26.133576171875" Y="-2.677171386719" />
                  <Point X="26.1200078125" Y="-2.681629394531" />
                  <Point X="26.092623046875" Y="-2.69297265625" />
                  <Point X="26.079876953125" Y="-2.6994140625" />
                  <Point X="26.05549609375" Y="-2.714134033203" />
                  <Point X="26.043861328125" Y="-2.722412597656" />
                  <Point X="25.90205078125" Y="-2.840322509766" />
                  <Point X="25.863876953125" Y="-2.872062988281" />
                  <Point X="25.852654296875" Y="-2.883087646484" />
                  <Point X="25.83218359375" Y="-2.906837402344" />
                  <Point X="25.822935546875" Y="-2.9195625" />
                  <Point X="25.80604296875" Y="-2.947388916016" />
                  <Point X="25.79901953125" Y="-2.961466552734" />
                  <Point X="25.78739453125" Y="-2.990587646484" />
                  <Point X="25.78279296875" Y="-3.005631103516" />
                  <Point X="25.740392578125" Y="-3.200705810547" />
                  <Point X="25.728978515625" Y="-3.253218994141" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520019531" />
                  <Point X="25.772154296875" Y="-3.689478515625" />
                  <Point X="25.833087890625" Y="-4.152311035156" />
                  <Point X="25.655068359375" Y="-3.4879375" />
                  <Point X="25.652607421875" Y="-3.480123779297" />
                  <Point X="25.642146484375" Y="-3.453578613281" />
                  <Point X="25.6267890625" Y="-3.423815429688" />
                  <Point X="25.62041015625" Y="-3.413209716797" />
                  <Point X="25.491408203125" Y="-3.227343261719" />
                  <Point X="25.456681640625" Y="-3.177309326172" />
                  <Point X="25.446669921875" Y="-3.165171386719" />
                  <Point X="25.42478515625" Y="-3.142716064453" />
                  <Point X="25.412912109375" Y="-3.132398681641" />
                  <Point X="25.38665625" Y="-3.113154541016" />
                  <Point X="25.3732421875" Y="-3.104937744141" />
                  <Point X="25.3452421875" Y="-3.090829833984" />
                  <Point X="25.33065625" Y="-3.084938720703" />
                  <Point X="25.13103515625" Y="-3.022983398438" />
                  <Point X="25.077296875" Y="-3.006305419922" />
                  <Point X="25.063376953125" Y="-3.003109375" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766357422" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.73539453125" Y="-3.068260253906" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.6670703125" Y="-3.090829589844" />
                  <Point X="24.6390703125" Y="-3.104937255859" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.587525390625" Y="-3.142717041016" />
                  <Point X="24.565640625" Y="-3.165173339844" />
                  <Point X="24.555630859375" Y="-3.177310302734" />
                  <Point X="24.426630859375" Y="-3.363176513672" />
                  <Point X="24.391904296875" Y="-3.413210693359" />
                  <Point X="24.387529296875" Y="-3.420132568359" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.272283203125" Y="-3.805021484375" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.029417916938" Y="-2.720260434853" />
                  <Point X="29.067711976745" Y="-2.615048370235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.631330583281" Y="-1.066518975591" />
                  <Point X="29.741070087216" Y="-0.765012166509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.928961328222" Y="-2.71850122597" />
                  <Point X="28.988685831156" Y="-2.55440950277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.534856505121" Y="-1.053817908884" />
                  <Point X="29.648956609384" Y="-0.740330448862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.845419738223" Y="-2.670268440061" />
                  <Point X="28.909659685568" Y="-2.493770635305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.438382428881" Y="-1.041116836899" />
                  <Point X="29.556843131553" Y="-0.715648731216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.76187813954" Y="-2.622035678013" />
                  <Point X="28.830633539979" Y="-2.43313176784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.341908352642" Y="-1.028415764914" />
                  <Point X="29.464729653722" Y="-0.690967013569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.678336540857" Y="-2.573802915964" />
                  <Point X="28.751607394391" Y="-2.372492900376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.245434276402" Y="-1.01571469293" />
                  <Point X="29.37261617589" Y="-0.666285295923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.594794942174" Y="-2.525570153916" />
                  <Point X="28.672581248803" Y="-2.311854032911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.148960200163" Y="-1.003013620945" />
                  <Point X="29.280502698059" Y="-0.641603578277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.772792810955" Y="0.710952390725" />
                  <Point X="29.780875682961" Y="0.733159899047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.511253343491" Y="-2.477337391868" />
                  <Point X="28.593555103214" Y="-2.251215165446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.052486123923" Y="-0.99031254896" />
                  <Point X="29.188389220228" Y="-0.61692186063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.660770870305" Y="0.680936056322" />
                  <Point X="29.749877157454" Y="0.925753568197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.427711744808" Y="-2.429104629819" />
                  <Point X="28.514528957626" Y="-2.190576297981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.956012047684" Y="-0.977611476976" />
                  <Point X="29.096275742396" Y="-0.592240142984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.548748993197" Y="0.650919896499" />
                  <Point X="29.709359441197" Y="1.092193475706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.344170146124" Y="-2.380871867771" />
                  <Point X="28.435502812037" Y="-2.129937430516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.859537971444" Y="-0.964910404991" />
                  <Point X="29.004162264565" Y="-0.567558425338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.436727116089" Y="0.620903736676" />
                  <Point X="29.610720556564" Y="1.098946785513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.667370991629" Y="-3.962600844237" />
                  <Point X="27.679328075039" Y="-3.929749027565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.260628547441" Y="-2.332639105722" />
                  <Point X="28.356476666449" Y="-2.069298563051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.763063895205" Y="-0.952209333006" />
                  <Point X="28.912048772673" Y="-0.542876746322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.324705238981" Y="0.590887576852" />
                  <Point X="29.504535538452" Y="1.084967263982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.598799789548" Y="-3.873237255563" />
                  <Point X="27.617321132295" Y="-3.82235028459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.177086948758" Y="-2.284406343674" />
                  <Point X="28.277450551325" Y="-2.008659611886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.666589818965" Y="-0.939508261021" />
                  <Point X="28.819935271583" Y="-0.518195092578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.212683361874" Y="0.560871417029" />
                  <Point X="29.39835052034" Y="1.070987742451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.530228660972" Y="-3.783873464938" />
                  <Point X="27.55531418955" Y="-3.714951541614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.093545350075" Y="-2.236173581625" />
                  <Point X="28.198424482808" Y="-1.948020532669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.570115742726" Y="-0.926807189037" />
                  <Point X="28.727821770493" Y="-0.493513438833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.100661484766" Y="0.530855257206" />
                  <Point X="29.292165502228" Y="1.057008220919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.461657532395" Y="-3.694509674313" />
                  <Point X="27.493307246806" Y="-3.607552798638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.010003751392" Y="-2.187940819577" />
                  <Point X="28.119398414291" Y="-1.887381453452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.473641666486" Y="-0.914106117052" />
                  <Point X="28.638158804429" Y="-0.462098995442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.988639607658" Y="0.500839097382" />
                  <Point X="29.185980484116" Y="1.043028699388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.393086403819" Y="-3.605145883688" />
                  <Point X="27.431300304061" Y="-3.500154055662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.926462152709" Y="-2.139708057528" />
                  <Point X="28.040836216095" Y="-1.825467901002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.3739539093" Y="-0.910234560901" />
                  <Point X="28.554580499394" Y="-0.413967083265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.87661773055" Y="0.470822937559" />
                  <Point X="29.079795466005" Y="1.029049177857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.324515275242" Y="-3.515782093063" />
                  <Point X="27.369293361317" Y="-3.392755312687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.842825563087" Y="-2.091736280939" />
                  <Point X="27.972021243854" Y="-1.736774065341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.264879718704" Y="-0.932152018594" />
                  <Point X="28.476876892557" Y="-0.349694570445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.764595853442" Y="0.440806777736" />
                  <Point X="28.973610447893" Y="1.015069656325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.255944146666" Y="-3.426418302438" />
                  <Point X="27.307286418572" Y="-3.285356569711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.752214879603" Y="-2.062925669756" />
                  <Point X="27.907469447802" Y="-1.636367249363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.154315466312" Y="-0.958163387424" />
                  <Point X="28.407795152218" Y="-0.261733674108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.650587152226" Y="0.40533186354" />
                  <Point X="28.867425429781" Y="1.001090134794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.187373018089" Y="-3.337054511813" />
                  <Point X="27.245279475828" Y="-3.177957826735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.657353487433" Y="-2.045793784708" />
                  <Point X="27.859762495355" Y="-1.489679605946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.015427169506" Y="-1.06199442871" />
                  <Point X="28.359553448498" Y="-0.116515247739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.522057613339" Y="0.329961275728" />
                  <Point X="28.761240404842" Y="0.987110594507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.118801889513" Y="-3.247690721188" />
                  <Point X="27.183272748811" Y="-3.070558491054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.562492129191" Y="-2.028661806439" />
                  <Point X="28.65505536863" Y="0.973131023245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.050230760936" Y="-3.158326930563" />
                  <Point X="27.121266069863" Y="-2.963159023302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.460390076575" Y="-2.031423472466" />
                  <Point X="28.548870332418" Y="0.959151451984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.090808603855" Y="2.448114615496" />
                  <Point X="29.116150015827" Y="2.517739572665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.98165963236" Y="-3.068963139938" />
                  <Point X="27.060205466643" Y="-2.853160233852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.340645768451" Y="-2.082656837131" />
                  <Point X="28.445223739274" Y="0.952146195732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.950535785729" Y="2.340479633147" />
                  <Point X="29.053022605754" Y="2.622059856958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.913088503783" Y="-2.979599349314" />
                  <Point X="27.020856289352" Y="-2.683509791919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.215335792289" Y="-2.149181749051" />
                  <Point X="28.348659120179" Y="0.964598503268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.810262930649" Y="2.232844549269" />
                  <Point X="28.986460514921" Y="2.716943433417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.844517375207" Y="-2.890235558689" />
                  <Point X="28.258437572421" Y="0.99447825607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.66999007557" Y="2.125209465391" />
                  <Point X="28.900215064399" Y="2.757747423593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.769611930165" Y="-2.81827515952" />
                  <Point X="28.174640210932" Y="1.042008315584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.529717220491" Y="2.017574381513" />
                  <Point X="28.772221916788" Y="2.683850558701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.687685573204" Y="-2.765604557312" />
                  <Point X="28.100888288585" Y="1.117137992309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.389444365411" Y="1.909939297635" />
                  <Point X="28.644228769176" Y="2.609953693809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.605759216243" Y="-2.712933955104" />
                  <Point X="28.03795435737" Y="1.221989855393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.249171510332" Y="1.802304213758" />
                  <Point X="28.516235621565" Y="2.536056828918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.5218364944" Y="-2.665748320331" />
                  <Point X="28.000907306384" Y="1.397965337369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.094399848112" Y="1.654833984654" />
                  <Point X="28.388242473954" Y="2.462159964026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.427693879938" Y="-2.646641609759" />
                  <Point X="28.260249326342" Y="2.388263099134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.809974342792" Y="-4.06605067161" />
                  <Point X="25.818608345972" Y="-4.042328942832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.323809558874" Y="-2.654300018104" />
                  <Point X="28.132256178731" Y="2.314366234243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.767106641955" Y="-3.906067293667" />
                  <Point X="25.791753758657" Y="-3.838349897073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.219209273387" Y="-2.663925522532" />
                  <Point X="28.004263031119" Y="2.240469369351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.724238941118" Y="-3.746083915725" />
                  <Point X="25.764899208049" Y="-3.634370750464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.110188280366" Y="-2.685696821088" />
                  <Point X="27.876269840585" Y="2.166572386529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.681371240281" Y="-3.586100537783" />
                  <Point X="25.7380447566" Y="-3.430391331416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.974840323652" Y="-2.779800857912" />
                  <Point X="27.748276620967" Y="2.0926753238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.634149412005" Y="-3.438080026662" />
                  <Point X="25.769813891653" Y="-3.065344932205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.822782744779" Y="-2.919814204308" />
                  <Point X="27.621040254488" Y="2.020857697982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.569042346516" Y="-3.339198800923" />
                  <Point X="27.506828295031" Y="1.984824336357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.502723627989" Y="-3.243646564547" />
                  <Point X="27.402899615171" Y="1.977044053225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.434670619382" Y="-3.152859251005" />
                  <Point X="27.30692866056" Y="1.991127440523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.35446013332" Y="-3.09547433225" />
                  <Point X="27.220555278541" Y="2.031579941799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.264653764319" Y="-3.064453885189" />
                  <Point X="27.14089375424" Y="2.090473120597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.770223187041" Y="3.819541526618" />
                  <Point X="27.803818856825" Y="3.911844870741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.17381799987" Y="-3.036261678874" />
                  <Point X="27.071915355395" Y="2.178717945357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.49668042942" Y="3.345750394813" />
                  <Point X="27.723509110709" Y="3.968957074739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.082982094466" Y="-3.008069859832" />
                  <Point X="27.020114105761" Y="2.314156599702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.223138157161" Y="2.87196059653" />
                  <Point X="27.643199364593" Y="4.026069278738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.985474498487" Y="-2.998208359996" />
                  <Point X="27.560321040799" Y="4.076124373567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.874606626435" Y="-3.025053916985" />
                  <Point X="27.476911112263" Y="4.124718896372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.76063517744" Y="-3.060426481547" />
                  <Point X="27.3935011786" Y="4.173313405091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.644296003711" Y="-3.102304316348" />
                  <Point X="27.310091244937" Y="4.221907913809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.942464520854" Y="-4.752809049744" />
                  <Point X="24.230011992413" Y="-3.962778864616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.472087766974" Y="-3.297681140211" />
                  <Point X="27.225831763481" Y="4.268168309151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.864176697403" Y="-4.69014165888" />
                  <Point X="27.139440373175" Y="4.308571333063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.861279707703" Y="-4.420339654649" />
                  <Point X="27.053048832564" Y="4.348973944018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.825553116273" Y="-4.240736239861" />
                  <Point X="26.966657291953" Y="4.389376554973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.774750117296" Y="-4.102554914375" />
                  <Point X="26.880265751342" Y="4.429779165928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.701265160699" Y="-4.026691755281" />
                  <Point X="26.79170122782" Y="4.464211555402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.624629976721" Y="-3.95948377478" />
                  <Point X="26.702394058165" Y="4.496603541396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.547994758846" Y="-3.89227588741" />
                  <Point X="26.61308686419" Y="4.52899546057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.469199566791" Y="-3.831002480329" />
                  <Point X="26.523779670215" Y="4.561387379744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.37984158978" Y="-3.7987500864" />
                  <Point X="26.433696659671" Y="4.591647760413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.281629490383" Y="-3.790824193793" />
                  <Point X="26.340766332358" Y="4.614085202555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.182888169807" Y="-3.784352324429" />
                  <Point X="26.247835655438" Y="4.636521684157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.084146820097" Y="-3.777880535109" />
                  <Point X="26.154904921931" Y="4.658958010289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.980443890316" Y="-3.785040574997" />
                  <Point X="26.061974188424" Y="4.681394336421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.85644486878" Y="-3.847963668685" />
                  <Point X="25.969043454917" Y="4.703830662553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.722860697182" Y="-3.937221745733" />
                  <Point X="25.87611272141" Y="4.726266988685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.578556989194" Y="-4.055931506957" />
                  <Point X="25.78103969482" Y="4.742817412943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.449369286722" Y="-4.133110384356" />
                  <Point X="25.68365485946" Y="4.753016194812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.366865409505" Y="-4.082026506009" />
                  <Point X="25.586270047699" Y="4.763215041515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.284361603124" Y="-4.030942433045" />
                  <Point X="25.488885235937" Y="4.773413888217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.204024891276" Y="-3.973904316785" />
                  <Point X="22.660242316471" Y="-2.720457242702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.680700254512" Y="-2.664249519885" />
                  <Point X="25.15667099068" Y="4.138424168966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.339645454363" Y="4.641142376274" />
                  <Point X="25.391500424176" Y="4.78361273492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.125057994135" Y="-3.913102665549" />
                  <Point X="22.38604838419" Y="-3.19603746218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.649281742681" Y="-2.472809753677" />
                  <Point X="25.037100032375" Y="4.087667079018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.046091096994" Y="-3.852301014312" />
                  <Point X="22.112506401388" Y="-3.669826465186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.576071920996" Y="-2.396190667623" />
                  <Point X="24.939029801922" Y="4.095982753342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.489908692919" Y="-2.355160773138" />
                  <Point X="24.85387394762" Y="4.139780384529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.389413044842" Y="-2.353508878967" />
                  <Point X="24.786372439035" Y="4.232082931927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.269952771547" Y="-2.403961864353" />
                  <Point X="24.741145543876" Y="4.385584476743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.141959718626" Y="-2.477858469084" />
                  <Point X="24.698278076539" Y="4.545568496219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.013966640116" Y="-2.55175514412" />
                  <Point X="24.655410609201" Y="4.705552515694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.885973460601" Y="-2.625652096667" />
                  <Point X="24.580008060223" Y="4.776147133024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.757980281085" Y="-2.699549049215" />
                  <Point X="24.474413063621" Y="4.763788682267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.629987101569" Y="-2.773446001763" />
                  <Point X="24.368818067018" Y="4.751430231511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.501993922054" Y="-2.847342954311" />
                  <Point X="21.973072408165" Y="-1.553065450931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.050529320568" Y="-1.340254333122" />
                  <Point X="24.263223070416" Y="4.739071780754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.374000742538" Y="-2.921239906859" />
                  <Point X="21.830500109843" Y="-1.667018203194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.998693318699" Y="-1.204911159757" />
                  <Point X="24.157628073814" Y="4.726713329998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.246007563023" Y="-2.995136859407" />
                  <Point X="21.690227134325" Y="-1.774653617975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.921624322391" Y="-1.138895068836" />
                  <Point X="24.050921602096" Y="4.711301126457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.159410636423" Y="-2.955298541819" />
                  <Point X="21.549954158806" Y="-1.882289032755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.832750657765" Y="-1.105312037565" />
                  <Point X="23.938336022732" Y="4.679736207415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.091017649964" Y="-2.865445309747" />
                  <Point X="21.409681183288" Y="-1.989924447536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.732400510831" Y="-1.10326038229" />
                  <Point X="23.825750388307" Y="4.648171137094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.022625283197" Y="-2.775590375088" />
                  <Point X="21.26940820777" Y="-2.097559862316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.626215503031" Y="-1.117239875489" />
                  <Point X="23.713164746103" Y="4.616606045399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.957657871196" Y="-2.676325454546" />
                  <Point X="21.129135232252" Y="-2.205195277097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.520030495231" Y="-1.131219368689" />
                  <Point X="23.440539788573" Y="4.145336548619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.537697940344" Y="4.412276376727" />
                  <Point X="23.600579103899" Y="4.585040953705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.89487716329" Y="-2.571052613879" />
                  <Point X="20.988862256734" Y="-2.312830691877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.413845487431" Y="-1.145198861888" />
                  <Point X="23.311472284197" Y="4.068487912777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.832096455384" Y="-2.465779773211" />
                  <Point X="20.848589586888" Y="-2.420465266829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.307660466982" Y="-1.159178389841" />
                  <Point X="21.665025299373" Y="-0.17732658234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.700193355523" Y="-0.080703142183" />
                  <Point X="23.196822618667" Y="4.031251963601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.201475446113" Y="-1.173157918948" />
                  <Point X="21.518459954925" Y="-0.302250138672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.668167861634" Y="0.109068954526" />
                  <Point X="23.098732582299" Y="4.039513221622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.095290425243" Y="-1.187137448056" />
                  <Point X="21.405327175523" Y="-0.335318477461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.59758162371" Y="0.192896277722" />
                  <Point X="23.011881541464" Y="4.078653366087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.989105404374" Y="-1.201116977164" />
                  <Point X="21.293305285027" Y="-0.365334674068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.514157800949" Y="0.241452626457" />
                  <Point X="22.928322302695" Y="4.126837662397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.882920383504" Y="-1.215096506272" />
                  <Point X="21.181283394532" Y="-0.395350870674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.422755518183" Y="0.268088336486" />
                  <Point X="22.855792959804" Y="4.205326348572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.776735362635" Y="-1.229076035379" />
                  <Point X="21.069261504036" Y="-0.425367067281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.330642077641" Y="0.292770156583" />
                  <Point X="22.770046885049" Y="4.247502362391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.670550341765" Y="-1.243055564487" />
                  <Point X="20.95723961354" Y="-0.455383263888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.238528637098" Y="0.317451976679" />
                  <Point X="22.150147030765" Y="2.822102928439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.247300133256" Y="3.089028883763" />
                  <Point X="22.643325088675" Y="4.177098506316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.564365320896" Y="-1.257035093595" />
                  <Point X="20.845217723044" Y="-0.485399460494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.146415184867" Y="0.342133764662" />
                  <Point X="21.49880884935" Y="1.310327400588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.578675483435" Y="1.529759174306" />
                  <Point X="22.016430312838" Y="2.732480683345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.203082723777" Y="3.245303967688" />
                  <Point X="22.516603292301" Y="4.106694650241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.458180300027" Y="-1.271014622703" />
                  <Point X="20.733195832548" Y="-0.515415657101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.054301709132" Y="0.366815488068" />
                  <Point X="21.367748149325" Y="1.228002504706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.530457163065" Y="1.675041845898" />
                  <Point X="21.91269661083" Y="2.725236097458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.14107575151" Y="3.35270262955" />
                  <Point X="22.389881495927" Y="4.036290794166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.351995279157" Y="-1.28499415181" />
                  <Point X="20.621173942052" Y="-0.545431853708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.962188233397" Y="0.391497211475" />
                  <Point X="21.258109192939" Y="1.20453336576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.461953248598" Y="1.764590305773" />
                  <Point X="21.815664025663" Y="2.736402678776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.079068938514" Y="3.460101729009" />
                  <Point X="22.253729433094" Y="3.939977493937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.302460856813" Y="-1.14332744067" />
                  <Point X="20.509152051556" Y="-0.575448050314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.870074757663" Y="0.416178934882" />
                  <Point X="21.160353470247" Y="1.213713143056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.383088307026" Y="1.825672077634" />
                  <Point X="21.727816965648" Y="2.772806283034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.017062151137" Y="3.567500898851" />
                  <Point X="22.113501622845" Y="3.832466169714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.26077182927" Y="-0.980105684468" />
                  <Point X="20.39713016106" Y="-0.605464246921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.777961281928" Y="0.440860658289" />
                  <Point X="21.063879385252" Y="1.226414190986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.304062327259" Y="1.886311400689" />
                  <Point X="21.644275344934" Y="2.821038984554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.955055363759" Y="3.674900068693" />
                  <Point X="21.973273903936" Y="3.724955096444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.231767895402" Y="-0.782031919831" />
                  <Point X="20.285108282915" Y="-0.635480409593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.685847806193" Y="0.465542381696" />
                  <Point X="20.967405300258" Y="1.239115238916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.225036243433" Y="1.946950437845" />
                  <Point X="21.56073372422" Y="2.869271686074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.593734330458" Y="0.490224105103" />
                  <Point X="20.870931215263" Y="1.251816286846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.146010099676" Y="2.00758931034" />
                  <Point X="21.477192103507" Y="2.917504387594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.501620854724" Y="0.51490582851" />
                  <Point X="20.774457130268" Y="1.264517334776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.066983955919" Y="2.068228182835" />
                  <Point X="21.393650482793" Y="2.965737089114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.409507378989" Y="0.539587551916" />
                  <Point X="20.677983045273" Y="1.277218382706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.987957812161" Y="2.12886705533" />
                  <Point X="21.310108862079" Y="3.013969790634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.317393903254" Y="0.564269275323" />
                  <Point X="20.581508960279" Y="1.289919430636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.908931668404" Y="2.189505927826" />
                  <Point X="21.166570748957" Y="2.897363484015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.22528042752" Y="0.58895099873" />
                  <Point X="20.485034875284" Y="1.302620478566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.829905524646" Y="2.250144800321" />
                  <Point X="20.967860460912" Y="2.629172872614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.298786129707" Y="1.068667673706" />
                  <Point X="20.388560790289" Y="1.315321526496" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.78716015625" Y="-4.715017089844" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.335318359375" Y="-3.335677734375" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399658203" />
                  <Point X="25.07471484375" Y="-3.204444335938" />
                  <Point X="25.0209765625" Y="-3.187766357422" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.79171484375" Y="-3.249721679688" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.582720703125" Y="-3.471510009766" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.455810546875" Y="-3.854197265625" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.96185546875" Y="-4.950119140625" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.668234375" Y="-4.878497070312" />
                  <Point X="23.648412109375" Y="-4.873396484375" />
                  <Point X="23.652857421875" Y="-4.839624023438" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.642626953125" Y="-4.295005371094" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.429931640625" Y="-4.041451660156" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.106833984375" Y="-3.969775146484" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791503906" />
                  <Point X="22.806869140625" Y="-4.109600097656" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.69256640625" Y="-4.219458007813" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.23519140625" Y="-4.223970703125" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.823578125" Y="-3.920768554688" />
                  <Point X="21.771419921875" Y="-3.880608154297" />
                  <Point X="21.90742578125" Y="-3.645039306641" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597592529297" />
                  <Point X="22.48601953125" Y="-2.568762939453" />
                  <Point X="22.46868359375" Y="-2.551425048828" />
                  <Point X="22.4398515625" Y="-2.537199707031" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="22.134837890625" Y="-2.70136328125" />
                  <Point X="21.15704296875" Y="-3.265893554688" />
                  <Point X="20.9102109375" Y="-2.941609375" />
                  <Point X="20.838296875" Y="-2.847127685547" />
                  <Point X="20.6084609375" Y="-2.461727050781" />
                  <Point X="20.56898046875" Y="-2.395526367188" />
                  <Point X="20.80995703125" Y="-2.210619873047" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396013305664" />
                  <Point X="21.86024609375" Y="-1.372579956055" />
                  <Point X="21.8618828125" Y="-1.366270874023" />
                  <Point X="21.85967578125" Y="-1.334597412109" />
                  <Point X="21.838841796875" Y="-1.310638793945" />
                  <Point X="21.8179765625" Y="-1.298358154297" />
                  <Point X="21.812359375" Y="-1.295052246094" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.43523046875" Y="-1.334023071289" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.100734375" Y="-1.121305053711" />
                  <Point X="20.072609375" Y="-1.011189025879" />
                  <Point X="20.011798828125" Y="-0.586019287109" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="20.2734765625" Y="-0.441894622803" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.458103515625" Y="-0.121425811768" />
                  <Point X="21.479970703125" Y="-0.106249320984" />
                  <Point X="21.485857421875" Y="-0.102163780212" />
                  <Point X="21.5051015625" Y="-0.075908058167" />
                  <Point X="21.512390625" Y="-0.05242313385" />
                  <Point X="21.51435546875" Y="-0.046092662811" />
                  <Point X="21.5143515625" Y="-0.01645889473" />
                  <Point X="21.507064453125" Y="0.007017140865" />
                  <Point X="21.50510546875" Y="0.013339464188" />
                  <Point X="21.485857421875" Y="0.03960370636" />
                  <Point X="21.463990234375" Y="0.054780345917" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="21.127833984375" Y="0.150410064697" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.064228515625" Y="0.873923217773" />
                  <Point X="20.08235546875" Y="0.996414794922" />
                  <Point X="20.20476171875" Y="1.448136230469" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.40872265625" Y="1.504306762695" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866333008" />
                  <Point X="21.316693359375" Y="1.411126098633" />
                  <Point X="21.32972265625" Y="1.415233764648" />
                  <Point X="21.348466796875" Y="1.426057006836" />
                  <Point X="21.3608828125" Y="1.44378918457" />
                  <Point X="21.380302734375" Y="1.490672851562" />
                  <Point X="21.385529296875" Y="1.503293579102" />
                  <Point X="21.38928515625" Y="1.524608520508" />
                  <Point X="21.38368359375" Y="1.545514038086" />
                  <Point X="21.360251953125" Y="1.590526855469" />
                  <Point X="21.353943359375" Y="1.602643920898" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.159517578125" Y="1.757735961914" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.7695546875" Y="2.66633984375" />
                  <Point X="20.839986328125" Y="2.787006591797" />
                  <Point X="21.164236328125" Y="3.203785644531" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.32616015625" Y="3.224096191406" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.928890625" Y="2.914537597656" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927405029297" />
                  <Point X="22.03459375" Y="2.975249267578" />
                  <Point X="22.04747265625" Y="2.988128417969" />
                  <Point X="22.0591015625" Y="3.006383056641" />
                  <Point X="22.061927734375" Y="3.027842041016" />
                  <Point X="22.056029296875" Y="3.095246582031" />
                  <Point X="22.054443359375" Y="3.113386474609" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.96794140625" Y="3.272582763672" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="22.12445703125" Y="4.080282714844" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.757810546875" Y="4.458057617188" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.87461328125" Y="4.49291015625" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.123775390625" Y="4.234606933594" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.218491699219" />
                  <Point X="23.186193359375" Y="4.22225" />
                  <Point X="23.26433203125" Y="4.254616699219" />
                  <Point X="23.2853671875" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275743164062" />
                  <Point X="23.31391796875" Y="4.294485839844" />
                  <Point X="23.339349609375" Y="4.3751484375" />
                  <Point X="23.346197265625" Y="4.396862304687" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.33898828125" Y="4.487503417969" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.87310546875" Y="4.858773925781" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.651013671875" Y="4.97575390625" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.8056015625" Y="4.879137207031" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.09544140625" Y="4.463868652344" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.721548828125" Y="4.940086914062" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.37241015625" Y="4.801904296875" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.84251171875" Y="4.64789453125" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.25341015625" Y="4.465021484375" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.650125" Y="4.243697265625" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="28.0262421875" Y="3.986815673828" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.908732421875" Y="3.679448974609" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491517333984" />
                  <Point X="27.2079375" Y="2.428259521484" />
                  <Point X="27.202044921875" Y="2.392326416016" />
                  <Point X="27.208640625" Y="2.337626464844" />
                  <Point X="27.210416015625" Y="2.322901611328" />
                  <Point X="27.218681640625" Y="2.300812011719" />
                  <Point X="27.252529296875" Y="2.250930908203" />
                  <Point X="27.27494140625" Y="2.224203369141" />
                  <Point X="27.324822265625" Y="2.190356933594" />
                  <Point X="27.33825" Y="2.181245849609" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.415037109375" Y="2.166384277344" />
                  <Point X="27.4486640625" Y="2.165946044922" />
                  <Point X="27.511921875" Y="2.182862060547" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="27.85756640625" Y="2.375167236328" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.153716796875" Y="2.809806884766" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.3663359375" Y="2.471293212891" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="29.182423828125" Y="2.278924560547" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.279373046875" Y="1.583833740234" />
                  <Point X="28.23384765625" Y="1.524440673828" />
                  <Point X="28.221591796875" Y="1.508452392578" />
                  <Point X="28.21312109375" Y="1.491503417969" />
                  <Point X="28.196162109375" Y="1.430862792969" />
                  <Point X="28.191595703125" Y="1.41453894043" />
                  <Point X="28.190779296875" Y="1.390964111328" />
                  <Point X="28.204701171875" Y="1.323493896484" />
                  <Point X="28.215646484375" Y="1.287957885742" />
                  <Point X="28.25351171875" Y="1.230405273438" />
                  <Point X="28.263703125" Y="1.214912475586" />
                  <Point X="28.28094921875" Y="1.198819580078" />
                  <Point X="28.3358203125" Y="1.167931762695" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.442755859375" Y="1.143814575195" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.77648828125" Y="1.180757568359" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.918197265625" Y="1.037614379883" />
                  <Point X="29.93919140625" Y="0.951369750977" />
                  <Point X="29.9907890625" Y="0.619965759277" />
                  <Point X="29.997859375" Y="0.574556274414" />
                  <Point X="29.765490234375" Y="0.51229296875" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819351196" />
                  <Point X="28.65619921875" Y="0.190688308716" />
                  <Point X="28.636578125" Y="0.179346832275" />
                  <Point X="28.622267578125" Y="0.166927749634" />
                  <Point X="28.57853515625" Y="0.111201332092" />
                  <Point X="28.56676171875" Y="0.096200080872" />
                  <Point X="28.556986328125" Y="0.074734199524" />
                  <Point X="28.542408203125" Y="-0.001385390401" />
                  <Point X="28.538484375" Y="-0.021876363754" />
                  <Point X="28.538484375" Y="-0.04068371582" />
                  <Point X="28.5530625" Y="-0.11680329895" />
                  <Point X="28.556986328125" Y="-0.137294281006" />
                  <Point X="28.56676171875" Y="-0.158760162354" />
                  <Point X="28.610494140625" Y="-0.214486587524" />
                  <Point X="28.636578125" Y="-0.241906906128" />
                  <Point X="28.709466796875" Y="-0.28403793335" />
                  <Point X="28.729087890625" Y="-0.295379425049" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.01691015625" Y="-0.374271636963" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.960154296875" Y="-0.88865625" />
                  <Point X="29.948431640625" Y="-0.966412353516" />
                  <Point X="29.882326171875" Y="-1.256095092773" />
                  <Point X="29.874548828125" Y="-1.290178710938" />
                  <Point X="29.59990625" Y="-1.254021484375" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.251783203125" Y="-1.129434814453" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.185447265625" Y="-1.154696533203" />
                  <Point X="28.09898046875" Y="-1.258690185547" />
                  <Point X="28.075703125" Y="-1.286684570312" />
                  <Point X="28.064359375" Y="-1.314070800781" />
                  <Point X="28.051966796875" Y="-1.448747192383" />
                  <Point X="28.048630859375" Y="-1.485001220703" />
                  <Point X="28.05636328125" Y="-1.516622680664" />
                  <Point X="28.13553125" Y="-1.639764282227" />
                  <Point X="28.15684375" Y="-1.672913330078" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.4243515625" Y="-1.881891601562" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.237171875" Y="-2.748677978516" />
                  <Point X="29.20413671875" Y="-2.802133544922" />
                  <Point X="29.06741796875" Y="-2.996394775391" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.811462890625" Y="-2.870056640625" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.567083984375" Y="-2.222564453125" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219245117188" />
                  <Point X="27.347634765625" Y="-2.293685302734" />
                  <Point X="27.309560546875" Y="-2.313724121094" />
                  <Point X="27.2886015625" Y="-2.33468359375" />
                  <Point X="27.21416015625" Y="-2.476126220703" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.1891640625" Y="-2.546374511719" />
                  <Point X="27.2199140625" Y="-2.716632568359" />
                  <Point X="27.22819140625" Y="-2.76246484375" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.39852734375" Y="-3.063387939453" />
                  <Point X="27.98667578125" Y="-4.082088623047" />
                  <Point X="27.8745078125" Y="-4.162207519531" />
                  <Point X="27.835294921875" Y="-4.190215332031" />
                  <Point X="27.682443359375" Y="-4.289153808594" />
                  <Point X="27.67977734375" Y="-4.290879882812" />
                  <Point X="27.49053515625" Y="-4.044255371094" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467529297" />
                  <Point X="26.50262890625" Y="-2.872510253906" />
                  <Point X="26.45742578125" Y="-2.843448730469" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.24215625" Y="-2.852616699219" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509277344" />
                  <Point X="26.0235234375" Y="-2.986419189453" />
                  <Point X="25.985349609375" Y="-3.018159667969" />
                  <Point X="25.96845703125" Y="-3.045986083984" />
                  <Point X="25.926056640625" Y="-3.241060791016" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.960529296875" Y="-3.664678710938" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.0314375" Y="-4.9551171875" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#196" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.152257696655" Y="4.92335796233" Z="2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2" />
                  <Point X="-0.360954918021" Y="5.056694984639" Z="2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2" />
                  <Point X="-1.146550008965" Y="4.93819746967" Z="2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2" />
                  <Point X="-1.716740053323" Y="4.512257635698" Z="2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2" />
                  <Point X="-1.714492271444" Y="4.421466676115" Z="2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2" />
                  <Point X="-1.760959773676" Y="4.332091198772" Z="2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2" />
                  <Point X="-1.859294244982" Y="4.310237606989" Z="2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2" />
                  <Point X="-2.091875459526" Y="4.554627941542" Z="2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2" />
                  <Point X="-2.272629071537" Y="4.533045028235" Z="2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2" />
                  <Point X="-2.912122563418" Y="4.151242672369" Z="2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2" />
                  <Point X="-3.081516452808" Y="3.278862704959" Z="2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2" />
                  <Point X="-2.999937199237" Y="3.122168027714" Z="2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2" />
                  <Point X="-3.00691937952" Y="3.041884188367" Z="2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2" />
                  <Point X="-3.072908437892" Y="2.995627499936" Z="2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2" />
                  <Point X="-3.654996643338" Y="3.298677495938" Z="2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2" />
                  <Point X="-3.881382546032" Y="3.26576831667" Z="2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2" />
                  <Point X="-4.279784398772" Y="2.722824848339" Z="2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2" />
                  <Point X="-3.877077523697" Y="1.749348316568" Z="2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2" />
                  <Point X="-3.690254513767" Y="1.598716994342" Z="2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2" />
                  <Point X="-3.672050123662" Y="1.541083619616" Z="2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2" />
                  <Point X="-3.704498299413" Y="1.49009227837" Z="2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2" />
                  <Point X="-4.590908060061" Y="1.585158939563" Z="2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2" />
                  <Point X="-4.849654274755" Y="1.492493568424" Z="2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2" />
                  <Point X="-4.991388594706" Y="0.912522549103" Z="2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2" />
                  <Point X="-3.89126521743" Y="0.133393845103" Z="2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2" />
                  <Point X="-3.570674394677" Y="0.044983575252" Z="2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2" />
                  <Point X="-3.546845712033" Y="0.023484944033" Z="2" />
                  <Point X="-3.539556741714" Y="0" Z="2" />
                  <Point X="-3.541518889102" Y="-0.006322007027" Z="2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2" />
                  <Point X="-3.554694200447" Y="-0.033892407882" Z="2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2" />
                  <Point X="-4.745622571343" Y="-0.362318211653" Z="2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2" />
                  <Point X="-5.04385436002" Y="-0.561818357019" Z="2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2" />
                  <Point X="-4.953867582857" Y="-1.102398802452" Z="2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2" />
                  <Point X="-3.564400900169" Y="-1.352315272426" Z="2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2" />
                  <Point X="-3.213541974219" Y="-1.310169178897" Z="2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2" />
                  <Point X="-3.19431041723" Y="-1.328759172675" Z="2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2" />
                  <Point X="-4.226638217834" Y="-2.139672148681" Z="2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2" />
                  <Point X="-4.440640018341" Y="-2.456057214045" Z="2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2" />
                  <Point X="-4.135732125995" Y="-2.940612228811" Z="2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2" />
                  <Point X="-2.846319062376" Y="-2.713384468229" Z="2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2" />
                  <Point X="-2.569159903276" Y="-2.559170565855" Z="2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2" />
                  <Point X="-3.142032544939" Y="-3.588759320116" Z="2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2" />
                  <Point X="-3.213082234576" Y="-3.929105723616" Z="2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2" />
                  <Point X="-2.797288428268" Y="-4.235201283272" Z="2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2" />
                  <Point X="-2.273922250596" Y="-4.218615973508" Z="2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2" />
                  <Point X="-2.171508038" Y="-4.119893239683" Z="2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2" />
                  <Point X="-1.902592629708" Y="-3.988388176886" Z="2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2" />
                  <Point X="-1.609192588712" Y="-4.047763755623" Z="2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2" />
                  <Point X="-1.412568204343" Y="-4.27348029809" Z="2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2" />
                  <Point X="-1.402871555745" Y="-4.801817748132" Z="2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2" />
                  <Point X="-1.350382140972" Y="-4.895639765763" Z="2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2" />
                  <Point X="-1.053828783803" Y="-4.96792332626" Z="2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2" />
                  <Point X="-0.502049438282" Y="-3.835858058771" Z="2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2" />
                  <Point X="-0.382360394526" Y="-3.468738977926" Z="2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2" />
                  <Point X="-0.199622020615" Y="-3.266194676897" Z="2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2" />
                  <Point X="0.053737058745" Y="-3.220917368391" Z="2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2" />
                  <Point X="0.288085464907" Y="-3.332906777842" Z="2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2" />
                  <Point X="0.732705422119" Y="-4.696677971044" Z="2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2" />
                  <Point X="0.855918348982" Y="-5.006814273076" Z="2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2" />
                  <Point X="1.035984855772" Y="-4.972677345318" Z="2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2" />
                  <Point X="1.003945330326" Y="-3.626872349998" Z="2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2" />
                  <Point X="0.968759697009" Y="-3.22040074011" Z="2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2" />
                  <Point X="1.049334280704" Y="-2.993585364317" Z="2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2" />
                  <Point X="1.240581071497" Y="-2.871126172647" Z="2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2" />
                  <Point X="1.469433599986" Y="-2.883288033955" Z="2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2" />
                  <Point X="2.444710959394" Y="-4.043413263621" Z="2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2" />
                  <Point X="2.703453988291" Y="-4.2998484609" Z="2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2" />
                  <Point X="2.897410875619" Y="-4.171614867574" Z="2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2" />
                  <Point X="2.435671643947" Y="-3.007107840019" Z="2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2" />
                  <Point X="2.262959589746" Y="-2.676466182504" Z="2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2" />
                  <Point X="2.252249976792" Y="-2.46813270172" Z="2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2" />
                  <Point X="2.364765676652" Y="-2.306651333175" Z="2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2" />
                  <Point X="2.552040621009" Y="-2.240488419745" Z="2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2" />
                  <Point X="3.780305757088" Y="-2.882077856328" Z="2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2" />
                  <Point X="4.102148901373" Y="-2.993892495468" Z="2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2" />
                  <Point X="4.27354904176" Y="-2.7436828713" Z="2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2" />
                  <Point X="3.448632289793" Y="-1.810943947933" Z="2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2" />
                  <Point X="3.171431108517" Y="-1.581444055339" Z="2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2" />
                  <Point X="3.09559859193" Y="-1.422048459567" Z="2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2" />
                  <Point X="3.131268000525" Y="-1.259377775801" Z="2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2" />
                  <Point X="3.256245035487" Y="-1.147013982373" Z="2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2" />
                  <Point X="4.587224503372" Y="-1.272313658901" Z="2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2" />
                  <Point X="4.924914559203" Y="-1.235939280925" Z="2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2" />
                  <Point X="5.003438223241" Y="-0.864830415777" Z="2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2" />
                  <Point X="4.023693875799" Y="-0.294695258951" Z="2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2" />
                  <Point X="3.728331478628" Y="-0.209469219419" Z="2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2" />
                  <Point X="3.643669982071" Y="-0.152336979471" Z="2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2" />
                  <Point X="3.596012479502" Y="-0.076119590834" Z="2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2" />
                  <Point X="3.585358970922" Y="0.020490940354" Z="2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2" />
                  <Point X="3.611709456329" Y="0.111611759104" Z="2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2" />
                  <Point X="3.675063935724" Y="0.178679624823" Z="2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2" />
                  <Point X="4.772273941507" Y="0.495276673817" Z="2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2" />
                  <Point X="5.034037358562" Y="0.658938160744" Z="2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2" />
                  <Point X="4.960621499661" Y="1.080720728522" Z="2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2" />
                  <Point X="3.763805710107" Y="1.261609933866" Z="2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2" />
                  <Point X="3.443150058918" Y="1.224663541027" Z="2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2" />
                  <Point X="3.354189647068" Y="1.242783357099" Z="2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2" />
                  <Point X="3.289125620607" Y="1.289163838708" Z="2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2" />
                  <Point X="3.247513436957" Y="1.364878985198" Z="2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2" />
                  <Point X="3.238157229558" Y="1.448673252837" Z="2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2" />
                  <Point X="3.267371493118" Y="1.525301977539" Z="2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2" />
                  <Point X="4.20670452826" Y="2.27053682632" Z="2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2" />
                  <Point X="4.40295618179" Y="2.528459588407" Z="2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2" />
                  <Point X="4.188145120816" Y="2.870290050633" Z="2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2" />
                  <Point X="2.826410037847" Y="2.449748660788" Z="2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2" />
                  <Point X="2.492849330852" Y="2.262445101232" Z="2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2" />
                  <Point X="2.414866751458" Y="2.247304725525" Z="2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2" />
                  <Point X="2.346739109923" Y="2.263011754948" Z="2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2" />
                  <Point X="2.287746845287" Y="2.310285750459" Z="2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2" />
                  <Point X="2.252124977233" Y="2.374891692374" Z="2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2" />
                  <Point X="2.250082841426" Y="2.44662024503" Z="2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2" />
                  <Point X="2.945876692072" Y="3.685729044108" Z="2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2" />
                  <Point X="3.049062373163" Y="4.058842833636" Z="2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2" />
                  <Point X="2.66913885132" Y="4.318179743037" Z="2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2" />
                  <Point X="2.268435126136" Y="4.541593597698" Z="2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2" />
                  <Point X="1.853402931596" Y="4.726178002601" Z="2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2" />
                  <Point X="1.377987269856" Y="4.881787615144" Z="2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2" />
                  <Point X="0.720598203951" Y="5.021095021613" Z="2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2" />
                  <Point X="0.040986864898" Y="4.50808954362" Z="2" />
                  <Point X="0" Y="4.355124473572" Z="2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>