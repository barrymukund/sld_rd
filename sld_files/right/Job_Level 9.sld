<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#133" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="806" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.626587890625" Y="-3.748703369141" />
                  <Point X="25.5633046875" Y="-3.512524414063" />
                  <Point X="25.55772265625" Y="-3.497140625" />
                  <Point X="25.542365234375" Y="-3.467377929688" />
                  <Point X="25.5165" Y="-3.430110107422" />
                  <Point X="25.37863671875" Y="-3.231477539062" />
                  <Point X="25.356751953125" Y="-3.209020263672" />
                  <Point X="25.33049609375" Y="-3.189776611328" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.262470703125" Y="-3.163246826172" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.923150390625" Y="-3.109458251953" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.18977734375" />
                  <Point X="24.655560546875" Y="-3.209021484375" />
                  <Point X="24.63367578125" Y="-3.231477783203" />
                  <Point X="24.607810546875" Y="-3.268745361328" />
                  <Point X="24.46994921875" Y="-3.467378173828" />
                  <Point X="24.4618125" Y="-3.481571289062" />
                  <Point X="24.449009765625" Y="-3.512524169922" />
                  <Point X="24.111712890625" Y="-4.771334960938" />
                  <Point X="24.083416015625" Y="-4.87694140625" />
                  <Point X="23.9206640625" Y="-4.845351074219" />
                  <Point X="23.878171875" Y="-4.83441796875" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.777419921875" Y="-4.621303222656" />
                  <Point X="23.7850390625" Y="-4.563439453125" />
                  <Point X="23.78580078125" Y="-4.547930175781" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.7739296875" Y="-4.468150878906" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131205078125" />
                  <Point X="23.639505859375" Y="-4.098887695313" />
                  <Point X="23.443095703125" Y="-3.926640380859" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.308064453125" Y="-3.887760498047" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801513672" />
                  <Point X="22.916587890625" Y="-3.922032226562" />
                  <Point X="22.699376953125" Y="-4.067169433594" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.5198515625" Y="-4.288489746094" />
                  <Point X="22.198291015625" Y="-4.08938671875" />
                  <Point X="22.13945703125" Y="-4.044086914063" />
                  <Point X="21.89527734375" Y="-3.856077392578" />
                  <Point X="22.4586328125" Y="-2.880319335938" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.588720703125" Y="-2.646978271484" />
                  <Point X="22.59413671875" Y="-2.6244453125" />
                  <Point X="22.5939375" Y="-2.601271484375" />
                  <Point X="22.58979296875" Y="-2.569790283203" />
                  <Point X="22.5834296875" Y="-2.545973144531" />
                  <Point X="22.571154296875" Y="-2.524594726562" />
                  <Point X="22.558830078125" Y="-2.508428710938" />
                  <Point X="22.55045703125" Y="-2.498850097656" />
                  <Point X="22.535849609375" Y="-2.4842421875" />
                  <Point X="22.510693359375" Y="-2.466214355469" />
                  <Point X="22.481865234375" Y="-2.451997070313" />
                  <Point X="22.452248046875" Y="-2.443011962891" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.2751171875" Y="-3.088026367188" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="20.917142578125" Y="-2.793861328125" />
                  <Point X="20.874962890625" Y="-2.723135253906" />
                  <Point X="20.693857421875" Y="-2.419449707031" />
                  <Point X="21.6882734375" Y="-1.656408203125" />
                  <Point X="21.894044921875" Y="-1.498513305664" />
                  <Point X="21.91692578125" Y="-1.475893066406" />
                  <Point X="21.935625" Y="-1.446474487305" />
                  <Point X="21.9435625" Y="-1.426698608398" />
                  <Point X="21.947365234375" Y="-1.415127563477" />
                  <Point X="21.9538515625" Y="-1.390079711914" />
                  <Point X="21.95796484375" Y="-1.358597290039" />
                  <Point X="21.957263671875" Y="-1.335729003906" />
                  <Point X="21.9511171875" Y="-1.31369128418" />
                  <Point X="21.939111328125" Y="-1.284709838867" />
                  <Point X="21.926806640625" Y="-1.26335925293" />
                  <Point X="21.90942578125" Y="-1.245891601562" />
                  <Point X="21.892740234375" Y="-1.23300012207" />
                  <Point X="21.88284375" Y="-1.226304443359" />
                  <Point X="21.860544921875" Y="-1.213180297852" />
                  <Point X="21.831283203125" Y="-1.201956665039" />
                  <Point X="21.799396484375" Y="-1.195474975586" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="20.397478515625" Y="-1.374825927734" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.165921875" Y="-0.992647888184" />
                  <Point X="20.154765625" Y="-0.914634887695" />
                  <Point X="20.107578125" Y="-0.584698242188" />
                  <Point X="21.23271875" Y="-0.283217193604" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.4859921875" Y="-0.213123596191" />
                  <Point X="21.505943359375" Y="-0.202860733032" />
                  <Point X="21.516654296875" Y="-0.19642741394" />
                  <Point X="21.5400234375" Y="-0.180208297729" />
                  <Point X="21.5639765625" Y="-0.158684570312" />
                  <Point X="21.584083984375" Y="-0.13012991333" />
                  <Point X="21.592970703125" Y="-0.110682685852" />
                  <Point X="21.597294921875" Y="-0.099355934143" />
                  <Point X="21.605083984375" Y="-0.074257850647" />
                  <Point X="21.609267578125" Y="-0.042062957764" />
                  <Point X="21.607806640625" Y="-0.007714312077" />
                  <Point X="21.603623046875" Y="0.016406698227" />
                  <Point X="21.595833984375" Y="0.04150478363" />
                  <Point X="21.582517578125" Y="0.070831138611" />
                  <Point X="21.561173828125" Y="0.098748733521" />
                  <Point X="21.54496875" Y="0.1133099823" />
                  <Point X="21.535638671875" Y="0.120691551208" />
                  <Point X="21.51226953125" Y="0.136910507202" />
                  <Point X="21.498076171875" Y="0.145046325684" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="20.217765625" Y="0.492613372803" />
                  <Point X="20.10818359375" Y="0.521975769043" />
                  <Point X="20.17551171875" Y="0.976969238281" />
                  <Point X="20.1979765625" Y="1.059873779297" />
                  <Point X="20.29644921875" Y="1.423268066406" />
                  <Point X="21.066306640625" Y="1.32191418457" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228027344" />
                  <Point X="21.2968671875" Y="1.305264648438" />
                  <Point X="21.3065703125" Y="1.308324462891" />
                  <Point X="21.35829296875" Y="1.324632202148" />
                  <Point X="21.3772265625" Y="1.332963500977" />
                  <Point X="21.39597265625" Y="1.343787475586" />
                  <Point X="21.412654296875" Y="1.356021362305" />
                  <Point X="21.426294921875" Y="1.371576660156" />
                  <Point X="21.438708984375" Y="1.389309814453" />
                  <Point X="21.448654296875" Y="1.407443237305" />
                  <Point X="21.452544921875" Y="1.416838134766" />
                  <Point X="21.473298828125" Y="1.466941894531" />
                  <Point X="21.47908203125" Y="1.486786010742" />
                  <Point X="21.482841796875" Y="1.508098999023" />
                  <Point X="21.484197265625" Y="1.528739257812" />
                  <Point X="21.481052734375" Y="1.549183349609" />
                  <Point X="21.475453125" Y="1.570088134766" />
                  <Point X="21.467953125" Y="1.58937512207" />
                  <Point X="21.463251953125" Y="1.598405639648" />
                  <Point X="21.438208984375" Y="1.646510131836" />
                  <Point X="21.426716796875" Y="1.663708374023" />
                  <Point X="21.4128046875" Y="1.680287719727" />
                  <Point X="21.39786328125" Y="1.694590454102" />
                  <Point X="20.681228515625" Y="2.244484375" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.918853515625" Y="2.733668212891" />
                  <Point X="20.978349609375" Y="2.810142089844" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.690146484375" Y="2.904252441406" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796142578" />
                  <Point X="21.86672265625" Y="2.824613769531" />
                  <Point X="21.938755859375" Y="2.818311523438" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.019537109375" Y="2.835653564453" />
                  <Point X="22.037791015625" Y="2.847282714844" />
                  <Point X="22.053919921875" Y="2.860226806641" />
                  <Point X="22.063513671875" Y="2.869819824219" />
                  <Point X="22.11464453125" Y="2.920950195312" />
                  <Point X="22.12759765625" Y="2.937089599609" />
                  <Point X="22.1392265625" Y="2.955345947266" />
                  <Point X="22.148375" Y="2.973897705078" />
                  <Point X="22.1532890625" Y="2.993989501953" />
                  <Point X="22.15611328125" Y="3.015450683594" />
                  <Point X="22.156564453125" Y="3.036123046875" />
                  <Point X="22.1553828125" Y="3.049633056641" />
                  <Point X="22.14908203125" Y="3.121667480469" />
                  <Point X="22.145044921875" Y="3.141961669922" />
                  <Point X="22.13853515625" Y="3.162604248047" />
                  <Point X="22.130205078125" Y="3.181532714844" />
                  <Point X="21.816666015625" Y="3.724595947266" />
                  <Point X="22.299376953125" Y="4.094684814453" />
                  <Point X="22.393087890625" Y="4.146748046875" />
                  <Point X="22.83296484375" Y="4.391133789062" />
                  <Point X="22.925212890625" Y="4.270912597656" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.004888671875" Y="4.189394042969" />
                  <Point X="23.019931640625" Y="4.181563476563" />
                  <Point X="23.10010546875" Y="4.139827148438" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.22255078125" Y="4.134483398438" />
                  <Point X="23.238216796875" Y="4.140973144531" />
                  <Point X="23.321724609375" Y="4.175562988281" />
                  <Point X="23.33985546875" Y="4.185509765625" />
                  <Point X="23.3575859375" Y="4.197924316406" />
                  <Point X="23.373140625" Y="4.211565429688" />
                  <Point X="23.385373046875" Y="4.22825" />
                  <Point X="23.396193359375" Y="4.246994140625" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.40962109375" Y="4.282095214844" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583984375" />
                  <Point X="23.44272265625" Y="4.410145019531" />
                  <Point X="23.442271484375" Y="4.430826171875" />
                  <Point X="23.415798828125" Y="4.6318984375" />
                  <Point X="23.41587890625" Y="4.631920898438" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.163966796875" Y="4.823103515625" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.835568359375" Y="4.400250976562" />
                  <Point X="24.866095703125" Y="4.286315917969" />
                  <Point X="24.87887109375" Y="4.258122558594" />
                  <Point X="24.89673046875" Y="4.231395507813" />
                  <Point X="24.91788671875" Y="4.208808105469" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="25.938033203125" Y="4.809046386719" />
                  <Point X="26.4810234375" Y="4.677951660156" />
                  <Point X="26.54095703125" Y="4.656213867188" />
                  <Point X="26.894646484375" Y="4.527928222656" />
                  <Point X="26.95380859375" Y="4.500260253906" />
                  <Point X="27.294578125" Y="4.340893066406" />
                  <Point X="27.35176171875" Y="4.307577636719" />
                  <Point X="27.680984375" Y="4.115770996094" />
                  <Point X="27.73488671875" Y="4.077440185547" />
                  <Point X="27.943263671875" Y="3.929254638672" />
                  <Point X="27.283703125" Y="2.786864746094" />
                  <Point X="27.147583984375" Y="2.55109765625" />
                  <Point X="27.1398828125" Y="2.53409375" />
                  <Point X="27.13148828125" Y="2.509327392578" />
                  <Point X="27.129685546875" Y="2.503373535156" />
                  <Point X="27.111607421875" Y="2.435770751953" />
                  <Point X="27.1083828125" Y="2.411278320312" />
                  <Point X="27.1083671875" Y="2.381407958984" />
                  <Point X="27.10905078125" Y="2.369985351562" />
                  <Point X="27.116099609375" Y="2.311528320312" />
                  <Point X="27.12144140625" Y="2.289607177734" />
                  <Point X="27.12970703125" Y="2.267518066406" />
                  <Point X="27.140072265625" Y="2.247467285156" />
                  <Point X="27.146859375" Y="2.237465820312" />
                  <Point X="27.18303125" Y="2.184158691406" />
                  <Point X="27.199611328125" Y="2.165549072266" />
                  <Point X="27.222912109375" Y="2.145462402344" />
                  <Point X="27.2316015625" Y="2.1388046875" />
                  <Point X="27.28491015625" Y="2.102633789063" />
                  <Point X="27.304955078125" Y="2.092271728516" />
                  <Point X="27.32704296875" Y="2.084006103516" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.359931640625" Y="2.077341064453" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.443830078125" Y="2.070656005859" />
                  <Point X="27.475416015625" Y="2.075385498047" />
                  <Point X="27.485888671875" Y="2.077562744141" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.845150390625" Y="2.835652587891" />
                  <Point X="28.967326171875" Y="2.906191162109" />
                  <Point X="29.123283203125" Y="2.689448242188" />
                  <Point X="29.15332421875" Y="2.639802978516" />
                  <Point X="29.26219921875" Y="2.459884277344" />
                  <Point X="28.41" Y="1.805967651367" />
                  <Point X="28.23078515625" Y="1.668451171875" />
                  <Point X="28.216888671875" Y="1.655373535156" />
                  <Point X="28.198513671875" Y="1.63421496582" />
                  <Point X="28.19484375" Y="1.629717529297" />
                  <Point X="28.14619140625" Y="1.566244995117" />
                  <Point X="28.13362109375" Y="1.544319946289" />
                  <Point X="28.121751953125" Y="1.515210693359" />
                  <Point X="28.118232421875" Y="1.504929931641" />
                  <Point X="28.100107421875" Y="1.440124389648" />
                  <Point X="28.09665234375" Y="1.41781628418" />
                  <Point X="28.095837890625" Y="1.394239501953" />
                  <Point X="28.0977421875" Y="1.371756225586" />
                  <Point X="28.100533203125" Y="1.358234130859" />
                  <Point X="28.11541015625" Y="1.286129638672" />
                  <Point X="28.123869140625" Y="1.262076538086" />
                  <Point X="28.138658203125" Y="1.233159057617" />
                  <Point X="28.143875" Y="1.224201416016" />
                  <Point X="28.18433984375" Y="1.162695800781" />
                  <Point X="28.198892578125" Y="1.145449951172" />
                  <Point X="28.21613671875" Y="1.129360229492" />
                  <Point X="28.2343515625" Y="1.116032226562" />
                  <Point X="28.24535546875" Y="1.109838867188" />
                  <Point X="28.303994140625" Y="1.076829833984" />
                  <Point X="28.32820703125" Y="1.067291870117" />
                  <Point X="28.36105859375" Y="1.059327880859" />
                  <Point X="28.370994140625" Y="1.05747265625" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.681904296875" Y="1.204138427734" />
                  <Point X="29.776837890625" Y="1.21663659668" />
                  <Point X="29.84594140625" Y="0.932787353516" />
                  <Point X="29.855408203125" Y="0.871983825684" />
                  <Point X="29.890865234375" Y="0.644238891602" />
                  <Point X="28.92196484375" Y="0.384622406006" />
                  <Point X="28.716580078125" Y="0.329589874268" />
                  <Point X="28.69841015625" Y="0.322660339355" />
                  <Point X="28.67171484375" Y="0.309205200195" />
                  <Point X="28.666931640625" Y="0.30662020874" />
                  <Point X="28.589037109375" Y="0.261595214844" />
                  <Point X="28.568669921875" Y="0.245780944824" />
                  <Point X="28.545587890625" Y="0.222186798096" />
                  <Point X="28.53876171875" Y="0.214402084351" />
                  <Point X="28.492025390625" Y="0.15484803772" />
                  <Point X="28.48030078125" Y="0.135567016602" />
                  <Point X="28.47052734375" Y="0.114103088379" />
                  <Point X="28.46368359375" Y="0.092608657837" />
                  <Point X="28.460759765625" Y="0.077345985413" />
                  <Point X="28.445181640625" Y="-0.004001974583" />
                  <Point X="28.44383203125" Y="-0.029992891312" />
                  <Point X="28.44675390625" Y="-0.064064826965" />
                  <Point X="28.4481015625" Y="-0.073817810059" />
                  <Point X="28.463681640625" Y="-0.155165771484" />
                  <Point X="28.47052734375" Y="-0.176665817261" />
                  <Point X="28.48030078125" Y="-0.198128677368" />
                  <Point X="28.49202734375" Y="-0.217411056519" />
                  <Point X="28.500796875" Y="-0.22858480835" />
                  <Point X="28.547533203125" Y="-0.288139007568" />
                  <Point X="28.566685546875" Y="-0.306531616211" />
                  <Point X="28.59561328125" Y="-0.327399597168" />
                  <Point X="28.60365234375" Y="-0.332602966309" />
                  <Point X="28.681546875" Y="-0.3776277771" />
                  <Point X="28.692708984375" Y="-0.383137786865" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="29.8112578125" Y="-0.685468078613" />
                  <Point X="29.891474609375" Y="-0.706961914062" />
                  <Point X="29.8550234375" Y="-0.948730224609" />
                  <Point X="29.842890625" Y="-1.001892028809" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="28.6625703125" Y="-1.034798828125" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.3459765625" Y="-1.011743225098" />
                  <Point X="28.193095703125" Y="-1.044972412109" />
                  <Point X="28.1639765625" Y="-1.056595947266" />
                  <Point X="28.1361484375" Y="-1.07348840332" />
                  <Point X="28.1123984375" Y="-1.093959472656" />
                  <Point X="28.095060546875" Y="-1.114810913086" />
                  <Point X="28.002654296875" Y="-1.225947387695" />
                  <Point X="27.987931640625" Y="-1.250334594727" />
                  <Point X="27.97658984375" Y="-1.277719726562" />
                  <Point X="27.969759765625" Y="-1.3053671875" />
                  <Point X="27.967275390625" Y="-1.332370849609" />
                  <Point X="27.95403125" Y="-1.476297485352" />
                  <Point X="27.95634765625" Y="-1.507561157227" />
                  <Point X="27.964078125" Y="-1.539182373047" />
                  <Point X="27.97644921875" Y="-1.567994750977" />
                  <Point X="27.992322265625" Y="-1.592685668945" />
                  <Point X="28.0769296875" Y="-1.724285400391" />
                  <Point X="28.0869375" Y="-1.737243286133" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="29.126498046875" Y="-2.540412353516" />
                  <Point X="29.213125" Y="-2.606883056641" />
                  <Point X="29.124814453125" Y="-2.749782226562" />
                  <Point X="29.09971875" Y="-2.785439697266" />
                  <Point X="29.028982421875" Y="-2.885945556641" />
                  <Point X="28.01309375" Y="-2.299421630859" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.72008984375" Y="-2.153659912109" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.4448359375" Y="-2.135176025391" />
                  <Point X="27.416474609375" Y="-2.150101806641" />
                  <Point X="27.265318359375" Y="-2.229655029297" />
                  <Point X="27.242384765625" Y="-2.246549560547" />
                  <Point X="27.22142578125" Y="-2.267509033203" />
                  <Point X="27.204533203125" Y="-2.290439208984" />
                  <Point X="27.189607421875" Y="-2.318799560547" />
                  <Point X="27.110052734375" Y="-2.469957275391" />
                  <Point X="27.100228515625" Y="-2.499735107422" />
                  <Point X="27.095271484375" Y="-2.531908447266" />
                  <Point X="27.095677734375" Y="-2.563260498047" />
                  <Point X="27.10184375" Y="-2.5973984375" />
                  <Point X="27.134705078125" Y="-2.779350830078" />
                  <Point X="27.13898828125" Y="-2.795140136719" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.804619140625" Y="-3.956756347656" />
                  <Point X="27.86128515625" Y="-4.054905029297" />
                  <Point X="27.7818515625" Y="-4.111642578125" />
                  <Point X="27.753796875" Y="-4.129801757813" />
                  <Point X="27.701765625" Y="-4.163480957031" />
                  <Point X="26.9204296875" Y="-3.145222900391" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900556640625" />
                  <Point X="26.688255859375" Y="-2.878910400391" />
                  <Point X="26.50880078125" Y="-2.763537841797" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.38027734375" Y="-2.744505126953" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.104595703125" Y="-2.795461425781" />
                  <Point X="26.076162109375" Y="-2.819103271484" />
                  <Point X="25.924611328125" Y="-2.945111816406" />
                  <Point X="25.904142578125" Y="-2.968861572266" />
                  <Point X="25.88725" Y="-2.996688476562" />
                  <Point X="25.875625" Y="-3.025808105469" />
                  <Point X="25.867123046875" Y="-3.064922119141" />
                  <Point X="25.821810546875" Y="-3.273395996094" />
                  <Point X="25.819724609375" Y="-3.289627685547" />
                  <Point X="25.8197421875" Y="-3.323119873047" />
                  <Point X="26.004740234375" Y="-4.728317382812" />
                  <Point X="26.02206640625" Y="-4.859915039062" />
                  <Point X="25.975671875" Y="-4.870084960938" />
                  <Point X="25.949759765625" Y="-4.874791992188" />
                  <Point X="25.92931640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941572265625" Y="-4.75263671875" />
                  <Point X="23.90184375" Y="-4.742414550781" />
                  <Point X="23.858755859375" Y="-4.731328125" />
                  <Point X="23.871607421875" Y="-4.633702636719" />
                  <Point X="23.8792265625" Y="-4.575838867188" />
                  <Point X="23.879923828125" Y="-4.568099609375" />
                  <Point X="23.88055078125" Y="-4.54103125" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688964844" />
                  <Point X="23.867103515625" Y="-4.449616699219" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.811875" Y="-4.178468261719" />
                  <Point X="23.80097265625" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.769423828125" Y="-4.094859619141" />
                  <Point X="23.749791015625" Y="-4.070937255859" />
                  <Point X="23.738994140625" Y="-4.059781494141" />
                  <Point X="23.70214453125" Y="-4.027464111328" />
                  <Point X="23.505734375" Y="-3.855216796875" />
                  <Point X="23.493263671875" Y="-3.845967529297" />
                  <Point X="23.46698046875" Y="-3.829622558594" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.31427734375" Y="-3.792963867188" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.79549609375" />
                  <Point X="22.918134765625" Y="-3.808269042969" />
                  <Point X="22.9045625" Y="-3.815811767578" />
                  <Point X="22.86380859375" Y="-3.843042480469" />
                  <Point X="22.64659765625" Y="-3.9881796875" />
                  <Point X="22.640314453125" Y="-3.992760253906" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629394531" />
                  <Point X="22.496798828125" Y="-4.162479003906" />
                  <Point X="22.252412109375" Y="-4.011161132812" />
                  <Point X="22.1974140625" Y="-3.968814208984" />
                  <Point X="22.01913671875" Y="-3.831546386719" />
                  <Point X="22.540904296875" Y="-2.927819335938" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.663791015625" Y="-2.713489746094" />
                  <Point X="22.6762734375" Y="-2.683848388672" />
                  <Point X="22.68108984375" Y="-2.669180175781" />
                  <Point X="22.686505859375" Y="-2.646647216797" />
                  <Point X="22.6891328125" Y="-2.623628662109" />
                  <Point X="22.68893359375" Y="-2.600454833984" />
                  <Point X="22.688125" Y="-2.588871582031" />
                  <Point X="22.68398046875" Y="-2.557390380859" />
                  <Point X="22.68157421875" Y="-2.545269042969" />
                  <Point X="22.6752109375" Y="-2.521451904297" />
                  <Point X="22.665814453125" Y="-2.498668212891" />
                  <Point X="22.6535390625" Y="-2.477289794922" />
                  <Point X="22.646703125" Y="-2.466999267578" />
                  <Point X="22.63437890625" Y="-2.450833251953" />
                  <Point X="22.6176328125" Y="-2.431676025391" />
                  <Point X="22.603025390625" Y="-2.417068115234" />
                  <Point X="22.5911875" Y="-2.4070234375" />
                  <Point X="22.56603125" Y="-2.388995605469" />
                  <Point X="22.552712890625" Y="-2.381012451172" />
                  <Point X="22.523884765625" Y="-2.366795166016" />
                  <Point X="22.5094453125" Y="-2.361088378906" />
                  <Point X="22.479828125" Y="-2.352103271484" />
                  <Point X="22.449140625" Y="-2.348062744141" />
                  <Point X="22.418205078125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.2276171875" Y="-3.00575390625" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="20.995986328125" Y="-2.740592529297" />
                  <Point X="20.9565546875" Y="-2.674475341797" />
                  <Point X="20.818734375" Y="-2.443372802734" />
                  <Point X="21.74610546875" Y="-1.731776855469" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.960833984375" Y="-1.566072021484" />
                  <Point X="21.98371484375" Y="-1.543452026367" />
                  <Point X="21.997099609375" Y="-1.526854125977" />
                  <Point X="22.015798828125" Y="-1.497435546875" />
                  <Point X="22.0237890625" Y="-1.481860961914" />
                  <Point X="22.0317265625" Y="-1.462085083008" />
                  <Point X="22.03933203125" Y="-1.438942993164" />
                  <Point X="22.045818359375" Y="-1.413895141602" />
                  <Point X="22.04805078125" Y="-1.402387207031" />
                  <Point X="22.0521640625" Y="-1.370904785156" />
                  <Point X="22.052919921875" Y="-1.355685791016" />
                  <Point X="22.05221875" Y="-1.332817504883" />
                  <Point X="22.048771484375" Y="-1.31020690918" />
                  <Point X="22.042625" Y="-1.288169189453" />
                  <Point X="22.038884765625" Y="-1.277332885742" />
                  <Point X="22.02687890625" Y="-1.24835144043" />
                  <Point X="22.021419921875" Y="-1.237273681641" />
                  <Point X="22.009115234375" Y="-1.215923095703" />
                  <Point X="21.9941484375" Y="-1.1963515625" />
                  <Point X="21.976767578125" Y="-1.178883911133" />
                  <Point X="21.9675078125" Y="-1.170715332031" />
                  <Point X="21.950822265625" Y="-1.157823974609" />
                  <Point X="21.931029296875" Y="-1.144432250977" />
                  <Point X="21.90873046875" Y="-1.131308105469" />
                  <Point X="21.89456640625" Y="-1.124481323242" />
                  <Point X="21.8653046875" Y="-1.113257568359" />
                  <Point X="21.85020703125" Y="-1.108860595703" />
                  <Point X="21.8183203125" Y="-1.10237902832" />
                  <Point X="21.802703125" Y="-1.100532592773" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="20.385078125" Y="-1.280638671875" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.259236328125" Y="-0.974109375" />
                  <Point X="20.24880859375" Y="-0.901186279297" />
                  <Point X="20.213548828125" Y="-0.654654418945" />
                  <Point X="21.257306640625" Y="-0.374980133057" />
                  <Point X="21.491712890625" Y="-0.312171234131" />
                  <Point X="21.50134375" Y="-0.309031860352" />
                  <Point X="21.5202109375" Y="-0.30174710083" />
                  <Point X="21.529447265625" Y="-0.297602020264" />
                  <Point X="21.5493984375" Y="-0.287339172363" />
                  <Point X="21.5708203125" Y="-0.274472290039" />
                  <Point X="21.594189453125" Y="-0.258253234863" />
                  <Point X="21.60351953125" Y="-0.250871276855" />
                  <Point X="21.62747265625" Y="-0.229347564697" />
                  <Point X="21.641650390625" Y="-0.213380859375" />
                  <Point X="21.6617578125" Y="-0.18482623291" />
                  <Point X="21.670490234375" Y="-0.169614395142" />
                  <Point X="21.679376953125" Y="-0.150167236328" />
                  <Point X="21.688025390625" Y="-0.127513839722" />
                  <Point X="21.695814453125" Y="-0.102415748596" />
                  <Point X="21.699291015625" Y="-0.086499725342" />
                  <Point X="21.703474609375" Y="-0.054304889679" />
                  <Point X="21.704181640625" Y="-0.038026077271" />
                  <Point X="21.702720703125" Y="-0.003677401304" />
                  <Point X="21.70141015625" Y="0.008520226479" />
                  <Point X="21.6972265625" Y="0.032641273499" />
                  <Point X="21.694353515625" Y="0.044564689636" />
                  <Point X="21.686564453125" Y="0.069662780762" />
                  <Point X="21.682333984375" Y="0.080782447815" />
                  <Point X="21.669017578125" Y="0.110108718872" />
                  <Point X="21.65798828125" Y="0.12853036499" />
                  <Point X="21.63664453125" Y="0.156447982788" />
                  <Point X="21.624669921875" Y="0.169412216187" />
                  <Point X="21.60846484375" Y="0.183973388672" />
                  <Point X="21.5898046875" Y="0.198736694336" />
                  <Point X="21.566435546875" Y="0.214955764771" />
                  <Point X="21.559513671875" Y="0.219330184937" />
                  <Point X="21.53438671875" Y="0.232833312988" />
                  <Point X="21.503435546875" Y="0.245635391235" />
                  <Point X="21.491712890625" Y="0.249611343384" />
                  <Point X="20.242353515625" Y="0.584376342773" />
                  <Point X="20.2145546875" Y="0.591825195312" />
                  <Point X="20.26866796875" Y="0.95752355957" />
                  <Point X="20.289669921875" Y="1.03502746582" />
                  <Point X="20.3664140625" Y="1.318237182617" />
                  <Point X="21.05390625" Y="1.227727050781" />
                  <Point X="21.221935546875" Y="1.20560546875" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.20470324707" />
                  <Point X="21.28485546875" Y="1.206589355469" />
                  <Point X="21.295111328125" Y="1.208053833008" />
                  <Point X="21.31540234375" Y="1.212090332031" />
                  <Point X="21.335140625" Y="1.21772265625" />
                  <Point X="21.38686328125" Y="1.234030273438" />
                  <Point X="21.3965546875" Y="1.237678100586" />
                  <Point X="21.41548828125" Y="1.246009399414" />
                  <Point X="21.42473046875" Y="1.250692749023" />
                  <Point X="21.4434765625" Y="1.261516845703" />
                  <Point X="21.452154296875" Y="1.267180541992" />
                  <Point X="21.4688359375" Y="1.279414428711" />
                  <Point X="21.48408203125" Y="1.293386108398" />
                  <Point X="21.49772265625" Y="1.30894140625" />
                  <Point X="21.50412109375" Y="1.317095214844" />
                  <Point X="21.51653515625" Y="1.334828369141" />
                  <Point X="21.52200390625" Y="1.343626586914" />
                  <Point X="21.53194921875" Y="1.361760009766" />
                  <Point X="21.54031640625" Y="1.380490112305" />
                  <Point X="21.5610703125" Y="1.43059375" />
                  <Point X="21.56450390625" Y="1.440361694336" />
                  <Point X="21.570287109375" Y="1.460205810547" />
                  <Point X="21.57263671875" Y="1.470282104492" />
                  <Point X="21.576396484375" Y="1.491594970703" />
                  <Point X="21.57763671875" Y="1.501873657227" />
                  <Point X="21.5789921875" Y="1.522513916016" />
                  <Point X="21.57809375" Y="1.543181518555" />
                  <Point X="21.57494921875" Y="1.563625488281" />
                  <Point X="21.572818359375" Y="1.573763793945" />
                  <Point X="21.56721875" Y="1.594668579102" />
                  <Point X="21.563994140625" Y="1.604518554688" />
                  <Point X="21.556494140625" Y="1.623805419922" />
                  <Point X="21.547517578125" Y="1.642273071289" />
                  <Point X="21.522474609375" Y="1.690377563477" />
                  <Point X="21.517197265625" Y="1.699291503906" />
                  <Point X="21.505705078125" Y="1.716489868164" />
                  <Point X="21.499490234375" Y="1.724774169922" />
                  <Point X="21.485578125" Y="1.741353515625" />
                  <Point X="21.478498046875" Y="1.748913574219" />
                  <Point X="21.463556640625" Y="1.763216308594" />
                  <Point X="21.4556953125" Y="1.769958984375" />
                  <Point X="20.77238671875" Y="2.294281005859" />
                  <Point X="20.997716796875" Y="2.680324951172" />
                  <Point X="21.053330078125" Y="2.751807617188" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.642646484375" Y="2.821979980469" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656982422" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.858443359375" Y="2.729975097656" />
                  <Point X="21.9304765625" Y="2.723672851562" />
                  <Point X="21.940830078125" Y="2.723334228516" />
                  <Point X="21.961509765625" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.02356640625" Y="2.734227294922" />
                  <Point X="22.043005859375" Y="2.741302978516" />
                  <Point X="22.061556640625" Y="2.750451904297" />
                  <Point X="22.070580078125" Y="2.755531738281" />
                  <Point X="22.088833984375" Y="2.767160888672" />
                  <Point X="22.097251953125" Y="2.773192138672" />
                  <Point X="22.113380859375" Y="2.786136230469" />
                  <Point X="22.130685546875" Y="2.802642089844" />
                  <Point X="22.18181640625" Y="2.853772460938" />
                  <Point X="22.188734375" Y="2.861487792969" />
                  <Point X="22.2016875" Y="2.877627197266" />
                  <Point X="22.20772265625" Y="2.886051269531" />
                  <Point X="22.2193515625" Y="2.904307617188" />
                  <Point X="22.2244296875" Y="2.913329589844" />
                  <Point X="22.233578125" Y="2.931881347656" />
                  <Point X="22.240654296875" Y="2.951327880859" />
                  <Point X="22.245568359375" Y="2.971419677734" />
                  <Point X="22.2474765625" Y="2.981594726562" />
                  <Point X="22.25030078125" Y="3.003055908203" />
                  <Point X="22.25108984375" Y="3.013377929688" />
                  <Point X="22.251541015625" Y="3.034050292969" />
                  <Point X="22.250021484375" Y="3.057910644531" />
                  <Point X="22.243720703125" Y="3.129945068359" />
                  <Point X="22.242255859375" Y="3.140202636719" />
                  <Point X="22.23821875" Y="3.160496826172" />
                  <Point X="22.235646484375" Y="3.170533447266" />
                  <Point X="22.22913671875" Y="3.191176025391" />
                  <Point X="22.22548828125" Y="3.200870361328" />
                  <Point X="22.217158203125" Y="3.219798828125" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="21.940611328125" Y="3.699915039062" />
                  <Point X="22.351630859375" Y="4.015039306641" />
                  <Point X="22.439224609375" Y="4.063703613281" />
                  <Point X="22.8074765625" Y="4.268295898437" />
                  <Point X="22.84984375" Y="4.213080566406" />
                  <Point X="22.881435546875" Y="4.171909179688" />
                  <Point X="22.8881796875" Y="4.164048828125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93491015625" Y="4.121896972656" />
                  <Point X="22.952111328125" Y="4.110403808594" />
                  <Point X="22.97606640625" Y="4.097296875" />
                  <Point X="23.056240234375" Y="4.055560302734" />
                  <Point X="23.06567578125" Y="4.05128515625" />
                  <Point X="23.084955078125" Y="4.043788574219" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.2292734375" Y="4.037489257812" />
                  <Point X="23.24913671875" Y="4.043279296875" />
                  <Point X="23.27457421875" Y="4.053205810547" />
                  <Point X="23.35808203125" Y="4.087795654297" />
                  <Point X="23.36741796875" Y="4.092273681641" />
                  <Point X="23.385548828125" Y="4.102220214844" />
                  <Point X="23.39434375" Y="4.107689453125" />
                  <Point X="23.41207421875" Y="4.120104003906" />
                  <Point X="23.420224609375" Y="4.126499511719" />
                  <Point X="23.435779296875" Y="4.140140625" />
                  <Point X="23.449755859375" Y="4.15539453125" />
                  <Point X="23.46198828125" Y="4.172079101562" />
                  <Point X="23.4676484375" Y="4.180755371094" />
                  <Point X="23.47846875" Y="4.199499511719" />
                  <Point X="23.4831484375" Y="4.208733398438" />
                  <Point X="23.4914765625" Y="4.227660644531" />
                  <Point X="23.500224609375" Y="4.253527832031" />
                  <Point X="23.527404296875" Y="4.33973046875" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370050292969" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401863769531" />
                  <Point X="23.53769921875" Y="4.412216796875" />
                  <Point X="23.537248046875" Y="4.432897949219" />
                  <Point X="23.536458984375" Y="4.4432265625" />
                  <Point X="23.520734375" Y="4.562655273438" />
                  <Point X="24.068826171875" Y="4.7163203125" />
                  <Point X="24.175009765625" Y="4.728747558594" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.7438046875" Y="4.375663085938" />
                  <Point X="24.77433203125" Y="4.261728027344" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912597656" />
                  <Point X="24.7998828125" Y="4.205341308594" />
                  <Point X="24.8177421875" Y="4.178614257812" />
                  <Point X="24.82739453125" Y="4.166452636719" />
                  <Point X="24.84855078125" Y="4.143865234375" />
                  <Point X="24.873103515625" Y="4.125025390625" />
                  <Point X="24.9003984375" Y="4.110436035156" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="25.91573828125" Y="4.716699707031" />
                  <Point X="26.45359765625" Y="4.58684375" />
                  <Point X="26.50856640625" Y="4.566906738281" />
                  <Point X="26.85825" Y="4.44007421875" />
                  <Point X="26.913564453125" Y="4.414205566406" />
                  <Point X="27.250458984375" Y="4.256650390625" />
                  <Point X="27.303939453125" Y="4.225492675781" />
                  <Point X="27.629435546875" Y="4.035856933594" />
                  <Point X="27.67983203125" Y="4.00001953125" />
                  <Point X="27.817783203125" Y="3.901916015625" />
                  <Point X="27.201431640625" Y="2.834364746094" />
                  <Point X="27.0653125" Y="2.59859765625" />
                  <Point X="27.061044921875" Y="2.590291259766" />
                  <Point X="27.04991015625" Y="2.56458984375" />
                  <Point X="27.041515625" Y="2.539823486328" />
                  <Point X="27.03791015625" Y="2.527915771484" />
                  <Point X="27.01983203125" Y="2.460312988281" />
                  <Point X="27.017419921875" Y="2.448171142578" />
                  <Point X="27.0141953125" Y="2.423678710938" />
                  <Point X="27.0133828125" Y="2.411328125" />
                  <Point X="27.0133671875" Y="2.381457763672" />
                  <Point X="27.014734375" Y="2.358612548828" />
                  <Point X="27.021783203125" Y="2.300155517578" />
                  <Point X="27.02380078125" Y="2.289036621094" />
                  <Point X="27.029142578125" Y="2.267115478516" />
                  <Point X="27.032466796875" Y="2.256313232422" />
                  <Point X="27.040732421875" Y="2.234224121094" />
                  <Point X="27.04531640625" Y="2.223892333984" />
                  <Point X="27.055681640625" Y="2.203841552734" />
                  <Point X="27.06825" Y="2.18412109375" />
                  <Point X="27.104421875" Y="2.130813964844" />
                  <Point X="27.112099609375" Y="2.120962890625" />
                  <Point X="27.1286796875" Y="2.102353271484" />
                  <Point X="27.13758203125" Y="2.093594726562" />
                  <Point X="27.1608828125" Y="2.073508056641" />
                  <Point X="27.17826171875" Y="2.060192626953" />
                  <Point X="27.2315703125" Y="2.024021728516" />
                  <Point X="27.24128515625" Y="2.018242797852" />
                  <Point X="27.261330078125" Y="2.007880737305" />
                  <Point X="27.27166015625" Y="2.003297485352" />
                  <Point X="27.293748046875" Y="1.995031860352" />
                  <Point X="27.304548828125" Y="1.991707519531" />
                  <Point X="27.326470703125" Y="1.986364990234" />
                  <Point X="27.34855859375" Y="1.983024414062" />
                  <Point X="27.407015625" Y="1.975975341797" />
                  <Point X="27.419748046875" Y="1.975301879883" />
                  <Point X="27.445189453125" Y="1.975665771484" />
                  <Point X="27.4578984375" Y="1.976703369141" />
                  <Point X="27.489484375" Y="1.981432861328" />
                  <Point X="27.5104296875" Y="1.985787475586" />
                  <Point X="27.578033203125" Y="2.003865356445" />
                  <Point X="27.583994140625" Y="2.005670654297" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.892650390625" Y="2.753380126953" />
                  <Point X="28.94040625" Y="2.780951904297" />
                  <Point X="29.043962890625" Y="2.637033447266" />
                  <Point X="29.072046875" Y="2.590620605469" />
                  <Point X="29.13688671875" Y="2.483472412109" />
                  <Point X="28.35216796875" Y="1.881336181641" />
                  <Point X="28.172953125" Y="1.743819824219" />
                  <Point X="28.1656796875" Y="1.737633666992" />
                  <Point X="28.145162109375" Y="1.717664672852" />
                  <Point X="28.126787109375" Y="1.696506103516" />
                  <Point X="28.1194453125" Y="1.687511230469" />
                  <Point X="28.07079296875" Y="1.624038452148" />
                  <Point X="28.063775390625" Y="1.613496337891" />
                  <Point X="28.051205078125" Y="1.591571289062" />
                  <Point X="28.04565234375" Y="1.580188720703" />
                  <Point X="28.033783203125" Y="1.551079345703" />
                  <Point X="28.026744140625" Y="1.530517822266" />
                  <Point X="28.008619140625" Y="1.465712402344" />
                  <Point X="28.0062265625" Y="1.454664672852" />
                  <Point X="28.002771484375" Y="1.432356567383" />
                  <Point X="28.001708984375" Y="1.421096069336" />
                  <Point X="28.00089453125" Y="1.397519287109" />
                  <Point X="28.001177734375" Y="1.386221801758" />
                  <Point X="28.00308203125" Y="1.363738647461" />
                  <Point X="28.007494140625" Y="1.339030517578" />
                  <Point X="28.02237109375" Y="1.266926025391" />
                  <Point X="28.025791015625" Y="1.254612304688" />
                  <Point X="28.03425" Y="1.230559204102" />
                  <Point X="28.0392890625" Y="1.218820068359" />
                  <Point X="28.054078125" Y="1.189902587891" />
                  <Point X="28.06451171875" Y="1.171987304688" />
                  <Point X="28.1049765625" Y="1.110481689453" />
                  <Point X="28.111736328125" Y="1.101429199219" />
                  <Point X="28.1262890625" Y="1.084183349609" />
                  <Point X="28.13408203125" Y="1.075989990234" />
                  <Point X="28.151326171875" Y="1.059900268555" />
                  <Point X="28.160037109375" Y="1.052692504883" />
                  <Point X="28.178251953125" Y="1.039364501953" />
                  <Point X="28.198759765625" Y="1.027051025391" />
                  <Point X="28.2573984375" Y="0.994041931152" />
                  <Point X="28.26917578125" Y="0.988440490723" />
                  <Point X="28.293388671875" Y="0.978902587891" />
                  <Point X="28.30582421875" Y="0.974966003418" />
                  <Point X="28.33867578125" Y="0.967002075195" />
                  <Point X="28.358546875" Y="0.963291687012" />
                  <Point X="28.437833984375" Y="0.952813049316" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797302246" />
                  <Point X="29.6943046875" Y="1.109951049805" />
                  <Point X="29.704701171875" Y="1.111319946289" />
                  <Point X="29.752689453125" Y="0.914205078125" />
                  <Point X="29.7615390625" Y="0.857368896484" />
                  <Point X="29.78387109375" Y="0.713921386719" />
                  <Point X="28.897376953125" Y="0.476385314941" />
                  <Point X="28.6919921875" Y="0.421352813721" />
                  <Point X="28.682728515625" Y="0.418353729248" />
                  <Point X="28.65565234375" Y="0.407493865967" />
                  <Point X="28.62895703125" Y="0.394038757324" />
                  <Point X="28.619390625" Y="0.388868591309" />
                  <Point X="28.54149609375" Y="0.343843597412" />
                  <Point X="28.530775390625" Y="0.33663168335" />
                  <Point X="28.510408203125" Y="0.32081729126" />
                  <Point X="28.50076171875" Y="0.312215026855" />
                  <Point X="28.4776796875" Y="0.28862097168" />
                  <Point X="28.46402734375" Y="0.273051544189" />
                  <Point X="28.417291015625" Y="0.213497467041" />
                  <Point X="28.41085546875" Y="0.204207199097" />
                  <Point X="28.399130859375" Y="0.184926208496" />
                  <Point X="28.393841796875" Y="0.174935333252" />
                  <Point X="28.384068359375" Y="0.153471511841" />
                  <Point X="28.380005859375" Y="0.142925094604" />
                  <Point X="28.373162109375" Y="0.121430656433" />
                  <Point X="28.36745703125" Y="0.095219825745" />
                  <Point X="28.35187890625" Y="0.01387183857" />
                  <Point X="28.35030859375" Y="0.000924408793" />
                  <Point X="28.348958984375" Y="-0.025066610336" />
                  <Point X="28.3491796875" Y="-0.038109901428" />
                  <Point X="28.3521015625" Y="-0.072181846619" />
                  <Point X="28.354796875" Y="-0.091687850952" />
                  <Point X="28.370376953125" Y="-0.173035690308" />
                  <Point X="28.37316015625" Y="-0.183988449097" />
                  <Point X="28.380005859375" Y="-0.205488388062" />
                  <Point X="28.384068359375" Y="-0.216035858154" />
                  <Point X="28.393841796875" Y="-0.237498626709" />
                  <Point X="28.3991328125" Y="-0.247491287231" />
                  <Point X="28.410859375" Y="-0.266773620605" />
                  <Point X="28.426064453125" Y="-0.287237060547" />
                  <Point X="28.47280078125" Y="-0.346791290283" />
                  <Point X="28.48173046875" Y="-0.356659393311" />
                  <Point X="28.5008828125" Y="-0.375052062988" />
                  <Point X="28.51110546875" Y="-0.383576782227" />
                  <Point X="28.540033203125" Y="-0.404444763184" />
                  <Point X="28.556111328125" Y="-0.414851348877" />
                  <Point X="28.634005859375" Y="-0.459876190186" />
                  <Point X="28.63949609375" Y="-0.462814178467" />
                  <Point X="28.659154296875" Y="-0.472014831543" />
                  <Point X="28.683025390625" Y="-0.481027008057" />
                  <Point X="28.6919921875" Y="-0.483912841797" />
                  <Point X="29.78487890625" Y="-0.776751342773" />
                  <Point X="29.7616171875" Y="-0.931037414551" />
                  <Point X="29.750271484375" Y="-0.980754211426" />
                  <Point X="29.727802734375" Y="-1.079219604492" />
                  <Point X="28.674970703125" Y="-0.940611572266" />
                  <Point X="28.436783203125" Y="-0.909253662109" />
                  <Point X="28.428625" Y="-0.908535644531" />
                  <Point X="28.40009765625" Y="-0.908042358398" />
                  <Point X="28.36672265625" Y="-0.910840942383" />
                  <Point X="28.354482421875" Y="-0.912676269531" />
                  <Point X="28.325798828125" Y="-0.918910705566" />
                  <Point X="28.17291796875" Y="-0.952139953613" />
                  <Point X="28.157876953125" Y="-0.956741943359" />
                  <Point X="28.1287578125" Y="-0.96836541748" />
                  <Point X="28.1146796875" Y="-0.975386962891" />
                  <Point X="28.0868515625" Y="-0.992279418945" />
                  <Point X="28.074125" Y="-1.001529907227" />
                  <Point X="28.050375" Y="-1.022000915527" />
                  <Point X="28.0393515625" Y="-1.033221191406" />
                  <Point X="28.022013671875" Y="-1.054072631836" />
                  <Point X="27.929607421875" Y="-1.165209106445" />
                  <Point X="27.921326171875" Y="-1.176848999023" />
                  <Point X="27.906603515625" Y="-1.201236206055" />
                  <Point X="27.900162109375" Y="-1.213983764648" />
                  <Point X="27.8888203125" Y="-1.241368896484" />
                  <Point X="27.88436328125" Y="-1.254935791016" />
                  <Point X="27.877533203125" Y="-1.282583129883" />
                  <Point X="27.87516015625" Y="-1.296663818359" />
                  <Point X="27.87267578125" Y="-1.323667480469" />
                  <Point X="27.859431640625" Y="-1.467594116211" />
                  <Point X="27.859291015625" Y="-1.483317016602" />
                  <Point X="27.861607421875" Y="-1.514580688477" />
                  <Point X="27.864064453125" Y="-1.530121337891" />
                  <Point X="27.871794921875" Y="-1.561742675781" />
                  <Point X="27.87678515625" Y="-1.576663330078" />
                  <Point X="27.88915625" Y="-1.605475830078" />
                  <Point X="27.896537109375" Y="-1.619367553711" />
                  <Point X="27.91241015625" Y="-1.644058349609" />
                  <Point X="27.997017578125" Y="-1.775658081055" />
                  <Point X="28.001744140625" Y="-1.782354370117" />
                  <Point X="28.01980078125" Y="-1.804457397461" />
                  <Point X="28.043494140625" Y="-1.828123291016" />
                  <Point X="28.052798828125" Y="-1.836277709961" />
                  <Point X="29.068666015625" Y="-2.615781005859" />
                  <Point X="29.087173828125" Y="-2.629982177734" />
                  <Point X="29.045494140625" Y="-2.697424804688" />
                  <Point X="29.02203125" Y="-2.730762939453" />
                  <Point X="29.001275390625" Y="-2.760252929688" />
                  <Point X="28.06059375" Y="-2.217149169922" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.736974609375" Y="-2.060172363281" />
                  <Point X="27.555021484375" Y="-2.027312011719" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503295898" />
                  <Point X="27.460140625" Y="-2.03146105957" />
                  <Point X="27.44484375" Y="-2.035136230469" />
                  <Point X="27.4150703125" Y="-2.044959228516" />
                  <Point X="27.40059375" Y="-2.051107177734" />
                  <Point X="27.372232421875" Y="-2.066032958984" />
                  <Point X="27.221076171875" Y="-2.145586181641" />
                  <Point X="27.20897265625" Y="-2.153168457031" />
                  <Point X="27.1860390625" Y="-2.170062988281" />
                  <Point X="27.175208984375" Y="-2.179375244141" />
                  <Point X="27.15425" Y="-2.200334716797" />
                  <Point X="27.144939453125" Y="-2.211162353516" />
                  <Point X="27.128046875" Y="-2.234092529297" />
                  <Point X="27.12046484375" Y="-2.246195068359" />
                  <Point X="27.1055390625" Y="-2.274555419922" />
                  <Point X="27.025984375" Y="-2.425713134766" />
                  <Point X="27.0198359375" Y="-2.440193115234" />
                  <Point X="27.01001171875" Y="-2.469970947266" />
                  <Point X="27.0063359375" Y="-2.485268798828" />
                  <Point X="27.00137890625" Y="-2.517442138672" />
                  <Point X="27.000279296875" Y="-2.533139404297" />
                  <Point X="27.000685546875" Y="-2.564491455078" />
                  <Point X="27.00219140625" Y="-2.580146240234" />
                  <Point X="27.008357421875" Y="-2.614284179688" />
                  <Point X="27.04121875" Y="-2.796236572266" />
                  <Point X="27.04301953125" Y="-2.804222900391" />
                  <Point X="27.05123828125" Y="-2.831540771484" />
                  <Point X="27.064072265625" Y="-2.862479248047" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.72234765625" Y="-4.004256347656" />
                  <Point X="27.735896484375" Y="-4.02772265625" />
                  <Point X="27.723755859375" Y="-4.036082763672" />
                  <Point X="26.995798828125" Y="-3.087390625" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849626464844" />
                  <Point X="26.78325390625" Y="-2.828004882812" />
                  <Point X="26.77330078125" Y="-2.820645996094" />
                  <Point X="26.739630859375" Y="-2.798999755859" />
                  <Point X="26.56017578125" Y="-2.683627197266" />
                  <Point X="26.54628125" Y="-2.676244140625" />
                  <Point X="26.51746875" Y="-2.663873046875" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.371572265625" Y="-2.649904785156" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079876953125" Y="-2.699413330078" />
                  <Point X="26.055494140625" Y="-2.714134521484" />
                  <Point X="26.043857421875" Y="-2.722413818359" />
                  <Point X="26.015423828125" Y="-2.746055664062" />
                  <Point X="25.863873046875" Y="-2.872064208984" />
                  <Point X="25.852650390625" Y="-2.883091552734" />
                  <Point X="25.832181640625" Y="-2.906841308594" />
                  <Point X="25.822935546875" Y="-2.919563720703" />
                  <Point X="25.80604296875" Y="-2.947390625" />
                  <Point X="25.799021484375" Y="-2.961466064453" />
                  <Point X="25.787396484375" Y="-2.990585693359" />
                  <Point X="25.78279296875" Y="-3.005629882812" />
                  <Point X="25.774291015625" Y="-3.044743896484" />
                  <Point X="25.728978515625" Y="-3.253217773438" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677490234" />
                  <Point X="25.7247421875" Y="-3.323169677734" />
                  <Point X="25.7255546875" Y="-3.335519775391" />
                  <Point X="25.83308984375" Y="-4.15232421875" />
                  <Point X="25.7183515625" Y="-3.724115722656" />
                  <Point X="25.655068359375" Y="-3.487936767578" />
                  <Point X="25.652607421875" Y="-3.480120849609" />
                  <Point X="25.642146484375" Y="-3.453578369141" />
                  <Point X="25.6267890625" Y="-3.423815673828" />
                  <Point X="25.62041015625" Y="-3.413211914062" />
                  <Point X="25.594544921875" Y="-3.375944091797" />
                  <Point X="25.456681640625" Y="-3.177311523438" />
                  <Point X="25.446673828125" Y="-3.165175292969" />
                  <Point X="25.4247890625" Y="-3.142718017578" />
                  <Point X="25.412912109375" Y="-3.132396972656" />
                  <Point X="25.38665625" Y="-3.113153320312" />
                  <Point X="25.373240234375" Y="-3.104936279297" />
                  <Point X="25.345240234375" Y="-3.090829101562" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.290630859375" Y="-3.072516357422" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.894990234375" Y="-3.018727539062" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.667068359375" Y="-3.090830078125" />
                  <Point X="24.639068359375" Y="-3.104938232422" />
                  <Point X="24.62565625" Y="-3.113154785156" />
                  <Point X="24.599400390625" Y="-3.132398925781" />
                  <Point X="24.587525390625" Y="-3.142717773438" />
                  <Point X="24.565640625" Y="-3.165174072266" />
                  <Point X="24.555630859375" Y="-3.177311523438" />
                  <Point X="24.529765625" Y="-3.214579101563" />
                  <Point X="24.391904296875" Y="-3.413211914062" />
                  <Point X="24.38753125" Y="-3.420129638672" />
                  <Point X="24.374025390625" Y="-3.445260742188" />
                  <Point X="24.36122265625" Y="-3.476213623047" />
                  <Point X="24.35724609375" Y="-3.487936279297" />
                  <Point X="24.01994921875" Y="-4.746747070312" />
                  <Point X="24.01457421875" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.669037282005" Y="-1.071482982245" />
                  <Point X="29.691398455817" Y="-0.751703298447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.574674014066" Y="-1.059059816445" />
                  <Point X="29.597918005385" Y="-0.72665525412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.697025725417" Y="0.690651173527" />
                  <Point X="29.721590617211" Y="1.041945492694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.480310746127" Y="-1.046636650645" />
                  <Point X="29.504437554952" Y="-0.701607209793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.599975322706" Y="0.664646521755" />
                  <Point X="29.630526906946" Y="1.101554531597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.385947478188" Y="-1.034213484845" />
                  <Point X="29.41095710452" Y="-0.676559165467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.502924919994" Y="0.638641869983" />
                  <Point X="29.534410069748" Y="1.088900488659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.291584210249" Y="-1.021790319046" />
                  <Point X="29.317476654087" Y="-0.65151112114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.405874517282" Y="0.612637218212" />
                  <Point X="29.438293232549" Y="1.076246445721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.083478358666" Y="-2.635961881113" />
                  <Point X="29.084063391732" Y="-2.627595518488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.19722094231" Y="-1.009367153246" />
                  <Point X="29.223996203655" Y="-0.626463076813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.30882411457" Y="0.58663256644" />
                  <Point X="29.342176395351" Y="1.063592402783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.980397971721" Y="-2.748199324928" />
                  <Point X="28.993681017507" Y="-2.558242920276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.102857674371" Y="-0.996943987446" />
                  <Point X="29.130515753222" Y="-0.601415032487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.211773711858" Y="0.560627914668" />
                  <Point X="29.246059558152" Y="1.050938359844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.888861531258" Y="-2.69535064284" />
                  <Point X="28.903298664822" Y="-2.488890014034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.008494406432" Y="-0.984520821646" />
                  <Point X="29.03703530279" Y="-0.57636698816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.114723309147" Y="0.534623262897" />
                  <Point X="29.149942720954" Y="1.038284316906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.797325090794" Y="-2.642501960752" />
                  <Point X="28.812916312137" Y="-2.419537107792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.914131138492" Y="-0.972097655847" />
                  <Point X="28.943554852357" Y="-0.551318943833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.017672906435" Y="0.508618611125" />
                  <Point X="29.053825883755" Y="1.025630273968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.70578865033" Y="-2.589653278664" />
                  <Point X="28.722533959452" Y="-2.350184201549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.819767870553" Y="-0.959674490047" />
                  <Point X="28.850074401925" Y="-0.526270899507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.920622503723" Y="0.482613959353" />
                  <Point X="28.957709046556" Y="1.01297623103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.0562071522" Y="2.421564766754" />
                  <Point X="29.068444942361" Y="2.596573319568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.614252209866" Y="-2.536804596576" />
                  <Point X="28.632151606766" Y="-2.280831295307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.725404602614" Y="-0.947251324247" />
                  <Point X="28.756593951492" Y="-0.50122285518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.823572109693" Y="0.45660943174" />
                  <Point X="28.861592209358" Y="1.000322188092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.95557560916" Y="2.344347422334" />
                  <Point X="28.982058183658" Y="2.723065881844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.522715769402" Y="-2.483955914487" />
                  <Point X="28.541769254081" Y="-2.211478389065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.631041333602" Y="-0.934828173797" />
                  <Point X="28.663295079209" Y="-0.473578122345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.726521718398" Y="0.430604943232" />
                  <Point X="28.765475372159" Y="0.987668145154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.85494406612" Y="2.267130077913" />
                  <Point X="28.888790134487" Y="2.751151405729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.431179328938" Y="-2.431107232399" />
                  <Point X="28.451386901396" Y="-2.142125482823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.536678063357" Y="-0.922405040969" />
                  <Point X="28.571545823213" Y="-0.423772844146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.628723973568" Y="0.393912801257" />
                  <Point X="28.669358534961" Y="0.975014102216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.75431252308" Y="2.189912733492" />
                  <Point X="28.789551670383" Y="2.693856018238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.339642888474" Y="-2.378258550311" />
                  <Point X="28.361004548711" Y="-2.072772576581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.442314793112" Y="-0.909981908142" />
                  <Point X="28.481058777729" Y="-0.355917114681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.529412508709" Y="0.335573454323" />
                  <Point X="28.573241697762" Y="0.962360059278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.653680980041" Y="2.112695389071" />
                  <Point X="28.690313206279" Y="2.636560630748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.24810644801" Y="-2.325409868223" />
                  <Point X="28.270622196026" Y="-2.003419670339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.346777296563" Y="-0.914350993812" />
                  <Point X="28.394076437123" Y="-0.237941770428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.42646125529" Y="0.225182705963" />
                  <Point X="28.477256539833" Y="0.951589117625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.553049437001" Y="2.03547804465" />
                  <Point X="28.591074742175" Y="2.579265243258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.156570007547" Y="-2.272561186135" />
                  <Point X="28.18023984334" Y="-1.934066764097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.250075562528" Y="-0.935369451192" />
                  <Point X="28.382620405558" Y="0.960110113024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.452417893961" Y="1.958260700229" />
                  <Point X="28.491836278071" Y="2.521969855767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.065033567083" Y="-2.219712504047" />
                  <Point X="28.089857490655" Y="-1.864713857854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.153219057749" Y="-0.958601233338" />
                  <Point X="28.288828144896" Y="0.980699063324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.351786350938" Y="1.881043356056" />
                  <Point X="28.392597813967" Y="2.464674468277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.973497126054" Y="-2.166863830042" />
                  <Point X="28.00050718884" Y="-1.780601936565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.053757618733" Y="-1.019085310636" />
                  <Point X="28.196914869428" Y="1.028158753779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.251154812465" Y="1.803826076934" />
                  <Point X="28.293359349863" Y="2.407379080786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.881960684996" Y="-2.114015156449" />
                  <Point X="27.914586550163" Y="-1.647443547362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.950024778817" Y="-1.140653266647" />
                  <Point X="28.107228581397" Y="1.107465848342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.150243676901" Y="1.722610373145" />
                  <Point X="28.194120885759" Y="2.350083693296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.658290526643" Y="-3.95076667515" />
                  <Point X="27.661874483468" Y="-3.899513704715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.789706651912" Y="-2.071428526928" />
                  <Point X="28.022991051506" Y="1.26469381456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.044928460428" Y="1.578413378094" />
                  <Point X="28.094882421655" Y="2.292788305806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.571012250684" Y="-3.837023403604" />
                  <Point X="27.576930633177" Y="-3.752386590793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.695781985845" Y="-2.052733062137" />
                  <Point X="27.995643957551" Y="2.235492918315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.483733974726" Y="-3.723280132059" />
                  <Point X="27.491986782887" Y="-3.605259476871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.60173765704" Y="-2.035748854216" />
                  <Point X="27.896405493447" Y="2.178197530825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.396455698767" Y="-3.609536860514" />
                  <Point X="27.407042932596" Y="-3.45813236295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.507225279612" Y="-2.025458053457" />
                  <Point X="27.797167029343" Y="2.120902143335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.309177422809" Y="-3.495793588969" />
                  <Point X="27.322099082305" Y="-3.311005249028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.41049373493" Y="-2.046902822963" />
                  <Point X="27.697928565239" Y="2.063606755844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.22189914685" Y="-3.382050317424" />
                  <Point X="27.237155232014" Y="-3.163878135106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.311696169637" Y="-2.097893063697" />
                  <Point X="27.59902620326" Y="2.011117852674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.719317899898" Y="3.731369259748" />
                  <Point X="27.735343281312" Y="3.960542890974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.134620870892" Y="-3.268307045879" />
                  <Point X="27.152211381724" Y="-3.016751021185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.212765087835" Y="-2.150792679483" />
                  <Point X="27.501898929726" Y="1.984013896918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.610962241661" Y="3.543691921764" />
                  <Point X="27.644622587234" Y="4.025057289879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.047342594933" Y="-3.154563774333" />
                  <Point X="27.067308636833" Y="-2.869036072656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.109390504529" Y="-2.267237327279" />
                  <Point X="27.406112454555" Y="1.976084251086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.502606583425" Y="3.356014583781" />
                  <Point X="27.553249586985" Y="4.080243275935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.960064326269" Y="-3.040820398473" />
                  <Point X="27.311848559747" Y="1.989928518873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.394250925188" Y="3.168337245798" />
                  <Point X="27.461745460065" Y="4.133554063237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.872786068126" Y="-2.927076872148" />
                  <Point X="27.219569984568" Y="2.032164180085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.285895266951" Y="2.980659907815" />
                  <Point X="27.370241333146" Y="4.18686485054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.784413341704" Y="-2.828984971415" />
                  <Point X="27.12920964407" Y="2.10183187526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.177539580327" Y="2.792982163888" />
                  <Point X="27.278737197091" Y="4.240175507201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.693358371475" Y="-2.769250944194" />
                  <Point X="27.042894648888" Y="2.229350703702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.069183793352" Y="2.605302984856" />
                  <Point X="27.186740997454" Y="4.286449326786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.602223478191" Y="-2.710659869897" />
                  <Point X="27.094524729954" Y="4.329576029324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.510427776489" Y="-2.661518796256" />
                  <Point X="27.002308462454" Y="4.372702731862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.416249745255" Y="-2.64644662216" />
                  <Point X="26.91009219406" Y="4.415829421611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.320446965807" Y="-2.65460943001" />
                  <Point X="26.817586909445" Y="4.454822986834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.224598218464" Y="-2.663429609403" />
                  <Point X="26.724710544083" Y="4.488509850142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.128283794921" Y="-2.678909268714" />
                  <Point X="26.631834178721" Y="4.522196713451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.029154835711" Y="-2.734638663259" />
                  <Point X="26.538957813358" Y="4.555883576759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.928044121755" Y="-2.818708471026" />
                  <Point X="26.446018730498" Y="4.58867353805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.758914251777" Y="-3.875497528133" />
                  <Point X="25.772002663578" Y="-3.688324519144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.826060313129" Y="-2.915264114285" />
                  <Point X="26.352367812189" Y="4.611283778178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.683391555259" Y="-3.593641638352" />
                  <Point X="26.258716893881" Y="4.633894018306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.602573555224" Y="-3.387512116905" />
                  <Point X="26.165065975573" Y="4.656504258433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.516058051683" Y="-3.262860691581" />
                  <Point X="26.071415057264" Y="4.679114498561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.428930119398" Y="-3.146967405338" />
                  <Point X="25.977764138956" Y="4.701724738689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.337832908177" Y="-3.087837452442" />
                  <Point X="25.88411323106" Y="4.724335127711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.244669758697" Y="-3.058251793094" />
                  <Point X="25.790107256204" Y="4.74186782265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.151460658449" Y="-3.029323260339" />
                  <Point X="25.695567611294" Y="4.751768680251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.058117475607" Y="-3.00231219801" />
                  <Point X="25.601027966383" Y="4.761669537851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.962978645095" Y="-3.000980093731" />
                  <Point X="25.506488321473" Y="4.771570395451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.865873736387" Y="-3.027764217556" />
                  <Point X="25.411948676563" Y="4.781471253051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.768529116674" Y="-3.057976368473" />
                  <Point X="25.295342347958" Y="4.47580383174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.671114036334" Y="-3.089196153296" />
                  <Point X="25.177958842199" Y="4.159022259333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.570950135632" Y="-3.159725900719" />
                  <Point X="25.078441160906" Y="4.097733880009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.46539722158" Y="-3.307322129513" />
                  <Point X="24.982422002601" Y="4.086476710324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.357610611132" Y="-3.48686170497" />
                  <Point X="24.889280953502" Y="4.11637841985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.22877674404" Y="-3.967391073324" />
                  <Point X="24.800233197491" Y="4.204816947721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.099915832246" Y="-4.448307198937" />
                  <Point X="24.722506187035" Y="4.455149679539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.982842910563" Y="-4.760647212138" />
                  <Point X="24.646983318897" Y="4.737003115047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.889114899918" Y="-4.739139443793" />
                  <Point X="24.554277923027" Y="4.773134955998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.827907188708" Y="-4.252569726646" />
                  <Point X="24.458260148792" Y="4.761897579436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.745673670986" Y="-4.066683051221" />
                  <Point X="24.362242374556" Y="4.750660202873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.656013120291" Y="-3.987007895614" />
                  <Point X="24.266224600321" Y="4.739422826311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.566283727696" Y="-3.908317225045" />
                  <Point X="24.170206825704" Y="4.728185444295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.476155623623" Y="-3.835328394252" />
                  <Point X="24.074189043845" Y="4.716947958706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.383423054798" Y="-3.799585144663" />
                  <Point X="23.977115185937" Y="4.690607881997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.288770979289" Y="-3.791292119531" />
                  <Point X="23.879978853744" Y="4.663374381393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.193973471987" Y="-3.785078865935" />
                  <Point X="23.782842521551" Y="4.63614088079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.099175964685" Y="-3.778865612339" />
                  <Point X="23.685706189358" Y="4.608907380187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.003879826578" Y="-3.779783111469" />
                  <Point X="23.588569857165" Y="4.581673879584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.90619179402" Y="-3.814906294871" />
                  <Point X="23.465021327637" Y="4.176728359779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.806305563503" Y="-3.881465173644" />
                  <Point X="23.363761045025" Y="4.09051962058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.706405861054" Y="-3.948216710024" />
                  <Point X="23.265677046045" Y="4.049733853535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.605873592404" Y="-4.024014364528" />
                  <Point X="23.168989632261" Y="4.028920185278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.501376378394" Y="-4.156513379345" />
                  <Point X="22.593688070218" Y="-2.836394682984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.62166611189" Y="-2.436290046509" />
                  <Point X="23.075066232789" Y="4.047633763218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.409506714934" Y="-4.108430008122" />
                  <Point X="22.485332368478" Y="-3.024072643084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.531046699875" Y="-2.370327246631" />
                  <Point X="22.983052718273" Y="4.0936599685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.318226865038" Y="-4.051911909959" />
                  <Point X="22.376976711866" Y="-3.211749957833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.437344616013" Y="-2.348448708006" />
                  <Point X="22.892432165885" Y="4.159606460292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.227201820123" Y="-3.991749930792" />
                  <Point X="22.268621055254" Y="-3.399427272582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.340799591337" Y="-2.36722611715" />
                  <Point X="22.804692324598" Y="4.266749040125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.136835306544" Y="-3.922170514779" />
                  <Point X="22.160265398641" Y="-3.587104587331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.241864821532" Y="-2.420178473827" />
                  <Point X="22.705611064817" Y="4.211701779186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.046468785915" Y="-3.852591199586" />
                  <Point X="22.051909742029" Y="-3.77478190208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.142626364949" Y="-2.477473753774" />
                  <Point X="22.606529805035" Y="4.156654518248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.043387908365" Y="-2.53476903372" />
                  <Point X="22.507448545254" Y="4.101607257309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.944149451781" Y="-2.592064313667" />
                  <Point X="22.021465763613" Y="-1.486389541953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.036488856727" Y="-1.271549301199" />
                  <Point X="22.408367304926" Y="4.046560274563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.844910995197" Y="-2.649359593614" />
                  <Point X="21.918314876188" Y="-1.599635189621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.949282189666" Y="-1.156781974701" />
                  <Point X="22.235572675381" Y="2.937362713979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.246676413279" Y="3.096153563862" />
                  <Point X="22.308625557688" Y="3.982067602949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.745672538614" Y="-2.70665487356" />
                  <Point X="21.817683304568" Y="-1.676852942754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.857257612349" Y="-1.110913974836" />
                  <Point X="22.130937746674" Y="2.802894287234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.166325862591" Y="3.308967922423" />
                  <Point X="22.207998759696" Y="3.904918115868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.64643408203" Y="-2.763950153507" />
                  <Point X="21.717051744304" Y="-1.754070533488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.762799028496" Y="-1.099853890114" />
                  <Point X="22.031095741873" Y="2.736967865667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.081381964031" Y="3.456094346057" />
                  <Point X="22.107371961704" Y="3.827768628787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.547195625446" Y="-2.821245433453" />
                  <Point X="21.616420212018" Y="-1.83128772413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.666724232515" Y="-1.111906715626" />
                  <Point X="21.934923912369" Y="2.723527396123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.996438065471" Y="3.603220769691" />
                  <Point X="22.006745163711" Y="3.750619141707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.447957168862" Y="-2.8785407134" />
                  <Point X="21.515788679731" Y="-1.908504914773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.570607396578" Y="-1.124560740525" />
                  <Point X="21.633696966237" Y="-0.222337860554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.658206567871" Y="0.128165772494" />
                  <Point X="21.840284179492" Y="2.731996929106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.348718712278" Y="-2.935835993346" />
                  <Point X="21.415157147445" Y="-1.985722105415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.474490560641" Y="-1.137214765424" />
                  <Point X="21.533342101311" Y="-0.295598523602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.568922822887" Y="0.213229500828" />
                  <Point X="21.747132416437" Y="2.761745421922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.249480255695" Y="-2.993131273293" />
                  <Point X="21.314525615158" Y="-2.062939296057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.378373724704" Y="-1.149868790323" />
                  <Point X="21.435905593289" Y="-0.32712473856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.476519583428" Y="0.25368237977" />
                  <Point X="21.55835963891" Y="1.42404969965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.569654586393" Y="1.585574973985" />
                  <Point X="21.655589887259" Y="2.814507031351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.157105440244" Y="-2.952271912083" />
                  <Point X="21.213894082872" Y="-2.1401564867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.282256888767" Y="-1.162522815222" />
                  <Point X="21.338855197864" Y="-0.353129286122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.383039131563" Y="0.278730403611" />
                  <Point X="21.452158514811" Y="1.267183635318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.485333935824" Y="1.741614259143" />
                  <Point X="21.564053444986" Y="2.867355687568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.06988630939" Y="-2.837682826136" />
                  <Point X="21.113262550585" Y="-2.217373677342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.186140052829" Y="-1.175176840121" />
                  <Point X="21.24180480189" Y="-0.379133841538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.289558679698" Y="0.303778427452" />
                  <Point X="21.353881060006" Y="1.223631321082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.395323377908" Y="1.816284078301" />
                  <Point X="21.472516997816" Y="2.920204273754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.982969740331" Y="-2.718766904947" />
                  <Point X="21.012631018298" Y="-2.294590867984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.090023216892" Y="-1.18783086502" />
                  <Point X="21.144754403027" Y="-0.405138438267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.196078227833" Y="0.328826451294" />
                  <Point X="21.25731180378" Y="1.204507384619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.304941027266" Y="1.885637013752" />
                  <Point X="21.380980550646" Y="2.973052859939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.897732217583" Y="-2.575839502819" />
                  <Point X="20.911999486012" Y="-2.371808058626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.993906380955" Y="-1.200484889919" />
                  <Point X="21.047704004165" Y="-0.431143034997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.102597775968" Y="0.353874475135" />
                  <Point X="21.162701918701" Y="1.213403761006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.214558676623" Y="1.954989949203" />
                  <Point X="21.289444103476" Y="3.025901446125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.897789545018" Y="-1.213138914817" />
                  <Point X="20.950653605302" Y="-0.457147631726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.009117324103" Y="0.378922498977" />
                  <Point X="21.068338654456" Y="1.225826979633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.12417632598" Y="2.024342884654" />
                  <Point X="21.187065926784" Y="2.923706076769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.801672709081" Y="-1.225792939716" />
                  <Point X="20.853603206439" Y="-0.483152228455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.915636872238" Y="0.403970522818" />
                  <Point X="20.973975387286" Y="1.23825015643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.033793975338" Y="2.093695820105" />
                  <Point X="21.082429082721" Y="2.789210259153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.705555873144" Y="-1.238446964615" />
                  <Point X="20.756552807576" Y="-0.509156825185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.822156420373" Y="0.42901854666" />
                  <Point X="20.879612119588" Y="1.250673325675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.943411624695" Y="2.163048755556" />
                  <Point X="20.977114976339" Y="2.64502913915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.609439037207" Y="-1.251100989514" />
                  <Point X="20.659502408713" Y="-0.535161421914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.728675968508" Y="0.454066570501" />
                  <Point X="20.785248851889" Y="1.26309649492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.853029274052" Y="2.232401691007" />
                  <Point X="20.868921254134" Y="2.45966759432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.513322201269" Y="-1.263755014413" />
                  <Point X="20.56245200985" Y="-0.561166018643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.635195516643" Y="0.479114594343" />
                  <Point X="20.690885584191" Y="1.275519664165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.417205365332" Y="-1.276409039312" />
                  <Point X="20.465401610987" Y="-0.587170615373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.541715064778" Y="0.504162618184" />
                  <Point X="20.596522316493" Y="1.28794283341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.325085718699" Y="-1.231900594018" />
                  <Point X="20.368351212124" Y="-0.613175212102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.448234612913" Y="0.529210642025" />
                  <Point X="20.502159048795" Y="1.300366002655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.251609742684" Y="-0.9207752374" />
                  <Point X="20.271300813262" Y="-0.639179808831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.354754161048" Y="0.554258665867" />
                  <Point X="20.407795781097" Y="1.3127891719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.261273709183" Y="0.579306689708" />
                  <Point X="20.294347994611" Y="1.052291007297" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.53482421875" Y="-3.773291259766" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521543945312" />
                  <Point X="25.438455078125" Y="-3.484276123047" />
                  <Point X="25.300591796875" Y="-3.285643554688" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.234310546875" Y="-3.253977294922" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.951310546875" Y="-3.200188964844" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285644042969" />
                  <Point X="24.68585546875" Y="-3.322911621094" />
                  <Point X="24.547994140625" Y="-3.521544433594" />
                  <Point X="24.5407734375" Y="-3.537112060547" />
                  <Point X="24.2034765625" Y="-4.795922851563" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.1506640625" Y="-4.986767578125" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.8545" Y="-4.926421386719" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.683232421875" Y="-4.608903320312" />
                  <Point X="23.6908515625" Y="-4.551039550781" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.680755859375" Y="-4.486685058594" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.5768671875" Y="-4.170311523438" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.3018515625" Y="-3.982557128906" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.9693671875" Y="-4.001021972656" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.550900390625" Y="-4.404081054688" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.511951171875" Y="-4.395333984375" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="22.0815" Y="-4.119359375" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.376361328125" Y="-2.832819335938" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.49975" Y="-2.613671386719" />
                  <Point X="22.49560546875" Y="-2.582190185547" />
                  <Point X="22.48328125" Y="-2.566024169922" />
                  <Point X="22.468673828125" Y="-2.551416259766" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.3226171875" Y="-3.170298828125" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.128865234375" Y="-3.228877685547" />
                  <Point X="20.838302734375" Y="-2.847135742188" />
                  <Point X="20.79337109375" Y="-2.771794677734" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.63044140625" Y="-1.581039672852" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.8474609375" Y="-1.411088012695" />
                  <Point X="21.8553984375" Y="-1.391312133789" />
                  <Point X="21.861884765625" Y="-1.366264282227" />
                  <Point X="21.863349609375" Y="-1.350049682617" />
                  <Point X="21.85134375" Y="-1.321068115234" />
                  <Point X="21.834658203125" Y="-1.308176757812" />
                  <Point X="21.812359375" Y="-1.295052734375" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.40987890625" Y="-1.469013183594" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.186251953125" Y="-1.456100952148" />
                  <Point X="20.072607421875" Y="-1.011187866211" />
                  <Point X="20.06072265625" Y="-0.92808404541" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="21.208130859375" Y="-0.191454208374" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.46248828125" Y="-0.118382423401" />
                  <Point X="21.485857421875" Y="-0.102163330078" />
                  <Point X="21.497677734375" Y="-0.090645225525" />
                  <Point X="21.506564453125" Y="-0.07119808197" />
                  <Point X="21.514353515625" Y="-0.046099964142" />
                  <Point X="21.512892578125" Y="-0.011751200676" />
                  <Point X="21.505103515625" Y="0.013346917152" />
                  <Point X="21.497677734375" Y="0.028085149765" />
                  <Point X="21.48147265625" Y="0.042646335602" />
                  <Point X="21.458103515625" Y="0.058865276337" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.193177734375" Y="0.400850463867" />
                  <Point X="20.001814453125" Y="0.45212612915" />
                  <Point X="20.00911328125" Y="0.501456359863" />
                  <Point X="20.08235546875" Y="0.996414855957" />
                  <Point X="20.106283203125" Y="1.084719726562" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="21.07870703125" Y="1.41610144043" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.278" Y="1.398926391602" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.34846875" Y="1.426058105469" />
                  <Point X="21.3608828125" Y="1.443791259766" />
                  <Point X="21.3647734375" Y="1.453186157227" />
                  <Point X="21.38552734375" Y="1.503289916992" />
                  <Point X="21.389287109375" Y="1.524602905273" />
                  <Point X="21.3836875" Y="1.54550769043" />
                  <Point X="21.378986328125" Y="1.554538208008" />
                  <Point X="21.353943359375" Y="1.602642578125" />
                  <Point X="21.34003125" Y="1.619221923828" />
                  <Point X="20.623396484375" Y="2.169115966797" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.55539453125" Y="2.299432617188" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="20.903369140625" Y="2.868475830078" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.737646484375" Y="2.986524902344" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.875001953125" Y="2.919252441406" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404541016" />
                  <Point X="21.996341796875" Y="2.936997558594" />
                  <Point X="22.04747265625" Y="2.988127929688" />
                  <Point X="22.0591015625" Y="3.006384277344" />
                  <Point X="22.06192578125" Y="3.027845458984" />
                  <Point X="22.060744140625" Y="3.04135546875" />
                  <Point X="22.054443359375" Y="3.113389892578" />
                  <Point X="22.04793359375" Y="3.134032470703" />
                  <Point X="21.730369140625" Y="3.684067871094" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.751455078125" Y="3.794307373047" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.346951171875" Y="4.229792480469" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="23.00058203125" Y="4.328744628906" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.063796875" Y="4.265830078125" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.201859375" Y="4.228740234375" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744628906" />
                  <Point X="23.31391796875" Y="4.294488769531" />
                  <Point X="23.319017578125" Y="4.310662597656" />
                  <Point X="23.346197265625" Y="4.396865234375" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.31198046875" Y="4.692658691406" />
                  <Point X="23.31086328125" Y="4.701141113281" />
                  <Point X="23.390232421875" Y="4.723393554688" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.152923828125" Y="4.917459472656" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.92733203125" Y="4.424838867188" />
                  <Point X="24.957859375" Y="4.310903808594" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.217169921875" Y="4.918166992188" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.29993359375" Y="4.984241210938" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="25.960328125" Y="4.901393066406" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.573349609375" Y="4.745520996094" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="26.994052734375" Y="4.586314941406" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.399583984375" Y="4.389663085938" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="27.78994140625" Y="4.154860351562" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.365974609375" Y="2.739364746094" />
                  <Point X="27.22985546875" Y="2.50359765625" />
                  <Point X="27.2214609375" Y="2.478831298828" />
                  <Point X="27.2033828125" Y="2.411228515625" />
                  <Point X="27.2033671875" Y="2.381358154297" />
                  <Point X="27.210416015625" Y="2.322901123047" />
                  <Point X="27.218681640625" Y="2.300812011719" />
                  <Point X="27.22546875" Y="2.290810546875" />
                  <Point X="27.261640625" Y="2.237503417969" />
                  <Point X="27.28494140625" Y="2.217416748047" />
                  <Point X="27.33825" Y="2.181245849609" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.3713046875" Y="2.171657714844" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.46134765625" Y="2.169338134766" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.797650390625" Y="2.917925048828" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.005103515625" Y="3.016344970703" />
                  <Point X="29.20259765625" Y="2.741874511719" />
                  <Point X="29.2346015625" Y="2.688985351562" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.46783203125" Y="1.730599243164" />
                  <Point X="28.2886171875" Y="1.593082641602" />
                  <Point X="28.2702421875" Y="1.571923950195" />
                  <Point X="28.22158984375" Y="1.508451416016" />
                  <Point X="28.209720703125" Y="1.479342041016" />
                  <Point X="28.191595703125" Y="1.414536499023" />
                  <Point X="28.19078125" Y="1.390959716797" />
                  <Point X="28.193572265625" Y="1.3774375" />
                  <Point X="28.20844921875" Y="1.305333007812" />
                  <Point X="28.22323828125" Y="1.276415527344" />
                  <Point X="28.263703125" Y="1.214909912109" />
                  <Point X="28.280947265625" Y="1.19882019043" />
                  <Point X="28.291951171875" Y="1.192626831055" />
                  <Point X="28.35058984375" Y="1.159617675781" />
                  <Point X="28.38344140625" Y="1.151653564453" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.66950390625" Y="1.298325683594" />
                  <Point X="29.848974609375" Y="1.321953125" />
                  <Point X="29.8543671875" Y="1.299801269531" />
                  <Point X="29.939193359375" Y="0.951366333008" />
                  <Point X="29.94927734375" Y="0.886598754883" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="28.946552734375" Y="0.29285949707" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.71447265625" Y="0.22437171936" />
                  <Point X="28.636578125" Y="0.179346832275" />
                  <Point X="28.61349609375" Y="0.155752685547" />
                  <Point X="28.566759765625" Y="0.096198562622" />
                  <Point X="28.556986328125" Y="0.074734649658" />
                  <Point X="28.5540625" Y="0.059472068787" />
                  <Point X="28.538484375" Y="-0.021875907898" />
                  <Point X="28.54140625" Y="-0.055947822571" />
                  <Point X="28.556986328125" Y="-0.137295791626" />
                  <Point X="28.566759765625" Y="-0.158758636475" />
                  <Point X="28.575529296875" Y="-0.169932342529" />
                  <Point X="28.622265625" Y="-0.229486465454" />
                  <Point X="28.651193359375" Y="-0.250354537964" />
                  <Point X="28.729087890625" Y="-0.295379272461" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.835845703125" Y="-0.59370513916" />
                  <Point X="29.998068359375" Y="-0.637172607422" />
                  <Point X="29.99579296875" Y="-0.652271240234" />
                  <Point X="29.948431640625" Y="-0.966412902832" />
                  <Point X="29.935509765625" Y="-1.023030456543" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.650169921875" Y="-1.128986083984" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.366154296875" Y="-1.104575683594" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.1854453125" Y="-1.154697387695" />
                  <Point X="28.168107421875" Y="-1.175548950195" />
                  <Point X="28.075701171875" Y="-1.286685424805" />
                  <Point X="28.064359375" Y="-1.314070556641" />
                  <Point X="28.061875" Y="-1.34107421875" />
                  <Point X="28.048630859375" Y="-1.485000854492" />
                  <Point X="28.056361328125" Y="-1.516622070312" />
                  <Point X="28.072234375" Y="-1.541312988281" />
                  <Point X="28.156841796875" Y="-1.672912719727" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="29.184330078125" Y="-2.465043701172" />
                  <Point X="29.339076171875" Y="-2.583784179688" />
                  <Point X="29.33763671875" Y="-2.586112060547" />
                  <Point X="29.204134765625" Y="-2.802138427734" />
                  <Point X="29.17740625" Y="-2.840116210938" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="27.96559375" Y="-2.381694091797" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.703205078125" Y="-2.247147460938" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.460716796875" Y="-2.234170654297" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334683349609" />
                  <Point X="27.27367578125" Y="-2.363043701172" />
                  <Point X="27.19412109375" Y="-2.514201416016" />
                  <Point X="27.1891640625" Y="-2.546374755859" />
                  <Point X="27.195330078125" Y="-2.580512695312" />
                  <Point X="27.22819140625" Y="-2.762465087891" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.886890625" Y="-3.909256347656" />
                  <Point X="27.98667578125" Y="-4.082087646484" />
                  <Point X="27.83529296875" Y="-4.190215820312" />
                  <Point X="27.80541796875" Y="-4.209553222656" />
                  <Point X="27.679775390625" Y="-4.29087890625" />
                  <Point X="26.845060546875" Y="-3.203055175781" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.636880859375" Y="-2.958821044922" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.388982421875" Y="-2.83910546875" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.136900390625" Y="-2.892150878906" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045986328125" />
                  <Point X="25.959955078125" Y="-3.085100341797" />
                  <Point X="25.914642578125" Y="-3.29357421875" />
                  <Point X="25.9139296875" Y="-3.310719970703" />
                  <Point X="26.098927734375" Y="-4.715917480469" />
                  <Point X="26.127642578125" Y="-4.934028320312" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.96673828125" Y="-4.968262207031" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#132" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.030528837156" Y="4.469059650747" Z="0.4" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.4" />
                  <Point X="-0.859045964258" Y="4.998400702517" Z="0.4" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.4" />
                  <Point X="-1.629421755161" Y="4.802817165748" Z="0.4" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.4" />
                  <Point X="-1.743749236756" Y="4.717412958834" Z="0.4" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.4" />
                  <Point X="-1.73482565947" Y="4.356977565837" Z="0.4" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.4" />
                  <Point X="-1.823431524828" Y="4.306214334649" Z="0.4" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.4" />
                  <Point X="-1.919272926231" Y="4.341460754392" Z="0.4" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.4" />
                  <Point X="-1.965907245416" Y="4.390462892805" Z="0.4" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.4" />
                  <Point X="-2.683489695037" Y="4.304779848199" Z="0.4" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.4" />
                  <Point X="-3.285123066913" Y="3.865267279419" Z="0.4" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.4" />
                  <Point X="-3.319087842277" Y="3.690348398154" Z="0.4" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.4" />
                  <Point X="-2.995222443605" Y="3.068278669706" Z="0.4" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.4" />
                  <Point X="-3.045170430908" Y="3.003633136978" Z="0.4" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.4" />
                  <Point X="-3.126797795901" Y="3.000342255568" Z="0.4" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.4" />
                  <Point X="-3.243510950143" Y="3.061106106468" Z="0.4" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.4" />
                  <Point X="-4.1422512232" Y="2.93045837439" Z="0.4" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.4" />
                  <Point X="-4.49394420981" Y="2.355917798171" Z="0.4" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.4" />
                  <Point X="-4.413198389528" Y="2.16072827957" Z="0.4" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.4" />
                  <Point X="-3.671520745474" Y="1.562729755556" Z="0.4" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.4" />
                  <Point X="-3.687576091951" Y="1.50360061398" Z="0.4" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.4" />
                  <Point X="-3.743191945802" Y="1.477892230536" Z="0.4" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.4" />
                  <Point X="-3.920923908327" Y="1.496953825991" Z="0.4" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.4" />
                  <Point X="-4.94813293892" Y="1.12907711649" Z="0.4" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.4" />
                  <Point X="-5.046504362885" Y="0.540055446174" Z="0.4" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.4" />
                  <Point X="-4.825921179415" Y="0.38383415917" Z="0.4" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.4" />
                  <Point X="-3.553192267622" Y="0.032849980732" Z="0.4" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.4" />
                  <Point X="-3.541018236225" Y="0.004708911587" Z="0.4" />
                  <Point X="-3.539556741714" Y="0" Z="0.4" />
                  <Point X="-3.54734636491" Y="-0.025098039474" Z="0.4" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.4" />
                  <Point X="-3.572176327501" Y="-0.046026002402" Z="0.4" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.4" />
                  <Point X="-3.810966609357" Y="-0.111877897586" Z="0.4" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.4" />
                  <Point X="-4.994931317408" Y="-0.903883110755" Z="0.4" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.4" />
                  <Point X="-4.868350479233" Y="-1.437195074712" Z="0.4" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.4" />
                  <Point X="-4.589751741561" Y="-1.487305245705" Z="0.4" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.4" />
                  <Point X="-3.196860032493" Y="-1.319987443863" Z="0.4" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.4" />
                  <Point X="-3.199163643371" Y="-1.347497671794" Z="0.4" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.4" />
                  <Point X="-3.40615329321" Y="-1.510091949363" Z="0.4" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.4" />
                  <Point X="-4.255729323935" Y="-2.766124222808" Z="0.4" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.4" />
                  <Point X="-3.917076378774" Y="-3.227880864227" Z="0.4" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.4" />
                  <Point X="-3.658539159899" Y="-3.182319953662" Z="0.4" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.4" />
                  <Point X="-2.558231696556" Y="-2.570098772576" Z="0.4" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.4" />
                  <Point X="-2.673097059506" Y="-2.776539222593" Z="0.4" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.4" />
                  <Point X="-2.955160636368" Y="-4.127696763658" Z="0.4" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.4" />
                  <Point X="-2.520527631943" Y="-4.406564695894" Z="0.4" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.4" />
                  <Point X="-2.415588695114" Y="-4.403239213951" Z="0.4" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.4" />
                  <Point X="-2.009009591475" Y="-4.011315089263" Z="0.4" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.4" />
                  <Point X="-1.70757584091" Y="-4.001170266431" Z="0.4" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.4" />
                  <Point X="-1.462256605416" Y="-4.176623330081" Z="0.4" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.4" />
                  <Point X="-1.374440680063" Y="-4.46516012784" Z="0.4" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.4" />
                  <Point X="-1.372496427572" Y="-4.571095842946" Z="0.4" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.4" />
                  <Point X="-1.164116177721" Y="-4.943564378934" Z="0.4" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.4" />
                  <Point X="-0.865020114528" Y="-5.004571754218" Z="0.4" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.4" />
                  <Point X="-0.7543841194" Y="-4.777583976523" Z="0.4" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.4" />
                  <Point X="-0.279224833474" Y="-3.320140297226" Z="0.4" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.4" />
                  <Point X="-0.040025747755" Y="-3.216661883454" Z="0.4" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.4" />
                  <Point X="0.213333331606" Y="-3.270450161834" Z="0.4" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.4" />
                  <Point X="0.391221025958" Y="-3.481505458542" Z="0.4" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.4" />
                  <Point X="0.480370741001" Y="-3.754952053292" Z="0.4" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.4" />
                  <Point X="0.969519659796" Y="-4.986177060929" Z="0.4" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.4" />
                  <Point X="1.148767733789" Y="-4.947955403336" Z="0.4" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.4" />
                  <Point X="1.14234356295" Y="-4.678111169199" Z="0.4" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.4" />
                  <Point X="1.002658426557" Y="-3.064439993946" Z="0.4" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.4" />
                  <Point X="1.162709863562" Y="-2.899317284961" Z="0.4" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.4" />
                  <Point X="1.387407344699" Y="-2.857615085432" Z="0.4" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.4" />
                  <Point X="1.603684588297" Y="-2.969599015139" Z="0.4" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.4" />
                  <Point X="1.799235186664" Y="-3.202213030684" Z="0.4" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.4" />
                  <Point X="2.82643156981" Y="-4.220247442195" Z="0.4" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.4" />
                  <Point X="3.016616562522" Y="-4.086469021107" Z="0.4" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.4" />
                  <Point X="2.924034301514" Y="-3.852976421443" Z="0.4" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.4" />
                  <Point X="2.238376418643" Y="-2.540346217782" Z="0.4" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.4" />
                  <Point X="2.311764361318" Y="-2.355050505131" Z="0.4" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.4" />
                  <Point X="2.477847873242" Y="-2.247136948649" Z="0.4" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.4" />
                  <Point X="2.688160585731" Y="-2.265071590848" Z="0.4" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.4" />
                  <Point X="2.934437175664" Y="-2.39371519876" Z="0.4" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.4" />
                  <Point X="4.212137738877" Y="-2.837613507429" Z="0.4" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.4" />
                  <Point X="4.374012850382" Y="-2.58111729211" Z="0.4" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.4" />
                  <Point X="4.208610708218" Y="-2.394095976762" Z="0.4" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.4" />
                  <Point X="3.108136419019" Y="-1.482993325571" Z="0.4" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.4" />
                  <Point X="3.105506592394" Y="-1.314375849207" Z="0.4" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.4" />
                  <Point X="3.200398125633" Y="-1.176235696724" Z="0.4" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.4" />
                  <Point X="3.370616269366" Y="-1.122154921547" Z="0.4" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.4" />
                  <Point X="3.637487871427" Y="-1.147278468694" Z="0.4" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.4" />
                  <Point X="4.978099850241" Y="-1.002874077761" Z="0.4" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.4" />
                  <Point X="5.039077108073" Y="-0.628445224462" Z="0.4" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.4" />
                  <Point X="4.842630844309" Y="-0.514128748239" Z="0.4" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.4" />
                  <Point X="3.670057471473" Y="-0.175785798747" Z="0.4" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.4" />
                  <Point X="3.608705577778" Y="-0.107784111933" Z="0.4" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.4" />
                  <Point X="3.584357678071" Y="-0.015262562411" Z="0.4" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.4" />
                  <Point X="3.597013772353" Y="0.081347968827" Z="0.4" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.4" />
                  <Point X="3.646673860622" Y="0.156164626642" Z="0.4" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.4" />
                  <Point X="3.73333794288" Y="0.212363045495" Z="0.4" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.4" />
                  <Point X="3.953336972998" Y="0.275843184529" Z="0.4" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.4" />
                  <Point X="4.992524140223" Y="0.925570785492" Z="0.4" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.4" />
                  <Point X="4.896793203092" Y="1.342908179594" Z="0.4" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.4" />
                  <Point X="4.656822448593" Y="1.379177854064" Z="0.4" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.4" />
                  <Point X="3.38383617128" Y="1.232502593841" Z="0.4" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.4" />
                  <Point X="3.310320627854" Y="1.267477815238" Z="0.4" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.4" />
                  <Point X="3.258853143836" Y="1.335176672761" Z="0.4" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.4" />
                  <Point X="3.236383381685" Y="1.418820907933" Z="0.4" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.4" />
                  <Point X="3.251715625027" Y="1.497154826835" Z="0.4" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.4" />
                  <Point X="3.303769712976" Y="1.572786323576" Z="0.4" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.4" />
                  <Point X="3.492113198361" Y="1.722211632384" Z="0.4" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.4" />
                  <Point X="4.271222067695" Y="2.746151598906" Z="0.4" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.4" />
                  <Point X="4.039532322732" Y="3.076828126456" Z="0.4" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.4" />
                  <Point X="3.766493983716" Y="2.992506349036" Z="0.4" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.4" />
                  <Point X="2.442275305261" Y="2.248920947927" Z="0.4" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.4" />
                  <Point X="2.37113455072" Y="2.252578018927" Z="0.4" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.4" />
                  <Point X="2.306859603296" Y="2.290071625797" Z="0.4" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.4" />
                  <Point X="2.260686974438" Y="2.350165257086" Z="0.4" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.4" />
                  <Point X="2.246851683832" Y="2.418623893113" Z="0.4" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.4" />
                  <Point X="2.26360699473" Y="2.497194270621" Z="0.4" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.4" />
                  <Point X="2.403119003823" Y="2.745645098239" Z="0.4" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.4" />
                  <Point X="2.81276079731" Y="4.226887459588" Z="0.4" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.4" />
                  <Point X="2.418597040398" Y="4.464145982215" Z="0.4" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.4" />
                  <Point X="2.009076708665" Y="4.662886855183" Z="0.4" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.4" />
                  <Point X="1.584241568022" Y="4.823804817821" Z="0.4" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.4" />
                  <Point X="0.965906055851" Y="4.981276723544" Z="0.4" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.4" />
                  <Point X="0.298983015673" Y="5.065249392201" Z="0.4" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.4" />
                  <Point X="0.162715724397" Y="4.962387855204" Z="0.4" />
                  <Point X="0" Y="4.355124473572" Z="0.4" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>