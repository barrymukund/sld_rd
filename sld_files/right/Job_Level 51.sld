<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#217" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3620" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999524414062" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.557724609375" Y="-3.497142822266" />
                  <Point X="25.5423671875" Y="-3.467379882812" />
                  <Point X="25.381134765625" Y="-3.235076416016" />
                  <Point X="25.378638671875" Y="-3.231479492188" />
                  <Point X="25.3567578125" Y="-3.209026123047" />
                  <Point X="25.3305" Y="-3.189779052734" />
                  <Point X="25.30249609375" Y="-3.175668945312" />
                  <Point X="25.053" Y="-3.098234619141" />
                  <Point X="25.04913671875" Y="-3.097035644531" />
                  <Point X="25.02097265625" Y="-3.092766357422" />
                  <Point X="24.991330078125" Y="-3.092767333984" />
                  <Point X="24.963173828125" Y="-3.097036621094" />
                  <Point X="24.713677734375" Y="-3.174470703125" />
                  <Point X="24.709814453125" Y="-3.175669921875" />
                  <Point X="24.68180859375" Y="-3.189782470703" />
                  <Point X="24.655552734375" Y="-3.209029785156" />
                  <Point X="24.633673828125" Y="-3.231480957031" />
                  <Point X="24.472443359375" Y="-3.463784179688" />
                  <Point X="24.465755859375" Y="-3.474991455078" />
                  <Point X="24.456041015625" Y="-3.494152587891" />
                  <Point X="24.449009765625" Y="-3.512526611328" />
                  <Point X="24.44290234375" Y="-3.535322021484" />
                  <Point X="24.083416015625" Y="-4.876941894531" />
                  <Point X="23.92095703125" Y="-4.845408203125" />
                  <Point X="23.920669921875" Y="-4.845352539063" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223632812" />
                  <Point X="23.72388671875" Y="-4.216571777344" />
                  <Point X="23.72295703125" Y="-4.211902832031" />
                  <Point X="23.7120546875" Y="-4.182947753906" />
                  <Point X="23.695984375" Y="-4.155120117188" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.44665234375" Y="-3.929758789062" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295898438" />
                  <Point X="23.387115234375" Y="-3.897994628906" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.052103515625" Y="-3.870984130859" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894801513672" />
                  <Point X="22.70330859375" Y="-4.064541259766" />
                  <Point X="22.70329296875" Y="-4.064552246094" />
                  <Point X="22.684345703125" Y="-4.079621582031" />
                  <Point X="22.669037109375" Y="-4.095219238281" />
                  <Point X="22.66146875" Y="-4.103930664062" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.19871484375" Y="-4.089648925781" />
                  <Point X="22.198291015625" Y="-4.089386962891" />
                  <Point X="21.895279296875" Y="-3.856076904297" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647654541016" />
                  <Point X="22.593412109375" Y="-2.616128173828" />
                  <Point X="22.59442578125" Y="-2.585194091797" />
                  <Point X="22.58544140625" Y="-2.555575927734" />
                  <Point X="22.571224609375" Y="-2.526747070312" />
                  <Point X="22.553197265625" Y="-2.501589355469" />
                  <Point X="22.536154296875" Y="-2.484546386719" />
                  <Point X="22.535955078125" Y="-2.484346923828" />
                  <Point X="22.510875" Y="-2.466336669922" />
                  <Point X="22.48202734375" Y="-2.452060546875" />
                  <Point X="22.45237890625" Y="-2.443029052734" />
                  <Point X="22.421400390625" Y="-2.444021728516" />
                  <Point X="22.389828125" Y="-2.450286621094" />
                  <Point X="22.3608203125" Y="-2.461195800781" />
                  <Point X="22.341158203125" Y="-2.472546875" />
                  <Point X="21.1819765625" Y="-3.141801025391" />
                  <Point X="20.917474609375" Y="-2.794298828125" />
                  <Point X="20.91713671875" Y="-2.793854003906" />
                  <Point X="20.693857421875" Y="-2.419450439453" />
                  <Point X="21.894044921875" Y="-1.498513305664" />
                  <Point X="21.915421875" Y="-1.475594848633" />
                  <Point X="21.93338671875" Y="-1.448465087891" />
                  <Point X="21.946142578125" Y="-1.419836425781" />
                  <Point X="21.95373046875" Y="-1.390542480469" />
                  <Point X="21.9538828125" Y="-1.389952148438" />
                  <Point X="21.956658203125" Y="-1.35958190918" />
                  <Point X="21.954435546875" Y="-1.327927490234" />
                  <Point X="21.94742578125" Y="-1.298199951172" />
                  <Point X="21.931345703125" Y="-1.272233032227" />
                  <Point X="21.91051953125" Y="-1.248290771484" />
                  <Point X="21.887029296875" Y="-1.228768066406" />
                  <Point X="21.860966796875" Y="-1.213428710938" />
                  <Point X="21.860578125" Y="-1.21319921875" />
                  <Point X="21.831236328125" Y="-1.201938354492" />
                  <Point X="21.79937109375" Y="-1.195469848633" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="21.743251953125" Y="-1.197651367188" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.1660546875" Y="-0.993169555664" />
                  <Point X="20.165921875" Y="-0.992652648926" />
                  <Point X="20.107576171875" Y="-0.584698669434" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.482505859375" Y="-0.214828353882" />
                  <Point X="21.512271484375" Y="-0.199469894409" />
                  <Point X="21.5396015625" Y="-0.180501403809" />
                  <Point X="21.54011328125" Y="-0.180145614624" />
                  <Point X="21.562427734375" Y="-0.158374160767" />
                  <Point X="21.581703125" Y="-0.132097732544" />
                  <Point X="21.595833984375" Y="-0.104064933777" />
                  <Point X="21.604943359375" Y="-0.074712463379" />
                  <Point X="21.605166015625" Y="-0.073988578796" />
                  <Point X="21.609365234375" Y="-0.046021652222" />
                  <Point X="21.609353515625" Y="-0.016422374725" />
                  <Point X="21.605083984375" Y="0.011698008537" />
                  <Point X="21.595974609375" Y="0.041050323486" />
                  <Point X="21.595814453125" Y="0.041563831329" />
                  <Point X="21.581763671875" Y="0.069431480408" />
                  <Point X="21.5625078125" Y="0.095728408813" />
                  <Point X="21.540025390625" Y="0.117647544861" />
                  <Point X="21.5126953125" Y="0.136616027832" />
                  <Point X="21.498927734375" Y="0.14455406189" />
                  <Point X="21.467126953125" Y="0.157847717285" />
                  <Point X="21.44450390625" Y="0.163910064697" />
                  <Point X="20.108185546875" Y="0.521975646973" />
                  <Point X="20.1754296875" Y="0.976414245605" />
                  <Point X="20.17551171875" Y="0.97696472168" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255017578125" Y="1.299341796875" />
                  <Point X="21.276578125" Y="1.301228271484" />
                  <Point X="21.296865234375" Y="1.305263793945" />
                  <Point X="21.35733984375" Y="1.324331542969" />
                  <Point X="21.35823828125" Y="1.324614868164" />
                  <Point X="21.377193359375" Y="1.332947998047" />
                  <Point X="21.3959453125" Y="1.34376940918" />
                  <Point X="21.4126328125" Y="1.356001342773" />
                  <Point X="21.42627734375" Y="1.371555908203" />
                  <Point X="21.438697265625" Y="1.389290649414" />
                  <Point X="21.448650390625" Y="1.407431274414" />
                  <Point X="21.472921875" Y="1.466028076172" />
                  <Point X="21.473318359375" Y="1.466989746094" />
                  <Point X="21.479109375" Y="1.486886230469" />
                  <Point X="21.4828515625" Y="1.508184204102" />
                  <Point X="21.484193359375" Y="1.528803466797" />
                  <Point X="21.481044921875" Y="1.549225097656" />
                  <Point X="21.4754453125" Y="1.570111450195" />
                  <Point X="21.467953125" Y="1.589375610352" />
                  <Point X="21.43866796875" Y="1.645634277344" />
                  <Point X="21.43807421875" Y="1.646768310547" />
                  <Point X="21.426640625" Y="1.663823852539" />
                  <Point X="21.41276171875" Y="1.68033972168" />
                  <Point X="21.3978671875" Y="1.694588378906" />
                  <Point X="21.384890625" Y="1.704546264648" />
                  <Point X="20.648140625" Y="2.269874511719" />
                  <Point X="20.918521484375" Y="2.733103515625" />
                  <Point X="20.91884375" Y="2.733654052734" />
                  <Point X="21.249494140625" Y="3.158661865234" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.812275390625" Y="2.836340576172" />
                  <Point X="21.832916015625" Y="2.829832275391" />
                  <Point X="21.853205078125" Y="2.825796630859" />
                  <Point X="21.937451171875" Y="2.818426025391" />
                  <Point X="21.93875390625" Y="2.818312011719" />
                  <Point X="21.959431640625" Y="2.818762939453" />
                  <Point X="21.980890625" Y="2.821587646484" />
                  <Point X="22.000982421875" Y="2.826503662109" />
                  <Point X="22.019533203125" Y="2.835651611328" />
                  <Point X="22.0377890625" Y="2.84728125" />
                  <Point X="22.053921875" Y="2.860229003906" />
                  <Point X="22.113720703125" Y="2.920026611328" />
                  <Point X="22.114646484375" Y="2.920952392578" />
                  <Point X="22.127595703125" Y="2.9370859375" />
                  <Point X="22.139224609375" Y="2.955340332031" />
                  <Point X="22.14837109375" Y="2.973889160156" />
                  <Point X="22.1532890625" Y="2.993978027344" />
                  <Point X="22.156115234375" Y="3.015437011719" />
                  <Point X="22.15656640625" Y="3.036122070312" />
                  <Point X="22.1491953125" Y="3.1203671875" />
                  <Point X="22.149083984375" Y="3.121639404297" />
                  <Point X="22.145056640625" Y="3.141898925781" />
                  <Point X="22.138544921875" Y="3.162572998047" />
                  <Point X="22.130201171875" Y="3.181541015625" />
                  <Point X="22.12444921875" Y="3.191501464844" />
                  <Point X="21.81666796875" Y="3.724595703125" />
                  <Point X="22.298826171875" Y="4.094260986328" />
                  <Point X="22.29938671875" Y="4.094690429687" />
                  <Point X="22.83296484375" Y="4.391133300781" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.098650390625" Y="4.140583984375" />
                  <Point X="23.100134765625" Y="4.139811523438" />
                  <Point X="23.119419921875" Y="4.132317382813" />
                  <Point X="23.1403203125" Y="4.12672265625" />
                  <Point X="23.160755859375" Y="4.123581542969" />
                  <Point X="23.181388671875" Y="4.124936035156" />
                  <Point X="23.202697265625" Y="4.128694824219" />
                  <Point X="23.222548828125" Y="4.134481933594" />
                  <Point X="23.320169921875" Y="4.174918457031" />
                  <Point X="23.32168359375" Y="4.175544921875" />
                  <Point X="23.339826171875" Y="4.185492675781" />
                  <Point X="23.357564453125" Y="4.197907226562" />
                  <Point X="23.373125" Y="4.211548828125" />
                  <Point X="23.38536328125" Y="4.228235839844" />
                  <Point X="23.396189453125" Y="4.246985839844" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.43630859375" Y="4.366736816406" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583984375" />
                  <Point X="23.44272265625" Y="4.410145507812" />
                  <Point X="23.44226953125" Y="4.430835449219" />
                  <Point X="23.441615234375" Y="4.435801757812" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="24.0496484375" Y="4.809606933594" />
                  <Point X="24.0503671875" Y="4.80980859375" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.878236328125" Y="4.256521484375" />
                  <Point X="24.890337890625" Y="4.236764160156" />
                  <Point X="24.906861328125" Y="4.2205234375" />
                  <Point X="24.932052734375" Y="4.201192871094" />
                  <Point X="24.959349609375" Y="4.186602539062" />
                  <Point X="24.98988671875" Y="4.181560546875" />
                  <Point X="25.02242578125" Y="4.181560546875" />
                  <Point X="25.052962890625" Y="4.186602539062" />
                  <Point X="25.080259765625" Y="4.201192871094" />
                  <Point X="25.105451171875" Y="4.2205234375" />
                  <Point X="25.1239296875" Y="4.239307617188" />
                  <Point X="25.13653515625" Y="4.2624453125" />
                  <Point X="25.146318359375" Y="4.288454589844" />
                  <Point X="25.1491640625" Y="4.2973125" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.84340625" Y="4.831805175781" />
                  <Point X="25.844041015625" Y="4.831738769531" />
                  <Point X="26.478890625" Y="4.678466796875" />
                  <Point X="26.481025390625" Y="4.677951171875" />
                  <Point X="26.89423046875" Y="4.528079101562" />
                  <Point X="26.8946484375" Y="4.527927246094" />
                  <Point X="27.29421875" Y="4.341061523438" />
                  <Point X="27.294578125" Y="4.340893066406" />
                  <Point X="27.68059765625" Y="4.115997558594" />
                  <Point X="27.6809765625" Y="4.115776855469" />
                  <Point X="27.94326171875" Y="3.929254394531" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.539934326172" />
                  <Point X="27.133078125" Y="2.516057128906" />
                  <Point X="27.111935546875" Y="2.436995117188" />
                  <Point X="27.10903515625" Y="2.420289550781" />
                  <Point X="27.107369140625" Y="2.400162109375" />
                  <Point X="27.107728515625" Y="2.380951904297" />
                  <Point X="27.1159609375" Y="2.3126796875" />
                  <Point X="27.116087890625" Y="2.31162109375" />
                  <Point X="27.121427734375" Y="2.289666992188" />
                  <Point X="27.129697265625" Y="2.267546386719" />
                  <Point X="27.1400703125" Y="2.247471191406" />
                  <Point X="27.182373046875" Y="2.185127929688" />
                  <Point X="27.193080078125" Y="2.172031494141" />
                  <Point X="27.221599609375" Y="2.145593505859" />
                  <Point X="27.28394140625" Y="2.103290771484" />
                  <Point X="27.284966796875" Y="2.102595214844" />
                  <Point X="27.305025390625" Y="2.092234130859" />
                  <Point X="27.327076171875" Y="2.083992675781" />
                  <Point X="27.348962890625" Y="2.078663818359" />
                  <Point X="27.417328125" Y="2.070419921875" />
                  <Point X="27.4344453125" Y="2.069910400391" />
                  <Point X="27.473203125" Y="2.074170410156" />
                  <Point X="27.552265625" Y="2.095312744141" />
                  <Point X="27.563037109375" Y="2.098894775391" />
                  <Point X="27.58853515625" Y="2.110145263672" />
                  <Point X="27.611291015625" Y="2.123283447266" />
                  <Point X="28.967328125" Y="2.90619140625" />
                  <Point X="29.123046875" Y="2.689776611328" />
                  <Point X="29.1232734375" Y="2.689462402344" />
                  <Point X="29.26219921875" Y="2.4598828125" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.2214296875" Y="1.660244628906" />
                  <Point X="28.20397265625" Y="1.641627075195" />
                  <Point X="28.147072265625" Y="1.567395141602" />
                  <Point X="28.138064453125" Y="1.553197875977" />
                  <Point X="28.12871484375" Y="1.535096557617" />
                  <Point X="28.121630859375" Y="1.517086181641" />
                  <Point X="28.100455078125" Y="1.441367675781" />
                  <Point X="28.100166015625" Y="1.440336791992" />
                  <Point X="28.096673828125" Y="1.417956420898" />
                  <Point X="28.09583984375" Y="1.394309082031" />
                  <Point X="28.0977421875" Y="1.371762451172" />
                  <Point X="28.115142578125" Y="1.287435302734" />
                  <Point X="28.119966796875" Y="1.271377685547" />
                  <Point X="28.127431640625" Y="1.252699462891" />
                  <Point X="28.13628125" Y="1.235741210938" />
                  <Point X="28.183599609375" Y="1.163818359375" />
                  <Point X="28.1843203125" Y="1.162724975586" />
                  <Point X="28.198880859375" Y="1.145465698242" />
                  <Point X="28.216130859375" Y="1.129366943359" />
                  <Point X="28.234345703125" Y="1.116034912109" />
                  <Point X="28.30292578125" Y="1.077430175781" />
                  <Point X="28.318455078125" Y="1.07043737793" />
                  <Point X="28.33748046875" Y="1.063849121094" />
                  <Point X="28.356119140625" Y="1.059438476562" />
                  <Point X="28.44884375" Y="1.047183837891" />
                  <Point X="28.46002734375" Y="1.046373291016" />
                  <Point X="28.488205078125" Y="1.046984619141" />
                  <Point X="28.5098203125" Y="1.049830566406" />
                  <Point X="29.77683984375" Y="1.21663659668" />
                  <Point X="29.845837890625" Y="0.933210449219" />
                  <Point X="29.845939453125" Y="0.932794250488" />
                  <Point X="29.8908671875" Y="0.644238586426" />
                  <Point X="28.716580078125" Y="0.329589874268" />
                  <Point X="28.7047890625" Y="0.325585662842" />
                  <Point X="28.681546875" Y="0.315068084717" />
                  <Point X="28.590447265625" Y="0.262410949707" />
                  <Point X="28.57680859375" Y="0.252839508057" />
                  <Point X="28.5610859375" Y="0.239603988647" />
                  <Point X="28.54753125" Y="0.225577194214" />
                  <Point X="28.49287109375" Y="0.155928024292" />
                  <Point X="28.49198828125" Y="0.154800674438" />
                  <Point X="28.48028125" Y="0.135536651611" />
                  <Point X="28.47051953125" Y="0.11408821106" />
                  <Point X="28.463681640625" Y="0.092605171204" />
                  <Point X="28.4454609375" Y="-0.002532188416" />
                  <Point X="28.443767578125" Y="-0.019138664246" />
                  <Point X="28.4434921875" Y="-0.039393573761" />
                  <Point X="28.4451796875" Y="-0.05855437851" />
                  <Point X="28.46339453125" Y="-0.153663345337" />
                  <Point X="28.463666015625" Y="-0.155082138062" />
                  <Point X="28.47051171875" Y="-0.176612686157" />
                  <Point X="28.480291015625" Y="-0.198104690552" />
                  <Point X="28.492025390625" Y="-0.217409698486" />
                  <Point X="28.546673828125" Y="-0.28704385376" />
                  <Point X="28.558216796875" Y="-0.299329589844" />
                  <Point X="28.57338671875" Y="-0.312843048096" />
                  <Point X="28.589037109375" Y="-0.324155670166" />
                  <Point X="28.68013671875" Y="-0.376812683105" />
                  <Point X="28.690029296875" Y="-0.381785797119" />
                  <Point X="28.716580078125" Y="-0.392150024414" />
                  <Point X="28.736404296875" Y="-0.397461791992" />
                  <Point X="29.89147265625" Y="-0.706961853027" />
                  <Point X="29.855078125" Y="-0.948363769531" />
                  <Point X="29.8550234375" Y="-0.948724060059" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="28.4243828125" Y="-1.003441040039" />
                  <Point X="28.408033203125" Y="-1.002710449219" />
                  <Point X="28.37465625" Y="-1.005509338379" />
                  <Point X="28.195859375" Y="-1.044371337891" />
                  <Point X="28.193091796875" Y="-1.044973022461" />
                  <Point X="28.16396875" Y="-1.056598999023" />
                  <Point X="28.13614453125" Y="-1.073490844727" />
                  <Point X="28.112396484375" Y="-1.093960449219" />
                  <Point X="28.004326171875" Y="-1.223936035156" />
                  <Point X="28.002658203125" Y="-1.225942016602" />
                  <Point X="27.987939453125" Y="-1.250319702148" />
                  <Point X="27.976591796875" Y="-1.277711669922" />
                  <Point X="27.969759765625" Y="-1.305365722656" />
                  <Point X="27.9542734375" Y="-1.473667602539" />
                  <Point X="27.95403125" Y="-1.476273803711" />
                  <Point X="27.95634765625" Y="-1.507548706055" />
                  <Point X="27.964078125" Y="-1.539177368164" />
                  <Point X="27.976451171875" Y="-1.567996337891" />
                  <Point X="28.0753984375" Y="-1.721903686523" />
                  <Point X="28.083859375" Y="-1.733139038086" />
                  <Point X="28.110630859375" Y="-1.760909545898" />
                  <Point X="28.12902734375" Y="-1.775025634766" />
                  <Point X="29.213123046875" Y="-2.606882568359" />
                  <Point X="29.124962890625" Y="-2.7495390625" />
                  <Point X="29.1248046875" Y="-2.749795166016" />
                  <Point X="29.028982421875" Y="-2.885944824219" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.786130859375" Y="-2.170011962891" />
                  <Point X="27.754224609375" Y="-2.159824951172" />
                  <Point X="27.5414296875" Y="-2.121394287109" />
                  <Point X="27.538134765625" Y="-2.120799316406" />
                  <Point X="27.50678125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353271484" />
                  <Point X="27.444833984375" Y="-2.135176757812" />
                  <Point X="27.268052734375" Y="-2.228215087891" />
                  <Point X="27.265283203125" Y="-2.229673828125" />
                  <Point X="27.242375" Y="-2.246557128906" />
                  <Point X="27.221421875" Y="-2.267513183594" />
                  <Point X="27.204533203125" Y="-2.290438964844" />
                  <Point X="27.111494140625" Y="-2.467219726562" />
                  <Point X="27.11005859375" Y="-2.469947265625" />
                  <Point X="27.100236328125" Y="-2.499713134766" />
                  <Point X="27.0952734375" Y="-2.531895996094" />
                  <Point X="27.09567578125" Y="-2.563259033203" />
                  <Point X="27.134107421875" Y="-2.776054443359" />
                  <Point X="27.137509765625" Y="-2.789331054688" />
                  <Point X="27.1440078125" Y="-2.808739501953" />
                  <Point X="27.1518203125" Y="-2.826075195312" />
                  <Point X="27.163640625" Y="-2.846550537109" />
                  <Point X="27.861287109375" Y="-4.054905273438" />
                  <Point X="27.782037109375" Y="-4.111510742188" />
                  <Point X="27.78185546875" Y="-4.111640625" />
                  <Point X="27.701765625" Y="-4.163481933594" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.747505859375" Y="-2.922179443359" />
                  <Point X="26.72192578125" Y="-2.900556640625" />
                  <Point X="26.51205078125" Y="-2.765627197266" />
                  <Point X="26.50880078125" Y="-2.763537841797" />
                  <Point X="26.47998828125" Y="-2.751166015625" />
                  <Point X="26.4483671875" Y="-2.743435058594" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.187568359375" Y="-2.762238525391" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.156361328125" Y="-2.769398193359" />
                  <Point X="26.1289765625" Y="-2.780741943359" />
                  <Point X="26.10459765625" Y="-2.795461669922" />
                  <Point X="25.927357421875" Y="-2.942830322266" />
                  <Point X="25.92461328125" Y="-2.945112060547" />
                  <Point X="25.904142578125" Y="-2.968861572266" />
                  <Point X="25.88725" Y="-2.996687988281" />
                  <Point X="25.875625" Y="-3.025809326172" />
                  <Point X="25.82263671875" Y="-3.269601074219" />
                  <Point X="25.820724609375" Y="-3.282815429688" />
                  <Point X="25.819185546875" Y="-3.303757324219" />
                  <Point X="25.8197421875" Y="-3.323125488281" />
                  <Point X="25.82309375" Y="-3.348572021484" />
                  <Point X="26.022064453125" Y="-4.859915039062" />
                  <Point X="25.97584765625" Y="-4.870045898438" />
                  <Point X="25.97566015625" Y="-4.870086914062" />
                  <Point X="25.92931640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941576171875" Y="-4.752637695312" />
                  <Point X="23.858755859375" Y="-4.731328613281" />
                  <Point X="23.8792265625" Y="-4.575838378906" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.509324707031" />
                  <Point X="23.876666015625" Y="-4.497689941406" />
                  <Point X="23.817060546875" Y="-4.198038085938" />
                  <Point X="23.817056640625" Y="-4.19801953125" />
                  <Point X="23.81186328125" Y="-4.178427246094" />
                  <Point X="23.8009609375" Y="-4.149472167969" />
                  <Point X="23.794322265625" Y="-4.135438964844" />
                  <Point X="23.778251953125" Y="-4.107611328125" />
                  <Point X="23.76941796875" Y="-4.094849853516" />
                  <Point X="23.7497890625" Y="-4.070933837891" />
                  <Point X="23.738994140625" Y="-4.059779541016" />
                  <Point X="23.509291015625" Y="-3.858334228516" />
                  <Point X="23.49326171875" Y="-3.845965087891" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822527587891" />
                  <Point X="23.423470703125" Y="-3.810226318359" />
                  <Point X="23.4086875" Y="-3.805476318359" />
                  <Point X="23.378544921875" Y="-3.798447998047" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.05831640625" Y="-3.7761875" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.946326171875" Y="-3.79549609375" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812011719" />
                  <Point X="22.650529296875" Y="-3.985551757812" />
                  <Point X="22.644158203125" Y="-3.990200683594" />
                  <Point X="22.6252109375" Y="-4.005270019531" />
                  <Point X="22.616544921875" Y="-4.013077636719" />
                  <Point X="22.601236328125" Y="-4.028675292969" />
                  <Point X="22.586099609375" Y="-4.046098144531" />
                  <Point X="22.496796875" Y="-4.162478515625" />
                  <Point X="22.25241015625" Y="-4.011158691406" />
                  <Point X="22.01913671875" Y="-3.831546386719" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.710085205078" />
                  <Point X="22.67605078125" Y="-2.681120117188" />
                  <Point X="22.680314453125" Y="-2.666189453125" />
                  <Point X="22.6865859375" Y="-2.634663085938" />
                  <Point X="22.688361328125" Y="-2.619239501953" />
                  <Point X="22.689375" Y="-2.588305419922" />
                  <Point X="22.6853359375" Y="-2.557617675781" />
                  <Point X="22.6763515625" Y="-2.527999511719" />
                  <Point X="22.67064453125" Y="-2.51355859375" />
                  <Point X="22.656427734375" Y="-2.484729736328" />
                  <Point X="22.6484453125" Y="-2.471412597656" />
                  <Point X="22.63041796875" Y="-2.446254882812" />
                  <Point X="22.620373046875" Y="-2.434414306641" />
                  <Point X="22.60337109375" Y="-2.417412353516" />
                  <Point X="22.5913671875" Y="-2.407182128906" />
                  <Point X="22.566287109375" Y="-2.389171875" />
                  <Point X="22.55301171875" Y="-2.381192382812" />
                  <Point X="22.5241640625" Y="-2.366916259766" />
                  <Point X="22.5097109375" Y="-2.361183349609" />
                  <Point X="22.4800625" Y="-2.352151855469" />
                  <Point X="22.4493359375" Y="-2.348077880859" />
                  <Point X="22.418357421875" Y="-2.349070556641" />
                  <Point X="22.40291015625" Y="-2.350838623047" />
                  <Point X="22.371337890625" Y="-2.357103515625" />
                  <Point X="22.35638671875" Y="-2.361366943359" />
                  <Point X="22.32737890625" Y="-2.372276123047" />
                  <Point X="22.313322265625" Y="-2.378921875" />
                  <Point X="22.29366015625" Y="-2.390272949219" />
                  <Point X="21.206912109375" Y="-3.017707763672" />
                  <Point X="20.995978515625" Y="-2.740583740234" />
                  <Point X="20.818734375" Y="-2.443373779297" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.963515625" Y="-1.563311523438" />
                  <Point X="21.984892578125" Y="-1.540393066406" />
                  <Point X="21.994630859375" Y="-1.528045166016" />
                  <Point X="22.012595703125" Y="-1.500915405273" />
                  <Point X="22.020162109375" Y="-1.487129272461" />
                  <Point X="22.03291796875" Y="-1.458500610352" />
                  <Point X="22.038107421875" Y="-1.443657714844" />
                  <Point X="22.0456953125" Y="-1.414363769531" />
                  <Point X="22.04848828125" Y="-1.39859777832" />
                  <Point X="22.051263671875" Y="-1.368227539062" />
                  <Point X="22.05142578125" Y="-1.352927734375" />
                  <Point X="22.049203125" Y="-1.32127331543" />
                  <Point X="22.046900390625" Y="-1.306124389648" />
                  <Point X="22.039890625" Y="-1.276396850586" />
                  <Point X="22.028193359375" Y="-1.248184448242" />
                  <Point X="22.01211328125" Y="-1.222217407227" />
                  <Point X="22.0030234375" Y="-1.209884399414" />
                  <Point X="21.982197265625" Y="-1.185942138672" />
                  <Point X="21.971240234375" Y="-1.175229492188" />
                  <Point X="21.94775" Y="-1.155706787109" />
                  <Point X="21.935216796875" Y="-1.146895996094" />
                  <Point X="21.909154296875" Y="-1.131556640625" />
                  <Point X="21.8946171875" Y="-1.124506713867" />
                  <Point X="21.865275390625" Y="-1.113245849609" />
                  <Point X="21.850134765625" Y="-1.108837280273" />
                  <Point X="21.81826953125" Y="-1.102368652344" />
                  <Point X="21.802666015625" Y="-1.100526977539" />
                  <Point X="21.7713671875" Y="-1.099441040039" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="21.7308515625" Y="-1.103464111328" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.259236328125" Y="-0.974107543945" />
                  <Point X="20.213548828125" Y="-0.654654846191" />
                  <Point X="21.491712890625" Y="-0.312171234131" />
                  <Point X="21.4995234375" Y="-0.309713012695" />
                  <Point X="21.52606640625" Y="-0.299252471924" />
                  <Point X="21.55583203125" Y="-0.283893951416" />
                  <Point X="21.5664375" Y="-0.277514434814" />
                  <Point X="21.593767578125" Y="-0.25854586792" />
                  <Point X="21.606455078125" Y="-0.248143005371" />
                  <Point X="21.62876953125" Y="-0.226371536255" />
                  <Point X="21.63902734375" Y="-0.214565078735" />
                  <Point X="21.658302734375" Y="-0.188288711548" />
                  <Point X="21.66653515625" Y="-0.174859893799" />
                  <Point X="21.680666015625" Y="-0.146827087402" />
                  <Point X="21.686564453125" Y="-0.132222808838" />
                  <Point X="21.695673828125" Y="-0.102870384216" />
                  <Point X="21.69911328125" Y="-0.088094596863" />
                  <Point X="21.7033125" Y="-0.060127784729" />
                  <Point X="21.704365234375" Y="-0.045984088898" />
                  <Point X="21.704353515625" Y="-0.016384805679" />
                  <Point X="21.70327734375" Y="-0.002161893845" />
                  <Point X="21.6990078125" Y="0.025958444595" />
                  <Point X="21.695814453125" Y="0.039856021881" />
                  <Point X="21.686705078125" Y="0.069208297729" />
                  <Point X="21.680642578125" Y="0.084333648682" />
                  <Point X="21.666591796875" Y="0.112201324463" />
                  <Point X="21.658412109375" Y="0.125556877136" />
                  <Point X="21.63915625" Y="0.151853759766" />
                  <Point X="21.628826171875" Y="0.163750274658" />
                  <Point X="21.60634375" Y="0.18566947937" />
                  <Point X="21.59419140625" Y="0.195692001343" />
                  <Point X="21.566861328125" Y="0.214660598755" />
                  <Point X="21.560146484375" Y="0.218916107178" />
                  <Point X="21.535568359375" Y="0.232203887939" />
                  <Point X="21.503767578125" Y="0.245497619629" />
                  <Point X="21.491716796875" Y="0.249610153198" />
                  <Point X="21.46909375" Y="0.255672485352" />
                  <Point X="20.2145546875" Y="0.591825195312" />
                  <Point X="20.26866796875" Y="0.957513366699" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="21.221935546875" Y="1.20560546875" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.2529453125" Y="1.204364379883" />
                  <Point X="21.263298828125" Y="1.204703369141" />
                  <Point X="21.284859375" Y="1.20658984375" />
                  <Point X="21.29511328125" Y="1.208053833008" />
                  <Point X="21.315400390625" Y="1.212089355469" />
                  <Point X="21.325431640625" Y="1.214660766602" />
                  <Point X="21.38590625" Y="1.233728515625" />
                  <Point X="21.396470703125" Y="1.237647827148" />
                  <Point X="21.41542578125" Y="1.245980957031" />
                  <Point X="21.424677734375" Y="1.250666015625" />
                  <Point X="21.4434296875" Y="1.261487426758" />
                  <Point X="21.452107421875" Y="1.267148803711" />
                  <Point X="21.468794921875" Y="1.279380737305" />
                  <Point X="21.484048828125" Y="1.293354248047" />
                  <Point X="21.497693359375" Y="1.308908813477" />
                  <Point X="21.50409375" Y="1.317060546875" />
                  <Point X="21.516513671875" Y="1.334795166016" />
                  <Point X="21.521984375" Y="1.34359375" />
                  <Point X="21.5319375" Y="1.36173449707" />
                  <Point X="21.536419921875" Y="1.371076538086" />
                  <Point X="21.56069140625" Y="1.429673339844" />
                  <Point X="21.564533203125" Y="1.440441162109" />
                  <Point X="21.57032421875" Y="1.460337524414" />
                  <Point X="21.57267578125" Y="1.470446044922" />
                  <Point X="21.57641796875" Y="1.491743896484" />
                  <Point X="21.577650390625" Y="1.502015136719" />
                  <Point X="21.5789921875" Y="1.522634521484" />
                  <Point X="21.578083984375" Y="1.543278808594" />
                  <Point X="21.574935546875" Y="1.563700439453" />
                  <Point X="21.5728046875" Y="1.573825683594" />
                  <Point X="21.567205078125" Y="1.594712036133" />
                  <Point X="21.563984375" Y="1.604546142578" />
                  <Point X="21.5564921875" Y="1.623810424805" />
                  <Point X="21.552220703125" Y="1.633240234375" />
                  <Point X="21.522935546875" Y="1.689498901367" />
                  <Point X="21.516984375" Y="1.699667236328" />
                  <Point X="21.50555078125" Y="1.71672265625" />
                  <Point X="21.49937109375" Y="1.72494152832" />
                  <Point X="21.4854921875" Y="1.741457397461" />
                  <Point X="21.478431640625" Y="1.748986694336" />
                  <Point X="21.463537109375" Y="1.763235473633" />
                  <Point X="21.442724609375" Y="1.779913085938" />
                  <Point X="20.77238671875" Y="2.294281982422" />
                  <Point X="20.997703125" Y="2.680306396484" />
                  <Point X="21.27366015625" Y="3.035012939453" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.75508203125" Y="2.757716552734" />
                  <Point X="21.774013671875" Y="2.749386230469" />
                  <Point X="21.78370703125" Y="2.745737792969" />
                  <Point X="21.80434765625" Y="2.739229492188" />
                  <Point X="21.8143828125" Y="2.736657470703" />
                  <Point X="21.834671875" Y="2.732621826172" />
                  <Point X="21.84492578125" Y="2.731158203125" />
                  <Point X="21.929171875" Y="2.723787597656" />
                  <Point X="21.94082421875" Y="2.723334472656" />
                  <Point X="21.961501953125" Y="2.723785400391" />
                  <Point X="21.971830078125" Y="2.724575439453" />
                  <Point X="21.9932890625" Y="2.727400146484" />
                  <Point X="22.00346875" Y="2.729309814453" />
                  <Point X="22.023560546875" Y="2.734225830078" />
                  <Point X="22.042998046875" Y="2.741300292969" />
                  <Point X="22.061548828125" Y="2.750448242188" />
                  <Point X="22.07057421875" Y="2.755528076172" />
                  <Point X="22.088830078125" Y="2.767157714844" />
                  <Point X="22.097251953125" Y="2.773191894531" />
                  <Point X="22.113384765625" Y="2.786139648438" />
                  <Point X="22.121095703125" Y="2.793053222656" />
                  <Point X="22.18089453125" Y="2.852850830078" />
                  <Point X="22.188734375" Y="2.861487792969" />
                  <Point X="22.20168359375" Y="2.877621337891" />
                  <Point X="22.20771875" Y="2.886043701172" />
                  <Point X="22.21934765625" Y="2.904298095703" />
                  <Point X="22.2244296875" Y="2.913325927734" />
                  <Point X="22.233576171875" Y="2.931874755859" />
                  <Point X="22.240646484375" Y="2.951299316406" />
                  <Point X="22.245564453125" Y="2.971388183594" />
                  <Point X="22.2474765625" Y="2.981573486328" />
                  <Point X="22.250302734375" Y="3.003032470703" />
                  <Point X="22.251091796875" Y="3.013365478516" />
                  <Point X="22.25154296875" Y="3.034050537109" />
                  <Point X="22.251205078125" Y="3.044402587891" />
                  <Point X="22.243833984375" Y="3.128647705078" />
                  <Point X="22.24226171875" Y="3.140161865234" />
                  <Point X="22.238234375" Y="3.160421386719" />
                  <Point X="22.23566796875" Y="3.170438964844" />
                  <Point X="22.22915625" Y="3.191113037109" />
                  <Point X="22.22550390625" Y="3.200824707031" />
                  <Point X="22.21716015625" Y="3.219792724609" />
                  <Point X="22.206716796875" Y="3.239009521484" />
                  <Point X="21.94061328125" Y="3.699914794922" />
                  <Point X="22.351640625" Y="4.015045166016" />
                  <Point X="22.8074765625" Y="4.268295410156" />
                  <Point X="22.881435546875" Y="4.171909179688" />
                  <Point X="22.8881796875" Y="4.164048828125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.934908203125" Y="4.121897460938" />
                  <Point X="22.952107421875" Y="4.110405273438" />
                  <Point X="22.96101953125" Y="4.105129394531" />
                  <Point X="23.054783203125" Y="4.056318115234" />
                  <Point X="23.065724609375" Y="4.051262451172" />
                  <Point X="23.085009765625" Y="4.043768310547" />
                  <Point X="23.09485546875" Y="4.040548339844" />
                  <Point X="23.115755859375" Y="4.034953613281" />
                  <Point X="23.12588671875" Y="4.032825439453" />
                  <Point X="23.146322265625" Y="4.029684326172" />
                  <Point X="23.166978515625" Y="4.028785644531" />
                  <Point X="23.187611328125" Y="4.030140136719" />
                  <Point X="23.197892578125" Y="4.031380371094" />
                  <Point X="23.219201171875" Y="4.035139160156" />
                  <Point X="23.22928515625" Y="4.037491210938" />
                  <Point X="23.24913671875" Y="4.043278320312" />
                  <Point X="23.258904296875" Y="4.046713623047" />
                  <Point X="23.3565" Y="4.087139648438" />
                  <Point X="23.367357421875" Y="4.092245117188" />
                  <Point X="23.3855" Y="4.102192871094" />
                  <Point X="23.394298828125" Y="4.107661132812" />
                  <Point X="23.412037109375" Y="4.120075683594" />
                  <Point X="23.420189453125" Y="4.126471679688" />
                  <Point X="23.43575" Y="4.14011328125" />
                  <Point X="23.44973046875" Y="4.155365722656" />
                  <Point X="23.46196875" Y="4.172052734375" />
                  <Point X="23.467634765625" Y="4.180732910156" />
                  <Point X="23.4784609375" Y="4.199482910156" />
                  <Point X="23.48314453125" Y="4.208724121094" />
                  <Point X="23.4914765625" Y="4.227659667969" />
                  <Point X="23.495125" Y="4.237354003906" />
                  <Point X="23.526912109375" Y="4.338169433594" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370050292969" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401864257812" />
                  <Point X="23.53769921875" Y="4.412225585938" />
                  <Point X="23.53724609375" Y="4.432915527344" />
                  <Point X="23.53580078125" Y="4.448210449219" />
                  <Point X="23.520734375" Y="4.562654785156" />
                  <Point X="24.068828125" Y="4.716320800781" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.778119140625" Y="4.250466308594" />
                  <Point X="24.790259765625" Y="4.220672363281" />
                  <Point X="24.797224609375" Y="4.206901367188" />
                  <Point X="24.809326171875" Y="4.187144042969" />
                  <Point X="24.823744140625" Y="4.16901171875" />
                  <Point X="24.840267578125" Y="4.152770996094" />
                  <Point X="24.84902734375" Y="4.145155761719" />
                  <Point X="24.87421875" Y="4.125825195313" />
                  <Point X="24.88726953125" Y="4.11741015625" />
                  <Point X="24.91456640625" Y="4.102819824219" />
                  <Point X="24.943873046875" Y="4.092871582031" />
                  <Point X="24.97441015625" Y="4.087829589844" />
                  <Point X="24.98988671875" Y="4.086560546875" />
                  <Point X="25.02242578125" Y="4.086560546875" />
                  <Point X="25.03790234375" Y="4.087829589844" />
                  <Point X="25.068439453125" Y="4.092871582031" />
                  <Point X="25.09774609375" Y="4.102819824219" />
                  <Point X="25.12504296875" Y="4.11741015625" />
                  <Point X="25.13809375" Y="4.125825195313" />
                  <Point X="25.16328515625" Y="4.145155761719" />
                  <Point X="25.17317578125" Y="4.153901367188" />
                  <Point X="25.191654296875" Y="4.172685546875" />
                  <Point X="25.207353515625" Y="4.193858398438" />
                  <Point X="25.219958984375" Y="4.21699609375" />
                  <Point X="25.225453125" Y="4.228999511719" />
                  <Point X="25.235236328125" Y="4.255008789062" />
                  <Point X="25.240927734375" Y="4.272724609375" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.827876953125" Y="4.737911621094" />
                  <Point X="26.453591796875" Y="4.586845214844" />
                  <Point X="26.85826171875" Y="4.440069335938" />
                  <Point X="27.250453125" Y="4.256654296875" />
                  <Point X="27.629427734375" Y="4.035863037109" />
                  <Point X="27.81778125" Y="3.901915527344" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.062376953125" Y="2.593106933594" />
                  <Point X="27.0531875" Y="2.573447753906" />
                  <Point X="27.044185546875" Y="2.549570556641" />
                  <Point X="27.041302734375" Y="2.540599365234" />
                  <Point X="27.02016015625" Y="2.461537353516" />
                  <Point X="27.0183359375" Y="2.453245849609" />
                  <Point X="27.014359375" Y="2.428126220703" />
                  <Point X="27.012693359375" Y="2.407998779297" />
                  <Point X="27.01238671875" Y="2.398385253906" />
                  <Point X="27.013412109375" Y="2.369578857422" />
                  <Point X="27.02163671875" Y="2.301367675781" />
                  <Point X="27.023779296875" Y="2.289168945312" />
                  <Point X="27.029119140625" Y="2.26721484375" />
                  <Point X="27.032443359375" Y="2.256400878906" />
                  <Point X="27.040712890625" Y="2.234280273438" />
                  <Point X="27.045298828125" Y="2.223936523438" />
                  <Point X="27.055671875" Y="2.203861328125" />
                  <Point X="27.061458984375" Y="2.194129882813" />
                  <Point X="27.10376171875" Y="2.131786621094" />
                  <Point X="27.10882421875" Y="2.124998046875" />
                  <Point X="27.12849609375" Y="2.102362060547" />
                  <Point X="27.157015625" Y="2.075924072266" />
                  <Point X="27.1682578125" Y="2.066982910156" />
                  <Point X="27.230599609375" Y="2.024680175781" />
                  <Point X="27.241369140625" Y="2.018190429688" />
                  <Point X="27.261427734375" Y="2.007829345703" />
                  <Point X="27.271765625" Y="2.003246337891" />
                  <Point X="27.29381640625" Y="1.995004882812" />
                  <Point X="27.304603515625" Y="1.991689086914" />
                  <Point X="27.326490234375" Y="1.986360473633" />
                  <Point X="27.33758984375" Y="1.984347045898" />
                  <Point X="27.405955078125" Y="1.976103149414" />
                  <Point X="27.414501953125" Y="1.975462036133" />
                  <Point X="27.44482421875" Y="1.975479125977" />
                  <Point X="27.48358203125" Y="1.979739135742" />
                  <Point X="27.497744140625" Y="1.982395141602" />
                  <Point X="27.576806640625" Y="2.003537475586" />
                  <Point X="27.582244140625" Y="2.005166503906" />
                  <Point X="27.60138671875" Y="2.011979248047" />
                  <Point X="27.626884765625" Y="2.023229736328" />
                  <Point X="27.63603515625" Y="2.027872802734" />
                  <Point X="27.658791015625" Y="2.041011108398" />
                  <Point X="28.94040625" Y="2.780951416016" />
                  <Point X="29.043958984375" Y="2.637034912109" />
                  <Point X="29.136884765625" Y="2.483470947266" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.16813671875" Y="1.739867797852" />
                  <Point X="28.15212890625" Y="1.725225097656" />
                  <Point X="28.134671875" Y="1.706607543945" />
                  <Point X="28.12857421875" Y="1.699421020508" />
                  <Point X="28.071673828125" Y="1.625189208984" />
                  <Point X="28.06685546875" Y="1.618290405273" />
                  <Point X="28.053658203125" Y="1.596794799805" />
                  <Point X="28.04430859375" Y="1.578693359375" />
                  <Point X="28.04030859375" Y="1.569869628906" />
                  <Point X="28.030140625" Y="1.542672485352" />
                  <Point X="28.00896875" Y="1.466965332031" />
                  <Point X="28.006302734375" Y="1.454983154297" />
                  <Point X="28.002810546875" Y="1.432602783203" />
                  <Point X="28.001732421875" Y="1.421304931641" />
                  <Point X="28.0008984375" Y="1.397657470703" />
                  <Point X="28.00117578125" Y="1.386321899414" />
                  <Point X="28.003078125" Y="1.363775390625" />
                  <Point X="28.004703125" Y="1.352564208984" />
                  <Point X="28.022103515625" Y="1.268236938477" />
                  <Point X="28.02416015625" Y="1.260101196289" />
                  <Point X="28.031751953125" Y="1.236121826172" />
                  <Point X="28.039216796875" Y="1.217443603516" />
                  <Point X="28.043208984375" Y="1.208748657227" />
                  <Point X="28.056916015625" Y="1.183527099609" />
                  <Point X="28.104234375" Y="1.111604248047" />
                  <Point X="28.111708984375" Y="1.101466918945" />
                  <Point X="28.12626953125" Y="1.084207641602" />
                  <Point X="28.1340625" Y="1.076012939453" />
                  <Point X="28.1513125" Y="1.05991418457" />
                  <Point X="28.160021484375" Y="1.052707275391" />
                  <Point X="28.178236328125" Y="1.039375244141" />
                  <Point X="28.187744140625" Y="1.033249755859" />
                  <Point X="28.25632421875" Y="0.994645141602" />
                  <Point X="28.263919921875" Y="0.990807250977" />
                  <Point X="28.287369140625" Y="0.980667480469" />
                  <Point X="28.30639453125" Y="0.974079162598" />
                  <Point X="28.315603515625" Y="0.97140234375" />
                  <Point X="28.343671875" Y="0.965257385254" />
                  <Point X="28.436396484375" Y="0.953002807617" />
                  <Point X="28.4419765625" Y="0.952432434082" />
                  <Point X="28.462087890625" Y="0.951395629883" />
                  <Point X="28.490265625" Y="0.952006896973" />
                  <Point X="28.50060546875" Y="0.952797424316" />
                  <Point X="28.522220703125" Y="0.955643432617" />
                  <Point X="29.704703125" Y="1.111319946289" />
                  <Point X="29.7526875" Y="0.914211181641" />
                  <Point X="29.783873046875" Y="0.713920898438" />
                  <Point X="28.6919921875" Y="0.421352813721" />
                  <Point X="28.68603125" Y="0.419544219971" />
                  <Point X="28.665623046875" Y="0.412136413574" />
                  <Point X="28.642380859375" Y="0.401618804932" />
                  <Point X="28.634005859375" Y="0.397316772461" />
                  <Point X="28.54290625" Y="0.344659545898" />
                  <Point X="28.535875" Y="0.340172607422" />
                  <Point X="28.51562890625" Y="0.325516601562" />
                  <Point X="28.49990625" Y="0.312281005859" />
                  <Point X="28.492771484375" Y="0.305619567871" />
                  <Point X="28.472796875" Y="0.28422769165" />
                  <Point X="28.41813671875" Y="0.214578414917" />
                  <Point X="28.4108046875" Y="0.204137496948" />
                  <Point X="28.39909765625" Y="0.184873458862" />
                  <Point X="28.393814453125" Y="0.174889419556" />
                  <Point X="28.384052734375" Y="0.153441055298" />
                  <Point X="28.379994140625" Y="0.142901611328" />
                  <Point X="28.37315625" Y="0.121418617249" />
                  <Point X="28.370376953125" Y="0.110474777222" />
                  <Point X="28.35215625" Y="0.015337409019" />
                  <Point X="28.350951171875" Y="0.007105039597" />
                  <Point X="28.348775390625" Y="-0.017847105026" />
                  <Point X="28.3485" Y="-0.038102024078" />
                  <Point X="28.348859375" Y="-0.047728031158" />
                  <Point X="28.351875" Y="-0.076423538208" />
                  <Point X="28.37008984375" Y="-0.171532516479" />
                  <Point X="28.3731328125" Y="-0.183867630005" />
                  <Point X="28.379978515625" Y="-0.205398178101" />
                  <Point X="28.38404296875" Y="-0.215957977295" />
                  <Point X="28.393822265625" Y="-0.237450027466" />
                  <Point X="28.399111328125" Y="-0.247449081421" />
                  <Point X="28.410845703125" Y="-0.266753997803" />
                  <Point X="28.417291015625" Y="-0.276060180664" />
                  <Point X="28.471939453125" Y="-0.34569430542" />
                  <Point X="28.4774375" Y="-0.352093414307" />
                  <Point X="28.495025390625" Y="-0.370265960693" />
                  <Point X="28.5101953125" Y="-0.38377935791" />
                  <Point X="28.517734375" Y="-0.389835296631" />
                  <Point X="28.54149609375" Y="-0.40640435791" />
                  <Point X="28.632595703125" Y="-0.459061309814" />
                  <Point X="28.655484375" Y="-0.470282318115" />
                  <Point X="28.68203515625" Y="-0.48064654541" />
                  <Point X="28.6919921875" Y="-0.483913116455" />
                  <Point X="28.71181640625" Y="-0.489224914551" />
                  <Point X="29.78487890625" Y="-0.776751281738" />
                  <Point X="29.7616171875" Y="-0.931035583496" />
                  <Point X="29.727802734375" Y="-1.079219726563" />
                  <Point X="28.436783203125" Y="-0.90925378418" />
                  <Point X="28.428623046875" Y="-0.908535827637" />
                  <Point X="28.40009375" Y="-0.908042663574" />
                  <Point X="28.366716796875" Y="-0.910841674805" />
                  <Point X="28.354478515625" Y="-0.912676879883" />
                  <Point X="28.175681640625" Y="-0.951538818359" />
                  <Point X="28.15787109375" Y="-0.956743408203" />
                  <Point X="28.128748046875" Y="-0.968369445801" />
                  <Point X="28.11466796875" Y="-0.975392333984" />
                  <Point X="28.08684375" Y="-0.992284179688" />
                  <Point X="28.07412109375" Y="-1.001532714844" />
                  <Point X="28.050373046875" Y="-1.022002258301" />
                  <Point X="28.03934765625" Y="-1.033223510742" />
                  <Point X="27.93127734375" Y="-1.16319921875" />
                  <Point X="27.92133203125" Y="-1.176839233398" />
                  <Point X="27.90661328125" Y="-1.201216796875" />
                  <Point X="27.900171875" Y="-1.213960571289" />
                  <Point X="27.88882421875" Y="-1.241352539062" />
                  <Point X="27.884365234375" Y="-1.254926635742" />
                  <Point X="27.877533203125" Y="-1.282580688477" />
                  <Point X="27.87516015625" Y="-1.296661010742" />
                  <Point X="27.859681640625" Y="-1.464877319336" />
                  <Point X="27.859291015625" Y="-1.483290893555" />
                  <Point X="27.861607421875" Y="-1.514565795898" />
                  <Point X="27.864064453125" Y="-1.530104003906" />
                  <Point X="27.871794921875" Y="-1.561732788086" />
                  <Point X="27.876783203125" Y="-1.57665612793" />
                  <Point X="27.88915625" Y="-1.605475097656" />
                  <Point X="27.896541015625" Y="-1.61937097168" />
                  <Point X="27.99548828125" Y="-1.773278198242" />
                  <Point X="27.999509765625" Y="-1.779052368164" />
                  <Point X="28.01546484375" Y="-1.799072753906" />
                  <Point X="28.042236328125" Y="-1.826843261719" />
                  <Point X="28.052798828125" Y="-1.836278320312" />
                  <Point X="28.0711953125" Y="-1.850394287109" />
                  <Point X="29.087171875" Y="-2.629981689453" />
                  <Point X="29.045486328125" Y="-2.697434814453" />
                  <Point X="29.001275390625" Y="-2.760251953125" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.84119140625" Y="-2.090885009766" />
                  <Point X="27.815025390625" Y="-2.079512695312" />
                  <Point X="27.783119140625" Y="-2.069325683594" />
                  <Point X="27.771109375" Y="-2.066337402344" />
                  <Point X="27.558314453125" Y="-2.027906616211" />
                  <Point X="27.539359375" Y="-2.025807250977" />
                  <Point X="27.508005859375" Y="-2.025403198242" />
                  <Point X="27.4923125" Y="-2.026503662109" />
                  <Point X="27.460140625" Y="-2.031461669922" />
                  <Point X="27.444845703125" Y="-2.035136352539" />
                  <Point X="27.4150703125" Y="-2.044959838867" />
                  <Point X="27.40058984375" Y="-2.051108642578" />
                  <Point X="27.22380859375" Y="-2.144146972656" />
                  <Point X="27.208921875" Y="-2.15319921875" />
                  <Point X="27.186013671875" Y="-2.170082519531" />
                  <Point X="27.1751953125" Y="-2.17938671875" />
                  <Point X="27.1542421875" Y="-2.200342773438" />
                  <Point X="27.144935546875" Y="-2.21116796875" />
                  <Point X="27.128046875" Y="-2.23409375" />
                  <Point X="27.12046484375" Y="-2.246194335938" />
                  <Point X="27.02742578125" Y="-2.422975097656" />
                  <Point X="27.01984375" Y="-2.440177734375" />
                  <Point X="27.010021484375" Y="-2.469943603516" />
                  <Point X="27.006345703125" Y="-2.485234375" />
                  <Point X="27.0013828125" Y="-2.517417236328" />
                  <Point X="27.00028125" Y="-2.533114501953" />
                  <Point X="27.00068359375" Y="-2.564477539062" />
                  <Point X="27.0021875" Y="-2.580143310547" />
                  <Point X="27.040619140625" Y="-2.792938720703" />
                  <Point X="27.04208203125" Y="-2.799637695312" />
                  <Point X="27.047423828125" Y="-2.819491943359" />
                  <Point X="27.053921875" Y="-2.838900390625" />
                  <Point X="27.057396484375" Y="-2.847771728516" />
                  <Point X="27.069546875" Y="-2.873571777344" />
                  <Point X="27.0813671875" Y="-2.894047119141" />
                  <Point X="27.735896484375" Y="-4.027721923828" />
                  <Point X="27.723755859375" Y="-4.036083984375" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.828654296875" Y="-2.87014453125" />
                  <Point X="26.808833984375" Y="-2.849627197266" />
                  <Point X="26.78325390625" Y="-2.828004394531" />
                  <Point X="26.77330078125" Y="-2.820646484375" />
                  <Point X="26.56342578125" Y="-2.685717041016" />
                  <Point X="26.546283203125" Y="-2.676244873047" />
                  <Point X="26.517470703125" Y="-2.663873046875" />
                  <Point X="26.50255078125" Y="-2.658884033203" />
                  <Point X="26.4709296875" Y="-2.651153076172" />
                  <Point X="26.455392578125" Y="-2.648695068359" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.17886328125" Y="-2.667638183594" />
                  <Point X="26.1612265625" Y="-2.670339355469" />
                  <Point X="26.13357421875" Y="-2.677171875" />
                  <Point X="26.12000390625" Y="-2.681630371094" />
                  <Point X="26.092619140625" Y="-2.692974121094" />
                  <Point X="26.079873046875" Y="-2.699416503906" />
                  <Point X="26.055494140625" Y="-2.714136230469" />
                  <Point X="26.043861328125" Y="-2.722413574219" />
                  <Point X="25.86662109375" Y="-2.869782226562" />
                  <Point X="25.852654296875" Y="-2.883088134766" />
                  <Point X="25.83218359375" Y="-2.906837646484" />
                  <Point X="25.822935546875" Y="-2.919562988281" />
                  <Point X="25.80604296875" Y="-2.947389404297" />
                  <Point X="25.79901953125" Y="-2.961467285156" />
                  <Point X="25.78739453125" Y="-2.990588623047" />
                  <Point X="25.78279296875" Y="-3.005632080078" />
                  <Point X="25.7298046875" Y="-3.249423828125" />
                  <Point X="25.728615234375" Y="-3.255996337891" />
                  <Point X="25.72598046875" Y="-3.275852539062" />
                  <Point X="25.72444140625" Y="-3.296794433594" />
                  <Point X="25.724224609375" Y="-3.306486572266" />
                  <Point X="25.7255546875" Y="-3.335530761719" />
                  <Point X="25.72890625" Y="-3.360977294922" />
                  <Point X="25.833087890625" Y="-4.152315917969" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652609375" Y="-3.480127441406" />
                  <Point X="25.6421484375" Y="-3.453580810547" />
                  <Point X="25.626791015625" Y="-3.423817871094" />
                  <Point X="25.620412109375" Y="-3.413212646484" />
                  <Point X="25.4591796875" Y="-3.180909179688" />
                  <Point X="25.44667578125" Y="-3.165177246094" />
                  <Point X="25.424794921875" Y="-3.142723876953" />
                  <Point X="25.412921875" Y="-3.132405517578" />
                  <Point X="25.3866640625" Y="-3.113158447266" />
                  <Point X="25.373248046875" Y="-3.104939941406" />
                  <Point X="25.345244140625" Y="-3.090829833984" />
                  <Point X="25.33065625" Y="-3.084938232422" />
                  <Point X="25.08116015625" Y="-3.00750390625" />
                  <Point X="25.063375" Y="-3.003108642578" />
                  <Point X="25.0352109375" Y="-2.998839355469" />
                  <Point X="25.02096875" Y="-2.997766357422" />
                  <Point X="24.991326171875" Y="-2.997767333984" />
                  <Point X="24.977087890625" Y="-2.998840820312" />
                  <Point X="24.948931640625" Y="-3.003110107422" />
                  <Point X="24.935013671875" Y="-3.006305908203" />
                  <Point X="24.685517578125" Y="-3.083739990234" />
                  <Point X="24.667064453125" Y="-3.090832519531" />
                  <Point X="24.63905859375" Y="-3.104945068359" />
                  <Point X="24.625642578125" Y="-3.113164306641" />
                  <Point X="24.59938671875" Y="-3.132411621094" />
                  <Point X="24.587515625" Y="-3.142727294922" />
                  <Point X="24.56563671875" Y="-3.165178466797" />
                  <Point X="24.55562890625" Y="-3.177313964844" />
                  <Point X="24.3943984375" Y="-3.4096171875" />
                  <Point X="24.3810234375" Y="-3.432031738281" />
                  <Point X="24.37130859375" Y="-3.451192871094" />
                  <Point X="24.36731640625" Y="-3.460199707031" />
                  <Point X="24.35724609375" Y="-3.487940917969" />
                  <Point X="24.351138671875" Y="-3.510736328125" />
                  <Point X="24.014576171875" Y="-4.766807128906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.66311704519" Y="-3.957057819394" />
                  <Point X="27.689544907692" Y="-3.947438864088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.602478231004" Y="-3.878031654414" />
                  <Point X="27.641312031386" Y="-3.863897306992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.541839416819" Y="-3.799005489433" />
                  <Point X="27.593079155079" Y="-3.780355749895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.481200602634" Y="-3.719979324453" />
                  <Point X="27.544846278773" Y="-3.696814192798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.420561788449" Y="-3.640953159472" />
                  <Point X="27.496613402466" Y="-3.613272635702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.824624468712" Y="-4.120729951226" />
                  <Point X="25.828732666992" Y="-4.119234689335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.359922974263" Y="-3.561926994492" />
                  <Point X="27.44838052616" Y="-3.529731078605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.864908015127" Y="-4.732911519547" />
                  <Point X="24.040813718883" Y="-4.668887079342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.799942770854" Y="-4.028616466192" />
                  <Point X="25.816031620663" Y="-4.022760603758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.299284160078" Y="-3.482900829511" />
                  <Point X="27.400147649853" Y="-3.446189521509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.872206884606" Y="-4.629158059928" />
                  <Point X="24.070829826755" Y="-4.556865221143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.775261072995" Y="-3.936502981158" />
                  <Point X="25.803330574334" Y="-3.92628651818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.238645345893" Y="-3.40387466453" />
                  <Point X="27.351914773547" Y="-3.362647964412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.879415415214" Y="-4.525437480968" />
                  <Point X="24.100845934626" Y="-4.444843362944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.750579375137" Y="-3.844389496125" />
                  <Point X="25.790629528005" Y="-3.829812432603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.178006531708" Y="-3.32484849955" />
                  <Point X="27.30368189724" Y="-3.279106407315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.896083768431" Y="-2.699519525212" />
                  <Point X="29.087160784067" Y="-2.629973179068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.86324630971" Y="-4.430225665701" />
                  <Point X="24.130862042497" Y="-4.332821504746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.725897677279" Y="-3.752276011091" />
                  <Point X="25.777928481676" Y="-3.733338347026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.117367717522" Y="-3.245822334569" />
                  <Point X="27.255449020934" Y="-3.195564850219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.788684778242" Y="-2.637512672446" />
                  <Point X="28.997797189983" Y="-2.561401978957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.844494190274" Y="-4.33595399062" />
                  <Point X="24.160878150368" Y="-4.220799646547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.70121597942" Y="-3.660162526057" />
                  <Point X="25.765227435347" Y="-3.636864261448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.056728903337" Y="-3.166796169589" />
                  <Point X="27.207216144627" Y="-3.112023293122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.681285788053" Y="-2.57550581968" />
                  <Point X="28.908433595899" Y="-2.492830778845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.825742070837" Y="-4.241682315539" />
                  <Point X="24.190894258239" Y="-4.108777788348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.676534281562" Y="-3.568049041024" />
                  <Point X="25.752526389018" Y="-3.540390175871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.996090089152" Y="-3.087770004608" />
                  <Point X="27.158983268321" Y="-3.028481736026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.573886797864" Y="-2.513498966914" />
                  <Point X="28.819070001816" Y="-2.424259578733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.801004931422" Y="-4.149589009582" />
                  <Point X="24.22091036611" Y="-3.996755930149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.651069796812" Y="-3.476220467119" />
                  <Point X="25.739825342689" Y="-3.443916090293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.935451274966" Y="-3.008743839628" />
                  <Point X="27.110750392014" Y="-2.944940178929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.466487807675" Y="-2.451492114148" />
                  <Point X="28.729706407732" Y="-2.355688378622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.747068980904" Y="-4.068123201742" />
                  <Point X="24.250926473981" Y="-3.88473407195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.605490209243" Y="-3.391713191899" />
                  <Point X="25.72712355278" Y="-3.347442275357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.874812460781" Y="-2.929717674647" />
                  <Point X="27.06362428373" Y="-2.860995791216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.359088817485" Y="-2.389485261382" />
                  <Point X="28.640342813648" Y="-2.28711717851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.666618843149" Y="-3.996307768843" />
                  <Point X="24.280942581853" Y="-3.772712213751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.549473682453" Y="-3.311004651892" />
                  <Point X="25.730761590579" Y="-3.245021249502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.810983730931" Y="-2.851852544018" />
                  <Point X="27.036439032425" Y="-2.769793525117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.251689827296" Y="-2.327478408615" />
                  <Point X="28.550979219564" Y="-2.218545978398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.585151600325" Y="-3.924862531913" />
                  <Point X="24.310958689724" Y="-3.660690355553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.493457155663" Y="-3.230296111885" />
                  <Point X="25.754622690579" Y="-3.13523963096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.717508941763" Y="-2.784777696544" />
                  <Point X="27.019306739818" Y="-2.674932281285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.144290837107" Y="-2.265471555849" />
                  <Point X="28.46161562548" Y="-2.149974778287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.503164373202" Y="-3.853606553791" />
                  <Point X="24.340974797595" Y="-3.548668497354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.433043297056" Y="-3.15118806977" />
                  <Point X="25.778483790579" Y="-3.025458012417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.617102256361" Y="-2.720225852967" />
                  <Point X="27.002180355382" Y="-2.580068887056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.036891846918" Y="-2.203464703083" />
                  <Point X="28.372252031396" Y="-2.081403578175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.452367293669" Y="-4.134968524609" />
                  <Point X="22.543303589481" Y="-4.101870419719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.37741168599" Y="-3.79827990043" />
                  <Point X="24.380424478153" Y="-3.433213099494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.33380636962" Y="-3.086210469112" />
                  <Point X="25.834722718333" Y="-2.903891828323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.505326239375" Y="-2.659812107669" />
                  <Point X="27.008394871905" Y="-2.476710099635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.929492856729" Y="-2.141457850317" />
                  <Point X="28.282888437312" Y="-2.012832378063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.349537946619" Y="-4.071298457759" />
                  <Point X="22.728782016391" Y="-3.93326490484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.144778599255" Y="-3.781854531122" />
                  <Point X="24.471092043229" Y="-3.299115916208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.184321894099" Y="-3.039521480301" />
                  <Point X="26.034746450789" Y="-2.729992255177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.21526660426" Y="-2.664288292628" />
                  <Point X="27.062767599968" Y="-2.355823156679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.818169696765" Y="-2.080879278543" />
                  <Point X="28.193524843229" Y="-1.944261177952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.247466753053" Y="-4.007352445608" />
                  <Point X="24.568008993496" Y="-3.16274414273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.021228058259" Y="-2.997785893554" />
                  <Point X="27.131194546016" Y="-2.229820896711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.643620875712" Y="-2.043312965448" />
                  <Point X="28.104161249145" Y="-1.87568997784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.158310939744" Y="-3.938705619479" />
                  <Point X="28.021033429891" Y="-1.804849141303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.069155126436" Y="-3.87005879335" />
                  <Point X="27.96410986555" Y="-1.724470735966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.051575789343" Y="-3.775360260405" />
                  <Point X="27.911439249143" Y="-1.642544384173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.530520447168" Y="-1.053247021232" />
                  <Point X="29.752135794142" Y="-0.972585631477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.125472657971" Y="-3.647367111433" />
                  <Point X="27.870480580224" Y="-1.55635523211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.326540923196" Y="-1.026392607973" />
                  <Point X="29.771666972117" Y="-0.864379975669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.199369526599" Y="-3.519373962462" />
                  <Point X="27.860223207061" Y="-1.458991722238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.122561399225" Y="-0.999538194713" />
                  <Point X="29.755956007611" Y="-0.769001410716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.273266395226" Y="-3.391380813491" />
                  <Point X="27.869848036939" Y="-1.354391682267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.918581875253" Y="-0.972683781453" />
                  <Point X="29.595972212674" Y="-0.726133861652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.347163263854" Y="-3.26338766452" />
                  <Point X="27.88694547471" Y="-1.247071835451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.714602351281" Y="-0.945829368193" />
                  <Point X="29.435988417738" Y="-0.683266312589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.421060132482" Y="-3.135394515549" />
                  <Point X="27.971051681018" Y="-1.115362791453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.51062282731" Y="-0.918974954933" />
                  <Point X="29.276004622802" Y="-0.640398763525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.494957001109" Y="-3.007401366578" />
                  <Point X="29.116020827865" Y="-0.597531214462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.568853869737" Y="-2.879408217607" />
                  <Point X="28.956037032929" Y="-0.554663665398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.642750738364" Y="-2.751415068635" />
                  <Point X="28.796053237993" Y="-0.511796116335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.68662193375" Y="-2.634350370988" />
                  <Point X="28.645627106267" Y="-0.465449862354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.678808011899" Y="-2.53609751757" />
                  <Point X="28.537584938653" Y="-0.403677107026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.178910117386" Y="-2.980918817226" />
                  <Point X="21.427086078518" Y="-2.890590154513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.634031653146" Y="-2.451297890969" />
                  <Point X="28.460206331297" Y="-0.330743728487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.118653332336" Y="-2.901753605011" />
                  <Point X="21.900872926879" Y="-2.617048955938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.551262244891" Y="-2.380326603497" />
                  <Point X="28.401318513466" Y="-0.251080252953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.058396547287" Y="-2.822588392797" />
                  <Point X="28.36826675132" Y="-0.162013222179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.998139762237" Y="-2.743423180582" />
                  <Point X="28.350909260969" Y="-0.067233943623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.948217658225" Y="-2.660496452089" />
                  <Point X="28.356063552589" Y="0.03573895349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.898680020817" Y="-2.577429789196" />
                  <Point X="28.381178389745" Y="0.145976895039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.849142383408" Y="-2.494363126304" />
                  <Point X="28.468613967658" Y="0.2788977312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.328850119948" Y="0.591998085074" />
                  <Point X="29.77743497854" Y="0.755269621143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.915521774235" Y="-2.369106115489" />
                  <Point X="29.762538227293" Y="0.850944535488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.166160147256" Y="-2.176784319759" />
                  <Point X="29.745029436082" Y="0.945668745034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.416798520277" Y="-1.984462524029" />
                  <Point X="29.722421482918" Y="1.03853701141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.667436893297" Y="-1.7921407283" />
                  <Point X="29.610586425187" Y="1.098929267634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.918075266318" Y="-1.59981893257" />
                  <Point X="29.175420356354" Y="1.041638660001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.033550077563" Y="-1.456692650084" />
                  <Point X="28.740254287521" Y="0.984348052368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.051162993247" Y="-1.349185184651" />
                  <Point X="28.392361891151" Y="0.958822463747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.031170505118" Y="-1.255364966854" />
                  <Point X="28.239320407198" Y="1.004216807366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.971874834018" Y="-1.17584993777" />
                  <Point X="28.141008052609" Y="1.06953092502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.865684916663" Y="-1.113403018481" />
                  <Point X="28.080009161441" Y="1.148426032697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.549708804497" Y="-1.127312029664" />
                  <Point X="28.033204467234" Y="1.232487405567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.114542113135" Y="-1.184602863878" />
                  <Point X="28.01033692756" Y="1.325261190179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.679375421773" Y="-1.241893698093" />
                  <Point X="28.001922370037" Y="1.423295430092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.333930946962" Y="-1.266528316131" />
                  <Point X="28.027646883814" Y="1.533755275783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.310303853918" Y="-1.174030986336" />
                  <Point X="28.098982832806" Y="1.660816326234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.286676760874" Y="-1.081533656541" />
                  <Point X="28.284557120407" Y="1.829456731551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.26304966783" Y="-0.989036326746" />
                  <Point X="28.53519585504" Y="2.021778658897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.247711169183" Y="-0.893522195306" />
                  <Point X="28.785834589672" Y="2.214100586243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.23396788703" Y="-0.797427452546" />
                  <Point X="29.036473324304" Y="2.406422513588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.220224604877" Y="-0.701332709786" />
                  <Point X="20.724977159313" Y="-0.517617804301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.666488735885" Y="-0.174935615212" />
                  <Point X="29.106835276168" Y="2.533129058077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.703263550443" Y="-0.060453788957" />
                  <Point X="27.32672334203" Y="1.986318188774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.968894877126" Y="2.220049512842" />
                  <Point X="29.056700804277" Y="2.615978490983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.696222462968" Y="0.03808035317" />
                  <Point X="27.203982935042" Y="2.042741222474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.442683850039" Y="2.493591484691" />
                  <Point X="29.000964027765" Y="2.696788851764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.658482964259" Y="0.12544118737" />
                  <Point X="27.11925230355" Y="2.112998683065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.916472822952" Y="2.76713345654" />
                  <Point X="28.943318224941" Y="2.776904383791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.587153903192" Y="0.200576420688" />
                  <Point X="27.062040881809" Y="2.193272316876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.464298135163" Y="0.256957466403" />
                  <Point X="27.025729049819" Y="2.281152779266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.304314304155" Y="0.299825002337" />
                  <Point X="27.013124384327" Y="2.377661944599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.144330473148" Y="0.342692538272" />
                  <Point X="27.026020710034" Y="2.483452711673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.98434664214" Y="0.385560074207" />
                  <Point X="27.065497241167" Y="2.598917882342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.824362811133" Y="0.428427610141" />
                  <Point X="27.139393870204" Y="2.72691094411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.664378980125" Y="0.471295146076" />
                  <Point X="27.213290499242" Y="2.854904005877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.504395149118" Y="0.514162682011" />
                  <Point X="27.287187128279" Y="2.982897067645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.34441131811" Y="0.557030217945" />
                  <Point X="27.361083757316" Y="3.110890129412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.217532227449" Y="0.611946893979" />
                  <Point X="27.434980386354" Y="3.23888319118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.233343794463" Y="0.718798722114" />
                  <Point X="27.508877015391" Y="3.366876252947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.249155361476" Y="0.825650550249" />
                  <Point X="21.305447947577" Y="1.210109610266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.438002623017" Y="1.258355566539" />
                  <Point X="27.582773644428" Y="3.494869314714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.26496692849" Y="0.932502378385" />
                  <Point X="21.070197162291" Y="1.225582215219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.548221818672" Y="1.399568961387" />
                  <Point X="27.656670273466" Y="3.622862376482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.291945961835" Y="1.043418831857" />
                  <Point X="20.866217664226" Y="1.252436637908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.578274387315" Y="1.511604090222" />
                  <Point X="27.730566902503" Y="3.750855438249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.322337831247" Y="1.155577456072" />
                  <Point X="20.662238166161" Y="1.279291060597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.562978079963" Y="1.607133578037" />
                  <Point X="27.804463531541" Y="3.878848500017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.35272970066" Y="1.267736080287" />
                  <Point X="20.458258668096" Y="1.306145483286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.520929718555" Y="1.69292611447" />
                  <Point X="27.740694846514" Y="3.956735485174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.455042855786" Y="1.770042145978" />
                  <Point X="27.646661703496" Y="4.023607108466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.366049435337" Y="1.838748078274" />
                  <Point X="27.542198963784" Y="4.086682669006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.276685814398" Y="1.907319268611" />
                  <Point X="27.435395790727" Y="4.148906381473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.187322193459" Y="1.975890458948" />
                  <Point X="27.32859261767" Y="4.211130093941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.097958572519" Y="2.044461649286" />
                  <Point X="27.217828045418" Y="4.271911975015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.00859495158" Y="2.113032839623" />
                  <Point X="27.096264381103" Y="4.328763308021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.919231330641" Y="2.18160402996" />
                  <Point X="26.974700716788" Y="4.385614641028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.829867709702" Y="2.250175220297" />
                  <Point X="26.852396838821" Y="4.442196558297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.799118482745" Y="2.340080305343" />
                  <Point X="21.868040095539" Y="2.729135955164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.165414927652" Y="2.837371542473" />
                  <Point X="26.713274356218" Y="4.492657004098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.874044774134" Y="2.468448133558" />
                  <Point X="21.721029017183" Y="2.77672518692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.244557225714" Y="2.967273871624" />
                  <Point X="26.574151873614" Y="4.543117449898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.948971065524" Y="2.596815961773" />
                  <Point X="21.613629810209" Y="2.838731960783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.24896751432" Y="3.069975973787" />
                  <Point X="25.051803350281" Y="4.090124789611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.121358538751" Y="4.115440807853" />
                  <Point X="26.431310930837" Y="4.592224486878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.036061633681" Y="2.729611224653" />
                  <Point X="21.506230603235" Y="2.900738734646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.236653773092" Y="3.166591026892" />
                  <Point X="24.873546106062" Y="4.126341347058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.236319373327" Y="4.25838001813" />
                  <Point X="26.264319139994" Y="4.63254133403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.145782766494" Y="2.870643339452" />
                  <Point X="21.398831396261" Y="2.962745508509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.198233002873" Y="3.25370389854" />
                  <Point X="24.800851724891" Y="4.200979644499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.267182957892" Y="4.37071033262" />
                  <Point X="26.097327349151" Y="4.672858181181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.255503899308" Y="3.011675454251" />
                  <Point X="21.291432189287" Y="3.024752282372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.150000278959" Y="3.337245511103" />
                  <Point X="24.766837830406" Y="4.28969648774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.297199166262" Y="4.482732227397" />
                  <Point X="25.930335558308" Y="4.713175028333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.101767555046" Y="3.420787123666" />
                  <Point X="24.74215626883" Y="4.381810022376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.327215374632" Y="4.594754122174" />
                  <Point X="25.744521904061" Y="4.746641277452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.053534831132" Y="3.504328736229" />
                  <Point X="24.717474707255" Y="4.473923557013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.357231583001" Y="4.706776016952" />
                  <Point X="25.528824991277" Y="4.76923090996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.005302107218" Y="3.587870348792" />
                  <Point X="24.692793145679" Y="4.566037091649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.957069383305" Y="3.671411961356" />
                  <Point X="23.03824480962" Y="4.064927634554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.492449387727" Y="4.230244581253" />
                  <Point X="24.668111584103" Y="4.658150626286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.106000239714" Y="3.826715248437" />
                  <Point X="22.929059820693" Y="4.126284436941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.528582283488" Y="4.344492768173" />
                  <Point X="24.643430022527" Y="4.750264160922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.362980527994" Y="4.02134531255" />
                  <Point X="22.85855999683" Y="4.20172148792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.53579996144" Y="4.448216676493" />
                  <Point X="24.368897593199" Y="4.751439416691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.523099290727" Y="4.544690898783" />
                  <Point X="23.745894042494" Y="4.625781556778" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.9998359375" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.464322265625" Y="-3.521547119141" />
                  <Point X="25.30308984375" Y="-3.289243652344" />
                  <Point X="25.30059375" Y="-3.285646728516" />
                  <Point X="25.2743359375" Y="-3.266399658203" />
                  <Point X="25.02483984375" Y="-3.188965332031" />
                  <Point X="25.0209765625" Y="-3.187766357422" />
                  <Point X="24.991333984375" Y="-3.187767333984" />
                  <Point X="24.741837890625" Y="-3.265201416016" />
                  <Point X="24.737974609375" Y="-3.266400634766" />
                  <Point X="24.71171875" Y="-3.285647949219" />
                  <Point X="24.55048828125" Y="-3.517951171875" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.534666015625" Y="-3.559907714844" />
                  <Point X="24.152255859375" Y="-4.987077148438" />
                  <Point X="23.90285546875" Y="-4.938667480469" />
                  <Point X="23.89976171875" Y="-4.938066894531" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.630712890625" Y="-4.23510546875" />
                  <Point X="23.629787109375" Y="-4.230456542969" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.384013671875" Y="-4.001183349609" />
                  <Point X="23.38045703125" Y="-3.998064208984" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.045890625" Y="-3.965780761719" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.756087890625" Y="-4.143530761719" />
                  <Point X="22.752146484375" Y="-4.146165527344" />
                  <Point X="22.736837890625" Y="-4.161763183594" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.148703125" Y="-4.170419433594" />
                  <Point X="22.144169921875" Y="-4.167612304688" />
                  <Point X="21.771419921875" Y="-3.880607421875" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597593261719" />
                  <Point X="22.486021484375" Y="-2.568764404297" />
                  <Point X="22.4689375" Y="-2.551680419922" />
                  <Point X="22.46873828125" Y="-2.551480957031" />
                  <Point X="22.439890625" Y="-2.537204833984" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="22.38865625" Y="-2.554820800781" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.841880859375" Y="-2.851836914062" />
                  <Point X="20.838296875" Y="-2.847128173828" />
                  <Point X="20.56898046875" Y="-2.395527099609" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396014892578" />
                  <Point X="21.861765625" Y="-1.366721069336" />
                  <Point X="21.861890625" Y="-1.366236206055" />
                  <Point X="21.85966796875" Y="-1.334581665039" />
                  <Point X="21.838841796875" Y="-1.310639526367" />
                  <Point X="21.812763671875" Y="-1.295290527344" />
                  <Point X="21.812337890625" Y="-1.295039550781" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.75565234375" Y="-1.291838623047" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.074009765625" Y="-1.016680786133" />
                  <Point X="20.072607421875" Y="-1.011191772461" />
                  <Point X="20.001603515625" Y="-0.514742553711" />
                  <Point X="21.442537109375" Y="-0.128645339966" />
                  <Point X="21.45810546875" Y="-0.121425285339" />
                  <Point X="21.485435546875" Y="-0.102456840515" />
                  <Point X="21.485828125" Y="-0.102183486938" />
                  <Point X="21.505103515625" Y="-0.075906921387" />
                  <Point X="21.514212890625" Y="-0.0465545578" />
                  <Point X="21.514365234375" Y="-0.046059268951" />
                  <Point X="21.514353515625" Y="-0.01646002388" />
                  <Point X="21.505244140625" Y="0.012892336845" />
                  <Point X="21.505115234375" Y="0.013306093216" />
                  <Point X="21.485859375" Y="0.039603046417" />
                  <Point X="21.458529296875" Y="0.058571491241" />
                  <Point X="21.442537109375" Y="0.066085273743" />
                  <Point X="21.4199140625" Y="0.072147567749" />
                  <Point X="20.001814453125" Y="0.452126098633" />
                  <Point X="20.081453125" Y="0.9903203125" />
                  <Point X="20.08235546875" Y="0.996415039063" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866699219" />
                  <Point X="21.328771484375" Y="1.414934570312" />
                  <Point X="21.329708984375" Y="1.415229736328" />
                  <Point X="21.3484609375" Y="1.426051269531" />
                  <Point X="21.360880859375" Y="1.443786010742" />
                  <Point X="21.38515234375" Y="1.50238293457" />
                  <Point X="21.38554296875" Y="1.503326538086" />
                  <Point X="21.38928515625" Y="1.524624511719" />
                  <Point X="21.383685546875" Y="1.545510986328" />
                  <Point X="21.354400390625" Y="1.60176965332" />
                  <Point X="21.35391015625" Y="1.602706054688" />
                  <Point X="21.34003125" Y="1.619221801758" />
                  <Point X="21.3270546875" Y="1.62917980957" />
                  <Point X="20.52389453125" Y="2.245466308594" />
                  <Point X="20.836474609375" Y="2.780992919922" />
                  <Point X="20.8399921875" Y="2.787013183594" />
                  <Point X="21.225328125" Y="3.282311035156" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861484375" Y="2.920435058594" />
                  <Point X="21.94573046875" Y="2.913064453125" />
                  <Point X="21.947033203125" Y="2.912950439453" />
                  <Point X="21.9684921875" Y="2.915775146484" />
                  <Point X="21.986748046875" Y="2.927404785156" />
                  <Point X="22.046546875" Y="2.987202392578" />
                  <Point X="22.04747265625" Y="2.988128173828" />
                  <Point X="22.0591015625" Y="3.006382568359" />
                  <Point X="22.061927734375" Y="3.027841552734" />
                  <Point X="22.054556640625" Y="3.112086669922" />
                  <Point X="22.0544453125" Y="3.113358886719" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="22.042181640625" Y="3.143993408203" />
                  <Point X="21.69272265625" Y="3.749276367188" />
                  <Point X="22.2410234375" Y="4.169652832031" />
                  <Point X="22.247126953125" Y="4.174333007812" />
                  <Point X="22.858453125" Y="4.513971191406" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.142517578125" Y="4.224849609375" />
                  <Point X="23.143984375" Y="4.224086425781" />
                  <Point X="23.164884765625" Y="4.218491699219" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.28383984375" Y="4.262697753906" />
                  <Point X="23.285353515625" Y="4.26332421875" />
                  <Point X="23.303091796875" Y="4.275738769531" />
                  <Point X="23.31391796875" Y="4.294488769531" />
                  <Point X="23.345705078125" Y="4.395304199219" />
                  <Point X="23.346197265625" Y="4.396865234375" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.3474296875" Y="4.423393066406" />
                  <Point X="23.31086328125" Y="4.701141113281" />
                  <Point X="24.024001953125" Y="4.901080078125" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.9646953125" Y="4.295891113281" />
                  <Point X="24.98988671875" Y="4.276560546875" />
                  <Point X="25.02242578125" Y="4.276560546875" />
                  <Point X="25.0476171875" Y="4.295891113281" />
                  <Point X="25.057400390625" Y="4.321900390625" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.85330078125" Y="4.926288574219" />
                  <Point X="25.86020703125" Y="4.925564941406" />
                  <Point X="26.501185546875" Y="4.770813476562" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.926623046875" Y="4.617386230469" />
                  <Point X="26.931033203125" Y="4.615786621094" />
                  <Point X="27.334462890625" Y="4.427115722656" />
                  <Point X="27.33869921875" Y="4.425134765625" />
                  <Point X="27.728419921875" Y="4.198082519531" />
                  <Point X="27.732525390625" Y="4.195690917969" />
                  <Point X="28.0687421875" Y="3.956593261719" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491514892578" />
                  <Point X="27.2037109375" Y="2.412452880859" />
                  <Point X="27.2037109375" Y="2.412452636719" />
                  <Point X="27.202044921875" Y="2.392325439453" />
                  <Point X="27.21028515625" Y="2.323991699219" />
                  <Point X="27.210412109375" Y="2.322933105469" />
                  <Point X="27.218681640625" Y="2.3008125" />
                  <Point X="27.260984375" Y="2.238469238281" />
                  <Point X="27.27494140625" Y="2.224204101562" />
                  <Point X="27.337283203125" Y="2.181901367187" />
                  <Point X="27.33828515625" Y="2.181221923828" />
                  <Point X="27.3603359375" Y="2.17298046875" />
                  <Point X="27.428701171875" Y="2.164736572266" />
                  <Point X="27.448662109375" Y="2.165945556641" />
                  <Point X="27.527724609375" Y="2.187087890625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="27.563791015625" Y="2.205555664062" />
                  <Point X="28.99425" Y="3.031431152344" />
                  <Point X="29.20016015625" Y="2.745262207031" />
                  <Point X="29.20258984375" Y="2.741885986328" />
                  <Point X="29.387513671875" Y="2.436294921875" />
                  <Point X="28.2886171875" Y="1.593082763672" />
                  <Point X="28.27937109375" Y="1.583833007812" />
                  <Point X="28.222470703125" Y="1.509601074219" />
                  <Point X="28.22246875" Y="1.509596923828" />
                  <Point X="28.21312109375" Y="1.491499755859" />
                  <Point X="28.1919453125" Y="1.41578125" />
                  <Point X="28.191615234375" Y="1.414608032227" />
                  <Point X="28.19078125" Y="1.390960693359" />
                  <Point X="28.208181640625" Y="1.306633544922" />
                  <Point X="28.215646484375" Y="1.287955322266" />
                  <Point X="28.26296484375" Y="1.216032470703" />
                  <Point X="28.263697265625" Y="1.214918579102" />
                  <Point X="28.280947265625" Y="1.198819946289" />
                  <Point X="28.34952734375" Y="1.160215209961" />
                  <Point X="28.349541015625" Y="1.160207519531" />
                  <Point X="28.36856640625" Y="1.153619506836" />
                  <Point X="28.461291015625" Y="1.141364868164" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.497419921875" Y="1.144017700195" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.938142578125" Y="0.955681152344" />
                  <Point X="29.93919140625" Y="0.951371887207" />
                  <Point X="29.997861328125" Y="0.574556274414" />
                  <Point X="28.74116796875" Y="0.237826843262" />
                  <Point X="28.729087890625" Y="0.232819335938" />
                  <Point X="28.63798828125" Y="0.180162307739" />
                  <Point X="28.637982421875" Y="0.180158798218" />
                  <Point X="28.622265625" Y="0.166926818848" />
                  <Point X="28.56760546875" Y="0.097277633667" />
                  <Point X="28.566748046875" Y="0.096184066772" />
                  <Point X="28.556986328125" Y="0.074735519409" />
                  <Point X="28.538765625" Y="-0.020401813507" />
                  <Point X="28.538759765625" Y="-0.020430257797" />
                  <Point X="28.538484375" Y="-0.040685150146" />
                  <Point X="28.55669921875" Y="-0.1357940979" />
                  <Point X="28.55698046875" Y="-0.137267288208" />
                  <Point X="28.566759765625" Y="-0.158759185791" />
                  <Point X="28.621408203125" Y="-0.228393447876" />
                  <Point X="28.621408203125" Y="-0.228393463135" />
                  <Point X="28.636578125" Y="-0.241907012939" />
                  <Point X="28.727677734375" Y="-0.294564056396" />
                  <Point X="28.74116796875" Y="-0.300386901855" />
                  <Point X="28.7609921875" Y="-0.305698669434" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.949015625" Y="-0.962526611328" />
                  <Point X="29.948431640625" Y="-0.966403686523" />
                  <Point X="29.874546875" Y="-1.290178466797" />
                  <Point X="28.411982421875" Y="-1.097628295898" />
                  <Point X="28.394833984375" Y="-1.098341796875" />
                  <Point X="28.216037109375" Y="-1.137203857422" />
                  <Point X="28.21326953125" Y="-1.137805541992" />
                  <Point X="28.1854453125" Y="-1.154697387695" />
                  <Point X="28.077375" Y="-1.284672973633" />
                  <Point X="28.07570703125" Y="-1.286679077148" />
                  <Point X="28.064359375" Y="-1.314070922852" />
                  <Point X="28.04887109375" Y="-1.482386962891" />
                  <Point X="28.048630859375" Y="-1.484993286133" />
                  <Point X="28.056361328125" Y="-1.516621826172" />
                  <Point X="28.15530859375" Y="-1.670529174805" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.186859375" Y="-1.699656616211" />
                  <Point X="29.33907421875" Y="-2.583783447266" />
                  <Point X="29.205775390625" Y="-2.799480957031" />
                  <Point X="29.204126953125" Y="-2.802148925781" />
                  <Point X="29.056689453125" Y="-3.011637695312" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.73733984375" Y="-2.2533125" />
                  <Point X="27.524544921875" Y="-2.214881835938" />
                  <Point X="27.52125" Y="-2.214286865234" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.312296875" Y="-2.312283203125" />
                  <Point X="27.3095546875" Y="-2.313727539062" />
                  <Point X="27.2886015625" Y="-2.33468359375" />
                  <Point X="27.1955625" Y="-2.511464355469" />
                  <Point X="27.194126953125" Y="-2.514191894531" />
                  <Point X="27.1891640625" Y="-2.546374755859" />
                  <Point X="27.227595703125" Y="-2.759170166016" />
                  <Point X="27.227595703125" Y="-2.759179199219" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.2459140625" Y="-2.799053955078" />
                  <Point X="27.98667578125" Y="-4.082087890625" />
                  <Point X="27.83725390625" Y="-4.188815917969" />
                  <Point X="27.8353046875" Y="-4.190208496094" />
                  <Point X="27.679775390625" Y="-4.290879394531" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980466796875" />
                  <Point X="26.46067578125" Y="-2.845537353516" />
                  <Point X="26.45742578125" Y="-2.843447998047" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.1962734375" Y="-2.856838867188" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509765625" />
                  <Point X="25.98809375" Y="-3.015878417969" />
                  <Point X="25.985349609375" Y="-3.01816015625" />
                  <Point X="25.96845703125" Y="-3.045986572266" />
                  <Point X="25.91546875" Y="-3.289778320312" />
                  <Point X="25.91546875" Y="-3.289778076172" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.91728125" Y="-3.336166748047" />
                  <Point X="26.127642578125" Y="-4.934028320312" />
                  <Point X="25.996189453125" Y="-4.962842773438" />
                  <Point X="25.994333984375" Y="-4.963249511719" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#216" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.190297965249" Y="5.0653261847" Z="2.5" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.5" />
                  <Point X="-0.205301466071" Y="5.074911947802" Z="2.5" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.5" />
                  <Point X="-0.995652588278" Y="4.980503814645" Z="2.5" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.5" />
                  <Point X="-1.7082996835" Y="4.448146597217" Z="2.5" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.5" />
                  <Point X="-1.708138087686" Y="4.441619523077" Z="2.5" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.5" />
                  <Point X="-1.741437351441" Y="4.340177718811" Z="2.5" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.5" />
                  <Point X="-1.840550907092" Y="4.300480373425" Z="2.5" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.5" />
                  <Point X="-2.131240526435" Y="4.605929519272" Z="2.5" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.5" />
                  <Point X="-2.144235126693" Y="4.604377896997" Z="2.5" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.5" />
                  <Point X="-2.795559906076" Y="4.240609982665" Z="2.5" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.5" />
                  <Point X="-3.007275393598" Y="3.150273425835" Z="2.5" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.5" />
                  <Point X="-3.001410560372" Y="3.139008452092" Z="2.5" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.5" />
                  <Point X="-2.994965925961" Y="3.053837641926" Z="2.5" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.5" />
                  <Point X="-3.056068013515" Y="2.994154138801" Z="2.5" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.5" />
                  <Point X="-3.783585922462" Y="3.372918555147" Z="2.5" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.5" />
                  <Point X="-3.799861084417" Y="3.370552673632" Z="2.5" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.5" />
                  <Point X="-4.212859457823" Y="2.837483301517" Z="2.5" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.5" />
                  <Point X="-3.709539753124" Y="1.62079207813" Z="2.5" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.5" />
                  <Point X="-3.696108816359" Y="1.609963006463" Z="2.5" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.5" />
                  <Point X="-3.667198258572" Y="1.552797058877" Z="2.5" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.5" />
                  <Point X="-3.692406534916" Y="1.493904793318" Z="2.5" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.5" />
                  <Point X="-4.800278107478" Y="1.612723037554" Z="2.5" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.5" />
                  <Point X="-4.818879692203" Y="1.606061209653" Z="2.5" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.5" />
                  <Point X="-4.97416491715" Y="1.028918518768" Z="2.5" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.5" />
                  <Point X="-3.59918522931" Y="0.055131246957" Z="2.5" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.5" />
                  <Point X="-3.576137559381" Y="0.04877532354" Z="2.5" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.5" />
                  <Point X="-3.548666798223" Y="0.029352454173" Z="2.5" />
                  <Point X="-3.539556741714" Y="0" Z="2.5" />
                  <Point X="-3.539697802912" Y="-0.000454496888" Z="2.5" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.5" />
                  <Point X="-3.549231035742" Y="-0.030100659594" Z="2.5" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.5" />
                  <Point X="-5.037702559463" Y="-0.440580809799" Z="2.5" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.5" />
                  <Point X="-5.059142810836" Y="-0.454923121477" Z="2.5" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.5" />
                  <Point X="-4.980591677739" Y="-0.997774967371" Z="2.5" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.5" />
                  <Point X="-3.243978762234" Y="-1.310130905777" Z="2.5" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.5" />
                  <Point X="-3.218755081009" Y="-1.307100971095" Z="2.5" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.5" />
                  <Point X="-3.192793784061" Y="-1.3229033917" Z="2.5" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.5" />
                  <Point X="-4.483039756779" Y="-2.336415960968" Z="2.5" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.5" />
                  <Point X="-4.498424610343" Y="-2.359161273806" Z="2.5" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.5" />
                  <Point X="-4.204062047001" Y="-2.850840780243" Z="2.5" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.5" />
                  <Point X="-2.5925002819" Y="-2.566842129031" Z="2.5" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.5" />
                  <Point X="-2.572574967876" Y="-2.555755501255" Z="2.5" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.5" />
                  <Point X="-3.288574884137" Y="-3.842578100592" Z="2.5" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.5" />
                  <Point X="-3.293682734016" Y="-3.867046023603" Z="2.5" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.5" />
                  <Point X="-2.88377617712" Y="-4.181650216827" Z="2.5" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.5" />
                  <Point X="-2.229651486684" Y="-4.160921210869" Z="2.5" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.5" />
                  <Point X="-2.222288802539" Y="-4.153823911689" Z="2.5" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.5" />
                  <Point X="-1.963535376207" Y="-3.984393773903" Z="2.5" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.5" />
                  <Point X="-1.655110083492" Y="-4.007495138604" Z="2.5" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.5" />
                  <Point X="-1.42448305568" Y="-4.213580351293" Z="2.5" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.5" />
                  <Point X="-1.412363783299" Y="-4.873918343503" Z="2.5" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.5" />
                  <Point X="-1.408590254488" Y="-4.880663324148" Z="2.5" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.5" />
                  <Point X="-1.112831492951" Y="-4.956470692524" Z="2.5" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.5" />
                  <Point X="-0.423194850433" Y="-3.541568709474" Z="2.5" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.5" />
                  <Point X="-0.414590257354" Y="-3.515176065645" Z="2.5" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.5" />
                  <Point X="-0.249495855884" Y="-3.281673674848" Z="2.5" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.5" />
                  <Point X="0.003863223476" Y="-3.20543837044" Z="2.5" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.5" />
                  <Point X="0.255855602078" Y="-3.286469690123" Z="2.5" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.5" />
                  <Point X="0.811560009968" Y="-4.990967320342" Z="2.5" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.5" />
                  <Point X="0.820417939353" Y="-5.013263401872" Z="2.5" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.5" />
                  <Point X="1.000740206392" Y="-4.980402952187" Z="2.5" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.5" />
                  <Point X="0.960695882631" Y="-3.298360218998" Z="2.5" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.5" />
                  <Point X="0.958166344025" Y="-3.269138473286" Z="2.5" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.5" />
                  <Point X="1.013904411061" Y="-3.023044139116" Z="2.5" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.5" />
                  <Point X="1.194697861121" Y="-2.875348387402" Z="2.5" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.5" />
                  <Point X="1.427480166139" Y="-2.856315852334" Z="2.5" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.5" />
                  <Point X="2.646422138372" Y="-4.306288336414" Z="2.5" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.5" />
                  <Point X="2.665023494066" Y="-4.324723779245" Z="2.5" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.5" />
                  <Point X="2.860159098462" Y="-4.198222944594" Z="2.5" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.5" />
                  <Point X="2.283058313457" Y="-2.742773908323" Z="2.5" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.5" />
                  <Point X="2.270641830715" Y="-2.719003671479" Z="2.5" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.5" />
                  <Point X="2.233651731628" Y="-2.503470888155" Z="2.5" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.5" />
                  <Point X="2.329427490217" Y="-2.325249578339" Z="2.5" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.5" />
                  <Point X="2.509503132034" Y="-2.232806178776" Z="2.5" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.5" />
                  <Point X="4.044639688783" Y="-3.034691186818" Z="2.5" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.5" />
                  <Point X="4.067777389652" Y="-3.04272967923" Z="2.5" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.5" />
                  <Point X="4.242154101566" Y="-2.794484614797" Z="2.5" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.5" />
                  <Point X="3.211139034035" Y="-1.628708938924" Z="2.5" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.5" />
                  <Point X="3.191210698985" Y="-1.612209908391" Z="2.5" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.5" />
                  <Point X="3.092502341785" Y="-1.455696150305" Z="2.5" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.5" />
                  <Point X="3.109664836429" Y="-1.285359675513" Z="2.5" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.5" />
                  <Point X="3.220504024901" Y="-1.154782438881" Z="2.5" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.5" />
                  <Point X="4.884017200855" Y="-1.311387155841" Z="2.5" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.5" />
                  <Point X="4.908294155754" Y="-1.308772156914" Z="2.5" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.5" />
                  <Point X="4.992301071731" Y="-0.938700788063" Z="2.5" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.5" />
                  <Point X="3.76777607314" Y="-0.226122293549" Z="2.5" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.5" />
                  <Point X="3.746542105864" Y="-0.219995288379" Z="2.5" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.5" />
                  <Point X="3.654596358413" Y="-0.166259750576" Z="2.5" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.5" />
                  <Point X="3.59965460495" Y="-0.095137412217" Z="2.5" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.5" />
                  <Point X="3.581716845474" Y="0.001473118956" Z="2.5" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.5" />
                  <Point X="3.600783079987" Y="0.097688987999" Z="2.5" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.5" />
                  <Point X="3.656853308488" Y="0.168153555863" Z="2.5" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.5" />
                  <Point X="5.028191744166" Y="0.56384963922" Z="2.5" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.5" />
                  <Point X="5.047010239293" Y="0.57561546551" Z="2.5" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.5" />
                  <Point X="4.980567842339" Y="0.998787150061" Z="2.5" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.5" />
                  <Point X="3.48473797933" Y="1.224869958804" Z="2.5" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.5" />
                  <Point X="3.461685648805" Y="1.222213837023" Z="2.5" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.5" />
                  <Point X="3.367898715572" Y="1.235066338931" Z="2.5" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.5" />
                  <Point X="3.298585769598" Y="1.274784828066" Z="2.5" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.5" />
                  <Point X="3.25099157923" Y="1.348022134343" Z="2.5" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.5" />
                  <Point X="3.233920230973" Y="1.433522760962" Z="2.5" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.5" />
                  <Point X="3.255997049412" Y="1.510463119403" Z="2.5" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.5" />
                  <Point X="4.430014318854" Y="2.441888449425" Z="2.5" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.5" />
                  <Point X="4.444123092444" Y="2.460430835126" Z="2.5" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.5" />
                  <Point X="4.234586620218" Y="2.805746901938" Z="2.5" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.5" />
                  <Point X="2.532633804763" Y="2.28013688321" Z="2.5" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.5" />
                  <Point X="2.508653713849" Y="2.266671399139" Z="2.5" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.5" />
                  <Point X="2.428533064189" Y="2.245656821337" Z="2.5" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.5" />
                  <Point X="2.359201455744" Y="2.254555545308" Z="2.5" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.5" />
                  <Point X="2.296203054927" Y="2.297823404638" Z="2.5" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.5" />
                  <Point X="2.253772881421" Y="2.361225379643" Z="2.5" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.5" />
                  <Point X="2.245856543518" Y="2.430815862033" Z="2.5" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.5" />
                  <Point X="3.115488469649" Y="3.979505277192" Z="2.5" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.5" />
                  <Point X="3.122906615617" Y="4.006328888027" Z="2.5" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.5" />
                  <Point X="2.747433167234" Y="4.272565293294" Z="2.5" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.5" />
                  <Point X="2.349484631596" Y="4.503689454734" Z="2.5" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.5" />
                  <Point X="1.937515857713" Y="4.695669622845" Z="2.5" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.5" />
                  <Point X="1.506762649233" Y="4.850697268768" Z="2.5" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.5" />
                  <Point X="0.852352950287" Y="5.007296780804" Z="2.5" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.5" />
                  <Point X="0.002946596304" Y="4.366121321251" Z="2.5" />
                  <Point X="0" Y="4.355124473572" Z="2.5" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>