<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#123" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="471" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.58716015625" Y="-3.601559082031" />
                  <Point X="25.5633046875" Y="-3.512524902344" />
                  <Point X="25.553966796875" Y="-3.489878417969" />
                  <Point X="25.536994140625" Y="-3.460260742188" />
                  <Point X="25.53261328125" Y="-3.453328125" />
                  <Point X="25.37863671875" Y="-3.231476806641" />
                  <Point X="25.357109375" Y="-3.207521972656" />
                  <Point X="25.32480859375" Y="-3.185802246094" />
                  <Point X="25.294982421875" Y="-3.173693847656" />
                  <Point X="25.287408203125" Y="-3.170986083984" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0203984375" Y="-3.091593017578" />
                  <Point X="24.984712890625" Y="-3.092971435547" />
                  <Point X="24.9548046875" Y="-3.099901367188" />
                  <Point X="24.948087890625" Y="-3.10171875" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.680494140625" Y="-3.188985107422" />
                  <Point X="24.64985546875" Y="-3.213492431641" />
                  <Point X="24.628587890625" Y="-3.239359863281" />
                  <Point X="24.62392578125" Y="-3.245525878906" />
                  <Point X="24.46994921875" Y="-3.467377197266" />
                  <Point X="24.4618125" Y="-3.481571044922" />
                  <Point X="24.449009765625" Y="-3.512523925781" />
                  <Point X="24.083416015625" Y="-4.87694140625" />
                  <Point X="23.920671875" Y="-4.845352539063" />
                  <Point X="23.907275390625" Y="-4.841905761719" />
                  <Point X="23.753583984375" Y="-4.802362304688" />
                  <Point X="23.782166015625" Y="-4.585254882812" />
                  <Point X="23.7850390625" Y="-4.56344140625" />
                  <Point X="23.785171875" Y="-4.539695800781" />
                  <Point X="23.781033203125" Y="-4.505289550781" />
                  <Point X="23.77988671875" Y="-4.498101074219" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.7129609375" Y="-4.181754882812" />
                  <Point X="23.6919765625" Y="-4.148804199219" />
                  <Point X="23.667751953125" Y="-4.124029296875" />
                  <Point X="23.66246484375" Y="-4.119020996094" />
                  <Point X="23.443095703125" Y="-3.926639160156" />
                  <Point X="23.416794921875" Y="-3.908789550781" />
                  <Point X="23.380078125" Y="-3.895419433594" />
                  <Point X="23.345775390625" Y="-3.890511962891" />
                  <Point X="23.33853515625" Y="-3.8897578125" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.015646484375" Y="-3.872525634766" />
                  <Point X="22.978384765625" Y="-3.884268554688" />
                  <Point X="22.94818359375" Y="-3.901256835938" />
                  <Point X="22.941978515625" Y="-3.905067138672" />
                  <Point X="22.699376953125" Y="-4.067169921875" />
                  <Point X="22.6872109375" Y="-4.076825439453" />
                  <Point X="22.664900390625" Y="-4.0994609375" />
                  <Point X="22.5198515625" Y="-4.288490234375" />
                  <Point X="22.19828515625" Y="-4.089383056641" />
                  <Point X="22.1797578125" Y="-4.075117431641" />
                  <Point X="21.89527734375" Y="-3.856077636719" />
                  <Point X="22.53190234375" Y="-2.753410400391" />
                  <Point X="22.57623828125" Y="-2.676620117188" />
                  <Point X="22.587140625" Y="-2.647655517578" />
                  <Point X="22.593412109375" Y="-2.616129638672" />
                  <Point X="22.5943359375" Y="-2.584535644531" />
                  <Point X="22.58484375" Y="-2.554387451172" />
                  <Point X="22.56959375" Y="-2.524525390625" />
                  <Point X="22.552166015625" Y="-2.500559082031" />
                  <Point X="22.5358515625" Y="-2.484243652344" />
                  <Point X="22.5106953125" Y="-2.466215576172" />
                  <Point X="22.4818671875" Y="-2.451997558594" />
                  <Point X="22.452248046875" Y="-2.44301171875" />
                  <Point X="22.4213125" Y="-2.444023925781" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.1819765625" Y="-3.141801757812" />
                  <Point X="20.91714453125" Y="-2.7938671875" />
                  <Point X="20.903857421875" Y="-2.7715859375" />
                  <Point X="20.693857421875" Y="-2.419449707031" />
                  <Point X="21.816474609375" Y="-1.558036010742" />
                  <Point X="21.894044921875" Y="-1.498513183594" />
                  <Point X="21.9163203125" Y="-1.474210327148" />
                  <Point X="21.934744140625" Y="-1.445308227539" />
                  <Point X="21.9466015625" Y="-1.418062744141" />
                  <Point X="21.95384765625" Y="-1.390087036133" />
                  <Point X="21.95665234375" Y="-1.359667236328" />
                  <Point X="21.954447265625" Y="-1.328002319336" />
                  <Point X="21.947130859375" Y="-1.297495117188" />
                  <Point X="21.93027734375" Y="-1.271034790039" />
                  <Point X="21.907865234375" Y="-1.246143432617" />
                  <Point X="21.885451171875" Y="-1.227837890625" />
                  <Point X="21.860544921875" Y="-1.21317956543" />
                  <Point X="21.83128125" Y="-1.201955810547" />
                  <Point X="21.799392578125" Y="-1.195474609375" />
                  <Point X="21.7680703125" Y="-1.194383789062" />
                  <Point X="20.2678984375" Y="-1.391885131836" />
                  <Point X="20.165923828125" Y="-0.992652526855" />
                  <Point X="20.162408203125" Y="-0.968078918457" />
                  <Point X="20.107578125" Y="-0.584698242188" />
                  <Point X="21.378759765625" Y="-0.244085800171" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.4840625" Y="-0.214089279175" />
                  <Point X="21.5139296875" Y="-0.198318862915" />
                  <Point X="21.5400234375" Y="-0.180209213257" />
                  <Point X="21.56345703125" Y="-0.156965698242" />
                  <Point X="21.58325390625" Y="-0.128933349609" />
                  <Point X="21.596384765625" Y="-0.102288375854" />
                  <Point X="21.605083984375" Y="-0.074256790161" />
                  <Point X="21.60933984375" Y="-0.04443422699" />
                  <Point X="21.6087890625" Y="-0.013020145416" />
                  <Point X="21.604533203125" Y="0.0134715271" />
                  <Point X="21.595833984375" Y="0.041503417969" />
                  <Point X="21.580642578125" Y="0.070954696655" />
                  <Point X="21.559744140625" Y="0.098358177185" />
                  <Point X="21.538373046875" Y="0.118793716431" />
                  <Point X="21.5122734375" Y="0.136908691406" />
                  <Point X="21.498076171875" Y="0.145046768188" />
                  <Point X="21.467125" Y="0.157848175049" />
                  <Point X="20.108185546875" Y="0.521975158691" />
                  <Point X="20.175513671875" Y="0.976972595215" />
                  <Point X="20.18258984375" Y="1.003090637207" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="21.1709921875" Y="1.308132324219" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255017578125" Y="1.299341796875" />
                  <Point X="21.276578125" Y="1.301228271484" />
                  <Point X="21.296865234375" Y="1.305263793945" />
                  <Point X="21.3005234375" Y="1.306416992188" />
                  <Point X="21.358291015625" Y="1.324631103516" />
                  <Point X="21.3772265625" Y="1.332963989258" />
                  <Point X="21.3959765625" Y="1.343790527344" />
                  <Point X="21.412662109375" Y="1.356027832031" />
                  <Point X="21.4263046875" Y="1.371587890625" />
                  <Point X="21.43871875" Y="1.389325439453" />
                  <Point X="21.448666015625" Y="1.40746862793" />
                  <Point X="21.450123046875" Y="1.410987915039" />
                  <Point X="21.473296875" Y="1.466935791016" />
                  <Point X="21.479083984375" Y="1.486787841797" />
                  <Point X="21.48284375" Y="1.508103027344" />
                  <Point X="21.484197265625" Y="1.528749633789" />
                  <Point X="21.481048828125" Y="1.549199707031" />
                  <Point X="21.4754453125" Y="1.570106201172" />
                  <Point X="21.4679453125" Y="1.589384399414" />
                  <Point X="21.466173828125" Y="1.592786743164" />
                  <Point X="21.438205078125" Y="1.646514038086" />
                  <Point X="21.42671875" Y="1.663704589844" />
                  <Point X="21.412806640625" Y="1.68028515625" />
                  <Point X="21.39786328125" Y="1.694590454102" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.918853515625" Y="2.733667724609" />
                  <Point X="20.93758984375" Y="2.757752197266" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.75444140625" Y="2.867132080078" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.812275390625" Y="2.836340820312" />
                  <Point X="21.83291796875" Y="2.82983203125" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.858302734375" Y="2.825350585938" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.019537109375" Y="2.835653564453" />
                  <Point X="22.037791015625" Y="2.847282714844" />
                  <Point X="22.053916015625" Y="2.86022265625" />
                  <Point X="22.057533203125" Y="2.863839111328" />
                  <Point X="22.114640625" Y="2.920946044922" />
                  <Point X="22.127595703125" Y="2.937085693359" />
                  <Point X="22.139224609375" Y="2.955340087891" />
                  <Point X="22.14837109375" Y="2.973888916016" />
                  <Point X="22.1532890625" Y="2.993977783203" />
                  <Point X="22.156115234375" Y="3.015436767578" />
                  <Point X="22.15656640625" Y="3.036113037109" />
                  <Point X="22.15612109375" Y="3.041208007812" />
                  <Point X="22.14908203125" Y="3.121662597656" />
                  <Point X="22.145044921875" Y="3.141963378906" />
                  <Point X="22.13853515625" Y="3.16260546875" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.81666796875" Y="3.724596191406" />
                  <Point X="22.299373046875" Y="4.094682861328" />
                  <Point X="22.328890625" Y="4.111081054688" />
                  <Point X="22.832962890625" Y="4.391133300781" />
                  <Point X="22.94489453125" Y="4.245260742188" />
                  <Point X="22.9568046875" Y="4.229739746094" />
                  <Point X="22.971111328125" Y="4.214796386719" />
                  <Point X="22.98769140625" Y="4.200884765625" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.010556640625" Y="4.186443359375" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222552734375" Y="4.134483886719" />
                  <Point X="23.228458984375" Y="4.136930664062" />
                  <Point X="23.3217265625" Y="4.175563476562" />
                  <Point X="23.339853515625" Y="4.185508789062" />
                  <Point X="23.357583984375" Y="4.197922851562" />
                  <Point X="23.373140625" Y="4.211563964844" />
                  <Point X="23.385373046875" Y="4.228249511719" />
                  <Point X="23.396193359375" Y="4.246993652344" />
                  <Point X="23.4045234375" Y="4.265928710937" />
                  <Point X="23.4064453125" Y="4.272025878906" />
                  <Point X="23.436802734375" Y="4.368305175781" />
                  <Point X="23.4408359375" Y="4.388583007812" />
                  <Point X="23.44272265625" Y="4.410143066406" />
                  <Point X="23.442271484375" Y="4.430824707031" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="24.05036328125" Y="4.809808105469" />
                  <Point X="24.086140625" Y="4.813995117188" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.854587890625" Y="4.329266601562" />
                  <Point X="24.866095703125" Y="4.28631640625" />
                  <Point X="24.878869140625" Y="4.258127441406" />
                  <Point X="24.8967265625" Y="4.231400390625" />
                  <Point X="24.9178828125" Y="4.208810546875" />
                  <Point X="24.945177734375" Y="4.194219726563" />
                  <Point X="24.975615234375" Y="4.18388671875" />
                  <Point X="25.00615234375" Y="4.178844238281" />
                  <Point X="25.0366875" Y="4.183884765625" />
                  <Point X="25.067126953125" Y="4.194216308594" />
                  <Point X="25.094423828125" Y="4.208805175781" />
                  <Point X="25.11558203125" Y="4.231394042969" />
                  <Point X="25.13344140625" Y="4.258120605469" />
                  <Point X="25.146216796875" Y="4.286314453125" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.84404296875" Y="4.83173828125" />
                  <Point X="25.8736484375" Y="4.824591308594" />
                  <Point X="26.481029296875" Y="4.677950195312" />
                  <Point X="26.49890234375" Y="4.671467773438" />
                  <Point X="26.894638671875" Y="4.527931640625" />
                  <Point X="26.913283203125" Y="4.519212402344" />
                  <Point X="27.294583984375" Y="4.340891113281" />
                  <Point X="27.312615234375" Y="4.330385742188" />
                  <Point X="27.680974609375" Y="4.115778320312" />
                  <Point X="27.697962890625" Y="4.103697265625" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.1988984375" Y="2.639977050781" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.140830078125" Y="2.536755859375" />
                  <Point X="27.131798828125" Y="2.511275878906" />
                  <Point X="27.111607421875" Y="2.435770996094" />
                  <Point X="27.1084453125" Y="2.414670898438" />
                  <Point X="27.107609375" Y="2.391616455078" />
                  <Point X="27.10823046875" Y="2.376803222656" />
                  <Point X="27.116099609375" Y="2.311531494141" />
                  <Point X="27.121443359375" Y="2.289603271484" />
                  <Point X="27.1297109375" Y="2.267512939453" />
                  <Point X="27.140076171875" Y="2.247466552734" />
                  <Point X="27.142634765625" Y="2.243696289062" />
                  <Point X="27.18303515625" Y="2.184157958984" />
                  <Point X="27.19698046875" Y="2.16790625" />
                  <Point X="27.214048828125" Y="2.152048095703" />
                  <Point X="27.22537109375" Y="2.143033691406" />
                  <Point X="27.28491015625" Y="2.102634521484" />
                  <Point X="27.304953125" Y="2.092272216797" />
                  <Point X="27.327041015625" Y="2.084006347656" />
                  <Point X="27.3489609375" Y="2.0786640625" />
                  <Point X="27.353095703125" Y="2.078165283203" />
                  <Point X="27.418384765625" Y="2.070292480469" />
                  <Point X="27.440193359375" Y="2.070183105469" />
                  <Point X="27.463876953125" Y="2.072799560547" />
                  <Point X="27.477986328125" Y="2.075449707031" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.967326171875" Y="2.906190429688" />
                  <Point X="29.123283203125" Y="2.689447021484" />
                  <Point X="29.132740234375" Y="2.673817138672" />
                  <Point X="29.26219921875" Y="2.459884033203" />
                  <Point X="28.298345703125" Y="1.720291992188" />
                  <Point X="28.23078515625" Y="1.668451171875" />
                  <Point X="28.21882421875" Y="1.657533081055" />
                  <Point X="28.20053125" Y="1.637137329102" />
                  <Point X="28.14619140625" Y="1.566245483398" />
                  <Point X="28.135048828125" Y="1.54764050293" />
                  <Point X="28.125296875" Y="1.52610559082" />
                  <Point X="28.12034765625" Y="1.512502685547" />
                  <Point X="28.10010546875" Y="1.440121704102" />
                  <Point X="28.09665234375" Y="1.41782421875" />
                  <Point X="28.0958359375" Y="1.39425378418" />
                  <Point X="28.097740234375" Y="1.371760009766" />
                  <Point X="28.09879296875" Y="1.36666015625" />
                  <Point X="28.11541015625" Y="1.286127319336" />
                  <Point X="28.12223828125" Y="1.26542199707" />
                  <Point X="28.132296875" Y="1.243694458008" />
                  <Point X="28.13914453125" Y="1.231390258789" />
                  <Point X="28.18433984375" Y="1.16269519043" />
                  <Point X="28.198892578125" Y="1.145449707031" />
                  <Point X="28.21613671875" Y="1.129359985352" />
                  <Point X="28.234353515625" Y="1.116030639648" />
                  <Point X="28.238501953125" Y="1.113695922852" />
                  <Point X="28.30399609375" Y="1.076828369141" />
                  <Point X="28.32448828125" Y="1.068273803711" />
                  <Point X="28.348072265625" Y="1.061534545898" />
                  <Point X="28.3617265625" Y="1.058697265625" />
                  <Point X="28.45028125" Y="1.046993896484" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.776837890625" Y="1.21663659668" />
                  <Point X="29.84594140625" Y="0.932785217285" />
                  <Point X="29.848921875" Y="0.91364440918" />
                  <Point X="29.890865234375" Y="0.644238891602" />
                  <Point X="28.794005859375" Y="0.350335998535" />
                  <Point X="28.716580078125" Y="0.329589874268" />
                  <Point X="28.701060546875" Y="0.323944946289" />
                  <Point X="28.676037109375" Y="0.311883239746" />
                  <Point X="28.589037109375" Y="0.261595367432" />
                  <Point X="28.571361328125" Y="0.248425598145" />
                  <Point X="28.553744140625" Y="0.231793457031" />
                  <Point X="28.5442265625" Y="0.221365463257" />
                  <Point X="28.492025390625" Y="0.154849853516" />
                  <Point X="28.48030078125" Y="0.135567611694" />
                  <Point X="28.47052734375" Y="0.11410369873" />
                  <Point X="28.463681640625" Y="0.092599098206" />
                  <Point X="28.462580078125" Y="0.086845481873" />
                  <Point X="28.4451796875" Y="-0.004011537552" />
                  <Point X="28.443580078125" Y="-0.026131343842" />
                  <Point X="28.444681640625" Y="-0.050694671631" />
                  <Point X="28.44628125" Y="-0.064307685852" />
                  <Point X="28.463681640625" Y="-0.155164550781" />
                  <Point X="28.470525390625" Y="-0.176659744263" />
                  <Point X="28.480294921875" Y="-0.198117752075" />
                  <Point X="28.492005859375" Y="-0.217384048462" />
                  <Point X="28.495330078125" Y="-0.221622329712" />
                  <Point X="28.54753125" Y="-0.288137786865" />
                  <Point X="28.56350390625" Y="-0.304132568359" />
                  <Point X="28.583326171875" Y="-0.319737060547" />
                  <Point X="28.594546875" Y="-0.327340087891" />
                  <Point X="28.681546875" Y="-0.377627929688" />
                  <Point X="28.692708984375" Y="-0.383137786865" />
                  <Point X="28.716580078125" Y="-0.392150024414" />
                  <Point X="29.891474609375" Y="-0.706961914062" />
                  <Point X="29.855025390625" Y="-0.948729492188" />
                  <Point X="29.851203125" Y="-0.965472167969" />
                  <Point X="29.801177734375" Y="-1.18469921875" />
                  <Point X="28.514173828125" Y="-1.015262145996" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.401634765625" Y="-1.003193481445" />
                  <Point X="28.373677734375" Y="-1.006256774902" />
                  <Point X="28.36384765625" Y="-1.007859069824" />
                  <Point X="28.193095703125" Y="-1.044972290039" />
                  <Point X="28.1627265625" Y="-1.055693725586" />
                  <Point X="28.131490234375" Y="-1.076037353516" />
                  <Point X="28.112455078125" Y="-1.094671875" />
                  <Point X="28.105865234375" Y="-1.10182019043" />
                  <Point X="28.00265625" Y="-1.225947631836" />
                  <Point X="27.98662890625" Y="-1.250416015625" />
                  <Point X="27.97451171875" Y="-1.282390991211" />
                  <Point X="27.969978515625" Y="-1.307123657227" />
                  <Point X="27.968822265625" Y="-1.315546386719" />
                  <Point X="27.95403125" Y="-1.47629699707" />
                  <Point X="27.955111328125" Y="-1.508485717773" />
                  <Point X="27.9652421875" Y="-1.544657104492" />
                  <Point X="27.977736328125" Y="-1.569128417969" />
                  <Point X="27.982435546875" Y="-1.577304443359" />
                  <Point X="28.076931640625" Y="-1.724287109375" />
                  <Point X="28.086935546875" Y="-1.737241210938" />
                  <Point X="28.110630859375" Y="-1.760909423828" />
                  <Point X="29.213125" Y="-2.606883300781" />
                  <Point X="29.1248203125" Y="-2.749770751953" />
                  <Point X="29.116900390625" Y="-2.761024414062" />
                  <Point X="29.028982421875" Y="-2.885944580078" />
                  <Point X="27.880927734375" Y="-2.223114501953" />
                  <Point X="27.800955078125" Y="-2.176942871094" />
                  <Point X="27.779396484375" Y="-2.167825683594" />
                  <Point X="27.7504140625" Y="-2.159599121094" />
                  <Point X="27.741357421875" Y="-2.157501220703" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.505974609375" Y="-2.119082275391" />
                  <Point X="27.46874609375" Y="-2.126161621094" />
                  <Point X="27.4423828125" Y="-2.136958740234" />
                  <Point X="27.434142578125" Y="-2.140803710938" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.24114453125" Y="-2.246129638672" />
                  <Point X="27.2173984375" Y="-2.271526123047" />
                  <Point X="27.202880859375" Y="-2.294285888672" />
                  <Point X="27.19890625" Y="-2.301129638672" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.098732421875" Y="-2.500112304688" />
                  <Point X="27.094302734375" Y="-2.538049560547" />
                  <Point X="27.09683984375" Y="-2.567419921875" />
                  <Point X="27.098" Y="-2.576128417969" />
                  <Point X="27.134703125" Y="-2.779349609375" />
                  <Point X="27.13898828125" Y="-2.795140136719" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.86128515625" Y="-4.054905029297" />
                  <Point X="27.781853515625" Y="-4.111641601562" />
                  <Point X="27.7730078125" Y="-4.117368164063" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="26.81957421875" Y="-3.01378515625" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74166015625" Y="-2.917221191406" />
                  <Point X="26.716337890625" Y="-2.897440917969" />
                  <Point X="26.709232421875" Y="-2.892396972656" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479744140625" Y="-2.749644287109" />
                  <Point X="26.44208203125" Y="-2.7419375" />
                  <Point X="26.411744140625" Y="-2.741994628906" />
                  <Point X="26.40321875" Y="-2.742394042969" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.155380859375" Y="-2.768534667969" />
                  <Point X="26.123302734375" Y="-2.783205078125" />
                  <Point X="26.099751953125" Y="-2.799864990234" />
                  <Point X="26.09387890625" Y="-2.804373291016" />
                  <Point X="25.92461328125" Y="-2.945111328125" />
                  <Point X="25.902615234375" Y="-2.968639160156" />
                  <Point X="25.883837890625" Y="-3.002524658203" />
                  <Point X="25.874513671875" Y="-3.032596191406" />
                  <Point X="25.872419921875" Y="-3.040553710938" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323119628906" />
                  <Point X="26.02206640625" Y="-4.859915527344" />
                  <Point X="25.9756796875" Y="-4.870083007812" />
                  <Point X="25.9675078125" Y="-4.871567871094" />
                  <Point X="25.92931640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941580078125" Y="-4.752637695312" />
                  <Point X="23.930947265625" Y="-4.74990234375" />
                  <Point X="23.858755859375" Y="-4.731328125" />
                  <Point X="23.876353515625" Y="-4.597654785156" />
                  <Point X="23.8792265625" Y="-4.575841308594" />
                  <Point X="23.880037109375" Y="-4.56397265625" />
                  <Point X="23.880169921875" Y="-4.540227050781" />
                  <Point X="23.8794921875" Y="-4.528350097656" />
                  <Point X="23.875353515625" Y="-4.493943847656" />
                  <Point X="23.873060546875" Y="-4.479566894531" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.813138671875" Y="-4.182043457031" />
                  <Point X="23.80313671875" Y="-4.151866699219" />
                  <Point X="23.793091796875" Y="-4.130724609375" />
                  <Point X="23.772107421875" Y="-4.097773925781" />
                  <Point X="23.75990234375" Y="-4.082387695312" />
                  <Point X="23.735677734375" Y="-4.057612792969" />
                  <Point X="23.725103515625" Y="-4.047596191406" />
                  <Point X="23.505734375" Y="-3.855214355469" />
                  <Point X="23.496443359375" Y="-3.848032714844" />
                  <Point X="23.470142578125" Y="-3.830183105469" />
                  <Point X="23.44930078125" Y="-3.819523681641" />
                  <Point X="23.412583984375" Y="-3.806153564453" />
                  <Point X="23.39353125" Y="-3.801376953125" />
                  <Point X="23.359228515625" Y="-3.796469482422" />
                  <Point X="23.344748046875" Y="-3.794961181641" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0418515625" Y="-3.7758359375" />
                  <Point X="23.010115234375" Y="-3.777686767578" />
                  <Point X="22.987091796875" Y="-3.781918701172" />
                  <Point X="22.949830078125" Y="-3.793661621094" />
                  <Point X="22.931810546875" Y="-3.801468994141" />
                  <Point X="22.901609375" Y="-3.818457275391" />
                  <Point X="22.88919921875" Y="-3.826077880859" />
                  <Point X="22.64659765625" Y="-3.988180664062" />
                  <Point X="22.6403203125" Y="-3.992757324219" />
                  <Point X="22.619552734375" Y="-4.010137695313" />
                  <Point X="22.5972421875" Y="-4.032773193359" />
                  <Point X="22.58953125" Y="-4.041628173828" />
                  <Point X="22.496798828125" Y="-4.162479492188" />
                  <Point X="22.252404296875" Y="-4.011156005859" />
                  <Point X="22.23771484375" Y="-3.999845214844" />
                  <Point X="22.01913671875" Y="-3.831547119141" />
                  <Point X="22.614173828125" Y="-2.800910400391" />
                  <Point X="22.658509765625" Y="-2.724120117188" />
                  <Point X="22.6651484375" Y="-2.710086181641" />
                  <Point X="22.67605078125" Y="-2.681121582031" />
                  <Point X="22.680314453125" Y="-2.666190917969" />
                  <Point X="22.6865859375" Y="-2.634665039062" />
                  <Point X="22.68837109375" Y="-2.61890625" />
                  <Point X="22.689294921875" Y="-2.587312255859" />
                  <Point X="22.684951171875" Y="-2.556005615234" />
                  <Point X="22.675458984375" Y="-2.525857421875" />
                  <Point X="22.66944921875" Y="-2.511180664062" />
                  <Point X="22.65419921875" Y="-2.481318603516" />
                  <Point X="22.646427734375" Y="-2.468653808594" />
                  <Point X="22.629" Y="-2.4446875" />
                  <Point X="22.61934375" Y="-2.433385986328" />
                  <Point X="22.603029296875" Y="-2.417070556641" />
                  <Point X="22.591189453125" Y="-2.407025146484" />
                  <Point X="22.566033203125" Y="-2.388997070312" />
                  <Point X="22.552716796875" Y="-2.381014404297" />
                  <Point X="22.523888671875" Y="-2.366796386719" />
                  <Point X="22.509447265625" Y="-2.361089111328" />
                  <Point X="22.479828125" Y="-2.352103271484" />
                  <Point X="22.449140625" Y="-2.3480625" />
                  <Point X="22.418205078125" Y="-2.349074707031" />
                  <Point X="22.402779296875" Y="-2.350849121094" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.206912109375" Y="-3.017708740234" />
                  <Point X="20.99598828125" Y="-2.740597900391" />
                  <Point X="20.985451171875" Y="-2.722928955078" />
                  <Point X="20.818734375" Y="-2.443372558594" />
                  <Point X="21.874306640625" Y="-1.633404663086" />
                  <Point X="21.951876953125" Y="-1.573881713867" />
                  <Point X="21.964078125" Y="-1.562703491211" />
                  <Point X="21.986353515625" Y="-1.538400634766" />
                  <Point X="21.996427734375" Y="-1.525275634766" />
                  <Point X="22.0148515625" Y="-1.496373535156" />
                  <Point X="22.0218515625" Y="-1.483218261719" />
                  <Point X="22.033708984375" Y="-1.45597277832" />
                  <Point X="22.03856640625" Y="-1.44188293457" />
                  <Point X="22.0458125" Y="-1.413907226562" />
                  <Point X="22.048447265625" Y="-1.398808959961" />
                  <Point X="22.051251953125" Y="-1.368389160156" />
                  <Point X="22.051421875" Y="-1.353067626953" />
                  <Point X="22.049216796875" Y="-1.321402709961" />
                  <Point X="22.046828125" Y="-1.305847167969" />
                  <Point X="22.03951171875" Y="-1.27533996582" />
                  <Point X="22.0272578125" Y="-1.246459228516" />
                  <Point X="22.010404296875" Y="-1.219999023438" />
                  <Point X="22.000876953125" Y="-1.207467651367" />
                  <Point X="21.97846484375" Y="-1.182576293945" />
                  <Point X="21.96795703125" Y="-1.172564086914" />
                  <Point X="21.94554296875" Y="-1.154258544922" />
                  <Point X="21.93363671875" Y="-1.145965087891" />
                  <Point X="21.90873046875" Y="-1.131306640625" />
                  <Point X="21.894564453125" Y="-1.124479736328" />
                  <Point X="21.86530078125" Y="-1.113255981445" />
                  <Point X="21.850203125" Y="-1.108859130859" />
                  <Point X="21.818314453125" Y="-1.102378051758" />
                  <Point X="21.80269921875" Y="-1.100532226562" />
                  <Point X="21.771376953125" Y="-1.09944140625" />
                  <Point X="21.755669921875" Y="-1.100196533203" />
                  <Point X="20.339080078125" Y="-1.286694091797" />
                  <Point X="20.259240234375" Y="-0.974120056152" />
                  <Point X="20.256451171875" Y="-0.954624694824" />
                  <Point X="20.21355078125" Y="-0.654654296875" />
                  <Point X="21.40334765625" Y="-0.335848693848" />
                  <Point X="21.491712890625" Y="-0.312171295166" />
                  <Point X="21.50033203125" Y="-0.309415679932" />
                  <Point X="21.528419921875" Y="-0.298097442627" />
                  <Point X="21.558287109375" Y="-0.282327056885" />
                  <Point X="21.568095703125" Y="-0.276364471436" />
                  <Point X="21.594189453125" Y="-0.258254760742" />
                  <Point X="21.606923828125" Y="-0.247657348633" />
                  <Point X="21.630357421875" Y="-0.224413772583" />
                  <Point X="21.641056640625" Y="-0.21176789856" />
                  <Point X="21.660853515625" Y="-0.183735534668" />
                  <Point X="21.66846875" Y="-0.170927658081" />
                  <Point X="21.681599609375" Y="-0.144282699585" />
                  <Point X="21.687115234375" Y="-0.130445602417" />
                  <Point X="21.695814453125" Y="-0.102413986206" />
                  <Point X="21.699130859375" Y="-0.087677879333" />
                  <Point X="21.70338671875" Y="-0.057855354309" />
                  <Point X="21.704326171875" Y="-0.042768791199" />
                  <Point X="21.703775390625" Y="-0.011354816437" />
                  <Point X="21.7025859375" Y="0.002048294544" />
                  <Point X="21.698330078125" Y="0.028540029526" />
                  <Point X="21.695263671875" Y="0.041628505707" />
                  <Point X="21.686564453125" Y="0.069660415649" />
                  <Point X="21.680263671875" Y="0.085053588867" />
                  <Point X="21.665072265625" Y="0.114504852295" />
                  <Point X="21.656181640625" Y="0.128562942505" />
                  <Point X="21.635283203125" Y="0.15596647644" />
                  <Point X="21.625400390625" Y="0.167019424438" />
                  <Point X="21.604029296875" Y="0.187454910278" />
                  <Point X="21.592541015625" Y="0.196837478638" />
                  <Point X="21.56644140625" Y="0.214952529907" />
                  <Point X="21.559517578125" Y="0.219328292847" />
                  <Point X="21.534384765625" Y="0.232834396362" />
                  <Point X="21.50343359375" Y="0.245635726929" />
                  <Point X="21.491712890625" Y="0.249611099243" />
                  <Point X="20.214556640625" Y="0.591824523926" />
                  <Point X="20.268671875" Y="0.957536865234" />
                  <Point X="20.274283203125" Y="0.978247802734" />
                  <Point X="20.3664140625" Y="1.318237182617" />
                  <Point X="21.158591796875" Y="1.21394519043" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.2529453125" Y="1.204364257812" />
                  <Point X="21.263298828125" Y="1.204703369141" />
                  <Point X="21.284859375" Y="1.20658984375" />
                  <Point X="21.29511328125" Y="1.208053833008" />
                  <Point X="21.315400390625" Y="1.212089233398" />
                  <Point X="21.3290859375" Y="1.215812255859" />
                  <Point X="21.386853515625" Y="1.234026367188" />
                  <Point X="21.396556640625" Y="1.237678344727" />
                  <Point X="21.4154921875" Y="1.246011108398" />
                  <Point X="21.42473046875" Y="1.250693847656" />
                  <Point X="21.44348046875" Y="1.261520385742" />
                  <Point X="21.45216015625" Y="1.267184814453" />
                  <Point X="21.468845703125" Y="1.279422119141" />
                  <Point X="21.48409375" Y="1.29339831543" />
                  <Point X="21.497736328125" Y="1.308958374023" />
                  <Point X="21.50413671875" Y="1.317115478516" />
                  <Point X="21.51655078125" Y="1.334853027344" />
                  <Point X="21.52201953125" Y="1.343654174805" />
                  <Point X="21.531966796875" Y="1.361797485352" />
                  <Point X="21.537892578125" Y="1.374633666992" />
                  <Point X="21.56106640625" Y="1.430581542969" />
                  <Point X="21.5645" Y="1.440348754883" />
                  <Point X="21.570287109375" Y="1.460200805664" />
                  <Point X="21.572640625" Y="1.470285644531" />
                  <Point X="21.576400390625" Y="1.491600830078" />
                  <Point X="21.577640625" Y="1.501888427734" />
                  <Point X="21.578994140625" Y="1.52253515625" />
                  <Point X="21.578091796875" Y="1.543205200195" />
                  <Point X="21.574943359375" Y="1.563655273438" />
                  <Point X="21.572810546875" Y="1.573794189453" />
                  <Point X="21.56720703125" Y="1.594700683594" />
                  <Point X="21.56398046875" Y="1.604550292969" />
                  <Point X="21.55648046875" Y="1.623828491211" />
                  <Point X="21.550435546875" Y="1.636659423828" />
                  <Point X="21.522466796875" Y="1.69038671875" />
                  <Point X="21.5171953125" Y="1.69929309082" />
                  <Point X="21.505708984375" Y="1.716483642578" />
                  <Point X="21.499494140625" Y="1.724767822266" />
                  <Point X="21.48558203125" Y="1.741348388672" />
                  <Point X="21.4785" Y="1.748909423828" />
                  <Point X="21.463556640625" Y="1.76321472168" />
                  <Point X="21.4556953125" Y="1.769958984375" />
                  <Point X="20.77238671875" Y="2.294280761719" />
                  <Point X="20.99771875" Y="2.680325927734" />
                  <Point X="21.012572265625" Y="2.699420166016" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.70694140625" Y="2.784859619141" />
                  <Point X="21.74584375" Y="2.762398681641" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774013671875" Y="2.749386474609" />
                  <Point X="21.78370703125" Y="2.745738037109" />
                  <Point X="21.804349609375" Y="2.739229248047" />
                  <Point X="21.814384765625" Y="2.736657226562" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.8500234375" Y="2.730712158203" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.02356640625" Y="2.734227294922" />
                  <Point X="22.043005859375" Y="2.741302978516" />
                  <Point X="22.061556640625" Y="2.750451904297" />
                  <Point X="22.070580078125" Y="2.755531738281" />
                  <Point X="22.088833984375" Y="2.767160888672" />
                  <Point X="22.097248046875" Y="2.773189697266" />
                  <Point X="22.113373046875" Y="2.786129638672" />
                  <Point X="22.124701171875" Y="2.796657226562" />
                  <Point X="22.18180859375" Y="2.853764160156" />
                  <Point X="22.1887265625" Y="2.861478759766" />
                  <Point X="22.201681640625" Y="2.877618408203" />
                  <Point X="22.20771875" Y="2.886043457031" />
                  <Point X="22.21934765625" Y="2.904297851562" />
                  <Point X="22.2244296875" Y="2.913325683594" />
                  <Point X="22.233576171875" Y="2.931874511719" />
                  <Point X="22.240646484375" Y="2.951299072266" />
                  <Point X="22.245564453125" Y="2.971387939453" />
                  <Point X="22.2474765625" Y="2.981573242188" />
                  <Point X="22.250302734375" Y="3.003032226562" />
                  <Point X="22.251091796875" Y="3.013364257812" />
                  <Point X="22.25154296875" Y="3.034040527344" />
                  <Point X="22.250759765625" Y="3.049479736328" />
                  <Point X="22.243720703125" Y="3.129934326172" />
                  <Point X="22.2422578125" Y="3.140191894531" />
                  <Point X="22.238220703125" Y="3.160492675781" />
                  <Point X="22.235646484375" Y="3.170535888672" />
                  <Point X="22.22913671875" Y="3.191177978516" />
                  <Point X="22.225486328125" Y="3.200873291016" />
                  <Point X="22.21715625" Y="3.21980078125" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="21.94061328125" Y="3.699915527344" />
                  <Point X="22.351626953125" Y="4.015036621094" />
                  <Point X="22.375025390625" Y="4.028035644531" />
                  <Point X="22.807474609375" Y="4.268295410156" />
                  <Point X="22.869525390625" Y="4.187428710937" />
                  <Point X="22.88818359375" Y="4.164042480469" />
                  <Point X="22.902490234375" Y="4.149099121094" />
                  <Point X="22.910048828125" Y="4.142020507812" />
                  <Point X="22.92662890625" Y="4.128108886719" />
                  <Point X="22.934912109375" Y="4.121895507812" />
                  <Point X="22.952107421875" Y="4.110405761719" />
                  <Point X="22.966689453125" Y="4.102177734375" />
                  <Point X="23.056236328125" Y="4.0555625" />
                  <Point X="23.065673828125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.0437890625" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.2292734375" Y="4.037489013672" />
                  <Point X="23.249138671875" Y="4.043279541016" />
                  <Point X="23.264818359375" Y="4.049163818359" />
                  <Point X="23.3580859375" Y="4.087796630859" />
                  <Point X="23.367421875" Y="4.092275390625" />
                  <Point X="23.385548828125" Y="4.102220703125" />
                  <Point X="23.39433984375" Y="4.1076875" />
                  <Point X="23.4120703125" Y="4.1201015625" />
                  <Point X="23.420216796875" Y="4.126494140625" />
                  <Point X="23.4357734375" Y="4.140135253906" />
                  <Point X="23.4497578125" Y="4.155395507812" />
                  <Point X="23.461990234375" Y="4.172081054688" />
                  <Point X="23.4676484375" Y="4.180754882812" />
                  <Point X="23.47846875" Y="4.199499023438" />
                  <Point X="23.483150390625" Y="4.208738769531" />
                  <Point X="23.49148046875" Y="4.227673828125" />
                  <Point X="23.49705078125" Y="4.243466308594" />
                  <Point X="23.527408203125" Y="4.339745605469" />
                  <Point X="23.529978515625" Y="4.349772949219" />
                  <Point X="23.53401171875" Y="4.37005078125" />
                  <Point X="23.535474609375" Y="4.380301269531" />
                  <Point X="23.537361328125" Y="4.401861328125" />
                  <Point X="23.53769921875" Y="4.41221484375" />
                  <Point X="23.537248046875" Y="4.432896484375" />
                  <Point X="23.536458984375" Y="4.443225097656" />
                  <Point X="23.520734375" Y="4.562655273438" />
                  <Point X="24.0688203125" Y="4.716319824219" />
                  <Point X="24.09718359375" Y="4.719639160156" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.76282421875" Y="4.304678710938" />
                  <Point X="24.77433203125" Y="4.261728515625" />
                  <Point X="24.779564453125" Y="4.247106445313" />
                  <Point X="24.792337890625" Y="4.218917480469" />
                  <Point X="24.79987890625" Y="4.205350585938" />
                  <Point X="24.817736328125" Y="4.178623535156" />
                  <Point X="24.82738671875" Y="4.166461425781" />
                  <Point X="24.84854296875" Y="4.143871582031" />
                  <Point X="24.87309765625" Y="4.125029785156" />
                  <Point X="24.900392578125" Y="4.110438964844" />
                  <Point X="24.914638671875" Y="4.104262207031" />
                  <Point X="24.945076171875" Y="4.093929199219" />
                  <Point X="24.960138671875" Y="4.090156005859" />
                  <Point X="24.99067578125" Y="4.085113525391" />
                  <Point X="25.021625" Y="4.085112792969" />
                  <Point X="25.05216015625" Y="4.090153320312" />
                  <Point X="25.067220703125" Y="4.093925292969" />
                  <Point X="25.09766015625" Y="4.104256835938" />
                  <Point X="25.11190625" Y="4.110431640625" />
                  <Point X="25.139203125" Y="4.125020507812" />
                  <Point X="25.1637578125" Y="4.143861816406" />
                  <Point X="25.184916015625" Y="4.166450683594" />
                  <Point X="25.1945703125" Y="4.178612304688" />
                  <Point X="25.2124296875" Y="4.205338867188" />
                  <Point X="25.21997265625" Y="4.218911132812" />
                  <Point X="25.232748046875" Y="4.247104980469" />
                  <Point X="25.23798046875" Y="4.2617265625" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.82787890625" Y="4.737911621094" />
                  <Point X="25.85135546875" Y="4.732244140625" />
                  <Point X="26.453599609375" Y="4.586843261719" />
                  <Point X="26.46651171875" Y="4.58216015625" />
                  <Point X="26.858248046875" Y="4.44007421875" />
                  <Point X="26.8730390625" Y="4.433157714844" />
                  <Point X="27.250458984375" Y="4.256651367187" />
                  <Point X="27.264791015625" Y="4.248301269531" />
                  <Point X="27.629427734375" Y="4.035862548828" />
                  <Point X="27.64290625" Y="4.026277587891" />
                  <Point X="27.81778125" Y="3.901915283203" />
                  <Point X="27.116626953125" Y="2.687477050781" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.06163671875" Y="2.591571777344" />
                  <Point X="27.0512890625" Y="2.568493408203" />
                  <Point X="27.0422578125" Y="2.543013427734" />
                  <Point X="27.0400234375" Y="2.535818359375" />
                  <Point X="27.01983203125" Y="2.460313476562" />
                  <Point X="27.01765625" Y="2.449850585938" />
                  <Point X="27.014494140625" Y="2.428750488281" />
                  <Point X="27.0135078125" Y="2.41811328125" />
                  <Point X="27.012671875" Y="2.395058837891" />
                  <Point X="27.012693359375" Y="2.38763671875" />
                  <Point X="27.0139140625" Y="2.365432373047" />
                  <Point X="27.021783203125" Y="2.300160644531" />
                  <Point X="27.02380078125" Y="2.289038818359" />
                  <Point X="27.02914453125" Y="2.267110595703" />
                  <Point X="27.032470703125" Y="2.256304199219" />
                  <Point X="27.04073828125" Y="2.234213867188" />
                  <Point X="27.04532421875" Y="2.223879638672" />
                  <Point X="27.055689453125" Y="2.203833251953" />
                  <Point X="27.06402734375" Y="2.190350830078" />
                  <Point X="27.104427734375" Y="2.1308125" />
                  <Point X="27.110939453125" Y="2.122293701172" />
                  <Point X="27.124884765625" Y="2.106041992188" />
                  <Point X="27.132318359375" Y="2.098309082031" />
                  <Point X="27.14938671875" Y="2.082450927734" />
                  <Point X="27.154876953125" Y="2.077726806641" />
                  <Point X="27.17203125" Y="2.064422119141" />
                  <Point X="27.2315703125" Y="2.024022827148" />
                  <Point X="27.24128125" Y="2.018245727539" />
                  <Point X="27.26132421875" Y="2.007883422852" />
                  <Point X="27.27165625" Y="2.003298339844" />
                  <Point X="27.293744140625" Y="1.995032470703" />
                  <Point X="27.304546875" Y="1.991708007812" />
                  <Point X="27.326466796875" Y="1.986365722656" />
                  <Point X="27.34171875" Y="1.983848876953" />
                  <Point X="27.4070078125" Y="1.975976196289" />
                  <Point X="27.417908203125" Y="1.975293701172" />
                  <Point X="27.439716796875" Y="1.975184326172" />
                  <Point X="27.450625" Y="1.975757568359" />
                  <Point X="27.47430859375" Y="1.978373901367" />
                  <Point X="27.4814140625" Y="1.979432250977" />
                  <Point X="27.50252734375" Y="1.983674316406" />
                  <Point X="27.578033203125" Y="2.003865112305" />
                  <Point X="27.583994140625" Y="2.005670654297" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.940404296875" Y="2.780950439453" />
                  <Point X="29.043970703125" Y="2.637018310547" />
                  <Point X="29.0514609375" Y="2.624637939453" />
                  <Point X="29.13688671875" Y="2.483471923828" />
                  <Point X="28.240513671875" Y="1.795660522461" />
                  <Point X="28.172953125" Y="1.743819702148" />
                  <Point X="28.16673828125" Y="1.738615356445" />
                  <Point X="28.1481015625" Y="1.720963500977" />
                  <Point X="28.12980859375" Y="1.700567749023" />
                  <Point X="28.1251328125" Y="1.694931152344" />
                  <Point X="28.07079296875" Y="1.624039428711" />
                  <Point X="28.064689453125" Y="1.615056762695" />
                  <Point X="28.053546875" Y="1.596451782227" />
                  <Point X="28.0485078125" Y="1.586829711914" />
                  <Point X="28.038755859375" Y="1.565294677734" />
                  <Point X="28.036021484375" Y="1.558586914062" />
                  <Point X="28.028857421875" Y="1.538088867188" />
                  <Point X="28.008615234375" Y="1.465707885742" />
                  <Point X="28.006224609375" Y="1.454660644531" />
                  <Point X="28.002771484375" Y="1.43236315918" />
                  <Point X="28.001708984375" Y="1.421112792969" />
                  <Point X="28.000892578125" Y="1.397542480469" />
                  <Point X="28.001173828125" Y="1.386239868164" />
                  <Point X="28.003078125" Y="1.36374597168" />
                  <Point X="28.00575390625" Y="1.347454711914" />
                  <Point X="28.02237109375" Y="1.266921875" />
                  <Point X="28.025189453125" Y="1.256374633789" />
                  <Point X="28.032017578125" Y="1.235669311523" />
                  <Point X="28.03602734375" Y="1.22551171875" />
                  <Point X="28.0460859375" Y="1.203784179688" />
                  <Point X="28.049287109375" Y="1.197496582031" />
                  <Point X="28.05978125" Y="1.17917590332" />
                  <Point X="28.1049765625" Y="1.110480712891" />
                  <Point X="28.111736328125" Y="1.101427856445" />
                  <Point X="28.1262890625" Y="1.084182373047" />
                  <Point X="28.13408203125" Y="1.075989746094" />
                  <Point X="28.151326171875" Y="1.059900024414" />
                  <Point X="28.1600390625" Y="1.052692016602" />
                  <Point X="28.178255859375" Y="1.039362670898" />
                  <Point X="28.191908203125" Y="1.030906616211" />
                  <Point X="28.25740234375" Y="0.994039123535" />
                  <Point X="28.2673984375" Y="0.989160583496" />
                  <Point X="28.287890625" Y="0.980606140137" />
                  <Point X="28.29838671875" Y="0.976930114746" />
                  <Point X="28.321970703125" Y="0.970190795898" />
                  <Point X="28.328744140625" Y="0.968521484375" />
                  <Point X="28.349279296875" Y="0.964516235352" />
                  <Point X="28.437833984375" Y="0.952812744141" />
                  <Point X="28.444033203125" Y="0.952199584961" />
                  <Point X="28.46571875" Y="0.951222961426" />
                  <Point X="28.49121875" Y="0.952032348633" />
                  <Point X="28.50060546875" Y="0.952797180176" />
                  <Point X="29.704701171875" Y="1.111319824219" />
                  <Point X="29.752689453125" Y="0.914204223633" />
                  <Point X="29.755052734375" Y="0.899027770996" />
                  <Point X="29.78387109375" Y="0.713921264648" />
                  <Point X="28.76941796875" Y="0.442098999023" />
                  <Point X="28.6919921875" Y="0.421352874756" />
                  <Point X="28.684107421875" Y="0.418867614746" />
                  <Point X="28.659810546875" Y="0.409522216797" />
                  <Point X="28.634787109375" Y="0.397460571289" />
                  <Point X="28.62849609375" Y="0.394131713867" />
                  <Point X="28.54149609375" Y="0.343843811035" />
                  <Point X="28.53227734375" Y="0.337775085449" />
                  <Point X="28.5146015625" Y="0.324605316162" />
                  <Point X="28.50614453125" Y="0.317504272461" />
                  <Point X="28.48852734375" Y="0.300872039795" />
                  <Point X="28.483576171875" Y="0.295835784912" />
                  <Point X="28.4694921875" Y="0.280016082764" />
                  <Point X="28.417291015625" Y="0.213500488281" />
                  <Point X="28.410853515625" Y="0.20420664978" />
                  <Point X="28.39912890625" Y="0.184924468994" />
                  <Point X="28.393841796875" Y="0.174935958862" />
                  <Point X="28.384068359375" Y="0.153472137451" />
                  <Point X="28.38000390625" Y="0.142920806885" />
                  <Point X="28.373158203125" Y="0.121416114807" />
                  <Point X="28.369275390625" Y="0.104709266663" />
                  <Point X="28.351875" Y="0.013852222443" />
                  <Point X="28.350427734375" Y="0.002840603113" />
                  <Point X="28.348828125" Y="-0.019279239655" />
                  <Point X="28.34867578125" Y="-0.030387464523" />
                  <Point X="28.34977734375" Y="-0.054950817108" />
                  <Point X="28.350330078125" Y="-0.061781524658" />
                  <Point X="28.3529765625" Y="-0.082176750183" />
                  <Point X="28.370376953125" Y="-0.17303364563" />
                  <Point X="28.373158203125" Y="-0.183985671997" />
                  <Point X="28.380001953125" Y="-0.20548085022" />
                  <Point X="28.384064453125" Y="-0.216024002075" />
                  <Point X="28.393833984375" Y="-0.237482040405" />
                  <Point X="28.399115234375" Y="-0.247462356567" />
                  <Point X="28.410826171875" Y="-0.266728637695" />
                  <Point X="28.420595703125" Y="-0.280272949219" />
                  <Point X="28.472796875" Y="-0.346788543701" />
                  <Point X="28.480310546875" Y="-0.355266418457" />
                  <Point X="28.496283203125" Y="-0.371261230469" />
                  <Point X="28.5047421875" Y="-0.378778137207" />
                  <Point X="28.524564453125" Y="-0.394382629395" />
                  <Point X="28.530037109375" Y="-0.398383117676" />
                  <Point X="28.547005859375" Y="-0.409588531494" />
                  <Point X="28.634005859375" Y="-0.459876434326" />
                  <Point X="28.63949609375" Y="-0.462814727783" />
                  <Point X="28.659154296875" Y="-0.47201461792" />
                  <Point X="28.683025390625" Y="-0.481026977539" />
                  <Point X="28.6919921875" Y="-0.48391293335" />
                  <Point X="29.784880859375" Y="-0.776751403809" />
                  <Point X="29.761619140625" Y="-0.931038208008" />
                  <Point X="29.7585859375" Y="-0.94432824707" />
                  <Point X="29.7278046875" Y="-1.079219726563" />
                  <Point X="28.52657421875" Y="-0.921074829102" />
                  <Point X="28.436783203125" Y="-0.909253540039" />
                  <Point X="28.425416015625" Y="-0.908446533203" />
                  <Point X="28.40266796875" Y="-0.908199035645" />
                  <Point X="28.391287109375" Y="-0.908758605957" />
                  <Point X="28.363330078125" Y="-0.911821899414" />
                  <Point X="28.343669921875" Y="-0.915026489258" />
                  <Point X="28.17291796875" Y="-0.952139709473" />
                  <Point X="28.161470703125" Y="-0.955390991211" />
                  <Point X="28.1311015625" Y="-0.966112365723" />
                  <Point X="28.110880859375" Y="-0.976088195801" />
                  <Point X="28.07964453125" Y="-0.996431884766" />
                  <Point X="28.065033203125" Y="-1.008151672363" />
                  <Point X="28.045998046875" Y="-1.026786132813" />
                  <Point X="28.032818359375" Y="-1.041082763672" />
                  <Point X="27.929609375" Y="-1.165210205078" />
                  <Point X="27.9231875" Y="-1.173893432617" />
                  <Point X="27.90716015625" Y="-1.198361816406" />
                  <Point X="27.89779296875" Y="-1.216751220703" />
                  <Point X="27.88567578125" Y="-1.248726196289" />
                  <Point X="27.881068359375" Y="-1.265263916016" />
                  <Point X="27.87653515625" Y="-1.289996582031" />
                  <Point X="27.87422265625" Y="-1.306842041016" />
                  <Point X="27.859431640625" Y="-1.467592651367" />
                  <Point X="27.859083984375" Y="-1.479482910156" />
                  <Point X="27.8601640625" Y="-1.511671630859" />
                  <Point X="27.863630859375" Y="-1.534107299805" />
                  <Point X="27.87376171875" Y="-1.570278686523" />
                  <Point X="27.8806328125" Y="-1.587855834961" />
                  <Point X="27.893126953125" Y="-1.612327148438" />
                  <Point X="27.902525390625" Y="-1.628679199219" />
                  <Point X="27.997021484375" Y="-1.775661865234" />
                  <Point X="28.0017421875" Y="-1.782352539063" />
                  <Point X="28.019798828125" Y="-1.804454956055" />
                  <Point X="28.043494140625" Y="-1.828123046875" />
                  <Point X="28.052798828125" Y="-1.836277832031" />
                  <Point X="29.087173828125" Y="-2.629981689453" />
                  <Point X="29.045501953125" Y="-2.697409423828" />
                  <Point X="29.0392109375" Y="-2.706349365234" />
                  <Point X="29.001275390625" Y="-2.760251220703" />
                  <Point X="27.928427734375" Y="-2.140842041016" />
                  <Point X="27.848455078125" Y="-2.094670410156" />
                  <Point X="27.83795703125" Y="-2.089445556641" />
                  <Point X="27.8163984375" Y="-2.080328369141" />
                  <Point X="27.805337890625" Y="-2.076436035156" />
                  <Point X="27.77635546875" Y="-2.068209472656" />
                  <Point X="27.7582421875" Y="-2.064013671875" />
                  <Point X="27.555021484375" Y="-2.027312011719" />
                  <Point X="27.543201171875" Y="-2.025934692383" />
                  <Point X="27.5110390625" Y="-2.024217285156" />
                  <Point X="27.4882265625" Y="-2.025754638672" />
                  <Point X="27.450998046875" Y="-2.032834106445" />
                  <Point X="27.4327421875" Y="-2.038249023438" />
                  <Point X="27.40637890625" Y="-2.049045898438" />
                  <Point X="27.3898984375" Y="-2.056735839844" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.211814453125" Y="-2.151153564453" />
                  <Point X="27.187642578125" Y="-2.167627197266" />
                  <Point X="27.171751953125" Y="-2.181247070313" />
                  <Point X="27.148005859375" Y="-2.206643554688" />
                  <Point X="27.1373046875" Y="-2.2204375" />
                  <Point X="27.122787109375" Y="-2.243197265625" />
                  <Point X="27.114837890625" Y="-2.256884765625" />
                  <Point X="27.025984375" Y="-2.425711669922" />
                  <Point X="27.02111328125" Y="-2.436569091797" />
                  <Point X="27.00979296875" Y="-2.466724853516" />
                  <Point X="27.004373046875" Y="-2.489094726562" />
                  <Point X="26.999943359375" Y="-2.527031982422" />
                  <Point X="26.999654296875" Y="-2.546225585938" />
                  <Point X="27.00219140625" Y="-2.575595947266" />
                  <Point X="27.00451171875" Y="-2.593012939453" />
                  <Point X="27.04121484375" Y="-2.796234130859" />
                  <Point X="27.04301953125" Y="-2.80423046875" />
                  <Point X="27.05123828125" Y="-2.831540771484" />
                  <Point X="27.064072265625" Y="-2.862479248047" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.735896484375" Y="-4.027722900391" />
                  <Point X="27.7237578125" Y="-4.036082519531" />
                  <Point X="26.894943359375" Y="-2.955952880859" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.82601171875" Y="-2.867368164062" />
                  <Point X="26.809123046875" Y="-2.850334228516" />
                  <Point X="26.800140625" Y="-2.842354980469" />
                  <Point X="26.774818359375" Y="-2.822574707031" />
                  <Point X="26.760607421875" Y="-2.812486816406" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.549783203125" Y="-2.677832519531" />
                  <Point X="26.5207265625" Y="-2.663938476562" />
                  <Point X="26.4987890625" Y="-2.656572998047" />
                  <Point X="26.461126953125" Y="-2.648866210938" />
                  <Point X="26.44190234375" Y="-2.646937744141" />
                  <Point X="26.411564453125" Y="-2.646994873047" />
                  <Point X="26.394513671875" Y="-2.647793701172" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.164626953125" Y="-2.669564941406" />
                  <Point X="26.135994140625" Y="-2.675533935547" />
                  <Point X="26.115869140625" Y="-2.682140869141" />
                  <Point X="26.083791015625" Y="-2.696811279297" />
                  <Point X="26.068439453125" Y="-2.705648681641" />
                  <Point X="26.044888671875" Y="-2.72230859375" />
                  <Point X="26.033142578125" Y="-2.731325195312" />
                  <Point X="25.863876953125" Y="-2.872063232422" />
                  <Point X="25.855220703125" Y="-2.880229980469" />
                  <Point X="25.83322265625" Y="-2.9037578125" />
                  <Point X="25.81951953125" Y="-2.922593017578" />
                  <Point X="25.8007421875" Y="-2.956478515625" />
                  <Point X="25.793099609375" Y="-2.974389648438" />
                  <Point X="25.783775390625" Y="-3.004461181641" />
                  <Point X="25.779587890625" Y="-3.020376220703" />
                  <Point X="25.728978515625" Y="-3.253218994141" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323169433594" />
                  <Point X="25.7255546875" Y="-3.335519775391" />
                  <Point X="25.83308984375" Y="-4.152330078125" />
                  <Point X="25.678923828125" Y="-3.576971435547" />
                  <Point X="25.655068359375" Y="-3.487937255859" />
                  <Point X="25.651130859375" Y="-3.476311035156" />
                  <Point X="25.64179296875" Y="-3.453664550781" />
                  <Point X="25.636392578125" Y="-3.442644042969" />
                  <Point X="25.619419921875" Y="-3.413026367188" />
                  <Point X="25.610658203125" Y="-3.399161132812" />
                  <Point X="25.456681640625" Y="-3.177309814453" />
                  <Point X="25.449296875" Y="-3.167977294922" />
                  <Point X="25.42776953125" Y="-3.144022460938" />
                  <Point X="25.410119140625" Y="-3.128687255859" />
                  <Point X="25.377818359375" Y="-3.106967529297" />
                  <Point X="25.36054296875" Y="-3.097779296875" />
                  <Point X="25.330716796875" Y="-3.085670898438" />
                  <Point X="25.315568359375" Y="-3.080255371094" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.066814453125" Y="-3.0036953125" />
                  <Point X="25.038076171875" Y="-2.998252441406" />
                  <Point X="25.016732421875" Y="-2.996663818359" />
                  <Point X="24.981046875" Y="-2.998042236328" />
                  <Point X="24.96326953125" Y="-3.000423339844" />
                  <Point X="24.933361328125" Y="-3.007353271484" />
                  <Point X="24.919927734375" Y="-3.010988037109" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.67053515625" Y="-3.089170654297" />
                  <Point X="24.641212890625" Y="-3.102486572266" />
                  <Point X="24.621154296875" Y="-3.114798339844" />
                  <Point X="24.590515625" Y="-3.139305664062" />
                  <Point X="24.57647265625" Y="-3.153159423828" />
                  <Point X="24.555205078125" Y="-3.179026855469" />
                  <Point X="24.545880859375" Y="-3.191358886719" />
                  <Point X="24.391904296875" Y="-3.413210205078" />
                  <Point X="24.38753125" Y="-3.420130371094" />
                  <Point X="24.374025390625" Y="-3.445260498047" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936279297" />
                  <Point X="24.01457421875" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.758136386249" Y="0.707025661169" />
                  <Point X="29.607837573616" Y="1.098567454397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.090025496411" Y="2.447514034288" />
                  <Point X="28.986762113917" Y="2.716524342808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.665867988464" Y="0.682302384841" />
                  <Point X="29.510973975356" Y="1.085815084574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.011419935514" Y="2.387197850992" />
                  <Point X="28.874809794483" Y="2.743079435518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.573599590679" Y="0.657579108513" />
                  <Point X="29.414110377097" Y="1.073062714752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.932814374616" Y="2.326881667697" />
                  <Point X="28.79151184527" Y="2.694987341709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.481331192894" Y="0.632855832184" />
                  <Point X="29.317246778838" Y="1.06031034493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.854208813718" Y="2.266565484402" />
                  <Point X="28.708213896058" Y="2.646895247899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.38906279511" Y="0.608132555856" />
                  <Point X="29.220383180578" Y="1.047557975108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.77560325282" Y="2.206249301106" />
                  <Point X="28.624915946845" Y="2.59880315409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.296794397325" Y="0.583409279528" />
                  <Point X="29.123519582319" Y="1.034805605286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.696997691922" Y="2.145933117811" />
                  <Point X="28.541617997633" Y="2.550711060281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.765781086236" Y="-0.903433485656" />
                  <Point X="29.709387440014" Y="-0.756523014564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.20452599954" Y="0.5586860032" />
                  <Point X="29.02665598406" Y="1.022053235463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.618392131025" Y="2.085616934515" />
                  <Point X="28.45832004842" Y="2.502618966471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.729182530474" Y="-1.073181658672" />
                  <Point X="29.595962199072" Y="-0.726130830142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.112257601755" Y="0.533962726871" />
                  <Point X="28.929792385801" Y="1.009300865641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.539786570127" Y="2.02530075122" />
                  <Point X="28.375022099207" Y="2.454526872662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.624521994028" Y="-1.065622310085" />
                  <Point X="29.482536958131" Y="-0.695738645719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.01998920397" Y="0.509239450543" />
                  <Point X="28.832928787541" Y="0.996548495819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.461181009229" Y="1.964984567925" />
                  <Point X="28.291724149995" Y="2.406434778853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.757645594418" Y="3.797756983673" />
                  <Point X="27.680069851285" Y="3.999848703795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.517346954425" Y="-1.051512456823" />
                  <Point X="29.369111717189" Y="-0.665346461297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.927720806185" Y="0.484516174215" />
                  <Point X="28.736065189282" Y="0.983796125997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.382575448331" Y="1.904668384629" />
                  <Point X="28.208426200782" Y="2.358342685043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.696524582697" Y="3.691891992516" />
                  <Point X="27.54577951727" Y="4.084596314017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.410171914823" Y="-1.037402603561" />
                  <Point X="29.255686476248" Y="-0.634954276875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.8354524084" Y="0.459792897886" />
                  <Point X="28.639201591023" Y="0.971043756174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.303969887433" Y="1.844352201334" />
                  <Point X="28.12512825157" Y="2.310250591234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.635403570976" Y="3.586027001359" />
                  <Point X="27.414707776557" Y="4.160959202025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.30299687522" Y="-1.023292750298" />
                  <Point X="29.142261235306" Y="-0.604562092452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.743184001236" Y="0.435069645993" />
                  <Point X="28.542337992763" Y="0.958291386352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.225364311673" Y="1.784036056757" />
                  <Point X="28.041830302357" Y="2.262158497425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.574282559255" Y="3.480162010203" />
                  <Point X="27.283636035844" Y="4.237322090033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.195821835618" Y="-1.009182897036" />
                  <Point X="29.028835994365" Y="-0.57416990803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.65257140811" Y="0.406032851055" />
                  <Point X="28.442873614996" Y="0.952314278785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.14790216363" Y="1.720741181136" />
                  <Point X="27.958532353145" Y="2.214066403615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.513161547534" Y="3.374297019046" />
                  <Point X="27.157828401307" Y="4.299971512604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.088646796015" Y="-0.995073043774" />
                  <Point X="28.915410753424" Y="-0.543777723608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.568658030023" Y="0.359544004277" />
                  <Point X="28.335391139322" Y="0.967225030395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.079102429535" Y="1.634879945666" />
                  <Point X="27.875234403932" Y="2.165974309806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.452040535813" Y="3.268432027889" />
                  <Point X="27.033804928289" Y="4.357973035516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.981471756413" Y="-0.980963190511" />
                  <Point X="28.801985512482" Y="-0.513385539185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.489183514587" Y="0.301491524945" />
                  <Point X="28.213950165652" Y="1.018498912495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.022805350681" Y="1.516448179748" />
                  <Point X="27.791936454719" Y="2.117882215997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.390919524092" Y="3.162567036732" />
                  <Point X="26.90978145527" Y="4.415974558428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.87429671681" Y="-0.966853337249" />
                  <Point X="28.688479254828" Y="-0.482782299011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.419916931259" Y="0.216846473308" />
                  <Point X="27.708638505507" Y="2.069790122187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.329798512371" Y="3.056702045575" />
                  <Point X="26.789151308102" Y="4.465136165274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.767121677207" Y="-0.952743483987" />
                  <Point X="28.561936966549" Y="-0.418219038009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.36658870295" Y="0.0906805873" />
                  <Point X="27.625069254979" Y="2.022404792467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.26867750065" Y="2.950837054418" />
                  <Point X="26.670932917002" Y="4.508014932759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.659946637605" Y="-0.938633630724" />
                  <Point X="27.534858945334" Y="1.992320013231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.207556488929" Y="2.844972063261" />
                  <Point X="26.552714525902" Y="4.550893700244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.552771598002" Y="-0.924523777462" />
                  <Point X="27.439677869976" Y="1.975184521399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.146435477208" Y="2.739107072104" />
                  <Point X="26.435476403826" Y="4.591218779615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.445596527548" Y="-0.910413843828" />
                  <Point X="27.334111269442" Y="1.985104247632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.085314418221" Y="2.633242204079" />
                  <Point X="26.323323651021" Y="4.618296019109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.035342662008" Y="-2.711845720069" />
                  <Point X="28.969155920416" Y="-2.53942336332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.345494235395" Y="-0.914729127604" />
                  <Point X="27.212426853059" Y="2.03701231968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.032193122849" Y="2.506537239339" />
                  <Point X="26.211170898215" Y="4.645373258604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.938182002384" Y="-2.723824218578" />
                  <Point X="28.824909461985" Y="-2.428739162256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.251536610654" Y="-0.935051817261" />
                  <Point X="26.09901814541" Y="4.672450498098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.807449825666" Y="-2.648345925022" />
                  <Point X="28.680663003554" Y="-2.318054961192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.158049012594" Y="-0.956598968285" />
                  <Point X="25.986865392605" Y="4.699527737592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.676717648948" Y="-2.572867631466" />
                  <Point X="28.536416545124" Y="-2.207370760127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.073479015004" Y="-1.001377262777" />
                  <Point X="25.874712639799" Y="4.726604977086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.545985472231" Y="-2.49738933791" />
                  <Point X="28.392170086693" Y="-2.096686559063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.0014455214" Y="-1.07881426671" />
                  <Point X="25.766131314063" Y="4.744378330976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.415253295513" Y="-2.421911044354" />
                  <Point X="28.247923628262" Y="-1.986002357999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.93182716341" Y="-1.162542914023" />
                  <Point X="25.660110352541" Y="4.755481708053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.284521118796" Y="-2.346432750798" />
                  <Point X="28.103677169831" Y="-1.875318156934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.877329550986" Y="-1.28566245026" />
                  <Point X="25.554089391018" Y="4.766585085129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.153788942078" Y="-2.270954457241" />
                  <Point X="27.91423276098" Y="-1.646889269482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.859956829703" Y="-1.505495634435" />
                  <Point X="25.448068429496" Y="4.777688462206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.023056765361" Y="-2.195476163685" />
                  <Point X="25.363931726456" Y="4.73178139682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.89232472798" Y="-2.119998233115" />
                  <Point X="25.322100359837" Y="4.575665162146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.770132796488" Y="-2.066768039004" />
                  <Point X="25.280268993218" Y="4.419548927472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.660543688418" Y="-2.046369322377" />
                  <Point X="25.238437626599" Y="4.263432692798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.551303184342" Y="-2.026878750201" />
                  <Point X="25.177107034018" Y="4.158113678445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.451773827703" Y="-2.03268658202" />
                  <Point X="25.096210798952" Y="4.103764905378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.364398364718" Y="-2.070156389289" />
                  <Point X="25.001611717882" Y="4.085113266589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.279742282089" Y="-2.114710424586" />
                  <Point X="24.887482101678" Y="4.117340411305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.640898052494" Y="4.759713821361" />
                  <Point X="24.632243356776" Y="4.782260074536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.196087156592" Y="-2.161872042363" />
                  <Point X="24.534859612089" Y="4.770862732483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.124509611502" Y="-2.240496832786" />
                  <Point X="24.437475867403" Y="4.75946539043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.700253161705" Y="-4.005450729901" />
                  <Point X="27.655050290653" Y="-3.887693224831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.065226412826" Y="-2.35114949061" />
                  <Point X="24.340092122717" Y="4.748068048377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.496629628861" Y="-3.740083961589" />
                  <Point X="27.351408932844" Y="-3.361771114428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.009034513414" Y="-2.46985525834" />
                  <Point X="24.24270837803" Y="4.736670706324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.293006096017" Y="-3.474717193277" />
                  <Point X="24.145324633344" Y="4.725273364271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.089382563173" Y="-3.209350424966" />
                  <Point X="24.049122703821" Y="4.710797288459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.885758987841" Y="-2.943983545969" />
                  <Point X="23.957251341036" Y="4.685039700595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.724649302453" Y="-2.789369136763" />
                  <Point X="23.86537997825" Y="4.659282112732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.589549457006" Y="-2.702512677161" />
                  <Point X="23.773508615465" Y="4.633524524868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.467715243801" Y="-2.650214371051" />
                  <Point X="23.68163725268" Y="4.607766937004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.366033297299" Y="-2.650414514556" />
                  <Point X="23.589765889894" Y="4.58200934914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.267746393112" Y="-2.659459045671" />
                  <Point X="23.533941397413" Y="4.462346453631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.169582064781" Y="-2.668822897807" />
                  <Point X="23.505687817078" Y="4.270858876384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.079512485419" Y="-2.699274291962" />
                  <Point X="23.448696039408" Y="4.154236862755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.000481341075" Y="-2.758481792477" />
                  <Point X="23.370147923207" Y="4.093771030909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.923342774258" Y="-2.822619626011" />
                  <Point X="23.282673281978" Y="4.056559591799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.847052194783" Y="-2.888966542096" />
                  <Point X="23.190903014671" Y="4.03053864121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.786390345447" Y="-2.99602769216" />
                  <Point X="23.083900461205" Y="4.044199152725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.748637975587" Y="-3.162770076686" />
                  <Point X="22.958004689013" Y="4.107078181738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.732008129714" Y="-3.38453851747" />
                  <Point X="22.796665618291" Y="4.262290160171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.785125463244" Y="-3.788004572608" />
                  <Point X="22.71279390094" Y="4.215692783463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.442425343198" Y="-3.160330907823" />
                  <Point X="22.62892218359" Y="4.169095406755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.309165654608" Y="-3.078268220725" />
                  <Point X="22.54505046624" Y="4.122498030047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.193644044977" Y="-3.042414809153" />
                  <Point X="22.461178748889" Y="4.075900653339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.078122435345" Y="-3.006561397582" />
                  <Point X="22.377307031539" Y="4.029303276631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.973482388505" Y="-2.999055426242" />
                  <Point X="22.29707860215" Y="3.973214810294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.880948033088" Y="-3.023085859253" />
                  <Point X="22.218458244796" Y="3.912937173086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.790021910514" Y="-3.051305882055" />
                  <Point X="22.139837887441" Y="3.852659535879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.69909578794" Y="-3.079525904857" />
                  <Point X="22.061217530087" Y="3.792381898671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.613291173129" Y="-3.121087911526" />
                  <Point X="22.251490029277" Y="3.031614421304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.102761314892" Y="3.419065968754" />
                  <Point X="21.982597172732" Y="3.732104261464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.541132887912" Y="-3.198199822194" />
                  <Point X="22.206346060796" Y="2.884127809517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.475612093059" Y="-3.292602986428" />
                  <Point X="22.134429697969" Y="2.806385669475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.410091298207" Y="-3.387006150663" />
                  <Point X="22.055322164944" Y="2.747377168278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.353065333099" Y="-3.50353910297" />
                  <Point X="21.962587621323" Y="2.723868243372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.31123407316" Y="-3.659655615557" />
                  <Point X="21.858485919362" Y="2.729971778352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.269402813222" Y="-3.815772128144" />
                  <Point X="21.743834258384" Y="2.7635588962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.227571553284" Y="-3.971888640731" />
                  <Point X="21.613101829181" Y="2.839037847502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.185740293346" Y="-4.128005153318" />
                  <Point X="21.482369722994" Y="2.91451595732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.143909033407" Y="-4.284121665905" />
                  <Point X="21.351637616807" Y="2.989994067137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.102077773469" Y="-4.440238178492" />
                  <Point X="21.246164822215" Y="2.999669420539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.060246513531" Y="-4.596354691078" />
                  <Point X="21.178025890786" Y="2.912086735269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.018415253593" Y="-4.752471203665" />
                  <Point X="23.829397748782" Y="-4.260063768849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.762405711495" Y="-4.085543545089" />
                  <Point X="21.109886959358" Y="2.824504049998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.913996218354" Y="-4.745540987234" />
                  <Point X="23.871469411558" Y="-4.634754867892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.605977815071" Y="-3.943125613117" />
                  <Point X="21.551796137493" Y="1.408200612037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.395074178068" Y="1.816475274731" />
                  <Point X="21.04174802793" Y="2.736921364727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.458597978386" Y="-3.824278682626" />
                  <Point X="21.491057145786" Y="1.301340424718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.250827645037" Y="1.927159670137" />
                  <Point X="20.975998473431" Y="2.643114139746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.345620139226" Y="-3.795052019693" />
                  <Point X="21.411253343934" Y="1.244145765828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.106581112005" Y="2.037844065543" />
                  <Point X="20.914610979002" Y="2.537943359778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.24122181542" Y="-3.788175758387" />
                  <Point X="21.321194826489" Y="1.213665554394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.962334578974" Y="2.148528460948" />
                  <Point X="20.853223484572" Y="2.43277257981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.136836753225" Y="-3.781334044761" />
                  <Point X="22.688530456241" Y="-2.613456212853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.619460362986" Y="-2.433522468219" />
                  <Point X="21.703964640856" Y="-0.048574573723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.618773342424" Y="0.173356346229" />
                  <Point X="21.222547967693" Y="1.205558737095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.818088045943" Y="2.259212856354" />
                  <Point X="20.791835990143" Y="2.327601799843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.033161982955" Y="-3.776342704859" />
                  <Point X="22.640935761976" Y="-2.7545584657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.487320186255" Y="-2.354376209224" />
                  <Point X="21.656471272223" Y="-0.189940788866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.487287981558" Y="0.250796751575" />
                  <Point X="21.115386487364" Y="1.219633267242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.939730978224" Y="-3.798037286546" />
                  <Point X="22.579814459715" Y="-2.860422699975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.385524914826" Y="-2.354281131202" />
                  <Point X="22.037015237935" Y="-1.446382382992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.91821080033" Y="-1.13688624175" />
                  <Point X="21.583723911872" Y="-0.265518106346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.373862716541" Y="0.281188998715" />
                  <Point X="21.008211454497" Y="1.233743102959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.856995885857" Y="-3.847595672569" />
                  <Point X="22.518693432744" Y="-2.966287651405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.296871073632" Y="-2.388420649379" />
                  <Point X="21.975235299356" Y="-1.550530810997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.802494287383" Y="-1.100525089681" />
                  <Point X="21.498981571937" Y="-0.309847433678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.260437451524" Y="0.311581245856" />
                  <Point X="20.901036421629" Y="1.247852938675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.776009495546" Y="-3.901709583196" />
                  <Point X="22.457572405774" Y="-3.072152602836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.213573124038" Y="-2.436512742193" />
                  <Point X="21.898239325456" Y="-1.615040111778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.703258109403" Y="-1.10709667802" />
                  <Point X="21.406844114218" Y="-0.334911820541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.147012186508" Y="0.341973492996" />
                  <Point X="20.793861388762" Y="1.261962774392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.695023105235" Y="-3.955823493822" />
                  <Point X="22.396451378803" Y="-3.178017554267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.130275174443" Y="-2.484604835007" />
                  <Point X="21.819633847468" Y="-1.675356511062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.606394497478" Y="-1.11984901224" />
                  <Point X="21.314575717552" Y="-0.359635099782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.033586921491" Y="0.372365740136" />
                  <Point X="20.686686355894" Y="1.276072610109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.615638152712" Y="-4.014109292517" />
                  <Point X="22.335330351833" Y="-3.283882505698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.046977224849" Y="-2.532696927821" />
                  <Point X="21.741028255805" Y="-1.735672614211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.509530885552" Y="-1.13260134646" />
                  <Point X="21.222307321312" Y="-0.384358380135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.920161656475" Y="0.402757987277" />
                  <Point X="20.579511323027" Y="1.290182445826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.546146575927" Y="-4.09816821616" />
                  <Point X="22.274209324862" Y="-3.389747457129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.963679275254" Y="-2.580789020635" />
                  <Point X="21.662422664142" Y="-1.79598871736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.412667273627" Y="-1.145353680681" />
                  <Point X="21.130038925072" Y="-0.409081660488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.806736391458" Y="0.433150234417" />
                  <Point X="20.472336290159" Y="1.304292281542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.460430592322" Y="-4.139961115016" />
                  <Point X="22.213088297892" Y="-3.495612408559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.880381325659" Y="-2.628881113449" />
                  <Point X="21.583817072478" Y="-1.856304820508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.315803661701" Y="-1.158106014901" />
                  <Point X="21.037770528832" Y="-0.433804940841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.693311126441" Y="0.463542481558" />
                  <Point X="20.365921838665" Y="1.31642073504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.326944985087" Y="-4.057309889728" />
                  <Point X="22.151967270921" Y="-3.60147735999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.797083376065" Y="-2.676973206263" />
                  <Point X="21.505211480815" Y="-1.916620923657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.218940049776" Y="-1.170858349121" />
                  <Point X="20.945502132592" Y="-0.458528221194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.579885861425" Y="0.493934728698" />
                  <Point X="20.323813053271" Y="1.161027200985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.188615704321" Y="-3.962040463492" />
                  <Point X="22.090846243951" Y="-3.707342311421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.71378542647" Y="-2.725065299077" />
                  <Point X="21.426605889152" Y="-1.976937026806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.12207643785" Y="-1.183610683342" />
                  <Point X="20.853233736352" Y="-0.483251501547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.466460596408" Y="0.524326975838" />
                  <Point X="20.281704267877" Y="1.005633666929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.0441616779" Y="-3.850815529326" />
                  <Point X="22.02972521698" Y="-3.813207262852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.630487476875" Y="-2.773157391891" />
                  <Point X="21.348000297489" Y="-2.037253129955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.025212825924" Y="-1.196363017562" />
                  <Point X="20.760965340112" Y="-0.5079747819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.353035331391" Y="0.554719222979" />
                  <Point X="20.249122477378" Y="0.82542146265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.547189527281" Y="-2.821249484705" />
                  <Point X="21.269394705826" Y="-2.097569233104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.928349213999" Y="-1.209115351783" />
                  <Point X="20.668696943872" Y="-0.532698062253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.239610066375" Y="0.585111470119" />
                  <Point X="20.220810257119" Y="0.634086647631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.463891577686" Y="-2.869341577519" />
                  <Point X="21.190789114163" Y="-2.157885336253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.831485602073" Y="-1.221867686003" />
                  <Point X="20.576428547632" Y="-0.557421342606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.380593628092" Y="-2.917433670333" />
                  <Point X="21.112183522499" Y="-2.218201439402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.734621990148" Y="-1.234620020223" />
                  <Point X="20.484160151392" Y="-0.582144622959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.297295678497" Y="-2.965525763148" />
                  <Point X="21.033577930836" Y="-2.278517542551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.637758378222" Y="-1.247372354444" />
                  <Point X="20.391891755152" Y="-0.606867903311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.213997728902" Y="-3.013617855962" />
                  <Point X="20.954972339173" Y="-2.3388336457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.540894766297" Y="-1.260124688664" />
                  <Point X="20.299623358912" Y="-0.631591183664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.019084043418" Y="-2.770941015761" />
                  <Point X="20.87636674751" Y="-2.399149748849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.444031154371" Y="-1.272877022884" />
                  <Point X="20.217608260161" Y="-0.683025217181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.347167542445" Y="-1.285629357105" />
                  <Point X="20.322183450039" Y="-1.220543571187" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.495396484375" Y="-3.626146972656" />
                  <Point X="25.471541015625" Y="-3.537112792969" />
                  <Point X="25.454568359375" Y="-3.507495117188" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.28907421875" Y="-3.273825195312" />
                  <Point X="25.259248046875" Y="-3.261716796875" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="25.00615625" Y="-3.18551953125" />
                  <Point X="24.976248046875" Y="-3.192449462891" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.72323828125" Y="-3.273825439453" />
                  <Point X="24.701970703125" Y="-3.299692871094" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537111816406" />
                  <Point X="24.164048828125" Y="-4.943067382812" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.883603515625" Y="-4.933909667969" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.687978515625" Y="-4.572854980469" />
                  <Point X="23.6908515625" Y="-4.551041503906" />
                  <Point X="23.686712890625" Y="-4.516635253906" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.62405078125" Y="-4.215220703125" />
                  <Point X="23.599826171875" Y="-4.190445800781" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.366625" Y="-3.989461914063" />
                  <Point X="23.332322265625" Y="-3.984554443359" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.024958984375" Y="-3.967068115234" />
                  <Point X="22.9947578125" Y="-3.984056396484" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.74026953125" Y="-4.157293457031" />
                  <Point X="22.54290625" Y="-4.414500976563" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="22.12180078125" Y="-4.150389648438" />
                  <Point X="21.771419921875" Y="-3.880608154297" />
                  <Point X="22.449630859375" Y="-2.705910400391" />
                  <Point X="22.493966796875" Y="-2.629120117188" />
                  <Point X="22.50023828125" Y="-2.597594238281" />
                  <Point X="22.48498828125" Y="-2.567732177734" />
                  <Point X="22.468673828125" Y="-2.551416748047" />
                  <Point X="22.439845703125" Y="-2.537198730469" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.195708984375" Y="-3.243569824219" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.83830078125" Y="-2.847135742188" />
                  <Point X="20.822263671875" Y="-2.820242675781" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.758642578125" Y="-1.482667480469" />
                  <Point X="21.836212890625" Y="-1.42314453125" />
                  <Point X="21.85463671875" Y="-1.394242553711" />
                  <Point X="21.8618828125" Y="-1.366266845703" />
                  <Point X="21.859677734375" Y="-1.334601928711" />
                  <Point X="21.837265625" Y="-1.309710693359" />
                  <Point X="21.812359375" Y="-1.295052368164" />
                  <Point X="21.780470703125" Y="-1.288571044922" />
                  <Point X="20.249666015625" Y="-1.490105224609" />
                  <Point X="20.196716796875" Y="-1.497076049805" />
                  <Point X="20.072607421875" Y="-1.011186828613" />
                  <Point X="20.068365234375" Y="-0.981531738281" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="21.354171875" Y="-0.152322845459" />
                  <Point X="21.442537109375" Y="-0.128645339966" />
                  <Point X="21.459763671875" Y="-0.120273399353" />
                  <Point X="21.485857421875" Y="-0.102163726807" />
                  <Point X="21.505654296875" Y="-0.074131278992" />
                  <Point X="21.514353515625" Y="-0.046099601746" />
                  <Point X="21.513802734375" Y="-0.014685463905" />
                  <Point X="21.505103515625" Y="0.01334636879" />
                  <Point X="21.484205078125" Y="0.040749809265" />
                  <Point X="21.45810546875" Y="0.05886472702" />
                  <Point X="21.442537109375" Y="0.066085227966" />
                  <Point X="20.04713671875" Y="0.439981689453" />
                  <Point X="20.001814453125" Y="0.45212588501" />
                  <Point X="20.08235546875" Y="0.996414855957" />
                  <Point X="20.090896484375" Y="1.027936035156" />
                  <Point X="20.226484375" Y="1.528298706055" />
                  <Point X="21.183392578125" Y="1.402319458008" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866699219" />
                  <Point X="21.271955078125" Y="1.397020141602" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.34847265625" Y="1.426060668945" />
                  <Point X="21.36088671875" Y="1.443798095703" />
                  <Point X="21.362353515625" Y="1.447342163086" />
                  <Point X="21.38552734375" Y="1.503290039063" />
                  <Point X="21.389287109375" Y="1.524605224609" />
                  <Point X="21.38368359375" Y="1.54551171875" />
                  <Point X="21.381912109375" Y="1.5489140625" />
                  <Point X="21.353943359375" Y="1.602641357422" />
                  <Point X="21.34003125" Y="1.619221923828" />
                  <Point X="20.539626953125" Y="2.233394042969" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.83998828125" Y="2.7870078125" />
                  <Point X="20.862607421875" Y="2.816083740234" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.80194140625" Y="2.949404541016" />
                  <Point X="21.84084375" Y="2.926943603516" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.86658203125" Y="2.919989013672" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404541016" />
                  <Point X="21.990365234375" Y="2.931020996094" />
                  <Point X="22.04747265625" Y="2.988127929688" />
                  <Point X="22.0591015625" Y="3.006382324219" />
                  <Point X="22.061927734375" Y="3.027841308594" />
                  <Point X="22.061482421875" Y="3.032936279297" />
                  <Point X="22.054443359375" Y="3.113390869141" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.69325" Y="3.748363037109" />
                  <Point X="21.693173828125" Y="3.749623535156" />
                  <Point X="22.247123046875" Y="4.174331054688" />
                  <Point X="22.28275390625" Y="4.194125976562" />
                  <Point X="22.858451171875" Y="4.513971679688" />
                  <Point X="23.020263671875" Y="4.303093261719" />
                  <Point X="23.032173828125" Y="4.287572265625" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.054423828125" Y="4.270708984375" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.192099609375" Y="4.224697265625" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.29448828125" />
                  <Point X="23.31583984375" Y="4.300585449219" />
                  <Point X="23.346197265625" Y="4.396864746094" />
                  <Point X="23.348083984375" Y="4.418424804688" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.31478515625" Y="4.702240234375" />
                  <Point X="24.03190625" Y="4.903296386719" />
                  <Point X="24.07509765625" Y="4.908351074219" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.9463515625" Y="4.353854492188" />
                  <Point X="24.957859375" Y="4.310904296875" />
                  <Point X="24.975716796875" Y="4.284177246094" />
                  <Point X="25.006154296875" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.28417578125" />
                  <Point X="25.054453125" Y="4.31090234375" />
                  <Point X="25.236189453125" Y="4.989149902344" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.860205078125" Y="4.925565429688" />
                  <Point X="25.89594140625" Y="4.916938476562" />
                  <Point X="26.508455078125" Y="4.769058105469" />
                  <Point X="26.53129296875" Y="4.760774902344" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="26.95352734375" Y="4.605267089844" />
                  <Point X="27.338701171875" Y="4.425134277344" />
                  <Point X="27.3604375" Y="4.412470703125" />
                  <Point X="27.73252734375" Y="4.195689941406" />
                  <Point X="27.75301953125" Y="4.1811171875" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.281169921875" Y="2.592477050781" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.22357421875" Y="2.486733398438" />
                  <Point X="27.2033828125" Y="2.411228515625" />
                  <Point X="27.202546875" Y="2.388174072266" />
                  <Point X="27.210416015625" Y="2.32290234375" />
                  <Point X="27.21868359375" Y="2.300812011719" />
                  <Point X="27.2212421875" Y="2.297041748047" />
                  <Point X="27.261642578125" Y="2.237503417969" />
                  <Point X="27.2787109375" Y="2.221645263672" />
                  <Point X="27.33825" Y="2.18124609375" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.36447265625" Y="2.172481445312" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4534453125" Y="2.167225097656" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.9445390625" Y="3.002730957031" />
                  <Point X="28.994248046875" Y="3.031430908203" />
                  <Point X="29.202595703125" Y="2.741875244141" />
                  <Point X="29.214017578125" Y="2.722999755859" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.356177734375" Y="1.644923461914" />
                  <Point X="28.2886171875" Y="1.593082763672" />
                  <Point X="28.2759296875" Y="1.579343383789" />
                  <Point X="28.22158984375" Y="1.508451538086" />
                  <Point X="28.211837890625" Y="1.486916503906" />
                  <Point X="28.191595703125" Y="1.414535644531" />
                  <Point X="28.190779296875" Y="1.390965087891" />
                  <Point X="28.19183203125" Y="1.385865112305" />
                  <Point X="28.20844921875" Y="1.305332275391" />
                  <Point X="28.2185078125" Y="1.283604736328" />
                  <Point X="28.263703125" Y="1.214909667969" />
                  <Point X="28.280947265625" Y="1.198819946289" />
                  <Point X="28.285095703125" Y="1.196485229492" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.374173828125" Y="1.152878295898" />
                  <Point X="28.462728515625" Y="1.141174926758" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.8090390625" Y="1.316695556641" />
                  <Point X="29.848974609375" Y="1.321953125" />
                  <Point X="29.939193359375" Y="0.951365966797" />
                  <Point X="29.942791015625" Y="0.928260131836" />
                  <Point X="29.997859375" Y="0.57455645752" />
                  <Point X="28.81859375" Y="0.258572998047" />
                  <Point X="28.74116796875" Y="0.237826919556" />
                  <Point X="28.723578125" Y="0.22963470459" />
                  <Point X="28.636578125" Y="0.179346893311" />
                  <Point X="28.6189609375" Y="0.162714797974" />
                  <Point X="28.566759765625" Y="0.096199226379" />
                  <Point X="28.556986328125" Y="0.074735321045" />
                  <Point X="28.555884765625" Y="0.068981674194" />
                  <Point X="28.538484375" Y="-0.021875238419" />
                  <Point X="28.5395859375" Y="-0.046438518524" />
                  <Point X="28.556986328125" Y="-0.137295425415" />
                  <Point X="28.566755859375" Y="-0.15875340271" />
                  <Point X="28.570064453125" Y="-0.162971588135" />
                  <Point X="28.622265625" Y="-0.229487014771" />
                  <Point X="28.642087890625" Y="-0.245091552734" />
                  <Point X="28.729087890625" Y="-0.295379364014" />
                  <Point X="28.74116796875" Y="-0.300387023926" />
                  <Point X="29.9638046875" Y="-0.627991638184" />
                  <Point X="29.998068359375" Y="-0.637172668457" />
                  <Point X="29.948431640625" Y="-0.966412353516" />
                  <Point X="29.9438203125" Y="-0.986613769531" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.5017734375" Y="-1.109449462891" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.384025390625" Y="-1.10069152832" />
                  <Point X="28.2132734375" Y="-1.13780480957" />
                  <Point X="28.197947265625" Y="-1.143923095703" />
                  <Point X="28.178912109375" Y="-1.162557617188" />
                  <Point X="28.075703125" Y="-1.286685180664" />
                  <Point X="28.067955078125" Y="-1.299518066406" />
                  <Point X="28.063421875" Y="-1.324250732422" />
                  <Point X="28.048630859375" Y="-1.485001220703" />
                  <Point X="28.0498515625" Y="-1.501458374023" />
                  <Point X="28.062345703125" Y="-1.5259296875" />
                  <Point X="28.156841796875" Y="-1.672912353516" />
                  <Point X="28.168462890625" Y="-1.685540893555" />
                  <Point X="29.303076171875" Y="-2.556161132812" />
                  <Point X="29.339076171875" Y="-2.583784179688" />
                  <Point X="29.2041328125" Y="-2.802139648438" />
                  <Point X="29.194591796875" Y="-2.815697509766" />
                  <Point X="29.056689453125" Y="-3.011638183594" />
                  <Point X="27.833427734375" Y="-2.305386962891" />
                  <Point X="27.753455078125" Y="-2.259215332031" />
                  <Point X="27.72447265625" Y="-2.250988769531" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.50475" Y="-2.214074462891" />
                  <Point X="27.47838671875" Y="-2.224871582031" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2974921875" Y="-2.322614746094" />
                  <Point X="27.282974609375" Y="-2.345374511719" />
                  <Point X="27.19412109375" Y="-2.514201416016" />
                  <Point X="27.188951171875" Y="-2.529873535156" />
                  <Point X="27.19148828125" Y="-2.559243896484" />
                  <Point X="27.22819140625" Y="-2.762465087891" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.963197265625" Y="-4.041423339844" />
                  <Point X="27.986673828125" Y="-4.082087158203" />
                  <Point X="27.83529296875" Y="-4.190215820312" />
                  <Point X="27.824634765625" Y="-4.197115722656" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="26.744205078125" Y="-3.071617431641" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.657857421875" Y="-2.972307128906" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.44226171875" Y="-2.836937255859" />
                  <Point X="26.411923828125" Y="-2.836994384766" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.178166015625" Y="-2.860761474609" />
                  <Point X="26.154615234375" Y="-2.877421386719" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.974576171875" Y="-3.030659667969" />
                  <Point X="25.965251953125" Y="-3.060731201172" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310719726562" />
                  <Point X="26.120552734375" Y="-4.880173339844" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="25.994353515625" Y="-4.963245605469" />
                  <Point X="25.984490234375" Y="-4.965037597656" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#122" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.011508702859" Y="4.398075539562" Z="0.15" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.15" />
                  <Point X="-0.936872690232" Y="4.989292220936" Z="0.15" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.15" />
                  <Point X="-1.704870465505" Y="4.78166399326" Z="0.15" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.15" />
                  <Point X="-1.747969421667" Y="4.749468478074" Z="0.15" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.15" />
                  <Point X="-1.738002751349" Y="4.346901142356" Z="0.15" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.15" />
                  <Point X="-1.833192735946" Y="4.30217107463" Z="0.15" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.15" />
                  <Point X="-1.928644595176" Y="4.346339371174" Z="0.15" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.15" />
                  <Point X="-1.946224711962" Y="4.36481210394" Z="0.15" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.15" />
                  <Point X="-2.747686667459" Y="4.269113413819" Z="0.15" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.15" />
                  <Point X="-3.343404395585" Y="3.820583624271" Z="0.15" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.15" />
                  <Point X="-3.356208371882" Y="3.754643037716" Z="0.15" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.15" />
                  <Point X="-2.994485763037" Y="3.059858457517" Z="0.15" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.15" />
                  <Point X="-3.051147157687" Y="2.997656410199" Z="0.15" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.15" />
                  <Point X="-3.135218008089" Y="3.001078936136" Z="0.15" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.15" />
                  <Point X="-3.179216310581" Y="3.023985576864" Z="0.15" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.15" />
                  <Point X="-4.183011954007" Y="2.878066195908" Z="0.15" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.15" />
                  <Point X="-4.527406680284" Y="2.298588571582" Z="0.15" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.15" />
                  <Point X="-4.496967274814" Y="2.225006398789" Z="0.15" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.15" />
                  <Point X="-3.668593594178" Y="1.557106749496" Z="0.15" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.15" />
                  <Point X="-3.690002024496" Y="1.497743894349" Z="0.15" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.15" />
                  <Point X="-3.74923782805" Y="1.475985973062" Z="0.15" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.15" />
                  <Point X="-3.816238884618" Y="1.483171776995" Z="0.15" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.15" />
                  <Point X="-4.963520230196" Y="1.072293295875" Z="0.15" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.15" />
                  <Point X="-5.055116201663" Y="0.481857461341" Z="0.15" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.15" />
                  <Point X="-4.971961173475" Y="0.422965458243" Z="0.15" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.15" />
                  <Point X="-3.55046068527" Y="0.030954106588" Z="0.15" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.15" />
                  <Point X="-3.54010769313" Y="0.001775156517" Z="0.15" />
                  <Point X="-3.539556741714" Y="0" Z="0.15" />
                  <Point X="-3.548256908005" Y="-0.028031794544" Z="0.15" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.15" />
                  <Point X="-3.574907909853" Y="-0.047921876546" Z="0.15" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.15" />
                  <Point X="-3.664926615297" Y="-0.072746598513" Z="0.15" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.15" />
                  <Point X="-4.987287092" Y="-0.957330728527" Z="0.15" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.15" />
                  <Point X="-4.854988431792" Y="-1.489506992252" Z="0.15" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.15" />
                  <Point X="-4.749962810529" Y="-1.508397429029" Z="0.15" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.15" />
                  <Point X="-3.194253479099" Y="-1.321521547764" Z="0.15" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.15" />
                  <Point X="-3.199921959956" Y="-1.350425562282" Z="0.15" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.15" />
                  <Point X="-3.277952523737" Y="-1.411720043219" Z="0.15" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.15" />
                  <Point X="-4.226837027934" Y="-2.814572192927" Z="0.15" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.15" />
                  <Point X="-3.882911418271" Y="-3.272766588511" Z="0.15" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.15" />
                  <Point X="-3.785448550137" Y="-3.255591123261" Z="0.15" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.15" />
                  <Point X="-2.556524164256" Y="-2.571806304876" Z="0.15" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.15" />
                  <Point X="-2.599825889907" Y="-2.649629832355" Z="0.15" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.15" />
                  <Point X="-2.914860386648" Y="-4.158726613664" Z="0.15" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.15" />
                  <Point X="-2.477283757517" Y="-4.433340229116" Z="0.15" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.15" />
                  <Point X="-2.43772407707" Y="-4.43208659527" Z="0.15" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.15" />
                  <Point X="-1.983619209206" Y="-3.99434975326" Z="0.15" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.15" />
                  <Point X="-1.67710446766" Y="-4.003167467923" Z="0.15" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.15" />
                  <Point X="-1.439297858026" Y="-4.19675763859" Z="0.15" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.15" />
                  <Point X="-1.368483254394" Y="-4.495110101238" Z="0.15" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.15" />
                  <Point X="-1.367750313795" Y="-4.535045545261" Z="0.15" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.15" />
                  <Point X="-1.135012120963" Y="-4.951052599742" Z="0.15" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.15" />
                  <Point X="-0.835518759953" Y="-5.010298071087" Z="0.15" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.15" />
                  <Point X="-0.793811413325" Y="-4.924728651172" Z="0.15" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.15" />
                  <Point X="-0.26310990206" Y="-3.296921753366" Z="0.15" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.15" />
                  <Point X="-0.015088830121" Y="-3.208922384478" Z="0.15" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.15" />
                  <Point X="0.238270249241" Y="-3.27818966081" Z="0.15" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.15" />
                  <Point X="0.407335957372" Y="-3.504724002402" Z="0.15" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.15" />
                  <Point X="0.440943447076" Y="-3.607807378644" Z="0.15" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.15" />
                  <Point X="0.987269864611" Y="-4.982952496531" Z="0.15" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.15" />
                  <Point X="1.166390058479" Y="-4.944092599902" Z="0.15" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.15" />
                  <Point X="1.163968286797" Y="-4.8423672347" Z="0.15" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.15" />
                  <Point X="1.007955103049" Y="-3.040071127358" Z="0.15" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.15" />
                  <Point X="1.180424798384" Y="-2.884587897561" Z="0.15" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.15" />
                  <Point X="1.410348949887" Y="-2.855503978055" Z="0.15" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.15" />
                  <Point X="1.62466130522" Y="-2.983085105949" Z="0.15" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.15" />
                  <Point X="1.698379597175" Y="-3.070775494288" Z="0.15" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.15" />
                  <Point X="2.845646816922" Y="-4.207809783023" Z="0.15" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.15" />
                  <Point X="3.0352424511" Y="-4.073164982596" Z="0.15" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.15" />
                  <Point X="3.000340966759" Y="-3.985143387291" Z="0.15" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.15" />
                  <Point X="2.234535298159" Y="-2.519077473294" Z="0.15" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.15" />
                  <Point X="2.3210634839" Y="-2.337381411913" Z="0.15" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.15" />
                  <Point X="2.495516966459" Y="-2.237837826067" Z="0.15" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.15" />
                  <Point X="2.709429330218" Y="-2.268912711332" Z="0.15" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.15" />
                  <Point X="2.802270209816" Y="-2.317408533515" Z="0.15" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.15" />
                  <Point X="4.229323494738" Y="-2.813194915548" Z="0.15" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.15" />
                  <Point X="4.38971032048" Y="-2.555716420361" Z="0.15" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.15" />
                  <Point X="4.327357336097" Y="-2.485213481267" Z="0.15" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.15" />
                  <Point X="3.098246623784" Y="-1.467610399045" Z="0.15" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.15" />
                  <Point X="3.107054717467" Y="-1.297552003838" Z="0.15" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.15" />
                  <Point X="3.211199707681" Y="-1.163244746868" Z="0.15" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.15" />
                  <Point X="3.388486774659" Y="-1.118270693293" Z="0.15" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.15" />
                  <Point X="3.489091522686" Y="-1.127741720224" Z="0.15" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.15" />
                  <Point X="4.986410051966" Y="-0.966457639766" Z="0.15" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.15" />
                  <Point X="5.044645683828" Y="-0.591510038319" Z="0.15" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.15" />
                  <Point X="4.970589745638" Y="-0.548415230941" Z="0.15" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.15" />
                  <Point X="3.660952157854" Y="-0.170522764267" Z="0.15" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.15" />
                  <Point X="3.603242389607" Y="-0.100822726381" Z="0.15" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.15" />
                  <Point X="3.582536615348" Y="-0.005753651719" Z="0.15" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.15" />
                  <Point X="3.598834835076" Y="0.090856879526" Z="0.15" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.15" />
                  <Point X="3.652137048793" Y="0.163126012194" Z="0.15" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.15" />
                  <Point X="3.742443256498" Y="0.217626079975" Z="0.15" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.15" />
                  <Point X="3.825378071668" Y="0.241556701828" Z="0.15" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.15" />
                  <Point X="4.986037699857" Y="0.967232133109" Z="0.15" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.15" />
                  <Point X="4.886820031753" Y="1.383874968824" Z="0.15" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.15" />
                  <Point X="4.796356313982" Y="1.397547841595" Z="0.15" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.15" />
                  <Point X="3.374568376336" Y="1.233727445843" Z="0.15" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.15" />
                  <Point X="3.303466093602" Y="1.271336324322" Z="0.15" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.15" />
                  <Point X="3.25412306934" Y="1.342366178082" Z="0.15" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.15" />
                  <Point X="3.234644310549" Y="1.42724933336" Z="0.15" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.15" />
                  <Point X="3.253834124319" Y="1.504730072772" Z="0.15" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.15" />
                  <Point X="3.309456934829" Y="1.580205752644" Z="0.15" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.15" />
                  <Point X="3.380458303065" Y="1.636535820832" Z="0.15" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.15" />
                  <Point X="4.250638612368" Y="2.780165975547" Z="0.15" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.15" />
                  <Point X="4.016311573031" Y="3.109099700803" Z="0.15" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.15" />
                  <Point X="3.913382100258" Y="3.077312237825" Z="0.15" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.15" />
                  <Point X="2.434373113763" Y="2.246807798973" Z="0.15" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.15" />
                  <Point X="2.364301394354" Y="2.253401971021" Z="0.15" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.15" />
                  <Point X="2.300628430385" Y="2.294299730617" Z="0.15" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.15" />
                  <Point X="2.256458869618" Y="2.356396429997" Z="0.15" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.15" />
                  <Point X="2.246027731738" Y="2.425457049478" Z="0.15" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.15" />
                  <Point X="2.265720143684" Y="2.505096462119" Z="0.15" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.15" />
                  <Point X="2.318313115035" Y="2.598756981697" Z="0.15" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.15" />
                  <Point X="2.775838676083" Y="4.253144432393" Z="0.15" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.15" />
                  <Point X="2.379449882441" Y="4.486953207086" Z="0.15" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.15" />
                  <Point X="1.968551955935" Y="4.681838926665" Z="0.15" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.15" />
                  <Point X="1.542185104964" Y="4.839059007699" Z="0.15" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.15" />
                  <Point X="0.901518366163" Y="4.996821896732" Z="0.15" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.15" />
                  <Point X="0.233105642505" Y="5.072148512605" Z="0.15" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.15" />
                  <Point X="0.181735858694" Y="5.033371966389" Z="0.15" />
                  <Point X="0" Y="4.355124473572" Z="0.15" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>