<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#183" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2481" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.823724609375" Y="-4.484426757812" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467376708984" />
                  <Point X="25.435923828125" Y="-3.314016357422" />
                  <Point X="25.37863671875" Y="-3.231476318359" />
                  <Point X="25.356751953125" Y="-3.209021240234" />
                  <Point X="25.33049609375" Y="-3.189777099609" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.137787109375" Y="-3.124549072266" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766357422" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097036132812" />
                  <Point X="24.798466796875" Y="-3.148156005859" />
                  <Point X="24.70981640625" Y="-3.175669433594" />
                  <Point X="24.68181640625" Y="-3.18977734375" />
                  <Point X="24.655560546875" Y="-3.209021484375" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.527236328125" Y="-3.384837402344" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481571777344" />
                  <Point X="24.449009765625" Y="-3.512524658203" />
                  <Point X="24.308849609375" Y="-4.035612060547" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="24.021259765625" Y="-4.864876953125" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.75358203125" Y="-4.802362792969" />
                  <Point X="23.753689453125" Y="-4.801552734375" />
                  <Point X="23.7850390625" Y="-4.5634375" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.744142578125" Y="-4.318401367187" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182963867188" />
                  <Point X="23.69598828125" Y="-4.155126953125" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.524712890625" Y="-3.998215576172" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.15570703125" Y="-3.877774658203" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.78963671875" Y="-4.006859130859" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.586208984375" Y="-4.20201171875" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.345744140625" Y="-4.180686035156" />
                  <Point X="22.198287109375" Y="-4.089383789063" />
                  <Point X="21.937955078125" Y="-3.888937988281" />
                  <Point X="21.895279296875" Y="-3.856078613281" />
                  <Point X="22.09227734375" Y="-3.514865966797" />
                  <Point X="22.576240234375" Y="-2.676619384766" />
                  <Point X="22.587140625" Y="-2.647654296875" />
                  <Point X="22.593412109375" Y="-2.616127197266" />
                  <Point X="22.594423828125" Y="-2.585190917969" />
                  <Point X="22.585439453125" Y="-2.555571044922" />
                  <Point X="22.571220703125" Y="-2.526741699219" />
                  <Point X="22.5531953125" Y="-2.501589355469" />
                  <Point X="22.535853515625" Y="-2.484246826172" />
                  <Point X="22.510705078125" Y="-2.466222412109" />
                  <Point X="22.481875" Y="-2.452000976562" />
                  <Point X="22.45225390625" Y="-2.443012939453" />
                  <Point X="22.42131640625" Y="-2.444023925781" />
                  <Point X="22.389787109375" Y="-2.450294433594" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.9096640625" Y="-2.721670410156" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="21.033634765625" Y="-2.946910888672" />
                  <Point X="20.917142578125" Y="-2.79386328125" />
                  <Point X="20.730501953125" Y="-2.480895996094" />
                  <Point X="20.693857421875" Y="-2.419449951172" />
                  <Point X="21.04726953125" Y="-2.148267822266" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.915421875" Y="-1.475594238281" />
                  <Point X="21.933388671875" Y="-1.44846105957" />
                  <Point X="21.94614453125" Y="-1.41983215332" />
                  <Point X="21.953849609375" Y="-1.390084960938" />
                  <Point X="21.956654296875" Y="-1.359647949219" />
                  <Point X="21.954443359375" Y="-1.327978881836" />
                  <Point X="21.94744140625" Y="-1.298236816406" />
                  <Point X="21.931359375" Y="-1.272255859375" />
                  <Point X="21.91052734375" Y="-1.248300537109" />
                  <Point X="21.887029296875" Y="-1.228767700195" />
                  <Point X="21.860546875" Y="-1.213181152344" />
                  <Point X="21.83128515625" Y="-1.201957397461" />
                  <Point X="21.7993984375" Y="-1.195475097656" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="21.19853515625" Y="-1.269364868164" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.211486328125" Y="-1.171030273438" />
                  <Point X="20.165923828125" Y="-0.992650268555" />
                  <Point X="20.11654296875" Y="-0.647395751953" />
                  <Point X="20.107576171875" Y="-0.584698425293" />
                  <Point X="20.50251953125" Y="-0.478873657227" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.482509765625" Y="-0.214826828003" />
                  <Point X="21.51226953125" Y="-0.199470809937" />
                  <Point X="21.5303125" Y="-0.186948394775" />
                  <Point X="21.5400234375" Y="-0.180208755493" />
                  <Point X="21.56248046875" Y="-0.158324523926" />
                  <Point X="21.5817265625" Y="-0.132066467285" />
                  <Point X="21.595833984375" Y="-0.104063110352" />
                  <Point X="21.60184765625" Y="-0.084685394287" />
                  <Point X="21.605083984375" Y="-0.07425617981" />
                  <Point X="21.609353515625" Y="-0.046098609924" />
                  <Point X="21.609353515625" Y="-0.016461536407" />
                  <Point X="21.605083984375" Y="0.011696187019" />
                  <Point X="21.5990703125" Y="0.031073747635" />
                  <Point X="21.595833984375" Y="0.041502960205" />
                  <Point X="21.5817265625" Y="0.069506469727" />
                  <Point X="21.56248046875" Y="0.095764381409" />
                  <Point X="21.5400234375" Y="0.117648757935" />
                  <Point X="21.52198046875" Y="0.130171020508" />
                  <Point X="21.513740234375" Y="0.135286956787" />
                  <Point X="21.488462890625" Y="0.149246505737" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="20.94796484375" Y="0.296956939697" />
                  <Point X="20.10818359375" Y="0.521975646973" />
                  <Point X="20.1461484375" Y="0.778540344238" />
                  <Point X="20.17551171875" Y="0.976968078613" />
                  <Point X="20.2749140625" Y="1.343791870117" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.5428828125" Y="1.390824462891" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255017578125" Y="1.299341796875" />
                  <Point X="21.276578125" Y="1.301228271484" />
                  <Point X="21.29686328125" Y="1.305263549805" />
                  <Point X="21.336796875" Y="1.317854492188" />
                  <Point X="21.3582890625" Y="1.324631103516" />
                  <Point X="21.377224609375" Y="1.332963256836" />
                  <Point X="21.39596875" Y="1.343785766602" />
                  <Point X="21.4126484375" Y="1.35601550293" />
                  <Point X="21.42628515625" Y="1.371564819336" />
                  <Point X="21.438701171875" Y="1.389295776367" />
                  <Point X="21.44865234375" Y="1.407432983398" />
                  <Point X="21.46467578125" Y="1.44611706543" />
                  <Point X="21.473298828125" Y="1.466937255859" />
                  <Point X="21.479087890625" Y="1.486806152344" />
                  <Point X="21.48284375" Y="1.508120605469" />
                  <Point X="21.484193359375" Y="1.528755615234" />
                  <Point X="21.481048828125" Y="1.549193969727" />
                  <Point X="21.475447265625" Y="1.570099731445" />
                  <Point X="21.46794921875" Y="1.589378417969" />
                  <Point X="21.448615234375" Y="1.626518798828" />
                  <Point X="21.438208984375" Y="1.646508056641" />
                  <Point X="21.426716796875" Y="1.663707763672" />
                  <Point X="21.4128046875" Y="1.680287231445" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="21.100072265625" Y="1.92309387207" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.80475390625" Y="2.538189208984" />
                  <Point X="20.918853515625" Y="2.733665527344" />
                  <Point X="21.18215234375" Y="3.072102539062" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.368671875" Y="3.089855224609" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.90882421875" Y="2.820930664063" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.00098828125" Y="2.826504882812" />
                  <Point X="22.0195390625" Y="2.835654541016" />
                  <Point X="22.03779296875" Y="2.847284179688" />
                  <Point X="22.053921875" Y="2.860228515625" />
                  <Point X="22.093400390625" Y="2.899705322266" />
                  <Point X="22.114646484375" Y="2.920951904297" />
                  <Point X="22.127595703125" Y="2.937086181641" />
                  <Point X="22.139224609375" Y="2.955340576172" />
                  <Point X="22.14837109375" Y="2.973889404297" />
                  <Point X="22.1532890625" Y="2.993978271484" />
                  <Point X="22.156115234375" Y="3.015437255859" />
                  <Point X="22.15656640625" Y="3.036120605469" />
                  <Point X="22.151701171875" Y="3.091736816406" />
                  <Point X="22.14908203125" Y="3.121670166016" />
                  <Point X="22.145044921875" Y="3.141964355469" />
                  <Point X="22.13853515625" Y="3.162605957031" />
                  <Point X="22.130205078125" Y="3.181533203125" />
                  <Point X="21.998244140625" Y="3.410095458984" />
                  <Point X="21.81666796875" Y="3.724595703125" />
                  <Point X="22.100666015625" Y="3.942333984375" />
                  <Point X="22.29937890625" Y="4.094685791016" />
                  <Point X="22.714072265625" Y="4.325080566406" />
                  <Point X="22.83296484375" Y="4.391134277344" />
                  <Point X="22.9568046875" Y="4.229740722656" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.066787109375" Y="4.157171386719" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222548828125" Y="4.134482421875" />
                  <Point X="23.287021484375" Y="4.161188476562" />
                  <Point X="23.32172265625" Y="4.175562011719" />
                  <Point X="23.339857421875" Y="4.18551171875" />
                  <Point X="23.3575859375" Y="4.19792578125" />
                  <Point X="23.37313671875" Y="4.2115625" />
                  <Point X="23.3853671875" Y="4.2282421875" />
                  <Point X="23.396189453125" Y="4.246985839844" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.425505859375" Y="4.332477050781" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583007812" />
                  <Point X="23.44272265625" Y="4.410145019531" />
                  <Point X="23.442271484375" Y="4.430827636719" />
                  <Point X="23.427267578125" Y="4.544782714844" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.793123046875" Y="4.737687011719" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.553099609375" Y="4.868645996094" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.740466796875" Y="4.755171386719" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.21383203125" Y="4.538658691406" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.61942578125" Y="4.855262207031" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.25997265625" Y="4.731320800781" />
                  <Point X="26.48102734375" Y="4.677950683594" />
                  <Point X="26.751240234375" Y="4.579943359375" />
                  <Point X="26.894640625" Y="4.527930664062" />
                  <Point X="27.156431640625" Y="4.4055" />
                  <Point X="27.294580078125" Y="4.340891601563" />
                  <Point X="27.547498046875" Y="4.193541992188" />
                  <Point X="27.68097265625" Y="4.115778320312" />
                  <Point X="27.91949609375" Y="3.946155761719" />
                  <Point X="27.94326171875" Y="3.929254394531" />
                  <Point X="27.707732421875" Y="3.521305664062" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142078125" Y="2.539932373047" />
                  <Point X="27.133078125" Y="2.51605859375" />
                  <Point X="27.119119140625" Y="2.463864013672" />
                  <Point X="27.111607421875" Y="2.435772216797" />
                  <Point X="27.108619140625" Y="2.417936035156" />
                  <Point X="27.107728515625" Y="2.380953613281" />
                  <Point X="27.113171875" Y="2.335820068359" />
                  <Point X="27.116099609375" Y="2.311528808594" />
                  <Point X="27.12144140625" Y="2.289607177734" />
                  <Point X="27.129708984375" Y="2.267514892578" />
                  <Point X="27.140072265625" Y="2.247469726562" />
                  <Point X="27.168" Y="2.206312255859" />
                  <Point X="27.18303125" Y="2.184161132813" />
                  <Point X="27.194470703125" Y="2.170325439453" />
                  <Point X="27.2216015625" Y="2.145592529297" />
                  <Point X="27.2627578125" Y="2.117665527344" />
                  <Point X="27.28491015625" Y="2.102635009766" />
                  <Point X="27.304955078125" Y="2.092271972656" />
                  <Point X="27.327041015625" Y="2.084006591797" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.39409765625" Y="2.073221191406" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.473205078125" Y="2.074170898438" />
                  <Point X="27.525400390625" Y="2.088128417969" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.1107109375" Y="2.411623046875" />
                  <Point X="28.967326171875" Y="2.906190673828" />
                  <Point X="29.04409375" Y="2.799501220703" />
                  <Point X="29.12328125" Y="2.689451416016" />
                  <Point X="29.2562421875" Y="2.469729492188" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.968275390625" Y="2.234346679688" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.22142578125" Y="1.660241699219" />
                  <Point X="28.20397265625" Y="1.641626831055" />
                  <Point X="28.166408203125" Y="1.59262109375" />
                  <Point X="28.14619140625" Y="1.566245727539" />
                  <Point X="28.13660546875" Y="1.550909790039" />
                  <Point X="28.1216328125" Y="1.517088623047" />
                  <Point X="28.107638671875" Y="1.467053466797" />
                  <Point X="28.100107421875" Y="1.440124145508" />
                  <Point X="28.09665234375" Y="1.417824707031" />
                  <Point X="28.0958359375" Y="1.394255004883" />
                  <Point X="28.097740234375" Y="1.371766479492" />
                  <Point X="28.109228515625" Y="1.316095947266" />
                  <Point X="28.11541015625" Y="1.286133544922" />
                  <Point X="28.1206796875" Y="1.268982299805" />
                  <Point X="28.136283203125" Y="1.235741455078" />
                  <Point X="28.167525390625" Y="1.188254150391" />
                  <Point X="28.18433984375" Y="1.162695922852" />
                  <Point X="28.198892578125" Y="1.145451538086" />
                  <Point X="28.21613671875" Y="1.129361083984" />
                  <Point X="28.23434765625" Y="1.116034545898" />
                  <Point X="28.279623046875" Y="1.090548828125" />
                  <Point X="28.303990234375" Y="1.07683215332" />
                  <Point X="28.3205234375" Y="1.069501098633" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.417333984375" Y="1.051348266602" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.984236328125" Y="1.112288330078" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.8119296875" Y="1.072496826172" />
                  <Point X="29.845939453125" Y="0.932788513184" />
                  <Point X="29.88783984375" Y="0.663676757812" />
                  <Point X="29.890865234375" Y="0.644238830566" />
                  <Point X="29.561759765625" Y="0.556054626465" />
                  <Point X="28.716580078125" Y="0.32958972168" />
                  <Point X="28.704791015625" Y="0.325586120605" />
                  <Point X="28.681546875" Y="0.315067810059" />
                  <Point X="28.62140625" Y="0.280305053711" />
                  <Point X="28.589037109375" Y="0.26159552002" />
                  <Point X="28.574314453125" Y="0.251098770142" />
                  <Point X="28.54753125" Y="0.225576583862" />
                  <Point X="28.511447265625" Y="0.179596084595" />
                  <Point X="28.492025390625" Y="0.154848937988" />
                  <Point X="28.480298828125" Y="0.135564727783" />
                  <Point X="28.470525390625" Y="0.114098838806" />
                  <Point X="28.463681640625" Y="0.09260395813" />
                  <Point X="28.45165234375" Y="0.029796735764" />
                  <Point X="28.4451796875" Y="-0.004006680489" />
                  <Point X="28.443484375" Y="-0.021876962662" />
                  <Point X="28.4451796875" Y="-0.058553314209" />
                  <Point X="28.45720703125" Y="-0.121360534668" />
                  <Point X="28.463681640625" Y="-0.155163955688" />
                  <Point X="28.47052734375" Y="-0.176665664673" />
                  <Point X="28.48030078125" Y="-0.198128982544" />
                  <Point X="28.492025390625" Y="-0.217408935547" />
                  <Point X="28.528109375" Y="-0.263389434814" />
                  <Point X="28.54753125" Y="-0.28813671875" />
                  <Point X="28.560001953125" Y="-0.301238983154" />
                  <Point X="28.589037109375" Y="-0.324155670166" />
                  <Point X="28.649177734375" Y="-0.358918395996" />
                  <Point X="28.681546875" Y="-0.377628082275" />
                  <Point X="28.6927109375" Y="-0.38313885498" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="29.17146484375" Y="-0.514035583496" />
                  <Point X="29.891474609375" Y="-0.706961730957" />
                  <Point X="29.87401171875" Y="-0.822784667969" />
                  <Point X="29.855025390625" Y="-0.948724487305" />
                  <Point X="29.80133984375" Y="-1.18397644043" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="29.404552734375" Y="-1.132482788086" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.256623046875" Y="-1.031164550781" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.163978515625" Y="-1.056595947266" />
                  <Point X="28.136150390625" Y="-1.07348840332" />
                  <Point X="28.1123984375" Y="-1.093960083008" />
                  <Point X="28.041052734375" Y="-1.179766357422" />
                  <Point X="28.002654296875" Y="-1.225948120117" />
                  <Point X="27.987931640625" Y="-1.250334228516" />
                  <Point X="27.97658984375" Y="-1.277718994141" />
                  <Point X="27.969759765625" Y="-1.305365722656" />
                  <Point X="27.95953515625" Y="-1.416488769531" />
                  <Point X="27.95403125" Y="-1.476296386719" />
                  <Point X="27.95634765625" Y="-1.507560546875" />
                  <Point X="27.964078125" Y="-1.539182373047" />
                  <Point X="27.976451171875" Y="-1.567996826172" />
                  <Point X="28.0417734375" Y="-1.669602294922" />
                  <Point X="28.076931640625" Y="-1.724287353516" />
                  <Point X="28.0869375" Y="-1.737243774414" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.532765625" Y="-2.084824707031" />
                  <Point X="29.213123046875" Y="-2.606882080078" />
                  <Point X="29.178333984375" Y="-2.663175048828" />
                  <Point X="29.124796875" Y="-2.749804199219" />
                  <Point X="29.028982421875" Y="-2.885944824219" />
                  <Point X="28.6739296875" Y="-2.680954833984" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.61374609375" Y="-2.134454345703" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.32812890625" Y="-2.196598388672" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246549072266" />
                  <Point X="27.22142578125" Y="-2.267508300781" />
                  <Point X="27.204533203125" Y="-2.290438476562" />
                  <Point X="27.143111328125" Y="-2.407144287109" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531909423828" />
                  <Point X="27.09567578125" Y="-2.563259277344" />
                  <Point X="27.121046875" Y="-2.703740966797" />
                  <Point X="27.134703125" Y="-2.779349609375" />
                  <Point X="27.13898828125" Y="-2.795141601562" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.4230859375" Y="-3.295921630859" />
                  <Point X="27.861287109375" Y="-4.05490625" />
                  <Point X="27.8453671875" Y="-4.066276855469" />
                  <Point X="27.78186328125" Y="-4.111635253906" />
                  <Point X="27.701767578125" Y="-4.16348046875" />
                  <Point X="27.42470703125" Y="-3.80241015625" />
                  <Point X="26.758548828125" Y="-2.934254882812" />
                  <Point X="26.74750390625" Y="-2.922177490234" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.58337109375" Y="-2.811480224609" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479990234375" Y="-2.751167236328" />
                  <Point X="26.448369140625" Y="-2.743435546875" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.265568359375" Y="-2.755060791016" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.156365234375" Y="-2.769396972656" />
                  <Point X="26.12898046875" Y="-2.780739746094" />
                  <Point X="26.10459765625" Y="-2.7954609375" />
                  <Point X="25.98758984375" Y="-2.892749755859" />
                  <Point X="25.92461328125" Y="-2.945111328125" />
                  <Point X="25.904142578125" Y="-2.968860839844" />
                  <Point X="25.88725" Y="-2.9966875" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.840640625" Y="-3.186767089844" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120361328" />
                  <Point X="25.8966171875" Y="-3.907037353516" />
                  <Point X="26.022064453125" Y="-4.859915039062" />
                  <Point X="25.9756796875" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941580078125" Y="-4.752637695312" />
                  <Point X="23.858755859375" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575837890625" />
                  <Point X="23.879923828125" Y="-4.568098144531" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497689453125" />
                  <Point X="23.83731640625" Y="-4.299867675781" />
                  <Point X="23.81613671875" Y="-4.193397949219" />
                  <Point X="23.811875" Y="-4.17846875" />
                  <Point X="23.80097265625" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135462402344" />
                  <Point X="23.778259765625" Y="-4.107625488281" />
                  <Point X="23.769423828125" Y="-4.094859863281" />
                  <Point X="23.749791015625" Y="-4.070937011719" />
                  <Point X="23.738994140625" Y="-4.059779785156" />
                  <Point X="23.5873515625" Y="-3.926791259766" />
                  <Point X="23.505734375" Y="-3.855215332031" />
                  <Point X="23.493263671875" Y="-3.845965820312" />
                  <Point X="23.46698046875" Y="-3.829621582031" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.161919921875" Y="-3.782978027344" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812255859" />
                  <Point X="22.736857421875" Y="-3.927869628906" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629394531" />
                  <Point X="22.51083984375" Y="-4.144179199219" />
                  <Point X="22.496798828125" Y="-4.162478515625" />
                  <Point X="22.395755859375" Y="-4.099915527344" />
                  <Point X="22.25241015625" Y="-4.011158447266" />
                  <Point X="22.019138671875" Y="-3.831548828125" />
                  <Point X="22.17455078125" Y="-3.562365722656" />
                  <Point X="22.658513671875" Y="-2.724119140625" />
                  <Point X="22.66515234375" Y="-2.710079589844" />
                  <Point X="22.676052734375" Y="-2.681114501953" />
                  <Point X="22.680314453125" Y="-2.666188964844" />
                  <Point X="22.6865859375" Y="-2.634661865234" />
                  <Point X="22.688361328125" Y="-2.619232421875" />
                  <Point X="22.689373046875" Y="-2.588296142578" />
                  <Point X="22.685333984375" Y="-2.557615966797" />
                  <Point X="22.676349609375" Y="-2.52799609375" />
                  <Point X="22.670640625" Y="-2.513549560547" />
                  <Point X="22.656421875" Y="-2.484720214844" />
                  <Point X="22.648439453125" Y="-2.471403320312" />
                  <Point X="22.6304140625" Y="-2.446250976562" />
                  <Point X="22.62037109375" Y="-2.434415527344" />
                  <Point X="22.603029296875" Y="-2.417072998047" />
                  <Point X="22.5911953125" Y="-2.40703125" />
                  <Point X="22.566046875" Y="-2.389006835938" />
                  <Point X="22.552732421875" Y="-2.381024169922" />
                  <Point X="22.52390234375" Y="-2.366802734375" />
                  <Point X="22.509458984375" Y="-2.36109375" />
                  <Point X="22.479837890625" Y="-2.352105712891" />
                  <Point X="22.449150390625" Y="-2.348063720703" />
                  <Point X="22.418212890625" Y="-2.349074707031" />
                  <Point X="22.40278515625" Y="-2.350848632812" />
                  <Point X="22.371255859375" Y="-2.357119140625" />
                  <Point X="22.35632421875" Y="-2.361383056641" />
                  <Point X="22.32735546875" Y="-2.372285888672" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.8621640625" Y="-2.639397949219" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="21.109228515625" Y="-2.889372558594" />
                  <Point X="20.99598046875" Y="-2.740587646484" />
                  <Point X="20.818734375" Y="-2.443373291016" />
                  <Point X="21.1051015625" Y="-2.223636474609" />
                  <Point X="21.951876953125" Y="-1.573882080078" />
                  <Point X="21.963517578125" Y="-1.563310546875" />
                  <Point X="21.98489453125" Y="-1.540391235352" />
                  <Point X="21.994630859375" Y="-1.528043945312" />
                  <Point X="22.01259765625" Y="-1.500910766602" />
                  <Point X="22.0201640625" Y="-1.487125" />
                  <Point X="22.032919921875" Y="-1.45849609375" />
                  <Point X="22.038109375" Y="-1.443652832031" />
                  <Point X="22.045814453125" Y="-1.413905639648" />
                  <Point X="22.04844921875" Y="-1.398802001953" />
                  <Point X="22.05125390625" Y="-1.368364990234" />
                  <Point X="22.051423828125" Y="-1.353031738281" />
                  <Point X="22.049212890625" Y="-1.321362548828" />
                  <Point X="22.046916015625" Y="-1.306208862305" />
                  <Point X="22.0399140625" Y="-1.276466796875" />
                  <Point X="22.02821875" Y="-1.248236328125" />
                  <Point X="22.01213671875" Y="-1.222255371094" />
                  <Point X="22.003044921875" Y="-1.209916625977" />
                  <Point X="21.982212890625" Y="-1.185961303711" />
                  <Point X="21.971255859375" Y="-1.175244873047" />
                  <Point X="21.9477578125" Y="-1.155711914062" />
                  <Point X="21.935216796875" Y="-1.146895629883" />
                  <Point X="21.908734375" Y="-1.131309082031" />
                  <Point X="21.894568359375" Y="-1.124482177734" />
                  <Point X="21.865306640625" Y="-1.113258422852" />
                  <Point X="21.8502109375" Y="-1.108861572266" />
                  <Point X="21.81832421875" Y="-1.102379272461" />
                  <Point X="21.802705078125" Y="-1.100532714844" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="21.186134765625" Y="-1.175177612305" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.30353125" Y="-1.147519042969" />
                  <Point X="20.259240234375" Y="-0.97411340332" />
                  <Point X="20.213548828125" Y="-0.654654602051" />
                  <Point X="20.527107421875" Y="-0.570636657715" />
                  <Point X="21.491712890625" Y="-0.312171325684" />
                  <Point X="21.4995234375" Y="-0.30971282959" />
                  <Point X="21.526072265625" Y="-0.299250213623" />
                  <Point X="21.55583203125" Y="-0.283894195557" />
                  <Point X="21.566435546875" Y="-0.277516052246" />
                  <Point X="21.584478515625" Y="-0.264993530273" />
                  <Point X="21.606326171875" Y="-0.248245956421" />
                  <Point X="21.628783203125" Y="-0.226361816406" />
                  <Point X="21.639103515625" Y="-0.214485519409" />
                  <Point X="21.658349609375" Y="-0.188227416992" />
                  <Point X="21.666568359375" Y="-0.174807815552" />
                  <Point X="21.68067578125" Y="-0.146804428101" />
                  <Point X="21.686564453125" Y="-0.132220657349" />
                  <Point X="21.692578125" Y="-0.112842910767" />
                  <Point X="21.699009765625" Y="-0.088498184204" />
                  <Point X="21.703279296875" Y="-0.060340679169" />
                  <Point X="21.704353515625" Y="-0.046098594666" />
                  <Point X="21.704353515625" Y="-0.016461551666" />
                  <Point X="21.703279296875" Y="-0.002219613791" />
                  <Point X="21.699009765625" Y="0.025938182831" />
                  <Point X="21.695814453125" Y="0.03985389328" />
                  <Point X="21.68980078125" Y="0.059231498718" />
                  <Point X="21.68067578125" Y="0.084244140625" />
                  <Point X="21.666568359375" Y="0.112247665405" />
                  <Point X="21.65834765625" Y="0.125667572021" />
                  <Point X="21.6391015625" Y="0.151925521851" />
                  <Point X="21.628783203125" Y="0.163801376343" />
                  <Point X="21.606326171875" Y="0.185685806274" />
                  <Point X="21.594189453125" Y="0.195694213867" />
                  <Point X="21.576146484375" Y="0.208216598511" />
                  <Point X="21.559666015625" Y="0.2184480896" />
                  <Point X="21.534388671875" Y="0.23240763855" />
                  <Point X="21.523982421875" Y="0.237356491089" />
                  <Point X="21.50264453125" Y="0.245958343506" />
                  <Point X="21.491712890625" Y="0.249611343384" />
                  <Point X="20.972552734375" Y="0.388719848633" />
                  <Point X="20.2145546875" Y="0.591825134277" />
                  <Point X="20.240125" Y="0.764634277344" />
                  <Point X="20.26866796875" Y="0.95752130127" />
                  <Point X="20.366416015625" Y="1.318236938477" />
                  <Point X="20.530482421875" Y="1.296637207031" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.2529453125" Y="1.204364379883" />
                  <Point X="21.263298828125" Y="1.204703369141" />
                  <Point X="21.284859375" Y="1.20658984375" />
                  <Point X="21.29511328125" Y="1.208053955078" />
                  <Point X="21.3153984375" Y="1.212089233398" />
                  <Point X="21.3254296875" Y="1.214660400391" />
                  <Point X="21.36536328125" Y="1.227251342773" />
                  <Point X="21.38685546875" Y="1.234027954102" />
                  <Point X="21.39655078125" Y="1.237677001953" />
                  <Point X="21.415486328125" Y="1.246009155273" />
                  <Point X="21.4247265625" Y="1.250691894531" />
                  <Point X="21.443470703125" Y="1.261514404297" />
                  <Point X="21.452142578125" Y="1.267172851562" />
                  <Point X="21.468822265625" Y="1.279402587891" />
                  <Point X="21.484072265625" Y="1.293376953125" />
                  <Point X="21.497708984375" Y="1.308926147461" />
                  <Point X="21.504103515625" Y="1.317073242188" />
                  <Point X="21.51651953125" Y="1.334804077148" />
                  <Point X="21.52198828125" Y="1.343599121094" />
                  <Point X="21.531939453125" Y="1.361736328125" />
                  <Point X="21.536421875" Y="1.371078125" />
                  <Point X="21.5524453125" Y="1.409762207031" />
                  <Point X="21.561068359375" Y="1.430582397461" />
                  <Point X="21.564505859375" Y="1.440362915039" />
                  <Point X="21.570294921875" Y="1.460231689453" />
                  <Point X="21.572646484375" Y="1.470320068359" />
                  <Point X="21.57640234375" Y="1.491634521484" />
                  <Point X="21.577640625" Y="1.501920532227" />
                  <Point X="21.578990234375" Y="1.522555541992" />
                  <Point X="21.578087890625" Y="1.543201782227" />
                  <Point X="21.574943359375" Y="1.563640258789" />
                  <Point X="21.5728125" Y="1.57378125" />
                  <Point X="21.5672109375" Y="1.594686889648" />
                  <Point X="21.563986328125" Y="1.60453527832" />
                  <Point X="21.55648828125" Y="1.623813964844" />
                  <Point X="21.55221484375" Y="1.633244384766" />
                  <Point X="21.532880859375" Y="1.670384765625" />
                  <Point X="21.522474609375" Y="1.690374023438" />
                  <Point X="21.51719921875" Y="1.699286254883" />
                  <Point X="21.50570703125" Y="1.716485961914" />
                  <Point X="21.499490234375" Y="1.72477331543" />
                  <Point X="21.485578125" Y="1.741352783203" />
                  <Point X="21.478498046875" Y="1.748912719727" />
                  <Point X="21.463556640625" Y="1.763215698242" />
                  <Point X="21.4556953125" Y="1.769958618164" />
                  <Point X="21.157904296875" Y="1.998462402344" />
                  <Point X="20.77238671875" Y="2.294280517578" />
                  <Point X="20.88680078125" Y="2.490299560547" />
                  <Point X="20.99771875" Y="2.680324462891" />
                  <Point X="21.2571328125" Y="3.013768554688" />
                  <Point X="21.273662109375" Y="3.035013427734" />
                  <Point X="21.321171875" Y="3.007583007812" />
                  <Point X="21.74584375" Y="2.762398925781" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774015625" Y="2.749385742188" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.900544921875" Y="2.726292236328" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310546875" />
                  <Point X="22.023568359375" Y="2.734227294922" />
                  <Point X="22.04301171875" Y="2.7413046875" />
                  <Point X="22.0615625" Y="2.750454345703" />
                  <Point X="22.070583984375" Y="2.755533691406" />
                  <Point X="22.088837890625" Y="2.767163330078" />
                  <Point X="22.09725390625" Y="2.773194091797" />
                  <Point X="22.1133828125" Y="2.786138427734" />
                  <Point X="22.121095703125" Y="2.793052001953" />
                  <Point X="22.16057421875" Y="2.832528808594" />
                  <Point X="22.1818203125" Y="2.853775390625" />
                  <Point X="22.188734375" Y="2.861489013672" />
                  <Point X="22.20168359375" Y="2.877623291016" />
                  <Point X="22.20771875" Y="2.886043945312" />
                  <Point X="22.21934765625" Y="2.904298339844" />
                  <Point X="22.2244296875" Y="2.913326171875" />
                  <Point X="22.233576171875" Y="2.931875" />
                  <Point X="22.240646484375" Y="2.951299560547" />
                  <Point X="22.245564453125" Y="2.971388427734" />
                  <Point X="22.2474765625" Y="2.981573730469" />
                  <Point X="22.250302734375" Y="3.003032714844" />
                  <Point X="22.251091796875" Y="3.013365478516" />
                  <Point X="22.25154296875" Y="3.034048828125" />
                  <Point X="22.251205078125" Y="3.044399414062" />
                  <Point X="22.24633984375" Y="3.100015625" />
                  <Point X="22.243720703125" Y="3.129948974609" />
                  <Point X="22.242255859375" Y="3.140205322266" />
                  <Point X="22.23821875" Y="3.160499511719" />
                  <Point X="22.235646484375" Y="3.170537353516" />
                  <Point X="22.22913671875" Y="3.191178955078" />
                  <Point X="22.225486328125" Y="3.200874267578" />
                  <Point X="22.21715625" Y="3.219801513672" />
                  <Point X="22.2124765625" Y="3.229033447266" />
                  <Point X="22.080515625" Y="3.457595703125" />
                  <Point X="21.94061328125" Y="3.699914550781" />
                  <Point X="22.15846875" Y="3.866942138672" />
                  <Point X="22.351634765625" Y="4.015041503906" />
                  <Point X="22.760208984375" Y="4.242036621094" />
                  <Point X="22.807474609375" Y="4.268296875" />
                  <Point X="22.881435546875" Y="4.171908691406" />
                  <Point X="22.8881796875" Y="4.164047363281" />
                  <Point X="22.902482421875" Y="4.149106445313" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.934908203125" Y="4.121897460938" />
                  <Point X="22.952107421875" Y="4.110405273438" />
                  <Point X="22.96101953125" Y="4.105129394531" />
                  <Point X="23.022919921875" Y="4.072905517578" />
                  <Point X="23.056236328125" Y="4.055562255859" />
                  <Point X="23.065673828125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.0437890625" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.229271484375" Y="4.037488525391" />
                  <Point X="23.2491328125" Y="4.043277587891" />
                  <Point X="23.258904296875" Y="4.046714111328" />
                  <Point X="23.323376953125" Y="4.073420166016" />
                  <Point X="23.358078125" Y="4.087793701172" />
                  <Point X="23.36741796875" Y="4.092274169922" />
                  <Point X="23.385552734375" Y="4.102224121094" />
                  <Point X="23.39434765625" Y="4.107693359375" />
                  <Point X="23.412076171875" Y="4.120107421875" />
                  <Point X="23.420220703125" Y="4.126499023438" />
                  <Point X="23.435771484375" Y="4.140135742188" />
                  <Point X="23.449748046875" Y="4.15538671875" />
                  <Point X="23.461978515625" Y="4.17206640625" />
                  <Point X="23.467638671875" Y="4.180740234375" />
                  <Point X="23.4784609375" Y="4.199483886719" />
                  <Point X="23.48314453125" Y="4.208724121094" />
                  <Point X="23.4914765625" Y="4.227659667969" />
                  <Point X="23.495125" Y="4.237354980469" />
                  <Point X="23.516109375" Y="4.303910644531" />
                  <Point X="23.527404296875" Y="4.339731445312" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370048828125" />
                  <Point X="23.535474609375" Y="4.380301757812" />
                  <Point X="23.537361328125" Y="4.401863769531" />
                  <Point X="23.53769921875" Y="4.412216796875" />
                  <Point X="23.537248046875" Y="4.432899414062" />
                  <Point X="23.536458984375" Y="4.443229003906" />
                  <Point X="23.521455078125" Y="4.557184082031" />
                  <Point X="23.520734375" Y="4.562655273438" />
                  <Point X="23.81876953125" Y="4.646214355469" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.564142578125" Y="4.774290039063" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.648703125" Y="4.730583984375" />
                  <Point X="24.77433203125" Y="4.261728027344" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.305595703125" Y="4.514070800781" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.60953125" Y="4.760778808594" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.237677734375" Y="4.638974121094" />
                  <Point X="26.453595703125" Y="4.586844238281" />
                  <Point X="26.71884765625" Y="4.490636230469" />
                  <Point X="26.858255859375" Y="4.440071777344" />
                  <Point X="27.1161875" Y="4.319445800781" />
                  <Point X="27.250453125" Y="4.256653808594" />
                  <Point X="27.49967578125" Y="4.111456542969" />
                  <Point X="27.629419921875" Y="4.035865966797" />
                  <Point X="27.81778125" Y="3.901915527344" />
                  <Point X="27.6254609375" Y="3.568805664062" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.062380859375" Y="2.593113037109" />
                  <Point X="27.053185546875" Y="2.573443603516" />
                  <Point X="27.044185546875" Y="2.549569824219" />
                  <Point X="27.041302734375" Y="2.540603027344" />
                  <Point X="27.02734375" Y="2.488408447266" />
                  <Point X="27.01983203125" Y="2.460316650391" />
                  <Point X="27.0179140625" Y="2.451469726562" />
                  <Point X="27.013646484375" Y="2.420223144531" />
                  <Point X="27.012755859375" Y="2.383240722656" />
                  <Point X="27.013412109375" Y="2.369578613281" />
                  <Point X="27.01885546875" Y="2.324445068359" />
                  <Point X="27.021783203125" Y="2.300153808594" />
                  <Point X="27.02380078125" Y="2.289037597656" />
                  <Point X="27.029142578125" Y="2.267115966797" />
                  <Point X="27.032466796875" Y="2.256310546875" />
                  <Point X="27.040734375" Y="2.234218261719" />
                  <Point X="27.0453203125" Y="2.223885986328" />
                  <Point X="27.05568359375" Y="2.203840820312" />
                  <Point X="27.0614609375" Y="2.194127929688" />
                  <Point X="27.089388671875" Y="2.152970458984" />
                  <Point X="27.104419921875" Y="2.130819335937" />
                  <Point X="27.10981640625" Y="2.123625976562" />
                  <Point X="27.130470703125" Y="2.100119384766" />
                  <Point X="27.1576015625" Y="2.075386474609" />
                  <Point X="27.168259765625" Y="2.066981933594" />
                  <Point X="27.209416015625" Y="2.039055053711" />
                  <Point X="27.231568359375" Y="2.024024414062" />
                  <Point X="27.24128125" Y="2.018245727539" />
                  <Point X="27.261326171875" Y="2.00788269043" />
                  <Point X="27.271658203125" Y="2.003298461914" />
                  <Point X="27.293744140625" Y="1.995033081055" />
                  <Point X="27.304546875" Y="1.991708007812" />
                  <Point X="27.326470703125" Y="1.986365112305" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.382724609375" Y="1.978904541016" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.416044921875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975497314453" />
                  <Point X="27.4843125" Y="1.979822509766" />
                  <Point X="27.49774609375" Y="1.982395629883" />
                  <Point X="27.54994140625" Y="1.996353149414" />
                  <Point X="27.578033203125" Y="2.003865356445" />
                  <Point X="27.583994140625" Y="2.005670776367" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572753906" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.1582109375" Y="2.329350585938" />
                  <Point X="28.940404296875" Y="2.780950683594" />
                  <Point X="28.96698046875" Y="2.744015625" />
                  <Point X="29.04396484375" Y="2.637028808594" />
                  <Point X="29.13688671875" Y="2.483471923828" />
                  <Point X="28.910443359375" Y="2.309715087891" />
                  <Point X="28.172953125" Y="1.743819824219" />
                  <Point X="28.168140625" Y="1.739869506836" />
                  <Point X="28.152123046875" Y="1.725219482422" />
                  <Point X="28.134669921875" Y="1.706604614258" />
                  <Point X="28.12857421875" Y="1.699421386719" />
                  <Point X="28.091009765625" Y="1.650415649414" />
                  <Point X="28.07079296875" Y="1.624040283203" />
                  <Point X="28.065634765625" Y="1.616599365234" />
                  <Point X="28.049736328125" Y="1.589366333008" />
                  <Point X="28.034763671875" Y="1.555545288086" />
                  <Point X="28.03014453125" Y="1.542677001953" />
                  <Point X="28.016150390625" Y="1.492641723633" />
                  <Point X="28.008619140625" Y="1.465712402344" />
                  <Point X="28.006228515625" Y="1.454669921875" />
                  <Point X="28.0027734375" Y="1.432370483398" />
                  <Point X="28.001708984375" Y="1.42111328125" />
                  <Point X="28.000892578125" Y="1.397543579102" />
                  <Point X="28.001173828125" Y="1.386239257812" />
                  <Point X="28.003078125" Y="1.363750732422" />
                  <Point X="28.004701171875" Y="1.352566650391" />
                  <Point X="28.016189453125" Y="1.296896118164" />
                  <Point X="28.02237109375" Y="1.26693371582" />
                  <Point X="28.024599609375" Y="1.258233032227" />
                  <Point X="28.03468359375" Y="1.228614746094" />
                  <Point X="28.050287109375" Y="1.195373901367" />
                  <Point X="28.056919921875" Y="1.18352722168" />
                  <Point X="28.088162109375" Y="1.136039916992" />
                  <Point X="28.1049765625" Y="1.110481689453" />
                  <Point X="28.11173828125" Y="1.101426391602" />
                  <Point X="28.126291015625" Y="1.084182006836" />
                  <Point X="28.13408203125" Y="1.075993041992" />
                  <Point X="28.151326171875" Y="1.059902709961" />
                  <Point X="28.16003515625" Y="1.052696166992" />
                  <Point X="28.17824609375" Y="1.039369628906" />
                  <Point X="28.187748046875" Y="1.033249267578" />
                  <Point X="28.2330234375" Y="1.00776348877" />
                  <Point X="28.257390625" Y="0.994046813965" />
                  <Point X="28.265482421875" Y="0.989986877441" />
                  <Point X="28.294681640625" Y="0.978083496094" />
                  <Point X="28.33027734375" Y="0.968021179199" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.40488671875" Y="0.957167236328" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="28.99663671875" Y="1.018101135254" />
                  <Point X="29.704703125" Y="1.111319824219" />
                  <Point X="29.719625" Y="1.050026123047" />
                  <Point X="29.7526875" Y="0.914208007812" />
                  <Point X="29.783873046875" Y="0.713921203613" />
                  <Point X="29.537171875" Y="0.647817565918" />
                  <Point X="28.6919921875" Y="0.42135269165" />
                  <Point X="28.68603125" Y="0.419543945312" />
                  <Point X="28.665625" Y="0.412137023926" />
                  <Point X="28.642380859375" Y="0.401618682861" />
                  <Point X="28.634005859375" Y="0.397316192627" />
                  <Point X="28.573865234375" Y="0.362553436279" />
                  <Point X="28.54149609375" Y="0.343843902588" />
                  <Point X="28.53388671875" Y="0.338948425293" />
                  <Point X="28.50877734375" Y="0.319873413086" />
                  <Point X="28.481994140625" Y="0.294351287842" />
                  <Point X="28.472796875" Y="0.284225921631" />
                  <Point X="28.436712890625" Y="0.238245407104" />
                  <Point X="28.417291015625" Y="0.213498199463" />
                  <Point X="28.41085546875" Y="0.204208084106" />
                  <Point X="28.39912890625" Y="0.184923812866" />
                  <Point X="28.393837890625" Y="0.174930114746" />
                  <Point X="28.384064453125" Y="0.15346421814" />
                  <Point X="28.380001953125" Y="0.142920303345" />
                  <Point X="28.373158203125" Y="0.121425422668" />
                  <Point X="28.370376953125" Y="0.11047429657" />
                  <Point X="28.35834765625" Y="0.047666999817" />
                  <Point X="28.351875" Y="0.013863613129" />
                  <Point X="28.350603515625" Y="0.004965411663" />
                  <Point X="28.3485859375" Y="-0.026263528824" />
                  <Point X="28.35028125" Y="-0.062939945221" />
                  <Point X="28.351875" Y="-0.076420783997" />
                  <Point X="28.36390234375" Y="-0.139228088379" />
                  <Point X="28.370376953125" Y="-0.173031463623" />
                  <Point X="28.373158203125" Y="-0.183984527588" />
                  <Point X="28.38000390625" Y="-0.205486251831" />
                  <Point X="28.384068359375" Y="-0.216034912109" />
                  <Point X="28.393841796875" Y="-0.237498291016" />
                  <Point X="28.399130859375" Y="-0.24749005127" />
                  <Point X="28.41085546875" Y="-0.266770019531" />
                  <Point X="28.417291015625" Y="-0.276058197021" />
                  <Point X="28.453375" Y="-0.322038696289" />
                  <Point X="28.472796875" Y="-0.346785919189" />
                  <Point X="28.47871875" Y="-0.353632965088" />
                  <Point X="28.50114453125" Y="-0.375810028076" />
                  <Point X="28.5301796875" Y="-0.398726776123" />
                  <Point X="28.54149609375" Y="-0.406404052734" />
                  <Point X="28.60163671875" Y="-0.441166809082" />
                  <Point X="28.634005859375" Y="-0.459876495361" />
                  <Point X="28.639498046875" Y="-0.462815063477" />
                  <Point X="28.659158203125" Y="-0.472016296387" />
                  <Point X="28.68302734375" Y="-0.481027313232" />
                  <Point X="28.6919921875" Y="-0.483912841797" />
                  <Point X="29.146876953125" Y="-0.605798461914" />
                  <Point X="29.78487890625" Y="-0.776750854492" />
                  <Point X="29.78007421875" Y="-0.808621459961" />
                  <Point X="29.761619140625" Y="-0.931037109375" />
                  <Point X="29.727802734375" Y="-1.079219726563" />
                  <Point X="29.416953125" Y="-1.038295532227" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042297363" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.2364453125" Y="-0.938332092285" />
                  <Point X="28.17291796875" Y="-0.952140075684" />
                  <Point X="28.157875" Y="-0.956742736816" />
                  <Point X="28.1287578125" Y="-0.968366088867" />
                  <Point X="28.114681640625" Y="-0.975387084961" />
                  <Point X="28.086853515625" Y="-0.992279541016" />
                  <Point X="28.07412890625" Y="-1.001528198242" />
                  <Point X="28.050376953125" Y="-1.021999938965" />
                  <Point X="28.0393515625" Y="-1.03322277832" />
                  <Point X="27.968005859375" Y="-1.119029052734" />
                  <Point X="27.929607421875" Y="-1.16521081543" />
                  <Point X="27.921326171875" Y="-1.176848022461" />
                  <Point X="27.906603515625" Y="-1.201234130859" />
                  <Point X="27.900162109375" Y="-1.213983032227" />
                  <Point X="27.8888203125" Y="-1.241367797852" />
                  <Point X="27.88436328125" Y="-1.254934448242" />
                  <Point X="27.877533203125" Y="-1.282581176758" />
                  <Point X="27.87516015625" Y="-1.296661376953" />
                  <Point X="27.864935546875" Y="-1.407784423828" />
                  <Point X="27.859431640625" Y="-1.467592041016" />
                  <Point X="27.859291015625" Y="-1.483315795898" />
                  <Point X="27.861607421875" Y="-1.514579956055" />
                  <Point X="27.864064453125" Y="-1.530120605469" />
                  <Point X="27.871794921875" Y="-1.56174230957" />
                  <Point X="27.87678515625" Y="-1.576666137695" />
                  <Point X="27.889158203125" Y="-1.60548059082" />
                  <Point X="27.896541015625" Y="-1.619371337891" />
                  <Point X="27.96186328125" Y="-1.72097668457" />
                  <Point X="27.997021484375" Y="-1.775661987305" />
                  <Point X="28.0017421875" Y="-1.782353393555" />
                  <Point X="28.019802734375" Y="-1.804458618164" />
                  <Point X="28.04349609375" Y="-1.828124023438" />
                  <Point X="28.052798828125" Y="-1.836277587891" />
                  <Point X="28.47493359375" Y="-2.160193359375" />
                  <Point X="29.087171875" Y="-2.62998046875" />
                  <Point X="29.04547265625" Y="-2.697453613281" />
                  <Point X="29.001275390625" Y="-2.760251953125" />
                  <Point X="28.7214296875" Y="-2.598682373047" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.630630859375" Y="-2.040966796875" />
                  <Point X="27.555021484375" Y="-2.027311889648" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.03513659668" />
                  <Point X="27.415068359375" Y="-2.044960571289" />
                  <Point X="27.40058984375" Y="-2.051109130859" />
                  <Point X="27.283884765625" Y="-2.112530517578" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.208970703125" Y="-2.153169433594" />
                  <Point X="27.1860390625" Y="-2.1700625" />
                  <Point X="27.175208984375" Y="-2.179374267578" />
                  <Point X="27.15425" Y="-2.200333496094" />
                  <Point X="27.144939453125" Y="-2.211161621094" />
                  <Point X="27.128046875" Y="-2.234091796875" />
                  <Point X="27.12046484375" Y="-2.246193847656" />
                  <Point X="27.05904296875" Y="-2.362899658203" />
                  <Point X="27.025984375" Y="-2.425711914062" />
                  <Point X="27.0198359375" Y="-2.440193359375" />
                  <Point X="27.01001171875" Y="-2.469972167969" />
                  <Point X="27.0063359375" Y="-2.48526953125" />
                  <Point X="27.00137890625" Y="-2.517443603516" />
                  <Point X="27.000279296875" Y="-2.533134521484" />
                  <Point X="27.00068359375" Y="-2.564484375" />
                  <Point X="27.0021875" Y="-2.580143310547" />
                  <Point X="27.02755859375" Y="-2.720625" />
                  <Point X="27.04121484375" Y="-2.796233642578" />
                  <Point X="27.04301953125" Y="-2.804228271484" />
                  <Point X="27.05123828125" Y="-2.831543701172" />
                  <Point X="27.064072265625" Y="-2.862480712891" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.340814453125" Y="-3.343421630859" />
                  <Point X="27.735896484375" Y="-4.027722900391" />
                  <Point X="27.7237578125" Y="-4.036082275391" />
                  <Point X="27.500076171875" Y="-3.744577636719" />
                  <Point X="26.83391796875" Y="-2.876422363281" />
                  <Point X="26.828654296875" Y="-2.870143066406" />
                  <Point X="26.808830078125" Y="-2.849624023438" />
                  <Point X="26.783251953125" Y="-2.828003662109" />
                  <Point X="26.77330078125" Y="-2.820646728516" />
                  <Point X="26.63474609375" Y="-2.731569824219" />
                  <Point X="26.56017578125" Y="-2.683627929688" />
                  <Point X="26.546283203125" Y="-2.676245605469" />
                  <Point X="26.51747265625" Y="-2.663874511719" />
                  <Point X="26.5025546875" Y="-2.658885742188" />
                  <Point X="26.47093359375" Y="-2.651154052734" />
                  <Point X="26.45539453125" Y="-2.648695800781" />
                  <Point X="26.424125" Y="-2.646376953125" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.25686328125" Y="-2.660460449219" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670339111328" />
                  <Point X="26.133578125" Y="-2.677170410156" />
                  <Point X="26.12001171875" Y="-2.681627929688" />
                  <Point X="26.092626953125" Y="-2.692970703125" />
                  <Point X="26.07987890625" Y="-2.699412841797" />
                  <Point X="26.05549609375" Y="-2.714134033203" />
                  <Point X="26.043861328125" Y="-2.722413085938" />
                  <Point X="25.926853515625" Y="-2.819701904297" />
                  <Point X="25.863876953125" Y="-2.872063476562" />
                  <Point X="25.852654296875" Y="-2.883087402344" />
                  <Point X="25.83218359375" Y="-2.906836914062" />
                  <Point X="25.822935546875" Y="-2.9195625" />
                  <Point X="25.80604296875" Y="-2.947389160156" />
                  <Point X="25.79901953125" Y="-2.961466552734" />
                  <Point X="25.78739453125" Y="-2.990587646484" />
                  <Point X="25.78279296875" Y="-3.005631347656" />
                  <Point X="25.74780859375" Y="-3.16658984375" />
                  <Point X="25.728978515625" Y="-3.253219238281" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323170166016" />
                  <Point X="25.7255546875" Y="-3.335520507812" />
                  <Point X="25.8024296875" Y="-3.9194375" />
                  <Point X="25.83308984375" Y="-4.1523203125" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480122314453" />
                  <Point X="25.642146484375" Y="-3.453578857422" />
                  <Point X="25.6267890625" Y="-3.423815429688" />
                  <Point X="25.62041015625" Y="-3.413209228516" />
                  <Point X="25.51396875" Y="-3.259848876953" />
                  <Point X="25.456681640625" Y="-3.177308837891" />
                  <Point X="25.446669921875" Y="-3.165170654297" />
                  <Point X="25.42478515625" Y="-3.142715576172" />
                  <Point X="25.412912109375" Y="-3.132398681641" />
                  <Point X="25.38665625" Y="-3.113154541016" />
                  <Point X="25.3732421875" Y="-3.104937744141" />
                  <Point X="25.3452421875" Y="-3.090829833984" />
                  <Point X="25.33065625" Y="-3.084938720703" />
                  <Point X="25.165947265625" Y="-3.033818603516" />
                  <Point X="25.077296875" Y="-3.006305419922" />
                  <Point X="25.063376953125" Y="-3.003109375" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766357422" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.998840087891" />
                  <Point X="24.948935546875" Y="-3.003109619141" />
                  <Point X="24.935015625" Y="-3.006305664062" />
                  <Point X="24.770306640625" Y="-3.057425537109" />
                  <Point X="24.68165625" Y="-3.084938964844" />
                  <Point X="24.6670703125" Y="-3.090830078125" />
                  <Point X="24.6390703125" Y="-3.104937988281" />
                  <Point X="24.62565625" Y="-3.113154785156" />
                  <Point X="24.599400390625" Y="-3.132398925781" />
                  <Point X="24.587525390625" Y="-3.142717041016" />
                  <Point X="24.565640625" Y="-3.165172851562" />
                  <Point X="24.555630859375" Y="-3.177310546875" />
                  <Point X="24.44919140625" Y="-3.330670654297" />
                  <Point X="24.391904296875" Y="-3.4132109375" />
                  <Point X="24.38753125" Y="-3.420131591797" />
                  <Point X="24.374025390625" Y="-3.445261230469" />
                  <Point X="24.36122265625" Y="-3.476214111328" />
                  <Point X="24.35724609375" Y="-3.487937011719" />
                  <Point X="24.2170859375" Y="-4.011024414062" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.067230590538" Y="-3.748250737224" />
                  <Point X="22.528208831817" Y="-4.121543556662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.031574191686" Y="-2.787350562798" />
                  <Point X="21.270620290237" Y="-2.980926276602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.115322509202" Y="-3.664952646324" />
                  <Point X="22.586061091991" Y="-4.046149234476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.875686294364" Y="-2.538872873818" />
                  <Point X="21.358745934052" Y="-2.930046857119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.163414427865" Y="-3.581654555423" />
                  <Point X="22.656916897967" Y="-3.981284976054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.864933661765" Y="-2.407923404865" />
                  <Point X="21.446871577867" Y="-2.879167437635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.211506532937" Y="-3.498356615474" />
                  <Point X="22.739626263641" Y="-3.926019541013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.942443919624" Y="-2.348447815328" />
                  <Point X="21.534997221682" Y="-2.828288018152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.259598694183" Y="-3.415058721012" />
                  <Point X="22.82233603219" Y="-3.870754432213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.86128960422" Y="-4.712082446074" />
                  <Point X="23.897303493812" Y="-4.741245918839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.019954177483" Y="-2.288972225791" />
                  <Point X="21.623122865497" Y="-2.777408598669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.307690855428" Y="-3.331760826551" />
                  <Point X="22.905085595024" Y="-3.81552154819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.875832670209" Y="-4.601617029945" />
                  <Point X="24.026206454262" Y="-4.723387319283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.097464435341" Y="-2.229496636254" />
                  <Point X="21.711248509313" Y="-2.726529179185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.355783016674" Y="-3.24846293209" />
                  <Point X="23.010726537692" Y="-3.778825738055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.872483504667" Y="-4.476662770406" />
                  <Point X="24.053121442483" Y="-4.622940488238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.174974581752" Y="-2.170020956468" />
                  <Point X="21.799374153128" Y="-2.675649759702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.403875177919" Y="-3.165165037628" />
                  <Point X="23.16724139975" Y="-3.783326815547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.843499017535" Y="-4.330949436756" />
                  <Point X="24.080036430703" Y="-4.522493657193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.252484715981" Y="-2.110545266818" />
                  <Point X="21.887499845255" Y="-2.624770379341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.451967339165" Y="-3.081867143167" />
                  <Point X="23.331492271311" Y="-3.794092390016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.813594038826" Y="-4.184490703724" />
                  <Point X="24.106951418924" Y="-4.422046826148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.326827768797" Y="-1.238720892012" />
                  <Point X="20.379499005574" Y="-1.281373218563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.32999485021" Y="-2.051069577167" />
                  <Point X="21.975625657114" Y="-2.573891095937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.50005950041" Y="-2.998569248705" />
                  <Point X="24.133866407144" Y="-4.321599995102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.287460494088" Y="-1.084599742763" />
                  <Point X="20.509345421939" Y="-1.264278614543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.40750498444" Y="-1.991593887517" />
                  <Point X="22.063751468974" Y="-2.523011812534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.548151661656" Y="-2.915271354244" />
                  <Point X="24.160781395365" Y="-4.221153164057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.253641912392" Y="-0.93497183652" />
                  <Point X="20.639191838304" Y="-1.247184010523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.485015118669" Y="-1.932118197867" />
                  <Point X="22.151877280834" Y="-2.47213252913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.596243822901" Y="-2.831973459782" />
                  <Point X="24.187696383586" Y="-4.120706333012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.233867632044" Y="-0.796716781267" />
                  <Point X="20.769038254669" Y="-1.230089406503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.562525252898" Y="-1.872642508217" />
                  <Point X="22.240003092693" Y="-2.421253245726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.644335984147" Y="-2.748675565321" />
                  <Point X="24.214611371806" Y="-4.020259501967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.214093351697" Y="-0.658461726014" />
                  <Point X="20.898884671034" Y="-1.212994802483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.640035387128" Y="-1.813166818567" />
                  <Point X="22.329495369908" Y="-2.371480504149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.682134062628" Y="-2.657041687001" />
                  <Point X="24.24152600664" Y="-3.919812384755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.323850633649" Y="-0.625099261705" />
                  <Point X="21.028731087399" Y="-1.195900198464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.717545521357" Y="-1.753691128917" />
                  <Point X="22.451997665532" Y="-2.348438748416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.677201738608" Y="-2.530805411003" />
                  <Point X="24.268440605694" Y="-3.819365238569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.437275824136" Y="-0.594707011164" />
                  <Point X="21.158577503764" Y="-1.178805594444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.795055655586" Y="-1.694215439267" />
                  <Point X="24.295355204748" Y="-3.718918092383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.550701024939" Y="-0.564314768977" />
                  <Point X="21.288423900945" Y="-1.161710974889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.872565789815" Y="-1.634739749616" />
                  <Point X="24.322269803802" Y="-3.618470946197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.664126265023" Y="-0.533922558598" />
                  <Point X="21.418270292958" Y="-1.144616351149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.950075924045" Y="-1.575264059966" />
                  <Point X="24.349184402856" Y="-3.518023800011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.777551505106" Y="-0.503530348219" />
                  <Point X="21.548116684972" Y="-1.12752172741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.011416504438" Y="-1.502694523796" />
                  <Point X="24.385021275562" Y="-3.424801768567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.89097674519" Y="-0.47313813784" />
                  <Point X="21.677963076985" Y="-1.11042710367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.046670181141" Y="-1.409000229542" />
                  <Point X="24.438592950725" Y="-3.345941096986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.004401985273" Y="-0.442745927461" />
                  <Point X="21.819201585023" Y="-1.102557633592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.041316162829" Y="-1.282422472239" />
                  <Point X="24.492908225635" Y="-3.267682580607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.117827225357" Y="-0.412353717082" />
                  <Point X="24.547223489236" Y="-3.18942405507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.23125246544" Y="-0.381961506703" />
                  <Point X="24.614286339343" Y="-3.121488321547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.818079394987" Y="-4.096300717279" />
                  <Point X="25.826625733609" Y="-4.103221405837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.344677705524" Y="-0.351569296324" />
                  <Point X="24.70945424461" Y="-3.076311612945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.776247960611" Y="-3.940184130876" />
                  <Point X="25.808611415487" Y="-3.966391539893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.226975878228" Y="0.675769956053" />
                  <Point X="20.388045519256" Y="0.545338332516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.458102945607" Y="-0.321177085945" />
                  <Point X="24.818584853345" Y="-3.042441678672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.734416526235" Y="-3.784067544473" />
                  <Point X="25.790597186146" Y="-3.829561745843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.24312859493" Y="0.784931902734" />
                  <Point X="20.613653481572" Y="0.48488676563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.559955768971" Y="-0.281413717281" />
                  <Point X="24.927715134766" Y="-3.008571479345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.69258509186" Y="-3.62795095807" />
                  <Point X="25.772583003186" Y="-3.692731989351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.259282036537" Y="0.8940932624" />
                  <Point X="20.839261443889" Y="0.424435198744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.634625891249" Y="-0.219638231299" />
                  <Point X="25.075309902072" Y="-3.005849206533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.648678978664" Y="-3.470154329884" />
                  <Point X="25.754568820227" Y="-3.55590223286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.280048646283" Y="0.999518952164" />
                  <Point X="21.064869321109" Y="0.363983700769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.684363104819" Y="-0.137672473943" />
                  <Point X="25.319756111425" Y="-3.081555685082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.480653508656" Y="-3.211847828342" />
                  <Point X="25.736554637268" Y="-3.419072476369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.30721330898" Y="1.099763600804" />
                  <Point X="21.290477075462" Y="0.303532302288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.704353515625" Y="-0.031618230671" />
                  <Point X="25.724950731434" Y="-3.287433659941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.334377971677" Y="1.200008249445" />
                  <Point X="21.522332557107" Y="0.238021594003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.654360283278" Y="0.131107649411" />
                  <Point X="25.744645854577" Y="-3.181140297435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.361542634374" Y="1.300252898086" />
                  <Point X="25.767238889056" Y="-3.077193617258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.514339697252" Y="1.298762435008" />
                  <Point X="25.793221226328" Y="-2.975991540366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.69460292136" Y="1.275030313113" />
                  <Point X="25.84311604216" Y="-2.894153406806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.874866151412" Y="1.251298186405" />
                  <Point X="25.914656188111" Y="-2.82984331597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.055129381463" Y="1.227566059697" />
                  <Point X="25.989137212701" Y="-2.767914701698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.234233618852" Y="1.204772466742" />
                  <Point X="26.065913316384" Y="-2.707844605832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.359653982133" Y="1.225451217879" />
                  <Point X="26.168947218708" Y="-2.669037656052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.504913738891" Y="-3.75088201298" />
                  <Point X="27.638533153106" Y="-3.859084881135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.455787914216" Y="1.26984565339" />
                  <Point X="26.303959844131" Y="-2.656126565639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.25718042625" Y="-3.428029373153" />
                  <Point X="27.505987497148" Y="-3.629509366511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.519871132589" Y="1.340194245115" />
                  <Point X="26.444766869301" Y="-2.647907687624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.009447257669" Y="-3.105176849983" />
                  <Point X="27.37344184119" Y="-3.399933851887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.560601012671" Y="1.429453997311" />
                  <Point X="27.240896648944" Y="-3.17035871277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.578344618773" Y="1.537327667158" />
                  <Point X="27.108351608118" Y="-2.94078369627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.779497681097" Y="2.306463321015" />
                  <Point X="21.194962231526" Y="1.970026761719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.502067215158" Y="1.721338049459" />
                  <Point X="27.034432549103" Y="-2.758683063771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.827948283401" Y="2.38947095563" />
                  <Point X="27.008572987706" Y="-2.615500245087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.876398885705" Y="2.472478590246" />
                  <Point X="27.005489813043" Y="-2.490761380713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.924850097666" Y="2.555485731171" />
                  <Point X="27.040965680575" Y="-2.397247013044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.973301476297" Y="2.638492737128" />
                  <Point X="27.086076206095" Y="-2.31153463758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.026660583307" Y="2.717525543006" />
                  <Point X="27.133035876471" Y="-2.227319670095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.085005661188" Y="2.792520789282" />
                  <Point X="27.200306441476" Y="-2.15955214078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.143350739069" Y="2.867516035559" />
                  <Point X="27.289528248321" Y="-2.109560376615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.20169581695" Y="2.942511281835" />
                  <Point X="27.381021406305" Y="-2.061407916338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.260041017052" Y="3.017506429139" />
                  <Point X="27.489424383483" Y="-2.026948757647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.676810839842" Y="2.802255039886" />
                  <Point X="27.665459250352" Y="-2.047256823364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.924120936397" Y="2.724229431208" />
                  <Point X="27.940820519323" Y="-2.147997823577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.049835204783" Y="2.744670182684" />
                  <Point X="28.466742018317" Y="-2.451638497417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.130022244558" Y="2.801978156965" />
                  <Point X="28.992666513446" Y="-2.755281597478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.196122024823" Y="2.870693769068" />
                  <Point X="29.054390990177" Y="-2.683022934432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.241786761622" Y="2.955957353088" />
                  <Point X="27.90163851115" Y="-1.627300223931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.24874091338" Y="3.07256815079" />
                  <Point X="27.859402449697" Y="-1.470855976981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.216652758601" Y="3.220794784944" />
                  <Point X="27.869618719575" Y="-1.356886790447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.085196602932" Y="3.449488039631" />
                  <Point X="27.886530028159" Y="-1.248339139359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.952652109487" Y="3.67906261287" />
                  <Point X="27.931709526151" Y="-1.162682616701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.011111796501" Y="3.7539650505" />
                  <Point X="27.992451676076" Y="-1.089628481092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.088653299767" Y="3.813415338005" />
                  <Point X="28.054987073903" Y="-1.018026489002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.166194779393" Y="3.872865644653" />
                  <Point X="28.139381175309" Y="-0.964125326057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.243736045409" Y="3.93231612428" />
                  <Point X="28.25382168175" Y="-0.934555262164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.321277311425" Y="3.991766603906" />
                  <Point X="28.374670372776" Y="-0.910174443829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.406114265762" Y="4.045309151619" />
                  <Point X="28.541517087429" Y="-0.923042090586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.495645171593" Y="4.095050612359" />
                  <Point X="28.721780315284" Y="-0.946774215516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.585176077425" Y="4.144792073099" />
                  <Point X="28.902043543139" Y="-0.970506340445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.674706983257" Y="4.194533533839" />
                  <Point X="29.082306770995" Y="-0.994238465375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.764237859043" Y="4.244275018909" />
                  <Point X="29.26256999885" Y="-1.017970590305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.180226385237" Y="4.029656311165" />
                  <Point X="28.370445130219" Y="-0.173299957311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.784694112961" Y="-0.508752169303" />
                  <Point X="29.442833230523" Y="-1.041702718325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.292786653859" Y="4.060748961623" />
                  <Point X="28.348926552451" Y="-0.033632397858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.010301546034" Y="-0.569203307616" />
                  <Point X="29.623096484965" Y="-1.065434864784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.389496798474" Y="4.104676789425" />
                  <Point X="28.363871420322" Y="0.076507645522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.235909191617" Y="-0.629654618016" />
                  <Point X="29.737671755625" Y="-1.035973930804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.460210915254" Y="4.169655785495" />
                  <Point X="28.393689301263" Y="0.174603760392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.461517163191" Y="-0.690106192398" />
                  <Point X="29.761217181864" Y="-0.932798482267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.501723104009" Y="4.258282036618" />
                  <Point X="28.44811411478" Y="0.252773574156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.687125134764" Y="-0.75055776678" />
                  <Point X="29.77776263742" Y="-0.823954569239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.531325141276" Y="4.356552938249" />
                  <Point X="28.512611912342" Y="0.322786446274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.531829999455" Y="4.478386270917" />
                  <Point X="28.597552367834" Y="0.376245180404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.563809356957" Y="4.57473205658" />
                  <Point X="28.692603460941" Y="0.421516481629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.675942637525" Y="4.606170475145" />
                  <Point X="28.806028751674" Y="0.451908650992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.788075918094" Y="4.637608893711" />
                  <Point X="28.060806267159" Y="1.17762007889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.313934208967" Y="0.972641113259" />
                  <Point X="28.919454042408" Y="0.482300820355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.900209481959" Y="4.669047082868" />
                  <Point X="28.006502776575" Y="1.343836337272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.490373609943" Y="0.952005462282" />
                  <Point X="29.032879333142" Y="0.512692989718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.012343152594" Y="4.700485185563" />
                  <Point X="27.033257865577" Y="2.254196685347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.3715516029" Y="1.980251818332" />
                  <Point X="28.008370892573" Y="1.464565725525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.62080980122" Y="0.968622475994" />
                  <Point X="29.146304623876" Y="0.543085159081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.134284433375" Y="4.72398124216" />
                  <Point X="24.841899909494" Y="4.150965527957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.875544136406" Y="4.123720970195" />
                  <Point X="27.012987359831" Y="2.392853576004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.514370835553" Y="1.986841242856" />
                  <Point X="28.037986647254" Y="1.562825519012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.750656177293" Y="0.985717112643" />
                  <Point X="29.259729914609" Y="0.573477328445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.266178629888" Y="4.739417586512" />
                  <Point X="24.7516802327" Y="4.346266140465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.064222247798" Y="4.093174606935" />
                  <Point X="27.030654800224" Y="2.500788923626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.622720651676" Y="2.02134345052" />
                  <Point X="28.087091299136" Y="1.645303514723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.880502553365" Y="1.002811749291" />
                  <Point X="29.373155205343" Y="0.603869497808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.398072826401" Y="4.754853930864" />
                  <Point X="24.709849237415" Y="4.502382371298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.158021018442" Y="4.139460018895" />
                  <Point X="27.063957605653" Y="2.596063002289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.711681199195" Y="2.071546778315" />
                  <Point X="28.14661475" Y="1.719344533372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.010348934031" Y="1.019906382219" />
                  <Point X="29.486580496077" Y="0.634261667171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.529967022915" Y="4.770290275217" />
                  <Point X="24.668018242131" Y="4.658498602132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.217145111008" Y="4.213824431517" />
                  <Point X="27.111975604475" Y="2.679420952296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.799806919443" Y="2.122426135904" />
                  <Point X="28.221420563418" Y="1.781010138836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.140195353597" Y="1.037000983647" />
                  <Point X="29.600005702372" Y="0.664653904911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.250633274484" Y="4.308948410193" />
                  <Point X="27.160067623298" Y="2.76271896209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.887932639691" Y="2.173305493493" />
                  <Point X="28.298930802929" Y="1.840485743231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.270041773164" Y="1.054095585074" />
                  <Point X="29.713430840681" Y="0.695046197705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.277548002548" Y="4.409395451909" />
                  <Point X="27.208159642121" Y="2.846016971883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.976058359939" Y="2.224184851082" />
                  <Point X="28.376441042439" Y="1.899961347626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.399888192731" Y="1.071190186501" />
                  <Point X="29.77561950141" Y="0.766928971961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.304462730612" Y="4.509842493625" />
                  <Point X="27.256251660944" Y="2.929314981676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.064184080187" Y="2.275064208672" />
                  <Point X="28.45395128195" Y="1.959436952021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.529734612298" Y="1.088284787928" />
                  <Point X="29.75383970441" Y="0.906808062578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.331377642396" Y="4.610289386568" />
                  <Point X="27.304343679766" Y="3.012612991469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.152309800435" Y="2.325943566261" />
                  <Point X="28.53146152146" Y="2.018912556416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.659581031865" Y="1.105379389355" />
                  <Point X="29.717584092742" Y="1.05840943678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.358292562253" Y="4.710736272972" />
                  <Point X="27.352435698589" Y="3.095911001262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.240435444807" Y="2.376822985293" />
                  <Point X="28.60897176097" Y="2.078388160812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.423376623941" Y="4.780274397761" />
                  <Point X="27.400527717412" Y="3.179209011055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.328561083734" Y="2.427702408735" />
                  <Point X="28.686482000481" Y="2.137863765207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.596755960268" Y="4.762116738278" />
                  <Point X="27.448619736234" Y="3.262507020848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.41668672266" Y="2.478581832177" />
                  <Point X="28.763992239991" Y="2.197339369602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.770134879871" Y="4.74395941625" />
                  <Point X="27.496711755057" Y="3.345805030641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.504812361587" Y="2.529461255619" />
                  <Point X="28.841502479502" Y="2.256814973997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.971327109894" Y="4.703279319535" />
                  <Point X="27.54480377388" Y="3.429103040434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.592938000513" Y="2.580340679062" />
                  <Point X="28.919012694769" Y="2.316290598024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.186408356114" Y="4.651352119266" />
                  <Point X="27.592895792703" Y="3.512401050227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.68106363944" Y="2.631220102504" />
                  <Point X="28.996522715002" Y="2.375766379987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.401490499501" Y="4.599424192486" />
                  <Point X="27.640987793996" Y="3.595699074215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.769189278366" Y="2.682099525946" />
                  <Point X="29.074032735235" Y="2.435242161949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.660780532846" Y="4.511697422276" />
                  <Point X="27.689079758526" Y="3.678997127974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.857314917293" Y="2.732978949388" />
                  <Point X="29.109459665644" Y="2.528796158118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.957506642435" Y="4.393655515259" />
                  <Point X="27.737171723056" Y="3.762295181732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.347381319172" Y="4.20018338585" />
                  <Point X="27.785263687586" Y="3.845593235491" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.9998359375" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.7319609375" Y="-4.509014648438" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.35787890625" Y="-3.368183837891" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399658203" />
                  <Point X="25.109626953125" Y="-3.215279541016" />
                  <Point X="25.0209765625" Y="-3.187766357422" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.826626953125" Y="-3.238886474609" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285644042969" />
                  <Point X="24.60528125" Y="-3.439004150391" />
                  <Point X="24.547994140625" Y="-3.521544433594" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.40061328125" Y="-4.060199707031" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.003158203125" Y="-4.958136230469" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.708978515625" Y="-4.88898046875" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.659501953125" Y="-4.789153808594" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534756835938" />
                  <Point X="23.65096875" Y="-4.336935058594" />
                  <Point X="23.6297890625" Y="-4.230465332031" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.46207421875" Y="-4.069639892578" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.149494140625" Y="-3.972571289062" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.842416015625" Y="-4.085848632812" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.661578125" Y="-4.259844238281" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.295732421875" Y="-4.261456542969" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.879998046875" Y="-3.964210693359" />
                  <Point X="21.771419921875" Y="-3.880608154297" />
                  <Point X="22.01000390625" Y="-3.467366210938" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597592529297" />
                  <Point X="22.48601953125" Y="-2.568763183594" />
                  <Point X="22.468677734375" Y="-2.551420654297" />
                  <Point X="22.43984765625" Y="-2.53719921875" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.9571640625" Y="-2.803942871094" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.958041015625" Y="-3.004448974609" />
                  <Point X="20.838302734375" Y="-2.847136962891" />
                  <Point X="20.64891015625" Y="-2.529554443359" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="20.9894375" Y="-2.072899169922" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.8541796875" Y="-1.396011474609" />
                  <Point X="21.861884765625" Y="-1.366264282227" />
                  <Point X="21.859673828125" Y="-1.334595092773" />
                  <Point X="21.838841796875" Y="-1.310639770508" />
                  <Point X="21.812359375" Y="-1.295053222656" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.210935546875" Y="-1.363552124023" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.11944140625" Y="-1.194541503906" />
                  <Point X="20.072607421875" Y="-1.011187805176" />
                  <Point X="20.0225" Y="-0.660846191406" />
                  <Point X="20.001603515625" Y="-0.51474230957" />
                  <Point X="20.477931640625" Y="-0.387110717773" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.458103515625" Y="-0.121425506592" />
                  <Point X="21.476146484375" Y="-0.108903144836" />
                  <Point X="21.485857421875" Y="-0.102163482666" />
                  <Point X="21.505103515625" Y="-0.075905471802" />
                  <Point X="21.5111171875" Y="-0.056527839661" />
                  <Point X="21.514353515625" Y="-0.04609859848" />
                  <Point X="21.514353515625" Y="-0.016461481094" />
                  <Point X="21.50833984375" Y="0.002916153193" />
                  <Point X="21.505103515625" Y="0.013345396042" />
                  <Point X="21.485857421875" Y="0.039603401184" />
                  <Point X="21.467814453125" Y="0.052125762939" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.923376953125" Y="0.205193969727" />
                  <Point X="20.001814453125" Y="0.452126068115" />
                  <Point X="20.052171875" Y="0.792446105957" />
                  <Point X="20.08235546875" Y="0.996415283203" />
                  <Point X="20.183220703125" Y="1.368639038086" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.555283203125" Y="1.48501171875" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866699219" />
                  <Point X="21.30823046875" Y="1.408457641602" />
                  <Point X="21.32972265625" Y="1.41523425293" />
                  <Point X="21.348466796875" Y="1.426056884766" />
                  <Point X="21.3608828125" Y="1.443787719727" />
                  <Point X="21.37690625" Y="1.482471801758" />
                  <Point X="21.385529296875" Y="1.503291992188" />
                  <Point X="21.38928515625" Y="1.524606689453" />
                  <Point X="21.38368359375" Y="1.545512451172" />
                  <Point X="21.364349609375" Y="1.582652832031" />
                  <Point X="21.353943359375" Y="1.602642211914" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.042240234375" Y="1.847725341797" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.72270703125" Y="2.586078857422" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="21.107171875" Y="3.130436767578" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.416171875" Y="3.172127441406" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.917103515625" Y="2.915569091797" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927405029297" />
                  <Point X="22.0262265625" Y="2.966881835938" />
                  <Point X="22.04747265625" Y="2.988128417969" />
                  <Point X="22.0591015625" Y="3.0063828125" />
                  <Point X="22.061927734375" Y="3.027841796875" />
                  <Point X="22.0570625" Y="3.083458007812" />
                  <Point X="22.054443359375" Y="3.113391357422" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.91597265625" Y="3.362595214844" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="22.04286328125" Y="4.017725830078" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.667935546875" Y="4.408124511719" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.902169921875" Y="4.456999023438" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.110654296875" Y="4.241437011719" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.250666015625" Y="4.248956542969" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.303095703125" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.294487792969" />
                  <Point X="23.33490234375" Y="4.361043457031" />
                  <Point X="23.346197265625" Y="4.396864257812" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.333080078125" Y="4.532381347656" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.7674765625" Y="4.829159667969" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.542056640625" Y="4.963001953125" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.83223046875" Y="4.779759277344" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.122068359375" Y="4.563246582031" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.6293203125" Y="4.949745605469" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.282267578125" Y="4.823667480469" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.7836328125" Y="4.669250488281" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.19667578125" Y="4.4915546875" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.5953203125" Y="4.275626953125" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.974552734375" Y="4.023575439453" />
                  <Point X="28.0687421875" Y="3.956593017578" />
                  <Point X="27.79000390625" Y="3.473805664062" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491514160156" />
                  <Point X="27.21089453125" Y="2.439319580078" />
                  <Point X="27.2033828125" Y="2.411227783203" />
                  <Point X="27.202044921875" Y="2.392328613281" />
                  <Point X="27.20748828125" Y="2.347195068359" />
                  <Point X="27.210416015625" Y="2.322903808594" />
                  <Point X="27.21868359375" Y="2.300811523438" />
                  <Point X="27.246611328125" Y="2.259654052734" />
                  <Point X="27.261642578125" Y="2.237502929688" />
                  <Point X="27.274943359375" Y="2.224203125" />
                  <Point X="27.316099609375" Y="2.196276123047" />
                  <Point X="27.338251953125" Y="2.181245605469" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.405470703125" Y="2.167537841797" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.500859375" Y="2.179903808594" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.0632109375" Y="2.493895507813" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.12120703125" Y="2.854987060547" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.33751953125" Y="2.518913330078" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="29.026107421875" Y="2.158978271484" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583832275391" />
                  <Point X="28.241806640625" Y="1.534826538086" />
                  <Point X="28.22158984375" Y="1.508451171875" />
                  <Point X="28.21312109375" Y="1.491500366211" />
                  <Point X="28.199126953125" Y="1.441465209961" />
                  <Point X="28.191595703125" Y="1.414535888672" />
                  <Point X="28.190779296875" Y="1.390966308594" />
                  <Point X="28.202267578125" Y="1.335295776367" />
                  <Point X="28.20844921875" Y="1.305333496094" />
                  <Point X="28.215646484375" Y="1.287955688477" />
                  <Point X="28.246888671875" Y="1.240468383789" />
                  <Point X="28.263703125" Y="1.21491027832" />
                  <Point X="28.280947265625" Y="1.198819946289" />
                  <Point X="28.32622265625" Y="1.173334228516" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.42978125" Y="1.145529296875" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.9718359375" Y="1.206475585938" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.904234375" Y="1.094967529297" />
                  <Point X="29.93919140625" Y="0.951367797852" />
                  <Point X="29.981708984375" Y="0.678291992188" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="29.58634765625" Y="0.464291778564" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819503784" />
                  <Point X="28.668947265625" Y="0.198056671143" />
                  <Point X="28.636578125" Y="0.179347137451" />
                  <Point X="28.622265625" Y="0.166927307129" />
                  <Point X="28.586181640625" Y="0.120946815491" />
                  <Point X="28.566759765625" Y="0.096199623108" />
                  <Point X="28.556986328125" Y="0.074733589172" />
                  <Point X="28.54495703125" Y="0.011926455498" />
                  <Point X="28.538484375" Y="-0.021876972198" />
                  <Point X="28.538484375" Y="-0.040685844421" />
                  <Point X="28.55051171875" Y="-0.103492973328" />
                  <Point X="28.556986328125" Y="-0.137296401978" />
                  <Point X="28.566759765625" Y="-0.15875970459" />
                  <Point X="28.60284375" Y="-0.204740188599" />
                  <Point X="28.622265625" Y="-0.229487380981" />
                  <Point X="28.636578125" Y="-0.241907211304" />
                  <Point X="28.69671875" Y="-0.276669891357" />
                  <Point X="28.729087890625" Y="-0.295379577637" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.196052734375" Y="-0.422272674561" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.96794921875" Y="-0.836947387695" />
                  <Point X="29.948431640625" Y="-0.966413330078" />
                  <Point X="29.893958984375" Y="-1.205112670898" />
                  <Point X="29.874546875" Y="-1.290178588867" />
                  <Point X="29.39215234375" Y="-1.226670043945" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.27680078125" Y="-1.123997070312" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.1854453125" Y="-1.154697387695" />
                  <Point X="28.114099609375" Y="-1.240503662109" />
                  <Point X="28.075701171875" Y="-1.286685424805" />
                  <Point X="28.064359375" Y="-1.31407019043" />
                  <Point X="28.054134765625" Y="-1.425193115234" />
                  <Point X="28.048630859375" Y="-1.485000610352" />
                  <Point X="28.056361328125" Y="-1.516622314453" />
                  <Point X="28.12168359375" Y="-1.618227783203" />
                  <Point X="28.156841796875" Y="-1.672912841797" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.59059765625" Y="-2.009456054688" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.2591484375" Y="-2.713116699219" />
                  <Point X="29.2041328125" Y="-2.802139648438" />
                  <Point X="29.0914765625" Y="-2.962209472656" />
                  <Point X="29.056689453125" Y="-3.011638183594" />
                  <Point X="28.6264296875" Y="-2.763227294922" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.596861328125" Y="-2.227941894531" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.372373046875" Y="-2.280666259766" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334683105469" />
                  <Point X="27.2271796875" Y="-2.451388916016" />
                  <Point X="27.19412109375" Y="-2.514201171875" />
                  <Point X="27.1891640625" Y="-2.546375244141" />
                  <Point X="27.21453515625" Y="-2.686856933594" />
                  <Point X="27.22819140625" Y="-2.762465576172" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.505357421875" Y="-3.248421630859" />
                  <Point X="27.98667578125" Y="-4.082088867188" />
                  <Point X="27.900583984375" Y="-4.14358203125" />
                  <Point X="27.835314453125" Y="-4.190202148438" />
                  <Point X="27.709341796875" Y="-4.271741699219" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="27.349337890625" Y="-3.860242675781" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467529297" />
                  <Point X="26.53199609375" Y="-2.891390625" />
                  <Point X="26.45742578125" Y="-2.843448730469" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.2742734375" Y="-2.849661132812" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868508789062" />
                  <Point X="26.048326171875" Y="-2.965797607422" />
                  <Point X="25.985349609375" Y="-3.018159179688" />
                  <Point X="25.96845703125" Y="-3.045985839844" />
                  <Point X="25.93347265625" Y="-3.206944335938" />
                  <Point X="25.914642578125" Y="-3.293573730469" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.9908046875" Y="-3.894637207031" />
                  <Point X="26.127642578125" Y="-4.934028320312" />
                  <Point X="26.056107421875" Y="-4.949708984375" />
                  <Point X="25.994349609375" Y="-4.96324609375" />
                  <Point X="25.87798828125" Y="-4.984385253906" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#182" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.125629508639" Y="4.823980206672" Z="1.65" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.65" />
                  <Point X="-0.469912334385" Y="5.043943110425" Z="1.65" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.65" />
                  <Point X="-1.252178203445" Y="4.908583028187" Z="1.65" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.65" />
                  <Point X="-1.722648312199" Y="4.557135362634" Z="1.65" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.65" />
                  <Point X="-1.718940200075" Y="4.407359683241" Z="1.65" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.65" />
                  <Point X="-1.77462546924" Y="4.326430634745" Z="1.65" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.65" />
                  <Point X="-1.872414581505" Y="4.317067670483" Z="1.65" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.65" />
                  <Point X="-2.064319912689" Y="4.518716837131" Z="1.65" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.65" />
                  <Point X="-2.362504832927" Y="4.483112020102" Z="1.65" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.65" />
                  <Point X="-2.993716423558" Y="4.088685555161" Z="1.65" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.65" />
                  <Point X="-3.133485194254" Y="3.368875200345" Z="1.65" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.65" />
                  <Point X="-2.998905846443" Y="3.11037973065" Z="1.65" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.65" />
                  <Point X="-3.015286797011" Y="3.033516770875" Z="1.65" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.65" />
                  <Point X="-3.084696734957" Y="2.99665885273" Z="1.65" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.65" />
                  <Point X="-3.564984147952" Y="3.246708754492" Z="1.65" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.65" />
                  <Point X="-3.938447569163" Y="3.192419266796" Z="1.65" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.65" />
                  <Point X="-4.326631857437" Y="2.642563931115" Z="1.65" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.65" />
                  <Point X="-3.994353963097" Y="1.839337683475" Z="1.65" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.65" />
                  <Point X="-3.686156501953" Y="1.590844785858" Z="1.65" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.65" />
                  <Point X="-3.675446429225" Y="1.532884212133" Z="1.65" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.65" />
                  <Point X="-3.712962534561" Y="1.487423517907" Z="1.65" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.65" />
                  <Point X="-4.444349026869" Y="1.565864070969" Z="1.65" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.65" />
                  <Point X="-4.871196482541" Y="1.412996219563" Z="1.65" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.65" />
                  <Point X="-5.003445168995" Y="0.831045370337" Z="1.65" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.65" />
                  <Point X="-4.095721209114" Y="0.188177663805" Z="1.65" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.65" />
                  <Point X="-3.566850179384" Y="0.042329351451" Z="1.65" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.65" />
                  <Point X="-3.5455709517" Y="0.019377686936" Z="1.65" />
                  <Point X="-3.539556741714" Y="0" Z="1.65" />
                  <Point X="-3.542793649435" Y="-0.010429264125" Z="1.65" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.65" />
                  <Point X="-3.55851841574" Y="-0.036546631683" Z="1.65" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.65" />
                  <Point X="-4.541166579658" Y="-0.307534392951" Z="1.65" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.65" />
                  <Point X="-5.033152444448" Y="-0.636645021899" Z="1.65" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.65" />
                  <Point X="-4.935160716439" Y="-1.175635487009" Z="1.65" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.65" />
                  <Point X="-3.788696396723" Y="-1.381844329081" Z="1.65" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.65" />
                  <Point X="-3.209892799467" Y="-1.312316924358" Z="1.65" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.65" />
                  <Point X="-3.195372060448" Y="-1.332858219357" Z="1.65" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.65" />
                  <Point X="-4.047157140572" Y="-2.00195148008" Z="1.65" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.65" />
                  <Point X="-4.40019080394" Y="-2.523884372212" Z="1.65" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.65" />
                  <Point X="-4.08790118129" Y="-3.003452242808" Z="1.65" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.65" />
                  <Point X="-3.023992208709" Y="-2.815964105668" Z="1.65" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.65" />
                  <Point X="-2.566769358056" Y="-2.561561111075" Z="1.65" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.65" />
                  <Point X="-3.039452907501" Y="-3.411086173783" Z="1.65" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.65" />
                  <Point X="-3.156661884968" Y="-3.972547513625" Z="1.65" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.65" />
                  <Point X="-2.736747004072" Y="-4.272687029783" Z="1.65" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.65" />
                  <Point X="-2.304911785335" Y="-4.259002307355" Z="1.65" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.65" />
                  <Point X="-2.135961502823" Y="-4.096141769278" Z="1.65" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.65" />
                  <Point X="-1.859932707158" Y="-3.991184258974" Z="1.65" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.65" />
                  <Point X="-1.577050342366" Y="-4.075951787535" Z="1.65" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.65" />
                  <Point X="-1.404227808407" Y="-4.315410260848" Z="1.65" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.65" />
                  <Point X="-1.396226996457" Y="-4.751347331373" Z="1.65" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.65" />
                  <Point X="-1.309636461511" Y="-4.906123274894" Z="1.65" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.65" />
                  <Point X="-1.012526887399" Y="-4.975940169876" Z="1.65" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.65" />
                  <Point X="-0.557247649777" Y="-4.04186060328" Z="1.65" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.65" />
                  <Point X="-0.359799490546" Y="-3.436233016523" Z="1.65" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.65" />
                  <Point X="-0.164710335927" Y="-3.255359378332" Z="1.65" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.65" />
                  <Point X="0.088648743434" Y="-3.231752666957" Z="1.65" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.65" />
                  <Point X="0.310646368887" Y="-3.365412739245" Z="1.65" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.65" />
                  <Point X="0.677507210624" Y="-4.490675426536" Z="1.65" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.65" />
                  <Point X="0.880768635723" Y="-5.002299882919" Z="1.65" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.65" />
                  <Point X="1.060656110338" Y="-4.967269420509" Z="1.65" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.65" />
                  <Point X="1.034219943712" Y="-3.856830841699" Z="1.65" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.65" />
                  <Point X="0.976175044097" Y="-3.186284326887" Z="1.65" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.65" />
                  <Point X="1.074135189454" Y="-2.972964221958" Z="1.65" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.65" />
                  <Point X="1.27269931876" Y="-2.868170622319" Z="1.65" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.65" />
                  <Point X="1.498801003679" Y="-2.902168561089" Z="1.65" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.65" />
                  <Point X="2.303513134109" Y="-3.859400712666" Z="1.65" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.65" />
                  <Point X="2.730355334248" Y="-4.282435738058" Z="1.65" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.65" />
                  <Point X="2.923487119629" Y="-4.152989213659" Z="1.65" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.65" />
                  <Point X="2.54250097529" Y="-3.192141592205" Z="1.65" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.65" />
                  <Point X="2.257582021067" Y="-2.646689940221" Z="1.65" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.65" />
                  <Point X="2.265268748407" Y="-2.443395971216" Z="1.65" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.65" />
                  <Point X="2.389502407156" Y="-2.29363256156" Z="1.65" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.65" />
                  <Point X="2.581816863292" Y="-2.245865988424" Z="1.65" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.65" />
                  <Point X="3.595272004902" Y="-2.775248524985" Z="1.65" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.65" />
                  <Point X="4.126208959577" Y="-2.959706466834" Z="1.65" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.65" />
                  <Point X="4.295525499896" Y="-2.708121650852" Z="1.65" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.65" />
                  <Point X="3.614877568823" Y="-1.938508454239" Z="1.65" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.65" />
                  <Point X="3.157585395189" Y="-1.559907958202" Z="1.65" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.65" />
                  <Point X="3.097765967032" Y="-1.398495076051" Z="1.65" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.65" />
                  <Point X="3.146390215393" Y="-1.241190446003" Z="1.65" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.65" />
                  <Point X="3.281263742898" Y="-1.141576062817" Z="1.65" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.65" />
                  <Point X="4.379469615134" Y="-1.244962211043" Z="1.65" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.65" />
                  <Point X="4.936548841618" Y="-1.184956267733" Z="1.65" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.65" />
                  <Point X="5.011234229298" Y="-0.813121155177" Z="1.65" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.65" />
                  <Point X="4.202836337661" Y="-0.342696334733" Z="1.65" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.65" />
                  <Point X="3.715584039563" Y="-0.202100971147" Z="1.65" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.65" />
                  <Point X="3.636021518632" Y="-0.142591039697" Z="1.65" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.65" />
                  <Point X="3.593462991689" Y="-0.062807115867" Z="1.65" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.65" />
                  <Point X="3.587908458735" Y="0.033803415333" Z="1.65" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.65" />
                  <Point X="3.619357919768" Y="0.121357698878" Z="1.65" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.65" />
                  <Point X="3.68781137479" Y="0.186047873095" Z="1.65" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.65" />
                  <Point X="4.593131479646" Y="0.447275598036" Z="1.65" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.65" />
                  <Point X="5.02495634205" Y="0.717264047407" Z="1.65" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.65" />
                  <Point X="4.946659059787" Y="1.138074233444" Z="1.65" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.65" />
                  <Point X="3.959153121651" Y="1.287327916409" Z="1.65" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.65" />
                  <Point X="3.430175145997" Y="1.22637833383" Z="1.65" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.65" />
                  <Point X="3.344593299115" Y="1.248185269817" Z="1.65" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.65" />
                  <Point X="3.282503516313" Y="1.299229146157" Z="1.65" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.65" />
                  <Point X="3.245078737367" Y="1.376678780796" Z="1.65" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.65" />
                  <Point X="3.241123128566" Y="1.459278597149" Z="1.65" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.65" />
                  <Point X="3.275333603712" Y="1.535689178235" Z="1.65" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.65" />
                  <Point X="4.050387674845" Y="2.150590690147" Z="1.65" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.65" />
                  <Point X="4.374139344331" Y="2.576079715703" Z="1.65" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.65" />
                  <Point X="4.155636071235" Y="2.915470254719" Z="1.65" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.65" />
                  <Point X="3.032053401006" Y="2.568476905092" Z="1.65" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.65" />
                  <Point X="2.481786262754" Y="2.259486692696" Z="1.65" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.65" />
                  <Point X="2.405300332547" Y="2.248458258457" Z="1.65" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.65" />
                  <Point X="2.338015467848" Y="2.268931101696" Z="1.65" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.65" />
                  <Point X="2.281827498539" Y="2.319009392534" Z="1.65" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.65" />
                  <Point X="2.250971444302" Y="2.384458111286" Z="1.65" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.65" />
                  <Point X="2.253041249961" Y="2.457683313128" Z="1.65" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.65" />
                  <Point X="2.827148447767" Y="3.480085680949" Z="1.65" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.65" />
                  <Point X="2.997371403445" Y="4.095602595563" Z="1.65" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.65" />
                  <Point X="2.614332830181" Y="4.350109857857" Z="1.65" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.65" />
                  <Point X="2.211700472314" Y="4.568126497772" Z="1.65" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.65" />
                  <Point X="1.794523883314" Y="4.74753386843" Z="1.65" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.65" />
                  <Point X="1.287844504293" Y="4.903550857606" Z="1.65" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.65" />
                  <Point X="0.628369881515" Y="5.030753790179" Z="1.65" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.65" />
                  <Point X="0.067615052914" Y="4.607467299279" Z="1.65" />
                  <Point X="0" Y="4.355124473572" Z="1.65" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>