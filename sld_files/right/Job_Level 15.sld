<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#145" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1208" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004715820312" />
                  <Width Value="9.78389453125" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.673900390625" Y="-3.925276855469" />
                  <Point X="25.5633046875" Y="-3.512524414063" />
                  <Point X="25.55772265625" Y="-3.497139648438" />
                  <Point X="25.54236328125" Y="-3.467376220703" />
                  <Point X="25.497158203125" Y="-3.402246337891" />
                  <Point X="25.378634765625" Y="-3.231475830078" />
                  <Point X="25.356751953125" Y="-3.209021484375" />
                  <Point X="25.33049609375" Y="-3.18977734375" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.232546875" Y="-3.153959472656" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.8932265625" Y="-3.118745605469" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.18977734375" />
                  <Point X="24.655560546875" Y="-3.209021484375" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.58847265625" Y="-3.296606933594" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481571289062" />
                  <Point X="24.449009765625" Y="-3.512524169922" />
                  <Point X="24.159025390625" Y="-4.59476171875" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.84324609375" Y="-4.825432128906" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.771724609375" Y="-4.6645625" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.516222167969" />
                  <Point X="23.766779296875" Y="-4.432209960938" />
                  <Point X="23.722962890625" Y="-4.211930664062" />
                  <Point X="23.712060546875" Y="-4.182965332031" />
                  <Point X="23.69598828125" Y="-4.155127929687" />
                  <Point X="23.67635546875" Y="-4.131204589844" />
                  <Point X="23.611955078125" Y="-4.074726074219" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.271498046875" Y="-3.885364013672" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894802001953" />
                  <Point X="22.88612109375" Y="-3.942391113281" />
                  <Point X="22.699376953125" Y="-4.067170166016" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.5100703125" Y="-4.282432617188" />
                  <Point X="22.19828515625" Y="-4.0893828125" />
                  <Point X="22.09109765625" Y="-4.0068515625" />
                  <Point X="21.89527734375" Y="-3.856077148438" />
                  <Point X="22.37070703125" Y="-3.032610595703" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647654785156" />
                  <Point X="22.593412109375" Y="-2.616128662109" />
                  <Point X="22.59442578125" Y="-2.585194335938" />
                  <Point X="22.58544140625" Y="-2.555576171875" />
                  <Point X="22.571224609375" Y="-2.526747314453" />
                  <Point X="22.55319921875" Y="-2.501591308594" />
                  <Point X="22.5358515625" Y="-2.484242675781" />
                  <Point X="22.51069140625" Y="-2.466212890625" />
                  <Point X="22.481861328125" Y="-2.451995605469" />
                  <Point X="22.452244140625" Y="-2.443011474609" />
                  <Point X="22.421310546875" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450295166016" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.427408203125" Y="-3.000100830078" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="21.1634609375" Y="-3.117476318359" />
                  <Point X="20.917138671875" Y="-2.793859130859" />
                  <Point X="20.84029296875" Y="-2.664998535156" />
                  <Point X="20.693857421875" Y="-2.419449951172" />
                  <Point X="21.534431640625" Y="-1.774454467773" />
                  <Point X="21.894044921875" Y="-1.498513305664" />
                  <Point X="21.91693359375" Y="-1.475882446289" />
                  <Point X="21.93627734375" Y="-1.444805908203" />
                  <Point X="21.9451171875" Y="-1.421525878906" />
                  <Point X="21.94826953125" Y="-1.411624145508" />
                  <Point X="21.95384765625" Y="-1.390089599609" />
                  <Point X="21.957962890625" Y="-1.358610351562" />
                  <Point X="21.957265625" Y="-1.335737792969" />
                  <Point X="21.9511171875" Y="-1.313695922852" />
                  <Point X="21.939111328125" Y="-1.284712280273" />
                  <Point X="21.9262734375" Y="-1.262668457031" />
                  <Point X="21.90803125" Y="-1.244835693359" />
                  <Point X="21.88821875" Y="-1.230102661133" />
                  <Point X="21.879716796875" Y="-1.224462036133" />
                  <Point X="21.860544921875" Y="-1.213178833008" />
                  <Point X="21.83128125" Y="-1.201955566406" />
                  <Point X="21.79939453125" Y="-1.195474609375" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="20.589732421875" Y="-1.349515136719" />
                  <Point X="20.267900390625" Y="-1.391885375977" />
                  <Point X="20.26226171875" Y="-1.369814819336" />
                  <Point X="20.165921875" Y="-0.99264855957" />
                  <Point X="20.145591796875" Y="-0.850496887207" />
                  <Point X="20.107578125" Y="-0.584698364258" />
                  <Point X="21.057470703125" Y="-0.330174713135" />
                  <Point X="21.467125" Y="-0.220408279419" />
                  <Point X="21.48766015625" Y="-0.212245132446" />
                  <Point X="21.510888671875" Y="-0.199707565308" />
                  <Point X="21.519931640625" Y="-0.194153549194" />
                  <Point X="21.5400234375" Y="-0.180209625244" />
                  <Point X="21.563982421875" Y="-0.158679229736" />
                  <Point X="21.58481640625" Y="-0.128485855103" />
                  <Point X="21.59479296875" Y="-0.105513748169" />
                  <Point X="21.598384765625" Y="-0.095834228516" />
                  <Point X="21.605080078125" Y="-0.07426512146" />
                  <Point X="21.610525390625" Y="-0.045534244537" />
                  <Point X="21.60960546875" Y="-0.011917840958" />
                  <Point X="21.6048046875" Y="0.011137292862" />
                  <Point X="21.602529296875" Y="0.019931875229" />
                  <Point X="21.59583203125" Y="0.04150933075" />
                  <Point X="21.582515625" Y="0.070833404541" />
                  <Point X="21.559794921875" Y="0.099963188171" />
                  <Point X="21.540314453125" Y="0.116798866272" />
                  <Point X="21.532361328125" Y="0.122967475891" />
                  <Point X="21.51226953125" Y="0.1369115448" />
                  <Point X="21.49807421875" Y="0.145047805786" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="20.393013671875" Y="0.445655700684" />
                  <Point X="20.10818359375" Y="0.521975646973" />
                  <Point X="20.113423828125" Y="0.557388427734" />
                  <Point X="20.17551171875" Y="0.976968505859" />
                  <Point X="20.21644140625" Y="1.128013916016" />
                  <Point X="20.29644921875" Y="1.423268066406" />
                  <Point X="20.940685546875" Y="1.338452758789" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255017578125" Y="1.299341796875" />
                  <Point X="21.276578125" Y="1.301228271484" />
                  <Point X="21.29686328125" Y="1.305263671875" />
                  <Point X="21.313822265625" Y="1.310610961914" />
                  <Point X="21.3582890625" Y="1.324631225586" />
                  <Point X="21.37722265625" Y="1.332962402344" />
                  <Point X="21.395966796875" Y="1.343784423828" />
                  <Point X="21.4126484375" Y="1.356016113281" />
                  <Point X="21.426287109375" Y="1.371568603516" />
                  <Point X="21.438701171875" Y="1.389298706055" />
                  <Point X="21.448650390625" Y="1.407432617188" />
                  <Point X="21.455455078125" Y="1.423861450195" />
                  <Point X="21.473296875" Y="1.466937011719" />
                  <Point X="21.479083984375" Y="1.486788330078" />
                  <Point X="21.48284375" Y="1.508103271484" />
                  <Point X="21.484197265625" Y="1.528750244141" />
                  <Point X="21.481048828125" Y="1.549200439453" />
                  <Point X="21.4754453125" Y="1.570106689453" />
                  <Point X="21.46794921875" Y="1.589378295898" />
                  <Point X="21.45973828125" Y="1.605151245117" />
                  <Point X="21.438208984375" Y="1.64650793457" />
                  <Point X="21.42671875" Y="1.663705810547" />
                  <Point X="21.412806640625" Y="1.680285888672" />
                  <Point X="21.39786328125" Y="1.694590332031" />
                  <Point X="20.78175" Y="2.167350830078" />
                  <Point X="20.648140625" Y="2.269873779297" />
                  <Point X="20.677595703125" Y="2.320338378906" />
                  <Point X="20.918853515625" Y="2.733666503906" />
                  <Point X="21.02726171875" Y="2.873012695312" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.6129921875" Y="2.948796875" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.876826171875" Y="2.823729980469" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.019537109375" Y="2.835653076172" />
                  <Point X="22.037791015625" Y="2.847281982422" />
                  <Point X="22.053921875" Y="2.860228271484" />
                  <Point X="22.0706875" Y="2.876993408203" />
                  <Point X="22.114646484375" Y="2.920951660156" />
                  <Point X="22.127595703125" Y="2.937085449219" />
                  <Point X="22.139224609375" Y="2.95533984375" />
                  <Point X="22.14837109375" Y="2.973888671875" />
                  <Point X="22.1532890625" Y="2.993977539062" />
                  <Point X="22.156115234375" Y="3.015436523438" />
                  <Point X="22.15656640625" Y="3.036120849609" />
                  <Point X="22.1545" Y="3.059740234375" />
                  <Point X="22.14908203125" Y="3.121670410156" />
                  <Point X="22.145044921875" Y="3.141963134766" />
                  <Point X="22.13853515625" Y="3.162605224609" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.857185546875" Y="3.654414794922" />
                  <Point X="21.81666796875" Y="3.724595947266" />
                  <Point X="21.8791953125" Y="3.772536376953" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.470123046875" Y="4.189547851562" />
                  <Point X="22.83296484375" Y="4.391134277344" />
                  <Point X="22.90159375" Y="4.301693847656" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.031177734375" Y="4.175709472656" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222548828125" Y="4.134482421875" />
                  <Point X="23.2499296875" Y="4.14582421875" />
                  <Point X="23.32172265625" Y="4.175562011719" />
                  <Point X="23.33985546875" Y="4.185509765625" />
                  <Point X="23.3575859375" Y="4.197924316406" />
                  <Point X="23.373140625" Y="4.211565429688" />
                  <Point X="23.385373046875" Y="4.22825" />
                  <Point X="23.396193359375" Y="4.246994140625" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.41343359375" Y="4.294186523438" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583984375" />
                  <Point X="23.44272265625" Y="4.410145507812" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.41580078125" Y="4.631897949219" />
                  <Point X="23.50641796875" Y="4.657304199219" />
                  <Point X="24.050369140625" Y="4.80980859375" />
                  <Point X="24.257359375" Y="4.834033691406" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.812744140625" Y="4.485431640625" />
                  <Point X="24.866095703125" Y="4.286315917969" />
                  <Point X="24.87887109375" Y="4.258122558594" />
                  <Point X="24.89673046875" Y="4.231395507813" />
                  <Point X="24.91788671875" Y="4.208808105469" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.286109375" Y="4.808397949219" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.369091796875" Y="4.881479003906" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.015298828125" Y="4.790392089844" />
                  <Point X="26.481029296875" Y="4.677950195312" />
                  <Point X="26.59142578125" Y="4.637909179688" />
                  <Point X="26.894640625" Y="4.527930175781" />
                  <Point X="27.0024375" Y="4.477517578125" />
                  <Point X="27.294576171875" Y="4.340894042969" />
                  <Point X="27.39873828125" Y="4.280208984375" />
                  <Point X="27.6809765625" Y="4.115775390625" />
                  <Point X="27.77919140625" Y="4.045932128906" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.385470703125" Y="2.963130859375" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.539935546875" />
                  <Point X="27.133078125" Y="2.516058105469" />
                  <Point X="27.127150390625" Y="2.493891845703" />
                  <Point X="27.111607421875" Y="2.435771728516" />
                  <Point X="27.1084140625" Y="2.408797607422" />
                  <Point X="27.109388671875" Y="2.370728515625" />
                  <Point X="27.110041015625" Y="2.361789794922" />
                  <Point X="27.116099609375" Y="2.311532470703" />
                  <Point X="27.12144140625" Y="2.289610351562" />
                  <Point X="27.12970703125" Y="2.267520019531" />
                  <Point X="27.140072265625" Y="2.247467773438" />
                  <Point X="27.15193359375" Y="2.229988769531" />
                  <Point X="27.18303125" Y="2.184159179688" />
                  <Point X="27.194466796875" Y="2.170326660156" />
                  <Point X="27.221599609375" Y="2.145593261719" />
                  <Point X="27.239078125" Y="2.133732910156" />
                  <Point X="27.284908203125" Y="2.102635742188" />
                  <Point X="27.304953125" Y="2.092272216797" />
                  <Point X="27.327041015625" Y="2.084006347656" />
                  <Point X="27.34896484375" Y="2.078663330078" />
                  <Point X="27.3681328125" Y="2.076352050781" />
                  <Point X="27.418388671875" Y="2.070291748047" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.47320703125" Y="2.074171386719" />
                  <Point X="27.495373046875" Y="2.080099121094" />
                  <Point X="27.553494140625" Y="2.095641113281" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.668884765625" Y="2.733885498047" />
                  <Point X="28.967328125" Y="2.906191650391" />
                  <Point X="29.1232734375" Y="2.689462158203" />
                  <Point X="29.178025390625" Y="2.598984375" />
                  <Point X="29.26219921875" Y="2.459884033203" />
                  <Point X="28.543986328125" Y="1.908778808594" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.221427734375" Y="1.660243408203" />
                  <Point X="28.203974609375" Y="1.641627441406" />
                  <Point X="28.188021484375" Y="1.620815429688" />
                  <Point X="28.146193359375" Y="1.56624609375" />
                  <Point X="28.13660546875" Y="1.55091003418" />
                  <Point X="28.1216328125" Y="1.517090087891" />
                  <Point X="28.115689453125" Y="1.495841186523" />
                  <Point X="28.100107421875" Y="1.440125366211" />
                  <Point X="28.09665234375" Y="1.417824584961" />
                  <Point X="28.0958359375" Y="1.394253417969" />
                  <Point X="28.097740234375" Y="1.371765258789" />
                  <Point X="28.102619140625" Y="1.348122802734" />
                  <Point X="28.11541015625" Y="1.286132446289" />
                  <Point X="28.1206796875" Y="1.268979614258" />
                  <Point X="28.13628125" Y="1.235742553711" />
                  <Point X="28.149548828125" Y="1.215575317383" />
                  <Point X="28.184337890625" Y="1.162697143555" />
                  <Point X="28.198892578125" Y="1.145450073242" />
                  <Point X="28.21613671875" Y="1.129360107422" />
                  <Point X="28.23434765625" Y="1.116033935547" />
                  <Point X="28.253576171875" Y="1.105210449219" />
                  <Point X="28.303990234375" Y="1.076831542969" />
                  <Point X="28.3205234375" Y="1.069500976563" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.3821171875" Y="1.056002563477" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.51446484375" Y="1.182094238281" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.84594140625" Y="0.932787963867" />
                  <Point X="29.86319140625" Y="0.821990783691" />
                  <Point X="29.890865234375" Y="0.644238708496" />
                  <Point X="29.075515625" Y="0.425766021729" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.7047890625" Y="0.325585754395" />
                  <Point X="28.681546875" Y="0.315068023682" />
                  <Point X="28.656005859375" Y="0.300304748535" />
                  <Point X="28.589037109375" Y="0.261595489502" />
                  <Point X="28.574314453125" Y="0.251097961426" />
                  <Point X="28.54753125" Y="0.225575637817" />
                  <Point X="28.53220703125" Y="0.206048400879" />
                  <Point X="28.492025390625" Y="0.154848007202" />
                  <Point X="28.48030078125" Y="0.135568191528" />
                  <Point X="28.47052734375" Y="0.114104736328" />
                  <Point X="28.463681640625" Y="0.09260181427" />
                  <Point X="28.45857421875" Y="0.065928604126" />
                  <Point X="28.4451796875" Y="-0.004008804321" />
                  <Point X="28.443484375" Y="-0.021874832153" />
                  <Point X="28.4451796875" Y="-0.058551330566" />
                  <Point X="28.450287109375" Y="-0.085224693298" />
                  <Point X="28.463681640625" Y="-0.155161941528" />
                  <Point X="28.47052734375" Y="-0.176664871216" />
                  <Point X="28.48030078125" Y="-0.198128326416" />
                  <Point X="28.492025390625" Y="-0.217408447266" />
                  <Point X="28.507349609375" Y="-0.236935516357" />
                  <Point X="28.54753125" Y="-0.288136077881" />
                  <Point X="28.560001953125" Y="-0.30123815918" />
                  <Point X="28.589037109375" Y="-0.324155303955" />
                  <Point X="28.614578125" Y="-0.338918609619" />
                  <Point X="28.681546875" Y="-0.377627716064" />
                  <Point X="28.692708984375" Y="-0.383138305664" />
                  <Point X="28.716580078125" Y="-0.392149810791" />
                  <Point X="29.65770703125" Y="-0.644324279785" />
                  <Point X="29.89147265625" Y="-0.706961791992" />
                  <Point X="29.855025390625" Y="-0.948725769043" />
                  <Point X="29.832919921875" Y="-1.045594116211" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="28.840646484375" Y="-1.058243041992" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.32453125" Y="-1.01640447998" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.163974609375" Y="-1.056597167969" />
                  <Point X="28.1361484375" Y="-1.073489257812" />
                  <Point X="28.1123984375" Y="-1.093960571289" />
                  <Point X="28.082099609375" Y="-1.130401245117" />
                  <Point X="28.002654296875" Y="-1.225948608398" />
                  <Point X="27.987935546875" Y="-1.250329101562" />
                  <Point X="27.976591796875" Y="-1.277714355469" />
                  <Point X="27.969759765625" Y="-1.305366943359" />
                  <Point X="27.96541796875" Y="-1.352559326172" />
                  <Point X="27.95403125" Y="-1.476297241211" />
                  <Point X="27.95634765625" Y="-1.507560791016" />
                  <Point X="27.964078125" Y="-1.539182373047" />
                  <Point X="27.97644921875" Y="-1.567995239258" />
                  <Point X="28.004189453125" Y="-1.611145629883" />
                  <Point X="28.0769296875" Y="-1.724285766602" />
                  <Point X="28.0869375" Y="-1.737243652344" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.984001953125" Y="-2.431071289062" />
                  <Point X="29.213123046875" Y="-2.606882324219" />
                  <Point X="29.1248046875" Y="-2.749794189453" />
                  <Point X="29.079095703125" Y="-2.814741699219" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="28.1716953125" Y="-2.390989501953" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.69456640625" Y="-2.149050537109" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.395271484375" Y="-2.16126171875" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246549804688" />
                  <Point X="27.22142578125" Y="-2.267509521484" />
                  <Point X="27.20453515625" Y="-2.290437744141" />
                  <Point X="27.17844921875" Y="-2.340000976562" />
                  <Point X="27.1100546875" Y="-2.469955810547" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531908447266" />
                  <Point X="27.09567578125" Y="-2.563259765625" />
                  <Point X="27.106451171875" Y="-2.622920166016" />
                  <Point X="27.134703125" Y="-2.779350097656" />
                  <Point X="27.13898828125" Y="-2.795140136719" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.71305078125" Y="-3.798156005859" />
                  <Point X="27.86128515625" Y="-4.054904541016" />
                  <Point X="27.78184765625" Y="-4.111645019531" />
                  <Point X="27.73073828125" Y="-4.144727050781" />
                  <Point X="27.701765625" Y="-4.163480957031" />
                  <Point X="27.041455078125" Y="-3.302947753906" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.663083984375" Y="-2.862727539062" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.352748046875" Y="-2.747038574219" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.104595703125" Y="-2.795461181641" />
                  <Point X="26.054904296875" Y="-2.836778320312" />
                  <Point X="25.924611328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968861083984" />
                  <Point X="25.88725" Y="-2.996687744141" />
                  <Point X="25.875625" Y="-3.025808837891" />
                  <Point X="25.860767578125" Y="-3.094165527344" />
                  <Point X="25.821810546875" Y="-3.273396728516" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.978791015625" Y="-4.531209960938" />
                  <Point X="26.02206640625" Y="-4.859915039062" />
                  <Point X="25.9756796875" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941580078125" Y="-4.752637695312" />
                  <Point X="23.86691796875" Y="-4.733428222656" />
                  <Point X="23.858755859375" Y="-4.731328125" />
                  <Point X="23.865912109375" Y="-4.676961914062" />
                  <Point X="23.8792265625" Y="-4.575837890625" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.54103125" />
                  <Point X="23.8782421875" Y="-4.509323730469" />
                  <Point X="23.876666015625" Y="-4.497686523438" />
                  <Point X="23.859953125" Y="-4.413674316406" />
                  <Point X="23.81613671875" Y="-4.193395019531" />
                  <Point X="23.811873046875" Y="-4.178465332031" />
                  <Point X="23.800970703125" Y="-4.1495" />
                  <Point X="23.79433203125" Y="-4.135464355469" />
                  <Point X="23.778259765625" Y="-4.107626953125" />
                  <Point X="23.76942578125" Y="-4.094861572266" />
                  <Point X="23.74979296875" Y="-4.070938232422" />
                  <Point X="23.738994140625" Y="-4.059780273438" />
                  <Point X="23.67459375" Y="-4.003301757813" />
                  <Point X="23.505734375" Y="-3.855215332031" />
                  <Point X="23.493263671875" Y="-3.845965820312" />
                  <Point X="23.46698046875" Y="-3.829621582031" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.2777109375" Y="-3.790567382812" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.94632421875" Y="-3.795496582031" />
                  <Point X="22.9181328125" Y="-3.808270507812" />
                  <Point X="22.9045625" Y="-3.815812988281" />
                  <Point X="22.833341796875" Y="-3.863402099609" />
                  <Point X="22.64659765625" Y="-3.988181152344" />
                  <Point X="22.640318359375" Y="-3.992758789062" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629394531" />
                  <Point X="22.496798828125" Y="-4.162478027344" />
                  <Point X="22.25240625" Y="-4.01115625" />
                  <Point X="22.1490546875" Y="-3.931579101562" />
                  <Point X="22.01913671875" Y="-3.831546386719" />
                  <Point X="22.452978515625" Y="-3.080110595703" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.710085449219" />
                  <Point X="22.67605078125" Y="-2.681120605469" />
                  <Point X="22.680314453125" Y="-2.666189941406" />
                  <Point X="22.6865859375" Y="-2.634663818359" />
                  <Point X="22.688361328125" Y="-2.619239990234" />
                  <Point X="22.689375" Y="-2.588305664062" />
                  <Point X="22.6853359375" Y="-2.557617919922" />
                  <Point X="22.6763515625" Y="-2.527999755859" />
                  <Point X="22.67064453125" Y="-2.513558837891" />
                  <Point X="22.656427734375" Y="-2.484729980469" />
                  <Point X="22.648447265625" Y="-2.471414306641" />
                  <Point X="22.630421875" Y="-2.446258300781" />
                  <Point X="22.620376953125" Y="-2.43441796875" />
                  <Point X="22.603029296875" Y="-2.417069335938" />
                  <Point X="22.5911875" Y="-2.407022705078" />
                  <Point X="22.56602734375" Y="-2.388992919922" />
                  <Point X="22.552708984375" Y="-2.381009765625" />
                  <Point X="22.52387890625" Y="-2.366792480469" />
                  <Point X="22.5094375" Y="-2.361086181641" />
                  <Point X="22.4798203125" Y="-2.352102050781" />
                  <Point X="22.449134765625" Y="-2.348062255859" />
                  <Point X="22.418201171875" Y="-2.349074951172" />
                  <Point X="22.40277734375" Y="-2.350849609375" />
                  <Point X="22.371251953125" Y="-2.357120605469" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372285888672" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.379908203125" Y="-2.917828369141" />
                  <Point X="21.20691015625" Y="-3.017708740234" />
                  <Point X="20.99598828125" Y="-2.740600585938" />
                  <Point X="20.92188671875" Y="-2.616340820313" />
                  <Point X="20.818734375" Y="-2.443372558594" />
                  <Point X="21.592263671875" Y="-1.849822998047" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.960837890625" Y="-1.566067871094" />
                  <Point X="21.9837265625" Y="-1.543437011719" />
                  <Point X="21.9975859375" Y="-1.526084594727" />
                  <Point X="22.0169296875" Y="-1.495008178711" />
                  <Point X="22.02508984375" Y="-1.478529663086" />
                  <Point X="22.0339296875" Y="-1.455249633789" />
                  <Point X="22.040234375" Y="-1.435445922852" />
                  <Point X="22.0458125" Y="-1.413911376953" />
                  <Point X="22.048046875" Y="-1.402404052734" />
                  <Point X="22.052162109375" Y="-1.370924804688" />
                  <Point X="22.05291796875" Y="-1.355715698242" />
                  <Point X="22.052220703125" Y="-1.332843139648" />
                  <Point X="22.048771484375" Y="-1.310212646484" />
                  <Point X="22.042623046875" Y="-1.288170776367" />
                  <Point X="22.038884765625" Y="-1.27733984375" />
                  <Point X="22.02687890625" Y="-1.248356201172" />
                  <Point X="22.021205078125" Y="-1.236902832031" />
                  <Point X="22.0083671875" Y="-1.214859130859" />
                  <Point X="21.992681640625" Y="-1.194735351562" />
                  <Point X="21.974439453125" Y="-1.176902587891" />
                  <Point X="21.96471875" Y="-1.168603149414" />
                  <Point X="21.94490625" Y="-1.153869995117" />
                  <Point X="21.92790234375" Y="-1.142588745117" />
                  <Point X="21.90873046875" Y="-1.131305541992" />
                  <Point X="21.8945625" Y="-1.124478393555" />
                  <Point X="21.865298828125" Y="-1.113255249023" />
                  <Point X="21.850203125" Y="-1.108859130859" />
                  <Point X="21.81831640625" Y="-1.102378051758" />
                  <Point X="21.802701171875" Y="-1.100531982422" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="20.57733203125" Y="-1.25532800293" />
                  <Point X="20.339080078125" Y="-1.286694580078" />
                  <Point X="20.25923828125" Y="-0.974118225098" />
                  <Point X="20.239634765625" Y="-0.837047180176" />
                  <Point X="20.213548828125" Y="-0.654654602051" />
                  <Point X="21.08205859375" Y="-0.421937683105" />
                  <Point X="21.491712890625" Y="-0.312171234131" />
                  <Point X="21.50221875" Y="-0.308688842773" />
                  <Point X="21.52275390625" Y="-0.300525726318" />
                  <Point X="21.532783203125" Y="-0.295844848633" />
                  <Point X="21.55601171875" Y="-0.283307312012" />
                  <Point X="21.574095703125" Y="-0.272199554443" />
                  <Point X="21.5941875" Y="-0.258255584717" />
                  <Point X="21.603521484375" Y="-0.250870529175" />
                  <Point X="21.62748046875" Y="-0.229340133667" />
                  <Point X="21.642173828125" Y="-0.212633132935" />
                  <Point X="21.6630078125" Y="-0.182439651489" />
                  <Point X="21.671953125" Y="-0.166328781128" />
                  <Point X="21.6819296875" Y="-0.1433565979" />
                  <Point X="21.68911328125" Y="-0.123997718811" />
                  <Point X="21.69580859375" Y="-0.10242868042" />
                  <Point X="21.69841796875" Y="-0.091955375671" />
                  <Point X="21.70386328125" Y="-0.063224491119" />
                  <Point X="21.705490234375" Y="-0.042935539246" />
                  <Point X="21.7045703125" Y="-0.009319125175" />
                  <Point X="21.702611328125" Y="0.007448655605" />
                  <Point X="21.697810546875" Y="0.030503778458" />
                  <Point X="21.693259765625" Y="0.048092849731" />
                  <Point X="21.6865625" Y="0.06967036438" />
                  <Point X="21.682330078125" Y="0.080789581299" />
                  <Point X="21.669013671875" Y="0.110113616943" />
                  <Point X="21.657423828125" Y="0.129260559082" />
                  <Point X="21.634703125" Y="0.158390335083" />
                  <Point X="21.6219140625" Y="0.171840118408" />
                  <Point X="21.60243359375" Y="0.188675811768" />
                  <Point X="21.58652734375" Y="0.201013153076" />
                  <Point X="21.566435546875" Y="0.214957244873" />
                  <Point X="21.559509765625" Y="0.219332855225" />
                  <Point X="21.5343828125" Y="0.232835540771" />
                  <Point X="21.50343359375" Y="0.245635986328" />
                  <Point X="21.491712890625" Y="0.249611190796" />
                  <Point X="20.4176015625" Y="0.537418640137" />
                  <Point X="20.2145546875" Y="0.591825012207" />
                  <Point X="20.26866796875" Y="0.957521972656" />
                  <Point X="20.308134765625" Y="1.103167236328" />
                  <Point X="20.3664140625" Y="1.318237182617" />
                  <Point X="20.92828515625" Y="1.24426550293" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815551758" />
                  <Point X="21.2529453125" Y="1.204364379883" />
                  <Point X="21.263298828125" Y="1.204703369141" />
                  <Point X="21.284859375" Y="1.20658984375" />
                  <Point X="21.29511328125" Y="1.208054077148" />
                  <Point X="21.3153984375" Y="1.212089477539" />
                  <Point X="21.342390625" Y="1.220008178711" />
                  <Point X="21.386857421875" Y="1.234028320312" />
                  <Point X="21.39655078125" Y="1.237677001953" />
                  <Point X="21.415484375" Y="1.246008178711" />
                  <Point X="21.42472265625" Y="1.250690185547" />
                  <Point X="21.443466796875" Y="1.261512207031" />
                  <Point X="21.452142578125" Y="1.267172607422" />
                  <Point X="21.46882421875" Y="1.279404418945" />
                  <Point X="21.48407421875" Y="1.293379516602" />
                  <Point X="21.497712890625" Y="1.308932006836" />
                  <Point X="21.504107421875" Y="1.317080810547" />
                  <Point X="21.516521484375" Y="1.334810913086" />
                  <Point X="21.52198828125" Y="1.343602661133" />
                  <Point X="21.5319375" Y="1.361736572266" />
                  <Point X="21.543224609375" Y="1.387508178711" />
                  <Point X="21.56106640625" Y="1.430583740234" />
                  <Point X="21.5645" Y="1.440349121094" />
                  <Point X="21.570287109375" Y="1.460200317383" />
                  <Point X="21.572640625" Y="1.470285888672" />
                  <Point X="21.576400390625" Y="1.491600830078" />
                  <Point X="21.577640625" Y="1.501888916016" />
                  <Point X="21.578994140625" Y="1.522535888672" />
                  <Point X="21.578091796875" Y="1.543205810547" />
                  <Point X="21.574943359375" Y="1.563656005859" />
                  <Point X="21.572810546875" Y="1.573795166016" />
                  <Point X="21.56720703125" Y="1.594701416016" />
                  <Point X="21.563982421875" Y="1.604545410156" />
                  <Point X="21.556486328125" Y="1.623817016602" />
                  <Point X="21.54400390625" Y="1.649017578125" />
                  <Point X="21.522474609375" Y="1.690374267578" />
                  <Point X="21.517201171875" Y="1.699283691406" />
                  <Point X="21.5057109375" Y="1.716481689453" />
                  <Point X="21.499494140625" Y="1.724770019531" />
                  <Point X="21.48558203125" Y="1.741350097656" />
                  <Point X="21.478498046875" Y="1.748911987305" />
                  <Point X="21.4635546875" Y="1.763216552734" />
                  <Point X="21.4556953125" Y="1.769958862305" />
                  <Point X="20.83958203125" Y="2.242719482422" />
                  <Point X="20.77238671875" Y="2.29428125" />
                  <Point X="20.997716796875" Y="2.680324707031" />
                  <Point X="21.1022421875" Y="2.814678955078" />
                  <Point X="21.273662109375" Y="3.035013427734" />
                  <Point X="21.5654921875" Y="2.866524414062" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.868546875" Y="2.729091552734" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.02356640625" Y="2.734227294922" />
                  <Point X="22.04300390625" Y="2.741302001953" />
                  <Point X="22.0615546875" Y="2.750450439453" />
                  <Point X="22.070580078125" Y="2.755530761719" />
                  <Point X="22.088833984375" Y="2.767159667969" />
                  <Point X="22.09725390625" Y="2.773192626953" />
                  <Point X="22.113384765625" Y="2.786138916016" />
                  <Point X="22.121095703125" Y="2.793052246094" />
                  <Point X="22.137861328125" Y="2.809817382812" />
                  <Point X="22.1818203125" Y="2.853775634766" />
                  <Point X="22.188734375" Y="2.861487548828" />
                  <Point X="22.20168359375" Y="2.877621337891" />
                  <Point X="22.20771875" Y="2.886043212891" />
                  <Point X="22.21934765625" Y="2.904297607422" />
                  <Point X="22.2244296875" Y="2.913325439453" />
                  <Point X="22.233576171875" Y="2.931874267578" />
                  <Point X="22.240646484375" Y="2.951298828125" />
                  <Point X="22.245564453125" Y="2.971387695312" />
                  <Point X="22.2474765625" Y="2.981572998047" />
                  <Point X="22.250302734375" Y="3.003031982422" />
                  <Point X="22.251091796875" Y="3.013364746094" />
                  <Point X="22.25154296875" Y="3.034049072266" />
                  <Point X="22.251205078125" Y="3.044400634766" />
                  <Point X="22.249138671875" Y="3.068020019531" />
                  <Point X="22.243720703125" Y="3.129950195312" />
                  <Point X="22.242255859375" Y="3.140206787109" />
                  <Point X="22.23821875" Y="3.160499511719" />
                  <Point X="22.235646484375" Y="3.170535644531" />
                  <Point X="22.22913671875" Y="3.191177734375" />
                  <Point X="22.225486328125" Y="3.200872558594" />
                  <Point X="22.21715625" Y="3.219800292969" />
                  <Point X="22.2124765625" Y="3.229033203125" />
                  <Point X="21.940611328125" Y="3.699915771484" />
                  <Point X="22.351630859375" Y="4.015039550781" />
                  <Point X="22.516259765625" Y="4.10650390625" />
                  <Point X="22.8074765625" Y="4.268296875" />
                  <Point X="22.826224609375" Y="4.243862304688" />
                  <Point X="22.881435546875" Y="4.171909667969" />
                  <Point X="22.8881796875" Y="4.164048828125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93491015625" Y="4.121895996094" />
                  <Point X="22.952111328125" Y="4.110403320312" />
                  <Point X="22.9610234375" Y="4.105128417969" />
                  <Point X="22.9873125" Y="4.091443115234" />
                  <Point X="23.056240234375" Y="4.055561279297" />
                  <Point X="23.06567578125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.043789550781" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.229271484375" Y="4.037488525391" />
                  <Point X="23.2491328125" Y="4.043277587891" />
                  <Point X="23.258904296875" Y="4.046714111328" />
                  <Point X="23.28628515625" Y="4.058055908203" />
                  <Point X="23.358078125" Y="4.087793701172" />
                  <Point X="23.367416015625" Y="4.092272460938" />
                  <Point X="23.385548828125" Y="4.102220214844" />
                  <Point X="23.39434375" Y="4.107689453125" />
                  <Point X="23.41207421875" Y="4.120104003906" />
                  <Point X="23.420224609375" Y="4.126499511719" />
                  <Point X="23.435779296875" Y="4.140140625" />
                  <Point X="23.449755859375" Y="4.15539453125" />
                  <Point X="23.46198828125" Y="4.172079101562" />
                  <Point X="23.4676484375" Y="4.180755371094" />
                  <Point X="23.47846875" Y="4.199499511719" />
                  <Point X="23.4831484375" Y="4.208733398438" />
                  <Point X="23.4914765625" Y="4.227660644531" />
                  <Point X="23.495125" Y="4.237354003906" />
                  <Point X="23.504037109375" Y="4.265619140625" />
                  <Point X="23.527404296875" Y="4.33973046875" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370050292969" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401864257812" />
                  <Point X="23.53769921875" Y="4.412217285156" />
                  <Point X="23.537248046875" Y="4.4328984375" />
                  <Point X="23.536458984375" Y="4.443226074219" />
                  <Point X="23.520736328125" Y="4.562655273438" />
                  <Point X="23.532064453125" Y="4.565831542969" />
                  <Point X="24.068826171875" Y="4.7163203125" />
                  <Point X="24.26840234375" Y="4.739677734375" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.72098046875" Y="4.46084375" />
                  <Point X="24.77433203125" Y="4.261728027344" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912597656" />
                  <Point X="24.7998828125" Y="4.205341308594" />
                  <Point X="24.8177421875" Y="4.178614257812" />
                  <Point X="24.82739453125" Y="4.166452636719" />
                  <Point X="24.84855078125" Y="4.143865234375" />
                  <Point X="24.873103515625" Y="4.125025390625" />
                  <Point X="24.9003984375" Y="4.110436035156" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.377873046875" Y="4.783810058594" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="25.99300390625" Y="4.698045410156" />
                  <Point X="26.45359765625" Y="4.58684375" />
                  <Point X="26.559033203125" Y="4.548602050781" />
                  <Point X="26.858251953125" Y="4.440072753906" />
                  <Point X="26.962193359375" Y="4.391462890625" />
                  <Point X="27.25045703125" Y="4.256651367187" />
                  <Point X="27.350916015625" Y="4.198124023438" />
                  <Point X="27.629435546875" Y="4.035857177734" />
                  <Point X="27.72413671875" Y="3.968511962891" />
                  <Point X="27.817783203125" Y="3.901916015625" />
                  <Point X="27.30319921875" Y="3.010630859375" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.06237890625" Y="2.593110595703" />
                  <Point X="27.0531875" Y="2.573448486328" />
                  <Point X="27.044185546875" Y="2.549571044922" />
                  <Point X="27.041302734375" Y="2.540600830078" />
                  <Point X="27.035375" Y="2.518434570312" />
                  <Point X="27.01983203125" Y="2.460314453125" />
                  <Point X="27.017265625" Y="2.446940429688" />
                  <Point X="27.014072265625" Y="2.419966308594" />
                  <Point X="27.0134453125" Y="2.406366210938" />
                  <Point X="27.014419921875" Y="2.368297119141" />
                  <Point X="27.015724609375" Y="2.350419677734" />
                  <Point X="27.021783203125" Y="2.300162353516" />
                  <Point X="27.02380078125" Y="2.289041748047" />
                  <Point X="27.029142578125" Y="2.267119628906" />
                  <Point X="27.032466796875" Y="2.256318115234" />
                  <Point X="27.040732421875" Y="2.234227783203" />
                  <Point X="27.045314453125" Y="2.223896728516" />
                  <Point X="27.0556796875" Y="2.203844482422" />
                  <Point X="27.061462890625" Y="2.194123291016" />
                  <Point X="27.07332421875" Y="2.176644287109" />
                  <Point X="27.104421875" Y="2.130814697266" />
                  <Point X="27.1098125" Y="2.123628173828" />
                  <Point X="27.130466796875" Y="2.100118896484" />
                  <Point X="27.157599609375" Y="2.075385498047" />
                  <Point X="27.1682578125" Y="2.066982910156" />
                  <Point X="27.185736328125" Y="2.055122558594" />
                  <Point X="27.23156640625" Y="2.024025512695" />
                  <Point X="27.24127734375" Y="2.018247314453" />
                  <Point X="27.261322265625" Y="2.007883789062" />
                  <Point X="27.27165625" Y="2.003298339844" />
                  <Point X="27.293744140625" Y="1.995032470703" />
                  <Point X="27.304546875" Y="1.991707763672" />
                  <Point X="27.326470703125" Y="1.986364746094" />
                  <Point X="27.356759765625" Y="1.982035400391" />
                  <Point X="27.407015625" Y="1.975974975586" />
                  <Point X="27.416044921875" Y="1.975320678711" />
                  <Point X="27.44757421875" Y="1.975497436523" />
                  <Point X="27.484314453125" Y="1.979823120117" />
                  <Point X="27.49775" Y="1.982396362305" />
                  <Point X="27.519916015625" Y="1.98832421875" />
                  <Point X="27.578037109375" Y="2.003866088867" />
                  <Point X="27.583998046875" Y="2.005671630859" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.716384765625" Y="2.651613037109" />
                  <Point X="28.94040625" Y="2.780951660156" />
                  <Point X="29.043953125" Y="2.637043457031" />
                  <Point X="29.096748046875" Y="2.549800292969" />
                  <Point X="29.136884765625" Y="2.483471923828" />
                  <Point X="28.486154296875" Y="1.984147216797" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.168140625" Y="1.739869506836" />
                  <Point X="28.152123046875" Y="1.725219238281" />
                  <Point X="28.134669921875" Y="1.706603149414" />
                  <Point X="28.128578125" Y="1.699422119141" />
                  <Point X="28.112625" Y="1.678610107422" />
                  <Point X="28.070796875" Y="1.624040771484" />
                  <Point X="28.065640625" Y="1.616606811523" />
                  <Point X="28.04973828125" Y="1.589367797852" />
                  <Point X="28.034765625" Y="1.555547851562" />
                  <Point X="28.03014453125" Y="1.5426796875" />
                  <Point X="28.024201171875" Y="1.521430786133" />
                  <Point X="28.008619140625" Y="1.46571496582" />
                  <Point X="28.0062265625" Y="1.454670288086" />
                  <Point X="28.002771484375" Y="1.432369506836" />
                  <Point X="28.001708984375" Y="1.421113037109" />
                  <Point X="28.000892578125" Y="1.397541870117" />
                  <Point X="28.001173828125" Y="1.386237548828" />
                  <Point X="28.003078125" Y="1.363749389648" />
                  <Point X="28.004701171875" Y="1.352565429688" />
                  <Point X="28.009580078125" Y="1.328922973633" />
                  <Point X="28.02237109375" Y="1.266932617188" />
                  <Point X="28.024599609375" Y="1.25823425293" />
                  <Point X="28.03468359375" Y="1.228612548828" />
                  <Point X="28.05028515625" Y="1.195375366211" />
                  <Point X="28.056916015625" Y="1.183530029297" />
                  <Point X="28.07018359375" Y="1.163362548828" />
                  <Point X="28.10497265625" Y="1.11048449707" />
                  <Point X="28.111734375" Y="1.101428344727" />
                  <Point X="28.1262890625" Y="1.084181274414" />
                  <Point X="28.13408203125" Y="1.075990600586" />
                  <Point X="28.151326171875" Y="1.059900512695" />
                  <Point X="28.16003515625" Y="1.052694458008" />
                  <Point X="28.17824609375" Y="1.039368286133" />
                  <Point X="28.187748046875" Y="1.033247924805" />
                  <Point X="28.2069765625" Y="1.022424560547" />
                  <Point X="28.257390625" Y="0.994045654297" />
                  <Point X="28.265484375" Y="0.989985229492" />
                  <Point X="28.294681640625" Y="0.978083374023" />
                  <Point X="28.33027734375" Y="0.968020996094" />
                  <Point X="28.343671875" Y="0.9652578125" />
                  <Point X="28.369669921875" Y="0.961821655273" />
                  <Point X="28.437833984375" Y="0.952813049316" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797302246" />
                  <Point X="29.526865234375" Y="1.087906860352" />
                  <Point X="29.704703125" Y="1.111319824219" />
                  <Point X="29.752689453125" Y="0.91420703125" />
                  <Point X="29.769322265625" Y="0.807376342773" />
                  <Point X="29.783873046875" Y="0.713921264648" />
                  <Point X="29.050927734375" Y="0.517529052734" />
                  <Point X="28.6919921875" Y="0.421352813721" />
                  <Point X="28.686033203125" Y="0.419544647217" />
                  <Point X="28.665623046875" Y="0.412136260986" />
                  <Point X="28.642380859375" Y="0.401618499756" />
                  <Point X="28.634005859375" Y="0.397316467285" />
                  <Point X="28.60846484375" Y="0.382553161621" />
                  <Point X="28.54149609375" Y="0.343843902588" />
                  <Point X="28.533884765625" Y="0.338946472168" />
                  <Point X="28.50877734375" Y="0.319872497559" />
                  <Point X="28.481994140625" Y="0.294350097656" />
                  <Point X="28.472796875" Y="0.284224578857" />
                  <Point X="28.45747265625" Y="0.26469744873" />
                  <Point X="28.417291015625" Y="0.213497024536" />
                  <Point X="28.41085546875" Y="0.204209442139" />
                  <Point X="28.399130859375" Y="0.184929626465" />
                  <Point X="28.393841796875" Y="0.174937271118" />
                  <Point X="28.384068359375" Y="0.153473892212" />
                  <Point X="28.38000390625" Y="0.142923904419" />
                  <Point X="28.373158203125" Y="0.12142099762" />
                  <Point X="28.370376953125" Y="0.110467933655" />
                  <Point X="28.36526953125" Y="0.08379473877" />
                  <Point X="28.351875" Y="0.013857274055" />
                  <Point X="28.35060546875" Y="0.004965464592" />
                  <Point X="28.3485859375" Y="-0.026261388779" />
                  <Point X="28.35028125" Y="-0.062937797546" />
                  <Point X="28.351875" Y="-0.076417449951" />
                  <Point X="28.356982421875" Y="-0.103090797424" />
                  <Point X="28.370376953125" Y="-0.17302796936" />
                  <Point X="28.373158203125" Y="-0.183981018066" />
                  <Point X="28.38000390625" Y="-0.205483932495" />
                  <Point X="28.384068359375" Y="-0.216033920288" />
                  <Point X="28.393841796875" Y="-0.237497436523" />
                  <Point X="28.399130859375" Y="-0.247489212036" />
                  <Point X="28.41085546875" Y="-0.266769317627" />
                  <Point X="28.417291015625" Y="-0.276057800293" />
                  <Point X="28.432615234375" Y="-0.295584899902" />
                  <Point X="28.472796875" Y="-0.346785339355" />
                  <Point X="28.47871875" Y="-0.35363269043" />
                  <Point X="28.50114453125" Y="-0.37580871582" />
                  <Point X="28.5301796875" Y="-0.398725769043" />
                  <Point X="28.54149609375" Y="-0.406403778076" />
                  <Point X="28.567037109375" Y="-0.42116708374" />
                  <Point X="28.634005859375" Y="-0.459876190186" />
                  <Point X="28.6394921875" Y="-0.462812255859" />
                  <Point X="28.65915625" Y="-0.472016174316" />
                  <Point X="28.68302734375" Y="-0.481027618408" />
                  <Point X="28.6919921875" Y="-0.483912689209" />
                  <Point X="29.633119140625" Y="-0.736087219238" />
                  <Point X="29.784876953125" Y="-0.776750793457" />
                  <Point X="29.76161328125" Y="-0.931063415527" />
                  <Point X="29.74030078125" Y="-1.024458374023" />
                  <Point X="29.7278046875" Y="-1.079219726563" />
                  <Point X="28.853046875" Y="-0.964055786133" />
                  <Point X="28.436783203125" Y="-0.909253662109" />
                  <Point X="28.428625" Y="-0.908535644531" />
                  <Point X="28.40009765625" Y="-0.908042541504" />
                  <Point X="28.36672265625" Y="-0.910840942383" />
                  <Point X="28.354482421875" Y="-0.912676147461" />
                  <Point X="28.304353515625" Y="-0.923571960449" />
                  <Point X="28.17291796875" Y="-0.952140136719" />
                  <Point X="28.157875" Y="-0.956742370605" />
                  <Point X="28.12875390625" Y="-0.96836706543" />
                  <Point X="28.11467578125" Y="-0.975389343262" />
                  <Point X="28.086849609375" Y="-0.99228137207" />
                  <Point X="28.074125" Y="-1.001531066895" />
                  <Point X="28.050375" Y="-1.022002380371" />
                  <Point X="28.039349609375" Y="-1.033223999023" />
                  <Point X="28.00905078125" Y="-1.069664672852" />
                  <Point X="27.92960546875" Y="-1.165211914063" />
                  <Point X="27.921326171875" Y="-1.176849853516" />
                  <Point X="27.906607421875" Y="-1.20123046875" />
                  <Point X="27.90016796875" Y="-1.213973022461" />
                  <Point X="27.88882421875" Y="-1.241358276367" />
                  <Point X="27.884365234375" Y="-1.254928100586" />
                  <Point X="27.877533203125" Y="-1.282580810547" />
                  <Point X="27.87516015625" Y="-1.296663452148" />
                  <Point X="27.870818359375" Y="-1.343855712891" />
                  <Point X="27.859431640625" Y="-1.46759387207" />
                  <Point X="27.859291015625" Y="-1.483316772461" />
                  <Point X="27.861607421875" Y="-1.514580322266" />
                  <Point X="27.864064453125" Y="-1.530120849609" />
                  <Point X="27.871794921875" Y="-1.561742431641" />
                  <Point X="27.87678515625" Y="-1.576662841797" />
                  <Point X="27.88915625" Y="-1.605475708008" />
                  <Point X="27.896537109375" Y="-1.619368286133" />
                  <Point X="27.92427734375" Y="-1.662518554688" />
                  <Point X="27.997017578125" Y="-1.775658691406" />
                  <Point X="28.001744140625" Y="-1.782354736328" />
                  <Point X="28.019802734375" Y="-1.804458251953" />
                  <Point X="28.04349609375" Y="-1.828123779297" />
                  <Point X="28.052798828125" Y="-1.836277709961" />
                  <Point X="28.926169921875" Y="-2.506439697266" />
                  <Point X="29.087171875" Y="-2.629981201172" />
                  <Point X="29.04549609375" Y="-2.697419677734" />
                  <Point X="29.00140625" Y="-2.760065673828" />
                  <Point X="29.001275390625" Y="-2.760251953125" />
                  <Point X="28.2191953125" Y="-2.308717041016" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.711451171875" Y="-2.055562988281" />
                  <Point X="27.555021484375" Y="-2.027311889648" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503295898" />
                  <Point X="27.460140625" Y="-2.03146105957" />
                  <Point X="27.44484375" Y="-2.03513659668" />
                  <Point X="27.415068359375" Y="-2.044960571289" />
                  <Point X="27.40058984375" Y="-2.051109130859" />
                  <Point X="27.35102734375" Y="-2.077193847656" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.20896875" Y="-2.153170654297" />
                  <Point X="27.186037109375" Y="-2.170064453125" />
                  <Point X="27.175208984375" Y="-2.179375732422" />
                  <Point X="27.15425" Y="-2.200335449219" />
                  <Point X="27.144939453125" Y="-2.2111640625" />
                  <Point X="27.128048828125" Y="-2.234092285156" />
                  <Point X="27.12046875" Y="-2.246191894531" />
                  <Point X="27.0943828125" Y="-2.295755126953" />
                  <Point X="27.02598828125" Y="-2.425709960938" />
                  <Point X="27.01983984375" Y="-2.440187988281" />
                  <Point X="27.010013671875" Y="-2.469967529297" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517442138672" />
                  <Point X="27.000279296875" Y="-2.533133544922" />
                  <Point X="27.00068359375" Y="-2.564484863281" />
                  <Point X="27.0021875" Y="-2.580144775391" />
                  <Point X="27.012962890625" Y="-2.639805175781" />
                  <Point X="27.04121484375" Y="-2.796235107422" />
                  <Point X="27.04301953125" Y="-2.804231445312" />
                  <Point X="27.05123828125" Y="-2.831540771484" />
                  <Point X="27.064072265625" Y="-2.862479248047" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.630779296875" Y="-3.845656005859" />
                  <Point X="27.735896484375" Y="-4.027722412109" />
                  <Point X="27.723755859375" Y="-4.036082763672" />
                  <Point X="27.11682421875" Y="-3.245115478516" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.714458984375" Y="-2.782817382812" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.34404296875" Y="-2.652438232422" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079876953125" Y="-2.699413085938" />
                  <Point X="26.055494140625" Y="-2.714134033203" />
                  <Point X="26.043857421875" Y="-2.722413330078" />
                  <Point X="25.994166015625" Y="-2.76373046875" />
                  <Point X="25.863873046875" Y="-2.872063720703" />
                  <Point X="25.852650390625" Y="-2.883091064453" />
                  <Point X="25.832181640625" Y="-2.906840576172" />
                  <Point X="25.822935546875" Y="-2.919562744141" />
                  <Point X="25.80604296875" Y="-2.947389404297" />
                  <Point X="25.79901953125" Y="-2.961466796875" />
                  <Point X="25.78739453125" Y="-2.990587890625" />
                  <Point X="25.78279296875" Y="-3.005631591797" />
                  <Point X="25.767935546875" Y="-3.07398828125" />
                  <Point X="25.728978515625" Y="-3.253219482422" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520263672" />
                  <Point X="25.83308984375" Y="-4.152326660156" />
                  <Point X="25.7656640625" Y="-3.900689208984" />
                  <Point X="25.655068359375" Y="-3.487936767578" />
                  <Point X="25.652607421875" Y="-3.480122558594" />
                  <Point X="25.64214453125" Y="-3.453573974609" />
                  <Point X="25.62678515625" Y="-3.423810546875" />
                  <Point X="25.62040625" Y="-3.413208007812" />
                  <Point X="25.575201171875" Y="-3.348078125" />
                  <Point X="25.456677734375" Y="-3.177307617188" />
                  <Point X="25.446669921875" Y="-3.165172119141" />
                  <Point X="25.424787109375" Y="-3.142717773438" />
                  <Point X="25.412912109375" Y="-3.132398925781" />
                  <Point X="25.38665625" Y="-3.113154785156" />
                  <Point X="25.3732421875" Y="-3.104937988281" />
                  <Point X="25.3452421875" Y="-3.090830078125" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.26070703125" Y="-3.063229003906" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.86506640625" Y="-3.028014892578" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.667068359375" Y="-3.090830078125" />
                  <Point X="24.639068359375" Y="-3.104938232422" />
                  <Point X="24.62565625" Y="-3.113154785156" />
                  <Point X="24.599400390625" Y="-3.132398925781" />
                  <Point X="24.587525390625" Y="-3.142717041016" />
                  <Point X="24.565640625" Y="-3.165172851562" />
                  <Point X="24.555630859375" Y="-3.177310546875" />
                  <Point X="24.510427734375" Y="-3.242440185547" />
                  <Point X="24.391904296875" Y="-3.4132109375" />
                  <Point X="24.38753125" Y="-3.420130371094" />
                  <Point X="24.374025390625" Y="-3.445260742188" />
                  <Point X="24.36122265625" Y="-3.476213623047" />
                  <Point X="24.35724609375" Y="-3.487936279297" />
                  <Point X="24.06726171875" Y="-4.570173828125" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.083981722889" Y="2.442877856724" />
                  <Point X="28.857325250357" Y="2.732984912122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.008610379363" Y="2.385043198855" />
                  <Point X="28.774244250714" Y="2.685018164089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.933239035837" Y="2.327208540987" />
                  <Point X="28.691163269979" Y="2.637051391854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.768981899323" Y="3.817389720589" />
                  <Point X="27.572280560472" Y="4.069155953293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.857867692312" Y="2.269373883119" />
                  <Point X="28.608082332619" Y="2.589084564103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.71775148419" Y="3.728656083432" />
                  <Point X="27.351002126644" Y="4.198073854735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.733047777237" Y="0.994888878436" />
                  <Point X="29.647922360536" Y="1.10384444323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.782496348786" Y="2.211539225251" />
                  <Point X="28.525001395258" Y="2.541117736351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.666521069057" Y="3.639922446274" />
                  <Point X="27.146808245573" Y="4.305124525837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.77189331977" Y="0.790863273002" />
                  <Point X="29.53860943846" Y="1.08945302481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.70712500526" Y="2.153704567383" />
                  <Point X="28.441920457898" Y="2.493150908599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.615290653924" Y="3.551188809117" />
                  <Point X="26.956840909725" Y="4.393966049425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.723987096789" Y="0.697874863915" />
                  <Point X="29.429296446702" Y="1.07506169558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.631753661735" Y="2.095869909515" />
                  <Point X="28.358839520538" Y="2.445184080847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.564060238791" Y="3.462455171959" />
                  <Point X="26.777330101633" Y="4.469423827809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.624299469427" Y="0.67116363007" />
                  <Point X="29.319983446556" Y="1.060670377086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.556382318209" Y="2.038035251647" />
                  <Point X="28.275758583178" Y="2.397217253095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.512829823658" Y="3.373721534802" />
                  <Point X="26.609100633709" Y="4.530442149246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.524611842065" Y="0.644452396224" />
                  <Point X="29.21067044641" Y="1.046279058592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.481010964748" Y="1.980200606495" />
                  <Point X="28.192677645817" Y="2.349250425344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.461599408525" Y="3.284987897644" />
                  <Point X="26.442358054877" Y="4.589557339431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.424924214703" Y="0.617741162379" />
                  <Point X="29.101357446264" Y="1.031887740098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.405639475629" Y="1.922366134978" />
                  <Point X="28.109596708457" Y="2.301283597592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.410368993392" Y="3.196254260487" />
                  <Point X="26.293774506641" Y="4.625430030356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.325236587341" Y="0.591029928533" />
                  <Point X="28.992044446118" Y="1.017496421604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.330267986511" Y="1.864531663461" />
                  <Point X="28.026515771097" Y="2.25331676984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.359138578259" Y="3.107520623329" />
                  <Point X="26.145190958405" Y="4.661302721281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.225548959979" Y="0.564318694688" />
                  <Point X="28.882731445972" Y="1.003105103109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.254896497392" Y="1.806697191943" />
                  <Point X="27.943434833737" Y="2.205349942088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.307908163126" Y="3.018786986171" />
                  <Point X="25.99660741017" Y="4.697175412206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.125861332616" Y="0.537607460842" />
                  <Point X="28.773418445826" Y="0.988713784615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.179525008273" Y="1.748862720426" />
                  <Point X="27.860353896376" Y="2.157383114336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.256677831323" Y="2.930053242356" />
                  <Point X="25.848023534545" Y="4.733048522171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.026173691733" Y="0.510896244303" />
                  <Point X="28.664105445681" Y="0.974322466121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.11323445317" Y="1.679405183403" />
                  <Point X="27.777272959016" Y="2.109416286584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.205447507956" Y="2.841319487744" />
                  <Point X="25.714380300546" Y="4.749798482906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.926486009921" Y="0.484185080151" />
                  <Point X="28.554792445535" Y="0.959931147627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.055419584466" Y="1.599099262496" />
                  <Point X="27.694192021656" Y="2.061449458833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.154217184588" Y="2.752585733132" />
                  <Point X="25.58308038312" Y="4.763549135202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.826798328109" Y="0.457473915998" />
                  <Point X="28.439961397933" Y="0.952602607799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.016647120263" Y="1.494420175291" />
                  <Point X="27.609601105575" Y="2.015415315709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.10298686122" Y="2.663851978521" />
                  <Point X="25.451780465694" Y="4.777299787498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.766380435828" Y="-0.899441918856" />
                  <Point X="29.640245665245" Y="-0.73799677472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.727110646296" Y="0.430762751846" />
                  <Point X="28.300861861537" Y="0.97633631713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.004195130774" Y="1.35605241672" />
                  <Point X="27.511887691276" Y="1.986177204373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.053146352145" Y="2.573339342735" />
                  <Point X="25.36466093285" Y="4.734502126242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.741023358639" Y="-1.021291918412" />
                  <Point X="29.487768600098" Y="-0.697140609405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.633095938704" Y="0.396790511811" />
                  <Point X="27.398499453061" Y="1.977001952744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.020102927239" Y="2.461327419622" />
                  <Point X="25.333873498075" Y="4.619602667438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.658607277604" Y="-1.070109723454" />
                  <Point X="29.335291541394" Y="-0.656284452336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.550044972243" Y="0.348785323057" />
                  <Point X="27.248727841582" Y="2.014395295277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.021111977076" Y="2.305730316405" />
                  <Point X="25.3030860633" Y="4.504703208634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.524228612235" Y="-1.05241845349" />
                  <Point X="29.182814482689" Y="-0.615428295266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.476630862778" Y="0.288445519831" />
                  <Point X="25.272298628525" Y="4.38980374983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.389849946866" Y="-1.034727183527" />
                  <Point X="29.030337423985" Y="-0.574572138197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.416040408308" Y="0.211692186699" />
                  <Point X="25.24151119375" Y="4.274904291026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.255471281497" Y="-1.017035913563" />
                  <Point X="28.87786036528" Y="-0.533715981128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.371405145289" Y="0.114517139781" />
                  <Point X="25.195314797552" Y="4.179727403456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.121092616128" Y="-0.9993446436" />
                  <Point X="28.725383306576" Y="-0.492859824059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.349522495832" Y="-0.011779924477" />
                  <Point X="25.123892086344" Y="4.116838726694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.650959121874" Y="4.722165317155" />
                  <Point X="24.606373772431" Y="4.779231962094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.986713950759" Y="-0.981653373637" />
                  <Point X="28.532523339238" Y="-0.400315901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.37979757925" Y="-0.204835842484" />
                  <Point X="25.027380542834" Y="4.086062290899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.713887424178" Y="4.487315184874" />
                  <Point X="24.495916904916" Y="4.766304727067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.852335285353" Y="-0.963962103626" />
                  <Point X="24.878733677815" Y="4.122016023611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.778349002945" Y="4.250502548211" />
                  <Point X="24.385460037401" Y="4.753377492041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.7179566131" Y="-0.946270824852" />
                  <Point X="24.275003169885" Y="4.740450257014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.583577940847" Y="-0.928579546077" />
                  <Point X="24.164546368382" Y="4.727522937497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.449199268594" Y="-0.910888267302" />
                  <Point X="24.055632163139" Y="4.712621184804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.333587668273" Y="-0.917217745208" />
                  <Point X="23.95673771873" Y="4.684894723075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.230531568808" Y="-0.939617531372" />
                  <Point X="23.85784327432" Y="4.657168261346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.131560975144" Y="-0.967246526499" />
                  <Point X="23.758948829911" Y="4.629441799617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.052412257599" Y="-1.0202463661" />
                  <Point X="23.660054385502" Y="4.601715337888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.989157647338" Y="-1.093589735319" />
                  <Point X="23.561159941093" Y="4.573988876158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.079129314025" Y="-2.642995427745" />
                  <Point X="29.041702364579" Y="-2.595091116981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.927200023158" Y="-1.16859317102" />
                  <Point X="23.53518081151" Y="4.452935067359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.024453256014" Y="-2.727318843132" />
                  <Point X="28.74068481967" Y="-2.364111807553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.881897896904" Y="-1.264914671923" />
                  <Point X="23.520407280886" Y="4.317538745939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.870749616248" Y="-2.684892733897" />
                  <Point X="28.439667578534" Y="-2.133132886936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.86578137485" Y="-1.3985920427" />
                  <Point X="23.483764576252" Y="4.210133690795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.651125803247" Y="-2.558092650537" />
                  <Point X="28.138650337397" Y="-1.902153966318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.871386527523" Y="-1.560071889282" />
                  <Point X="23.425165011995" Y="4.130832134395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.431501990246" Y="-2.431292567177" />
                  <Point X="23.343085514111" Y="4.081583522565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.211878203979" Y="-2.304492518034" />
                  <Point X="23.251740222875" Y="4.044194585403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.992255193396" Y="-2.177693461722" />
                  <Point X="23.142002409147" Y="4.030347003498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.788332367109" Y="-2.070989724925" />
                  <Point X="22.964388757144" Y="4.103376532822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.646567947525" Y="-2.043845120656" />
                  <Point X="22.742978412875" Y="4.232463271929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.5116393946" Y="-2.025450026716" />
                  <Point X="22.658911878801" Y="4.185757950443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.408503955236" Y="-2.047748262441" />
                  <Point X="22.574845344728" Y="4.139052628957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.322629416141" Y="-2.092139443028" />
                  <Point X="22.490778856157" Y="4.092347249231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.237199936885" Y="-2.137100274233" />
                  <Point X="22.406712472203" Y="4.0456417356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.16087317079" Y="-2.193712046979" />
                  <Point X="22.3256358572" Y="3.995109492219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.104710454482" Y="-2.276132626519" />
                  <Point X="22.250240891193" Y="3.937305069748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.056186708537" Y="-2.368330642256" />
                  <Point X="22.174845925186" Y="3.879500647277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.011505374977" Y="-2.465446721572" />
                  <Point X="22.099450959179" Y="3.821696224806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.008690338053" Y="-2.616149216936" />
                  <Point X="22.024055993172" Y="3.763891802336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.04799426427" Y="-2.820761526732" />
                  <Point X="21.948661027165" Y="3.706087379865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.355054877191" Y="-3.368086767137" />
                  <Point X="22.227965438876" Y="3.19428845694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.696357905634" Y="-3.959240300755" />
                  <Point X="22.250884334576" Y="3.01064802985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.598366739855" Y="-2.708181274215" />
                  <Point X="22.216684186686" Y="2.900116644639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.429854956822" Y="-2.646801605917" />
                  <Point X="22.154038326149" Y="2.825994111304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.315737083524" Y="-2.655042967227" />
                  <Point X="22.082567240426" Y="2.763167351098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.203266394522" Y="-2.665392628292" />
                  <Point X="21.990265906986" Y="2.727002092155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.10141226269" Y="-2.68933086287" />
                  <Point X="21.868042215755" Y="2.72913570468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.021334020253" Y="-2.741140964863" />
                  <Point X="21.701491659137" Y="2.78800511764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.948252125037" Y="-2.801905982938" />
                  <Point X="21.481867860907" Y="2.914805182094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.875170029046" Y="-2.862670744032" />
                  <Point X="21.270534739109" Y="3.030993664624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.812496781429" Y="-2.936758223503" />
                  <Point X="21.21038344096" Y="2.953678237034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.774730600635" Y="-3.042725294737" />
                  <Point X="21.150232142812" Y="2.876362809444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.748491320478" Y="-3.163446125986" />
                  <Point X="21.090080963163" Y="2.79904723018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.724933996462" Y="-3.287599704556" />
                  <Point X="21.029930251131" Y="2.721731052396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.742524813202" Y="-3.464420501566" />
                  <Point X="20.973773043473" Y="2.639303422104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.766956592176" Y="-3.649997330945" />
                  <Point X="25.662755579" Y="-3.516626116064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.323829135418" Y="-3.082820050672" />
                  <Point X="20.922220603095" Y="2.550981958464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.79138837115" Y="-3.835574160323" />
                  <Point X="25.725682425162" Y="-3.75147438457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.164682004897" Y="-3.033426590996" />
                  <Point X="21.575408645466" Y="1.560633811063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.346258289279" Y="1.853932891978" />
                  <Point X="20.870668162717" Y="2.462660494825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.815820150125" Y="-4.021150989702" />
                  <Point X="25.7886094407" Y="-3.986322869867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.01626463728" Y="-2.997766601562" />
                  <Point X="21.559469794808" Y="1.426729031268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.045241311246" Y="2.084911475838" />
                  <Point X="20.819115722339" Y="2.374339031185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.908748339759" Y="-3.014457594547" />
                  <Point X="21.513783054881" Y="1.330899813418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.811719396689" Y="-3.044571789105" />
                  <Point X="21.446097096194" Y="1.263228311537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.714690405829" Y="-3.074685922494" />
                  <Point X="21.355964028031" Y="1.224287799594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.624723166396" Y="-3.113838685532" />
                  <Point X="21.250938983547" Y="1.204408148132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.554748902161" Y="-3.178581289875" />
                  <Point X="21.118842669512" Y="1.219178141604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.498034809543" Y="-3.260296139923" />
                  <Point X="20.984463996273" Y="1.23686942164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.441320665201" Y="-3.342010923766" />
                  <Point X="20.850085325461" Y="1.25456069857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.385194524975" Y="-3.424478318558" />
                  <Point X="21.702150582975" Y="0.009661323812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.524639294428" Y="0.236865412208" />
                  <Point X="20.715706656392" Y="1.272251973269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.34625740715" Y="-3.528946658737" />
                  <Point X="21.688117121693" Y="-0.126682243171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.368285581888" Y="0.282683459914" />
                  <Point X="20.581327987323" Y="1.289943247969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.315470158282" Y="-3.64384635549" />
                  <Point X="21.63822264487" Y="-0.217125803389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.215808548744" Y="0.323539584268" />
                  <Point X="20.446949318254" Y="1.307634522668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.284682909413" Y="-3.758746052243" />
                  <Point X="21.565039993208" Y="-0.277761859093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.063331515599" Y="0.364395708623" />
                  <Point X="20.353974543505" Y="1.272331229292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.253895660545" Y="-3.873645748997" />
                  <Point X="21.47488883872" Y="-0.316679221595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.910854482454" Y="0.405251832977" />
                  <Point X="20.322928851869" Y="1.157762324197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.223108411676" Y="-3.98854544575" />
                  <Point X="21.375201187927" Y="-0.343390425451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.758377449309" Y="0.446107957332" />
                  <Point X="20.291883134183" Y="1.043193452443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.192321162807" Y="-4.103445142503" />
                  <Point X="22.052477896509" Y="-1.3645706596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.853427855943" Y="-1.10979822579" />
                  <Point X="21.275513537135" Y="-0.370101629306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.605900416164" Y="0.486964081687" />
                  <Point X="20.263825921277" Y="0.924799469004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.161533913939" Y="-4.218344839257" />
                  <Point X="22.023486043084" Y="-1.481768357728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.728195620833" Y="-1.103813852701" />
                  <Point X="21.175825886342" Y="-0.396812833162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.453423383019" Y="0.527820206041" />
                  <Point X="20.244628744164" Y="0.79506515689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.13074666507" Y="-4.33324453601" />
                  <Point X="21.96532504752" Y="-1.561631256456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.618882634146" Y="-1.118205188422" />
                  <Point X="21.076138238268" Y="-0.423524040498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.300946298409" Y="0.568676396269" />
                  <Point X="20.225431567052" Y="0.665330844776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.099959416202" Y="-4.448144232763" />
                  <Point X="22.685881169942" Y="-2.638206614197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.460349021704" Y="-2.349538628268" />
                  <Point X="21.890897124303" Y="-1.620673437254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.509569647459" Y="-1.132596524143" />
                  <Point X="20.976450633258" Y="-0.450235302953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.069172167333" Y="-4.563043929516" />
                  <Point X="23.828352792036" Y="-4.254809185234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.538836573091" Y="-3.884245323412" />
                  <Point X="22.647434458217" Y="-2.74330264556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.350699973699" Y="-2.363499825118" />
                  <Point X="21.815525693625" Y="-1.678507983572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.400256660773" Y="-1.146987859864" />
                  <Point X="20.876763028249" Y="-0.476946565409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.038384087738" Y="-4.677942562988" />
                  <Point X="23.869531046618" Y="-4.461820525936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.348727223343" Y="-3.795222030321" />
                  <Point X="22.59620409098" Y="-2.832036344022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.264303786668" Y="-2.407223326795" />
                  <Point X="21.740154262946" Y="-1.736342529889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.290943674086" Y="-1.161379195585" />
                  <Point X="20.777075423239" Y="-0.503657827864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.982371857615" Y="-4.760555756063" />
                  <Point X="23.873288768778" Y="-4.620935769291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.221663829691" Y="-3.786893881179" />
                  <Point X="22.544973723744" Y="-2.920770042484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.181222785334" Y="-2.455190072665" />
                  <Point X="21.664782832267" Y="-1.794177076206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.181630687399" Y="-1.175770531306" />
                  <Point X="20.67738781823" Y="-0.53036909032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.094600452034" Y="-3.778565752509" />
                  <Point X="22.493743356507" Y="-3.009503740946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.098141784001" Y="-2.503156818534" />
                  <Point X="21.589411400719" Y="-1.85201162141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.072317700712" Y="-1.190161867027" />
                  <Point X="20.57770021322" Y="-0.557080352776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.979334555748" Y="-3.785337711401" />
                  <Point X="22.442512980569" Y="-3.098237428271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.015060782667" Y="-2.551123564404" />
                  <Point X="21.514039947061" Y="-1.909846138315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.963004714025" Y="-1.204553202748" />
                  <Point X="20.478012608211" Y="-0.583791615231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.890124869836" Y="-3.825460098729" />
                  <Point X="22.391282570741" Y="-3.186971072219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.931979781334" Y="-2.599090310274" />
                  <Point X="21.438668493403" Y="-1.96768065522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.853691727339" Y="-1.218944538469" />
                  <Point X="20.378325003201" Y="-0.610502877687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.8109178808" Y="-3.878385354222" />
                  <Point X="22.340052160913" Y="-3.275704716166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.84889878" Y="-2.647057056143" />
                  <Point X="21.363297039744" Y="-2.025515172125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.744378740652" Y="-1.23333587419" />
                  <Point X="20.278637398192" Y="-0.637214140142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.731710594094" Y="-3.931310228715" />
                  <Point X="22.288821751084" Y="-3.364438360113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.765817778666" Y="-2.695023802013" />
                  <Point X="21.287925586086" Y="-2.08334968903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.635065753965" Y="-1.247727209911" />
                  <Point X="20.222924721903" Y="-0.720210744639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.652503307388" Y="-3.984235103208" />
                  <Point X="22.237591341256" Y="-3.453172004061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.682736777333" Y="-2.742990547883" />
                  <Point X="21.212554132428" Y="-2.141184205935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.525752781851" Y="-1.262118564284" />
                  <Point X="20.249938460761" Y="-0.909092331966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.583216025717" Y="-4.049857005136" />
                  <Point X="22.186360931428" Y="-3.541905648008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.599655775999" Y="-2.790957293752" />
                  <Point X="21.13718267877" Y="-2.19901872284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.416439826048" Y="-1.276509939534" />
                  <Point X="20.297637799144" Y="-1.124450279312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.523480971377" Y="-4.127705200506" />
                  <Point X="22.1351305216" Y="-3.630639291955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.516574774665" Y="-2.838924039622" />
                  <Point X="21.061811225112" Y="-2.256853239745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.36758351547" Y="-4.082471134659" />
                  <Point X="22.083900111771" Y="-3.719372935903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.433493773332" Y="-2.886890785492" />
                  <Point X="20.986439771454" Y="-2.314687756649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.099064886038" Y="-3.89308854015" />
                  <Point X="22.032669701943" Y="-3.80810657985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.35041278072" Y="-2.934857542525" />
                  <Point X="20.911068317796" Y="-2.372522273554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.267331803954" Y="-2.982824319839" />
                  <Point X="20.835696864138" Y="-2.430356790459" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.58213671875" Y="-3.949864746094" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544433594" />
                  <Point X="25.419115234375" Y="-3.456414550781" />
                  <Point X="25.300591796875" Y="-3.285644042969" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.20438671875" Y="-3.244689941406" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.92138671875" Y="-3.209476318359" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285644042969" />
                  <Point X="24.666517578125" Y="-3.350773681641" />
                  <Point X="24.547994140625" Y="-3.521544433594" />
                  <Point X="24.5407734375" Y="-3.537112060547" />
                  <Point X="24.2507890625" Y="-4.619349609375" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.11526171875" Y="-4.979895996094" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.81957421875" Y="-4.917435546875" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.677537109375" Y="-4.652163085938" />
                  <Point X="23.6908515625" Y="-4.5510390625" />
                  <Point X="23.690318359375" Y="-4.5347578125" />
                  <Point X="23.67360546875" Y="-4.450745605469" />
                  <Point X="23.6297890625" Y="-4.230466308594" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.54931640625" Y="-4.146150390625" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.26528515625" Y="-3.980160644531" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.938900390625" Y="-4.021380126953" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.577462890625" Y="-4.369464355469" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.46005859375" Y="-4.363203125" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="22.033140625" Y="-4.082124023438" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.288435546875" Y="-2.985110595703" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597593505859" />
                  <Point X="22.486021484375" Y="-2.568764648438" />
                  <Point X="22.468673828125" Y="-2.551416015625" />
                  <Point X="22.43984375" Y="-2.537198730469" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.474908203125" Y="-3.082373291016" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.0878671875" Y="-3.175014892578" />
                  <Point X="20.838302734375" Y="-2.847136962891" />
                  <Point X="20.758701171875" Y="-2.713656738281" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.476599609375" Y="-1.6990859375" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.84746484375" Y="-1.41108215332" />
                  <Point X="21.8563046875" Y="-1.387802124023" />
                  <Point X="21.8618828125" Y="-1.366267700195" />
                  <Point X="21.863349609375" Y="-1.350052001953" />
                  <Point X="21.85134375" Y="-1.321068359375" />
                  <Point X="21.83153125" Y="-1.306335327148" />
                  <Point X="21.812359375" Y="-1.295052124023" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.6021328125" Y="-1.443702392578" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.170216796875" Y="-1.393326782227" />
                  <Point X="20.072607421875" Y="-1.011188110352" />
                  <Point X="20.051548828125" Y="-0.863946960449" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="21.0328828125" Y="-0.238411727905" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.465765625" Y="-0.116107826233" />
                  <Point X="21.485857421875" Y="-0.102163780212" />
                  <Point X="21.4976796875" Y="-0.090642791748" />
                  <Point X="21.50765625" Y="-0.06767074585" />
                  <Point X="21.5143515625" Y="-0.046101638794" />
                  <Point X="21.516599609375" Y="-0.031284263611" />
                  <Point X="21.511798828125" Y="-0.008229101181" />
                  <Point X="21.5051015625" Y="0.013348438263" />
                  <Point X="21.49767578125" Y="0.028086112976" />
                  <Point X="21.4781953125" Y="0.044921691895" />
                  <Point X="21.458103515625" Y="0.058865730286" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.36842578125" Y="0.353892791748" />
                  <Point X="20.001814453125" Y="0.452126098633" />
                  <Point X="20.019447265625" Y="0.571293884277" />
                  <Point X="20.08235546875" Y="0.996414794922" />
                  <Point X="20.124748046875" Y="1.152860351562" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.9530859375" Y="1.432640014648" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866699219" />
                  <Point X="21.285255859375" Y="1.401213867188" />
                  <Point X="21.32972265625" Y="1.41523425293" />
                  <Point X="21.348466796875" Y="1.426056396484" />
                  <Point X="21.360880859375" Y="1.443786499023" />
                  <Point X="21.367685546875" Y="1.460215087891" />
                  <Point X="21.38552734375" Y="1.503290771484" />
                  <Point X="21.389287109375" Y="1.524605712891" />
                  <Point X="21.38368359375" Y="1.545511962891" />
                  <Point X="21.37547265625" Y="1.561284912109" />
                  <Point X="21.353943359375" Y="1.602641601562" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.72391796875" Y="2.091982177734" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.595548828125" Y="2.368227783203" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="20.95228125" Y="2.931346435547" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.6604921875" Y="3.031069335938" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.88510546875" Y="2.918368408203" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404296875" />
                  <Point X="22.003513671875" Y="2.944169433594" />
                  <Point X="22.04747265625" Y="2.988127685547" />
                  <Point X="22.0591015625" Y="3.006382080078" />
                  <Point X="22.061927734375" Y="3.027841064453" />
                  <Point X="22.059861328125" Y="3.051460449219" />
                  <Point X="22.054443359375" Y="3.113390625" />
                  <Point X="22.04793359375" Y="3.134032714844" />
                  <Point X="21.7749140625" Y="3.606914550781" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.821392578125" Y="3.847927734375" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.423986328125" Y="4.272591796875" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.976962890625" Y="4.359525878906" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.07504296875" Y="4.259975585938" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.21357421875" Y="4.233592285156" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744628906" />
                  <Point X="23.31391796875" Y="4.294488769531" />
                  <Point X="23.322830078125" Y="4.32275390625" />
                  <Point X="23.346197265625" Y="4.396865234375" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.317044921875" Y="4.654192871094" />
                  <Point X="23.31086328125" Y="4.701141113281" />
                  <Point X="23.480771484375" Y="4.74877734375" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.24631640625" Y="4.928389648438" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.9045078125" Y="4.51001953125" />
                  <Point X="24.957859375" Y="4.310903808594" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.194345703125" Y="4.832985839844" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.378986328125" Y="4.975962402344" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.03759375" Y="4.882738769531" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.623818359375" Y="4.727216308594" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.042681640625" Y="4.563572265625" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.446560546875" Y="4.362294433594" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="27.834248046875" Y="4.123352050781" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.4677421875" Y="2.915630859375" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491515380859" />
                  <Point X="27.21892578125" Y="2.469349121094" />
                  <Point X="27.2033828125" Y="2.411229003906" />
                  <Point X="27.204357421875" Y="2.373159912109" />
                  <Point X="27.210416015625" Y="2.322902587891" />
                  <Point X="27.218681640625" Y="2.300812255859" />
                  <Point X="27.23054296875" Y="2.283333251953" />
                  <Point X="27.261640625" Y="2.237503662109" />
                  <Point X="27.27494140625" Y="2.224203613281" />
                  <Point X="27.292419921875" Y="2.212343261719" />
                  <Point X="27.33825" Y="2.18124609375" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.379505859375" Y="2.170668945312" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.470830078125" Y="2.171874023438" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.621384765625" Y="2.816157958984" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.03296875" Y="2.977619140625" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.259302734375" Y="2.64816796875" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.601818359375" Y="1.83341027832" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583832763672" />
                  <Point X="28.26341796875" Y="1.563020751953" />
                  <Point X="28.22158984375" Y="1.508451416016" />
                  <Point X="28.21312109375" Y="1.491500610352" />
                  <Point X="28.207177734375" Y="1.470251586914" />
                  <Point X="28.191595703125" Y="1.414536010742" />
                  <Point X="28.190779296875" Y="1.390965087891" />
                  <Point X="28.195658203125" Y="1.367322631836" />
                  <Point X="28.20844921875" Y="1.305332275391" />
                  <Point X="28.215646484375" Y="1.287955200195" />
                  <Point X="28.2289140625" Y="1.267787963867" />
                  <Point X="28.263703125" Y="1.214909790039" />
                  <Point X="28.280947265625" Y="1.198819946289" />
                  <Point X="28.30017578125" Y="1.187996459961" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.394564453125" Y="1.15018371582" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.502064453125" Y="1.276281494141" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.8663359375" Y="1.250641235352" />
                  <Point X="29.939193359375" Y="0.95136730957" />
                  <Point X="29.957060546875" Y="0.836605163574" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="29.100103515625" Y="0.334003143311" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819351196" />
                  <Point X="28.703546875" Y="0.218056167603" />
                  <Point X="28.636578125" Y="0.179346832275" />
                  <Point X="28.622265625" Y="0.166926849365" />
                  <Point X="28.60694140625" Y="0.147399520874" />
                  <Point X="28.566759765625" Y="0.096199172974" />
                  <Point X="28.556986328125" Y="0.074735717773" />
                  <Point X="28.55187890625" Y="0.04806243515" />
                  <Point X="28.538484375" Y="-0.02187484169" />
                  <Point X="28.538484375" Y="-0.04068523407" />
                  <Point X="28.543591796875" Y="-0.067358520508" />
                  <Point X="28.556986328125" Y="-0.137295791626" />
                  <Point X="28.566759765625" Y="-0.158759246826" />
                  <Point X="28.582083984375" Y="-0.178286407471" />
                  <Point X="28.622265625" Y="-0.229486923218" />
                  <Point X="28.636578125" Y="-0.241906906128" />
                  <Point X="28.662119140625" Y="-0.256670257568" />
                  <Point X="28.729087890625" Y="-0.295379425049" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.682294921875" Y="-0.552561340332" />
                  <Point X="29.998068359375" Y="-0.637172424316" />
                  <Point X="29.989109375" Y="-0.696593688965" />
                  <Point X="29.948431640625" Y="-0.966412414551" />
                  <Point X="29.9255390625" Y="-1.066729858398" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.82824609375" Y="-1.152430297852" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.344708984375" Y="-1.109236938477" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.185447265625" Y="-1.154697143555" />
                  <Point X="28.1551484375" Y="-1.191137817383" />
                  <Point X="28.075703125" Y="-1.286685180664" />
                  <Point X="28.064359375" Y="-1.31407043457" />
                  <Point X="28.060017578125" Y="-1.361262695312" />
                  <Point X="28.048630859375" Y="-1.485000732422" />
                  <Point X="28.056361328125" Y="-1.516622314453" />
                  <Point X="28.0841015625" Y="-1.559772705078" />
                  <Point X="28.156841796875" Y="-1.672912841797" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="29.041833984375" Y="-2.355702636719" />
                  <Point X="29.33907421875" Y="-2.583783691406" />
                  <Point X="29.318798828125" Y="-2.616593261719" />
                  <Point X="29.204134765625" Y="-2.802138183594" />
                  <Point X="29.156783203125" Y="-2.869418457031" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.1241953125" Y="-2.473261962891" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.677681640625" Y="-2.242538085938" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.439515625" Y="-2.245329589844" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.33468359375" />
                  <Point X="27.262515625" Y="-2.384246826172" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.1891640625" Y="-2.546374755859" />
                  <Point X="27.199939453125" Y="-2.60603515625" />
                  <Point X="27.22819140625" Y="-2.762465087891" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.795322265625" Y="-3.750656005859" />
                  <Point X="27.98667578125" Y="-4.082087646484" />
                  <Point X="27.971359375" Y="-4.093027099609" />
                  <Point X="27.83529296875" Y="-4.190215820312" />
                  <Point X="27.782359375" Y="-4.224478515625" />
                  <Point X="27.679775390625" Y="-4.29087890625" />
                  <Point X="26.9660859375" Y="-3.360780029297" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.611708984375" Y="-2.942637695312" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.361453125" Y="-2.841638916016" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.115642578125" Y="-2.909826171875" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045986083984" />
                  <Point X="25.953599609375" Y="-3.114342773438" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310719970703" />
                  <Point X="26.072978515625" Y="-4.518810058594" />
                  <Point X="26.127642578125" Y="-4.934028320312" />
                  <Point X="26.123072265625" Y="-4.935030273438" />
                  <Point X="25.994349609375" Y="-4.96324609375" />
                  <Point X="25.945439453125" Y="-4.972131835938" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#144" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.053352998312" Y="4.554240584169" Z="0.7" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.7" />
                  <Point X="-0.765653893088" Y="5.009330880415" Z="0.7" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.7" />
                  <Point X="-1.53888330275" Y="4.828200972733" Z="0.7" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.7" />
                  <Point X="-1.738685014862" Y="4.678946335746" Z="0.7" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.7" />
                  <Point X="-1.731013149215" Y="4.369069274014" Z="0.7" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.7" />
                  <Point X="-1.811718071487" Y="4.311066246672" Z="0.7" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.7" />
                  <Point X="-1.908026923497" Y="4.335606414254" Z="0.7" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.7" />
                  <Point X="-1.989526285562" Y="4.421243839443" Z="0.7" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.7" />
                  <Point X="-2.606453328131" Y="4.347579569456" Z="0.7" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.7" />
                  <Point X="-3.215185472508" Y="3.918887665597" Z="0.7" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.7" />
                  <Point X="-3.274543206752" Y="3.61319483068" Z="0.7" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.7" />
                  <Point X="-2.996106460286" Y="3.078382924333" Z="0.7" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.7" />
                  <Point X="-3.037998358773" Y="3.010805209114" Z="0.7" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.7" />
                  <Point X="-3.116693541274" Y="2.999458238887" Z="0.7" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.7" />
                  <Point X="-3.320664517617" Y="3.105650741994" Z="0.7" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.7" />
                  <Point X="-4.093338346231" Y="2.993328988567" Z="0.7" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.7" />
                  <Point X="-4.45378924524" Y="2.424712870077" Z="0.7" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.7" />
                  <Point X="-4.312675727185" Y="2.083594536508" Z="0.7" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.7" />
                  <Point X="-3.675033327029" Y="1.569477362828" Z="0.7" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.7" />
                  <Point X="-3.684664972897" Y="1.510628677537" Z="0.7" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.7" />
                  <Point X="-3.735936887104" Y="1.480179739505" Z="0.7" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.7" />
                  <Point X="-4.046545936777" Y="1.513492284786" Z="0.7" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.7" />
                  <Point X="-4.929668189389" Y="1.197217701228" Z="0.7" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.7" />
                  <Point X="-5.036170156351" Y="0.609893027973" Z="0.7" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.7" />
                  <Point X="-4.650673186543" Y="0.336876600282" Z="0.7" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.7" />
                  <Point X="-3.556470166445" Y="0.035125029704" Z="0.7" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.7" />
                  <Point X="-3.542110887939" Y="0.008229417671" Z="0.7" />
                  <Point X="-3.539556741714" Y="0" Z="0.7" />
                  <Point X="-3.546253713196" Y="-0.02157753339" Z="0.7" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.7" />
                  <Point X="-3.568898428678" Y="-0.04375095343" Z="0.7" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.7" />
                  <Point X="-3.98621460223" Y="-0.158835456474" Z="0.7" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.7" />
                  <Point X="-5.004104387898" Y="-0.83974596943" Z="0.7" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.7" />
                  <Point X="-4.884384936162" Y="-1.374420773663" Z="0.7" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.7" />
                  <Point X="-4.3974984588" Y="-1.461994625715" Z="0.7" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.7" />
                  <Point X="-3.199987896567" Y="-1.318146519182" Z="0.7" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.7" />
                  <Point X="-3.19825366347" Y="-1.343984203209" Z="0.7" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.7" />
                  <Point X="-3.559994216577" Y="-1.628138236735" Z="0.7" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.7" />
                  <Point X="-4.290400079136" Y="-2.707986658665" Z="0.7" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.7" />
                  <Point X="-3.958074331378" Y="-3.174017995086" Z="0.7" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.7" />
                  <Point X="-3.506247891613" Y="-3.094394550144" Z="0.7" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.7" />
                  <Point X="-2.560280735316" Y="-2.568049733816" Z="0.7" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.7" />
                  <Point X="-2.761022463025" Y="-2.928830490879" Z="0.7" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.7" />
                  <Point X="-3.003520936032" Y="-4.09046094365" Z="0.7" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.7" />
                  <Point X="-2.572420281254" Y="-4.374434056027" Z="0.7" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.7" />
                  <Point X="-2.389026236767" Y="-4.368622356368" Z="0.7" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.7" />
                  <Point X="-2.039478050199" Y="-4.031673492466" Z="0.7" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.7" />
                  <Point X="-1.744141488809" Y="-3.998773624641" Z="0.7" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.7" />
                  <Point X="-1.489807102284" Y="-4.15246215987" Z="0.7" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.7" />
                  <Point X="-1.381589590865" Y="-4.429220159762" Z="0.7" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.7" />
                  <Point X="-1.378191764104" Y="-4.614356200169" Z="0.7" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.7" />
                  <Point X="-1.199041045831" Y="-4.934578513964" Z="0.7" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.7" />
                  <Point X="-0.900421740017" Y="-4.997700173976" Z="0.7" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.7" />
                  <Point X="-0.707071366691" Y="-4.601010366945" Z="0.7" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.7" />
                  <Point X="-0.298562751171" Y="-3.348002549857" Z="0.7" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.7" />
                  <Point X="-0.069950048917" Y="-3.225949282224" Z="0.7" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.7" />
                  <Point X="0.183409030445" Y="-3.261162763064" Z="0.7" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.7" />
                  <Point X="0.371883108261" Y="-3.453643205911" Z="0.7" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.7" />
                  <Point X="0.52768349371" Y="-3.931525662871" Z="0.7" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.7" />
                  <Point X="0.948219414019" Y="-4.990046538207" Z="0.7" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.7" />
                  <Point X="1.127620944161" Y="-4.952590767458" Z="0.7" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.7" />
                  <Point X="1.116393894333" Y="-4.481003890599" Z="0.7" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.7" />
                  <Point X="0.996302414766" Y="-3.093682633852" Z="0.7" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.7" />
                  <Point X="1.141451941776" Y="-2.91699254984" Z="0.7" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.7" />
                  <Point X="1.359877418474" Y="-2.860148414285" Z="0.7" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.7" />
                  <Point X="1.578512527988" Y="-2.953415706167" Z="0.7" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.7" />
                  <Point X="1.920261894051" Y="-3.35993807436" Z="0.7" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.7" />
                  <Point X="2.803373273275" Y="-4.235172633202" Z="0.7" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.7" />
                  <Point X="2.994265496227" Y="-4.102433867319" Z="0.7" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.7" />
                  <Point X="2.83246630322" Y="-3.694376062426" Z="0.7" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.7" />
                  <Point X="2.242985763225" Y="-2.565868711167" Z="0.7" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.7" />
                  <Point X="2.300605414219" Y="-2.376253416991" Z="0.7" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.7" />
                  <Point X="2.456644961381" Y="-2.258295895748" Z="0.7" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.7" />
                  <Point X="2.662638092345" Y="-2.260462246266" Z="0.7" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.7" />
                  <Point X="3.093037534681" Y="-2.485283197054" Z="0.7" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.7" />
                  <Point X="4.191514831845" Y="-2.866915817686" Z="0.7" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.7" />
                  <Point X="4.355175886266" Y="-2.611598338208" Z="0.7" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.7" />
                  <Point X="4.066114754763" Y="-2.284754971357" Z="0.7" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.7" />
                  <Point X="3.1200041733" Y="-1.501452837403" Z="0.7" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.7" />
                  <Point X="3.103648842307" Y="-1.334564463649" Z="0.7" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.7" />
                  <Point X="3.187436227175" Y="-1.191824836551" Z="0.7" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.7" />
                  <Point X="3.349171663013" Y="-1.126815995452" Z="0.7" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.7" />
                  <Point X="3.815563489917" Y="-1.170722566858" Z="0.7" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.7" />
                  <Point X="4.968127608171" Y="-1.046573803354" Z="0.7" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.7" />
                  <Point X="5.032394817167" Y="-0.672767447834" Z="0.7" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.7" />
                  <Point X="4.689080162713" Y="-0.472984968998" Z="0.7" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.7" />
                  <Point X="3.680983847814" Y="-0.182101440123" Z="0.7" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.7" />
                  <Point X="3.615261403583" Y="-0.116137774597" Z="0.7" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.7" />
                  <Point X="3.58654295334" Y="-0.02667325524" Z="0.7" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.7" />
                  <Point X="3.594828497084" Y="0.069937275988" Z="0.7" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.7" />
                  <Point X="3.640118034817" Y="0.147810963978" Z="0.7" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.7" />
                  <Point X="3.722411566538" Y="0.206047404119" Z="0.7" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.7" />
                  <Point X="4.106887654593" Y="0.316986963771" Z="0.7" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.7" />
                  <Point X="5.000307868661" Y="0.875577168352" Z="0.7" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.7" />
                  <Point X="4.908761008699" Y="1.293748032518" Z="0.7" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.7" />
                  <Point X="4.489381810127" Y="1.357133869027" Z="0.7" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.7" />
                  <Point X="3.394957525212" Y="1.231032771438" Z="0.7" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.7" />
                  <Point X="3.318546068957" Y="1.262847604337" Z="0.7" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.7" />
                  <Point X="3.26452923323" Y="1.326549266376" Z="0.7" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.7" />
                  <Point X="3.238470267049" Y="1.40870679742" Z="0.7" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.7" />
                  <Point X="3.249173425876" Y="1.48806453171" Z="0.7" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.7" />
                  <Point X="3.296945046753" Y="1.563883008694" Z="0.7" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.7" />
                  <Point X="3.626099072717" Y="1.825022606247" Z="0.7" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.7" />
                  <Point X="4.295922214088" Y="2.705334346938" Z="0.7" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.7" />
                  <Point X="4.067397222373" Y="3.038102237239" Z="0.7" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.7" />
                  <Point X="3.590228243866" Y="2.890739282489" Z="0.7" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.7" />
                  <Point X="2.451757935059" Y="2.251456726672" Z="0.7" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.7" />
                  <Point X="2.379334338358" Y="2.251589276414" Z="0.7" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.7" />
                  <Point X="2.314337010788" Y="2.284997900013" Z="0.7" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.7" />
                  <Point X="2.265760700222" Y="2.342687849594" Z="0.7" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.7" />
                  <Point X="2.247840426344" Y="2.410424105474" Z="0.7" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.7" />
                  <Point X="2.261071215986" Y="2.487711640823" Z="0.7" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.7" />
                  <Point X="2.50488607037" Y="2.921910838089" Z="0.7" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.7" />
                  <Point X="2.857067342783" Y="4.195379092222" Z="0.7" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.7" />
                  <Point X="2.465573629946" Y="4.436777312369" Z="0.7" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.7" />
                  <Point X="2.05770641194" Y="4.640144369404" Z="0.7" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.7" />
                  <Point X="1.634709323692" Y="4.805499789967" Z="0.7" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.7" />
                  <Point X="1.043171283477" Y="4.962622515719" Z="0.7" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.7" />
                  <Point X="0.378035863475" Y="5.056970447716" Z="0.7" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.7" />
                  <Point X="0.139891563241" Y="4.877206921782" Z="0.7" />
                  <Point X="0" Y="4.355124473572" Z="0.7" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>