<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#169" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2012" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.76852734375" Y="-4.278424804688" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497139648438" />
                  <Point X="25.542365234375" Y="-3.467376708984" />
                  <Point X="25.458484375" Y="-3.346522216797" />
                  <Point X="25.37863671875" Y="-3.231476318359" />
                  <Point X="25.356751953125" Y="-3.209021484375" />
                  <Point X="25.33049609375" Y="-3.18977734375" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.172697265625" Y="-3.135384277344" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.833376953125" Y="-3.137320556641" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.18977734375" />
                  <Point X="24.655560546875" Y="-3.209021484375" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.549796875" Y="-3.352331542969" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481571777344" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.253650390625" Y="-4.241614257813" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="24.062560546875" Y="-4.872893554688" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.773396484375" Y="-4.807460449219" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.760333984375" Y="-4.751083007812" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.752482421875" Y="-4.360331054688" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.556853515625" Y="-4.026403564453" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295898438" />
                  <Point X="23.387115234375" Y="-3.897994628906" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.1983671875" Y="-3.880570800781" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894801513672" />
                  <Point X="22.82518359375" Y="-3.983107421875" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.55521875" Y="-4.242397949219" />
                  <Point X="22.5198515625" Y="-4.288488769531" />
                  <Point X="22.40628515625" Y="-4.218171386719" />
                  <Point X="22.19828515625" Y="-4.0893828125" />
                  <Point X="21.994376953125" Y="-3.932379638672" />
                  <Point X="21.89527734375" Y="-3.856077148438" />
                  <Point X="22.19485546875" Y="-3.337193115234" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647654052734" />
                  <Point X="22.593412109375" Y="-2.616126708984" />
                  <Point X="22.594423828125" Y="-2.585190429688" />
                  <Point X="22.585439453125" Y="-2.555570800781" />
                  <Point X="22.571220703125" Y="-2.526741455078" />
                  <Point X="22.55319921875" Y="-2.501591552734" />
                  <Point X="22.535857421875" Y="-2.484247802734" />
                  <Point X="22.510701171875" Y="-2.466219482422" />
                  <Point X="22.48187109375" Y="-2.451999267578" />
                  <Point X="22.45225390625" Y="-2.443012207031" />
                  <Point X="22.42131640625" Y="-2.444023925781" />
                  <Point X="22.389787109375" Y="-2.450294677734" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.7319921875" Y="-2.82425" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="21.08146484375" Y="-3.009750732422" />
                  <Point X="20.917140625" Y="-2.793861083984" />
                  <Point X="20.770951171875" Y="-2.548723144531" />
                  <Point X="20.693857421875" Y="-2.419449951172" />
                  <Point X="21.22675" Y="-2.01054699707" />
                  <Point X="21.894044921875" Y="-1.498513305664" />
                  <Point X="21.915421875" Y="-1.475594726562" />
                  <Point X="21.93338671875" Y="-1.44846484375" />
                  <Point X="21.946142578125" Y="-1.419835327148" />
                  <Point X="21.95384765625" Y="-1.390088134766" />
                  <Point X="21.95665234375" Y="-1.359663574219" />
                  <Point X="21.9544453125" Y="-1.327991699219" />
                  <Point X="21.9474453125" Y="-1.298242553711" />
                  <Point X="21.931361328125" Y="-1.272255859375" />
                  <Point X="21.91052734375" Y="-1.248298706055" />
                  <Point X="21.887029296875" Y="-1.228766601562" />
                  <Point X="21.860546875" Y="-1.213180053711" />
                  <Point X="21.83128125" Y="-1.201955566406" />
                  <Point X="21.79939453125" Y="-1.195474609375" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="20.97423828125" Y="-1.298893798828" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.230193359375" Y="-1.244266967773" />
                  <Point X="20.165921875" Y="-0.992650268555" />
                  <Point X="20.12724609375" Y="-0.722222351074" />
                  <Point X="20.107578125" Y="-0.584698364258" />
                  <Point X="20.7069765625" Y="-0.42408984375" />
                  <Point X="21.467125" Y="-0.220408432007" />
                  <Point X="21.4898125" Y="-0.211047363281" />
                  <Point X="21.51959765625" Y="-0.193959106445" />
                  <Point X="21.53276953125" Y="-0.184844848633" />
                  <Point X="21.558123046875" Y="-0.163933303833" />
                  <Point X="21.574263671875" Y="-0.146853469849" />
                  <Point X="21.585716796875" Y="-0.126334373474" />
                  <Point X="21.597880859375" Y="-0.096326332092" />
                  <Point X="21.60241796875" Y="-0.081954216003" />
                  <Point X="21.609177734375" Y="-0.052596595764" />
                  <Point X="21.611599609375" Y="-0.031193738937" />
                  <Point X="21.609138671875" Y="-0.00979535675" />
                  <Point X="21.60215234375" Y="0.020296171188" />
                  <Point X="21.597580078125" Y="0.034683013916" />
                  <Point X="21.585642578125" Y="0.063957149506" />
                  <Point X="21.574146484375" Y="0.08445135498" />
                  <Point X="21.55797265625" Y="0.101497032166" />
                  <Point X="21.531935546875" Y="0.122882926941" />
                  <Point X="21.51873828125" Y="0.131973648071" />
                  <Point X="21.48963671875" Y="0.148587554932" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="20.743509765625" Y="0.351740600586" />
                  <Point X="20.10818359375" Y="0.521975585938" />
                  <Point X="20.134091796875" Y="0.697063354492" />
                  <Point X="20.17551171875" Y="0.976967895508" />
                  <Point X="20.25337109375" Y="1.264294799805" />
                  <Point X="20.29644921875" Y="1.423268066406" />
                  <Point X="20.68944140625" Y="1.371529663086" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228149414" />
                  <Point X="21.296865234375" Y="1.305263671875" />
                  <Point X="21.328333984375" Y="1.315185913086" />
                  <Point X="21.358291015625" Y="1.324631225586" />
                  <Point X="21.377224609375" Y="1.332962524414" />
                  <Point X="21.39596875" Y="1.343784790039" />
                  <Point X="21.412646484375" Y="1.356014282227" />
                  <Point X="21.426283203125" Y="1.371563232422" />
                  <Point X="21.43869921875" Y="1.389293457031" />
                  <Point X="21.44865234375" Y="1.407432495117" />
                  <Point X="21.461279296875" Y="1.437917358398" />
                  <Point X="21.473298828125" Y="1.466936889648" />
                  <Point X="21.4790859375" Y="1.486796875" />
                  <Point X="21.48284375" Y="1.508111572266" />
                  <Point X="21.484197265625" Y="1.528754638672" />
                  <Point X="21.481048828125" Y="1.549200927734" />
                  <Point X="21.4754453125" Y="1.570107055664" />
                  <Point X="21.46794921875" Y="1.589378662109" />
                  <Point X="21.452712890625" Y="1.618646728516" />
                  <Point X="21.438208984375" Y="1.646508300781" />
                  <Point X="21.42671875" Y="1.663705810547" />
                  <Point X="21.412806640625" Y="1.680285888672" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="20.982796875" Y="2.013083129883" />
                  <Point X="20.648140625" Y="2.269873535156" />
                  <Point X="20.75790625" Y="2.457928222656" />
                  <Point X="20.9188515625" Y="2.733665039062" />
                  <Point X="21.125087890625" Y="2.998753662109" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.458685546875" Y="3.037886230469" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.89703515625" Y="2.821961914062" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.95943359375" Y="2.818762939453" />
                  <Point X="21.980892578125" Y="2.821587890625" />
                  <Point X="22.000984375" Y="2.826504150391" />
                  <Point X="22.019537109375" Y="2.835652832031" />
                  <Point X="22.037791015625" Y="2.847281738281" />
                  <Point X="22.053923828125" Y="2.860229003906" />
                  <Point X="22.085033203125" Y="2.891338378906" />
                  <Point X="22.1146484375" Y="2.920952392578" />
                  <Point X="22.127595703125" Y="2.937085693359" />
                  <Point X="22.139224609375" Y="2.955340332031" />
                  <Point X="22.14837109375" Y="2.973889160156" />
                  <Point X="22.1532890625" Y="2.993977783203" />
                  <Point X="22.156115234375" Y="3.015437255859" />
                  <Point X="22.15656640625" Y="3.036120361328" />
                  <Point X="22.152732421875" Y="3.079948242188" />
                  <Point X="22.14908203125" Y="3.121669921875" />
                  <Point X="22.145044921875" Y="3.141963867188" />
                  <Point X="22.13853515625" Y="3.162605712891" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.946275390625" Y="3.500107666016" />
                  <Point X="21.81666796875" Y="3.724595703125" />
                  <Point X="22.0190703125" Y="3.879776855469" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.6241953125" Y="4.275147460938" />
                  <Point X="22.83296484375" Y="4.391134277344" />
                  <Point X="22.85435546875" Y="4.363255859375" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.053666015625" Y="4.164001464844" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.202685546875" Y="4.128692871094" />
                  <Point X="23.222548828125" Y="4.134481933594" />
                  <Point X="23.27335546875" Y="4.155527832031" />
                  <Point X="23.32172265625" Y="4.175561523437" />
                  <Point X="23.339853515625" Y="4.185509277344" />
                  <Point X="23.357583984375" Y="4.197923828125" />
                  <Point X="23.37313671875" Y="4.2115625" />
                  <Point X="23.3853671875" Y="4.228244140625" />
                  <Point X="23.396189453125" Y="4.246988769531" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.42105859375" Y="4.318370117188" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583984375" />
                  <Point X="23.44272265625" Y="4.410145019531" />
                  <Point X="23.442271484375" Y="4.430827148438" />
                  <Point X="23.421359375" Y="4.589659667969" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.687494140625" Y="4.708072265625" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.444142578125" Y="4.855894042969" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.767095703125" Y="4.655793457031" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.2404609375" Y="4.638036132812" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.527197265625" Y="4.864920898438" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.169830078125" Y="4.753083984375" />
                  <Point X="26.4810234375" Y="4.677952148438" />
                  <Point X="26.692359375" Y="4.601298828125" />
                  <Point X="26.894650390625" Y="4.527926269531" />
                  <Point X="27.099697265625" Y="4.432033203125" />
                  <Point X="27.294576171875" Y="4.340893554688" />
                  <Point X="27.49269140625" Y="4.225472167969" />
                  <Point X="27.68097265625" Y="4.115777832031" />
                  <Point X="27.8678046875" Y="3.982915527344" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.58900390625" Y="3.315662353516" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.539934326172" />
                  <Point X="27.133078125" Y="2.51605859375" />
                  <Point X="27.122078125" Y="2.474927001953" />
                  <Point X="27.111607421875" Y="2.435772216797" />
                  <Point X="27.108619140625" Y="2.417936035156" />
                  <Point X="27.107728515625" Y="2.380952880859" />
                  <Point X="27.112017578125" Y="2.345385742188" />
                  <Point X="27.116099609375" Y="2.311528076172" />
                  <Point X="27.121443359375" Y="2.289603027344" />
                  <Point X="27.1297109375" Y="2.267512451172" />
                  <Point X="27.140072265625" Y="2.247469970703" />
                  <Point X="27.162080078125" Y="2.215036132812" />
                  <Point X="27.18303125" Y="2.184161376953" />
                  <Point X="27.194466796875" Y="2.170328125" />
                  <Point X="27.221599609375" Y="2.145592529297" />
                  <Point X="27.254033203125" Y="2.123584716797" />
                  <Point X="27.284908203125" Y="2.102635009766" />
                  <Point X="27.304953125" Y="2.092272216797" />
                  <Point X="27.327041015625" Y="2.084006347656" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.38453125" Y="2.074374755859" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.473205078125" Y="2.074170898438" />
                  <Point X="27.514337890625" Y="2.085170166016" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.316353515625" Y="2.530351318359" />
                  <Point X="28.967326171875" Y="2.906190185547" />
                  <Point X="29.011583984375" Y="2.844681396484" />
                  <Point X="29.12328125" Y="2.689449462891" />
                  <Point X="29.227423828125" Y="2.517350341797" />
                  <Point X="29.26219921875" Y="2.459883300781" />
                  <Point X="28.811958984375" Y="2.114400634766" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.2214296875" Y="1.660244873047" />
                  <Point X="28.20397265625" Y="1.641626708984" />
                  <Point X="28.17437109375" Y="1.603008056641" />
                  <Point X="28.14619140625" Y="1.566245361328" />
                  <Point X="28.13660546875" Y="1.550911499023" />
                  <Point X="28.121630859375" Y="1.517088256836" />
                  <Point X="28.110603515625" Y="1.477658325195" />
                  <Point X="28.10010546875" Y="1.440123657227" />
                  <Point X="28.09665234375" Y="1.417825195312" />
                  <Point X="28.0958359375" Y="1.394253417969" />
                  <Point X="28.097740234375" Y="1.371766357422" />
                  <Point X="28.10679296875" Y="1.327895629883" />
                  <Point X="28.11541015625" Y="1.286133544922" />
                  <Point X="28.1206796875" Y="1.268977905273" />
                  <Point X="28.136283203125" Y="1.235740722656" />
                  <Point X="28.160904296875" Y="1.198318725586" />
                  <Point X="28.18433984375" Y="1.1626953125" />
                  <Point X="28.198890625" Y="1.145452880859" />
                  <Point X="28.216134765625" Y="1.129362182617" />
                  <Point X="28.23434765625" Y="1.116034545898" />
                  <Point X="28.27002734375" Y="1.095950561523" />
                  <Point X="28.303990234375" Y="1.07683203125" />
                  <Point X="28.320521484375" Y="1.069501342773" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.404359375" Y="1.053063232422" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.179583984375" Y="1.138006347656" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.797966796875" Y="1.129850585938" />
                  <Point X="29.84594140625" Y="0.932785949707" />
                  <Point X="29.878759765625" Y="0.722002990723" />
                  <Point X="29.8908671875" Y="0.644238952637" />
                  <Point X="29.3826171875" Y="0.50805368042" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.7047890625" Y="0.325585296631" />
                  <Point X="28.681546875" Y="0.315068023682" />
                  <Point X="28.63415234375" Y="0.287673370361" />
                  <Point X="28.589037109375" Y="0.261595489502" />
                  <Point X="28.57431640625" Y="0.251099029541" />
                  <Point X="28.54753125" Y="0.225576080322" />
                  <Point X="28.519095703125" Y="0.189341445923" />
                  <Point X="28.492025390625" Y="0.154848464966" />
                  <Point X="28.48030078125" Y="0.135567596436" />
                  <Point X="28.47052734375" Y="0.114103523254" />
                  <Point X="28.463681640625" Y="0.092603782654" />
                  <Point X="28.454203125" Y="0.043109046936" />
                  <Point X="28.4451796875" Y="-0.004006831169" />
                  <Point X="28.443484375" Y="-0.021875137329" />
                  <Point X="28.4451796875" Y="-0.058553302765" />
                  <Point X="28.454658203125" Y="-0.108047889709" />
                  <Point X="28.463681640625" Y="-0.155163772583" />
                  <Point X="28.47052734375" Y="-0.176663650513" />
                  <Point X="28.48030078125" Y="-0.198127731323" />
                  <Point X="28.492025390625" Y="-0.217408584595" />
                  <Point X="28.5204609375" Y="-0.253643234253" />
                  <Point X="28.54753125" Y="-0.288136383057" />
                  <Point X="28.56000390625" Y="-0.301239532471" />
                  <Point X="28.589037109375" Y="-0.324155609131" />
                  <Point X="28.636431640625" Y="-0.351550140381" />
                  <Point X="28.681546875" Y="-0.377628173828" />
                  <Point X="28.692708984375" Y="-0.383137695312" />
                  <Point X="28.716580078125" Y="-0.392149810791" />
                  <Point X="29.35060546875" Y="-0.562036621094" />
                  <Point X="29.891474609375" Y="-0.706961975098" />
                  <Point X="29.88180859375" Y="-0.771075744629" />
                  <Point X="29.855025390625" Y="-0.948723388672" />
                  <Point X="29.812974609375" Y="-1.132993408203" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="29.196796875" Y="-1.105131225586" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.281642578125" Y="-1.02572644043" />
                  <Point X="28.193095703125" Y="-1.044972412109" />
                  <Point X="28.163974609375" Y="-1.056597045898" />
                  <Point X="28.1361484375" Y="-1.073489013672" />
                  <Point X="28.1123984375" Y="-1.093960327148" />
                  <Point X="28.05617578125" Y="-1.161579223633" />
                  <Point X="28.002654296875" Y="-1.225948242188" />
                  <Point X="27.987935546875" Y="-1.250329956055" />
                  <Point X="27.976591796875" Y="-1.277716064453" />
                  <Point X="27.969759765625" Y="-1.305365478516" />
                  <Point X="27.961701171875" Y="-1.392935180664" />
                  <Point X="27.95403125" Y="-1.476295898438" />
                  <Point X="27.95634765625" Y="-1.507562133789" />
                  <Point X="27.964078125" Y="-1.539182739258" />
                  <Point X="27.976451171875" Y="-1.567996582031" />
                  <Point X="28.027927734375" Y="-1.648065795898" />
                  <Point X="28.076931640625" Y="-1.724286987305" />
                  <Point X="28.0869375" Y="-1.737242919922" />
                  <Point X="28.110630859375" Y="-1.760909301758" />
                  <Point X="28.699009765625" Y="-2.212389160156" />
                  <Point X="29.213123046875" Y="-2.606882080078" />
                  <Point X="29.200310546875" Y="-2.627614013672" />
                  <Point X="29.124802734375" Y="-2.749797607422" />
                  <Point X="29.037849609375" Y="-2.873346435547" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="28.48889453125" Y="-2.574125488281" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.64351953125" Y="-2.13983203125" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.352865234375" Y="-2.183579589844" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246549072266" />
                  <Point X="27.22142578125" Y="-2.267508300781" />
                  <Point X="27.204533203125" Y="-2.290438964844" />
                  <Point X="27.156130859375" Y="-2.382407958984" />
                  <Point X="27.110052734375" Y="-2.46995703125" />
                  <Point X="27.100228515625" Y="-2.499735107422" />
                  <Point X="27.095271484375" Y="-2.531908691406" />
                  <Point X="27.09567578125" Y="-2.563259277344" />
                  <Point X="27.115669921875" Y="-2.67396484375" />
                  <Point X="27.134703125" Y="-2.779349609375" />
                  <Point X="27.13898828125" Y="-2.795140136719" />
                  <Point X="27.1518203125" Y="-2.826078369141" />
                  <Point X="27.529912109375" Y="-3.480955078125" />
                  <Point X="27.86128515625" Y="-4.054907226563" />
                  <Point X="27.78186328125" Y="-4.111635253906" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="27.283509765625" Y="-3.618397949219" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.612740234375" Y="-2.830360839844" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.2976875" Y="-2.752105224609" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.012390625" Y="-2.872128662109" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968860595703" />
                  <Point X="25.88725" Y="-2.996686767578" />
                  <Point X="25.875625" Y="-3.025809082031" />
                  <Point X="25.848056640625" Y="-3.152651123047" />
                  <Point X="25.821810546875" Y="-3.273396972656" />
                  <Point X="25.819724609375" Y="-3.289627197266" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.926892578125" Y="-4.13699609375" />
                  <Point X="26.02206640625" Y="-4.859915527344" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94157421875" Y="-4.75263671875" />
                  <Point X="23.85875390625" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575836425781" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030761719" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688964844" />
                  <Point X="23.84565625" Y="-4.341796875" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.811875" Y="-4.178468261719" />
                  <Point X="23.80097265625" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.74979296875" Y="-4.070937744141" />
                  <Point X="23.738994140625" Y="-4.059779296875" />
                  <Point X="23.6194921875" Y="-3.954978759766" />
                  <Point X="23.505734375" Y="-3.85521484375" />
                  <Point X="23.49326171875" Y="-3.845965087891" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822527587891" />
                  <Point X="23.423470703125" Y="-3.810226318359" />
                  <Point X="23.4086875" Y="-3.805476318359" />
                  <Point X="23.378544921875" Y="-3.798447998047" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.204580078125" Y="-3.785774169922" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.946326171875" Y="-3.79549609375" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812011719" />
                  <Point X="22.772404296875" Y="-3.904117919922" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629150391" />
                  <Point X="22.496798828125" Y="-4.162478027344" />
                  <Point X="22.456296875" Y="-4.137400878906" />
                  <Point X="22.25240625" Y="-4.011156494141" />
                  <Point X="22.052333984375" Y="-3.857107177734" />
                  <Point X="22.01913671875" Y="-3.831546142578" />
                  <Point X="22.277126953125" Y="-3.384693115234" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.710084716797" />
                  <Point X="22.67605078125" Y="-2.681119140625" />
                  <Point X="22.680314453125" Y="-2.666188476562" />
                  <Point X="22.6865859375" Y="-2.634661132812" />
                  <Point X="22.688361328125" Y="-2.619231933594" />
                  <Point X="22.689373046875" Y="-2.588295654297" />
                  <Point X="22.685333984375" Y="-2.557615234375" />
                  <Point X="22.676349609375" Y="-2.527995605469" />
                  <Point X="22.670640625" Y="-2.513549316406" />
                  <Point X="22.656421875" Y="-2.484719970703" />
                  <Point X="22.64844140625" Y="-2.471407470703" />
                  <Point X="22.630419921875" Y="-2.446257568359" />
                  <Point X="22.62037890625" Y="-2.434420166016" />
                  <Point X="22.603037109375" Y="-2.417076416016" />
                  <Point X="22.5911953125" Y="-2.407029785156" />
                  <Point X="22.5660390625" Y="-2.389001464844" />
                  <Point X="22.552724609375" Y="-2.381019775391" />
                  <Point X="22.52389453125" Y="-2.366799560547" />
                  <Point X="22.509455078125" Y="-2.361092285156" />
                  <Point X="22.479837890625" Y="-2.352105224609" />
                  <Point X="22.4491484375" Y="-2.348062988281" />
                  <Point X="22.4182109375" Y="-2.349074707031" />
                  <Point X="22.40278515625" Y="-2.350848876953" />
                  <Point X="22.371255859375" Y="-2.357119628906" />
                  <Point X="22.35632421875" Y="-2.361383056641" />
                  <Point X="22.32735546875" Y="-2.372285644531" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.6844921875" Y="-2.741977539062" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="21.15705859375" Y="-2.952212158203" />
                  <Point X="20.9959765625" Y="-2.740583007812" />
                  <Point X="20.85254296875" Y="-2.500064941406" />
                  <Point X="20.818734375" Y="-2.443373291016" />
                  <Point X="21.28458203125" Y="-2.085915527344" />
                  <Point X="21.951876953125" Y="-1.573881835938" />
                  <Point X="21.963515625" Y="-1.563311279297" />
                  <Point X="21.984892578125" Y="-1.540392822266" />
                  <Point X="21.994630859375" Y="-1.528044921875" />
                  <Point X="22.012595703125" Y="-1.500914916992" />
                  <Point X="22.0201640625" Y="-1.487128173828" />
                  <Point X="22.032919921875" Y="-1.458498535156" />
                  <Point X="22.038107421875" Y="-1.44365612793" />
                  <Point X="22.0458125" Y="-1.413908813477" />
                  <Point X="22.048447265625" Y="-1.39880871582" />
                  <Point X="22.051251953125" Y="-1.368384033203" />
                  <Point X="22.051421875" Y="-1.353059570313" />
                  <Point X="22.04921484375" Y="-1.321387695312" />
                  <Point X="22.046919921875" Y="-1.306232421875" />
                  <Point X="22.039919921875" Y="-1.276483276367" />
                  <Point X="22.028224609375" Y="-1.248245483398" />
                  <Point X="22.012140625" Y="-1.222259033203" />
                  <Point X="22.003046875" Y="-1.209916015625" />
                  <Point X="21.982212890625" Y="-1.185958862305" />
                  <Point X="21.97125390625" Y="-1.175241943359" />
                  <Point X="21.947755859375" Y="-1.155709838867" />
                  <Point X="21.935216796875" Y="-1.14689453125" />
                  <Point X="21.908734375" Y="-1.131307983398" />
                  <Point X="21.89456640625" Y="-1.124480224609" />
                  <Point X="21.86530078125" Y="-1.113255737305" />
                  <Point X="21.850203125" Y="-1.108859008789" />
                  <Point X="21.81831640625" Y="-1.102377929688" />
                  <Point X="21.802701171875" Y="-1.100532104492" />
                  <Point X="21.77137890625" Y="-1.09944128418" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="20.961837890625" Y="-1.204706665039" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.32223828125" Y="-1.220755493164" />
                  <Point X="20.259236328125" Y="-0.974112060547" />
                  <Point X="20.2212890625" Y="-0.708772583008" />
                  <Point X="20.213548828125" Y="-0.654654602051" />
                  <Point X="20.731564453125" Y="-0.515852844238" />
                  <Point X="21.491712890625" Y="-0.312171417236" />
                  <Point X="21.503359375" Y="-0.30822668457" />
                  <Point X="21.526046875" Y="-0.298865661621" />
                  <Point X="21.537087890625" Y="-0.29344909668" />
                  <Point X="21.566873046875" Y="-0.276360900879" />
                  <Point X="21.573654296875" Y="-0.272080566406" />
                  <Point X="21.593216796875" Y="-0.258132598877" />
                  <Point X="21.6185703125" Y="-0.237221054077" />
                  <Point X="21.627169921875" Y="-0.229183242798" />
                  <Point X="21.643310546875" Y="-0.212103347778" />
                  <Point X="21.657216796875" Y="-0.19315512085" />
                  <Point X="21.668669921875" Y="-0.17263609314" />
                  <Point X="21.6737578125" Y="-0.16202293396" />
                  <Point X="21.685921875" Y="-0.132014938354" />
                  <Point X="21.688474609375" Y="-0.124925476074" />
                  <Point X="21.69499609375" Y="-0.103270683289" />
                  <Point X="21.701755859375" Y="-0.07391305542" />
                  <Point X="21.703576171875" Y="-0.063278347015" />
                  <Point X="21.705998046875" Y="-0.041875465393" />
                  <Point X="21.7059765625" Y="-0.02033971405" />
                  <Point X="21.703515625" Y="0.001058710814" />
                  <Point X="21.701677734375" Y="0.011689258575" />
                  <Point X="21.69469140625" Y="0.041780784607" />
                  <Point X="21.692689453125" Y="0.049069843292" />
                  <Point X="21.685546875" Y="0.070554618835" />
                  <Point X="21.673609375" Y="0.099828712463" />
                  <Point X="21.668498046875" Y="0.110433998108" />
                  <Point X="21.657001953125" Y="0.130928207397" />
                  <Point X="21.643060546875" Y="0.149840911865" />
                  <Point X="21.62688671875" Y="0.166886611938" />
                  <Point X="21.61826953125" Y="0.17490838623" />
                  <Point X="21.592232421875" Y="0.196294326782" />
                  <Point X="21.585826171875" Y="0.201118179321" />
                  <Point X="21.565837890625" Y="0.214475799561" />
                  <Point X="21.536736328125" Y="0.231089614868" />
                  <Point X="21.525779296875" Y="0.236444046021" />
                  <Point X="21.503267578125" Y="0.245704742432" />
                  <Point X="21.491712890625" Y="0.249611297607" />
                  <Point X="20.76809765625" Y="0.443503509521" />
                  <Point X="20.2145546875" Y="0.591825012207" />
                  <Point X="20.228068359375" Y="0.683157287598" />
                  <Point X="20.26866796875" Y="0.957520446777" />
                  <Point X="20.345064453125" Y="1.239447998047" />
                  <Point X="20.3664140625" Y="1.318237182617" />
                  <Point X="20.677041015625" Y="1.277342407227" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815551758" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.204703369141" />
                  <Point X="21.28485546875" Y="1.206589599609" />
                  <Point X="21.295109375" Y="1.208053344727" />
                  <Point X="21.3153984375" Y="1.212088867188" />
                  <Point X="21.32543359375" Y="1.214660766602" />
                  <Point X="21.35690234375" Y="1.224583007812" />
                  <Point X="21.386859375" Y="1.234028320312" />
                  <Point X="21.396552734375" Y="1.237677124023" />
                  <Point X="21.415486328125" Y="1.246008422852" />
                  <Point X="21.4247265625" Y="1.250690795898" />
                  <Point X="21.443470703125" Y="1.261513061523" />
                  <Point X="21.452146484375" Y="1.267174438477" />
                  <Point X="21.46882421875" Y="1.279403808594" />
                  <Point X="21.4840703125" Y="1.293374755859" />
                  <Point X="21.49770703125" Y="1.308923583984" />
                  <Point X="21.504099609375" Y="1.317069946289" />
                  <Point X="21.516515625" Y="1.334800170898" />
                  <Point X="21.521984375" Y="1.343593505859" />
                  <Point X="21.5319375" Y="1.361732666016" />
                  <Point X="21.536421875" Y="1.371078369141" />
                  <Point X="21.549048828125" Y="1.401563110352" />
                  <Point X="21.561068359375" Y="1.430582641602" />
                  <Point X="21.564505859375" Y="1.440359741211" />
                  <Point X="21.57029296875" Y="1.460219848633" />
                  <Point X="21.572642578125" Y="1.470302612305" />
                  <Point X="21.576400390625" Y="1.49161730957" />
                  <Point X="21.577640625" Y="1.501895996094" />
                  <Point X="21.578994140625" Y="1.52253894043" />
                  <Point X="21.57808984375" Y="1.543212890625" />
                  <Point X="21.57494140625" Y="1.563659179688" />
                  <Point X="21.572810546875" Y="1.573795898438" />
                  <Point X="21.56720703125" Y="1.594702026367" />
                  <Point X="21.563982421875" Y="1.604545776367" />
                  <Point X="21.556486328125" Y="1.623817504883" />
                  <Point X="21.55221484375" Y="1.633245483398" />
                  <Point X="21.536978515625" Y="1.662513549805" />
                  <Point X="21.522474609375" Y="1.69037512207" />
                  <Point X="21.517201171875" Y="1.69928503418" />
                  <Point X="21.5057109375" Y="1.716482421875" />
                  <Point X="21.499494140625" Y="1.724770019531" />
                  <Point X="21.48558203125" Y="1.741350097656" />
                  <Point X="21.478498046875" Y="1.748912597656" />
                  <Point X="21.4635546875" Y="1.763216796875" />
                  <Point X="21.4556953125" Y="1.769958496094" />
                  <Point X="21.04062890625" Y="2.088451660156" />
                  <Point X="20.77238671875" Y="2.294281005859" />
                  <Point X="20.839953125" Y="2.410038574219" />
                  <Point X="20.997716796875" Y="2.680322998047" />
                  <Point X="21.200068359375" Y="2.940419433594" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.411185546875" Y="2.955613769531" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.888755859375" Y="2.727323486328" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.961505859375" Y="2.723785644531" />
                  <Point X="21.97183203125" Y="2.724575683594" />
                  <Point X="21.993291015625" Y="2.727400634766" />
                  <Point X="22.00347265625" Y="2.729310302734" />
                  <Point X="22.023564453125" Y="2.7342265625" />
                  <Point X="22.043" Y="2.741300292969" />
                  <Point X="22.061552734375" Y="2.750448974609" />
                  <Point X="22.070580078125" Y="2.755530517578" />
                  <Point X="22.088833984375" Y="2.767159423828" />
                  <Point X="22.097251953125" Y="2.773191162109" />
                  <Point X="22.113384765625" Y="2.786138427734" />
                  <Point X="22.121099609375" Y="2.793053955078" />
                  <Point X="22.152208984375" Y="2.824163330078" />
                  <Point X="22.18182421875" Y="2.85377734375" />
                  <Point X="22.188740234375" Y="2.861492675781" />
                  <Point X="22.2016875" Y="2.877625976562" />
                  <Point X="22.20771875" Y="2.886043945312" />
                  <Point X="22.21934765625" Y="2.904298583984" />
                  <Point X="22.2244296875" Y="2.913325927734" />
                  <Point X="22.233576171875" Y="2.931874755859" />
                  <Point X="22.240646484375" Y="2.951299072266" />
                  <Point X="22.245564453125" Y="2.971387695312" />
                  <Point X="22.2474765625" Y="2.981573486328" />
                  <Point X="22.250302734375" Y="3.003032958984" />
                  <Point X="22.251091796875" Y="3.013365478516" />
                  <Point X="22.25154296875" Y="3.034048583984" />
                  <Point X="22.251205078125" Y="3.044399169922" />
                  <Point X="22.24737109375" Y="3.088227050781" />
                  <Point X="22.243720703125" Y="3.129948730469" />
                  <Point X="22.242255859375" Y="3.140205322266" />
                  <Point X="22.23821875" Y="3.160499267578" />
                  <Point X="22.235646484375" Y="3.170536621094" />
                  <Point X="22.22913671875" Y="3.191178466797" />
                  <Point X="22.225486328125" Y="3.200874023438" />
                  <Point X="22.21715625" Y="3.219801269531" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="22.028546875" Y="3.547607666016" />
                  <Point X="21.94061328125" Y="3.699915039062" />
                  <Point X="22.076873046875" Y="3.804385253906" />
                  <Point X="22.3516328125" Y="4.015041015625" />
                  <Point X="22.67033203125" Y="4.192103515625" />
                  <Point X="22.807474609375" Y="4.268296386719" />
                  <Point X="22.88143359375" Y="4.171911132813" />
                  <Point X="22.8881796875" Y="4.164048828125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.934908203125" Y="4.121897460938" />
                  <Point X="22.952107421875" Y="4.110405273438" />
                  <Point X="22.96101953125" Y="4.105129394531" />
                  <Point X="23.009798828125" Y="4.079735839844" />
                  <Point X="23.056236328125" Y="4.0555625" />
                  <Point X="23.065673828125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.0437890625" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031377929688" />
                  <Point X="23.2191796875" Y="4.035135742188" />
                  <Point X="23.229267578125" Y="4.037487548828" />
                  <Point X="23.249130859375" Y="4.043276611328" />
                  <Point X="23.25890625" Y="4.046714111328" />
                  <Point X="23.309712890625" Y="4.067760009766" />
                  <Point X="23.358080078125" Y="4.087793701172" />
                  <Point X="23.367419921875" Y="4.092274169922" />
                  <Point X="23.38555078125" Y="4.102221679688" />
                  <Point X="23.394341796875" Y="4.107688964844" />
                  <Point X="23.412072265625" Y="4.120103515625" />
                  <Point X="23.420220703125" Y="4.126497558594" />
                  <Point X="23.4357734375" Y="4.140136230469" />
                  <Point X="23.449751953125" Y="4.155391113281" />
                  <Point X="23.461982421875" Y="4.172072753906" />
                  <Point X="23.467638671875" Y="4.180744140625" />
                  <Point X="23.4784609375" Y="4.199488769531" />
                  <Point X="23.483140625" Y="4.208722167969" />
                  <Point X="23.49147265625" Y="4.227654785156" />
                  <Point X="23.495125" Y="4.237354003906" />
                  <Point X="23.511662109375" Y="4.289802734375" />
                  <Point X="23.527404296875" Y="4.33973046875" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370050292969" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401863769531" />
                  <Point X="23.53769921875" Y="4.412216796875" />
                  <Point X="23.537248046875" Y="4.432898925781" />
                  <Point X="23.536458984375" Y="4.443228027344" />
                  <Point X="23.520734375" Y="4.562655273438" />
                  <Point X="23.713140625" Y="4.616599609375" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.455185546875" Y="4.761538085938" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.67533203125" Y="4.631205566406" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.332224609375" Y="4.613448242188" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.517302734375" Y="4.7704375" />
                  <Point X="25.827876953125" Y="4.737912109375" />
                  <Point X="26.14753515625" Y="4.660737304688" />
                  <Point X="26.453591796875" Y="4.586845214844" />
                  <Point X="26.659966796875" Y="4.511991699219" />
                  <Point X="26.85826171875" Y="4.440068847656" />
                  <Point X="27.059453125" Y="4.345978515625" />
                  <Point X="27.250453125" Y="4.256652832031" />
                  <Point X="27.444869140625" Y="4.14338671875" />
                  <Point X="27.629421875" Y="4.035865478516" />
                  <Point X="27.812748046875" Y="3.905495605469" />
                  <Point X="27.81778125" Y="3.901915527344" />
                  <Point X="27.506732421875" Y="3.363162353516" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.062376953125" Y="2.593106933594" />
                  <Point X="27.0531875" Y="2.573449462891" />
                  <Point X="27.044185546875" Y="2.549573730469" />
                  <Point X="27.041302734375" Y="2.540602294922" />
                  <Point X="27.030302734375" Y="2.499470703125" />
                  <Point X="27.01983203125" Y="2.460315917969" />
                  <Point X="27.0179140625" Y="2.451469726562" />
                  <Point X="27.013646484375" Y="2.420223144531" />
                  <Point X="27.012755859375" Y="2.383239990234" />
                  <Point X="27.013412109375" Y="2.369579101562" />
                  <Point X="27.017701171875" Y="2.334011962891" />
                  <Point X="27.021783203125" Y="2.300154296875" />
                  <Point X="27.02380078125" Y="2.289032470703" />
                  <Point X="27.02914453125" Y="2.267107421875" />
                  <Point X="27.032470703125" Y="2.256304199219" />
                  <Point X="27.04073828125" Y="2.234213623047" />
                  <Point X="27.0453203125" Y="2.223885498047" />
                  <Point X="27.055681640625" Y="2.203843017578" />
                  <Point X="27.0614609375" Y="2.194128662109" />
                  <Point X="27.08346875" Y="2.161694824219" />
                  <Point X="27.104419921875" Y="2.130820068359" />
                  <Point X="27.109810546875" Y="2.123632324219" />
                  <Point X="27.13046484375" Y="2.100123291016" />
                  <Point X="27.15759765625" Y="2.075387695312" />
                  <Point X="27.1682578125" Y="2.066981445312" />
                  <Point X="27.20069140625" Y="2.044973754883" />
                  <Point X="27.23156640625" Y="2.024024047852" />
                  <Point X="27.24128125" Y="2.018245117188" />
                  <Point X="27.261326171875" Y="2.007882446289" />
                  <Point X="27.27165625" Y="2.003298339844" />
                  <Point X="27.293744140625" Y="1.995032470703" />
                  <Point X="27.304548828125" Y="1.991707519531" />
                  <Point X="27.32647265625" Y="1.986364868164" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.373158203125" Y="1.980057983398" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.416044921875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975497314453" />
                  <Point X="27.4843125" Y="1.979822387695" />
                  <Point X="27.49774609375" Y="1.982395507812" />
                  <Point X="27.53887890625" Y="1.993394775391" />
                  <Point X="27.578033203125" Y="2.003865112305" />
                  <Point X="27.583994140625" Y="2.005670654297" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.363853515625" Y="2.448078857422" />
                  <Point X="28.940404296875" Y="2.780950195312" />
                  <Point X="29.043962890625" Y="2.637028564453" />
                  <Point X="29.136884765625" Y="2.483470947266" />
                  <Point X="28.754126953125" Y="2.189769042969" />
                  <Point X="28.172953125" Y="1.743820068359" />
                  <Point X="28.168138671875" Y="1.739868530273" />
                  <Point X="28.15212890625" Y="1.725224121094" />
                  <Point X="28.134671875" Y="1.706606079102" />
                  <Point X="28.12857421875" Y="1.699420288086" />
                  <Point X="28.09897265625" Y="1.660801635742" />
                  <Point X="28.07079296875" Y="1.62403894043" />
                  <Point X="28.06563671875" Y="1.616603881836" />
                  <Point X="28.04973828125" Y="1.589370361328" />
                  <Point X="28.034763671875" Y="1.555547119141" />
                  <Point X="28.030140625" Y="1.542675048828" />
                  <Point X="28.01911328125" Y="1.503245117188" />
                  <Point X="28.008615234375" Y="1.465710571289" />
                  <Point X="28.006224609375" Y="1.454661987305" />
                  <Point X="28.002771484375" Y="1.432363525391" />
                  <Point X="28.001708984375" Y="1.421113525391" />
                  <Point X="28.000892578125" Y="1.397541748047" />
                  <Point X="28.001173828125" Y="1.386237060547" />
                  <Point X="28.003078125" Y="1.36375" />
                  <Point X="28.004701171875" Y="1.352567626953" />
                  <Point X="28.01375390625" Y="1.308696777344" />
                  <Point X="28.02237109375" Y="1.266934814453" />
                  <Point X="28.02459765625" Y="1.258239501953" />
                  <Point X="28.03468359375" Y="1.228606689453" />
                  <Point X="28.050287109375" Y="1.195369506836" />
                  <Point X="28.056919921875" Y="1.183525146484" />
                  <Point X="28.081541015625" Y="1.146103149414" />
                  <Point X="28.1049765625" Y="1.110479736328" />
                  <Point X="28.111736328125" Y="1.101426513672" />
                  <Point X="28.126287109375" Y="1.084184204102" />
                  <Point X="28.134078125" Y="1.075994873047" />
                  <Point X="28.151322265625" Y="1.059904174805" />
                  <Point X="28.160033203125" Y="1.052696411133" />
                  <Point X="28.17824609375" Y="1.039368896484" />
                  <Point X="28.187748046875" Y="1.033248901367" />
                  <Point X="28.223427734375" Y="1.013164855957" />
                  <Point X="28.257390625" Y="0.994046325684" />
                  <Point X="28.26548046875" Y="0.989987731934" />
                  <Point X="28.2946796875" Y="0.978083618164" />
                  <Point X="28.33027734375" Y="0.968020935059" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.391912109375" Y="0.958882141113" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199768066" />
                  <Point X="28.46571875" Y="0.951222839355" />
                  <Point X="28.49121875" Y="0.952032409668" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="29.191984375" Y="1.043819091797" />
                  <Point X="29.704703125" Y="1.111319702148" />
                  <Point X="29.705662109375" Y="1.107380371094" />
                  <Point X="29.752689453125" Y="0.914207641602" />
                  <Point X="29.783873046875" Y="0.713921325684" />
                  <Point X="29.358029296875" Y="0.599816711426" />
                  <Point X="28.6919921875" Y="0.421352752686" />
                  <Point X="28.686029296875" Y="0.419543548584" />
                  <Point X="28.665623046875" Y="0.412136352539" />
                  <Point X="28.642380859375" Y="0.40161920166" />
                  <Point X="28.634005859375" Y="0.397316864014" />
                  <Point X="28.586611328125" Y="0.369922241211" />
                  <Point X="28.54149609375" Y="0.343844299316" />
                  <Point X="28.5338828125" Y="0.338945678711" />
                  <Point X="28.50878125" Y="0.31987512207" />
                  <Point X="28.48199609375" Y="0.294352111816" />
                  <Point X="28.472796875" Y="0.284225128174" />
                  <Point X="28.444361328125" Y="0.24799041748" />
                  <Point X="28.417291015625" Y="0.213497421265" />
                  <Point X="28.41085546875" Y="0.204207901001" />
                  <Point X="28.399130859375" Y="0.184927047729" />
                  <Point X="28.393841796875" Y="0.17493572998" />
                  <Point X="28.384068359375" Y="0.153471618652" />
                  <Point X="28.380005859375" Y="0.142926528931" />
                  <Point X="28.37316015625" Y="0.121426742554" />
                  <Point X="28.370376953125" Y="0.110472045898" />
                  <Point X="28.3608984375" Y="0.060977359772" />
                  <Point X="28.351875" Y="0.013861531258" />
                  <Point X="28.350603515625" Y="0.004966303349" />
                  <Point X="28.3485859375" Y="-0.026261442184" />
                  <Point X="28.35028125" Y="-0.06293963623" />
                  <Point X="28.351875" Y="-0.076421661377" />
                  <Point X="28.361353515625" Y="-0.12591619873" />
                  <Point X="28.370376953125" Y="-0.173032180786" />
                  <Point X="28.37316015625" Y="-0.183986572266" />
                  <Point X="28.380005859375" Y="-0.20548651123" />
                  <Point X="28.384068359375" Y="-0.21603175354" />
                  <Point X="28.393841796875" Y="-0.237495864868" />
                  <Point X="28.399130859375" Y="-0.247487182617" />
                  <Point X="28.41085546875" Y="-0.266768035889" />
                  <Point X="28.417291015625" Y="-0.276057556152" />
                  <Point X="28.4457265625" Y="-0.312292266846" />
                  <Point X="28.472796875" Y="-0.346785400391" />
                  <Point X="28.478720703125" Y="-0.353635559082" />
                  <Point X="28.50114453125" Y="-0.375809509277" />
                  <Point X="28.530177734375" Y="-0.398725524902" />
                  <Point X="28.54149609375" Y="-0.406404571533" />
                  <Point X="28.588890625" Y="-0.433799041748" />
                  <Point X="28.634005859375" Y="-0.45987713623" />
                  <Point X="28.639498046875" Y="-0.462816009521" />
                  <Point X="28.659154296875" Y="-0.47201473999" />
                  <Point X="28.683025390625" Y="-0.481026916504" />
                  <Point X="28.6919921875" Y="-0.483912719727" />
                  <Point X="29.326017578125" Y="-0.653799499512" />
                  <Point X="29.784880859375" Y="-0.776751403809" />
                  <Point X="29.761619140625" Y="-0.931034729004" />
                  <Point X="29.727802734375" Y="-1.079219604492" />
                  <Point X="29.209197265625" Y="-1.010944030762" />
                  <Point X="28.436783203125" Y="-0.909253540039" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042419434" />
                  <Point X="28.36672265625" Y="-0.910840820312" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.26146484375" Y="-0.932893981934" />
                  <Point X="28.17291796875" Y="-0.952140014648" />
                  <Point X="28.157875" Y="-0.95674230957" />
                  <Point X="28.12875390625" Y="-0.968366943359" />
                  <Point X="28.114677734375" Y="-0.975388977051" />
                  <Point X="28.0868515625" Y="-0.992281005859" />
                  <Point X="28.074125" Y="-1.001530822754" />
                  <Point X="28.050375" Y="-1.02200213623" />
                  <Point X="28.039349609375" Y="-1.033223510742" />
                  <Point X="27.983126953125" Y="-1.100842285156" />
                  <Point X="27.92960546875" Y="-1.165211303711" />
                  <Point X="27.92132421875" Y="-1.176851318359" />
                  <Point X="27.90660546875" Y="-1.201233032227" />
                  <Point X="27.90016796875" Y="-1.213974853516" />
                  <Point X="27.88882421875" Y="-1.241361083984" />
                  <Point X="27.884365234375" Y="-1.254927368164" />
                  <Point X="27.877533203125" Y="-1.282576782227" />
                  <Point X="27.87516015625" Y="-1.296659912109" />
                  <Point X="27.8671015625" Y="-1.384229614258" />
                  <Point X="27.859431640625" Y="-1.467590332031" />
                  <Point X="27.859291015625" Y="-1.483314697266" />
                  <Point X="27.861607421875" Y="-1.514580932617" />
                  <Point X="27.864064453125" Y="-1.530122924805" />
                  <Point X="27.871794921875" Y="-1.561743530273" />
                  <Point X="27.87678515625" Y="-1.576667114258" />
                  <Point X="27.889158203125" Y="-1.605480957031" />
                  <Point X="27.896541015625" Y="-1.61937097168" />
                  <Point X="27.948017578125" Y="-1.699440185547" />
                  <Point X="27.997021484375" Y="-1.775661499023" />
                  <Point X="28.001744140625" Y="-1.782354370117" />
                  <Point X="28.01980078125" Y="-1.804456176758" />
                  <Point X="28.043494140625" Y="-1.828122680664" />
                  <Point X="28.052798828125" Y="-1.836277832031" />
                  <Point X="28.641177734375" Y="-2.287757568359" />
                  <Point X="29.087171875" Y="-2.629980712891" />
                  <Point X="29.045478515625" Y="-2.697446777344" />
                  <Point X="29.001275390625" Y="-2.760252441406" />
                  <Point X="28.53639453125" Y="-2.491853027344" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.66040234375" Y="-2.046344360352" />
                  <Point X="27.555017578125" Y="-2.027311889648" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.03513659668" />
                  <Point X="27.415068359375" Y="-2.044960571289" />
                  <Point X="27.40058984375" Y="-2.051109130859" />
                  <Point X="27.30862109375" Y="-2.09951171875" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.208970703125" Y="-2.153169433594" />
                  <Point X="27.1860390625" Y="-2.1700625" />
                  <Point X="27.175208984375" Y="-2.179374267578" />
                  <Point X="27.15425" Y="-2.200333496094" />
                  <Point X="27.144939453125" Y="-2.211162353516" />
                  <Point X="27.128046875" Y="-2.234093017578" />
                  <Point X="27.12046484375" Y="-2.246194824219" />
                  <Point X="27.0720625" Y="-2.338163818359" />
                  <Point X="27.025984375" Y="-2.425712890625" />
                  <Point X="27.0198359375" Y="-2.440193115234" />
                  <Point X="27.01001171875" Y="-2.469971191406" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517442626953" />
                  <Point X="27.000279296875" Y="-2.533133789062" />
                  <Point X="27.00068359375" Y="-2.564484375" />
                  <Point X="27.0021875" Y="-2.580143798828" />
                  <Point X="27.022181640625" Y="-2.690849365234" />
                  <Point X="27.04121484375" Y="-2.796234130859" />
                  <Point X="27.04301953125" Y="-2.80423046875" />
                  <Point X="27.051236328125" Y="-2.831536132812" />
                  <Point X="27.064068359375" Y="-2.862474365234" />
                  <Point X="27.069546875" Y="-2.873578125" />
                  <Point X="27.447638671875" Y="-3.528454833984" />
                  <Point X="27.73589453125" Y="-4.027724853516" />
                  <Point X="27.723755859375" Y="-4.036081787109" />
                  <Point X="27.35887890625" Y="-3.560565429688" />
                  <Point X="26.83391796875" Y="-2.876422607422" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.664115234375" Y="-2.750450683594" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.288982421875" Y="-2.657504882812" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.951654296875" Y="-2.799080810547" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.85265625" Y="-2.883086914062" />
                  <Point X="25.832185546875" Y="-2.9068359375" />
                  <Point X="25.822935546875" Y="-2.919561767578" />
                  <Point X="25.80604296875" Y="-2.947387939453" />
                  <Point X="25.79901953125" Y="-2.961467041016" />
                  <Point X="25.78739453125" Y="-2.990589355469" />
                  <Point X="25.78279296875" Y="-3.005632568359" />
                  <Point X="25.755224609375" Y="-3.132474609375" />
                  <Point X="25.728978515625" Y="-3.253220458984" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677001953" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520263672" />
                  <Point X="25.832705078125" Y="-4.149396484375" />
                  <Point X="25.83308984375" Y="-4.152323730469" />
                  <Point X="25.655068359375" Y="-3.487937255859" />
                  <Point X="25.652607421875" Y="-3.480123291016" />
                  <Point X="25.642146484375" Y="-3.453577636719" />
                  <Point X="25.6267890625" Y="-3.423814697266" />
                  <Point X="25.62041015625" Y="-3.413208984375" />
                  <Point X="25.536529296875" Y="-3.292354492188" />
                  <Point X="25.456681640625" Y="-3.17730859375" />
                  <Point X="25.446669921875" Y="-3.165170410156" />
                  <Point X="25.42478515625" Y="-3.142715576172" />
                  <Point X="25.412912109375" Y="-3.132398925781" />
                  <Point X="25.38665625" Y="-3.113154785156" />
                  <Point X="25.373244140625" Y="-3.104938232422" />
                  <Point X="25.345244140625" Y="-3.090830078125" />
                  <Point X="25.33065625" Y="-3.084938476562" />
                  <Point X="25.200857421875" Y="-3.044653564453" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.063376953125" Y="-3.003109130859" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.805216796875" Y="-3.04658984375" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.667068359375" Y="-3.090830078125" />
                  <Point X="24.639068359375" Y="-3.104938232422" />
                  <Point X="24.62565625" Y="-3.113154785156" />
                  <Point X="24.599400390625" Y="-3.132398925781" />
                  <Point X="24.587525390625" Y="-3.142717041016" />
                  <Point X="24.565640625" Y="-3.165172851562" />
                  <Point X="24.555630859375" Y="-3.177310546875" />
                  <Point X="24.471751953125" Y="-3.298164794922" />
                  <Point X="24.391904296875" Y="-3.4132109375" />
                  <Point X="24.38753125" Y="-3.420131591797" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213623047" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.16188671875" Y="-4.217026367187" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.849192339257" Y="4.213928766327" />
                  <Point X="21.963409548511" Y="3.660430247941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.640101925614" Y="4.596121978653" />
                  <Point X="23.525737226839" Y="4.524658983448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.911473459725" Y="4.14082438139" />
                  <Point X="22.010938061333" Y="3.578107410626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.965269812423" Y="4.687287477025" />
                  <Point X="23.537529310472" Y="4.420005546786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.000625923961" Y="4.084511075619" />
                  <Point X="22.058466910606" Y="3.495784783548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.298307213771" Y="3.020784286439" />
                  <Point X="21.228802576601" Y="2.977352968857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.219161903707" Y="4.733914915242" />
                  <Point X="23.512454204268" Y="4.292314933104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.105139807027" Y="4.037796649676" />
                  <Point X="22.105995957892" Y="3.413462280203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.391486360925" Y="2.966987131413" />
                  <Point X="21.059199945509" Y="2.759351534365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.439749743272" Y="4.75973154727" />
                  <Point X="22.153525005177" Y="3.331139776858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.484665546061" Y="2.913190000122" />
                  <Point X="20.932101803835" Y="2.567909852629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.637756592904" Y="4.771438010754" />
                  <Point X="22.201054052463" Y="3.248817273513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.57784474138" Y="2.859392875193" />
                  <Point X="20.829174132166" Y="2.39157155682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.663468019541" Y="4.675482344934" />
                  <Point X="22.238303832405" Y="3.160071571045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.671023936699" Y="2.805595750264" />
                  <Point X="20.808456435946" Y="2.26660375509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.689179201106" Y="4.579526525976" />
                  <Point X="22.250233631046" Y="3.055504188271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.765992081313" Y="2.752916484922" />
                  <Point X="20.888920650892" Y="2.204861428616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.714890172701" Y="4.483570575814" />
                  <Point X="22.234140969896" Y="2.933426429208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.902397326362" Y="2.726129993673" />
                  <Point X="20.969384865838" Y="2.143119102142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.740601144296" Y="4.387614625652" />
                  <Point X="21.049849053647" Y="2.081376758711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.53033433503" Y="4.769072744408" />
                  <Point X="25.34246917307" Y="4.651681562408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.766312115891" Y="4.29165867549" />
                  <Point X="21.130313031771" Y="2.019634284254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.68387403567" Y="4.75299304932" />
                  <Point X="25.306416324808" Y="4.51713129416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.802110532141" Y="4.202006060333" />
                  <Point X="21.210777009895" Y="1.957891809797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.835908825218" Y="4.735972981413" />
                  <Point X="25.27036333564" Y="4.382580937865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.866409165042" Y="4.130162357082" />
                  <Point X="21.29124098802" Y="1.89614933534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.965219853139" Y="4.704753531306" />
                  <Point X="25.23271542487" Y="4.247033963942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.977160392516" Y="4.087345456498" />
                  <Point X="21.371704966144" Y="1.834406860883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.514467705293" Y="1.298745569263" />
                  <Point X="20.329876697977" Y="1.183400306153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.094530881061" Y="4.673534081198" />
                  <Point X="21.452168944268" Y="1.772664386426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.662542697301" Y="1.279251145233" />
                  <Point X="20.293333323341" Y="1.048543523008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.223841592783" Y="4.642314433508" />
                  <Point X="21.516261276314" Y="1.700691772095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.810617677499" Y="1.259756713824" />
                  <Point X="20.26273090095" Y="0.917399058843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.353152084866" Y="4.611094648571" />
                  <Point X="21.559619304612" Y="1.615762926818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.958692656415" Y="1.240262281614" />
                  <Point X="20.244465188988" Y="0.793963426927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.478917265796" Y="4.577659507352" />
                  <Point X="21.578536667663" Y="1.515561858888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.10676763533" Y="1.220767849403" />
                  <Point X="20.226199699719" Y="0.670527934166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.592348521897" Y="4.536517274519" />
                  <Point X="21.539856172616" Y="1.379369654697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.2601678012" Y="1.204600963314" />
                  <Point X="20.260023757021" Y="0.579641602611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.705779770677" Y="4.495375037111" />
                  <Point X="20.38549373501" Y="0.546021998122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.819211008652" Y="4.454232792952" />
                  <Point X="20.510963712999" Y="0.512402393633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.925496547427" Y="4.408625420364" />
                  <Point X="20.636433690988" Y="0.478782789143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.028030504493" Y="4.360673799345" />
                  <Point X="20.761903668977" Y="0.445163184654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.130563942451" Y="4.312721853951" />
                  <Point X="20.887373656414" Y="0.411543586069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.233097151026" Y="4.264769765223" />
                  <Point X="21.012843644342" Y="0.37792398779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.32752345344" Y="4.211751919297" />
                  <Point X="21.138313632269" Y="0.344304389511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.420297867848" Y="4.157701859182" />
                  <Point X="21.263783620197" Y="0.310684791232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.513071841144" Y="4.10365152343" />
                  <Point X="21.389253608125" Y="0.277065192953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.605845655524" Y="4.049601088376" />
                  <Point X="21.512355866253" Y="0.241966072889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.691962246019" Y="3.991390758149" />
                  <Point X="21.603598355516" Y="0.186958759702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.28915668802" Y="-0.634395553189" />
                  <Point X="20.217091856076" Y="-0.679426658022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.775810662721" Y="3.931763115632" />
                  <Point X="21.666397746469" Y="0.114178226107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.603013662333" Y="-0.550297897378" />
                  <Point X="20.231798621768" Y="-0.782258799194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.767249247204" Y="3.814391401147" />
                  <Point X="21.699140787592" Y="0.022616400674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.91687092986" Y="-0.466200058345" />
                  <Point X="20.246505086652" Y="-0.885091128333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.666071894962" Y="3.639146826304" />
                  <Point X="21.698028601189" Y="-0.090100518842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.230728400796" Y="-0.382102092209" />
                  <Point X="20.262550550854" Y="-0.987086757835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.564894542719" Y="3.463902251461" />
                  <Point X="20.2872264938" Y="-1.083689465678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.463717330051" Y="3.288657763834" />
                  <Point X="20.311902436746" Y="-1.180292173521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.362540306104" Y="3.113413394133" />
                  <Point X="20.336578750108" Y="-1.276894649903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.261363282158" Y="2.938169024432" />
                  <Point X="20.543166985408" Y="-1.259825941518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.160186258211" Y="2.762924654731" />
                  <Point X="20.770292354638" Y="-1.229924207564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.060182974361" Y="2.588413719243" />
                  <Point X="20.997417668635" Y="-1.200022508124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.017691499156" Y="2.44984015035" />
                  <Point X="21.224542685282" Y="-1.170120994489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.017273660754" Y="2.33755710762" />
                  <Point X="21.451667701929" Y="-1.140219480854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.038923242799" Y="2.239063319602" />
                  <Point X="21.678792718576" Y="-1.110317967219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.086717860469" Y="2.156906763051" />
                  <Point X="21.857158562482" Y="-1.110884566254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.148624295578" Y="2.083568248717" />
                  <Point X="21.95502545692" Y="-1.161752491672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.232099120147" Y="2.023707159927" />
                  <Point X="22.019204045473" Y="-1.233671206956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.346636533945" Y="1.983256131137" />
                  <Point X="22.049585629478" Y="-1.326708634569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.544569731029" Y="1.994916571401" />
                  <Point X="22.037102718822" Y="-1.446530771179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.953231136099" Y="2.763123962327" />
                  <Point X="21.685652282264" Y="-1.778163326019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.008835372653" Y="2.685847397266" />
                  <Point X="20.899302443353" Y="-2.381551188553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.062076853426" Y="2.607094418532" />
                  <Point X="20.86241674358" Y="-2.516621880184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.111265060437" Y="2.52580867325" />
                  <Point X="20.911085343432" Y="-2.598232312056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.760114703479" Y="2.194363628955" />
                  <Point X="20.959753943283" Y="-2.679842743928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.111348812262" Y="1.67694775865" />
                  <Point X="21.010753356979" Y="-2.759996721663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.020484286384" Y="1.508147352934" />
                  <Point X="21.068536340799" Y="-2.835911854331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.001349014246" Y="1.384168359515" />
                  <Point X="21.126319324619" Y="-2.911826987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.019017613133" Y="1.283186977131" />
                  <Point X="21.184102679025" Y="-2.9877418881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.052222372203" Y="1.191913665092" />
                  <Point X="22.373200935603" Y="-2.356732779475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.103868620044" Y="1.112163874189" />
                  <Point X="22.5308628493" Y="-2.370236629962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.17290254243" Y="1.043279108211" />
                  <Point X="22.615395818228" Y="-2.429436516772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.26633414036" Y="0.989639701938" />
                  <Point X="22.668147950896" Y="-2.50849527414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.395603583032" Y="0.958394266483" />
                  <Point X="22.68874010163" Y="-2.607649818576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.583352993047" Y="0.963691170321" />
                  <Point X="22.645465670713" Y="-2.746712632497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.810478196021" Y="0.993592800386" />
                  <Point X="22.54428786254" Y="-2.921957492237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.037603398994" Y="1.023494430451" />
                  <Point X="22.443110054367" Y="-3.097202351977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.26472855566" Y="1.053396031579" />
                  <Point X="22.341932246194" Y="-3.272447211717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.491853614048" Y="1.083297571297" />
                  <Point X="22.240754623585" Y="-3.447691955503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.706190868444" Y="1.105208404223" />
                  <Point X="22.139577331598" Y="-3.622936492695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.729861456275" Y="1.00797748078" />
                  <Point X="28.8654425499" Y="0.467828598976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.392698801026" Y="0.172425518997" />
                  <Point X="22.038400039611" Y="-3.798181029886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.753255255429" Y="0.910573600577" />
                  <Point X="29.179299518038" Y="0.551926250929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.356450639092" Y="0.037753205222" />
                  <Point X="22.084157690254" Y="-3.881610424703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.76915012429" Y="0.808483868661" />
                  <Point X="29.493156681082" Y="0.636024024673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.351995797517" Y="-0.077052437065" />
                  <Point X="22.164469504665" Y="-3.943447981601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.371375992938" Y="-0.176964295232" />
                  <Point X="22.244781319075" Y="-4.005285538499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.409775286987" Y="-0.264991701564" />
                  <Point X="22.333903540818" Y="-4.061617741878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.467977680698" Y="-0.340644757846" />
                  <Point X="22.919892742841" Y="-3.807472997303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.875925451226" Y="-3.834946810319" />
                  <Point X="22.423949983604" Y="-4.117372427851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.541764393108" Y="-0.406559651003" />
                  <Point X="23.140601495839" Y="-3.781580810176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.634924501937" Y="-0.460368702494" />
                  <Point X="23.302855137677" Y="-3.792215430475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.751150978669" Y="-0.499764287623" />
                  <Point X="23.441411459625" Y="-3.817657779696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.876620943142" Y="-0.533383900559" />
                  <Point X="28.224367113464" Y="-0.94095732839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.998947218873" Y="-1.08181531183" />
                  <Point X="24.903433134935" Y="-3.016107191287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.486842684064" Y="-3.276421796334" />
                  <Point X="23.528553805927" Y="-3.875227146558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.002090907614" Y="-0.567003513494" />
                  <Point X="28.451314743342" Y="-0.91116665831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.881294463285" Y="-1.267354861284" />
                  <Point X="25.091391626791" Y="-3.010679638614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.366395475482" Y="-3.463707513819" />
                  <Point X="23.603142886103" Y="-3.940640664688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.127560872087" Y="-0.600623126429" />
                  <Point X="28.599389647491" Y="-0.93066113724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.866709524226" Y="-1.388490491021" />
                  <Point X="25.211171638917" Y="-3.047854728385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.327151197835" Y="-3.600252008478" />
                  <Point X="23.677732259995" Y="-4.006053999284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.253030836559" Y="-0.634242739365" />
                  <Point X="28.747464551639" Y="-0.95015561617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.860837001179" Y="-1.504181999011" />
                  <Point X="25.330924470353" Y="-3.085046802536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.291098529461" Y="-3.734802164319" />
                  <Point X="23.750937411619" Y="-4.072332291952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.378500836606" Y="-0.667862330071" />
                  <Point X="28.895539455788" Y="-0.9696500951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.886797018758" Y="-1.59998232797" />
                  <Point X="26.178074222224" Y="-2.667710834433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.902701735764" Y="-2.839782661581" />
                  <Point X="25.421908237857" Y="-3.14021578302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.255045861088" Y="-3.869352320159" />
                  <Point X="23.802065873781" Y="-4.152405631256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.503970886125" Y="-0.701481889863" />
                  <Point X="29.043614359937" Y="-0.98914457403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.936234551158" Y="-1.681112277459" />
                  <Point X="26.388306837812" Y="-2.6483648645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.777486465222" Y="-3.030047794853" />
                  <Point X="25.482435933703" Y="-3.214415829264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.218993192715" Y="-4.003902476" />
                  <Point X="23.827148281544" Y="-4.248754351692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.629440935644" Y="-0.735101449656" />
                  <Point X="29.191689264086" Y="-1.00863905296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.987613911696" Y="-1.761028838057" />
                  <Point X="27.560024770329" Y="-2.028216187707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.076010795511" Y="-2.330661686466" />
                  <Point X="26.532460230984" Y="-2.670309775452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.749312156564" Y="-3.159675005164" />
                  <Point X="25.536665815489" Y="-3.292551186498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.182940524341" Y="-4.13845263184" />
                  <Point X="23.846968047666" Y="-4.3483915356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.754910985163" Y="-0.768721009448" />
                  <Point X="29.339764267904" Y="-1.028133469609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.050161757143" Y="-1.833966554729" />
                  <Point X="27.699101425435" Y="-2.053333396684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.006173589043" Y="-2.48632276473" />
                  <Point X="26.624371254645" Y="-2.724899341983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.725005993391" Y="-3.286885129913" />
                  <Point X="25.590896482687" Y="-3.370686052951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.146887715677" Y="-4.273002875344" />
                  <Point X="23.866787705152" Y="-4.448028787391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.770688190422" Y="-0.870884265742" />
                  <Point X="29.487839285088" Y="-1.047627877907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.130419166424" Y="-1.895838107725" />
                  <Point X="27.827678213849" Y="-2.085011650557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.005545577262" Y="-2.598737138364" />
                  <Point X="26.712732445842" Y="-2.781707090025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.733223789148" Y="-3.393772029523" />
                  <Point X="25.641017214179" Y="-3.451389092266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.110834710088" Y="-4.4075532419" />
                  <Point X="23.880305889551" Y="-4.551603636586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.746304839562" Y="-0.998142622711" />
                  <Point X="29.635914302271" Y="-1.067122286205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.210883283673" Y="-1.957580495247" />
                  <Point X="27.922800963066" Y="-2.137594308221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.023725737639" Y="-2.699398861652" />
                  <Point X="26.797985375981" Y="-2.840457095141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.746850912376" Y="-3.497278806183" />
                  <Point X="25.670375857516" Y="-3.54506572415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.074781704499" Y="-4.542103608457" />
                  <Point X="23.86653491587" Y="-4.672230644304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.291347400922" Y="-2.019322882769" />
                  <Point X="28.015980195611" Y="-2.191391409889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.042057108628" Y="-2.799966098061" />
                  <Point X="26.861719112411" Y="-2.912653784882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.760478035603" Y="-3.600785582843" />
                  <Point X="25.696087085556" Y="-3.641021514068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.03872869891" Y="-4.676653975013" />
                  <Point X="23.924259957153" Y="-4.748181983489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.371811518171" Y="-2.081065270291" />
                  <Point X="28.109159428156" Y="-2.245188511557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.078555389382" Y="-2.88918138934" />
                  <Point X="26.91981890473" Y="-2.988370953629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.774105158831" Y="-3.704292359503" />
                  <Point X="25.721798313597" Y="-3.736977303985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.45227563542" Y="-2.142807657813" />
                  <Point X="28.202338660701" Y="-2.298985613224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.126084209032" Y="-2.971504034928" />
                  <Point X="26.977918697049" Y="-3.064088122376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.787732282058" Y="-3.807799136163" />
                  <Point X="25.747509541637" Y="-3.832933093902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.532739752669" Y="-2.204550045335" />
                  <Point X="28.295517893246" Y="-2.352782714892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.173613028681" Y="-3.053826680516" />
                  <Point X="27.036018489368" Y="-3.139805291123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.801359405286" Y="-3.911305912823" />
                  <Point X="25.773220769677" Y="-3.928888883819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.613203869918" Y="-2.266292432857" />
                  <Point X="28.388697125791" Y="-2.406579816559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.22114184833" Y="-3.136149326104" />
                  <Point X="27.094118281687" Y="-3.21552245987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.814986528513" Y="-4.014812689482" />
                  <Point X="25.798931997717" Y="-4.024844673736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.693668050032" Y="-2.328034781097" />
                  <Point X="28.481876358336" Y="-2.460376918227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.26867066798" Y="-3.218471971692" />
                  <Point X="27.152218074006" Y="-3.291239628617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.828613651741" Y="-4.118319466142" />
                  <Point X="25.824643225757" Y="-4.120800463654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.774132263649" Y="-2.389777108401" />
                  <Point X="28.575055584524" Y="-2.514174023867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.316199487629" Y="-3.300794617281" />
                  <Point X="27.210317866325" Y="-3.366956797364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.854596477267" Y="-2.451519435706" />
                  <Point X="28.668234801748" Y="-2.567971135108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.363728307279" Y="-3.383117262869" />
                  <Point X="27.268417658644" Y="-3.442673966111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.935060690884" Y="-2.51326176301" />
                  <Point X="28.761414018972" Y="-2.621768246349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.411257126928" Y="-3.465439908457" />
                  <Point X="27.326517450964" Y="-3.518391134858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.015524904501" Y="-2.575004090315" />
                  <Point X="28.854593236196" Y="-2.675565357591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.458786039867" Y="-3.547762495751" />
                  <Point X="27.384617273647" Y="-3.594108284631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.074813491607" Y="-2.649978417634" />
                  <Point X="28.94777245342" Y="-2.729362468832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.506315257277" Y="-3.630084892791" />
                  <Point X="27.442717134509" Y="-3.669825410547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.553844474687" Y="-3.71240728983" />
                  <Point X="27.500816995371" Y="-3.745542536464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.601373692096" Y="-3.79472968687" />
                  <Point X="27.558916856233" Y="-3.82125966238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.648902909506" Y="-3.87705208391" />
                  <Point X="27.617016717095" Y="-3.896976788297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.696432126916" Y="-3.95937448095" />
                  <Point X="27.675116577957" Y="-3.972693914213" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.676763671875" Y="-4.303012207031" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544433594" />
                  <Point X="25.380439453125" Y="-3.400689941406" />
                  <Point X="25.300591796875" Y="-3.285644042969" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.144537109375" Y="-3.226114990234" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.861537109375" Y="-3.228051269531" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285644042969" />
                  <Point X="24.627841796875" Y="-3.406498291016" />
                  <Point X="24.547994140625" Y="-3.521544433594" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.3454140625" Y="-4.266202148438" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.044458984375" Y="-4.966152832031" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.749724609375" Y="-4.899463867188" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.666146484375" Y="-4.73868359375" />
                  <Point X="23.6908515625" Y="-4.5510390625" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.65930859375" Y="-4.378865234375" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.49421484375" Y="-4.097828125" />
                  <Point X="23.38045703125" Y="-3.998064208984" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.192154296875" Y="-3.975367431641" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.877962890625" Y="-4.062096923828" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.630587890625" Y="-4.30023046875" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.3562734375" Y="-4.298941894531" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.936419921875" Y="-4.007652099609" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.112583984375" Y="-3.289693115234" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597592285156" />
                  <Point X="22.48601953125" Y="-2.568762939453" />
                  <Point X="22.468677734375" Y="-2.551419189453" />
                  <Point X="22.43984765625" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.7794921875" Y="-2.906522460938" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.00587109375" Y="-3.0672890625" />
                  <Point X="20.838302734375" Y="-2.847136962891" />
                  <Point X="20.689359375" Y="-2.597381591797" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.16891796875" Y="-1.935178466797" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396014770508" />
                  <Point X="21.8618828125" Y="-1.366267456055" />
                  <Point X="21.85967578125" Y="-1.334595703125" />
                  <Point X="21.838841796875" Y="-1.310638671875" />
                  <Point X="21.812359375" Y="-1.295052124023" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.986638671875" Y="-1.393081054688" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.1381484375" Y="-1.267778198242" />
                  <Point X="20.072607421875" Y="-1.011188232422" />
                  <Point X="20.033203125" Y="-0.735672485352" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="20.682388671875" Y="-0.332326934814" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.472322265625" Y="-0.111557266235" />
                  <Point X="21.49767578125" Y="-0.090645690918" />
                  <Point X="21.50983984375" Y="-0.060637718201" />
                  <Point X="21.516599609375" Y="-0.031280040741" />
                  <Point X="21.50961328125" Y="-0.001188552022" />
                  <Point X="21.49767578125" Y="0.028085613251" />
                  <Point X="21.471638671875" Y="0.049471488953" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.718921875" Y="0.259977722168" />
                  <Point X="20.001814453125" Y="0.452126068115" />
                  <Point X="20.040115234375" Y="0.710968994141" />
                  <Point X="20.08235546875" Y="0.996414978027" />
                  <Point X="20.161677734375" Y="1.289141601563" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.701841796875" Y="1.465716918945" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.299765625" Y="1.405788818359" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056518555" />
                  <Point X="21.3608828125" Y="1.443786743164" />
                  <Point X="21.373509765625" Y="1.474271606445" />
                  <Point X="21.385529296875" Y="1.503291137695" />
                  <Point X="21.389287109375" Y="1.524605834961" />
                  <Point X="21.38368359375" Y="1.54551184082" />
                  <Point X="21.368447265625" Y="1.574780029297" />
                  <Point X="21.353943359375" Y="1.602641601562" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.92496484375" Y="1.93771472168" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.675859375" Y="2.505817871094" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="21.050107421875" Y="3.057087890625" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.506185546875" Y="3.120158691406" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.905314453125" Y="2.916600341797" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775146484" />
                  <Point X="21.986748046875" Y="2.927404052734" />
                  <Point X="22.017857421875" Y="2.958513427734" />
                  <Point X="22.04747265625" Y="2.988127441406" />
                  <Point X="22.0591015625" Y="3.006382080078" />
                  <Point X="22.061927734375" Y="3.027841552734" />
                  <Point X="22.05809375" Y="3.071669433594" />
                  <Point X="22.054443359375" Y="3.113391113281" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.86400390625" Y="3.452607666016" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.961267578125" Y="3.955168457031" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.57805859375" Y="4.35819140625" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.929724609375" Y="4.421087890625" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.097533203125" Y="4.248267089844" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.18619140625" Y="4.22225" />
                  <Point X="23.236998046875" Y="4.243295898438" />
                  <Point X="23.285365234375" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.294488769531" />
                  <Point X="23.330455078125" Y="4.3469375" />
                  <Point X="23.346197265625" Y="4.396865234375" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.327171875" Y="4.577258789062" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.66184765625" Y="4.799544921875" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.433099609375" Y="4.95025" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.858859375" Y="4.680381347656" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.148697265625" Y="4.662624023438" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.537091796875" Y="4.959404296875" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.192125" Y="4.845430664062" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.724751953125" Y="4.690605957031" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="27.13994140625" Y="4.518087402344" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.540513671875" Y="4.307557128906" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="27.922861328125" Y="4.060335449219" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.671275390625" Y="3.268162353516" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491514892578" />
                  <Point X="27.213853515625" Y="2.450383300781" />
                  <Point X="27.2033828125" Y="2.411228515625" />
                  <Point X="27.202044921875" Y="2.392326660156" />
                  <Point X="27.206333984375" Y="2.356759521484" />
                  <Point X="27.210416015625" Y="2.322901855469" />
                  <Point X="27.21868359375" Y="2.300811279297" />
                  <Point X="27.24069140625" Y="2.268377441406" />
                  <Point X="27.261642578125" Y="2.237502685547" />
                  <Point X="27.27494140625" Y="2.224203613281" />
                  <Point X="27.307375" Y="2.202195800781" />
                  <Point X="27.33825" Y="2.18124609375" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.395904296875" Y="2.16869140625" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.489796875" Y="2.176945556641" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.268853515625" Y="2.612623779297" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.088697265625" Y="2.900166992188" />
                  <Point X="29.20259765625" Y="2.741874023438" />
                  <Point X="29.308701171875" Y="2.566533447266" />
                  <Point X="29.387513671875" Y="2.436295654297" />
                  <Point X="28.869791015625" Y="2.039032226562" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583833129883" />
                  <Point X="28.24976953125" Y="1.545214477539" />
                  <Point X="28.22158984375" Y="1.508451782227" />
                  <Point X="28.21312109375" Y="1.491501464844" />
                  <Point X="28.20209375" Y="1.452071533203" />
                  <Point X="28.191595703125" Y="1.414536865234" />
                  <Point X="28.190779296875" Y="1.390965087891" />
                  <Point X="28.19983203125" Y="1.347094360352" />
                  <Point X="28.20844921875" Y="1.305332275391" />
                  <Point X="28.215646484375" Y="1.287956298828" />
                  <Point X="28.240267578125" Y="1.250534301758" />
                  <Point X="28.263703125" Y="1.214910888672" />
                  <Point X="28.280947265625" Y="1.19882019043" />
                  <Point X="28.316626953125" Y="1.178736206055" />
                  <Point X="28.35058984375" Y="1.159617675781" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.416806640625" Y="1.147244262695" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.16718359375" Y="1.232193481445" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.890271484375" Y="1.152320922852" />
                  <Point X="29.939193359375" Y="0.951366699219" />
                  <Point X="29.97262890625" Y="0.736618041992" />
                  <Point X="29.997859375" Y="0.57455645752" />
                  <Point X="29.407205078125" Y="0.416290740967" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819198608" />
                  <Point X="28.681693359375" Y="0.205424575806" />
                  <Point X="28.636578125" Y="0.179346679688" />
                  <Point X="28.622265625" Y="0.166927154541" />
                  <Point X="28.593830078125" Y="0.130692443848" />
                  <Point X="28.566759765625" Y="0.09619947052" />
                  <Point X="28.556986328125" Y="0.074735412598" />
                  <Point X="28.5475078125" Y="0.025240736008" />
                  <Point X="28.538484375" Y="-0.021875146866" />
                  <Point X="28.538484375" Y="-0.040684932709" />
                  <Point X="28.547962890625" Y="-0.090179611206" />
                  <Point X="28.556986328125" Y="-0.13729548645" />
                  <Point X="28.566759765625" Y="-0.158759552002" />
                  <Point X="28.5951953125" Y="-0.194994094849" />
                  <Point X="28.622265625" Y="-0.229487228394" />
                  <Point X="28.636578125" Y="-0.24190675354" />
                  <Point X="28.68397265625" Y="-0.269301361084" />
                  <Point X="28.729087890625" Y="-0.295379272461" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.375193359375" Y="-0.470273742676" />
                  <Point X="29.998068359375" Y="-0.637172546387" />
                  <Point X="29.97574609375" Y="-0.785237915039" />
                  <Point X="29.948431640625" Y="-0.966413330078" />
                  <Point X="29.90559375" Y="-1.154129638672" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.184396484375" Y="-1.199318481445" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.3018203125" Y="-1.118558959961" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.185447265625" Y="-1.154697021484" />
                  <Point X="28.129224609375" Y="-1.222316040039" />
                  <Point X="28.075703125" Y="-1.286685058594" />
                  <Point X="28.064359375" Y="-1.314071166992" />
                  <Point X="28.05630078125" Y="-1.40164074707" />
                  <Point X="28.048630859375" Y="-1.485001464844" />
                  <Point X="28.056361328125" Y="-1.516622192383" />
                  <Point X="28.107837890625" Y="-1.59669140625" />
                  <Point X="28.156841796875" Y="-1.672912719727" />
                  <Point X="28.168462890625" Y="-1.685540771484" />
                  <Point X="28.756841796875" Y="-2.137020751953" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.281125" Y="-2.677555419922" />
                  <Point X="29.204134765625" Y="-2.802138183594" />
                  <Point X="29.115537109375" Y="-2.928023193359" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.44139453125" Y="-2.656397949219" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.62663671875" Y="-2.233319580078" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.397109375" Y="-2.267647460938" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334683105469" />
                  <Point X="27.24019921875" Y="-2.426652099609" />
                  <Point X="27.19412109375" Y="-2.514201171875" />
                  <Point X="27.1891640625" Y="-2.546374755859" />
                  <Point X="27.209158203125" Y="-2.657080322266" />
                  <Point X="27.22819140625" Y="-2.762465087891" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.612185546875" Y="-3.433455322266" />
                  <Point X="27.98667578125" Y="-4.082088867188" />
                  <Point X="27.92666015625" Y="-4.124956542969" />
                  <Point X="27.83530078125" Y="-4.1902109375" />
                  <Point X="27.73624609375" Y="-4.254328613281" />
                  <Point X="27.67977734375" Y="-4.290879394531" />
                  <Point X="27.208140625" Y="-3.676230224609" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.561365234375" Y="-2.910270996094" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.306392578125" Y="-2.846705566406" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.073126953125" Y="-2.945176513672" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045985595703" />
                  <Point X="25.940888671875" Y="-3.172827636719" />
                  <Point X="25.914642578125" Y="-3.293573486328" />
                  <Point X="25.9139296875" Y="-3.310719970703" />
                  <Point X="26.021080078125" Y="-4.124595703125" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.080779296875" Y="-4.944301269531" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.902837890625" Y="-4.979870605469" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#168" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.099001320624" Y="4.724602451013" Z="1.3" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.3" />
                  <Point X="-0.578869750749" Y="5.03119123621" Z="1.3" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.3" />
                  <Point X="-1.357806397926" Y="4.878968586704" Z="1.3" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.3" />
                  <Point X="-1.728556571075" Y="4.60201308957" Z="1.3" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.3" />
                  <Point X="-1.723388128706" Y="4.393252690368" Z="1.3" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.3" />
                  <Point X="-1.788291164805" Y="4.320770070718" Z="1.3" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.3" />
                  <Point X="-1.885534918028" Y="4.323897733978" Z="1.3" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.3" />
                  <Point X="-2.036764365853" Y="4.48280573272" Z="1.3" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.3" />
                  <Point X="-2.452380594318" Y="4.43317901197" Z="1.3" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.3" />
                  <Point X="-3.075310283697" Y="4.026128437953" Z="1.3" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.3" />
                  <Point X="-3.185453935701" Y="3.458887695732" Z="1.3" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.3" />
                  <Point X="-2.997874493648" Y="3.098591433586" Z="1.3" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.3" />
                  <Point X="-3.023654214502" Y="3.025149353384" Z="1.3" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.3" />
                  <Point X="-3.096485032021" Y="2.997690205525" Z="1.3" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.3" />
                  <Point X="-3.474971652565" Y="3.194740013045" Z="1.3" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.3" />
                  <Point X="-3.995512592293" Y="3.119070216922" Z="1.3" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.3" />
                  <Point X="-4.373479316101" Y="2.562303013891" Z="1.3" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.3" />
                  <Point X="-4.111630402498" Y="1.929327050382" Z="1.3" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.3" />
                  <Point X="-3.682058490139" Y="1.582972577373" Z="1.3" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.3" />
                  <Point X="-3.678842734788" Y="1.52468480465" Z="1.3" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.3" />
                  <Point X="-3.721426769708" Y="1.484754757443" Z="1.3" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.3" />
                  <Point X="-4.297789993677" Y="1.546569202375" Z="1.3" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.3" />
                  <Point X="-4.892738690327" Y="1.333498870703" Z="1.3" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.3" />
                  <Point X="-5.015501743284" Y="0.749568191571" Z="1.3" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.3" />
                  <Point X="-4.300177200798" Y="0.242961482507" Z="1.3" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.3" />
                  <Point X="-3.56302596409" Y="0.039675127649" Z="1.3" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.3" />
                  <Point X="-3.544296191367" Y="0.015270429838" Z="1.3" />
                  <Point X="-3.539556741714" Y="0" Z="1.3" />
                  <Point X="-3.544068409768" Y="-0.014536521223" Z="1.3" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.3" />
                  <Point X="-3.562342631033" Y="-0.039200855485" Z="1.3" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.3" />
                  <Point X="-4.336710587974" Y="-0.252750574249" Z="1.3" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.3" />
                  <Point X="-5.022450528877" Y="-0.711471686779" Z="1.3" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.3" />
                  <Point X="-4.916453850021" Y="-1.248872171566" Z="1.3" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.3" />
                  <Point X="-4.012991893278" Y="-1.411373385736" Z="1.3" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.3" />
                  <Point X="-3.206243624714" Y="-1.31446466982" Z="1.3" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.3" />
                  <Point X="-3.196433703667" Y="-1.33695726604" Z="1.3" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.3" />
                  <Point X="-3.867676063311" Y="-1.864230811479" Z="1.3" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.3" />
                  <Point X="-4.359741589539" Y="-2.591711530379" Z="1.3" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.3" />
                  <Point X="-4.040070236586" Y="-3.066292256805" Z="1.3" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.3" />
                  <Point X="-3.201665355042" Y="-2.918543743106" Z="1.3" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.3" />
                  <Point X="-2.564378812836" Y="-2.563951656295" Z="1.3" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.3" />
                  <Point X="-2.936873270062" Y="-3.23341302745" Z="1.3" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.3" />
                  <Point X="-3.10024153536" Y="-4.015989303634" Z="1.3" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.3" />
                  <Point X="-2.676205579876" Y="-4.310172776294" Z="1.3" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.3" />
                  <Point X="-2.335901320073" Y="-4.299388641202" Z="1.3" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.3" />
                  <Point X="-2.100414967645" Y="-4.072390298874" Z="1.3" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.3" />
                  <Point X="-1.817272784609" Y="-3.993980341062" Z="1.3" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.3" />
                  <Point X="-1.54490809602" Y="-4.104139819448" Z="1.3" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.3" />
                  <Point X="-1.39588741247" Y="-4.357340223605" Z="1.3" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.3" />
                  <Point X="-1.389582437169" Y="-4.700876914613" Z="1.3" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.3" />
                  <Point X="-1.26889078205" Y="-4.916606784025" Z="1.3" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.3" />
                  <Point X="-0.971224990995" Y="-4.983957013492" Z="1.3" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.3" />
                  <Point X="-0.612445861271" Y="-4.247863147788" Z="1.3" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.3" />
                  <Point X="-0.337238586566" Y="-3.40372705512" Z="1.3" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.3" />
                  <Point X="-0.129798651239" Y="-3.244524079766" Z="1.3" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.3" />
                  <Point X="0.123560428122" Y="-3.242587965522" Z="1.3" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.3" />
                  <Point X="0.333207272867" Y="-3.397918700648" Z="1.3" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.3" />
                  <Point X="0.62230899913" Y="-4.284672882028" Z="1.3" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.3" />
                  <Point X="0.905618922463" Y="-4.997785492762" Z="1.3" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.3" />
                  <Point X="1.085327364905" Y="-4.961861495701" Z="1.3" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.3" />
                  <Point X="1.064494557099" Y="-4.086789333399" Z="1.3" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.3" />
                  <Point X="0.983590391186" Y="-3.152167913664" Z="1.3" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.3" />
                  <Point X="1.098936098204" Y="-2.952343079598" Z="1.3" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.3" />
                  <Point X="1.304817566023" Y="-2.86521507199" Z="1.3" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.3" />
                  <Point X="1.528168407372" Y="-2.921049088223" Z="1.3" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.3" />
                  <Point X="2.162315308824" Y="-3.675388161711" Z="1.3" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.3" />
                  <Point X="2.757256680205" Y="-4.265023015216" Z="1.3" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.3" />
                  <Point X="2.949563363639" Y="-4.134363559744" Z="1.3" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.3" />
                  <Point X="2.649330306633" Y="-3.377175344392" Z="1.3" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.3" />
                  <Point X="2.252204452388" Y="-2.616913697938" Z="1.3" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.3" />
                  <Point X="2.278287520022" Y="-2.418659240712" Z="1.3" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.3" />
                  <Point X="2.41423913766" Y="-2.280613789945" Z="1.3" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.3" />
                  <Point X="2.611593105575" Y="-2.251243557102" Z="1.3" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.3" />
                  <Point X="3.410238252715" Y="-2.668419193642" Z="1.3" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.3" />
                  <Point X="4.150269017781" Y="-2.9255204382" Z="1.3" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.3" />
                  <Point X="4.317501958032" Y="-2.672560430404" Z="1.3" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.3" />
                  <Point X="3.781122847854" Y="-2.066072960546" Z="1.3" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.3" />
                  <Point X="3.143739681861" Y="-1.538371861065" Z="1.3" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.3" />
                  <Point X="3.099933342133" Y="-1.374941692535" Z="1.3" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.3" />
                  <Point X="3.16151243026" Y="-1.223003116205" Z="1.3" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.3" />
                  <Point X="3.306282450309" Y="-1.136138143262" Z="1.3" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.3" />
                  <Point X="4.171714726896" Y="-1.217610763186" Z="1.3" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.3" />
                  <Point X="4.948183124032" Y="-1.133973254541" Z="1.3" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.3" />
                  <Point X="5.019030235355" Y="-0.761411894577" Z="1.3" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.3" />
                  <Point X="4.381978799522" Y="-0.390697410515" Z="1.3" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.3" />
                  <Point X="3.702836600498" Y="-0.194732722875" Z="1.3" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.3" />
                  <Point X="3.628373055193" Y="-0.132845099923" Z="1.3" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.3" />
                  <Point X="3.590913503876" Y="-0.049494640899" Z="1.3" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.3" />
                  <Point X="3.590457946548" Y="0.047115890311" Z="1.3" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.3" />
                  <Point X="3.627006383207" Y="0.131103638652" Z="1.3" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.3" />
                  <Point X="3.700558813855" Y="0.193416121367" Z="1.3" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.3" />
                  <Point X="4.413989017784" Y="0.399274522254" Z="1.3" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.3" />
                  <Point X="5.015875325539" Y="0.775589934071" Z="1.3" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.3" />
                  <Point X="4.932696619912" Y="1.195427738366" Z="1.3" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.3" />
                  <Point X="4.154500533195" Y="1.313045898952" Z="1.3" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.3" />
                  <Point X="3.417200233077" Y="1.228093126633" Z="1.3" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.3" />
                  <Point X="3.334996951162" Y="1.253587182535" Z="1.3" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.3" />
                  <Point X="3.275881412019" Y="1.309294453606" Z="1.3" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.3" />
                  <Point X="3.242644037776" Y="1.388478576394" Z="1.3" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.3" />
                  <Point X="3.244089027575" Y="1.469883941461" Z="1.3" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.3" />
                  <Point X="3.283295714306" Y="1.54607637893" Z="1.3" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.3" />
                  <Point X="3.89407082143" Y="2.030644553973" Z="1.3" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.3" />
                  <Point X="4.345322506873" Y="2.623699843" Z="1.3" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.3" />
                  <Point X="4.123127021654" Y="2.960650458805" Z="1.3" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.3" />
                  <Point X="3.237696764165" Y="2.687205149396" Z="1.3" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.3" />
                  <Point X="2.470723194656" Y="2.256528284161" Z="1.3" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.3" />
                  <Point X="2.395733913635" Y="2.249611791389" Z="1.3" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.3" />
                  <Point X="2.329291825774" Y="2.274850448444" Z="1.3" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.3" />
                  <Point X="2.275908151791" Y="2.327733034608" Z="1.3" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.3" />
                  <Point X="2.24981791137" Y="2.394024530197" Z="1.3" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.3" />
                  <Point X="2.255999658497" Y="2.468746381226" Z="1.3" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.3" />
                  <Point X="2.708420203463" Y="3.27444231779" Z="1.3" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.3" />
                  <Point X="2.945680433728" Y="4.13236235749" Z="1.3" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.3" />
                  <Point X="2.559526809042" Y="4.382039972677" Z="1.3" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.3" />
                  <Point X="2.154965818492" Y="4.594659397847" Z="1.3" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.3" />
                  <Point X="1.735644835032" Y="4.76888973426" Z="1.3" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.3" />
                  <Point X="1.197701738729" Y="4.925314100069" Z="1.3" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.3" />
                  <Point X="0.536141559079" Y="5.040412558745" Z="1.3" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.3" />
                  <Point X="0.094243240929" Y="4.706845054938" Z="1.3" />
                  <Point X="0" Y="4.355124473572" Z="1.3" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>