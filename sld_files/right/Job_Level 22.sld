<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#159" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1677" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.729099609375" Y="-4.131279296875" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467376708984" />
                  <Point X="25.474599609375" Y="-3.369740722656" />
                  <Point X="25.37863671875" Y="-3.231476318359" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.197634765625" Y="-3.143124023438" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.858314453125" Y="-3.129581054688" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.565912109375" Y="-3.329113037109" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.21422265625" Y="-4.388759277344" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.802501953125" Y="-4.814948730469" />
                  <Point X="23.753583984375" Y="-4.802362304688" />
                  <Point X="23.765080078125" Y="-4.715032714844" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.51622265625" />
                  <Point X="23.758439453125" Y="-4.390280761719" />
                  <Point X="23.722962890625" Y="-4.211931152344" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.5798125" Y="-4.046537841797" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295898438" />
                  <Point X="23.387115234375" Y="-3.897994628906" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.228837890625" Y="-3.882567871094" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.85057421875" Y="-3.966142333984" />
                  <Point X="22.699376953125" Y="-4.067169921875" />
                  <Point X="22.68721484375" Y="-4.076821533203" />
                  <Point X="22.6648984375" Y="-4.099461425781" />
                  <Point X="22.533083984375" Y="-4.271245117188" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.449529296875" Y="-4.244947265625" />
                  <Point X="22.198287109375" Y="-4.089383789063" />
                  <Point X="22.03467578125" Y="-3.963409423828" />
                  <Point X="21.89527734375" Y="-3.856077392578" />
                  <Point X="22.268126953125" Y="-3.210283691406" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647655029297" />
                  <Point X="22.593412109375" Y="-2.616129150391" />
                  <Point X="22.59442578125" Y="-2.585194824219" />
                  <Point X="22.58544140625" Y="-2.555576660156" />
                  <Point X="22.571224609375" Y="-2.526747558594" />
                  <Point X="22.55319921875" Y="-2.501591308594" />
                  <Point X="22.5358515625" Y="-2.484242675781" />
                  <Point X="22.51069140625" Y="-2.466212890625" />
                  <Point X="22.481861328125" Y="-2.451995605469" />
                  <Point X="22.452244140625" Y="-2.443011474609" />
                  <Point X="22.421310546875" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450295166016" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.60508203125" Y="-2.897521240234" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="21.115630859375" Y="-3.05463671875" />
                  <Point X="20.917140625" Y="-2.793861083984" />
                  <Point X="20.79984375" Y="-2.597171386719" />
                  <Point X="20.693857421875" Y="-2.419449951172" />
                  <Point X="21.354951171875" Y="-1.912175170898" />
                  <Point X="21.894044921875" Y="-1.498513305664" />
                  <Point X="21.915423828125" Y="-1.475592651367" />
                  <Point X="21.933388671875" Y="-1.44846105957" />
                  <Point X="21.946142578125" Y="-1.419833618164" />
                  <Point X="21.95384765625" Y="-1.390086425781" />
                  <Point X="21.95665234375" Y="-1.359655761719" />
                  <Point X="21.954443359375" Y="-1.327985595703" />
                  <Point X="21.947443359375" Y="-1.298240966797" />
                  <Point X="21.931361328125" Y="-1.272257568359" />
                  <Point X="21.910529296875" Y="-1.248301513672" />
                  <Point X="21.887029296875" Y="-1.228767456055" />
                  <Point X="21.860546875" Y="-1.213180908203" />
                  <Point X="21.831283203125" Y="-1.20195703125" />
                  <Point X="21.799396484375" Y="-1.195475097656" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="20.81402734375" Y="-1.319986083984" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.2435546875" Y="-1.296578369141" />
                  <Point X="20.165921875" Y="-0.992649414063" />
                  <Point X="20.134890625" Y="-0.775670166016" />
                  <Point X="20.107578125" Y="-0.584698303223" />
                  <Point X="20.853015625" Y="-0.384958587646" />
                  <Point X="21.467125" Y="-0.220408432007" />
                  <Point X="21.489052734375" Y="-0.211478302002" />
                  <Point X="21.51610546875" Y="-0.19628666687" />
                  <Point X="21.529474609375" Y="-0.187201858521" />
                  <Point X="21.5575625" Y="-0.164393829346" />
                  <Point X="21.57380078125" Y="-0.1474818573" />
                  <Point X="21.58540234375" Y="-0.127108207703" />
                  <Point X="21.59665625" Y="-0.100032905579" />
                  <Point X="21.601361328125" Y="-0.085519622803" />
                  <Point X="21.609029296875" Y="-0.053229255676" />
                  <Point X="21.61159765625" Y="-0.031902353287" />
                  <Point X="21.60930859375" Y="-0.010543683052" />
                  <Point X="21.603234375" Y="0.01661328125" />
                  <Point X="21.598763671875" Y="0.031074197769" />
                  <Point X="21.585916015625" Y="0.063283050537" />
                  <Point X="21.574611328125" Y="0.083819145203" />
                  <Point X="21.55862109375" Y="0.100961517334" />
                  <Point X="21.535314453125" Y="0.120452003479" />
                  <Point X="21.522123046875" Y="0.129703018188" />
                  <Point X="21.4902890625" Y="0.148212341309" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="20.59746875" Y="0.39087197876" />
                  <Point X="20.10818359375" Y="0.521975524902" />
                  <Point X="20.12548046875" Y="0.63886541748" />
                  <Point X="20.17551171875" Y="0.976967773438" />
                  <Point X="20.237984375" Y="1.207510742188" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.794126953125" Y="1.357747558594" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228149414" />
                  <Point X="21.296865234375" Y="1.305263916016" />
                  <Point X="21.322287109375" Y="1.313279785156" />
                  <Point X="21.358291015625" Y="1.324631347656" />
                  <Point X="21.377224609375" Y="1.332962402344" />
                  <Point X="21.39596875" Y="1.343784545898" />
                  <Point X="21.412646484375" Y="1.356014160156" />
                  <Point X="21.426283203125" Y="1.371563598633" />
                  <Point X="21.43869921875" Y="1.389294067383" />
                  <Point X="21.44865234375" Y="1.407432250977" />
                  <Point X="21.458853515625" Y="1.432060546875" />
                  <Point X="21.473298828125" Y="1.466936645508" />
                  <Point X="21.4790859375" Y="1.486797119141" />
                  <Point X="21.48284375" Y="1.508112182617" />
                  <Point X="21.484197265625" Y="1.528754882812" />
                  <Point X="21.481048828125" Y="1.549200927734" />
                  <Point X="21.4754453125" Y="1.570107177734" />
                  <Point X="21.46794921875" Y="1.589377563477" />
                  <Point X="21.455640625" Y="1.613022827148" />
                  <Point X="21.438208984375" Y="1.646507324219" />
                  <Point X="21.426716796875" Y="1.663707763672" />
                  <Point X="21.4128046875" Y="1.680287231445" />
                  <Point X="21.39786328125" Y="1.694590332031" />
                  <Point X="20.89902734375" Y="2.077361328125" />
                  <Point X="20.648140625" Y="2.269873535156" />
                  <Point X="20.724443359375" Y="2.400599365234" />
                  <Point X="20.9188515625" Y="2.733665283203" />
                  <Point X="21.084326171875" Y="2.946361572266" />
                  <Point X="21.24949609375" Y="3.158662353516" />
                  <Point X="21.52298046875" Y="3.000765625" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.888615234375" Y="2.822698486328" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.95943359375" Y="2.818762939453" />
                  <Point X="21.980892578125" Y="2.821587890625" />
                  <Point X="22.000984375" Y="2.826504394531" />
                  <Point X="22.019537109375" Y="2.835653320313" />
                  <Point X="22.037791015625" Y="2.847282470703" />
                  <Point X="22.053923828125" Y="2.860228759766" />
                  <Point X="22.079056640625" Y="2.885361328125" />
                  <Point X="22.1146484375" Y="2.920952148438" />
                  <Point X="22.127595703125" Y="2.937085449219" />
                  <Point X="22.139224609375" Y="2.95533984375" />
                  <Point X="22.14837109375" Y="2.973888671875" />
                  <Point X="22.1532890625" Y="2.993977539062" />
                  <Point X="22.156115234375" Y="3.015436523438" />
                  <Point X="22.15656640625" Y="3.036120605469" />
                  <Point X="22.15346875" Y="3.071528320312" />
                  <Point X="22.14908203125" Y="3.121670166016" />
                  <Point X="22.145044921875" Y="3.141962646484" />
                  <Point X="22.13853515625" Y="3.162604980469" />
                  <Point X="22.130205078125" Y="3.181533203125" />
                  <Point X="21.909154296875" Y="3.564402587891" />
                  <Point X="21.81666796875" Y="3.724595947266" />
                  <Point X="21.9607890625" Y="3.835093261719" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.56" Y="4.23948046875" />
                  <Point X="22.832962890625" Y="4.391133300781" />
                  <Point X="22.874037109375" Y="4.337604003906" />
                  <Point X="22.9568046875" Y="4.229740234375" />
                  <Point X="22.971109375" Y="4.214797851563" />
                  <Point X="22.987689453125" Y="4.200885742188" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.044294921875" Y="4.168879882812" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.14029296875" Y="4.126728515625" />
                  <Point X="23.160736328125" Y="4.12358203125" />
                  <Point X="23.181376953125" Y="4.124935546875" />
                  <Point X="23.20269140625" Y="4.128694335938" />
                  <Point X="23.222548828125" Y="4.134481933594" />
                  <Point X="23.263595703125" Y="4.151484375" />
                  <Point X="23.32172265625" Y="4.175561523437" />
                  <Point X="23.339853515625" Y="4.185508789062" />
                  <Point X="23.357583984375" Y="4.197922851562" />
                  <Point X="23.373140625" Y="4.211563964844" />
                  <Point X="23.385373046875" Y="4.228249511719" />
                  <Point X="23.396193359375" Y="4.246993652344" />
                  <Point X="23.404521484375" Y="4.265922363281" />
                  <Point X="23.417880859375" Y="4.308294433594" />
                  <Point X="23.43680078125" Y="4.368298828125" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410145507812" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.417140625" Y="4.62171484375" />
                  <Point X="23.41580078125" Y="4.631897460938" />
                  <Point X="23.612046875" Y="4.686918457031" />
                  <Point X="24.050369140625" Y="4.80980859375" />
                  <Point X="24.36631640625" Y="4.846785644531" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.786115234375" Y="4.584809570312" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.25948046875" Y="4.709020507812" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.4613203125" Y="4.8718203125" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.10544140625" Y="4.76862890625" />
                  <Point X="26.48102734375" Y="4.677950195312" />
                  <Point X="26.6503046875" Y="4.616553222656" />
                  <Point X="26.894640625" Y="4.527930175781" />
                  <Point X="27.059171875" Y="4.450984863281" />
                  <Point X="27.294576171875" Y="4.340893554688" />
                  <Point X="27.453544921875" Y="4.248279296875" />
                  <Point X="27.680978515625" Y="4.115775390625" />
                  <Point X="27.8308828125" Y="4.009172607422" />
                  <Point X="27.94326171875" Y="3.929254150391" />
                  <Point X="27.50419921875" Y="3.168774169922" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.539934326172" />
                  <Point X="27.133078125" Y="2.516058837891" />
                  <Point X="27.12419140625" Y="2.482829589844" />
                  <Point X="27.111607421875" Y="2.435772460938" />
                  <Point X="27.108619140625" Y="2.417935791016" />
                  <Point X="27.107728515625" Y="2.380953125" />
                  <Point X="27.111193359375" Y="2.352219238281" />
                  <Point X="27.116099609375" Y="2.311528320312" />
                  <Point X="27.12144140625" Y="2.289606933594" />
                  <Point X="27.12970703125" Y="2.267518310547" />
                  <Point X="27.140072265625" Y="2.247468261719" />
                  <Point X="27.157853515625" Y="2.221265625" />
                  <Point X="27.18303125" Y="2.184159667969" />
                  <Point X="27.19446484375" Y="2.170329101562" />
                  <Point X="27.22159765625" Y="2.145593505859" />
                  <Point X="27.247798828125" Y="2.127813964844" />
                  <Point X="27.28490625" Y="2.102635986328" />
                  <Point X="27.304955078125" Y="2.092271728516" />
                  <Point X="27.32704296875" Y="2.084006103516" />
                  <Point X="27.34896484375" Y="2.078663330078" />
                  <Point X="27.37769921875" Y="2.075198486328" />
                  <Point X="27.418388671875" Y="2.070291748047" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.473205078125" Y="2.074170898438" />
                  <Point X="27.506435546875" Y="2.083056884766" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.4632421875" Y="2.615157226563" />
                  <Point X="28.967326171875" Y="2.906190429688" />
                  <Point X="28.98836328125" Y="2.876953369141" />
                  <Point X="29.123279296875" Y="2.689451904297" />
                  <Point X="29.206841796875" Y="2.551364257812" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.700302734375" Y="2.028724853516" />
                  <Point X="28.23078515625" Y="1.668451171875" />
                  <Point X="28.2214296875" Y="1.66024621582" />
                  <Point X="28.20397265625" Y="1.641626220703" />
                  <Point X="28.18005859375" Y="1.610427124023" />
                  <Point X="28.14619140625" Y="1.566244873047" />
                  <Point X="28.13660546875" Y="1.550911621094" />
                  <Point X="28.121630859375" Y="1.517087036133" />
                  <Point X="28.11272265625" Y="1.485232666016" />
                  <Point X="28.10010546875" Y="1.440122680664" />
                  <Point X="28.09665234375" Y="1.417825805664" />
                  <Point X="28.0958359375" Y="1.394254272461" />
                  <Point X="28.097740234375" Y="1.371764648438" />
                  <Point X="28.1050546875" Y="1.336322387695" />
                  <Point X="28.11541015625" Y="1.286131835938" />
                  <Point X="28.1206796875" Y="1.268980712891" />
                  <Point X="28.136283203125" Y="1.235740722656" />
                  <Point X="28.156173828125" Y="1.205508178711" />
                  <Point X="28.18433984375" Y="1.16269543457" />
                  <Point X="28.198890625" Y="1.145452270508" />
                  <Point X="28.216134765625" Y="1.129361572266" />
                  <Point X="28.23434765625" Y="1.116034545898" />
                  <Point X="28.263171875" Y="1.099809204102" />
                  <Point X="28.303990234375" Y="1.07683215332" />
                  <Point X="28.3205234375" Y="1.069500976563" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.395091796875" Y="1.054287963867" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.3191171875" Y="1.156376220703" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.787994140625" Y="1.170817504883" />
                  <Point X="29.845939453125" Y="0.932788574219" />
                  <Point X="29.872271484375" Y="0.76366394043" />
                  <Point X="29.890865234375" Y="0.644238586426" />
                  <Point X="29.254658203125" Y="0.473767272949" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.7047890625" Y="0.325585296631" />
                  <Point X="28.681546875" Y="0.315068023682" />
                  <Point X="28.6432578125" Y="0.292936553955" />
                  <Point X="28.589037109375" Y="0.26159564209" />
                  <Point X="28.574314453125" Y="0.251097961426" />
                  <Point X="28.54753125" Y="0.225576080322" />
                  <Point X="28.52455859375" Y="0.196302856445" />
                  <Point X="28.492025390625" Y="0.154848464966" />
                  <Point X="28.48030078125" Y="0.135566680908" />
                  <Point X="28.47052734375" Y="0.114102157593" />
                  <Point X="28.463681640625" Y="0.092604545593" />
                  <Point X="28.4560234375" Y="0.052618865967" />
                  <Point X="28.4451796875" Y="-0.00400592041" />
                  <Point X="28.443484375" Y="-0.021875896454" />
                  <Point X="28.4451796875" Y="-0.05855406189" />
                  <Point X="28.452837890625" Y="-0.098539894104" />
                  <Point X="28.463681640625" Y="-0.155164672852" />
                  <Point X="28.47052734375" Y="-0.176662139893" />
                  <Point X="28.48030078125" Y="-0.198126663208" />
                  <Point X="28.492025390625" Y="-0.217408584595" />
                  <Point X="28.514998046875" Y="-0.24668182373" />
                  <Point X="28.54753125" Y="-0.288136383057" />
                  <Point X="28.560001953125" Y="-0.30123815918" />
                  <Point X="28.589037109375" Y="-0.324155914307" />
                  <Point X="28.627326171875" Y="-0.346287261963" />
                  <Point X="28.681546875" Y="-0.377628173828" />
                  <Point X="28.692708984375" Y="-0.383137695312" />
                  <Point X="28.716580078125" Y="-0.392149810791" />
                  <Point X="29.478564453125" Y="-0.596323181152" />
                  <Point X="29.891474609375" Y="-0.706962036133" />
                  <Point X="29.887376953125" Y="-0.73414074707" />
                  <Point X="29.855025390625" Y="-0.948723144531" />
                  <Point X="29.82128515625" Y="-1.096576782227" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="29.048400390625" Y="-1.085594482422" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.299513671875" Y="-1.021842224121" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.1639765625" Y="-1.056596069336" />
                  <Point X="28.1361484375" Y="-1.07348840332" />
                  <Point X="28.1123984375" Y="-1.093959960937" />
                  <Point X="28.0669765625" Y="-1.148587890625" />
                  <Point X="28.002654296875" Y="-1.225947998047" />
                  <Point X="27.987931640625" Y="-1.250334716797" />
                  <Point X="27.97658984375" Y="-1.277719970703" />
                  <Point X="27.969759765625" Y="-1.305365844727" />
                  <Point X="27.96325" Y="-1.376111572266" />
                  <Point X="27.95403125" Y="-1.476296142578" />
                  <Point X="27.95634765625" Y="-1.507561157227" />
                  <Point X="27.964078125" Y="-1.539182617188" />
                  <Point X="27.976451171875" Y="-1.56799597168" />
                  <Point X="28.018037109375" Y="-1.632682373047" />
                  <Point X="28.076931640625" Y="-1.724286376953" />
                  <Point X="28.0869375" Y="-1.737244140625" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.8177578125" Y="-2.303506835938" />
                  <Point X="29.213123046875" Y="-2.606882080078" />
                  <Point X="29.124802734375" Y="-2.749798339844" />
                  <Point X="29.05503515625" Y="-2.848927490234" />
                  <Point X="29.028982421875" Y="-2.885945068359" />
                  <Point X="28.356728515625" Y="-2.497818847656" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.664791015625" Y="-2.143673095703" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135176513672" />
                  <Point X="27.370533203125" Y="-2.174280029297" />
                  <Point X="27.26531640625" Y="-2.229655517578" />
                  <Point X="27.242384765625" Y="-2.246549072266" />
                  <Point X="27.22142578125" Y="-2.267508300781" />
                  <Point X="27.204533203125" Y="-2.290438720703" />
                  <Point X="27.1654296875" Y="-2.364738769531" />
                  <Point X="27.110052734375" Y="-2.469956787109" />
                  <Point X="27.100228515625" Y="-2.499735107422" />
                  <Point X="27.095271484375" Y="-2.531908935547" />
                  <Point X="27.09567578125" Y="-2.563259033203" />
                  <Point X="27.111828125" Y="-2.652695800781" />
                  <Point X="27.134703125" Y="-2.779349365234" />
                  <Point X="27.13898828125" Y="-2.795140869141" />
                  <Point X="27.1518203125" Y="-2.826078613281" />
                  <Point X="27.60621875" Y="-3.613122314453" />
                  <Point X="27.861283203125" Y="-4.054904785156" />
                  <Point X="27.781826171875" Y="-4.111659179688" />
                  <Point X="27.703837890625" Y="-4.162140136719" />
                  <Point X="27.701767578125" Y="-4.16348046875" />
                  <Point X="27.182654296875" Y="-3.486960449219" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.633716796875" Y="-2.843846923828" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.32062890625" Y="-2.749994140625" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.03010546875" Y="-2.857399414062" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968860595703" />
                  <Point X="25.88725" Y="-2.996686767578" />
                  <Point X="25.875625" Y="-3.025809326172" />
                  <Point X="25.853353515625" Y="-3.128282470703" />
                  <Point X="25.821810546875" Y="-3.273397216797" />
                  <Point X="25.819724609375" Y="-3.289627197266" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.948515625" Y="-4.301251953125" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94157421875" Y="-4.75263671875" />
                  <Point X="23.85875390625" Y="-4.731328125" />
                  <Point X="23.859267578125" Y="-4.727431640625" />
                  <Point X="23.8792265625" Y="-4.575837402344" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030761719" />
                  <Point X="23.8782421875" Y="-4.509323730469" />
                  <Point X="23.876666015625" Y="-4.497687988281" />
                  <Point X="23.85161328125" Y="-4.37174609375" />
                  <Point X="23.81613671875" Y="-4.193396484375" />
                  <Point X="23.811873046875" Y="-4.178467285156" />
                  <Point X="23.800970703125" Y="-4.149500488281" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.74979296875" Y="-4.070937744141" />
                  <Point X="23.738994140625" Y="-4.059779541016" />
                  <Point X="23.642451171875" Y="-3.97511328125" />
                  <Point X="23.505734375" Y="-3.855215087891" />
                  <Point X="23.49326171875" Y="-3.845965087891" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822527587891" />
                  <Point X="23.423470703125" Y="-3.810226318359" />
                  <Point X="23.4086875" Y="-3.805476318359" />
                  <Point X="23.378544921875" Y="-3.798447998047" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.23505078125" Y="-3.787771240234" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.808270019531" />
                  <Point X="22.9045625" Y="-3.8158125" />
                  <Point X="22.797794921875" Y="-3.887153076172" />
                  <Point X="22.64659765625" Y="-3.988180664062" />
                  <Point X="22.640322265625" Y="-3.992754882812" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032771484375" />
                  <Point X="22.589529296875" Y="-4.041629150391" />
                  <Point X="22.496798828125" Y="-4.162478515625" />
                  <Point X="22.25241015625" Y="-4.011158203125" />
                  <Point X="22.0926328125" Y="-3.88813671875" />
                  <Point X="22.01913671875" Y="-3.831546630859" />
                  <Point X="22.3503984375" Y="-3.257783691406" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.710085693359" />
                  <Point X="22.67605078125" Y="-2.68112109375" />
                  <Point X="22.680314453125" Y="-2.666190429688" />
                  <Point X="22.6865859375" Y="-2.634664550781" />
                  <Point X="22.688361328125" Y="-2.619240478516" />
                  <Point X="22.689375" Y="-2.588306152344" />
                  <Point X="22.6853359375" Y="-2.557618408203" />
                  <Point X="22.6763515625" Y="-2.528000244141" />
                  <Point X="22.67064453125" Y="-2.513559570312" />
                  <Point X="22.656427734375" Y="-2.48473046875" />
                  <Point X="22.648447265625" Y="-2.471414794922" />
                  <Point X="22.630421875" Y="-2.446258544922" />
                  <Point X="22.620376953125" Y="-2.43441796875" />
                  <Point X="22.603029296875" Y="-2.417069335938" />
                  <Point X="22.5911875" Y="-2.407022705078" />
                  <Point X="22.56602734375" Y="-2.388992919922" />
                  <Point X="22.552708984375" Y="-2.381009765625" />
                  <Point X="22.52387890625" Y="-2.366792480469" />
                  <Point X="22.5094375" Y="-2.361086181641" />
                  <Point X="22.4798203125" Y="-2.352102050781" />
                  <Point X="22.449134765625" Y="-2.348062255859" />
                  <Point X="22.418201171875" Y="-2.349074951172" />
                  <Point X="22.40277734375" Y="-2.350849609375" />
                  <Point X="22.371251953125" Y="-2.357120605469" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372285888672" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.55758203125" Y="-2.815248779297" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="21.191224609375" Y="-2.997098388672" />
                  <Point X="20.995978515625" Y="-2.740584472656" />
                  <Point X="20.881435546875" Y="-2.548513183594" />
                  <Point X="20.818734375" Y="-2.443373291016" />
                  <Point X="21.412783203125" Y="-1.987543701172" />
                  <Point X="21.951876953125" Y="-1.573881835938" />
                  <Point X="21.963515625" Y="-1.563311279297" />
                  <Point X="21.98489453125" Y="-1.54039074707" />
                  <Point X="21.994634765625" Y="-1.528040527344" />
                  <Point X="22.012599609375" Y="-1.500908935547" />
                  <Point X="22.020166015625" Y="-1.487121704102" />
                  <Point X="22.032919921875" Y="-1.458494262695" />
                  <Point X="22.038107421875" Y="-1.443654296875" />
                  <Point X="22.0458125" Y="-1.413907104492" />
                  <Point X="22.048447265625" Y="-1.398805297852" />
                  <Point X="22.051251953125" Y="-1.368374633789" />
                  <Point X="22.051421875" Y="-1.353045654297" />
                  <Point X="22.049212890625" Y="-1.321375366211" />
                  <Point X="22.04691796875" Y="-1.306223144531" />
                  <Point X="22.03991796875" Y="-1.276478515625" />
                  <Point X="22.02822265625" Y="-1.248243896484" />
                  <Point X="22.012140625" Y="-1.222260498047" />
                  <Point X="22.003046875" Y="-1.209919433594" />
                  <Point X="21.98221484375" Y="-1.185963378906" />
                  <Point X="21.971255859375" Y="-1.175245239258" />
                  <Point X="21.947755859375" Y="-1.155711181641" />
                  <Point X="21.935216796875" Y="-1.146895385742" />
                  <Point X="21.908734375" Y="-1.131308959961" />
                  <Point X="21.89456640625" Y="-1.124481201172" />
                  <Point X="21.865302734375" Y="-1.113257324219" />
                  <Point X="21.85020703125" Y="-1.108861083984" />
                  <Point X="21.8183203125" Y="-1.102379150391" />
                  <Point X="21.802703125" Y="-1.100532714844" />
                  <Point X="21.77137890625" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="20.801626953125" Y="-1.225798828125" />
                  <Point X="20.339080078125" Y="-1.286694335938" />
                  <Point X="20.335599609375" Y="-1.273066040039" />
                  <Point X="20.259236328125" Y="-0.97410949707" />
                  <Point X="20.22893359375" Y="-0.76222064209" />
                  <Point X="20.213548828125" Y="-0.65465447998" />
                  <Point X="20.877603515625" Y="-0.476721466064" />
                  <Point X="21.491712890625" Y="-0.312171417236" />
                  <Point X="21.50295703125" Y="-0.308391937256" />
                  <Point X="21.524884765625" Y="-0.299461791992" />
                  <Point X="21.535568359375" Y="-0.294311401367" />
                  <Point X="21.56262109375" Y="-0.279119781494" />
                  <Point X="21.5695" Y="-0.274861572266" />
                  <Point X="21.589359375" Y="-0.260949890137" />
                  <Point X="21.617447265625" Y="-0.238141769409" />
                  <Point X="21.626087890625" Y="-0.230190307617" />
                  <Point X="21.642326171875" Y="-0.213278213501" />
                  <Point X="21.65635546875" Y="-0.194491241455" />
                  <Point X="21.66795703125" Y="-0.17411756897" />
                  <Point X="21.673126953125" Y="-0.163570846558" />
                  <Point X="21.684380859375" Y="-0.13649546814" />
                  <Point X="21.687025390625" Y="-0.129329925537" />
                  <Point X="21.693791015625" Y="-0.107468833923" />
                  <Point X="21.701458984375" Y="-0.07517842865" />
                  <Point X="21.70334765625" Y="-0.064587860107" />
                  <Point X="21.705916015625" Y="-0.043260925293" />
                  <Point X="21.706056640625" Y="-0.02177897644" />
                  <Point X="21.703767578125" Y="-0.000420235425" />
                  <Point X="21.702017578125" Y="0.010192626953" />
                  <Point X="21.695943359375" Y="0.037349594116" />
                  <Point X="21.69399609375" Y="0.044672836304" />
                  <Point X="21.687001953125" Y="0.066271606445" />
                  <Point X="21.674154296875" Y="0.098480407715" />
                  <Point X="21.669140625" Y="0.109095947266" />
                  <Point X="21.6578359375" Y="0.129632064819" />
                  <Point X="21.644080078125" Y="0.148619232178" />
                  <Point X="21.62808984375" Y="0.165761535645" />
                  <Point X="21.619564453125" Y="0.173837402344" />
                  <Point X="21.5962578125" Y="0.193327957153" />
                  <Point X="21.589861328125" Y="0.198231765747" />
                  <Point X="21.569875" Y="0.211829864502" />
                  <Point X="21.538041015625" Y="0.23033921814" />
                  <Point X="21.52677734375" Y="0.235925796509" />
                  <Point X="21.50361328125" Y="0.245561767578" />
                  <Point X="21.491712890625" Y="0.249611297607" />
                  <Point X="20.622056640625" Y="0.482634857178" />
                  <Point X="20.2145546875" Y="0.591824951172" />
                  <Point X="20.21945703125" Y="0.624959106445" />
                  <Point X="20.26866796875" Y="0.957520141602" />
                  <Point X="20.329677734375" Y="1.182663696289" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="20.7817265625" Y="1.263560424805" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815551758" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.204703369141" />
                  <Point X="21.28485546875" Y="1.206589599609" />
                  <Point X="21.295109375" Y="1.208053588867" />
                  <Point X="21.3153984375" Y="1.212089355469" />
                  <Point X="21.32543359375" Y="1.214661254883" />
                  <Point X="21.35085546875" Y="1.222677246094" />
                  <Point X="21.386859375" Y="1.234028686523" />
                  <Point X="21.396552734375" Y="1.237677001953" />
                  <Point X="21.415486328125" Y="1.246008056641" />
                  <Point X="21.424724609375" Y="1.250690429688" />
                  <Point X="21.44346875" Y="1.261512695312" />
                  <Point X="21.452146484375" Y="1.267174438477" />
                  <Point X="21.46882421875" Y="1.279404052734" />
                  <Point X="21.4840703125" Y="1.293375732422" />
                  <Point X="21.49770703125" Y="1.308925170898" />
                  <Point X="21.504099609375" Y="1.317071044922" />
                  <Point X="21.516515625" Y="1.334801391602" />
                  <Point X="21.521984375" Y="1.343592407227" />
                  <Point X="21.5319375" Y="1.36173059082" />
                  <Point X="21.536421875" Y="1.371077880859" />
                  <Point X="21.546623046875" Y="1.395706176758" />
                  <Point X="21.561068359375" Y="1.430582275391" />
                  <Point X="21.564505859375" Y="1.440360107422" />
                  <Point X="21.57029296875" Y="1.460220581055" />
                  <Point X="21.572642578125" Y="1.470303100586" />
                  <Point X="21.576400390625" Y="1.491618164063" />
                  <Point X="21.577640625" Y="1.501896484375" />
                  <Point X="21.578994140625" Y="1.52253918457" />
                  <Point X="21.57808984375" Y="1.543213378906" />
                  <Point X="21.57494140625" Y="1.563659423828" />
                  <Point X="21.572810546875" Y="1.573795654297" />
                  <Point X="21.56720703125" Y="1.594701904297" />
                  <Point X="21.563982421875" Y="1.604547729492" />
                  <Point X="21.556486328125" Y="1.623817993164" />
                  <Point X="21.55221484375" Y="1.633242675781" />
                  <Point X="21.53990625" Y="1.656887939453" />
                  <Point X="21.522474609375" Y="1.690372436523" />
                  <Point X="21.517201171875" Y="1.699283935547" />
                  <Point X="21.505708984375" Y="1.716484619141" />
                  <Point X="21.499490234375" Y="1.72477331543" />
                  <Point X="21.485578125" Y="1.741352783203" />
                  <Point X="21.478498046875" Y="1.748912231445" />
                  <Point X="21.463556640625" Y="1.763215332031" />
                  <Point X="21.4556953125" Y="1.769958862305" />
                  <Point X="20.956859375" Y="2.152729736328" />
                  <Point X="20.77238671875" Y="2.294281005859" />
                  <Point X="20.806490234375" Y="2.352709960938" />
                  <Point X="20.997716796875" Y="2.680323486328" />
                  <Point X="21.159306640625" Y="2.888027587891" />
                  <Point X="21.273662109375" Y="3.035013671875" />
                  <Point X="21.47548046875" Y="2.918493164063" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.8803359375" Y="2.728060058594" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.961505859375" Y="2.723785644531" />
                  <Point X="21.97183203125" Y="2.724575683594" />
                  <Point X="21.993291015625" Y="2.727400634766" />
                  <Point X="22.00347265625" Y="2.729310546875" />
                  <Point X="22.023564453125" Y="2.734227050781" />
                  <Point X="22.043" Y="2.741301025391" />
                  <Point X="22.061552734375" Y="2.750449951172" />
                  <Point X="22.070580078125" Y="2.755531494141" />
                  <Point X="22.088833984375" Y="2.767160644531" />
                  <Point X="22.09725" Y="2.773189697266" />
                  <Point X="22.1133828125" Y="2.786135986328" />
                  <Point X="22.121099609375" Y="2.793053222656" />
                  <Point X="22.146232421875" Y="2.818185791016" />
                  <Point X="22.18182421875" Y="2.853776611328" />
                  <Point X="22.188740234375" Y="2.861492431641" />
                  <Point X="22.2016875" Y="2.877625732422" />
                  <Point X="22.20771875" Y="2.886043212891" />
                  <Point X="22.21934765625" Y="2.904297607422" />
                  <Point X="22.2244296875" Y="2.913325439453" />
                  <Point X="22.233576171875" Y="2.931874267578" />
                  <Point X="22.240646484375" Y="2.951298828125" />
                  <Point X="22.245564453125" Y="2.971387695312" />
                  <Point X="22.2474765625" Y="2.981572998047" />
                  <Point X="22.250302734375" Y="3.003031982422" />
                  <Point X="22.251091796875" Y="3.013364746094" />
                  <Point X="22.25154296875" Y="3.034048828125" />
                  <Point X="22.251205078125" Y="3.044400146484" />
                  <Point X="22.248107421875" Y="3.079807861328" />
                  <Point X="22.243720703125" Y="3.129949707031" />
                  <Point X="22.242255859375" Y="3.140206787109" />
                  <Point X="22.23821875" Y="3.160499267578" />
                  <Point X="22.235646484375" Y="3.170534667969" />
                  <Point X="22.22913671875" Y="3.191177001953" />
                  <Point X="22.225486328125" Y="3.200871582031" />
                  <Point X="22.21715625" Y="3.219799804688" />
                  <Point X="22.2124765625" Y="3.229033447266" />
                  <Point X="21.99142578125" Y="3.611902832031" />
                  <Point X="21.94061328125" Y="3.699915283203" />
                  <Point X="22.018591796875" Y="3.759701904297" />
                  <Point X="22.3516328125" Y="4.015040039062" />
                  <Point X="22.60613671875" Y="4.156436035156" />
                  <Point X="22.807474609375" Y="4.268295410156" />
                  <Point X="22.881435546875" Y="4.171908203125" />
                  <Point X="22.888181640625" Y="4.164045410156" />
                  <Point X="22.902486328125" Y="4.149103027344" />
                  <Point X="22.910044921875" Y="4.142022949219" />
                  <Point X="22.926625" Y="4.128110839844" />
                  <Point X="22.93491015625" Y="4.121895996094" />
                  <Point X="22.952107421875" Y="4.110405273438" />
                  <Point X="22.96101953125" Y="4.105129394531" />
                  <Point X="23.000427734375" Y="4.084614257813" />
                  <Point X="23.056236328125" Y="4.0555625" />
                  <Point X="23.065673828125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.0437890625" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.11570703125" Y="4.034965332031" />
                  <Point X="23.125841796875" Y="4.032834228516" />
                  <Point X="23.14628515625" Y="4.029687744141" />
                  <Point X="23.166953125" Y="4.028785644531" />
                  <Point X="23.18759375" Y="4.030139160156" />
                  <Point X="23.197875" Y="4.031379150391" />
                  <Point X="23.219189453125" Y="4.035137939453" />
                  <Point X="23.2292734375" Y="4.037489257812" />
                  <Point X="23.249130859375" Y="4.043276855469" />
                  <Point X="23.258904296875" Y="4.046713623047" />
                  <Point X="23.299951171875" Y="4.063716064453" />
                  <Point X="23.358078125" Y="4.087793212891" />
                  <Point X="23.36741796875" Y="4.092273193359" />
                  <Point X="23.385548828125" Y="4.102220214844" />
                  <Point X="23.39433984375" Y="4.1076875" />
                  <Point X="23.4120703125" Y="4.1201015625" />
                  <Point X="23.420216796875" Y="4.126494140625" />
                  <Point X="23.4357734375" Y="4.140135253906" />
                  <Point X="23.4497578125" Y="4.155395507812" />
                  <Point X="23.461990234375" Y="4.172081054688" />
                  <Point X="23.4676484375" Y="4.180754882812" />
                  <Point X="23.47846875" Y="4.199499023438" />
                  <Point X="23.4831484375" Y="4.208735351562" />
                  <Point X="23.4914765625" Y="4.2276640625" />
                  <Point X="23.495125" Y="4.237356445312" />
                  <Point X="23.508484375" Y="4.279728515625" />
                  <Point X="23.527404296875" Y="4.339732910156" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370048828125" />
                  <Point X="23.535474609375" Y="4.380302246094" />
                  <Point X="23.537361328125" Y="4.401864257812" />
                  <Point X="23.53769921875" Y="4.412217285156" />
                  <Point X="23.537248046875" Y="4.4328984375" />
                  <Point X="23.536458984375" Y="4.4432265625" />
                  <Point X="23.520736328125" Y="4.562654785156" />
                  <Point X="23.637693359375" Y="4.595445800781" />
                  <Point X="24.068826171875" Y="4.7163203125" />
                  <Point X="24.377359375" Y="4.7524296875" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.6943515625" Y="4.560221679688" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.351244140625" Y="4.684432617188" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.45142578125" Y="4.777336914062" />
                  <Point X="25.827876953125" Y="4.737912109375" />
                  <Point X="26.083146484375" Y="4.676282226562" />
                  <Point X="26.453595703125" Y="4.586843261719" />
                  <Point X="26.617912109375" Y="4.52724609375" />
                  <Point X="26.858251953125" Y="4.440072753906" />
                  <Point X="27.018927734375" Y="4.364930175781" />
                  <Point X="27.250453125" Y="4.25665234375" />
                  <Point X="27.40572265625" Y="4.166193847656" />
                  <Point X="27.629431640625" Y="4.035860107422" />
                  <Point X="27.775826171875" Y="3.931752929688" />
                  <Point X="27.817783203125" Y="3.901915771484" />
                  <Point X="27.421927734375" Y="3.216274169922" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.062376953125" Y="2.593106933594" />
                  <Point X="27.053189453125" Y="2.573449707031" />
                  <Point X="27.0441875" Y="2.54957421875" />
                  <Point X="27.041302734375" Y="2.540602783203" />
                  <Point X="27.032416015625" Y="2.507373535156" />
                  <Point X="27.01983203125" Y="2.46031640625" />
                  <Point X="27.0179140625" Y="2.451469482422" />
                  <Point X="27.013646484375" Y="2.420222900391" />
                  <Point X="27.012755859375" Y="2.383240234375" />
                  <Point X="27.013412109375" Y="2.369580078125" />
                  <Point X="27.016876953125" Y="2.340846191406" />
                  <Point X="27.021783203125" Y="2.300155273438" />
                  <Point X="27.02380078125" Y="2.289036865234" />
                  <Point X="27.029142578125" Y="2.267115478516" />
                  <Point X="27.032466796875" Y="2.2563125" />
                  <Point X="27.040732421875" Y="2.234223876953" />
                  <Point X="27.04531640625" Y="2.223891357422" />
                  <Point X="27.055681640625" Y="2.203841308594" />
                  <Point X="27.061462890625" Y="2.194123779297" />
                  <Point X="27.079244140625" Y="2.167921142578" />
                  <Point X="27.104421875" Y="2.130815185547" />
                  <Point X="27.1098125" Y="2.123629638672" />
                  <Point X="27.130462890625" Y="2.100124267578" />
                  <Point X="27.157595703125" Y="2.075388671875" />
                  <Point X="27.16825390625" Y="2.066983642578" />
                  <Point X="27.194455078125" Y="2.049204101563" />
                  <Point X="27.2315625" Y="2.024026000977" />
                  <Point X="27.24128125" Y="2.018245361328" />
                  <Point X="27.261330078125" Y="2.007881103516" />
                  <Point X="27.27166015625" Y="2.003297363281" />
                  <Point X="27.293748046875" Y="1.995031860352" />
                  <Point X="27.304548828125" Y="1.991707763672" />
                  <Point X="27.326470703125" Y="1.986364990234" />
                  <Point X="27.337591796875" Y="1.984346557617" />
                  <Point X="27.366326171875" Y="1.980881713867" />
                  <Point X="27.407015625" Y="1.975975097656" />
                  <Point X="27.416044921875" Y="1.975320678711" />
                  <Point X="27.44757421875" Y="1.975497314453" />
                  <Point X="27.4843125" Y="1.979822387695" />
                  <Point X="27.49774609375" Y="1.982395507812" />
                  <Point X="27.5309765625" Y="1.991281616211" />
                  <Point X="27.578033203125" Y="2.003865112305" />
                  <Point X="27.583994140625" Y="2.005670654297" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.5107421875" Y="2.532884765625" />
                  <Point X="28.940404296875" Y="2.780950439453" />
                  <Point X="29.043958984375" Y="2.637033935547" />
                  <Point X="29.125564453125" Y="2.502180175781" />
                  <Point X="29.136884765625" Y="2.483471679688" />
                  <Point X="28.642470703125" Y="2.104093261719" />
                  <Point X="28.172953125" Y="1.743819702148" />
                  <Point X="28.16814453125" Y="1.739874267578" />
                  <Point X="28.152125" Y="1.725222045898" />
                  <Point X="28.13466796875" Y="1.706602172852" />
                  <Point X="28.12857421875" Y="1.699419189453" />
                  <Point X="28.10466015625" Y="1.668219970703" />
                  <Point X="28.07079296875" Y="1.624037841797" />
                  <Point X="28.065638671875" Y="1.616604736328" />
                  <Point X="28.04973828125" Y="1.589369140625" />
                  <Point X="28.034763671875" Y="1.555544555664" />
                  <Point X="28.030140625" Y="1.542672485352" />
                  <Point X="28.021232421875" Y="1.510818115234" />
                  <Point X="28.008615234375" Y="1.465708129883" />
                  <Point X="28.006224609375" Y="1.454661987305" />
                  <Point X="28.002771484375" Y="1.432364990234" />
                  <Point X="28.001708984375" Y="1.421114135742" />
                  <Point X="28.000892578125" Y="1.397542602539" />
                  <Point X="28.001173828125" Y="1.386238891602" />
                  <Point X="28.003078125" Y="1.363749267578" />
                  <Point X="28.004701171875" Y="1.352563476562" />
                  <Point X="28.012015625" Y="1.31712121582" />
                  <Point X="28.02237109375" Y="1.266930664062" />
                  <Point X="28.024599609375" Y="1.258231079102" />
                  <Point X="28.03468359375" Y="1.228612304688" />
                  <Point X="28.050287109375" Y="1.195372314453" />
                  <Point X="28.056919921875" Y="1.183525756836" />
                  <Point X="28.076810546875" Y="1.153293212891" />
                  <Point X="28.1049765625" Y="1.11048046875" />
                  <Point X="28.111736328125" Y="1.101428100586" />
                  <Point X="28.126287109375" Y="1.084184936523" />
                  <Point X="28.134078125" Y="1.075994262695" />
                  <Point X="28.151322265625" Y="1.059903686523" />
                  <Point X="28.16003515625" Y="1.052694702148" />
                  <Point X="28.178248046875" Y="1.039367675781" />
                  <Point X="28.187748046875" Y="1.033249267578" />
                  <Point X="28.216572265625" Y="1.017023864746" />
                  <Point X="28.257390625" Y="0.994046936035" />
                  <Point X="28.26548046875" Y="0.989987121582" />
                  <Point X="28.294681640625" Y="0.978083312988" />
                  <Point X="28.33027734375" Y="0.968021118164" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.38264453125" Y="0.960106933594" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199768066" />
                  <Point X="28.46571875" Y="0.951222839355" />
                  <Point X="28.49121875" Y="0.952032409668" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="29.331517578125" Y="1.062188964844" />
                  <Point X="29.704703125" Y="1.111319702148" />
                  <Point X="29.7526875" Y="0.914209228516" />
                  <Point X="29.77840234375" Y="0.749048950195" />
                  <Point X="29.78387109375" Y="0.713920898438" />
                  <Point X="29.2300703125" Y="0.565530151367" />
                  <Point X="28.6919921875" Y="0.421352752686" />
                  <Point X="28.686029296875" Y="0.419543548584" />
                  <Point X="28.665623046875" Y="0.412136352539" />
                  <Point X="28.642380859375" Y="0.40161920166" />
                  <Point X="28.634005859375" Y="0.397316864014" />
                  <Point X="28.595716796875" Y="0.375185424805" />
                  <Point X="28.54149609375" Y="0.343844573975" />
                  <Point X="28.533884765625" Y="0.338946258545" />
                  <Point X="28.50877734375" Y="0.319873046875" />
                  <Point X="28.481994140625" Y="0.294351074219" />
                  <Point X="28.472796875" Y="0.284225402832" />
                  <Point X="28.44982421875" Y="0.254952056885" />
                  <Point X="28.417291015625" Y="0.213497711182" />
                  <Point X="28.410853515625" Y="0.204206115723" />
                  <Point X="28.39912890625" Y="0.184924377441" />
                  <Point X="28.393841796875" Y="0.17493409729" />
                  <Point X="28.384068359375" Y="0.153469680786" />
                  <Point X="28.380005859375" Y="0.142927719116" />
                  <Point X="28.37316015625" Y="0.121430160522" />
                  <Point X="28.370376953125" Y="0.11047442627" />
                  <Point X="28.36271875" Y="0.070488777161" />
                  <Point X="28.351875" Y="0.013864057541" />
                  <Point X="28.350603515625" Y="0.004966452122" />
                  <Point X="28.3485859375" Y="-0.02626218605" />
                  <Point X="28.35028125" Y="-0.062940380096" />
                  <Point X="28.351875" Y="-0.07642388916" />
                  <Point X="28.359533203125" Y="-0.116409835815" />
                  <Point X="28.370376953125" Y="-0.173034561157" />
                  <Point X="28.37316015625" Y="-0.183990432739" />
                  <Point X="28.380005859375" Y="-0.205487838745" />
                  <Point X="28.384068359375" Y="-0.216029663086" />
                  <Point X="28.393841796875" Y="-0.23749407959" />
                  <Point X="28.39912890625" Y="-0.247484054565" />
                  <Point X="28.410853515625" Y="-0.266766082764" />
                  <Point X="28.417291015625" Y="-0.276057861328" />
                  <Point X="28.440263671875" Y="-0.305331054688" />
                  <Point X="28.472796875" Y="-0.346785552979" />
                  <Point X="28.47871875" Y="-0.353633789062" />
                  <Point X="28.501142578125" Y="-0.375808013916" />
                  <Point X="28.530177734375" Y="-0.39872567749" />
                  <Point X="28.54149609375" Y="-0.406404876709" />
                  <Point X="28.57978515625" Y="-0.4285362854" />
                  <Point X="28.634005859375" Y="-0.45987713623" />
                  <Point X="28.639498046875" Y="-0.462816009521" />
                  <Point X="28.659154296875" Y="-0.47201473999" />
                  <Point X="28.683025390625" Y="-0.481026916504" />
                  <Point X="28.6919921875" Y="-0.483912719727" />
                  <Point X="29.4539765625" Y="-0.68808605957" />
                  <Point X="29.784880859375" Y="-0.776751342773" />
                  <Point X="29.761619140625" Y="-0.931036010742" />
                  <Point X="29.728666015625" Y="-1.075440917969" />
                  <Point X="29.7278046875" Y="-1.079219604492" />
                  <Point X="29.06080078125" Y="-0.991407226562" />
                  <Point X="28.436783203125" Y="-0.909253540039" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042419434" />
                  <Point X="28.36672265625" Y="-0.910840820312" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.2793359375" Y="-0.92900970459" />
                  <Point X="28.17291796875" Y="-0.952140014648" />
                  <Point X="28.157876953125" Y="-0.956742126465" />
                  <Point X="28.1287578125" Y="-0.968365600586" />
                  <Point X="28.114681640625" Y="-0.975387023926" />
                  <Point X="28.086853515625" Y="-0.99227935791" />
                  <Point X="28.074123046875" Y="-1.001530517578" />
                  <Point X="28.050373046875" Y="-1.02200213623" />
                  <Point X="28.0393515625" Y="-1.03322253418" />
                  <Point X="27.9939296875" Y="-1.087850341797" />
                  <Point X="27.929607421875" Y="-1.165210571289" />
                  <Point X="27.921326171875" Y="-1.176848876953" />
                  <Point X="27.906603515625" Y="-1.201235473633" />
                  <Point X="27.900162109375" Y="-1.213984008789" />
                  <Point X="27.8888203125" Y="-1.241369140625" />
                  <Point X="27.88436328125" Y="-1.254934692383" />
                  <Point X="27.877533203125" Y="-1.282580566406" />
                  <Point X="27.87516015625" Y="-1.296661010742" />
                  <Point X="27.868650390625" Y="-1.367406738281" />
                  <Point X="27.859431640625" Y="-1.467591308594" />
                  <Point X="27.859291015625" Y="-1.483315551758" />
                  <Point X="27.861607421875" Y="-1.514580444336" />
                  <Point X="27.864064453125" Y="-1.530121337891" />
                  <Point X="27.871794921875" Y="-1.561742797852" />
                  <Point X="27.876787109375" Y="-1.576667602539" />
                  <Point X="27.88916015625" Y="-1.605480957031" />
                  <Point X="27.896541015625" Y="-1.619369506836" />
                  <Point X="27.938126953125" Y="-1.684055908203" />
                  <Point X="27.997021484375" Y="-1.775659912109" />
                  <Point X="28.001740234375" Y="-1.782348632812" />
                  <Point X="28.019802734375" Y="-1.804459472656" />
                  <Point X="28.04349609375" Y="-1.828124511719" />
                  <Point X="28.052798828125" Y="-1.836277709961" />
                  <Point X="28.75992578125" Y="-2.378875488281" />
                  <Point X="29.087171875" Y="-2.629980224609" />
                  <Point X="29.045470703125" Y="-2.697457519531" />
                  <Point X="29.001275390625" Y="-2.760251953125" />
                  <Point X="28.404228515625" Y="-2.415546386719" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.68167578125" Y="-2.050185546875" />
                  <Point X="27.555021484375" Y="-2.027311889648" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.444845703125" Y="-2.035136108398" />
                  <Point X="27.4150703125" Y="-2.044959594727" />
                  <Point X="27.40058984375" Y="-2.051108154297" />
                  <Point X="27.3262890625" Y="-2.090211669922" />
                  <Point X="27.221072265625" Y="-2.145587158203" />
                  <Point X="27.208970703125" Y="-2.153169677734" />
                  <Point X="27.1860390625" Y="-2.170063232422" />
                  <Point X="27.175208984375" Y="-2.179374267578" />
                  <Point X="27.15425" Y="-2.200333496094" />
                  <Point X="27.144939453125" Y="-2.211162109375" />
                  <Point X="27.128046875" Y="-2.234092529297" />
                  <Point X="27.12046484375" Y="-2.246194335938" />
                  <Point X="27.081361328125" Y="-2.320494384766" />
                  <Point X="27.025984375" Y="-2.425712402344" />
                  <Point X="27.0198359375" Y="-2.440193115234" />
                  <Point X="27.01001171875" Y="-2.469971435547" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517442871094" />
                  <Point X="27.000279296875" Y="-2.533134033203" />
                  <Point X="27.00068359375" Y="-2.564484130859" />
                  <Point X="27.0021875" Y="-2.580143066406" />
                  <Point X="27.01833984375" Y="-2.669579833984" />
                  <Point X="27.04121484375" Y="-2.796233398438" />
                  <Point X="27.04301953125" Y="-2.804228759766" />
                  <Point X="27.051236328125" Y="-2.831537353516" />
                  <Point X="27.064068359375" Y="-2.862475097656" />
                  <Point X="27.069546875" Y="-2.873578613281" />
                  <Point X="27.5239453125" Y="-3.660622314453" />
                  <Point X="27.735892578125" Y="-4.027723632812" />
                  <Point X="27.7237578125" Y="-4.036082275391" />
                  <Point X="27.2580234375" Y="-3.429127929688" />
                  <Point X="26.83391796875" Y="-2.876422607422" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.685091796875" Y="-2.763936767578" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.311923828125" Y="-2.655393798828" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.969369140625" Y="-2.7843515625" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.85265625" Y="-2.883086914062" />
                  <Point X="25.832185546875" Y="-2.9068359375" />
                  <Point X="25.822935546875" Y="-2.919561767578" />
                  <Point X="25.80604296875" Y="-2.947387939453" />
                  <Point X="25.79901953125" Y="-2.961467285156" />
                  <Point X="25.78739453125" Y="-2.99058984375" />
                  <Point X="25.78279296875" Y="-3.005633056641" />
                  <Point X="25.760521484375" Y="-3.108106201172" />
                  <Point X="25.728978515625" Y="-3.253220947266" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677001953" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520019531" />
                  <Point X="25.833087890625" Y="-4.152313964844" />
                  <Point X="25.82086328125" Y="-4.106691894531" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480122314453" />
                  <Point X="25.642146484375" Y="-3.453578857422" />
                  <Point X="25.6267890625" Y="-3.423815429688" />
                  <Point X="25.62041015625" Y="-3.413209228516" />
                  <Point X="25.55264453125" Y="-3.315573242188" />
                  <Point X="25.456681640625" Y="-3.177308837891" />
                  <Point X="25.446671875" Y="-3.165171386719" />
                  <Point X="25.424787109375" Y="-3.142715820312" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104936767578" />
                  <Point X="25.3452421875" Y="-3.090829345703" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.225794921875" Y="-3.052393554688" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.830154296875" Y="-3.038850341797" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.6670703125" Y="-3.090829589844" />
                  <Point X="24.6390703125" Y="-3.104937255859" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.587525390625" Y="-3.142717285156" />
                  <Point X="24.565640625" Y="-3.165173828125" />
                  <Point X="24.555630859375" Y="-3.177310791016" />
                  <Point X="24.4878671875" Y="-3.274946533203" />
                  <Point X="24.391904296875" Y="-3.413211181641" />
                  <Point X="24.38753125" Y="-3.420132080078" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.122458984375" Y="-4.364171386719" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.322877213614" Y="3.0065991661" />
                  <Point X="20.831056623282" Y="2.249261870565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.405262412937" Y="2.95903379449" />
                  <Point X="20.906658170247" Y="2.191250590363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.058234835625" Y="3.790095696941" />
                  <Point X="21.968407231443" Y="3.651773316478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.487647621233" Y="2.911468436699" />
                  <Point X="20.98225972922" Y="2.133239328652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.446172100559" Y="1.307736771742" />
                  <Point X="20.304421057976" Y="1.089459307685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.283832829804" Y="3.9630586905" />
                  <Point X="22.021717440412" Y="3.559436385899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.570032881318" Y="2.863903158654" />
                  <Point X="21.057861311927" Y="2.075228103488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.550524900348" Y="1.293998538429" />
                  <Point X="20.249974378418" Y="0.831191319854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.475586101744" Y="4.083905382074" />
                  <Point X="22.075028069332" Y="3.467100101989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.652418141403" Y="2.81633788061" />
                  <Point X="21.133462894634" Y="2.017216878323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.654877700137" Y="1.280260305116" />
                  <Point X="20.216546912921" Y="0.605290083321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.65279780104" Y="4.182360015415" />
                  <Point X="22.128338698252" Y="3.374763818078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.734803401487" Y="2.768772602565" />
                  <Point X="21.209064477341" Y="1.959205653159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.759230499926" Y="1.266522071803" />
                  <Point X="20.3052885817" Y="0.567512816321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.815276510564" Y="4.258127833993" />
                  <Point X="22.181649327173" Y="3.282427534167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.825753406963" Y="2.734395875872" />
                  <Point X="21.284666060048" Y="1.901194427995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.863583254177" Y="1.252783768369" />
                  <Point X="20.401773824904" Y="0.541659608272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.876628033101" Y="4.17817344044" />
                  <Point X="22.231270678112" Y="3.184410260353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.932031565794" Y="2.723622435491" />
                  <Point X="21.360267642756" Y="1.843183202831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.967935995915" Y="1.239045445665" />
                  <Point X="20.498259068108" Y="0.515806400224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.947773806979" Y="4.113300871375" />
                  <Point X="22.251320728717" Y="3.040857177218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.06340527798" Y="2.751492758468" />
                  <Point X="21.435869225463" Y="1.785171977666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.072288737652" Y="1.22530712296" />
                  <Point X="20.594744311312" Y="0.489953192175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.031810037914" Y="4.068277865498" />
                  <Point X="21.505081111709" Y="1.717321482793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.176641479389" Y="1.211568800255" />
                  <Point X="20.691229586047" Y="0.464100032679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.122520697139" Y="4.033532577899" />
                  <Point X="21.556916164187" Y="1.622713010418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.286869168513" Y="1.206877103196" />
                  <Point X="20.787714873231" Y="0.438246892354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.592472401605" Y="4.582767288707" />
                  <Point X="23.530626145751" Y="4.487532406175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.240487188612" Y="4.040757591439" />
                  <Point X="21.573174725238" Y="1.473321545359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.430918931431" Y="1.254266832575" />
                  <Point X="20.884200160415" Y="0.412393752029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.730961951705" Y="4.621595041177" />
                  <Point X="23.508043830829" Y="4.278331237042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.399633391481" Y="4.111393799777" />
                  <Point X="20.9806854476" Y="0.386540611703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.323691127081" Y="-0.625141923889" />
                  <Point X="20.229969085141" Y="-0.769461212609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.869451388771" Y="4.660422619589" />
                  <Point X="21.077170734784" Y="0.360687471378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.460828724925" Y="-0.58839599533" />
                  <Point X="20.25041236065" Y="-0.912408782491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.007940825836" Y="4.699250198002" />
                  <Point X="21.173656021968" Y="0.334834331053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.597966322769" Y="-0.551650066772" />
                  <Point X="20.277410999481" Y="-1.045261978069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.137521941607" Y="4.724360164565" />
                  <Point X="21.270141309153" Y="0.308981190728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.735103920614" Y="-0.514904138214" />
                  <Point X="20.309387930699" Y="-1.170449275619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.260113912635" Y="4.738707792013" />
                  <Point X="21.366626596337" Y="0.283128050403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.872241518458" Y="-0.478158209656" />
                  <Point X="20.347928527576" Y="-1.285529414387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.382705881932" Y="4.753055416793" />
                  <Point X="21.463111883521" Y="0.257274910078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.009379090715" Y="-0.441412320498" />
                  <Point X="20.471792981903" Y="-1.269222334492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.505297813236" Y="4.767402983072" />
                  <Point X="21.553178448084" Y="0.221537803675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.146516661931" Y="-0.404666432944" />
                  <Point X="20.595657436229" Y="-1.252915254596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.627889744541" Y="4.78175054935" />
                  <Point X="21.629352424721" Y="0.16440798787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.283654233147" Y="-0.36792054539" />
                  <Point X="20.719521890556" Y="-1.2366081747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.666004525947" Y="4.666014712258" />
                  <Point X="21.683917718181" Y="0.074003717925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.420791804363" Y="-0.331174657836" />
                  <Point X="20.843386334302" Y="-1.220301111097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.699090567796" Y="4.542535295308" />
                  <Point X="21.70189312135" Y="-0.072744042108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.571584931914" Y="-0.273401057519" />
                  <Point X="20.967250757246" Y="-1.203994079526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.732176495855" Y="4.419055703138" />
                  <Point X="21.09111518019" Y="-1.187687047956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.765262423914" Y="4.295576110968" />
                  <Point X="21.214979603134" Y="-1.171380016385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.810053018937" Y="4.190120125368" />
                  <Point X="21.338844026077" Y="-1.155072984814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.879005848961" Y="4.121870718894" />
                  <Point X="21.462708449021" Y="-1.138765953244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.420077129795" Y="4.780619973593" />
                  <Point X="25.346770897791" Y="4.667738275301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.970566425133" Y="4.088434188624" />
                  <Point X="21.586572871965" Y="-1.122458921673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.526138333949" Y="4.769512452305" />
                  <Point X="25.267202851219" Y="4.370786774561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.093115140612" Y="4.102715208367" />
                  <Point X="21.710437294909" Y="-1.106151890103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.865179567667" Y="-2.407734649676" />
                  <Point X="20.829888855563" Y="-2.462077580793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.632199533493" Y="4.758404923921" />
                  <Point X="21.82524739789" Y="-1.103787288614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.090964471365" Y="-2.234483840698" />
                  <Point X="20.884114338434" Y="-2.553005113159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.738260733038" Y="4.747297395536" />
                  <Point X="21.917354426841" Y="-1.136382355396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.316749375062" Y="-2.061233031719" />
                  <Point X="20.938339688596" Y="-2.643932849877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.843059885184" Y="4.734246484579" />
                  <Point X="21.991498021535" Y="-1.196638685218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.542534521756" Y="-1.88798184856" />
                  <Point X="20.992565038758" Y="-2.734860586596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.940981563399" Y="4.710605192476" />
                  <Point X="22.043377479035" Y="-1.291178779857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.768319848299" Y="-1.714730388456" />
                  <Point X="21.053254823138" Y="-2.815833967552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.038903241614" Y="4.686963900373" />
                  <Point X="21.114378879967" Y="-2.896138627578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.136824824008" Y="4.663322460721" />
                  <Point X="21.175502936796" Y="-2.976443287604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.234746327426" Y="4.639680899453" />
                  <Point X="21.295011158823" Y="-2.966844217201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.332667830843" Y="4.616039338185" />
                  <Point X="21.476231773583" Y="-2.862216395395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.43058933426" Y="4.592397776918" />
                  <Point X="21.657452277005" Y="-2.757588745035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.523735964898" Y="4.56140355635" />
                  <Point X="21.838672689735" Y="-2.652961234327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.615416236499" Y="4.528151340879" />
                  <Point X="22.019893102465" Y="-2.54833372362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.707095990716" Y="4.494898328704" />
                  <Point X="22.201113515195" Y="-2.443706212913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.798775730453" Y="4.461645294234" />
                  <Point X="22.370472915038" Y="-2.357343060386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.888771814102" Y="4.425799656741" />
                  <Point X="22.485944550538" Y="-2.353959788149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.975658377005" Y="4.385165777198" />
                  <Point X="22.57315248406" Y="-2.394098800335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.062544816966" Y="4.34453170834" />
                  <Point X="22.642031814566" Y="-2.462461386141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.149431134965" Y="4.303897451679" />
                  <Point X="22.686713533791" Y="-2.568085025768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.236317452965" Y="4.263263195018" />
                  <Point X="22.336825178384" Y="-3.28129329909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.319264746395" Y="4.21656337243" />
                  <Point X="22.068218688351" Y="-3.869338475729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.401446675356" Y="4.168684991913" />
                  <Point X="22.143734075689" Y="-3.927482430122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.483628096603" Y="4.120805829583" />
                  <Point X="22.219249656885" Y="-3.985626086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.565809489984" Y="4.072926624343" />
                  <Point X="22.297723105328" Y="-4.039215025736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.646931111388" Y="4.023415513369" />
                  <Point X="22.37851231733" Y="-4.089238002304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.72441977356" Y="3.968310135755" />
                  <Point X="22.459301529332" Y="-4.139260978872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.801908494815" Y="3.913204849123" />
                  <Point X="22.689155684461" Y="-3.959744072185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.096121644593" Y="2.651960952961" />
                  <Point X="22.889260560355" Y="-3.826037038291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.015385911505" Y="2.353211372667" />
                  <Point X="23.034729912294" Y="-3.776461333516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.045057707131" Y="2.224474477582" />
                  <Point X="23.144513319196" Y="-3.781837165203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.100889704245" Y="2.136020760214" />
                  <Point X="23.253163248152" Y="-3.788958399867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.16900153034" Y="2.066476321255" />
                  <Point X="23.361813119244" Y="-3.796079723633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.248523262626" Y="2.014501597081" />
                  <Point X="23.456725511334" Y="-3.824354910004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.341879136189" Y="1.983829582363" />
                  <Point X="23.53391168197" Y="-3.879926083735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.449922113508" Y="1.975773724139" />
                  <Point X="23.606083169798" Y="-3.943219191827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.582273456526" Y="2.005149466582" />
                  <Point X="23.67825468871" Y="-4.006512252052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.754853882216" Y="2.096472563959" />
                  <Point X="23.749731825842" Y="-4.070874566453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.936074415284" Y="2.201100259969" />
                  <Point X="23.804997720864" Y="-4.160200004599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.117294948351" Y="2.30572795598" />
                  <Point X="24.560038937244" Y="-3.171965942842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.359871430509" Y="-3.480196873358" />
                  <Point X="23.83503061503" Y="-4.288380856694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.298515481418" Y="2.41035565199" />
                  <Point X="24.741990121992" Y="-3.066213141908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.279364322658" Y="-3.778594401659" />
                  <Point X="23.861591624601" Y="-4.421907942236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.479736014485" Y="2.514983348" />
                  <Point X="24.883858461308" Y="-3.022182510305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.199796104968" Y="-4.075546165897" />
                  <Point X="23.879923028335" Y="-4.568107509472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.660956621873" Y="2.619611158454" />
                  <Point X="25.01298885066" Y="-2.997766601563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.120227861101" Y="-4.372497970444" />
                  <Point X="23.883128264775" Y="-4.737599331761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.842177244602" Y="2.724238992531" />
                  <Point X="28.237798469669" Y="1.793577292138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.002694403499" Y="1.431548777793" />
                  <Point X="25.113434485216" Y="-3.017521341724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.040658709854" Y="-4.669451172234" />
                  <Point X="23.981592852444" Y="-4.760404616617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.967671409663" Y="2.743055606888" />
                  <Point X="28.463583458039" Y="1.966828231502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.019162813729" Y="1.282480452133" />
                  <Point X="25.207708118766" Y="-3.046780129993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.027210517722" Y="2.660310339781" />
                  <Point X="28.689368582867" Y="2.14007938099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.062584307062" Y="1.174916234808" />
                  <Point X="25.301981507922" Y="-3.076039294596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.083227779149" Y="2.572141904438" />
                  <Point X="28.915154228195" Y="2.313331331979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.121003405057" Y="1.090446303445" />
                  <Point X="25.389703268786" Y="-3.115387082061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.194621198593" Y="1.02938031084" />
                  <Point X="25.459824568343" Y="-3.18183720324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.278790034899" Y="0.98456149933" />
                  <Point X="25.51834399347" Y="-3.266152644368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.376694260163" Y="0.960893332039" />
                  <Point X="25.915319415181" Y="-2.829291554563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.773734074757" Y="-3.047313859672" />
                  <Point X="25.576863596392" Y="-3.350467811715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.484066972379" Y="0.951805356066" />
                  <Point X="26.12570257449" Y="-2.67975735215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.724728559352" Y="-3.297203189462" />
                  <Point X="25.633747648479" Y="-3.437301506491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.607089610931" Y="0.966816153345" />
                  <Point X="26.251168244529" Y="-2.660984616279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.740314360274" Y="-3.447630614272" />
                  <Point X="25.672322229938" Y="-3.552329313592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.730954061841" Y="0.983123227979" />
                  <Point X="26.371642242036" Y="-2.649898382052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.759407513796" Y="-3.592657189699" />
                  <Point X="25.705408398025" Y="-3.675808536151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.85481851275" Y="0.999430302613" />
                  <Point X="26.482296746024" Y="-2.653932841857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.778500667319" Y="-3.737683765125" />
                  <Point X="25.738494566112" Y="-3.799287758711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.97868296366" Y="1.015737377247" />
                  <Point X="28.54286424084" Y="0.344635395402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.361835235171" Y="0.065875172139" />
                  <Point X="26.57154160227" Y="-2.690935268106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.797593820841" Y="-3.882710340551" />
                  <Point X="25.7715807342" Y="-3.922766981271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.102547414569" Y="1.032044451881" />
                  <Point X="28.708902037088" Y="0.42588372693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.358895336473" Y="-0.113079328448" />
                  <Point X="26.651452816086" Y="-2.742310243318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.816686974363" Y="-4.027736915977" />
                  <Point X="25.804666902287" Y="-4.04624620383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.226411865479" Y="1.048351526515" />
                  <Point X="28.846039579085" Y="0.462629569491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.392824649609" Y="-0.235260221486" />
                  <Point X="26.731363888741" Y="-2.7936854359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.35027631271" Y="1.064658595484" />
                  <Point X="28.983177121082" Y="0.499375412051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.451388751157" Y="-0.319506866959" />
                  <Point X="27.288597180012" Y="-2.110048867584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.000543394967" Y="-2.55361279887" />
                  <Point X="26.80849523163" Y="-2.849341036958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.474140739327" Y="1.080965632711" />
                  <Point X="29.120314663079" Y="0.536121254612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.518965274452" Y="-0.389875599945" />
                  <Point X="28.148374616665" Y="-0.960535169788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.866091754826" Y="-1.39521265862" />
                  <Point X="27.451569793018" Y="-2.033520504339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.022728412628" Y="-2.693878321035" />
                  <Point X="26.871974540965" Y="-2.926018926169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.598005165944" Y="1.097272669938" />
                  <Point X="29.257452223781" Y="0.572867125978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.59966928141" Y="-0.440029780774" />
                  <Point X="28.282579898386" Y="-0.928304612091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.871637775183" Y="-1.561099989766" />
                  <Point X="27.567421878691" Y="-2.029551390211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.049770284173" Y="-2.826664944071" />
                  <Point X="26.933325883137" Y="-3.005973597459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.708983480523" Y="1.093736834717" />
                  <Point X="29.394589859463" Y="0.6096131128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.68575041276" Y="-0.481903916147" />
                  <Point X="28.408913857193" Y="-0.908194828768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.921678802923" Y="-1.658471017981" />
                  <Point X="27.668805832643" Y="-2.047861245211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.099211556883" Y="-2.924959514043" />
                  <Point X="26.994677225308" Y="-3.08592826875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.739868330923" Y="0.966867880178" />
                  <Point X="29.531727495145" Y="0.646359099623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.782049636501" Y="-0.50804356905" />
                  <Point X="28.514828266498" Y="-0.919528394299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.978031454758" Y="-1.746122997386" />
                  <Point X="27.770189719484" Y="-2.066171203553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.152521962792" Y="-3.017296141361" />
                  <Point X="27.05602856748" Y="-3.165882940041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.765500989124" Y="0.831911258887" />
                  <Point X="29.668865130826" Y="0.683105086446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.878534898743" Y="-0.533896747782" />
                  <Point X="28.619181000977" Y="-0.933266728181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.040194851067" Y="-1.824827214961" />
                  <Point X="27.86045665612" Y="-2.10159976402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.205832368701" Y="-3.109632768679" />
                  <Point X="27.117379909652" Y="-3.245837611331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.975020160986" Y="-0.559749926514" />
                  <Point X="28.723533735456" Y="-0.947005062062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.115025345372" Y="-1.884025812139" />
                  <Point X="27.942841990493" Y="-2.149164927671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.259142774609" Y="-3.201969395997" />
                  <Point X="27.178731251824" Y="-3.325792282622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.071505423228" Y="-0.585603105246" />
                  <Point X="28.827886469935" Y="-0.960743395944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.190626958777" Y="-1.942036990033" />
                  <Point X="28.025227324865" Y="-2.196730091322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.312453180518" Y="-3.294306023315" />
                  <Point X="27.240082593995" Y="-3.405746953912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.167990685471" Y="-0.611456283978" />
                  <Point X="28.932239204414" Y="-0.974481729826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.266228572182" Y="-2.000048167926" />
                  <Point X="28.107612659238" Y="-2.244295254973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.365763586427" Y="-3.386642650633" />
                  <Point X="27.301434029744" Y="-3.485701481107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.264475947714" Y="-0.63730946271" />
                  <Point X="29.036591938893" Y="-0.988220063707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.341830185587" Y="-2.05805934582" />
                  <Point X="28.189997993611" Y="-2.291860418624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.419073992336" Y="-3.478979277952" />
                  <Point X="27.362785504166" Y="-3.56565594875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.360961209956" Y="-0.663162641442" />
                  <Point X="29.14094471512" Y="-1.001958333301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.417431798993" Y="-2.116070523713" />
                  <Point X="28.272383327983" Y="-2.339425582275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.472384398244" Y="-3.57131590527" />
                  <Point X="27.424136978589" Y="-3.645610416392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.457446474293" Y="-0.689015816949" />
                  <Point X="29.245297503959" Y="-1.015696583476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.493033412398" Y="-2.174081701607" />
                  <Point X="28.354768662356" Y="-2.386990745926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.525694812453" Y="-3.663652519807" />
                  <Point X="27.485488453011" Y="-3.725564884035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.553931794756" Y="-0.714868906029" />
                  <Point X="29.349650292798" Y="-1.02943483365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.568635025803" Y="-2.2320928795" />
                  <Point X="28.437153980594" Y="-2.434555934422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.579005471271" Y="-3.755988757679" />
                  <Point X="27.546839927433" Y="-3.805519351677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.65041711522" Y="-0.740721995108" />
                  <Point X="29.454003081637" Y="-1.043173083825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.644236639208" Y="-2.290104057394" />
                  <Point X="28.519539274596" Y="-2.482121160239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.63231613009" Y="-3.84832499555" />
                  <Point X="27.608191401856" Y="-3.88547381932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.746902435684" Y="-0.766575084188" />
                  <Point X="29.558355870475" Y="-1.056911333999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.719838252613" Y="-2.348115235287" />
                  <Point X="28.601924568598" Y="-2.529686386055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.685626788908" Y="-3.940661233421" />
                  <Point X="27.669542876278" Y="-3.965428286962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.76411196996" Y="-0.914502178895" />
                  <Point X="29.662708659314" Y="-1.070649584174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.795439871094" Y="-2.406126405364" />
                  <Point X="28.6843098626" Y="-2.577251611872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.871041495306" Y="-2.464137566616" />
                  <Point X="28.766695156602" Y="-2.624816837689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.946643119518" Y="-2.522148727868" />
                  <Point X="28.849080450604" Y="-2.672382063505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.02224474373" Y="-2.580159889121" />
                  <Point X="28.931465744606" Y="-2.719947289322" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.6373359375" Y="-4.1558671875" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.3965546875" Y="-3.423908203125" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.169474609375" Y="-3.233854492188" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.886474609375" Y="-3.220311767578" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.64395703125" Y="-3.383279541016" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.305986328125" Y="-4.413347167969" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.0739609375" Y="-4.971879394531" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.778830078125" Y="-4.906952148438" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.670892578125" Y="-4.702633300781" />
                  <Point X="23.6908515625" Y="-4.5510390625" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.665265625" Y="-4.408815429688" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.517173828125" Y="-4.117962402344" />
                  <Point X="23.38045703125" Y="-3.998064208984" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.222625" Y="-3.977364501953" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.903353515625" Y="-4.045131591797" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157293945313" />
                  <Point X="22.608453125" Y="-4.329077636719" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.399517578125" Y="-4.325717773438" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.97671875" Y="-4.038682128906" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.18585546875" Y="-3.162783691406" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.59759375" />
                  <Point X="22.486021484375" Y="-2.568764648438" />
                  <Point X="22.468673828125" Y="-2.551416015625" />
                  <Point X="22.43984375" Y="-2.537198730469" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.65258203125" Y="-2.979793701172" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.040037109375" Y="-3.112174804688" />
                  <Point X="20.838302734375" Y="-2.847136962891" />
                  <Point X="20.718251953125" Y="-2.645829589844" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.297119140625" Y="-1.836806518555" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396013183594" />
                  <Point X="21.8618828125" Y="-1.366265869141" />
                  <Point X="21.859673828125" Y="-1.334595703125" />
                  <Point X="21.838841796875" Y="-1.310639526367" />
                  <Point X="21.812359375" Y="-1.295052978516" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.826427734375" Y="-1.414173339844" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.151509765625" Y="-1.320090087891" />
                  <Point X="20.072607421875" Y="-1.011188293457" />
                  <Point X="20.04084765625" Y="-0.789120056152" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="20.828427734375" Y="-0.293195648193" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.46958984375" Y="-0.113453697205" />
                  <Point X="21.497677734375" Y="-0.090645721436" />
                  <Point X="21.508931640625" Y="-0.063570365906" />
                  <Point X="21.516599609375" Y="-0.031280040741" />
                  <Point X="21.510525390625" Y="-0.004123178959" />
                  <Point X="21.497677734375" Y="0.028085645676" />
                  <Point X="21.47437109375" Y="0.047576118469" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.572880859375" Y="0.299109039307" />
                  <Point X="20.001814453125" Y="0.452126068115" />
                  <Point X="20.03150390625" Y="0.652771118164" />
                  <Point X="20.08235546875" Y="0.996414978027" />
                  <Point X="20.146291015625" Y="1.232357788086" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.80652734375" Y="1.451934814453" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.29371875" Y="1.403882446289" />
                  <Point X="21.32972265625" Y="1.415234008789" />
                  <Point X="21.348466796875" Y="1.426056274414" />
                  <Point X="21.3608828125" Y="1.443786743164" />
                  <Point X="21.371083984375" Y="1.468414916992" />
                  <Point X="21.385529296875" Y="1.503291137695" />
                  <Point X="21.389287109375" Y="1.524606201172" />
                  <Point X="21.38368359375" Y="1.545512451172" />
                  <Point X="21.371375" Y="1.569157714844" />
                  <Point X="21.353943359375" Y="1.602642211914" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.8411953125" Y="2.001992797852" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.642396484375" Y="2.448488769531" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="21.009345703125" Y="3.004695556641" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.57048046875" Y="3.083038085938" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.89689453125" Y="2.917336914062" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775146484" />
                  <Point X="21.986748046875" Y="2.927404296875" />
                  <Point X="22.011880859375" Y="2.952536865234" />
                  <Point X="22.04747265625" Y="2.988127685547" />
                  <Point X="22.0591015625" Y="3.006382080078" />
                  <Point X="22.061927734375" Y="3.027841064453" />
                  <Point X="22.058830078125" Y="3.063248779297" />
                  <Point X="22.054443359375" Y="3.113390625" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.8268828125" Y="3.51690234375" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.902986328125" Y="3.910484863281" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.51386328125" Y="4.322524902344" />
                  <Point X="22.858451171875" Y="4.513971679688" />
                  <Point X="22.94940625" Y="4.395436523437" />
                  <Point X="23.032173828125" Y="4.287572753906" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.088162109375" Y="4.253145507813" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.16487890625" Y="4.218491699219" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.227240234375" Y="4.239252929688" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.29448828125" />
                  <Point X="23.32727734375" Y="4.336860351562" />
                  <Point X="23.346197265625" Y="4.396864746094" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.322953125" Y="4.609314941406" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.586400390625" Y="4.778391601562" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.3552734375" Y="4.941141601562" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.87787890625" Y="4.609397460938" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.167716796875" Y="4.733608398438" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.47121484375" Y="4.966303710938" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.127736328125" Y="4.860975585938" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.682697265625" Y="4.705860351562" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.099416015625" Y="4.537039550781" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.5013671875" Y="4.330364746094" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.885939453125" Y="4.086592285156" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.586470703125" Y="3.121274169922" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491514892578" />
                  <Point X="27.215966796875" Y="2.458285644531" />
                  <Point X="27.2033828125" Y="2.411228515625" />
                  <Point X="27.202044921875" Y="2.392326171875" />
                  <Point X="27.205509765625" Y="2.363592285156" />
                  <Point X="27.210416015625" Y="2.322901367188" />
                  <Point X="27.218681640625" Y="2.300812744141" />
                  <Point X="27.236462890625" Y="2.274610107422" />
                  <Point X="27.261640625" Y="2.237504150391" />
                  <Point X="27.27494140625" Y="2.224203369141" />
                  <Point X="27.301142578125" Y="2.206423828125" />
                  <Point X="27.33825" Y="2.181245849609" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.389072265625" Y="2.169515380859" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.48189453125" Y="2.174832275391" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.4157421875" Y="2.6974296875" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.0654765625" Y="2.932438964844" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.288119140625" Y="2.600547851562" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.758134765625" Y="1.953356323242" />
                  <Point X="28.2886171875" Y="1.593082641602" />
                  <Point X="28.27937109375" Y="1.583833374023" />
                  <Point X="28.25545703125" Y="1.552634155273" />
                  <Point X="28.22158984375" Y="1.508452026367" />
                  <Point X="28.21312109375" Y="1.491501708984" />
                  <Point X="28.204212890625" Y="1.459647216797" />
                  <Point X="28.191595703125" Y="1.414537231445" />
                  <Point X="28.190779296875" Y="1.390965820312" />
                  <Point X="28.19809375" Y="1.35552355957" />
                  <Point X="28.20844921875" Y="1.305333007812" />
                  <Point X="28.215646484375" Y="1.287955810547" />
                  <Point X="28.235537109375" Y="1.257723266602" />
                  <Point X="28.263703125" Y="1.214910400391" />
                  <Point X="28.280947265625" Y="1.198819824219" />
                  <Point X="28.309771484375" Y="1.182594482422" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.4075390625" Y="1.148468994141" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.306716796875" Y="1.250563598633" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.880298828125" Y="1.193287841797" />
                  <Point X="29.93919140625" Y="0.951367736816" />
                  <Point X="29.966140625" Y="0.778279052734" />
                  <Point X="29.997859375" Y="0.574556335449" />
                  <Point X="29.27924609375" Y="0.382004333496" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819198608" />
                  <Point X="28.690798828125" Y="0.210687652588" />
                  <Point X="28.636578125" Y="0.179346832275" />
                  <Point X="28.622265625" Y="0.166926849365" />
                  <Point X="28.59929296875" Y="0.137653594971" />
                  <Point X="28.566759765625" Y="0.096199172974" />
                  <Point X="28.556986328125" Y="0.074734649658" />
                  <Point X="28.549328125" Y="0.034748916626" />
                  <Point X="28.538484375" Y="-0.021875907898" />
                  <Point X="28.538484375" Y="-0.040684169769" />
                  <Point X="28.546142578125" Y="-0.080669906616" />
                  <Point X="28.556986328125" Y="-0.137294723511" />
                  <Point X="28.566759765625" Y="-0.158759246826" />
                  <Point X="28.589732421875" Y="-0.188032348633" />
                  <Point X="28.622265625" Y="-0.229486923218" />
                  <Point X="28.636578125" Y="-0.241906906128" />
                  <Point X="28.6748671875" Y="-0.264038299561" />
                  <Point X="28.729087890625" Y="-0.295379272461" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.50315234375" Y="-0.504560272217" />
                  <Point X="29.998068359375" Y="-0.637172546387" />
                  <Point X="29.981314453125" Y="-0.748302856445" />
                  <Point X="29.948431640625" Y="-0.966412963867" />
                  <Point X="29.913904296875" Y="-1.117712890625" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.036" Y="-1.179781738281" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.31969140625" Y="-1.114674682617" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.1854453125" Y="-1.154697387695" />
                  <Point X="28.1400234375" Y="-1.209325439453" />
                  <Point X="28.075701171875" Y="-1.286685424805" />
                  <Point X="28.064359375" Y="-1.314070678711" />
                  <Point X="28.057849609375" Y="-1.38481628418" />
                  <Point X="28.048630859375" Y="-1.485000976562" />
                  <Point X="28.056361328125" Y="-1.516622436523" />
                  <Point X="28.097947265625" Y="-1.581308837891" />
                  <Point X="28.156841796875" Y="-1.672912963867" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.87558984375" Y="-2.228138183594" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.296822265625" Y="-2.652154541016" />
                  <Point X="29.204134765625" Y="-2.802138183594" />
                  <Point X="29.13272265625" Y="-2.903604492188" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.309228515625" Y="-2.580091308594" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.64790625" Y="-2.237160644531" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.41477734375" Y="-2.258348388672" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334683105469" />
                  <Point X="27.249498046875" Y="-2.408983154297" />
                  <Point X="27.19412109375" Y="-2.514201171875" />
                  <Point X="27.1891640625" Y="-2.546375" />
                  <Point X="27.20531640625" Y="-2.635811767578" />
                  <Point X="27.22819140625" Y="-2.762465332031" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.6884921875" Y="-3.565622314453" />
                  <Point X="27.98667578125" Y="-4.082087646484" />
                  <Point X="27.945283203125" Y="-4.11165234375" />
                  <Point X="27.83529296875" Y="-4.190215820312" />
                  <Point X="27.755458984375" Y="-4.241891113281" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="27.10728515625" Y="-3.544792724609" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.582341796875" Y="-2.923757080078" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.329333984375" Y="-2.844594482422" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.090841796875" Y="-2.930447265625" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045985595703" />
                  <Point X="25.946185546875" Y="-3.148458740234" />
                  <Point X="25.914642578125" Y="-3.293573486328" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="26.042703125" Y="-4.288852050781" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.09840234375" Y="-4.940438476562" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.920587890625" Y="-4.976645996094" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#158" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.079981186327" Y="4.653618339828" Z="1.05" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.05" />
                  <Point X="-0.656696476724" Y="5.022082754629" Z="1.05" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.05" />
                  <Point X="-1.433255108269" Y="4.857815414216" Z="1.05" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.05" />
                  <Point X="-1.732776755986" Y="4.63406860881" Z="1.05" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.05" />
                  <Point X="-1.726565220585" Y="4.383176266887" Z="1.05" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.05" />
                  <Point X="-1.798052375923" Y="4.316726810699" Z="1.05" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.05" />
                  <Point X="-1.894906586973" Y="4.328776350759" Z="1.05" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.05" />
                  <Point X="-2.017081832398" Y="4.457154943855" Z="1.05" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.05" />
                  <Point X="-2.51657756674" Y="4.397512577589" Z="1.05" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.05" />
                  <Point X="-3.133591612369" Y="3.981444782805" Z="1.05" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.05" />
                  <Point X="-3.222574465305" Y="3.523182335294" Z="1.05" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.05" />
                  <Point X="-2.99713781308" Y="3.090171221397" Z="1.05" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.05" />
                  <Point X="-3.029630941282" Y="3.019172626605" Z="1.05" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.05" />
                  <Point X="-3.10490524421" Y="2.998426886092" Z="1.05" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.05" />
                  <Point X="-3.410677013004" Y="3.15761948344" Z="1.05" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.05" />
                  <Point X="-4.0362733231" Y="3.066678038441" Z="1.05" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.05" />
                  <Point X="-4.406941786576" Y="2.504973787302" Z="1.05" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.05" />
                  <Point X="-4.195399287784" Y="1.993605169601" Z="1.05" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.05" />
                  <Point X="-3.679131338843" Y="1.577349571313" Z="1.05" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.05" />
                  <Point X="-3.681268667334" Y="1.518828085019" Z="1.05" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.05" />
                  <Point X="-3.727472651956" Y="1.482848499969" Z="1.05" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.05" />
                  <Point X="-4.193104969969" Y="1.532787153379" Z="1.05" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.05" />
                  <Point X="-4.908125981603" Y="1.276715050088" Z="1.05" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.05" />
                  <Point X="-5.024113582062" Y="0.691370206739" Z="1.05" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.05" />
                  <Point X="-4.446217194859" Y="0.28209278158" Z="1.05" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.05" />
                  <Point X="-3.560294381738" Y="0.037779253506" Z="1.05" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.05" />
                  <Point X="-3.543385648272" Y="0.012336674768" Z="1.05" />
                  <Point X="-3.539556741714" Y="0" Z="1.05" />
                  <Point X="-3.544978952863" Y="-0.017470276292" Z="1.05" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.05" />
                  <Point X="-3.565074213385" Y="-0.041096729628" Z="1.05" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.05" />
                  <Point X="-4.190670593914" Y="-0.213619275176" Z="1.05" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.05" />
                  <Point X="-5.014806303469" Y="-0.76491930455" Z="1.05" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.05" />
                  <Point X="-4.90309180258" Y="-1.301184089106" Z="1.05" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.05" />
                  <Point X="-4.173202962245" Y="-1.43246556906" Z="1.05" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.05" />
                  <Point X="-3.20363707132" Y="-1.315998773721" Z="1.05" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.05" />
                  <Point X="-3.197192020251" Y="-1.339885156527" Z="1.05" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.05" />
                  <Point X="-3.739475293838" Y="-1.765858905336" Z="1.05" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.05" />
                  <Point X="-4.330849293538" Y="-2.640159500498" Z="1.05" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.05" />
                  <Point X="-4.005905276083" Y="-3.111177981089" Z="1.05" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.05" />
                  <Point X="-3.32857474528" Y="-2.991814912705" Z="1.05" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.05" />
                  <Point X="-2.562671280536" Y="-2.565659188596" Z="1.05" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.05" />
                  <Point X="-2.863602100463" Y="-3.106503637212" Z="1.05" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.05" />
                  <Point X="-3.05994128564" Y="-4.047019153641" Z="1.05" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.05" />
                  <Point X="-2.63296170545" Y="-4.336948309516" Z="1.05" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.05" />
                  <Point X="-2.358036702029" Y="-4.328236022521" Z="1.05" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.05" />
                  <Point X="-2.075024585376" Y="-4.055424962871" Z="1.05" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.05" />
                  <Point X="-1.786801411359" Y="-3.995977542553" Z="1.05" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.05" />
                  <Point X="-1.52194934863" Y="-4.124274127957" Z="1.05" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.05" />
                  <Point X="-1.389929986802" Y="-4.387290197004" Z="1.05" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.05" />
                  <Point X="-1.384836323392" Y="-4.664826616928" Z="1.05" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.05" />
                  <Point X="-1.239786725292" Y="-4.924095004833" Z="1.05" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.05" />
                  <Point X="-0.941723636421" Y="-4.989683330361" Z="1.05" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.05" />
                  <Point X="-0.651873155196" Y="-4.395007822436" Z="1.05" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.05" />
                  <Point X="-0.321123655151" Y="-3.38050851126" Z="1.05" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.05" />
                  <Point X="-0.104861733605" Y="-3.23678458079" Z="1.05" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.05" />
                  <Point X="0.148497345756" Y="-3.250327464498" Z="1.05" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.05" />
                  <Point X="0.349322204281" Y="-3.421137244508" Z="1.05" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.05" />
                  <Point X="0.582881705205" Y="-4.137528207379" Z="1.05" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.05" />
                  <Point X="0.923369127278" Y="-4.994560928364" Z="1.05" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.05" />
                  <Point X="1.102949689595" Y="-4.957998692266" Z="1.05" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.05" />
                  <Point X="1.086119280946" Y="-4.251045398899" Z="1.05" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.05" />
                  <Point X="0.988887067678" Y="-3.127799047075" Z="1.05" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.05" />
                  <Point X="1.116651033026" Y="-2.937613692199" Z="1.05" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.05" />
                  <Point X="1.327759171211" Y="-2.863103964613" Z="1.05" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.05" />
                  <Point X="1.549145124295" Y="-2.934535179033" Z="1.05" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.05" />
                  <Point X="2.061459719335" Y="-3.543950625315" Z="1.05" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.05" />
                  <Point X="2.776471927318" Y="-4.252585356044" Z="1.05" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.05" />
                  <Point X="2.968189252217" Y="-4.121059521234" Z="1.05" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.05" />
                  <Point X="2.725636971877" Y="-3.509342310239" Z="1.05" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.05" />
                  <Point X="2.248363331904" Y="-2.59564495345" Z="1.05" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.05" />
                  <Point X="2.287586642604" Y="-2.400990147495" Z="1.05" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.05" />
                  <Point X="2.431908230877" Y="-2.271314667363" Z="1.05" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.05" />
                  <Point X="2.632861850062" Y="-2.255084677587" Z="1.05" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.05" />
                  <Point X="3.278071286868" Y="-2.592112528397" Z="1.05" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.05" />
                  <Point X="4.167454773641" Y="-2.901101846319" Z="1.05" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.05" />
                  <Point X="4.33319942813" Y="-2.647159558656" Z="1.05" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.05" />
                  <Point X="3.899869475733" Y="-2.15719046505" Z="1.05" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.05" />
                  <Point X="3.133849886627" Y="-1.522988934539" Z="1.05" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.05" />
                  <Point X="3.101481467206" Y="-1.358117847166" Z="1.05" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.05" />
                  <Point X="3.172314012308" Y="-1.210012166349" Z="1.05" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.05" />
                  <Point X="3.324152955603" Y="-1.132253915008" Z="1.05" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.05" />
                  <Point X="4.023318378155" Y="-1.198074014716" Z="1.05" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.05" />
                  <Point X="4.956493325757" Y="-1.097556816546" Z="1.05" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.05" />
                  <Point X="5.02459881111" Y="-0.724476708434" Z="1.05" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.05" />
                  <Point X="4.509937700852" Y="-0.424983893216" Z="1.05" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.05" />
                  <Point X="3.693731286879" Y="-0.189469688395" Z="1.05" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.05" />
                  <Point X="3.622909867022" Y="-0.12588371437" Z="1.05" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.05" />
                  <Point X="3.589092441153" Y="-0.039985730208" Z="1.05" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.05" />
                  <Point X="3.592279009271" Y="0.05662480101" Z="1.05" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.05" />
                  <Point X="3.632469571378" Y="0.138065024205" Z="1.05" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.05" />
                  <Point X="3.709664127473" Y="0.198679155847" Z="1.05" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.05" />
                  <Point X="4.286030116455" Y="0.364988039553" Z="1.05" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.05" />
                  <Point X="5.009388885173" Y="0.817251281688" Z="1.05" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.05" />
                  <Point X="4.922723448573" Y="1.236394527596" Z="1.05" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.05" />
                  <Point X="4.294034398583" Y="1.331415886483" Z="1.05" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.05" />
                  <Point X="3.407932438133" Y="1.229317978635" Z="1.05" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.05" />
                  <Point X="3.32814241691" Y="1.257445691619" Z="1.05" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.05" />
                  <Point X="3.271151337524" Y="1.316483958927" Z="1.05" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.05" />
                  <Point X="3.24090496664" Y="1.396907001822" Z="1.05" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.05" />
                  <Point X="3.246207526867" Y="1.477459187398" Z="1.05" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.05" />
                  <Point X="3.288982936159" Y="1.553495807999" Z="1.05" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.05" />
                  <Point X="3.782415926133" Y="1.944968742421" Z="1.05" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.05" />
                  <Point X="4.324739051546" Y="2.657714219641" Z="1.05" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.05" />
                  <Point X="4.099906271954" Y="2.992922033153" Z="1.05" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.05" />
                  <Point X="3.384584880707" Y="2.772011038185" Z="1.05" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.05" />
                  <Point X="2.462821003157" Y="2.254415135207" Z="1.05" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.05" />
                  <Point X="2.38890075727" Y="2.250435743483" Z="1.05" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.05" />
                  <Point X="2.323060652863" Y="2.279078553264" Z="1.05" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.05" />
                  <Point X="2.271680046971" Y="2.333964207519" Z="1.05" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.05" />
                  <Point X="2.248993959276" Y="2.400857686563" Z="1.05" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.05" />
                  <Point X="2.25811280745" Y="2.476648572725" Z="1.05" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.05" />
                  <Point X="2.623614314674" Y="3.127554201248" Z="1.05" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.05" />
                  <Point X="2.908758312501" Y="4.158619330295" Z="1.05" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.05" />
                  <Point X="2.520379651085" Y="4.404847197549" Z="1.05" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.05" />
                  <Point X="2.114441065762" Y="4.613611469329" Z="1.05" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.05" />
                  <Point X="1.693588371974" Y="4.784143924138" Z="1.05" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.05" />
                  <Point X="1.133314049041" Y="4.940859273257" Z="1.05" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.05" />
                  <Point X="0.470264185911" Y="5.04731167915" Z="1.05" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.05" />
                  <Point X="0.113263375226" Y="4.777829166123" Z="1.05" />
                  <Point X="0" Y="4.355124473572" Z="1.05" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>