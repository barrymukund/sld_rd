<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#161" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1744" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.78389453125" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.736984375" Y="-4.160708496094" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467376708984" />
                  <Point X="25.471376953125" Y="-3.365097167969" />
                  <Point X="25.37863671875" Y="-3.231476318359" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.192646484375" Y="-3.141575927734" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097036132812" />
                  <Point X="24.853328125" Y="-3.131129150391" />
                  <Point X="24.70981640625" Y="-3.175669433594" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.562689453125" Y="-3.333756835938" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524658203" />
                  <Point X="24.222109375" Y="-4.359330078125" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.7966796875" Y="-4.813451171875" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.764130859375" Y="-4.722243164062" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.757248046875" Y="-4.384291015625" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.575220703125" Y="-4.042510986328" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295898438" />
                  <Point X="23.387115234375" Y="-3.897994628906" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.222744140625" Y="-3.882168457031" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.84549609375" Y="-3.969535400391" />
                  <Point X="22.699376953125" Y="-4.067169921875" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461425781" />
                  <Point X="22.537509765625" Y="-4.265475585938" />
                  <Point X="22.5198515625" Y="-4.28848828125" />
                  <Point X="22.440880859375" Y="-4.239591796875" />
                  <Point X="22.19828515625" Y="-4.089383544922" />
                  <Point X="22.0266171875" Y="-3.957203613281" />
                  <Point X="21.89527734375" Y="-3.856077148438" />
                  <Point X="22.25347265625" Y="-3.235665527344" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647655029297" />
                  <Point X="22.593412109375" Y="-2.616129394531" />
                  <Point X="22.59442578125" Y="-2.585194824219" />
                  <Point X="22.58544140625" Y="-2.555576660156" />
                  <Point X="22.571224609375" Y="-2.526747802734" />
                  <Point X="22.55319921875" Y="-2.501591796875" />
                  <Point X="22.5358515625" Y="-2.484243164062" />
                  <Point X="22.5106953125" Y="-2.466215087891" />
                  <Point X="22.4818671875" Y="-2.451997314453" />
                  <Point X="22.452248046875" Y="-2.44301171875" />
                  <Point X="22.4213125" Y="-2.444023925781" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.63046484375" Y="-2.882866943359" />
                  <Point X="21.1819765625" Y="-3.141801513672" />
                  <Point X="21.108796875" Y="-3.045659423828" />
                  <Point X="20.917142578125" Y="-2.793862060547" />
                  <Point X="20.794064453125" Y="-2.587481689453" />
                  <Point X="20.693857421875" Y="-2.419449951172" />
                  <Point X="21.329310546875" Y="-1.931849487305" />
                  <Point X="21.894044921875" Y="-1.498513305664" />
                  <Point X="21.91541796875" Y="-1.475599365234" />
                  <Point X="21.9333828125" Y="-1.448472900391" />
                  <Point X="21.946142578125" Y="-1.419839355469" />
                  <Point X="21.953849609375" Y="-1.390085205078" />
                  <Point X="21.956654296875" Y="-1.359647827148" />
                  <Point X="21.954443359375" Y="-1.327978759766" />
                  <Point X="21.94744140625" Y="-1.298236572266" />
                  <Point X="21.931359375" Y="-1.272255615234" />
                  <Point X="21.91052734375" Y="-1.248300537109" />
                  <Point X="21.887029296875" Y="-1.228767822266" />
                  <Point X="21.860546875" Y="-1.213181152344" />
                  <Point X="21.83128515625" Y="-1.20195715332" />
                  <Point X="21.7993984375" Y="-1.195475097656" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="20.8460703125" Y="-1.315767700195" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.2408828125" Y="-1.286116333008" />
                  <Point X="20.165921875" Y="-0.992649841309" />
                  <Point X="20.133361328125" Y="-0.76498059082" />
                  <Point X="20.107578125" Y="-0.584698425293" />
                  <Point X="20.82380859375" Y="-0.392784820557" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.489216796875" Y="-0.211385437012" />
                  <Point X="21.51681640625" Y="-0.195814331055" />
                  <Point X="21.53012890625" Y="-0.186735900879" />
                  <Point X="21.55766796875" Y="-0.164307647705" />
                  <Point X="21.573892578125" Y="-0.147354537964" />
                  <Point X="21.58546875" Y="-0.126941108704" />
                  <Point X="21.596904296875" Y="-0.099280044556" />
                  <Point X="21.601568359375" Y="-0.084821853638" />
                  <Point X="21.609056640625" Y="-0.053117092133" />
                  <Point X="21.61159765625" Y="-0.031759788513" />
                  <Point X="21.609271484375" Y="-0.010377929688" />
                  <Point X="21.603013671875" Y="0.017366771698" />
                  <Point X="21.598529296875" Y="0.031789136887" />
                  <Point X="21.58586328125" Y="0.063410263062" />
                  <Point X="21.574515625" Y="0.0839479599" />
                  <Point X="21.55848046875" Y="0.101077804565" />
                  <Point X="21.53462890625" Y="0.120946861267" />
                  <Point X="21.521455078125" Y="0.130151123047" />
                  <Point X="21.49016796875" Y="0.148281585693" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="20.626677734375" Y="0.38304574585" />
                  <Point X="20.10818359375" Y="0.521975585938" />
                  <Point X="20.127203125" Y="0.650504821777" />
                  <Point X="20.17551171875" Y="0.976968994141" />
                  <Point X="20.241060546875" Y="1.218868041992" />
                  <Point X="20.29644921875" Y="1.423268066406" />
                  <Point X="20.773189453125" Y="1.36050402832" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228027344" />
                  <Point X="21.296865234375" Y="1.305263916016" />
                  <Point X="21.32349609375" Y="1.313661132812" />
                  <Point X="21.358291015625" Y="1.324631225586" />
                  <Point X="21.37722265625" Y="1.332961791992" />
                  <Point X="21.395966796875" Y="1.343783935547" />
                  <Point X="21.4126484375" Y="1.35601550293" />
                  <Point X="21.426287109375" Y="1.371567993164" />
                  <Point X="21.438701171875" Y="1.389297973633" />
                  <Point X="21.448650390625" Y="1.40743359375" />
                  <Point X="21.4593359375" Y="1.433232543945" />
                  <Point X="21.473296875" Y="1.466937744141" />
                  <Point X="21.479083984375" Y="1.486788085938" />
                  <Point X="21.48284375" Y="1.508103149414" />
                  <Point X="21.484197265625" Y="1.52874987793" />
                  <Point X="21.481048828125" Y="1.549199951172" />
                  <Point X="21.4754453125" Y="1.570106445312" />
                  <Point X="21.46794921875" Y="1.58937878418" />
                  <Point X="21.4550546875" Y="1.61414831543" />
                  <Point X="21.438208984375" Y="1.646508422852" />
                  <Point X="21.42671875" Y="1.663705810547" />
                  <Point X="21.412806640625" Y="1.680285888672" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="20.91578125" Y="2.064505615234" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.73113671875" Y="2.412064941406" />
                  <Point X="20.918853515625" Y="2.733666992188" />
                  <Point X="21.092478515625" Y="2.956840087891" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.51012109375" Y="3.008189697266" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.890298828125" Y="2.822551269531" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.95943359375" Y="2.818762939453" />
                  <Point X="21.980892578125" Y="2.821587890625" />
                  <Point X="22.000984375" Y="2.826504150391" />
                  <Point X="22.019537109375" Y="2.835652832031" />
                  <Point X="22.037791015625" Y="2.847281738281" />
                  <Point X="22.053923828125" Y="2.860228515625" />
                  <Point X="22.080251953125" Y="2.886556396484" />
                  <Point X="22.1146484375" Y="2.920951904297" />
                  <Point X="22.127595703125" Y="2.937084228516" />
                  <Point X="22.139224609375" Y="2.955338134766" />
                  <Point X="22.14837109375" Y="2.973887451172" />
                  <Point X="22.1532890625" Y="2.9939765625" />
                  <Point X="22.156115234375" Y="3.015435058594" />
                  <Point X="22.15656640625" Y="3.036122070312" />
                  <Point X="22.1533203125" Y="3.073213867188" />
                  <Point X="22.14908203125" Y="3.121671630859" />
                  <Point X="22.145044921875" Y="3.141960205078" />
                  <Point X="22.13853515625" Y="3.162603515625" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.916578125" Y="3.551543457031" />
                  <Point X="21.81666796875" Y="3.724595947266" />
                  <Point X="21.9724453125" Y="3.844030029297" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.572837890625" Y="4.246614257813" />
                  <Point X="22.83296484375" Y="4.391134277344" />
                  <Point X="22.8701015625" Y="4.342734863281" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.046171875" Y="4.167903808594" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222548828125" Y="4.134482421875" />
                  <Point X="23.265546875" Y="4.152293457031" />
                  <Point X="23.32172265625" Y="4.175562011719" />
                  <Point X="23.339857421875" Y="4.18551171875" />
                  <Point X="23.3575859375" Y="4.19792578125" />
                  <Point X="23.37313671875" Y="4.2115625" />
                  <Point X="23.3853671875" Y="4.2282421875" />
                  <Point X="23.396189453125" Y="4.246985839844" />
                  <Point X="23.404521484375" Y="4.265922851563" />
                  <Point X="23.418515625" Y="4.310310546875" />
                  <Point X="23.43680078125" Y="4.368299316406" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410145996094" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.417984375" Y="4.615303710938" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.62713671875" Y="4.691149902344" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.381880859375" Y="4.848607421875" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.782310546875" Y="4.599006835938" />
                  <Point X="24.866095703125" Y="4.286315917969" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.255677734375" Y="4.694823730469" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.474494140625" Y="4.870440429688" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.118318359375" Y="4.765520019531" />
                  <Point X="26.4810234375" Y="4.677951660156" />
                  <Point X="26.65871484375" Y="4.613501953125" />
                  <Point X="26.894650390625" Y="4.527926757812" />
                  <Point X="27.06727734375" Y="4.447194824219" />
                  <Point X="27.294576171875" Y="4.34089453125" />
                  <Point X="27.461373046875" Y="4.243718261719" />
                  <Point X="27.680978515625" Y="4.115775878906" />
                  <Point X="27.838265625" Y="4.003921386719" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.52116015625" Y="3.198151855469" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.539936767578" />
                  <Point X="27.133078125" Y="2.516057861328" />
                  <Point X="27.12376953125" Y="2.481248046875" />
                  <Point X="27.111607421875" Y="2.435771484375" />
                  <Point X="27.108619140625" Y="2.417937011719" />
                  <Point X="27.107728515625" Y="2.380950683594" />
                  <Point X="27.111359375" Y="2.350850097656" />
                  <Point X="27.116099609375" Y="2.311525878906" />
                  <Point X="27.121443359375" Y="2.289605224609" />
                  <Point X="27.1297109375" Y="2.267513671875" />
                  <Point X="27.140072265625" Y="2.247470458984" />
                  <Point X="27.158697265625" Y="2.220021728516" />
                  <Point X="27.18303125" Y="2.184161865234" />
                  <Point X="27.19446484375" Y="2.170329345703" />
                  <Point X="27.22159765625" Y="2.145593261719" />
                  <Point X="27.249044921875" Y="2.126968017578" />
                  <Point X="27.28490625" Y="2.102635742188" />
                  <Point X="27.304955078125" Y="2.092270996094" />
                  <Point X="27.32704296875" Y="2.084005615234" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.379064453125" Y="2.075033935547" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.473205078125" Y="2.074170898438" />
                  <Point X="27.508015625" Y="2.083479492188" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.43386328125" Y="2.598196044922" />
                  <Point X="28.967326171875" Y="2.906190673828" />
                  <Point X="28.9930078125" Y="2.870498779297" />
                  <Point X="29.123279296875" Y="2.689452148438" />
                  <Point X="29.21095703125" Y="2.544561279297" />
                  <Point X="29.26219921875" Y="2.459883300781" />
                  <Point X="28.722634765625" Y="2.045860229492" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.221427734375" Y="1.660242919922" />
                  <Point X="28.203974609375" Y="1.641627319336" />
                  <Point X="28.178921875" Y="1.608944213867" />
                  <Point X="28.146193359375" Y="1.566245849609" />
                  <Point X="28.136607421875" Y="1.550912353516" />
                  <Point X="28.121630859375" Y="1.517087402344" />
                  <Point X="28.112298828125" Y="1.483717773438" />
                  <Point X="28.10010546875" Y="1.440122924805" />
                  <Point X="28.09665234375" Y="1.417825561523" />
                  <Point X="28.0958359375" Y="1.39425402832" />
                  <Point X="28.097740234375" Y="1.371765136719" />
                  <Point X="28.10540234375" Y="1.334637207031" />
                  <Point X="28.11541015625" Y="1.286132324219" />
                  <Point X="28.1206796875" Y="1.268981445313" />
                  <Point X="28.13628125" Y="1.235741210938" />
                  <Point X="28.1571171875" Y="1.204070800781" />
                  <Point X="28.184337890625" Y="1.162695922852" />
                  <Point X="28.198892578125" Y="1.145450561523" />
                  <Point X="28.21613671875" Y="1.129360473633" />
                  <Point X="28.23434765625" Y="1.116034301758" />
                  <Point X="28.26454296875" Y="1.099037353516" />
                  <Point X="28.303990234375" Y="1.07683215332" />
                  <Point X="28.3205234375" Y="1.069501098633" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.3969453125" Y="1.05404284668" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.2912109375" Y="1.152702270508" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.78998828125" Y="1.162624145508" />
                  <Point X="29.84594140625" Y="0.932788085938" />
                  <Point X="29.8735703125" Y="0.75533203125" />
                  <Point X="29.890865234375" Y="0.644238891602" />
                  <Point X="29.28025" Y="0.480624511719" />
                  <Point X="28.716580078125" Y="0.329589874268" />
                  <Point X="28.7047890625" Y="0.325585662842" />
                  <Point X="28.681546875" Y="0.315067932129" />
                  <Point X="28.6414375" Y="0.291883789062" />
                  <Point X="28.589037109375" Y="0.26159552002" />
                  <Point X="28.5743125" Y="0.251096343994" />
                  <Point X="28.547533203125" Y="0.225577194214" />
                  <Point X="28.523466796875" Y="0.194911590576" />
                  <Point X="28.49202734375" Y="0.154849395752" />
                  <Point X="28.48030078125" Y="0.135568069458" />
                  <Point X="28.47052734375" Y="0.114104911804" />
                  <Point X="28.463681640625" Y="0.092603347778" />
                  <Point X="28.45566015625" Y="0.050715881348" />
                  <Point X="28.4451796875" Y="-0.004007135868" />
                  <Point X="28.443484375" Y="-0.021874988556" />
                  <Point X="28.4451796875" Y="-0.058553012848" />
                  <Point X="28.453201171875" Y="-0.100440483093" />
                  <Point X="28.463681640625" Y="-0.155163345337" />
                  <Point X="28.47052734375" Y="-0.176665054321" />
                  <Point X="28.48030078125" Y="-0.198128219604" />
                  <Point X="28.49202734375" Y="-0.217409698486" />
                  <Point X="28.51609375" Y="-0.248075149536" />
                  <Point X="28.547533203125" Y="-0.288137329102" />
                  <Point X="28.56" Y="-0.301235626221" />
                  <Point X="28.589037109375" Y="-0.324155670166" />
                  <Point X="28.629146484375" Y="-0.347339691162" />
                  <Point X="28.681546875" Y="-0.377627929688" />
                  <Point X="28.692708984375" Y="-0.383138244629" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="29.45297265625" Y="-0.589465820312" />
                  <Point X="29.89147265625" Y="-0.706961669922" />
                  <Point X="29.88626171875" Y="-0.741527770996" />
                  <Point X="29.85501953125" Y="-0.94874810791" />
                  <Point X="29.819623046875" Y="-1.103860107422" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="29.078080078125" Y="-1.089501831055" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.295939453125" Y="-1.022619140625" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.163978515625" Y="-1.056595703125" />
                  <Point X="28.136150390625" Y="-1.07348815918" />
                  <Point X="28.1123984375" Y="-1.093960083008" />
                  <Point X="28.06481640625" Y="-1.151186157227" />
                  <Point X="28.002654296875" Y="-1.225948120117" />
                  <Point X="27.9879296875" Y="-1.250335449219" />
                  <Point X="27.976587890625" Y="-1.277721069336" />
                  <Point X="27.969759765625" Y="-1.305365234375" />
                  <Point X="27.962939453125" Y="-1.379475708008" />
                  <Point X="27.95403125" Y="-1.476295532227" />
                  <Point X="27.95634765625" Y="-1.507562133789" />
                  <Point X="27.964078125" Y="-1.539182983398" />
                  <Point X="27.976451171875" Y="-1.56799621582" />
                  <Point X="28.020015625" Y="-1.635759277344" />
                  <Point X="28.076931640625" Y="-1.724286743164" />
                  <Point X="28.0869375" Y="-1.737243286133" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.7940078125" Y="-2.285283447266" />
                  <Point X="29.213123046875" Y="-2.6068828125" />
                  <Point X="29.212869140625" Y="-2.607293701172" />
                  <Point X="29.124802734375" Y="-2.749798339844" />
                  <Point X="29.05159765625" Y="-2.853811767578" />
                  <Point X="29.028982421875" Y="-2.885945068359" />
                  <Point X="28.383162109375" Y="-2.513080078125" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.660537109375" Y="-2.142904785156" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135176757812" />
                  <Point X="27.367" Y="-2.176140136719" />
                  <Point X="27.26531640625" Y="-2.229655761719" />
                  <Point X="27.242384765625" Y="-2.246550292969" />
                  <Point X="27.22142578125" Y="-2.267510253906" />
                  <Point X="27.20453515625" Y="-2.290438232422" />
                  <Point X="27.1635703125" Y="-2.368271972656" />
                  <Point X="27.1100546875" Y="-2.469956298828" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531907958984" />
                  <Point X="27.09567578125" Y="-2.563259765625" />
                  <Point X="27.11259765625" Y="-2.656950195312" />
                  <Point X="27.134703125" Y="-2.779350097656" />
                  <Point X="27.13898828125" Y="-2.795139648438" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.5909609375" Y="-3.586688964844" />
                  <Point X="27.861287109375" Y="-4.05490625" />
                  <Point X="27.781865234375" Y="-4.111634277344" />
                  <Point X="27.701765625" Y="-4.163481445312" />
                  <Point X="27.20282421875" Y="-3.513247802734" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.629521484375" Y="-2.841149658203" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.316041015625" Y="-2.750416259766" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.0265625" Y="-2.860345214844" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968861083984" />
                  <Point X="25.88725" Y="-2.996687744141" />
                  <Point X="25.875625" Y="-3.025808837891" />
                  <Point X="25.85229296875" Y="-3.133155761719" />
                  <Point X="25.821810546875" Y="-3.273396728516" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120361328" />
                  <Point X="25.94419140625" Y="-4.268400390625" />
                  <Point X="26.02206640625" Y="-4.859916015625" />
                  <Point X="25.97569140625" Y="-4.870080566406" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94157421875" Y="-4.75263671875" />
                  <Point X="23.85875390625" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575837402344" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030761719" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688964844" />
                  <Point X="23.850421875" Y="-4.365756835938" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.811875" Y="-4.178468261719" />
                  <Point X="23.80097265625" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.74979296875" Y="-4.070937744141" />
                  <Point X="23.738994140625" Y="-4.059779541016" />
                  <Point X="23.637859375" Y="-3.971086425781" />
                  <Point X="23.505734375" Y="-3.855215087891" />
                  <Point X="23.49326171875" Y="-3.845965087891" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822527587891" />
                  <Point X="23.423470703125" Y="-3.810226318359" />
                  <Point X="23.4086875" Y="-3.805476318359" />
                  <Point X="23.378544921875" Y="-3.798447998047" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.22895703125" Y="-3.787371826172" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.808270019531" />
                  <Point X="22.9045625" Y="-3.8158125" />
                  <Point X="22.792716796875" Y="-3.890546142578" />
                  <Point X="22.64659765625" Y="-3.988180664062" />
                  <Point X="22.64031640625" Y="-3.992759277344" />
                  <Point X="22.619560546875" Y="-4.010130859375" />
                  <Point X="22.597244140625" Y="-4.032769287109" />
                  <Point X="22.589529296875" Y="-4.041628662109" />
                  <Point X="22.496796875" Y="-4.162477539063" />
                  <Point X="22.490892578125" Y="-4.158821289062" />
                  <Point X="22.25240234375" Y="-4.011154541016" />
                  <Point X="22.08457421875" Y="-3.881931396484" />
                  <Point X="22.01913671875" Y="-3.831546386719" />
                  <Point X="22.335744140625" Y="-3.283165527344" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.710085693359" />
                  <Point X="22.67605078125" Y="-2.68112109375" />
                  <Point X="22.680314453125" Y="-2.666190429688" />
                  <Point X="22.6865859375" Y="-2.634664794922" />
                  <Point X="22.688361328125" Y="-2.619240722656" />
                  <Point X="22.689375" Y="-2.588306152344" />
                  <Point X="22.6853359375" Y="-2.557618408203" />
                  <Point X="22.6763515625" Y="-2.528000244141" />
                  <Point X="22.67064453125" Y="-2.513559326172" />
                  <Point X="22.656427734375" Y="-2.48473046875" />
                  <Point X="22.648447265625" Y="-2.471414794922" />
                  <Point X="22.630421875" Y="-2.446258789062" />
                  <Point X="22.620376953125" Y="-2.434418457031" />
                  <Point X="22.603029296875" Y="-2.417069824219" />
                  <Point X="22.591189453125" Y="-2.407024658203" />
                  <Point X="22.566033203125" Y="-2.388996582031" />
                  <Point X="22.552716796875" Y="-2.381013671875" />
                  <Point X="22.523888671875" Y="-2.366795898438" />
                  <Point X="22.5094453125" Y="-2.361088623047" />
                  <Point X="22.479826171875" Y="-2.352103027344" />
                  <Point X="22.449140625" Y="-2.3480625" />
                  <Point X="22.418205078125" Y="-2.349074707031" />
                  <Point X="22.402779296875" Y="-2.350849121094" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.58296484375" Y="-2.800594482422" />
                  <Point X="21.206912109375" Y="-3.017708740234" />
                  <Point X="21.184390625" Y="-2.988120849609" />
                  <Point X="20.99598046875" Y="-2.7405859375" />
                  <Point X="20.87565625" Y="-2.538822753906" />
                  <Point X="20.818734375" Y="-2.443373291016" />
                  <Point X="21.387142578125" Y="-2.007217895508" />
                  <Point X="21.951876953125" Y="-1.573881835938" />
                  <Point X="21.963515625" Y="-1.563312011719" />
                  <Point X="21.984888671875" Y="-1.540398071289" />
                  <Point X="21.994623046875" Y="-1.528054199219" />
                  <Point X="22.012587890625" Y="-1.500927734375" />
                  <Point X="22.02015625" Y="-1.487141479492" />
                  <Point X="22.032916015625" Y="-1.45850793457" />
                  <Point X="22.038107421875" Y="-1.443660522461" />
                  <Point X="22.045814453125" Y="-1.41390637207" />
                  <Point X="22.04844921875" Y="-1.398802124023" />
                  <Point X="22.05125390625" Y="-1.368364746094" />
                  <Point X="22.051423828125" Y="-1.353031616211" />
                  <Point X="22.049212890625" Y="-1.321362426758" />
                  <Point X="22.046916015625" Y="-1.306208862305" />
                  <Point X="22.0399140625" Y="-1.276466674805" />
                  <Point X="22.02821875" Y="-1.248235961914" />
                  <Point X="22.01213671875" Y="-1.222255126953" />
                  <Point X="22.003044921875" Y="-1.209916015625" />
                  <Point X="21.982212890625" Y="-1.1859609375" />
                  <Point X="21.971255859375" Y="-1.175244628906" />
                  <Point X="21.9477578125" Y="-1.155711914062" />
                  <Point X="21.935216796875" Y="-1.146895874023" />
                  <Point X="21.908734375" Y="-1.131309204102" />
                  <Point X="21.894568359375" Y="-1.124482421875" />
                  <Point X="21.865306640625" Y="-1.113258422852" />
                  <Point X="21.8502109375" Y="-1.108861206055" />
                  <Point X="21.81832421875" Y="-1.102379150391" />
                  <Point X="21.802705078125" Y="-1.100532714844" />
                  <Point X="21.77137890625" Y="-1.09944128418" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="20.833669921875" Y="-1.221580566406" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.332927734375" Y="-1.262604492188" />
                  <Point X="20.259236328125" Y="-0.974110961914" />
                  <Point X="20.227404296875" Y="-0.751530822754" />
                  <Point X="20.213548828125" Y="-0.654654724121" />
                  <Point X="20.848396484375" Y="-0.484547790527" />
                  <Point X="21.491712890625" Y="-0.312171264648" />
                  <Point X="21.503044921875" Y="-0.308355682373" />
                  <Point X="21.52513671875" Y="-0.299332794189" />
                  <Point X="21.535896484375" Y="-0.294125640869" />
                  <Point X="21.56349609375" Y="-0.27855456543" />
                  <Point X="21.57033984375" Y="-0.274301116943" />
                  <Point X="21.59012109375" Y="-0.260397613525" />
                  <Point X="21.61766015625" Y="-0.23796937561" />
                  <Point X="21.62630078125" Y="-0.229991897583" />
                  <Point X="21.642525390625" Y="-0.213038772583" />
                  <Point X="21.656529296875" Y="-0.194217025757" />
                  <Point X="21.66810546875" Y="-0.173803527832" />
                  <Point X="21.67326171875" Y="-0.163236297607" />
                  <Point X="21.684697265625" Y="-0.135575195312" />
                  <Point X="21.68731640625" Y="-0.128446060181" />
                  <Point X="21.694025390625" Y="-0.106658836365" />
                  <Point X="21.701513671875" Y="-0.074954162598" />
                  <Point X="21.703390625" Y="-0.064340705872" />
                  <Point X="21.705931640625" Y="-0.042983448029" />
                  <Point X="21.706041015625" Y="-0.021485149384" />
                  <Point X="21.70371484375" Y="-0.000103371479" />
                  <Point X="21.701943359375" Y="0.010524204254" />
                  <Point X="21.695685546875" Y="0.038268974304" />
                  <Point X="21.69373046875" Y="0.04557334137" />
                  <Point X="21.686716796875" Y="0.067113548279" />
                  <Point X="21.67405078125" Y="0.098734550476" />
                  <Point X="21.669015625" Y="0.109353805542" />
                  <Point X="21.65766796875" Y="0.129891555786" />
                  <Point X="21.64387109375" Y="0.148870391846" />
                  <Point X="21.6278359375" Y="0.16600038147" />
                  <Point X="21.61928515625" Y="0.174069702148" />
                  <Point X="21.59543359375" Y="0.193938796997" />
                  <Point X="21.5890390625" Y="0.198822250366" />
                  <Point X="21.5690859375" Y="0.212347381592" />
                  <Point X="21.537798828125" Y="0.230477874756" />
                  <Point X="21.52659375" Y="0.236020477295" />
                  <Point X="21.50355078125" Y="0.245587173462" />
                  <Point X="21.491712890625" Y="0.249611297607" />
                  <Point X="20.651265625" Y="0.474808685303" />
                  <Point X="20.2145546875" Y="0.591824951172" />
                  <Point X="20.2211796875" Y="0.636598327637" />
                  <Point X="20.26866796875" Y="0.957522949219" />
                  <Point X="20.33275390625" Y="1.194021362305" />
                  <Point X="20.3664140625" Y="1.318237182617" />
                  <Point X="20.7607890625" Y="1.266316772461" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.204703125" />
                  <Point X="21.28485546875" Y="1.206589477539" />
                  <Point X="21.295111328125" Y="1.208053588867" />
                  <Point X="21.315400390625" Y="1.212089477539" />
                  <Point X="21.32543359375" Y="1.214661376953" />
                  <Point X="21.352064453125" Y="1.22305859375" />
                  <Point X="21.386859375" Y="1.234028686523" />
                  <Point X="21.396552734375" Y="1.237677368164" />
                  <Point X="21.415484375" Y="1.246008056641" />
                  <Point X="21.42472265625" Y="1.250689819336" />
                  <Point X="21.443466796875" Y="1.26151184082" />
                  <Point X="21.452140625" Y="1.267171875" />
                  <Point X="21.468822265625" Y="1.279403442383" />
                  <Point X="21.48407421875" Y="1.29337890625" />
                  <Point X="21.497712890625" Y="1.308931396484" />
                  <Point X="21.504107421875" Y="1.317079956055" />
                  <Point X="21.516521484375" Y="1.334809936523" />
                  <Point X="21.521990234375" Y="1.343605224609" />
                  <Point X="21.531939453125" Y="1.361740844727" />
                  <Point X="21.536419921875" Y="1.371080810547" />
                  <Point X="21.54710546875" Y="1.396879638672" />
                  <Point X="21.56106640625" Y="1.430584960938" />
                  <Point X="21.5645" Y="1.440348632813" />
                  <Point X="21.570287109375" Y="1.460198974609" />
                  <Point X="21.572640625" Y="1.470285766602" />
                  <Point X="21.576400390625" Y="1.491600708008" />
                  <Point X="21.577640625" Y="1.501888793945" />
                  <Point X="21.578994140625" Y="1.522535400391" />
                  <Point X="21.578091796875" Y="1.543205444336" />
                  <Point X="21.574943359375" Y="1.563655517578" />
                  <Point X="21.572810546875" Y="1.573794433594" />
                  <Point X="21.56720703125" Y="1.594700927734" />
                  <Point X="21.563984375" Y="1.604544067383" />
                  <Point X="21.55648828125" Y="1.62381640625" />
                  <Point X="21.55221484375" Y="1.633245849609" />
                  <Point X="21.5393203125" Y="1.658015258789" />
                  <Point X="21.522474609375" Y="1.690375366211" />
                  <Point X="21.51719921875" Y="1.699285522461" />
                  <Point X="21.505708984375" Y="1.716482788086" />
                  <Point X="21.499494140625" Y="1.724770019531" />
                  <Point X="21.48558203125" Y="1.741350097656" />
                  <Point X="21.478498046875" Y="1.748912597656" />
                  <Point X="21.4635546875" Y="1.763216796875" />
                  <Point X="21.4556953125" Y="1.769958618164" />
                  <Point X="20.97361328125" Y="2.139874023438" />
                  <Point X="20.77238671875" Y="2.294280517578" />
                  <Point X="20.81318359375" Y="2.364175292969" />
                  <Point X="20.99771875" Y="2.680326416016" />
                  <Point X="21.167458984375" Y="2.898506103516" />
                  <Point X="21.273662109375" Y="3.035013427734" />
                  <Point X="21.46262109375" Y="2.925917236328" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.88201953125" Y="2.727912841797" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.961505859375" Y="2.723785644531" />
                  <Point X="21.97183203125" Y="2.724575683594" />
                  <Point X="21.993291015625" Y="2.727400634766" />
                  <Point X="22.00347265625" Y="2.729310302734" />
                  <Point X="22.023564453125" Y="2.7342265625" />
                  <Point X="22.043" Y="2.741300292969" />
                  <Point X="22.061552734375" Y="2.750448974609" />
                  <Point X="22.070580078125" Y="2.755530517578" />
                  <Point X="22.088833984375" Y="2.767159423828" />
                  <Point X="22.09725" Y="2.773190185547" />
                  <Point X="22.1133828125" Y="2.786136962891" />
                  <Point X="22.121099609375" Y="2.793052978516" />
                  <Point X="22.147427734375" Y="2.819380859375" />
                  <Point X="22.18182421875" Y="2.853776367188" />
                  <Point X="22.18873828125" Y="2.861489990234" />
                  <Point X="22.201685546875" Y="2.877622314453" />
                  <Point X="22.20771875" Y="2.886041015625" />
                  <Point X="22.21934765625" Y="2.904294921875" />
                  <Point X="22.2244296875" Y="2.913324462891" />
                  <Point X="22.233576171875" Y="2.931873779297" />
                  <Point X="22.240646484375" Y="2.951297851562" />
                  <Point X="22.245564453125" Y="2.971386962891" />
                  <Point X="22.2474765625" Y="2.981571777344" />
                  <Point X="22.250302734375" Y="3.003030273438" />
                  <Point X="22.251091796875" Y="3.013363769531" />
                  <Point X="22.25154296875" Y="3.03405078125" />
                  <Point X="22.251205078125" Y="3.044404296875" />
                  <Point X="22.247958984375" Y="3.08149609375" />
                  <Point X="22.243720703125" Y="3.129953857422" />
                  <Point X="22.242255859375" Y="3.140211669922" />
                  <Point X="22.23821875" Y="3.160500244141" />
                  <Point X="22.235646484375" Y="3.170531005859" />
                  <Point X="22.22913671875" Y="3.191174316406" />
                  <Point X="22.22548828125" Y="3.200867919922" />
                  <Point X="22.217158203125" Y="3.219797363281" />
                  <Point X="22.2124765625" Y="3.229033203125" />
                  <Point X="21.998849609375" Y="3.599043701172" />
                  <Point X="21.94061328125" Y="3.699915039062" />
                  <Point X="22.030248046875" Y="3.768638427734" />
                  <Point X="22.3516328125" Y="4.015041015625" />
                  <Point X="22.618974609375" Y="4.1635703125" />
                  <Point X="22.807474609375" Y="4.268296875" />
                  <Point X="22.881435546875" Y="4.17191015625" />
                  <Point X="22.8881796875" Y="4.164048828125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93491015625" Y="4.121895996094" />
                  <Point X="22.952111328125" Y="4.110403320312" />
                  <Point X="22.9610234375" Y="4.105128417969" />
                  <Point X="23.002306640625" Y="4.083637939453" />
                  <Point X="23.056240234375" Y="4.055561767578" />
                  <Point X="23.06567578125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.043789550781" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.229271484375" Y="4.037488525391" />
                  <Point X="23.2491328125" Y="4.043277587891" />
                  <Point X="23.258904296875" Y="4.046714355469" />
                  <Point X="23.30190234375" Y="4.064525390625" />
                  <Point X="23.358078125" Y="4.087793945312" />
                  <Point X="23.36741796875" Y="4.092274169922" />
                  <Point X="23.385552734375" Y="4.102224121094" />
                  <Point X="23.39434765625" Y="4.107693359375" />
                  <Point X="23.412076171875" Y="4.120107421875" />
                  <Point X="23.420220703125" Y="4.126499023438" />
                  <Point X="23.435771484375" Y="4.140135742188" />
                  <Point X="23.449748046875" Y="4.15538671875" />
                  <Point X="23.461978515625" Y="4.17206640625" />
                  <Point X="23.467638671875" Y="4.180740234375" />
                  <Point X="23.4784609375" Y="4.199483886719" />
                  <Point X="23.48314453125" Y="4.2087265625" />
                  <Point X="23.4914765625" Y="4.227663574219" />
                  <Point X="23.495125" Y="4.237357910156" />
                  <Point X="23.509119140625" Y="4.281745605469" />
                  <Point X="23.527404296875" Y="4.339734375" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370048339844" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401865234375" />
                  <Point X="23.53769921875" Y="4.412218261719" />
                  <Point X="23.537248046875" Y="4.432898925781" />
                  <Point X="23.536458984375" Y="4.4432265625" />
                  <Point X="23.520736328125" Y="4.562655761719" />
                  <Point X="23.652783203125" Y="4.599677246094" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.392923828125" Y="4.754251464844" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.690546875" Y="4.574419433594" />
                  <Point X="24.77433203125" Y="4.261728515625" />
                  <Point X="24.779564453125" Y="4.247106933594" />
                  <Point X="24.79233984375" Y="4.218912597656" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.34744140625" Y="4.670235839844" />
                  <Point X="25.378193359375" Y="4.785005859375" />
                  <Point X="25.464599609375" Y="4.77595703125" />
                  <Point X="25.827876953125" Y="4.737912109375" />
                  <Point X="26.0960234375" Y="4.673173339844" />
                  <Point X="26.45359375" Y="4.586844726562" />
                  <Point X="26.626322265625" Y="4.524194824219" />
                  <Point X="26.858265625" Y="4.440067382812" />
                  <Point X="27.027033203125" Y="4.361140625" />
                  <Point X="27.250453125" Y="4.256654296875" />
                  <Point X="27.41355078125" Y="4.161633300781" />
                  <Point X="27.6294296875" Y="4.035861572266" />
                  <Point X="27.783208984375" Y="3.926501953125" />
                  <Point X="27.81778125" Y="3.901915283203" />
                  <Point X="27.438888671875" Y="3.245651855469" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.062380859375" Y="2.593114257812" />
                  <Point X="27.0531875" Y="2.573447998047" />
                  <Point X="27.044185546875" Y="2.549569091797" />
                  <Point X="27.041302734375" Y="2.540599853516" />
                  <Point X="27.031994140625" Y="2.505790039062" />
                  <Point X="27.01983203125" Y="2.460313476562" />
                  <Point X="27.0179140625" Y="2.451470458984" />
                  <Point X="27.013646484375" Y="2.420223876953" />
                  <Point X="27.012755859375" Y="2.383237548828" />
                  <Point X="27.013412109375" Y="2.369573730469" />
                  <Point X="27.01704296875" Y="2.339473144531" />
                  <Point X="27.021783203125" Y="2.300148925781" />
                  <Point X="27.023802734375" Y="2.289025878906" />
                  <Point X="27.029146484375" Y="2.267105224609" />
                  <Point X="27.032470703125" Y="2.256307617188" />
                  <Point X="27.04073828125" Y="2.234216064453" />
                  <Point X="27.0453203125" Y="2.223887939453" />
                  <Point X="27.055681640625" Y="2.203844726562" />
                  <Point X="27.0614609375" Y="2.194129638672" />
                  <Point X="27.0800859375" Y="2.166680908203" />
                  <Point X="27.104419921875" Y="2.130821044922" />
                  <Point X="27.109806640625" Y="2.123636962891" />
                  <Point X="27.130462890625" Y="2.100125" />
                  <Point X="27.157595703125" Y="2.075388916016" />
                  <Point X="27.16825390625" Y="2.066983398438" />
                  <Point X="27.195701171875" Y="2.048358154297" />
                  <Point X="27.2315625" Y="2.024026000977" />
                  <Point X="27.241279296875" Y="2.01824597168" />
                  <Point X="27.261328125" Y="2.007881103516" />
                  <Point X="27.27166015625" Y="2.003296508789" />
                  <Point X="27.293748046875" Y="1.99503112793" />
                  <Point X="27.30455078125" Y="1.991706420898" />
                  <Point X="27.32647265625" Y="1.986364501953" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.36769140625" Y="1.980717163086" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.416044921875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975497314453" />
                  <Point X="27.4843125" Y="1.979822387695" />
                  <Point X="27.49774609375" Y="1.982395507812" />
                  <Point X="27.532556640625" Y="1.991704101562" />
                  <Point X="27.578033203125" Y="2.003865112305" />
                  <Point X="27.583994140625" Y="2.005670654297" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872680664" />
                  <Point X="28.48136328125" Y="2.515923583984" />
                  <Point X="28.940404296875" Y="2.780950683594" />
                  <Point X="29.0439609375" Y="2.637031494141" />
                  <Point X="29.1296796875" Y="2.495377929688" />
                  <Point X="29.136884765625" Y="2.483470947266" />
                  <Point X="28.664802734375" Y="2.121228759766" />
                  <Point X="28.172953125" Y="1.743820068359" />
                  <Point X="28.16813671875" Y="1.739867675781" />
                  <Point X="28.152123046875" Y="1.725219482422" />
                  <Point X="28.134669921875" Y="1.706603759766" />
                  <Point X="28.128578125" Y="1.699422119141" />
                  <Point X="28.103525390625" Y="1.666738891602" />
                  <Point X="28.070796875" Y="1.624040527344" />
                  <Point X="28.065638671875" Y="1.616605224609" />
                  <Point X="28.0497421875" Y="1.589373779297" />
                  <Point X="28.034765625" Y="1.555548828125" />
                  <Point X="28.030140625" Y="1.542673095703" />
                  <Point X="28.02080859375" Y="1.509303588867" />
                  <Point X="28.008615234375" Y="1.465708618164" />
                  <Point X="28.006224609375" Y="1.454661987305" />
                  <Point X="28.002771484375" Y="1.432364624023" />
                  <Point X="28.001708984375" Y="1.421113891602" />
                  <Point X="28.000892578125" Y="1.397542358398" />
                  <Point X="28.001173828125" Y="1.38623840332" />
                  <Point X="28.003078125" Y="1.363749633789" />
                  <Point X="28.004701171875" Y="1.352564453125" />
                  <Point X="28.01236328125" Y="1.315436645508" />
                  <Point X="28.02237109375" Y="1.266931762695" />
                  <Point X="28.024599609375" Y="1.258231201172" />
                  <Point X="28.034681640625" Y="1.228617553711" />
                  <Point X="28.050283203125" Y="1.195377197266" />
                  <Point X="28.056916015625" Y="1.18352734375" />
                  <Point X="28.077751953125" Y="1.151856933594" />
                  <Point X="28.10497265625" Y="1.110482055664" />
                  <Point X="28.11173828125" Y="1.101423461914" />
                  <Point X="28.12629296875" Y="1.084178222656" />
                  <Point X="28.13408203125" Y="1.075991333008" />
                  <Point X="28.151326171875" Y="1.059901245117" />
                  <Point X="28.16003515625" Y="1.052694824219" />
                  <Point X="28.17824609375" Y="1.039368652344" />
                  <Point X="28.187748046875" Y="1.033248779297" />
                  <Point X="28.217943359375" Y="1.01625177002" />
                  <Point X="28.257390625" Y="0.994046630859" />
                  <Point X="28.265482421875" Y="0.989986816406" />
                  <Point X="28.294681640625" Y="0.978083618164" />
                  <Point X="28.33027734375" Y="0.968021118164" />
                  <Point X="28.343671875" Y="0.965257751465" />
                  <Point X="28.384498046875" Y="0.959861877441" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199768066" />
                  <Point X="28.46571875" Y="0.951222839355" />
                  <Point X="28.49121875" Y="0.952032409668" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="29.303611328125" Y="1.058515014648" />
                  <Point X="29.704703125" Y="1.111319702148" />
                  <Point X="29.752689453125" Y="0.914210083008" />
                  <Point X="29.779701171875" Y="0.740717163086" />
                  <Point X="29.783873046875" Y="0.713921508789" />
                  <Point X="29.255662109375" Y="0.572387451172" />
                  <Point X="28.6919921875" Y="0.421352752686" />
                  <Point X="28.68603125" Y="0.419544311523" />
                  <Point X="28.665623046875" Y="0.412136199951" />
                  <Point X="28.642380859375" Y="0.401618469238" />
                  <Point X="28.634005859375" Y="0.39731640625" />
                  <Point X="28.593896484375" Y="0.374132293701" />
                  <Point X="28.54149609375" Y="0.343843994141" />
                  <Point X="28.5338828125" Y="0.338945831299" />
                  <Point X="28.508775390625" Y="0.31987008667" />
                  <Point X="28.48199609375" Y="0.294350952148" />
                  <Point X="28.47280078125" Y="0.284228088379" />
                  <Point X="28.448734375" Y="0.253562423706" />
                  <Point X="28.417294921875" Y="0.213500244141" />
                  <Point X="28.410859375" Y="0.204213989258" />
                  <Point X="28.3991328125" Y="0.184932693481" />
                  <Point X="28.393841796875" Y="0.174937667847" />
                  <Point X="28.384068359375" Y="0.153474441528" />
                  <Point X="28.38000390625" Y="0.142925628662" />
                  <Point X="28.373158203125" Y="0.121424064636" />
                  <Point X="28.370376953125" Y="0.110471305847" />
                  <Point X="28.36235546875" Y="0.068583732605" />
                  <Point X="28.351875" Y="0.013860787392" />
                  <Point X="28.350603515625" Y="0.004966154575" />
                  <Point X="28.3485859375" Y="-0.026261293411" />
                  <Point X="28.35028125" Y="-0.062939338684" />
                  <Point X="28.351875" Y="-0.076420921326" />
                  <Point X="28.359896484375" Y="-0.11830834198" />
                  <Point X="28.370376953125" Y="-0.173031280518" />
                  <Point X="28.373158203125" Y="-0.183983901978" />
                  <Point X="28.38000390625" Y="-0.205485610962" />
                  <Point X="28.384068359375" Y="-0.216034576416" />
                  <Point X="28.393841796875" Y="-0.237497787476" />
                  <Point X="28.3991328125" Y="-0.247492538452" />
                  <Point X="28.410859375" Y="-0.266773986816" />
                  <Point X="28.417294921875" Y="-0.276060668945" />
                  <Point X="28.441361328125" Y="-0.306726165771" />
                  <Point X="28.47280078125" Y="-0.346788360596" />
                  <Point X="28.47871875" Y="-0.353633178711" />
                  <Point X="28.501140625" Y="-0.375804443359" />
                  <Point X="28.530177734375" Y="-0.398724487305" />
                  <Point X="28.54149609375" Y="-0.406404266357" />
                  <Point X="28.58160546875" Y="-0.429588256836" />
                  <Point X="28.634005859375" Y="-0.459876556396" />
                  <Point X="28.639494140625" Y="-0.462813323975" />
                  <Point X="28.65915625" Y="-0.472015930176" />
                  <Point X="28.68302734375" Y="-0.481027526855" />
                  <Point X="28.6919921875" Y="-0.483912872314" />
                  <Point X="29.428384765625" Y="-0.681228759766" />
                  <Point X="29.784876953125" Y="-0.776750610352" />
                  <Point X="29.761607421875" Y="-0.931085144043" />
                  <Point X="29.7278046875" Y="-1.079219726563" />
                  <Point X="29.09048046875" Y="-0.99531451416" />
                  <Point X="28.436783203125" Y="-0.909253540039" />
                  <Point X="28.428625" Y="-0.908535705566" />
                  <Point X="28.40009765625" Y="-0.908042419434" />
                  <Point X="28.36672265625" Y="-0.910841003418" />
                  <Point X="28.354482421875" Y="-0.912676330566" />
                  <Point X="28.27576171875" Y="-0.929786682129" />
                  <Point X="28.17291796875" Y="-0.952140014648" />
                  <Point X="28.157875" Y="-0.956742431641" />
                  <Point X="28.1287578125" Y="-0.968365600586" />
                  <Point X="28.114681640625" Y="-0.97538671875" />
                  <Point X="28.086853515625" Y="-0.992279174805" />
                  <Point X="28.074126953125" Y="-1.001528442383" />
                  <Point X="28.050375" Y="-1.022000366211" />
                  <Point X="28.0393515625" Y="-1.03322277832" />
                  <Point X="27.99176953125" Y="-1.090448974609" />
                  <Point X="27.929607421875" Y="-1.16521081543" />
                  <Point X="27.921328125" Y="-1.176844970703" />
                  <Point X="27.906603515625" Y="-1.201232421875" />
                  <Point X="27.90016015625" Y="-1.213985229492" />
                  <Point X="27.888818359375" Y="-1.241370849609" />
                  <Point X="27.884359375" Y="-1.254940673828" />
                  <Point X="27.87753125" Y="-1.282584838867" />
                  <Point X="27.87516015625" Y="-1.296659301758" />
                  <Point X="27.86833984375" Y="-1.370769775391" />
                  <Point X="27.859431640625" Y="-1.467589599609" />
                  <Point X="27.859291015625" Y="-1.483314453125" />
                  <Point X="27.861607421875" Y="-1.514580932617" />
                  <Point X="27.864064453125" Y="-1.530122680664" />
                  <Point X="27.871794921875" Y="-1.561743530273" />
                  <Point X="27.876787109375" Y="-1.57666809082" />
                  <Point X="27.88916015625" Y="-1.605481323242" />
                  <Point X="27.896541015625" Y="-1.619370239258" />
                  <Point X="27.94010546875" Y="-1.687133300781" />
                  <Point X="27.997021484375" Y="-1.775660644531" />
                  <Point X="28.0017421875" Y="-1.782352416992" />
                  <Point X="28.01980078125" Y="-1.804457397461" />
                  <Point X="28.043494140625" Y="-1.828123291016" />
                  <Point X="28.052798828125" Y="-1.836277709961" />
                  <Point X="28.73617578125" Y="-2.360652099609" />
                  <Point X="29.087171875" Y="-2.629981689453" />
                  <Point X="29.045482421875" Y="-2.697441650391" />
                  <Point X="29.001275390625" Y="-2.760252197266" />
                  <Point X="28.430662109375" Y="-2.430807617188" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.677421875" Y="-2.049417236328" />
                  <Point X="27.555021484375" Y="-2.027312011719" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.035136352539" />
                  <Point X="27.415068359375" Y="-2.044960083008" />
                  <Point X="27.40058984375" Y="-2.051108642578" />
                  <Point X="27.322755859375" Y="-2.092072021484" />
                  <Point X="27.221072265625" Y="-2.145587646484" />
                  <Point X="27.20896875" Y="-2.153171630859" />
                  <Point X="27.186037109375" Y="-2.170066162109" />
                  <Point X="27.175208984375" Y="-2.179376708984" />
                  <Point X="27.15425" Y="-2.200336669922" />
                  <Point X="27.144939453125" Y="-2.211164306641" />
                  <Point X="27.128048828125" Y="-2.234092285156" />
                  <Point X="27.12046875" Y="-2.246192626953" />
                  <Point X="27.07950390625" Y="-2.324026367188" />
                  <Point X="27.02598828125" Y="-2.425710693359" />
                  <Point X="27.01983984375" Y="-2.440187988281" />
                  <Point X="27.010013671875" Y="-2.469967041016" />
                  <Point X="27.0063359375" Y="-2.485268798828" />
                  <Point X="27.00137890625" Y="-2.51744140625" />
                  <Point X="27.000279296875" Y="-2.5331328125" />
                  <Point X="27.00068359375" Y="-2.564484619141" />
                  <Point X="27.0021875" Y="-2.580145019531" />
                  <Point X="27.019109375" Y="-2.673835449219" />
                  <Point X="27.04121484375" Y="-2.796235351563" />
                  <Point X="27.04301953125" Y="-2.804232177734" />
                  <Point X="27.05123828125" Y="-2.831539794922" />
                  <Point X="27.064072265625" Y="-2.862478759766" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.508689453125" Y="-3.634188964844" />
                  <Point X="27.735896484375" Y="-4.02772265625" />
                  <Point X="27.723755859375" Y="-4.036083496094" />
                  <Point X="27.278193359375" Y="-3.455415527344" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.680896484375" Y="-2.761239501953" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.3073359375" Y="-2.655815917969" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.965826171875" Y="-2.787297363281" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883087646484" />
                  <Point X="25.83218359375" Y="-2.906837158203" />
                  <Point X="25.822935546875" Y="-2.919562744141" />
                  <Point X="25.80604296875" Y="-2.947389404297" />
                  <Point X="25.79901953125" Y="-2.961466796875" />
                  <Point X="25.78739453125" Y="-2.990587890625" />
                  <Point X="25.78279296875" Y="-3.005631591797" />
                  <Point X="25.7594609375" Y="-3.112978515625" />
                  <Point X="25.728978515625" Y="-3.253219482422" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323170166016" />
                  <Point X="25.7255546875" Y="-3.335520507812" />
                  <Point X="25.83308984375" Y="-4.152323242188" />
                  <Point X="25.828748046875" Y="-4.136120605469" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480122314453" />
                  <Point X="25.642146484375" Y="-3.453578857422" />
                  <Point X="25.6267890625" Y="-3.423815429688" />
                  <Point X="25.62041015625" Y="-3.413209228516" />
                  <Point X="25.549421875" Y="-3.3109296875" />
                  <Point X="25.456681640625" Y="-3.177308837891" />
                  <Point X="25.446671875" Y="-3.165171386719" />
                  <Point X="25.424787109375" Y="-3.142715820312" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104937255859" />
                  <Point X="25.3452421875" Y="-3.090829589844" />
                  <Point X="25.33065625" Y="-3.084938476562" />
                  <Point X="25.220806640625" Y="-3.050845214844" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.063376953125" Y="-3.003109130859" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.998840087891" />
                  <Point X="24.948935546875" Y="-3.003109619141" />
                  <Point X="24.935015625" Y="-3.006305664062" />
                  <Point X="24.82516796875" Y="-3.040398681641" />
                  <Point X="24.68165625" Y="-3.084938964844" />
                  <Point X="24.6670703125" Y="-3.090829345703" />
                  <Point X="24.6390703125" Y="-3.104936767578" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.587525390625" Y="-3.142717285156" />
                  <Point X="24.565640625" Y="-3.165173828125" />
                  <Point X="24.555630859375" Y="-3.177310791016" />
                  <Point X="24.48464453125" Y="-3.279590332031" />
                  <Point X="24.391904296875" Y="-3.413211181641" />
                  <Point X="24.38753125" Y="-3.420132080078" />
                  <Point X="24.374025390625" Y="-3.445261230469" />
                  <Point X="24.36122265625" Y="-3.476213867188" />
                  <Point X="24.35724609375" Y="-3.487937011719" />
                  <Point X="24.130345703125" Y="-4.3347421875" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.878938774766" Y="-2.397176783564" />
                  <Point X="21.272104953839" Y="-2.980069614796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.954448156318" Y="-2.339236338661" />
                  <Point X="21.354578256294" Y="-2.932453607213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.32688504987" Y="-1.238948065016" />
                  <Point X="20.357458473764" Y="-1.284275029955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.029957537869" Y="-2.281295893759" />
                  <Point X="21.437051558748" Y="-2.884837599629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.045184028731" Y="-3.78643106331" />
                  <Point X="22.136640746171" Y="-3.922021222896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.258266422518" Y="-0.967329059643" />
                  <Point X="20.462703351305" Y="-1.270419270786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.105466919421" Y="-2.223355448857" />
                  <Point X="21.519524861203" Y="-2.837221592046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.098032583587" Y="-3.694894561236" />
                  <Point X="22.353628462633" Y="-4.073831035221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.227432528039" Y="-0.751728224435" />
                  <Point X="20.567948228846" Y="-1.256563511616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.180976300973" Y="-2.165415003954" />
                  <Point X="21.601998167903" Y="-2.789605590755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.150881138444" Y="-3.603358059162" />
                  <Point X="22.513409324432" Y="-4.140828197692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.266903352251" Y="-0.640358421058" />
                  <Point X="20.673193106387" Y="-1.242707752447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.256485682524" Y="-2.107474559052" />
                  <Point X="21.68447148875" Y="-2.74198961044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.203729693301" Y="-3.511821557087" />
                  <Point X="22.574393636685" Y="-4.061353451983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.36395374817" Y="-0.614353843281" />
                  <Point X="20.778437983927" Y="-1.228851993278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.331995064076" Y="-2.04953411415" />
                  <Point X="21.766944809597" Y="-2.694373630124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.256578248158" Y="-3.420285055013" />
                  <Point X="22.641925728422" Y="-3.991586188568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.46100414409" Y="-0.588349265503" />
                  <Point X="20.883682858054" Y="-1.214996229046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.407504450525" Y="-1.991593676509" />
                  <Point X="21.849418130444" Y="-2.646757649809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.309426803015" Y="-3.328748552939" />
                  <Point X="22.720783922654" Y="-3.938610562636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.55805454001" Y="-0.562344687725" />
                  <Point X="20.98892772841" Y="-1.201140459225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.483013850239" Y="-1.933653258533" />
                  <Point X="21.931891451292" Y="-2.599141669493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.362275391002" Y="-3.237212099982" />
                  <Point X="22.799774075552" Y="-3.885830573473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.65510493593" Y="-0.536340109947" />
                  <Point X="21.094172598765" Y="-1.187284689403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.558523249953" Y="-1.875712840558" />
                  <Point X="22.014364772139" Y="-2.551525689178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.415124011851" Y="-3.145675695745" />
                  <Point X="22.878764199954" Y="-3.833050542061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.75215533185" Y="-0.51033553217" />
                  <Point X="21.199417469121" Y="-1.173428919581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.634032649667" Y="-1.817772422582" />
                  <Point X="22.096838092986" Y="-2.503909708862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.4679726327" Y="-3.054139291508" />
                  <Point X="22.963952312844" Y="-3.789459406465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.849205727484" Y="-0.484330953969" />
                  <Point X="21.304662339477" Y="-1.159573149759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.709542049381" Y="-1.759832004607" />
                  <Point X="22.179311413833" Y="-2.456293728547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.520821253549" Y="-2.962602887271" />
                  <Point X="23.070112556063" Y="-3.776960732722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.218758239935" Y="0.620233590516" />
                  <Point X="20.243074629227" Y="0.584183060857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.946256089161" Y="-0.458326325424" />
                  <Point X="21.409907209832" Y="-1.145717379938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.785051449095" Y="-1.701891586631" />
                  <Point X="22.261784734681" Y="-2.408677748231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.573669874398" Y="-2.871066483035" />
                  <Point X="23.190003533307" Y="-3.784818709315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.239374259428" Y="0.759556791436" />
                  <Point X="20.382944573994" Y="0.546705046824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.043306450839" Y="-0.432321696879" />
                  <Point X="21.515152080188" Y="-1.131861610116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.860560848809" Y="-1.643951168656" />
                  <Point X="22.346873894414" Y="-2.364939908549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.626518495247" Y="-2.779530078798" />
                  <Point X="23.30989457617" Y="-3.79267678319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.87251677915" Y="-4.626798501346" />
                  <Point X="23.959779256622" Y="-4.756170444462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.259990358383" Y="0.89887987455" />
                  <Point X="20.522814518762" Y="0.509227032791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.140356812516" Y="-0.406317068335" />
                  <Point X="21.620396950544" Y="-1.118005840294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.936070248523" Y="-1.586010750681" />
                  <Point X="22.45017227042" Y="-2.348198342178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.675578257928" Y="-2.682376461326" />
                  <Point X="23.441305599133" Y="-3.81761392992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.866897017208" Y="-4.448579154892" />
                  <Point X="24.033613149516" Y="-4.695745985473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.287686250818" Y="1.027706732186" />
                  <Point X="20.662684467669" Y="0.47174901262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.237407174193" Y="-0.38031243979" />
                  <Point X="21.725641820899" Y="-1.104150070472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.00301828829" Y="-1.515377594609" />
                  <Point X="22.638667188009" Y="-2.457765843011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.65302112742" Y="-2.479046433327" />
                  <Point X="23.690630550238" Y="-4.017365664157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.818968788299" Y="-4.207634926674" />
                  <Point X="24.066192768854" Y="-4.574159550725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.320528162269" Y="1.148904302884" />
                  <Point X="20.802554463145" Y="0.43427092341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.33445753587" Y="-0.354307811245" />
                  <Point X="21.842329558356" Y="-1.107259048783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.046503074047" Y="-1.40995873395" />
                  <Point X="24.098772388191" Y="-4.452573115977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.353370233055" Y="1.27010163736" />
                  <Point X="20.94242445862" Y="0.396792834199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.431507897547" Y="-0.328303182701" />
                  <Point X="24.131351992369" Y="-4.330986658752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.442225179908" Y="1.308256468043" />
                  <Point X="21.082294454096" Y="0.359314744988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.526208101487" Y="-0.298814302024" />
                  <Point X="24.163931120883" Y="-4.209399496329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.567983287513" Y="1.291700112982" />
                  <Point X="21.222164449571" Y="0.321836655777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.606105633373" Y="-0.247379557532" />
                  <Point X="24.196510249398" Y="-4.087812333906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.693741395117" Y="1.27514375792" />
                  <Point X="21.362034445047" Y="0.284358566567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.669349292885" Y="-0.171254431882" />
                  <Point X="24.229089377913" Y="-3.966225171483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.819499516498" Y="1.258587382435" />
                  <Point X="21.502546517775" Y="0.245928558682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.70465540807" Y="-0.053710193459" />
                  <Point X="24.261668506428" Y="-3.84463800906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.945257653611" Y="1.242030983625" />
                  <Point X="24.294247634943" Y="-3.723050846637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.071015790725" Y="1.225474584815" />
                  <Point X="24.326826763457" Y="-3.601463684214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.196773927838" Y="1.208918186005" />
                  <Point X="24.359788067607" Y="-3.48044312047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.309956052186" Y="1.21100649286" />
                  <Point X="24.410732404106" Y="-3.386083498583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.404267266338" Y="1.241072074613" />
                  <Point X="24.468846081293" Y="-3.302352861369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.789121404877" Y="2.322951025505" />
                  <Point X="20.84716039126" Y="2.236904689641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.48376665797" Y="1.293097086307" />
                  <Point X="24.526959373419" Y="-3.21862165328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.84228117394" Y="2.414026133545" />
                  <Point X="21.084688051785" Y="2.054643157953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.539972030073" Y="1.379656902154" />
                  <Point X="24.589307396185" Y="-3.14116869155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.89544100846" Y="2.50510114454" />
                  <Point X="21.322216093374" Y="1.872381061312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.576802998642" Y="1.494940452467" />
                  <Point X="24.66932863249" Y="-3.0899173464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.94860084298" Y="2.596176155536" />
                  <Point X="24.763440606975" Y="-3.059556379694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.002385461652" Y="2.686324885934" />
                  <Point X="24.858195203256" Y="-3.03014813898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.063762519791" Y="2.765217361921" />
                  <Point X="24.954027464806" Y="-3.002337602731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.125139577931" Y="2.844109837909" />
                  <Point X="25.070194377526" Y="-3.004674426616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.825705544561" Y="-4.124765794138" />
                  <Point X="25.830372773837" Y="-4.131685246092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.186516857373" Y="2.923001985801" />
                  <Point X="25.21463587056" Y="-3.048930039674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.750183193805" Y="-3.842911597909" />
                  <Point X="25.802582102601" Y="-3.920596174882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.247894628242" Y="3.001893405124" />
                  <Point X="25.363792444929" Y="-3.100176048284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.674660843049" Y="-3.56105740168" />
                  <Point X="25.774791431366" Y="-3.709507103673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.382549696583" Y="2.972146763335" />
                  <Point X="25.74700076013" Y="-3.498418032463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.570227590278" Y="2.863790550238" />
                  <Point X="25.724727670119" Y="-3.295509111817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.756907686531" Y="2.756913632682" />
                  <Point X="25.748917758874" Y="-3.161484686483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.891626611837" Y="2.727072319052" />
                  <Point X="25.776844411867" Y="-3.032999945444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.004532805166" Y="2.729569710466" />
                  <Point X="25.819139045637" Y="-2.925816611901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.092160897183" Y="2.769543428243" />
                  <Point X="25.885401482313" Y="-2.854167007449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.162780531874" Y="2.834733220987" />
                  <Point X="25.958818255066" Y="-2.79312414242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.224398341375" Y="2.913268768404" />
                  <Point X="26.032234592966" Y="-2.732080632693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.251239045043" Y="3.043363495525" />
                  <Point X="26.114369987529" Y="-2.683963656058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.045385614271" Y="3.518441463969" />
                  <Point X="26.215664793973" Y="-2.664251675658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.004521031442" Y="3.748913406213" />
                  <Point X="26.323558581085" Y="-2.654323086428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.080051649088" Y="3.806822367311" />
                  <Point X="26.433245572765" Y="-2.647053032299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.155582430299" Y="3.864731085914" />
                  <Point X="26.581947751114" Y="-2.697625371105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.23111321151" Y="3.922639804517" />
                  <Point X="26.786107235491" Y="-2.830416547247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.306643992721" Y="3.980548523119" />
                  <Point X="27.642530903141" Y="-3.930229142669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.385338333807" Y="4.03376707133" />
                  <Point X="27.218995367333" Y="-3.132424181754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.468692651476" Y="4.080076920144" />
                  <Point X="27.017224327019" Y="-2.663398606061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.552046969145" Y="4.126386768957" />
                  <Point X="27.00773595671" Y="-2.479443811839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.635401284425" Y="4.172696621313" />
                  <Point X="27.05232694067" Y="-2.375664957458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.718755589971" Y="4.219006488101" />
                  <Point X="27.102550735742" Y="-2.280237088975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.802109895517" Y="4.265316354888" />
                  <Point X="27.159623372983" Y="-2.194963046572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.059036399808" Y="4.054294854597" />
                  <Point X="27.235712722172" Y="-2.137882439051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.189745055725" Y="4.030399009836" />
                  <Point X="27.320282079632" Y="-2.093373960806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.285812690621" Y="4.057860590748" />
                  <Point X="27.405078676999" Y="-2.049202379578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.374551252732" Y="4.096187968908" />
                  <Point X="27.503814905775" Y="-2.025697151792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.449439206797" Y="4.155049717947" />
                  <Point X="27.628438063455" Y="-2.040570874393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.499389218383" Y="4.250883487139" />
                  <Point X="27.758924060347" Y="-2.064136613576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.533916943593" Y="4.369581736159" />
                  <Point X="27.923229382849" Y="-2.137841564889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.521332597502" Y="4.558126503235" />
                  <Point X="28.110906760517" Y="-2.246197012945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.615035431467" Y="4.589094045707" />
                  <Point X="28.298584138185" Y="-2.354552461001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.711402320092" Y="4.616111964722" />
                  <Point X="27.868029451124" Y="-1.546341180408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.075175686625" Y="-1.853448103935" />
                  <Point X="28.486261651852" Y="-2.462908110684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.807769246024" Y="4.643129828427" />
                  <Point X="27.867842411392" Y="-1.376176175853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.312703534997" Y="-2.035709914119" />
                  <Point X="28.673939488587" Y="-2.571264239335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.904136171956" Y="4.670147692132" />
                  <Point X="27.889840714944" Y="-1.238902295327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.550231383369" Y="-2.217971724303" />
                  <Point X="28.861617325322" Y="-2.679620367986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.000503097887" Y="4.697165555837" />
                  <Point X="27.943475637132" Y="-1.148531630764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.787759313545" Y="-2.400233655766" />
                  <Point X="29.016246844261" Y="-2.738980350598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.099733863516" Y="4.719937602588" />
                  <Point X="28.006742613158" Y="-1.072441073269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.025287538601" Y="-2.582496024408" />
                  <Point X="29.072897220544" Y="-2.653080280579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.205940414633" Y="4.732367622049" />
                  <Point X="28.073731845441" Y="-1.001868987616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.312146965751" Y="4.744797641509" />
                  <Point X="28.157882712587" Y="-0.956740071964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.418353547075" Y="4.757227616185" />
                  <Point X="28.257038119162" Y="-0.933856300823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.524560224353" Y="4.769657448605" />
                  <Point X="24.713150751714" Y="4.490060493708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.985741092324" Y="4.085928694326" />
                  <Point X="28.357080038148" Y="-0.912286838379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.63076690163" Y="4.782087281026" />
                  <Point X="24.637629082611" Y="4.771913679345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.089774148475" Y="4.101581052589" />
                  <Point X="28.472825328339" Y="-0.913998581159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.170785824671" Y="4.151364010214" />
                  <Point X="28.598583459964" Y="-0.930554971832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.227945970571" Y="4.236508315696" />
                  <Point X="28.724341591589" Y="-0.947111362504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.262870715536" Y="4.354617958722" />
                  <Point X="28.850099723214" Y="-0.963667753177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.295450195066" Y="4.476204600744" />
                  <Point X="28.349776563492" Y="-0.052020458183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.619046766638" Y="-0.451229951351" />
                  <Point X="28.975857854838" Y="-0.980224143849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.328029674596" Y="4.597791242765" />
                  <Point X="28.366924727537" Y="0.092444049869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.769732750664" Y="-0.504743403022" />
                  <Point X="29.101615983788" Y="-0.996780530557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.3606088497" Y="4.719378336118" />
                  <Point X="27.050182529432" Y="2.21448234512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.116919389591" Y="2.115540881088" />
                  <Point X="28.408614900924" Y="0.200523532782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.909602729981" Y="-0.542221468278" />
                  <Point X="29.227374085208" Y="-1.013336876448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.434941792403" Y="4.779062923339" />
                  <Point X="27.01572647382" Y="2.435453255047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.317100160533" Y="1.98864839019" />
                  <Point X="28.469544678871" Y="0.280079128926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.049472709299" Y="-0.579699533534" />
                  <Point X="29.353132186628" Y="-1.02989322234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.558242293192" Y="4.766150120218" />
                  <Point X="27.047469443315" Y="2.558280074196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.440587742107" Y="1.975458228399" />
                  <Point X="28.541237610479" Y="0.343677693552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.189342688617" Y="-0.61717759879" />
                  <Point X="29.478890288048" Y="-1.046449568232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.681542852122" Y="4.7532372309" />
                  <Point X="27.097389894913" Y="2.654157667874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.54243812316" Y="1.994346535569" />
                  <Point X="28.017649269073" Y="1.289817038837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.192517697485" Y="1.030563932248" />
                  <Point X="28.623675972992" Y="0.391345501729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.329212667934" Y="-0.654655664045" />
                  <Point X="29.604648389467" Y="-1.063005914124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.804843411052" Y="4.740324341582" />
                  <Point X="27.150238441102" Y="2.745694182798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.634831642276" Y="2.027255217132" />
                  <Point X="28.010076252848" Y="1.470932203853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.351890664666" Y="0.964171498416" />
                  <Point X="28.714041479988" Y="0.427260834905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.469082648461" Y="-0.692133731094" />
                  <Point X="29.728403982773" Y="-1.076593419647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.93918783007" Y="4.711038256355" />
                  <Point X="27.203086987292" Y="2.837230697722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.717342501377" Y="2.074815544698" />
                  <Point X="28.047774567966" Y="1.584929860028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.475016252035" Y="0.951518015106" />
                  <Point X="28.81109189723" Y="0.453265381071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.608952631935" Y="-0.729611802511" />
                  <Point X="29.757370904747" Y="-0.949650940796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.076069266983" Y="4.677990887421" />
                  <Point X="27.255935533481" Y="2.928767212646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.799815792718" Y="2.122431568758" />
                  <Point X="28.105471717321" Y="1.669278025147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.581555668203" Y="0.963454541834" />
                  <Point X="28.908142314472" Y="0.479269927238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.748822615409" Y="-0.767089873929" />
                  <Point X="29.77948054708" Y="-0.8125421268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.212950755088" Y="4.644943442592" />
                  <Point X="27.308784079671" Y="3.02030372757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.882289084059" Y="2.170047592818" />
                  <Point X="28.170912617785" Y="1.742145607121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.686800547039" Y="0.977310299082" />
                  <Point X="29.005192731714" Y="0.505274473404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.34983225193" Y="4.611895984811" />
                  <Point X="27.361632625861" Y="3.111840242494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.9647623754" Y="2.217663616878" />
                  <Point X="28.246373753033" Y="1.800157580111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.792045425876" Y="0.99116605633" />
                  <Point X="29.102243148956" Y="0.531279019571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.490300527814" Y="4.573530908418" />
                  <Point X="27.41448117205" Y="3.203376757418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.047235666741" Y="2.265279640939" />
                  <Point X="28.321883214389" Y="1.858097906698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.897290304713" Y="1.005021813578" />
                  <Point X="29.199293566198" Y="0.557283565737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.642005800692" Y="4.518506298878" />
                  <Point X="27.467329690872" Y="3.294913312917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.129708958082" Y="2.312895664999" />
                  <Point X="28.397392675745" Y="1.916038233286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.00253518355" Y="1.018877570827" />
                  <Point X="29.296343959124" Y="0.583288147953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.793711010701" Y="4.463481782546" />
                  <Point X="27.520178186206" Y="3.386449903237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.212182249423" Y="2.360511689059" />
                  <Point X="28.472902137101" Y="1.973978559873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.107780062386" Y="1.032733328075" />
                  <Point X="29.393394318359" Y="0.609292780119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.954429169816" Y="4.395095019658" />
                  <Point X="27.573026681541" Y="3.477986493556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.294655540764" Y="2.408127713119" />
                  <Point X="28.548411598457" Y="2.031918886461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.213024941223" Y="1.046589085323" />
                  <Point X="29.490444677593" Y="0.635297412285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.121823678582" Y="4.316810161366" />
                  <Point X="27.625875176875" Y="3.569523083876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.377128832105" Y="2.45574373718" />
                  <Point X="28.623921059813" Y="2.089859213048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.318269821702" Y="1.060444840136" />
                  <Point X="29.587495036828" Y="0.661302044451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.294169082451" Y="4.231185299235" />
                  <Point X="27.67872367221" Y="3.661059674195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.459602123446" Y="2.50335976124" />
                  <Point X="28.699430467029" Y="2.147799619901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.423514712329" Y="1.074300579904" />
                  <Point X="29.684545396063" Y="0.687306676617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.48294187513" Y="4.121205831639" />
                  <Point X="27.731572167545" Y="3.752596264515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.542075454371" Y="2.550975726615" />
                  <Point X="28.774939810328" Y="2.205740121515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.528759602956" Y="1.088156319672" />
                  <Point X="29.781595755297" Y="0.713311308783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.678760958854" Y="4.000779807967" />
                  <Point X="27.784420662879" Y="3.844132854834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.624548799483" Y="2.598591670956" />
                  <Point X="28.850449153628" Y="2.263680623128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.634004493584" Y="1.10201205944" />
                  <Point X="29.748177445573" Y="0.93274369716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.707022144596" Y="2.646207615297" />
                  <Point X="28.925958496927" Y="2.321621124742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.789495489708" Y="2.693823559638" />
                  <Point X="29.001467840226" Y="2.379561626356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.87196883482" Y="2.741439503979" />
                  <Point X="29.076977183526" Y="2.437502127969" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.645220703125" Y="-4.185296386719" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.39333203125" Y="-3.419264648438" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.164486328125" Y="-3.232306640625" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.88148828125" Y="-3.221859619141" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.640734375" Y="-3.387923339844" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.313873046875" Y="-4.38391796875" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.068060546875" Y="-4.970733886719" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.7730078125" Y="-4.905454589844" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.669943359375" Y="-4.70984375" />
                  <Point X="23.6908515625" Y="-4.5510390625" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.66407421875" Y="-4.402825195312" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.51258203125" Y="-4.113935546875" />
                  <Point X="23.38045703125" Y="-3.998064208984" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.21653125" Y="-3.976965087891" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.898275390625" Y="-4.048524658203" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.61287890625" Y="-4.32330859375" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.390869140625" Y="-4.320362304688" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.96866015625" Y="-4.032476074219" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.171201171875" Y="-3.188165527344" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597593994141" />
                  <Point X="22.486021484375" Y="-2.568765136719" />
                  <Point X="22.468673828125" Y="-2.551416503906" />
                  <Point X="22.439845703125" Y="-2.537198730469" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.67796484375" Y="-2.965139404297" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.033203125" Y="-3.103197753906" />
                  <Point X="20.838302734375" Y="-2.847135986328" />
                  <Point X="20.71247265625" Y="-2.636140380859" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.271478515625" Y="-1.856480957031" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396018188477" />
                  <Point X="21.861884765625" Y="-1.366264038086" />
                  <Point X="21.859673828125" Y="-1.334594970703" />
                  <Point X="21.838841796875" Y="-1.310639770508" />
                  <Point X="21.812359375" Y="-1.295053100586" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.858470703125" Y="-1.409954956055" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.148837890625" Y="-1.309627685547" />
                  <Point X="20.072607421875" Y="-1.011188171387" />
                  <Point X="20.039318359375" Y="-0.778430725098" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="20.799220703125" Y="-0.301021881104" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.47013671875" Y="-0.113074172974" />
                  <Point X="21.49767578125" Y="-0.09064591217" />
                  <Point X="21.509111328125" Y="-0.062984870911" />
                  <Point X="21.516599609375" Y="-0.031280040741" />
                  <Point X="21.510341796875" Y="-0.00353524971" />
                  <Point X="21.49767578125" Y="0.028085834503" />
                  <Point X="21.47382421875" Y="0.04795488739" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.60208984375" Y="0.291282806396" />
                  <Point X="20.001814453125" Y="0.452126068115" />
                  <Point X="20.0332265625" Y="0.664410766602" />
                  <Point X="20.08235546875" Y="0.996414611816" />
                  <Point X="20.1493671875" Y="1.243714355469" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.78558984375" Y="1.45469128418" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.294927734375" Y="1.404263671875" />
                  <Point X="21.32972265625" Y="1.415234008789" />
                  <Point X="21.348466796875" Y="1.426056030273" />
                  <Point X="21.360880859375" Y="1.443786010742" />
                  <Point X="21.37156640625" Y="1.469585205078" />
                  <Point X="21.38552734375" Y="1.503290283203" />
                  <Point X="21.389287109375" Y="1.52460546875" />
                  <Point X="21.38368359375" Y="1.545511962891" />
                  <Point X="21.3707890625" Y="1.570281616211" />
                  <Point X="21.353943359375" Y="1.602641601562" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.85794921875" Y="1.989137084961" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.64908984375" Y="2.459954589844" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="21.017498046875" Y="3.015174072266" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.55762109375" Y="3.090462158203" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.898578125" Y="2.917189697266" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775146484" />
                  <Point X="21.986748046875" Y="2.927404052734" />
                  <Point X="22.013076171875" Y="2.953731933594" />
                  <Point X="22.04747265625" Y="2.988127441406" />
                  <Point X="22.0591015625" Y="3.006381347656" />
                  <Point X="22.061927734375" Y="3.02783984375" />
                  <Point X="22.058681640625" Y="3.064931640625" />
                  <Point X="22.054443359375" Y="3.113389404297" />
                  <Point X="22.04793359375" Y="3.134032714844" />
                  <Point X="21.834306640625" Y="3.504043212891" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.914642578125" Y="3.919421630859" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.526701171875" Y="4.329658203125" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.945470703125" Y="4.400566894531" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.090037109375" Y="4.252169921875" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.22919140625" Y="4.240061523438" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.303095703125" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.294487792969" />
                  <Point X="23.327912109375" Y="4.338875488281" />
                  <Point X="23.346197265625" Y="4.396864257812" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.323796875" Y="4.602903808594" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.601490234375" Y="4.782622558594" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.370837890625" Y="4.942963378906" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.87407421875" Y="4.623594238281" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.1639140625" Y="4.719411621094" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.484388671875" Y="4.964923828125" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.14061328125" Y="4.857866699219" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.691107421875" Y="4.702809082031" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="27.107521484375" Y="4.533249023438" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.5091953125" Y="4.325803222656" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.893322265625" Y="4.081340820312" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.603431640625" Y="3.150651855469" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491515869141" />
                  <Point X="27.215544921875" Y="2.456706054688" />
                  <Point X="27.2033828125" Y="2.411229492188" />
                  <Point X="27.202044921875" Y="2.392327636719" />
                  <Point X="27.20567578125" Y="2.362227050781" />
                  <Point X="27.210416015625" Y="2.322902832031" />
                  <Point X="27.21868359375" Y="2.300811279297" />
                  <Point X="27.23730859375" Y="2.273362548828" />
                  <Point X="27.261642578125" Y="2.237502685547" />
                  <Point X="27.27494140625" Y="2.224203125" />
                  <Point X="27.302388671875" Y="2.205577880859" />
                  <Point X="27.33825" Y="2.181245605469" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.3904375" Y="2.169350585938" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.483474609375" Y="2.175254882813" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.38636328125" Y="2.680468505859" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.07012109375" Y="2.925984619141" />
                  <Point X="29.202595703125" Y="2.741875244141" />
                  <Point X="29.292234375" Y="2.593744873047" />
                  <Point X="29.387513671875" Y="2.436295654297" />
                  <Point X="28.780466796875" Y="1.970491699219" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583832641602" />
                  <Point X="28.254318359375" Y="1.551149536133" />
                  <Point X="28.22158984375" Y="1.508451293945" />
                  <Point X="28.21312109375" Y="1.491501708984" />
                  <Point X="28.2037890625" Y="1.458132080078" />
                  <Point X="28.191595703125" Y="1.414537231445" />
                  <Point X="28.190779296875" Y="1.390965698242" />
                  <Point X="28.19844140625" Y="1.353837768555" />
                  <Point X="28.20844921875" Y="1.305332885742" />
                  <Point X="28.215646484375" Y="1.287955200195" />
                  <Point X="28.236482421875" Y="1.256284912109" />
                  <Point X="28.263703125" Y="1.214909912109" />
                  <Point X="28.280947265625" Y="1.198819824219" />
                  <Point X="28.311142578125" Y="1.181822875977" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.409392578125" Y="1.148223999023" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.278810546875" Y="1.246889526367" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.88229296875" Y="1.185094360352" />
                  <Point X="29.939193359375" Y="0.951366943359" />
                  <Point X="29.967439453125" Y="0.769947021484" />
                  <Point X="29.997859375" Y="0.57455645752" />
                  <Point X="29.304837890625" Y="0.388861572266" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819351196" />
                  <Point X="28.688978515625" Y="0.209635314941" />
                  <Point X="28.636578125" Y="0.179346984863" />
                  <Point X="28.622265625" Y="0.166926239014" />
                  <Point X="28.59819921875" Y="0.136260665894" />
                  <Point X="28.566759765625" Y="0.096198562622" />
                  <Point X="28.556986328125" Y="0.074735412598" />
                  <Point X="28.54896484375" Y="0.032847919464" />
                  <Point X="28.538484375" Y="-0.021874994278" />
                  <Point X="28.538484375" Y="-0.040685081482" />
                  <Point X="28.546505859375" Y="-0.08257257843" />
                  <Point X="28.556986328125" Y="-0.13729548645" />
                  <Point X="28.566759765625" Y="-0.158758636475" />
                  <Point X="28.590826171875" Y="-0.189424057007" />
                  <Point X="28.622265625" Y="-0.229486312866" />
                  <Point X="28.636578125" Y="-0.241907058716" />
                  <Point X="28.6766875" Y="-0.265091094971" />
                  <Point X="28.729087890625" Y="-0.295379425049" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.477560546875" Y="-0.497702880859" />
                  <Point X="29.998068359375" Y="-0.637172546387" />
                  <Point X="29.980201171875" Y="-0.75568963623" />
                  <Point X="29.948431640625" Y="-0.966413208008" />
                  <Point X="29.9122421875" Y="-1.12499621582" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.0656796875" Y="-1.183689086914" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.3161171875" Y="-1.115451538086" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.1854453125" Y="-1.154697265625" />
                  <Point X="28.13786328125" Y="-1.211923461914" />
                  <Point X="28.075701171875" Y="-1.286685424805" />
                  <Point X="28.064359375" Y="-1.314071166992" />
                  <Point X="28.0575390625" Y="-1.388181640625" />
                  <Point X="28.048630859375" Y="-1.485001464844" />
                  <Point X="28.056361328125" Y="-1.516622192383" />
                  <Point X="28.09992578125" Y="-1.584385253906" />
                  <Point X="28.156841796875" Y="-1.672912719727" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.85183984375" Y="-2.209914794922" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.29368359375" Y="-2.657234619141" />
                  <Point X="29.2041328125" Y="-2.802140625" />
                  <Point X="29.12928515625" Y="-2.908488525391" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.335662109375" Y="-2.595352539062" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.64365234375" Y="-2.236392333984" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.411244140625" Y="-2.260208251953" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334683837891" />
                  <Point X="27.24763671875" Y="-2.412517578125" />
                  <Point X="27.19412109375" Y="-2.514201904297" />
                  <Point X="27.1891640625" Y="-2.546374511719" />
                  <Point X="27.2060859375" Y="-2.640064941406" />
                  <Point X="27.22819140625" Y="-2.76246484375" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.673232421875" Y="-3.539188964844" />
                  <Point X="27.98667578125" Y="-4.082088867188" />
                  <Point X="27.941560546875" Y="-4.114313476562" />
                  <Point X="27.835314453125" Y="-4.190202148438" />
                  <Point X="27.751615234375" Y="-4.24437890625" />
                  <Point X="27.679775390625" Y="-4.29087890625" />
                  <Point X="27.127455078125" Y="-3.571080078125" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.578146484375" Y="-2.921059814453" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.32474609375" Y="-2.845016601562" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.087298828125" Y="-2.933393066406" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045986083984" />
                  <Point X="25.945125" Y="-3.153333007812" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="26.03837890625" Y="-4.256000488281" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.094876953125" Y="-4.9412109375" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.9170390625" Y="-4.977291015625" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#160" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.083785213187" Y="4.667815162065" Z="1.1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.1" />
                  <Point X="-0.641131131529" Y="5.023904450945" Z="1.1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.1" />
                  <Point X="-1.4181653662" Y="4.862046048714" Z="1.1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.1" />
                  <Point X="-1.731932719004" Y="4.627657504962" Z="1.1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.1" />
                  <Point X="-1.725929802209" Y="4.385191551583" Z="1.1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.1" />
                  <Point X="-1.796100133699" Y="4.317535462703" Z="1.1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.1" />
                  <Point X="-1.893032253184" Y="4.327800627403" Z="1.1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.1" />
                  <Point X="-2.021018339089" Y="4.462285101628" Z="1.1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.1" />
                  <Point X="-2.503738172256" Y="4.404645864465" Z="1.1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.1" />
                  <Point X="-3.121935346634" Y="3.990381513835" Z="1.1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.1" />
                  <Point X="-3.215150359384" Y="3.510323407381" Z="1.1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.1" />
                  <Point X="-2.997285149194" Y="3.091855263835" Z="1.1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.1" />
                  <Point X="-3.028435595926" Y="3.020367971961" Z="1.1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.1" />
                  <Point X="-3.103221201772" Y="2.998279549979" Z="1.1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.1" />
                  <Point X="-3.423535940916" Y="3.165043589361" Z="1.1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.1" />
                  <Point X="-4.028121176939" Y="3.077156474137" Z="1.1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.1" />
                  <Point X="-4.400249292481" Y="2.516439632619" Z="1.1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.1" />
                  <Point X="-4.178645510727" Y="1.980749545757" Z="1.1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.1" />
                  <Point X="-3.679716769102" Y="1.578474172525" Z="1.1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.1" />
                  <Point X="-3.680783480825" Y="1.519999428946" Z="1.1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.1" />
                  <Point X="-3.726263475507" Y="1.483229751464" Z="1.1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.1" />
                  <Point X="-4.214041974711" Y="1.535543563178" Z="1.1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.1" />
                  <Point X="-4.905048523348" Y="1.288071814211" Z="1.1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.1" />
                  <Point X="-5.022391214307" Y="0.703009803705" Z="1.1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.1" />
                  <Point X="-4.417009196047" Y="0.274266521766" Z="1.1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.1" />
                  <Point X="-3.560840698209" Y="0.038158428334" Z="1.1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.1" />
                  <Point X="-3.543567756891" Y="0.012923425782" Z="1.1" />
                  <Point X="-3.539556741714" Y="0" Z="1.1" />
                  <Point X="-3.544796844244" Y="-0.016883525278" Z="1.1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.1" />
                  <Point X="-3.564527896915" Y="-0.0407175548" Z="1.1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.1" />
                  <Point X="-4.219878592726" Y="-0.22144553499" Z="1.1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.1" />
                  <Point X="-5.016335148551" Y="-0.754229780996" Z="1.1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.1" />
                  <Point X="-4.905764212068" Y="-1.290721705598" Z="1.1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.1" />
                  <Point X="-4.141160748452" Y="-1.428247132395" Z="1.1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.1" />
                  <Point X="-3.204158381999" Y="-1.315691952941" Z="1.1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.1" />
                  <Point X="-3.197040356934" Y="-1.33929957843" Z="1.1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.1" />
                  <Point X="-3.765115447733" Y="-1.785533286564" Z="1.1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.1" />
                  <Point X="-4.336627752738" Y="-2.630469906474" Z="1.1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.1" />
                  <Point X="-4.012738268183" Y="-3.102200836232" Z="1.1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.1" />
                  <Point X="-3.303192867233" Y="-2.977160678785" Z="1.1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.1" />
                  <Point X="-2.563012786996" Y="-2.565317682136" Z="1.1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.1" />
                  <Point X="-2.878256334383" Y="-3.131885515259" Z="1.1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.1" />
                  <Point X="-3.068001335584" Y="-4.040813183639" Z="1.1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.1" />
                  <Point X="-2.641610480335" Y="-4.331593202872" Z="1.1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.1" />
                  <Point X="-2.353609625638" Y="-4.322466546257" Z="1.1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.1" />
                  <Point X="-2.08010266183" Y="-4.058818030071" Z="1.1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.1" />
                  <Point X="-1.792895686009" Y="-3.995578102255" Z="1.1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.1" />
                  <Point X="-1.526541098108" Y="-4.120247266256" Z="1.1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.1" />
                  <Point X="-1.391121471935" Y="-4.381300202324" Z="1.1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.1" />
                  <Point X="-1.385785546148" Y="-4.672036676465" Z="1.1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.1" />
                  <Point X="-1.245607536643" Y="-4.922597360672" Z="1.1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.1" />
                  <Point X="-0.947623907336" Y="-4.988538066987" Z="1.1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.1" />
                  <Point X="-0.643987696411" Y="-4.365578887507" Z="1.1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.1" />
                  <Point X="-0.324346641434" Y="-3.385152220032" Z="1.1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.1" />
                  <Point X="-0.109849117132" Y="-3.238332480585" Z="1.1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.1" />
                  <Point X="0.143509962229" Y="-3.248779564703" Z="1.1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.1" />
                  <Point X="0.346099217998" Y="-3.416493535736" Z="1.1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.1" />
                  <Point X="0.59076716399" Y="-4.166957142309" Z="1.1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.1" />
                  <Point X="0.919819086315" Y="-4.995205841244" Z="1.1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.1" />
                  <Point X="1.099425224657" Y="-4.958771252953" Z="1.1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.1" />
                  <Point X="1.081794336177" Y="-4.218194185799" Z="1.1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.1" />
                  <Point X="0.987827732379" Y="-3.132672820393" Z="1.1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.1" />
                  <Point X="1.113108046062" Y="-2.940559569679" Z="1.1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.1" />
                  <Point X="1.323170850173" Y="-2.863526186089" Z="1.1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.1" />
                  <Point X="1.544949780911" Y="-2.931837960871" Z="1.1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.1" />
                  <Point X="2.081630837233" Y="-3.570238132594" Z="1.1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.1" />
                  <Point X="2.772628877895" Y="-4.255072887878" Z="1.1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.1" />
                  <Point X="2.964464074502" Y="-4.123720328936" Z="1.1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.1" />
                  <Point X="2.710375638829" Y="-3.48290891707" Z="1.1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.1" />
                  <Point X="2.249131556001" Y="-2.599898702348" Z="1.1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.1" />
                  <Point X="2.285726818088" Y="-2.404523966139" Z="1.1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.1" />
                  <Point X="2.428374412234" Y="-2.273174491879" Z="1.1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.1" />
                  <Point X="2.628608101165" Y="-2.25431645349" Z="1.1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.1" />
                  <Point X="3.304504680037" Y="-2.607373861446" Z="1.1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.1" />
                  <Point X="4.164017622469" Y="-2.905985564696" Z="1.1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.1" />
                  <Point X="4.33005993411" Y="-2.652239733005" Z="1.1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.1" />
                  <Point X="3.876120150157" Y="-2.138966964149" Z="1.1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.1" />
                  <Point X="3.135827845674" Y="-1.526065519844" Z="1.1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.1" />
                  <Point X="3.101171842191" Y="-1.36148261624" Z="1.1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.1" />
                  <Point X="3.170153695898" Y="-1.21261035632" Z="1.1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.1" />
                  <Point X="3.320578854544" Y="-1.133030760658" Z="1.1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.1" />
                  <Point X="4.052997647903" Y="-1.20198136441" Z="1.1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.1" />
                  <Point X="4.954831285412" Y="-1.104840104145" Z="1.1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.1" />
                  <Point X="5.023485095959" Y="-0.731863745663" Z="1.1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.1" />
                  <Point X="4.484345920586" Y="-0.418126596676" Z="1.1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.1" />
                  <Point X="3.695552349603" Y="-0.190522295291" Z="1.1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.1" />
                  <Point X="3.624002504656" Y="-0.127275991481" Z="1.1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.1" />
                  <Point X="3.589456653697" Y="-0.041887512346" Z="1.1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.1" />
                  <Point X="3.591914796727" Y="0.05472301887" Z="1.1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.1" />
                  <Point X="3.631376933744" Y="0.136672747094" Z="1.1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.1" />
                  <Point X="3.707843064749" Y="0.197626548951" Z="1.1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.1" />
                  <Point X="4.311621896721" Y="0.371845336093" Z="1.1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.1" />
                  <Point X="5.010686173246" Y="0.808919012165" Z="1.1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.1" />
                  <Point X="4.924718082841" Y="1.22820116975" Z="1.1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.1" />
                  <Point X="4.266127625506" Y="1.327741888977" Z="1.1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.1" />
                  <Point X="3.409785997122" Y="1.229073008235" Z="1.1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.1" />
                  <Point X="3.32951332376" Y="1.256673989802" Z="1.1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.1" />
                  <Point X="3.272097352423" Y="1.315046057863" Z="1.1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.1" />
                  <Point X="3.241252780867" Y="1.395221316736" Z="1.1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.1" />
                  <Point X="3.245783827009" Y="1.475944138211" Z="1.1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.1" />
                  <Point X="3.287845491788" Y="1.552011922185" Z="1.1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.1" />
                  <Point X="3.804746905192" Y="1.962103904731" Z="1.1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.1" />
                  <Point X="4.328855742611" Y="2.650911344313" Z="1.1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.1" />
                  <Point X="4.104550421894" Y="2.986467718283" Z="1.1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.1" />
                  <Point X="3.355207257399" Y="2.755049860427" Z="1.1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.1" />
                  <Point X="2.464401441457" Y="2.254837764998" Z="1.1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.1" />
                  <Point X="2.390267388543" Y="2.250270953064" Z="1.1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.1" />
                  <Point X="2.324306887445" Y="2.2782329323" Z="1.1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.1" />
                  <Point X="2.272525667935" Y="2.332717972937" Z="1.1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.1" />
                  <Point X="2.249158749695" Y="2.39949105529" Z="1.1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.1" />
                  <Point X="2.25769017766" Y="2.475068134425" Z="1.1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.1" />
                  <Point X="2.640575492432" Y="3.156931824556" Z="1.1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.1" />
                  <Point X="2.916142736746" Y="4.153367935734" Z="1.1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.1" />
                  <Point X="2.528209082677" Y="4.400285752575" Z="1.1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.1" />
                  <Point X="2.122546016308" Y="4.609821055033" Z="1.1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.1" />
                  <Point X="1.701999664586" Y="4.781093086162" Z="1.1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.1" />
                  <Point X="1.146191586978" Y="4.937750238619" Z="1.1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.1" />
                  <Point X="0.483439660544" Y="5.045931855069" Z="1.1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.1" />
                  <Point X="0.109459348366" Y="4.763632343886" Z="1.1" />
                  <Point X="0" Y="4.355124473572" Z="1.1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>