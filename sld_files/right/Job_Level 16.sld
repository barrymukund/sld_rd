<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#147" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1275" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004715820312" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.681787109375" Y="-3.954706054688" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.54236328125" Y="-3.467376464844" />
                  <Point X="25.493935546875" Y="-3.397602783203" />
                  <Point X="25.378634765625" Y="-3.231476074219" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.22755859375" Y="-3.152411376953" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.88823828125" Y="-3.120293701172" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.58525" Y="-3.301250732422" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.16691015625" Y="-4.565332519531" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.920658203125" Y="-4.845349609375" />
                  <Point X="23.83742578125" Y="-4.823934570312" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.770775390625" Y="-4.671772460938" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.51622265625" />
                  <Point X="23.765587890625" Y="-4.426220703125" />
                  <Point X="23.722962890625" Y="-4.211931152344" />
                  <Point X="23.712060546875" Y="-4.182965332031" />
                  <Point X="23.69598828125" Y="-4.155127929687" />
                  <Point X="23.67635546875" Y="-4.131204589844" />
                  <Point X="23.60736328125" Y="-4.07069921875" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.265404296875" Y="-3.884964599609" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.88104296875" Y="-3.945783935547" />
                  <Point X="22.699376953125" Y="-4.067169921875" />
                  <Point X="22.68721484375" Y="-4.076821533203" />
                  <Point X="22.6648984375" Y="-4.099461425781" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.501421875" Y="-4.277077636719" />
                  <Point X="22.19828515625" Y="-4.0893828125" />
                  <Point X="22.083037109375" Y="-4.000645507812" />
                  <Point X="21.89527734375" Y="-3.856077148438" />
                  <Point X="22.356052734375" Y="-3.057992431641" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647655029297" />
                  <Point X="22.593412109375" Y="-2.616129150391" />
                  <Point X="22.59442578125" Y="-2.585194824219" />
                  <Point X="22.58544140625" Y="-2.555576416016" />
                  <Point X="22.571224609375" Y="-2.526747558594" />
                  <Point X="22.55319921875" Y="-2.501591552734" />
                  <Point X="22.5358515625" Y="-2.484242919922" />
                  <Point X="22.510693359375" Y="-2.466214599609" />
                  <Point X="22.481865234375" Y="-2.451997070313" />
                  <Point X="22.452248046875" Y="-2.44301171875" />
                  <Point X="22.4213125" Y="-2.444023925781" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.452791015625" Y="-2.985446533203" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="21.15662890625" Y="-3.108499511719" />
                  <Point X="20.917142578125" Y="-2.793862548828" />
                  <Point X="20.834513671875" Y="-2.65530859375" />
                  <Point X="20.693857421875" Y="-2.419450195312" />
                  <Point X="21.50879296875" Y="-1.79412890625" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.916935546875" Y="-1.475879516602" />
                  <Point X="21.93636328125" Y="-1.444576293945" />
                  <Point X="21.94535546875" Y="-1.420709960938" />
                  <Point X="21.948421875" Y="-1.411034179688" />
                  <Point X="21.95384765625" Y="-1.390085449219" />
                  <Point X="21.957962890625" Y="-1.358609619141" />
                  <Point X="21.957265625" Y="-1.335736572266" />
                  <Point X="21.9511171875" Y="-1.313694213867" />
                  <Point X="21.939111328125" Y="-1.284711303711" />
                  <Point X="21.926197265625" Y="-1.262571411133" />
                  <Point X="21.9078359375" Y="-1.244689453125" />
                  <Point X="21.887501953125" Y="-1.229649902344" />
                  <Point X="21.8791953125" Y="-1.224155395508" />
                  <Point X="21.860544921875" Y="-1.213179199219" />
                  <Point X="21.83128125" Y="-1.201955810547" />
                  <Point X="21.79939453125" Y="-1.195474609375" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="20.621775390625" Y="-1.34529675293" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.25958984375" Y="-1.359352783203" />
                  <Point X="20.165921875" Y="-0.992649658203" />
                  <Point X="20.1440625" Y="-0.839807067871" />
                  <Point X="20.107576171875" Y="-0.584698364258" />
                  <Point X="21.028263671875" Y="-0.33800088501" />
                  <Point X="21.467125" Y="-0.220408279419" />
                  <Point X="21.487888671875" Y="-0.212121124268" />
                  <Point X="21.5116640625" Y="-0.199203918457" />
                  <Point X="21.520478515625" Y="-0.193773162842" />
                  <Point X="21.5400234375" Y="-0.180208267212" />
                  <Point X="21.563978515625" Y="-0.158682266235" />
                  <Point X="21.58490625" Y="-0.128276382446" />
                  <Point X="21.595068359375" Y="-0.104720947266" />
                  <Point X="21.598572265625" Y="-0.095244522095" />
                  <Point X="21.6050859375" Y="-0.074253890991" />
                  <Point X="21.61052734375" Y="-0.045516941071" />
                  <Point X="21.609556640625" Y="-0.011683779716" />
                  <Point X="21.60457421875" Y="0.011950889587" />
                  <Point X="21.602349609375" Y="0.020509588242" />
                  <Point X="21.5958359375" Y="0.041500526428" />
                  <Point X="21.582517578125" Y="0.070828399658" />
                  <Point X="21.559603515625" Y="0.100128334045" />
                  <Point X="21.53957421875" Y="0.117344398499" />
                  <Point X="21.531814453125" Y="0.123345733643" />
                  <Point X="21.51226953125" Y="0.136910629272" />
                  <Point X="21.498076171875" Y="0.145046905518" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="20.422220703125" Y="0.437829559326" />
                  <Point X="20.10818359375" Y="0.521975524902" />
                  <Point X="20.115146484375" Y="0.569027893066" />
                  <Point X="20.17551171875" Y="0.976968017578" />
                  <Point X="20.21951953125" Y="1.139370483398" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.919748046875" Y="1.341209106445" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228149414" />
                  <Point X="21.296865234375" Y="1.305263671875" />
                  <Point X="21.315033203125" Y="1.3109921875" />
                  <Point X="21.358291015625" Y="1.324631225586" />
                  <Point X="21.37722265625" Y="1.332961914062" />
                  <Point X="21.395966796875" Y="1.343783935547" />
                  <Point X="21.4126484375" Y="1.356015380859" />
                  <Point X="21.426287109375" Y="1.371567382813" />
                  <Point X="21.438701171875" Y="1.38929699707" />
                  <Point X="21.448650390625" Y="1.407435424805" />
                  <Point X="21.455939453125" Y="1.42503527832" />
                  <Point X="21.473296875" Y="1.466939697266" />
                  <Point X="21.479083984375" Y="1.486788452148" />
                  <Point X="21.48284375" Y="1.508104614258" />
                  <Point X="21.484197265625" Y="1.528750488281" />
                  <Point X="21.481048828125" Y="1.549199829102" />
                  <Point X="21.4754453125" Y="1.570107055664" />
                  <Point X="21.467953125" Y="1.589373901367" />
                  <Point X="21.459158203125" Y="1.606271484375" />
                  <Point X="21.438212890625" Y="1.646503662109" />
                  <Point X="21.42671484375" Y="1.663710449219" />
                  <Point X="21.412802734375" Y="1.6802890625" />
                  <Point X="21.39786328125" Y="1.694590332031" />
                  <Point X="20.79850390625" Y="2.154495117188" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.6842890625" Y="2.331803955078" />
                  <Point X="20.918853515625" Y="2.733665771484" />
                  <Point X="21.0354140625" Y="2.883490966797" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.6001328125" Y="2.956221191406" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.853205078125" Y="2.825796386719" />
                  <Point X="21.8785078125" Y="2.823582519531" />
                  <Point X="21.93875390625" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.019537109375" Y="2.835653076172" />
                  <Point X="22.037791015625" Y="2.847281982422" />
                  <Point X="22.053921875" Y="2.860228271484" />
                  <Point X="22.0718828125" Y="2.878188720703" />
                  <Point X="22.114646484375" Y="2.920951660156" />
                  <Point X="22.127595703125" Y="2.937084960938" />
                  <Point X="22.139224609375" Y="2.955339111328" />
                  <Point X="22.14837109375" Y="2.973887939453" />
                  <Point X="22.1532890625" Y="2.993977050781" />
                  <Point X="22.156115234375" Y="3.015435302734" />
                  <Point X="22.15656640625" Y="3.036123779297" />
                  <Point X="22.1543515625" Y="3.061427246094" />
                  <Point X="22.14908203125" Y="3.121673339844" />
                  <Point X="22.145044921875" Y="3.1419609375" />
                  <Point X="22.13853515625" Y="3.162604003906" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.864609375" Y="3.641555908203" />
                  <Point X="21.81666796875" Y="3.724595947266" />
                  <Point X="21.8908515625" Y="3.781473144531" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.482962890625" Y="4.196680664062" />
                  <Point X="22.832962890625" Y="4.391133789062" />
                  <Point X="22.89765625" Y="4.306823242188" />
                  <Point X="22.9568046875" Y="4.229740722656" />
                  <Point X="22.971109375" Y="4.214797851563" />
                  <Point X="22.987689453125" Y="4.200885742188" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.033048828125" Y="4.174734375" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.22255078125" Y="4.134482421875" />
                  <Point X="23.2518828125" Y="4.1466328125" />
                  <Point X="23.321724609375" Y="4.175562011719" />
                  <Point X="23.339853515625" Y="4.185508789062" />
                  <Point X="23.357583984375" Y="4.197922851562" />
                  <Point X="23.373140625" Y="4.211563964844" />
                  <Point X="23.385373046875" Y="4.228249511719" />
                  <Point X="23.396193359375" Y="4.246993652344" />
                  <Point X="23.404521484375" Y="4.265922851563" />
                  <Point X="23.414068359375" Y="4.296203613281" />
                  <Point X="23.43680078125" Y="4.368299316406" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410145507812" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.41580078125" Y="4.631897949219" />
                  <Point X="23.5215078125" Y="4.66153515625" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.272923828125" Y="4.83585546875" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.808939453125" Y="4.499628417969" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183885742188" />
                  <Point X="25.067130859375" Y="4.194217773438" />
                  <Point X="25.094423828125" Y="4.208806640625" />
                  <Point X="25.11558203125" Y="4.23139453125" />
                  <Point X="25.13344140625" Y="4.25812109375" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.2823046875" Y="4.794200683594" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.382267578125" Y="4.880099121094" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.02817578125" Y="4.787283203125" />
                  <Point X="26.4810234375" Y="4.677952148438" />
                  <Point X="26.5998359375" Y="4.634857910156" />
                  <Point X="26.894650390625" Y="4.527926269531" />
                  <Point X="27.01054296875" Y="4.473727539062" />
                  <Point X="27.294572265625" Y="4.340895996094" />
                  <Point X="27.406568359375" Y="4.275647949219" />
                  <Point X="27.6809765625" Y="4.115775390625" />
                  <Point X="27.786576171875" Y="4.040680664062" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.402431640625" Y="2.992508300781" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142078125" Y="2.539933105469" />
                  <Point X="27.133080078125" Y="2.516061279297" />
                  <Point X="27.126728515625" Y="2.492314453125" />
                  <Point X="27.111609375" Y="2.435774902344" />
                  <Point X="27.108619140625" Y="2.417935546875" />
                  <Point X="27.107728515625" Y="2.380951416016" />
                  <Point X="27.110205078125" Y="2.360417236328" />
                  <Point X="27.116099609375" Y="2.311526611328" />
                  <Point X="27.12144140625" Y="2.289608642578" />
                  <Point X="27.12970703125" Y="2.267519042969" />
                  <Point X="27.140072265625" Y="2.247467529297" />
                  <Point X="27.152779296875" Y="2.228742431641" />
                  <Point X="27.18303125" Y="2.184158935547" />
                  <Point X="27.194466796875" Y="2.170326660156" />
                  <Point X="27.221599609375" Y="2.145593017578" />
                  <Point X="27.24032421875" Y="2.132887207031" />
                  <Point X="27.284908203125" Y="2.102635498047" />
                  <Point X="27.304953125" Y="2.092272216797" />
                  <Point X="27.327041015625" Y="2.084006347656" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.369498046875" Y="2.0761875" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.473205078125" Y="2.074170898438" />
                  <Point X="27.496953125" Y="2.080521240234" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.6395078125" Y="2.716924316406" />
                  <Point X="28.967328125" Y="2.906191650391" />
                  <Point X="29.1232734375" Y="2.689463378906" />
                  <Point X="29.182140625" Y="2.592181884766" />
                  <Point X="29.26219921875" Y="2.459883544922" />
                  <Point X="28.566318359375" Y="1.92591394043" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.2214296875" Y="1.660244873047" />
                  <Point X="28.20397265625" Y="1.641626342773" />
                  <Point X="28.1868828125" Y="1.619330444336" />
                  <Point X="28.14619140625" Y="1.566244995117" />
                  <Point X="28.13660546875" Y="1.550909423828" />
                  <Point X="28.1216328125" Y="1.51708996582" />
                  <Point X="28.115265625" Y="1.494325683594" />
                  <Point X="28.100107421875" Y="1.440125366211" />
                  <Point X="28.09665234375" Y="1.417824584961" />
                  <Point X="28.0958359375" Y="1.394253417969" />
                  <Point X="28.097740234375" Y="1.371766235352" />
                  <Point X="28.102966796875" Y="1.34643762207" />
                  <Point X="28.11541015625" Y="1.286133178711" />
                  <Point X="28.1206796875" Y="1.268978271484" />
                  <Point X="28.136283203125" Y="1.235739990234" />
                  <Point X="28.150498046875" Y="1.214134765625" />
                  <Point X="28.18433984375" Y="1.162694580078" />
                  <Point X="28.198892578125" Y="1.145451416016" />
                  <Point X="28.21613671875" Y="1.129361206055" />
                  <Point X="28.23434765625" Y="1.116034179688" />
                  <Point X="28.254947265625" Y="1.104438964844" />
                  <Point X="28.303990234375" Y="1.076831542969" />
                  <Point X="28.320521484375" Y="1.069501342773" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.383970703125" Y="1.0557578125" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.48655859375" Y="1.178420166016" />
                  <Point X="29.77683984375" Y="1.216636230469" />
                  <Point X="29.84594140625" Y="0.932788269043" />
                  <Point X="29.86448828125" Y="0.813658691406" />
                  <Point X="29.890865234375" Y="0.644238708496" />
                  <Point X="29.101107421875" Y="0.432623443604" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.704791015625" Y="0.32558605957" />
                  <Point X="28.681544921875" Y="0.315067443848" />
                  <Point X="28.65418359375" Y="0.299251647949" />
                  <Point X="28.58903515625" Y="0.26159487915" />
                  <Point X="28.5743125" Y="0.251096298218" />
                  <Point X="28.547533203125" Y="0.225577758789" />
                  <Point X="28.531115234375" Y="0.204658309937" />
                  <Point X="28.49202734375" Y="0.154850128174" />
                  <Point X="28.48030078125" Y="0.135567443848" />
                  <Point X="28.47052734375" Y="0.11410382843" />
                  <Point X="28.463681640625" Y="0.092604545593" />
                  <Point X="28.458208984375" Y="0.064029403687" />
                  <Point X="28.4451796875" Y="-0.004006072044" />
                  <Point X="28.443484375" Y="-0.021875591278" />
                  <Point X="28.4451796875" Y="-0.058553909302" />
                  <Point X="28.45065234375" Y="-0.087129051208" />
                  <Point X="28.463681640625" Y="-0.155164520264" />
                  <Point X="28.47052734375" Y="-0.176663955688" />
                  <Point X="28.48030078125" Y="-0.198127578735" />
                  <Point X="28.49202734375" Y="-0.217410263062" />
                  <Point X="28.5084453125" Y="-0.238329711914" />
                  <Point X="28.547533203125" Y="-0.288137878418" />
                  <Point X="28.56" Y="-0.301235565186" />
                  <Point X="28.58903515625" Y="-0.324154998779" />
                  <Point X="28.616396484375" Y="-0.339970794678" />
                  <Point X="28.681544921875" Y="-0.377627563477" />
                  <Point X="28.6927109375" Y="-0.383138763428" />
                  <Point X="28.716580078125" Y="-0.392149810791" />
                  <Point X="29.632115234375" Y="-0.63746697998" />
                  <Point X="29.891474609375" Y="-0.706961853027" />
                  <Point X="29.855025390625" Y="-0.948724182129" />
                  <Point X="29.8312578125" Y="-1.052877197266" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="28.870326171875" Y="-1.062150268555" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.32095703125" Y="-1.017181335449" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.1639765625" Y="-1.056597045898" />
                  <Point X="28.136150390625" Y="-1.073489013672" />
                  <Point X="28.1123984375" Y="-1.093960693359" />
                  <Point X="28.079939453125" Y="-1.132999633789" />
                  <Point X="28.002654296875" Y="-1.225948608398" />
                  <Point X="27.987935546875" Y="-1.250329589844" />
                  <Point X="27.976591796875" Y="-1.277715454102" />
                  <Point X="27.969759765625" Y="-1.305365478516" />
                  <Point X="27.965107421875" Y="-1.355922485352" />
                  <Point X="27.95403125" Y="-1.476295898438" />
                  <Point X="27.95634765625" Y="-1.507561767578" />
                  <Point X="27.964078125" Y="-1.539182739258" />
                  <Point X="27.976451171875" Y="-1.56799597168" />
                  <Point X="28.006169921875" Y="-1.614222900391" />
                  <Point X="28.076931640625" Y="-1.724286376953" />
                  <Point X="28.0869375" Y="-1.737242919922" />
                  <Point X="28.110630859375" Y="-1.760909301758" />
                  <Point X="28.960251953125" Y="-2.41284765625" />
                  <Point X="29.213123046875" Y="-2.606883300781" />
                  <Point X="29.12480859375" Y="-2.749788574219" />
                  <Point X="29.075658203125" Y="-2.819625488281" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="28.198126953125" Y="-2.406250732422" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.690310546875" Y="-2.148282470703" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135176513672" />
                  <Point X="27.391736328125" Y="-2.16312109375" />
                  <Point X="27.26531640625" Y="-2.229655517578" />
                  <Point X="27.242384765625" Y="-2.246549804688" />
                  <Point X="27.22142578125" Y="-2.267509521484" />
                  <Point X="27.204533203125" Y="-2.290438476562" />
                  <Point X="27.176587890625" Y="-2.343535644531" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531908691406" />
                  <Point X="27.09567578125" Y="-2.563259033203" />
                  <Point X="27.10721875" Y="-2.627173339844" />
                  <Point X="27.134703125" Y="-2.779349365234" />
                  <Point X="27.13898828125" Y="-2.795140869141" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.6977890625" Y="-3.77172265625" />
                  <Point X="27.86128515625" Y="-4.054905029297" />
                  <Point X="27.781849609375" Y="-4.111645019531" />
                  <Point X="27.726896484375" Y="-4.147215332031" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="27.061626953125" Y="-3.329235351562" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.658888671875" Y="-2.860030273438" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.348158203125" Y="-2.747460693359" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.05136328125" Y="-2.839724121094" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968860839844" />
                  <Point X="25.88725" Y="-2.996687255859" />
                  <Point X="25.875625" Y="-3.025809326172" />
                  <Point X="25.859708984375" Y="-3.099039794922" />
                  <Point X="25.821810546875" Y="-3.273397216797" />
                  <Point X="25.819724609375" Y="-3.289627197266" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.974466796875" Y="-4.498358886719" />
                  <Point X="26.02206640625" Y="-4.859916015625" />
                  <Point X="25.975693359375" Y="-4.870080566406" />
                  <Point X="25.92931640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94156640625" Y="-4.752635253906" />
                  <Point X="23.86109765625" Y="-4.731931152344" />
                  <Point X="23.858755859375" Y="-4.731328613281" />
                  <Point X="23.864962890625" Y="-4.684171875" />
                  <Point X="23.8792265625" Y="-4.575837890625" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030761719" />
                  <Point X="23.8782421875" Y="-4.509323730469" />
                  <Point X="23.876666015625" Y="-4.4976875" />
                  <Point X="23.85876171875" Y="-4.407685546875" />
                  <Point X="23.81613671875" Y="-4.193395996094" />
                  <Point X="23.811873046875" Y="-4.178466308594" />
                  <Point X="23.800970703125" Y="-4.149500488281" />
                  <Point X="23.79433203125" Y="-4.135464355469" />
                  <Point X="23.778259765625" Y="-4.107626953125" />
                  <Point X="23.76942578125" Y="-4.094861572266" />
                  <Point X="23.74979296875" Y="-4.070938232422" />
                  <Point X="23.738994140625" Y="-4.059780273438" />
                  <Point X="23.670001953125" Y="-3.999274902344" />
                  <Point X="23.505734375" Y="-3.855215332031" />
                  <Point X="23.493263671875" Y="-3.845965820312" />
                  <Point X="23.46698046875" Y="-3.829621582031" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.2716171875" Y="-3.79016796875" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.808270019531" />
                  <Point X="22.9045625" Y="-3.8158125" />
                  <Point X="22.828263671875" Y="-3.866794677734" />
                  <Point X="22.64659765625" Y="-3.988180664062" />
                  <Point X="22.640322265625" Y="-3.992754882812" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032771484375" />
                  <Point X="22.589529296875" Y="-4.04162890625" />
                  <Point X="22.496796875" Y="-4.162478515625" />
                  <Point X="22.252412109375" Y="-4.011160644531" />
                  <Point X="22.140994140625" Y="-3.925373046875" />
                  <Point X="22.01913671875" Y="-3.831546386719" />
                  <Point X="22.43832421875" Y="-3.105492431641" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.710085693359" />
                  <Point X="22.67605078125" Y="-2.68112109375" />
                  <Point X="22.680314453125" Y="-2.666190429688" />
                  <Point X="22.6865859375" Y="-2.634664550781" />
                  <Point X="22.688361328125" Y="-2.619240478516" />
                  <Point X="22.689375" Y="-2.588306152344" />
                  <Point X="22.6853359375" Y="-2.557618652344" />
                  <Point X="22.6763515625" Y="-2.528000244141" />
                  <Point X="22.67064453125" Y="-2.513559082031" />
                  <Point X="22.656427734375" Y="-2.484730224609" />
                  <Point X="22.648447265625" Y="-2.471414550781" />
                  <Point X="22.630421875" Y="-2.446258544922" />
                  <Point X="22.620376953125" Y="-2.434418212891" />
                  <Point X="22.603029296875" Y="-2.417069580078" />
                  <Point X="22.5911875" Y="-2.407022705078" />
                  <Point X="22.566029296875" Y="-2.388994384766" />
                  <Point X="22.552712890625" Y="-2.381012939453" />
                  <Point X="22.523884765625" Y="-2.366795410156" />
                  <Point X="22.5094453125" Y="-2.361088623047" />
                  <Point X="22.479828125" Y="-2.352103271484" />
                  <Point X="22.449140625" Y="-2.3480625" />
                  <Point X="22.418205078125" Y="-2.349074707031" />
                  <Point X="22.402779296875" Y="-2.350849121094" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.405291015625" Y="-2.903174072266" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="20.995982421875" Y="-2.740589111328" />
                  <Point X="20.91610546875" Y="-2.606649658203" />
                  <Point X="20.818734375" Y="-2.443373535156" />
                  <Point X="21.566625" Y="-1.869497680664" />
                  <Point X="21.951876953125" Y="-1.573882080078" />
                  <Point X="21.96083984375" Y="-1.56606628418" />
                  <Point X="21.98373046875" Y="-1.543432373047" />
                  <Point X="21.997654296875" Y="-1.525975585938" />
                  <Point X="22.01708203125" Y="-1.494672363281" />
                  <Point X="22.02526171875" Y="-1.478071166992" />
                  <Point X="22.03425390625" Y="-1.454204833984" />
                  <Point X="22.04038671875" Y="-1.434853515625" />
                  <Point X="22.0458125" Y="-1.413904785156" />
                  <Point X="22.048046875" Y="-1.402401245117" />
                  <Point X="22.052162109375" Y="-1.370925415039" />
                  <Point X="22.05291796875" Y="-1.35571496582" />
                  <Point X="22.052220703125" Y="-1.332841918945" />
                  <Point X="22.048771484375" Y="-1.310211914062" />
                  <Point X="22.042623046875" Y="-1.288169555664" />
                  <Point X="22.038884765625" Y="-1.277337524414" />
                  <Point X="22.02687890625" Y="-1.248354492188" />
                  <Point X="22.021171875" Y="-1.236845947266" />
                  <Point X="22.0082578125" Y="-1.214706054688" />
                  <Point X="21.992478515625" Y="-1.194513671875" />
                  <Point X="21.9741171875" Y="-1.176631835938" />
                  <Point X="21.964328125" Y="-1.168310791016" />
                  <Point X="21.943994140625" Y="-1.153271240234" />
                  <Point X="21.92737890625" Y="-1.142282104492" />
                  <Point X="21.908728515625" Y="-1.131305786133" />
                  <Point X="21.894564453125" Y="-1.124479003906" />
                  <Point X="21.86530078125" Y="-1.113255615234" />
                  <Point X="21.850203125" Y="-1.10885949707" />
                  <Point X="21.81831640625" Y="-1.102378173828" />
                  <Point X="21.802701171875" Y="-1.100532104492" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="20.609375" Y="-1.25110949707" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.259236328125" Y="-0.974108032227" />
                  <Point X="20.23810546875" Y="-0.826357177734" />
                  <Point X="20.213548828125" Y="-0.654654541016" />
                  <Point X="21.0528515625" Y="-0.429763793945" />
                  <Point X="21.491712890625" Y="-0.312171295166" />
                  <Point X="21.50233984375" Y="-0.308640441895" />
                  <Point X="21.523103515625" Y="-0.300353240967" />
                  <Point X="21.533240234375" Y="-0.295596710205" />
                  <Point X="21.557015625" Y="-0.282679443359" />
                  <Point X="21.57464453125" Y="-0.271818237305" />
                  <Point X="21.594189453125" Y="-0.258253265381" />
                  <Point X="21.60351953125" Y="-0.250870437622" />
                  <Point X="21.627474609375" Y="-0.229344482422" />
                  <Point X="21.642234375" Y="-0.21254385376" />
                  <Point X="21.663162109375" Y="-0.182137985229" />
                  <Point X="21.672134765625" Y="-0.165907913208" />
                  <Point X="21.682296875" Y="-0.142352523804" />
                  <Point X="21.6893046875" Y="-0.123399841309" />
                  <Point X="21.695818359375" Y="-0.102409233093" />
                  <Point X="21.698427734375" Y="-0.091928344727" />
                  <Point X="21.703869140625" Y="-0.063191364288" />
                  <Point X="21.70548828125" Y="-0.042792423248" />
                  <Point X="21.704517578125" Y="-0.008959312439" />
                  <Point X="21.702513671875" Y="0.007912508011" />
                  <Point X="21.69753125" Y="0.031547117233" />
                  <Point X="21.69308203125" Y="0.048664463043" />
                  <Point X="21.686568359375" Y="0.069655517578" />
                  <Point X="21.682333984375" Y="0.080781272888" />
                  <Point X="21.669015625" Y="0.110109184265" />
                  <Point X="21.6573515625" Y="0.129351837158" />
                  <Point X="21.6344375" Y="0.158651794434" />
                  <Point X="21.621529296875" Y="0.172172180176" />
                  <Point X="21.6015" Y="0.189388198853" />
                  <Point X="21.58598046875" Y="0.201390838623" />
                  <Point X="21.566435546875" Y="0.214955657959" />
                  <Point X="21.559515625" Y="0.219329177856" />
                  <Point X="21.534384765625" Y="0.23283454895" />
                  <Point X="21.50343359375" Y="0.245635879517" />
                  <Point X="21.491712890625" Y="0.249611251831" />
                  <Point X="20.44680859375" Y="0.529592590332" />
                  <Point X="20.2145546875" Y="0.591824768066" />
                  <Point X="20.26866796875" Y="0.957522644043" />
                  <Point X="20.311212890625" Y="1.1145234375" />
                  <Point X="20.3664140625" Y="1.318236938477" />
                  <Point X="20.90734765625" Y="1.247021972656" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815551758" />
                  <Point X="21.252943359375" Y="1.204364257812" />
                  <Point X="21.263294921875" Y="1.204703369141" />
                  <Point X="21.28485546875" Y="1.20658984375" />
                  <Point X="21.295109375" Y="1.208053344727" />
                  <Point X="21.3153984375" Y="1.212088867188" />
                  <Point X="21.3436015625" Y="1.220389282227" />
                  <Point X="21.386859375" Y="1.234028198242" />
                  <Point X="21.3965546875" Y="1.237677490234" />
                  <Point X="21.415486328125" Y="1.246008300781" />
                  <Point X="21.42472265625" Y="1.250689575195" />
                  <Point X="21.443466796875" Y="1.26151171875" />
                  <Point X="21.452140625" Y="1.267171630859" />
                  <Point X="21.468822265625" Y="1.279403076172" />
                  <Point X="21.48407421875" Y="1.293377685547" />
                  <Point X="21.497712890625" Y="1.3089296875" />
                  <Point X="21.504107421875" Y="1.317078613281" />
                  <Point X="21.516521484375" Y="1.334808227539" />
                  <Point X="21.521994140625" Y="1.34360949707" />
                  <Point X="21.531943359375" Y="1.361748046875" />
                  <Point X="21.543708984375" Y="1.388684814453" />
                  <Point X="21.56106640625" Y="1.430589233398" />
                  <Point X="21.5645" Y="1.440348632813" />
                  <Point X="21.570287109375" Y="1.460197387695" />
                  <Point X="21.572640625" Y="1.470286987305" />
                  <Point X="21.576400390625" Y="1.491603149414" />
                  <Point X="21.577640625" Y="1.501889770508" />
                  <Point X="21.578994140625" Y="1.522535766602" />
                  <Point X="21.578091796875" Y="1.543206665039" />
                  <Point X="21.574943359375" Y="1.563656005859" />
                  <Point X="21.572810546875" Y="1.573793579102" />
                  <Point X="21.56720703125" Y="1.594700805664" />
                  <Point X="21.563986328125" Y="1.604537597656" />
                  <Point X="21.556494140625" Y="1.623804443359" />
                  <Point X="21.543427734375" Y="1.650132080078" />
                  <Point X="21.522482421875" Y="1.690364379883" />
                  <Point X="21.517201171875" Y="1.699285522461" />
                  <Point X="21.505703125" Y="1.716492431641" />
                  <Point X="21.499486328125" Y="1.724777832031" />
                  <Point X="21.48557421875" Y="1.741356445312" />
                  <Point X="21.47849609375" Y="1.748913818359" />
                  <Point X="21.463556640625" Y="1.763215209961" />
                  <Point X="21.4556953125" Y="1.769958862305" />
                  <Point X="20.8563359375" Y="2.229863769531" />
                  <Point X="20.77238671875" Y="2.294280517578" />
                  <Point X="20.99771875" Y="2.680325927734" />
                  <Point X="21.11039453125" Y="2.825157226562" />
                  <Point X="21.273662109375" Y="3.035013427734" />
                  <Point X="21.5526328125" Y="2.873948730469" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.814384765625" Y="2.736657226562" />
                  <Point X="21.834669921875" Y="2.732622070312" />
                  <Point X="21.844923828125" Y="2.731157958984" />
                  <Point X="21.8702265625" Y="2.728944091797" />
                  <Point X="21.93047265625" Y="2.723673339844" />
                  <Point X="21.940826171875" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.02356640625" Y="2.734227294922" />
                  <Point X="22.04300390625" Y="2.741302001953" />
                  <Point X="22.0615546875" Y="2.750450439453" />
                  <Point X="22.070580078125" Y="2.755530761719" />
                  <Point X="22.088833984375" Y="2.767159667969" />
                  <Point X="22.09725390625" Y="2.773192626953" />
                  <Point X="22.113384765625" Y="2.786138916016" />
                  <Point X="22.121095703125" Y="2.793052246094" />
                  <Point X="22.139056640625" Y="2.811012695312" />
                  <Point X="22.1818203125" Y="2.853775634766" />
                  <Point X="22.188734375" Y="2.861486572266" />
                  <Point X="22.20168359375" Y="2.877619873047" />
                  <Point X="22.20771875" Y="2.886042236328" />
                  <Point X="22.21934765625" Y="2.904296386719" />
                  <Point X="22.2244296875" Y="2.913324707031" />
                  <Point X="22.233576171875" Y="2.931873535156" />
                  <Point X="22.240646484375" Y="2.951298339844" />
                  <Point X="22.245564453125" Y="2.971387451172" />
                  <Point X="22.2474765625" Y="2.981572265625" />
                  <Point X="22.250302734375" Y="3.003030517578" />
                  <Point X="22.251091796875" Y="3.013364013672" />
                  <Point X="22.25154296875" Y="3.034052490234" />
                  <Point X="22.251205078125" Y="3.044407470703" />
                  <Point X="22.248990234375" Y="3.0697109375" />
                  <Point X="22.243720703125" Y="3.12995703125" />
                  <Point X="22.242255859375" Y="3.140214111328" />
                  <Point X="22.23821875" Y="3.160501708984" />
                  <Point X="22.235646484375" Y="3.170532226562" />
                  <Point X="22.22913671875" Y="3.191175292969" />
                  <Point X="22.22548828125" Y="3.200869384766" />
                  <Point X="22.217158203125" Y="3.219798339844" />
                  <Point X="22.2124765625" Y="3.229033203125" />
                  <Point X="21.946880859375" Y="3.689056152344" />
                  <Point X="21.948654296875" Y="3.70608203125" />
                  <Point X="22.351630859375" Y="4.015039306641" />
                  <Point X="22.529099609375" Y="4.113636230469" />
                  <Point X="22.807474609375" Y="4.268295898437" />
                  <Point X="22.822287109375" Y="4.248991210937" />
                  <Point X="22.881435546875" Y="4.171908691406" />
                  <Point X="22.8881796875" Y="4.164046875" />
                  <Point X="22.902484375" Y="4.149104003906" />
                  <Point X="22.910044921875" Y="4.142022949219" />
                  <Point X="22.926625" Y="4.128110839844" />
                  <Point X="22.93491015625" Y="4.121895996094" />
                  <Point X="22.952107421875" Y="4.110405273438" />
                  <Point X="22.96101953125" Y="4.105129394531" />
                  <Point X="22.989181640625" Y="4.09046875" />
                  <Point X="23.056236328125" Y="4.0555625" />
                  <Point X="23.065673828125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.0437890625" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.22926953125" Y="4.037488037109" />
                  <Point X="23.2491328125" Y="4.043277099609" />
                  <Point X="23.258908203125" Y="4.046714599609" />
                  <Point X="23.288240234375" Y="4.058864990234" />
                  <Point X="23.35808203125" Y="4.087794189453" />
                  <Point X="23.367421875" Y="4.092274658203" />
                  <Point X="23.38555078125" Y="4.102221679688" />
                  <Point X="23.39433984375" Y="4.1076875" />
                  <Point X="23.4120703125" Y="4.1201015625" />
                  <Point X="23.420216796875" Y="4.126494140625" />
                  <Point X="23.4357734375" Y="4.140135253906" />
                  <Point X="23.4497578125" Y="4.155395507812" />
                  <Point X="23.461990234375" Y="4.172081054688" />
                  <Point X="23.4676484375" Y="4.180754882812" />
                  <Point X="23.47846875" Y="4.199499023438" />
                  <Point X="23.483150390625" Y="4.208736328125" />
                  <Point X="23.491478515625" Y="4.227665527344" />
                  <Point X="23.495125" Y="4.237357421875" />
                  <Point X="23.504671875" Y="4.267638183594" />
                  <Point X="23.527404296875" Y="4.339733886719" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370048339844" />
                  <Point X="23.535474609375" Y="4.380302246094" />
                  <Point X="23.537361328125" Y="4.401864257812" />
                  <Point X="23.53769921875" Y="4.412217285156" />
                  <Point X="23.537248046875" Y="4.4328984375" />
                  <Point X="23.536458984375" Y="4.443226074219" />
                  <Point X="23.520736328125" Y="4.562655761719" />
                  <Point X="23.547154296875" Y="4.5700625" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.283966796875" Y="4.741499511719" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.71717578125" Y="4.475040527344" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.021630859375" Y="4.085113037109" />
                  <Point X="25.05216796875" Y="4.090154541016" />
                  <Point X="25.06723046875" Y="4.093927246094" />
                  <Point X="25.09766796875" Y="4.104259277344" />
                  <Point X="25.1119140625" Y="4.110436035156" />
                  <Point X="25.13920703125" Y="4.125024902344" />
                  <Point X="25.1637578125" Y="4.143861328125" />
                  <Point X="25.184916015625" Y="4.16644921875" />
                  <Point X="25.1945703125" Y="4.178612792969" />
                  <Point X="25.2124296875" Y="4.205339355469" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.24710546875" />
                  <Point X="25.23798046875" Y="4.261727050781" />
                  <Point X="25.374068359375" Y="4.769612792969" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.005880859375" Y="4.694936523438" />
                  <Point X="26.453591796875" Y="4.586845703125" />
                  <Point X="26.567443359375" Y="4.54555078125" />
                  <Point X="26.858259765625" Y="4.440069335938" />
                  <Point X="26.970298828125" Y="4.387672851562" />
                  <Point X="27.25044921875" Y="4.256654785156" />
                  <Point X="27.35874609375" Y="4.1935625" />
                  <Point X="27.629435546875" Y="4.035856445312" />
                  <Point X="27.731521484375" Y="3.963260498047" />
                  <Point X="27.817783203125" Y="3.901916015625" />
                  <Point X="27.32016015625" Y="3.040008300781" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.062380859375" Y="2.593115234375" />
                  <Point X="27.05318359375" Y="2.573440429688" />
                  <Point X="27.044185546875" Y="2.549568603516" />
                  <Point X="27.041306640625" Y="2.540608154297" />
                  <Point X="27.034955078125" Y="2.516861328125" />
                  <Point X="27.0198359375" Y="2.460321777344" />
                  <Point X="27.017916015625" Y="2.451479736328" />
                  <Point X="27.013646484375" Y="2.42022265625" />
                  <Point X="27.012755859375" Y="2.383238525391" />
                  <Point X="27.013412109375" Y="2.369576171875" />
                  <Point X="27.015888671875" Y="2.349041992188" />
                  <Point X="27.021783203125" Y="2.300151367188" />
                  <Point X="27.02380078125" Y="2.289031982422" />
                  <Point X="27.029142578125" Y="2.267114013672" />
                  <Point X="27.032466796875" Y="2.256315429688" />
                  <Point X="27.040732421875" Y="2.234225830078" />
                  <Point X="27.04531640625" Y="2.22389453125" />
                  <Point X="27.055681640625" Y="2.203843017578" />
                  <Point X="27.061462890625" Y="2.194122802734" />
                  <Point X="27.074169921875" Y="2.175397705078" />
                  <Point X="27.104421875" Y="2.130814208984" />
                  <Point X="27.1098125" Y="2.123627197266" />
                  <Point X="27.130466796875" Y="2.100119384766" />
                  <Point X="27.157599609375" Y="2.075385742188" />
                  <Point X="27.1682578125" Y="2.066982421875" />
                  <Point X="27.186982421875" Y="2.054276611328" />
                  <Point X="27.23156640625" Y="2.024025024414" />
                  <Point X="27.241279296875" Y="2.018246459961" />
                  <Point X="27.26132421875" Y="2.007883300781" />
                  <Point X="27.27165625" Y="2.003298339844" />
                  <Point X="27.293744140625" Y="1.995032470703" />
                  <Point X="27.304548828125" Y="1.991707519531" />
                  <Point X="27.32647265625" Y="1.986364868164" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.358125" Y="1.981870727539" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.416044921875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975497314453" />
                  <Point X="27.4843125" Y="1.979822631836" />
                  <Point X="27.49774609375" Y="1.982395507812" />
                  <Point X="27.521494140625" Y="1.988745849609" />
                  <Point X="27.578033203125" Y="2.003865112305" />
                  <Point X="27.583994140625" Y="2.005670654297" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.6870078125" Y="2.634651855469" />
                  <Point X="28.94040625" Y="2.780951660156" />
                  <Point X="29.043958984375" Y="2.637036865234" />
                  <Point X="29.10086328125" Y="2.542999023438" />
                  <Point X="29.136884765625" Y="2.483471435547" />
                  <Point X="28.508486328125" Y="2.001282592773" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.168138671875" Y="1.739868530273" />
                  <Point X="28.152126953125" Y="1.725223510742" />
                  <Point X="28.134669921875" Y="1.706604980469" />
                  <Point X="28.12857421875" Y="1.699419677734" />
                  <Point X="28.111484375" Y="1.677123657227" />
                  <Point X="28.07079296875" Y="1.624038208008" />
                  <Point X="28.065634765625" Y="1.616599487305" />
                  <Point X="28.04973828125" Y="1.589367675781" />
                  <Point X="28.034765625" Y="1.555548217773" />
                  <Point X="28.03014453125" Y="1.542679321289" />
                  <Point X="28.02377734375" Y="1.519915161133" />
                  <Point X="28.008619140625" Y="1.46571484375" />
                  <Point X="28.0062265625" Y="1.454670288086" />
                  <Point X="28.002771484375" Y="1.432369506836" />
                  <Point X="28.001708984375" Y="1.421113037109" />
                  <Point X="28.000892578125" Y="1.397541870117" />
                  <Point X="28.001173828125" Y="1.386237182617" />
                  <Point X="28.003078125" Y="1.36375" />
                  <Point X="28.004701171875" Y="1.352567504883" />
                  <Point X="28.009927734375" Y="1.327238891602" />
                  <Point X="28.02237109375" Y="1.266934448242" />
                  <Point X="28.02459765625" Y="1.258238037109" />
                  <Point X="28.03468359375" Y="1.228608154297" />
                  <Point X="28.050287109375" Y="1.195369750977" />
                  <Point X="28.056919921875" Y="1.183524169922" />
                  <Point X="28.071134765625" Y="1.161918945312" />
                  <Point X="28.1049765625" Y="1.110478759766" />
                  <Point X="28.111740234375" Y="1.101422485352" />
                  <Point X="28.12629296875" Y="1.084179321289" />
                  <Point X="28.13408203125" Y="1.075992431641" />
                  <Point X="28.151326171875" Y="1.05990222168" />
                  <Point X="28.160033203125" Y="1.052697265625" />
                  <Point X="28.178244140625" Y="1.039370361328" />
                  <Point X="28.187748046875" Y="1.033248291016" />
                  <Point X="28.20834765625" Y="1.021652832031" />
                  <Point X="28.257390625" Y="0.994045532227" />
                  <Point X="28.265482421875" Y="0.989986328125" />
                  <Point X="28.2946796875" Y="0.978083557129" />
                  <Point X="28.33027734375" Y="0.968020935059" />
                  <Point X="28.343671875" Y="0.96525769043" />
                  <Point X="28.3715234375" Y="0.961576782227" />
                  <Point X="28.437833984375" Y="0.952812927246" />
                  <Point X="28.444033203125" Y="0.952199707031" />
                  <Point X="28.46571875" Y="0.951222961426" />
                  <Point X="28.49121875" Y="0.952032348633" />
                  <Point X="28.50060546875" Y="0.952797180176" />
                  <Point X="29.498958984375" Y="1.084232910156" />
                  <Point X="29.704703125" Y="1.111319580078" />
                  <Point X="29.752689453125" Y="0.91420489502" />
                  <Point X="29.770619140625" Y="0.799044555664" />
                  <Point X="29.78387109375" Y="0.713921203613" />
                  <Point X="29.07651953125" Y="0.524386474609" />
                  <Point X="28.6919921875" Y="0.421352722168" />
                  <Point X="28.68603125" Y="0.419543701172" />
                  <Point X="28.665626953125" Y="0.412137817383" />
                  <Point X="28.642380859375" Y="0.401619171143" />
                  <Point X="28.634001953125" Y="0.39731552124" />
                  <Point X="28.606640625" Y="0.381499664307" />
                  <Point X="28.5414921875" Y="0.343842926025" />
                  <Point X="28.53387890625" Y="0.338943267822" />
                  <Point X="28.508775390625" Y="0.319870941162" />
                  <Point X="28.48199609375" Y="0.294352386475" />
                  <Point X="28.47280078125" Y="0.284229370117" />
                  <Point X="28.4563828125" Y="0.263309967041" />
                  <Point X="28.417294921875" Y="0.213501831055" />
                  <Point X="28.410859375" Y="0.204212142944" />
                  <Point X="28.3991328125" Y="0.184929519653" />
                  <Point X="28.393841796875" Y="0.174936264038" />
                  <Point X="28.384068359375" Y="0.153472732544" />
                  <Point X="28.380005859375" Y="0.142927352905" />
                  <Point X="28.37316015625" Y="0.121428153992" />
                  <Point X="28.370376953125" Y="0.110474052429" />
                  <Point X="28.364904296875" Y="0.081898918152" />
                  <Point X="28.351875" Y="0.013863368988" />
                  <Point X="28.350603515625" Y="0.004966505051" />
                  <Point X="28.3485859375" Y="-0.02626184082" />
                  <Point X="28.35028125" Y="-0.062940189362" />
                  <Point X="28.351875" Y="-0.076423408508" />
                  <Point X="28.35734765625" Y="-0.104998542786" />
                  <Point X="28.370376953125" Y="-0.173033935547" />
                  <Point X="28.37316015625" Y="-0.18398789978" />
                  <Point X="28.380005859375" Y="-0.205487380981" />
                  <Point X="28.384068359375" Y="-0.216032775879" />
                  <Point X="28.393841796875" Y="-0.237496444702" />
                  <Point X="28.3991328125" Y="-0.247489562988" />
                  <Point X="28.410859375" Y="-0.266772338867" />
                  <Point X="28.417294921875" Y="-0.276061859131" />
                  <Point X="28.433712890625" Y="-0.296981292725" />
                  <Point X="28.47280078125" Y="-0.346789581299" />
                  <Point X="28.478720703125" Y="-0.353635284424" />
                  <Point X="28.501138671875" Y="-0.375803283691" />
                  <Point X="28.530173828125" Y="-0.398722717285" />
                  <Point X="28.5414921875" Y="-0.406402954102" />
                  <Point X="28.568853515625" Y="-0.422218811035" />
                  <Point X="28.634001953125" Y="-0.459875549316" />
                  <Point X="28.639498046875" Y="-0.462816192627" />
                  <Point X="28.659158203125" Y="-0.472016265869" />
                  <Point X="28.68302734375" Y="-0.481027252197" />
                  <Point X="28.6919921875" Y="-0.483912780762" />
                  <Point X="29.60752734375" Y="-0.729229858398" />
                  <Point X="29.784880859375" Y="-0.776751220703" />
                  <Point X="29.761619140625" Y="-0.931036987305" />
                  <Point X="29.738638671875" Y="-1.031741699219" />
                  <Point X="29.7278046875" Y="-1.079219726563" />
                  <Point X="28.8827265625" Y="-0.967963012695" />
                  <Point X="28.436783203125" Y="-0.909253540039" />
                  <Point X="28.428625" Y="-0.908535522461" />
                  <Point X="28.40009765625" Y="-0.908042541504" />
                  <Point X="28.36672265625" Y="-0.910840820312" />
                  <Point X="28.354482421875" Y="-0.912676147461" />
                  <Point X="28.300779296875" Y="-0.924348876953" />
                  <Point X="28.17291796875" Y="-0.952140014648" />
                  <Point X="28.157875" Y="-0.956743164062" />
                  <Point X="28.128755859375" Y="-0.968367553711" />
                  <Point X="28.1146796875" Y="-0.975388916016" />
                  <Point X="28.086853515625" Y="-0.992280944824" />
                  <Point X="28.07412890625" Y="-1.001528869629" />
                  <Point X="28.050376953125" Y="-1.022000488281" />
                  <Point X="28.039349609375" Y="-1.033224365234" />
                  <Point X="28.006890625" Y="-1.072263305664" />
                  <Point X="27.92960546875" Y="-1.165212280273" />
                  <Point X="27.921326171875" Y="-1.176850585938" />
                  <Point X="27.906607421875" Y="-1.201231567383" />
                  <Point X="27.90016796875" Y="-1.213974121094" />
                  <Point X="27.88882421875" Y="-1.241360229492" />
                  <Point X="27.884365234375" Y="-1.254927246094" />
                  <Point X="27.877533203125" Y="-1.282577270508" />
                  <Point X="27.87516015625" Y="-1.29666027832" />
                  <Point X="27.8705078125" Y="-1.347217163086" />
                  <Point X="27.859431640625" Y="-1.467590576172" />
                  <Point X="27.859291015625" Y="-1.483314941406" />
                  <Point X="27.861607421875" Y="-1.514580810547" />
                  <Point X="27.864064453125" Y="-1.530122314453" />
                  <Point X="27.871794921875" Y="-1.561743286133" />
                  <Point X="27.876787109375" Y="-1.57666784668" />
                  <Point X="27.88916015625" Y="-1.605481079102" />
                  <Point X="27.896541015625" Y="-1.619369750977" />
                  <Point X="27.926259765625" Y="-1.665596679688" />
                  <Point X="27.997021484375" Y="-1.77566015625" />
                  <Point X="28.0017421875" Y="-1.782352050781" />
                  <Point X="28.01980078125" Y="-1.804456176758" />
                  <Point X="28.043494140625" Y="-1.828122680664" />
                  <Point X="28.052798828125" Y="-1.836277832031" />
                  <Point X="28.902419921875" Y="-2.488216064453" />
                  <Point X="29.087171875" Y="-2.629981933594" />
                  <Point X="29.0455" Y="-2.697411865234" />
                  <Point X="29.001275390625" Y="-2.760251708984" />
                  <Point X="28.245626953125" Y="-2.323978271484" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.707193359375" Y="-2.054794921875" />
                  <Point X="27.555017578125" Y="-2.027312011719" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.444845703125" Y="-2.035136108398" />
                  <Point X="27.4150703125" Y="-2.044959594727" />
                  <Point X="27.40058984375" Y="-2.051108154297" />
                  <Point X="27.3474921875" Y="-2.079052734375" />
                  <Point X="27.221072265625" Y="-2.145587158203" />
                  <Point X="27.20896875" Y="-2.153170898438" />
                  <Point X="27.186037109375" Y="-2.170065185547" />
                  <Point X="27.175208984375" Y="-2.179375732422" />
                  <Point X="27.15425" Y="-2.200335449219" />
                  <Point X="27.14494140625" Y="-2.211160888672" />
                  <Point X="27.128048828125" Y="-2.23408984375" />
                  <Point X="27.12046484375" Y="-2.246193359375" />
                  <Point X="27.09251953125" Y="-2.299290527344" />
                  <Point X="27.025984375" Y="-2.425711425781" />
                  <Point X="27.0198359375" Y="-2.440193359375" />
                  <Point X="27.01001171875" Y="-2.469972167969" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517442382812" />
                  <Point X="27.000279296875" Y="-2.533133789062" />
                  <Point X="27.00068359375" Y="-2.564484130859" />
                  <Point X="27.0021875" Y="-2.580143066406" />
                  <Point X="27.01373046875" Y="-2.644057373047" />
                  <Point X="27.04121484375" Y="-2.796233398438" />
                  <Point X="27.04301953125" Y="-2.804228759766" />
                  <Point X="27.05123828125" Y="-2.831542236328" />
                  <Point X="27.064072265625" Y="-2.862479980469" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.615517578125" Y="-3.81922265625" />
                  <Point X="27.735896484375" Y="-4.027722900391" />
                  <Point X="27.7237578125" Y="-4.036083007813" />
                  <Point X="27.13699609375" Y="-3.271402832031" />
                  <Point X="26.83391796875" Y="-2.876422607422" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.710263671875" Y="-2.780120117188" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.339453125" Y="-2.652860351562" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.990626953125" Y="-2.766676269531" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883087158203" />
                  <Point X="25.83218359375" Y="-2.906836425781" />
                  <Point X="25.822935546875" Y="-2.919562255859" />
                  <Point X="25.80604296875" Y="-2.947388671875" />
                  <Point X="25.79901953125" Y="-2.961467285156" />
                  <Point X="25.78739453125" Y="-2.990589355469" />
                  <Point X="25.78279296875" Y="-3.0056328125" />
                  <Point X="25.766876953125" Y="-3.07886328125" />
                  <Point X="25.728978515625" Y="-3.253220703125" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677001953" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520263672" />
                  <Point X="25.83308984375" Y="-4.152323242188" />
                  <Point X="25.77355078125" Y="-3.930118408203" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480122314453" />
                  <Point X="25.64214453125" Y="-3.453574707031" />
                  <Point X="25.62678515625" Y="-3.423811035156" />
                  <Point X="25.62040625" Y="-3.413208740234" />
                  <Point X="25.571978515625" Y="-3.343435058594" />
                  <Point X="25.456677734375" Y="-3.177308349609" />
                  <Point X="25.446671875" Y="-3.165173828125" />
                  <Point X="25.4247890625" Y="-3.142718505859" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104937255859" />
                  <Point X="25.3452421875" Y="-3.090829589844" />
                  <Point X="25.33065625" Y="-3.084938476562" />
                  <Point X="25.25571875" Y="-3.061680664062" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.063376953125" Y="-3.003109130859" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.860078125" Y="-3.029562988281" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.6670703125" Y="-3.090829589844" />
                  <Point X="24.6390703125" Y="-3.104937255859" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.587525390625" Y="-3.142717285156" />
                  <Point X="24.565640625" Y="-3.165173828125" />
                  <Point X="24.555630859375" Y="-3.177310791016" />
                  <Point X="24.507205078125" Y="-3.247084228516" />
                  <Point X="24.391904296875" Y="-3.413211181641" />
                  <Point X="24.38753125" Y="-3.420132080078" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.075146484375" Y="-4.540744628906" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.638012428921" Y="4.770482631675" />
                  <Point X="23.532377561748" Y="4.474228661892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.66260020316" Y="4.678719668787" />
                  <Point X="23.534940601235" Y="4.376564189114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.862628129607" Y="4.19641860528" />
                  <Point X="22.506200556507" Y="4.100914124907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.412795264228" Y="4.781382592427" />
                  <Point X="25.374470503804" Y="4.771113503821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.687187977398" Y="4.586956705899" />
                  <Point X="23.505520592397" Y="4.270329884365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.939675265444" Y="4.118712085967" />
                  <Point X="22.243571793655" Y="3.932191722853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.676700572351" Y="4.753744569478" />
                  <Point X="25.346078844629" Y="4.665154744534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.711775751637" Y="4.495193743011" />
                  <Point X="23.451255446986" Y="4.157438345336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.061741300314" Y="4.053068344295" />
                  <Point X="22.046371886368" Y="3.781000929809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.910352629254" Y="4.718000212296" />
                  <Point X="25.317687216949" Y="4.559195993687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.736363304862" Y="4.403430720902" />
                  <Point X="21.963413431054" Y="3.660421041563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.103432631669" Y="4.671384605879" />
                  <Point X="25.289295589269" Y="4.453237242839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.760950795888" Y="4.311667682127" />
                  <Point X="22.012589262814" Y="3.575246428832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.296513252376" Y="4.624769165132" />
                  <Point X="25.260903961589" Y="4.347278491992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.791202392602" Y="4.221422335898" />
                  <Point X="22.061765094575" Y="3.4900718161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.482670500174" Y="4.576298612206" />
                  <Point X="25.229796653838" Y="4.240592076862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.853793282634" Y="4.139842277197" />
                  <Point X="22.110940926336" Y="3.404897203369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.638620831928" Y="4.51973414052" />
                  <Point X="25.112732803804" Y="4.110873675643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.016592940266" Y="4.085113076848" />
                  <Point X="22.160116758096" Y="3.319722590637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.794571218134" Y="4.463169683424" />
                  <Point X="22.209292589857" Y="3.234547977905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.33419295248" Y="3.000065736774" />
                  <Point X="21.223370411693" Y="2.970370926467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.937357996214" Y="4.403078048161" />
                  <Point X="22.241346185457" Y="3.144785475822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.450543277755" Y="2.932890475331" />
                  <Point X="21.126701423063" Y="2.846117311891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.07105683455" Y="4.340551306783" />
                  <Point X="22.250805807361" Y="3.048968936733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.566893664846" Y="2.865715230452" />
                  <Point X="21.030034770029" Y="2.721864323137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.204755322262" Y="4.278024471456" />
                  <Point X="22.239274061098" Y="2.947527777495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.683244494457" Y="2.798540104146" />
                  <Point X="20.95241825877" Y="2.602715804486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.326563504528" Y="4.212311638387" />
                  <Point X="22.154507662161" Y="2.826463452216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.817232298069" Y="2.736090790781" />
                  <Point X="20.884368304244" Y="2.486130636987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.442195783771" Y="4.14494397709" />
                  <Point X="20.816318349718" Y="2.369545469488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.55782721537" Y="4.077576088668" />
                  <Point X="20.806056605019" Y="2.268444606144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.667680010518" Y="4.008659819275" />
                  <Point X="20.901056441778" Y="2.195548498546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.768132558646" Y="3.937224761285" />
                  <Point X="20.996056398405" Y="2.122652423066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.783811047325" Y="3.843074562526" />
                  <Point X="21.091056355033" Y="2.049756347586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.716635839921" Y="3.726723782812" />
                  <Point X="21.186056311661" Y="1.976860272107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.649460632517" Y="3.610373003097" />
                  <Point X="21.281056268288" Y="1.903964196627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.582285425113" Y="3.494022223383" />
                  <Point X="21.376056224916" Y="1.831068121148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.515110217709" Y="3.377671443669" />
                  <Point X="21.469312789545" Y="1.75770490519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.447935010305" Y="3.261320663955" />
                  <Point X="21.530141478054" Y="1.675652666014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.380759802902" Y="3.14496988424" />
                  <Point X="21.569074402278" Y="1.58773347448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.498434030368" Y="1.300856251442" />
                  <Point X="20.350999499624" Y="1.261351287993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.313584624797" Y="3.028619112377" />
                  <Point X="21.576352641886" Y="1.491332435766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.74455753993" Y="1.268453609929" />
                  <Point X="20.322262283771" Y="1.155299937073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.246409716713" Y="2.912268412865" />
                  <Point X="21.541507419357" Y="1.38364444939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.990680951016" Y="1.236050942029" />
                  <Point X="20.293524303834" Y="1.049248381418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.179234808629" Y="2.795917713353" />
                  <Point X="21.435424412197" Y="1.256868356152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.240490656808" Y="1.204636013818" />
                  <Point X="20.26662087807" Y="0.943688393072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.112059900545" Y="2.679567013842" />
                  <Point X="20.251466736742" Y="0.841276616002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.049829154127" Y="2.564541098456" />
                  <Point X="20.236312595414" Y="0.738864838932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.01933597433" Y="2.458019238415" />
                  <Point X="20.221158454086" Y="0.636453061863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.014755139095" Y="2.358440570175" />
                  <Point X="20.318105005146" Y="0.564078574789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.943353467003" Y="2.776855697523" />
                  <Point X="28.924615555793" Y="2.771834889346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.030043946501" Y="2.264185956633" />
                  <Point X="20.501630975932" Y="0.514902973313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.00268260987" Y="2.694401656303" />
                  <Point X="28.606739798554" Y="2.588309099762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.072871921097" Y="2.177310440701" />
                  <Point X="20.685156858023" Y="0.46572734807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.05954154649" Y="2.611285725313" />
                  <Point X="28.288863555114" Y="2.4047831799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.135322714213" Y="2.095692843144" />
                  <Point X="20.868682740114" Y="0.416551722828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.110752549593" Y="2.5266564351" />
                  <Point X="27.970987311674" Y="2.221257260038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.232603620857" Y="2.023407946379" />
                  <Point X="21.052208622205" Y="0.367376097585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.040437023404" Y="2.409464209503" />
                  <Point X="27.653111068234" Y="2.037731340176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.420280658092" Y="1.975344619805" />
                  <Point X="21.235734504296" Y="0.318200472343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.843490571964" Y="2.258341329748" />
                  <Point X="21.419260386387" Y="0.2690248471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.646544120523" Y="2.107218449994" />
                  <Point X="21.571473668168" Y="0.211459035892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.449596974359" Y="1.956095384088" />
                  <Point X="21.652976347587" Y="0.134946375884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.252648199492" Y="1.804971881773" />
                  <Point X="21.693403530224" Y="0.047427569685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.10345095069" Y="1.666643362305" />
                  <Point X="21.705091454173" Y="-0.047791897671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.032516690009" Y="1.549285347301" />
                  <Point X="21.677468418717" Y="-0.153544704852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.004483974904" Y="1.443422766788" />
                  <Point X="21.551598152946" Y="-0.285622778056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.006155543987" Y="1.345519425235" />
                  <Point X="20.225649109274" Y="-0.739260990652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.026509349998" Y="1.252621973979" />
                  <Point X="20.239195996775" Y="-0.833982350225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.068724539163" Y="1.165582262686" />
                  <Point X="20.252742750813" Y="-0.928703745559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.127447520287" Y="1.082965800916" />
                  <Point X="20.271478522544" Y="-1.022034747793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.226835680952" Y="1.011245541165" />
                  <Point X="20.294991615033" Y="-1.114085670788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.396300214177" Y="0.958302188849" />
                  <Point X="20.318504707522" Y="-1.206136593783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.976755811104" Y="1.015483560149" />
                  <Point X="20.429178487415" Y="-1.274832880976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.698352010107" Y="1.110483441794" />
                  <Point X="21.150775573573" Y="-1.179832761619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.726982109671" Y="1.019803616712" />
                  <Point X="21.81038935324" Y="-1.101441019179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.749458923886" Y="0.927475023791" />
                  <Point X="21.95463524958" Y="-1.161141684883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.765276146396" Y="0.83336199865" />
                  <Point X="22.023281102373" Y="-1.241099321202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.779974744017" Y="0.738949238873" />
                  <Point X="22.05205292932" Y="-1.331741170546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.48013915731" Y="0.292308105983" />
                  <Point X="22.040841812212" Y="-1.43309641746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.391714014845" Y="0.17026342333" />
                  <Point X="21.979127174944" Y="-1.547984041816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.36143818605" Y="0.063799802315" />
                  <Point X="21.792169608254" Y="-1.696430407968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.349121630686" Y="-0.037851645887" />
                  <Point X="21.595223463167" Y="-1.847553205636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.36263033558" Y="-0.132583236459" />
                  <Point X="21.398275747332" Y="-1.998676424184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.387784726682" Y="-0.224194374816" />
                  <Point X="21.201327764663" Y="-2.14979971423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.442309565572" Y="-0.307935725407" />
                  <Point X="21.004379781995" Y="-2.300923004276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.515061137583" Y="-0.386793237577" />
                  <Point X="20.821636608255" Y="-2.448240127241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.626198816277" Y="-0.455365223462" />
                  <Point X="20.872208177192" Y="-2.533040753323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.789350907639" Y="-0.509999989477" />
                  <Point X="20.922779820337" Y="-2.617841359521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.972876750342" Y="-0.559175625273" />
                  <Point X="20.973351951564" Y="-2.702641834939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.156402593045" Y="-0.60835126107" />
                  <Point X="22.548497647435" Y="-2.378934054907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.109616815525" Y="-2.496531819391" />
                  <Point X="21.030336914248" Y="-2.785723997346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.339928435748" Y="-0.657526896866" />
                  <Point X="28.404694833536" Y="-0.908121985313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.084660165828" Y="-0.993875016076" />
                  <Point X="22.635877250739" Y="-2.453871997906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.791739982226" Y="-2.680057897305" />
                  <Point X="21.092515748275" Y="-2.867414466121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.52345427845" Y="-0.706702532663" />
                  <Point X="28.65855822029" Y="-0.938450732984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.963240171544" Y="-1.124760642628" />
                  <Point X="22.68010455196" Y="-2.5403725654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.473863148927" Y="-2.863583975219" />
                  <Point X="21.154694582301" Y="-2.949104934896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.706980542095" Y="-0.755878055668" />
                  <Point X="28.904681498164" Y="-0.970853436578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.888214653904" Y="-1.24321490663" />
                  <Point X="22.686097891087" Y="-2.63711789216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.775987251228" Y="-0.835739000823" />
                  <Point X="29.150804624705" Y="-1.003256180721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.870593337889" Y="-1.346287761165" />
                  <Point X="22.645708945182" Y="-2.746291314737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.759941266235" Y="-0.938389746682" />
                  <Point X="29.396927751247" Y="-1.035658924865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.861314765752" Y="-1.447125184215" />
                  <Point X="22.578533852741" Y="-2.862642063647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.736036259933" Y="-1.043146310955" />
                  <Point X="29.643050877788" Y="-1.068061669009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.867418295588" Y="-1.543840985464" />
                  <Point X="22.5113587603" Y="-2.978992812557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.904777727149" Y="-1.632181793086" />
                  <Point X="22.44418366786" Y="-3.095343561467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.958716486042" Y="-1.716080183339" />
                  <Point X="22.377008518889" Y="-3.211694325524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.015516354362" Y="-1.799211941631" />
                  <Point X="22.309833364517" Y="-3.328045091028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.102346454605" Y="-1.874297123531" />
                  <Point X="27.537014883312" Y="-2.025777261515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.357142437083" Y="-2.073973938223" />
                  <Point X="22.242658210145" Y="-3.444395856533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.197346342536" Y="-1.947193217418" />
                  <Point X="27.760107332552" Y="-2.064351057043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.128396932059" Y="-2.233617348705" />
                  <Point X="22.175483055773" Y="-3.560746622037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.292346230467" Y="-2.020089311305" />
                  <Point X="27.900932207769" Y="-2.124968382593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.066607481498" Y="-2.348525019223" />
                  <Point X="22.108307901401" Y="-3.677097387541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.387346118398" Y="-2.092985405192" />
                  <Point X="28.017282856796" Y="-2.192143557286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.012881971714" Y="-2.461271963321" />
                  <Point X="26.276110441048" Y="-2.658689299969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.047226950017" Y="-2.720018446552" />
                  <Point X="25.010656723809" Y="-2.997766601562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.674421213015" Y="-3.087860635146" />
                  <Point X="22.041132747029" Y="-3.793448153046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.48234600633" Y="-2.165881499078" />
                  <Point X="28.133633505822" Y="-2.25931873198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.000663128925" Y="-2.562897229518" />
                  <Point X="26.556794801222" Y="-2.681831389472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.871978945115" Y="-2.86532724508" />
                  <Point X="25.201721936559" Y="-3.044922069244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.520903737128" Y="-3.227346755974" />
                  <Point X="22.082866890734" Y="-3.880616760082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.577345894261" Y="-2.238777592965" />
                  <Point X="28.249984161778" Y="-2.326493904817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.016088824615" Y="-2.657115163954" />
                  <Point X="26.665185716952" Y="-2.751139368274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.789292705334" Y="-2.985834193393" />
                  <Point X="25.36336564249" Y="-3.099961005917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.437048377254" Y="-3.348166969072" />
                  <Point X="22.17762521504" Y="-3.953577580747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.672345782192" Y="-2.311673686852" />
                  <Point X="28.366334995838" Y="-2.393669029931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.033032003775" Y="-2.75092648992" />
                  <Point X="26.773162766351" Y="-2.820558242225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.764262317547" Y="-3.090892302726" />
                  <Point X="25.453976963658" Y="-3.174033012724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.365585021021" Y="-3.465666754802" />
                  <Point X="23.177541788999" Y="-3.784001979395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.80128915302" Y="-3.884818569356" />
                  <Point X="22.275778619163" Y="-4.025628692517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.767345670123" Y="-2.384569780739" />
                  <Point X="28.482685829897" Y="-2.460844155045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.056038297816" Y="-2.84311320915" />
                  <Point X="26.850598166701" Y="-2.898160726375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.741562561379" Y="-3.195325921198" />
                  <Point X="25.511894490592" Y="-3.256865295293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.334638201756" Y="-3.572310167171" />
                  <Point X="23.432604781174" Y="-3.814009293762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.591480834557" Y="-4.039387775992" />
                  <Point X="22.386643382343" Y="-4.094273805893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.862345558055" Y="-2.457465874625" />
                  <Point X="28.599036663957" Y="-2.528019280159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.101683314933" Y="-2.929233900814" />
                  <Point X="26.913195288336" Y="-2.979739115323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.724729076042" Y="-3.298187677139" />
                  <Point X="25.56945154284" Y="-3.339794166763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.306246587916" Y="-3.67826891431" />
                  <Point X="23.538531289993" Y="-3.883977608406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.957345307059" Y="-2.530362005737" />
                  <Point X="28.715387498017" Y="-2.595194405273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.15085885192" Y="-3.01440859253" />
                  <Point X="26.97579240997" Y="-3.061317504272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.733286156335" Y="-3.394246051524" />
                  <Point X="25.626252458026" Y="-3.422925644549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.277854974077" Y="-3.784227661449" />
                  <Point X="23.624432792562" Y="-3.959311607303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.052344954702" Y="-2.603258164009" />
                  <Point X="28.831738332077" Y="-2.662369530387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.200034388906" Y="-3.099583284246" />
                  <Point X="27.038389531604" Y="-3.14289589322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.745793256409" Y="-3.489246021298" />
                  <Point X="25.661473041301" Y="-3.511839554842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.249463360237" Y="-3.890186408588" />
                  <Point X="23.71033402817" Y="-4.034645677732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.04026863516" Y="-2.704845241217" />
                  <Point X="28.948089166136" Y="-2.729544655501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.249209925893" Y="-3.184757975962" />
                  <Point X="27.100986653239" Y="-3.224474282169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.758300356484" Y="-3.584245991073" />
                  <Point X="25.686060903596" Y="-3.603602494136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.221071746397" Y="-3.996145155728" />
                  <Point X="23.781844487742" Y="-4.113835744978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.298385462879" Y="-3.269932667678" />
                  <Point X="27.163583912048" Y="-3.306052634362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.770807456558" Y="-3.679245960847" />
                  <Point X="25.71064876589" Y="-3.695365433429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.192680132557" Y="-4.102103902867" />
                  <Point X="23.817950112658" Y="-4.202512509078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.347560999866" Y="-3.355107359394" />
                  <Point X="27.226181356642" Y="-3.387630936773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.783314556633" Y="-3.774245930622" />
                  <Point X="25.735236628184" Y="-3.787128372723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.164288518718" Y="-4.208062650006" />
                  <Point X="23.836523525637" Y="-4.295887015209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.396736536852" Y="-3.44028205111" />
                  <Point X="27.288778801236" Y="-3.469209239185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.795821656707" Y="-3.869245900396" />
                  <Point X="25.759824490479" Y="-3.878891312017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.135896904878" Y="-4.314021397145" />
                  <Point X="23.855096938617" Y="-4.389261521339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.445912073839" Y="-3.525456742826" />
                  <Point X="27.351376245831" Y="-3.550787541597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.808328756782" Y="-3.96424587017" />
                  <Point X="25.78441223688" Y="-3.970654282364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.107505291038" Y="-4.419980144284" />
                  <Point X="23.873671718492" Y="-4.482635661211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.495087610825" Y="-3.610631434542" />
                  <Point X="27.413973690425" Y="-3.632365844008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.820835856856" Y="-4.059245839945" />
                  <Point X="25.808999836822" Y="-4.062417291954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.079113677198" Y="-4.525938891423" />
                  <Point X="23.878726969561" Y="-4.579632347909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.544263147812" Y="-3.695806126258" />
                  <Point X="27.476571135019" Y="-3.71394414642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.050721584293" Y="-4.631897766927" />
                  <Point X="23.865304116962" Y="-4.681580227562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.593438684798" Y="-3.780980817974" />
                  <Point X="27.539168579613" Y="-3.795522448832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.022329413575" Y="-4.737856663282" />
                  <Point X="23.956416985533" Y="-4.755517845147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.642614507327" Y="-3.86615543318" />
                  <Point X="27.601766024208" Y="-3.877100751243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.691790562521" Y="-3.951329986042" />
                  <Point X="27.664363468802" Y="-3.958679053655" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.5900234375" Y="-3.979293701172" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.415892578125" Y="-3.451770507813" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.1993984375" Y="-3.243142089844" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.9163984375" Y="-3.211024414062" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.663294921875" Y="-3.355417236328" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.258673828125" Y="-4.589920410156" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.109361328125" Y="-4.978750976562" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.81375390625" Y="-4.915937988281" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.676587890625" Y="-4.659373046875" />
                  <Point X="23.6908515625" Y="-4.5510390625" />
                  <Point X="23.690318359375" Y="-4.5347578125" />
                  <Point X="23.6724140625" Y="-4.444755859375" />
                  <Point X="23.6297890625" Y="-4.230466308594" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.544724609375" Y="-4.142123535156" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.25919140625" Y="-3.979761230469" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.933822265625" Y="-4.024773193359" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157293945313" />
                  <Point X="22.581890625" Y="-4.363694335938" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.45141015625" Y="-4.357848144531" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="22.025080078125" Y="-4.07591796875" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.27378125" Y="-3.010492431641" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.59759375" />
                  <Point X="22.486021484375" Y="-2.568764892578" />
                  <Point X="22.468673828125" Y="-2.551416259766" />
                  <Point X="22.439845703125" Y="-2.537198730469" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.500291015625" Y="-3.067718994141" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.08103515625" Y="-3.166037597656" />
                  <Point X="20.838302734375" Y="-2.847135742188" />
                  <Point X="20.752921875" Y="-2.703967529297" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="21.4509609375" Y="-1.718760253906" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.84746484375" Y="-1.411081420898" />
                  <Point X="21.85645703125" Y="-1.387215087891" />
                  <Point X="21.8618828125" Y="-1.366266235352" />
                  <Point X="21.863349609375" Y="-1.350051025391" />
                  <Point X="21.85134375" Y="-1.321068115234" />
                  <Point X="21.831009765625" Y="-1.306028564453" />
                  <Point X="21.812359375" Y="-1.295052246094" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.63417578125" Y="-1.439484008789" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.167544921875" Y="-1.382864257812" />
                  <Point X="20.072607421875" Y="-1.011187866211" />
                  <Point X="20.05001953125" Y="-0.853257446289" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="21.00367578125" Y="-0.246237960815" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.4663125" Y="-0.115728294373" />
                  <Point X="21.485857421875" Y="-0.102163330078" />
                  <Point X="21.497677734375" Y="-0.090644638062" />
                  <Point X="21.50783984375" Y="-0.067089271545" />
                  <Point X="21.514353515625" Y="-0.04609859848" />
                  <Point X="21.516599609375" Y="-0.031280040741" />
                  <Point X="21.5116171875" Y="-0.007645431042" />
                  <Point X="21.505103515625" Y="0.013345396042" />
                  <Point X="21.497677734375" Y="0.028084554672" />
                  <Point X="21.4776484375" Y="0.045300613403" />
                  <Point X="21.458103515625" Y="0.058865428925" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.3976328125" Y="0.346066558838" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.021169921875" Y="0.58293347168" />
                  <Point X="20.08235546875" Y="0.996415039063" />
                  <Point X="20.127826171875" Y="1.164217163086" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.9321484375" Y="1.435396362305" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.28646484375" Y="1.401595092773" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056152344" />
                  <Point X="21.360880859375" Y="1.443785766602" />
                  <Point X="21.368169921875" Y="1.461385742188" />
                  <Point X="21.38552734375" Y="1.503289916992" />
                  <Point X="21.389287109375" Y="1.524606079102" />
                  <Point X="21.38368359375" Y="1.545513305664" />
                  <Point X="21.374888671875" Y="1.562410888672" />
                  <Point X="21.353943359375" Y="1.602643066406" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.740671875" Y="2.079126464844" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.6022421875" Y="2.379693603516" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="20.96043359375" Y="2.941824951172" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.6476328125" Y="3.038493652344" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.8867890625" Y="2.918220947266" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404296875" />
                  <Point X="22.004708984375" Y="2.945364746094" />
                  <Point X="22.04747265625" Y="2.988127685547" />
                  <Point X="22.0591015625" Y="3.006381835938" />
                  <Point X="22.061927734375" Y="3.027840087891" />
                  <Point X="22.059712890625" Y="3.053143554688" />
                  <Point X="22.054443359375" Y="3.113389648438" />
                  <Point X="22.04793359375" Y="3.134032714844" />
                  <Point X="21.782337890625" Y="3.594055664062" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.833048828125" Y="3.856864501953" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.436826171875" Y="4.279725097656" />
                  <Point X="22.858451171875" Y="4.513971679688" />
                  <Point X="22.973025390625" Y="4.364655273438" />
                  <Point X="23.032173828125" Y="4.287572753906" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.076916015625" Y="4.259" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.215525390625" Y="4.234400878906" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.29448828125" />
                  <Point X="23.32346484375" Y="4.324769042969" />
                  <Point X="23.346197265625" Y="4.396864746094" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.317888671875" Y="4.647781738281" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.495861328125" Y="4.7530078125" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.261880859375" Y="4.930211425781" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.900703125" Y="4.524216308594" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176269531" />
                  <Point X="25.054453125" Y="4.310902832031" />
                  <Point X="25.190541015625" Y="4.818788574219" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.392162109375" Y="4.974582519531" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.050470703125" Y="4.879629882812" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.632228515625" Y="4.724165039062" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="27.050787109375" Y="4.559781738281" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.454390625" Y="4.357733398438" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="27.8416328125" Y="4.118100585938" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.484703125" Y="2.945008300781" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491514404297" />
                  <Point X="27.218501953125" Y="2.467767578125" />
                  <Point X="27.2033828125" Y="2.411228027344" />
                  <Point X="27.202044921875" Y="2.392326660156" />
                  <Point X="27.204521484375" Y="2.371792480469" />
                  <Point X="27.210416015625" Y="2.322901855469" />
                  <Point X="27.218681640625" Y="2.300812255859" />
                  <Point X="27.231388671875" Y="2.282087158203" />
                  <Point X="27.261640625" Y="2.237503662109" />
                  <Point X="27.27494140625" Y="2.224203613281" />
                  <Point X="27.293666015625" Y="2.211497802734" />
                  <Point X="27.33825" Y="2.18124609375" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.38087109375" Y="2.170504150391" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.472412109375" Y="2.172296630859" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.5920078125" Y="2.799196777344" />
                  <Point X="28.994248046875" Y="3.031430908203" />
                  <Point X="29.03761328125" Y="2.971165039062" />
                  <Point X="29.202595703125" Y="2.741876464844" />
                  <Point X="29.26341796875" Y="2.641365234375" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.624150390625" Y="1.850545410156" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583833129883" />
                  <Point X="28.26228125" Y="1.561537231445" />
                  <Point X="28.22158984375" Y="1.508451782227" />
                  <Point X="28.21312109375" Y="1.491500488281" />
                  <Point X="28.20675390625" Y="1.468736206055" />
                  <Point X="28.191595703125" Y="1.414536010742" />
                  <Point X="28.190779296875" Y="1.390965087891" />
                  <Point X="28.196005859375" Y="1.36563671875" />
                  <Point X="28.20844921875" Y="1.30533215332" />
                  <Point X="28.215646484375" Y="1.287955810547" />
                  <Point X="28.229861328125" Y="1.266350585938" />
                  <Point X="28.263703125" Y="1.214910400391" />
                  <Point X="28.280947265625" Y="1.19882019043" />
                  <Point X="28.301546875" Y="1.187224975586" />
                  <Point X="28.35058984375" Y="1.159617675781" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.39641796875" Y="1.149938842773" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.474158203125" Y="1.272607421875" />
                  <Point X="29.8489765625" Y="1.321953125" />
                  <Point X="29.868330078125" Y="1.242447631836" />
                  <Point X="29.939193359375" Y="0.951366882324" />
                  <Point X="29.958357421875" Y="0.828272827148" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="29.1256953125" Y="0.340860534668" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819503784" />
                  <Point X="28.7017265625" Y="0.217003662109" />
                  <Point X="28.636578125" Y="0.179346984863" />
                  <Point X="28.622265625" Y="0.166926239014" />
                  <Point X="28.60584765625" Y="0.146006744385" />
                  <Point X="28.566759765625" Y="0.096198562622" />
                  <Point X="28.556986328125" Y="0.074734954834" />
                  <Point X="28.551513671875" Y="0.046159915924" />
                  <Point X="28.538484375" Y="-0.021875602722" />
                  <Point X="28.538484375" Y="-0.040684474945" />
                  <Point X="28.54395703125" Y="-0.069259513855" />
                  <Point X="28.556986328125" Y="-0.137295028687" />
                  <Point X="28.566759765625" Y="-0.158758636475" />
                  <Point X="28.583177734375" Y="-0.179678131104" />
                  <Point X="28.622265625" Y="-0.229486312866" />
                  <Point X="28.636578125" Y="-0.241907058716" />
                  <Point X="28.663939453125" Y="-0.257722900391" />
                  <Point X="28.729087890625" Y="-0.295379577637" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.656703125" Y="-0.545704101563" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.98799609375" Y="-0.70398059082" />
                  <Point X="29.948431640625" Y="-0.966412597656" />
                  <Point X="29.923876953125" Y="-1.074012939453" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.85792578125" Y="-1.156337646484" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.341134765625" Y="-1.110013793945" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.185447265625" Y="-1.154697021484" />
                  <Point X="28.15298828125" Y="-1.193735961914" />
                  <Point X="28.075703125" Y="-1.286685058594" />
                  <Point X="28.064359375" Y="-1.314070800781" />
                  <Point X="28.05970703125" Y="-1.364627807617" />
                  <Point X="28.048630859375" Y="-1.485001220703" />
                  <Point X="28.056361328125" Y="-1.516622192383" />
                  <Point X="28.086080078125" Y="-1.562849121094" />
                  <Point X="28.156841796875" Y="-1.672912719727" />
                  <Point X="28.168462890625" Y="-1.685540771484" />
                  <Point X="29.018083984375" Y="-2.337479248047" />
                  <Point X="29.33907421875" Y="-2.583784179688" />
                  <Point X="29.31566015625" Y="-2.621673339844" />
                  <Point X="29.204134765625" Y="-2.802138183594" />
                  <Point X="29.153345703125" Y="-2.874302246094" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.150626953125" Y="-2.488523193359" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.673427734375" Y="-2.241770019531" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.43598046875" Y="-2.247189453125" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.33468359375" />
                  <Point X="27.26065625" Y="-2.387780761719" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.1891640625" Y="-2.546375" />
                  <Point X="27.20070703125" Y="-2.610289306641" />
                  <Point X="27.22819140625" Y="-2.762465332031" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.780060546875" Y="-3.72422265625" />
                  <Point X="27.98667578125" Y="-4.082087646484" />
                  <Point X="27.967634765625" Y="-4.0956875" />
                  <Point X="27.83529296875" Y="-4.190216308594" />
                  <Point X="27.778517578125" Y="-4.226966308594" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="26.9862578125" Y="-3.387067626953" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.607513671875" Y="-2.939940429688" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.35686328125" Y="-2.842061035156" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.112099609375" Y="-2.912771972656" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045985839844" />
                  <Point X="25.952541015625" Y="-3.119216308594" />
                  <Point X="25.914642578125" Y="-3.293573730469" />
                  <Point X="25.9139296875" Y="-3.310719970703" />
                  <Point X="26.068654296875" Y="-4.485958984375" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.119548828125" Y="-4.935803222656" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.941888671875" Y="-4.972776855469" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#146" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.057157025171" Y="4.568437406406" Z="0.75" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.75" />
                  <Point X="-0.750088547893" Y="5.011152576731" Z="0.75" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.75" />
                  <Point X="-1.523793560681" Y="4.832431607231" Z="0.75" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.75" />
                  <Point X="-1.73784097788" Y="4.672535231898" Z="0.75" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.75" />
                  <Point X="-1.730377730839" Y="4.37108455871" Z="0.75" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.75" />
                  <Point X="-1.809765829264" Y="4.311874898676" Z="0.75" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.75" />
                  <Point X="-1.906152589708" Y="4.334630690897" Z="0.75" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.75" />
                  <Point X="-1.993462792253" Y="4.426373997216" Z="0.75" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.75" />
                  <Point X="-2.593613933646" Y="4.354712856332" Z="0.75" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.75" />
                  <Point X="-3.203529206774" Y="3.927824396627" Z="0.75" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.75" />
                  <Point X="-3.267119100831" Y="3.600335902768" Z="0.75" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.75" />
                  <Point X="-2.996253796399" Y="3.08006696677" Z="0.75" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.75" />
                  <Point X="-3.036803013417" Y="3.01200055447" Z="0.75" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.75" />
                  <Point X="-3.115009498836" Y="2.999310902774" Z="0.75" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.75" />
                  <Point X="-3.333523445529" Y="3.113074847915" Z="0.75" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.75" />
                  <Point X="-4.085186200069" Y="3.003807424264" Z="0.75" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.75" />
                  <Point X="-4.447096751145" Y="2.436178715395" Z="0.75" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.75" />
                  <Point X="-4.295921950127" Y="2.070738912664" Z="0.75" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.75" />
                  <Point X="-3.675618757288" Y="1.57060196404" Z="0.75" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.75" />
                  <Point X="-3.684179786388" Y="1.511800021463" Z="0.75" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.75" />
                  <Point X="-3.734727710654" Y="1.480560991" Z="0.75" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.75" />
                  <Point X="-4.067482941519" Y="1.516248694585" Z="0.75" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.75" />
                  <Point X="-4.926590731134" Y="1.20857446535" Z="0.75" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.75" />
                  <Point X="-5.034447788596" Y="0.621532624939" Z="0.75" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.75" />
                  <Point X="-4.621465187731" Y="0.329050340468" Z="0.75" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.75" />
                  <Point X="-3.557016482916" Y="0.035504204533" Z="0.75" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.75" />
                  <Point X="-3.542292996558" Y="0.008816168685" Z="0.75" />
                  <Point X="-3.539556741714" Y="0" Z="0.75" />
                  <Point X="-3.546071604577" Y="-0.020990782376" Z="0.75" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.75" />
                  <Point X="-3.568352112208" Y="-0.043371778601" Z="0.75" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.75" />
                  <Point X="-4.015422601042" Y="-0.166661716288" Z="0.75" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.75" />
                  <Point X="-5.005633232979" Y="-0.829056445876" Z="0.75" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.75" />
                  <Point X="-4.887057345651" Y="-1.363958390155" Z="0.75" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.75" />
                  <Point X="-4.365456245006" Y="-1.45777618905" Z="0.75" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.75" />
                  <Point X="-3.200509207246" Y="-1.317839698402" Z="0.75" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.75" />
                  <Point X="-3.198102000153" Y="-1.343398625112" Z="0.75" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.75" />
                  <Point X="-3.585634370471" Y="-1.647812617964" Z="0.75" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.75" />
                  <Point X="-4.296178538336" Y="-2.698297064641" Z="0.75" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.75" />
                  <Point X="-3.964907323479" Y="-3.16504085023" Z="0.75" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.75" />
                  <Point X="-3.480866013566" Y="-3.079740316224" Z="0.75" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.75" />
                  <Point X="-2.560622241776" Y="-2.567708227356" Z="0.75" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.75" />
                  <Point X="-2.775676696944" Y="-2.954212368926" Z="0.75" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.75" />
                  <Point X="-3.011580985976" Y="-4.084254973649" Z="0.75" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.75" />
                  <Point X="-2.581069056139" Y="-4.369078949383" Z="0.75" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.75" />
                  <Point X="-2.384599160376" Y="-4.362852880104" Z="0.75" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.75" />
                  <Point X="-2.044556126652" Y="-4.035066559667" Z="0.75" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.75" />
                  <Point X="-1.750235763459" Y="-3.998374184343" Z="0.75" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.75" />
                  <Point X="-1.494398851762" Y="-4.148435298168" Z="0.75" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.75" />
                  <Point X="-1.382781075999" Y="-4.423230165082" Z="0.75" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.75" />
                  <Point X="-1.37914098686" Y="-4.621566259706" Z="0.75" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.75" />
                  <Point X="-1.204861857182" Y="-4.933080869803" Z="0.75" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.75" />
                  <Point X="-0.906322010932" Y="-4.996554910603" Z="0.75" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.75" />
                  <Point X="-0.699185907906" Y="-4.571581432015" Z="0.75" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.75" />
                  <Point X="-0.301785737454" Y="-3.352646258629" Z="0.75" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.75" />
                  <Point X="-0.074937432443" Y="-3.22749718202" Z="0.75" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.75" />
                  <Point X="0.178421646918" Y="-3.259614863269" Z="0.75" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.75" />
                  <Point X="0.368660121978" Y="-3.448999497139" Z="0.75" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.75" />
                  <Point X="0.535568952495" Y="-3.960954597801" Z="0.75" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.75" />
                  <Point X="0.944669373056" Y="-4.990691451087" Z="0.75" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.75" />
                  <Point X="1.124096479223" Y="-4.953363328145" Z="0.75" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.75" />
                  <Point X="1.112068949563" Y="-4.448152677499" Z="0.75" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.75" />
                  <Point X="0.995243079468" Y="-3.09855640717" Z="0.75" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.75" />
                  <Point X="1.137908954812" Y="-2.91993842732" Z="0.75" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.75" />
                  <Point X="1.355289097436" Y="-2.86057063576" Z="0.75" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.75" />
                  <Point X="1.574317184604" Y="-2.950718488005" Z="0.75" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.75" />
                  <Point X="1.940433011949" Y="-3.386225581639" Z="0.75" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.75" />
                  <Point X="2.799530223853" Y="-4.237660165037" Z="0.75" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.75" />
                  <Point X="2.990540318512" Y="-4.105094675021" Z="0.75" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.75" />
                  <Point X="2.817204970171" Y="-3.667942669256" Z="0.75" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.75" />
                  <Point X="2.243753987322" Y="-2.570122460065" Z="0.75" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.75" />
                  <Point X="2.298745589703" Y="-2.379787235635" Z="0.75" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.75" />
                  <Point X="2.453111142738" Y="-2.260155720264" Z="0.75" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.75" />
                  <Point X="2.658384343448" Y="-2.259694022169" Z="0.75" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.75" />
                  <Point X="3.11947092785" Y="-2.500544530103" Z="0.75" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.75" />
                  <Point X="4.188077680673" Y="-2.871799536062" Z="0.75" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.75" />
                  <Point X="4.352036392246" Y="-2.616678512558" Z="0.75" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.75" />
                  <Point X="4.042365429188" Y="-2.266531470456" Z="0.75" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.75" />
                  <Point X="3.121982132346" Y="-1.504529422708" Z="0.75" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.75" />
                  <Point X="3.103339217293" Y="-1.337929232723" Z="0.75" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.75" />
                  <Point X="3.185275910766" Y="-1.194423026522" Z="0.75" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.75" />
                  <Point X="3.345597561955" Y="-1.127592841103" Z="0.75" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.75" />
                  <Point X="3.845242759665" Y="-1.174629916552" Z="0.75" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.75" />
                  <Point X="4.966465567826" Y="-1.053857090953" Z="0.75" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.75" />
                  <Point X="5.031281102016" Y="-0.680154485062" Z="0.75" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.75" />
                  <Point X="4.663488382447" Y="-0.466127672458" Z="0.75" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.75" />
                  <Point X="3.682804910538" Y="-0.183154047019" Z="0.75" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.75" />
                  <Point X="3.616354041217" Y="-0.117530051707" Z="0.75" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.75" />
                  <Point X="3.586907165884" Y="-0.028575037378" Z="0.75" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.75" />
                  <Point X="3.59446428454" Y="0.068035493849" Z="0.75" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.75" />
                  <Point X="3.639025397183" Y="0.146418686868" Z="0.75" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.75" />
                  <Point X="3.720590503815" Y="0.204994797223" Z="0.75" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.75" />
                  <Point X="4.132479434859" Y="0.323844260311" Z="0.75" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.75" />
                  <Point X="5.001605156735" Y="0.867244898828" Z="0.75" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.75" />
                  <Point X="4.910755642967" Y="1.285554674672" Z="0.75" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.75" />
                  <Point X="4.461475037049" Y="1.35345987152" Z="0.75" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.75" />
                  <Point X="3.396811084201" Y="1.230787801038" Z="0.75" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.75" />
                  <Point X="3.319916975807" Y="1.26207590252" Z="0.75" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.75" />
                  <Point X="3.265475248129" Y="1.325111365312" Z="0.75" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.75" />
                  <Point X="3.238818081276" Y="1.407021112334" Z="0.75" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.75" />
                  <Point X="3.248749726018" Y="1.486549482523" Z="0.75" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.75" />
                  <Point X="3.295807602382" Y="1.562399122881" Z="0.75" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.75" />
                  <Point X="3.648430051777" Y="1.842157768558" Z="0.75" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.75" />
                  <Point X="4.300038905153" Y="2.698531471609" Z="0.75" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.75" />
                  <Point X="4.072041372313" Y="3.031647922369" Z="0.75" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.75" />
                  <Point X="3.560850620557" Y="2.873778104732" Z="0.75" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.75" />
                  <Point X="2.453338373359" Y="2.251879356463" Z="0.75" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.75" />
                  <Point X="2.380700969631" Y="2.251424485995" Z="0.75" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.75" />
                  <Point X="2.31558324537" Y="2.284152279049" Z="0.75" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.75" />
                  <Point X="2.266606321186" Y="2.341441615012" Z="0.75" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.75" />
                  <Point X="2.248005216763" Y="2.409057474201" Z="0.75" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.75" />
                  <Point X="2.260648586195" Y="2.486131202523" Z="0.75" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.75" />
                  <Point X="2.521847248128" Y="2.951288461398" Z="0.75" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.75" />
                  <Point X="2.864451767028" Y="4.190127697661" Z="0.75" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.75" />
                  <Point X="2.473403061537" Y="4.432215867395" Z="0.75" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.75" />
                  <Point X="2.065811362486" Y="4.636353955108" Z="0.75" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.75" />
                  <Point X="1.643120616304" Y="4.802448951991" Z="0.75" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.75" />
                  <Point X="1.056048821415" Y="4.959513481082" Z="0.75" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.75" />
                  <Point X="0.391211338109" Y="5.055590623635" Z="0.75" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.75" />
                  <Point X="0.136087536382" Y="4.863010099545" Z="0.75" />
                  <Point X="0" Y="4.355124473572" Z="0.75" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>