<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#213" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3486" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.5633046875" Y="-3.512524902344" />
                  <Point X="25.557720703125" Y="-3.497135498047" />
                  <Point X="25.54236328125" Y="-3.467375244141" />
                  <Point X="25.387578125" Y="-3.244359130859" />
                  <Point X="25.378634765625" Y="-3.231474853516" />
                  <Point X="25.356748046875" Y="-3.209018066406" />
                  <Point X="25.330494140625" Y="-3.189776123047" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.062974609375" Y="-3.101330566406" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.020978515625" Y="-3.092766601562" />
                  <Point X="24.99133984375" Y="-3.092766113281" />
                  <Point X="24.963177734375" Y="-3.097035644531" />
                  <Point X="24.723658203125" Y="-3.171374023438" />
                  <Point X="24.709818359375" Y="-3.175668945312" />
                  <Point X="24.681818359375" Y="-3.189776611328" />
                  <Point X="24.65555859375" Y="-3.2090234375" />
                  <Point X="24.633673828125" Y="-3.231479248047" />
                  <Point X="24.478888671875" Y="-3.454495117188" />
                  <Point X="24.474330078125" Y="-3.461741699219" />
                  <Point X="24.458169921875" Y="-3.490191894531" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.427130859375" Y="-3.594177734375" />
                  <Point X="24.0834140625" Y="-4.876941894531" />
                  <Point X="23.932755859375" Y="-4.847698730469" />
                  <Point X="23.92066796875" Y="-4.845352050781" />
                  <Point X="23.753583984375" Y="-4.802362304688" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516221191406" />
                  <Point X="23.72626953125" Y="-4.228549316406" />
                  <Point X="23.722962890625" Y="-4.2119296875" />
                  <Point X="23.71205859375" Y="-4.182959960937" />
                  <Point X="23.695986328125" Y="-4.155125" />
                  <Point X="23.67635546875" Y="-4.131204589844" />
                  <Point X="23.4558359375" Y="-3.937812255859" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.06429296875" Y="-3.871782958984" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.016578125" Y="-3.873709472656" />
                  <Point X="22.98552734375" Y="-3.882030761719" />
                  <Point X="22.95733984375" Y="-3.894803222656" />
                  <Point X="22.713462890625" Y="-4.057756591797" />
                  <Point X="22.70730078125" Y="-4.062240966797" />
                  <Point X="22.681326171875" Y="-4.082789306641" />
                  <Point X="22.664904296875" Y="-4.099456542969" />
                  <Point X="22.652619140625" Y="-4.115464355469" />
                  <Point X="22.5198515625" Y="-4.288490234375" />
                  <Point X="22.21601171875" Y="-4.100359863281" />
                  <Point X="22.198291015625" Y="-4.089387451172" />
                  <Point X="21.895279296875" Y="-3.856078125" />
                  <Point X="22.576240234375" Y="-2.676619140625" />
                  <Point X="22.587140625" Y="-2.647655029297" />
                  <Point X="22.593412109375" Y="-2.616129150391" />
                  <Point X="22.59442578125" Y="-2.585194580078" />
                  <Point X="22.58544140625" Y="-2.555576171875" />
                  <Point X="22.571224609375" Y="-2.526747558594" />
                  <Point X="22.553197265625" Y="-2.501591552734" />
                  <Point X="22.536818359375" Y="-2.4852109375" />
                  <Point X="22.53587109375" Y="-2.484262451172" />
                  <Point X="22.510703125" Y="-2.466221435547" />
                  <Point X="22.481873046875" Y="-2.452000488281" />
                  <Point X="22.45225390625" Y="-2.443012695312" />
                  <Point X="22.42131640625" Y="-2.444023925781" />
                  <Point X="22.389787109375" Y="-2.450294433594" />
                  <Point X="22.360818359375" Y="-2.461197021484" />
                  <Point X="22.290392578125" Y="-2.501856689453" />
                  <Point X="21.1819765625" Y="-3.141801025391" />
                  <Point X="20.931140625" Y="-2.812253662109" />
                  <Point X="20.917142578125" Y="-2.793862792969" />
                  <Point X="20.693857421875" Y="-2.419451171875" />
                  <Point X="21.894044921875" Y="-1.498514282227" />
                  <Point X="21.915419921875" Y="-1.475596069336" />
                  <Point X="21.933384765625" Y="-1.448467041016" />
                  <Point X="21.946142578125" Y="-1.419839111328" />
                  <Point X="21.953427734375" Y="-1.391716552734" />
                  <Point X="21.953857421875" Y="-1.390053588867" />
                  <Point X="21.956658203125" Y="-1.359593383789" />
                  <Point X="21.9544375" Y="-1.327936279297" />
                  <Point X="21.9474296875" Y="-1.298205322266" />
                  <Point X="21.93134765625" Y="-1.272235229492" />
                  <Point X="21.91051953125" Y="-1.248290893555" />
                  <Point X="21.887029296875" Y="-1.228768554688" />
                  <Point X="21.862013671875" Y="-1.214044555664" />
                  <Point X="21.860556640625" Y="-1.213186035156" />
                  <Point X="21.83126171875" Y="-1.201947875977" />
                  <Point X="21.799384765625" Y="-1.195472412109" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="21.67916796875" Y="-1.206088378906" />
                  <Point X="20.267900390625" Y="-1.391885375977" />
                  <Point X="20.171400390625" Y="-1.014095703125" />
                  <Point X="20.16592578125" Y="-0.99265625" />
                  <Point X="20.107578125" Y="-0.584698303223" />
                  <Point X="21.467125" Y="-0.220408370972" />
                  <Point X="21.482509765625" Y="-0.214826126099" />
                  <Point X="21.5122734375" Y="-0.199468887329" />
                  <Point X="21.53851171875" Y="-0.181258895874" />
                  <Point X="21.54002734375" Y="-0.180206848145" />
                  <Point X="21.562486328125" Y="-0.158318222046" />
                  <Point X="21.5817265625" Y="-0.132065170288" />
                  <Point X="21.59583203125" Y="-0.104068946838" />
                  <Point X="21.604578125" Y="-0.075889976501" />
                  <Point X="21.60511328125" Y="-0.074164123535" />
                  <Point X="21.60936328125" Y="-0.046026592255" />
                  <Point X="21.6093515625" Y="-0.016421098709" />
                  <Point X="21.60508203125" Y="0.011701253891" />
                  <Point X="21.59634765625" Y="0.039843032837" />
                  <Point X="21.595865234375" Y="0.041401161194" />
                  <Point X="21.581759765625" Y="0.069438659668" />
                  <Point X="21.5625078125" Y="0.095728752136" />
                  <Point X="21.54002734375" Y="0.117646827698" />
                  <Point X="21.5137890625" Y="0.135856811523" />
                  <Point X="21.501029296875" Y="0.143313354492" />
                  <Point X="21.467125" Y="0.157848205566" />
                  <Point X="21.3860859375" Y="0.179562866211" />
                  <Point X="20.10818359375" Y="0.521975402832" />
                  <Point X="20.171984375" Y="0.953133605957" />
                  <Point X="20.17551171875" Y="0.976973083496" />
                  <Point X="20.29644921875" Y="1.423267822266" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255013671875" Y="1.299341430664" />
                  <Point X="21.276572265625" Y="1.301226928711" />
                  <Point X="21.29686328125" Y="1.305262573242" />
                  <Point X="21.35493359375" Y="1.323572509766" />
                  <Point X="21.358279296875" Y="1.324627319336" />
                  <Point X="21.377220703125" Y="1.3329609375" />
                  <Point X="21.39596484375" Y="1.343782958984" />
                  <Point X="21.41264453125" Y="1.356013183594" />
                  <Point X="21.426283203125" Y="1.371563476562" />
                  <Point X="21.43869921875" Y="1.389294433594" />
                  <Point X="21.448650390625" Y="1.407431762695" />
                  <Point X="21.471951171875" Y="1.463686279297" />
                  <Point X="21.4732890625" Y="1.466917358398" />
                  <Point X="21.47908984375" Y="1.486808349609" />
                  <Point X="21.482845703125" Y="1.508120605469" />
                  <Point X="21.4841953125" Y="1.5287578125" />
                  <Point X="21.481048828125" Y="1.549198364258" />
                  <Point X="21.475447265625" Y="1.570101806641" />
                  <Point X="21.467951171875" Y="1.589377563477" />
                  <Point X="21.4398359375" Y="1.64338684082" />
                  <Point X="21.43819921875" Y="1.646530517578" />
                  <Point X="21.42670703125" Y="1.663723144531" />
                  <Point X="21.412796875" Y="1.680296142578" />
                  <Point X="21.39786328125" Y="1.69458972168" />
                  <Point X="21.35137890625" Y="1.730259033203" />
                  <Point X="20.648140625" Y="2.269873046875" />
                  <Point X="20.905140625" Y="2.710175537109" />
                  <Point X="20.918853515625" Y="2.733665283203" />
                  <Point X="21.24949609375" Y="3.158661621094" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.934083984375" Y="2.818720458984" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818763183594" />
                  <Point X="21.98089453125" Y="2.821588623047" />
                  <Point X="22.000986328125" Y="2.826505126953" />
                  <Point X="22.019537109375" Y="2.835653808594" />
                  <Point X="22.037791015625" Y="2.847282958984" />
                  <Point X="22.053921875" Y="2.860229003906" />
                  <Point X="22.111330078125" Y="2.917635986328" />
                  <Point X="22.114646484375" Y="2.920952392578" />
                  <Point X="22.127595703125" Y="2.9370859375" />
                  <Point X="22.139224609375" Y="2.955340332031" />
                  <Point X="22.14837109375" Y="2.973889160156" />
                  <Point X="22.1532890625" Y="2.993978027344" />
                  <Point X="22.156115234375" Y="3.015436767578" />
                  <Point X="22.15656640625" Y="3.036121582031" />
                  <Point X="22.149490234375" Y="3.116998535156" />
                  <Point X="22.14908203125" Y="3.121664794922" />
                  <Point X="22.145046875" Y="3.141948974609" />
                  <Point X="22.138537109375" Y="3.162597412109" />
                  <Point X="22.130205078125" Y="3.181534423828" />
                  <Point X="22.10960546875" Y="3.217212646484" />
                  <Point X="21.81666796875" Y="3.724595458984" />
                  <Point X="22.27551171875" Y="4.076385498047" />
                  <Point X="22.299384765625" Y="4.094689697266" />
                  <Point X="22.832962890625" Y="4.391134277344" />
                  <Point X="22.9568046875" Y="4.229740722656" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.18939453125" />
                  <Point X="23.09490234375" Y="4.14253515625" />
                  <Point X="23.100103515625" Y="4.139827636719" />
                  <Point X="23.119380859375" Y="4.132331054688" />
                  <Point X="23.14028515625" Y="4.126729492187" />
                  <Point X="23.1607265625" Y="4.123583007812" />
                  <Point X="23.18136328125" Y="4.124934082031" />
                  <Point X="23.202677734375" Y="4.128690429688" />
                  <Point X="23.222544921875" Y="4.13448046875" />
                  <Point X="23.31630078125" Y="4.17331640625" />
                  <Point X="23.32171875" Y="4.175560058594" />
                  <Point X="23.339853515625" Y="4.185508789062" />
                  <Point X="23.357583984375" Y="4.197923828125" />
                  <Point X="23.3731328125" Y="4.211560058594" />
                  <Point X="23.38536328125" Y="4.22823828125" />
                  <Point X="23.3961875" Y="4.246983886719" />
                  <Point X="23.404521484375" Y="4.265922363281" />
                  <Point X="23.435037109375" Y="4.362707519531" />
                  <Point X="23.436794921875" Y="4.368283203125" />
                  <Point X="23.440833984375" Y="4.388571777344" />
                  <Point X="23.44272265625" Y="4.410139160156" />
                  <Point X="23.442271484375" Y="4.430826171875" />
                  <Point X="23.4399296875" Y="4.448614746094" />
                  <Point X="23.415798828125" Y="4.6318984375" />
                  <Point X="24.01946875" Y="4.801146484375" />
                  <Point X="24.05036328125" Y="4.809808105469" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.156771484375" Y="4.325706054688" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.8170546875" Y="4.834564941406" />
                  <Point X="25.84404296875" Y="4.83173828125" />
                  <Point X="26.453134765625" Y="4.684685058594" />
                  <Point X="26.4810234375" Y="4.677952148438" />
                  <Point X="26.877408203125" Y="4.534180175781" />
                  <Point X="26.89465625" Y="4.527924316406" />
                  <Point X="27.278005859375" Y="4.34864453125" />
                  <Point X="27.294578125" Y="4.34089453125" />
                  <Point X="27.664939453125" Y="4.12512109375" />
                  <Point X="27.6809609375" Y="4.115786621094" />
                  <Point X="27.94326171875" Y="3.929254150391" />
                  <Point X="27.147583984375" Y="2.551099365234" />
                  <Point X="27.142078125" Y="2.53993359375" />
                  <Point X="27.133078125" Y="2.516056396484" />
                  <Point X="27.11278125" Y="2.440155273438" />
                  <Point X="27.11010546875" Y="2.425802001953" />
                  <Point X="27.10759375" Y="2.402517089844" />
                  <Point X="27.107728515625" Y="2.380955810547" />
                  <Point X="27.115642578125" Y="2.315322998047" />
                  <Point X="27.116099609375" Y="2.311531005859" />
                  <Point X="27.121439453125" Y="2.289612792969" />
                  <Point X="27.129705078125" Y="2.267521484375" />
                  <Point X="27.1400703125" Y="2.247471435547" />
                  <Point X="27.180681640625" Y="2.187620605469" />
                  <Point X="27.189859375" Y="2.176123535156" />
                  <Point X="27.2055078125" Y="2.159365966797" />
                  <Point X="27.221599609375" Y="2.145593261719" />
                  <Point X="27.28144921875" Y="2.104981933594" />
                  <Point X="27.28493359375" Y="2.102619140625" />
                  <Point X="27.30496484375" Y="2.092265869141" />
                  <Point X="27.32704296875" Y="2.084004882813" />
                  <Point X="27.348962890625" Y="2.078663818359" />
                  <Point X="27.414595703125" Y="2.070749511719" />
                  <Point X="27.429650390625" Y="2.070137451172" />
                  <Point X="27.45234765625" Y="2.071017822266" />
                  <Point X="27.47320703125" Y="2.074171142578" />
                  <Point X="27.549109375" Y="2.094468261719" />
                  <Point X="27.557919921875" Y="2.097290283203" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="27.670046875" Y="2.157205566406" />
                  <Point X="28.967328125" Y="2.906190673828" />
                  <Point X="29.1137578125" Y="2.702685302734" />
                  <Point X="29.123275390625" Y="2.689458496094" />
                  <Point X="29.26219921875" Y="2.459883300781" />
                  <Point X="28.230783203125" Y="1.668450317383" />
                  <Point X="28.2214296875" Y="1.660244750977" />
                  <Point X="28.203974609375" Y="1.64162890625" />
                  <Point X="28.149349609375" Y="1.570364746094" />
                  <Point X="28.141572265625" Y="1.558471679688" />
                  <Point X="28.1299453125" Y="1.537402709961" />
                  <Point X="28.1216328125" Y="1.517089599609" />
                  <Point X="28.101283203125" Y="1.444328735352" />
                  <Point X="28.100107421875" Y="1.44012512207" />
                  <Point X="28.09665234375" Y="1.417826171875" />
                  <Point X="28.0958359375" Y="1.394255859375" />
                  <Point X="28.097740234375" Y="1.371768554688" />
                  <Point X="28.1144453125" Y="1.290812744141" />
                  <Point X="28.118390625" Y="1.277034667969" />
                  <Point X="28.1265546875" Y="1.254978027344" />
                  <Point X="28.13628515625" Y="1.235739746094" />
                  <Point X="28.181689453125" Y="1.166727416992" />
                  <Point X="28.1843125" Y="1.162737304688" />
                  <Point X="28.198876953125" Y="1.145470825195" />
                  <Point X="28.216130859375" Y="1.129367919922" />
                  <Point X="28.234349609375" Y="1.116034301758" />
                  <Point X="28.30018359375" Y="1.078976196289" />
                  <Point X="28.3135234375" Y="1.072773803711" />
                  <Point X="28.33530859375" Y="1.064631713867" />
                  <Point X="28.35612109375" Y="1.059438354492" />
                  <Point X="28.445138671875" Y="1.047673339844" />
                  <Point X="28.454029296875" Y="1.046921020508" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.565634765625" Y="1.057178344727" />
                  <Point X="29.77683984375" Y="1.21663659668" />
                  <Point X="29.841849609375" Y="0.949598266602" />
                  <Point X="29.845939453125" Y="0.932795532227" />
                  <Point X="29.890865234375" Y="0.64423828125" />
                  <Point X="28.716580078125" Y="0.32958972168" />
                  <Point X="28.7047890625" Y="0.325585662842" />
                  <Point X="28.681546875" Y="0.315067932129" />
                  <Point X="28.59408984375" Y="0.264515991211" />
                  <Point X="28.582638671875" Y="0.256731567383" />
                  <Point X="28.563271484375" Y="0.241388305664" />
                  <Point X="28.547529296875" Y="0.225574478149" />
                  <Point X="28.4950546875" Y="0.158709762573" />
                  <Point X="28.4920234375" Y="0.154846847534" />
                  <Point X="28.480298828125" Y="0.135562789917" />
                  <Point X="28.47052734375" Y="0.114101913452" />
                  <Point X="28.463681640625" Y="0.092605056763" />
                  <Point X="28.446189453125" Y="0.001271131039" />
                  <Point X="28.444578125" Y="-0.012619204521" />
                  <Point X="28.443568359375" Y="-0.036705905914" />
                  <Point X="28.4451796875" Y="-0.058554153442" />
                  <Point X="28.462669921875" Y="-0.149880493164" />
                  <Point X="28.463677734375" Y="-0.155143676758" />
                  <Point X="28.4705234375" Y="-0.176649932861" />
                  <Point X="28.480296875" Y="-0.198118255615" />
                  <Point X="28.4920234375" Y="-0.217406707764" />
                  <Point X="28.544498046875" Y="-0.28427142334" />
                  <Point X="28.55420703125" Y="-0.294878845215" />
                  <Point X="28.571552734375" Y="-0.311164733887" />
                  <Point X="28.589037109375" Y="-0.324155548096" />
                  <Point X="28.676494140625" Y="-0.374707519531" />
                  <Point X="28.684138671875" Y="-0.37867578125" />
                  <Point X="28.716580078125" Y="-0.392149902344" />
                  <Point X="28.787587890625" Y="-0.411176208496" />
                  <Point X="29.891474609375" Y="-0.706961914062" />
                  <Point X="29.857306640625" Y="-0.933590820313" />
                  <Point X="29.855025390625" Y="-0.948727905273" />
                  <Point X="29.801177734375" Y="-1.18469921875" />
                  <Point X="28.4243828125" Y="-1.003440795898" />
                  <Point X="28.40803515625" Y="-1.002710205078" />
                  <Point X="28.374658203125" Y="-1.005508789063" />
                  <Point X="28.203009765625" Y="-1.042817016602" />
                  <Point X="28.19309375" Y="-1.04497265625" />
                  <Point X="28.16397265625" Y="-1.056598022461" />
                  <Point X="28.1361484375" Y="-1.073489624023" />
                  <Point X="28.1123984375" Y="-1.093959594727" />
                  <Point X="28.0086484375" Y="-1.218738769531" />
                  <Point X="28.002654296875" Y="-1.225947753906" />
                  <Point X="27.987935546875" Y="-1.250327636719" />
                  <Point X="27.976591796875" Y="-1.277711791992" />
                  <Point X="27.969759765625" Y="-1.305364379883" />
                  <Point X="27.954890625" Y="-1.466958740234" />
                  <Point X="27.95403125" Y="-1.476294677734" />
                  <Point X="27.956349609375" Y="-1.507563598633" />
                  <Point X="27.96408203125" Y="-1.539187011719" />
                  <Point X="27.976453125" Y="-1.567997558594" />
                  <Point X="28.0714453125" Y="-1.715751831055" />
                  <Point X="28.077474609375" Y="-1.724098144531" />
                  <Point X="28.09458203125" Y="-1.74526171875" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.176525390625" Y="-1.811472045898" />
                  <Point X="29.213123046875" Y="-2.606882080078" />
                  <Point X="29.1312421875" Y="-2.739377197266" />
                  <Point X="29.12480078125" Y="-2.749800537109" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="27.800955078125" Y="-2.176942871094" />
                  <Point X="27.78612890625" Y="-2.170011474609" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.5499375" Y="-2.122930908203" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.50678125" Y="-2.120395507812" />
                  <Point X="27.474609375" Y="-2.125353271484" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.27512109375" Y="-2.224495849609" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.2423828125" Y="-2.24655078125" />
                  <Point X="27.221423828125" Y="-2.267510986328" />
                  <Point X="27.204533203125" Y="-2.290439208984" />
                  <Point X="27.115212890625" Y="-2.46015234375" />
                  <Point X="27.110052734375" Y="-2.469957275391" />
                  <Point X="27.100228515625" Y="-2.499735595703" />
                  <Point X="27.095271484375" Y="-2.531907958984" />
                  <Point X="27.09567578125" Y="-2.563258544922" />
                  <Point X="27.1325703125" Y="-2.767546386719" />
                  <Point X="27.134765625" Y="-2.776939453125" />
                  <Point X="27.14280078125" Y="-2.804855712891" />
                  <Point X="27.1518203125" Y="-2.826078369141" />
                  <Point X="27.1941640625" Y="-2.899420410156" />
                  <Point X="27.86128515625" Y="-4.054906005859" />
                  <Point X="27.789486328125" Y="-4.106189453125" />
                  <Point X="27.7818359375" Y="-4.111653320313" />
                  <Point X="27.701767578125" Y="-4.163481445312" />
                  <Point X="26.758548828125" Y="-2.934254882812" />
                  <Point X="26.747505859375" Y="-2.922178710938" />
                  <Point X="26.72192578125" Y="-2.900556884766" />
                  <Point X="26.52044140625" Y="-2.771021728516" />
                  <Point X="26.50880078125" Y="-2.763538085938" />
                  <Point X="26.47998828125" Y="-2.751166503906" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.196744140625" Y="-2.761394042969" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397949219" />
                  <Point X="26.128978515625" Y="-2.780741455078" />
                  <Point X="26.10459765625" Y="-2.795461425781" />
                  <Point X="25.934443359375" Y="-2.936938232422" />
                  <Point X="25.92461328125" Y="-2.945111816406" />
                  <Point X="25.904142578125" Y="-2.968860351563" />
                  <Point X="25.88725" Y="-2.996686035156" />
                  <Point X="25.875625" Y="-3.025808105469" />
                  <Point X="25.82475" Y="-3.259873046875" />
                  <Point X="25.823248046875" Y="-3.268816650391" />
                  <Point X="25.819595703125" Y="-3.299486328125" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.8317421875" Y="-3.414269042969" />
                  <Point X="26.022064453125" Y="-4.859915039062" />
                  <Point X="25.982896484375" Y="-4.868500976562" />
                  <Point X="25.975671875" Y="-4.870084960938" />
                  <Point X="25.92931640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.950857421875" Y="-4.754439453125" />
                  <Point X="23.941578125" Y="-4.752637695312" />
                  <Point X="23.858755859375" Y="-4.731328613281" />
                  <Point X="23.8792265625" Y="-4.575838378906" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030761719" />
                  <Point X="23.8782421875" Y="-4.509322753906" />
                  <Point X="23.876666015625" Y="-4.497687011719" />
                  <Point X="23.819443359375" Y="-4.210015136719" />
                  <Point X="23.811873046875" Y="-4.178463378906" />
                  <Point X="23.80096875" Y="-4.149493652344" />
                  <Point X="23.794328125" Y="-4.135456054688" />
                  <Point X="23.778255859375" Y="-4.10762109375" />
                  <Point X="23.769421875" Y="-4.094857910156" />
                  <Point X="23.749791015625" Y="-4.0709375" />
                  <Point X="23.738994140625" Y="-4.059780029297" />
                  <Point X="23.518474609375" Y="-3.866387695312" />
                  <Point X="23.49326171875" Y="-3.845965332031" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.070505859375" Y="-3.776986328125" />
                  <Point X="23.038068359375" Y="-3.776132568359" />
                  <Point X="23.007263671875" Y="-3.779167236328" />
                  <Point X="22.991986328125" Y="-3.781947509766" />
                  <Point X="22.960935546875" Y="-3.790268798828" />
                  <Point X="22.946318359375" Y="-3.795499755859" />
                  <Point X="22.918130859375" Y="-3.808272216797" />
                  <Point X="22.904560546875" Y="-3.815813720703" />
                  <Point X="22.66068359375" Y="-3.978767089844" />
                  <Point X="22.648359375" Y="-3.987735839844" />
                  <Point X="22.622384765625" Y="-4.008284179688" />
                  <Point X="22.613654296875" Y="-4.016114013672" />
                  <Point X="22.597232421875" Y="-4.03278125" />
                  <Point X="22.589541015625" Y="-4.041618408203" />
                  <Point X="22.577255859375" Y="-4.057626220703" />
                  <Point X="22.496798828125" Y="-4.162479980469" />
                  <Point X="22.2660234375" Y="-4.019589355469" />
                  <Point X="22.252412109375" Y="-4.011161132812" />
                  <Point X="22.01913671875" Y="-3.831547607422" />
                  <Point X="22.65851171875" Y="-2.724119140625" />
                  <Point X="22.66515234375" Y="-2.710080322266" />
                  <Point X="22.676052734375" Y="-2.681116210938" />
                  <Point X="22.680314453125" Y="-2.666190429688" />
                  <Point X="22.6865859375" Y="-2.634664550781" />
                  <Point X="22.688361328125" Y="-2.619240478516" />
                  <Point X="22.689375" Y="-2.588305908203" />
                  <Point X="22.6853359375" Y="-2.557618408203" />
                  <Point X="22.6763515625" Y="-2.528" />
                  <Point X="22.67064453125" Y="-2.51355859375" />
                  <Point X="22.656427734375" Y="-2.484729980469" />
                  <Point X="22.648443359375" Y="-2.471410644531" />
                  <Point X="22.630416015625" Y="-2.446254638672" />
                  <Point X="22.620375" Y="-2.434419921875" />
                  <Point X="22.604037109375" Y="-2.418079833984" />
                  <Point X="22.59121875" Y="-2.407050537109" />
                  <Point X="22.56605078125" Y="-2.389009521484" />
                  <Point X="22.552728515625" Y="-2.381022705078" />
                  <Point X="22.5238984375" Y="-2.366801757812" />
                  <Point X="22.509458984375" Y="-2.36109375" />
                  <Point X="22.47983984375" Y="-2.352105957031" />
                  <Point X="22.449150390625" Y="-2.348063476562" />
                  <Point X="22.418212890625" Y="-2.349074707031" />
                  <Point X="22.40278515625" Y="-2.350848632812" />
                  <Point X="22.371255859375" Y="-2.357119140625" />
                  <Point X="22.35632421875" Y="-2.3613828125" />
                  <Point X="22.32735546875" Y="-2.372285400391" />
                  <Point X="22.313318359375" Y="-2.378924316406" />
                  <Point X="22.242892578125" Y="-2.419583984375" />
                  <Point X="21.206912109375" Y="-3.017707763672" />
                  <Point X="21.006734375" Y="-2.754715576172" />
                  <Point X="20.995982421875" Y="-2.740589111328" />
                  <Point X="20.818734375" Y="-2.443374267578" />
                  <Point X="21.951876953125" Y="-1.5738828125" />
                  <Point X="21.963517578125" Y="-1.563309570312" />
                  <Point X="21.984892578125" Y="-1.540391479492" />
                  <Point X="21.994626953125" Y="-1.528047363281" />
                  <Point X="22.012591796875" Y="-1.500918334961" />
                  <Point X="22.020158203125" Y="-1.487136962891" />
                  <Point X="22.032916015625" Y="-1.458509155273" />
                  <Point X="22.038107421875" Y="-1.443662353516" />
                  <Point X="22.045392578125" Y="-1.415539794922" />
                  <Point X="22.048458984375" Y="-1.398752197266" />
                  <Point X="22.051259765625" Y="-1.368291870117" />
                  <Point X="22.05142578125" Y="-1.352945556641" />
                  <Point X="22.049205078125" Y="-1.321288452148" />
                  <Point X="22.046904296875" Y="-1.306141357422" />
                  <Point X="22.039896484375" Y="-1.276410400391" />
                  <Point X="22.028197265625" Y="-1.248189697266" />
                  <Point X="22.012115234375" Y="-1.222219482422" />
                  <Point X="22.003025390625" Y="-1.209886352539" />
                  <Point X="21.982197265625" Y="-1.185942016602" />
                  <Point X="21.971240234375" Y="-1.175229125977" />
                  <Point X="21.94775" Y="-1.155706787109" />
                  <Point X="21.93521875" Y="-1.146897583008" />
                  <Point X="21.910203125" Y="-1.132173583984" />
                  <Point X="21.89458203125" Y="-1.124488647461" />
                  <Point X="21.865287109375" Y="-1.113250488281" />
                  <Point X="21.850173828125" Y="-1.108849365234" />
                  <Point X="21.818296875" Y="-1.102373901367" />
                  <Point X="21.802685546875" Y="-1.100529785156" />
                  <Point X="21.771373046875" Y="-1.099441162109" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="21.666767578125" Y="-1.111901123047" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.2634453125" Y="-0.990584533691" />
                  <Point X="20.2592421875" Y="-0.974121643066" />
                  <Point X="20.21355078125" Y="-0.654654296875" />
                  <Point X="21.491712890625" Y="-0.312171356201" />
                  <Point X="21.49952734375" Y="-0.309711517334" />
                  <Point X="21.5260703125" Y="-0.299250396729" />
                  <Point X="21.555833984375" Y="-0.283893188477" />
                  <Point X="21.566439453125" Y="-0.277514434814" />
                  <Point X="21.592677734375" Y="-0.259304443359" />
                  <Point X="21.606333984375" Y="-0.248240356445" />
                  <Point X="21.62879296875" Y="-0.226351623535" />
                  <Point X="21.639111328125" Y="-0.214475021362" />
                  <Point X="21.6583515625" Y="-0.188221969604" />
                  <Point X="21.66656640625" Y="-0.174810546875" />
                  <Point X="21.680671875" Y="-0.146814300537" />
                  <Point X="21.6865625" Y="-0.132229492187" />
                  <Point X="21.69530859375" Y="-0.104050598145" />
                  <Point X="21.699046875" Y="-0.088352310181" />
                  <Point X="21.703296875" Y="-0.060214878082" />
                  <Point X="21.70436328125" Y="-0.045988994598" />
                  <Point X="21.7043515625" Y="-0.016383468628" />
                  <Point X="21.703275390625" Y="-0.002161596537" />
                  <Point X="21.699005859375" Y="0.025960674286" />
                  <Point X="21.6958125" Y="0.039861225128" />
                  <Point X="21.68709765625" Y="0.067940620422" />
                  <Point X="21.68073046875" Y="0.084096298218" />
                  <Point X="21.666625" Y="0.112133850098" />
                  <Point X="21.65840625" Y="0.125566085815" />
                  <Point X="21.639154296875" Y="0.151856292725" />
                  <Point X="21.628826171875" Y="0.163749389648" />
                  <Point X="21.606345703125" Y="0.185667388916" />
                  <Point X="21.594193359375" Y="0.195692306519" />
                  <Point X="21.567955078125" Y="0.213902328491" />
                  <Point X="21.561720703125" Y="0.217878433228" />
                  <Point X="21.5384609375" Y="0.230628036499" />
                  <Point X="21.504556640625" Y="0.245162918091" />
                  <Point X="21.491712890625" Y="0.249611053467" />
                  <Point X="21.410673828125" Y="0.271325744629" />
                  <Point X="20.2145546875" Y="0.591824768066" />
                  <Point X="20.2659609375" Y="0.939227416992" />
                  <Point X="20.26866796875" Y="0.957525878906" />
                  <Point X="20.3664140625" Y="1.318236938477" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.23226171875" Y="1.204815551758" />
                  <Point X="21.252939453125" Y="1.204364135742" />
                  <Point X="21.263291015625" Y="1.204702636719" />
                  <Point X="21.284849609375" Y="1.206588012695" />
                  <Point X="21.295103515625" Y="1.208051879883" />
                  <Point X="21.31539453125" Y="1.212087524414" />
                  <Point X="21.325431640625" Y="1.214659667969" />
                  <Point X="21.383501953125" Y="1.232969726562" />
                  <Point X="21.396537109375" Y="1.237671386719" />
                  <Point X="21.415478515625" Y="1.246004882812" />
                  <Point X="21.424720703125" Y="1.250688842773" />
                  <Point X="21.44346484375" Y="1.261510742188" />
                  <Point X="21.452140625" Y="1.267171264648" />
                  <Point X="21.4688203125" Y="1.279401367188" />
                  <Point X="21.48406640625" Y="1.293371704102" />
                  <Point X="21.497705078125" Y="1.308921875" />
                  <Point X="21.5041015625" Y="1.317071777344" />
                  <Point X="21.516517578125" Y="1.334802856445" />
                  <Point X="21.521986328125" Y="1.343598022461" />
                  <Point X="21.5319375" Y="1.361735351562" />
                  <Point X="21.536419921875" Y="1.371077636719" />
                  <Point X="21.559720703125" Y="1.42733215332" />
                  <Point X="21.564490234375" Y="1.440320556641" />
                  <Point X="21.570291015625" Y="1.460211547852" />
                  <Point X="21.5726484375" Y="1.470320556641" />
                  <Point X="21.576404296875" Y="1.4916328125" />
                  <Point X="21.577642578125" Y="1.501921142578" />
                  <Point X="21.5789921875" Y="1.522558349609" />
                  <Point X="21.57808984375" Y="1.543211303711" />
                  <Point X="21.574943359375" Y="1.563651855469" />
                  <Point X="21.572810546875" Y="1.573788208008" />
                  <Point X="21.567208984375" Y="1.59469152832" />
                  <Point X="21.56398828125" Y="1.604533935547" />
                  <Point X="21.5564921875" Y="1.623809814453" />
                  <Point X="21.552216796875" Y="1.633243408203" />
                  <Point X="21.5241015625" Y="1.687252685547" />
                  <Point X="21.5171796875" Y="1.699323852539" />
                  <Point X="21.5056875" Y="1.716516357422" />
                  <Point X="21.49947265625" Y="1.724797485352" />
                  <Point X="21.4855625" Y="1.741370605469" />
                  <Point X="21.478484375" Y="1.74892565918" />
                  <Point X="21.46355078125" Y="1.763219360352" />
                  <Point X="21.4556953125" Y="1.769957763672" />
                  <Point X="21.4092109375" Y="1.805627075195" />
                  <Point X="20.77238671875" Y="2.294280273438" />
                  <Point X="20.9871875" Y="2.662285888672" />
                  <Point X="20.997716796875" Y="2.680320800781" />
                  <Point X="21.273662109375" Y="3.035012451172" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.9258046875" Y="2.72408203125" />
                  <Point X="21.940830078125" Y="2.723334472656" />
                  <Point X="21.961509765625" Y="2.723785888672" />
                  <Point X="21.9718359375" Y="2.724576171875" />
                  <Point X="21.993294921875" Y="2.727401611328" />
                  <Point X="22.003474609375" Y="2.729311279297" />
                  <Point X="22.02356640625" Y="2.734227783203" />
                  <Point X="22.043005859375" Y="2.741302978516" />
                  <Point X="22.061556640625" Y="2.750451660156" />
                  <Point X="22.070580078125" Y="2.755531982422" />
                  <Point X="22.088833984375" Y="2.767161132812" />
                  <Point X="22.097251953125" Y="2.773193115234" />
                  <Point X="22.1133828125" Y="2.786139160156" />
                  <Point X="22.121095703125" Y="2.793053222656" />
                  <Point X="22.17850390625" Y="2.850460205078" />
                  <Point X="22.188734375" Y="2.861487792969" />
                  <Point X="22.20168359375" Y="2.877621337891" />
                  <Point X="22.20771875" Y="2.886043701172" />
                  <Point X="22.21934765625" Y="2.904298095703" />
                  <Point X="22.2244296875" Y="2.913325927734" />
                  <Point X="22.233576171875" Y="2.931874755859" />
                  <Point X="22.240646484375" Y="2.951299316406" />
                  <Point X="22.245564453125" Y="2.971388183594" />
                  <Point X="22.2474765625" Y="2.981573486328" />
                  <Point X="22.250302734375" Y="3.003032226562" />
                  <Point X="22.251091796875" Y="3.013365234375" />
                  <Point X="22.25154296875" Y="3.034050048828" />
                  <Point X="22.251205078125" Y="3.044401855469" />
                  <Point X="22.24412890625" Y="3.125278808594" />
                  <Point X="22.242255859375" Y="3.140199951172" />
                  <Point X="22.238220703125" Y="3.160484130859" />
                  <Point X="22.235650390625" Y="3.170513427734" />
                  <Point X="22.229140625" Y="3.191161865234" />
                  <Point X="22.2254921875" Y="3.200856689453" />
                  <Point X="22.21716015625" Y="3.219793701172" />
                  <Point X="22.2124765625" Y="3.229035888672" />
                  <Point X="22.191876953125" Y="3.264714111328" />
                  <Point X="21.94061328125" Y="3.699914794922" />
                  <Point X="22.333314453125" Y="4.000993652344" />
                  <Point X="22.351640625" Y="4.015045654297" />
                  <Point X="22.807474609375" Y="4.268296386719" />
                  <Point X="22.881435546875" Y="4.171908203125" />
                  <Point X="22.8881796875" Y="4.164047363281" />
                  <Point X="22.902482421875" Y="4.149106445313" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93490625" Y="4.121898925781" />
                  <Point X="22.95210546875" Y="4.11040625" />
                  <Point X="22.96101953125" Y="4.105128417969" />
                  <Point X="23.05103515625" Y="4.058269287109" />
                  <Point X="23.065671875" Y="4.051286865234" />
                  <Point X="23.08494921875" Y="4.043790283203" />
                  <Point X="23.094791015625" Y="4.040568359375" />
                  <Point X="23.1156953125" Y="4.034966796875" />
                  <Point X="23.12583203125" Y="4.032835205078" />
                  <Point X="23.1462734375" Y="4.029688720703" />
                  <Point X="23.16693359375" Y="4.028785888672" />
                  <Point X="23.1875703125" Y="4.030136962891" />
                  <Point X="23.1978515625" Y="4.031375976562" />
                  <Point X="23.219166015625" Y="4.035132324219" />
                  <Point X="23.2292578125" Y="4.037484863281" />
                  <Point X="23.249125" Y="4.043274902344" />
                  <Point X="23.258900390625" Y="4.046712158203" />
                  <Point X="23.35265625" Y="4.085548095703" />
                  <Point X="23.367412109375" Y="4.092270263672" />
                  <Point X="23.385546875" Y="4.102219238281" />
                  <Point X="23.39434375" Y="4.107689453125" />
                  <Point X="23.41207421875" Y="4.120104492188" />
                  <Point X="23.42022265625" Y="4.126499511719" />
                  <Point X="23.435771484375" Y="4.140135742188" />
                  <Point X="23.4497421875" Y="4.155381347656" />
                  <Point X="23.46197265625" Y="4.172059570313" />
                  <Point X="23.4676328125" Y="4.180733398438" />
                  <Point X="23.47845703125" Y="4.199479003906" />
                  <Point X="23.483140625" Y="4.208719726563" />
                  <Point X="23.491474609375" Y="4.227658203125" />
                  <Point X="23.495125" Y="4.237355957031" />
                  <Point X="23.525640625" Y="4.334141113281" />
                  <Point X="23.529966796875" Y="4.349734375" />
                  <Point X="23.534005859375" Y="4.370022949219" />
                  <Point X="23.53547265625" Y="4.380284179688" />
                  <Point X="23.537361328125" Y="4.4018515625" />
                  <Point X="23.53769921875" Y="4.412210449219" />
                  <Point X="23.537248046875" Y="4.432897460938" />
                  <Point X="23.5341171875" Y="4.461014160156" />
                  <Point X="23.520734375" Y="4.562655273438" />
                  <Point X="24.045115234375" Y="4.709673339844" />
                  <Point X="24.06882421875" Y="4.7163203125" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.24853515625" Y="4.301118164063" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.80716015625" Y="4.740081542969" />
                  <Point X="25.82787890625" Y="4.737911621094" />
                  <Point X="26.43083984375" Y="4.592338378906" />
                  <Point X="26.453591796875" Y="4.586845703125" />
                  <Point X="26.845015625" Y="4.444873046875" />
                  <Point X="26.858271484375" Y="4.440064941406" />
                  <Point X="27.23776171875" Y="4.262590332031" />
                  <Point X="27.25045703125" Y="4.256653808594" />
                  <Point X="27.6171171875" Y="4.043035888672" />
                  <Point X="27.629412109375" Y="4.035873046875" />
                  <Point X="27.81778125" Y="3.901915283203" />
                  <Point X="27.0653125" Y="2.598599365234" />
                  <Point X="27.06237890625" Y="2.593113769531" />
                  <Point X="27.05318359375" Y="2.573440673828" />
                  <Point X="27.04418359375" Y="2.549563476562" />
                  <Point X="27.041302734375" Y="2.540598144531" />
                  <Point X="27.021005859375" Y="2.464697021484" />
                  <Point X="27.019390625" Y="2.457565429688" />
                  <Point X="27.015654296875" Y="2.435990478516" />
                  <Point X="27.013142578125" Y="2.412705566406" />
                  <Point X="27.012595703125" Y="2.401923339844" />
                  <Point X="27.01273046875" Y="2.380362060547" />
                  <Point X="27.013412109375" Y="2.369583007812" />
                  <Point X="27.021326171875" Y="2.303950195312" />
                  <Point X="27.023798828125" Y="2.289044189453" />
                  <Point X="27.029138671875" Y="2.267125976563" />
                  <Point X="27.032462890625" Y="2.256321777344" />
                  <Point X="27.040728515625" Y="2.23423046875" />
                  <Point X="27.045314453125" Y="2.22389453125" />
                  <Point X="27.0556796875" Y="2.203844482422" />
                  <Point X="27.061458984375" Y="2.194130371094" />
                  <Point X="27.1020703125" Y="2.134279541016" />
                  <Point X="27.106435546875" Y="2.128353027344" />
                  <Point X="27.12042578125" Y="2.111285400391" />
                  <Point X="27.13607421875" Y="2.094527832031" />
                  <Point X="27.143734375" Y="2.087191650391" />
                  <Point X="27.159826171875" Y="2.073418945312" />
                  <Point X="27.1682578125" Y="2.066982421875" />
                  <Point X="27.228107421875" Y="2.02637121582" />
                  <Point X="27.241314453125" Y="2.018225097656" />
                  <Point X="27.261345703125" Y="2.007871948242" />
                  <Point X="27.271671875" Y="2.003290283203" />
                  <Point X="27.29375" Y="1.995029418945" />
                  <Point X="27.304552734375" Y="1.991705322266" />
                  <Point X="27.32647265625" Y="1.986364257812" />
                  <Point X="27.33758984375" Y="1.984347045898" />
                  <Point X="27.40322265625" Y="1.976432739258" />
                  <Point X="27.410736328125" Y="1.975827758789" />
                  <Point X="27.43333203125" Y="1.975208740234" />
                  <Point X="27.456029296875" Y="1.976089233398" />
                  <Point X="27.466546875" Y="1.977084960938" />
                  <Point X="27.48740625" Y="1.98023828125" />
                  <Point X="27.497748046875" Y="1.982395874023" />
                  <Point X="27.573650390625" Y="2.002692993164" />
                  <Point X="27.594697265625" Y="2.009698242188" />
                  <Point X="27.6253125" Y="2.022552856445" />
                  <Point X="27.63603515625" Y="2.027872436523" />
                  <Point X="27.717546875" Y="2.074933105469" />
                  <Point X="28.94040625" Y="2.780950683594" />
                  <Point X="29.03664453125" Y="2.647199951172" />
                  <Point X="29.04395703125" Y="2.637037841797" />
                  <Point X="29.136884765625" Y="2.483471435547" />
                  <Point X="28.172951171875" Y="1.743818847656" />
                  <Point X="28.1681328125" Y="1.739864624023" />
                  <Point X="28.15212890625" Y="1.725224731445" />
                  <Point X="28.134673828125" Y="1.706608764648" />
                  <Point X="28.128576171875" Y="1.699422729492" />
                  <Point X="28.073951171875" Y="1.628158691406" />
                  <Point X="28.069841796875" Y="1.622358642578" />
                  <Point X="28.058396484375" Y="1.604372192383" />
                  <Point X="28.04676953125" Y="1.583303222656" />
                  <Point X="28.042021484375" Y="1.573382446289" />
                  <Point X="28.033708984375" Y="1.553069335938" />
                  <Point X="28.03014453125" Y="1.542677124023" />
                  <Point X="28.009794921875" Y="1.469916259766" />
                  <Point X="28.006228515625" Y="1.454671142578" />
                  <Point X="28.0027734375" Y="1.432372192383" />
                  <Point X="28.001708984375" Y="1.421114746094" />
                  <Point X="28.000892578125" Y="1.397544433594" />
                  <Point X="28.001173828125" Y="1.386239746094" />
                  <Point X="28.003078125" Y="1.363752319336" />
                  <Point X="28.004701171875" Y="1.352569946289" />
                  <Point X="28.02140625" Y="1.271614135742" />
                  <Point X="28.023115234375" Y="1.264660766602" />
                  <Point X="28.029296875" Y="1.244057739258" />
                  <Point X="28.0374609375" Y="1.222001098633" />
                  <Point X="28.04178125" Y="1.212100585938" />
                  <Point X="28.05151171875" Y="1.192862426758" />
                  <Point X="28.056921875" Y="1.183525024414" />
                  <Point X="28.102306640625" Y="1.114542236328" />
                  <Point X="28.1116953125" Y="1.101484741211" />
                  <Point X="28.126259765625" Y="1.084218261719" />
                  <Point X="28.13405859375" Y="1.076019042969" />
                  <Point X="28.1513125" Y="1.059916137695" />
                  <Point X="28.160025390625" Y="1.052705688477" />
                  <Point X="28.178244140625" Y="1.039372070312" />
                  <Point X="28.18775" Y="1.033248901367" />
                  <Point X="28.253583984375" Y="0.996190673828" />
                  <Point X="28.260130859375" Y="0.992832275391" />
                  <Point X="28.280265625" Y="0.983785888672" />
                  <Point X="28.30205078125" Y="0.975643737793" />
                  <Point X="28.31230859375" Y="0.972458007812" />
                  <Point X="28.33312109375" Y="0.967264648438" />
                  <Point X="28.343673828125" Y="0.965257385254" />
                  <Point X="28.43269140625" Y="0.953492248535" />
                  <Point X="28.454205078125" Y="0.951921142578" />
                  <Point X="28.488380859375" Y="0.951984619141" />
                  <Point X="28.50060546875" Y="0.952797302246" />
                  <Point X="28.57803515625" Y="0.962991027832" />
                  <Point X="29.704703125" Y="1.111319946289" />
                  <Point X="29.749544921875" Y="0.92712713623" />
                  <Point X="29.7526875" Y="0.914215698242" />
                  <Point X="29.78387109375" Y="0.713920776367" />
                  <Point X="28.6919921875" Y="0.421352661133" />
                  <Point X="28.686033203125" Y="0.419544494629" />
                  <Point X="28.665623046875" Y="0.412136108398" />
                  <Point X="28.642380859375" Y="0.401618347168" />
                  <Point X="28.634005859375" Y="0.397316467285" />
                  <Point X="28.546548828125" Y="0.34676449585" />
                  <Point X="28.540681640625" Y="0.343081604004" />
                  <Point X="28.523646484375" Y="0.331195495605" />
                  <Point X="28.504279296875" Y="0.315852264404" />
                  <Point X="28.495943359375" Y="0.308410705566" />
                  <Point X="28.480201171875" Y="0.292596954346" />
                  <Point X="28.472794921875" Y="0.284224731445" />
                  <Point X="28.4203203125" Y="0.217360031128" />
                  <Point X="28.410849609375" Y="0.204200363159" />
                  <Point X="28.399125" Y="0.184916259766" />
                  <Point X="28.39383984375" Y="0.174929244995" />
                  <Point X="28.384068359375" Y="0.153468399048" />
                  <Point X="28.380005859375" Y="0.142928359985" />
                  <Point X="28.37316015625" Y="0.121431549072" />
                  <Point X="28.370376953125" Y="0.110474624634" />
                  <Point X="28.352884765625" Y="0.019140668869" />
                  <Point X="28.351822265625" Y="0.012218111992" />
                  <Point X="28.349662109375" Y="-0.008640067101" />
                  <Point X="28.34865234375" Y="-0.032726783752" />
                  <Point X="28.348826171875" Y="-0.043693218231" />
                  <Point X="28.3504375" Y="-0.065541526794" />
                  <Point X="28.351875" Y="-0.076423240662" />
                  <Point X="28.369365234375" Y="-0.167749465942" />
                  <Point X="28.37315234375" Y="-0.183958724976" />
                  <Point X="28.379998046875" Y="-0.205465057373" />
                  <Point X="28.3840625" Y="-0.216011627197" />
                  <Point X="28.3938359375" Y="-0.237479904175" />
                  <Point X="28.39912109375" Y="-0.247469436646" />
                  <Point X="28.41084765625" Y="-0.266758026123" />
                  <Point X="28.4172890625" Y="-0.276056915283" />
                  <Point X="28.469763671875" Y="-0.342921600342" />
                  <Point X="28.474419921875" Y="-0.348413208008" />
                  <Point X="28.489181640625" Y="-0.364136474609" />
                  <Point X="28.50652734375" Y="-0.380422424316" />
                  <Point X="28.51489453125" Y="-0.387420318604" />
                  <Point X="28.53237890625" Y="-0.400411132812" />
                  <Point X="28.54149609375" Y="-0.406404052734" />
                  <Point X="28.628953125" Y="-0.456956054688" />
                  <Point X="28.64769921875" Y="-0.466409515381" />
                  <Point X="28.680140625" Y="-0.479883514404" />
                  <Point X="28.6919921875" Y="-0.483912963867" />
                  <Point X="28.763" Y="-0.502939239502" />
                  <Point X="29.784880859375" Y="-0.776751220703" />
                  <Point X="29.763369140625" Y="-0.919428100586" />
                  <Point X="29.761619140625" Y="-0.931044250488" />
                  <Point X="29.7278046875" Y="-1.079219848633" />
                  <Point X="28.436783203125" Y="-0.909253479004" />
                  <Point X="28.428625" Y="-0.908535522461" />
                  <Point X="28.40009765625" Y="-0.908042358398" />
                  <Point X="28.366720703125" Y="-0.910840942383" />
                  <Point X="28.35448046875" Y="-0.912676269531" />
                  <Point X="28.18283203125" Y="-0.94998449707" />
                  <Point X="28.15787109375" Y="-0.956743286133" />
                  <Point X="28.12875" Y="-0.968368713379" />
                  <Point X="28.114673828125" Y="-0.975390991211" />
                  <Point X="28.086849609375" Y="-0.992282714844" />
                  <Point X="28.074126953125" Y="-1.001529418945" />
                  <Point X="28.050376953125" Y="-1.021999389648" />
                  <Point X="28.039349609375" Y="-1.03322253418" />
                  <Point X="27.935599609375" Y="-1.158001708984" />
                  <Point X="27.921326171875" Y="-1.176848144531" />
                  <Point X="27.906607421875" Y="-1.201227905273" />
                  <Point X="27.90016796875" Y="-1.213970336914" />
                  <Point X="27.88882421875" Y="-1.241354492188" />
                  <Point X="27.884365234375" Y="-1.25492565918" />
                  <Point X="27.877533203125" Y="-1.28257824707" />
                  <Point X="27.87516015625" Y="-1.296659667969" />
                  <Point X="27.860291015625" Y="-1.45825402832" />
                  <Point X="27.859291015625" Y="-1.483318969727" />
                  <Point X="27.861609375" Y="-1.514587890625" />
                  <Point X="27.864068359375" Y="-1.530127807617" />
                  <Point X="27.87180078125" Y="-1.561751220703" />
                  <Point X="27.8767890625" Y="-1.576670043945" />
                  <Point X="27.88916015625" Y="-1.60548059082" />
                  <Point X="27.89654296875" Y="-1.619372192383" />
                  <Point X="27.99153515625" Y="-1.767126586914" />
                  <Point X="28.00359375" Y="-1.783819091797" />
                  <Point X="28.020701171875" Y="-1.804982788086" />
                  <Point X="28.028263671875" Y="-1.813281982422" />
                  <Point X="28.0443125" Y="-1.828929443359" />
                  <Point X="28.052798828125" Y="-1.83627746582" />
                  <Point X="28.118693359375" Y="-1.886840454102" />
                  <Point X="29.087171875" Y="-2.629980957031" />
                  <Point X="29.0504296875" Y="-2.689435302734" />
                  <Point X="29.04548046875" Y="-2.697443359375" />
                  <Point X="29.001275390625" Y="-2.760252441406" />
                  <Point X="27.848455078125" Y="-2.094670410156" />
                  <Point X="27.841189453125" Y="-2.090883300781" />
                  <Point X="27.8150234375" Y="-2.079512207031" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771109375" Y="-2.066337646484" />
                  <Point X="27.566822265625" Y="-2.029443237305" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.508005859375" Y="-2.025403442383" />
                  <Point X="27.4923125" Y="-2.02650378418" />
                  <Point X="27.460140625" Y="-2.031461547852" />
                  <Point X="27.44484375" Y="-2.03513659668" />
                  <Point X="27.415068359375" Y="-2.044960327148" />
                  <Point X="27.40058984375" Y="-2.051109130859" />
                  <Point X="27.230876953125" Y="-2.140427978516" />
                  <Point X="27.208970703125" Y="-2.153169921875" />
                  <Point X="27.186037109375" Y="-2.170064697266" />
                  <Point X="27.175205078125" Y="-2.179377685547" />
                  <Point X="27.15424609375" Y="-2.200337890625" />
                  <Point X="27.1449375" Y="-2.211165527344" />
                  <Point X="27.128046875" Y="-2.23409375" />
                  <Point X="27.12046484375" Y="-2.246194335938" />
                  <Point X="27.03114453125" Y="-2.415907470703" />
                  <Point X="27.0198359375" Y="-2.440193603516" />
                  <Point X="27.01001171875" Y="-2.469971923828" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.51744140625" />
                  <Point X="27.000279296875" Y="-2.533133056641" />
                  <Point X="27.00068359375" Y="-2.564483642578" />
                  <Point X="27.0021875" Y="-2.580142578125" />
                  <Point X="27.03908203125" Y="-2.784430419922" />
                  <Point X="27.04347265625" Y="-2.803216552734" />
                  <Point X="27.0515078125" Y="-2.8311328125" />
                  <Point X="27.055369140625" Y="-2.842013671875" />
                  <Point X="27.064388671875" Y="-2.863236328125" />
                  <Point X="27.069546875" Y="-2.873578125" />
                  <Point X="27.111890625" Y="-2.946920166016" />
                  <Point X="27.73589453125" Y="-4.027723632812" />
                  <Point X="27.723755859375" Y="-4.036082275391" />
                  <Point X="26.83391796875" Y="-2.876422607422" />
                  <Point X="26.82865625" Y="-2.870145751953" />
                  <Point X="26.80883203125" Y="-2.849625" />
                  <Point X="26.783251953125" Y="-2.828003173828" />
                  <Point X="26.77330078125" Y="-2.820646728516" />
                  <Point X="26.57181640625" Y="-2.691111572266" />
                  <Point X="26.546283203125" Y="-2.676244873047" />
                  <Point X="26.517470703125" Y="-2.663873291016" />
                  <Point X="26.50255078125" Y="-2.658884765625" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.1880390625" Y="-2.666793701172" />
                  <Point X="26.161224609375" Y="-2.670339355469" />
                  <Point X="26.13357421875" Y="-2.677171630859" />
                  <Point X="26.1200078125" Y="-2.681629882812" />
                  <Point X="26.092623046875" Y="-2.692973388672" />
                  <Point X="26.079876953125" Y="-2.699414550781" />
                  <Point X="26.05549609375" Y="-2.714134521484" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.87370703125" Y="-2.863890136719" />
                  <Point X="25.85265625" Y="-2.883086425781" />
                  <Point X="25.832185546875" Y="-2.906834960938" />
                  <Point X="25.822935546875" Y="-2.919560791016" />
                  <Point X="25.80604296875" Y="-2.947386474609" />
                  <Point X="25.79901953125" Y="-2.961466064453" />
                  <Point X="25.78739453125" Y="-2.990588134766" />
                  <Point X="25.78279296875" Y="-3.005630615234" />
                  <Point X="25.73191796875" Y="-3.239695556641" />
                  <Point X="25.7289140625" Y="-3.257582763672" />
                  <Point X="25.72526171875" Y="-3.288252441406" />
                  <Point X="25.72459765625" Y="-3.300075195312" />
                  <Point X="25.724744140625" Y="-3.323708984375" />
                  <Point X="25.7255546875" Y="-3.335520019531" />
                  <Point X="25.7375546875" Y="-3.426668945312" />
                  <Point X="25.833087890625" Y="-4.152318847656" />
                  <Point X="25.655068359375" Y="-3.487937255859" />
                  <Point X="25.652607421875" Y="-3.480121582031" />
                  <Point X="25.642142578125" Y="-3.453570556641" />
                  <Point X="25.62678515625" Y="-3.423810302734" />
                  <Point X="25.620408203125" Y="-3.413208251953" />
                  <Point X="25.465623046875" Y="-3.190192138672" />
                  <Point X="25.44666796875" Y="-3.165168701172" />
                  <Point X="25.42478125" Y="-3.142711914062" />
                  <Point X="25.41290625" Y="-3.132394287109" />
                  <Point X="25.38665234375" Y="-3.11315234375" />
                  <Point X="25.373240234375" Y="-3.104936767578" />
                  <Point X="25.3452421875" Y="-3.090829833984" />
                  <Point X="25.33065625" Y="-3.084938476562" />
                  <Point X="25.091134765625" Y="-3.010599853516" />
                  <Point X="25.063376953125" Y="-3.003109375" />
                  <Point X="25.03521875" Y="-2.998840087891" />
                  <Point X="25.02098046875" Y="-2.997766601562" />
                  <Point X="24.991341796875" Y="-2.997766113281" />
                  <Point X="24.977099609375" Y="-2.998839355469" />
                  <Point X="24.9489375" Y="-3.003108886719" />
                  <Point X="24.935017578125" Y="-3.006305175781" />
                  <Point X="24.695498046875" Y="-3.080643554688" />
                  <Point X="24.667072265625" Y="-3.090829345703" />
                  <Point X="24.639072265625" Y="-3.104937011719" />
                  <Point X="24.625658203125" Y="-3.113153808594" />
                  <Point X="24.5993984375" Y="-3.132400634766" />
                  <Point X="24.5875234375" Y="-3.142718994141" />
                  <Point X="24.565638671875" Y="-3.165174804688" />
                  <Point X="24.55562890625" Y="-3.177312255859" />
                  <Point X="24.40084375" Y="-3.400328125" />
                  <Point X="24.3917265625" Y="-3.414821289062" />
                  <Point X="24.37556640625" Y="-3.443271484375" />
                  <Point X="24.370275390625" Y="-3.454140380859" />
                  <Point X="24.361115234375" Y="-3.476472900391" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.3353671875" Y="-3.56958984375" />
                  <Point X="24.014572265625" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.115871374756" Y="2.832193075219" />
                  <Point X="20.849896866662" Y="2.234804549119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.437639758505" Y="1.308859923895" />
                  <Point X="20.262422678908" Y="0.915315919704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.302698851536" Y="3.018248091563" />
                  <Point X="20.927407014574" Y="2.175328824801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.535872276016" Y="1.295927403739" />
                  <Point X="20.221550941759" Y="0.589950128174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.385424455744" Y="2.970486473879" />
                  <Point X="21.004917162486" Y="2.115853100483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.634104793528" Y="1.282994883584" />
                  <Point X="20.314457736165" Y="0.565055838076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.468150059953" Y="2.922724856195" />
                  <Point X="21.082427310399" Y="2.056377376165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.73233731104" Y="1.270062363429" />
                  <Point X="20.407364530571" Y="0.540161547979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.550875664161" Y="2.874963238512" />
                  <Point X="21.159937458311" Y="1.996901651847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.830569828552" Y="1.257129843274" />
                  <Point X="20.500271324977" Y="0.515267257881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.064419630626" Y="3.794835505328" />
                  <Point X="21.98665375649" Y="3.620170492263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.63360126837" Y="2.827201620828" />
                  <Point X="21.237447606223" Y="1.937425927529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.928802346063" Y="1.244197323119" />
                  <Point X="20.593178119384" Y="0.490372967783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.222304101608" Y="3.915883466303" />
                  <Point X="22.045367030192" Y="3.518476297235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.716326872579" Y="2.779440003145" />
                  <Point X="21.314957754135" Y="1.877950203211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.027034863575" Y="1.231264802964" />
                  <Point X="20.68608491379" Y="0.465478677686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.376624336429" Y="4.028926021788" />
                  <Point X="22.104080303894" Y="3.416782102206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.802652666627" Y="2.739764544233" />
                  <Point X="21.392467902047" Y="1.818474478892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.125267381087" Y="1.218332282809" />
                  <Point X="20.778991708196" Y="0.440584387588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.301899984126" Y="-0.630981169198" />
                  <Point X="20.232468795924" Y="-0.786926171155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.514791672025" Y="4.105688571611" />
                  <Point X="22.162793577596" Y="3.315087907178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.900641011018" Y="2.726283602272" />
                  <Point X="21.469375131912" Y="1.757644578469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.223537032529" Y="1.205483166827" />
                  <Point X="20.871898502602" Y="0.415690097491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.41997686268" Y="-0.599342524697" />
                  <Point X="20.257752330449" Y="-0.963704789718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.652959007622" Y="4.182451121434" />
                  <Point X="22.220736925928" Y="3.211664431456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.006285733541" Y="2.729999167146" />
                  <Point X="21.533762367225" Y="1.668694309871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.33262262177" Y="1.216927044885" />
                  <Point X="20.964805297009" Y="0.390795807393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.538053741234" Y="-0.567703880197" />
                  <Point X="20.294917821377" Y="-1.113796097253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.791126343219" Y="4.259213671257" />
                  <Point X="22.251049413102" Y="3.046181025477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.152196187635" Y="2.824153045859" />
                  <Point X="21.578428912916" Y="1.535450647177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.462299747879" Y="1.274620271983" />
                  <Point X="21.057712091415" Y="0.365901517296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.656130619788" Y="-0.536065235696" />
                  <Point X="20.332828705699" Y="-1.262213223815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.865495105656" Y="4.192682279641" />
                  <Point X="21.150618885821" Y="0.341007227198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.774207498343" Y="-0.504426591195" />
                  <Point X="20.431326409138" Y="-1.274550126625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.937268106351" Y="4.120320711696" />
                  <Point X="21.243525680228" Y="0.316112937101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.892284376897" Y="-0.472787946694" />
                  <Point X="20.541791842205" Y="-1.260007068591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.020669388892" Y="4.074076692394" />
                  <Point X="21.336432474634" Y="0.291218647003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.010361255451" Y="-0.441149302194" />
                  <Point X="20.652257275272" Y="-1.245464010557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.10814747842" Y="4.036989331505" />
                  <Point X="21.429339237073" Y="0.266324285108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.128438134005" Y="-0.409510657693" />
                  <Point X="20.762722708339" Y="-1.230920952523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.210642322303" Y="4.033630153122" />
                  <Point X="21.520806423923" Y="0.238196583499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.246515012559" Y="-0.377872013192" />
                  <Point X="20.873188141406" Y="-1.216377894489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.554368339803" Y="4.572085061696" />
                  <Point X="23.527452570612" Y="4.511631254294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.33437698202" Y="4.077976382175" />
                  <Point X="21.60273538427" Y="0.188645674405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.364591891113" Y="-0.346233368692" />
                  <Point X="20.983653574472" Y="-1.201834836455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.673191012055" Y="4.605398786268" />
                  <Point X="21.669826921034" Y="0.105769366315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.482668769667" Y="-0.314594724191" />
                  <Point X="21.094119007539" Y="-1.187291778421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.792013684307" Y="4.638712510839" />
                  <Point X="21.70398126608" Y="-0.051085085602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.623766447917" Y="-0.231250517009" />
                  <Point X="21.204584440606" Y="-1.172748720387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.910836356558" Y="4.67202623541" />
                  <Point X="21.315049873673" Y="-1.158205662353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.02965902881" Y="4.705339959982" />
                  <Point X="21.42551530674" Y="-1.143662604319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.861434197069" Y="-2.410609520106" />
                  <Point X="20.834829905808" Y="-2.470363736621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.142370517086" Y="4.724927740611" />
                  <Point X="21.535980739807" Y="-1.129119546285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.019387061834" Y="-2.289407944179" />
                  <Point X="20.894369619976" Y="-2.57020171597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.252077458571" Y="4.73776719866" />
                  <Point X="21.646446172874" Y="-1.114576488251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.177339926599" Y="-2.168206368253" />
                  <Point X="20.953909334145" Y="-2.670039695319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.361784400056" Y="4.750606656708" />
                  <Point X="21.756864534098" Y="-1.100139155307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.335292791364" Y="-2.047004792326" />
                  <Point X="21.015230145909" Y="-2.765877263973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.471491341541" Y="4.763446114757" />
                  <Point X="21.856196126621" Y="-1.110603112569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.49324565613" Y="-1.925803216399" />
                  <Point X="21.080841805752" Y="-2.852077430048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.581198283027" Y="4.776285572806" />
                  <Point X="21.941927489687" Y="-1.151613685326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.651198520895" Y="-1.804601640472" />
                  <Point X="21.146453465595" Y="-2.938277596123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.654766330684" Y="4.70795574635" />
                  <Point X="22.013492198241" Y="-1.22444308508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.80915138566" Y="-1.683400064545" />
                  <Point X="21.217905105318" Y="-3.011360952629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.693836585791" Y="4.562142609205" />
                  <Point X="22.050693085013" Y="-1.374454892247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.969433788424" Y="-1.556966260607" />
                  <Point X="21.35787530949" Y="-2.930549093686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.732906840897" Y="4.41632947206" />
                  <Point X="21.497845513663" Y="-2.849737234742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.771977096004" Y="4.270516334916" />
                  <Point X="21.637815717836" Y="-2.768925375799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.828913258922" Y="4.164830683717" />
                  <Point X="21.777785922009" Y="-2.688113516856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.907345067897" Y="4.107425044037" />
                  <Point X="21.917756126182" Y="-2.607301657912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.001401677542" Y="4.08511328125" />
                  <Point X="22.057726330354" Y="-2.526489798969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.415275530824" Y="4.781122808599" />
                  <Point X="25.319531238548" Y="4.566077607256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.118150992165" Y="4.113770168342" />
                  <Point X="22.197696534527" Y="-2.445677940025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.514633142621" Y="4.770717291583" />
                  <Point X="22.335774114154" Y="-2.369116985411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.613990754418" Y="4.760311774567" />
                  <Point X="22.449138006453" Y="-2.348063881354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.713348366215" Y="4.749906257551" />
                  <Point X="22.541024614216" Y="-2.375249548168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.812705967756" Y="4.739500717499" />
                  <Point X="22.6191957981" Y="-2.433240561385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.90743705118" Y="4.718703847622" />
                  <Point X="22.678233856356" Y="-2.534205278362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.375203315955" Y="-3.214823015718" />
                  <Point X="22.079817775042" Y="-3.878269803089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.001334308293" Y="4.696034173186" />
                  <Point X="22.157260198539" Y="-3.937897638934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.095231565406" Y="4.673364498751" />
                  <Point X="22.234702622035" Y="-3.99752547478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.189128822518" Y="4.650694824315" />
                  <Point X="22.315288460763" Y="-4.050093084422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.283026079631" Y="4.62802514988" />
                  <Point X="22.396806438857" Y="-4.100567074768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.376923336744" Y="4.605355475445" />
                  <Point X="22.478324416951" Y="-4.151041065115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.47001977781" Y="4.580887138718" />
                  <Point X="22.658216974797" Y="-3.980562131719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.559551856913" Y="4.548413113949" />
                  <Point X="22.806337557127" Y="-3.881444223715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.649083936016" Y="4.51593908918" />
                  <Point X="22.949024045955" Y="-3.794531489547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.73861601512" Y="4.48346506441" />
                  <Point X="23.060938219801" Y="-3.776734506446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.828148094223" Y="4.450991039641" />
                  <Point X="23.162142410537" Y="-3.78299253926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.915382395238" Y="4.413356120787" />
                  <Point X="23.263184254295" Y="-3.789615209355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.001451652761" Y="4.373104471405" />
                  <Point X="23.364190125506" Y="-3.796318675115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.087520910283" Y="4.332852822023" />
                  <Point X="23.455889488325" Y="-3.82392490096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.173590167805" Y="4.292601172641" />
                  <Point X="23.534656080996" Y="-3.880578604146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.259285722097" Y="4.251510172062" />
                  <Point X="23.609444660591" Y="-3.946167070987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.341857646245" Y="4.20340338331" />
                  <Point X="23.684233240185" Y="-4.011755537829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.424429570393" Y="4.155296594559" />
                  <Point X="23.757624428582" Y="-4.08048259669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.50700149454" Y="4.107189805807" />
                  <Point X="23.814015467126" Y="-4.187392617281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.589573418688" Y="4.059083017055" />
                  <Point X="23.846769787384" Y="-4.347391576357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.670287930867" Y="4.006804412718" />
                  <Point X="23.878301447413" Y="-4.510136675269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.74927068825" Y="3.950636223424" />
                  <Point X="27.423608755602" Y="3.219187546837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.020216424753" Y="2.313153537438" />
                  <Point X="24.59336560374" Y="-3.137642651337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.236089093137" Y="-3.940098832603" />
                  <Point X="23.881235788551" Y="-4.737112404044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.067349352009" Y="2.185449458437" />
                  <Point X="24.727100629511" Y="-3.070835232376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.078910644577" Y="-4.526693775014" />
                  <Point X="23.975391504243" Y="-4.759201571007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.132542672689" Y="2.098309687218" />
                  <Point X="24.84776484513" Y="-3.033385333679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.209980427272" Y="2.038671364821" />
                  <Point X="24.966414946648" Y="-3.000459209323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.294445010547" Y="1.994815558068" />
                  <Point X="25.068598159003" Y="-3.004518323578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.39091183854" Y="1.977917234324" />
                  <Point X="25.160320652247" Y="-3.032072597638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.496809086296" Y="1.982199980158" />
                  <Point X="25.25168600243" Y="-3.060429028146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.617260551354" Y="2.019172033271" />
                  <Point X="25.342613802704" Y="-3.089768211838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.755948725122" Y="2.09710440478" />
                  <Point X="25.423520067701" Y="-3.141616132296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.895918873056" Y="2.177916137408" />
                  <Point X="25.490149562008" Y="-3.225530204736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.035889020989" Y="2.258727870035" />
                  <Point X="25.553500803662" Y="-3.316807353188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.175859168922" Y="2.339539602663" />
                  <Point X="25.848395470043" Y="-2.888029454948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.77016127054" Y="-3.063746344007" />
                  <Point X="25.616852045316" Y="-3.40808450164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.315829316856" Y="2.420351335291" />
                  <Point X="26.015696673922" Y="-2.745831165595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.731852401279" Y="-3.383355840015" />
                  <Point X="25.66642284888" Y="-3.530313020802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.455799464789" Y="2.501163067918" />
                  <Point X="28.048696974785" Y="1.586795904622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.034027691354" Y="1.553848154589" />
                  <Point X="26.152318437837" Y="-2.672540026608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.75558444637" Y="-3.563619160898" />
                  <Point X="25.705493130339" Y="-3.676126098758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.595769612722" Y="2.581974800546" />
                  <Point X="28.248361398118" Y="1.801683174987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.018541217522" Y="1.285498597985" />
                  <Point X="26.261893145684" Y="-2.659997570172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.779316466199" Y="-3.743882538525" />
                  <Point X="25.744563411798" Y="-3.821939176713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.735739760656" Y="2.662786533174" />
                  <Point X="28.406314245019" Y="1.922884710791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.068974059784" Y="1.165206249436" />
                  <Point X="26.370326136837" Y="-2.650019451418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.803048486027" Y="-3.924145916151" />
                  <Point X="25.783633693258" Y="-3.967752254669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.875709908589" Y="2.743598265801" />
                  <Point X="28.56426709192" Y="2.044086246594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.133511766227" Y="1.076593944532" />
                  <Point X="26.473528722527" Y="-2.651789015675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.826780505855" Y="-4.104409293777" />
                  <Point X="25.822703974717" Y="-4.113565332624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.97495369324" Y="2.73293708883" />
                  <Point X="28.722219938821" Y="2.165287782398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.212100905302" Y="1.019541674044" />
                  <Point X="26.56244184598" Y="-2.685653237596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.039194045716" Y="2.643656915978" />
                  <Point X="28.880172785723" Y="2.286489318202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.29733195841" Y="0.977407386724" />
                  <Point X="26.643487106718" Y="-2.737188968509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.099425910923" Y="2.545373533316" />
                  <Point X="29.038125632624" Y="2.407690854005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.393009738948" Y="0.958736833379" />
                  <Point X="26.724335544554" Y="-2.789166770895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.494165110981" Y="0.952369151964" />
                  <Point X="26.80343815358" Y="-2.845065768992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.604431986624" Y="0.966466242721" />
                  <Point X="27.217755478591" Y="-2.148060187831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.008880350932" Y="-2.617201405707" />
                  <Point X="26.871602062602" Y="-2.925533489555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.714897428321" Y="0.981009320139" />
                  <Point X="27.353992598366" Y="-2.075632973723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.038889726203" Y="-2.783365612165" />
                  <Point X="26.937409075792" Y="-3.011294884827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.825362870018" Y="0.995552397557" />
                  <Point X="28.532242507404" Y="0.337193283945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.351008604787" Y="-0.069864726012" />
                  <Point X="27.478939217466" Y="-2.028564639331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.088273597662" Y="-2.906013987709" />
                  <Point X="27.003216088983" Y="-3.0970562801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.935828311716" Y="1.010095474975" />
                  <Point X="28.67036617169" Y="0.413857746397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.387029030721" Y="-0.222527891631" />
                  <Point X="28.010793522894" Y="-1.067566677859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.866398047662" Y="-1.391884225216" />
                  <Point X="27.581368818336" Y="-2.03207035592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.146986796897" Y="-3.007708349995" />
                  <Point X="27.069023102173" Y="-3.182817675373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.046293753413" Y="1.024638552393" />
                  <Point X="28.789302569792" Y="0.447426903411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.44912534889" Y="-0.316623644379" />
                  <Point X="28.164983124939" Y="-0.954817528392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.882350269079" Y="-1.589621316167" />
                  <Point X="27.677619849934" Y="-2.049453366305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.205700096855" Y="-3.109402486051" />
                  <Point X="27.134830115364" Y="-3.268579070646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.15675919511" Y="1.039181629811" />
                  <Point X="28.907379430409" Y="0.479065507624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.519929501921" Y="-0.391161279806" />
                  <Point X="28.280585095538" Y="-0.928737618169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.941772308886" Y="-1.689723596459" />
                  <Point X="27.773795370025" Y="-2.067005978309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.264413396813" Y="-3.211096622108" />
                  <Point X="27.200637128554" Y="-3.354340465918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.267224636807" Y="1.053724707229" />
                  <Point X="29.025456291025" Y="0.510704111837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.60165216998" Y="-0.441175528962" />
                  <Point X="28.393545046926" Y="-0.908591780254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.003771073011" Y="-1.784038459179" />
                  <Point X="27.861989699416" Y="-2.102484638127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.323126696771" Y="-3.312790758164" />
                  <Point X="27.266444141745" Y="-3.440101861191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.377690078504" Y="1.068267784647" />
                  <Point X="29.143533151642" Y="0.54234271605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.687321680923" Y="-0.482325023862" />
                  <Point X="28.493893345111" Y="-0.916772179211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.076429927589" Y="-1.854410366725" />
                  <Point X="27.944715269011" Y="-2.150246333554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.381839996729" Y="-3.41448489422" />
                  <Point X="27.332251154936" Y="-3.525863256464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.488155520202" Y="1.082810862064" />
                  <Point X="29.261610012259" Y="0.573981320263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.780094712151" Y="-0.507519750977" />
                  <Point X="28.592125842095" Y="-0.929704745473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.153940026149" Y="-1.913886201891" />
                  <Point X="28.027440838606" Y="-2.19800802898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.440553296687" Y="-3.516179030277" />
                  <Point X="27.398058168126" Y="-3.611624651737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.598620961899" Y="1.097353939482" />
                  <Point X="29.379686872876" Y="0.605619924476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.873001508549" Y="-0.532414036602" />
                  <Point X="28.690358339078" Y="-0.942637311735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.23145018465" Y="-1.973361902426" />
                  <Point X="28.110166408201" Y="-2.245769724407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.499266596645" Y="-3.617873166333" />
                  <Point X="27.463865181317" Y="-3.697386047009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.706161803493" Y="1.105328257521" />
                  <Point X="29.497763733493" Y="0.637258528689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.965908304946" Y="-0.557308322228" />
                  <Point X="28.788590836062" Y="-0.955569877996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.30896034315" Y="-2.032837602962" />
                  <Point X="28.192891977796" Y="-2.293531419834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.557979896604" Y="-3.719567302389" />
                  <Point X="27.529672194507" Y="-3.783147442282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.742922771463" Y="0.954328376546" />
                  <Point X="29.61584059411" Y="0.668897132902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.058815101343" Y="-0.582202607853" />
                  <Point X="28.886823333046" Y="-0.968502444258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.386470501651" Y="-2.092313303498" />
                  <Point X="28.275617547391" Y="-2.341293115261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.616693196562" Y="-3.821261438445" />
                  <Point X="27.595479207698" Y="-3.868908837555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.772472862673" Y="0.787132601195" />
                  <Point X="29.733917454727" Y="0.700535737116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.15172189774" Y="-0.607096893479" />
                  <Point X="28.98505583003" Y="-0.98143501052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.463980660152" Y="-2.151789004033" />
                  <Point X="28.358343116987" Y="-2.389054810688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.67540649652" Y="-3.922955574502" />
                  <Point X="27.661286220888" Y="-3.954670232828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.244628694138" Y="-0.631991179105" />
                  <Point X="29.083288327013" Y="-0.994367576781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.541490818653" Y="-2.211264704569" />
                  <Point X="28.441068686582" Y="-2.436816506114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.734119796478" Y="-4.024649710558" />
                  <Point X="27.731361426324" Y="-4.030845111359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.337535490535" Y="-0.65688546473" />
                  <Point X="29.181520823997" Y="-1.007300143043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.619000977154" Y="-2.270740405104" />
                  <Point X="28.523794256177" Y="-2.484578201541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.430442286932" Y="-0.681779750356" />
                  <Point X="29.279753320981" Y="-1.020232709305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.696511135654" Y="-2.33021610564" />
                  <Point X="28.606519825772" Y="-2.532339896968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.52334908333" Y="-0.706674035981" />
                  <Point X="29.377985817965" Y="-1.033165275566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.774021294155" Y="-2.389691806175" />
                  <Point X="28.689245395367" Y="-2.580101592395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.616255879727" Y="-0.731568321607" />
                  <Point X="29.476218314948" Y="-1.046097841828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.851531452656" Y="-2.449167506711" />
                  <Point X="28.771970964962" Y="-2.627863287821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.709162676124" Y="-0.756462607233" />
                  <Point X="29.574450811932" Y="-1.05903040809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.929041611157" Y="-2.508643207247" />
                  <Point X="28.854696534557" Y="-2.675624983248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.775029701037" Y="-0.842089213969" />
                  <Point X="29.672683308916" Y="-1.071962974351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.006551769658" Y="-2.568118907782" />
                  <Point X="28.937422104152" Y="-2.723386678675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.084061928159" Y="-2.627594608318" />
                  <Point X="29.072246582864" Y="-2.654132308346" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001625976562" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.8502421875" Y="-4.950448730469" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.464318359375" Y="-3.521542236328" />
                  <Point X="25.309533203125" Y="-3.298526123047" />
                  <Point X="25.30058984375" Y="-3.285641845703" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.034814453125" Y="-3.192061279297" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.991337890625" Y="-3.187766113281" />
                  <Point X="24.751818359375" Y="-3.262104492188" />
                  <Point X="24.737978515625" Y="-3.266399414062" />
                  <Point X="24.71171875" Y="-3.285646240234" />
                  <Point X="24.55693359375" Y="-3.508662109375" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.51889453125" Y="-3.618765625" />
                  <Point X="24.152255859375" Y="-4.987077148438" />
                  <Point X="23.914654296875" Y="-4.940958007812" />
                  <Point X="23.899759765625" Y="-4.93806640625" />
                  <Point X="23.648412109375" Y="-4.873396484375" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534755371094" />
                  <Point X="23.633095703125" Y="-4.247083496094" />
                  <Point X="23.6297890625" Y="-4.230463867188" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.393197265625" Y="-4.009236816406" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.058080078125" Y="-3.966579589844" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.010119140625" Y="-3.973792724609" />
                  <Point X="22.7662421875" Y="-4.13674609375" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.727982421875" Y="-4.173302246094" />
                  <Point X="22.542904296875" Y="-4.414500488281" />
                  <Point X="22.166" Y="-4.181130371094" />
                  <Point X="22.144169921875" Y="-4.167612792969" />
                  <Point X="21.771419921875" Y="-3.880608154297" />
                  <Point X="21.79019140625" Y="-3.848094482422" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.59759375" />
                  <Point X="22.486021484375" Y="-2.568765136719" />
                  <Point X="22.469625" Y="-2.552367919922" />
                  <Point X="22.468677734375" Y="-2.551420166016" />
                  <Point X="22.43984765625" Y="-2.53719921875" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="22.337892578125" Y="-2.584129394531" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.855546875" Y="-2.869791748047" />
                  <Point X="20.838302734375" Y="-2.847136474609" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="20.6048359375" Y="-2.368014892578" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.39601574707" />
                  <Point X="21.861462890625" Y="-1.367893188477" />
                  <Point X="21.861890625" Y="-1.366241210938" />
                  <Point X="21.859669921875" Y="-1.334584106445" />
                  <Point X="21.838841796875" Y="-1.310639770508" />
                  <Point X="21.813806640625" Y="-1.295904418945" />
                  <Point X="21.812349609375" Y="-1.295046386719" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.691568359375" Y="-1.300275634766" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.07935546875" Y="-1.037606811523" />
                  <Point X="20.072609375" Y="-1.01119152832" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="20.0398125" Y="-0.504504608154" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.458107421875" Y="-0.121423377991" />
                  <Point X="21.484345703125" Y="-0.103213386536" />
                  <Point X="21.485861328125" Y="-0.102161346436" />
                  <Point X="21.5051015625" Y="-0.075908363342" />
                  <Point X="21.51384765625" Y="-0.047729434967" />
                  <Point X="21.51436328125" Y="-0.046064220428" />
                  <Point X="21.5143515625" Y="-0.016458742142" />
                  <Point X="21.5056171875" Y="0.011683167458" />
                  <Point X="21.50511328125" Y="0.013311170578" />
                  <Point X="21.485861328125" Y="0.039601272583" />
                  <Point X="21.459623046875" Y="0.057811260223" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="21.361498046875" Y="0.087800071716" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.0780078125" Y="0.967039672852" />
                  <Point X="20.08235546875" Y="0.99642010498" />
                  <Point X="20.226484375" Y="1.528298706055" />
                  <Point X="20.2412265625" Y="1.526357910156" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268294921875" Y="1.395865722656" />
                  <Point X="21.326365234375" Y="1.414175537109" />
                  <Point X="21.329720703125" Y="1.415233154297" />
                  <Point X="21.34846484375" Y="1.426055175781" />
                  <Point X="21.360880859375" Y="1.443786132813" />
                  <Point X="21.384181640625" Y="1.500040405273" />
                  <Point X="21.38553125" Y="1.503296142578" />
                  <Point X="21.389287109375" Y="1.524608398438" />
                  <Point X="21.383685546875" Y="1.54551171875" />
                  <Point X="21.3555703125" Y="1.599520996094" />
                  <Point X="21.35394140625" Y="1.602648681641" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.293546875" Y="1.654890991211" />
                  <Point X="20.52389453125" Y="2.245465820312" />
                  <Point X="20.82309375" Y="2.758065185547" />
                  <Point X="20.839990234375" Y="2.787010253906" />
                  <Point X="21.225330078125" Y="3.282310791016" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.94236328125" Y="2.913358886719" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775634766" />
                  <Point X="21.986748046875" Y="2.927404785156" />
                  <Point X="22.04415625" Y="2.984811767578" />
                  <Point X="22.04747265625" Y="2.988128173828" />
                  <Point X="22.0591015625" Y="3.006382568359" />
                  <Point X="22.061927734375" Y="3.027841308594" />
                  <Point X="22.0548515625" Y="3.108718261719" />
                  <Point X="22.054443359375" Y="3.113384521484" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="22.027333984375" Y="3.169711181641" />
                  <Point X="21.69272265625" Y="3.749276123047" />
                  <Point X="22.217708984375" Y="4.15177734375" />
                  <Point X="22.24712890625" Y="4.174333984375" />
                  <Point X="22.858451171875" Y="4.513971679688" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.13876953125" Y="4.226801269531" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164875" Y="4.2184921875" />
                  <Point X="23.186189453125" Y="4.222248535156" />
                  <Point X="23.2799453125" Y="4.261084472656" />
                  <Point X="23.28536328125" Y="4.263328125" />
                  <Point X="23.30309375" Y="4.275743164062" />
                  <Point X="23.31391796875" Y="4.294488769531" />
                  <Point X="23.34443359375" Y="4.391273925781" />
                  <Point X="23.3461953125" Y="4.396859375" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.3457421875" Y="4.436215332031" />
                  <Point X="23.31086328125" Y="4.701141113281" />
                  <Point X="23.993822265625" Y="4.892619140625" />
                  <Point X="24.03190625" Y="4.903296386719" />
                  <Point X="24.775537109375" Y="4.990327148438" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.0650078125" Y="4.350293945312" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.82694921875" Y="4.929048339844" />
                  <Point X="25.86020703125" Y="4.925564941406" />
                  <Point X="26.4754296875" Y="4.777031738281" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.90980078125" Y="4.623487304688" />
                  <Point X="26.9310390625" Y="4.615784179688" />
                  <Point X="27.31825" Y="4.434698730469" />
                  <Point X="27.338701171875" Y="4.425134277344" />
                  <Point X="27.71276171875" Y="4.207206054688" />
                  <Point X="27.732515625" Y="4.195696289062" />
                  <Point X="28.0687421875" Y="3.956592285156" />
                  <Point X="28.044421875" Y="3.914469970703" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491514648438" />
                  <Point X="27.204556640625" Y="2.415613525391" />
                  <Point X="27.204556640625" Y="2.41561328125" />
                  <Point X="27.202044921875" Y="2.392328613281" />
                  <Point X="27.209958984375" Y="2.326695800781" />
                  <Point X="27.210416015625" Y="2.322903808594" />
                  <Point X="27.218681640625" Y="2.3008125" />
                  <Point X="27.25929296875" Y="2.240961669922" />
                  <Point X="27.25930078125" Y="2.240949951172" />
                  <Point X="27.27494140625" Y="2.224204101562" />
                  <Point X="27.334791015625" Y="2.183592773438" />
                  <Point X="27.3382578125" Y="2.181241455078" />
                  <Point X="27.3603359375" Y="2.17298046875" />
                  <Point X="27.42596875" Y="2.165066162109" />
                  <Point X="27.448666015625" Y="2.165946533203" />
                  <Point X="27.524568359375" Y="2.186243652344" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="27.622546875" Y="2.239478027344" />
                  <Point X="28.99425" Y="3.031430908203" />
                  <Point X="29.19087109375" Y="2.758170898438" />
                  <Point X="29.20259375" Y="2.741879638672" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="29.361072265625" Y="2.416005859375" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.279373046875" Y="1.583835083008" />
                  <Point X="28.224748046875" Y="1.512570922852" />
                  <Point X="28.21312109375" Y="1.491502075195" />
                  <Point X="28.192771484375" Y="1.418741210938" />
                  <Point X="28.191595703125" Y="1.414537597656" />
                  <Point X="28.190779296875" Y="1.390967163086" />
                  <Point X="28.207484375" Y="1.310011352539" />
                  <Point X="28.2156484375" Y="1.287954589844" />
                  <Point X="28.261072265625" Y="1.218912719727" />
                  <Point X="28.2636953125" Y="1.214922607422" />
                  <Point X="28.28094921875" Y="1.198819702148" />
                  <Point X="28.346783203125" Y="1.16176159668" />
                  <Point X="28.346783203125" Y="1.161761108398" />
                  <Point X="28.368568359375" Y="1.153619384766" />
                  <Point X="28.4575859375" Y="1.141854370117" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.553234375" Y="1.151365600586" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.934154296875" Y="0.972069274902" />
                  <Point X="29.93919140625" Y="0.951375061035" />
                  <Point X="29.997859375" Y="0.574556152344" />
                  <Point X="29.970224609375" Y="0.567151245117" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819351196" />
                  <Point X="28.641630859375" Y="0.182267471313" />
                  <Point X="28.622263671875" Y="0.166924255371" />
                  <Point X="28.5697890625" Y="0.100059577942" />
                  <Point X="28.5667578125" Y="0.096196586609" />
                  <Point X="28.556986328125" Y="0.074735565186" />
                  <Point X="28.539494140625" Y="-0.016598384857" />
                  <Point X="28.5394921875" Y="-0.016605688095" />
                  <Point X="28.538484375" Y="-0.040685081482" />
                  <Point X="28.555974609375" Y="-0.132011474609" />
                  <Point X="28.556984375" Y="-0.137288192749" />
                  <Point X="28.5667578125" Y="-0.158756515503" />
                  <Point X="28.619232421875" Y="-0.225621185303" />
                  <Point X="28.636578125" Y="-0.241907058716" />
                  <Point X="28.72403515625" Y="-0.292458953857" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="28.81217578125" Y="-0.31941317749" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.951244140625" Y="-0.947753295898" />
                  <Point X="29.948431640625" Y="-0.966412902832" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.837341796875" Y="-1.285280273438" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.3948359375" Y="-1.098341186523" />
                  <Point X="28.2231875" Y="-1.135649536133" />
                  <Point X="28.213271484375" Y="-1.137805053711" />
                  <Point X="28.185447265625" Y="-1.154696655273" />
                  <Point X="28.081697265625" Y="-1.279475830078" />
                  <Point X="28.075703125" Y="-1.286684814453" />
                  <Point X="28.064359375" Y="-1.314069091797" />
                  <Point X="28.049490234375" Y="-1.475663452148" />
                  <Point X="28.048630859375" Y="-1.484999389648" />
                  <Point X="28.05636328125" Y="-1.516622802734" />
                  <Point X="28.15135546875" Y="-1.664377075195" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.234357421875" Y="-1.736103637695" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.212056640625" Y="-2.789318847656" />
                  <Point X="29.204134765625" Y="-2.802136474609" />
                  <Point X="29.056689453125" Y="-3.011638183594" />
                  <Point X="29.0229296875" Y="-2.992147216797" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.73733984375" Y="-2.253312744141" />
                  <Point X="27.533052734375" Y="-2.216418457031" />
                  <Point X="27.52125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.319365234375" Y="-2.308563720703" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334684082031" />
                  <Point X="27.19928125" Y="-2.504397216797" />
                  <Point X="27.19412109375" Y="-2.514202148438" />
                  <Point X="27.1891640625" Y="-2.546374511719" />
                  <Point X="27.22605859375" Y="-2.750662353516" />
                  <Point X="27.22605859375" Y="-2.750667480469" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.2764375" Y="-2.851920654297" />
                  <Point X="27.98667578125" Y="-4.082087890625" />
                  <Point X="27.844703125" Y="-4.183494628906" />
                  <Point X="27.835287109375" Y="-4.190220214844" />
                  <Point X="27.67977734375" Y="-4.290879882812" />
                  <Point X="27.651904296875" Y="-4.254555175781" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467041016" />
                  <Point X="26.46906640625" Y="-2.850931884766" />
                  <Point X="26.45742578125" Y="-2.843448242188" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.20544921875" Y="-2.855994384766" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509521484" />
                  <Point X="25.9951796875" Y="-3.009986328125" />
                  <Point X="25.985349609375" Y="-3.018159912109" />
                  <Point X="25.96845703125" Y="-3.045985595703" />
                  <Point X="25.91758203125" Y="-3.280050537109" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.9259296875" Y="-3.401869140625" />
                  <Point X="26.127642578125" Y="-4.934028320312" />
                  <Point X="26.00323828125" Y="-4.961297851562" />
                  <Point X="25.994345703125" Y="-4.963247558594" />
                  <Point X="25.860201171875" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#212" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.18268991153" Y="5.036932540226" Z="2.4" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.4" />
                  <Point X="-0.236432156461" Y="5.071268555169" Z="2.4" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.4" />
                  <Point X="-1.025832072416" Y="4.97204254565" Z="2.4" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.4" />
                  <Point X="-1.709987757464" Y="4.460968804913" Z="2.4" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.4" />
                  <Point X="-1.709408924438" Y="4.437588953684" Z="2.4" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.4" />
                  <Point X="-1.745341835888" Y="4.338560414803" Z="2.4" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.4" />
                  <Point X="-1.84429957467" Y="4.302431820138" Z="2.4" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.4" />
                  <Point X="-2.123367513053" Y="4.595669203726" Z="2.4" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.4" />
                  <Point X="-2.169913915662" Y="4.590111323244" Z="2.4" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.4" />
                  <Point X="-2.818872437545" Y="4.222736520606" Z="2.4" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.4" />
                  <Point X="-3.02212360544" Y="3.17599128166" Z="2.4" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.4" />
                  <Point X="-3.001115888145" Y="3.135640367216" Z="2.4" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.4" />
                  <Point X="-2.997356616673" Y="3.051446951214" Z="2.4" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.4" />
                  <Point X="-3.05943609839" Y="2.994448811028" Z="2.4" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.4" />
                  <Point X="-3.757868066637" Y="3.358070343305" Z="2.4" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.4" />
                  <Point X="-3.81616537674" Y="3.34959580224" Z="2.4" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.4" />
                  <Point X="-4.226244446013" Y="2.814551610882" Z="2.4" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.4" />
                  <Point X="-3.743047307239" Y="1.646503325818" Z="2.4" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.4" />
                  <Point X="-3.694937955841" Y="1.607713804039" Z="2.4" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.4" />
                  <Point X="-3.66816863159" Y="1.550454371025" Z="2.4" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.4" />
                  <Point X="-3.694824887816" Y="1.493142290329" Z="2.4" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.4" />
                  <Point X="-4.758404097995" Y="1.607210217955" Z="2.4" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.4" />
                  <Point X="-4.825034608714" Y="1.583347681407" Z="2.4" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.4" />
                  <Point X="-4.977609652661" Y="1.005639324835" Z="2.4" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.4" />
                  <Point X="-3.657601226934" Y="0.070783766587" Z="2.4" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.4" />
                  <Point X="-3.57504492644" Y="0.048016973882" Z="2.4" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.4" />
                  <Point X="-3.548302580985" Y="0.028178952145" Z="2.4" />
                  <Point X="-3.539556741714" Y="0" Z="2.4" />
                  <Point X="-3.54006202015" Y="-0.001627998916" Z="2.4" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.4" />
                  <Point X="-3.550323668683" Y="-0.030859009252" Z="2.4" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.4" />
                  <Point X="-4.979286561839" Y="-0.424928290169" Z="2.4" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.4" />
                  <Point X="-5.056085120672" Y="-0.476302168585" Z="2.4" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.4" />
                  <Point X="-4.975246858762" Y="-1.018699734387" Z="2.4" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.4" />
                  <Point X="-3.308063189821" Y="-1.318567779107" Z="2.4" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.4" />
                  <Point X="-3.217712459651" Y="-1.307714612656" Z="2.4" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.4" />
                  <Point X="-3.193097110694" Y="-1.324074547895" Z="2.4" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.4" />
                  <Point X="-4.43175944899" Y="-2.29706719851" Z="2.4" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.4" />
                  <Point X="-4.486867691943" Y="-2.378540461854" Z="2.4" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.4" />
                  <Point X="-4.1903960628" Y="-2.868795069957" Z="2.4" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.4" />
                  <Point X="-2.643264037996" Y="-2.596150596871" Z="2.4" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.4" />
                  <Point X="-2.571891954956" Y="-2.556438514175" Z="2.4" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.4" />
                  <Point X="-3.259266416298" Y="-3.791814344497" Z="2.4" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.4" />
                  <Point X="-3.277562634128" Y="-3.879457963605" Z="2.4" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.4" />
                  <Point X="-2.86647862735" Y="-4.192360430116" Z="2.4" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.4" />
                  <Point X="-2.238505639467" Y="-4.172460163397" Z="2.4" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.4" />
                  <Point X="-2.212132649631" Y="-4.147037777288" Z="2.4" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.4" />
                  <Point X="-1.951346826908" Y="-3.985192654499" Z="2.4" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.4" />
                  <Point X="-1.645926584536" Y="-4.015548862008" Z="2.4" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.4" />
                  <Point X="-1.422100085413" Y="-4.225560340652" Z="2.4" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.4" />
                  <Point X="-1.410465337788" Y="-4.859498224429" Z="2.4" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.4" />
                  <Point X="-1.396948631784" Y="-4.883658612471" Z="2.4" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.4" />
                  <Point X="-1.101030951122" Y="-4.958761219271" Z="2.4" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.4" />
                  <Point X="-0.438965768003" Y="-3.600426579333" Z="2.4" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.4" />
                  <Point X="-0.408144284789" Y="-3.505888648101" Z="2.4" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.4" />
                  <Point X="-0.23952108883" Y="-3.278577875258" Z="2.4" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.4" />
                  <Point X="0.01383799053" Y="-3.20853417003" Z="2.4" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.4" />
                  <Point X="0.262301574644" Y="-3.295757107667" Z="2.4" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.4" />
                  <Point X="0.795789092398" Y="-4.932109450482" Z="2.4" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.4" />
                  <Point X="0.827518021279" Y="-5.011973576113" Z="2.4" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.4" />
                  <Point X="1.007789136268" Y="-4.978857830813" Z="2.4" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.4" />
                  <Point X="0.96934577217" Y="-3.364062645198" Z="2.4" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.4" />
                  <Point X="0.960285014622" Y="-3.259390926651" Z="2.4" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.4" />
                  <Point X="1.020990384989" Y="-3.017152384156" Z="2.4" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.4" />
                  <Point X="1.203874503196" Y="-2.874503944451" Z="2.4" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.4" />
                  <Point X="1.435870852908" Y="-2.861710288658" Z="2.4" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.4" />
                  <Point X="2.606079902576" Y="-4.253713321856" Z="2.4" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.4" />
                  <Point X="2.672709592911" Y="-4.319748715576" Z="2.4" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.4" />
                  <Point X="2.867609453893" Y="-4.19290132919" Z="2.4" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.4" />
                  <Point X="2.313580979555" Y="-2.795640694662" Z="2.4" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.4" />
                  <Point X="2.269105382521" Y="-2.710496173684" Z="2.4" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.4" />
                  <Point X="2.237371380661" Y="-2.496403250868" Z="2.4" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.4" />
                  <Point X="2.336495127504" Y="-2.321529929306" Z="2.4" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.4" />
                  <Point X="2.518010629829" Y="-2.23434262697" Z="2.4" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.4" />
                  <Point X="3.991772902444" Y="-3.00416852072" Z="2.4" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.4" />
                  <Point X="4.074651691996" Y="-3.032962242477" Z="2.4" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.4" />
                  <Point X="4.248433089604" Y="-2.784324266097" Z="2.4" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.4" />
                  <Point X="3.258637685187" Y="-1.665155940725" Z="2.4" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.4" />
                  <Point X="3.187254780891" Y="-1.60605673778" Z="2.4" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.4" />
                  <Point X="3.093121591814" Y="-1.448966612158" Z="2.4" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.4" />
                  <Point X="3.113985469248" Y="-1.280163295571" Z="2.4" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.4" />
                  <Point X="3.227652227018" Y="-1.15322874758" Z="2.4" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.4" />
                  <Point X="4.824658661359" Y="-1.303572456453" Z="2.4" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.4" />
                  <Point X="4.911618236444" Y="-1.294205581716" Z="2.4" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.4" />
                  <Point X="4.994528502033" Y="-0.923926713606" Z="2.4" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.4" />
                  <Point X="3.818959633672" Y="-0.239836886629" Z="2.4" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.4" />
                  <Point X="3.742899980417" Y="-0.217890074587" Z="2.4" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.4" />
                  <Point X="3.652411083145" Y="-0.163475196355" Z="2.4" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.4" />
                  <Point X="3.59892617986" Y="-0.09133384794" Z="2.4" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.4" />
                  <Point X="3.582445270564" Y="0.005276683236" Z="2.4" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.4" />
                  <Point X="3.602968355256" Y="0.10047354222" Z="2.4" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.4" />
                  <Point X="3.660495433935" Y="0.170258769655" Z="2.4" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.4" />
                  <Point X="4.977008183635" Y="0.550135046139" Z="2.4" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.4" />
                  <Point X="5.044415663147" Y="0.592280004557" Z="2.4" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.4" />
                  <Point X="4.976578573803" Y="1.015173865753" Z="2.4" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.4" />
                  <Point X="3.540551525485" Y="1.232217953817" Z="2.4" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.4" />
                  <Point X="3.457978530828" Y="1.222703777824" Z="2.4" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.4" />
                  <Point X="3.365156901871" Y="1.236609742565" Z="2.4" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.4" />
                  <Point X="3.2966937398" Y="1.277660630194" Z="2.4" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.4" />
                  <Point X="3.250295950775" Y="1.351393504514" Z="2.4" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.4" />
                  <Point X="3.23476763069" Y="1.436552859337" Z="2.4" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.4" />
                  <Point X="3.258271938154" Y="1.51343089103" Z="2.4" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.4" />
                  <Point X="4.385352360735" Y="2.407618124804" Z="2.4" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.4" />
                  <Point X="4.435889710313" Y="2.474036585782" Z="2.4" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.4" />
                  <Point X="4.225298320337" Y="2.818655531677" Z="2.4" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.4" />
                  <Point X="2.59138905138" Y="2.314059238726" Z="2.4" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.4" />
                  <Point X="2.50549283725" Y="2.265826139558" Z="2.4" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.4" />
                  <Point X="2.425799801643" Y="2.245986402175" Z="2.4" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.4" />
                  <Point X="2.35670898658" Y="2.256246787236" Z="2.4" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.4" />
                  <Point X="2.294511812999" Y="2.300315873802" Z="2.4" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.4" />
                  <Point X="2.253443300584" Y="2.363958642189" Z="2.4" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.4" />
                  <Point X="2.2467018031" Y="2.433976738632" Z="2.4" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.4" />
                  <Point X="3.081566114134" Y="3.920750030575" Z="2.4" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.4" />
                  <Point X="3.108137767126" Y="4.016831677149" Z="2.4" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.4" />
                  <Point X="2.731774304051" Y="4.281688183243" Z="2.4" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.4" />
                  <Point X="2.333274730504" Y="4.511270283326" Z="2.4" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.4" />
                  <Point X="1.92069327249" Y="4.701771298796" Z="2.4" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.4" />
                  <Point X="1.481007573357" Y="4.856915338043" Z="2.4" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.4" />
                  <Point X="0.82600200102" Y="5.010056428966" Z="2.4" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.4" />
                  <Point X="0.010554650023" Y="4.394514965725" Z="2.4" />
                  <Point X="0" Y="4.355124473572" Z="2.4" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>