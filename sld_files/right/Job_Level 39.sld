<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#193" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2816" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004715576172" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766443847656" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.86315234375" Y="-4.631571777344" />
                  <Point X="25.5633046875" Y="-3.512524902344" />
                  <Point X="25.55772265625" Y="-3.497139648438" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.41980859375" Y="-3.290797851562" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209021728516" />
                  <Point X="25.33049609375" Y="-3.18977734375" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.112849609375" Y="-3.116809570313" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766357422" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.773529296875" Y="-3.155895263672" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.18977734375" />
                  <Point X="24.655560546875" Y="-3.209021484375" />
                  <Point X="24.63367578125" Y="-3.231477050781" />
                  <Point X="24.51112109375" Y="-3.408055908203" />
                  <Point X="24.46994921875" Y="-3.467377441406" />
                  <Point X="24.4618125" Y="-3.481571777344" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.348275390625" Y="-3.888467041016" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.9917578125" Y="-4.859150390625" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.75358203125" Y="-4.802362304688" />
                  <Point X="23.7850390625" Y="-4.563436523438" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.51622265625" />
                  <Point X="23.738185546875" Y="-4.288450683594" />
                  <Point X="23.722962890625" Y="-4.211931152344" />
                  <Point X="23.712060546875" Y="-4.182962890625" />
                  <Point X="23.69598828125" Y="-4.155126464844" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.50175390625" Y="-3.978080810547" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.125234375" Y="-3.87577734375" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.76424609375" Y="-4.023824462891" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461425781" />
                  <Point X="22.60834375" Y="-4.1731640625" />
                  <Point X="22.5198515625" Y="-4.288488769531" />
                  <Point X="22.3025" Y="-4.15391015625" />
                  <Point X="22.19828515625" Y="-4.089383544922" />
                  <Point X="21.89765625" Y="-3.857907958984" />
                  <Point X="21.895279296875" Y="-3.856077148438" />
                  <Point X="22.019005859375" Y="-3.641775634766" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647654541016" />
                  <Point X="22.593412109375" Y="-2.616127929688" />
                  <Point X="22.59442578125" Y="-2.585193847656" />
                  <Point X="22.58544140625" Y="-2.555575927734" />
                  <Point X="22.571224609375" Y="-2.526746826172" />
                  <Point X="22.5531953125" Y="-2.501587890625" />
                  <Point X="22.5358515625" Y="-2.484244628906" />
                  <Point X="22.510705078125" Y="-2.466222412109" />
                  <Point X="22.481875" Y="-2.452000976562" />
                  <Point X="22.45225390625" Y="-2.443012939453" />
                  <Point X="22.42131640625" Y="-2.444023925781" />
                  <Point X="22.389787109375" Y="-2.450294433594" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="22.03657421875" Y="-2.648399169922" />
                  <Point X="21.181978515625" Y="-3.141800537109" />
                  <Point X="20.999470703125" Y="-2.902025146484" />
                  <Point X="20.917134765625" Y="-2.793849365234" />
                  <Point X="20.701609375" Y="-2.432448242188" />
                  <Point X="20.693857421875" Y="-2.419449951172" />
                  <Point X="20.919068359375" Y="-2.246639892578" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.915421875" Y="-1.47559375" />
                  <Point X="21.93338671875" Y="-1.448462768555" />
                  <Point X="21.946142578125" Y="-1.419834960938" />
                  <Point X="21.95191015625" Y="-1.397568237305" />
                  <Point X="21.95384765625" Y="-1.390087646484" />
                  <Point X="21.95665234375" Y="-1.359656860352" />
                  <Point X="21.954443359375" Y="-1.327985107422" />
                  <Point X="21.94744140625" Y="-1.298238769531" />
                  <Point X="21.931357421875" Y="-1.272254394531" />
                  <Point X="21.9105234375" Y="-1.248297729492" />
                  <Point X="21.88702734375" Y="-1.228766845703" />
                  <Point X="21.867205078125" Y="-1.217099853516" />
                  <Point X="21.860544921875" Y="-1.213180297852" />
                  <Point X="21.831279296875" Y="-1.201955444336" />
                  <Point X="21.79939453125" Y="-1.195474609375" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="21.35874609375" Y="-1.248272705078" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.198125" Y="-1.118718505859" />
                  <Point X="20.165923828125" Y="-0.992649841309" />
                  <Point X="20.1088984375" Y="-0.593948059082" />
                  <Point X="20.107576171875" Y="-0.584698608398" />
                  <Point X="20.35648046875" Y="-0.518005065918" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.4825078125" Y="-0.214827133179" />
                  <Point X="21.5122734375" Y="-0.199468673706" />
                  <Point X="21.533033203125" Y="-0.185060073853" />
                  <Point X="21.540013671875" Y="-0.180216949463" />
                  <Point X="21.5624765625" Y="-0.158329238892" />
                  <Point X="21.581724609375" Y="-0.132070251465" />
                  <Point X="21.5958359375" Y="-0.104063110352" />
                  <Point X="21.602759765625" Y="-0.081751747131" />
                  <Point X="21.6050859375" Y="-0.07425617981" />
                  <Point X="21.609353515625" Y="-0.046099822998" />
                  <Point X="21.609353515625" Y="-0.016460323334" />
                  <Point X="21.6050859375" Y="0.011696035385" />
                  <Point X="21.598162109375" Y="0.034007553101" />
                  <Point X="21.5958359375" Y="0.041503112793" />
                  <Point X="21.581724609375" Y="0.069507835388" />
                  <Point X="21.56248046875" Y="0.095763168335" />
                  <Point X="21.5400234375" Y="0.1176483078" />
                  <Point X="21.519248046875" Y="0.132066574097" />
                  <Point X="21.50990625" Y="0.137781784058" />
                  <Point X="21.487359375" Y="0.149846847534" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="21.094005859375" Y="0.257825653076" />
                  <Point X="20.108185546875" Y="0.521975402832" />
                  <Point X="20.15476171875" Y="0.83673828125" />
                  <Point X="20.17551171875" Y="0.976968383789" />
                  <Point X="20.29030078125" Y="1.400575927734" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.438197265625" Y="1.404606567383" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228027344" />
                  <Point X="21.296865234375" Y="1.305263549805" />
                  <Point X="21.34284375" Y="1.319760742188" />
                  <Point X="21.358291015625" Y="1.324631103516" />
                  <Point X="21.377224609375" Y="1.332963012695" />
                  <Point X="21.39596875" Y="1.343785766602" />
                  <Point X="21.4126484375" Y="1.356015625" />
                  <Point X="21.42628515625" Y="1.371565307617" />
                  <Point X="21.438701171875" Y="1.389296386719" />
                  <Point X="21.44865234375" Y="1.407433349609" />
                  <Point X="21.4671015625" Y="1.451974365234" />
                  <Point X="21.473298828125" Y="1.46693762207" />
                  <Point X="21.479087890625" Y="1.486805297852" />
                  <Point X="21.48284375" Y="1.508118896484" />
                  <Point X="21.4841953125" Y="1.528750732422" />
                  <Point X="21.48105078125" Y="1.549186157227" />
                  <Point X="21.475451171875" Y="1.570090942383" />
                  <Point X="21.467953125" Y="1.589374389648" />
                  <Point X="21.445693359375" Y="1.632137817383" />
                  <Point X="21.438201171875" Y="1.646524902344" />
                  <Point X="21.426712890625" Y="1.663714355469" />
                  <Point X="21.412802734375" Y="1.680290161133" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="21.183841796875" Y="1.858815551758" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.838216796875" Y="2.595518310547" />
                  <Point X="20.918853515625" Y="2.733665527344" />
                  <Point X="21.2229140625" Y="3.124494873047" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.30437890625" Y="3.126975585938" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.917244140625" Y="2.820193847656" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.00098828125" Y="2.826505126953" />
                  <Point X="22.019541015625" Y="2.835655029297" />
                  <Point X="22.037794921875" Y="2.847284912109" />
                  <Point X="22.053921875" Y="2.860228759766" />
                  <Point X="22.099376953125" Y="2.905682128906" />
                  <Point X="22.114646484375" Y="2.920952148438" />
                  <Point X="22.12759375" Y="2.937083007812" />
                  <Point X="22.13922265625" Y="2.955335693359" />
                  <Point X="22.14837109375" Y="2.973885498047" />
                  <Point X="22.153287109375" Y="2.993975585938" />
                  <Point X="22.15611328125" Y="3.015432373047" />
                  <Point X="22.15656640625" Y="3.036119384766" />
                  <Point X="22.150962890625" Y="3.100156005859" />
                  <Point X="22.14908203125" Y="3.121668945312" />
                  <Point X="22.145046875" Y="3.141956054688" />
                  <Point X="22.138537109375" Y="3.162601318359" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="22.035365234375" Y="3.345800537109" />
                  <Point X="21.81666796875" Y="3.724595703125" />
                  <Point X="22.158947265625" Y="3.987017822266" />
                  <Point X="22.29937890625" Y="4.094685791016" />
                  <Point X="22.77826953125" Y="4.360747070312" />
                  <Point X="22.83296484375" Y="4.391134277344" />
                  <Point X="22.9568046875" Y="4.229742675781" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.076158203125" Y="4.15229296875" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.11938671875" Y="4.132330078125" />
                  <Point X="23.140294921875" Y="4.126728515625" />
                  <Point X="23.160734375" Y="4.123582519531" />
                  <Point X="23.181373046875" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128692871094" />
                  <Point X="23.222548828125" Y="4.134481933594" />
                  <Point X="23.296783203125" Y="4.165231445312" />
                  <Point X="23.32172265625" Y="4.175561523437" />
                  <Point X="23.33985546875" Y="4.185509277344" />
                  <Point X="23.3575859375" Y="4.197923828125" />
                  <Point X="23.373140625" Y="4.211564453125" />
                  <Point X="23.385373046875" Y="4.228249511719" />
                  <Point X="23.396193359375" Y="4.246993652344" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.42868359375" Y="4.342553710938" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410145019531" />
                  <Point X="23.442271484375" Y="4.430827148438" />
                  <Point X="23.43148828125" Y="4.5127265625" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.868572265625" Y="4.75883984375" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.630927734375" Y="4.877754394531" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.721447265625" Y="4.826155273438" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.1948125" Y="4.467673828125" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.685302734375" Y="4.84836328125" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.324359375" Y="4.715775390625" />
                  <Point X="26.4810234375" Y="4.677951660156" />
                  <Point X="26.793294921875" Y="4.564688476562" />
                  <Point X="26.894650390625" Y="4.527926757812" />
                  <Point X="27.19695703125" Y="4.386548339844" />
                  <Point X="27.294576171875" Y="4.340894042969" />
                  <Point X="27.58664453125" Y="4.170734863281" />
                  <Point X="27.680978515625" Y="4.115775878906" />
                  <Point X="27.94326171875" Y="3.929254394531" />
                  <Point X="27.7925390625" Y="3.668193847656" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.14208203125" Y="2.539942382812" />
                  <Point X="27.133078125" Y="2.516059326172" />
                  <Point X="27.1170078125" Y="2.455962402344" />
                  <Point X="27.111607421875" Y="2.435772949219" />
                  <Point X="27.108619140625" Y="2.417937255859" />
                  <Point X="27.107728515625" Y="2.38094921875" />
                  <Point X="27.11399609375" Y="2.328982666016" />
                  <Point X="27.116099609375" Y="2.311530273438" />
                  <Point X="27.121439453125" Y="2.289617431641" />
                  <Point X="27.129705078125" Y="2.267523925781" />
                  <Point X="27.140072265625" Y="2.247469482422" />
                  <Point X="27.172228515625" Y="2.200081054688" />
                  <Point X="27.1775546875" Y="2.192971679688" />
                  <Point X="27.201658203125" Y="2.163751708984" />
                  <Point X="27.221599609375" Y="2.145592285156" />
                  <Point X="27.26898828125" Y="2.113437011719" />
                  <Point X="27.284908203125" Y="2.102634765625" />
                  <Point X="27.304953125" Y="2.092272216797" />
                  <Point X="27.327041015625" Y="2.084006347656" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.400931640625" Y="2.072397216797" />
                  <Point X="27.410298828125" Y="2.071735107422" />
                  <Point X="27.446658203125" Y="2.070967529297" />
                  <Point X="27.473205078125" Y="2.074170898438" />
                  <Point X="27.533302734375" Y="2.090241699219" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="27.963822265625" Y="2.326817138672" />
                  <Point X="28.967326171875" Y="2.906190429688" />
                  <Point X="29.067314453125" Y="2.767229492188" />
                  <Point X="29.12328125" Y="2.689448974609" />
                  <Point X="29.26219921875" Y="2.459883544922" />
                  <Point X="29.0799296875" Y="2.320022949219" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.221423828125" Y="1.660239379883" />
                  <Point X="28.203974609375" Y="1.641626586914" />
                  <Point X="28.16072265625" Y="1.585201293945" />
                  <Point X="28.146193359375" Y="1.566245239258" />
                  <Point X="28.136609375" Y="1.550916381836" />
                  <Point X="28.121630859375" Y="1.517089477539" />
                  <Point X="28.10551953125" Y="1.459479125977" />
                  <Point X="28.10010546875" Y="1.440125" />
                  <Point X="28.09665234375" Y="1.417827026367" />
                  <Point X="28.0958359375" Y="1.394253173828" />
                  <Point X="28.097740234375" Y="1.371766723633" />
                  <Point X="28.110966796875" Y="1.30766784668" />
                  <Point X="28.11541015625" Y="1.286133789062" />
                  <Point X="28.120681640625" Y="1.268976928711" />
                  <Point X="28.136283203125" Y="1.235741943359" />
                  <Point X="28.172255859375" Y="1.181065185547" />
                  <Point X="28.18433984375" Y="1.162696533203" />
                  <Point X="28.198890625" Y="1.14545324707" />
                  <Point X="28.216134765625" Y="1.129362548828" />
                  <Point X="28.23434765625" Y="1.116034912109" />
                  <Point X="28.286478515625" Y="1.086690795898" />
                  <Point X="28.303990234375" Y="1.076832519531" />
                  <Point X="28.32051953125" Y="1.069502563477" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.4266015625" Y="1.050123413086" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.844701171875" Y="1.093918212891" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.82190234375" Y="1.031529907227" />
                  <Point X="29.84594140625" Y="0.932786865234" />
                  <Point X="29.8908671875" Y="0.644238830566" />
                  <Point X="29.68971875" Y="0.590341186523" />
                  <Point X="28.716580078125" Y="0.32958972168" />
                  <Point X="28.704791015625" Y="0.325586120605" />
                  <Point X="28.681546875" Y="0.315067932129" />
                  <Point X="28.61230078125" Y="0.275042327881" />
                  <Point X="28.589037109375" Y="0.261595672607" />
                  <Point X="28.57430859375" Y="0.251093765259" />
                  <Point X="28.54753125" Y="0.225576126099" />
                  <Point X="28.505982421875" Y="0.172634216309" />
                  <Point X="28.492025390625" Y="0.154848480225" />
                  <Point X="28.48030078125" Y="0.135567153931" />
                  <Point X="28.47052734375" Y="0.114104003906" />
                  <Point X="28.463681640625" Y="0.092603652954" />
                  <Point X="28.44983203125" Y="0.02028767395" />
                  <Point X="28.4451796875" Y="-0.004006832123" />
                  <Point X="28.443484375" Y="-0.021876050949" />
                  <Point X="28.4451796875" Y="-0.058553161621" />
                  <Point X="28.459029296875" Y="-0.130869293213" />
                  <Point X="28.463681640625" Y="-0.155163803101" />
                  <Point X="28.47052734375" Y="-0.176664154053" />
                  <Point X="28.48030078125" Y="-0.198127304077" />
                  <Point X="28.49202734375" Y="-0.217408630371" />
                  <Point X="28.533576171875" Y="-0.270350402832" />
                  <Point X="28.547533203125" Y="-0.288136260986" />
                  <Point X="28.55999609375" Y="-0.301231994629" />
                  <Point X="28.589037109375" Y="-0.324155670166" />
                  <Point X="28.658283203125" Y="-0.364181427002" />
                  <Point X="28.681546875" Y="-0.377628082275" />
                  <Point X="28.6927109375" Y="-0.38313885498" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="29.043505859375" Y="-0.479749176025" />
                  <Point X="29.891474609375" Y="-0.706961914062" />
                  <Point X="29.868443359375" Y="-0.859719970703" />
                  <Point X="29.855025390625" Y="-0.948725036621" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="29.55294921875" Y="-1.15201953125" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.23875390625" Y="-1.035048706055" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.163974609375" Y="-1.056596923828" />
                  <Point X="28.1361484375" Y="-1.073489135742" />
                  <Point X="28.1123984375" Y="-1.093960083008" />
                  <Point X="28.030251953125" Y="-1.192757324219" />
                  <Point X="28.002654296875" Y="-1.225947998047" />
                  <Point X="27.987935546875" Y="-1.250329589844" />
                  <Point X="27.976591796875" Y="-1.277715454102" />
                  <Point X="27.969759765625" Y="-1.305365722656" />
                  <Point X="27.957986328125" Y="-1.433312744141" />
                  <Point X="27.95403125" Y="-1.476296386719" />
                  <Point X="27.95634765625" Y="-1.507562011719" />
                  <Point X="27.964078125" Y="-1.539182617188" />
                  <Point X="27.976451171875" Y="-1.567996704102" />
                  <Point X="28.0516640625" Y="-1.684984985352" />
                  <Point X="28.076931640625" Y="-1.724287231445" />
                  <Point X="28.086935546875" Y="-1.737241577148" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.414017578125" Y="-1.99370715332" />
                  <Point X="29.213123046875" Y="-2.606882080078" />
                  <Point X="29.16263671875" Y="-2.688575927734" />
                  <Point X="29.124796875" Y="-2.749803955078" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="28.806095703125" Y="-2.757261474609" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.592474609375" Y="-2.13061328125" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395507812" />
                  <Point X="27.474609375" Y="-2.125353515625" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.310458984375" Y="-2.205897460938" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.24238671875" Y="-2.246548095703" />
                  <Point X="27.221427734375" Y="-2.267506591797" />
                  <Point X="27.204533203125" Y="-2.290438232422" />
                  <Point X="27.1338125" Y="-2.424813232422" />
                  <Point X="27.110052734375" Y="-2.469956298828" />
                  <Point X="27.100228515625" Y="-2.499734863281" />
                  <Point X="27.095271484375" Y="-2.531909179688" />
                  <Point X="27.09567578125" Y="-2.563259277344" />
                  <Point X="27.124888671875" Y="-2.725009765625" />
                  <Point X="27.134703125" Y="-2.779349609375" />
                  <Point X="27.13898828125" Y="-2.795140869141" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.346779296875" Y="-3.163754638672" />
                  <Point X="27.86128515625" Y="-4.054904541016" />
                  <Point X="27.82673828125" Y="-4.079580810547" />
                  <Point X="27.781841796875" Y="-4.111649414062" />
                  <Point X="27.701765625" Y="-4.16348046875" />
                  <Point X="27.5255625" Y="-3.933847900391" />
                  <Point X="26.758548828125" Y="-2.934254882812" />
                  <Point X="26.74750390625" Y="-2.922177490234" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.56239453125" Y="-2.797994140625" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479990234375" Y="-2.751167236328" />
                  <Point X="26.448369140625" Y="-2.743435546875" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.242626953125" Y="-2.757171875" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.156365234375" Y="-2.769396972656" />
                  <Point X="26.12898046875" Y="-2.780739746094" />
                  <Point X="26.10459765625" Y="-2.7954609375" />
                  <Point X="25.969875" Y="-2.907479003906" />
                  <Point X="25.92461328125" Y="-2.945111328125" />
                  <Point X="25.904142578125" Y="-2.968860839844" />
                  <Point X="25.88725" Y="-2.9966875" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.83534375" Y="-3.211135986328" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120361328" />
                  <Point X="25.8749921875" Y="-3.74278125" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="26.01814453125" Y="-4.860774902344" />
                  <Point X="25.975669921875" Y="-4.870085449219" />
                  <Point X="25.92931640625" Y="-4.878506347656" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="24.009859375" Y="-4.765891113281" />
                  <Point X="23.94157421875" Y="-4.75263671875" />
                  <Point X="23.85875390625" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575837402344" />
                  <Point X="23.879923828125" Y="-4.568097167969" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.509323730469" />
                  <Point X="23.876666015625" Y="-4.497688964844" />
                  <Point X="23.831359375" Y="-4.269916992188" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.811875" Y="-4.17846875" />
                  <Point X="23.80097265625" Y="-4.149500488281" />
                  <Point X="23.79433203125" Y="-4.1354609375" />
                  <Point X="23.778259765625" Y="-4.107624511719" />
                  <Point X="23.769423828125" Y="-4.094858642578" />
                  <Point X="23.749791015625" Y="-4.070936279297" />
                  <Point X="23.738994140625" Y="-4.059779785156" />
                  <Point X="23.564392578125" Y="-3.906656494141" />
                  <Point X="23.505734375" Y="-3.855215087891" />
                  <Point X="23.49326171875" Y="-3.845965332031" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.131447265625" Y="-3.780980712891" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812255859" />
                  <Point X="22.711466796875" Y="-3.944834960938" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.619560546875" Y="-4.010130859375" />
                  <Point X="22.597244140625" Y="-4.032769287109" />
                  <Point X="22.589529296875" Y="-4.041628662109" />
                  <Point X="22.532974609375" Y="-4.115331054688" />
                  <Point X="22.496796875" Y="-4.162477539063" />
                  <Point X="22.35251171875" Y="-4.073139648438" />
                  <Point X="22.252404296875" Y="-4.01115625" />
                  <Point X="22.019138671875" Y="-3.831548095703" />
                  <Point X="22.101279296875" Y="-3.689275390625" />
                  <Point X="22.65851171875" Y="-2.724119384766" />
                  <Point X="22.6651484375" Y="-2.710085205078" />
                  <Point X="22.67605078125" Y="-2.681120117188" />
                  <Point X="22.680314453125" Y="-2.666189453125" />
                  <Point X="22.6865859375" Y="-2.634662841797" />
                  <Point X="22.688361328125" Y="-2.619239257812" />
                  <Point X="22.689375" Y="-2.588305175781" />
                  <Point X="22.6853359375" Y="-2.5576171875" />
                  <Point X="22.6763515625" Y="-2.527999267578" />
                  <Point X="22.67064453125" Y="-2.513558837891" />
                  <Point X="22.656427734375" Y="-2.484729736328" />
                  <Point X="22.648443359375" Y="-2.47141015625" />
                  <Point X="22.6304140625" Y="-2.446251220703" />
                  <Point X="22.620369140625" Y="-2.434411865234" />
                  <Point X="22.603025390625" Y="-2.417068603516" />
                  <Point X="22.59119140625" Y="-2.407027832031" />
                  <Point X="22.566044921875" Y="-2.389005615234" />
                  <Point X="22.552732421875" Y="-2.381024169922" />
                  <Point X="22.52390234375" Y="-2.366802734375" />
                  <Point X="22.509458984375" Y="-2.36109375" />
                  <Point X="22.479837890625" Y="-2.352105712891" />
                  <Point X="22.449150390625" Y="-2.348063720703" />
                  <Point X="22.418212890625" Y="-2.349074707031" />
                  <Point X="22.40278515625" Y="-2.350848632812" />
                  <Point X="22.371255859375" Y="-2.357119140625" />
                  <Point X="22.35632421875" Y="-2.361383056641" />
                  <Point X="22.32735546875" Y="-2.372285888672" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.98907421875" Y="-2.566126708984" />
                  <Point X="21.2069140625" Y="-3.017707519531" />
                  <Point X="21.075064453125" Y="-2.844486572266" />
                  <Point X="20.99597265625" Y="-2.740573242188" />
                  <Point X="20.818734375" Y="-2.443373291016" />
                  <Point X="20.976900390625" Y="-2.322008544922" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.963517578125" Y="-1.563309814453" />
                  <Point X="21.98489453125" Y="-1.540390136719" />
                  <Point X="21.994630859375" Y="-1.528042602539" />
                  <Point X="22.012595703125" Y="-1.500911499023" />
                  <Point X="22.020162109375" Y="-1.487127807617" />
                  <Point X="22.03291796875" Y="-1.45850012207" />
                  <Point X="22.038107421875" Y="-1.443656005859" />
                  <Point X="22.043875" Y="-1.421389282227" />
                  <Point X="22.048447265625" Y="-1.398806518555" />
                  <Point X="22.051251953125" Y="-1.368375732422" />
                  <Point X="22.051421875" Y="-1.35304699707" />
                  <Point X="22.049212890625" Y="-1.321375244141" />
                  <Point X="22.046916015625" Y="-1.306218139648" />
                  <Point X="22.0399140625" Y="-1.276471801758" />
                  <Point X="22.02821875" Y="-1.248238647461" />
                  <Point X="22.012134765625" Y="-1.222254150391" />
                  <Point X="22.003041015625" Y="-1.209913696289" />
                  <Point X="21.98220703125" Y="-1.185957275391" />
                  <Point X="21.97125" Y="-1.175241455078" />
                  <Point X="21.94775390625" Y="-1.155710693359" />
                  <Point X="21.93521484375" Y="-1.146895385742" />
                  <Point X="21.915392578125" Y="-1.135228393555" />
                  <Point X="21.89456640625" Y="-1.124480834961" />
                  <Point X="21.86530078125" Y="-1.113255981445" />
                  <Point X="21.850201171875" Y="-1.108858886719" />
                  <Point X="21.81831640625" Y="-1.102378295898" />
                  <Point X="21.802701171875" Y="-1.100532226562" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="21.346345703125" Y="-1.154085449219" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.290169921875" Y="-1.095207275391" />
                  <Point X="20.259240234375" Y="-0.974113525391" />
                  <Point X="20.213548828125" Y="-0.654654846191" />
                  <Point X="20.381068359375" Y="-0.609767944336" />
                  <Point X="21.491712890625" Y="-0.312171234131" />
                  <Point X="21.499525390625" Y="-0.309712127686" />
                  <Point X="21.526068359375" Y="-0.299251281738" />
                  <Point X="21.555833984375" Y="-0.28389276123" />
                  <Point X="21.56644140625" Y="-0.277512664795" />
                  <Point X="21.587201171875" Y="-0.263104125977" />
                  <Point X="21.587201171875" Y="-0.263103942871" />
                  <Point X="21.6063125" Y="-0.248257583618" />
                  <Point X="21.628775390625" Y="-0.226369888306" />
                  <Point X="21.63909765625" Y="-0.214492538452" />
                  <Point X="21.658345703125" Y="-0.188233551025" />
                  <Point X="21.666564453125" Y="-0.174816467285" />
                  <Point X="21.68067578125" Y="-0.146809371948" />
                  <Point X="21.686568359375" Y="-0.132219665527" />
                  <Point X="21.6934921875" Y="-0.109908241272" />
                  <Point X="21.699013671875" Y="-0.088492424011" />
                  <Point X="21.70328125" Y="-0.060336116791" />
                  <Point X="21.704353515625" Y="-0.046099822998" />
                  <Point X="21.704353515625" Y="-0.016460256577" />
                  <Point X="21.70328125" Y="-0.002224114895" />
                  <Point X="21.699013671875" Y="0.025932344437" />
                  <Point X="21.695818359375" Y="0.039852367401" />
                  <Point X="21.68889453125" Y="0.0621639328" />
                  <Point X="21.680673828125" Y="0.08425226593" />
                  <Point X="21.6665625" Y="0.112256980896" />
                  <Point X="21.658345703125" Y="0.125668861389" />
                  <Point X="21.6391015625" Y="0.151924133301" />
                  <Point X="21.628783203125" Y="0.163798965454" />
                  <Point X="21.606326171875" Y="0.185684127808" />
                  <Point X="21.5941875" Y="0.195694473267" />
                  <Point X="21.573412109375" Y="0.210112686157" />
                  <Point X="21.554728515625" Y="0.221543411255" />
                  <Point X="21.532181640625" Y="0.233608474731" />
                  <Point X="21.52229296875" Y="0.238190368652" />
                  <Point X="21.50205859375" Y="0.24619178772" />
                  <Point X="21.491712890625" Y="0.249611297607" />
                  <Point X="21.11859375" Y="0.349588592529" />
                  <Point X="20.2145546875" Y="0.591824829102" />
                  <Point X="20.24873828125" Y="0.822832275391" />
                  <Point X="20.26866796875" Y="0.957522155762" />
                  <Point X="20.366416015625" Y="1.318237060547" />
                  <Point X="20.425796875" Y="1.310419311523" />
                  <Point X="21.221935546875" Y="1.20560546875" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.20470324707" />
                  <Point X="21.28485546875" Y="1.206589599609" />
                  <Point X="21.295109375" Y="1.208053222656" />
                  <Point X="21.3153984375" Y="1.212088745117" />
                  <Point X="21.32543359375" Y="1.214660522461" />
                  <Point X="21.371412109375" Y="1.229157592773" />
                  <Point X="21.3965546875" Y="1.237678100586" />
                  <Point X="21.41548828125" Y="1.246010009766" />
                  <Point X="21.4247265625" Y="1.250692260742" />
                  <Point X="21.443470703125" Y="1.261514892578" />
                  <Point X="21.452142578125" Y="1.267173095703" />
                  <Point X="21.468822265625" Y="1.279402832031" />
                  <Point X="21.484072265625" Y="1.293377685547" />
                  <Point X="21.497708984375" Y="1.308927368164" />
                  <Point X="21.504103515625" Y="1.317073852539" />
                  <Point X="21.51651953125" Y="1.334804931641" />
                  <Point X="21.52198828125" Y="1.343599243164" />
                  <Point X="21.531939453125" Y="1.361736206055" />
                  <Point X="21.536421875" Y="1.371078857422" />
                  <Point X="21.55487109375" Y="1.415619873047" />
                  <Point X="21.564505859375" Y="1.440361572266" />
                  <Point X="21.570294921875" Y="1.460229370117" />
                  <Point X="21.572646484375" Y="1.470318481445" />
                  <Point X="21.57640234375" Y="1.491632080078" />
                  <Point X="21.577640625" Y="1.501908935547" />
                  <Point X="21.5789921875" Y="1.522540771484" />
                  <Point X="21.57808984375" Y="1.543198974609" />
                  <Point X="21.5749453125" Y="1.563634399414" />
                  <Point X="21.57281640625" Y="1.573766601562" />
                  <Point X="21.567216796875" Y="1.594671386719" />
                  <Point X="21.563994140625" Y="1.604519042969" />
                  <Point X="21.55649609375" Y="1.623802368164" />
                  <Point X="21.552220703125" Y="1.63323828125" />
                  <Point X="21.5299609375" Y="1.676001708984" />
                  <Point X="21.529953125" Y="1.676016601562" />
                  <Point X="21.517185546875" Y="1.6993125" />
                  <Point X="21.505697265625" Y="1.716501953125" />
                  <Point X="21.499484375" Y="1.724782714844" />
                  <Point X="21.48557421875" Y="1.741358642578" />
                  <Point X="21.4784921875" Y="1.748918212891" />
                  <Point X="21.463552734375" Y="1.763218139648" />
                  <Point X="21.4556953125" Y="1.769958496094" />
                  <Point X="21.241673828125" Y="1.934183959961" />
                  <Point X="20.77238671875" Y="2.294280517578" />
                  <Point X="20.920263671875" Y="2.547628662109" />
                  <Point X="20.997716796875" Y="2.680323242188" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.90896484375" Y="2.725555419922" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.023568359375" Y="2.734227783203" />
                  <Point X="22.0430078125" Y="2.741303466797" />
                  <Point X="22.061560546875" Y="2.750453369141" />
                  <Point X="22.070587890625" Y="2.755534667969" />
                  <Point X="22.088841796875" Y="2.767164550781" />
                  <Point X="22.097259765625" Y="2.773197265625" />
                  <Point X="22.11338671875" Y="2.786141113281" />
                  <Point X="22.121095703125" Y="2.793052246094" />
                  <Point X="22.16655078125" Y="2.838505615234" />
                  <Point X="22.1818203125" Y="2.853775634766" />
                  <Point X="22.188734375" Y="2.861487060547" />
                  <Point X="22.201681640625" Y="2.877617919922" />
                  <Point X="22.20771484375" Y="2.886037353516" />
                  <Point X="22.21934375" Y="2.904290039062" />
                  <Point X="22.224423828125" Y="2.913315673828" />
                  <Point X="22.233572265625" Y="2.931865478516" />
                  <Point X="22.2406484375" Y="2.951305419922" />
                  <Point X="22.245564453125" Y="2.971395507813" />
                  <Point X="22.24747265625" Y="2.981569824219" />
                  <Point X="22.250298828125" Y="3.003026611328" />
                  <Point X="22.25108984375" Y="3.013352050781" />
                  <Point X="22.25154296875" Y="3.0340390625" />
                  <Point X="22.251205078125" Y="3.044400634766" />
                  <Point X="22.2456015625" Y="3.108437255859" />
                  <Point X="22.243720703125" Y="3.129950195312" />
                  <Point X="22.242255859375" Y="3.140201660156" />
                  <Point X="22.238220703125" Y="3.160488769531" />
                  <Point X="22.235650390625" Y="3.170524414062" />
                  <Point X="22.229140625" Y="3.191169677734" />
                  <Point X="22.22548828125" Y="3.200869628906" />
                  <Point X="22.21715625" Y="3.219801269531" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="22.11763671875" Y="3.393300537109" />
                  <Point X="21.94061328125" Y="3.699914550781" />
                  <Point X="22.21675" Y="3.911625976562" />
                  <Point X="22.351634765625" Y="4.015041259766" />
                  <Point X="22.8074765625" Y="4.268296875" />
                  <Point X="22.881435546875" Y="4.171910644531" />
                  <Point X="22.88817578125" Y="4.164053710938" />
                  <Point X="22.902478515625" Y="4.149110839844" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.934908203125" Y="4.121897460938" />
                  <Point X="22.952107421875" Y="4.110405273438" />
                  <Point X="22.96101953125" Y="4.105129394531" />
                  <Point X="23.032291015625" Y="4.068027099609" />
                  <Point X="23.056236328125" Y="4.055562255859" />
                  <Point X="23.06567578125" Y="4.051286132812" />
                  <Point X="23.084958984375" Y="4.043788085938" />
                  <Point X="23.094802734375" Y="4.040566162109" />
                  <Point X="23.1157109375" Y="4.034964599609" />
                  <Point X="23.12584375" Y="4.032834228516" />
                  <Point X="23.146283203125" Y="4.029688232422" />
                  <Point X="23.166947265625" Y="4.028785888672" />
                  <Point X="23.1875859375" Y="4.030138427734" />
                  <Point X="23.1978671875" Y="4.031377929688" />
                  <Point X="23.219181640625" Y="4.035135742188" />
                  <Point X="23.229271484375" Y="4.037488037109" />
                  <Point X="23.2491328125" Y="4.043277099609" />
                  <Point X="23.258904296875" Y="4.046713623047" />
                  <Point X="23.333138671875" Y="4.077463134766" />
                  <Point X="23.358078125" Y="4.087793212891" />
                  <Point X="23.367416015625" Y="4.092271972656" />
                  <Point X="23.385548828125" Y="4.102219726562" />
                  <Point X="23.39434375" Y="4.107688964844" />
                  <Point X="23.41207421875" Y="4.120103515625" />
                  <Point X="23.42022265625" Y="4.126498046875" />
                  <Point X="23.43577734375" Y="4.140138671875" />
                  <Point X="23.449755859375" Y="4.15539453125" />
                  <Point X="23.46198828125" Y="4.172079589844" />
                  <Point X="23.4676484375" Y="4.180754882812" />
                  <Point X="23.47846875" Y="4.199499023438" />
                  <Point X="23.4831484375" Y="4.208733886719" />
                  <Point X="23.4914765625" Y="4.227661621094" />
                  <Point X="23.495125" Y="4.237354492188" />
                  <Point X="23.519287109375" Y="4.313986816406" />
                  <Point X="23.527404296875" Y="4.339730957031" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370049316406" />
                  <Point X="23.535474609375" Y="4.380302246094" />
                  <Point X="23.537361328125" Y="4.401863769531" />
                  <Point X="23.53769921875" Y="4.412216796875" />
                  <Point X="23.537248046875" Y="4.432898925781" />
                  <Point X="23.536458984375" Y="4.443228027344" />
                  <Point X="23.52567578125" Y="4.525127441406" />
                  <Point X="23.520734375" Y="4.562654785156" />
                  <Point X="23.89421875" Y="4.667366699219" />
                  <Point X="24.068826171875" Y="4.7163203125" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.77433203125" Y="4.261727050781" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.247104980469" />
                  <Point X="25.23798046875" Y="4.2617265625" />
                  <Point X="25.286576171875" Y="4.443085449219" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.675408203125" Y="4.753879882812" />
                  <Point X="25.827876953125" Y="4.737912109375" />
                  <Point X="26.302064453125" Y="4.623428710938" />
                  <Point X="26.45359375" Y="4.586844726562" />
                  <Point X="26.76090234375" Y="4.475381347656" />
                  <Point X="26.858265625" Y="4.440067382812" />
                  <Point X="27.156712890625" Y="4.300494140625" />
                  <Point X="27.250451171875" Y="4.256654296875" />
                  <Point X="27.538822265625" Y="4.088649658203" />
                  <Point X="27.6294296875" Y="4.035861572266" />
                  <Point X="27.81778125" Y="3.901916748047" />
                  <Point X="27.710265625" Y="3.715693603516" />
                  <Point X="27.065310546875" Y="2.59859765625" />
                  <Point X="27.0623828125" Y="2.593119384766" />
                  <Point X="27.053189453125" Y="2.573454833984" />
                  <Point X="27.044185546875" Y="2.549571777344" />
                  <Point X="27.041302734375" Y="2.540600585938" />
                  <Point X="27.025232421875" Y="2.480503662109" />
                  <Point X="27.01983203125" Y="2.460314208984" />
                  <Point X="27.0179140625" Y="2.451470947266" />
                  <Point X="27.013646484375" Y="2.420224121094" />
                  <Point X="27.012755859375" Y="2.383236083984" />
                  <Point X="27.013412109375" Y="2.369573974609" />
                  <Point X="27.0196796875" Y="2.317607421875" />
                  <Point X="27.02380078125" Y="2.289038330078" />
                  <Point X="27.029140625" Y="2.267125488281" />
                  <Point X="27.032462890625" Y="2.256329345703" />
                  <Point X="27.040728515625" Y="2.234235839844" />
                  <Point X="27.045314453125" Y="2.223897949219" />
                  <Point X="27.055681640625" Y="2.203843505859" />
                  <Point X="27.061462890625" Y="2.194126953125" />
                  <Point X="27.093619140625" Y="2.146738525391" />
                  <Point X="27.104271484375" Y="2.132519775391" />
                  <Point X="27.128375" Y="2.103299804688" />
                  <Point X="27.1376953125" Y="2.093511474609" />
                  <Point X="27.15763671875" Y="2.075352050781" />
                  <Point X="27.1682578125" Y="2.066980957031" />
                  <Point X="27.215646484375" Y="2.034825683594" />
                  <Point X="27.24128125" Y="2.018244628906" />
                  <Point X="27.261326171875" Y="2.007882080078" />
                  <Point X="27.27165625" Y="2.003298339844" />
                  <Point X="27.293744140625" Y="1.995032348633" />
                  <Point X="27.304548828125" Y="1.991707519531" />
                  <Point X="27.32647265625" Y="1.986364746094" />
                  <Point X="27.337591796875" Y="1.984346923828" />
                  <Point X="27.38955859375" Y="1.978080444336" />
                  <Point X="27.40829296875" Y="1.976756225586" />
                  <Point X="27.44465234375" Y="1.975988647461" />
                  <Point X="27.4580390625" Y="1.976651733398" />
                  <Point X="27.4845859375" Y="1.979854980469" />
                  <Point X="27.49774609375" Y="1.982395629883" />
                  <Point X="27.55784375" Y="1.998466430664" />
                  <Point X="27.578033203125" Y="2.003865356445" />
                  <Point X="27.583994140625" Y="2.005670654297" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.011322265625" Y="2.244544677734" />
                  <Point X="28.940404296875" Y="2.780950439453" />
                  <Point X="28.990201171875" Y="2.711743896484" />
                  <Point X="29.043962890625" Y="2.637026611328" />
                  <Point X="29.136884765625" Y="2.483471191406" />
                  <Point X="29.02209765625" Y="2.395391357422" />
                  <Point X="28.172953125" Y="1.743819824219" />
                  <Point X="28.16813671875" Y="1.7398671875" />
                  <Point X="28.1521171875" Y="1.725213378906" />
                  <Point X="28.13466796875" Y="1.706600585938" />
                  <Point X="28.128578125" Y="1.699421264648" />
                  <Point X="28.085326171875" Y="1.64299597168" />
                  <Point X="28.070796875" Y="1.624039916992" />
                  <Point X="28.065642578125" Y="1.616608154297" />
                  <Point X="28.049744140625" Y="1.589380126953" />
                  <Point X="28.034765625" Y="1.555553100586" />
                  <Point X="28.030140625" Y="1.542675537109" />
                  <Point X="28.014029296875" Y="1.485065185547" />
                  <Point X="28.008615234375" Y="1.46571105957" />
                  <Point X="28.006224609375" Y="1.454663818359" />
                  <Point X="28.002771484375" Y="1.432365722656" />
                  <Point X="28.001708984375" Y="1.421115112305" />
                  <Point X="28.000892578125" Y="1.397541259766" />
                  <Point X="28.001173828125" Y="1.386236694336" />
                  <Point X="28.003078125" Y="1.363750244141" />
                  <Point X="28.004701171875" Y="1.352568359375" />
                  <Point X="28.017927734375" Y="1.288469360352" />
                  <Point X="28.02237109375" Y="1.266935302734" />
                  <Point X="28.024599609375" Y="1.258232177734" />
                  <Point X="28.034685546875" Y="1.228607788086" />
                  <Point X="28.050287109375" Y="1.195372680664" />
                  <Point X="28.056919921875" Y="1.18352734375" />
                  <Point X="28.092892578125" Y="1.128850463867" />
                  <Point X="28.1049765625" Y="1.110481811523" />
                  <Point X="28.111736328125" Y="1.101429443359" />
                  <Point X="28.126287109375" Y="1.084186157227" />
                  <Point X="28.134078125" Y="1.075995239258" />
                  <Point X="28.151322265625" Y="1.059904541016" />
                  <Point X="28.160033203125" Y="1.052696899414" />
                  <Point X="28.17824609375" Y="1.039369262695" />
                  <Point X="28.187748046875" Y="1.033249145508" />
                  <Point X="28.23987890625" Y="1.003905090332" />
                  <Point X="28.257390625" Y="0.99404675293" />
                  <Point X="28.265478515625" Y="0.989988464355" />
                  <Point X="28.29467578125" Y="0.978085266113" />
                  <Point X="28.330275390625" Y="0.96802142334" />
                  <Point X="28.343671875" Y="0.965257751465" />
                  <Point X="28.414154296875" Y="0.955942382812" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199768066" />
                  <Point X="28.46571875" Y="0.951222839355" />
                  <Point X="28.49121875" Y="0.952032409668" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="28.8571015625" Y="0.999731018066" />
                  <Point X="29.704703125" Y="1.111319702148" />
                  <Point X="29.72959765625" Y="1.009059326172" />
                  <Point X="29.752689453125" Y="0.914205688477" />
                  <Point X="29.783873046875" Y="0.713921081543" />
                  <Point X="29.665130859375" Y="0.682104064941" />
                  <Point X="28.6919921875" Y="0.421352630615" />
                  <Point X="28.68603125" Y="0.419544036865" />
                  <Point X="28.665625" Y="0.412137268066" />
                  <Point X="28.642380859375" Y="0.40161907959" />
                  <Point X="28.634005859375" Y="0.397316467285" />
                  <Point X="28.564759765625" Y="0.35729095459" />
                  <Point X="28.54149609375" Y="0.343844299316" />
                  <Point X="28.5338828125" Y="0.338946136475" />
                  <Point X="28.508771484375" Y="0.319867126465" />
                  <Point X="28.481994140625" Y="0.294349456787" />
                  <Point X="28.472796875" Y="0.284226898193" />
                  <Point X="28.431248046875" Y="0.231285049438" />
                  <Point X="28.417291015625" Y="0.213499343872" />
                  <Point X="28.410853515625" Y="0.204207000732" />
                  <Point X="28.39912890625" Y="0.184925704956" />
                  <Point X="28.393841796875" Y="0.174936767578" />
                  <Point X="28.384068359375" Y="0.153473526001" />
                  <Point X="28.380005859375" Y="0.142926208496" />
                  <Point X="28.37316015625" Y="0.121425979614" />
                  <Point X="28.370376953125" Y="0.110472915649" />
                  <Point X="28.35652734375" Y="0.03815687561" />
                  <Point X="28.351875" Y="0.013862381935" />
                  <Point X="28.350603515625" Y="0.004965815067" />
                  <Point X="28.3485859375" Y="-0.026262531281" />
                  <Point X="28.35028125" Y="-0.062939689636" />
                  <Point X="28.351875" Y="-0.076422317505" />
                  <Point X="28.365724609375" Y="-0.148738510132" />
                  <Point X="28.370376953125" Y="-0.173032989502" />
                  <Point X="28.37316015625" Y="-0.183986053467" />
                  <Point X="28.380005859375" Y="-0.205486434937" />
                  <Point X="28.384068359375" Y="-0.2160337677" />
                  <Point X="28.393841796875" Y="-0.237496841431" />
                  <Point X="28.3991328125" Y="-0.247491882324" />
                  <Point X="28.410859375" Y="-0.266773162842" />
                  <Point X="28.417294921875" Y="-0.276059570312" />
                  <Point X="28.45884375" Y="-0.329001281738" />
                  <Point X="28.47280078125" Y="-0.346787139893" />
                  <Point X="28.478716796875" Y="-0.353628112793" />
                  <Point X="28.501134765625" Y="-0.375800109863" />
                  <Point X="28.53017578125" Y="-0.398723846436" />
                  <Point X="28.54149609375" Y="-0.406404083252" />
                  <Point X="28.6107421875" Y="-0.446429870605" />
                  <Point X="28.634005859375" Y="-0.459876525879" />
                  <Point X="28.639498046875" Y="-0.462815093994" />
                  <Point X="28.659158203125" Y="-0.472016357422" />
                  <Point X="28.68302734375" Y="-0.48102734375" />
                  <Point X="28.6919921875" Y="-0.483912872314" />
                  <Point X="29.01891796875" Y="-0.571512084961" />
                  <Point X="29.78487890625" Y="-0.776751098633" />
                  <Point X="29.774505859375" Y="-0.845556945801" />
                  <Point X="29.761619140625" Y="-0.931038146973" />
                  <Point X="29.7278046875" Y="-1.079219726563" />
                  <Point X="29.565349609375" Y="-1.057832275391" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535644531" />
                  <Point X="28.40009765625" Y="-0.908042358398" />
                  <Point X="28.36672265625" Y="-0.910840942383" />
                  <Point X="28.354482421875" Y="-0.912676269531" />
                  <Point X="28.218576171875" Y="-0.942216247559" />
                  <Point X="28.17291796875" Y="-0.952140075684" />
                  <Point X="28.157876953125" Y="-0.95674206543" />
                  <Point X="28.128755859375" Y="-0.968366455078" />
                  <Point X="28.11467578125" Y="-0.975389160156" />
                  <Point X="28.086849609375" Y="-0.99228137207" />
                  <Point X="28.074125" Y="-1.001530456543" />
                  <Point X="28.050375" Y="-1.022001342773" />
                  <Point X="28.039349609375" Y="-1.033223144531" />
                  <Point X="27.957203125" Y="-1.132020385742" />
                  <Point X="27.92960546875" Y="-1.16521105957" />
                  <Point X="27.92132421875" Y="-1.176850830078" />
                  <Point X="27.90660546875" Y="-1.201232421875" />
                  <Point X="27.90016796875" Y="-1.213974243164" />
                  <Point X="27.88882421875" Y="-1.241360107422" />
                  <Point X="27.884365234375" Y="-1.254927490234" />
                  <Point X="27.877533203125" Y="-1.282577758789" />
                  <Point X="27.87516015625" Y="-1.296660766602" />
                  <Point X="27.86338671875" Y="-1.424607910156" />
                  <Point X="27.859431640625" Y="-1.467591430664" />
                  <Point X="27.859291015625" Y="-1.483315551758" />
                  <Point X="27.861607421875" Y="-1.514581176758" />
                  <Point X="27.864064453125" Y="-1.530122802734" />
                  <Point X="27.871794921875" Y="-1.561743408203" />
                  <Point X="27.87678515625" Y="-1.576666748047" />
                  <Point X="27.889158203125" Y="-1.605480834961" />
                  <Point X="27.896541015625" Y="-1.619371582031" />
                  <Point X="27.97175390625" Y="-1.736359863281" />
                  <Point X="27.997021484375" Y="-1.775662109375" />
                  <Point X="28.0017421875" Y="-1.782351928711" />
                  <Point X="28.019798828125" Y="-1.804456054688" />
                  <Point X="28.043494140625" Y="-1.828123657227" />
                  <Point X="28.052798828125" Y="-1.836277587891" />
                  <Point X="28.356185546875" Y="-2.069075683594" />
                  <Point X="29.087171875" Y="-2.629980712891" />
                  <Point X="29.08182421875" Y="-2.638633789062" />
                  <Point X="29.04548828125" Y="-2.697427734375" />
                  <Point X="29.0012734375" Y="-2.760251464844" />
                  <Point X="28.853595703125" Y="-2.674989013672" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.609357421875" Y="-2.037125610352" />
                  <Point X="27.555017578125" Y="-2.027311889648" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403442383" />
                  <Point X="27.492314453125" Y="-2.02650390625" />
                  <Point X="27.460140625" Y="-2.031461791992" />
                  <Point X="27.444845703125" Y="-2.03513659668" />
                  <Point X="27.4150703125" Y="-2.044960083008" />
                  <Point X="27.40058984375" Y="-2.051108886719" />
                  <Point X="27.26621484375" Y="-2.121829345703" />
                  <Point X="27.221072265625" Y="-2.145587890625" />
                  <Point X="27.208970703125" Y="-2.153170166016" />
                  <Point X="27.186041015625" Y="-2.170062255859" />
                  <Point X="27.175212890625" Y="-2.179372070313" />
                  <Point X="27.15425390625" Y="-2.200330566406" />
                  <Point X="27.144943359375" Y="-2.211157958984" />
                  <Point X="27.128048828125" Y="-2.234089599609" />
                  <Point X="27.12046484375" Y="-2.246193847656" />
                  <Point X="27.049744140625" Y="-2.380568847656" />
                  <Point X="27.025984375" Y="-2.425711914062" />
                  <Point X="27.0198359375" Y="-2.440192871094" />
                  <Point X="27.01001171875" Y="-2.469971435547" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517443359375" />
                  <Point X="27.000279296875" Y="-2.533134277344" />
                  <Point X="27.00068359375" Y="-2.564484375" />
                  <Point X="27.0021875" Y="-2.580143554688" />
                  <Point X="27.031400390625" Y="-2.741894042969" />
                  <Point X="27.04121484375" Y="-2.796233886719" />
                  <Point X="27.04301953125" Y="-2.804229248047" />
                  <Point X="27.05123828125" Y="-2.831542236328" />
                  <Point X="27.064072265625" Y="-2.862479980469" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.2645078125" Y="-3.211254638672" />
                  <Point X="27.735896484375" Y="-4.02772265625" />
                  <Point X="27.723755859375" Y="-4.036082763672" />
                  <Point X="27.600931640625" Y="-3.876015625" />
                  <Point X="26.83391796875" Y="-2.876422607422" />
                  <Point X="26.828654296875" Y="-2.870143066406" />
                  <Point X="26.808830078125" Y="-2.849624023438" />
                  <Point X="26.783251953125" Y="-2.828003662109" />
                  <Point X="26.77330078125" Y="-2.820646728516" />
                  <Point X="26.61376953125" Y="-2.718083740234" />
                  <Point X="26.56017578125" Y="-2.683627929688" />
                  <Point X="26.546283203125" Y="-2.676245605469" />
                  <Point X="26.51747265625" Y="-2.663874511719" />
                  <Point X="26.5025546875" Y="-2.658885742188" />
                  <Point X="26.47093359375" Y="-2.651154052734" />
                  <Point X="26.45539453125" Y="-2.648695800781" />
                  <Point X="26.424125" Y="-2.646376953125" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.233921875" Y="-2.662571533203" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670339111328" />
                  <Point X="26.133578125" Y="-2.677170410156" />
                  <Point X="26.12001171875" Y="-2.681627929688" />
                  <Point X="26.092626953125" Y="-2.692970703125" />
                  <Point X="26.07987890625" Y="-2.699412841797" />
                  <Point X="26.05549609375" Y="-2.714134033203" />
                  <Point X="26.043861328125" Y="-2.722413085938" />
                  <Point X="25.909138671875" Y="-2.834431152344" />
                  <Point X="25.863876953125" Y="-2.872063476562" />
                  <Point X="25.852654296875" Y="-2.883087402344" />
                  <Point X="25.83218359375" Y="-2.906836914062" />
                  <Point X="25.822935546875" Y="-2.9195625" />
                  <Point X="25.80604296875" Y="-2.947389160156" />
                  <Point X="25.79901953125" Y="-2.961466552734" />
                  <Point X="25.78739453125" Y="-2.990587646484" />
                  <Point X="25.78279296875" Y="-3.005631347656" />
                  <Point X="25.74251171875" Y="-3.190958740234" />
                  <Point X="25.728978515625" Y="-3.253219238281" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323170166016" />
                  <Point X="25.7255546875" Y="-3.335520507812" />
                  <Point X="25.7808046875" Y="-3.755181396484" />
                  <Point X="25.833087890625" Y="-4.152315917969" />
                  <Point X="25.655068359375" Y="-3.487937744141" />
                  <Point X="25.652609375" Y="-3.480124023438" />
                  <Point X="25.642146484375" Y="-3.453577392578" />
                  <Point X="25.6267890625" Y="-3.423814697266" />
                  <Point X="25.62041015625" Y="-3.413209472656" />
                  <Point X="25.497853515625" Y="-3.236630371094" />
                  <Point X="25.456681640625" Y="-3.177309082031" />
                  <Point X="25.446669921875" Y="-3.165170654297" />
                  <Point X="25.42478515625" Y="-3.142715820312" />
                  <Point X="25.412912109375" Y="-3.132399414062" />
                  <Point X="25.38665625" Y="-3.113155029297" />
                  <Point X="25.373244140625" Y="-3.104938232422" />
                  <Point X="25.345244140625" Y="-3.090830078125" />
                  <Point X="25.33065625" Y="-3.084938720703" />
                  <Point X="25.141009765625" Y="-3.026079101562" />
                  <Point X="25.077296875" Y="-3.006305419922" />
                  <Point X="25.063376953125" Y="-3.003109375" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766357422" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.745369140625" Y="-3.065164550781" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.667068359375" Y="-3.090830078125" />
                  <Point X="24.639068359375" Y="-3.104938232422" />
                  <Point X="24.62565625" Y="-3.113154785156" />
                  <Point X="24.599400390625" Y="-3.132398925781" />
                  <Point X="24.587525390625" Y="-3.142716552734" />
                  <Point X="24.565640625" Y="-3.165172119141" />
                  <Point X="24.555630859375" Y="-3.177310058594" />
                  <Point X="24.433076171875" Y="-3.353888916016" />
                  <Point X="24.391904296875" Y="-3.413210449219" />
                  <Point X="24.38753125" Y="-3.420131835938" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213623047" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.25651171875" Y="-3.863879150391" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.315771389154" Y="-1.195435005857" />
                  <Point X="20.359002424758" Y="-1.284071764232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.895647038564" Y="-2.384356277481" />
                  <Point X="21.200341686906" Y="-3.009072885485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.236716872103" Y="-0.816637882745" />
                  <Point X="20.458322162972" Y="-1.270996061929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.972559702129" Y="-2.325339263946" />
                  <Point X="21.287545133629" Y="-2.971155104192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.257646598114" Y="-0.642838837283" />
                  <Point X="20.557641901186" Y="-1.257920359626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.049472295797" Y="-2.266322107103" />
                  <Point X="21.37001843043" Y="-2.923539078343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.351127007796" Y="-0.617790737259" />
                  <Point X="20.6569616394" Y="-1.244844657324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.126384885285" Y="-2.207304941689" />
                  <Point X="21.45249172723" Y="-2.875923052495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.444607436728" Y="-0.592742676703" />
                  <Point X="20.756281377613" Y="-1.231768955021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.203297474773" Y="-2.148287776274" />
                  <Point X="21.534965024031" Y="-2.828307026646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.021924752768" Y="-3.826722429169" />
                  <Point X="22.027369338332" Y="-3.837885483867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.538087874731" Y="-0.567694634746" />
                  <Point X="20.855601115827" Y="-1.218693252718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.28021006426" Y="-2.08927061086" />
                  <Point X="21.617438320832" Y="-2.780691000797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.079219926584" Y="-3.727483601041" />
                  <Point X="22.196631281342" Y="-3.968212552747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.631568312734" Y="-0.542646592789" />
                  <Point X="20.954920854041" Y="-1.205617550416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.357122653748" Y="-2.030253445446" />
                  <Point X="21.699911617633" Y="-2.733074974949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.13651519127" Y="-3.628244959225" />
                  <Point X="22.353934498003" Y="-4.074020599054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.725048750736" Y="-0.517598550832" />
                  <Point X="21.054240592255" Y="-1.192541848113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.434035243236" Y="-1.971236280031" />
                  <Point X="21.782384914433" Y="-2.6854589491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.193810512846" Y="-3.52900643405" />
                  <Point X="22.500451869172" Y="-4.157714384912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.232033489868" Y="0.70994387559" />
                  <Point X="20.300932487522" Y="0.56867999602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.818529188739" Y="-0.492550508874" />
                  <Point X="21.153560330468" Y="-1.17946614581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.510947832724" Y="-1.912219114617" />
                  <Point X="21.864858211234" Y="-2.637842923252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.251105834422" Y="-3.429767908874" />
                  <Point X="22.565074268415" Y="-4.073498595226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.256636628577" Y="0.876211308888" />
                  <Point X="20.422519570565" Y="0.536100875677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.912009626742" Y="-0.467502466917" />
                  <Point X="21.252880068682" Y="-1.166390443508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.587860422212" Y="-1.853201949203" />
                  <Point X="21.947331508035" Y="-2.590226897403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.308401155998" Y="-3.330529383699" />
                  <Point X="22.633979280609" Y="-3.998063463324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.287957494085" Y="1.028705361123" />
                  <Point X="20.544106653609" Y="0.503521755333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.005490064745" Y="-0.44245442496" />
                  <Point X="21.352199807309" Y="-1.153314742052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.664773011699" Y="-1.794184783788" />
                  <Point X="22.029804839132" Y="-2.542610941872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.365696477574" Y="-3.231290858524" />
                  <Point X="22.713162553979" Y="-3.943701889796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.325708429199" Y="1.168015816941" />
                  <Point X="20.665693736652" Y="0.47094263499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.098970502748" Y="-0.417406383003" />
                  <Point X="21.451519552533" Y="-1.140239054122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.741685601187" Y="-1.735167618374" />
                  <Point X="22.112278205377" Y="-2.494995058405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.42299179915" Y="-3.132052333349" />
                  <Point X="22.792880274678" Y="-3.890436095681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.363459364314" Y="1.307326272759" />
                  <Point X="20.787280819696" Y="0.438363514647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.192450940751" Y="-0.392358341046" />
                  <Point X="21.550839297757" Y="-1.127163366192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.818598190675" Y="-1.676150452959" />
                  <Point X="22.194751571622" Y="-2.447379174938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.480287120726" Y="-3.032813808173" />
                  <Point X="22.872597995377" Y="-3.837170301565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.470519684129" Y="1.304531430869" />
                  <Point X="20.908867902739" Y="0.405784394304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.285931378754" Y="-0.367310299089" />
                  <Point X="21.65015904298" Y="-1.114087678262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.895510780163" Y="-1.617133287545" />
                  <Point X="22.277224937867" Y="-2.399763291471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.537582442302" Y="-2.933575282998" />
                  <Point X="22.95623952846" Y="-3.791949515054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.583469528771" Y="1.289661273601" />
                  <Point X="21.030454985782" Y="0.37320527396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.379411816757" Y="-0.342262257132" />
                  <Point X="21.749478788204" Y="-1.101011990332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.971028702218" Y="-1.555256630137" />
                  <Point X="22.363239661132" Y="-2.359408265908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.594877763878" Y="-2.834336757823" />
                  <Point X="23.054114789541" Y="-3.775912195736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.696419373412" Y="1.274791116334" />
                  <Point X="21.152042091277" Y="0.340626107584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.47289225476" Y="-0.317214215175" />
                  <Point X="21.860460264606" Y="-1.111846394635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.031302685288" Y="-1.462125266065" />
                  <Point X="22.46438225931" Y="-2.350069980393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.652173085454" Y="-2.735098232648" />
                  <Point X="23.163302391823" Y="-3.783068613039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.809369218054" Y="1.259920959066" />
                  <Point X="21.273629255934" Y="0.308046819909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.560862361307" Y="-0.280868319465" />
                  <Point X="22.602567275588" Y="-2.416679907007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.689179805164" Y="-2.594261909128" />
                  <Point X="23.272490061083" Y="-3.79022516767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.922319062696" Y="1.245050801799" />
                  <Point X="21.395216420591" Y="0.275467532234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.635951546205" Y="-0.218112620616" />
                  <Point X="23.38266635967" Y="-3.799408712808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.035268907337" Y="1.230180644531" />
                  <Point X="21.518330699567" Y="0.239757196201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.69169637011" Y="-0.11569510411" />
                  <Point X="23.522942495949" Y="-3.870306070794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.876638863279" Y="-4.595491091485" />
                  <Point X="23.954508393592" Y="-4.755147288626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.148218751979" Y="1.215310487264" />
                  <Point X="23.707641605159" Y="-4.032284020837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.836742593865" Y="-4.296980273934" />
                  <Point X="24.032769224486" Y="-4.698894427748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.259155524075" Y="1.204567740369" />
                  <Point X="24.070247357879" Y="-4.559024645511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.805606112484" Y="2.351193184408" />
                  <Point X="20.869833913489" Y="2.21950667727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.355332239531" Y="1.224087594306" />
                  <Point X="24.107725491272" Y="-4.419154863275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.863187895206" Y="2.449844377194" />
                  <Point X="21.038746785268" Y="2.089895310476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.442927798538" Y="1.261201426275" />
                  <Point X="24.145203624665" Y="-4.279285081038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.920769679517" Y="2.548495566724" />
                  <Point X="21.207659657048" Y="1.960283943682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.514283558423" Y="1.331611780773" />
                  <Point X="24.182681758057" Y="-4.139415298801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.978351642988" Y="2.64714638892" />
                  <Point X="21.376573045401" Y="1.830671517754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.565416173141" Y="1.443485727494" />
                  <Point X="24.22015989145" Y="-3.999545516565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.040835621376" Y="2.735746591101" />
                  <Point X="24.257638028308" Y="-3.859675741434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.105803540058" Y="2.819253960956" />
                  <Point X="24.295116277026" Y="-3.719806195647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.17077145874" Y="2.90276133081" />
                  <Point X="24.332594525743" Y="-3.579936649861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.235739377421" Y="2.986268700664" />
                  <Point X="24.373374046281" Y="-3.446835714372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.334908987785" Y="2.999652210675" />
                  <Point X="24.433464885821" Y="-3.353328850417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.482036173706" Y="2.914708119286" />
                  <Point X="24.495539893519" Y="-3.263890134058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.629163359628" Y="2.829764027897" />
                  <Point X="24.557753664437" Y="-3.174735924465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.774074305164" Y="2.749363902686" />
                  <Point X="24.631630607887" Y="-3.109494762317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.890600012901" Y="2.727162139576" />
                  <Point X="24.719606293422" Y="-3.073160305228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.995938817753" Y="2.727896926428" />
                  <Point X="24.811407210222" Y="-3.044668734497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.083991193022" Y="2.764074146261" />
                  <Point X="24.903208086003" Y="-3.016177079665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.157701597406" Y="2.829656764095" />
                  <Point X="24.999925845699" Y="-2.99776653081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.223444357044" Y="2.911575474559" />
                  <Point X="25.115583146738" Y="-3.018187796332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.943546161766" Y="3.702163162695" />
                  <Point X="21.96657366391" Y="3.654949786588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.248329270272" Y="3.077265184478" />
                  <Point X="25.240134139179" Y="-3.056843831498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.0204761479" Y="3.7611446597" />
                  <Point X="25.367999710155" Y="-3.10229575977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.775738003729" Y="-3.938283149443" />
                  <Point X="25.815695851302" Y="-4.020208877823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.097406134034" Y="3.820126156704" />
                  <Point X="25.776617166045" Y="-3.723374356209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.174336120168" Y="3.879107653708" />
                  <Point X="25.737537463964" Y="-3.426537749799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.251266069811" Y="3.938089225529" />
                  <Point X="25.738141408204" Y="-3.211064675887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.328195974614" Y="3.997070889286" />
                  <Point X="25.770724171084" Y="-3.061157896682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.40945946604" Y="4.047167383741" />
                  <Point X="25.814299282246" Y="-2.933788771388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.492621852891" Y="4.093370565613" />
                  <Point X="25.882385465522" Y="-2.85667479141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.575784239741" Y="4.139573747486" />
                  <Point X="25.957586409641" Y="-2.79414823292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.658946626592" Y="4.185776929359" />
                  <Point X="26.032787043884" Y="-2.731621039091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.742109013442" Y="4.231980111231" />
                  <Point X="26.11509435915" Y="-2.683664700665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.869547801596" Y="4.18740321742" />
                  <Point X="26.211509643297" Y="-2.664633985032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.033869329257" Y="4.06720550111" />
                  <Point X="26.312666699708" Y="-2.655325343286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.158116942732" Y="4.029171485002" />
                  <Point X="26.414043039949" Y="-2.646466300021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.255792091779" Y="4.045619094791" />
                  <Point X="26.531080707114" Y="-2.669717735512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.343803392863" Y="4.081880529182" />
                  <Point X="26.681653587809" Y="-2.761726548132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.425487363944" Y="4.131114912587" />
                  <Point X="26.859643783287" Y="-2.909949186577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.487975963576" Y="4.219705639814" />
                  <Point X="27.149722882878" Y="-3.287988135723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.530051377145" Y="4.350149600844" />
                  <Point X="27.439801982469" Y="-3.66602708487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.53073530029" Y="4.565458693699" />
                  <Point X="27.000478343401" Y="-2.548568796883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.645790296613" Y="-3.871654373571" />
                  <Point X="27.725426340514" Y="-4.034932460309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.623717772699" Y="4.591527716426" />
                  <Point X="27.036504973817" Y="-2.405722992517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.716700245108" Y="4.617596739154" />
                  <Point X="27.091363610151" Y="-2.30148852223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.809682717517" Y="4.643665761881" />
                  <Point X="27.150086490014" Y="-2.205176925296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.902665193688" Y="4.669734776895" />
                  <Point X="27.225566514688" Y="-2.14322256674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.99564770751" Y="4.695803714714" />
                  <Point X="27.309674009621" Y="-2.098957143601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.090122634186" Y="4.718812752724" />
                  <Point X="27.393781636226" Y="-2.054691990428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.190112214879" Y="4.730515074417" />
                  <Point X="27.486190814125" Y="-2.027447539764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.290101795573" Y="4.74221739611" />
                  <Point X="27.595376843466" Y="-2.034600732061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.390091376266" Y="4.753919717803" />
                  <Point X="27.711283606547" Y="-2.055533470565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.49008095696" Y="4.765622039496" />
                  <Point X="27.832348172236" Y="-2.08704127157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.590070537653" Y="4.77732436119" />
                  <Point X="24.692391835599" Y="4.567534610936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.919079969983" Y="4.102755058168" />
                  <Point X="27.978341516944" Y="-2.169660643962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.032506008746" Y="4.086908558264" />
                  <Point X="28.125468601581" Y="-2.254604527685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.123664582223" Y="4.116717127979" />
                  <Point X="27.860062621916" Y="-1.493730284893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.998797507049" Y="-1.778178952843" />
                  <Point X="28.272595686217" Y="-2.339548411409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.197230988961" Y="4.182594984738" />
                  <Point X="27.874284406844" Y="-1.306177922059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.180697784888" Y="-1.934418448175" />
                  <Point X="28.419722770854" Y="-2.424492295133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.247324668779" Y="4.296599063678" />
                  <Point X="27.918879617083" Y="-1.180900309821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.349611241416" Y="-2.064031013881" />
                  <Point X="28.56684985549" Y="-2.509436178856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.284803079277" Y="4.436468277763" />
                  <Point X="27.984641363609" Y="-1.099020528245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.51852407672" Y="-2.193642305889" />
                  <Point X="28.713976940127" Y="-2.59438006258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.322281080157" Y="4.576338331691" />
                  <Point X="28.052063815948" Y="-1.020545698178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.687436886865" Y="-2.323253546316" />
                  <Point X="28.861104047185" Y="-2.679323992276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.359759060696" Y="4.716208427326" />
                  <Point X="28.131732048012" Y="-0.967178437324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.856349697011" Y="-2.452864786743" />
                  <Point X="29.004226030578" Y="-2.756056201534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.434792264011" Y="4.779078905429" />
                  <Point X="28.224614253522" Y="-0.940903836988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.025262507157" Y="-2.58247602717" />
                  <Point X="29.065519719712" Y="-2.665015544722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.546178951845" Y="4.76741369457" />
                  <Point X="28.320180344132" Y="-0.920132016584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.657565639678" Y="4.75574848371" />
                  <Point X="28.420150125169" Y="-0.908389099579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.768952376425" Y="4.744083172564" />
                  <Point X="28.532409221382" Y="-0.921843012689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.884303860798" Y="4.724288924129" />
                  <Point X="27.013384196062" Y="2.409331175287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.186345700056" Y="2.054707539203" />
                  <Point X="28.645359055971" Y="-0.936713149346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.004108468796" Y="4.695364419219" />
                  <Point X="27.047017591793" Y="2.557083837921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.325227946855" Y="1.986668078169" />
                  <Point X="28.365071145411" Y="-0.14532642647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.447778828624" Y="-0.314902307089" />
                  <Point X="28.75830889056" Y="-0.951583286003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.123913076794" Y="4.666439914309" />
                  <Point X="27.101714154143" Y="2.661650609121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.43604521462" Y="1.976170351465" />
                  <Point X="28.363505705375" Y="0.074594544357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.620328938356" Y="-0.45197111683" />
                  <Point X="28.871258725149" Y="-0.96645342266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.243717684792" Y="4.637515409399" />
                  <Point X="27.159009483601" Y="2.760889118136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.533980274824" Y="1.992085064411" />
                  <Point X="28.408152839086" Y="0.199765697702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.749063665176" Y="-0.499205078667" />
                  <Point X="28.984208559739" Y="-0.981323559317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.36352233025" Y="4.608590827684" />
                  <Point X="27.216304813059" Y="2.86012762715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.624921994232" Y="2.022338250856" />
                  <Point X="28.472709712562" Y="0.284115835119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.870650711102" Y="-0.531784122909" />
                  <Point X="29.097158394328" Y="-0.996193695974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.48546374794" Y="4.575285213655" />
                  <Point X="27.273600142517" Y="2.959366136165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.707730922738" Y="2.06926612973" />
                  <Point X="28.006614268196" Y="1.456464458353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.2225978691" Y="1.013632451701" />
                  <Point X="28.547564582135" Y="0.347351951581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.992237757028" Y="-0.564363167151" />
                  <Point X="29.210108228917" Y="-1.011063832631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.613878014455" Y="4.528708292811" />
                  <Point X="27.330895471974" Y="3.05860464518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.790204236484" Y="2.116882120835" />
                  <Point X="28.048668712529" Y="1.586951412688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.352455155419" Y="0.96409690181" />
                  <Point X="28.630016981818" Y="0.39501082287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.113824877346" Y="-0.596942363918" />
                  <Point X="29.323058063506" Y="-1.025933969288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.742292280971" Y="4.482131371968" />
                  <Point X="27.388190801432" Y="3.157843154195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.87267755023" Y="2.16449811194" />
                  <Point X="28.110813806861" Y="1.676246430151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.464402519033" Y="0.951282135246" />
                  <Point X="28.719297895803" Y="0.428669165053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.235412018577" Y="-0.629521603563" />
                  <Point X="29.436007898096" Y="-1.040804105945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.871531276365" Y="4.433863506336" />
                  <Point X="27.44548613089" Y="3.25708166321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.955150863976" Y="2.212114103046" />
                  <Point X="28.180666508158" Y="1.749738511445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.565212241985" Y="0.961302916116" />
                  <Point X="28.812778360399" Y="0.453717152487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.356999159807" Y="-0.662100843208" />
                  <Point X="29.548957732685" Y="-1.055674242603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.00846163111" Y="4.369826017081" />
                  <Point X="27.502781460348" Y="3.356320172225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.03762417786" Y="2.259730093869" />
                  <Point X="28.257579118295" Y="1.808755634521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.664531962667" Y="0.974378654363" />
                  <Point X="28.906258824994" Y="0.478765139921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.478586301038" Y="-0.694680082852" />
                  <Point X="29.661907512036" Y="-1.070544266005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.145391985854" Y="4.305788527826" />
                  <Point X="27.560076789806" Y="3.455558681239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.120097492039" Y="2.307346084088" />
                  <Point X="28.334491728433" Y="1.867772757597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.76385168335" Y="0.987454392609" />
                  <Point X="28.99973928959" Y="0.503813127355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.600173442268" Y="-0.727259322497" />
                  <Point X="29.741839287774" Y="-1.017717349758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.284819330469" Y="4.236631450648" />
                  <Point X="27.617372119264" Y="3.554797190254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.202570806217" Y="2.354962074308" />
                  <Point X="28.41140433857" Y="1.926789880673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.86317140599" Y="1.000530126843" />
                  <Point X="29.093219754185" Y="0.528861114789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.721760583499" Y="-0.759838562142" />
                  <Point X="29.771923347244" Y="-0.862687469351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.432472523408" Y="4.150608885051" />
                  <Point X="27.674667448722" Y="3.654035699269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.285044120395" Y="2.402578064527" />
                  <Point X="28.488316948708" Y="1.985807003748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.962491158701" Y="1.013605799422" />
                  <Point X="29.186700218781" Y="0.553909102223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.580125814257" Y="4.06458611871" />
                  <Point X="27.731962753877" Y="3.753274258112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.367517434573" Y="2.450194054747" />
                  <Point X="28.565229558846" Y="2.044824126824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.061810911412" Y="1.026681472" />
                  <Point X="29.280180683376" Y="0.578957089657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.737219524035" Y="3.95920762517" />
                  <Point X="27.78925801916" Y="3.852512898706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.449990748751" Y="2.497810044967" />
                  <Point X="28.642142168983" Y="2.1038412499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.161130664124" Y="1.039757144579" />
                  <Point X="29.373661147972" Y="0.604005077092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.53246406293" Y="2.545426035186" />
                  <Point X="28.719054779121" Y="2.162858372976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.260450416835" Y="1.052832817158" />
                  <Point X="29.467141612568" Y="0.629053064526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.614937377108" Y="2.593042025406" />
                  <Point X="28.795967389259" Y="2.221875496052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.359770169546" Y="1.065908489736" />
                  <Point X="29.560622077163" Y="0.65410105196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.697410691286" Y="2.640658015626" />
                  <Point X="28.872879999396" Y="2.280892619128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.459089922257" Y="1.078984162315" />
                  <Point X="29.654102541759" Y="0.679149039394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.779884005464" Y="2.688274005845" />
                  <Point X="28.949792609534" Y="2.339909742204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.558409674968" Y="1.092059834893" />
                  <Point X="29.747582952929" Y="0.704197136365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.862357319642" Y="2.735889996065" />
                  <Point X="29.026705211342" Y="2.398926882358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.657729427679" Y="1.105135507472" />
                  <Point X="29.753551003127" Y="0.908672163226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.958012785599" Y="2.756478569853" />
                  <Point X="29.103617682436" Y="2.457944290516" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.9998359375" Y="0.001625976562" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.771388671875" Y="-4.656159179688" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544433594" />
                  <Point X="25.341763671875" Y="-3.344965332031" />
                  <Point X="25.300591796875" Y="-3.285644042969" />
                  <Point X="25.2743359375" Y="-3.266399658203" />
                  <Point X="25.084689453125" Y="-3.207540039062" />
                  <Point X="25.0209765625" Y="-3.187766357422" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.801689453125" Y="-3.246625976562" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285644042969" />
                  <Point X="24.589166015625" Y="-3.462222900391" />
                  <Point X="24.547994140625" Y="-3.521544433594" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.4400390625" Y="-3.913054931641" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.97365625" Y="-4.952409667969" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.679875" Y="-4.8814921875" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.654755859375" Y="-4.825204101562" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534756347656" />
                  <Point X="23.64501171875" Y="-4.306984375" />
                  <Point X="23.6297890625" Y="-4.23046484375" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.439115234375" Y="-4.049505371094" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.119021484375" Y="-3.970573974609" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.817025390625" Y="-4.102813964844" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.683712890625" Y="-4.230997070312" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.25248828125" Y="-4.234680664062" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.83969921875" Y="-3.933180419922" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="21.936734375" Y="-3.594275634766" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597593017578" />
                  <Point X="22.486021484375" Y="-2.568763916016" />
                  <Point X="22.468677734375" Y="-2.551420654297" />
                  <Point X="22.43984765625" Y="-2.53719921875" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="22.08407421875" Y="-2.730671630859" />
                  <Point X="21.15704296875" Y="-3.265893554688" />
                  <Point X="20.923876953125" Y="-2.959563720703" />
                  <Point X="20.838296875" Y="-2.847126464844" />
                  <Point X="20.620017578125" Y="-2.481106689453" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="20.861236328125" Y="-2.171271240234" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396013916016" />
                  <Point X="21.8599453125" Y="-1.373747192383" />
                  <Point X="21.8618828125" Y="-1.366266601562" />
                  <Point X="21.859673828125" Y="-1.334594970703" />
                  <Point X="21.83883984375" Y="-1.310638305664" />
                  <Point X="21.819017578125" Y="-1.298971313477" />
                  <Point X="21.812357421875" Y="-1.295051757812" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.371146484375" Y="-1.342459960938" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.106080078125" Y="-1.142229492188" />
                  <Point X="20.072607421875" Y="-1.011187194824" />
                  <Point X="20.01485546875" Y="-0.607398620605" />
                  <Point X="20.001603515625" Y="-0.514742370605" />
                  <Point X="20.331892578125" Y="-0.426242004395" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.45810546875" Y="-0.121424743652" />
                  <Point X="21.478875" Y="-0.107009292603" />
                  <Point X="21.48585546875" Y="-0.102165763855" />
                  <Point X="21.505103515625" Y="-0.075906845093" />
                  <Point X="21.51202734375" Y="-0.053595344543" />
                  <Point X="21.514353515625" Y="-0.046099815369" />
                  <Point X="21.514353515625" Y="-0.016460264206" />
                  <Point X="21.5074296875" Y="0.005851236343" />
                  <Point X="21.505103515625" Y="0.013346765518" />
                  <Point X="21.485859375" Y="0.039602031708" />
                  <Point X="21.465083984375" Y="0.05402022171" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="21.06941796875" Y="0.166062667847" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.06078515625" Y="0.85064440918" />
                  <Point X="20.08235546875" Y="0.996414794922" />
                  <Point X="20.198607421875" Y="1.425422851562" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.45059765625" Y="1.498793823242" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.314275390625" Y="1.410363769531" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056762695" />
                  <Point X="21.3608828125" Y="1.443787841797" />
                  <Point X="21.37933203125" Y="1.488328735352" />
                  <Point X="21.385529296875" Y="1.503292114258" />
                  <Point X="21.38928515625" Y="1.524605712891" />
                  <Point X="21.383685546875" Y="1.545510498047" />
                  <Point X="21.36142578125" Y="1.588273925781" />
                  <Point X="21.35394140625" Y="1.602645874023" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.126009765625" Y="1.783447143555" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.756169921875" Y="2.643407958984" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="21.14793359375" Y="3.182829101562" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.35187890625" Y="3.209248046875" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.9255234375" Y="2.914832275391" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927405273438" />
                  <Point X="22.032203125" Y="2.972858642578" />
                  <Point X="22.04747265625" Y="2.988128662109" />
                  <Point X="22.0591015625" Y="3.006381347656" />
                  <Point X="22.061927734375" Y="3.027838134766" />
                  <Point X="22.05632421875" Y="3.091874755859" />
                  <Point X="22.054443359375" Y="3.113387695312" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.95309375" Y="3.298300537109" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="22.10114453125" Y="4.062409667969" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.7321328125" Y="4.443791015625" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.882486328125" Y="4.482649414062" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.120025390625" Y="4.23655859375" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.16487890625" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.22225" />
                  <Point X="23.260427734375" Y="4.252999511719" />
                  <Point X="23.2853671875" Y="4.263329589844" />
                  <Point X="23.30309765625" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.29448828125" />
                  <Point X="23.338080078125" Y="4.371120605469" />
                  <Point X="23.346197265625" Y="4.396864746094" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.33730078125" Y="4.500325683594" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.84292578125" Y="4.8503125" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.619884765625" Y="4.972110351562" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.8132109375" Y="4.850743164062" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.103048828125" Y="4.492262207031" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.695197265625" Y="4.942846679688" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.346654296875" Y="4.808122070312" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.8256875" Y="4.653995605469" />
                  <Point X="26.9310390625" Y="4.615784179688" />
                  <Point X="27.237201171875" Y="4.472603027344" />
                  <Point X="27.3386953125" Y="4.42513671875" />
                  <Point X="27.634466796875" Y="4.252819824219" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="28.011474609375" Y="3.997318603516" />
                  <Point X="28.0687421875" Y="3.956593017578" />
                  <Point X="27.874810546875" Y="3.620693847656" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491518066406" />
                  <Point X="27.208783203125" Y="2.431421142578" />
                  <Point X="27.2033828125" Y="2.411231689453" />
                  <Point X="27.202044921875" Y="2.392324462891" />
                  <Point X="27.2083125" Y="2.340357910156" />
                  <Point X="27.210416015625" Y="2.322905517578" />
                  <Point X="27.218681640625" Y="2.300812011719" />
                  <Point X="27.250837890625" Y="2.253423583984" />
                  <Point X="27.27494140625" Y="2.224203613281" />
                  <Point X="27.322330078125" Y="2.192048339844" />
                  <Point X="27.33825" Y="2.18124609375" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.4123046875" Y="2.166713867188" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.50876171875" Y="2.182017089844" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="27.916322265625" Y="2.409089599609" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.144427734375" Y="2.822715332031" />
                  <Point X="29.20259765625" Y="2.741874023438" />
                  <Point X="29.3581015625" Y="2.484898925781" />
                  <Point X="29.387513671875" Y="2.436295654297" />
                  <Point X="29.13776171875" Y="2.244654296875" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583831665039" />
                  <Point X="28.236119140625" Y="1.527406494141" />
                  <Point X="28.22158984375" Y="1.508450561523" />
                  <Point X="28.21312109375" Y="1.491503417969" />
                  <Point X="28.197009765625" Y="1.433893066406" />
                  <Point X="28.191595703125" Y="1.41453894043" />
                  <Point X="28.190779296875" Y="1.390965087891" />
                  <Point X="28.204005859375" Y="1.326866088867" />
                  <Point X="28.20844921875" Y="1.30533215332" />
                  <Point X="28.215646484375" Y="1.287956665039" />
                  <Point X="28.251619140625" Y="1.233279907227" />
                  <Point X="28.263703125" Y="1.214911254883" />
                  <Point X="28.280947265625" Y="1.198820556641" />
                  <Point X="28.333078125" Y="1.169476318359" />
                  <Point X="28.35058984375" Y="1.159618164062" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.439048828125" Y="1.144304443359" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.83230078125" Y="1.18810546875" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.91420703125" Y="1.054000610352" />
                  <Point X="29.939193359375" Y="0.951367370605" />
                  <Point X="29.9881953125" Y="0.63663067627" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="29.714306640625" Y="0.498578338623" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819503784" />
                  <Point X="28.659841796875" Y="0.192793762207" />
                  <Point X="28.636578125" Y="0.179347137451" />
                  <Point X="28.622265625" Y="0.166925323486" />
                  <Point X="28.580716796875" Y="0.113983383179" />
                  <Point X="28.566759765625" Y="0.096197647095" />
                  <Point X="28.556986328125" Y="0.07473449707" />
                  <Point X="28.54313671875" Y="0.002418428898" />
                  <Point X="28.538484375" Y="-0.021876060486" />
                  <Point X="28.538484375" Y="-0.040684017181" />
                  <Point X="28.552333984375" Y="-0.113000091553" />
                  <Point X="28.556986328125" Y="-0.137294570923" />
                  <Point X="28.566759765625" Y="-0.158757720947" />
                  <Point X="28.60830859375" Y="-0.211699508667" />
                  <Point X="28.622265625" Y="-0.229485397339" />
                  <Point X="28.636578125" Y="-0.241907211304" />
                  <Point X="28.70582421875" Y="-0.281932952881" />
                  <Point X="28.729087890625" Y="-0.295379577637" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.06809375" Y="-0.38798614502" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.962380859375" Y="-0.873882568359" />
                  <Point X="29.948431640625" Y="-0.966411682129" />
                  <Point X="29.885650390625" Y="-1.241528442383" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.540548828125" Y="-1.246206787109" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.258931640625" Y="-1.127881103516" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.185447265625" Y="-1.154697021484" />
                  <Point X="28.10330078125" Y="-1.253494262695" />
                  <Point X="28.075703125" Y="-1.286684936523" />
                  <Point X="28.064359375" Y="-1.314070800781" />
                  <Point X="28.0525859375" Y="-1.442017700195" />
                  <Point X="28.048630859375" Y="-1.485001220703" />
                  <Point X="28.056361328125" Y="-1.516621826172" />
                  <Point X="28.13157421875" Y="-1.633610229492" />
                  <Point X="28.156841796875" Y="-1.672912353516" />
                  <Point X="28.168462890625" Y="-1.685540771484" />
                  <Point X="28.471849609375" Y="-1.918338745117" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.243451171875" Y="-2.738517578125" />
                  <Point X="29.2041328125" Y="-2.802139648438" />
                  <Point X="29.074291015625" Y="-2.986627929688" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.758595703125" Y="-2.839533935547" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.575591796875" Y="-2.224100830078" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219245117188" />
                  <Point X="27.354703125" Y="-2.289965576172" />
                  <Point X="27.309560546875" Y="-2.313724121094" />
                  <Point X="27.2886015625" Y="-2.334682617188" />
                  <Point X="27.217880859375" Y="-2.469057617188" />
                  <Point X="27.19412109375" Y="-2.514200683594" />
                  <Point X="27.1891640625" Y="-2.546375" />
                  <Point X="27.218376953125" Y="-2.708125488281" />
                  <Point X="27.22819140625" Y="-2.762465332031" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.42905078125" Y="-3.116254638672" />
                  <Point X="27.98667578125" Y="-4.082087646484" />
                  <Point X="27.881955078125" Y="-4.156886230469" />
                  <Point X="27.83529296875" Y="-4.190215820312" />
                  <Point X="27.690126953125" Y="-4.284179199219" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="27.450193359375" Y="-3.991680419922" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467529297" />
                  <Point X="26.51101953125" Y="-2.877904541016" />
                  <Point X="26.45742578125" Y="-2.843448730469" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.25133203125" Y="-2.851772216797" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868508789062" />
                  <Point X="26.030611328125" Y="-2.980526855469" />
                  <Point X="25.985349609375" Y="-3.018159179688" />
                  <Point X="25.96845703125" Y="-3.045985839844" />
                  <Point X="25.92817578125" Y="-3.231313232422" />
                  <Point X="25.914642578125" Y="-3.293573730469" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.9691796875" Y="-3.730381103516" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.038486328125" Y="-4.953571777344" />
                  <Point X="25.994341796875" Y="-4.963248535156" />
                  <Point X="25.860240234375" Y="-4.987609863281" />
                  <Point X="25.860201171875" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#192" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.144649642936" Y="4.894964317856" Z="1.9" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.9" />
                  <Point X="-0.39208560841" Y="5.053051592006" Z="1.9" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.9" />
                  <Point X="-1.176729493102" Y="4.929736200675" Z="1.9" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.9" />
                  <Point X="-1.718428127287" Y="4.525079843394" Z="1.9" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.9" />
                  <Point X="-1.715763108196" Y="4.417436106722" Z="1.9" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.9" />
                  <Point X="-1.764864258123" Y="4.330473894765" Z="1.9" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.9" />
                  <Point X="-1.86304291256" Y="4.312189053701" Z="1.9" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.9" />
                  <Point X="-2.084002446144" Y="4.544367625996" Z="1.9" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.9" />
                  <Point X="-2.298307860506" Y="4.518778454483" Z="1.9" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.9" />
                  <Point X="-2.935435094887" Y="4.133369210309" Z="1.9" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.9" />
                  <Point X="-3.09636466465" Y="3.304580560783" Z="1.9" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.9" />
                  <Point X="-2.99964252701" Y="3.118799942839" Z="1.9" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.9" />
                  <Point X="-3.009310070231" Y="3.039493497655" Z="1.9" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.9" />
                  <Point X="-3.076276522768" Y="2.995922172163" Z="1.9" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.9" />
                  <Point X="-3.629278787514" Y="3.283829284096" Z="1.9" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.9" />
                  <Point X="-3.897686838355" Y="3.244811445277" Z="1.9" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.9" />
                  <Point X="-4.293169386962" Y="2.699893157704" Z="1.9" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.9" />
                  <Point X="-3.910585077811" Y="1.775059564256" Z="1.9" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.9" />
                  <Point X="-3.689083653249" Y="1.596467791918" Z="1.9" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.9" />
                  <Point X="-3.67302049668" Y="1.538740931764" Z="1.9" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.9" />
                  <Point X="-3.706916652312" Y="1.489329775381" Z="1.9" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.9" />
                  <Point X="-4.549034050578" Y="1.579646119964" Z="1.9" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.9" />
                  <Point X="-4.855809191265" Y="1.469780040178" Z="1.9" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.9" />
                  <Point X="-4.994833330217" Y="0.88924335517" Z="1.9" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.9" />
                  <Point X="-3.949681215054" Y="0.149046364732" Z="1.9" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.9" />
                  <Point X="-3.569581761736" Y="0.044225225595" Z="1.9" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.9" />
                  <Point X="-3.546481494795" Y="0.022311442006" Z="1.9" />
                  <Point X="-3.539556741714" Y="0" Z="1.9" />
                  <Point X="-3.54188310634" Y="-0.007495509055" Z="1.9" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.9" />
                  <Point X="-3.555786833388" Y="-0.034650757539" Z="1.9" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.9" />
                  <Point X="-4.687206573719" Y="-0.346665692024" Z="1.9" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.9" />
                  <Point X="-5.040796669856" Y="-0.583197404128" Z="1.9" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.9" />
                  <Point X="-4.94852276388" Y="-1.123323569468" Z="1.9" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.9" />
                  <Point X="-3.628485327756" Y="-1.360752145756" Z="1.9" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.9" />
                  <Point X="-3.212499352862" Y="-1.310782820458" Z="1.9" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.9" />
                  <Point X="-3.194613743864" Y="-1.32993032887" Z="1.9" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.9" />
                  <Point X="-4.175357910045" Y="-2.100323386223" Z="1.9" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.9" />
                  <Point X="-4.429083099941" Y="-2.475436402093" Z="1.9" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.9" />
                  <Point X="-4.122066141794" Y="-2.958566518524" Z="1.9" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.9" />
                  <Point X="-2.897082818471" Y="-2.742692936069" Z="1.9" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.9" />
                  <Point X="-2.568476890356" Y="-2.559853578775" Z="1.9" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.9" />
                  <Point X="-3.1127240771" Y="-3.537995564021" Z="1.9" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.9" />
                  <Point X="-3.196962134688" Y="-3.941517663618" Z="1.9" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.9" />
                  <Point X="-2.779990878498" Y="-4.24591149656" Z="1.9" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.9" />
                  <Point X="-2.282776403379" Y="-4.230154926035" Z="1.9" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.9" />
                  <Point X="-2.161351885092" Y="-4.113107105281" Z="1.9" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.9" />
                  <Point X="-1.890404080408" Y="-3.989187057482" Z="1.9" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.9" />
                  <Point X="-1.600009089756" Y="-4.055817479026" Z="1.9" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.9" />
                  <Point X="-1.410185234075" Y="-4.285460287449" Z="1.9" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.9" />
                  <Point X="-1.400973110234" Y="-4.787397629058" Z="1.9" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.9" />
                  <Point X="-1.338740518269" Y="-4.898635054087" Z="1.9" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.9" />
                  <Point X="-1.042028241973" Y="-4.970213853008" Z="1.9" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.9" />
                  <Point X="-0.517820355852" Y="-3.894715928631" Z="1.9" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.9" />
                  <Point X="-0.37591442196" Y="-3.459451560382" Z="1.9" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.9" />
                  <Point X="-0.189647253562" Y="-3.263098877307" Z="1.9" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.9" />
                  <Point X="0.063711825799" Y="-3.224013167981" Z="1.9" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.9" />
                  <Point X="0.294531437472" Y="-3.342194195386" Z="1.9" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.9" />
                  <Point X="0.716934504549" Y="-4.637820101185" Z="1.9" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.9" />
                  <Point X="0.863018430908" Y="-5.005524447317" Z="1.9" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.9" />
                  <Point X="1.043033785648" Y="-4.971132223944" Z="1.9" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.9" />
                  <Point X="1.012595219865" Y="-3.692574776198" Z="1.9" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.9" />
                  <Point X="0.970878367605" Y="-3.210653193475" Z="1.9" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.9" />
                  <Point X="1.056420254633" Y="-2.987693609357" Z="1.9" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.9" />
                  <Point X="1.249757713572" Y="-2.870281729696" Z="1.9" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.9" />
                  <Point X="1.477824286755" Y="-2.888682470279" Z="1.9" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.9" />
                  <Point X="2.404368723598" Y="-3.990838249063" Z="1.9" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.9" />
                  <Point X="2.711140087136" Y="-4.294873397231" Z="1.9" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.9" />
                  <Point X="2.90486123105" Y="-4.166293252169" Z="1.9" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.9" />
                  <Point X="2.466194310045" Y="-3.059974626358" Z="1.9" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.9" />
                  <Point X="2.261423141552" Y="-2.667958684708" Z="1.9" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.9" />
                  <Point X="2.255969625825" Y="-2.461065064434" Z="1.9" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.9" />
                  <Point X="2.371833313939" Y="-2.302931684142" Z="1.9" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.9" />
                  <Point X="2.560548118804" Y="-2.242024867939" Z="1.9" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.9" />
                  <Point X="3.727438970749" Y="-2.85155519023" Z="1.9" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.9" />
                  <Point X="4.109023203717" Y="-2.984125058715" Z="1.9" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.9" />
                  <Point X="4.279828029799" Y="-2.7335225226" Z="1.9" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.9" />
                  <Point X="3.496130940945" Y="-1.847390949735" Z="1.9" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.9" />
                  <Point X="3.167475190423" Y="-1.575290884728" Z="1.9" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.9" />
                  <Point X="3.096217841959" Y="-1.41531892142" Z="1.9" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.9" />
                  <Point X="3.135588633344" Y="-1.254181395859" Z="1.9" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.9" />
                  <Point X="3.263393237605" Y="-1.145460291072" Z="1.9" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.9" />
                  <Point X="4.527865963876" Y="-1.264498959513" Z="1.9" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.9" />
                  <Point X="4.928238639893" Y="-1.221372705728" Z="1.9" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.9" />
                  <Point X="5.005665653543" Y="-0.85005634132" Z="1.9" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.9" />
                  <Point X="4.074877436331" Y="-0.308409852032" Z="1.9" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.9" />
                  <Point X="3.724689353181" Y="-0.207364005627" Z="1.9" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.9" />
                  <Point X="3.641484706803" Y="-0.149552425249" Z="1.9" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.9" />
                  <Point X="3.595284054413" Y="-0.072316026558" Z="1.9" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.9" />
                  <Point X="3.586087396011" Y="0.024294504634" Z="1.9" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.9" />
                  <Point X="3.613894731597" Y="0.114396313326" Z="1.9" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.9" />
                  <Point X="3.678706061172" Y="0.180784838615" Z="1.9" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.9" />
                  <Point X="4.721090380975" Y="0.481562080737" Z="1.9" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.9" />
                  <Point X="5.031442782416" Y="0.67560269979" Z="1.9" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.9" />
                  <Point X="4.956632231125" Y="1.097107444214" Z="1.9" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.9" />
                  <Point X="3.819619256262" Y="1.268957928878" Z="1.9" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.9" />
                  <Point X="3.439442940941" Y="1.225153481828" Z="1.9" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.9" />
                  <Point X="3.351447833367" Y="1.244326760733" Z="1.9" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.9" />
                  <Point X="3.287233590809" Y="1.292039640836" Z="1.9" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.9" />
                  <Point X="3.246817808503" Y="1.368250355369" Z="1.9" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.9" />
                  <Point X="3.239004629274" Y="1.451703351212" Z="1.9" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.9" />
                  <Point X="3.269646381859" Y="1.528269749167" Z="1.9" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.9" />
                  <Point X="4.162042570142" Y="2.236266501699" Z="1.9" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.9" />
                  <Point X="4.394722799659" Y="2.542065339063" Z="1.9" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.9" />
                  <Point X="4.178856820936" Y="2.883198680372" Z="1.9" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.9" />
                  <Point X="2.885165284464" Y="2.483671016303" Z="1.9" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.9" />
                  <Point X="2.489688454252" Y="2.26159984165" Z="1.9" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.9" />
                  <Point X="2.412133488912" Y="2.247634306363" Z="1.9" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.9" />
                  <Point X="2.344246640759" Y="2.264702996876" Z="1.9" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.9" />
                  <Point X="2.286055603359" Y="2.312778219623" Z="1.9" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.9" />
                  <Point X="2.251795396396" Y="2.37762495492" Z="1.9" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.9" />
                  <Point X="2.250928101007" Y="2.44978112163" Z="1.9" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.9" />
                  <Point X="2.911954336556" Y="3.626973797491" Z="1.9" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.9" />
                  <Point X="3.034293524672" Y="4.069345622758" Z="1.9" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.9" />
                  <Point X="2.653479988138" Y="4.327302632986" Z="1.9" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.9" />
                  <Point X="2.252225225044" Y="4.54917442629" Z="1.9" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.9" />
                  <Point X="1.836580346373" Y="4.732279678552" Z="1.9" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.9" />
                  <Point X="1.352232193981" Y="4.888005684419" Z="1.9" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.9" />
                  <Point X="0.694247254683" Y="5.023854669775" Z="1.9" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.9" />
                  <Point X="0.048594918617" Y="4.536483188094" Z="1.9" />
                  <Point X="0" Y="4.355124473572" Z="1.9" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>