<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#191" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2749" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.855265625" Y="-4.602142578125" />
                  <Point X="25.5633046875" Y="-3.512524414063" />
                  <Point X="25.55772265625" Y="-3.497141357422" />
                  <Point X="25.542365234375" Y="-3.467376708984" />
                  <Point X="25.423033203125" Y="-3.29544140625" />
                  <Point X="25.37863671875" Y="-3.231476318359" />
                  <Point X="25.35675" Y="-3.209019775391" />
                  <Point X="25.330494140625" Y="-3.189776367188" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.1178359375" Y="-3.118357421875" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.778515625" Y="-3.154347412109" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.189777587891" />
                  <Point X="24.655560546875" Y="-3.209021972656" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.51434375" Y="-3.403412353516" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481571533203" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.340390625" Y="-3.917895996094" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.997658203125" Y="-4.860295898438" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.75358203125" Y="-4.802362792969" />
                  <Point X="23.7850390625" Y="-4.563437011719" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.739376953125" Y="-4.29444140625" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182963867188" />
                  <Point X="23.69598828125" Y="-4.155126953125" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.506345703125" Y="-3.982107666016" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.131330078125" Y="-3.876176757812" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.76932421875" Y="-4.020431396484" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.664900390625" Y="-4.099461425781" />
                  <Point X="22.60391796875" Y="-4.17893359375" />
                  <Point X="22.519853515625" Y="-4.288489746094" />
                  <Point X="22.3111484375" Y="-4.159265625" />
                  <Point X="22.198287109375" Y="-4.089384033203" />
                  <Point X="21.90571484375" Y="-3.864114013672" />
                  <Point X="21.89527734375" Y="-3.856077880859" />
                  <Point X="22.03366015625" Y="-3.616393798828" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647654541016" />
                  <Point X="22.593412109375" Y="-2.616128173828" />
                  <Point X="22.59442578125" Y="-2.585194091797" />
                  <Point X="22.58544140625" Y="-2.555575927734" />
                  <Point X="22.571224609375" Y="-2.526747070312" />
                  <Point X="22.55319921875" Y="-2.501591552734" />
                  <Point X="22.53585546875" Y="-2.484246582031" />
                  <Point X="22.510701171875" Y="-2.466219726562" />
                  <Point X="22.48187109375" Y="-2.451999511719" />
                  <Point X="22.45225390625" Y="-2.443012695312" />
                  <Point X="22.42131640625" Y="-2.444023925781" />
                  <Point X="22.389787109375" Y="-2.450294433594" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="22.01119140625" Y="-2.663053466797" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="21.006302734375" Y="-2.911002441406" />
                  <Point X="20.917142578125" Y="-2.793862548828" />
                  <Point X="20.70738671875" Y="-2.442137695313" />
                  <Point X="20.693857421875" Y="-2.419450439453" />
                  <Point X="20.944708984375" Y="-2.226965332031" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.91541796875" Y="-1.475598754883" />
                  <Point X="21.9333828125" Y="-1.448471679688" />
                  <Point X="21.946142578125" Y="-1.4198359375" />
                  <Point X="21.9517578125" Y="-1.398154663086" />
                  <Point X="21.95384765625" Y="-1.390088500977" />
                  <Point X="21.95665234375" Y="-1.359666625977" />
                  <Point X="21.9544453125" Y="-1.327992919922" />
                  <Point X="21.9474453125" Y="-1.298242797852" />
                  <Point X="21.931359375" Y="-1.272255249023" />
                  <Point X="21.9105234375" Y="-1.24829675293" />
                  <Point X="21.88702734375" Y="-1.228766845703" />
                  <Point X="21.8677265625" Y="-1.217406616211" />
                  <Point X="21.860544921875" Y="-1.213180419922" />
                  <Point X="21.831279296875" Y="-1.201954956055" />
                  <Point X="21.79939453125" Y="-1.195474609375" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="21.326703125" Y="-1.252491088867" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.200796875" Y="-1.129180908203" />
                  <Point X="20.165923828125" Y="-0.992650634766" />
                  <Point X="20.110427734375" Y="-0.604637756348" />
                  <Point X="20.107576171875" Y="-0.584698364258" />
                  <Point X="20.3856875" Y="-0.510178741455" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.482505859375" Y="-0.214828048706" />
                  <Point X="21.512271484375" Y="-0.199470657349" />
                  <Point X="21.5325" Y="-0.185431564331" />
                  <Point X="21.540025390625" Y="-0.180208602905" />
                  <Point X="21.562484375" Y="-0.158319366455" />
                  <Point X="21.581728515625" Y="-0.132060699463" />
                  <Point X="21.595833984375" Y="-0.1040625" />
                  <Point X="21.602576171875" Y="-0.08233795929" />
                  <Point X="21.605083984375" Y="-0.074255577087" />
                  <Point X="21.609353515625" Y="-0.046097396851" />
                  <Point X="21.609353515625" Y="-0.016462751389" />
                  <Point X="21.605083984375" Y="0.01169527626" />
                  <Point X="21.598341796875" Y="0.033420124054" />
                  <Point X="21.595833984375" Y="0.041502353668" />
                  <Point X="21.581728515625" Y="0.069500549316" />
                  <Point X="21.562484375" Y="0.095759376526" />
                  <Point X="21.540025390625" Y="0.117648612976" />
                  <Point X="21.519796875" Y="0.131687698364" />
                  <Point X="21.510693359375" Y="0.137274810791" />
                  <Point X="21.487599609375" Y="0.149717971802" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="21.064796875" Y="0.265651824951" />
                  <Point X="20.108185546875" Y="0.521975402832" />
                  <Point X="20.1530390625" Y="0.825098693848" />
                  <Point X="20.17551171875" Y="0.976968505859" />
                  <Point X="20.28722265625" Y="1.389219360352" />
                  <Point X="20.29644921875" Y="1.423268066406" />
                  <Point X="20.459134765625" Y="1.401850097656" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228027344" />
                  <Point X="21.296865234375" Y="1.305263793945" />
                  <Point X="21.341634765625" Y="1.319379760742" />
                  <Point X="21.358291015625" Y="1.324631225586" />
                  <Point X="21.37722265625" Y="1.332961181641" />
                  <Point X="21.395966796875" Y="1.343782836914" />
                  <Point X="21.4126484375" Y="1.356014160156" />
                  <Point X="21.426287109375" Y="1.37156628418" />
                  <Point X="21.438701171875" Y="1.389295532227" />
                  <Point X="21.448650390625" Y="1.407432495117" />
                  <Point X="21.46661328125" Y="1.450802001953" />
                  <Point X="21.473296875" Y="1.466936767578" />
                  <Point X="21.479083984375" Y="1.486787597656" />
                  <Point X="21.48284375" Y="1.508103515625" />
                  <Point X="21.484197265625" Y="1.528749511719" />
                  <Point X="21.481048828125" Y="1.549198730469" />
                  <Point X="21.4754453125" Y="1.570106201172" />
                  <Point X="21.46794921875" Y="1.589378662109" />
                  <Point X="21.4462734375" Y="1.631017456055" />
                  <Point X="21.438208984375" Y="1.646508300781" />
                  <Point X="21.426716796875" Y="1.663707763672" />
                  <Point X="21.4128046875" Y="1.680287231445" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="21.167087890625" Y="1.871671264648" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.8315234375" Y="2.584052246094" />
                  <Point X="20.918853515625" Y="2.733665771484" />
                  <Point X="21.21476171875" Y="3.114016357422" />
                  <Point X="21.24949609375" Y="3.158661865234" />
                  <Point X="21.317236328125" Y="3.119551757813" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.915560546875" Y="2.820341308594" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.00098828125" Y="2.826505126953" />
                  <Point X="22.019541015625" Y="2.835655029297" />
                  <Point X="22.037794921875" Y="2.847284912109" />
                  <Point X="22.053921875" Y="2.860228759766" />
                  <Point X="22.098181640625" Y="2.904486816406" />
                  <Point X="22.114646484375" Y="2.920952148438" />
                  <Point X="22.127595703125" Y="2.937085449219" />
                  <Point X="22.139224609375" Y="2.955339355469" />
                  <Point X="22.14837109375" Y="2.973888427734" />
                  <Point X="22.1532890625" Y="2.993977783203" />
                  <Point X="22.156115234375" Y="3.015436035156" />
                  <Point X="22.15656640625" Y="3.036120605469" />
                  <Point X="22.151111328125" Y="3.098473144531" />
                  <Point X="22.14908203125" Y="3.121670166016" />
                  <Point X="22.145044921875" Y="3.141962158203" />
                  <Point X="22.13853515625" Y="3.162604736328" />
                  <Point X="22.130205078125" Y="3.181533447266" />
                  <Point X="22.027939453125" Y="3.358659912109" />
                  <Point X="21.81666796875" Y="3.724595703125" />
                  <Point X="22.147291015625" Y="3.978080810547" />
                  <Point X="22.29937890625" Y="4.094685791016" />
                  <Point X="22.7654296875" Y="4.353613769531" />
                  <Point X="22.83296484375" Y="4.391134277344" />
                  <Point X="22.9568046875" Y="4.229740722656" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.074287109375" Y="4.153268066406" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.1193828125" Y="4.132331054688" />
                  <Point X="23.1402890625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.12358203125" />
                  <Point X="23.181373046875" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222546875" Y="4.134481933594" />
                  <Point X="23.294828125" Y="4.164422851562" />
                  <Point X="23.321720703125" Y="4.175561523437" />
                  <Point X="23.3398515625" Y="4.185507324219" />
                  <Point X="23.35758203125" Y="4.197920898438" />
                  <Point X="23.373134765625" Y="4.211559082031" />
                  <Point X="23.3853671875" Y="4.228241210938" />
                  <Point X="23.396189453125" Y="4.246984863281" />
                  <Point X="23.404521484375" Y="4.265920898438" />
                  <Point X="23.428046875" Y="4.340537597656" />
                  <Point X="23.43680078125" Y="4.368297363281" />
                  <Point X="23.4408359375" Y="4.388583007812" />
                  <Point X="23.44272265625" Y="4.410146484375" />
                  <Point X="23.442271484375" Y="4.430827636719" />
                  <Point X="23.43064453125" Y="4.519138183594" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.853482421875" Y="4.754609375" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.615361328125" Y="4.875932617188" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.72525" Y="4.811958984375" />
                  <Point X="24.866095703125" Y="4.286315917969" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.1986171875" Y="4.481870605469" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.672126953125" Y="4.849743164062" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.311482421875" Y="4.718884277344" />
                  <Point X="26.48102734375" Y="4.677950683594" />
                  <Point X="26.784884765625" Y="4.567739746094" />
                  <Point X="26.89464453125" Y="4.527929199219" />
                  <Point X="27.1888515625" Y="4.390338378906" />
                  <Point X="27.294576171875" Y="4.34089453125" />
                  <Point X="27.578814453125" Y="4.175296386719" />
                  <Point X="27.6809765625" Y="4.115775878906" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.775578125" Y="3.638816162109" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.5399375" />
                  <Point X="27.133078125" Y="2.51605859375" />
                  <Point X="27.1174296875" Y="2.457542236328" />
                  <Point X="27.111607421875" Y="2.435772216797" />
                  <Point X="27.108619140625" Y="2.417936767578" />
                  <Point X="27.107728515625" Y="2.380953857422" />
                  <Point X="27.113830078125" Y="2.330353759766" />
                  <Point X="27.116099609375" Y="2.311529052734" />
                  <Point X="27.121443359375" Y="2.289603027344" />
                  <Point X="27.1297109375" Y="2.267512695312" />
                  <Point X="27.140072265625" Y="2.247469482422" />
                  <Point X="27.1713828125" Y="2.201327148438" />
                  <Point X="27.18303125" Y="2.184160888672" />
                  <Point X="27.194466796875" Y="2.170329345703" />
                  <Point X="27.221599609375" Y="2.145592529297" />
                  <Point X="27.2677421875" Y="2.114282958984" />
                  <Point X="27.284908203125" Y="2.102635009766" />
                  <Point X="27.304953125" Y="2.092272705078" />
                  <Point X="27.327041015625" Y="2.084006591797" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.399564453125" Y="2.072562011719" />
                  <Point X="27.40858984375" Y="2.071907714844" />
                  <Point X="27.44631640625" Y="2.070975341797" />
                  <Point X="27.473205078125" Y="2.074170898438" />
                  <Point X="27.53172265625" Y="2.089819091797" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="27.99319921875" Y="2.343778320312" />
                  <Point X="28.967326171875" Y="2.906190429688" />
                  <Point X="29.062669921875" Y="2.773684082031" />
                  <Point X="29.12328125" Y="2.689450439453" />
                  <Point X="29.262201171875" Y="2.459884033203" />
                  <Point X="29.057599609375" Y="2.302887451172" />
                  <Point X="28.23078515625" Y="1.668451171875" />
                  <Point X="28.2214296875" Y="1.660245605469" />
                  <Point X="28.20397265625" Y="1.641626953125" />
                  <Point X="28.161859375" Y="1.586685424805" />
                  <Point X="28.14619140625" Y="1.566245605469" />
                  <Point X="28.136607421875" Y="1.550915283203" />
                  <Point X="28.121630859375" Y="1.517089355469" />
                  <Point X="28.105943359375" Y="1.460994018555" />
                  <Point X="28.10010546875" Y="1.440125" />
                  <Point X="28.09665234375" Y="1.417827026367" />
                  <Point X="28.0958359375" Y="1.394253417969" />
                  <Point X="28.097740234375" Y="1.371766601562" />
                  <Point X="28.110619140625" Y="1.309353515625" />
                  <Point X="28.11541015625" Y="1.286133789062" />
                  <Point X="28.120681640625" Y="1.268976318359" />
                  <Point X="28.136283203125" Y="1.23574206543" />
                  <Point X="28.171310546875" Y="1.182503051758" />
                  <Point X="28.18433984375" Y="1.162696533203" />
                  <Point X="28.198888671875" Y="1.145456176758" />
                  <Point X="28.2161328125" Y="1.129364501953" />
                  <Point X="28.23434765625" Y="1.116034667969" />
                  <Point X="28.285107421875" Y="1.087462280273" />
                  <Point X="28.303990234375" Y="1.076832397461" />
                  <Point X="28.32051953125" Y="1.069502563477" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.424748046875" Y="1.050368408203" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.872609375" Y="1.097592285156" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.819908203125" Y="1.039723510742" />
                  <Point X="29.84594140625" Y="0.932786804199" />
                  <Point X="29.8908671875" Y="0.644239013672" />
                  <Point X="29.664126953125" Y="0.583484069824" />
                  <Point X="28.716580078125" Y="0.32958972168" />
                  <Point X="28.7047890625" Y="0.325585357666" />
                  <Point X="28.681546875" Y="0.315067810059" />
                  <Point X="28.61412109375" Y="0.276094543457" />
                  <Point X="28.589037109375" Y="0.261595367432" />
                  <Point X="28.574314453125" Y="0.251098312378" />
                  <Point X="28.54753125" Y="0.22557673645" />
                  <Point X="28.507076171875" Y="0.174027053833" />
                  <Point X="28.492025390625" Y="0.154848937988" />
                  <Point X="28.48030078125" Y="0.135569137573" />
                  <Point X="28.47052734375" Y="0.114106430054" />
                  <Point X="28.463681640625" Y="0.092604255676" />
                  <Point X="28.450197265625" Y="0.022190063477" />
                  <Point X="28.4451796875" Y="-0.004006225109" />
                  <Point X="28.443484375" Y="-0.021874078751" />
                  <Point X="28.4451796875" Y="-0.058553920746" />
                  <Point X="28.4586640625" Y="-0.128968109131" />
                  <Point X="28.463681640625" Y="-0.155164260864" />
                  <Point X="28.47052734375" Y="-0.176666427612" />
                  <Point X="28.48030078125" Y="-0.19812928772" />
                  <Point X="28.492025390625" Y="-0.217409088135" />
                  <Point X="28.53248046875" Y="-0.268958770752" />
                  <Point X="28.54753125" Y="-0.28813671875" />
                  <Point X="28.560001953125" Y="-0.301238525391" />
                  <Point X="28.589037109375" Y="-0.32415536499" />
                  <Point X="28.656462890625" Y="-0.363128631592" />
                  <Point X="28.681546875" Y="-0.377627929688" />
                  <Point X="28.692708984375" Y="-0.383137786865" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="29.06909765625" Y="-0.486606445312" />
                  <Point X="29.891474609375" Y="-0.706961730957" />
                  <Point X="29.869556640625" Y="-0.852333129883" />
                  <Point X="29.855025390625" Y="-0.948725341797" />
                  <Point X="29.801173828125" Y="-1.18469921875" />
                  <Point X="29.52326953125" Y="-1.148112304688" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.242328125" Y="-1.034271850586" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.163974609375" Y="-1.056596923828" />
                  <Point X="28.1361484375" Y="-1.073489135742" />
                  <Point X="28.1123984375" Y="-1.093960083008" />
                  <Point X="28.032412109375" Y="-1.190159057617" />
                  <Point X="28.002654296875" Y="-1.225947998047" />
                  <Point X="27.98793359375" Y="-1.250330810547" />
                  <Point X="27.97658984375" Y="-1.277717651367" />
                  <Point X="27.969759765625" Y="-1.305366210938" />
                  <Point X="27.958294921875" Y="-1.429948242188" />
                  <Point X="27.95403125" Y="-1.476296508789" />
                  <Point X="27.956349609375" Y="-1.507563232422" />
                  <Point X="27.964080078125" Y="-1.539183349609" />
                  <Point X="27.976451171875" Y="-1.567996948242" />
                  <Point X="28.049685546875" Y="-1.681908813477" />
                  <Point X="28.076931640625" Y="-1.724287475586" />
                  <Point X="28.0869375" Y="-1.737243774414" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.437767578125" Y="-2.011930664062" />
                  <Point X="29.213123046875" Y="-2.606882324219" />
                  <Point X="29.16577734375" Y="-2.683495361328" />
                  <Point X="29.124798828125" Y="-2.749803466797" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="28.779662109375" Y="-2.742000244141" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.596728515625" Y="-2.131381591797" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395507812" />
                  <Point X="27.474609375" Y="-2.125353515625" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.3139921875" Y="-2.204037597656" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246549072266" />
                  <Point X="27.22142578125" Y="-2.267508056641" />
                  <Point X="27.204533203125" Y="-2.290438476562" />
                  <Point X="27.135671875" Y="-2.421279541016" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.100228515625" Y="-2.499734863281" />
                  <Point X="27.095271484375" Y="-2.531908203125" />
                  <Point X="27.09567578125" Y="-2.563259277344" />
                  <Point X="27.12412109375" Y="-2.720755859375" />
                  <Point X="27.134703125" Y="-2.779349609375" />
                  <Point X="27.13898828125" Y="-2.795139648438" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.362041015625" Y="-3.190187988281" />
                  <Point X="27.861287109375" Y="-4.054906494141" />
                  <Point X="27.830466796875" Y="-4.076920166016" />
                  <Point X="27.781845703125" Y="-4.111646972656" />
                  <Point X="27.701767578125" Y="-4.163481933594" />
                  <Point X="27.505392578125" Y="-3.907560546875" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922177490234" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.56658984375" Y="-2.80069140625" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479990234375" Y="-2.751167236328" />
                  <Point X="26.448369140625" Y="-2.743435546875" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.247216796875" Y="-2.756749755859" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.156365234375" Y="-2.769396972656" />
                  <Point X="26.12898046875" Y="-2.780739746094" />
                  <Point X="26.10459765625" Y="-2.7954609375" />
                  <Point X="25.97341796875" Y="-2.904533203125" />
                  <Point X="25.92461328125" Y="-2.945111328125" />
                  <Point X="25.904140625" Y="-2.968861572266" />
                  <Point X="25.887248046875" Y="-2.996688720703" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.83640234375" Y="-3.206262207031" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627685547" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.87931640625" Y="-3.775632324219" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="26.02166796875" Y="-4.860002441406" />
                  <Point X="25.9756796875" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941580078125" Y="-4.752637695312" />
                  <Point X="23.85875390625" Y="-4.731327636719" />
                  <Point X="23.8792265625" Y="-4.575837890625" />
                  <Point X="23.879923828125" Y="-4.56809765625" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497689453125" />
                  <Point X="23.83255078125" Y="-4.275907714844" />
                  <Point X="23.81613671875" Y="-4.193397949219" />
                  <Point X="23.811875" Y="-4.17846875" />
                  <Point X="23.80097265625" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135462402344" />
                  <Point X="23.778259765625" Y="-4.107625488281" />
                  <Point X="23.769423828125" Y="-4.094859863281" />
                  <Point X="23.749791015625" Y="-4.070937011719" />
                  <Point X="23.738994140625" Y="-4.059779785156" />
                  <Point X="23.568984375" Y="-3.910683349609" />
                  <Point X="23.505734375" Y="-3.855215087891" />
                  <Point X="23.49326171875" Y="-3.845965332031" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.13754296875" Y="-3.781380126953" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812255859" />
                  <Point X="22.716544921875" Y="-3.941441894531" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.619556640625" Y="-4.010133789063" />
                  <Point X="22.5972421875" Y="-4.032772216797" />
                  <Point X="22.589533203125" Y="-4.041628417969" />
                  <Point X="22.52855078125" Y="-4.121100585938" />
                  <Point X="22.49680078125" Y="-4.162479492188" />
                  <Point X="22.36116015625" Y="-4.078494873047" />
                  <Point X="22.252408203125" Y="-4.011157714844" />
                  <Point X="22.01913671875" Y="-3.831547119141" />
                  <Point X="22.115931640625" Y="-3.663894042969" />
                  <Point X="22.658509765625" Y="-2.724119873047" />
                  <Point X="22.6651484375" Y="-2.710085205078" />
                  <Point X="22.67605078125" Y="-2.681120117188" />
                  <Point X="22.680314453125" Y="-2.666189453125" />
                  <Point X="22.6865859375" Y="-2.634663085938" />
                  <Point X="22.688361328125" Y="-2.619239501953" />
                  <Point X="22.689375" Y="-2.588305419922" />
                  <Point X="22.6853359375" Y="-2.557617675781" />
                  <Point X="22.6763515625" Y="-2.527999511719" />
                  <Point X="22.67064453125" Y="-2.51355859375" />
                  <Point X="22.656427734375" Y="-2.484729736328" />
                  <Point X="22.6484453125" Y="-2.471413330078" />
                  <Point X="22.630419921875" Y="-2.4462578125" />
                  <Point X="22.620376953125" Y="-2.434418701172" />
                  <Point X="22.603033203125" Y="-2.417073730469" />
                  <Point X="22.591193359375" Y="-2.407028320312" />
                  <Point X="22.5660390625" Y="-2.389001464844" />
                  <Point X="22.552724609375" Y="-2.381020019531" />
                  <Point X="22.52389453125" Y="-2.366799804688" />
                  <Point X="22.509455078125" Y="-2.361092285156" />
                  <Point X="22.479837890625" Y="-2.35210546875" />
                  <Point X="22.449150390625" Y="-2.348063476562" />
                  <Point X="22.418212890625" Y="-2.349074707031" />
                  <Point X="22.40278515625" Y="-2.350848632812" />
                  <Point X="22.371255859375" Y="-2.357119140625" />
                  <Point X="22.35632421875" Y="-2.361383056641" />
                  <Point X="22.32735546875" Y="-2.372285888672" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.96369140625" Y="-2.580781005859" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="21.081896484375" Y="-2.853464111328" />
                  <Point X="20.99598046875" Y="-2.740587402344" />
                  <Point X="20.818734375" Y="-2.443374023438" />
                  <Point X="21.002541015625" Y="-2.302333984375" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.963515625" Y="-1.563311035156" />
                  <Point X="21.984888671875" Y="-1.540396362305" />
                  <Point X="21.994623046875" Y="-1.528052734375" />
                  <Point X="22.012587890625" Y="-1.50092565918" />
                  <Point X="22.020158203125" Y="-1.487137573242" />
                  <Point X="22.03291796875" Y="-1.458501953125" />
                  <Point X="22.038107421875" Y="-1.443654174805" />
                  <Point X="22.04372265625" Y="-1.421972900391" />
                  <Point X="22.048447265625" Y="-1.398809814453" />
                  <Point X="22.051251953125" Y="-1.368387939453" />
                  <Point X="22.051421875" Y="-1.353062988281" />
                  <Point X="22.04921484375" Y="-1.321389404297" />
                  <Point X="22.046919921875" Y="-1.30623425293" />
                  <Point X="22.039919921875" Y="-1.276484130859" />
                  <Point X="22.02822265625" Y="-1.248242675781" />
                  <Point X="22.01213671875" Y="-1.222255249023" />
                  <Point X="22.00304296875" Y="-1.20991394043" />
                  <Point X="21.98220703125" Y="-1.185955566406" />
                  <Point X="21.971248046875" Y="-1.175238891602" />
                  <Point X="21.947751953125" Y="-1.155709106445" />
                  <Point X="21.93521484375" Y="-1.146895751953" />
                  <Point X="21.9159140625" Y="-1.135535522461" />
                  <Point X="21.89456640625" Y="-1.124481567383" />
                  <Point X="21.86530078125" Y="-1.113256103516" />
                  <Point X="21.850201171875" Y="-1.108858276367" />
                  <Point X="21.81831640625" Y="-1.102377929688" />
                  <Point X="21.802701171875" Y="-1.100532104492" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="21.314302734375" Y="-1.158303833008" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.292841796875" Y="-1.105669555664" />
                  <Point X="20.259240234375" Y="-0.974114257812" />
                  <Point X="20.213548828125" Y="-0.654654541016" />
                  <Point X="20.410275390625" Y="-0.601941711426" />
                  <Point X="21.491712890625" Y="-0.312171325684" />
                  <Point X="21.499525390625" Y="-0.309712524414" />
                  <Point X="21.526064453125" Y="-0.299253326416" />
                  <Point X="21.555830078125" Y="-0.283895965576" />
                  <Point X="21.5664375" Y="-0.277516204834" />
                  <Point X="21.586666015625" Y="-0.263477111816" />
                  <Point X="21.60633203125" Y="-0.248241195679" />
                  <Point X="21.628791015625" Y="-0.226351867676" />
                  <Point X="21.639109375" Y="-0.214475708008" />
                  <Point X="21.658353515625" Y="-0.188217010498" />
                  <Point X="21.6665703125" Y="-0.174803649902" />
                  <Point X="21.68067578125" Y="-0.146805465698" />
                  <Point X="21.686564453125" Y="-0.132220809937" />
                  <Point X="21.693306640625" Y="-0.110496154785" />
                  <Point X="21.699009765625" Y="-0.088497291565" />
                  <Point X="21.703279296875" Y="-0.060339195251" />
                  <Point X="21.704353515625" Y="-0.04609740448" />
                  <Point X="21.704353515625" Y="-0.016462741852" />
                  <Point X="21.703279296875" Y="-0.002220951557" />
                  <Point X="21.699009765625" Y="0.025937143326" />
                  <Point X="21.695814453125" Y="0.039853153229" />
                  <Point X="21.689072265625" Y="0.061577957153" />
                  <Point X="21.68067578125" Y="0.084245323181" />
                  <Point X="21.6665703125" Y="0.112243499756" />
                  <Point X="21.658353515625" Y="0.125656723022" />
                  <Point X="21.639109375" Y="0.151915557861" />
                  <Point X="21.628791015625" Y="0.163791870117" />
                  <Point X="21.60633203125" Y="0.18568119812" />
                  <Point X="21.59419140625" Y="0.195694076538" />
                  <Point X="21.573962890625" Y="0.209733139038" />
                  <Point X="21.555755859375" Y="0.220907348633" />
                  <Point X="21.532662109375" Y="0.233350509644" />
                  <Point X="21.52266015625" Y="0.238011474609" />
                  <Point X="21.502185546875" Y="0.246141738892" />
                  <Point X="21.491712890625" Y="0.249611343384" />
                  <Point X="21.089384765625" Y="0.357414825439" />
                  <Point X="20.2145546875" Y="0.591824890137" />
                  <Point X="20.247015625" Y="0.811192932129" />
                  <Point X="20.26866796875" Y="0.957522338867" />
                  <Point X="20.3664140625" Y="1.318237182617" />
                  <Point X="20.446734375" Y="1.307662841797" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.20470324707" />
                  <Point X="21.28485546875" Y="1.206589477539" />
                  <Point X="21.295109375" Y="1.208053588867" />
                  <Point X="21.3153984375" Y="1.212089233398" />
                  <Point X="21.32543359375" Y="1.214660766602" />
                  <Point X="21.370203125" Y="1.228776733398" />
                  <Point X="21.39655078125" Y="1.237676391602" />
                  <Point X="21.415482421875" Y="1.246006347656" />
                  <Point X="21.42472265625" Y="1.250688232422" />
                  <Point X="21.443466796875" Y="1.261509887695" />
                  <Point X="21.452140625" Y="1.267170166016" />
                  <Point X="21.468822265625" Y="1.279401611328" />
                  <Point X="21.48407421875" Y="1.293376708984" />
                  <Point X="21.497712890625" Y="1.308928833008" />
                  <Point X="21.504107421875" Y="1.317076782227" />
                  <Point X="21.516521484375" Y="1.334806030273" />
                  <Point X="21.5219921875" Y="1.34360534668" />
                  <Point X="21.53194140625" Y="1.3617421875" />
                  <Point X="21.536419921875" Y="1.371079956055" />
                  <Point X="21.5543828125" Y="1.414449462891" />
                  <Point X="21.5645" Y="1.440348266602" />
                  <Point X="21.570287109375" Y="1.46019909668" />
                  <Point X="21.572640625" Y="1.470285888672" />
                  <Point X="21.576400390625" Y="1.491601806641" />
                  <Point X="21.577640625" Y="1.501888671875" />
                  <Point X="21.578994140625" Y="1.522534790039" />
                  <Point X="21.578091796875" Y="1.543205810547" />
                  <Point X="21.574943359375" Y="1.563654907227" />
                  <Point X="21.572810546875" Y="1.573791992188" />
                  <Point X="21.56720703125" Y="1.594699584961" />
                  <Point X="21.563984375" Y="1.604543579102" />
                  <Point X="21.55648828125" Y="1.623816040039" />
                  <Point X="21.55221484375" Y="1.633244750977" />
                  <Point X="21.5305390625" Y="1.674883544922" />
                  <Point X="21.51719921875" Y="1.699287109375" />
                  <Point X="21.50570703125" Y="1.716486572266" />
                  <Point X="21.499490234375" Y="1.72477331543" />
                  <Point X="21.485578125" Y="1.741352783203" />
                  <Point X="21.478498046875" Y="1.748912719727" />
                  <Point X="21.463556640625" Y="1.763215698242" />
                  <Point X="21.4556953125" Y="1.769958496094" />
                  <Point X="21.224919921875" Y="1.947039794922" />
                  <Point X="20.77238671875" Y="2.294280761719" />
                  <Point X="20.9135703125" Y="2.536162841797" />
                  <Point X="20.997716796875" Y="2.680322509766" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.74584375" Y="2.762398925781" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774015625" Y="2.749385742188" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.90728125" Y="2.725702880859" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.023568359375" Y="2.734227783203" />
                  <Point X="22.0430078125" Y="2.741303466797" />
                  <Point X="22.061560546875" Y="2.750453369141" />
                  <Point X="22.070587890625" Y="2.755534667969" />
                  <Point X="22.088841796875" Y="2.767164550781" />
                  <Point X="22.097259765625" Y="2.773197265625" />
                  <Point X="22.11338671875" Y="2.786141113281" />
                  <Point X="22.121095703125" Y="2.793052246094" />
                  <Point X="22.16535546875" Y="2.837310302734" />
                  <Point X="22.1818203125" Y="2.853775634766" />
                  <Point X="22.188734375" Y="2.861487060547" />
                  <Point X="22.20168359375" Y="2.877620361328" />
                  <Point X="22.20771875" Y="2.886042236328" />
                  <Point X="22.21934765625" Y="2.904296142578" />
                  <Point X="22.2244296875" Y="2.913325195312" />
                  <Point X="22.233576171875" Y="2.931874267578" />
                  <Point X="22.240646484375" Y="2.951299072266" />
                  <Point X="22.245564453125" Y="2.971388427734" />
                  <Point X="22.2474765625" Y="2.981572998047" />
                  <Point X="22.250302734375" Y="3.00303125" />
                  <Point X="22.251091796875" Y="3.013364501953" />
                  <Point X="22.25154296875" Y="3.034049072266" />
                  <Point X="22.251205078125" Y="3.044400390625" />
                  <Point X="22.24575" Y="3.106752929688" />
                  <Point X="22.243720703125" Y="3.129949951172" />
                  <Point X="22.242255859375" Y="3.140207275391" />
                  <Point X="22.23821875" Y="3.160499267578" />
                  <Point X="22.235646484375" Y="3.170533935547" />
                  <Point X="22.22913671875" Y="3.191176513672" />
                  <Point X="22.22548828125" Y="3.200870361328" />
                  <Point X="22.217158203125" Y="3.219799072266" />
                  <Point X="22.2124765625" Y="3.229033935547" />
                  <Point X="22.1102109375" Y="3.406160400391" />
                  <Point X="21.94061328125" Y="3.699914550781" />
                  <Point X="22.20509375" Y="3.902688964844" />
                  <Point X="22.351634765625" Y="4.015041503906" />
                  <Point X="22.807474609375" Y="4.268296875" />
                  <Point X="22.881435546875" Y="4.171908691406" />
                  <Point X="22.8881796875" Y="4.164047363281" />
                  <Point X="22.902482421875" Y="4.149106445313" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93491015625" Y="4.121895996094" />
                  <Point X="22.952111328125" Y="4.110403320312" />
                  <Point X="22.9610234375" Y="4.105128417969" />
                  <Point X="23.030421875" Y="4.069001953125" />
                  <Point X="23.056240234375" Y="4.055561523438" />
                  <Point X="23.065673828125" Y="4.051286865234" />
                  <Point X="23.084951171875" Y="4.043790283203" />
                  <Point X="23.094794921875" Y="4.040568359375" />
                  <Point X="23.115701171875" Y="4.034966308594" />
                  <Point X="23.1258359375" Y="4.032834960938" />
                  <Point X="23.146279296875" Y="4.029687988281" />
                  <Point X="23.166947265625" Y="4.028785400391" />
                  <Point X="23.187587890625" Y="4.030138427734" />
                  <Point X="23.197869140625" Y="4.031378417969" />
                  <Point X="23.21918359375" Y="4.03513671875" />
                  <Point X="23.229271484375" Y="4.037488769531" />
                  <Point X="23.249130859375" Y="4.04327734375" />
                  <Point X="23.25890234375" Y="4.046713867188" />
                  <Point X="23.33118359375" Y="4.076654785156" />
                  <Point X="23.358076171875" Y="4.087793457031" />
                  <Point X="23.36741015625" Y="4.092270263672" />
                  <Point X="23.385541015625" Y="4.102216308594" />
                  <Point X="23.394337890625" Y="4.107685058594" />
                  <Point X="23.412068359375" Y="4.120098632812" />
                  <Point X="23.420216796875" Y="4.126493164063" />
                  <Point X="23.43576953125" Y="4.140131347656" />
                  <Point X="23.44974609375" Y="4.1553828125" />
                  <Point X="23.461978515625" Y="4.172064941406" />
                  <Point X="23.467638671875" Y="4.180739257812" />
                  <Point X="23.4784609375" Y="4.199482910156" />
                  <Point X="23.48314453125" Y="4.208724121094" />
                  <Point X="23.4914765625" Y="4.22766015625" />
                  <Point X="23.495125" Y="4.237354980469" />
                  <Point X="23.518650390625" Y="4.311971679688" />
                  <Point X="23.527404296875" Y="4.339731445312" />
                  <Point X="23.529974609375" Y="4.349763183594" />
                  <Point X="23.534009765625" Y="4.370048828125" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401866210938" />
                  <Point X="23.53769921875" Y="4.412218261719" />
                  <Point X="23.537248046875" Y="4.432899414062" />
                  <Point X="23.536458984375" Y="4.443228515625" />
                  <Point X="23.52483203125" Y="4.5315390625" />
                  <Point X="23.520734375" Y="4.562655273438" />
                  <Point X="23.87912890625" Y="4.66313671875" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.626404296875" Y="4.781576660156" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.77433203125" Y="4.261729003906" />
                  <Point X="24.779564453125" Y="4.247106933594" />
                  <Point X="24.79233984375" Y="4.218912597656" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.247104980469" />
                  <Point X="25.23798046875" Y="4.2617265625" />
                  <Point X="25.290380859375" Y="4.457282226563" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.662232421875" Y="4.755259765625" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.2891875" Y="4.626537597656" />
                  <Point X="26.453595703125" Y="4.586844238281" />
                  <Point X="26.7524921875" Y="4.478432617188" />
                  <Point X="26.858259765625" Y="4.440069824219" />
                  <Point X="27.148607421875" Y="4.304284179688" />
                  <Point X="27.250453125" Y="4.256654785156" />
                  <Point X="27.5309921875" Y="4.093211425781" />
                  <Point X="27.62942578125" Y="4.035863037109" />
                  <Point X="27.81778125" Y="3.901916015625" />
                  <Point X="27.6933046875" Y="3.686315917969" />
                  <Point X="27.065310546875" Y="2.59859765625" />
                  <Point X="27.062380859375" Y="2.593116699219" />
                  <Point X="27.0531875" Y="2.573448730469" />
                  <Point X="27.044185546875" Y="2.549569824219" />
                  <Point X="27.041302734375" Y="2.540601074219" />
                  <Point X="27.025654296875" Y="2.482084716797" />
                  <Point X="27.01983203125" Y="2.460314697266" />
                  <Point X="27.0179140625" Y="2.451470458984" />
                  <Point X="27.013646484375" Y="2.420223876953" />
                  <Point X="27.012755859375" Y="2.383240966797" />
                  <Point X="27.013412109375" Y="2.369580810547" />
                  <Point X="27.019513671875" Y="2.318980712891" />
                  <Point X="27.02380078125" Y="2.289034423828" />
                  <Point X="27.02914453125" Y="2.267108398438" />
                  <Point X="27.032470703125" Y="2.256303955078" />
                  <Point X="27.04073828125" Y="2.234213623047" />
                  <Point X="27.0453203125" Y="2.223886962891" />
                  <Point X="27.055681640625" Y="2.20384375" />
                  <Point X="27.0614609375" Y="2.194127197266" />
                  <Point X="27.092771484375" Y="2.147984863281" />
                  <Point X="27.104419921875" Y="2.130818603516" />
                  <Point X="27.109814453125" Y="2.123627197266" />
                  <Point X="27.130462890625" Y="2.100125976562" />
                  <Point X="27.157595703125" Y="2.075389160156" />
                  <Point X="27.1682578125" Y="2.066981201172" />
                  <Point X="27.214400390625" Y="2.035671630859" />
                  <Point X="27.23156640625" Y="2.024023681641" />
                  <Point X="27.24128125" Y="2.018244384766" />
                  <Point X="27.261326171875" Y="2.007882080078" />
                  <Point X="27.27165625" Y="2.003299194336" />
                  <Point X="27.293744140625" Y="1.995033081055" />
                  <Point X="27.304546875" Y="1.991708007812" />
                  <Point X="27.326470703125" Y="1.986365112305" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.38819140625" Y="1.978245239258" />
                  <Point X="27.4062421875" Y="1.976936767578" />
                  <Point X="27.44396875" Y="1.976004394531" />
                  <Point X="27.45752734375" Y="1.976639160156" />
                  <Point X="27.484416015625" Y="1.979834716797" />
                  <Point X="27.49774609375" Y="1.982395629883" />
                  <Point X="27.556263671875" Y="1.998043823242" />
                  <Point X="27.578033203125" Y="2.003865356445" />
                  <Point X="27.583994140625" Y="2.005670776367" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572753906" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.04069921875" Y="2.261505859375" />
                  <Point X="28.940404296875" Y="2.780950439453" />
                  <Point X="28.985556640625" Y="2.718198486328" />
                  <Point X="29.043962890625" Y="2.637028320312" />
                  <Point X="29.13688671875" Y="2.483471435547" />
                  <Point X="28.999767578125" Y="2.378255859375" />
                  <Point X="28.172953125" Y="1.743819824219" />
                  <Point X="28.168142578125" Y="1.739872070313" />
                  <Point X="28.152126953125" Y="1.725224121094" />
                  <Point X="28.134669921875" Y="1.70660546875" />
                  <Point X="28.12857421875" Y="1.699420410156" />
                  <Point X="28.0864609375" Y="1.644479003906" />
                  <Point X="28.07079296875" Y="1.62403918457" />
                  <Point X="28.06563671875" Y="1.616605102539" />
                  <Point X="28.049740234375" Y="1.589375732422" />
                  <Point X="28.034763671875" Y="1.555549804688" />
                  <Point X="28.030140625" Y="1.542675170898" />
                  <Point X="28.014453125" Y="1.486579833984" />
                  <Point X="28.008615234375" Y="1.46571081543" />
                  <Point X="28.006224609375" Y="1.454663696289" />
                  <Point X="28.002771484375" Y="1.432365722656" />
                  <Point X="28.001708984375" Y="1.421115112305" />
                  <Point X="28.000892578125" Y="1.397541503906" />
                  <Point X="28.001173828125" Y="1.386237060547" />
                  <Point X="28.003078125" Y="1.363750244141" />
                  <Point X="28.004701171875" Y="1.352567871094" />
                  <Point X="28.017580078125" Y="1.290154785156" />
                  <Point X="28.02237109375" Y="1.266935058594" />
                  <Point X="28.024599609375" Y="1.258233032227" />
                  <Point X="28.034685546875" Y="1.228606323242" />
                  <Point X="28.050287109375" Y="1.195372070312" />
                  <Point X="28.056919921875" Y="1.183526733398" />
                  <Point X="28.091947265625" Y="1.130287841797" />
                  <Point X="28.1049765625" Y="1.110481201172" />
                  <Point X="28.111736328125" Y="1.101428222656" />
                  <Point X="28.12628515625" Y="1.084187988281" />
                  <Point X="28.13407421875" Y="1.076000244141" />
                  <Point X="28.151318359375" Y="1.059908569336" />
                  <Point X="28.160029296875" Y="1.052700439453" />
                  <Point X="28.178244140625" Y="1.039370605469" />
                  <Point X="28.187748046875" Y="1.033248901367" />
                  <Point X="28.2385078125" Y="1.004676635742" />
                  <Point X="28.257390625" Y="0.994046630859" />
                  <Point X="28.26548046875" Y="0.989988037109" />
                  <Point X="28.29467578125" Y="0.978085327148" />
                  <Point X="28.330275390625" Y="0.96802142334" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.41230078125" Y="0.95618737793" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="28.885009765625" Y="1.003404968262" />
                  <Point X="29.704703125" Y="1.111319824219" />
                  <Point X="29.727603515625" Y="1.017252624512" />
                  <Point X="29.752689453125" Y="0.914205383301" />
                  <Point X="29.783873046875" Y="0.713921264648" />
                  <Point X="29.6395390625" Y="0.675246948242" />
                  <Point X="28.6919921875" Y="0.42135269165" />
                  <Point X="28.68603125" Y="0.419543792725" />
                  <Point X="28.665623046875" Y="0.412136138916" />
                  <Point X="28.642380859375" Y="0.401618530273" />
                  <Point X="28.634005859375" Y="0.397316497803" />
                  <Point X="28.566580078125" Y="0.358343261719" />
                  <Point X="28.54149609375" Y="0.343844055176" />
                  <Point X="28.53388671875" Y="0.338947540283" />
                  <Point X="28.508779296875" Y="0.319873718262" />
                  <Point X="28.48199609375" Y="0.294352172852" />
                  <Point X="28.472796875" Y="0.284226501465" />
                  <Point X="28.432341796875" Y="0.232676803589" />
                  <Point X="28.417291015625" Y="0.213498794556" />
                  <Point X="28.41085546875" Y="0.20421031189" />
                  <Point X="28.399130859375" Y="0.184930511475" />
                  <Point X="28.39384375" Y="0.174939331055" />
                  <Point X="28.3840703125" Y="0.153476699829" />
                  <Point X="28.38000390625" Y="0.14292640686" />
                  <Point X="28.373158203125" Y="0.121424232483" />
                  <Point X="28.370376953125" Y="0.110472213745" />
                  <Point X="28.356892578125" Y="0.040057952881" />
                  <Point X="28.351875" Y="0.013861680984" />
                  <Point X="28.350603515625" Y="0.004967195034" />
                  <Point X="28.3485859375" Y="-0.026260259628" />
                  <Point X="28.35028125" Y="-0.062940093994" />
                  <Point X="28.351875" Y="-0.076421829224" />
                  <Point X="28.365359375" Y="-0.146836090088" />
                  <Point X="28.370376953125" Y="-0.173032211304" />
                  <Point X="28.373158203125" Y="-0.183984237671" />
                  <Point X="28.38000390625" Y="-0.205486404419" />
                  <Point X="28.384068359375" Y="-0.216036392212" />
                  <Point X="28.393841796875" Y="-0.237499328613" />
                  <Point X="28.399130859375" Y="-0.247490646362" />
                  <Point X="28.41085546875" Y="-0.266770446777" />
                  <Point X="28.417291015625" Y="-0.276058807373" />
                  <Point X="28.45774609375" Y="-0.32760848999" />
                  <Point X="28.472796875" Y="-0.346786499023" />
                  <Point X="28.47871875" Y="-0.353634155273" />
                  <Point X="28.50114453125" Y="-0.375809448242" />
                  <Point X="28.5301796875" Y="-0.398726348877" />
                  <Point X="28.54149609375" Y="-0.406404052734" />
                  <Point X="28.608921875" Y="-0.445377288818" />
                  <Point X="28.634005859375" Y="-0.459876647949" />
                  <Point X="28.63949609375" Y="-0.462814758301" />
                  <Point X="28.659154296875" Y="-0.472014831543" />
                  <Point X="28.683025390625" Y="-0.481027008057" />
                  <Point X="28.6919921875" Y="-0.483912841797" />
                  <Point X="29.044509765625" Y="-0.578369384766" />
                  <Point X="29.78487890625" Y="-0.776750854492" />
                  <Point X="29.775619140625" Y="-0.838169921875" />
                  <Point X="29.761619140625" Y="-0.931036315918" />
                  <Point X="29.727802734375" Y="-1.079219848633" />
                  <Point X="29.535669921875" Y="-1.053925048828" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042297363" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.222150390625" Y="-0.94143939209" />
                  <Point X="28.17291796875" Y="-0.952140075684" />
                  <Point X="28.157876953125" Y="-0.9567421875" />
                  <Point X="28.128755859375" Y="-0.968366577148" />
                  <Point X="28.11467578125" Y="-0.975389282227" />
                  <Point X="28.086849609375" Y="-0.992281494141" />
                  <Point X="28.074125" Y="-1.001530395508" />
                  <Point X="28.050375" Y="-1.022001281738" />
                  <Point X="28.039349609375" Y="-1.033223022461" />
                  <Point X="27.95936328125" Y="-1.129422119141" />
                  <Point X="27.92960546875" Y="-1.1652109375" />
                  <Point X="27.921326171875" Y="-1.176847900391" />
                  <Point X="27.90660546875" Y="-1.201230712891" />
                  <Point X="27.9001640625" Y="-1.2139765625" />
                  <Point X="27.8888203125" Y="-1.24136340332" />
                  <Point X="27.884361328125" Y="-1.254934448242" />
                  <Point X="27.87753125" Y="-1.282583007812" />
                  <Point X="27.87516015625" Y="-1.296660522461" />
                  <Point X="27.8636953125" Y="-1.421242553711" />
                  <Point X="27.859431640625" Y="-1.467590942383" />
                  <Point X="27.859291015625" Y="-1.483321289062" />
                  <Point X="27.861609375" Y="-1.514588012695" />
                  <Point X="27.864068359375" Y="-1.530124267578" />
                  <Point X="27.871798828125" Y="-1.561744384766" />
                  <Point X="27.87678515625" Y="-1.576663208008" />
                  <Point X="27.88915625" Y="-1.60547668457" />
                  <Point X="27.896541015625" Y="-1.619371582031" />
                  <Point X="27.969775390625" Y="-1.733283447266" />
                  <Point X="27.997021484375" Y="-1.775662109375" />
                  <Point X="28.001744140625" Y="-1.782353881836" />
                  <Point X="28.019802734375" Y="-1.804458618164" />
                  <Point X="28.04349609375" Y="-1.828124023438" />
                  <Point X="28.052798828125" Y="-1.836277587891" />
                  <Point X="28.379935546875" Y="-2.087299072266" />
                  <Point X="29.087171875" Y="-2.62998046875" />
                  <Point X="29.084962890625" Y="-2.633553710938" />
                  <Point X="29.04546484375" Y="-2.697466064453" />
                  <Point X="29.001275390625" Y="-2.760252441406" />
                  <Point X="28.827162109375" Y="-2.659727783203" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.613611328125" Y="-2.037893920898" />
                  <Point X="27.555017578125" Y="-2.027311889648" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403442383" />
                  <Point X="27.492314453125" Y="-2.02650378418" />
                  <Point X="27.460140625" Y="-2.031461914062" />
                  <Point X="27.444845703125" Y="-2.03513659668" />
                  <Point X="27.4150703125" Y="-2.044960083008" />
                  <Point X="27.40058984375" Y="-2.051108886719" />
                  <Point X="27.269748046875" Y="-2.119969482422" />
                  <Point X="27.221072265625" Y="-2.145587890625" />
                  <Point X="27.208970703125" Y="-2.153169433594" />
                  <Point X="27.1860390625" Y="-2.1700625" />
                  <Point X="27.175208984375" Y="-2.179374023438" />
                  <Point X="27.15425" Y="-2.200333007812" />
                  <Point X="27.144939453125" Y="-2.211161865234" />
                  <Point X="27.128046875" Y="-2.234092285156" />
                  <Point X="27.12046484375" Y="-2.246193847656" />
                  <Point X="27.051603515625" Y="-2.377034912109" />
                  <Point X="27.025984375" Y="-2.425711914062" />
                  <Point X="27.0198359375" Y="-2.440192871094" />
                  <Point X="27.01001171875" Y="-2.469971191406" />
                  <Point X="27.0063359375" Y="-2.485268554688" />
                  <Point X="27.00137890625" Y="-2.517441894531" />
                  <Point X="27.000279296875" Y="-2.533133300781" />
                  <Point X="27.00068359375" Y="-2.564484375" />
                  <Point X="27.0021875" Y="-2.580144042969" />
                  <Point X="27.0306328125" Y="-2.737640625" />
                  <Point X="27.04121484375" Y="-2.796234375" />
                  <Point X="27.04301953125" Y="-2.804230957031" />
                  <Point X="27.05123828125" Y="-2.831539794922" />
                  <Point X="27.064072265625" Y="-2.862478759766" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.27976953125" Y="-3.237687988281" />
                  <Point X="27.735896484375" Y="-4.027722167969" />
                  <Point X="27.723755859375" Y="-4.036082519531" />
                  <Point X="27.58076171875" Y="-3.849728271484" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.828654296875" Y="-2.870144042969" />
                  <Point X="26.808830078125" Y="-2.849624023438" />
                  <Point X="26.783251953125" Y="-2.828003662109" />
                  <Point X="26.77330078125" Y="-2.820646728516" />
                  <Point X="26.61796484375" Y="-2.720781005859" />
                  <Point X="26.56017578125" Y="-2.683627929688" />
                  <Point X="26.546283203125" Y="-2.676245605469" />
                  <Point X="26.51747265625" Y="-2.663874511719" />
                  <Point X="26.5025546875" Y="-2.658885742188" />
                  <Point X="26.47093359375" Y="-2.651154052734" />
                  <Point X="26.45539453125" Y="-2.648695800781" />
                  <Point X="26.424125" Y="-2.646376953125" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.23851171875" Y="-2.662149414062" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670339111328" />
                  <Point X="26.133578125" Y="-2.677170410156" />
                  <Point X="26.12001171875" Y="-2.681627929688" />
                  <Point X="26.092626953125" Y="-2.692970703125" />
                  <Point X="26.07987890625" Y="-2.699412841797" />
                  <Point X="26.05549609375" Y="-2.714134033203" />
                  <Point X="26.043861328125" Y="-2.722413085938" />
                  <Point X="25.912681640625" Y="-2.831485351563" />
                  <Point X="25.863876953125" Y="-2.872063476562" />
                  <Point X="25.85265625" Y="-2.883084960938" />
                  <Point X="25.83218359375" Y="-2.906835205078" />
                  <Point X="25.822931640625" Y="-2.919563964844" />
                  <Point X="25.8060390625" Y="-2.947391113281" />
                  <Point X="25.799017578125" Y="-2.961471679688" />
                  <Point X="25.78739453125" Y="-2.990591552734" />
                  <Point X="25.78279296875" Y="-3.005630859375" />
                  <Point X="25.7435703125" Y="-3.186084472656" />
                  <Point X="25.728978515625" Y="-3.25321875" />
                  <Point X="25.7275859375" Y="-3.261287353516" />
                  <Point X="25.724724609375" Y="-3.289677490234" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520019531" />
                  <Point X="25.78512890625" Y="-3.788032226562" />
                  <Point X="25.833087890625" Y="-4.152317871094" />
                  <Point X="25.655068359375" Y="-3.487936767578" />
                  <Point X="25.652607421875" Y="-3.480119384766" />
                  <Point X="25.642146484375" Y="-3.453581542969" />
                  <Point X="25.6267890625" Y="-3.423816894531" />
                  <Point X="25.62041015625" Y="-3.413209716797" />
                  <Point X="25.501078125" Y="-3.241274414062" />
                  <Point X="25.456681640625" Y="-3.177309326172" />
                  <Point X="25.446669921875" Y="-3.165169921875" />
                  <Point X="25.424783203125" Y="-3.142713378906" />
                  <Point X="25.412908203125" Y="-3.132396240234" />
                  <Point X="25.38665234375" Y="-3.113152832031" />
                  <Point X="25.3732421875" Y="-3.104937255859" />
                  <Point X="25.345244140625" Y="-3.090830078125" />
                  <Point X="25.33065625" Y="-3.084938476562" />
                  <Point X="25.14599609375" Y="-3.027626708984" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.063376953125" Y="-3.003109130859" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.75035546875" Y="-3.063616699219" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.667068359375" Y="-3.090830322266" />
                  <Point X="24.639068359375" Y="-3.104938720703" />
                  <Point X="24.62565625" Y="-3.113155273438" />
                  <Point X="24.599400390625" Y="-3.132399658203" />
                  <Point X="24.58752734375" Y="-3.142716796875" />
                  <Point X="24.565642578125" Y="-3.165172119141" />
                  <Point X="24.555630859375" Y="-3.177310302734" />
                  <Point X="24.436298828125" Y="-3.349245361328" />
                  <Point X="24.391904296875" Y="-3.413210693359" />
                  <Point X="24.38753125" Y="-3.420130859375" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213867188" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.248626953125" Y="-3.893308105469" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.051294006509" Y="3.784772307267" />
                  <Point X="21.955345921476" Y="3.674396661512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.357474384184" Y="2.986624133434" />
                  <Point X="20.806634025615" Y="2.352954787514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.401351109669" Y="4.042662896228" />
                  <Point X="22.005582657924" Y="3.587383372767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.441286658993" Y="2.938235083275" />
                  <Point X="20.837843951264" Y="2.244053656735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.644805710371" Y="4.177921334232" />
                  <Point X="22.055819394373" Y="3.500370084021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.525098933802" Y="2.889846033115" />
                  <Point X="20.913353356501" Y="2.186113247731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.827058255504" Y="4.242774861051" />
                  <Point X="22.106056130821" Y="3.413356795276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.608911208611" Y="2.841456982956" />
                  <Point X="20.988862761739" Y="2.128172838727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.886350853453" Y="4.166179149276" />
                  <Point X="22.156293529559" Y="3.326344268409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.69272348342" Y="2.793067932796" />
                  <Point X="21.064372166976" Y="2.070232429724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.406127705202" Y="1.31300879667" />
                  <Point X="20.346368953078" Y="1.244264216172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.959790737723" Y="4.105858028734" />
                  <Point X="22.20653098801" Y="3.239331810233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.778997481764" Y="2.747510771619" />
                  <Point X="21.139881572213" Y="2.01229202072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.519077540731" Y="1.298138675826" />
                  <Point X="20.289358510739" Y="1.033877161187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.046398547561" Y="4.060684873752" />
                  <Point X="22.242817973036" Y="3.136271168163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.887426382061" Y="2.727439909713" />
                  <Point X="21.215390977451" Y="1.954351611716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.63202736156" Y="1.283268538073" />
                  <Point X="20.252217389485" Y="0.84634714545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.63695638574" Y="4.595239910194" />
                  <Point X="23.532296668574" Y="4.474842678057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.145441685212" Y="4.029816927021" />
                  <Point X="22.249844220897" Y="2.999549898487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.018023805696" Y="2.73287101669" />
                  <Point X="21.290900218746" Y="1.896411014118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.74497718239" Y="1.26839840032" />
                  <Point X="20.226394501348" Y="0.671837267517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.803397249682" Y="4.641904178506" />
                  <Point X="23.519822577058" Y="4.315688834031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.30125761491" Y="4.064258606651" />
                  <Point X="21.366409436364" Y="1.838470389283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.85792700322" Y="1.253528262567" />
                  <Point X="20.269839773438" Y="0.577011292736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.969837916154" Y="4.688568219655" />
                  <Point X="21.441918653982" Y="1.780529764448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.97087682405" Y="1.238658124814" />
                  <Point X="20.371935409305" Y="0.549654843515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.125616328078" Y="4.722966740023" />
                  <Point X="21.508495374016" Y="1.712313476593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.08382664488" Y="1.223787987061" />
                  <Point X="20.474031045172" Y="0.522298394295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.265749317341" Y="4.739367260444" />
                  <Point X="21.556783087395" Y="1.623058093285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.19677646571" Y="1.208917849308" />
                  <Point X="20.576126681039" Y="0.494941945074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.405882306605" Y="4.755767780865" />
                  <Point X="21.577666096325" Y="1.502277203769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.328479900062" Y="1.215621276072" />
                  <Point X="20.678222316906" Y="0.467585495853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.546015295868" Y="4.772168301285" />
                  <Point X="20.780317952773" Y="0.440229046633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.645649645682" Y="4.741980466349" />
                  <Point X="20.88241358864" Y="0.412872597412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.675307795803" Y="4.631294222027" />
                  <Point X="20.984509224507" Y="0.385516148192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.704965945924" Y="4.520607977706" />
                  <Point X="21.086604860374" Y="0.358159698971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.734624096044" Y="4.409921733384" />
                  <Point X="21.188700502842" Y="0.330803257345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.368039983203" Y="-0.613258677502" />
                  <Point X="20.230292264703" Y="-0.771719301031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.764282246165" Y="4.299235489063" />
                  <Point X="21.290796145495" Y="0.303446815931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.532138942856" Y="-0.569288461897" />
                  <Point X="20.24807698251" Y="-0.896064366771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.803643720848" Y="4.199711642762" />
                  <Point X="21.392891788148" Y="0.276090374517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.696237880293" Y="-0.525318271848" />
                  <Point X="20.269883223131" Y="-1.015783199718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.433292749193" Y="4.779235949371" />
                  <Point X="25.351406967863" Y="4.685037133528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.868000370573" Y="4.128941456163" />
                  <Point X="21.494847420451" Y="0.248572869619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.860336817729" Y="-0.481348081799" />
                  <Point X="20.298469735504" Y="-1.127702222248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.54866567862" Y="4.767153279195" />
                  <Point X="25.295318788525" Y="4.475711020762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.960158067521" Y="4.090152715978" />
                  <Point X="21.582069831604" Y="0.204106732573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.024435755166" Y="-0.43737789175" />
                  <Point X="20.327057521056" Y="-1.239619780154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.664038608191" Y="4.755070609185" />
                  <Point X="25.239227852453" Y="4.266381736737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.098679341599" Y="4.104699170168" />
                  <Point X="21.649844038267" Y="0.137267995506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.188534692603" Y="-0.393407701701" />
                  <Point X="20.421437675134" Y="-1.275851875871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.779411546835" Y="4.742987949612" />
                  <Point X="21.694558054662" Y="0.04390154409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.35263363004" Y="-0.349437511652" />
                  <Point X="20.563581389799" Y="-1.257138280472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.888212918537" Y="4.723345567044" />
                  <Point X="21.696318893796" Y="-0.098876885436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.520086224008" Y="-0.301609381081" />
                  <Point X="20.705725104464" Y="-1.238424685074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.992253753069" Y="4.698226812913" />
                  <Point X="20.847868819129" Y="-1.219711089675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.096294587601" Y="4.673108058783" />
                  <Point X="20.990012533794" Y="-1.200997494277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.200335422133" Y="4.647989304652" />
                  <Point X="21.132156248459" Y="-1.182283898878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.304376256025" Y="4.622870549785" />
                  <Point X="21.274299963124" Y="-1.163570303479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.408417086171" Y="4.59775179061" />
                  <Point X="21.416443652098" Y="-1.144856737635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.507740049034" Y="4.567205745961" />
                  <Point X="21.55858733101" Y="-1.126143183366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.603441899006" Y="4.532494087445" />
                  <Point X="21.700731009921" Y="-1.107429629097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.699143748979" Y="4.497782428929" />
                  <Point X="21.829094403861" Y="-1.104568479302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.794845537551" Y="4.46307069978" />
                  <Point X="21.923943245263" Y="-1.140261411928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.888452851439" Y="4.425949553125" />
                  <Point X="21.996155687301" Y="-1.201994543237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.113974386546" Y="-2.216828041067" />
                  <Point X="20.858734535708" Y="-2.510447901735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.977946664434" Y="4.384096364999" />
                  <Point X="22.043617706323" Y="-1.292199779248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.492012923968" Y="-1.926748494142" />
                  <Point X="20.909952561086" Y="-2.596332346697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.067440477429" Y="4.342243176872" />
                  <Point X="22.038222331511" Y="-1.443210491214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.87005146139" Y="-1.636668947216" />
                  <Point X="20.961170586464" Y="-2.682216791659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.156934308065" Y="4.300390009041" />
                  <Point X="21.014805963513" Y="-2.765320391629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.246428310669" Y="4.258537039035" />
                  <Point X="21.073569753124" Y="-2.842524427809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.330253425627" Y="4.210162759777" />
                  <Point X="21.132333828905" Y="-2.919728134788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.413811588006" Y="4.161481386706" />
                  <Point X="21.191097951931" Y="-2.996931787418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.497369750385" Y="4.112800013636" />
                  <Point X="21.391610621231" Y="-2.911072390644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.580927760463" Y="4.064118465363" />
                  <Point X="21.644315039651" Y="-2.765173254566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.662064924228" Y="4.012652051972" />
                  <Point X="21.897019458071" Y="-2.619274118488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.739853495273" Y="3.957333523308" />
                  <Point X="22.149722713545" Y="-2.473376320225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.817642066318" Y="3.902014994644" />
                  <Point X="22.377789289153" Y="-2.355819780141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.569287423861" Y="3.471511616938" />
                  <Point X="22.501247196359" Y="-2.358601747305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.320347721302" Y="3.040335204574" />
                  <Point X="22.587393852255" Y="-2.404305399213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.071408018743" Y="2.60915879221" />
                  <Point X="22.651093606618" Y="-2.475831257482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.013093752295" Y="2.397271859161" />
                  <Point X="22.688036673468" Y="-2.57813716375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.028416685441" Y="2.270094834121" />
                  <Point X="22.602142198352" Y="-2.821751497514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.073207334734" Y="2.176816538769" />
                  <Point X="22.353203170578" Y="-3.252927133628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.13155330313" Y="2.099131854264" />
                  <Point X="22.104263961144" Y="-3.684102978716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.206766954954" Y="2.040851219876" />
                  <Point X="22.068757088237" Y="-3.869753006784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.293042384032" Y="1.995295704571" />
                  <Point X="22.144162814189" Y="-3.927812685163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.403154032416" Y="1.977160622901" />
                  <Point X="22.219568540141" Y="-3.985872363541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.544433839363" Y="1.994880406155" />
                  <Point X="22.29860130425" Y="-4.039759611812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.762420436827" Y="2.100841257838" />
                  <Point X="22.380432443888" Y="-4.090427697282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.015124985818" Y="2.246740544121" />
                  <Point X="22.462263973843" Y="-4.141095333744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.267828976177" Y="2.392639187772" />
                  <Point X="22.82426369538" Y="-3.869466333903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.520532903636" Y="2.538537759064" />
                  <Point X="23.030637501746" Y="-3.776864470218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.773236831095" Y="2.684436330357" />
                  <Point X="23.151777310366" Y="-3.782313104762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.959699990698" Y="2.754133615037" />
                  <Point X="23.270868146697" Y="-3.790118812294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.016706447816" Y="2.674907999076" />
                  <Point X="28.276176597112" Y="1.823025854223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.029035119553" Y="1.538722106325" />
                  <Point X="23.387657225844" Y="-3.800572388572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.070924320028" Y="2.592474483138" />
                  <Point X="28.654212952267" Y="2.113102890736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.00305437961" Y="1.364030640661" />
                  <Point X="23.480806665435" Y="-3.838220259354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.122587073436" Y="2.507101639252" />
                  <Point X="29.032249632905" Y="2.403180301677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.028078619553" Y="1.248013692469" />
                  <Point X="23.55463259297" Y="-3.898097287921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.074573031875" Y="1.15669535228" />
                  <Point X="23.626057800427" Y="-3.96073602902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.132103178281" Y="1.078072171931" />
                  <Point X="23.697482873997" Y="-4.023374924138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.20874232995" Y="1.02143138753" />
                  <Point X="24.593152979102" Y="-3.13782837517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.542952563794" Y="-3.19557734697" />
                  <Point X="23.7654390386" Y="-4.09000434254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.29649206632" Y="0.977571868755" />
                  <Point X="24.795801489316" Y="-3.049511974487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.331651986164" Y="-3.58345489914" />
                  <Point X="23.812502741859" Y="-4.180667788421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.404657203108" Y="0.957197581642" />
                  <Point X="24.964000516728" Y="-3.000825170464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.275562849377" Y="-3.792782113325" />
                  <Point X="23.836840646817" Y="-4.297474274696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.530081640171" Y="0.956677848296" />
                  <Point X="25.083452353837" Y="-3.008215594106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.219473485271" Y="-4.002109589011" />
                  <Point X="23.860280460222" Y="-4.415313897121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.672225313551" Y="0.9753913962" />
                  <Point X="25.182583633589" Y="-3.038982144949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.163383911136" Y="-4.211437306307" />
                  <Point X="23.880266544053" Y="-4.537126580935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.81436898693" Y="0.994104944105" />
                  <Point X="25.28171479182" Y="-3.069748835586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.107294337001" Y="-4.420765023603" />
                  <Point X="23.862578728182" Y="-4.702278128744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.956512686267" Y="1.012818521871" />
                  <Point X="25.375695608923" Y="-3.106440315943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.051204762866" Y="-4.6300927409" />
                  <Point X="23.944230787573" Y="-4.753152222472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.098656411249" Y="1.031532129138" />
                  <Point X="25.448543827925" Y="-3.167442069518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.240800136231" Y="1.050245736405" />
                  <Point X="28.694755364661" Y="0.422093082263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.355263418238" Y="0.031552272592" />
                  <Point X="25.505144956266" Y="-3.247133962899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.382943861213" Y="1.068959343672" />
                  <Point X="28.858854256118" Y="0.466063219417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.358266409129" Y="-0.109797224797" />
                  <Point X="26.12364616202" Y="-2.680433759208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.763401010447" Y="-3.094848400432" />
                  <Point X="25.561028333452" Y="-3.327651534532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.525087586195" Y="1.087672950938" />
                  <Point X="29.022953147574" Y="0.510033356571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.386722878937" Y="-0.221865844186" />
                  <Point X="26.267756111631" Y="-2.659458269246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.725355384619" Y="-3.283418929655" />
                  <Point X="25.616911710638" Y="-3.408169106165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.667231311177" Y="1.106386558205" />
                  <Point X="29.18705203903" Y="0.554003493726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.440159181029" Y="-0.305198453698" />
                  <Point X="26.404577215917" Y="-2.646867636671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.736127771512" Y="-3.41583075934" />
                  <Point X="25.659398610547" Y="-3.504097562026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.724982733589" Y="1.028017926783" />
                  <Point X="29.351150930486" Y="0.59797363088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.503226055999" Y="-0.377452356427" />
                  <Point X="26.51607569705" Y="-2.66340734976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.752684107191" Y="-3.541588917072" />
                  <Point X="25.689056759315" Y="-3.614783807903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.752521820565" Y="0.914893979167" />
                  <Point X="29.515249821943" Y="0.641943768034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.583053787699" Y="-0.430425099096" />
                  <Point X="28.103343461329" Y="-0.982268703171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.898563220346" Y="-1.217841422821" />
                  <Point X="26.601358557739" Y="-2.710104684383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.769240442869" Y="-3.667347074805" />
                  <Point X="25.718714908084" Y="-3.725470053781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.771693882767" Y="0.792144870589" />
                  <Point X="29.679348765515" Y="0.685913965141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.669410631039" Y="-0.475886958008" />
                  <Point X="28.274627318995" Y="-0.930033207881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.86559794864" Y="-1.400567673163" />
                  <Point X="27.214241951896" Y="-2.149867033672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.105056751853" Y="-2.275470238337" />
                  <Point X="26.682107094806" Y="-2.762018161649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.785796778366" Y="-3.793105232747" />
                  <Point X="25.748373056852" Y="-3.836156299658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.77011369384" Y="-0.504845379288" />
                  <Point X="28.419330857493" Y="-0.908374872017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.867313549252" Y="-1.543398143657" />
                  <Point X="27.43784369393" Y="-2.037446697073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.000376808146" Y="-2.540694781684" />
                  <Point X="26.762855695243" Y="-2.813931566016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.802353109534" Y="-3.918863395669" />
                  <Point X="25.77803120562" Y="-3.946842545535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.872209386527" Y="-0.532201763144" />
                  <Point X="28.533387449769" Y="-0.921971814865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.909553103714" Y="-1.639611137906" />
                  <Point X="27.570153712103" Y="-2.030045475445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.017630734021" Y="-2.665650453694" />
                  <Point X="26.834148228299" Y="-2.876722931555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.818909440702" Y="-4.044621558591" />
                  <Point X="25.807689354389" Y="-4.057528791413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.974305079215" Y="-0.559558147" />
                  <Point X="28.646337258375" Y="-0.93684196668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.963069052866" Y="-1.722852123956" />
                  <Point X="27.678949736903" Y="-2.049694008921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.039284323139" Y="-2.785544892106" />
                  <Point X="26.89316536002" Y="-2.953635530975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.076400750734" Y="-0.586914555208" />
                  <Point X="28.759287066982" Y="-0.951712118494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.0189321716" Y="-1.803393000272" />
                  <Point X="27.786751948689" Y="-2.070485793491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.077166866824" Y="-2.886770053904" />
                  <Point X="26.952182491742" Y="-3.030548130394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.178496375653" Y="-0.614271017022" />
                  <Point X="28.872236875588" Y="-0.966582270308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.090844624308" Y="-1.865471229829" />
                  <Point X="27.877185357992" Y="-2.111258099708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.127403864162" Y="-2.973783042529" />
                  <Point X="27.011199623464" Y="-3.107460729814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.280592000572" Y="-0.641627478837" />
                  <Point X="28.985186684195" Y="-0.981452422123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.166353953507" Y="-1.923411726304" />
                  <Point X="27.960997621125" Y="-2.1596471633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.1776408615" Y="-3.060796031155" />
                  <Point X="27.070216755185" Y="-3.184373329234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.382687625492" Y="-0.668983940651" />
                  <Point X="29.098136492801" Y="-0.996322573937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.241863282706" Y="-1.98135222278" />
                  <Point X="28.044809884257" Y="-2.208036226892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.227877858838" Y="-3.147809019781" />
                  <Point X="27.129233886907" Y="-3.261285928653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.484783250411" Y="-0.696340402466" />
                  <Point X="29.211086301408" Y="-1.011192725752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.317372611906" Y="-2.039292719256" />
                  <Point X="28.12862214739" Y="-2.256425290483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.278114856176" Y="-3.234822008406" />
                  <Point X="27.188251018628" Y="-3.338198528073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.58687887533" Y="-0.72369686428" />
                  <Point X="29.324036110015" Y="-1.026062877566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.392881955739" Y="-2.097233198897" />
                  <Point X="28.212434410522" Y="-2.304814354075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.328351871464" Y="-3.321834976384" />
                  <Point X="27.24726815035" Y="-3.415111127493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.688974500249" Y="-0.751053326095" />
                  <Point X="29.436985918621" Y="-1.04093302938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.46839137029" Y="-2.155173597186" />
                  <Point X="28.296246673655" Y="-2.353203417667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.378588887362" Y="-3.408847943658" />
                  <Point X="27.306285282072" Y="-3.492023726913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.783277251712" Y="-0.787374463335" />
                  <Point X="29.549935727891" Y="-1.055803180432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.543900784842" Y="-2.213113995476" />
                  <Point X="28.380058936787" Y="-2.401592481258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.42882590326" Y="-3.495860910933" />
                  <Point X="27.365302413793" Y="-3.568936326332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.75355603473" Y="-0.966368855612" />
                  <Point X="29.66288554175" Y="-1.070673326204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.619410199393" Y="-2.271054393766" />
                  <Point X="28.46387119992" Y="-2.44998154485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.479062919159" Y="-3.582873878207" />
                  <Point X="27.424319545515" Y="-3.645848925752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.694919613944" Y="-2.328994792055" />
                  <Point X="28.547683463052" Y="-2.498370608442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.529299935057" Y="-3.669886845482" />
                  <Point X="27.483336677236" Y="-3.722761525172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.770429028495" Y="-2.386935190345" />
                  <Point X="28.631495726185" Y="-2.546759672033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.579536950956" Y="-3.756899812756" />
                  <Point X="27.542353808958" Y="-3.799674124592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.845938443047" Y="-2.444875588634" />
                  <Point X="28.715307989318" Y="-2.595148735625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.629773966854" Y="-3.843912780031" />
                  <Point X="27.60137089873" Y="-3.876586772268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.921447857598" Y="-2.502815986924" />
                  <Point X="28.79912025245" Y="-2.643537799217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.680010982753" Y="-3.930925747305" />
                  <Point X="27.660387910325" Y="-3.953499509878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.996957272149" Y="-2.560756385213" />
                  <Point X="28.882932470311" Y="-2.691926914887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.730247998651" Y="-4.017938714579" />
                  <Point X="27.719404921919" Y="-4.030412247489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.0724666867" Y="-2.618696783503" />
                  <Point X="28.966744665409" Y="-2.740316056744" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.9998359375" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.763501953125" Y="-4.62673046875" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521543701172" />
                  <Point X="25.34498828125" Y="-3.349608398438" />
                  <Point X="25.300591796875" Y="-3.285643310547" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.08967578125" Y="-3.209088134766" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.80667578125" Y="-3.245078125" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285644287109" />
                  <Point X="24.592388671875" Y="-3.457579345703" />
                  <Point X="24.547994140625" Y="-3.521544677734" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.432154296875" Y="-3.942483886719" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.979556640625" Y="-4.953555175781" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.6856953125" Y="-4.882989746094" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.655705078125" Y="-4.817994140625" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534756835938" />
                  <Point X="23.646203125" Y="-4.312975097656" />
                  <Point X="23.6297890625" Y="-4.230465332031" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.44370703125" Y="-4.053532226562" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.1251171875" Y="-3.970973388672" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.822103515625" Y="-4.099420898438" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.67928515625" Y="-4.236766601563" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.26113671875" Y="-4.240036132813" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.8477578125" Y="-3.93938671875" />
                  <Point X="21.771419921875" Y="-3.880608154297" />
                  <Point X="21.951388671875" Y="-3.568893798828" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597593261719" />
                  <Point X="22.486021484375" Y="-2.568764404297" />
                  <Point X="22.468677734375" Y="-2.551419433594" />
                  <Point X="22.43984765625" Y="-2.53719921875" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="22.05869140625" Y="-2.745325927734" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.930708984375" Y="-2.968540527344" />
                  <Point X="20.838302734375" Y="-2.847135742188" />
                  <Point X="20.625794921875" Y="-2.490796386719" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="20.886876953125" Y="-2.151596679688" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396017700195" />
                  <Point X="21.85979296875" Y="-1.374336425781" />
                  <Point X="21.8618828125" Y="-1.366270263672" />
                  <Point X="21.85967578125" Y="-1.334596435547" />
                  <Point X="21.83883984375" Y="-1.310637939453" />
                  <Point X="21.8195390625" Y="-1.299277709961" />
                  <Point X="21.812357421875" Y="-1.295051513672" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.339103515625" Y="-1.346678344727" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.108751953125" Y="-1.152692138672" />
                  <Point X="20.072607421875" Y="-1.011187805176" />
                  <Point X="20.016384765625" Y="-0.618088134766" />
                  <Point X="20.001603515625" Y="-0.51474230957" />
                  <Point X="20.361099609375" Y="-0.418415771484" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.45810546875" Y="-0.121425201416" />
                  <Point X="21.478333984375" Y="-0.10738608551" />
                  <Point X="21.485859375" Y="-0.10216317749" />
                  <Point X="21.505103515625" Y="-0.075904411316" />
                  <Point X="21.511845703125" Y="-0.054179775238" />
                  <Point X="21.514353515625" Y="-0.046097381592" />
                  <Point X="21.514353515625" Y="-0.016462697983" />
                  <Point X="21.507611328125" Y="0.005262090206" />
                  <Point X="21.505103515625" Y="0.013344331741" />
                  <Point X="21.485859375" Y="0.039603096008" />
                  <Point X="21.465630859375" Y="0.053642211914" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="21.040208984375" Y="0.173888900757" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.0590625" Y="0.839004760742" />
                  <Point X="20.08235546875" Y="0.996414611816" />
                  <Point X="20.195529296875" Y="1.414065917969" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.47153515625" Y="1.496037353516" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.31306640625" Y="1.409982666016" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426055786133" />
                  <Point X="21.360880859375" Y="1.44378503418" />
                  <Point X="21.37884375" Y="1.487154541016" />
                  <Point X="21.38552734375" Y="1.503289306641" />
                  <Point X="21.389287109375" Y="1.524605224609" />
                  <Point X="21.38368359375" Y="1.545512573242" />
                  <Point X="21.3620078125" Y="1.587151367188" />
                  <Point X="21.353943359375" Y="1.602642211914" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.109255859375" Y="1.796302856445" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.7494765625" Y="2.631941894531" />
                  <Point X="20.83998828125" Y="2.787007568359" />
                  <Point X="21.13978125" Y="3.172350585938" />
                  <Point X="21.225330078125" Y="3.282310791016" />
                  <Point X="21.364736328125" Y="3.201823974609" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.92383984375" Y="2.914979736328" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927405273438" />
                  <Point X="22.0310078125" Y="2.971663330078" />
                  <Point X="22.04747265625" Y="2.988128662109" />
                  <Point X="22.0591015625" Y="3.006382568359" />
                  <Point X="22.061927734375" Y="3.027840820312" />
                  <Point X="22.05647265625" Y="3.090193359375" />
                  <Point X="22.054443359375" Y="3.113390380859" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.94566796875" Y="3.311159423828" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="22.08948828125" Y="4.05347265625" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.71929296875" Y="4.436657714844" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.886423828125" Y="4.47751953125" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.11815234375" Y="4.237534179687" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.218491699219" />
                  <Point X="23.18619140625" Y="4.22225" />
                  <Point X="23.25847265625" Y="4.252190917969" />
                  <Point X="23.285365234375" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275743164062" />
                  <Point X="23.31391796875" Y="4.294486816406" />
                  <Point X="23.337443359375" Y="4.369103515625" />
                  <Point X="23.346197265625" Y="4.39686328125" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.33645703125" Y="4.506737304688" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.8278359375" Y="4.84608203125" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.604318359375" Y="4.970288574219" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.817013671875" Y="4.836546386719" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.106853515625" Y="4.506458984375" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.682021484375" Y="4.9442265625" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.33377734375" Y="4.811230957031" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.81727734375" Y="4.657046875" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.229095703125" Y="4.476392578125" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.62663671875" Y="4.257381347656" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="28.00408984375" Y="4.002569824219" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.857849609375" Y="3.591316162109" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491516113281" />
                  <Point X="27.209205078125" Y="2.432999755859" />
                  <Point X="27.2033828125" Y="2.411229736328" />
                  <Point X="27.202044921875" Y="2.392326904297" />
                  <Point X="27.208146484375" Y="2.341726806641" />
                  <Point X="27.210416015625" Y="2.322902099609" />
                  <Point X="27.21868359375" Y="2.300811767578" />
                  <Point X="27.249994140625" Y="2.254669433594" />
                  <Point X="27.261642578125" Y="2.237503173828" />
                  <Point X="27.27494140625" Y="2.224203857422" />
                  <Point X="27.321083984375" Y="2.192894287109" />
                  <Point X="27.33825" Y="2.181246337891" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.4109375" Y="2.166878662109" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.507181640625" Y="2.181594482422" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="27.94569921875" Y="2.42605078125" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.139783203125" Y="2.829169677734" />
                  <Point X="29.202599609375" Y="2.741871582031" />
                  <Point X="29.353986328125" Y="2.491702148438" />
                  <Point X="29.387513671875" Y="2.436296142578" />
                  <Point X="29.115431640625" Y="2.227519042969" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583833374023" />
                  <Point X="28.2372578125" Y="1.528891845703" />
                  <Point X="28.22158984375" Y="1.508452026367" />
                  <Point X="28.21312109375" Y="1.491503540039" />
                  <Point X="28.19743359375" Y="1.435408203125" />
                  <Point X="28.191595703125" Y="1.4145390625" />
                  <Point X="28.190779296875" Y="1.390965332031" />
                  <Point X="28.203658203125" Y="1.328552246094" />
                  <Point X="28.20844921875" Y="1.305332519531" />
                  <Point X="28.215646484375" Y="1.287957519531" />
                  <Point X="28.250673828125" Y="1.23471862793" />
                  <Point X="28.263703125" Y="1.214912109375" />
                  <Point X="28.280947265625" Y="1.19882043457" />
                  <Point X="28.33170703125" Y="1.170248046875" />
                  <Point X="28.35058984375" Y="1.159618164062" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.4371953125" Y="1.144549438477" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.860208984375" Y="1.191779541016" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.912212890625" Y="1.062194213867" />
                  <Point X="29.939193359375" Y="0.951367553711" />
                  <Point X="29.9868984375" Y="0.644963012695" />
                  <Point X="29.997859375" Y="0.57455645752" />
                  <Point X="29.68871484375" Y="0.491721069336" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819198608" />
                  <Point X="28.661662109375" Y="0.193845947266" />
                  <Point X="28.636578125" Y="0.179346679688" />
                  <Point X="28.622265625" Y="0.166926849365" />
                  <Point X="28.581810546875" Y="0.115377227783" />
                  <Point X="28.566759765625" Y="0.096199172974" />
                  <Point X="28.556986328125" Y="0.074736328125" />
                  <Point X="28.543501953125" Y="0.004322164059" />
                  <Point X="28.538484375" Y="-0.021874082565" />
                  <Point X="28.538484375" Y="-0.040685997009" />
                  <Point X="28.55196875" Y="-0.111100158691" />
                  <Point X="28.556986328125" Y="-0.137296401978" />
                  <Point X="28.566759765625" Y="-0.158759246826" />
                  <Point X="28.60721484375" Y="-0.210308868408" />
                  <Point X="28.622265625" Y="-0.229486923218" />
                  <Point X="28.636578125" Y="-0.24190675354" />
                  <Point X="28.70400390625" Y="-0.280880004883" />
                  <Point X="28.729087890625" Y="-0.295379272461" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.093685546875" Y="-0.394843536377" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.963494140625" Y="-0.866495849609" />
                  <Point X="29.948431640625" Y="-0.966414001465" />
                  <Point X="29.887310546875" Y="-1.23424597168" />
                  <Point X="29.874546875" Y="-1.290178588867" />
                  <Point X="29.510869140625" Y="-1.242299560547" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.262505859375" Y="-1.127104248047" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.185447265625" Y="-1.154697021484" />
                  <Point X="28.1054609375" Y="-1.250896118164" />
                  <Point X="28.075703125" Y="-1.286685058594" />
                  <Point X="28.064359375" Y="-1.314071899414" />
                  <Point X="28.05289453125" Y="-1.438653930664" />
                  <Point X="28.048630859375" Y="-1.485002197266" />
                  <Point X="28.056361328125" Y="-1.516622314453" />
                  <Point X="28.129595703125" Y="-1.630534179688" />
                  <Point X="28.156841796875" Y="-1.672912841797" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.495599609375" Y="-1.936562133789" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.246591796875" Y="-2.733436767578" />
                  <Point X="29.2041328125" Y="-2.802141357422" />
                  <Point X="29.077728515625" Y="-2.981744140625" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.732162109375" Y="-2.824272705078" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.579845703125" Y="-2.224869140625" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219245117188" />
                  <Point X="27.358236328125" Y="-2.288105712891" />
                  <Point X="27.309560546875" Y="-2.313724121094" />
                  <Point X="27.2886015625" Y="-2.334683105469" />
                  <Point X="27.219740234375" Y="-2.465524169922" />
                  <Point X="27.19412109375" Y="-2.514201171875" />
                  <Point X="27.1891640625" Y="-2.546374511719" />
                  <Point X="27.217609375" Y="-2.70387109375" />
                  <Point X="27.22819140625" Y="-2.76246484375" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.4443125" Y="-3.142687988281" />
                  <Point X="27.98667578125" Y="-4.082088623047" />
                  <Point X="27.88568359375" Y="-4.154225097656" />
                  <Point X="27.835294921875" Y="-4.190215332031" />
                  <Point X="27.69397265625" Y="-4.28169140625" />
                  <Point X="27.67977734375" Y="-4.290879882812" />
                  <Point X="27.4300234375" Y="-3.965392822266" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467529297" />
                  <Point X="26.51521484375" Y="-2.880601806641" />
                  <Point X="26.45742578125" Y="-2.843448730469" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.255921875" Y="-2.851350097656" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868508789062" />
                  <Point X="26.034154296875" Y="-2.977581054688" />
                  <Point X="25.985349609375" Y="-3.018159179688" />
                  <Point X="25.96845703125" Y="-3.045986328125" />
                  <Point X="25.929234375" Y="-3.226439941406" />
                  <Point X="25.914642578125" Y="-3.29357421875" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.97350390625" Y="-3.763232421875" />
                  <Point X="26.127642578125" Y="-4.934028320312" />
                  <Point X="26.042009765625" Y="-4.952799316406" />
                  <Point X="25.994349609375" Y="-4.96324609375" />
                  <Point X="25.863787109375" Y="-4.98696484375" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#190" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.140845616077" Y="4.88076749562" Z="1.85" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.85" />
                  <Point X="-0.407650953605" Y="5.05122989569" Z="1.85" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.85" />
                  <Point X="-1.191819235171" Y="4.925505566177" Z="1.85" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.85" />
                  <Point X="-1.71927216427" Y="4.531490947242" Z="1.85" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.85" />
                  <Point X="-1.716398526572" Y="4.415420822026" Z="1.85" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.85" />
                  <Point X="-1.766816500346" Y="4.329665242761" Z="1.85" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.85" />
                  <Point X="-1.864917246349" Y="4.313164777058" Z="1.85" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.85" />
                  <Point X="-2.080065939453" Y="4.539237468223" Z="1.85" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.85" />
                  <Point X="-2.31114725499" Y="4.511645167607" Z="1.85" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.85" />
                  <Point X="-2.947091360621" Y="4.12443247928" Z="1.85" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.85" />
                  <Point X="-3.10378877057" Y="3.317439488696" Z="1.85" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.85" />
                  <Point X="-2.999495190897" Y="3.117115900401" Z="1.85" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.85" />
                  <Point X="-3.010505415587" Y="3.038298152299" Z="1.85" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.85" />
                  <Point X="-3.077960565206" Y="2.996069508276" Z="1.85" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.85" />
                  <Point X="-3.616419859601" Y="3.276405178175" Z="1.85" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.85" />
                  <Point X="-3.905838984517" Y="3.234333009581" Z="1.85" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.85" />
                  <Point X="-4.299861881057" Y="2.688427312386" Z="1.85" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.85" />
                  <Point X="-3.927338854868" Y="1.7879151881" Z="1.85" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.85" />
                  <Point X="-3.68849822299" Y="1.595343190706" Z="1.85" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.85" />
                  <Point X="-3.673505683189" Y="1.537569587837" Z="1.85" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.85" />
                  <Point X="-3.708125828762" Y="1.488948523886" Z="1.85" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.85" />
                  <Point X="-4.528097045836" Y="1.576889710165" Z="1.85" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.85" />
                  <Point X="-4.858886649521" Y="1.458423276055" Z="1.85" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.85" />
                  <Point X="-4.996555697973" Y="0.877603758203" Z="1.85" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.85" />
                  <Point X="-3.978889213866" Y="0.156872624547" Z="1.85" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.85" />
                  <Point X="-3.569035445265" Y="0.043846050766" Z="1.85" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.85" />
                  <Point X="-3.546299386176" Y="0.021724690992" Z="1.85" />
                  <Point X="-3.539556741714" Y="0" Z="1.85" />
                  <Point X="-3.542065214959" Y="-0.008082260069" Z="1.85" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.85" />
                  <Point X="-3.556333149858" Y="-0.035029932368" Z="1.85" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.85" />
                  <Point X="-4.657998574907" Y="-0.338839432209" Z="1.85" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.85" />
                  <Point X="-5.039267824775" Y="-0.593886927682" Z="1.85" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.85" />
                  <Point X="-4.945850354392" Y="-1.133785952976" Z="1.85" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.85" />
                  <Point X="-3.660527541549" Y="-1.364970582421" Z="1.85" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.85" />
                  <Point X="-3.211978042183" Y="-1.311089641238" Z="1.85" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.85" />
                  <Point X="-3.19476540718" Y="-1.330515906967" Z="1.85" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.85" />
                  <Point X="-4.14971775615" Y="-2.080649004995" Z="1.85" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.85" />
                  <Point X="-4.423304640741" Y="-2.485125996116" Z="1.85" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.85" />
                  <Point X="-4.115233149693" Y="-2.967543663381" Z="1.85" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.85" />
                  <Point X="-2.922464696519" Y="-2.757347169988" Z="1.85" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.85" />
                  <Point X="-2.568135383896" Y="-2.560195085235" Z="1.85" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.85" />
                  <Point X="-3.09806984318" Y="-3.512613685973" Z="1.85" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.85" />
                  <Point X="-3.188902084744" Y="-3.94772363362" Z="1.85" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.85" />
                  <Point X="-2.771342103613" Y="-4.251266603205" Z="1.85" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.85" />
                  <Point X="-2.28720347977" Y="-4.235924402299" Z="1.85" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.85" />
                  <Point X="-2.156273808638" Y="-4.109714038081" Z="1.85" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.85" />
                  <Point X="-1.884309805758" Y="-3.98958649778" Z="1.85" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.85" />
                  <Point X="-1.595417340278" Y="-4.059844340728" Z="1.85" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.85" />
                  <Point X="-1.408993748942" Y="-4.291450282129" Z="1.85" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.85" />
                  <Point X="-1.400023887479" Y="-4.780187569521" Z="1.85" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.85" />
                  <Point X="-1.332919706917" Y="-4.900132698248" Z="1.85" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.85" />
                  <Point X="-1.036127971058" Y="-4.971359116382" Z="1.85" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.85" />
                  <Point X="-0.525705814637" Y="-3.924144863561" Z="1.85" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.85" />
                  <Point X="-0.372691435677" Y="-3.45480785161" Z="1.85" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.85" />
                  <Point X="-0.184659870035" Y="-3.261550977512" Z="1.85" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.85" />
                  <Point X="0.068699209326" Y="-3.225561067776" Z="1.85" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.85" />
                  <Point X="0.297754423755" Y="-3.346837904158" Z="1.85" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.85" />
                  <Point X="0.709049045764" Y="-4.608391166255" Z="1.85" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.85" />
                  <Point X="0.866568471871" Y="-5.004879534438" Z="1.85" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.85" />
                  <Point X="1.046558250586" Y="-4.970359663257" Z="1.85" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.85" />
                  <Point X="1.016920164634" Y="-3.725425989298" Z="1.85" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.85" />
                  <Point X="0.971937702904" Y="-3.205779420157" Z="1.85" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.85" />
                  <Point X="1.059963241597" Y="-2.984747731877" Z="1.85" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.85" />
                  <Point X="1.25434603461" Y="-2.869859508221" Z="1.85" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.85" />
                  <Point X="1.48201963014" Y="-2.891379688441" Z="1.85" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.85" />
                  <Point X="2.3841976057" Y="-3.964550741783" Z="1.85" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.85" />
                  <Point X="2.714983136558" Y="-4.292385865396" Z="1.85" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.85" />
                  <Point X="2.908586408766" Y="-4.163632444467" Z="1.85" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.85" />
                  <Point X="2.481455643094" Y="-3.086408019527" Z="1.85" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.85" />
                  <Point X="2.260654917455" Y="-2.663704935811" Z="1.85" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.85" />
                  <Point X="2.257829450342" Y="-2.45753124579" Z="1.85" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.85" />
                  <Point X="2.375367132582" Y="-2.301071859625" Z="1.85" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.85" />
                  <Point X="2.564801867702" Y="-2.242793092036" Z="1.85" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.85" />
                  <Point X="3.70100557758" Y="-2.836293857181" Z="1.85" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.85" />
                  <Point X="4.112460354889" Y="-2.979241340339" Z="1.85" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.85" />
                  <Point X="4.282967523818" Y="-2.728442348251" Z="1.85" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.85" />
                  <Point X="3.51988026652" Y="-1.865614450636" Z="1.85" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.85" />
                  <Point X="3.165497231376" Y="-1.572214299423" Z="1.85" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.85" />
                  <Point X="3.096527466974" Y="-1.411954152346" Z="1.85" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.85" />
                  <Point X="3.137748949754" Y="-1.251583205888" Z="1.85" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.85" />
                  <Point X="3.266967338664" Y="-1.144683445421" Z="1.85" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.85" />
                  <Point X="4.498186694127" Y="-1.260591609819" Z="1.85" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.85" />
                  <Point X="4.929900680238" Y="-1.214089418129" Z="1.85" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.85" />
                  <Point X="5.006779368694" Y="-0.842669304092" Z="1.85" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.85" />
                  <Point X="4.100469216597" Y="-0.315267148572" Z="1.85" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.85" />
                  <Point X="3.722868290457" Y="-0.206311398731" Z="1.85" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.85" />
                  <Point X="3.640392069169" Y="-0.148160148139" Z="1.85" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.85" />
                  <Point X="3.594919841868" Y="-0.07041424442" Z="1.85" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.85" />
                  <Point X="3.586451608556" Y="0.026196286773" Z="1.85" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.85" />
                  <Point X="3.614987369232" Y="0.115788590436" Z="1.85" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.85" />
                  <Point X="3.680527123895" Y="0.181837445511" Z="1.85" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.85" />
                  <Point X="4.695498600709" Y="0.474704784197" Z="1.85" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.85" />
                  <Point X="5.030145494343" Y="0.683934969314" Z="1.85" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.85" />
                  <Point X="4.954637596858" Y="1.10530080206" Z="1.85" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.85" />
                  <Point X="3.84752602934" Y="1.272631926384" Z="1.85" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.85" />
                  <Point X="3.437589381952" Y="1.225398452228" Z="1.85" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.85" />
                  <Point X="3.350076926516" Y="1.24509846255" Z="1.85" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.85" />
                  <Point X="3.28628757591" Y="1.2934775419" Z="1.85" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.85" />
                  <Point X="3.246469994276" Y="1.369936040454" Z="1.85" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.85" />
                  <Point X="3.239428329133" Y="1.453218400399" Z="1.85" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.85" />
                  <Point X="3.27078382623" Y="1.52975363498" Z="1.85" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.85" />
                  <Point X="4.139711591082" Y="2.219131339389" Z="1.85" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.85" />
                  <Point X="4.390606108593" Y="2.548868214391" Z="1.85" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.85" />
                  <Point X="4.174212670996" Y="2.889652995241" Z="1.85" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.85" />
                  <Point X="2.914542907773" Y="2.500632194061" Z="1.85" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.85" />
                  <Point X="2.488108015953" Y="2.261177211859" Z="1.85" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.85" />
                  <Point X="2.410766857639" Y="2.247799096782" Z="1.85" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.85" />
                  <Point X="2.343000406177" Y="2.26554861784" Z="1.85" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.85" />
                  <Point X="2.285209982395" Y="2.314024454205" Z="1.85" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.85" />
                  <Point X="2.251630605977" Y="2.378991586193" Z="1.85" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.85" />
                  <Point X="2.251350730798" Y="2.451361559929" Z="1.85" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.85" />
                  <Point X="2.894993158799" Y="3.597596174182" Z="1.85" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.85" />
                  <Point X="3.026909100427" Y="4.074597017319" Z="1.85" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.85" />
                  <Point X="2.645650556547" Y="4.33186407796" Z="1.85" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.85" />
                  <Point X="2.244120274498" Y="4.552964840587" Z="1.85" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.85" />
                  <Point X="1.828169053761" Y="4.735330516528" Z="1.85" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.85" />
                  <Point X="1.339354656043" Y="4.891114719056" Z="1.85" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.85" />
                  <Point X="0.681071780049" Y="5.025234493856" Z="1.85" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.85" />
                  <Point X="0.052398945476" Y="4.550680010331" Z="1.85" />
                  <Point X="0" Y="4.355124473572" Z="1.85" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>