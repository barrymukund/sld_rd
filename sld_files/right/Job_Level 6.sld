<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#127" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="605" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.602931640625" Y="-3.660416748047" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140869141" />
                  <Point X="25.54236328125" Y="-3.467376220703" />
                  <Point X="25.526166015625" Y="-3.444039550781" />
                  <Point X="25.378634765625" Y="-3.231475830078" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669677734" />
                  <Point X="25.27743359375" Y="-3.167890869141" />
                  <Point X="25.04913671875" Y="-3.097036376953" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097036376953" />
                  <Point X="24.93811328125" Y="-3.104815185547" />
                  <Point X="24.70981640625" Y="-3.175669677734" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.633673828125" Y="-3.231479980469" />
                  <Point X="24.617478515625" Y="-3.25481640625" />
                  <Point X="24.469947265625" Y="-3.467380371094" />
                  <Point X="24.4618125" Y="-3.481571044922" />
                  <Point X="24.449009765625" Y="-3.512523925781" />
                  <Point X="24.088056640625" Y="-4.859621582031" />
                  <Point X="24.083416015625" Y="-4.876941894531" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.895634765625" Y="-4.838911132812" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.780267578125" Y="-4.599673339844" />
                  <Point X="23.7850390625" Y="-4.563439941406" />
                  <Point X="23.78580078125" Y="-4.547930664062" />
                  <Point X="23.7834921875" Y="-4.516222167969" />
                  <Point X="23.77750390625" Y="-4.486120117188" />
                  <Point X="23.722962890625" Y="-4.211930664062" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.676357421875" Y="-4.131206054688" />
                  <Point X="23.653283203125" Y="-4.110969238281" />
                  <Point X="23.44309765625" Y="-3.926641113281" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.326345703125" Y="-3.888958984375" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801513672" />
                  <Point X="22.931822265625" Y="-3.911853027344" />
                  <Point X="22.699376953125" Y="-4.067169433594" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.198291015625" Y="-4.08938671875" />
                  <Point X="22.16363671875" Y="-4.062704589844" />
                  <Point X="21.895279296875" Y="-3.856077148438" />
                  <Point X="22.502595703125" Y="-2.804173339844" />
                  <Point X="22.57623828125" Y="-2.676619384766" />
                  <Point X="22.587142578125" Y="-2.647646972656" />
                  <Point X="22.593412109375" Y="-2.6161171875" />
                  <Point X="22.59427734375" Y="-2.584121582031" />
                  <Point X="22.584466796875" Y="-2.553655029297" />
                  <Point X="22.568537109375" Y="-2.523114990234" />
                  <Point X="22.551484375" Y="-2.499877685547" />
                  <Point X="22.535853515625" Y="-2.484245361328" />
                  <Point X="22.5106953125" Y="-2.466215332031" />
                  <Point X="22.481865234375" Y="-2.451997070313" />
                  <Point X="22.452248046875" Y="-2.443011962891" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.19897265625" Y="-3.131989013672" />
                  <Point X="21.1819765625" Y="-3.141801513672" />
                  <Point X="20.917142578125" Y="-2.793862304688" />
                  <Point X="20.892298828125" Y="-2.752204833984" />
                  <Point X="20.693857421875" Y="-2.419449951172" />
                  <Point X="21.765193359375" Y="-1.597385009766" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.91685546875" Y="-1.473361938477" />
                  <Point X="21.935583984375" Y="-1.443286254883" />
                  <Point X="21.946908203125" Y="-1.416885009766" />
                  <Point X="21.953849609375" Y="-1.390080444336" />
                  <Point X="21.95665234375" Y="-1.359659545898" />
                  <Point X="21.9544453125" Y="-1.3279921875" />
                  <Point X="21.9469296875" Y="-1.297026123047" />
                  <Point X="21.92959765625" Y="-1.270286987305" />
                  <Point X="21.90614453125" Y="-1.244787109375" />
                  <Point X="21.884408203125" Y="-1.227224609375" />
                  <Point X="21.860544921875" Y="-1.213179931641" />
                  <Point X="21.831283203125" Y="-1.201956176758" />
                  <Point X="21.799396484375" Y="-1.195474731445" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="20.301353515625" Y="-1.387481079102" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.165921875" Y="-0.992647338867" />
                  <Point X="20.1593515625" Y="-0.94670300293" />
                  <Point X="20.107578125" Y="-0.584698364258" />
                  <Point X="21.32034375" Y="-0.259738372803" />
                  <Point X="21.467125" Y="-0.220408279419" />
                  <Point X="21.484908203125" Y="-0.213672729492" />
                  <Point X="21.503220703125" Y="-0.204547088623" />
                  <Point X="21.515015625" Y="-0.197564437866" />
                  <Point X="21.5400234375" Y="-0.180207809448" />
                  <Point X="21.564046875" Y="-0.156119598389" />
                  <Point X="21.5842109375" Y="-0.12689919281" />
                  <Point X="21.596751953125" Y="-0.101100296021" />
                  <Point X="21.60508203125" Y="-0.074258598328" />
                  <Point X="21.609314453125" Y="-0.04343756485" />
                  <Point X="21.608400390625" Y="-0.010846960068" />
                  <Point X="21.60416796875" Y="0.014649418831" />
                  <Point X="21.59583203125" Y="0.041507659912" />
                  <Point X="21.579974609375" Y="0.071819892883" />
                  <Point X="21.55798046875" Y="0.099983375549" />
                  <Point X="21.5372734375" Y="0.119556137085" />
                  <Point X="21.51226953125" Y="0.136909866333" />
                  <Point X="21.498076171875" Y="0.145046295166" />
                  <Point X="21.467125" Y="0.157848144531" />
                  <Point X="20.130140625" Y="0.516092224121" />
                  <Point X="20.108185546875" Y="0.521975158691" />
                  <Point X="20.175513671875" Y="0.976974609375" />
                  <Point X="20.188744140625" Y="1.025804199219" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="21.129119140625" Y="1.313644897461" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255017578125" Y="1.299341796875" />
                  <Point X="21.276578125" Y="1.301228271484" />
                  <Point X="21.296865234375" Y="1.305263916016" />
                  <Point X="21.30294140625" Y="1.30717980957" />
                  <Point X="21.358291015625" Y="1.324631347656" />
                  <Point X="21.377224609375" Y="1.332963256836" />
                  <Point X="21.395970703125" Y="1.343787109375" />
                  <Point X="21.412654296875" Y="1.356020996094" />
                  <Point X="21.42629296875" Y="1.371576293945" />
                  <Point X="21.43870703125" Y="1.389309326172" />
                  <Point X="21.448654296875" Y="1.407444213867" />
                  <Point X="21.45108984375" Y="1.413325561523" />
                  <Point X="21.473298828125" Y="1.466943481445" />
                  <Point X="21.47908203125" Y="1.486786376953" />
                  <Point X="21.482841796875" Y="1.508099243164" />
                  <Point X="21.484197265625" Y="1.528739501953" />
                  <Point X="21.481052734375" Y="1.54918371582" />
                  <Point X="21.475453125" Y="1.570088378906" />
                  <Point X="21.46794921875" Y="1.589383911133" />
                  <Point X="21.46500390625" Y="1.595039916992" />
                  <Point X="21.438208984375" Y="1.646509643555" />
                  <Point X="21.426716796875" Y="1.663707275391" />
                  <Point X="21.4128046875" Y="1.680286865234" />
                  <Point X="21.39786328125" Y="1.694590454102" />
                  <Point X="20.648140625" Y="2.269874023438" />
                  <Point X="20.91884765625" Y="2.733657226562" />
                  <Point X="20.95389453125" Y="2.778707519531" />
                  <Point X="21.24949609375" Y="3.158661621094" />
                  <Point X="21.72872265625" Y="2.881979980469" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.853205078125" Y="2.825796386719" />
                  <Point X="21.86166796875" Y="2.825055908203" />
                  <Point X="21.93875390625" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.019537109375" Y="2.835653564453" />
                  <Point X="22.037791015625" Y="2.847282714844" />
                  <Point X="22.053919921875" Y="2.860225341797" />
                  <Point X="22.059927734375" Y="2.866232421875" />
                  <Point X="22.11464453125" Y="2.920948730469" />
                  <Point X="22.127595703125" Y="2.937085693359" />
                  <Point X="22.139224609375" Y="2.955340087891" />
                  <Point X="22.14837109375" Y="2.973888916016" />
                  <Point X="22.1532890625" Y="2.993977783203" />
                  <Point X="22.156115234375" Y="3.015436523438" />
                  <Point X="22.15656640625" Y="3.036118652344" />
                  <Point X="22.155826171875" Y="3.044581787109" />
                  <Point X="22.14908203125" Y="3.121668212891" />
                  <Point X="22.145044921875" Y="3.141963134766" />
                  <Point X="22.13853515625" Y="3.162605224609" />
                  <Point X="22.130205078125" Y="3.181532714844" />
                  <Point X="21.816666015625" Y="3.724595947266" />
                  <Point X="22.299380859375" Y="4.094687255859" />
                  <Point X="22.354568359375" Y="4.125348144531" />
                  <Point X="22.832962890625" Y="4.391133789062" />
                  <Point X="22.937021484375" Y="4.255521484375" />
                  <Point X="22.9568046875" Y="4.229740234375" />
                  <Point X="22.971109375" Y="4.214797851563" />
                  <Point X="22.987689453125" Y="4.200885742188" />
                  <Point X="23.004890625" Y="4.189393066406" />
                  <Point X="23.014310546875" Y="4.184489746094" />
                  <Point X="23.100107421875" Y="4.139826171875" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.22255078125" Y="4.134482910156" />
                  <Point X="23.232361328125" Y="4.138546875" />
                  <Point X="23.321724609375" Y="4.1755625" />
                  <Point X="23.33985546875" Y="4.185509765625" />
                  <Point X="23.3575859375" Y="4.197924316406" />
                  <Point X="23.373140625" Y="4.211565429688" />
                  <Point X="23.385373046875" Y="4.22825" />
                  <Point X="23.396193359375" Y="4.246994140625" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.40771484375" Y="4.276049316406" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410143066406" />
                  <Point X="23.442271484375" Y="4.430824707031" />
                  <Point X="23.415798828125" Y="4.6318984375" />
                  <Point X="24.05036328125" Y="4.809808105469" />
                  <Point X="24.11726953125" Y="4.817638671875" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.84698046875" Y="4.35766015625" />
                  <Point X="24.866095703125" Y="4.28631640625" />
                  <Point X="24.87887109375" Y="4.258123046875" />
                  <Point X="24.89673046875" Y="4.231395996094" />
                  <Point X="24.91788671875" Y="4.20880859375" />
                  <Point X="24.9451796875" Y="4.194219726563" />
                  <Point X="24.9756171875" Y="4.18388671875" />
                  <Point X="25.006154296875" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183885742188" />
                  <Point X="25.067130859375" Y="4.194217773438" />
                  <Point X="25.094423828125" Y="4.208806640625" />
                  <Point X="25.11558203125" Y="4.23139453125" />
                  <Point X="25.13344140625" Y="4.25812109375" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.844041015625" Y="4.831738769531" />
                  <Point X="25.899400390625" Y="4.818373535156" />
                  <Point X="26.4810234375" Y="4.677951660156" />
                  <Point X="26.51572265625" Y="4.665366210938" />
                  <Point X="26.894642578125" Y="4.527929199219" />
                  <Point X="26.929490234375" Y="4.511631835938" />
                  <Point X="27.294576171875" Y="4.340893066406" />
                  <Point X="27.3282734375" Y="4.32126171875" />
                  <Point X="27.68098828125" Y="4.11576953125" />
                  <Point X="27.71273046875" Y="4.093195556641" />
                  <Point X="27.943263671875" Y="3.929254394531" />
                  <Point X="27.2328203125" Y="2.698732177734" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.14035546875" Y="2.535450439453" />
                  <Point X="27.133228515625" Y="2.515424804688" />
                  <Point X="27.130953125" Y="2.508114013672" />
                  <Point X="27.111607421875" Y="2.435770019531" />
                  <Point X="27.10840234375" Y="2.413107177734" />
                  <Point X="27.107892578125" Y="2.387337158203" />
                  <Point X="27.108556640625" Y="2.374085693359" />
                  <Point X="27.116099609375" Y="2.311528564453" />
                  <Point X="27.12144140625" Y="2.289607177734" />
                  <Point X="27.12970703125" Y="2.267518066406" />
                  <Point X="27.14007421875" Y="2.247466796875" />
                  <Point X="27.14432421875" Y="2.241204101562" />
                  <Point X="27.183033203125" Y="2.184158203125" />
                  <Point X="27.198201171875" Y="2.1667890625" />
                  <Point X="27.217763671875" Y="2.149239257812" />
                  <Point X="27.22786328125" Y="2.141341796875" />
                  <Point X="27.28491015625" Y="2.102634033203" />
                  <Point X="27.304955078125" Y="2.092271728516" />
                  <Point X="27.32704296875" Y="2.084006103516" />
                  <Point X="27.3489609375" Y="2.078663818359" />
                  <Point X="27.355828125" Y="2.077835449219" />
                  <Point X="27.418384765625" Y="2.070292236328" />
                  <Point X="27.44191015625" Y="2.070388671875" />
                  <Point X="27.468755859375" Y="2.073850341797" />
                  <Point X="27.4811484375" Y="2.076295166016" />
                  <Point X="27.5534921875" Y="2.095640869141" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.933283203125" Y="2.886536132812" />
                  <Point X="28.967328125" Y="2.90619140625" />
                  <Point X="29.12327734375" Y="2.689458740234" />
                  <Point X="29.140974609375" Y="2.660211914062" />
                  <Point X="29.262201171875" Y="2.459884277344" />
                  <Point X="28.3430078125" Y="1.754562133789" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.217845703125" Y="1.656458862305" />
                  <Point X="28.202884765625" Y="1.639752319336" />
                  <Point X="28.198259765625" Y="1.634171142578" />
                  <Point X="28.146193359375" Y="1.566247070312" />
                  <Point X="28.134361328125" Y="1.546084106445" />
                  <Point X="28.123763671875" Y="1.52151940918" />
                  <Point X="28.11950390625" Y="1.509475708008" />
                  <Point X="28.100107421875" Y="1.440125" />
                  <Point X="28.09665234375" Y="1.417825195312" />
                  <Point X="28.0958359375" Y="1.394253173828" />
                  <Point X="28.097740234375" Y="1.371766235352" />
                  <Point X="28.09948828125" Y="1.363294799805" />
                  <Point X="28.11541015625" Y="1.286133300781" />
                  <Point X="28.122998046875" Y="1.263822753906" />
                  <Point X="28.13494921875" Y="1.239219848633" />
                  <Point X="28.14103515625" Y="1.228515136719" />
                  <Point X="28.184337890625" Y="1.162695800781" />
                  <Point X="28.198892578125" Y="1.145449829102" />
                  <Point X="28.21613671875" Y="1.129360107422" />
                  <Point X="28.234353515625" Y="1.116031005859" />
                  <Point X="28.241244140625" Y="1.112152954102" />
                  <Point X="28.30399609375" Y="1.076828613281" />
                  <Point X="28.326263671875" Y="1.067784912109" />
                  <Point X="28.3535546875" Y="1.060555664062" />
                  <Point X="28.36543359375" Y="1.058207519531" />
                  <Point X="28.45028125" Y="1.046993896484" />
                  <Point X="28.462705078125" Y="1.046174926758" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.765625" Y="1.21516027832" />
                  <Point X="29.77683984375" Y="1.21663671875" />
                  <Point X="29.845943359375" Y="0.932782775879" />
                  <Point X="29.851515625" Y="0.89698248291" />
                  <Point X="29.890865234375" Y="0.644238891602" />
                  <Point X="28.845189453125" Y="0.364050476074" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.6997109375" Y="0.323303588867" />
                  <Point X="28.67848046875" Y="0.313006256104" />
                  <Point X="28.672396484375" Y="0.309778594971" />
                  <Point X="28.589037109375" Y="0.261595947266" />
                  <Point X="28.57006640625" Y="0.247179794312" />
                  <Point X="28.550263671875" Y="0.227762924194" />
                  <Point X="28.542041015625" Y="0.218580673218" />
                  <Point X="28.492025390625" Y="0.15484967041" />
                  <Point X="28.48030078125" Y="0.135567443848" />
                  <Point X="28.47052734375" Y="0.114103370667" />
                  <Point X="28.463681640625" Y="0.09260181427" />
                  <Point X="28.4618515625" Y="0.083044784546" />
                  <Point X="28.4451796875" Y="-0.004008500576" />
                  <Point X="28.443681640625" Y="-0.027991376877" />
                  <Point X="28.44551171875" Y="-0.056358264923" />
                  <Point X="28.447009765625" Y="-0.068111083984" />
                  <Point X="28.463681640625" Y="-0.155164382935" />
                  <Point X="28.47052734375" Y="-0.176663650513" />
                  <Point X="28.48030078125" Y="-0.198127578735" />
                  <Point X="28.492021484375" Y="-0.217404647827" />
                  <Point X="28.49751171875" Y="-0.224401428223" />
                  <Point X="28.54752734375" Y="-0.288132568359" />
                  <Point X="28.565037109375" Y="-0.305314483643" />
                  <Point X="28.588501953125" Y="-0.323024169922" />
                  <Point X="28.598189453125" Y="-0.329445068359" />
                  <Point X="28.681546875" Y="-0.377627716064" />
                  <Point X="28.692708984375" Y="-0.3831378479" />
                  <Point X="28.716580078125" Y="-0.392149963379" />
                  <Point X="29.888033203125" Y="-0.706039978027" />
                  <Point X="29.891474609375" Y="-0.706961975098" />
                  <Point X="29.8550234375" Y="-0.948736694336" />
                  <Point X="29.84787890625" Y="-0.980041015625" />
                  <Point X="29.801177734375" Y="-1.18469921875" />
                  <Point X="28.573533203125" Y="-1.023076904297" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.399595703125" Y="-1.003439331055" />
                  <Point X="28.364490234375" Y="-1.008056213379" />
                  <Point X="28.35669921875" Y="-1.009412597656" />
                  <Point X="28.193095703125" Y="-1.044972412109" />
                  <Point X="28.162724609375" Y="-1.055693969727" />
                  <Point X="28.130103515625" Y="-1.077422973633" />
                  <Point X="28.106744140625" Y="-1.101254150391" />
                  <Point X="28.1015390625" Y="-1.107017578125" />
                  <Point X="28.00265234375" Y="-1.225948608398" />
                  <Point X="27.986623046875" Y="-1.250425292969" />
                  <Point X="27.974203125" Y="-1.284165161133" />
                  <Point X="27.96905078125" Y="-1.315626953125" />
                  <Point X="27.968203125" Y="-1.322275756836" />
                  <Point X="27.95403125" Y="-1.47629675293" />
                  <Point X="27.955111328125" Y="-1.508485717773" />
                  <Point X="27.96616015625" Y="-1.546411254883" />
                  <Point X="27.982609375" Y="-1.577035766602" />
                  <Point X="27.986390625" Y="-1.583458007812" />
                  <Point X="28.076931640625" Y="-1.724287841797" />
                  <Point X="28.0869375" Y="-1.737242797852" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="29.19774609375" Y="-2.595083007812" />
                  <Point X="29.213123046875" Y="-2.606881835938" />
                  <Point X="29.124798828125" Y="-2.749802001953" />
                  <Point X="29.110029296875" Y="-2.770788574219" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="27.93379296875" Y="-2.253637451172" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159824951172" />
                  <Point X="27.73284765625" Y="-2.155964355469" />
                  <Point X="27.538134765625" Y="-2.120799316406" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.4448359375" Y="-2.135175537109" />
                  <Point X="27.427076171875" Y="-2.144521728516" />
                  <Point X="27.265318359375" Y="-2.229654541016" />
                  <Point X="27.242384765625" Y="-2.246549804688" />
                  <Point X="27.22142578125" Y="-2.267509521484" />
                  <Point X="27.204537109375" Y="-2.290434326172" />
                  <Point X="27.195189453125" Y="-2.308193115234" />
                  <Point X="27.110056640625" Y="-2.469952392578" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531908447266" />
                  <Point X="27.095677734375" Y="-2.563261474609" />
                  <Point X="27.0995390625" Y="-2.584638183594" />
                  <Point X="27.134705078125" Y="-2.779351806641" />
                  <Point X="27.13898828125" Y="-2.795139892578" />
                  <Point X="27.151822265625" Y="-2.826078857422" />
                  <Point X="27.85040234375" Y="-4.036056884766" />
                  <Point X="27.86128515625" Y="-4.054905761719" />
                  <Point X="27.781861328125" Y="-4.111636230469" />
                  <Point X="27.765326171875" Y="-4.122339355469" />
                  <Point X="27.701765625" Y="-4.163480957031" />
                  <Point X="26.859916015625" Y="-3.066360351562" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.721923828125" Y="-2.900556640625" />
                  <Point X="26.70083984375" Y="-2.887001953125" />
                  <Point X="26.508798828125" Y="-2.763537841797" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.39404296875" Y="-2.743238525391" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.104595703125" Y="-2.795461425781" />
                  <Point X="26.086791015625" Y="-2.810265625" />
                  <Point X="25.924611328125" Y="-2.945111816406" />
                  <Point X="25.904142578125" Y="-2.968860839844" />
                  <Point X="25.88725" Y="-2.996687255859" />
                  <Point X="25.875623046875" Y="-3.025812988281" />
                  <Point X="25.87030078125" Y="-3.050305664062" />
                  <Point X="25.82180859375" Y="-3.273400878906" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="26.01771484375" Y="-4.82687109375" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="25.975673828125" Y="-4.870084472656" />
                  <Point X="25.96041015625" Y="-4.872857421875" />
                  <Point X="25.92931640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941572265625" Y="-4.752636230469" />
                  <Point X="23.9193046875" Y="-4.746907226563" />
                  <Point X="23.858755859375" Y="-4.731328613281" />
                  <Point X="23.874455078125" Y="-4.612072753906" />
                  <Point X="23.8792265625" Y="-4.575839355469" />
                  <Point X="23.879923828125" Y="-4.568100097656" />
                  <Point X="23.88055078125" Y="-4.541032226562" />
                  <Point X="23.8782421875" Y="-4.509323730469" />
                  <Point X="23.876666015625" Y="-4.497687011719" />
                  <Point X="23.870677734375" Y="-4.467584960938" />
                  <Point X="23.81613671875" Y="-4.193395507813" />
                  <Point X="23.811873046875" Y="-4.178466308594" />
                  <Point X="23.800970703125" Y="-4.1495" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.76942578125" Y="-4.094861816406" />
                  <Point X="23.749794921875" Y="-4.070940429688" />
                  <Point X="23.738998046875" Y="-4.059783203125" />
                  <Point X="23.715923828125" Y="-4.039546386719" />
                  <Point X="23.50573828125" Y="-3.855218261719" />
                  <Point X="23.493265625" Y="-3.845967773438" />
                  <Point X="23.46698046875" Y="-3.829622070312" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.33255859375" Y="-3.794162353516" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.79549609375" />
                  <Point X="22.918134765625" Y="-3.808269042969" />
                  <Point X="22.9045625" Y="-3.815811767578" />
                  <Point X="22.87904296875" Y="-3.83286328125" />
                  <Point X="22.64659765625" Y="-3.9881796875" />
                  <Point X="22.640314453125" Y="-3.992760253906" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629394531" />
                  <Point X="22.496798828125" Y="-4.162478515625" />
                  <Point X="22.2524140625" Y="-4.011162353516" />
                  <Point X="22.22159375" Y="-3.987431640625" />
                  <Point X="22.019138671875" Y="-3.831546386719" />
                  <Point X="22.5848671875" Y="-2.851673339844" />
                  <Point X="22.658509765625" Y="-2.724119384766" />
                  <Point X="22.665150390625" Y="-2.710082763672" />
                  <Point X="22.6760546875" Y="-2.681110351562" />
                  <Point X="22.680318359375" Y="-2.666174560547" />
                  <Point X="22.686587890625" Y="-2.634644775391" />
                  <Point X="22.688376953125" Y="-2.618685302734" />
                  <Point X="22.6892421875" Y="-2.586689697266" />
                  <Point X="22.684705078125" Y="-2.555002929688" />
                  <Point X="22.67489453125" Y="-2.524536376953" />
                  <Point X="22.668697265625" Y="-2.509720458984" />
                  <Point X="22.652767578125" Y="-2.479180419922" />
                  <Point X="22.645126953125" Y="-2.466909667969" />
                  <Point X="22.62807421875" Y="-2.443672363281" />
                  <Point X="22.618662109375" Y="-2.432705810547" />
                  <Point X="22.60303125" Y="-2.417073486328" />
                  <Point X="22.591193359375" Y="-2.407027832031" />
                  <Point X="22.56603515625" Y="-2.388997802734" />
                  <Point X="22.55271484375" Y="-2.381013427734" />
                  <Point X="22.523884765625" Y="-2.366795166016" />
                  <Point X="22.5094453125" Y="-2.361088378906" />
                  <Point X="22.479828125" Y="-2.352103271484" />
                  <Point X="22.449140625" Y="-2.348062744141" />
                  <Point X="22.418205078125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.206912109375" Y="-3.017708496094" />
                  <Point X="20.995984375" Y="-2.740592285156" />
                  <Point X="20.973890625" Y="-2.703544921875" />
                  <Point X="20.818734375" Y="-2.443372802734" />
                  <Point X="21.823025390625" Y="-1.672753540039" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.9644140625" Y="-1.562333862305" />
                  <Point X="21.987224609375" Y="-1.537182373047" />
                  <Point X="21.997498046875" Y="-1.523579101562" />
                  <Point X="22.0162265625" Y="-1.493503417969" />
                  <Point X="22.022890625" Y="-1.480734863281" />
                  <Point X="22.03421484375" Y="-1.454333740234" />
                  <Point X="22.038875" Y="-1.440700927734" />
                  <Point X="22.04581640625" Y="-1.413896362305" />
                  <Point X="22.04844921875" Y="-1.398796020508" />
                  <Point X="22.051251953125" Y="-1.36837512207" />
                  <Point X="22.051421875" Y="-1.3530546875" />
                  <Point X="22.04921484375" Y="-1.321387207031" />
                  <Point X="22.046765625" Y="-1.305585693359" />
                  <Point X="22.03925" Y="-1.274619628906" />
                  <Point X="22.0266484375" Y="-1.245353759766" />
                  <Point X="22.00931640625" Y="-1.218614746094" />
                  <Point X="21.99951953125" Y="-1.20597668457" />
                  <Point X="21.97606640625" Y="-1.180476928711" />
                  <Point X="21.965849609375" Y="-1.170893066406" />
                  <Point X="21.94411328125" Y="-1.153330566406" />
                  <Point X="21.93259375" Y="-1.145352050781" />
                  <Point X="21.90873046875" Y="-1.131307373047" />
                  <Point X="21.89456640625" Y="-1.124480957031" />
                  <Point X="21.8653046875" Y="-1.113257324219" />
                  <Point X="21.85020703125" Y="-1.108859863281" />
                  <Point X="21.8183203125" Y="-1.102378417969" />
                  <Point X="21.802703125" Y="-1.100532226562" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.259236328125" Y="-0.974108886719" />
                  <Point X="20.25339453125" Y="-0.933254333496" />
                  <Point X="20.213548828125" Y="-0.654654602051" />
                  <Point X="21.344931640625" Y="-0.351501281738" />
                  <Point X="21.491712890625" Y="-0.312171234131" />
                  <Point X="21.5007734375" Y="-0.309249298096" />
                  <Point X="21.527279296875" Y="-0.298700042725" />
                  <Point X="21.545591796875" Y="-0.289574462891" />
                  <Point X="21.5516171875" Y="-0.286295837402" />
                  <Point X="21.569181640625" Y="-0.275609100342" />
                  <Point X="21.594189453125" Y="-0.258252471924" />
                  <Point X="21.6072890625" Y="-0.247292434692" />
                  <Point X="21.6313125" Y="-0.223204238892" />
                  <Point X="21.642236328125" Y="-0.210076080322" />
                  <Point X="21.662400390625" Y="-0.180855636597" />
                  <Point X="21.669650390625" Y="-0.168432250977" />
                  <Point X="21.68219140625" Y="-0.142633239746" />
                  <Point X="21.687482421875" Y="-0.12925793457" />
                  <Point X="21.6958125" Y="-0.10241619873" />
                  <Point X="21.69919921875" Y="-0.087182945251" />
                  <Point X="21.703431640625" Y="-0.056361980438" />
                  <Point X="21.70427734375" Y="-0.040774116516" />
                  <Point X="21.70336328125" Y="-0.008183498383" />
                  <Point X="21.7021171875" Y="0.004710278988" />
                  <Point X="21.697884765625" Y="0.030206682205" />
                  <Point X="21.6948984375" Y="0.042809307098" />
                  <Point X="21.6865625" Y="0.069667541504" />
                  <Point X="21.680009765625" Y="0.085543884277" />
                  <Point X="21.66415234375" Y="0.115856109619" />
                  <Point X="21.65484765625" Y="0.130291854858" />
                  <Point X="21.632853515625" Y="0.158455444336" />
                  <Point X="21.62323828125" Y="0.169022827148" />
                  <Point X="21.60253125" Y="0.188595565796" />
                  <Point X="21.591439453125" Y="0.197601074219" />
                  <Point X="21.566435546875" Y="0.214954727173" />
                  <Point X="21.559515625" Y="0.219327941895" />
                  <Point X="21.53438671875" Y="0.232833450317" />
                  <Point X="21.503435546875" Y="0.245635238647" />
                  <Point X="21.491712890625" Y="0.249611053467" />
                  <Point X="20.214556640625" Y="0.591824523926" />
                  <Point X="20.268671875" Y="0.957533691406" />
                  <Point X="20.2804375" Y="1.000959533691" />
                  <Point X="20.3664140625" Y="1.318237182617" />
                  <Point X="21.11671875" Y="1.219457641602" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815551758" />
                  <Point X="21.2529453125" Y="1.204364379883" />
                  <Point X="21.263298828125" Y="1.204703369141" />
                  <Point X="21.284859375" Y="1.20658984375" />
                  <Point X="21.29511328125" Y="1.208053955078" />
                  <Point X="21.315400390625" Y="1.212089599609" />
                  <Point X="21.331509765625" Y="1.216577026367" />
                  <Point X="21.386859375" Y="1.234028564453" />
                  <Point X="21.3965546875" Y="1.237678344727" />
                  <Point X="21.41548828125" Y="1.246010253906" />
                  <Point X="21.4247265625" Y="1.250692382812" />
                  <Point X="21.44347265625" Y="1.261516235352" />
                  <Point X="21.4521484375" Y="1.267177001953" />
                  <Point X="21.46883203125" Y="1.279410888672" />
                  <Point X="21.4840859375" Y="1.293390869141" />
                  <Point X="21.497724609375" Y="1.308946166992" />
                  <Point X="21.5041171875" Y="1.317094726562" />
                  <Point X="21.51653125" Y="1.334827636719" />
                  <Point X="21.522" Y="1.343622070313" />
                  <Point X="21.531947265625" Y="1.361756835938" />
                  <Point X="21.538861328125" Y="1.376978149414" />
                  <Point X="21.5610703125" Y="1.430596069336" />
                  <Point X="21.56450390625" Y="1.440361694336" />
                  <Point X="21.570287109375" Y="1.460204589844" />
                  <Point X="21.57263671875" Y="1.470282470703" />
                  <Point X="21.576396484375" Y="1.491595336914" />
                  <Point X="21.57763671875" Y="1.501873901367" />
                  <Point X="21.5789921875" Y="1.522514282227" />
                  <Point X="21.57809375" Y="1.543181518555" />
                  <Point X="21.57494921875" Y="1.563625854492" />
                  <Point X="21.572818359375" Y="1.573764282227" />
                  <Point X="21.56721875" Y="1.594668945312" />
                  <Point X="21.563994140625" Y="1.604521118164" />
                  <Point X="21.556490234375" Y="1.623816650391" />
                  <Point X="21.54926953125" Y="1.638907958984" />
                  <Point X="21.522474609375" Y="1.690377685547" />
                  <Point X="21.5171953125" Y="1.699292358398" />
                  <Point X="21.505703125" Y="1.716489990234" />
                  <Point X="21.499490234375" Y="1.724772583008" />
                  <Point X="21.485578125" Y="1.741352172852" />
                  <Point X="21.4785" Y="1.748910644531" />
                  <Point X="21.46355859375" Y="1.763214355469" />
                  <Point X="21.4556953125" Y="1.769958984375" />
                  <Point X="20.77238671875" Y="2.29428125" />
                  <Point X="20.9977109375" Y="2.680313476562" />
                  <Point X="21.028876953125" Y="2.720375" />
                  <Point X="21.273662109375" Y="3.035012451172" />
                  <Point X="21.68122265625" Y="2.799707519531" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.814384765625" Y="2.736657226562" />
                  <Point X="21.834669921875" Y="2.732622070312" />
                  <Point X="21.85338671875" Y="2.730417480469" />
                  <Point X="21.93047265625" Y="2.723673339844" />
                  <Point X="21.940826171875" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.02356640625" Y="2.734227294922" />
                  <Point X="22.043005859375" Y="2.741302978516" />
                  <Point X="22.061556640625" Y="2.750451904297" />
                  <Point X="22.070580078125" Y="2.755531738281" />
                  <Point X="22.088833984375" Y="2.767160888672" />
                  <Point X="22.097248046875" Y="2.773188720703" />
                  <Point X="22.113376953125" Y="2.786131347656" />
                  <Point X="22.127099609375" Y="2.799053222656" />
                  <Point X="22.18181640625" Y="2.85376953125" />
                  <Point X="22.188734375" Y="2.861486328125" />
                  <Point X="22.201685546875" Y="2.877623291016" />
                  <Point X="22.20771875" Y="2.886043457031" />
                  <Point X="22.21934765625" Y="2.904297851562" />
                  <Point X="22.2244296875" Y="2.913325683594" />
                  <Point X="22.233576171875" Y="2.931874511719" />
                  <Point X="22.240646484375" Y="2.951299072266" />
                  <Point X="22.245564453125" Y="2.971387939453" />
                  <Point X="22.2474765625" Y="2.981573242188" />
                  <Point X="22.250302734375" Y="3.003031982422" />
                  <Point X="22.251091796875" Y="3.013364746094" />
                  <Point X="22.25154296875" Y="3.034046875" />
                  <Point X="22.25046484375" Y="3.052859375" />
                  <Point X="22.243720703125" Y="3.129945800781" />
                  <Point X="22.242255859375" Y="3.140202636719" />
                  <Point X="22.23821875" Y="3.160497558594" />
                  <Point X="22.235646484375" Y="3.170535644531" />
                  <Point X="22.22913671875" Y="3.191177734375" />
                  <Point X="22.225486328125" Y="3.200873046875" />
                  <Point X="22.21715625" Y="3.219800537109" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="21.940611328125" Y="3.699914794922" />
                  <Point X="22.35163671875" Y="4.015043212891" />
                  <Point X="22.400705078125" Y="4.042303955078" />
                  <Point X="22.807474609375" Y="4.268295898437" />
                  <Point X="22.86165234375" Y="4.197689453125" />
                  <Point X="22.881435546875" Y="4.171908203125" />
                  <Point X="22.888181640625" Y="4.164045410156" />
                  <Point X="22.902486328125" Y="4.149103027344" />
                  <Point X="22.910044921875" Y="4.142022949219" />
                  <Point X="22.926625" Y="4.128110839844" />
                  <Point X="22.934912109375" Y="4.12189453125" />
                  <Point X="22.95211328125" Y="4.110401855469" />
                  <Point X="22.970447265625" Y="4.100222167969" />
                  <Point X="23.056244140625" Y="4.055558837891" />
                  <Point X="23.0656796875" Y="4.051283935547" />
                  <Point X="23.08495703125" Y="4.043788330078" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.229271484375" Y="4.037488525391" />
                  <Point X="23.249134765625" Y="4.043278076172" />
                  <Point X="23.26871875" Y="4.050779296875" />
                  <Point X="23.35808203125" Y="4.087794921875" />
                  <Point X="23.367419921875" Y="4.092274169922" />
                  <Point X="23.38555078125" Y="4.102221191406" />
                  <Point X="23.39434375" Y="4.107689453125" />
                  <Point X="23.41207421875" Y="4.120104003906" />
                  <Point X="23.420224609375" Y="4.126499511719" />
                  <Point X="23.435779296875" Y="4.140140625" />
                  <Point X="23.449755859375" Y="4.15539453125" />
                  <Point X="23.46198828125" Y="4.172079101562" />
                  <Point X="23.4676484375" Y="4.180755371094" />
                  <Point X="23.47846875" Y="4.199499511719" />
                  <Point X="23.4831484375" Y="4.208733398438" />
                  <Point X="23.4914765625" Y="4.227660644531" />
                  <Point X="23.498318359375" Y="4.247481933594" />
                  <Point X="23.527404296875" Y="4.33973046875" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370049316406" />
                  <Point X="23.535474609375" Y="4.380301757812" />
                  <Point X="23.537361328125" Y="4.401861328125" />
                  <Point X="23.53769921875" Y="4.41221484375" />
                  <Point X="23.537248046875" Y="4.432896484375" />
                  <Point X="23.536458984375" Y="4.443225097656" />
                  <Point X="23.520734375" Y="4.562655273438" />
                  <Point X="24.06882421875" Y="4.7163203125" />
                  <Point X="24.1283125" Y="4.723282714844" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.755216796875" Y="4.333072265625" />
                  <Point X="24.77433203125" Y="4.261728515625" />
                  <Point X="24.779564453125" Y="4.247106445313" />
                  <Point X="24.79233984375" Y="4.218913085938" />
                  <Point X="24.7998828125" Y="4.205341796875" />
                  <Point X="24.8177421875" Y="4.178614746094" />
                  <Point X="24.82739453125" Y="4.166453125" />
                  <Point X="24.84855078125" Y="4.143865722656" />
                  <Point X="24.873103515625" Y="4.125026855469" />
                  <Point X="24.900396484375" Y="4.110437988281" />
                  <Point X="24.914640625" Y="4.104262207031" />
                  <Point X="24.945078125" Y="4.093929199219" />
                  <Point X="24.960140625" Y="4.090156005859" />
                  <Point X="24.990677734375" Y="4.085113525391" />
                  <Point X="25.021626953125" Y="4.085112792969" />
                  <Point X="25.052166015625" Y="4.090154296875" />
                  <Point X="25.06723046875" Y="4.093927246094" />
                  <Point X="25.09766796875" Y="4.104259277344" />
                  <Point X="25.1119140625" Y="4.110436035156" />
                  <Point X="25.13920703125" Y="4.125024902344" />
                  <Point X="25.1637578125" Y="4.143861328125" />
                  <Point X="25.184916015625" Y="4.16644921875" />
                  <Point X="25.1945703125" Y="4.178612792969" />
                  <Point X="25.2124296875" Y="4.205339355469" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.24710546875" />
                  <Point X="25.23798046875" Y="4.261727050781" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.827876953125" Y="4.737912109375" />
                  <Point X="25.87710546875" Y="4.726026855469" />
                  <Point X="26.453591796875" Y="4.586845214844" />
                  <Point X="26.483330078125" Y="4.576059082031" />
                  <Point X="26.858248046875" Y="4.440073242188" />
                  <Point X="26.889244140625" Y="4.425577636719" />
                  <Point X="27.250453125" Y="4.256651855469" />
                  <Point X="27.280451171875" Y="4.23917578125" />
                  <Point X="27.629451171875" Y="4.035847900391" />
                  <Point X="27.657673828125" Y="4.015776855469" />
                  <Point X="27.81778515625" Y="3.901916015625" />
                  <Point X="27.150548828125" Y="2.746232177734" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.061341796875" Y="2.590938476562" />
                  <Point X="27.05085546875" Y="2.567302978516" />
                  <Point X="27.043728515625" Y="2.54727734375" />
                  <Point X="27.039177734375" Y="2.532655761719" />
                  <Point X="27.01983203125" Y="2.460311767578" />
                  <Point X="27.01754296875" Y="2.449072998047" />
                  <Point X="27.014337890625" Y="2.42641015625" />
                  <Point X="27.013421875" Y="2.414986083984" />
                  <Point X="27.012912109375" Y="2.389216064453" />
                  <Point X="27.014240234375" Y="2.362713134766" />
                  <Point X="27.021783203125" Y="2.300156005859" />
                  <Point X="27.02380078125" Y="2.289037109375" />
                  <Point X="27.029142578125" Y="2.267115722656" />
                  <Point X="27.032466796875" Y="2.256313232422" />
                  <Point X="27.040732421875" Y="2.234224121094" />
                  <Point X="27.045318359375" Y="2.22388671875" />
                  <Point X="27.055685546875" Y="2.203835449219" />
                  <Point X="27.065716796875" Y="2.187858886719" />
                  <Point X="27.10442578125" Y="2.130812988281" />
                  <Point X="27.1114765625" Y="2.121670410156" />
                  <Point X="27.12664453125" Y="2.104301269531" />
                  <Point X="27.13476171875" Y="2.096074707031" />
                  <Point X="27.15432421875" Y="2.078524902344" />
                  <Point X="27.1745234375" Y="2.062729980469" />
                  <Point X="27.2315703125" Y="2.024022216797" />
                  <Point X="27.241283203125" Y="2.018243408203" />
                  <Point X="27.261328125" Y="2.007881103516" />
                  <Point X="27.27166015625" Y="2.003297485352" />
                  <Point X="27.293748046875" Y="1.995031860352" />
                  <Point X="27.304546875" Y="1.991708251953" />
                  <Point X="27.32646484375" Y="1.986365966797" />
                  <Point X="27.344451171875" Y="1.983519165039" />
                  <Point X="27.4070078125" Y="1.975975952148" />
                  <Point X="27.4187734375" Y="1.97529309082" />
                  <Point X="27.442298828125" Y="1.975389526367" />
                  <Point X="27.45405859375" Y="1.976168701172" />
                  <Point X="27.480904296875" Y="1.979630371094" />
                  <Point X="27.505689453125" Y="1.984519897461" />
                  <Point X="27.578033203125" Y="2.003865600586" />
                  <Point X="27.5839921875" Y="2.005670288086" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.94040625" Y="2.780951904297" />
                  <Point X="29.043955078125" Y="2.637042724609" />
                  <Point X="29.0596953125" Y="2.611030273438" />
                  <Point X="29.13688671875" Y="2.483471679688" />
                  <Point X="28.28517578125" Y="1.829930664062" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.16620703125" Y="1.738127563477" />
                  <Point X="28.14707421875" Y="1.719834960938" />
                  <Point X="28.13211328125" Y="1.703128417969" />
                  <Point X="28.12286328125" Y="1.691965942383" />
                  <Point X="28.070796875" Y="1.624041870117" />
                  <Point X="28.064259765625" Y="1.614327758789" />
                  <Point X="28.052427734375" Y="1.594164916992" />
                  <Point X="28.0471328125" Y="1.583716064453" />
                  <Point X="28.03653515625" Y="1.559151367188" />
                  <Point X="28.028015625" Y="1.535063964844" />
                  <Point X="28.008619140625" Y="1.465713256836" />
                  <Point X="28.006228515625" Y="1.454670532227" />
                  <Point X="28.0027734375" Y="1.432370727539" />
                  <Point X="28.001708984375" Y="1.421113647461" />
                  <Point X="28.000892578125" Y="1.397541503906" />
                  <Point X="28.001173828125" Y="1.386236816406" />
                  <Point X="28.003078125" Y="1.36374987793" />
                  <Point X="28.00644921875" Y="1.344096435547" />
                  <Point X="28.02237109375" Y="1.266934936523" />
                  <Point X="28.02546875" Y="1.255544189453" />
                  <Point X="28.033056640625" Y="1.233233642578" />
                  <Point X="28.037546875" Y="1.222313354492" />
                  <Point X="28.049498046875" Y="1.197710571289" />
                  <Point X="28.061669921875" Y="1.176301147461" />
                  <Point X="28.10497265625" Y="1.110481811523" />
                  <Point X="28.111736328125" Y="1.101424560547" />
                  <Point X="28.126291015625" Y="1.084178710938" />
                  <Point X="28.13408203125" Y="1.075989868164" />
                  <Point X="28.151326171875" Y="1.059900146484" />
                  <Point X="28.1600390625" Y="1.052691650391" />
                  <Point X="28.178255859375" Y="1.039362548828" />
                  <Point X="28.194650390625" Y="1.029363891602" />
                  <Point X="28.25740234375" Y="0.994039672852" />
                  <Point X="28.268248046875" Y="0.988810852051" />
                  <Point X="28.290515625" Y="0.979767150879" />
                  <Point X="28.3019375" Y="0.975952270508" />
                  <Point X="28.329228515625" Y="0.968722961426" />
                  <Point X="28.352986328125" Y="0.964026489258" />
                  <Point X="28.437833984375" Y="0.952812866211" />
                  <Point X="28.444033203125" Y="0.952199645996" />
                  <Point X="28.46571875" Y="0.95122277832" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797302246" />
                  <Point X="29.704703125" Y="1.111319946289" />
                  <Point X="29.752693359375" Y="0.914195068359" />
                  <Point X="29.757646484375" Y="0.882371765137" />
                  <Point X="29.783873046875" Y="0.713921508789" />
                  <Point X="28.8206015625" Y="0.45581338501" />
                  <Point X="28.6919921875" Y="0.421352813721" />
                  <Point X="28.68340625" Y="0.418609832764" />
                  <Point X="28.658251953125" Y="0.408779907227" />
                  <Point X="28.637021484375" Y="0.398482574463" />
                  <Point X="28.62485546875" Y="0.392027435303" />
                  <Point X="28.54149609375" Y="0.343844787598" />
                  <Point X="28.53155859375" Y="0.337234344482" />
                  <Point X="28.512587890625" Y="0.322818206787" />
                  <Point X="28.5035546875" Y="0.315012542725" />
                  <Point X="28.483751953125" Y="0.295595703125" />
                  <Point X="28.467306640625" Y="0.277231109619" />
                  <Point X="28.417291015625" Y="0.213500152588" />
                  <Point X="28.410853515625" Y="0.204206619263" />
                  <Point X="28.39912890625" Y="0.184924285889" />
                  <Point X="28.393841796875" Y="0.174935638428" />
                  <Point X="28.384068359375" Y="0.153471511841" />
                  <Point X="28.38000390625" Y="0.142924194336" />
                  <Point X="28.373158203125" Y="0.12142263031" />
                  <Point X="28.368546875" Y="0.100911781311" />
                  <Point X="28.351875" Y="0.013858463287" />
                  <Point X="28.350365234375" Y="0.001913939834" />
                  <Point X="28.3488671875" Y="-0.022068887711" />
                  <Point X="28.34887890625" Y="-0.034107486725" />
                  <Point X="28.350708984375" Y="-0.062474391937" />
                  <Point X="28.353705078125" Y="-0.085980140686" />
                  <Point X="28.370376953125" Y="-0.173033462524" />
                  <Point X="28.37316015625" Y="-0.183988006592" />
                  <Point X="28.380005859375" Y="-0.205487197876" />
                  <Point X="28.384068359375" Y="-0.216031997681" />
                  <Point X="28.393841796875" Y="-0.237495956421" />
                  <Point X="28.399126953125" Y="-0.247482070923" />
                  <Point X="28.41084765625" Y="-0.266759216309" />
                  <Point X="28.4227734375" Y="-0.283046630859" />
                  <Point X="28.4727890625" Y="-0.346777770996" />
                  <Point X="28.480990234375" Y="-0.355939483643" />
                  <Point X="28.4985" Y="-0.373121459961" />
                  <Point X="28.50780859375" Y="-0.381141906738" />
                  <Point X="28.5312734375" Y="-0.398851501465" />
                  <Point X="28.5506484375" Y="-0.41169342041" />
                  <Point X="28.634005859375" Y="-0.459876037598" />
                  <Point X="28.63949609375" Y="-0.462813720703" />
                  <Point X="28.659154296875" Y="-0.472014831543" />
                  <Point X="28.683025390625" Y="-0.481027008057" />
                  <Point X="28.6919921875" Y="-0.483912963867" />
                  <Point X="29.784880859375" Y="-0.776751342773" />
                  <Point X="29.761619140625" Y="-0.931042419434" />
                  <Point X="29.755259765625" Y="-0.958902893066" />
                  <Point X="29.7278046875" Y="-1.079219726563" />
                  <Point X="28.58593359375" Y="-0.928889709473" />
                  <Point X="28.436783203125" Y="-0.909253662109" />
                  <Point X="28.424388671875" Y="-0.908440856934" />
                  <Point X="28.3996015625" Y="-0.908439331055" />
                  <Point X="28.387208984375" Y="-0.909250366211" />
                  <Point X="28.352103515625" Y="-0.9138671875" />
                  <Point X="28.336521484375" Y="-0.91658013916" />
                  <Point X="28.17291796875" Y="-0.952139953613" />
                  <Point X="28.161470703125" Y="-0.95539050293" />
                  <Point X="28.131099609375" Y="-0.966111999512" />
                  <Point X="28.11005859375" Y="-0.976628540039" />
                  <Point X="28.0774375" Y="-0.998357666016" />
                  <Point X="28.062259765625" Y="-1.010922668457" />
                  <Point X="28.038900390625" Y="-1.03475390625" />
                  <Point X="28.028490234375" Y="-1.046280761719" />
                  <Point X="27.929603515625" Y="-1.165211791992" />
                  <Point X="27.923177734375" Y="-1.17390234375" />
                  <Point X="27.9071484375" Y="-1.19837902832" />
                  <Point X="27.897470703125" Y="-1.217607788086" />
                  <Point X="27.88505078125" Y="-1.25134765625" />
                  <Point X="27.880451171875" Y="-1.268812011719" />
                  <Point X="27.875298828125" Y="-1.300273681641" />
                  <Point X="27.873603515625" Y="-1.313571289062" />
                  <Point X="27.859431640625" Y="-1.467592285156" />
                  <Point X="27.859083984375" Y="-1.479482666016" />
                  <Point X="27.8601640625" Y="-1.511671630859" />
                  <Point X="27.86390234375" Y="-1.535057250977" />
                  <Point X="27.874951171875" Y="-1.572982910156" />
                  <Point X="27.88246875" Y="-1.591364013672" />
                  <Point X="27.89891796875" Y="-1.62198840332" />
                  <Point X="27.90648046875" Y="-1.634833007812" />
                  <Point X="27.997021484375" Y="-1.775662841797" />
                  <Point X="28.00174609375" Y="-1.782357910156" />
                  <Point X="28.01980078125" Y="-1.804456176758" />
                  <Point X="28.043494140625" Y="-1.828122558594" />
                  <Point X="28.052798828125" Y="-1.836277709961" />
                  <Point X="29.087171875" Y="-2.629980712891" />
                  <Point X="29.045482421875" Y="-2.697440185547" />
                  <Point X="29.03233984375" Y="-2.716113769531" />
                  <Point X="29.001275390625" Y="-2.760252197266" />
                  <Point X="27.98129296875" Y="-2.171364990234" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513671875" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337158203" />
                  <Point X="27.74973046875" Y="-2.0624765625" />
                  <Point X="27.555017578125" Y="-2.027311767578" />
                  <Point X="27.539359375" Y="-2.025807250977" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503295898" />
                  <Point X="27.460140625" Y="-2.03146105957" />
                  <Point X="27.444845703125" Y="-2.035135864258" />
                  <Point X="27.415072265625" Y="-2.044958251953" />
                  <Point X="27.40059375" Y="-2.051106201172" />
                  <Point X="27.382833984375" Y="-2.060452392578" />
                  <Point X="27.221076171875" Y="-2.145585205078" />
                  <Point X="27.208970703125" Y="-2.153169189453" />
                  <Point X="27.186037109375" Y="-2.170064453125" />
                  <Point X="27.175208984375" Y="-2.179375732422" />
                  <Point X="27.15425" Y="-2.200335449219" />
                  <Point X="27.144939453125" Y="-2.211162841797" />
                  <Point X="27.12805078125" Y="-2.234087646484" />
                  <Point X="27.12047265625" Y="-2.246185058594" />
                  <Point X="27.111125" Y="-2.263943847656" />
                  <Point X="27.0259921875" Y="-2.425703125" />
                  <Point X="27.019841796875" Y="-2.440182128906" />
                  <Point X="27.010013671875" Y="-2.469965087891" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517442138672" />
                  <Point X="27.000279296875" Y="-2.533139404297" />
                  <Point X="27.000685546875" Y="-2.564492431641" />
                  <Point X="27.00219140625" Y="-2.580148193359" />
                  <Point X="27.006052734375" Y="-2.601524902344" />
                  <Point X="27.04121875" Y="-2.796238525391" />
                  <Point X="27.04301953125" Y="-2.804225585938" />
                  <Point X="27.05123828125" Y="-2.831540039062" />
                  <Point X="27.064072265625" Y="-2.862479003906" />
                  <Point X="27.06955078125" Y="-2.873578857422" />
                  <Point X="27.73589453125" Y="-4.027723388672" />
                  <Point X="27.72375390625" Y="-4.036082519531" />
                  <Point X="26.93528515625" Y="-3.008528076172" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.808830078125" Y="-2.849624267578" />
                  <Point X="26.78325" Y="-2.828002685547" />
                  <Point X="26.773296875" Y="-2.820645996094" />
                  <Point X="26.752212890625" Y="-2.807091308594" />
                  <Point X="26.560171875" Y="-2.683627197266" />
                  <Point X="26.54628125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663874023438" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.385337890625" Y="-2.648638183594" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079876953125" Y="-2.699413330078" />
                  <Point X="26.055494140625" Y="-2.714134521484" />
                  <Point X="26.043857421875" Y="-2.722413818359" />
                  <Point X="26.026052734375" Y="-2.737218017578" />
                  <Point X="25.863873046875" Y="-2.872064208984" />
                  <Point X="25.852650390625" Y="-2.883090576172" />
                  <Point X="25.832181640625" Y="-2.906839599609" />
                  <Point X="25.822935546875" Y="-2.919562255859" />
                  <Point X="25.80604296875" Y="-2.947388671875" />
                  <Point X="25.79901953125" Y="-2.961466064453" />
                  <Point X="25.787392578125" Y="-2.990591796875" />
                  <Point X="25.7827890625" Y="-3.005640136719" />
                  <Point X="25.777466796875" Y="-3.0301328125" />
                  <Point X="25.728974609375" Y="-3.253228027344" />
                  <Point X="25.72758203125" Y="-3.261299316406" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520019531" />
                  <Point X="25.83308984375" Y="-4.152323242188" />
                  <Point X="25.6946953125" Y="-3.635829101562" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.48012109375" />
                  <Point X="25.64214453125" Y="-3.453576660156" />
                  <Point X="25.62678515625" Y="-3.423812011719" />
                  <Point X="25.62040625" Y="-3.413208251953" />
                  <Point X="25.604208984375" Y="-3.389871582031" />
                  <Point X="25.456677734375" Y="-3.177307861328" />
                  <Point X="25.446671875" Y="-3.165173339844" />
                  <Point X="25.4247890625" Y="-3.142718261719" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.373240234375" Y="-3.104936523438" />
                  <Point X="25.345240234375" Y="-3.090829345703" />
                  <Point X="25.33065625" Y="-3.084939453125" />
                  <Point X="25.30559375" Y="-3.077160644531" />
                  <Point X="25.077296875" Y="-3.006306152344" />
                  <Point X="25.06337890625" Y="-3.003109863281" />
                  <Point X="25.03521875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.97709375" Y="-2.998840087891" />
                  <Point X="24.94893359375" Y="-3.003109863281" />
                  <Point X="24.935015625" Y="-3.006306152344" />
                  <Point X="24.909953125" Y="-3.014084960938" />
                  <Point X="24.68165625" Y="-3.084939453125" />
                  <Point X="24.667072265625" Y="-3.090829345703" />
                  <Point X="24.639072265625" Y="-3.104936523438" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.5875234375" Y="-3.142718261719" />
                  <Point X="24.56563671875" Y="-3.165177490234" />
                  <Point X="24.555626953125" Y="-3.177316162109" />
                  <Point X="24.539431640625" Y="-3.200652587891" />
                  <Point X="24.391900390625" Y="-3.413216552734" />
                  <Point X="24.387529296875" Y="-3.420134277344" />
                  <Point X="24.374025390625" Y="-3.445260498047" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936035156" />
                  <Point X="24.01457421875" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.146246879543" Y="2.871237801725" />
                  <Point X="20.849585154035" Y="2.235044678322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.418851624251" Y="1.311333642805" />
                  <Point X="20.289199494784" Y="1.033293753904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.316030601425" Y="3.01055101789" />
                  <Point X="20.926783589321" Y="2.175808106645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.51760970832" Y="1.298331887147" />
                  <Point X="20.226826805769" Y="0.674745940261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.39861730138" Y="2.962869617087" />
                  <Point X="21.003982024606" Y="2.116571534967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.616367792388" Y="1.285330131489" />
                  <Point X="20.284270307445" Y="0.573144776744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.481204001335" Y="2.915188216283" />
                  <Point X="21.081180459891" Y="2.05733496329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.715125876457" Y="1.272328375831" />
                  <Point X="20.377448837755" Y="0.548177629436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.958257729195" Y="3.713444086735" />
                  <Point X="21.946883283358" Y="3.68905150892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.56379070129" Y="2.867506815479" />
                  <Point X="21.158378895177" Y="1.998098391612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.813883960526" Y="1.259326620173" />
                  <Point X="20.470627368064" Y="0.523210482128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.121406325722" Y="3.838528230659" />
                  <Point X="22.004870217333" Y="3.588615739729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.646377401245" Y="2.819825414675" />
                  <Point X="21.235577330462" Y="1.938861819935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.912642044594" Y="1.246324864515" />
                  <Point X="20.563805898373" Y="0.49824333482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.284554922249" Y="3.963612374583" />
                  <Point X="22.062857151309" Y="3.488179970538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.728964069347" Y="2.772143945562" />
                  <Point X="21.312775765748" Y="1.879625248257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.011400128663" Y="1.233323108857" />
                  <Point X="20.656984428683" Y="0.473276187511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.434939394889" Y="4.061323766497" />
                  <Point X="22.120844085284" Y="3.387744201348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.816995110663" Y="2.736137972485" />
                  <Point X="21.389974201033" Y="1.82038867658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.110158212732" Y="1.220321353199" />
                  <Point X="20.750162958992" Y="0.448309040203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.239025837989" Y="-0.647828053117" />
                  <Point X="20.218781600624" Y="-0.691241960247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.576411462849" Y="4.139922444896" />
                  <Point X="22.178831019259" Y="3.287308432157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.916570807518" Y="2.724889593104" />
                  <Point X="21.466722080717" Y="1.760185885298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.208916348038" Y="1.207319707419" />
                  <Point X="20.843341489301" Y="0.423341892895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.358813878188" Y="-0.615730922315" />
                  <Point X="20.24338509867" Y="-0.863268738817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.717883530809" Y="4.218521123295" />
                  <Point X="22.23304561372" Y="3.178782854772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.026191508554" Y="2.735182794708" />
                  <Point X="21.531187015175" Y="1.673642232974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.316045251577" Y="1.212269232046" />
                  <Point X="20.936520019611" Y="0.398374745587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.478601918387" Y="-0.583633791513" />
                  <Point X="20.272431738932" Y="-1.025767168159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.831384514463" Y="4.237135617826" />
                  <Point X="22.248094681196" Y="2.986266533723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.191537179403" Y="2.86497857972" />
                  <Point X="21.577369329666" Y="1.547891375604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.443986857493" Y="1.261851740955" />
                  <Point X="21.02969854992" Y="0.373407598278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.598389958587" Y="-0.551536660711" />
                  <Point X="20.309529489387" Y="-1.17099993597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.897556461184" Y="4.154252665114" />
                  <Point X="21.122877080229" Y="0.34844045097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.718177998786" Y="-0.519439529909" />
                  <Point X="20.361795667605" Y="-1.283703905474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.975866927386" Y="4.097400851433" />
                  <Point X="21.216055610538" Y="0.323473303662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.837966038985" Y="-0.487342399107" />
                  <Point X="20.473472475877" Y="-1.269001367673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.060316271466" Y="4.053713903844" />
                  <Point X="21.309234140848" Y="0.298506156354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.957754079184" Y="-0.455245268304" />
                  <Point X="20.58514928415" Y="-1.254298829871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.153781201623" Y="4.029360942991" />
                  <Point X="21.402412671157" Y="0.273539009045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.077542119384" Y="-0.423148137502" />
                  <Point X="20.696826092422" Y="-1.23959629207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.268561573161" Y="4.050719093694" />
                  <Point X="21.495480009861" Y="0.24833341057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.197330159583" Y="-0.3910510067" />
                  <Point X="20.808502900695" Y="-1.224893754268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.625843850132" Y="4.592124258835" />
                  <Point X="23.537379279064" Y="4.40241137396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.402665117954" Y="4.113515923169" />
                  <Point X="21.580261585364" Y="0.205358935568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.317118199782" Y="-0.358953875898" />
                  <Point X="20.920179708968" Y="-1.210191216467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.746429686719" Y="4.625932269512" />
                  <Point X="21.651861308138" Y="0.134115886163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.436906274389" Y="-0.326856671309" />
                  <Point X="21.03185651724" Y="-1.195488678666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.867015523307" Y="4.659740280189" />
                  <Point X="21.700600333481" Y="0.013847912911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.564215277323" Y="-0.278630783874" />
                  <Point X="21.143533325513" Y="-1.180786140864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.987601359894" Y="4.693548290867" />
                  <Point X="21.255210133785" Y="-1.166083603063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.105016239356" Y="4.720556162045" />
                  <Point X="21.366886942058" Y="-1.151381065261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.215887907017" Y="4.733532070233" />
                  <Point X="21.47856375033" Y="-1.13667852746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.897383763869" Y="-2.383023030489" />
                  <Point X="20.847078936392" Y="-2.490902081146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.326759533472" Y="4.746507890053" />
                  <Point X="21.590240558603" Y="-1.121975989659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.060607965918" Y="-2.257776749998" />
                  <Point X="20.905903530993" Y="-2.58954148133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.437631159926" Y="4.759483709873" />
                  <Point X="21.701917366875" Y="-1.107273451857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.223832167968" Y="-2.132530469506" />
                  <Point X="20.964728125593" Y="-2.688180881513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.54850278638" Y="4.772459529693" />
                  <Point X="21.809506708478" Y="-1.101336514617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.387056370018" Y="-2.007284189015" />
                  <Point X="21.026446852559" Y="-2.780613794808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.643263604576" Y="4.750885609708" />
                  <Point X="21.901889417519" Y="-1.128010306143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.550280572068" Y="-1.882037908524" />
                  <Point X="21.091446679003" Y="-2.866010367567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.68151554171" Y="4.608128003216" />
                  <Point X="21.980166158196" Y="-1.184934444445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.713504774118" Y="-1.756791628033" />
                  <Point X="21.156446505446" Y="-2.951406940327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.719767478845" Y="4.465370396724" />
                  <Point X="22.040590561525" Y="-1.280143043738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.876729080266" Y="-1.631545124303" />
                  <Point X="21.238985323198" Y="-2.999191024848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.758019291059" Y="4.322612522339" />
                  <Point X="21.382422877554" Y="-2.91637734727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.80475555197" Y="4.198049606904" />
                  <Point X="21.52586043191" Y="-2.833563669692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.875042167888" Y="4.123990590758" />
                  <Point X="21.669297986266" Y="-2.750749992113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.963803689401" Y="4.089551137518" />
                  <Point X="21.812735540622" Y="-2.667936314535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.392234957357" Y="4.783535806212" />
                  <Point X="25.358298994862" Y="4.710759899789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.071311125567" Y="4.095312427983" />
                  <Point X="21.956173094978" Y="-2.585122636957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.492175251891" Y="4.77306930908" />
                  <Point X="22.099610649334" Y="-2.502308959379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.592115546425" Y="4.762602811947" />
                  <Point X="22.24304820369" Y="-2.419495281801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.692055840959" Y="4.752136314814" />
                  <Point X="22.377538155988" Y="-2.355869798759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.791996135494" Y="4.741669817681" />
                  <Point X="22.483584079084" Y="-2.353242733188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.888266125686" Y="4.723332327486" />
                  <Point X="22.57030514465" Y="-2.392057958326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.982480293364" Y="4.700586111682" />
                  <Point X="22.64212783564" Y="-2.462822850848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.076694461042" Y="4.677839895878" />
                  <Point X="22.689229616906" Y="-2.586601905355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.484817931867" Y="-3.024964178554" />
                  <Point X="22.085040682506" Y="-3.882289256471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.17090862872" Y="4.655093680074" />
                  <Point X="22.16216903419" Y="-3.941676122916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.265122796398" Y="4.63234746427" />
                  <Point X="22.239297414016" Y="-4.001062929012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.359336964076" Y="4.609601248466" />
                  <Point X="22.319918645851" Y="-4.052959289799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.453551131754" Y="4.586855032662" />
                  <Point X="22.401255577833" Y="-4.103320826672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.54321007767" Y="4.554340112265" />
                  <Point X="22.482592509814" Y="-4.153682363545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.632866981937" Y="4.521820813537" />
                  <Point X="22.672731284073" Y="-3.970717596687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.722523886203" Y="4.489301514808" />
                  <Point X="22.824994218583" Y="-3.868977830293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.81218079047" Y="4.45678221608" />
                  <Point X="22.96731501507" Y="-3.788559047695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.900086326634" Y="4.420507096336" />
                  <Point X="23.07732389196" Y="-3.777433400286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.986140737825" Y="4.380262226276" />
                  <Point X="23.179036123165" Y="-3.784099966967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.072195149016" Y="4.340017356217" />
                  <Point X="23.280748354369" Y="-3.790766533647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.158249560208" Y="4.299772486157" />
                  <Point X="23.381649860963" Y="-3.799171704868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.244303971399" Y="4.259527616098" />
                  <Point X="23.471082131282" Y="-3.83217273265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.326991088919" Y="4.212061561457" />
                  <Point X="23.547910678706" Y="-3.892202531406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.409418747012" Y="4.164039094281" />
                  <Point X="23.62230758872" Y="-3.957446993417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.491846405106" Y="4.116016627105" />
                  <Point X="23.696704498733" Y="-4.022691455429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.5742740632" Y="4.067994159929" />
                  <Point X="23.768434869102" Y="-4.09365433016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.655474863719" Y="4.017340688192" />
                  <Point X="23.819309042483" Y="-4.209343463669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.734192381806" Y="3.961361800096" />
                  <Point X="27.239037197703" Y="2.899498081061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.013487564727" Y="2.415805332226" />
                  <Point X="23.850652857704" Y="-4.366915585413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.812909924827" Y="3.90538296547" />
                  <Point X="27.784031877304" Y="3.843453792707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.037572961949" Y="2.242667482853" />
                  <Point X="24.417165544985" Y="-3.376814357381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.346300919841" Y="-3.528784036422" />
                  <Point X="23.879704167492" Y="-4.529404000922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.09601510249" Y="2.143207907292" />
                  <Point X="24.651741705775" Y="-3.098553307581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.20470582697" Y="-4.057224843394" />
                  <Point X="23.886979863963" Y="-4.738590369889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.166305176738" Y="2.069156307561" />
                  <Point X="24.77666018335" Y="-3.055453918322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.063110734099" Y="-4.585665650366" />
                  <Point X="23.981625643011" Y="-4.760410992121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.246199808867" Y="2.015701748673" />
                  <Point X="24.899218139319" Y="-3.01741668398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.336593652174" Y="1.984762820817" />
                  <Point X="25.013202025536" Y="-2.997766601562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.437033651139" Y="1.975367943294" />
                  <Point X="25.109395443025" Y="-3.016268302449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.551882288316" Y="1.996872490133" />
                  <Point X="25.20096417068" Y="-3.044687682689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.684098522343" Y="2.055621968607" />
                  <Point X="25.292532898336" Y="-3.07310706293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.827536121893" Y="2.138435743104" />
                  <Point X="25.380451773162" Y="-3.109353577821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.970973721443" Y="2.221249517601" />
                  <Point X="25.454702237329" Y="-3.174912093963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.114411320993" Y="2.304063292098" />
                  <Point X="25.517525141264" Y="-3.264977092107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.257848920544" Y="2.386877066596" />
                  <Point X="25.580222383014" Y="-3.355311573678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.401286520094" Y="2.469690841093" />
                  <Point X="25.939838214314" Y="-2.808902085129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.727224037887" Y="-3.264854657875" />
                  <Point X="25.640609054739" Y="-3.450601088655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.544724119644" Y="2.55250461559" />
                  <Point X="28.163981779975" Y="1.736000033239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.001159174069" Y="1.386825828057" />
                  <Point X="26.100177624257" Y="-2.689842261275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.741745714952" Y="-3.458501971312" />
                  <Point X="25.68169000065" Y="-3.587291866248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.688161719194" Y="2.635318390087" />
                  <Point X="28.327885101053" Y="1.862702689184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.033666848807" Y="1.231749611104" />
                  <Point X="26.216988394426" Y="-2.664129906657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.76482422733" Y="-3.6337990922" />
                  <Point X="25.719941753437" Y="-3.730049868075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.831599318744" Y="2.718132164584" />
                  <Point X="28.491109374551" Y="1.987949122898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.091493434316" Y="1.130969973517" />
                  <Point X="26.326508840008" Y="-2.654051703567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.787902739709" Y="-3.809096213089" />
                  <Point X="25.758193654465" Y="-3.872807551997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.955762051411" Y="2.759610853659" />
                  <Point X="28.65433364805" Y="2.113195556613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.159875612883" Y="1.052826878294" />
                  <Point X="26.434548232442" Y="-2.647149629205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.810981252087" Y="-3.984393333977" />
                  <Point X="25.796445555493" Y="-4.015565235919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.019364615326" Y="2.671217841736" />
                  <Point X="28.817557921549" Y="2.238441990327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.241466610782" Y="1.003010187541" />
                  <Point X="26.529218216959" Y="-2.668918342645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.080267352318" Y="2.577035032293" />
                  <Point X="28.980782195048" Y="2.363688424041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.330208757989" Y="0.968529185968" />
                  <Point X="26.611725061531" Y="-2.716770993868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.428289244067" Y="0.954074316729" />
                  <Point X="26.692369460097" Y="-2.768617673444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.534601716034" Y="0.957272998199" />
                  <Point X="26.773013968214" Y="-2.820464118086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.6462785291" Y="0.97197554628" />
                  <Point X="27.177830473276" Y="-2.177121471843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.000591215975" Y="-2.55721228571" />
                  <Point X="26.845003882882" Y="-2.890870398273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.757955342166" Y="0.986678094361" />
                  <Point X="27.322166157676" Y="-2.092381748171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.028022987058" Y="-2.72317381318" />
                  <Point X="26.910202839936" Y="-2.97583993406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.869632155232" Y="1.001380642443" />
                  <Point X="28.570959435154" Y="0.360874927268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.356291042227" Y="-0.099482926979" />
                  <Point X="27.454796056888" Y="-2.032745161844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.066032673839" Y="-2.866450927232" />
                  <Point X="26.975401875224" Y="-3.060809302073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.981308968298" Y="1.016083190524" />
                  <Point X="28.705693572442" Y="0.425024066711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.395390709168" Y="-0.240422571034" />
                  <Point X="28.008276348122" Y="-1.070591997325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.868350214369" Y="-1.370664559519" />
                  <Point X="27.561596549003" Y="-2.028499917787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.123752565604" Y="-2.96745937029" />
                  <Point X="27.040600959428" Y="-3.145778565187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.092985781364" Y="1.030785738605" />
                  <Point X="28.825481503158" Y="0.457120962726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.458881169023" Y="-0.329055990888" />
                  <Point X="28.167632483538" Y="-0.9536408125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.876762880677" Y="-1.5774126888" />
                  <Point X="27.65827568395" Y="-2.045959994225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.18173928447" Y="-3.067895600785" />
                  <Point X="27.105800043631" Y="-3.230747828302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.20466259443" Y="1.045488286686" />
                  <Point X="28.945269551918" Y="0.489218111887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.531186490133" Y="-0.398785879778" />
                  <Point X="28.284458264122" Y="-0.927896267942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.934491588199" Y="-1.678402226408" />
                  <Point X="27.754954823444" Y="-2.06342006091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.239726003335" Y="-3.16833183128" />
                  <Point X="27.170999127834" Y="-3.315717091416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.316339407496" Y="1.060190834768" />
                  <Point X="29.065057600678" Y="0.521315261048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.613143708941" Y="-0.447817207257" />
                  <Point X="28.398312753731" Y="-0.908523677444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.995246516956" Y="-1.772902011633" />
                  <Point X="27.845839163882" Y="-2.093307114275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.297712722201" Y="-3.268768061774" />
                  <Point X="27.236198212038" Y="-3.400686354531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.428016220563" Y="1.074893382849" />
                  <Point X="29.184845649439" Y="0.553412410209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.700117635943" Y="-0.486090169296" />
                  <Point X="28.498975248568" Y="-0.917441411031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.065846314861" Y="-1.846289406838" />
                  <Point X="27.928479686967" Y="-2.140873091003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.355699441066" Y="-3.369204292269" />
                  <Point X="27.301397296241" Y="-3.485655617645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.539693033629" Y="1.08959593093" />
                  <Point X="29.304633698199" Y="0.58550955937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.793296190033" Y="-0.511057265608" />
                  <Point X="28.597733329921" Y="-0.930443172512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.143044777778" Y="-1.905525919259" />
                  <Point X="28.011066316458" Y="-2.188554642918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.413686159932" Y="-3.469640522764" />
                  <Point X="27.366596380445" Y="-3.570624880759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.651369846695" Y="1.104298479011" />
                  <Point X="29.424421746959" Y="0.617606708532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.886474744122" Y="-0.536024361919" />
                  <Point X="28.696491423411" Y="-0.943444907965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.220243240695" Y="-1.96476243168" />
                  <Point X="28.093652994656" Y="-2.236236090381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.471672878797" Y="-3.570076753259" />
                  <Point X="27.431795464648" Y="-3.655594143874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.723486706265" Y="1.034164433045" />
                  <Point X="29.544209795719" Y="0.649703857693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.979653298212" Y="-0.560991458231" />
                  <Point X="28.795249516902" Y="-0.956446643418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.297441703613" Y="-2.0239989441" />
                  <Point X="28.176239672853" Y="-2.283917537844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.529659597663" Y="-3.670512983753" />
                  <Point X="27.496994548851" Y="-3.740563406988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.757616214106" Y="0.882566248405" />
                  <Point X="29.66399784448" Y="0.681801006854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.072831852301" Y="-0.585958554542" />
                  <Point X="28.894007610392" Y="-0.969448378871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.37464016653" Y="-2.083235456521" />
                  <Point X="28.258826351051" Y="-2.331598985307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.587646316528" Y="-3.770949214248" />
                  <Point X="27.562193633055" Y="-3.825532670102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.783853957197" Y="0.714044119642" />
                  <Point X="29.78378589324" Y="0.713898156015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.166010406391" Y="-0.610925650854" />
                  <Point X="28.992765703883" Y="-0.982450114324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.451838629447" Y="-2.142471968942" />
                  <Point X="28.341413029248" Y="-2.37928043277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.645633035394" Y="-3.871385444743" />
                  <Point X="27.627392717258" Y="-3.910501933217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.25918896048" Y="-0.635892747165" />
                  <Point X="29.091523797373" Y="-0.995451849777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.529037092364" Y="-2.201708481363" />
                  <Point X="28.423999707446" Y="-2.426961880233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.703619754259" Y="-3.971821675238" />
                  <Point X="27.692591801462" Y="-3.995471196331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.35236751457" Y="-0.660859843477" />
                  <Point X="29.190281890864" Y="-1.008453585229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.606235555282" Y="-2.260944993784" />
                  <Point X="28.506586385643" Y="-2.474643327696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.445546068659" Y="-0.685826939788" />
                  <Point X="29.289039984354" Y="-1.021455320682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.683434018199" Y="-2.320181506204" />
                  <Point X="28.589173063841" Y="-2.522324775159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.538724622749" Y="-0.7107940361" />
                  <Point X="29.387798077845" Y="-1.034457056135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.760632481116" Y="-2.379418018625" />
                  <Point X="28.671759742038" Y="-2.570006222622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.631903176838" Y="-0.735761132411" />
                  <Point X="29.486556171335" Y="-1.047458791588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.837830944033" Y="-2.438654531046" />
                  <Point X="28.754346420236" Y="-2.617687670085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.725081730928" Y="-0.760728228722" />
                  <Point X="29.585314264826" Y="-1.060460527041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.91502940695" Y="-2.497891043467" />
                  <Point X="28.836933098434" Y="-2.665369117548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.766939559019" Y="-0.895752977102" />
                  <Point X="29.684072358316" Y="-1.073462262494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.992227869868" Y="-2.557127555888" />
                  <Point X="28.919519776631" Y="-2.713050565011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.069426332785" Y="-2.616364068308" />
                  <Point X="29.004401303394" Y="-2.755810693844" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.51116796875" Y="-3.685004638672" />
                  <Point X="25.471541015625" Y="-3.537112548828" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.448123046875" Y="-3.498207519531" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.2492734375" Y="-3.25862109375" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.9662734375" Y="-3.195545410156" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.695525390625" Y="-3.308980224609" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537111816406" />
                  <Point X="24.1798203125" Y="-4.884209472656" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.871962890625" Y="-4.930914550781" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.686080078125" Y="-4.5872734375" />
                  <Point X="23.6908515625" Y="-4.551040039062" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.684330078125" Y="-4.504655273438" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.590642578125" Y="-4.182392089844" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.3201328125" Y="-3.983755615234" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.9846015625" Y="-3.990842773438" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.5378984375" Y="-4.411399414062" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="22.1056796875" Y="-4.137977539062" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.42032421875" Y="-2.756673339844" />
                  <Point X="22.493966796875" Y="-2.629119384766" />
                  <Point X="22.500236328125" Y="-2.597589599609" />
                  <Point X="22.484306640625" Y="-2.567049560547" />
                  <Point X="22.46867578125" Y="-2.551417236328" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.24647265625" Y="-3.214261474609" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.149365234375" Y="-3.255809082031" />
                  <Point X="20.838302734375" Y="-2.847136962891" />
                  <Point X="20.81070703125" Y="-2.80086328125" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.707361328125" Y="-1.522016479492" />
                  <Point X="21.836212890625" Y="-1.423144897461" />
                  <Point X="21.85494140625" Y="-1.393069091797" />
                  <Point X="21.8618828125" Y="-1.366264526367" />
                  <Point X="21.85967578125" Y="-1.334597167969" />
                  <Point X="21.83622265625" Y="-1.309097167969" />
                  <Point X="21.812359375" Y="-1.295052490234" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.31375390625" Y="-1.481668334961" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.19426953125" Y="-1.48748815918" />
                  <Point X="20.072607421875" Y="-1.01118737793" />
                  <Point X="20.06530859375" Y="-0.960152709961" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="21.295755859375" Y="-0.167975448608" />
                  <Point X="21.442537109375" Y="-0.128645339966" />
                  <Point X="21.460849609375" Y="-0.119519729614" />
                  <Point X="21.485857421875" Y="-0.102163116455" />
                  <Point X="21.506021484375" Y="-0.072942703247" />
                  <Point X="21.5143515625" Y="-0.046100971222" />
                  <Point X="21.5134375" Y="-0.013510365486" />
                  <Point X="21.5051015625" Y="0.0133478899" />
                  <Point X="21.483107421875" Y="0.041511356354" />
                  <Point X="21.458103515625" Y="0.058865184784" />
                  <Point X="21.442537109375" Y="0.066085227966" />
                  <Point X="20.105552734375" Y="0.424329223633" />
                  <Point X="20.001814453125" Y="0.452125823975" />
                  <Point X="20.003947265625" Y="0.466537689209" />
                  <Point X="20.08235546875" Y="0.996414916992" />
                  <Point X="20.09705078125" Y="1.050649414062" />
                  <Point X="20.226484375" Y="1.528298706055" />
                  <Point X="21.14151953125" Y="1.40783215332" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866699219" />
                  <Point X="21.274373046875" Y="1.397782592773" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.34846875" Y="1.426057983398" />
                  <Point X="21.3608828125" Y="1.443791015625" />
                  <Point X="21.363318359375" Y="1.449672485352" />
                  <Point X="21.38552734375" Y="1.503290283203" />
                  <Point X="21.389287109375" Y="1.524603149414" />
                  <Point X="21.3836875" Y="1.5455078125" />
                  <Point X="21.3807421875" Y="1.5511640625" />
                  <Point X="21.353943359375" Y="1.602642211914" />
                  <Point X="21.34003125" Y="1.619221923828" />
                  <Point X="20.573134765625" Y="2.207682861328" />
                  <Point X="20.52389453125" Y="2.245466308594" />
                  <Point X="20.53531640625" Y="2.26503515625" />
                  <Point X="20.83998828125" Y="2.787007568359" />
                  <Point X="20.878912109375" Y="2.837040527344" />
                  <Point X="21.225330078125" Y="3.282310791016" />
                  <Point X="21.77622265625" Y="2.964252441406" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.86994921875" Y="2.919694335938" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404541016" />
                  <Point X="21.992755859375" Y="2.933411621094" />
                  <Point X="22.04747265625" Y="2.988127929688" />
                  <Point X="22.0591015625" Y="3.006382324219" />
                  <Point X="22.061927734375" Y="3.027841064453" />
                  <Point X="22.0611875" Y="3.036304199219" />
                  <Point X="22.054443359375" Y="3.113390625" />
                  <Point X="22.04793359375" Y="3.134032714844" />
                  <Point X="21.70809765625" Y="3.722644775391" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="21.716486328125" Y="3.767497070312" />
                  <Point X="22.247123046875" Y="4.174331054688" />
                  <Point X="22.308431640625" Y="4.208392578125" />
                  <Point X="22.858451171875" Y="4.513971679688" />
                  <Point X="23.012390625" Y="4.313354003906" />
                  <Point X="23.032173828125" Y="4.287572753906" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.058173828125" Y="4.268757324219" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.19600390625" Y="4.226314453125" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744628906" />
                  <Point X="23.31391796875" Y="4.294488769531" />
                  <Point X="23.317111328125" Y="4.304616699219" />
                  <Point X="23.346197265625" Y="4.396865234375" />
                  <Point X="23.348083984375" Y="4.418424804688" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.34496484375" Y="4.710701660156" />
                  <Point X="24.03190625" Y="4.903296386719" />
                  <Point X="24.1062265625" Y="4.911994628906" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.938744140625" Y="4.382248046875" />
                  <Point X="24.957859375" Y="4.310904296875" />
                  <Point X="24.97571875" Y="4.284177246094" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176269531" />
                  <Point X="25.054453125" Y="4.310902832031" />
                  <Point X="25.22858203125" Y="4.960756835938" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.26040625" Y="4.988380859375" />
                  <Point X="25.860205078125" Y="4.925565429688" />
                  <Point X="25.9216953125" Y="4.910720214844" />
                  <Point X="26.508455078125" Y="4.769058105469" />
                  <Point X="26.548115234375" Y="4.754673339844" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="26.969736328125" Y="4.597686035156" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.376095703125" Y="4.40334765625" />
                  <Point X="27.73252734375" Y="4.195689941406" />
                  <Point X="27.767787109375" Y="4.170614746094" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.315091796875" Y="2.651232177734" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.222728515625" Y="2.483572265625" />
                  <Point X="27.2033828125" Y="2.411228271484" />
                  <Point X="27.202873046875" Y="2.385458251953" />
                  <Point X="27.210416015625" Y="2.322901123047" />
                  <Point X="27.218681640625" Y="2.300812011719" />
                  <Point X="27.222931640625" Y="2.294549316406" />
                  <Point X="27.261640625" Y="2.237503417969" />
                  <Point X="27.281203125" Y="2.219953613281" />
                  <Point X="27.33825" Y="2.181245849609" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.367205078125" Y="2.172151855469" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.456607421875" Y="2.1680703125" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.885783203125" Y="2.96880859375" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.20259765625" Y="2.741874511719" />
                  <Point X="29.222251953125" Y="2.709394042969" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.40083984375" Y="1.679193725586" />
                  <Point X="28.2886171875" Y="1.593082763672" />
                  <Point X="28.27365625" Y="1.576376220703" />
                  <Point X="28.22158984375" Y="1.508452148438" />
                  <Point X="28.2109921875" Y="1.483887451172" />
                  <Point X="28.191595703125" Y="1.414536743164" />
                  <Point X="28.190779296875" Y="1.39096484375" />
                  <Point X="28.19252734375" Y="1.382493530273" />
                  <Point X="28.20844921875" Y="1.30533203125" />
                  <Point X="28.220400390625" Y="1.280729125977" />
                  <Point X="28.263703125" Y="1.214909790039" />
                  <Point X="28.280947265625" Y="1.198820068359" />
                  <Point X="28.287837890625" Y="1.194942016602" />
                  <Point X="28.35058984375" Y="1.159617675781" />
                  <Point X="28.377880859375" Y="1.152388549805" />
                  <Point X="28.462728515625" Y="1.141174926758" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.753224609375" Y="1.30934753418" />
                  <Point X="29.8489765625" Y="1.321953369141" />
                  <Point X="29.939193359375" Y="0.951367370605" />
                  <Point X="29.945384765625" Y="0.911595458984" />
                  <Point X="29.997859375" Y="0.57455645752" />
                  <Point X="28.86977734375" Y="0.272287506104" />
                  <Point X="28.74116796875" Y="0.237826919556" />
                  <Point X="28.7199375" Y="0.227529724121" />
                  <Point X="28.636578125" Y="0.179347045898" />
                  <Point X="28.616775390625" Y="0.159930160522" />
                  <Point X="28.566759765625" Y="0.096199226379" />
                  <Point X="28.556986328125" Y="0.074735168457" />
                  <Point X="28.55515625" Y="0.065178009033" />
                  <Point X="28.538484375" Y="-0.021875238419" />
                  <Point X="28.540314453125" Y="-0.050242031097" />
                  <Point X="28.556986328125" Y="-0.137295272827" />
                  <Point X="28.566759765625" Y="-0.158759185791" />
                  <Point X="28.57225" Y="-0.16575592041" />
                  <Point X="28.622265625" Y="-0.229487014771" />
                  <Point X="28.64573046875" Y="-0.247196685791" />
                  <Point X="28.729087890625" Y="-0.295379364014" />
                  <Point X="28.74116796875" Y="-0.300387023926" />
                  <Point X="29.91262109375" Y="-0.614276977539" />
                  <Point X="29.998068359375" Y="-0.637172363281" />
                  <Point X="29.948431640625" Y="-0.966412475586" />
                  <Point X="29.94049609375" Y="-1.001180419922" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.5611328125" Y="-1.117264160156" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.376876953125" Y="-1.102245117188" />
                  <Point X="28.2132734375" Y="-1.13780480957" />
                  <Point X="28.197947265625" Y="-1.143923217773" />
                  <Point X="28.174587890625" Y="-1.167754394531" />
                  <Point X="28.075701171875" Y="-1.286685424805" />
                  <Point X="28.067955078125" Y="-1.299518310547" />
                  <Point X="28.062802734375" Y="-1.330980224609" />
                  <Point X="28.048630859375" Y="-1.485001220703" />
                  <Point X="28.0498515625" Y="-1.501458496094" />
                  <Point X="28.06630078125" Y="-1.532083007812" />
                  <Point X="28.156841796875" Y="-1.672912597656" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="29.255578125" Y="-2.519714355469" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.2041328125" Y="-2.802140380859" />
                  <Point X="29.187716796875" Y="-2.825465332031" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="27.88629296875" Y="-2.335909912109" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.71596484375" Y="-2.249452148438" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.471318359375" Y="-2.228591064453" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.33468359375" />
                  <Point X="27.27925390625" Y="-2.352442382812" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.1891640625" Y="-2.546374755859" />
                  <Point X="27.193025390625" Y="-2.567751464844" />
                  <Point X="27.22819140625" Y="-2.762465087891" />
                  <Point X="27.23409375" Y="-2.778578857422" />
                  <Point X="27.932673828125" Y="-3.988556884766" />
                  <Point X="27.986673828125" Y="-4.082087890625" />
                  <Point X="27.835294921875" Y="-4.19021484375" />
                  <Point X="27.816947265625" Y="-4.202090820312" />
                  <Point X="27.679775390625" Y="-4.29087890625" />
                  <Point X="26.784546875" Y="-3.124192626953" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.649466796875" Y="-2.966912597656" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.402748046875" Y="-2.837838867188" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.147529296875" Y="-2.883313232422" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045985839844" />
                  <Point X="25.963134765625" Y="-3.070478515625" />
                  <Point X="25.914642578125" Y="-3.293573730469" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="26.11190234375" Y="-4.814471191406" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="25.99435546875" Y="-4.963245605469" />
                  <Point X="25.977388671875" Y="-4.966327636719" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#126" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.019116756578" Y="4.426469184036" Z="0.25" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.25" />
                  <Point X="-0.905741999842" Y="4.992935613568" Z="0.25" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.25" />
                  <Point X="-1.674690981367" Y="4.790125262255" Z="0.25" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.25" />
                  <Point X="-1.746281347703" Y="4.736646270378" Z="0.25" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.25" />
                  <Point X="-1.736731914597" Y="4.350931711748" Z="0.25" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.25" />
                  <Point X="-1.829288251499" Y="4.303788378637" Z="0.25" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.25" />
                  <Point X="-1.924895927598" Y="4.344387924461" Z="0.25" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.25" />
                  <Point X="-1.954097725343" Y="4.375072419486" Z="0.25" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.25" />
                  <Point X="-2.72200787849" Y="4.283379987571" Z="0.25" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.25" />
                  <Point X="-3.320091864116" Y="3.83845708633" Z="0.25" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.25" />
                  <Point X="-3.34136016004" Y="3.728925181891" Z="0.25" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.25" />
                  <Point X="-2.994780435264" Y="3.063226542393" Z="0.25" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.25" />
                  <Point X="-3.048756466976" Y="3.000047100911" Z="0.25" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.25" />
                  <Point X="-3.131849923214" Y="3.000784263909" Z="0.25" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.25" />
                  <Point X="-3.204934166406" Y="3.038833788706" Z="0.25" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.25" />
                  <Point X="-4.166707661684" Y="2.899023067301" Z="0.25" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.25" />
                  <Point X="-4.514021692094" Y="2.321520262217" Z="0.25" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.25" />
                  <Point X="-4.4634597207" Y="2.199295151102" Z="0.25" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.25" />
                  <Point X="-3.669764454696" Y="1.55935595192" Z="0.25" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.25" />
                  <Point X="-3.689031651478" Y="1.500086582201" Z="0.25" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.25" />
                  <Point X="-3.746819475151" Y="1.476748476052" Z="0.25" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.25" />
                  <Point X="-3.858112894101" Y="1.488684596593" Z="0.25" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.25" />
                  <Point X="-4.957365313686" Y="1.095006824121" Z="0.25" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.25" />
                  <Point X="-5.051671466152" Y="0.505136655274" Z="0.25" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.25" />
                  <Point X="-4.913545175851" Y="0.407312938614" Z="0.25" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.25" />
                  <Point X="-3.551553318211" Y="0.031712456245" Z="0.25" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.25" />
                  <Point X="-3.540471910368" Y="0.002948658545" Z="0.25" />
                  <Point X="-3.539556741714" Y="0" Z="0.25" />
                  <Point X="-3.547892690767" Y="-0.026858292516" Z="0.25" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.25" />
                  <Point X="-3.573815276912" Y="-0.047163526889" Z="0.25" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.25" />
                  <Point X="-3.723342612921" Y="-0.088399118142" Z="0.25" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.25" />
                  <Point X="-4.990344782163" Y="-0.935951681418" Z="0.25" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.25" />
                  <Point X="-4.860333250768" Y="-1.468582225236" Z="0.25" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.25" />
                  <Point X="-4.685878382942" Y="-1.499960555699" Z="0.25" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.25" />
                  <Point X="-3.195296100457" Y="-1.320907906204" Z="0.25" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.25" />
                  <Point X="-3.199618633322" Y="-1.349254406087" Z="0.25" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.25" />
                  <Point X="-3.329232831526" Y="-1.451068805677" Z="0.25" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.25" />
                  <Point X="-4.238393946334" Y="-2.795193004879" Z="0.25" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.25" />
                  <Point X="-3.896577402472" Y="-3.254812298797" Z="0.25" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.25" />
                  <Point X="-3.734684794042" Y="-3.226282655422" Z="0.25" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.25" />
                  <Point X="-2.557207177176" Y="-2.571123291956" Z="0.25" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.25" />
                  <Point X="-2.629134357747" Y="-2.700393588451" Z="0.25" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.25" />
                  <Point X="-2.930980486536" Y="-4.146314673662" Z="0.25" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.25" />
                  <Point X="-2.494581307287" Y="-4.422630015827" Z="0.25" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.25" />
                  <Point X="-2.428869924288" Y="-4.420547642742" Z="0.25" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.25" />
                  <Point X="-1.993775362114" Y="-4.001135887661" Z="0.25" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.25" />
                  <Point X="-1.68929301696" Y="-4.002368587326" Z="0.25" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.25" />
                  <Point X="-1.448481356982" Y="-4.188703915187" Z="0.25" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.25" />
                  <Point X="-1.370866224661" Y="-4.483130111879" Z="0.25" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.25" />
                  <Point X="-1.369648759305" Y="-4.549465664335" Z="0.25" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.25" />
                  <Point X="-1.146653743667" Y="-4.948057311419" Z="0.25" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.25" />
                  <Point X="-0.847319301783" Y="-5.00800754434" Z="0.25" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.25" />
                  <Point X="-0.778040495755" Y="-4.865870781312" Z="0.25" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.25" />
                  <Point X="-0.269555874626" Y="-3.30620917091" Z="0.25" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.25" />
                  <Point X="-0.025063597175" Y="-3.212018184068" Z="0.25" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.25" />
                  <Point X="0.228295482187" Y="-3.27509386122" Z="0.25" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.25" />
                  <Point X="0.400889984807" Y="-3.495436584858" Z="0.25" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.25" />
                  <Point X="0.456714364646" Y="-3.666665248503" Z="0.25" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.25" />
                  <Point X="0.980169782685" Y="-4.984242322291" Z="0.25" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.25" />
                  <Point X="1.159341128603" Y="-4.945637721276" Z="0.25" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.25" />
                  <Point X="1.155318397258" Y="-4.776664808499" Z="0.25" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.25" />
                  <Point X="1.005836432452" Y="-3.049818673994" Z="0.25" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.25" />
                  <Point X="1.173338824455" Y="-2.890479652521" Z="0.25" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.25" />
                  <Point X="1.401172307812" Y="-2.856348421006" Z="0.25" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.25" />
                  <Point X="1.616270618451" Y="-2.977690669625" Z="0.25" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.25" />
                  <Point X="1.738721832971" Y="-3.123350508847" Z="0.25" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.25" />
                  <Point X="2.837960718078" Y="-4.212784846692" Z="0.25" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.25" />
                  <Point X="3.027792095669" Y="-4.078486598001" Z="0.25" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.25" />
                  <Point X="2.969818300661" Y="-3.932276600952" Z="0.25" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.25" />
                  <Point X="2.236071746353" Y="-2.527584971089" Z="0.25" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.25" />
                  <Point X="2.317343834867" Y="-2.3444490492" Z="0.25" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.25" />
                  <Point X="2.488449329172" Y="-2.2415574751" Z="0.25" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.25" />
                  <Point X="2.700921832423" Y="-2.267376263138" Z="0.25" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.25" />
                  <Point X="2.855136996155" Y="-2.347931199613" Z="0.25" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.25" />
                  <Point X="4.222449192394" Y="-2.8229623523" Z="0.25" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.25" />
                  <Point X="4.383431332441" Y="-2.565876769061" Z="0.25" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.25" />
                  <Point X="4.279858684945" Y="-2.448766479465" Z="0.25" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.25" />
                  <Point X="3.102202541878" Y="-1.473763569655" Z="0.25" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.25" />
                  <Point X="3.106435467438" Y="-1.304281541985" Z="0.25" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.25" />
                  <Point X="3.206879074862" Y="-1.16844112681" Z="0.25" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.25" />
                  <Point X="3.381338572542" Y="-1.119824384595" Z="0.25" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.25" />
                  <Point X="3.548450062182" Y="-1.135556419612" Z="0.25" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.25" />
                  <Point X="4.983085971276" Y="-0.981024214964" Z="0.25" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.25" />
                  <Point X="5.042418253526" Y="-0.606284112777" Z="0.25" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.25" />
                  <Point X="4.919406185107" Y="-0.53470063786" Z="0.25" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.25" />
                  <Point X="3.664594283302" Y="-0.172627978059" Z="0.25" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.25" />
                  <Point X="3.605427664875" Y="-0.103607280602" Z="0.25" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.25" />
                  <Point X="3.583265040437" Y="-0.009557215996" Z="0.25" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.25" />
                  <Point X="3.598106409987" Y="0.087053315246" Z="0.25" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.25" />
                  <Point X="3.649951773525" Y="0.160341457973" Z="0.25" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.25" />
                  <Point X="3.738801131051" Y="0.215520866183" Z="0.25" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.25" />
                  <Point X="3.8765616322" Y="0.255271294909" Z="0.25" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.25" />
                  <Point X="4.988632276004" Y="0.950567594062" Z="0.25" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.25" />
                  <Point X="4.890809300289" Y="1.367488253132" Z="0.25" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.25" />
                  <Point X="4.740542767826" Y="1.390199846582" Z="0.25" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.25" />
                  <Point X="3.378275494314" Y="1.233237505042" Z="0.25" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.25" />
                  <Point X="3.306207907303" Y="1.269792920688" Z="0.25" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.25" />
                  <Point X="3.256015099138" Y="1.339490375954" Z="0.25" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.25" />
                  <Point X="3.235339939004" Y="1.423877963189" Z="0.25" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.25" />
                  <Point X="3.252986724602" Y="1.501699974397" Z="0.25" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.25" />
                  <Point X="3.307182046088" Y="1.577237981017" Z="0.25" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.25" />
                  <Point X="3.425120261183" Y="1.670806145453" Z="0.25" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.25" />
                  <Point X="4.258871994499" Y="2.76656022489" Z="0.25" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.25" />
                  <Point X="4.025599872912" Y="3.096191071064" Z="0.25" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.25" />
                  <Point X="3.854626853641" Y="3.043389882309" Z="0.25" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.25" />
                  <Point X="2.437533990362" Y="2.247653058555" Z="0.25" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.25" />
                  <Point X="2.3670346569" Y="2.253072390184" Z="0.25" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.25" />
                  <Point X="2.303120899549" Y="2.292608488689" Z="0.25" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.25" />
                  <Point X="2.258150111546" Y="2.353903960833" Z="0.25" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.25" />
                  <Point X="2.246357312575" Y="2.422723786932" Z="0.25" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.25" />
                  <Point X="2.264874884102" Y="2.50193558552" Z="0.25" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.25" />
                  <Point X="2.35223547055" Y="2.657512228314" Z="0.25" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.25" />
                  <Point X="2.790607524574" Y="4.242641643271" Z="0.25" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.25" />
                  <Point X="2.395108745624" Y="4.477830317138" Z="0.25" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.25" />
                  <Point X="1.984761857027" Y="4.674258098072" Z="0.25" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.25" />
                  <Point X="1.559007690187" Y="4.832957331747" Z="0.25" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.25" />
                  <Point X="0.927273442038" Y="4.990603827457" Z="0.25" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.25" />
                  <Point X="0.259456591772" Y="5.069388864444" Z="0.25" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.25" />
                  <Point X="0.174127804975" Y="5.004978321915" Z="0.25" />
                  <Point X="0" Y="4.355124473572" Z="0.25" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>