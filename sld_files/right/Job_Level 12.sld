<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#139" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1007" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.78389453125" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.650244140625" Y="-3.836989990234" />
                  <Point X="25.5633046875" Y="-3.512524414063" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467376708984" />
                  <Point X="25.506830078125" Y="-3.416177978516" />
                  <Point X="25.37863671875" Y="-3.231476318359" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.2475078125" Y="-3.158603027344" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097036132812" />
                  <Point X="24.908189453125" Y="-3.114102294922" />
                  <Point X="24.70981640625" Y="-3.175669433594" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231478271484" />
                  <Point X="24.598142578125" Y="-3.282677001953" />
                  <Point X="24.46994921875" Y="-3.467378662109" />
                  <Point X="24.4618125" Y="-3.481571533203" />
                  <Point X="24.449009765625" Y="-3.512524169922" />
                  <Point X="24.135369140625" Y="-4.683048339844" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.920658203125" Y="-4.845349609375" />
                  <Point X="23.860708984375" Y="-4.829925292969" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.774572265625" Y="-4.642932128906" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.516223632812" />
                  <Point X="23.77035546875" Y="-4.450181640625" />
                  <Point X="23.722962890625" Y="-4.211932128906" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131205078125" />
                  <Point X="23.62573046875" Y="-4.086806884766" />
                  <Point X="23.443095703125" Y="-3.926639892578" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.28978125" Y="-3.886562255859" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894802001953" />
                  <Point X="22.90135546875" Y="-3.932211914062" />
                  <Point X="22.699376953125" Y="-4.067170166016" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.5198515625" Y="-4.288488769531" />
                  <Point X="22.19828515625" Y="-4.089383544922" />
                  <Point X="22.11527734375" Y="-4.025469238281" />
                  <Point X="21.89527734375" Y="-3.856077392578" />
                  <Point X="22.414669921875" Y="-2.95646484375" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647655273438" />
                  <Point X="22.593412109375" Y="-2.616130126953" />
                  <Point X="22.59442578125" Y="-2.585197998047" />
                  <Point X="22.585443359375" Y="-2.555581542969" />
                  <Point X="22.571228515625" Y="-2.526752929688" />
                  <Point X="22.553201171875" Y="-2.501592529297" />
                  <Point X="22.535853515625" Y="-2.484243896484" />
                  <Point X="22.5106953125" Y="-2.466215576172" />
                  <Point X="22.481865234375" Y="-2.451997070313" />
                  <Point X="22.452248046875" Y="-2.44301171875" />
                  <Point X="22.4213125" Y="-2.444023925781" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.351263671875" Y="-3.044063476562" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="20.917140625" Y="-2.793861816406" />
                  <Point X="20.857626953125" Y="-2.694067382812" />
                  <Point X="20.693857421875" Y="-2.419450195312" />
                  <Point X="21.611353515625" Y="-1.715431518555" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.9169296875" Y="-1.475887207031" />
                  <Point X="21.935974609375" Y="-1.445587524414" />
                  <Point X="21.9443671875" Y="-1.424056030273" />
                  <Point X="21.9478203125" Y="-1.413369750977" />
                  <Point X="21.9538515625" Y="-1.390078735352" />
                  <Point X="21.95796484375" Y="-1.358597290039" />
                  <Point X="21.957263671875" Y="-1.335728759766" />
                  <Point X="21.9511171875" Y="-1.313690673828" />
                  <Point X="21.939111328125" Y="-1.284709716797" />
                  <Point X="21.926517578125" Y="-1.262984130859" />
                  <Point X="21.908673828125" Y="-1.245316772461" />
                  <Point X="21.890423828125" Y="-1.231505004883" />
                  <Point X="21.88128125" Y="-1.225385131836" />
                  <Point X="21.860546875" Y="-1.213181396484" />
                  <Point X="21.831283203125" Y="-1.20195703125" />
                  <Point X="21.799396484375" Y="-1.195475097656" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="20.49360546875" Y="-1.362170410156" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.165921875" Y="-0.992648803711" />
                  <Point X="20.150177734375" Y="-0.882564880371" />
                  <Point X="20.107578125" Y="-0.584698425293" />
                  <Point X="21.145095703125" Y="-0.306695983887" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.486896484375" Y="-0.212652740479" />
                  <Point X="21.508486328125" Y="-0.201252365112" />
                  <Point X="21.51829296875" Y="-0.195290344238" />
                  <Point X="21.5400234375" Y="-0.180208755493" />
                  <Point X="21.563978515625" Y="-0.158682907104" />
                  <Point X="21.584478515625" Y="-0.129254547119" />
                  <Point X="21.593912109375" Y="-0.108046394348" />
                  <Point X="21.59784375" Y="-0.097592590332" />
                  <Point X="21.6050859375" Y="-0.074254814148" />
                  <Point X="21.61052734375" Y="-0.045516647339" />
                  <Point X="21.6097578125" Y="-0.012664651871" />
                  <Point X="21.60550390625" Y="0.008623494148" />
                  <Point X="21.603078125" Y="0.018163824081" />
                  <Point X="21.5958359375" Y="0.041501747131" />
                  <Point X="21.582517578125" Y="0.070829170227" />
                  <Point X="21.5604296875" Y="0.099409172058" />
                  <Point X="21.5425859375" Y="0.115108398438" />
                  <Point X="21.534" Y="0.121829376221" />
                  <Point X="21.51226953125" Y="0.136910964966" />
                  <Point X="21.49807421875" Y="0.14504737854" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="20.305388671875" Y="0.46913458252" />
                  <Point X="20.108185546875" Y="0.521975341797" />
                  <Point X="20.1082578125" Y="0.522468994141" />
                  <Point X="20.17551171875" Y="0.976969909668" />
                  <Point X="20.207208984375" Y="1.093943725586" />
                  <Point X="20.29644921875" Y="1.423268066406" />
                  <Point X="21.00349609375" Y="1.33018347168" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228027344" />
                  <Point X="21.2968671875" Y="1.305264282227" />
                  <Point X="21.310197265625" Y="1.309467773438" />
                  <Point X="21.35829296875" Y="1.324631713867" />
                  <Point X="21.377224609375" Y="1.332962402344" />
                  <Point X="21.39596875" Y="1.343784790039" />
                  <Point X="21.412646484375" Y="1.356014282227" />
                  <Point X="21.426283203125" Y="1.371563354492" />
                  <Point X="21.43869921875" Y="1.389293701172" />
                  <Point X="21.448650390625" Y="1.407430786133" />
                  <Point X="21.454" Y="1.420345458984" />
                  <Point X="21.473296875" Y="1.466935180664" />
                  <Point X="21.4790859375" Y="1.486795166016" />
                  <Point X="21.48284375" Y="1.508107666016" />
                  <Point X="21.484197265625" Y="1.528744262695" />
                  <Point X="21.481052734375" Y="1.549184692383" />
                  <Point X="21.475453125" Y="1.570088745117" />
                  <Point X="21.467955078125" Y="1.58937097168" />
                  <Point X="21.461498046875" Y="1.601775878906" />
                  <Point X="21.4382109375" Y="1.646506225586" />
                  <Point X="21.426716796875" Y="1.663709350586" />
                  <Point X="21.4128046875" Y="1.680288330078" />
                  <Point X="21.39786328125" Y="1.694590454102" />
                  <Point X="20.731490234375" Y="2.205917480469" />
                  <Point X="20.648140625" Y="2.269873046875" />
                  <Point X="20.65751953125" Y="2.285940185547" />
                  <Point X="20.91885546875" Y="2.733670898438" />
                  <Point X="21.0028046875" Y="2.841577636719" />
                  <Point X="21.24949609375" Y="3.158662597656" />
                  <Point X="21.651568359375" Y="2.926524902344" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.871775390625" Y="2.824171875" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.019537109375" Y="2.835653076172" />
                  <Point X="22.037791015625" Y="2.847281982422" />
                  <Point X="22.053921875" Y="2.860228027344" />
                  <Point X="22.0671015625" Y="2.873407226562" />
                  <Point X="22.114646484375" Y="2.920951416016" />
                  <Point X="22.127595703125" Y="2.937084960938" />
                  <Point X="22.139224609375" Y="2.955339111328" />
                  <Point X="22.14837109375" Y="2.973888183594" />
                  <Point X="22.1532890625" Y="2.993977294922" />
                  <Point X="22.156115234375" Y="3.015436035156" />
                  <Point X="22.15656640625" Y="3.036123291016" />
                  <Point X="22.15494140625" Y="3.054690673828" />
                  <Point X="22.14908203125" Y="3.121672851562" />
                  <Point X="22.145044921875" Y="3.141961669922" />
                  <Point X="22.13853515625" Y="3.162604492188" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.8349140625" Y="3.692991455078" />
                  <Point X="21.81666796875" Y="3.724596435547" />
                  <Point X="21.8442265625" Y="3.745726074219" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.43160546875" Y="4.1681484375" />
                  <Point X="22.832962890625" Y="4.391133789062" />
                  <Point X="22.91340234375" Y="4.286302246094" />
                  <Point X="22.9568046875" Y="4.229740234375" />
                  <Point X="22.971109375" Y="4.214797851563" />
                  <Point X="22.987689453125" Y="4.200885742188" />
                  <Point X="23.004888671875" Y="4.189394042969" />
                  <Point X="23.0255546875" Y="4.178636230469" />
                  <Point X="23.10010546875" Y="4.139827148438" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.22255078125" Y="4.134482910156" />
                  <Point X="23.24407421875" Y="4.143398925781" />
                  <Point X="23.321724609375" Y="4.1755625" />
                  <Point X="23.33985546875" Y="4.185509765625" />
                  <Point X="23.3575859375" Y="4.197924316406" />
                  <Point X="23.373140625" Y="4.211565429688" />
                  <Point X="23.385373046875" Y="4.22825" />
                  <Point X="23.396193359375" Y="4.246994140625" />
                  <Point X="23.404521484375" Y="4.265920898438" />
                  <Point X="23.41152734375" Y="4.288140136719" />
                  <Point X="23.43680078125" Y="4.368297363281" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410144042969" />
                  <Point X="23.442271484375" Y="4.430826171875" />
                  <Point X="23.415798828125" Y="4.6318984375" />
                  <Point X="23.4611484375" Y="4.644612792969" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.210662109375" Y="4.828568847656" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.82415625" Y="4.442841308594" />
                  <Point X="24.866095703125" Y="4.286315917969" />
                  <Point X="24.87887109375" Y="4.258122558594" />
                  <Point X="24.89673046875" Y="4.231395507813" />
                  <Point X="24.91788671875" Y="4.208808105469" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.297521484375" Y="4.850988769531" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.329564453125" Y="4.885618652344" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="25.976666015625" Y="4.799719238281" />
                  <Point X="26.4810234375" Y="4.677951660156" />
                  <Point X="26.56619140625" Y="4.647061035156" />
                  <Point X="26.894650390625" Y="4.527926757812" />
                  <Point X="26.978123046875" Y="4.488889160156" />
                  <Point X="27.294572265625" Y="4.340895996094" />
                  <Point X="27.37525" Y="4.293893554688" />
                  <Point X="27.680982421875" Y="4.1157734375" />
                  <Point X="27.757037109375" Y="4.061686523438" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.334587890625" Y="2.874998046875" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.139572265625" Y="2.533160400391" />
                  <Point X="27.12991015625" Y="2.503652587891" />
                  <Point X="27.12841796875" Y="2.4986328125" />
                  <Point X="27.111607421875" Y="2.435771484375" />
                  <Point X="27.108392578125" Y="2.409885498047" />
                  <Point X="27.108873046875" Y="2.37591015625" />
                  <Point X="27.109546875" Y="2.365882324219" />
                  <Point X="27.116099609375" Y="2.311531494141" />
                  <Point X="27.12144140625" Y="2.289610351562" />
                  <Point X="27.12970703125" Y="2.267520019531" />
                  <Point X="27.140072265625" Y="2.247467773438" />
                  <Point X="27.149396484375" Y="2.233727539062" />
                  <Point X="27.18303125" Y="2.184159179688" />
                  <Point X="27.200681640625" Y="2.164641113281" />
                  <Point X="27.22772265625" Y="2.142017578125" />
                  <Point X="27.23533984375" Y="2.136268798828" />
                  <Point X="27.284908203125" Y="2.102634765625" />
                  <Point X="27.304953125" Y="2.092272216797" />
                  <Point X="27.327041015625" Y="2.084006347656" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.36403125" Y="2.076846679688" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.445236328125" Y="2.070877441406" />
                  <Point X="27.481564453125" Y="2.076874755859" />
                  <Point X="27.4906328125" Y="2.078830810547" />
                  <Point X="27.553494140625" Y="2.095640869141" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.757017578125" Y="2.784769042969" />
                  <Point X="28.967326171875" Y="2.906190673828" />
                  <Point X="29.123283203125" Y="2.689447265625" />
                  <Point X="29.165673828125" Y="2.619394287109" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.476994140625" Y="1.857373168945" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.221427734375" Y="1.660243408203" />
                  <Point X="28.203974609375" Y="1.641627929688" />
                  <Point X="28.19143359375" Y="1.625267456055" />
                  <Point X="28.146193359375" Y="1.566246459961" />
                  <Point X="28.133095703125" Y="1.543004272461" />
                  <Point X="28.119955078125" Y="1.509349609375" />
                  <Point X="28.1169609375" Y="1.500384155273" />
                  <Point X="28.100107421875" Y="1.440123901367" />
                  <Point X="28.09665234375" Y="1.417824829102" />
                  <Point X="28.0958359375" Y="1.39425390625" />
                  <Point X="28.097740234375" Y="1.371762695312" />
                  <Point X="28.101576171875" Y="1.353177246094" />
                  <Point X="28.11541015625" Y="1.286130004883" />
                  <Point X="28.124525390625" Y="1.26081628418" />
                  <Point X="28.14215234375" Y="1.227585205078" />
                  <Point X="28.146712890625" Y="1.219887451172" />
                  <Point X="28.18433984375" Y="1.162695556641" />
                  <Point X="28.198892578125" Y="1.145449951172" />
                  <Point X="28.21613671875" Y="1.129360229492" />
                  <Point X="28.23434765625" Y="1.116034301758" />
                  <Point X="28.249462890625" Y="1.107526000977" />
                  <Point X="28.303990234375" Y="1.07683190918" />
                  <Point X="28.3205234375" Y="1.069501098633" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.376556640625" Y="1.056737670898" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.598185546875" Y="1.193116333008" />
                  <Point X="29.776837890625" Y="1.216636474609" />
                  <Point X="29.845939453125" Y="0.932785583496" />
                  <Point X="29.859298828125" Y="0.84698638916" />
                  <Point X="29.890865234375" Y="0.644238769531" />
                  <Point X="28.998740234375" Y="0.405194396973" />
                  <Point X="28.716580078125" Y="0.329589874268" />
                  <Point X="28.7047890625" Y="0.325585662842" />
                  <Point X="28.681546875" Y="0.315068237305" />
                  <Point X="28.66146875" Y="0.303462799072" />
                  <Point X="28.589037109375" Y="0.261595672607" />
                  <Point X="28.56767578125" Y="0.244748321533" />
                  <Point X="28.54131640625" Y="0.216977661133" />
                  <Point X="28.535484375" Y="0.210226028442" />
                  <Point X="28.492025390625" Y="0.154848632812" />
                  <Point X="28.48030078125" Y="0.135567459106" />
                  <Point X="28.47052734375" Y="0.11410369873" />
                  <Point X="28.463681640625" Y="0.092604103088" />
                  <Point X="28.459666015625" Y="0.071636238098" />
                  <Point X="28.4451796875" Y="-0.004006376743" />
                  <Point X="28.443484375" Y="-0.021875444412" />
                  <Point X="28.4451796875" Y="-0.058553619385" />
                  <Point X="28.4491953125" Y="-0.079521484375" />
                  <Point X="28.463681640625" Y="-0.155164260864" />
                  <Point X="28.47052734375" Y="-0.176663848877" />
                  <Point X="28.48030078125" Y="-0.198127609253" />
                  <Point X="28.492025390625" Y="-0.217409545898" />
                  <Point X="28.504072265625" Y="-0.232759796143" />
                  <Point X="28.54753125" Y="-0.288137176514" />
                  <Point X="28.560001953125" Y="-0.301236999512" />
                  <Point X="28.589037109375" Y="-0.324155670166" />
                  <Point X="28.609115234375" Y="-0.335761108398" />
                  <Point X="28.681546875" Y="-0.377628234863" />
                  <Point X="28.692708984375" Y="-0.383138244629" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="29.734482421875" Y="-0.664896118164" />
                  <Point X="29.89147265625" Y="-0.706961425781" />
                  <Point X="29.85501953125" Y="-0.948749572754" />
                  <Point X="29.83790625" Y="-1.023744384766" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="28.751607421875" Y="-1.046520996094" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.33525390625" Y="-1.014073791504" />
                  <Point X="28.193095703125" Y="-1.044972412109" />
                  <Point X="28.163974609375" Y="-1.056597412109" />
                  <Point X="28.1361484375" Y="-1.073489746094" />
                  <Point X="28.1123984375" Y="-1.093960205078" />
                  <Point X="28.088580078125" Y="-1.122606323242" />
                  <Point X="28.002654296875" Y="-1.225948242188" />
                  <Point X="27.987935546875" Y="-1.250329467773" />
                  <Point X="27.976591796875" Y="-1.27771484375" />
                  <Point X="27.969759765625" Y="-1.305364990234" />
                  <Point X="27.966345703125" Y="-1.342463256836" />
                  <Point X="27.95403125" Y="-1.476295410156" />
                  <Point X="27.95634765625" Y="-1.507561767578" />
                  <Point X="27.964078125" Y="-1.539182739258" />
                  <Point X="27.97644921875" Y="-1.567994995117" />
                  <Point X="27.998255859375" Y="-1.601915649414" />
                  <Point X="28.0769296875" Y="-1.724285522461" />
                  <Point X="28.0869375" Y="-1.737243286133" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="29.05525" Y="-2.485741943359" />
                  <Point X="29.213123046875" Y="-2.606882568359" />
                  <Point X="29.1248046875" Y="-2.749794921875" />
                  <Point X="29.08940625" Y="-2.800091064453" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="28.09239453125" Y="-2.345205566406" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.707328125" Y="-2.151355224609" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.44483203125" Y="-2.135177246094" />
                  <Point X="27.40587109375" Y="-2.155682617187" />
                  <Point X="27.265314453125" Y="-2.22965625" />
                  <Point X="27.242384765625" Y="-2.246549804688" />
                  <Point X="27.22142578125" Y="-2.267509521484" />
                  <Point X="27.204533203125" Y="-2.290438232422" />
                  <Point X="27.18402734375" Y="-2.329399902344" />
                  <Point X="27.110052734375" Y="-2.469956298828" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531908447266" />
                  <Point X="27.09567578125" Y="-2.563260009766" />
                  <Point X="27.104146484375" Y="-2.610159179688" />
                  <Point X="27.134703125" Y="-2.779350341797" />
                  <Point X="27.13898828125" Y="-2.795139892578" />
                  <Point X="27.1518203125" Y="-2.826078857422" />
                  <Point X="27.75883203125" Y="-3.877456542969" />
                  <Point X="27.861283203125" Y="-4.054905761719" />
                  <Point X="27.781841796875" Y="-4.111648925781" />
                  <Point X="27.742267578125" Y="-4.137265136719" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="26.980943359375" Y="-3.224085449219" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900556884766" />
                  <Point X="26.675669921875" Y="-2.870818847656" />
                  <Point X="26.50880078125" Y="-2.763538085938" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.36651171875" Y="-2.745771972656" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.104595703125" Y="-2.795461181641" />
                  <Point X="26.065533203125" Y="-2.827940673828" />
                  <Point X="25.924611328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968861083984" />
                  <Point X="25.88725" Y="-2.996687744141" />
                  <Point X="25.875625" Y="-3.025808349609" />
                  <Point X="25.8639453125" Y="-3.079543701172" />
                  <Point X="25.821810546875" Y="-3.273396240234" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.991765625" Y="-4.629763671875" />
                  <Point X="26.02206640625" Y="-4.859915527344" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.939109375" Y="-4.8767265625" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94156640625" Y="-4.752635253906" />
                  <Point X="23.884380859375" Y="-4.737921875" />
                  <Point X="23.858755859375" Y="-4.731328613281" />
                  <Point X="23.868759765625" Y="-4.655331542969" />
                  <Point X="23.8792265625" Y="-4.575837890625" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030761719" />
                  <Point X="23.8782421875" Y="-4.509324707031" />
                  <Point X="23.876666015625" Y="-4.497689941406" />
                  <Point X="23.863529296875" Y="-4.431647949219" />
                  <Point X="23.81613671875" Y="-4.1933984375" />
                  <Point X="23.811875" Y="-4.178469238281" />
                  <Point X="23.80097265625" Y="-4.149501464844" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.769423828125" Y="-4.094859619141" />
                  <Point X="23.749791015625" Y="-4.070937255859" />
                  <Point X="23.738994140625" Y="-4.05978125" />
                  <Point X="23.688369140625" Y="-4.015383056641" />
                  <Point X="23.505734375" Y="-3.855216064453" />
                  <Point X="23.493263671875" Y="-3.845966308594" />
                  <Point X="23.46698046875" Y="-3.829621826172" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.295994140625" Y="-3.791765625" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.94632421875" Y="-3.795496582031" />
                  <Point X="22.9181328125" Y="-3.808270507812" />
                  <Point X="22.9045625" Y="-3.815812988281" />
                  <Point X="22.848576171875" Y="-3.853222900391" />
                  <Point X="22.64659765625" Y="-3.988181152344" />
                  <Point X="22.640318359375" Y="-3.992758789062" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629150391" />
                  <Point X="22.496798828125" Y="-4.162478027344" />
                  <Point X="22.252400390625" Y="-4.011153320313" />
                  <Point X="22.173234375" Y="-3.950197265625" />
                  <Point X="22.019134765625" Y="-3.831547119141" />
                  <Point X="22.49694140625" Y="-3.00396484375" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.7100859375" />
                  <Point X="22.67605078125" Y="-2.681121582031" />
                  <Point X="22.680314453125" Y="-2.666190917969" />
                  <Point X="22.6865859375" Y="-2.634665771484" />
                  <Point X="22.688361328125" Y="-2.619241699219" />
                  <Point X="22.689375" Y="-2.588309570312" />
                  <Point X="22.6853359375" Y="-2.557625488281" />
                  <Point X="22.676353515625" Y="-2.528009033203" />
                  <Point X="22.6706484375" Y="-2.513568603516" />
                  <Point X="22.65643359375" Y="-2.484739990234" />
                  <Point X="22.648453125" Y="-2.471422363281" />
                  <Point X="22.63042578125" Y="-2.446261962891" />
                  <Point X="22.62037890625" Y="-2.434419189453" />
                  <Point X="22.60303125" Y="-2.417070556641" />
                  <Point X="22.591189453125" Y="-2.407023681641" />
                  <Point X="22.56603125" Y="-2.388995361328" />
                  <Point X="22.55271484375" Y="-2.381013916016" />
                  <Point X="22.523884765625" Y="-2.366795410156" />
                  <Point X="22.5094453125" Y="-2.361088623047" />
                  <Point X="22.479828125" Y="-2.352103271484" />
                  <Point X="22.449140625" Y="-2.3480625" />
                  <Point X="22.418205078125" Y="-2.349074707031" />
                  <Point X="22.402779296875" Y="-2.350849121094" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.303763671875" Y="-2.961791015625" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="20.995978515625" Y="-2.740586425781" />
                  <Point X="20.93921875" Y="-2.645408691406" />
                  <Point X="20.818734375" Y="-2.443373779297" />
                  <Point X="21.669185546875" Y="-1.790800170898" />
                  <Point X="21.951876953125" Y="-1.573882080078" />
                  <Point X="21.960837890625" Y="-1.566069091797" />
                  <Point X="21.98372265625" Y="-1.543442749023" />
                  <Point X="21.997361328125" Y="-1.526442382812" />
                  <Point X="22.01640625" Y="-1.496142700195" />
                  <Point X="22.02448828125" Y="-1.480088500977" />
                  <Point X="22.032880859375" Y="-1.458557006836" />
                  <Point X="22.039787109375" Y="-1.437184692383" />
                  <Point X="22.045818359375" Y="-1.413893676758" />
                  <Point X="22.04805078125" Y="-1.40238659668" />
                  <Point X="22.0521640625" Y="-1.370905151367" />
                  <Point X="22.052919921875" Y="-1.355685913086" />
                  <Point X="22.05221875" Y="-1.332817382812" />
                  <Point X="22.048771484375" Y="-1.31020703125" />
                  <Point X="22.042625" Y="-1.288168945312" />
                  <Point X="22.038884765625" Y="-1.277331787109" />
                  <Point X="22.02687890625" Y="-1.248350830078" />
                  <Point X="22.02130078125" Y="-1.23706652832" />
                  <Point X="22.00870703125" Y="-1.215340942383" />
                  <Point X="21.993357421875" Y="-1.195476196289" />
                  <Point X="21.975513671875" Y="-1.177808959961" />
                  <Point X="21.96600390625" Y="-1.169565063477" />
                  <Point X="21.94775390625" Y="-1.155753295898" />
                  <Point X="21.92946875" Y="-1.143513549805" />
                  <Point X="21.908734375" Y="-1.131309936523" />
                  <Point X="21.894568359375" Y="-1.124482177734" />
                  <Point X="21.8653046875" Y="-1.1132578125" />
                  <Point X="21.85020703125" Y="-1.108861083984" />
                  <Point X="21.8183203125" Y="-1.102379150391" />
                  <Point X="21.802705078125" Y="-1.100532714844" />
                  <Point X="21.771380859375" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="20.481205078125" Y="-1.267983154297" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.259236328125" Y="-0.974109680176" />
                  <Point X="20.244220703125" Y="-0.869114807129" />
                  <Point X="20.213548828125" Y="-0.654654724121" />
                  <Point X="21.16968359375" Y="-0.398458953857" />
                  <Point X="21.491712890625" Y="-0.312171203613" />
                  <Point X="21.50181640625" Y="-0.308847717285" />
                  <Point X="21.521587890625" Y="-0.301092132568" />
                  <Point X="21.531255859375" Y="-0.29666003418" />
                  <Point X="21.552845703125" Y="-0.285259765625" />
                  <Point X="21.572458984375" Y="-0.273335632324" />
                  <Point X="21.594189453125" Y="-0.258254119873" />
                  <Point X="21.60351953125" Y="-0.250871124268" />
                  <Point X="21.627474609375" Y="-0.229345336914" />
                  <Point X="21.6419296875" Y="-0.212984191895" />
                  <Point X="21.6624296875" Y="-0.183555831909" />
                  <Point X="21.671279296875" Y="-0.167864227295" />
                  <Point X="21.680712890625" Y="-0.146656051636" />
                  <Point X="21.688576171875" Y="-0.125748527527" />
                  <Point X="21.695818359375" Y="-0.102410728455" />
                  <Point X="21.698427734375" Y="-0.091928504944" />
                  <Point X="21.703869140625" Y="-0.063190349579" />
                  <Point X="21.705501953125" Y="-0.043291980743" />
                  <Point X="21.704732421875" Y="-0.010439940453" />
                  <Point X="21.702916015625" Y="0.005950780869" />
                  <Point X="21.698662109375" Y="0.027238922119" />
                  <Point X="21.693810546875" Y="0.046319568634" />
                  <Point X="21.686568359375" Y="0.069657516479" />
                  <Point X="21.682333984375" Y="0.080782974243" />
                  <Point X="21.669015625" Y="0.110110427856" />
                  <Point X="21.657685546875" Y="0.128922210693" />
                  <Point X="21.63559765625" Y="0.157502243042" />
                  <Point X="21.623181640625" Y="0.170733383179" />
                  <Point X="21.605337890625" Y="0.18643270874" />
                  <Point X="21.588166015625" Y="0.199874755859" />
                  <Point X="21.566435546875" Y="0.214956268311" />
                  <Point X="21.55951171875" Y="0.219332015991" />
                  <Point X="21.534384765625" Y="0.23283454895" />
                  <Point X="21.503435546875" Y="0.2456355896" />
                  <Point X="21.491712890625" Y="0.249611236572" />
                  <Point X="20.3299765625" Y="0.560897460938" />
                  <Point X="20.2145546875" Y="0.591824829102" />
                  <Point X="20.26866796875" Y="0.957522766113" />
                  <Point X="20.29890234375" Y="1.069096923828" />
                  <Point X="20.3664140625" Y="1.318237182617" />
                  <Point X="20.991095703125" Y="1.23599621582" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.20470324707" />
                  <Point X="21.28485546875" Y="1.206589477539" />
                  <Point X="21.295109375" Y="1.208053710938" />
                  <Point X="21.315400390625" Y="1.21208984375" />
                  <Point X="21.338767578125" Y="1.218865722656" />
                  <Point X="21.38686328125" Y="1.234029663086" />
                  <Point X="21.396556640625" Y="1.237678100586" />
                  <Point X="21.41548828125" Y="1.246008789062" />
                  <Point X="21.4247265625" Y="1.250690673828" />
                  <Point X="21.443470703125" Y="1.261513183594" />
                  <Point X="21.452146484375" Y="1.267174438477" />
                  <Point X="21.46882421875" Y="1.279403930664" />
                  <Point X="21.4840703125" Y="1.293375" />
                  <Point X="21.49770703125" Y="1.308924072266" />
                  <Point X="21.504099609375" Y="1.3170703125" />
                  <Point X="21.516515625" Y="1.33480065918" />
                  <Point X="21.521986328125" Y="1.343596801758" />
                  <Point X="21.5319375" Y="1.361733886719" />
                  <Point X="21.541767578125" Y="1.383989501953" />
                  <Point X="21.561064453125" Y="1.430579223633" />
                  <Point X="21.564501953125" Y="1.440349609375" />
                  <Point X="21.570291015625" Y="1.460209716797" />
                  <Point X="21.572642578125" Y="1.470299194336" />
                  <Point X="21.576400390625" Y="1.491611572266" />
                  <Point X="21.577640625" Y="1.501890136719" />
                  <Point X="21.578994140625" Y="1.522526855469" />
                  <Point X="21.578091796875" Y="1.543188964844" />
                  <Point X="21.574947265625" Y="1.563629394531" />
                  <Point X="21.572818359375" Y="1.573765869141" />
                  <Point X="21.56721875" Y="1.594669921875" />
                  <Point X="21.563994140625" Y="1.604518798828" />
                  <Point X="21.55649609375" Y="1.623801147461" />
                  <Point X="21.545765625" Y="1.645639038086" />
                  <Point X="21.522478515625" Y="1.690369384766" />
                  <Point X="21.517201171875" Y="1.699283447266" />
                  <Point X="21.50570703125" Y="1.716486572266" />
                  <Point X="21.499490234375" Y="1.724776000977" />
                  <Point X="21.485578125" Y="1.741354858398" />
                  <Point X="21.47849609375" Y="1.748915527344" />
                  <Point X="21.4635546875" Y="1.763217651367" />
                  <Point X="21.4556953125" Y="1.769958984375" />
                  <Point X="20.789322265625" Y="2.281285888672" />
                  <Point X="20.77238671875" Y="2.294280761719" />
                  <Point X="20.99771875" Y="2.680327880859" />
                  <Point X="21.07778515625" Y="2.783243896484" />
                  <Point X="21.273662109375" Y="3.035013916016" />
                  <Point X="21.604068359375" Y="2.844252685547" />
                  <Point X="21.74584375" Y="2.762398925781" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774015625" Y="2.749385742188" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.86349609375" Y="2.729533447266" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.02356640625" Y="2.734227294922" />
                  <Point X="22.04300390625" Y="2.741302001953" />
                  <Point X="22.0615546875" Y="2.750450439453" />
                  <Point X="22.070580078125" Y="2.755530761719" />
                  <Point X="22.088833984375" Y="2.767159667969" />
                  <Point X="22.097251953125" Y="2.773192138672" />
                  <Point X="22.1133828125" Y="2.786138183594" />
                  <Point X="22.134275390625" Y="2.806230957031" />
                  <Point X="22.1818203125" Y="2.853775146484" />
                  <Point X="22.188734375" Y="2.861486816406" />
                  <Point X="22.20168359375" Y="2.877620361328" />
                  <Point X="22.20771875" Y="2.886042236328" />
                  <Point X="22.21934765625" Y="2.904296386719" />
                  <Point X="22.2244296875" Y="2.913324951172" />
                  <Point X="22.233576171875" Y="2.931874023438" />
                  <Point X="22.240646484375" Y="2.951298583984" />
                  <Point X="22.245564453125" Y="2.971387695312" />
                  <Point X="22.2474765625" Y="2.981572753906" />
                  <Point X="22.250302734375" Y="3.003031494141" />
                  <Point X="22.251091796875" Y="3.013364746094" />
                  <Point X="22.25154296875" Y="3.034052001953" />
                  <Point X="22.249580078125" Y="3.062973388672" />
                  <Point X="22.243720703125" Y="3.129955566406" />
                  <Point X="22.242255859375" Y="3.140212646484" />
                  <Point X="22.23821875" Y="3.160501464844" />
                  <Point X="22.235646484375" Y="3.170533203125" />
                  <Point X="22.22913671875" Y="3.191176025391" />
                  <Point X="22.22548828125" Y="3.200870605469" />
                  <Point X="22.217158203125" Y="3.219799072266" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="21.940611328125" Y="3.699916015625" />
                  <Point X="22.351630859375" Y="4.015039306641" />
                  <Point X="22.4777421875" Y="4.085104248047" />
                  <Point X="22.807474609375" Y="4.268296386719" />
                  <Point X="22.838033203125" Y="4.228470214844" />
                  <Point X="22.881435546875" Y="4.171908203125" />
                  <Point X="22.888181640625" Y="4.164045410156" />
                  <Point X="22.902486328125" Y="4.149103027344" />
                  <Point X="22.910044921875" Y="4.142022949219" />
                  <Point X="22.926625" Y="4.128110839844" />
                  <Point X="22.934912109375" Y="4.121895019531" />
                  <Point X="22.952111328125" Y="4.110403320312" />
                  <Point X="22.9610234375" Y="4.105127441406" />
                  <Point X="22.981689453125" Y="4.094369873047" />
                  <Point X="23.056240234375" Y="4.055560791016" />
                  <Point X="23.06567578125" Y="4.05128515625" />
                  <Point X="23.084955078125" Y="4.043788574219" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.229271484375" Y="4.037488525391" />
                  <Point X="23.249134765625" Y="4.043278076172" />
                  <Point X="23.258908203125" Y="4.046715332031" />
                  <Point X="23.280431640625" Y="4.055631347656" />
                  <Point X="23.35808203125" Y="4.087794921875" />
                  <Point X="23.367419921875" Y="4.092274169922" />
                  <Point X="23.38555078125" Y="4.102221191406" />
                  <Point X="23.39434375" Y="4.107689453125" />
                  <Point X="23.41207421875" Y="4.120104003906" />
                  <Point X="23.420224609375" Y="4.126499511719" />
                  <Point X="23.435779296875" Y="4.140140625" />
                  <Point X="23.449755859375" Y="4.15539453125" />
                  <Point X="23.46198828125" Y="4.172079101562" />
                  <Point X="23.4676484375" Y="4.180755371094" />
                  <Point X="23.47846875" Y="4.199499511719" />
                  <Point X="23.4831484375" Y="4.208732421875" />
                  <Point X="23.4914765625" Y="4.227659179688" />
                  <Point X="23.495125" Y="4.237353027344" />
                  <Point X="23.502130859375" Y="4.259572265625" />
                  <Point X="23.527404296875" Y="4.339729492188" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370049804688" />
                  <Point X="23.535474609375" Y="4.380301757812" />
                  <Point X="23.537361328125" Y="4.401862304688" />
                  <Point X="23.53769921875" Y="4.412215820312" />
                  <Point X="23.537248046875" Y="4.432897949219" />
                  <Point X="23.536458984375" Y="4.4432265625" />
                  <Point X="23.520734375" Y="4.562655273438" />
                  <Point X="24.068826171875" Y="4.7163203125" />
                  <Point X="24.221705078125" Y="4.734212890625" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.732392578125" Y="4.418253417969" />
                  <Point X="24.77433203125" Y="4.261728027344" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912597656" />
                  <Point X="24.7998828125" Y="4.205341308594" />
                  <Point X="24.8177421875" Y="4.178614257812" />
                  <Point X="24.82739453125" Y="4.166452636719" />
                  <Point X="24.84855078125" Y="4.143865234375" />
                  <Point X="24.873103515625" Y="4.125025390625" />
                  <Point X="24.9003984375" Y="4.110436035156" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="25.95437109375" Y="4.707372558594" />
                  <Point X="26.453591796875" Y="4.586844726562" />
                  <Point X="26.533798828125" Y="4.55775390625" />
                  <Point X="26.858265625" Y="4.440067871094" />
                  <Point X="26.93787890625" Y="4.402834960938" />
                  <Point X="27.2504453125" Y="4.256658203125" />
                  <Point X="27.327427734375" Y="4.211808105469" />
                  <Point X="27.62944140625" Y="4.035854980469" />
                  <Point X="27.70198046875" Y="3.984267578125" />
                  <Point X="27.817783203125" Y="3.901915527344" />
                  <Point X="27.25231640625" Y="2.922498046875" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.06084375" Y="2.589840576172" />
                  <Point X="27.0492890625" Y="2.562722900391" />
                  <Point X="27.039626953125" Y="2.533215087891" />
                  <Point X="27.036642578125" Y="2.523175537109" />
                  <Point X="27.01983203125" Y="2.460314208984" />
                  <Point X="27.01733203125" Y="2.447479736328" />
                  <Point X="27.0141171875" Y="2.42159375" />
                  <Point X="27.01340234375" Y="2.408542236328" />
                  <Point X="27.0138828125" Y="2.374566894531" />
                  <Point X="27.01523046875" Y="2.354511230469" />
                  <Point X="27.021783203125" Y="2.300160400391" />
                  <Point X="27.02380078125" Y="2.289039794922" />
                  <Point X="27.029142578125" Y="2.267118652344" />
                  <Point X="27.032466796875" Y="2.256318115234" />
                  <Point X="27.040732421875" Y="2.234227783203" />
                  <Point X="27.045314453125" Y="2.223896728516" />
                  <Point X="27.0556796875" Y="2.203844482422" />
                  <Point X="27.070787109375" Y="2.180383056641" />
                  <Point X="27.104421875" Y="2.130814697266" />
                  <Point X="27.1125703125" Y="2.120439941406" />
                  <Point X="27.130220703125" Y="2.100921875" />
                  <Point X="27.13972265625" Y="2.091778564453" />
                  <Point X="27.166763671875" Y="2.069155029297" />
                  <Point X="27.181998046875" Y="2.057657470703" />
                  <Point X="27.23156640625" Y="2.0240234375" />
                  <Point X="27.24128125" Y="2.018244628906" />
                  <Point X="27.261326171875" Y="2.007882080078" />
                  <Point X="27.27165625" Y="2.003298339844" />
                  <Point X="27.293744140625" Y="1.995032470703" />
                  <Point X="27.304548828125" Y="1.991707519531" />
                  <Point X="27.32647265625" Y="1.986364746094" />
                  <Point X="27.352658203125" Y="1.982529907227" />
                  <Point X="27.407015625" Y="1.975975341797" />
                  <Point X="27.420458984375" Y="1.975314575195" />
                  <Point X="27.447306640625" Y="1.975900024414" />
                  <Point X="27.4607109375" Y="1.977146118164" />
                  <Point X="27.4970390625" Y="1.983143432617" />
                  <Point X="27.51517578125" Y="1.987055664062" />
                  <Point X="27.578037109375" Y="2.003865722656" />
                  <Point X="27.584" Y="2.005671875" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.804517578125" Y="2.702496582031" />
                  <Point X="28.940404296875" Y="2.780950683594" />
                  <Point X="29.04396875" Y="2.63701953125" />
                  <Point X="29.084396484375" Y="2.570211425781" />
                  <Point X="29.136884765625" Y="2.483471679688" />
                  <Point X="28.419162109375" Y="1.932741699219" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.168140625" Y="1.739869384766" />
                  <Point X="28.152123046875" Y="1.725219970703" />
                  <Point X="28.134669921875" Y="1.706604492188" />
                  <Point X="28.128578125" Y="1.699423095703" />
                  <Point X="28.116037109375" Y="1.68306262207" />
                  <Point X="28.070796875" Y="1.624041503906" />
                  <Point X="28.0634296875" Y="1.612885986328" />
                  <Point X="28.05033203125" Y="1.589643798828" />
                  <Point X="28.0446015625" Y="1.577556884766" />
                  <Point X="28.0314609375" Y="1.54390234375" />
                  <Point X="28.02547265625" Y="1.525971679688" />
                  <Point X="28.008619140625" Y="1.465711547852" />
                  <Point X="28.006228515625" Y="1.454669921875" />
                  <Point X="28.0027734375" Y="1.432370849609" />
                  <Point X="28.001708984375" Y="1.42111328125" />
                  <Point X="28.000892578125" Y="1.397542480469" />
                  <Point X="28.001173828125" Y="1.386239013672" />
                  <Point X="28.003078125" Y="1.363747924805" />
                  <Point X="28.008537109375" Y="1.333974487305" />
                  <Point X="28.02237109375" Y="1.266927124023" />
                  <Point X="28.026029296875" Y="1.253944580078" />
                  <Point X="28.03514453125" Y="1.228630737305" />
                  <Point X="28.0406015625" Y="1.216299804688" />
                  <Point X="28.058228515625" Y="1.183068725586" />
                  <Point X="28.067349609375" Y="1.167673217773" />
                  <Point X="28.1049765625" Y="1.110481323242" />
                  <Point X="28.111736328125" Y="1.101428466797" />
                  <Point X="28.1262890625" Y="1.084182861328" />
                  <Point X="28.13408203125" Y="1.075989868164" />
                  <Point X="28.151326171875" Y="1.059900268555" />
                  <Point X="28.16003515625" Y="1.052694091797" />
                  <Point X="28.17824609375" Y="1.039368164063" />
                  <Point X="28.20286328125" Y="1.024740356445" />
                  <Point X="28.257390625" Y="0.994046264648" />
                  <Point X="28.265482421875" Y="0.989986206055" />
                  <Point X="28.294681640625" Y="0.978083557129" />
                  <Point X="28.33027734375" Y="0.968021240234" />
                  <Point X="28.343671875" Y="0.965257751465" />
                  <Point X="28.364109375" Y="0.962556640625" />
                  <Point X="28.437833984375" Y="0.952812927246" />
                  <Point X="28.444033203125" Y="0.952199707031" />
                  <Point X="28.46571875" Y="0.951222839355" />
                  <Point X="28.49121875" Y="0.952032348633" />
                  <Point X="28.50060546875" Y="0.952797180176" />
                  <Point X="29.6105859375" Y="1.098929077148" />
                  <Point X="29.704701171875" Y="1.111319702148" />
                  <Point X="29.7526875" Y="0.91420513916" />
                  <Point X="29.7654296875" Y="0.832370483398" />
                  <Point X="29.78387109375" Y="0.713921081543" />
                  <Point X="28.97415234375" Y="0.496957397461" />
                  <Point X="28.6919921875" Y="0.421352813721" />
                  <Point X="28.68603125" Y="0.419544219971" />
                  <Point X="28.665623046875" Y="0.412136566162" />
                  <Point X="28.642380859375" Y="0.401619110107" />
                  <Point X="28.634005859375" Y="0.397316925049" />
                  <Point X="28.613927734375" Y="0.385711578369" />
                  <Point X="28.54149609375" Y="0.343844360352" />
                  <Point X="28.53020703125" Y="0.336188201904" />
                  <Point X="28.508845703125" Y="0.319340759277" />
                  <Point X="28.4987734375" Y="0.310149780273" />
                  <Point X="28.4724140625" Y="0.282379150391" />
                  <Point X="28.46075" Y="0.26887588501" />
                  <Point X="28.417291015625" Y="0.213498413086" />
                  <Point X="28.41085546875" Y="0.20420741272" />
                  <Point X="28.399130859375" Y="0.184926269531" />
                  <Point X="28.393841796875" Y="0.174936141968" />
                  <Point X="28.384068359375" Y="0.153472335815" />
                  <Point X="28.380005859375" Y="0.14292678833" />
                  <Point X="28.37316015625" Y="0.121427314758" />
                  <Point X="28.370376953125" Y="0.110473068237" />
                  <Point X="28.366361328125" Y="0.089505203247" />
                  <Point X="28.351875" Y="0.013862569809" />
                  <Point X="28.350603515625" Y="0.004966303349" />
                  <Point X="28.3485859375" Y="-0.026261734009" />
                  <Point X="28.35028125" Y="-0.062939918518" />
                  <Point X="28.351875" Y="-0.076422683716" />
                  <Point X="28.355890625" Y="-0.097390548706" />
                  <Point X="28.370376953125" Y="-0.173033325195" />
                  <Point X="28.37316015625" Y="-0.183987426758" />
                  <Point X="28.380005859375" Y="-0.205487060547" />
                  <Point X="28.384068359375" Y="-0.216032440186" />
                  <Point X="28.393841796875" Y="-0.237496261597" />
                  <Point X="28.39912890625" Y="-0.247485046387" />
                  <Point X="28.410853515625" Y="-0.266766937256" />
                  <Point X="28.429337890625" Y="-0.291410522461" />
                  <Point X="28.472796875" Y="-0.346787994385" />
                  <Point X="28.478724609375" Y="-0.353639801025" />
                  <Point X="28.501142578125" Y="-0.37580557251" />
                  <Point X="28.530177734375" Y="-0.398724243164" />
                  <Point X="28.54149609375" Y="-0.40640447998" />
                  <Point X="28.56157421875" Y="-0.418009857178" />
                  <Point X="28.634005859375" Y="-0.459877044678" />
                  <Point X="28.63949609375" Y="-0.462814575195" />
                  <Point X="28.65915625" Y="-0.472015960693" />
                  <Point X="28.68302734375" Y="-0.481027557373" />
                  <Point X="28.6919921875" Y="-0.483912780762" />
                  <Point X="29.70989453125" Y="-0.756659057617" />
                  <Point X="29.78487890625" Y="-0.776750793457" />
                  <Point X="29.76161328125" Y="-0.931063964844" />
                  <Point X="29.745287109375" Y="-1.002609313965" />
                  <Point X="29.7278046875" Y="-1.079219970703" />
                  <Point X="28.7640078125" Y="-0.95233380127" />
                  <Point X="28.436783203125" Y="-0.909253662109" />
                  <Point X="28.428625" Y="-0.908535705566" />
                  <Point X="28.40009765625" Y="-0.908042419434" />
                  <Point X="28.36672265625" Y="-0.910840942383" />
                  <Point X="28.354482421875" Y="-0.912676147461" />
                  <Point X="28.315076171875" Y="-0.921241271973" />
                  <Point X="28.17291796875" Y="-0.952139831543" />
                  <Point X="28.157875" Y="-0.956742675781" />
                  <Point X="28.12875390625" Y="-0.968367675781" />
                  <Point X="28.11467578125" Y="-0.975389770508" />
                  <Point X="28.086849609375" Y="-0.992282104492" />
                  <Point X="28.074125" Y="-1.001530334473" />
                  <Point X="28.050375" Y="-1.022000732422" />
                  <Point X="28.039349609375" Y="-1.033223144531" />
                  <Point X="28.01553125" Y="-1.061869262695" />
                  <Point X="27.92960546875" Y="-1.165211181641" />
                  <Point X="27.921326171875" Y="-1.176850585938" />
                  <Point X="27.906607421875" Y="-1.201231811523" />
                  <Point X="27.90016796875" Y="-1.213973510742" />
                  <Point X="27.88882421875" Y="-1.241358886719" />
                  <Point X="27.884365234375" Y="-1.254926757812" />
                  <Point X="27.877533203125" Y="-1.282576904297" />
                  <Point X="27.87516015625" Y="-1.296659179688" />
                  <Point X="27.87174609375" Y="-1.333757324219" />
                  <Point X="27.859431640625" Y="-1.467589599609" />
                  <Point X="27.859291015625" Y="-1.483314331055" />
                  <Point X="27.861607421875" Y="-1.514580688477" />
                  <Point X="27.864064453125" Y="-1.530122314453" />
                  <Point X="27.871794921875" Y="-1.561743286133" />
                  <Point X="27.87678515625" Y="-1.576663818359" />
                  <Point X="27.88915625" Y="-1.605476196289" />
                  <Point X="27.896537109375" Y="-1.619367797852" />
                  <Point X="27.91834375" Y="-1.653288452148" />
                  <Point X="27.997017578125" Y="-1.775658325195" />
                  <Point X="28.001744140625" Y="-1.782354858398" />
                  <Point X="28.01980078125" Y="-1.804457397461" />
                  <Point X="28.043494140625" Y="-1.828123291016" />
                  <Point X="28.052798828125" Y="-1.836277709961" />
                  <Point X="28.99741796875" Y="-2.561110351562" />
                  <Point X="29.087171875" Y="-2.629981201172" />
                  <Point X="29.045486328125" Y="-2.697435058594" />
                  <Point X="29.01171875" Y="-2.7454140625" />
                  <Point X="29.001275390625" Y="-2.760252197266" />
                  <Point X="28.13989453125" Y="-2.262933105469" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.724212890625" Y="-2.057867675781" />
                  <Point X="27.555021484375" Y="-2.027312011719" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.444845703125" Y="-2.035136230469" />
                  <Point X="27.415068359375" Y="-2.044960449219" />
                  <Point X="27.4005859375" Y="-2.051109619141" />
                  <Point X="27.361625" Y="-2.071614990234" />
                  <Point X="27.221068359375" Y="-2.145588623047" />
                  <Point X="27.20896484375" Y="-2.153172851563" />
                  <Point X="27.18603515625" Y="-2.17006640625" />
                  <Point X="27.175208984375" Y="-2.179375732422" />
                  <Point X="27.15425" Y="-2.200335449219" />
                  <Point X="27.14494140625" Y="-2.211160400391" />
                  <Point X="27.128048828125" Y="-2.234089111328" />
                  <Point X="27.12046484375" Y="-2.246192871094" />
                  <Point X="27.099958984375" Y="-2.285154541016" />
                  <Point X="27.025984375" Y="-2.4257109375" />
                  <Point X="27.0198359375" Y="-2.440193359375" />
                  <Point X="27.01001171875" Y="-2.469972412109" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517442138672" />
                  <Point X="27.000279296875" Y="-2.533133544922" />
                  <Point X="27.00068359375" Y="-2.564485107422" />
                  <Point X="27.0021875" Y="-2.580145263672" />
                  <Point X="27.010658203125" Y="-2.627044433594" />
                  <Point X="27.04121484375" Y="-2.796235595703" />
                  <Point X="27.04301953125" Y="-2.804232421875" />
                  <Point X="27.051236328125" Y="-2.83153515625" />
                  <Point X="27.064068359375" Y="-2.862474121094" />
                  <Point X="27.069546875" Y="-2.873578857422" />
                  <Point X="27.67655859375" Y="-3.924956542969" />
                  <Point X="27.735892578125" Y="-4.027724609375" />
                  <Point X="27.723755859375" Y="-4.036081542969" />
                  <Point X="27.0563125" Y="-3.166253173828" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849626220703" />
                  <Point X="26.78325390625" Y="-2.828004882812" />
                  <Point X="26.77330078125" Y="-2.820646484375" />
                  <Point X="26.727044921875" Y="-2.790908447266" />
                  <Point X="26.56017578125" Y="-2.683627685547" />
                  <Point X="26.546283203125" Y="-2.676244628906" />
                  <Point X="26.517470703125" Y="-2.663873291016" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.357806640625" Y="-2.651171630859" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079876953125" Y="-2.699413085938" />
                  <Point X="26.055494140625" Y="-2.714134033203" />
                  <Point X="26.043857421875" Y="-2.722413330078" />
                  <Point X="26.004794921875" Y="-2.754892822266" />
                  <Point X="25.863873046875" Y="-2.872063720703" />
                  <Point X="25.852650390625" Y="-2.883091064453" />
                  <Point X="25.832181640625" Y="-2.906840576172" />
                  <Point X="25.822935546875" Y="-2.919562744141" />
                  <Point X="25.80604296875" Y="-2.947389404297" />
                  <Point X="25.79901953125" Y="-2.961466308594" />
                  <Point X="25.78739453125" Y="-2.990586914062" />
                  <Point X="25.78279296875" Y="-3.005630615234" />
                  <Point X="25.77111328125" Y="-3.059365966797" />
                  <Point X="25.728978515625" Y="-3.253218505859" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520263672" />
                  <Point X="25.83308984375" Y="-4.152324707031" />
                  <Point X="25.7420078125" Y="-3.81240234375" />
                  <Point X="25.655068359375" Y="-3.487936767578" />
                  <Point X="25.652607421875" Y="-3.480121582031" />
                  <Point X="25.642146484375" Y="-3.453578857422" />
                  <Point X="25.6267890625" Y="-3.423815429688" />
                  <Point X="25.62041015625" Y="-3.413209228516" />
                  <Point X="25.584875" Y="-3.362010498047" />
                  <Point X="25.456681640625" Y="-3.177308837891" />
                  <Point X="25.446671875" Y="-3.165171386719" />
                  <Point X="25.424787109375" Y="-3.142715820312" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104936767578" />
                  <Point X="25.3452421875" Y="-3.090829345703" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.27566796875" Y="-3.067872558594" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.998840087891" />
                  <Point X="24.948935546875" Y="-3.003109619141" />
                  <Point X="24.935015625" Y="-3.006305664062" />
                  <Point X="24.880029296875" Y="-3.023371826172" />
                  <Point X="24.68165625" Y="-3.084938964844" />
                  <Point X="24.6670703125" Y="-3.090829345703" />
                  <Point X="24.6390703125" Y="-3.104936767578" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.5875234375" Y="-3.14271875" />
                  <Point X="24.565638671875" Y="-3.165176269531" />
                  <Point X="24.555630859375" Y="-3.177312744141" />
                  <Point X="24.52009765625" Y="-3.228511474609" />
                  <Point X="24.391904296875" Y="-3.413213134766" />
                  <Point X="24.387533203125" Y="-3.420129394531" />
                  <Point X="24.374025390625" Y="-3.445260742188" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936279297" />
                  <Point X="24.04360546875" Y="-4.658460449219" />
                  <Point X="24.01457421875" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.242793176557" Y="-0.646818717481" />
                  <Point X="20.434578935657" Y="-1.274121669819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.334612057345" Y="-0.622215900248" />
                  <Point X="20.530075862135" Y="-1.261549197964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.877588905597" Y="-2.398213146127" />
                  <Point X="20.967834429801" Y="-2.693392955279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.426430938133" Y="-0.597613083016" />
                  <Point X="20.625572809671" Y="-1.248976794986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.958053106172" Y="-2.33647084339" />
                  <Point X="21.139084175262" Y="-2.92859678975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.518249818921" Y="-0.573010265784" />
                  <Point X="20.721069757208" Y="-1.236404392008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.038517306748" Y="-2.274728540654" />
                  <Point X="21.256853642109" Y="-2.988874514878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.229854110333" Y="0.695218436698" />
                  <Point X="20.265650516537" Y="0.578133667731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.610068699709" Y="-0.548407448551" />
                  <Point X="20.816566704744" Y="-1.223831989031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.118981507324" Y="-2.212986237917" />
                  <Point X="21.341290167291" Y="-2.940125100481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.262253491133" Y="0.914173681055" />
                  <Point X="20.373855439986" Y="0.54914015442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.701887580497" Y="-0.523804631319" />
                  <Point X="20.912063652281" Y="-1.211259586053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.1994457079" Y="-2.15124393518" />
                  <Point X="21.42572669307" Y="-2.891375688037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.306104084645" Y="1.095673696328" />
                  <Point X="20.482060330294" Y="0.520146749509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.793706461285" Y="-0.499201814086" />
                  <Point X="21.007560599817" Y="-1.198687183076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.279909908475" Y="-2.089501632444" />
                  <Point X="21.510163218849" Y="-2.842626275593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.35278135818" Y="1.267928057845" />
                  <Point X="20.590265220601" Y="0.491153344597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.885525342073" Y="-0.474598996854" />
                  <Point X="21.103057547353" Y="-1.186114780098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.360374109051" Y="-2.027759329707" />
                  <Point X="21.594599744628" Y="-2.793876863149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.439690423254" Y="1.308590158661" />
                  <Point X="20.698470110909" Y="0.462159939686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.977344222861" Y="-0.449996179621" />
                  <Point X="21.19855449489" Y="-1.17354237712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.440838309627" Y="-1.966017026971" />
                  <Point X="21.679036270407" Y="-2.745127450705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.543197317603" Y="1.294963206134" />
                  <Point X="20.806675001217" Y="0.433166534774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.069163103649" Y="-0.425393362389" />
                  <Point X="21.294051442426" Y="-1.160969974143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.521302510203" Y="-1.904274724234" />
                  <Point X="21.763472796186" Y="-2.696378038262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.078887437465" Y="-3.728052843596" />
                  <Point X="22.138666549972" Y="-3.923581510271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.646704211952" Y="1.281336253607" />
                  <Point X="20.914879891524" Y="0.404173129863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.160981984437" Y="-0.400790545157" />
                  <Point X="21.389548389962" Y="-1.148397571165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.601766710779" Y="-1.842532421497" />
                  <Point X="21.847909321965" Y="-2.647628625818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.143835576159" Y="-3.615559789225" />
                  <Point X="22.267671470214" Y="-4.020608747559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.7502111063" Y="1.26770930108" />
                  <Point X="21.023084781832" Y="0.375179724951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.252800851945" Y="-0.376187684486" />
                  <Point X="21.485045337499" Y="-1.135825168188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.682230893973" Y="-1.78079006191" />
                  <Point X="21.932345847744" Y="-2.598879213374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.208783714853" Y="-3.503066734854" />
                  <Point X="22.390208423011" Y="-4.096480216592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.853718000649" Y="1.254082348553" />
                  <Point X="21.13128967214" Y="0.34618632004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.344619718063" Y="-0.351584819268" />
                  <Point X="21.580542285035" Y="-1.12325276521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.762694987343" Y="-1.719047408517" />
                  <Point X="22.016782373523" Y="-2.55012980093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.273731853547" Y="-3.390573680482" />
                  <Point X="22.506043342839" Y="-4.150430323539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.957224894997" Y="1.240455396026" />
                  <Point X="21.239494562447" Y="0.317192915128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.43643858418" Y="-0.32698195405" />
                  <Point X="21.676039232571" Y="-1.110680362232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.843159080712" Y="-1.657304755124" />
                  <Point X="22.101218899303" Y="-2.501380388486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.338679992241" Y="-3.278080626111" />
                  <Point X="22.5770803275" Y="-4.057852986941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.060731785279" Y="1.226828456803" />
                  <Point X="21.347699452755" Y="0.288199510217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.527092483873" Y="-0.298568655353" />
                  <Point X="21.771949917022" Y="-1.099461231725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.923623174081" Y="-1.595562101731" />
                  <Point X="22.185655425082" Y="-2.452630976042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.403628130935" Y="-3.16558757174" />
                  <Point X="22.653674536933" Y="-3.983452513541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.812636950962" Y="2.363238939143" />
                  <Point X="20.852517957218" Y="2.232794045404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.164238673581" Y="1.21320152405" />
                  <Point X="21.455904343063" Y="0.259206105305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.610055106619" Y="-0.244998323315" />
                  <Point X="21.876864210606" Y="-1.117691579726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.000368950185" Y="-1.521657380572" />
                  <Point X="22.270091950861" Y="-2.403881563598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.468576269629" Y="-3.053094517369" />
                  <Point X="22.736164014508" Y="-3.928334593382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.877830172674" Y="2.474930363082" />
                  <Point X="20.982306528818" Y="2.133203600021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.266102480865" Y="1.204948867153" />
                  <Point X="21.569403307183" Y="0.212896565202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.679893064598" Y="-0.148499147156" />
                  <Point X="22.004325875213" Y="-1.209671055277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.0523675344" Y="-1.366808242026" />
                  <Point X="22.356430356562" Y="-2.361352920078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.533524333349" Y="-2.94060121777" />
                  <Point X="22.818653492084" Y="-3.873216673223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.943023394387" Y="2.586621787022" />
                  <Point X="21.112095100418" Y="2.033613154639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.359217191521" Y="1.225313215867" />
                  <Point X="22.451815465389" Y="-2.348414709166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.598472338938" Y="-2.82810772803" />
                  <Point X="22.901142755416" Y="-3.818098052307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.00920241869" Y="2.695088795975" />
                  <Point X="21.241883672017" Y="1.934022709256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.446821902868" Y="1.26369996025" />
                  <Point X="22.563009145346" Y="-2.387184004527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.663072097965" Y="-2.714475175113" />
                  <Point X="22.989624655708" Y="-3.782580463679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.080517634055" Y="2.786756080942" />
                  <Point X="21.371672243617" Y="1.834432263873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.521818301371" Y="1.343326637713" />
                  <Point X="23.087597621594" Y="-3.778106751805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.151833382014" Y="2.878421623876" />
                  <Point X="21.508224551637" Y="1.712718633512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.576189477073" Y="1.490415379182" />
                  <Point X="23.188969674046" Y="-3.784750951125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.223149129973" Y="2.97008716681" />
                  <Point X="23.290341726498" Y="-3.791395150444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.308851126446" Y="3.01469741112" />
                  <Point X="23.392859013443" Y="-3.801785243004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.429485601878" Y="2.945048665159" />
                  <Point X="23.509561225442" Y="-3.85857213482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.55012007731" Y="2.875399919198" />
                  <Point X="23.645294767925" Y="-3.977607703782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.86386473158" Y="-4.692517841724" />
                  <Point X="23.877179663471" Y="-4.736069021564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.670754418414" Y="2.805751612601" />
                  <Point X="23.791281157658" Y="-4.130178825019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.82643069503" Y="-4.24514778137" />
                  <Point X="23.984109825901" Y="-4.760892979462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.788948367057" Y="2.74408547008" />
                  <Point X="24.047588790336" Y="-4.643594472617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.893541754556" Y="2.726904758587" />
                  <Point X="24.093987921344" Y="-4.470430347885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.992752620476" Y="2.727329481894" />
                  <Point X="24.140387052352" Y="-4.297266223154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.081369822303" Y="2.762404519138" />
                  <Point X="24.18678618336" Y="-4.124102098423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.159576305623" Y="2.831531482273" />
                  <Point X="24.233185314368" Y="-3.950937973692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.983396415347" Y="3.732718781592" />
                  <Point X="22.052871350905" Y="3.505476506701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.230281807202" Y="2.925193051177" />
                  <Point X="24.279584445376" Y="-3.777773848961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.063873359844" Y="3.79441940084" />
                  <Point X="24.325983576384" Y="-3.60460972423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.144350304341" Y="3.856120020087" />
                  <Point X="24.375670602122" Y="-3.442199818585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.224827248839" Y="3.917820639335" />
                  <Point X="24.443445420166" Y="-3.338952415767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.305304193336" Y="3.979521258582" />
                  <Point X="24.512408162298" Y="-3.239590537563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.387665231302" Y="4.035059285775" />
                  <Point X="24.583419681416" Y="-3.146929906929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.472582142099" Y="4.082237429625" />
                  <Point X="24.665803823283" Y="-3.091467449192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.557499092449" Y="4.129415444102" />
                  <Point X="24.756086172637" Y="-3.061838864097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.642416045358" Y="4.176593450209" />
                  <Point X="24.846817676734" Y="-3.033679397966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.727332998268" Y="4.223771456316" />
                  <Point X="24.937607465311" Y="-3.005710571781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.816761404794" Y="4.256193162547" />
                  <Point X="25.034838956283" Y="-2.998811604742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.962526802403" Y="4.104344873977" />
                  <Point X="25.142674323677" Y="-3.026596354663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.079764362466" Y="4.045806937543" />
                  <Point X="25.252429398335" Y="-3.060660184115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.183967884405" Y="4.02990141884" />
                  <Point X="25.36386190228" Y="-3.100210637546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.276003045194" Y="4.053796816064" />
                  <Point X="25.510465689932" Y="-3.254801176381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.364071164709" Y="4.09066782063" />
                  <Point X="25.794430343245" Y="-3.858678862343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.445157337938" Y="4.150375742482" />
                  <Point X="25.724733060914" Y="-3.305780480046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.506691285678" Y="4.274036112277" />
                  <Point X="25.761815516383" Y="-3.102142882732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.522959819478" Y="4.545752979781" />
                  <Point X="25.81123069626" Y="-2.938843809342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.609522447499" Y="4.587548225141" />
                  <Point X="25.884827906596" Y="-2.854640593597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.701020350073" Y="4.613200914806" />
                  <Point X="25.96403416809" Y="-2.788783757523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.792518252646" Y="4.638853604471" />
                  <Point X="26.043240288761" Y="-2.722926460834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.88401615522" Y="4.664506294136" />
                  <Point X="26.129046941006" Y="-2.67865853013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.975514057794" Y="4.690158983801" />
                  <Point X="26.223755429227" Y="-2.663507192935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.067011960368" Y="4.715811673466" />
                  <Point X="26.320377798998" Y="-2.654615880221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.162833405633" Y="4.727322692198" />
                  <Point X="26.417218294245" Y="-2.646438023792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.258742344422" Y="4.738547532506" />
                  <Point X="26.522557229849" Y="-2.666057313256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.354651334988" Y="4.749772203462" />
                  <Point X="26.643683669917" Y="-2.737315203036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.450560325554" Y="4.760996874418" />
                  <Point X="26.767327172209" Y="-2.816806032382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.54646931612" Y="4.772221545374" />
                  <Point X="26.918658119787" Y="-2.986858414639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.832765240089" Y="4.160718616681" />
                  <Point X="27.08379554196" Y="-3.202069740479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.953143248458" Y="4.091908736683" />
                  <Point X="27.015457165962" Y="-2.653616140522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.09751887735" Y="-2.922027904093" />
                  <Point X="27.248932845589" Y="-3.417280678579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.052959572498" Y="4.090353095716" />
                  <Point X="27.03808874873" Y="-2.402711868393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.308676498752" Y="-3.287764519082" />
                  <Point X="27.414070149218" Y="-3.63249161668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.141226479117" Y="4.12657389696" />
                  <Point X="27.100926536395" Y="-2.283316166834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.519834120153" Y="-3.653501134072" />
                  <Point X="27.579207452847" Y="-3.84770255478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.215046497574" Y="4.210048340177" />
                  <Point X="27.170062857328" Y="-2.184522039303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.730990752987" Y="-4.019234515601" />
                  <Point X="27.73398748693" Y="-4.029036390667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.266288238145" Y="4.367373002737" />
                  <Point X="27.252450886228" Y="-2.129072295481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.3126875326" Y="4.540536592858" />
                  <Point X="27.338022809327" Y="-2.084036600332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.359086827055" Y="4.713700182979" />
                  <Point X="27.424468572266" Y="-2.041859106516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.438559893636" Y="4.778684338936" />
                  <Point X="27.51882084163" Y="-2.025542629939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.541186538187" Y="4.767936553765" />
                  <Point X="27.622424109077" Y="-2.03948480467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.643813182738" Y="4.757188768593" />
                  <Point X="27.727570422096" Y="-2.058474054047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.746439827289" Y="4.746440983422" />
                  <Point X="27.836150181944" Y="-2.088693601973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.850022593872" Y="4.732565863998" />
                  <Point X="27.95636600012" Y="-2.156972981756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.95728042402" Y="4.706670153289" />
                  <Point X="27.864085276854" Y="-1.53020749254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.882005648705" Y="-1.588822387733" />
                  <Point X="28.077000297332" Y="-2.226621144785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.064538174047" Y="4.680774704646" />
                  <Point X="27.882085284479" Y="-1.26415402073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.058289026804" Y="-1.840490492701" />
                  <Point X="28.197634624375" Y="-2.296269405388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.171795924073" Y="4.654879256003" />
                  <Point X="27.945376977111" Y="-1.146242975417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.188077578222" Y="-1.940080872069" />
                  <Point X="28.318268983912" Y="-2.365917772277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.2790536741" Y="4.62898380736" />
                  <Point X="28.018010457937" Y="-1.058887542485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.317866129639" Y="-2.039671251438" />
                  <Point X="28.43890334345" Y="-2.435566139166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.386311424126" Y="4.603088358717" />
                  <Point X="27.029906080702" Y="2.497985091014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.159035944479" Y="2.075620337954" />
                  <Point X="28.095400764518" Y="-0.987090985524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.447654681056" Y="-2.139261630806" />
                  <Point X="28.559537702988" Y="-2.505214506055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.495236156165" Y="4.571740457594" />
                  <Point X="27.087001279486" Y="2.636163954455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.281628686004" Y="1.999566392216" />
                  <Point X="28.183361830135" Y="-0.949869823439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.577443232474" Y="-2.238852010174" />
                  <Point X="28.680172062525" Y="-2.574862872944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.606966588329" Y="4.531215524873" />
                  <Point X="27.151949342243" Y="2.748657257204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.387461013714" Y="1.978333289888" />
                  <Point X="28.276512535168" Y="-0.929623207027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.707231783891" Y="-2.338442389543" />
                  <Point X="28.800806422063" Y="-2.644511239832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.718697143907" Y="4.49069018848" />
                  <Point X="27.216897405" Y="2.861150559953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.485893656346" Y="1.981303466877" />
                  <Point X="28.370026247315" Y="-0.910563933382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.837020335308" Y="-2.438032768911" />
                  <Point X="28.921440781601" Y="-2.714159606721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.830427699485" Y="4.450164852087" />
                  <Point X="27.281845461263" Y="2.973643883945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.578311032594" Y="2.003948693667" />
                  <Point X="28.470316085882" Y="-0.913668370582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.966808886725" Y="-2.537623148279" />
                  <Point X="29.024698523952" Y="-2.726971619786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.945299630833" Y="4.39936453853" />
                  <Point X="27.346793509735" Y="3.086137233418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.665191069198" Y="2.04470574233" />
                  <Point X="28.57382299697" Y="-0.927295377861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.061213737389" Y="4.345155423467" />
                  <Point X="27.411741558208" Y="3.19863058289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.749627577802" Y="2.093455210951" />
                  <Point X="28.364795807991" Y="0.081330594429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.505642247441" Y="-0.379357350851" />
                  <Point X="28.677329908058" Y="-0.94092238514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.177127843944" Y="4.290946308405" />
                  <Point X="27.47668960668" Y="3.311123932363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.834064086406" Y="2.142204679572" />
                  <Point X="28.024035951252" Y="1.520834708001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.169214500821" Y="1.045977068997" />
                  <Point X="28.421923903913" Y="0.219401856178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.628654640396" Y="-0.456783914571" />
                  <Point X="28.780836815299" Y="-0.954549379838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.294863160119" Y="4.23078028509" />
                  <Point X="27.541637655152" Y="3.423617281836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.91850059501" Y="2.190954148193" />
                  <Point X="28.085827895637" Y="1.643651208794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.288547921612" Y="0.980583881012" />
                  <Point X="28.494800028095" Y="0.305963638456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.740242136163" Y="-0.496841323403" />
                  <Point X="28.884343702731" Y="-0.968176309741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.415733084525" Y="4.160361420235" />
                  <Point X="27.606585703625" Y="3.536110631309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.002937103614" Y="2.239703616814" />
                  <Point X="28.158458924822" Y="1.731014660685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.394633491128" Y="0.958522462062" />
                  <Point X="28.576392555974" Y="0.364015348878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.848447035027" Y="-0.525834756304" />
                  <Point X="28.987850590163" Y="-0.981803239644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.536602985496" Y="4.089942632032" />
                  <Point X="27.671533752097" Y="3.648603980782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.087373612218" Y="2.288453085435" />
                  <Point X="28.238506287521" Y="1.794120378684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.495843234308" Y="0.952409152451" />
                  <Point X="28.661580428284" Y="0.410307217553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.956651933892" Y="-0.554828189205" />
                  <Point X="29.091357477594" Y="-0.995430169546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.658880845736" Y="4.014918616567" />
                  <Point X="27.736481800569" Y="3.761097330255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.171810120822" Y="2.337202554056" />
                  <Point X="28.318970560342" Y="1.855862445116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.591410397058" Y="0.964751891814" />
                  <Point X="28.752580736222" Y="0.437587465935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.064856832757" Y="-0.583821622105" />
                  <Point X="29.194864365026" Y="-1.009057099449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.785820910145" Y="3.924645218389" />
                  <Point X="27.801429849042" Y="3.873590679728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.256246629426" Y="2.385952022677" />
                  <Point X="28.399434833163" Y="1.917604511549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.686907337088" Y="0.977324319343" />
                  <Point X="28.844399623513" Y="0.462190261898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.173061731622" Y="-0.612815055006" />
                  <Point X="29.298371252458" Y="-1.022684029352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.34068313803" Y="2.434701491298" />
                  <Point X="28.479898999861" Y="1.979346925095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.782404277118" Y="0.989896746872" />
                  <Point X="28.936218510804" Y="0.486793057861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.281266630487" Y="-0.641808487906" />
                  <Point X="29.401878139889" Y="-1.036310959255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.425119646634" Y="2.483450959919" />
                  <Point X="28.56036313209" Y="2.041089451383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.877901217149" Y="1.0024691744" />
                  <Point X="29.028037394232" Y="0.51139586646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.389471529352" Y="-0.670801920807" />
                  <Point X="29.505385027321" Y="-1.049937889158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.509556155238" Y="2.532200428541" />
                  <Point X="28.640827264319" Y="2.102831977671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.973398157179" Y="1.015041601929" />
                  <Point X="29.11985627494" Y="0.535998683952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.497676428216" Y="-0.699795353708" />
                  <Point X="29.608891914753" Y="-1.063564819061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.593992663842" Y="2.580949897162" />
                  <Point X="28.721291396549" Y="2.164574503959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.068895097209" Y="1.027614029458" />
                  <Point X="29.211675155648" Y="0.560601501445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.605881327081" Y="-0.728788786608" />
                  <Point X="29.712398802184" Y="-1.077191748964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.678429172446" Y="2.629699365783" />
                  <Point X="28.801755528778" Y="2.226317030247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.16439203724" Y="1.040186456987" />
                  <Point X="29.303494036357" Y="0.585204318938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.71408622075" Y="-0.757782202515" />
                  <Point X="29.763413424083" Y="-0.919124214698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.76286568105" Y="2.678448834404" />
                  <Point X="28.882219661007" Y="2.288059556535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.25988897727" Y="1.052758884516" />
                  <Point X="29.395312917065" Y="0.609807136431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.847302204102" Y="2.727198255768" />
                  <Point X="28.962683793236" Y="2.349802082823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.3553859173" Y="1.065331312044" />
                  <Point X="29.487131797774" Y="0.634409953924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.931738741219" Y="2.775947631126" />
                  <Point X="29.043147925466" Y="2.411544609111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.450882857331" Y="1.077903739573" />
                  <Point X="29.578950678482" Y="0.659012771417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.103765023585" Y="2.538203858887" />
                  <Point X="29.123612057695" Y="2.473287135399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.546379797361" Y="1.090476167102" />
                  <Point X="29.67076955919" Y="0.683615588909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.641876727354" Y="1.103048627459" />
                  <Point X="29.762588439899" Y="0.708218406402" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.55848046875" Y="-3.861577880859" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.42878515625" Y="-3.470345458984" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.21934765625" Y="-3.249333496094" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.936349609375" Y="-3.204832763672" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.6761875" Y="-3.336842529297" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112060547" />
                  <Point X="24.2271328125" Y="-4.707636230469" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.132962890625" Y="-4.98333203125" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.837037109375" Y="-4.921928710938" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.680384765625" Y="-4.630532714844" />
                  <Point X="23.6908515625" Y="-4.5510390625" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.677181640625" Y="-4.468715332031" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.563091796875" Y="-4.158230957031" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.283568359375" Y="-3.981358886719" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.954134765625" Y="-4.011200927734" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.564181640625" Y="-4.386772949219" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.486005859375" Y="-4.379268554688" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="22.0573203125" Y="-4.100741699219" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.3323984375" Y="-2.90896484375" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597594482422" />
                  <Point X="22.4860234375" Y="-2.568765869141" />
                  <Point X="22.46867578125" Y="-2.551417236328" />
                  <Point X="22.439845703125" Y="-2.537198730469" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.398763671875" Y="-3.1263359375" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.1083671875" Y="-3.201946289062" />
                  <Point X="20.83830078125" Y="-2.847135742188" />
                  <Point X="20.77603515625" Y="-2.742725830078" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.553521484375" Y="-1.640062744141" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.8474609375" Y="-1.411086547852" />
                  <Point X="21.855853515625" Y="-1.389555053711" />
                  <Point X="21.861884765625" Y="-1.366263916016" />
                  <Point X="21.863349609375" Y="-1.350049560547" />
                  <Point X="21.85134375" Y="-1.321068481445" />
                  <Point X="21.83309375" Y="-1.307256713867" />
                  <Point X="21.812359375" Y="-1.295052978516" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.506005859375" Y="-1.456357666016" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.178234375" Y="-1.424713867188" />
                  <Point X="20.072607421875" Y="-1.011187744141" />
                  <Point X="20.056134765625" Y="-0.89601550293" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="21.1205078125" Y="-0.214933044434" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.464126953125" Y="-0.117245048523" />
                  <Point X="21.485857421875" Y="-0.102163482666" />
                  <Point X="21.497677734375" Y="-0.09064490509" />
                  <Point X="21.507111328125" Y="-0.069436729431" />
                  <Point X="21.514353515625" Y="-0.046098899841" />
                  <Point X="21.516599609375" Y="-0.031280040741" />
                  <Point X="21.512345703125" Y="-0.009991976738" />
                  <Point X="21.505103515625" Y="0.013345852852" />
                  <Point X="21.497677734375" Y="0.028084823608" />
                  <Point X="21.479833984375" Y="0.043784011841" />
                  <Point X="21.458103515625" Y="0.058865581512" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.28080078125" Y="0.377371643066" />
                  <Point X="20.001814453125" Y="0.452125976563" />
                  <Point X="20.01428125" Y="0.536375244141" />
                  <Point X="20.08235546875" Y="0.996414672852" />
                  <Point X="20.115515625" Y="1.118789916992" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="21.015896484375" Y="1.424370727539" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.281626953125" Y="1.400069946289" />
                  <Point X="21.32972265625" Y="1.415234008789" />
                  <Point X="21.348466796875" Y="1.426056396484" />
                  <Point X="21.3608828125" Y="1.443786743164" />
                  <Point X="21.366232421875" Y="1.456701416016" />
                  <Point X="21.385529296875" Y="1.503291137695" />
                  <Point X="21.389287109375" Y="1.524603515625" />
                  <Point X="21.3836875" Y="1.545507568359" />
                  <Point X="21.37723046875" Y="1.557912353516" />
                  <Point X="21.353943359375" Y="1.602642822266" />
                  <Point X="21.34003125" Y="1.619221923828" />
                  <Point X="20.673658203125" Y="2.130549072266" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.57547265625" Y="2.333830322266" />
                  <Point X="20.83998828125" Y="2.787007568359" />
                  <Point X="20.92782421875" Y="2.899911132812" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.699068359375" Y="3.008797119141" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.8800546875" Y="2.918810302734" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404296875" />
                  <Point X="21.999927734375" Y="2.940583496094" />
                  <Point X="22.04747265625" Y="2.988127685547" />
                  <Point X="22.0591015625" Y="3.006381835938" />
                  <Point X="22.061927734375" Y="3.027840576172" />
                  <Point X="22.060302734375" Y="3.046407958984" />
                  <Point X="22.054443359375" Y="3.113390136719" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.752642578125" Y="3.645491455078" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="21.786423828125" Y="3.821117431641" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.38546875" Y="4.251192382813" />
                  <Point X="22.858451171875" Y="4.513971679688" />
                  <Point X="22.988771484375" Y="4.344134765625" />
                  <Point X="23.032173828125" Y="4.287572753906" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.069419921875" Y="4.262902832031" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.207716796875" Y="4.231166503906" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744628906" />
                  <Point X="23.31391796875" Y="4.294488769531" />
                  <Point X="23.320923828125" Y="4.316708007812" />
                  <Point X="23.346197265625" Y="4.396865234375" />
                  <Point X="23.348083984375" Y="4.41842578125" />
                  <Point X="23.31451171875" Y="4.673424804688" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.435501953125" Y="4.736085449219" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.199619140625" Y="4.922924804688" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.915919921875" Y="4.467429199219" />
                  <Point X="24.957859375" Y="4.310903808594" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.2057578125" Y="4.875576660156" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.339458984375" Y="4.980102050781" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="25.9989609375" Y="4.892065917969" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.598583984375" Y="4.736368164062" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="27.0183671875" Y="4.574943359375" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.423072265625" Y="4.375979003906" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.81209375" Y="4.139105957031" />
                  <Point X="28.0687421875" Y="3.956592529297" />
                  <Point X="27.416859375" Y="2.827498046875" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.220193359375" Y="2.474090087891" />
                  <Point X="27.2033828125" Y="2.411228759766" />
                  <Point X="27.20386328125" Y="2.377253417969" />
                  <Point X="27.210416015625" Y="2.322902587891" />
                  <Point X="27.218681640625" Y="2.300812255859" />
                  <Point X="27.228005859375" Y="2.287072021484" />
                  <Point X="27.261640625" Y="2.237503662109" />
                  <Point X="27.288681640625" Y="2.214880126953" />
                  <Point X="27.33825" Y="2.18124609375" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.375404296875" Y="2.171163330078" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.46608984375" Y="2.170605957031" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.709517578125" Y="2.867041503906" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.01903515625" Y="2.996981933594" />
                  <Point X="29.20259765625" Y="2.741874511719" />
                  <Point X="29.246951171875" Y="2.668576660156" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.534826171875" Y="1.782004638672" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583832763672" />
                  <Point X="28.266830078125" Y="1.567472290039" />
                  <Point X="28.22158984375" Y="1.508451416016" />
                  <Point X="28.20844921875" Y="1.47479675293" />
                  <Point X="28.191595703125" Y="1.414536376953" />
                  <Point X="28.190779296875" Y="1.390965454102" />
                  <Point X="28.194615234375" Y="1.372380126953" />
                  <Point X="28.20844921875" Y="1.305332763672" />
                  <Point X="28.226076171875" Y="1.27210168457" />
                  <Point X="28.263703125" Y="1.214909790039" />
                  <Point X="28.280947265625" Y="1.198819946289" />
                  <Point X="28.2960625" Y="1.190311645508" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.38900390625" Y="1.150918701172" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.58578515625" Y="1.287303588867" />
                  <Point X="29.848974609375" Y="1.321953125" />
                  <Point X="29.8603515625" Y="1.275221191406" />
                  <Point X="29.93919140625" Y="0.951367248535" />
                  <Point X="29.95316796875" Y="0.861601806641" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="29.023328125" Y="0.313431396484" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819351196" />
                  <Point X="28.709009765625" Y="0.221213943481" />
                  <Point X="28.636578125" Y="0.179346832275" />
                  <Point X="28.61021875" Y="0.151576187134" />
                  <Point X="28.566759765625" Y="0.096198867798" />
                  <Point X="28.556986328125" Y="0.074735107422" />
                  <Point X="28.552970703125" Y="0.053767250061" />
                  <Point X="28.538484375" Y="-0.021875450134" />
                  <Point X="28.538484375" Y="-0.040684627533" />
                  <Point X="28.5425" Y="-0.061652484894" />
                  <Point X="28.556986328125" Y="-0.137295181274" />
                  <Point X="28.566759765625" Y="-0.15875894165" />
                  <Point X="28.578806640625" Y="-0.174109298706" />
                  <Point X="28.622265625" Y="-0.229486618042" />
                  <Point X="28.636578125" Y="-0.241906906128" />
                  <Point X="28.65665625" Y="-0.253512313843" />
                  <Point X="28.729087890625" Y="-0.295379425049" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.7590703125" Y="-0.573133239746" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.992451171875" Y="-0.674432312012" />
                  <Point X="29.948431640625" Y="-0.966412719727" />
                  <Point X="29.930525390625" Y="-1.044880004883" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.73920703125" Y="-1.140708251953" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.355431640625" Y="-1.10690625" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.185447265625" Y="-1.154697265625" />
                  <Point X="28.16162890625" Y="-1.183343383789" />
                  <Point X="28.075703125" Y="-1.286685424805" />
                  <Point X="28.064359375" Y="-1.314070800781" />
                  <Point X="28.0609453125" Y="-1.351168945312" />
                  <Point X="28.048630859375" Y="-1.485001220703" />
                  <Point X="28.056361328125" Y="-1.516622192383" />
                  <Point X="28.07816796875" Y="-1.55054284668" />
                  <Point X="28.156841796875" Y="-1.672912719727" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="29.11308203125" Y="-2.410373291016" />
                  <Point X="29.339076171875" Y="-2.583784179688" />
                  <Point X="29.32821875" Y="-2.601352539062" />
                  <Point X="29.204130859375" Y="-2.802142578125" />
                  <Point X="29.16709375" Y="-2.854767578125" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.04489453125" Y="-2.427478027344" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.690443359375" Y="-2.244842773438" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.4501171875" Y="-2.239750244141" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.33468359375" />
                  <Point X="27.268095703125" Y="-2.373645263672" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.1891640625" Y="-2.546374755859" />
                  <Point X="27.197634765625" Y="-2.593273925781" />
                  <Point X="27.22819140625" Y="-2.762465087891" />
                  <Point X="27.23409375" Y="-2.778578857422" />
                  <Point X="27.84110546875" Y="-3.829956542969" />
                  <Point X="27.98667578125" Y="-4.082087646484" />
                  <Point X="27.98253515625" Y="-4.085044433594" />
                  <Point X="27.83529296875" Y="-4.190215820312" />
                  <Point X="27.793888671875" Y="-4.217016113281" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="26.90557421875" Y="-3.281917724609" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.624294921875" Y="-2.950729248047" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.375216796875" Y="-2.840372314453" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.126271484375" Y="-2.900988525391" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045986083984" />
                  <Point X="25.95677734375" Y="-3.099721435547" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310719970703" />
                  <Point X="26.085953125" Y="-4.617363769531" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.95608984375" Y="-4.970196777344" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#138" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.041940917734" Y="4.511650117458" Z="0.55" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.55" />
                  <Point X="-0.812349928673" Y="5.003865791466" Z="0.55" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.55" />
                  <Point X="-1.584152528955" Y="4.815509069241" Z="0.55" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.55" />
                  <Point X="-1.741217125809" Y="4.69817964729" Z="0.55" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.55" />
                  <Point X="-1.732919404343" Y="4.363023419925" Z="0.55" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.55" />
                  <Point X="-1.817574798158" Y="4.30864029066" Z="0.55" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.55" />
                  <Point X="-1.913649924864" Y="4.338533584323" Z="0.55" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.55" />
                  <Point X="-1.977716765489" Y="4.405853366124" Z="0.55" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.55" />
                  <Point X="-2.644971511584" Y="4.326179708828" Z="0.55" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.55" />
                  <Point X="-3.250154269711" Y="3.892077472508" Z="0.55" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.55" />
                  <Point X="-3.296815524515" Y="3.651771614417" Z="0.55" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.55" />
                  <Point X="-2.995664451945" Y="3.073330797019" Z="0.55" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.55" />
                  <Point X="-3.04158439484" Y="3.007219173046" Z="0.55" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.55" />
                  <Point X="-3.121745668587" Y="2.999900247228" Z="0.55" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.55" />
                  <Point X="-3.28208773388" Y="3.083378424231" Z="0.55" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.55" />
                  <Point X="-4.117794784715" Y="2.961893681479" Z="0.55" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.55" />
                  <Point X="-4.473866727525" Y="2.390315334124" Z="0.55" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.55" />
                  <Point X="-4.362937058356" Y="2.122161408039" Z="0.55" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.55" />
                  <Point X="-3.673277036251" Y="1.566103559192" Z="0.55" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.55" />
                  <Point X="-3.686120532424" Y="1.507114645758" Z="0.55" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.55" />
                  <Point X="-3.739564416453" Y="1.479035985021" Z="0.55" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.55" />
                  <Point X="-3.983734922552" Y="1.505223055388" Z="0.55" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.55" />
                  <Point X="-4.938900564155" Y="1.163147408859" Z="0.55" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.55" />
                  <Point X="-5.041337259618" Y="0.574974237073" Z="0.55" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.55" />
                  <Point X="-4.738297182979" Y="0.360355379726" Z="0.55" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.55" />
                  <Point X="-3.554831217034" Y="0.033987505218" Z="0.55" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.55" />
                  <Point X="-3.541564562082" Y="0.006469164629" Z="0.55" />
                  <Point X="-3.539556741714" Y="0" Z="0.55" />
                  <Point X="-3.546800039053" Y="-0.023337786432" Z="0.55" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.55" />
                  <Point X="-3.570537378089" Y="-0.044888477916" Z="0.55" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.55" />
                  <Point X="-3.898590605794" Y="-0.13535667703" Z="0.55" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.55" />
                  <Point X="-4.999517852653" Y="-0.871814540093" Z="0.55" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.55" />
                  <Point X="-4.876367707698" Y="-1.405807924187" Z="0.55" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.55" />
                  <Point X="-4.49362510018" Y="-1.47464993571" Z="0.55" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.55" />
                  <Point X="-3.19842396453" Y="-1.319066981523" Z="0.55" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.55" />
                  <Point X="-3.19870865342" Y="-1.345740937502" Z="0.55" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.55" />
                  <Point X="-3.483073754893" Y="-1.569115093049" Z="0.55" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.55" />
                  <Point X="-4.273064701536" Y="-2.737055440736" Z="0.55" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.55" />
                  <Point X="-3.937575355076" Y="-3.200949429657" Z="0.55" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.55" />
                  <Point X="-3.582393525756" Y="-3.138357251903" Z="0.55" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.55" />
                  <Point X="-2.559256215936" Y="-2.569074253196" Z="0.55" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.55" />
                  <Point X="-2.717059761265" Y="-2.852684856736" Z="0.55" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.55" />
                  <Point X="-2.9793407862" Y="-4.109078853654" Z="0.55" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.55" />
                  <Point X="-2.546473956598" Y="-4.39049937596" Z="0.55" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.55" />
                  <Point X="-2.402307465941" Y="-4.385930785159" Z="0.55" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.55" />
                  <Point X="-2.024243820837" Y="-4.021494290865" Z="0.55" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.55" />
                  <Point X="-1.72585866486" Y="-3.999971945536" Z="0.55" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.55" />
                  <Point X="-1.47603185385" Y="-4.164542744976" Z="0.55" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.55" />
                  <Point X="-1.378015135464" Y="-4.447190143801" Z="0.55" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.55" />
                  <Point X="-1.375344095838" Y="-4.592726021557" Z="0.55" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.55" />
                  <Point X="-1.181578611776" Y="-4.939071446449" Z="0.55" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.55" />
                  <Point X="-0.882720927272" Y="-5.001135964097" Z="0.55" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.55" />
                  <Point X="-0.730727743045" Y="-4.689297171734" Z="0.55" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.55" />
                  <Point X="-0.288893792323" Y="-3.334071423541" Z="0.55" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.55" />
                  <Point X="-0.054987898336" Y="-3.221305582839" Z="0.55" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.55" />
                  <Point X="0.198371181025" Y="-3.265806462449" Z="0.55" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.55" />
                  <Point X="0.381552067109" Y="-3.467574332227" Z="0.55" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.55" />
                  <Point X="0.504027117356" Y="-3.843238858082" Z="0.55" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.55" />
                  <Point X="0.958869536907" Y="-4.988111799568" Z="0.55" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.55" />
                  <Point X="1.138194338975" Y="-4.950273085397" Z="0.55" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.55" />
                  <Point X="1.129368728641" Y="-4.579557529899" Z="0.55" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.55" />
                  <Point X="0.999480420662" Y="-3.079061313899" Z="0.55" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.55" />
                  <Point X="1.152080902669" Y="-2.9081549174" Z="0.55" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.55" />
                  <Point X="1.373642381586" Y="-2.858881749858" Z="0.55" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.55" />
                  <Point X="1.591098558143" Y="-2.961507360653" Z="0.55" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.55" />
                  <Point X="1.859748540357" Y="-3.281075552522" Z="0.55" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.55" />
                  <Point X="2.814902421543" Y="-4.227710037699" Z="0.55" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.55" />
                  <Point X="3.005441029374" Y="-4.094451444213" Z="0.55" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.55" />
                  <Point X="2.878250302367" Y="-3.773676241935" Z="0.55" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.55" />
                  <Point X="2.240681090934" Y="-2.553107464475" Z="0.55" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.55" />
                  <Point X="2.306184887768" Y="-2.365651961061" Z="0.55" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.55" />
                  <Point X="2.467246417311" Y="-2.252716422199" Z="0.55" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.55" />
                  <Point X="2.675399339038" Y="-2.262766918557" Z="0.55" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.55" />
                  <Point X="3.013737355172" Y="-2.439499197907" Z="0.55" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.55" />
                  <Point X="4.201826285361" Y="-2.852264662557" Z="0.55" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.55" />
                  <Point X="4.364594368324" Y="-2.596357815159" Z="0.55" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.55" />
                  <Point X="4.137362731491" Y="-2.33942547406" Z="0.55" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.55" />
                  <Point X="3.114070296159" Y="-1.492223081487" Z="0.55" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.55" />
                  <Point X="3.104577717351" Y="-1.324470156428" Z="0.55" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.55" />
                  <Point X="3.193917176404" Y="-1.184030266637" Z="0.55" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.55" />
                  <Point X="3.359893966189" Y="-1.1244854585" Z="0.55" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.55" />
                  <Point X="3.726525680672" Y="-1.159000517776" Z="0.55" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.55" />
                  <Point X="4.973113729206" Y="-1.024723940557" Z="0.55" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.55" />
                  <Point X="5.03573596262" Y="-0.650606336148" Z="0.55" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.55" />
                  <Point X="4.765855503511" Y="-0.493556858619" Z="0.55" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.55" />
                  <Point X="3.675520659643" Y="-0.178943619435" Z="0.55" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.55" />
                  <Point X="3.61198349068" Y="-0.111960943265" Z="0.55" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.55" />
                  <Point X="3.585450315706" Y="-0.020967908825" Z="0.55" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.55" />
                  <Point X="3.595921134719" Y="0.075642622408" Z="0.55" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.55" />
                  <Point X="3.64339594772" Y="0.15198779531" Z="0.55" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.55" />
                  <Point X="3.727874754709" Y="0.209205224807" Z="0.55" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.55" />
                  <Point X="4.030112313795" Y="0.29641507415" Z="0.55" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.55" />
                  <Point X="4.996416004442" Y="0.900573976922" Z="0.55" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.55" />
                  <Point X="4.902777105896" Y="1.318328106056" Z="0.55" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.55" />
                  <Point X="4.57310212936" Y="1.368155861545" Z="0.55" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.55" />
                  <Point X="3.389396848246" Y="1.23176768264" Z="0.55" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.55" />
                  <Point X="3.314433348405" Y="1.265162709787" Z="0.55" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.55" />
                  <Point X="3.261691188533" Y="1.330862969569" Z="0.55" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.55" />
                  <Point X="3.237426824367" Y="1.413763852676" Z="0.55" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.55" />
                  <Point X="3.250444525452" Y="1.492609679273" Z="0.55" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.55" />
                  <Point X="3.300357379864" Y="1.568334666135" Z="0.55" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.55" />
                  <Point X="3.559106135539" Y="1.773617119316" Z="0.55" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.55" />
                  <Point X="4.283572140891" Y="2.725742972922" Z="0.55" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.55" />
                  <Point X="4.053464772552" Y="3.057465181847" Z="0.55" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.55" />
                  <Point X="3.678361113791" Y="2.941622815763" Z="0.55" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.55" />
                  <Point X="2.44701662016" Y="2.2501888373" Z="0.55" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.55" />
                  <Point X="2.375234444539" Y="2.252083647671" Z="0.55" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.55" />
                  <Point X="2.310598307042" Y="2.287534762905" Z="0.55" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.55" />
                  <Point X="2.26322383733" Y="2.34642655334" Z="0.55" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.55" />
                  <Point X="2.247346055088" Y="2.414523999293" Z="0.55" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.55" />
                  <Point X="2.262339105358" Y="2.492452955722" Z="0.55" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.55" />
                  <Point X="2.454002537097" Y="2.833777968164" Z="0.55" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.55" />
                  <Point X="2.834914070046" Y="4.211133275905" Z="0.55" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.55" />
                  <Point X="2.442085335172" Y="4.450461647292" Z="0.55" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.55" />
                  <Point X="2.033391560302" Y="4.651515612293" Z="0.55" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.55" />
                  <Point X="1.609475445857" Y="4.814652303894" Z="0.55" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.55" />
                  <Point X="1.004538669664" Y="4.971949619632" Z="0.55" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.55" />
                  <Point X="0.338509439574" Y="5.061109919958" Z="0.55" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.55" />
                  <Point X="0.151303643819" Y="4.919797388493" Z="0.55" />
                  <Point X="0" Y="4.355124473572" Z="0.55" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>