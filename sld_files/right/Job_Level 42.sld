<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#199" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3017" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.88680859375" Y="-4.719858398438" />
                  <Point X="25.5633046875" Y="-3.512524902344" />
                  <Point X="25.55772265625" Y="-3.497140625" />
                  <Point X="25.542365234375" Y="-3.467376708984" />
                  <Point X="25.410140625" Y="-3.276866699219" />
                  <Point X="25.37863671875" Y="-3.231476318359" />
                  <Point X="25.356751953125" Y="-3.209020263672" />
                  <Point X="25.33049609375" Y="-3.189776611328" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.09788671875" Y="-3.112165771484" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.75856640625" Y="-3.1605390625" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.189776611328" />
                  <Point X="24.655560546875" Y="-3.209020263672" />
                  <Point X="24.63367578125" Y="-3.231477050781" />
                  <Point X="24.501453125" Y="-3.421987060547" />
                  <Point X="24.46994921875" Y="-3.467377441406" />
                  <Point X="24.4618125" Y="-3.481572265625" />
                  <Point X="24.449009765625" Y="-3.512524169922" />
                  <Point X="24.371931640625" Y="-3.800180175781" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.974056640625" Y="-4.85571484375" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.75358203125" Y="-4.802362792969" />
                  <Point X="23.7850390625" Y="-4.563435058594" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.734609375" Y="-4.270481445312" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.18296484375" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.487978515625" Y="-3.966000244141" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.106953125" Y="-3.874579101562" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.016580078125" Y="-3.873708984375" />
                  <Point X="22.98553125" Y="-3.882029052734" />
                  <Point X="22.957341796875" Y="-3.894802001953" />
                  <Point X="22.74901171875" Y="-4.034003662109" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.664900390625" Y="-4.099461425781" />
                  <Point X="22.621626953125" Y="-4.15585546875" />
                  <Point X="22.519853515625" Y="-4.288489746094" />
                  <Point X="22.276552734375" Y="-4.137844726562" />
                  <Point X="22.19828515625" Y="-4.0893828125" />
                  <Point X="21.89527734375" Y="-3.856078125" />
                  <Point X="21.97504296875" Y="-3.717921142578" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647655273438" />
                  <Point X="22.593412109375" Y="-2.616129882812" />
                  <Point X="22.59442578125" Y="-2.585197753906" />
                  <Point X="22.585443359375" Y="-2.555581298828" />
                  <Point X="22.571228515625" Y="-2.526752685547" />
                  <Point X="22.553201171875" Y="-2.501592285156" />
                  <Point X="22.535853515625" Y="-2.484243652344" />
                  <Point X="22.5106953125" Y="-2.46621484375" />
                  <Point X="22.481865234375" Y="-2.451996826172" />
                  <Point X="22.452248046875" Y="-2.443011962891" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="22.112720703125" Y="-2.604436523438" />
                  <Point X="21.1819765625" Y="-3.141801513672" />
                  <Point X="20.978970703125" Y="-2.87509375" />
                  <Point X="20.917142578125" Y="-2.79386328125" />
                  <Point X="20.693857421875" Y="-2.419450195312" />
                  <Point X="20.8421484375" Y="-2.305662841797" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.915421875" Y="-1.475592773438" />
                  <Point X="21.93338671875" Y="-1.448461303711" />
                  <Point X="21.946142578125" Y="-1.419834350586" />
                  <Point X="21.952365234375" Y="-1.395810791016" />
                  <Point X="21.95384765625" Y="-1.390087158203" />
                  <Point X="21.95665234375" Y="-1.359656005859" />
                  <Point X="21.954443359375" Y="-1.327985473633" />
                  <Point X="21.947443359375" Y="-1.298240966797" />
                  <Point X="21.931361328125" Y="-1.2722578125" />
                  <Point X="21.910529296875" Y="-1.248301513672" />
                  <Point X="21.88702734375" Y="-1.228767089844" />
                  <Point X="21.865640625" Y="-1.2161796875" />
                  <Point X="21.860544921875" Y="-1.213180419922" />
                  <Point X="21.831283203125" Y="-1.201956665039" />
                  <Point X="21.799396484375" Y="-1.195474975586" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="21.454873046875" Y="-1.23561730957" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.190107421875" Y="-1.087331298828" />
                  <Point X="20.165923828125" Y="-0.992650390625" />
                  <Point X="20.107576171875" Y="-0.584698425293" />
                  <Point X="20.26885546875" Y="-0.541483825684" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.482509765625" Y="-0.214826828003" />
                  <Point X="21.512271484375" Y="-0.199470199585" />
                  <Point X="21.534685546875" Y="-0.183914260864" />
                  <Point X="21.540025390625" Y="-0.180207992554" />
                  <Point X="21.56248046875" Y="-0.158323776245" />
                  <Point X="21.581724609375" Y="-0.132068588257" />
                  <Point X="21.595833984375" Y="-0.104065689087" />
                  <Point X="21.6033046875" Y="-0.079994010925" />
                  <Point X="21.60509375" Y="-0.07422278595" />
                  <Point X="21.609353515625" Y="-0.046095420837" />
                  <Point X="21.609353515625" Y="-0.016459867477" />
                  <Point X="21.605083984375" Y="0.011698616028" />
                  <Point X="21.59761328125" Y="0.035770446777" />
                  <Point X="21.595830078125" Y="0.041513889313" />
                  <Point X="21.58173046875" Y="0.06949659729" />
                  <Point X="21.562486328125" Y="0.095756637573" />
                  <Point X="21.540025390625" Y="0.117647850037" />
                  <Point X="21.517611328125" Y="0.133203781128" />
                  <Point X="21.507447265625" Y="0.139353271484" />
                  <Point X="21.4865390625" Y="0.150280349731" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="21.18162890625" Y="0.234346725464" />
                  <Point X="20.10818359375" Y="0.521975463867" />
                  <Point X="20.159927734375" Y="0.871656860352" />
                  <Point X="20.17551171875" Y="0.976968200684" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.37538671875" Y="1.412875854492" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255015625" Y="1.299341674805" />
                  <Point X="21.27657421875" Y="1.301227661133" />
                  <Point X="21.29686328125" Y="1.305263427734" />
                  <Point X="21.34646875" Y="1.320904541016" />
                  <Point X="21.3582890625" Y="1.324630981445" />
                  <Point X="21.37721875" Y="1.332959838867" />
                  <Point X="21.39596484375" Y="1.343782104492" />
                  <Point X="21.412646484375" Y="1.356012695312" />
                  <Point X="21.42628515625" Y="1.371563842773" />
                  <Point X="21.438701171875" Y="1.389294555664" />
                  <Point X="21.44865234375" Y="1.407432983398" />
                  <Point X="21.468556640625" Y="1.45548815918" />
                  <Point X="21.473298828125" Y="1.46693737793" />
                  <Point X="21.4790859375" Y="1.486797241211" />
                  <Point X="21.48284375" Y="1.508112426758" />
                  <Point X="21.484197265625" Y="1.528755004883" />
                  <Point X="21.481048828125" Y="1.549200927734" />
                  <Point X="21.4754453125" Y="1.570107299805" />
                  <Point X="21.46794921875" Y="1.58937890625" />
                  <Point X="21.443931640625" Y="1.635516113281" />
                  <Point X="21.438208984375" Y="1.646508666992" />
                  <Point X="21.426716796875" Y="1.663708496094" />
                  <Point X="21.4128046875" Y="1.680287719727" />
                  <Point X="21.39786328125" Y="1.69458996582" />
                  <Point X="21.234103515625" Y="1.820248657227" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.85829296875" Y="2.629915771484" />
                  <Point X="20.9188515625" Y="2.733664794922" />
                  <Point X="21.247369140625" Y="3.155929931641" />
                  <Point X="21.26580078125" Y="3.149248291016" />
                  <Point X="21.79334375" Y="2.844671386719" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.922294921875" Y="2.819751953125" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.00098828125" Y="2.826504882812" />
                  <Point X="22.0195390625" Y="2.835654541016" />
                  <Point X="22.03779296875" Y="2.847284179688" />
                  <Point X="22.053921875" Y="2.860228759766" />
                  <Point X="22.102962890625" Y="2.909268310547" />
                  <Point X="22.114646484375" Y="2.920952148438" />
                  <Point X="22.12759375" Y="2.937083740234" />
                  <Point X="22.13922265625" Y="2.955336914062" />
                  <Point X="22.14837109375" Y="2.973886474609" />
                  <Point X="22.1532890625" Y="2.993976318359" />
                  <Point X="22.156115234375" Y="3.015433837891" />
                  <Point X="22.15656640625" Y="3.036119628906" />
                  <Point X="22.150521484375" Y="3.105208251953" />
                  <Point X="22.14908203125" Y="3.121669189453" />
                  <Point X="22.145044921875" Y="3.141958251953" />
                  <Point X="22.13853515625" Y="3.162602539062" />
                  <Point X="22.130205078125" Y="3.181533447266" />
                  <Point X="22.05763671875" Y="3.307224121094" />
                  <Point X="21.81666796875" Y="3.724595947266" />
                  <Point X="22.193916015625" Y="4.013827880859" />
                  <Point X="22.29937890625" Y="4.094685791016" />
                  <Point X="22.816787109375" Y="4.382146972656" />
                  <Point X="22.83296484375" Y="4.391134277344" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971111328125" Y="4.214796875" />
                  <Point X="22.987693359375" Y="4.200883789062" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.081783203125" Y="4.149365234375" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.119384765625" Y="4.132330078125" />
                  <Point X="23.1402890625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.202685546875" Y="4.128692871094" />
                  <Point X="23.222546875" Y="4.134481933594" />
                  <Point X="23.30263671875" Y="4.167657226563" />
                  <Point X="23.321720703125" Y="4.175561523437" />
                  <Point X="23.339853515625" Y="4.185509277344" />
                  <Point X="23.357583984375" Y="4.197923828125" />
                  <Point X="23.37313671875" Y="4.211562011719" />
                  <Point X="23.3853671875" Y="4.228243164063" />
                  <Point X="23.396189453125" Y="4.246987304688" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.43058984375" Y="4.348599609375" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410145507812" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.434021484375" Y="4.493492675781" />
                  <Point X="23.415798828125" Y="4.631897460938" />
                  <Point X="23.913841796875" Y="4.77153125" />
                  <Point X="24.050369140625" Y="4.80980859375" />
                  <Point X="24.677623046875" Y="4.883219726562" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.71003515625" Y="4.868745605469" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.183400390625" Y="4.425083496094" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.724828125" Y="4.844223632812" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.3629921875" Y="4.706448242188" />
                  <Point X="26.48102734375" Y="4.677950683594" />
                  <Point X="26.818529296875" Y="4.555536621094" />
                  <Point X="26.89464453125" Y="4.527929199219" />
                  <Point X="27.221271484375" Y="4.375176757813" />
                  <Point X="27.294576171875" Y="4.34089453125" />
                  <Point X="27.6101328125" Y="4.15705078125" />
                  <Point X="27.680978515625" Y="4.115775878906" />
                  <Point X="27.94326171875" Y="3.929254150391" />
                  <Point X="27.843421875" Y="3.756326904297" />
                  <Point X="27.147583984375" Y="2.551098144531" />
                  <Point X="27.142078125" Y="2.539932373047" />
                  <Point X="27.133078125" Y="2.516057861328" />
                  <Point X="27.11573828125" Y="2.451219726562" />
                  <Point X="27.1136953125" Y="2.441612060547" />
                  <Point X="27.1082265625" Y="2.407260742188" />
                  <Point X="27.107728515625" Y="2.380949951172" />
                  <Point X="27.114490234375" Y="2.324883544922" />
                  <Point X="27.116099609375" Y="2.311531494141" />
                  <Point X="27.121439453125" Y="2.289618408203" />
                  <Point X="27.129705078125" Y="2.267524658203" />
                  <Point X="27.140072265625" Y="2.247469970703" />
                  <Point X="27.174765625" Y="2.196342773437" />
                  <Point X="27.180859375" Y="2.188313232422" />
                  <Point X="27.20242578125" Y="2.162831054688" />
                  <Point X="27.221599609375" Y="2.145592041016" />
                  <Point X="27.2727265625" Y="2.110899902344" />
                  <Point X="27.284908203125" Y="2.102634521484" />
                  <Point X="27.304955078125" Y="2.092270996094" />
                  <Point X="27.32704296875" Y="2.084005615234" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.40503125" Y="2.071902832031" />
                  <Point X="27.415599609375" Y="2.071222900391" />
                  <Point X="27.447859375" Y="2.070949462891" />
                  <Point X="27.47320703125" Y="2.074170898438" />
                  <Point X="27.538044921875" Y="2.091509521484" />
                  <Point X="27.543412109375" Y="2.093115722656" />
                  <Point X="27.5709453125" Y="2.102248535156" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="27.875689453125" Y="2.27593359375" />
                  <Point X="28.967326171875" Y="2.906190429688" />
                  <Point X="29.08124609375" Y="2.747866455078" />
                  <Point X="29.123279296875" Y="2.689449951172" />
                  <Point X="29.262201171875" Y="2.459883544922" />
                  <Point X="29.146923828125" Y="2.371427734375" />
                  <Point X="28.23078515625" Y="1.668451171875" />
                  <Point X="28.221423828125" Y="1.660239379883" />
                  <Point X="28.20397265625" Y="1.641626098633" />
                  <Point X="28.15730859375" Y="1.580749023438" />
                  <Point X="28.152064453125" Y="1.573173339844" />
                  <Point X="28.132478515625" Y="1.541722045898" />
                  <Point X="28.121630859375" Y="1.51708984375" />
                  <Point X="28.104248046875" Y="1.454934326172" />
                  <Point X="28.10010546875" Y="1.440125366211" />
                  <Point X="28.09665234375" Y="1.417826660156" />
                  <Point X="28.0958359375" Y="1.394251342773" />
                  <Point X="28.097740234375" Y="1.371765625" />
                  <Point X="28.112009765625" Y="1.302609741211" />
                  <Point X="28.114388671875" Y="1.293424438477" />
                  <Point X="28.124986328125" Y="1.259573608398" />
                  <Point X="28.136283203125" Y="1.235742431641" />
                  <Point X="28.17509375" Y="1.176751708984" />
                  <Point X="28.18433984375" Y="1.162696777344" />
                  <Point X="28.198892578125" Y="1.145451660156" />
                  <Point X="28.216138671875" Y="1.129359985352" />
                  <Point X="28.23434765625" Y="1.116034545898" />
                  <Point X="28.29058984375" Y="1.084375" />
                  <Point X="28.299548828125" Y="1.079936035156" />
                  <Point X="28.330923828125" Y="1.066395751953" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.432162109375" Y="1.049388549805" />
                  <Point X="28.437328125" Y="1.048848999023" />
                  <Point X="28.4685234375" Y="1.046451049805" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.760982421875" Y="1.082896484375" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.82788671875" Y="1.006950073242" />
                  <Point X="29.84594140625" Y="0.93278692627" />
                  <Point X="29.8908671875" Y="0.644238952637" />
                  <Point X="29.766494140625" Y="0.610913024902" />
                  <Point X="28.716580078125" Y="0.32958972168" />
                  <Point X="28.70479296875" Y="0.325586883545" />
                  <Point X="28.681546875" Y="0.315068237305" />
                  <Point X="28.606837890625" Y="0.271884490967" />
                  <Point X="28.589037109375" Y="0.261595672607" />
                  <Point X="28.574310546875" Y="0.251095123291" />
                  <Point X="28.54753125" Y="0.225576278687" />
                  <Point X="28.502705078125" Y="0.16845741272" />
                  <Point X="28.492025390625" Y="0.154848480225" />
                  <Point X="28.480302734375" Y="0.135569900513" />
                  <Point X="28.470529296875" Y="0.114108100891" />
                  <Point X="28.463681640625" Y="0.09260471344" />
                  <Point X="28.448740234375" Y="0.014583392143" />
                  <Point X="28.44747265625" Y="0.005718378067" />
                  <Point X="28.443912109375" Y="-0.03168277359" />
                  <Point X="28.4451796875" Y="-0.058554225922" />
                  <Point X="28.46012109375" Y="-0.136575546265" />
                  <Point X="28.463681640625" Y="-0.155164703369" />
                  <Point X="28.470529296875" Y="-0.176668243408" />
                  <Point X="28.480302734375" Y="-0.1981300354" />
                  <Point X="28.492025390625" Y="-0.217408630371" />
                  <Point X="28.5368515625" Y="-0.27452734375" />
                  <Point X="28.54305859375" Y="-0.281671813965" />
                  <Point X="28.56805078125" Y="-0.307702209473" />
                  <Point X="28.589037109375" Y="-0.324155670166" />
                  <Point X="28.66374609375" Y="-0.367339447021" />
                  <Point X="28.681546875" Y="-0.377628082275" />
                  <Point X="28.6927109375" Y="-0.383139312744" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="28.96673046875" Y="-0.459177185059" />
                  <Point X="29.891474609375" Y="-0.706961853027" />
                  <Point X="29.8651015625" Y="-0.881881530762" />
                  <Point X="29.855025390625" Y="-0.948725036621" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="29.641986328125" Y="-1.163741577148" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.22803125" Y="-1.037379394531" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.163974609375" Y="-1.056596923828" />
                  <Point X="28.136146484375" Y="-1.073490356445" />
                  <Point X="28.1123984375" Y="-1.093960571289" />
                  <Point X="28.02376953125" Y="-1.200552368164" />
                  <Point X="28.002654296875" Y="-1.225948608398" />
                  <Point X="27.9879296875" Y="-1.250336181641" />
                  <Point X="27.976587890625" Y="-1.277722045898" />
                  <Point X="27.969759765625" Y="-1.305366333008" />
                  <Point X="27.957056640625" Y="-1.443407592773" />
                  <Point X="27.95403125" Y="-1.47629699707" />
                  <Point X="27.956349609375" Y="-1.507563842773" />
                  <Point X="27.964080078125" Y="-1.53918359375" />
                  <Point X="27.976451171875" Y="-1.567996826172" />
                  <Point X="28.05759765625" Y="-1.69421496582" />
                  <Point X="28.076931640625" Y="-1.724287475586" />
                  <Point X="28.0869375" Y="-1.737242919922" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.34276953125" Y="-1.939036743164" />
                  <Point X="29.213123046875" Y="-2.606882080078" />
                  <Point X="29.15321875" Y="-2.703815917969" />
                  <Point X="29.124794921875" Y="-2.749806884766" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="28.885396484375" Y="-2.803045410156" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.579712890625" Y="-2.12830859375" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395507812" />
                  <Point X="27.474609375" Y="-2.125353515625" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.299857421875" Y="-2.211477050781" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246549072266" />
                  <Point X="27.22142578125" Y="-2.267508056641" />
                  <Point X="27.204533203125" Y="-2.290438232422" />
                  <Point X="27.128232421875" Y="-2.435414550781" />
                  <Point X="27.110052734375" Y="-2.469956298828" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531909423828" />
                  <Point X="27.09567578125" Y="-2.563259521484" />
                  <Point X="27.127193359375" Y="-2.737771240234" />
                  <Point X="27.134703125" Y="-2.779349853516" />
                  <Point X="27.13898828125" Y="-2.795141601562" />
                  <Point X="27.1518203125" Y="-2.826078125" />
                  <Point X="27.3009921875" Y="-3.084454101562" />
                  <Point X="27.86128515625" Y="-4.054905517578" />
                  <Point X="27.8155625" Y="-4.087563232422" />
                  <Point X="27.781822265625" Y="-4.111662597656" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="27.586076171875" Y="-4.012710693359" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922177490234" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.54980859375" Y="-2.789902587891" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479990234375" Y="-2.751167236328" />
                  <Point X="26.448369140625" Y="-2.743435546875" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.22886328125" Y="-2.758438476562" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397705078" />
                  <Point X="26.128978515625" Y="-2.780740966797" />
                  <Point X="26.10459765625" Y="-2.7954609375" />
                  <Point X="25.959244140625" Y="-2.916316650391" />
                  <Point X="25.92461328125" Y="-2.945111328125" />
                  <Point X="25.904142578125" Y="-2.968860107422" />
                  <Point X="25.88725" Y="-2.996685791016" />
                  <Point X="25.875625" Y="-3.025808105469" />
                  <Point X="25.832166015625" Y="-3.225756591797" />
                  <Point X="25.821810546875" Y="-3.273395996094" />
                  <Point X="25.819724609375" Y="-3.289626953125" />
                  <Point X="25.8197421875" Y="-3.323120361328" />
                  <Point X="25.862017578125" Y="-3.644227783203" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="26.0075703125" Y="-4.863092773438" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.992158203125" Y="-4.762455566406" />
                  <Point X="23.941580078125" Y="-4.752637695312" />
                  <Point X="23.85875390625" Y="-4.731327636719" />
                  <Point X="23.8792265625" Y="-4.575835449219" />
                  <Point X="23.879923828125" Y="-4.568096191406" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688964844" />
                  <Point X="23.827783203125" Y="-4.251947265625" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.811873046875" Y="-4.178467773438" />
                  <Point X="23.800970703125" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135463867188" />
                  <Point X="23.778259765625" Y="-4.107626464844" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.74979296875" Y="-4.070937744141" />
                  <Point X="23.738994140625" Y="-4.059779541016" />
                  <Point X="23.5506171875" Y="-3.894575683594" />
                  <Point X="23.505734375" Y="-3.85521484375" />
                  <Point X="23.49326171875" Y="-3.845965332031" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.113166015625" Y="-3.779782470703" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.007267578125" Y="-3.779166503906" />
                  <Point X="22.991990234375" Y="-3.781946533203" />
                  <Point X="22.96094140625" Y="-3.790266601562" />
                  <Point X="22.946322265625" Y="-3.795497558594" />
                  <Point X="22.9181328125" Y="-3.808270507812" />
                  <Point X="22.9045625" Y="-3.8158125" />
                  <Point X="22.696232421875" Y="-3.955014160156" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.619556640625" Y="-4.010133789063" />
                  <Point X="22.5972421875" Y="-4.032772216797" />
                  <Point X="22.589533203125" Y="-4.041628417969" />
                  <Point X="22.546259765625" Y="-4.098022460938" />
                  <Point X="22.49680078125" Y="-4.162479492188" />
                  <Point X="22.326564453125" Y="-4.057073974609" />
                  <Point X="22.25240625" Y="-4.011156738281" />
                  <Point X="22.01913671875" Y="-3.831547363281" />
                  <Point X="22.057314453125" Y="-3.765421386719" />
                  <Point X="22.658509765625" Y="-2.724119873047" />
                  <Point X="22.6651484375" Y="-2.7100859375" />
                  <Point X="22.67605078125" Y="-2.681121582031" />
                  <Point X="22.680314453125" Y="-2.666190917969" />
                  <Point X="22.6865859375" Y="-2.634665527344" />
                  <Point X="22.688361328125" Y="-2.619241455078" />
                  <Point X="22.689375" Y="-2.588309326172" />
                  <Point X="22.6853359375" Y="-2.557625244141" />
                  <Point X="22.676353515625" Y="-2.528008789062" />
                  <Point X="22.6706484375" Y="-2.513568359375" />
                  <Point X="22.65643359375" Y="-2.484739746094" />
                  <Point X="22.648453125" Y="-2.471422119141" />
                  <Point X="22.63042578125" Y="-2.44626171875" />
                  <Point X="22.62037890625" Y="-2.434418945313" />
                  <Point X="22.60303125" Y="-2.4170703125" />
                  <Point X="22.591189453125" Y="-2.407024169922" />
                  <Point X="22.56603125" Y="-2.388995361328" />
                  <Point X="22.55271484375" Y="-2.381012695312" />
                  <Point X="22.523884765625" Y="-2.366794677734" />
                  <Point X="22.509443359375" Y="-2.361087890625" />
                  <Point X="22.479826171875" Y="-2.352103027344" />
                  <Point X="22.449140625" Y="-2.348062744141" />
                  <Point X="22.418205078125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="22.065220703125" Y="-2.5221640625" />
                  <Point X="21.206912109375" Y="-3.017708496094" />
                  <Point X="21.054564453125" Y="-2.817555419922" />
                  <Point X="20.99598046875" Y="-2.740587646484" />
                  <Point X="20.818734375" Y="-2.443373779297" />
                  <Point X="20.89998046875" Y="-2.381031494141" />
                  <Point X="21.951876953125" Y="-1.573882080078" />
                  <Point X="21.96351953125" Y="-1.563308349609" />
                  <Point X="21.984896484375" Y="-1.540387695312" />
                  <Point X="21.994630859375" Y="-1.528040771484" />
                  <Point X="22.012595703125" Y="-1.500909301758" />
                  <Point X="22.020162109375" Y="-1.487127319336" />
                  <Point X="22.03291796875" Y="-1.458500488281" />
                  <Point X="22.038107421875" Y="-1.443655517578" />
                  <Point X="22.044330078125" Y="-1.419631713867" />
                  <Point X="22.048447265625" Y="-1.398806030273" />
                  <Point X="22.051251953125" Y="-1.368374755859" />
                  <Point X="22.051421875" Y="-1.353045898438" />
                  <Point X="22.049212890625" Y="-1.321375366211" />
                  <Point X="22.04691796875" Y="-1.30622277832" />
                  <Point X="22.03991796875" Y="-1.276478393555" />
                  <Point X="22.02822265625" Y="-1.248243530273" />
                  <Point X="22.012140625" Y="-1.222260375977" />
                  <Point X="22.003048828125" Y="-1.209920043945" />
                  <Point X="21.982216796875" Y="-1.185963745117" />
                  <Point X="21.97125390625" Y="-1.175243286133" />
                  <Point X="21.947751953125" Y="-1.155708862305" />
                  <Point X="21.93521484375" Y="-1.146895019531" />
                  <Point X="21.913828125" Y="-1.134307617188" />
                  <Point X="21.89456640625" Y="-1.124481445312" />
                  <Point X="21.8653046875" Y="-1.11325769043" />
                  <Point X="21.85020703125" Y="-1.108860473633" />
                  <Point X="21.8183203125" Y="-1.10237890625" />
                  <Point X="21.802703125" Y="-1.100532592773" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="21.44247265625" Y="-1.141430053711" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.28215234375" Y="-1.063820068359" />
                  <Point X="20.259240234375" Y="-0.974113830566" />
                  <Point X="20.213548828125" Y="-0.654654541016" />
                  <Point X="20.293443359375" Y="-0.633246765137" />
                  <Point X="21.491712890625" Y="-0.312171325684" />
                  <Point X="21.4995234375" Y="-0.30971282959" />
                  <Point X="21.526072265625" Y="-0.299250671387" />
                  <Point X="21.555833984375" Y="-0.283894042969" />
                  <Point X="21.5664375" Y="-0.277515594482" />
                  <Point X="21.5888515625" Y="-0.261959686279" />
                  <Point X="21.606330078125" Y="-0.248242385864" />
                  <Point X="21.62878515625" Y="-0.226358108521" />
                  <Point X="21.6391015625" Y="-0.214484924316" />
                  <Point X="21.658345703125" Y="-0.188229797363" />
                  <Point X="21.666564453125" Y="-0.174815246582" />
                  <Point X="21.680673828125" Y="-0.146812301636" />
                  <Point X="21.686564453125" Y="-0.132224227905" />
                  <Point X="21.69403515625" Y="-0.108152519226" />
                  <Point X="21.694044921875" Y="-0.108123092651" />
                  <Point X="21.6990234375" Y="-0.088447944641" />
                  <Point X="21.703283203125" Y="-0.060320617676" />
                  <Point X="21.704353515625" Y="-0.046095470428" />
                  <Point X="21.704353515625" Y="-0.016459917068" />
                  <Point X="21.703279296875" Y="-0.002218276262" />
                  <Point X="21.699009765625" Y="0.025940263748" />
                  <Point X="21.695814453125" Y="0.039857017517" />
                  <Point X="21.68834375" Y="0.063928871155" />
                  <Point X="21.688341796875" Y="0.063939277649" />
                  <Point X="21.680669921875" Y="0.084261528015" />
                  <Point X="21.6665703125" Y="0.112244247437" />
                  <Point X="21.658357421875" Y="0.125651069641" />
                  <Point X="21.63911328125" Y="0.151911102295" />
                  <Point X="21.62879296875" Y="0.163789047241" />
                  <Point X="21.60633203125" Y="0.18568031311" />
                  <Point X="21.59419140625" Y="0.195693328857" />
                  <Point X="21.57177734375" Y="0.211249237061" />
                  <Point X="21.55144921875" Y="0.22354838562" />
                  <Point X="21.530541015625" Y="0.23447543335" />
                  <Point X="21.52104296875" Y="0.238792922974" />
                  <Point X="21.50162890625" Y="0.246360961914" />
                  <Point X="21.491712890625" Y="0.249611343384" />
                  <Point X="21.206216796875" Y="0.326109619141" />
                  <Point X="20.2145546875" Y="0.591824768066" />
                  <Point X="20.253904296875" Y="0.857750671387" />
                  <Point X="20.26866796875" Y="0.957522094727" />
                  <Point X="20.3664140625" Y="1.318237304688" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364257812" />
                  <Point X="21.263294921875" Y="1.204703125" />
                  <Point X="21.284853515625" Y="1.206589111328" />
                  <Point X="21.295107421875" Y="1.208053100586" />
                  <Point X="21.315396484375" Y="1.212088989258" />
                  <Point X="21.325431640625" Y="1.214660766602" />
                  <Point X="21.375037109375" Y="1.230301757812" />
                  <Point X="21.396548828125" Y="1.23767565918" />
                  <Point X="21.415478515625" Y="1.246004516602" />
                  <Point X="21.424716796875" Y="1.250685913086" />
                  <Point X="21.443462890625" Y="1.261508178711" />
                  <Point X="21.45213671875" Y="1.26716796875" />
                  <Point X="21.468818359375" Y="1.27939855957" />
                  <Point X="21.4840703125" Y="1.293373046875" />
                  <Point X="21.497708984375" Y="1.308924194336" />
                  <Point X="21.504103515625" Y="1.317071655273" />
                  <Point X="21.51651953125" Y="1.334802368164" />
                  <Point X="21.521990234375" Y="1.343600341797" />
                  <Point X="21.53194140625" Y="1.361738769531" />
                  <Point X="21.536421875" Y="1.371079345703" />
                  <Point X="21.556326171875" Y="1.419134521484" />
                  <Point X="21.564505859375" Y="1.440359985352" />
                  <Point X="21.57029296875" Y="1.460219726562" />
                  <Point X="21.572642578125" Y="1.470303344727" />
                  <Point X="21.576400390625" Y="1.491618652344" />
                  <Point X="21.577640625" Y="1.501896728516" />
                  <Point X="21.578994140625" Y="1.522539306641" />
                  <Point X="21.57808984375" Y="1.543213500977" />
                  <Point X="21.57494140625" Y="1.563659423828" />
                  <Point X="21.572810546875" Y="1.573795410156" />
                  <Point X="21.56720703125" Y="1.594701904297" />
                  <Point X="21.563982421875" Y="1.604546020508" />
                  <Point X="21.556486328125" Y="1.623817626953" />
                  <Point X="21.55221484375" Y="1.633245117188" />
                  <Point X="21.528197265625" Y="1.679382324219" />
                  <Point X="21.51719921875" Y="1.699286499023" />
                  <Point X="21.50570703125" Y="1.716486328125" />
                  <Point X="21.499490234375" Y="1.724774536133" />
                  <Point X="21.485578125" Y="1.741353759766" />
                  <Point X="21.47849609375" Y="1.748914672852" />
                  <Point X="21.4635546875" Y="1.763217041016" />
                  <Point X="21.4556953125" Y="1.769958251953" />
                  <Point X="21.291935546875" Y="1.895616943359" />
                  <Point X="20.77238671875" Y="2.294280761719" />
                  <Point X="20.94033984375" Y="2.582026367188" />
                  <Point X="20.99771484375" Y="2.680322021484" />
                  <Point X="21.273662109375" Y="3.035013427734" />
                  <Point X="21.74584375" Y="2.762398925781" />
                  <Point X="21.75508203125" Y="2.757717285156" />
                  <Point X="21.774015625" Y="2.749385986328" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.914015625" Y="2.725113525391" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310546875" />
                  <Point X="22.023568359375" Y="2.734227294922" />
                  <Point X="22.04301171875" Y="2.7413046875" />
                  <Point X="22.0615625" Y="2.750454345703" />
                  <Point X="22.070583984375" Y="2.755533691406" />
                  <Point X="22.088837890625" Y="2.767163330078" />
                  <Point X="22.097255859375" Y="2.773194580078" />
                  <Point X="22.113384765625" Y="2.786139160156" />
                  <Point X="22.121095703125" Y="2.793052490234" />
                  <Point X="22.17013671875" Y="2.842092041016" />
                  <Point X="22.188734375" Y="2.861488525391" />
                  <Point X="22.201681640625" Y="2.877620117188" />
                  <Point X="22.20771484375" Y="2.8860390625" />
                  <Point X="22.21934375" Y="2.904292236328" />
                  <Point X="22.224423828125" Y="2.91331640625" />
                  <Point X="22.233572265625" Y="2.931865966797" />
                  <Point X="22.240646484375" Y="2.951297607422" />
                  <Point X="22.245564453125" Y="2.971387451172" />
                  <Point X="22.2474765625" Y="2.981571044922" />
                  <Point X="22.250302734375" Y="3.003028564453" />
                  <Point X="22.251091796875" Y="3.013362304688" />
                  <Point X="22.25154296875" Y="3.034048095703" />
                  <Point X="22.251205078125" Y="3.044400146484" />
                  <Point X="22.24516015625" Y="3.113488769531" />
                  <Point X="22.242255859375" Y="3.140208740234" />
                  <Point X="22.23821875" Y="3.160497802734" />
                  <Point X="22.235646484375" Y="3.170527832031" />
                  <Point X="22.22913671875" Y="3.191172119141" />
                  <Point X="22.225490234375" Y="3.200864501953" />
                  <Point X="22.21716015625" Y="3.219795410156" />
                  <Point X="22.2124765625" Y="3.229033935547" />
                  <Point X="22.139908203125" Y="3.354724609375" />
                  <Point X="21.94061328125" Y="3.699914794922" />
                  <Point X="22.25171875" Y="3.938436035156" />
                  <Point X="22.351634765625" Y="4.015041503906" />
                  <Point X="22.807474609375" Y="4.268296875" />
                  <Point X="22.881435546875" Y="4.171909179688" />
                  <Point X="22.888181640625" Y="4.164046386719" />
                  <Point X="22.90248828125" Y="4.149102050781" />
                  <Point X="22.910048828125" Y="4.142020507812" />
                  <Point X="22.926630859375" Y="4.128107421875" />
                  <Point X="22.934916015625" Y="4.121893554688" />
                  <Point X="22.952111328125" Y="4.110404296875" />
                  <Point X="22.961021484375" Y="4.10512890625" />
                  <Point X="23.037916015625" Y="4.065099365234" />
                  <Point X="23.05623828125" Y="4.055561767578" />
                  <Point X="23.065673828125" Y="4.051287353516" />
                  <Point X="23.084953125" Y="4.043789794922" />
                  <Point X="23.094796875" Y="4.040566894531" />
                  <Point X="23.115701171875" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834228516" />
                  <Point X="23.146283203125" Y="4.029688232422" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031377929688" />
                  <Point X="23.2191796875" Y="4.035135742188" />
                  <Point X="23.22926953125" Y="4.037488037109" />
                  <Point X="23.249130859375" Y="4.043277099609" />
                  <Point X="23.25890234375" Y="4.046713867188" />
                  <Point X="23.3389921875" Y="4.079889160156" />
                  <Point X="23.358076171875" Y="4.087793457031" />
                  <Point X="23.3674140625" Y="4.092271972656" />
                  <Point X="23.385546875" Y="4.102219726562" />
                  <Point X="23.394341796875" Y="4.107688964844" />
                  <Point X="23.412072265625" Y="4.120103515625" />
                  <Point X="23.42021875" Y="4.12649609375" />
                  <Point X="23.435771484375" Y="4.140134277344" />
                  <Point X="23.44975" Y="4.155389648437" />
                  <Point X="23.46198046875" Y="4.172070800781" />
                  <Point X="23.467638671875" Y="4.1807421875" />
                  <Point X="23.4784609375" Y="4.199486328125" />
                  <Point X="23.483142578125" Y="4.208723144531" />
                  <Point X="23.491474609375" Y="4.227657226562" />
                  <Point X="23.495125" Y="4.237354492188" />
                  <Point X="23.521193359375" Y="4.320032714844" />
                  <Point X="23.527404296875" Y="4.339730957031" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370049316406" />
                  <Point X="23.535474609375" Y="4.380302246094" />
                  <Point X="23.537361328125" Y="4.401864257812" />
                  <Point X="23.53769921875" Y="4.412217285156" />
                  <Point X="23.537248046875" Y="4.4328984375" />
                  <Point X="23.536458984375" Y="4.4432265625" />
                  <Point X="23.528208984375" Y="4.505892578125" />
                  <Point X="23.520736328125" Y="4.562654785156" />
                  <Point X="23.93948828125" Y="4.680058105469" />
                  <Point X="24.068830078125" Y="4.716320800781" />
                  <Point X="24.634779296875" Y="4.782557128906" />
                  <Point X="24.774333984375" Y="4.2617265625" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.247104980469" />
                  <Point X="25.23798046875" Y="4.2617265625" />
                  <Point X="25.2751640625" Y="4.400495117188" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.71493359375" Y="4.749740234375" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.340697265625" Y="4.6141015625" />
                  <Point X="26.453595703125" Y="4.586844238281" />
                  <Point X="26.78613671875" Y="4.466229492188" />
                  <Point X="26.858259765625" Y="4.440069824219" />
                  <Point X="27.18102734375" Y="4.289122558594" />
                  <Point X="27.250453125" Y="4.256654785156" />
                  <Point X="27.562310546875" Y="4.074965576172" />
                  <Point X="27.6294296875" Y="4.035861572266" />
                  <Point X="27.81778125" Y="3.901915771484" />
                  <Point X="27.761150390625" Y="3.803826904297" />
                  <Point X="27.0653125" Y="2.598598144531" />
                  <Point X="27.06237890625" Y="2.593112548828" />
                  <Point X="27.053185546875" Y="2.573442626953" />
                  <Point X="27.044185546875" Y="2.549568115234" />
                  <Point X="27.041302734375" Y="2.5406015625" />
                  <Point X="27.023962890625" Y="2.475763427734" />
                  <Point X="27.019876953125" Y="2.456548095703" />
                  <Point X="27.014408203125" Y="2.422196777344" />
                  <Point X="27.013244140625" Y="2.40905859375" />
                  <Point X="27.01274609375" Y="2.382747802734" />
                  <Point X="27.013412109375" Y="2.369575195312" />
                  <Point X="27.020173828125" Y="2.313508789062" />
                  <Point X="27.02380078125" Y="2.289039794922" />
                  <Point X="27.029140625" Y="2.267126708984" />
                  <Point X="27.032462890625" Y="2.256330566406" />
                  <Point X="27.040728515625" Y="2.234236816406" />
                  <Point X="27.045314453125" Y="2.223899169922" />
                  <Point X="27.055681640625" Y="2.203844482422" />
                  <Point X="27.061462890625" Y="2.194127441406" />
                  <Point X="27.09615625" Y="2.143000244141" />
                  <Point X="27.10834375" Y="2.126941162109" />
                  <Point X="27.12991015625" Y="2.101458984375" />
                  <Point X="27.13891015625" Y="2.092186279297" />
                  <Point X="27.158083984375" Y="2.074947265625" />
                  <Point X="27.1682578125" Y="2.066980957031" />
                  <Point X="27.219384765625" Y="2.03228894043" />
                  <Point X="27.24128125" Y="2.018244262695" />
                  <Point X="27.261328125" Y="2.007880737305" />
                  <Point X="27.27166015625" Y="2.003296508789" />
                  <Point X="27.293748046875" Y="1.99503112793" />
                  <Point X="27.30455078125" Y="1.991706542969" />
                  <Point X="27.32647265625" Y="1.986364501953" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.393658203125" Y="1.97758605957" />
                  <Point X="27.414794921875" Y="1.976226318359" />
                  <Point X="27.4470546875" Y="1.975952880859" />
                  <Point X="27.4598359375" Y="1.976707519531" />
                  <Point X="27.48518359375" Y="1.979928955078" />
                  <Point X="27.49775" Y="1.982395751953" />
                  <Point X="27.562587890625" Y="1.999734375" />
                  <Point X="27.573322265625" Y="2.002946777344" />
                  <Point X="27.60085546875" Y="2.012079589844" />
                  <Point X="27.6098515625" Y="2.015581054688" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="27.923189453125" Y="2.193661132813" />
                  <Point X="28.940404296875" Y="2.780950195312" />
                  <Point X="29.0041328125" Y="2.692381103516" />
                  <Point X="29.043958984375" Y="2.637032958984" />
                  <Point X="29.13688671875" Y="2.483470458984" />
                  <Point X="29.089091796875" Y="2.446796142578" />
                  <Point X="28.172953125" Y="1.743819580078" />
                  <Point X="28.168138671875" Y="1.739867919922" />
                  <Point X="28.152119140625" Y="1.725216186523" />
                  <Point X="28.13466796875" Y="1.706602905273" />
                  <Point X="28.12857421875" Y="1.699420410156" />
                  <Point X="28.08191015625" Y="1.638543457031" />
                  <Point X="28.071421875" Y="1.623392089844" />
                  <Point X="28.0518359375" Y="1.591940795898" />
                  <Point X="28.04553515625" Y="1.580010253906" />
                  <Point X="28.0346875" Y="1.555378173828" />
                  <Point X="28.030140625" Y="1.542676391602" />
                  <Point X="28.0127578125" Y="1.480520874023" />
                  <Point X="28.006224609375" Y="1.454663452148" />
                  <Point X="28.002771484375" Y="1.432364868164" />
                  <Point X="28.001708984375" Y="1.421114624023" />
                  <Point X="28.000892578125" Y="1.397539306641" />
                  <Point X="28.001173828125" Y="1.38623449707" />
                  <Point X="28.003078125" Y="1.363748901367" />
                  <Point X="28.004701171875" Y="1.352567871094" />
                  <Point X="28.018970703125" Y="1.283411987305" />
                  <Point X="28.023728515625" Y="1.265041137695" />
                  <Point X="28.034326171875" Y="1.231190429688" />
                  <Point X="28.039142578125" Y="1.218880615234" />
                  <Point X="28.050439453125" Y="1.195049438477" />
                  <Point X="28.056919921875" Y="1.183528076172" />
                  <Point X="28.09573046875" Y="1.124537475586" />
                  <Point X="28.111736328125" Y="1.101428833008" />
                  <Point X="28.1262890625" Y="1.08418359375" />
                  <Point X="28.13408203125" Y="1.07599206543" />
                  <Point X="28.151328125" Y="1.059900390625" />
                  <Point X="28.16003515625" Y="1.052695678711" />
                  <Point X="28.178244140625" Y="1.039370239258" />
                  <Point X="28.18774609375" Y="1.033249511719" />
                  <Point X="28.24398828125" Y="1.001590026855" />
                  <Point X="28.26190625" Y="0.992712036133" />
                  <Point X="28.29328125" Y="0.97917175293" />
                  <Point X="28.305638671875" Y="0.974822570801" />
                  <Point X="28.330833984375" Y="0.967865539551" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.41971484375" Y="0.955207458496" />
                  <Point X="28.430046875" Y="0.954128479004" />
                  <Point X="28.4612421875" Y="0.95173046875" />
                  <Point X="28.47109765625" Y="0.951485961914" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="28.7733828125" Y="0.98870916748" />
                  <Point X="29.704703125" Y="1.111319824219" />
                  <Point X="29.73558203125" Y="0.984479187012" />
                  <Point X="29.752689453125" Y="0.914205505371" />
                  <Point X="29.783873046875" Y="0.713921020508" />
                  <Point X="29.74190625" Y="0.70267590332" />
                  <Point X="28.6919921875" Y="0.42135269165" />
                  <Point X="28.68603125" Y="0.419544250488" />
                  <Point X="28.66562890625" Y="0.412138519287" />
                  <Point X="28.6423828125" Y="0.401619873047" />
                  <Point X="28.634005859375" Y="0.397316497803" />
                  <Point X="28.559296875" Y="0.354132781982" />
                  <Point X="28.54149609375" Y="0.343844055176" />
                  <Point X="28.5338828125" Y="0.338946044922" />
                  <Point X="28.5087734375" Y="0.319869262695" />
                  <Point X="28.481994140625" Y="0.294350402832" />
                  <Point X="28.472796875" Y="0.284226501465" />
                  <Point X="28.427970703125" Y="0.22710760498" />
                  <Point X="28.410853515625" Y="0.20420614624" />
                  <Point X="28.399130859375" Y="0.184927536011" />
                  <Point X="28.393845703125" Y="0.174941558838" />
                  <Point X="28.384072265625" Y="0.153479675293" />
                  <Point X="28.3800078125" Y="0.142934127808" />
                  <Point X="28.37316015625" Y="0.121430778503" />
                  <Point X="28.370376953125" Y="0.110472961426" />
                  <Point X="28.355435546875" Y="0.032451583862" />
                  <Point X="28.352900390625" Y="0.01472161293" />
                  <Point X="28.34933984375" Y="-0.022679637909" />
                  <Point X="28.349017578125" Y="-0.036159137726" />
                  <Point X="28.35028515625" Y="-0.063030605316" />
                  <Point X="28.351875" Y="-0.076422424316" />
                  <Point X="28.36681640625" Y="-0.15444380188" />
                  <Point X="28.37316015625" Y="-0.183990478516" />
                  <Point X="28.3800078125" Y="-0.205494125366" />
                  <Point X="28.384072265625" Y="-0.216039810181" />
                  <Point X="28.393845703125" Y="-0.237501708984" />
                  <Point X="28.399130859375" Y="-0.247487670898" />
                  <Point X="28.410853515625" Y="-0.266766296387" />
                  <Point X="28.417291015625" Y="-0.276058929443" />
                  <Point X="28.4621171875" Y="-0.333177703857" />
                  <Point X="28.47453125" Y="-0.347466461182" />
                  <Point X="28.4995234375" Y="-0.373496856689" />
                  <Point X="28.5094375" Y="-0.382464324951" />
                  <Point X="28.530423828125" Y="-0.398917755127" />
                  <Point X="28.54149609375" Y="-0.406404052734" />
                  <Point X="28.616205078125" Y="-0.449587768555" />
                  <Point X="28.634005859375" Y="-0.459876342773" />
                  <Point X="28.639494140625" Y="-0.462813720703" />
                  <Point X="28.65916015625" Y="-0.472017333984" />
                  <Point X="28.683029296875" Y="-0.481027893066" />
                  <Point X="28.6919921875" Y="-0.483912841797" />
                  <Point X="28.942142578125" Y="-0.550940124512" />
                  <Point X="29.78487890625" Y="-0.776750976562" />
                  <Point X="29.7711640625" Y="-0.867718261719" />
                  <Point X="29.761619140625" Y="-0.93103894043" />
                  <Point X="29.7278046875" Y="-1.079219848633" />
                  <Point X="29.65438671875" Y="-1.069554443359" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042297363" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.207853515625" Y="-0.944546936035" />
                  <Point X="28.17291796875" Y="-0.952140075684" />
                  <Point X="28.157876953125" Y="-0.9567421875" />
                  <Point X="28.128755859375" Y="-0.968366577148" />
                  <Point X="28.11467578125" Y="-0.975389282227" />
                  <Point X="28.08684765625" Y="-0.992282653809" />
                  <Point X="28.07412109375" Y="-1.001533081055" />
                  <Point X="28.050373046875" Y="-1.022003234863" />
                  <Point X="28.0393515625" Y="-1.033223022461" />
                  <Point X="27.95072265625" Y="-1.139814819336" />
                  <Point X="27.929607421875" Y="-1.16521105957" />
                  <Point X="27.921328125" Y="-1.176845947266" />
                  <Point X="27.906603515625" Y="-1.201233520508" />
                  <Point X="27.900158203125" Y="-1.213986328125" />
                  <Point X="27.88881640625" Y="-1.241372070312" />
                  <Point X="27.884359375" Y="-1.254941650391" />
                  <Point X="27.87753125" Y="-1.2825859375" />
                  <Point X="27.87516015625" Y="-1.296660766602" />
                  <Point X="27.86245703125" Y="-1.434702026367" />
                  <Point X="27.859431640625" Y="-1.467591430664" />
                  <Point X="27.859291015625" Y="-1.483321777344" />
                  <Point X="27.861609375" Y="-1.514588623047" />
                  <Point X="27.864068359375" Y="-1.53012512207" />
                  <Point X="27.871798828125" Y="-1.561744873047" />
                  <Point X="27.87678515625" Y="-1.576663696289" />
                  <Point X="27.88915625" Y="-1.605476928711" />
                  <Point X="27.896541015625" Y="-1.619371582031" />
                  <Point X="27.9776875" Y="-1.74558972168" />
                  <Point X="27.997021484375" Y="-1.775662231445" />
                  <Point X="28.001744140625" Y="-1.782356201172" />
                  <Point X="28.01980078125" Y="-1.804456542969" />
                  <Point X="28.043494140625" Y="-1.828122802734" />
                  <Point X="28.052798828125" Y="-1.836277587891" />
                  <Point X="28.2849375" Y="-2.014405151367" />
                  <Point X="29.087171875" Y="-2.629980957031" />
                  <Point X="29.07240625" Y="-2.653874023438" />
                  <Point X="29.04548828125" Y="-2.697427978516" />
                  <Point X="29.0012734375" Y="-2.760251464844" />
                  <Point X="28.932896484375" Y="-2.720773193359" />
                  <Point X="27.848455078125" Y="-2.094670898438" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.596595703125" Y="-2.034820922852" />
                  <Point X="27.555017578125" Y="-2.027311889648" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403442383" />
                  <Point X="27.492314453125" Y="-2.02650378418" />
                  <Point X="27.460140625" Y="-2.031461914062" />
                  <Point X="27.444845703125" Y="-2.03513659668" />
                  <Point X="27.4150703125" Y="-2.044960083008" />
                  <Point X="27.40058984375" Y="-2.051108886719" />
                  <Point X="27.25561328125" Y="-2.127408935547" />
                  <Point X="27.221072265625" Y="-2.145587890625" />
                  <Point X="27.208970703125" Y="-2.153169433594" />
                  <Point X="27.1860390625" Y="-2.1700625" />
                  <Point X="27.175208984375" Y="-2.179374023438" />
                  <Point X="27.15425" Y="-2.200333007812" />
                  <Point X="27.144939453125" Y="-2.211161376953" />
                  <Point X="27.128046875" Y="-2.234091552734" />
                  <Point X="27.12046484375" Y="-2.246193359375" />
                  <Point X="27.0441640625" Y="-2.391169677734" />
                  <Point X="27.025984375" Y="-2.425711425781" />
                  <Point X="27.0198359375" Y="-2.440193359375" />
                  <Point X="27.01001171875" Y="-2.469972412109" />
                  <Point X="27.0063359375" Y="-2.48526953125" />
                  <Point X="27.00137890625" Y="-2.517443603516" />
                  <Point X="27.000279296875" Y="-2.533134521484" />
                  <Point X="27.00068359375" Y="-2.564484619141" />
                  <Point X="27.0021875" Y="-2.580143798828" />
                  <Point X="27.033705078125" Y="-2.754655517578" />
                  <Point X="27.04121484375" Y="-2.796234130859" />
                  <Point X="27.04301953125" Y="-2.804228759766" />
                  <Point X="27.05123828125" Y="-2.831539306641" />
                  <Point X="27.0640703125" Y="-2.862475830078" />
                  <Point X="27.069546875" Y="-2.873577636719" />
                  <Point X="27.21871875" Y="-3.131953613281" />
                  <Point X="27.73589453125" Y="-4.027722900391" />
                  <Point X="27.723755859375" Y="-4.036083007813" />
                  <Point X="27.6614453125" Y="-3.954877929688" />
                  <Point X="26.83391796875" Y="-2.876422363281" />
                  <Point X="26.828654296875" Y="-2.870144042969" />
                  <Point X="26.808830078125" Y="-2.849624023438" />
                  <Point X="26.783251953125" Y="-2.828003662109" />
                  <Point X="26.77330078125" Y="-2.820646728516" />
                  <Point X="26.60118359375" Y="-2.7099921875" />
                  <Point X="26.56017578125" Y="-2.683627929688" />
                  <Point X="26.546283203125" Y="-2.676245605469" />
                  <Point X="26.51747265625" Y="-2.663874511719" />
                  <Point X="26.5025546875" Y="-2.658885742188" />
                  <Point X="26.47093359375" Y="-2.651154052734" />
                  <Point X="26.45539453125" Y="-2.648695800781" />
                  <Point X="26.424125" Y="-2.646376953125" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.220158203125" Y="-2.663838134766" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670339355469" />
                  <Point X="26.133576171875" Y="-2.677171386719" />
                  <Point X="26.1200078125" Y="-2.681629394531" />
                  <Point X="26.092623046875" Y="-2.69297265625" />
                  <Point X="26.079876953125" Y="-2.6994140625" />
                  <Point X="26.05549609375" Y="-2.714134033203" />
                  <Point X="26.043861328125" Y="-2.722412597656" />
                  <Point X="25.8985078125" Y="-2.843268310547" />
                  <Point X="25.863876953125" Y="-2.872062988281" />
                  <Point X="25.85265625" Y="-2.883086181641" />
                  <Point X="25.832185546875" Y="-2.906834960938" />
                  <Point X="25.822935546875" Y="-2.919560546875" />
                  <Point X="25.80604296875" Y="-2.947386230469" />
                  <Point X="25.79901953125" Y="-2.961466064453" />
                  <Point X="25.78739453125" Y="-2.990588378906" />
                  <Point X="25.78279296875" Y="-3.005630859375" />
                  <Point X="25.739333984375" Y="-3.205579345703" />
                  <Point X="25.728978515625" Y="-3.25321875" />
                  <Point X="25.7275859375" Y="-3.261286621094" />
                  <Point X="25.724724609375" Y="-3.289676757812" />
                  <Point X="25.7247421875" Y="-3.323170166016" />
                  <Point X="25.7255546875" Y="-3.335520507812" />
                  <Point X="25.767830078125" Y="-3.656627929688" />
                  <Point X="25.833087890625" Y="-4.152318359375" />
                  <Point X="25.655068359375" Y="-3.487937988281" />
                  <Point X="25.652607421875" Y="-3.480122070312" />
                  <Point X="25.642146484375" Y="-3.453579833984" />
                  <Point X="25.6267890625" Y="-3.423815917969" />
                  <Point X="25.62041015625" Y="-3.413209472656" />
                  <Point X="25.488185546875" Y="-3.222699462891" />
                  <Point X="25.456681640625" Y="-3.177309082031" />
                  <Point X="25.446671875" Y="-3.165172119141" />
                  <Point X="25.424787109375" Y="-3.142716064453" />
                  <Point X="25.412912109375" Y="-3.132396972656" />
                  <Point X="25.38665625" Y="-3.113153320312" />
                  <Point X="25.3732421875" Y="-3.104936523438" />
                  <Point X="25.3452421875" Y="-3.090829101562" />
                  <Point X="25.33065625" Y="-3.084938476562" />
                  <Point X="25.126046875" Y="-3.021435058594" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.063376953125" Y="-3.003109130859" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.73040625" Y="-3.069808349609" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.6670703125" Y="-3.090829101562" />
                  <Point X="24.6390703125" Y="-3.104936523438" />
                  <Point X="24.62565625" Y="-3.113153320312" />
                  <Point X="24.599400390625" Y="-3.132396972656" />
                  <Point X="24.587525390625" Y="-3.142717285156" />
                  <Point X="24.565640625" Y="-3.165174072266" />
                  <Point X="24.555630859375" Y="-3.177310546875" />
                  <Point X="24.423408203125" Y="-3.367820556641" />
                  <Point X="24.391904296875" Y="-3.4132109375" />
                  <Point X="24.387529296875" Y="-3.420133056641" />
                  <Point X="24.374025390625" Y="-3.445260742188" />
                  <Point X="24.36122265625" Y="-3.476212646484" />
                  <Point X="24.35724609375" Y="-3.487936035156" />
                  <Point X="24.28016796875" Y="-3.775592041016" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.055685070648" Y="2.617655674738" />
                  <Point X="28.903451685831" Y="2.759615602694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.769032238259" Y="3.817478851562" />
                  <Point X="27.377362058344" Y="4.182717203124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.094863597313" Y="2.451224998766" />
                  <Point X="28.817420090115" Y="2.709945254779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.720282911246" Y="3.733042225637" />
                  <Point X="27.066546478802" Y="4.342661311249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.018446959492" Y="2.392588557562" />
                  <Point X="28.731388494399" Y="2.660274906864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.671533523427" Y="3.648605656414" />
                  <Point X="26.800215262314" Y="4.461123079729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.942030317821" Y="2.333952119948" />
                  <Point X="28.645356898684" Y="2.610604558949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.622784135608" Y="3.56416908719" />
                  <Point X="26.572250787431" Y="4.543807282857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.86561367615" Y="2.275315682333" />
                  <Point X="28.559325302968" Y="2.560934211034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.57403474779" Y="3.479732517967" />
                  <Point X="26.363468434505" Y="4.608603867875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.789197034479" Y="2.216679244719" />
                  <Point X="28.473293707252" Y="2.511263863119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.525285359971" Y="3.395295948744" />
                  <Point X="26.17550833557" Y="4.65398338692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.712780392809" Y="2.158042807105" />
                  <Point X="28.387262111536" Y="2.461593515204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.476535972152" Y="3.31085937952" />
                  <Point X="25.987548258829" Y="4.699362885267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.70727182318" Y="1.100768436445" />
                  <Point X="29.697038883014" Y="1.110310807524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.636363751138" Y="2.09940636949" />
                  <Point X="28.301230515821" Y="2.411923167289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.427786584334" Y="3.226422810297" />
                  <Point X="25.804260312683" Y="4.740385551352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.74818143367" Y="0.932723498689" />
                  <Point X="29.574975300851" Y="1.094240830555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.559947109467" Y="2.040769931876" />
                  <Point X="28.215198920105" Y="2.362252819375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.379037196515" Y="3.141986241073" />
                  <Point X="25.647341001217" Y="4.756819067796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.773741716684" Y="0.778992040369" />
                  <Point X="29.452911718688" Y="1.078170853586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.483530467796" Y="1.982133494261" />
                  <Point X="28.129167324389" Y="2.31258247146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.330287808696" Y="3.05754967185" />
                  <Point X="25.490421302179" Y="4.773252945656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.722003178277" Y="0.697342899164" />
                  <Point X="29.330848136525" Y="1.062100876617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.407113826125" Y="1.923497056647" />
                  <Point X="28.043135728673" Y="2.262912123545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.281538420877" Y="2.973113102626" />
                  <Point X="25.370262244004" Y="4.755406971336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.613798249785" Y="0.668349518573" />
                  <Point X="29.208784554362" Y="1.046030899647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.330697184455" Y="1.864860619033" />
                  <Point X="27.957104132958" Y="2.21324177563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.232789033059" Y="2.888676533403" />
                  <Point X="25.342414865832" Y="4.651478962787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.505593321293" Y="0.639356137982" />
                  <Point X="29.086720972199" Y="1.029960922678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.254280542784" Y="1.806224181418" />
                  <Point X="27.871072539702" Y="2.163571425421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.18403964524" Y="2.804239964179" />
                  <Point X="25.314567487659" Y="4.547550954238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.397388392801" Y="0.610362757391" />
                  <Point X="28.964657390035" Y="1.013890945709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.177863901113" Y="1.747587743804" />
                  <Point X="27.785040948046" Y="2.11390107372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.135290257421" Y="2.719803394956" />
                  <Point X="25.286720109487" Y="4.443622945689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.289183464309" Y="0.5813693768" />
                  <Point X="28.842593807872" Y="0.99782096874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.112586780418" Y="1.678563534827" />
                  <Point X="27.699009356391" Y="2.064230722019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.086540869603" Y="2.635366825732" />
                  <Point X="25.258872473522" Y="4.339695177535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.180978535816" Y="0.552375996209" />
                  <Point X="28.720530244699" Y="0.981750974062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.057106226877" Y="1.600403879188" />
                  <Point X="27.611202927255" Y="2.016215433044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.043047635513" Y="2.546028813862" />
                  <Point X="25.228624762992" Y="4.23800551512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.072773607324" Y="0.523382615618" />
                  <Point X="28.598466706394" Y="0.965680956195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.019710130265" Y="1.505380194636" />
                  <Point X="27.505850587538" Y="1.984561970385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.017269711226" Y="2.440171008344" />
                  <Point X="25.176125931787" Y="4.15706535842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.677885088743" Y="4.621682461088" />
                  <Point X="24.519798816692" Y="4.769100294687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.964568678832" Y="0.494389235027" />
                  <Point X="28.474242582143" Y="0.951625717317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.001007384498" Y="1.392924678411" />
                  <Point X="27.371120429155" Y="1.98030376683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.021195862324" Y="2.306613704411" />
                  <Point X="25.094579671207" Y="4.103212367824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.724283610595" Y="4.448519030682" />
                  <Point X="24.396035301602" Y="4.754615530818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.85636375034" Y="0.465395854436" />
                  <Point X="28.31193480828" Y="0.973084056237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.033986655762" Y="1.232274901623" />
                  <Point X="24.97125223433" Y="4.088320954442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.770682132448" Y="4.275355600276" />
                  <Point X="24.272271786511" Y="4.740130766949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.748158821848" Y="0.436402473845" />
                  <Point X="24.14850827142" Y="4.72564600308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.644927362096" Y="0.402771258623" />
                  <Point X="24.030681332902" Y="4.705625291997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.783108199499" Y="-0.788495651812" />
                  <Point X="29.764721546217" Y="-0.771349820243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.558366121213" Y="0.353594812817" />
                  <Point X="23.923583964923" Y="4.675599094519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.765938989557" Y="-0.902381213329" />
                  <Point X="29.569261340714" Y="-0.718976338676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.48229450711" Y="0.29463663179" />
                  <Point X="23.816486710665" Y="4.645572790994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.743328323107" Y="-1.011192534561" />
                  <Point X="29.373801135211" Y="-0.66660285711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.422813407107" Y="0.220207546079" />
                  <Point X="23.709389456407" Y="4.615546487469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.668627786816" Y="-1.071429266332" />
                  <Point X="29.178340929708" Y="-0.614229375543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.376888106601" Y="0.133137472832" />
                  <Point X="23.602292202149" Y="4.585520183944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.506432551689" Y="-1.05007587148" />
                  <Point X="28.982880724205" Y="-0.561855893976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.354274153514" Y="0.024329216439" />
                  <Point X="23.525385349384" Y="4.527340875571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.344237250762" Y="-1.028722415269" />
                  <Point X="28.787420901624" Y="-0.50948276949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.358148091666" Y="-0.109179398135" />
                  <Point X="23.536100306432" Y="4.387452907672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.182041949834" Y="-1.007368959058" />
                  <Point X="28.524986578334" Y="-0.394654912707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.47579788735" Y="-0.348785716297" />
                  <Point X="23.509357666087" Y="4.282494714432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.019846648907" Y="-0.986015502847" />
                  <Point X="23.471676937003" Y="4.187736453956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.857651347979" Y="-0.964662046636" />
                  <Point X="23.407980314793" Y="4.117238406298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.695456047052" Y="-0.943308590425" />
                  <Point X="23.318042062654" Y="4.071211074434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.533260746125" Y="-0.921955134214" />
                  <Point X="23.217709545938" Y="4.034876551097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.380781464191" Y="-0.909662012292" />
                  <Point X="23.056221447462" Y="4.055570530353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.265465812328" Y="-0.932024536066" />
                  <Point X="22.733109539273" Y="4.226981150446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.15423479495" Y="-0.958196043119" />
                  <Point X="22.645819328605" Y="4.178484479962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.067516512791" Y="-1.007226045566" />
                  <Point X="22.558529117936" Y="4.129987809479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.002991858383" Y="-1.076951940707" />
                  <Point X="22.471238907267" Y="4.081491138995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.942156293908" Y="-1.150117967864" />
                  <Point X="22.383948696599" Y="4.032994468511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.892132184234" Y="-1.233365839728" />
                  <Point X="22.303489016209" Y="3.978128225496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.870856951517" Y="-1.343422473062" />
                  <Point X="22.227044046907" Y="3.919518203825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.859849064648" Y="-1.463053561295" />
                  <Point X="22.150598795031" Y="3.860908445659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.04500041021" Y="-2.698121179192" />
                  <Point X="28.436607320776" Y="-2.130785444993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.909052859957" Y="-1.63883295152" />
                  <Point X="22.074153543154" Y="3.802298687493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.925278919951" Y="-2.716375192195" />
                  <Point X="21.997708291278" Y="3.743688929327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.559544419551" Y="-2.505218361856" />
                  <Point X="21.981736843745" Y="3.628686436295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.193809919152" Y="-2.294061531517" />
                  <Point X="22.144200124171" Y="3.347290867549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.832653780662" Y="-2.087174092728" />
                  <Point X="22.243979741929" Y="3.124348759897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.646971621556" Y="-2.04391878694" />
                  <Point X="22.248605105286" Y="2.990139429983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.489469879898" Y="-2.026942145554" />
                  <Point X="22.21264461572" Y="2.893777020204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.384928151014" Y="-2.059351515044" />
                  <Point X="22.150176993271" Y="2.822132911729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.295885478313" Y="-2.106213988244" />
                  <Point X="22.077544139488" Y="2.759968034826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.207837621076" Y="-2.154004141873" />
                  <Point X="21.975661203117" Y="2.725079301207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.138788432925" Y="-2.219510841041" />
                  <Point X="21.826542103838" Y="2.734239002112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.090059130742" Y="-2.303966140423" />
                  <Point X="21.512701990019" Y="2.897003534079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.044201227233" Y="-2.391099062387" />
                  <Point X="21.253373446397" Y="3.008935204468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.006279938582" Y="-2.485632997438" />
                  <Point X="21.194805610034" Y="2.933654486635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.009043704123" Y="-2.618106359304" />
                  <Point X="21.136237773672" Y="2.858373768801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.037254976292" Y="-2.774309905005" />
                  <Point X="21.077669937309" Y="2.783093050967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.145515562885" Y="-3.005160644041" />
                  <Point X="21.019102100946" Y="2.707812333133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.307979496354" Y="-3.286556821759" />
                  <Point X="20.966546929759" Y="2.626924714316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.470446056667" Y="-3.567955449048" />
                  <Point X="27.098169449968" Y="-3.220801897085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.493112303679" Y="-2.656576980195" />
                  <Point X="20.917450710229" Y="2.542811570895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.632912616981" Y="-3.849354076338" />
                  <Point X="27.4485662893" Y="-3.677448344702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.348898232809" Y="-2.65199129228" />
                  <Point X="20.868354842041" Y="2.458698099843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.222112992932" Y="-2.663658252199" />
                  <Point X="20.819258973853" Y="2.374584628791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.107599751816" Y="-2.6867690361" />
                  <Point X="21.578370113245" Y="1.536805930449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.024124374261" Y="-2.738823096014" />
                  <Point X="21.558688130966" Y="1.425263567044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.95048610697" Y="-2.800050409652" />
                  <Point X="21.51647206192" Y="1.334734579503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.876848011511" Y="-2.861277883527" />
                  <Point X="21.450663041885" Y="1.266206374685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.814663646278" Y="-2.933186133629" />
                  <Point X="21.356229016314" Y="1.224371419371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.777781286007" Y="-3.028688885069" />
                  <Point X="21.238038841145" Y="1.204689431945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.754306241619" Y="-3.136694160834" />
                  <Point X="21.077346782397" Y="1.224641092145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.730830513687" Y="-3.244698799183" />
                  <Point X="20.915151489978" Y="1.245994540422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.730680624921" Y="-3.374455134452" />
                  <Point X="20.752956197559" Y="1.267347988699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.750175482953" Y="-3.522530492473" />
                  <Point X="25.597417211057" Y="-3.380081099398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.256101988752" Y="-3.06179950547" />
                  <Point X="20.59076090514" Y="1.288701436976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.769670267043" Y="-3.670605781543" />
                  <Point X="25.682144068544" Y="-3.58898628101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.052021603465" Y="-3.00138757621" />
                  <Point X="20.428565612721" Y="1.310054885252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.789164341781" Y="-3.81868040913" />
                  <Point X="25.728543002825" Y="-3.762150096012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.92224794515" Y="-3.010267790852" />
                  <Point X="21.703791475978" Y="-0.009008579272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.40014426248" Y="0.274147028179" />
                  <Point X="20.349082464322" Y="1.254278011424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.808658416519" Y="-3.966755036718" />
                  <Point X="25.774941937106" Y="-3.935313911015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.817735593434" Y="-3.042704554994" />
                  <Point X="21.688823119851" Y="-0.124946470173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.204684348975" Y="0.326520237453" />
                  <Point X="20.320983718986" Y="1.150584406547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.828152491256" Y="-4.114829664306" />
                  <Point X="25.821340871388" Y="-4.108477726017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.713223222447" Y="-3.075141301165" />
                  <Point X="21.641717533063" Y="-0.210915908657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.009224233895" Y="0.378893634698" />
                  <Point X="20.29288497365" Y="1.04689080167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.619515801616" Y="-3.117653826361" />
                  <Point X="21.570681690339" Y="-0.274570022464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.813764118816" Y="0.431267031944" />
                  <Point X="20.266334586425" Y="0.941753329496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.551136895066" Y="-3.183785573234" />
                  <Point X="21.476172896864" Y="-0.316335255581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.618304003737" Y="0.483640429189" />
                  <Point X="20.249444013413" Y="0.827607934838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.496405596783" Y="-3.262643920706" />
                  <Point X="21.367968010358" Y="-0.345328675324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.422843888658" Y="0.536013826434" />
                  <Point X="20.232553652529" Y="0.713462342369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.441674298501" Y="-3.341502268178" />
                  <Point X="21.259763123853" Y="-0.374322095068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.227383773578" Y="0.588387223679" />
                  <Point X="20.215663291645" Y="0.599316749899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.387252100308" Y="-3.420648856146" />
                  <Point X="21.151558237347" Y="-0.403315514812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.34982242105" Y="-3.515641224374" />
                  <Point X="22.050928186706" Y="-1.371887669413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.759383748172" Y="-1.1000180822" />
                  <Point X="21.043353350841" Y="-0.432308934556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.321974735659" Y="-3.619568946436" />
                  <Point X="22.024579668542" Y="-1.477213387532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.63702866612" Y="-1.115816231126" />
                  <Point X="20.935148464335" Y="-0.4613023543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.294127050267" Y="-3.723496668499" />
                  <Point X="21.970073213181" Y="-1.55628140442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.514965104918" Y="-1.131886227642" />
                  <Point X="20.826943577829" Y="-0.490295774044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.266279470474" Y="-3.827424489034" />
                  <Point X="21.89579697834" Y="-1.616913803694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.392901535832" Y="-1.147956216805" />
                  <Point X="20.718738691324" Y="-0.519289193788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.238431996815" Y="-3.931352408541" />
                  <Point X="21.819380440115" Y="-1.675550337773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.270837955216" Y="-1.164026195217" />
                  <Point X="20.610533804818" Y="-0.548282613532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.210584523156" Y="-4.035280328047" />
                  <Point X="22.688471264578" Y="-2.615886751613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.404091588502" Y="-2.350698413482" />
                  <Point X="21.742963901889" Y="-1.734186871852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.1487743746" Y="-1.180096173629" />
                  <Point X="20.502328918312" Y="-0.577276033275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.182737049497" Y="-4.139208247554" />
                  <Point X="22.660570031046" Y="-2.719764539227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.302044300041" Y="-2.385433886296" />
                  <Point X="21.666547363664" Y="-1.79282340593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.026710793984" Y="-1.196166152042" />
                  <Point X="20.394124031806" Y="-0.606269453019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.154889575838" Y="-4.24313616706" />
                  <Point X="22.612115907" Y="-2.804476446372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.216012717716" Y="-2.435104246698" />
                  <Point X="21.590130825438" Y="-1.851459940009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.904647213368" Y="-1.212236130454" />
                  <Point X="20.285919152753" Y="-0.635262879713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.127042102179" Y="-4.347064086567" />
                  <Point X="22.563366482791" Y="-2.888912981661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.12998113539" Y="-2.4847746071" />
                  <Point X="21.513714287213" Y="-1.910096474088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.782583632753" Y="-1.228306108866" />
                  <Point X="20.220648501269" Y="-0.704293121327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.09919462852" Y="-4.450992006073" />
                  <Point X="23.813660655084" Y="-4.18472726824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.406388433207" Y="-3.804939777174" />
                  <Point X="22.514617058582" Y="-2.97334951695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.043949548209" Y="-2.534444962973" />
                  <Point X="21.437297748988" Y="-1.968733008166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.660520052137" Y="-1.244376087278" />
                  <Point X="20.242086488042" Y="-0.854180476213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.071347154861" Y="-4.55491992558" />
                  <Point X="23.846306325367" Y="-4.34506595708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.249711368371" Y="-3.788732159367" />
                  <Point X="22.465867634373" Y="-3.057786052239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.957917946243" Y="-2.584115305059" />
                  <Point X="21.360881210762" Y="-2.027369542245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.538456471521" Y="-1.26044606569" />
                  <Point X="20.267943331963" Y="-1.008188482054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.043499681202" Y="-4.658847845086" />
                  <Point X="23.877531820575" Y="-4.504080311238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.099883962497" Y="-3.778911951877" />
                  <Point X="22.417118210164" Y="-3.142222587528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.871886344277" Y="-2.633785647146" />
                  <Point X="21.284464672537" Y="-2.086006076324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.416392890905" Y="-1.276516044102" />
                  <Point X="20.31149616931" Y="-1.178698268728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.015652207543" Y="-4.762775764593" />
                  <Point X="23.872223287313" Y="-4.62902613269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.970124857411" Y="-3.787805737625" />
                  <Point X="22.368368785955" Y="-3.226659122817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.785854742311" Y="-2.683455989232" />
                  <Point X="21.208048134311" Y="-2.144642610403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.879103872734" Y="-3.832823405063" />
                  <Point X="22.319619361746" Y="-3.311095658106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.699823140345" Y="-2.733126331319" />
                  <Point X="21.131631596086" Y="-2.203279144481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.797953977894" Y="-3.887046012691" />
                  <Point X="22.270869937537" Y="-3.395532193395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.613791538379" Y="-2.782796673406" />
                  <Point X="21.05521505786" Y="-2.26191567856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.716804083054" Y="-3.941268620318" />
                  <Point X="22.222120513328" Y="-3.479968728684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.527759936413" Y="-2.832467015492" />
                  <Point X="20.978798519635" Y="-2.320552212639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.636315639448" Y="-3.9961080412" />
                  <Point X="22.173371089119" Y="-3.564405263973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.441728334447" Y="-2.882137357579" />
                  <Point X="20.902381981409" Y="-2.379188746718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.571306210401" Y="-4.065381876678" />
                  <Point X="22.12462166491" Y="-3.648841799262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.355696732481" Y="-2.931807699665" />
                  <Point X="20.825965388984" Y="-2.437825230254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.513206383531" Y="-4.141099020423" />
                  <Point X="22.075872240701" Y="-3.733278334551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.269665130515" Y="-2.981478041752" />
                  <Point X="20.976735347233" Y="-2.708316599662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.150048062932" Y="-3.932344516612" />
                  <Point X="22.027122863168" Y="-3.817714913366" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.9998359375" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.795044921875" Y="-4.744445800781" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521543945312" />
                  <Point X="25.332095703125" Y="-3.331033935547" />
                  <Point X="25.300591796875" Y="-3.285643554688" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.0697265625" Y="-3.202896484375" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.7867265625" Y="-3.251269775391" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643554688" />
                  <Point X="24.579498046875" Y="-3.476153564453" />
                  <Point X="24.547994140625" Y="-3.521543945312" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.4636953125" Y="-3.824768310547" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.955955078125" Y="-4.948974121094" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.662412109375" Y="-4.876999511719" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.651908203125" Y="-4.846833984375" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.641435546875" Y="-4.289015625" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.42533984375" Y="-4.037424804688" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.100740234375" Y="-3.969375732422" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791503906" />
                  <Point X="22.801791015625" Y="-4.112993164062" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.696994140625" Y="-4.213688476562" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.226541015625" Y="-4.218615234375" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.815517578125" Y="-3.914562744141" />
                  <Point X="21.771419921875" Y="-3.880608398438" />
                  <Point X="21.892771484375" Y="-3.670421142578" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597594238281" />
                  <Point X="22.4860234375" Y="-2.568765625" />
                  <Point X="22.46867578125" Y="-2.551416992188" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="22.160220703125" Y="-2.686708984375" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.903376953125" Y="-2.932631835938" />
                  <Point X="20.838302734375" Y="-2.847136962891" />
                  <Point X="20.602681640625" Y="-2.452037841797" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="20.78431640625" Y="-2.230294189453" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396013305664" />
                  <Point X="21.860400390625" Y="-1.371989746094" />
                  <Point X="21.8618828125" Y="-1.366266113281" />
                  <Point X="21.859673828125" Y="-1.334595581055" />
                  <Point X="21.838841796875" Y="-1.310639282227" />
                  <Point X="21.817455078125" Y="-1.298052001953" />
                  <Point X="21.812359375" Y="-1.295052734375" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.4672734375" Y="-1.32980456543" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.0980625" Y="-1.110842407227" />
                  <Point X="20.072607421875" Y="-1.011187866211" />
                  <Point X="20.01026953125" Y="-0.575330078125" />
                  <Point X="20.001603515625" Y="-0.51474230957" />
                  <Point X="20.244267578125" Y="-0.449720855713" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.45810546875" Y="-0.121424591064" />
                  <Point X="21.48051953125" Y="-0.105868728638" />
                  <Point X="21.485859375" Y="-0.102162414551" />
                  <Point X="21.505103515625" Y="-0.075907302856" />
                  <Point X="21.51257421875" Y="-0.051835510254" />
                  <Point X="21.514353515625" Y="-0.046095401764" />
                  <Point X="21.514353515625" Y="-0.01645980835" />
                  <Point X="21.5068828125" Y="0.007611982346" />
                  <Point X="21.505103515625" Y="0.013342202187" />
                  <Point X="21.485859375" Y="0.039602336884" />
                  <Point X="21.4634453125" Y="0.055158203125" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="21.157041015625" Y="0.142583831787" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.065951171875" Y="0.885562866211" />
                  <Point X="20.08235546875" Y="0.996415466309" />
                  <Point X="20.20783984375" Y="1.459493286133" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.387787109375" Y="1.507063110352" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268294921875" Y="1.395866210938" />
                  <Point X="21.317900390625" Y="1.411507324219" />
                  <Point X="21.329720703125" Y="1.415233764648" />
                  <Point X="21.348466796875" Y="1.426056030273" />
                  <Point X="21.3608828125" Y="1.443786743164" />
                  <Point X="21.380787109375" Y="1.491841796875" />
                  <Point X="21.385529296875" Y="1.503291137695" />
                  <Point X="21.389287109375" Y="1.524606323242" />
                  <Point X="21.38368359375" Y="1.545512695312" />
                  <Point X="21.359666015625" Y="1.591649902344" />
                  <Point X="21.353943359375" Y="1.602642456055" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.176271484375" Y="1.744880371094" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.77624609375" Y="2.677805175781" />
                  <Point X="20.83998828125" Y="2.7870078125" />
                  <Point X="21.172388671875" Y="3.214264160156" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.31330078125" Y="3.231520263672" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.93057421875" Y="2.914390380859" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927405029297" />
                  <Point X="22.0357890625" Y="2.976444580078" />
                  <Point X="22.04747265625" Y="2.988128417969" />
                  <Point X="22.0591015625" Y="3.006381591797" />
                  <Point X="22.061927734375" Y="3.027839111328" />
                  <Point X="22.0558828125" Y="3.096927734375" />
                  <Point X="22.054443359375" Y="3.113388671875" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.975365234375" Y="3.259723632812" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="22.13611328125" Y="4.089219726562" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.770650390625" Y="4.465190917969" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.870677734375" Y="4.498040039062" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.048755859375" Y="4.27366015625" />
                  <Point X="23.125650390625" Y="4.233630859375" />
                  <Point X="23.14397265625" Y="4.224093261719" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.18619140625" Y="4.22225" />
                  <Point X="23.26628125" Y="4.255425292969" />
                  <Point X="23.285365234375" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.29448828125" />
                  <Point X="23.339986328125" Y="4.377166503906" />
                  <Point X="23.346197265625" Y="4.396864746094" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.339833984375" Y="4.481092773438" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.8881953125" Y="4.863004394531" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.666580078125" Y="4.977575683594" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.801798828125" Y="4.893333496094" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.09163671875" Y="4.449671875" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.73472265625" Y="4.93870703125" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.385287109375" Y="4.798794921875" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.850921875" Y="4.64484375" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.261515625" Y="4.461230957031" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.657955078125" Y="4.239135742188" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="28.033626953125" Y="3.981564208984" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.925693359375" Y="3.708826660156" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491514160156" />
                  <Point X="27.207513671875" Y="2.426676025391" />
                  <Point X="27.202044921875" Y="2.392324707031" />
                  <Point X="27.208806640625" Y="2.336258300781" />
                  <Point X="27.210416015625" Y="2.32290625" />
                  <Point X="27.218681640625" Y="2.3008125" />
                  <Point X="27.253375" Y="2.249685302734" />
                  <Point X="27.27494140625" Y="2.224203125" />
                  <Point X="27.326068359375" Y="2.189510986328" />
                  <Point X="27.33825" Y="2.181245605469" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.416404296875" Y="2.166219482422" />
                  <Point X="27.4486640625" Y="2.165946044922" />
                  <Point X="27.513501953125" Y="2.183284667969" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="27.828189453125" Y="2.358206054688" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.158359375" Y="2.803352050781" />
                  <Point X="29.202599609375" Y="2.741869384766" />
                  <Point X="29.370453125" Y="2.464490722656" />
                  <Point X="29.387513671875" Y="2.436296142578" />
                  <Point X="29.204755859375" Y="2.296059570312" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583831665039" />
                  <Point X="28.23270703125" Y="1.522954589844" />
                  <Point X="28.21312109375" Y="1.491503295898" />
                  <Point X="28.19573828125" Y="1.42934777832" />
                  <Point X="28.191595703125" Y="1.414538818359" />
                  <Point X="28.190779296875" Y="1.390963500977" />
                  <Point X="28.205048828125" Y="1.321807495117" />
                  <Point X="28.215646484375" Y="1.287956787109" />
                  <Point X="28.25445703125" Y="1.228966186523" />
                  <Point X="28.263703125" Y="1.214911254883" />
                  <Point X="28.28094921875" Y="1.198819580078" />
                  <Point X="28.33719140625" Y="1.16716003418" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.444609375" Y="1.143569580078" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.74858203125" Y="1.177083618164" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.92019140625" Y="1.029420898438" />
                  <Point X="29.939193359375" Y="0.951367492676" />
                  <Point X="29.992087890625" Y="0.611634338379" />
                  <Point X="29.997859375" Y="0.57455645752" />
                  <Point X="29.79108203125" Y="0.51915020752" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.23281980896" />
                  <Point X="28.65437890625" Y="0.189636123657" />
                  <Point X="28.636578125" Y="0.179347290039" />
                  <Point X="28.622265625" Y="0.166925933838" />
                  <Point X="28.577439453125" Y="0.109807189941" />
                  <Point X="28.566759765625" Y="0.096198257446" />
                  <Point X="28.556986328125" Y="0.074736480713" />
                  <Point X="28.542044921875" Y="-0.003284866333" />
                  <Point X="28.538484375" Y="-0.040685997009" />
                  <Point X="28.55342578125" Y="-0.118707336426" />
                  <Point X="28.556986328125" Y="-0.137296554565" />
                  <Point X="28.566759765625" Y="-0.158758331299" />
                  <Point X="28.6115859375" Y="-0.215877075195" />
                  <Point X="28.636578125" Y="-0.241907363892" />
                  <Point X="28.711287109375" Y="-0.285091033936" />
                  <Point X="28.729087890625" Y="-0.295379730225" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="28.991318359375" Y="-0.367414245605" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.9590390625" Y="-0.896044250488" />
                  <Point X="29.948431640625" Y="-0.966411010742" />
                  <Point X="29.8806640625" Y="-1.263378417969" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.6295859375" Y="-1.257928833008" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.248208984375" Y="-1.130211791992" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.1854453125" Y="-1.154698242188" />
                  <Point X="28.09681640625" Y="-1.261289916992" />
                  <Point X="28.075701171875" Y="-1.286686157227" />
                  <Point X="28.064359375" Y="-1.314072021484" />
                  <Point X="28.05165625" Y="-1.45211315918" />
                  <Point X="28.048630859375" Y="-1.485002441406" />
                  <Point X="28.056361328125" Y="-1.516622070312" />
                  <Point X="28.1375078125" Y="-1.642840209961" />
                  <Point X="28.156841796875" Y="-1.672912719727" />
                  <Point X="28.168462890625" Y="-1.685540771484" />
                  <Point X="28.4006015625" Y="-1.863668334961" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.234033203125" Y="-2.753757568359" />
                  <Point X="29.2041328125" Y="-2.802139160156" />
                  <Point X="29.06398046875" Y="-3.001278564453" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.837896484375" Y="-2.885317871094" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.562830078125" Y="-2.221796142578" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219245117188" />
                  <Point X="27.3441015625" Y="-2.295545166016" />
                  <Point X="27.309560546875" Y="-2.313724121094" />
                  <Point X="27.2886015625" Y="-2.334683105469" />
                  <Point X="27.21230078125" Y="-2.479659423828" />
                  <Point X="27.19412109375" Y="-2.514201171875" />
                  <Point X="27.1891640625" Y="-2.546375244141" />
                  <Point X="27.220681640625" Y="-2.720886962891" />
                  <Point X="27.22819140625" Y="-2.762465576172" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.383265625" Y="-3.036954589844" />
                  <Point X="27.98667578125" Y="-4.082087890625" />
                  <Point X="27.870779296875" Y="-4.164868652344" />
                  <Point X="27.835279296875" Y="-4.190224609375" />
                  <Point X="27.67977734375" Y="-4.290879394531" />
                  <Point X="27.51070703125" Y="-4.07054296875" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467529297" />
                  <Point X="26.49843359375" Y="-2.869812988281" />
                  <Point X="26.45742578125" Y="-2.843448730469" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.237568359375" Y="-2.853038818359" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509277344" />
                  <Point X="26.01998046875" Y="-2.989364990234" />
                  <Point X="25.985349609375" Y="-3.018159667969" />
                  <Point X="25.96845703125" Y="-3.045985351562" />
                  <Point X="25.924998046875" Y="-3.245933837891" />
                  <Point X="25.914642578125" Y="-3.293573242188" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.956205078125" Y="-3.631827636719" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.027912109375" Y="-4.955889648438" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#198" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.156061723514" Y="4.937554784567" Z="2.05" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.05" />
                  <Point X="-0.345389572826" Y="5.058516680955" Z="2.05" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.05" />
                  <Point X="-1.131460266896" Y="4.942428104167" Z="2.05" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.05" />
                  <Point X="-1.71589601634" Y="4.50584653185" Z="2.05" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.05" />
                  <Point X="-1.713856853068" Y="4.423481960811" Z="2.05" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.05" />
                  <Point X="-1.759007531452" Y="4.332899850776" Z="2.05" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.05" />
                  <Point X="-1.857419911193" Y="4.309261883632" Z="2.05" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.05" />
                  <Point X="-2.095811966217" Y="4.559758099315" Z="2.05" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.05" />
                  <Point X="-2.259789677052" Y="4.540178315111" Z="2.05" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.05" />
                  <Point X="-2.900466297684" Y="4.160179403398" Z="2.05" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.05" />
                  <Point X="-3.074092346887" Y="3.266003777046" Z="2.05" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.05" />
                  <Point X="-3.000084535351" Y="3.123852070152" Z="2.05" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.05" />
                  <Point X="-3.005724034164" Y="3.043079533723" Z="2.05" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.05" />
                  <Point X="-3.071224395455" Y="2.995480163822" Z="2.05" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.05" />
                  <Point X="-3.667855571251" Y="3.306101601859" Z="2.05" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.05" />
                  <Point X="-3.873230399871" Y="3.276246752366" Z="2.05" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.05" />
                  <Point X="-4.273091904677" Y="2.734290693657" Z="2.05" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.05" />
                  <Point X="-3.860323746639" Y="1.736492692725" Z="2.05" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.05" />
                  <Point X="-3.690839944027" Y="1.599841595554" Z="2.05" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.05" />
                  <Point X="-3.671564937153" Y="1.542254963542" Z="2.05" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.05" />
                  <Point X="-3.703289122963" Y="1.490473529865" Z="2.05" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.05" />
                  <Point X="-4.611845064803" Y="1.587915349362" Z="2.05" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.05" />
                  <Point X="-4.8465768165" Y="1.503850332546" Z="2.05" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.05" />
                  <Point X="-4.98966622695" Y="0.924162146069" Z="2.05" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.05" />
                  <Point X="-3.862057218618" Y="0.125567585289" Z="2.05" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.05" />
                  <Point X="-3.571220711147" Y="0.045362750081" Z="2.05" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.05" />
                  <Point X="-3.547027820652" Y="0.024071695047" Z="2.05" />
                  <Point X="-3.539556741714" Y="0" Z="2.05" />
                  <Point X="-3.541336780483" Y="-0.005735256013" Z="2.05" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.05" />
                  <Point X="-3.554147883976" Y="-0.033513233053" Z="2.05" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.05" />
                  <Point X="-4.774830570155" Y="-0.370144471467" Z="2.05" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.05" />
                  <Point X="-5.045383205101" Y="-0.551128833465" Z="2.05" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.05" />
                  <Point X="-4.956539992345" Y="-1.091936418944" Z="2.05" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.05" />
                  <Point X="-3.532358686375" Y="-1.348096835761" Z="2.05" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.05" />
                  <Point X="-3.214063284898" Y="-1.309862358117" Z="2.05" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.05" />
                  <Point X="-3.194158753913" Y="-1.328173594577" Z="2.05" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.05" />
                  <Point X="-4.252278371728" Y="-2.159346529909" Z="2.05" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.05" />
                  <Point X="-4.446418477542" Y="-2.446367620021" Z="2.05" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.05" />
                  <Point X="-4.142565118095" Y="-2.931635083954" Z="2.05" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.05" />
                  <Point X="-2.820937184329" Y="-2.698730234309" Z="2.05" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.05" />
                  <Point X="-2.569501409736" Y="-2.558829059395" Z="2.05" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.05" />
                  <Point X="-3.156686778859" Y="-3.614141198163" Z="2.05" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.05" />
                  <Point X="-3.22114228452" Y="-3.922899753615" Z="2.05" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.05" />
                  <Point X="-2.805937203153" Y="-4.229846176627" Z="2.05" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.05" />
                  <Point X="-2.269495174205" Y="-4.212846497244" Z="2.05" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.05" />
                  <Point X="-2.176586114454" Y="-4.123286306883" Z="2.05" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.05" />
                  <Point X="-1.908686904358" Y="-3.987988736587" Z="2.05" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.05" />
                  <Point X="-1.61378433819" Y="-4.043736893921" Z="2.05" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.05" />
                  <Point X="-1.413759689477" Y="-4.26749030341" Z="2.05" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.05" />
                  <Point X="-1.403820778501" Y="-4.809027807669" Z="2.05" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.05" />
                  <Point X="-1.356202952323" Y="-4.894142121602" Z="2.05" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.05" />
                  <Point X="-1.059729054718" Y="-4.966778062887" Z="2.05" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.05" />
                  <Point X="-0.494163979497" Y="-3.806429123842" Z="2.05" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.05" />
                  <Point X="-0.385583380809" Y="-3.473382686698" Z="2.05" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.05" />
                  <Point X="-0.204609404142" Y="-3.267742576692" Z="2.05" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.05" />
                  <Point X="0.048749675218" Y="-3.219369468596" Z="2.05" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.05" />
                  <Point X="0.284862478624" Y="-3.32826306907" Z="2.05" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.05" />
                  <Point X="0.740590880904" Y="-4.726106905974" Z="2.05" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.05" />
                  <Point X="0.852368308019" Y="-5.007459185956" Z="2.05" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.05" />
                  <Point X="1.032460390834" Y="-4.973449906005" Z="2.05" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.05" />
                  <Point X="0.999620385556" Y="-3.594021136898" Z="2.05" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.05" />
                  <Point X="0.96770036171" Y="-3.225274513428" Z="2.05" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.05" />
                  <Point X="1.04579129374" Y="-2.996531241797" Z="2.05" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.05" />
                  <Point X="1.235992750459" Y="-2.871548394122" Z="2.05" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.05" />
                  <Point X="1.465238256601" Y="-2.880590815793" Z="2.05" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.05" />
                  <Point X="2.464882077291" Y="-4.069700770901" Z="2.05" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.05" />
                  <Point X="2.699610938868" Y="-4.302335992734" Z="2.05" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.05" />
                  <Point X="2.893685697903" Y="-4.174275675276" Z="2.05" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.05" />
                  <Point X="2.420410310898" Y="-2.980674446849" Z="2.05" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.05" />
                  <Point X="2.263727813843" Y="-2.680719931401" Z="2.05" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.05" />
                  <Point X="2.250390152276" Y="-2.471666520364" Z="2.05" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.05" />
                  <Point X="2.361231858008" Y="-2.308511157691" Z="2.05" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.05" />
                  <Point X="2.547786872111" Y="-2.239720195648" Z="2.05" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.05" />
                  <Point X="3.806739150258" Y="-2.897339189377" Z="2.05" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.05" />
                  <Point X="4.098711750201" Y="-2.998776213844" Z="2.05" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.05" />
                  <Point X="4.270409547741" Y="-2.748763045649" Z="2.05" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.05" />
                  <Point X="3.424882964217" Y="-1.792720447032" Z="2.05" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.05" />
                  <Point X="3.173409067564" Y="-1.584520640644" Z="2.05" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.05" />
                  <Point X="3.095288966916" Y="-1.425413228641" Z="2.05" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.05" />
                  <Point X="3.129107684116" Y="-1.261975965772" Z="2.05" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.05" />
                  <Point X="3.252670934429" Y="-1.147790828024" Z="2.05" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.05" />
                  <Point X="4.616903773121" Y="-1.276221008595" Z="2.05" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.05" />
                  <Point X="4.923252518858" Y="-1.243222568524" Z="2.05" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.05" />
                  <Point X="5.00232450809" Y="-0.872217453006" Z="2.05" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.05" />
                  <Point X="3.998102095533" Y="-0.287837962411" Z="2.05" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.05" />
                  <Point X="3.730152541352" Y="-0.210521826315" Z="2.05" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.05" />
                  <Point X="3.644762619705" Y="-0.153729256581" Z="2.05" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.05" />
                  <Point X="3.596376692047" Y="-0.078021372973" Z="2.05" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.05" />
                  <Point X="3.584994758377" Y="0.018589158214" Z="2.05" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.05" />
                  <Point X="3.610616818695" Y="0.110219481994" Z="2.05" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.05" />
                  <Point X="3.673242873001" Y="0.177627017927" Z="2.05" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.05" />
                  <Point X="4.797865721773" Y="0.502133970358" Z="2.05" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.05" />
                  <Point X="5.035334646635" Y="0.65060589122" Z="2.05" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.05" />
                  <Point X="4.962616133929" Y="1.072527370675" Z="2.05" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.05" />
                  <Point X="3.735898937029" Y="1.25793593636" Z="2.05" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.05" />
                  <Point X="3.445003617907" Y="1.224418570627" Z="2.05" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.05" />
                  <Point X="3.355560553918" Y="1.242011655283" Z="2.05" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.05" />
                  <Point X="3.290071635506" Y="1.287725937644" Z="2.05" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.05" />
                  <Point X="3.247861251184" Y="1.363193300112" Z="2.05" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.05" />
                  <Point X="3.237733529699" Y="1.447158203649" Z="2.05" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.05" />
                  <Point X="3.266234048747" Y="1.523818091726" Z="2.05" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.05" />
                  <Point X="4.22903550732" Y="2.287671988631" Z="2.05" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.05" />
                  <Point X="4.407072872855" Y="2.521656713079" Z="2.05" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.05" />
                  <Point X="4.192789270756" Y="2.863835735763" Z="2.05" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.05" />
                  <Point X="2.797032414539" Y="2.43278748303" Z="2.05" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.05" />
                  <Point X="2.494429769152" Y="2.262867731022" Z="2.05" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.05" />
                  <Point X="2.416233382731" Y="2.247139935107" Z="2.05" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.05" />
                  <Point X="2.347985344505" Y="2.262166133984" Z="2.05" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.05" />
                  <Point X="2.288592466251" Y="2.309039515877" Z="2.05" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.05" />
                  <Point X="2.252289767652" Y="2.373525061101" Z="2.05" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.05" />
                  <Point X="2.249660211635" Y="2.44503980673" Z="2.05" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.05" />
                  <Point X="2.96283786983" Y="3.715106667416" Z="2.05" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.05" />
                  <Point X="3.056446797409" Y="4.053591439075" Z="2.05" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.05" />
                  <Point X="2.676968282912" Y="4.313618298063" Z="2.05" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.05" />
                  <Point X="2.276540076682" Y="4.537803183401" Z="2.05" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.05" />
                  <Point X="1.861814224208" Y="4.723127164625" Z="2.05" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.05" />
                  <Point X="1.390864807794" Y="4.878678580506" Z="2.05" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.05" />
                  <Point X="0.733773678584" Y="5.019715197532" Z="2.05" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.05" />
                  <Point X="0.037182838039" Y="4.493892721383" Z="2.05" />
                  <Point X="0" Y="4.355124473572" Z="2.05" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>