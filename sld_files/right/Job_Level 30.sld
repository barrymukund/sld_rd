<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#175" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2213" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.79218359375" Y="-4.366711425781" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.44881640625" Y="-3.332591308594" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.157736328125" Y="-3.130740966797" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.818416015625" Y="-3.141964111328" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.54012890625" Y="-3.366262695312" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.277306640625" Y="-4.153327636719" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="24.044859375" Y="-4.869458007812" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.75593359375" Y="-4.802967773438" />
                  <Point X="23.75358203125" Y="-4.802362792969" />
                  <Point X="23.757486328125" Y="-4.772712402344" />
                  <Point X="23.7850390625" Y="-4.5634375" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.748908203125" Y="-4.342361328125" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.543078125" Y="-4.014322753906" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295898438" />
                  <Point X="23.387115234375" Y="-3.897994628906" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.180083984375" Y="-3.879372314453" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.80994921875" Y="-3.993286865234" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.5685" Y="-4.22508984375" />
                  <Point X="22.5198515625" Y="-4.288488769531" />
                  <Point X="22.38033984375" Y="-4.202106445313" />
                  <Point X="22.19828515625" Y="-4.089383300781" />
                  <Point X="21.9701953125" Y="-3.91376171875" />
                  <Point X="21.895279296875" Y="-3.856078613281" />
                  <Point X="22.15089453125" Y="-3.413338623047" />
                  <Point X="22.576240234375" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647655273438" />
                  <Point X="22.593412109375" Y="-2.616130126953" />
                  <Point X="22.59442578125" Y="-2.585197753906" />
                  <Point X="22.585443359375" Y="-2.555581298828" />
                  <Point X="22.571228515625" Y="-2.526752929688" />
                  <Point X="22.553201171875" Y="-2.501592773438" />
                  <Point X="22.535853515625" Y="-2.484244140625" />
                  <Point X="22.510697265625" Y="-2.466216064453" />
                  <Point X="22.4818671875" Y="-2.451997314453" />
                  <Point X="22.452248046875" Y="-2.44301171875" />
                  <Point X="22.4213125" Y="-2.444023925781" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.80813671875" Y="-2.780287353516" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="21.060966796875" Y="-2.982819580078" />
                  <Point X="20.917142578125" Y="-2.79386328125" />
                  <Point X="20.753615234375" Y="-2.519654541016" />
                  <Point X="20.693857421875" Y="-2.419450195312" />
                  <Point X="21.149830078125" Y="-2.069570068359" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.915421875" Y="-1.475594482422" />
                  <Point X="21.93338671875" Y="-1.448464233398" />
                  <Point X="21.946142578125" Y="-1.419834960938" />
                  <Point X="21.95384765625" Y="-1.390087768555" />
                  <Point X="21.95665234375" Y="-1.359662963867" />
                  <Point X="21.9544453125" Y="-1.327991699219" />
                  <Point X="21.9474453125" Y="-1.298242553711" />
                  <Point X="21.931361328125" Y="-1.272255615234" />
                  <Point X="21.91052734375" Y="-1.248298706055" />
                  <Point X="21.88702734375" Y="-1.228766723633" />
                  <Point X="21.860544921875" Y="-1.213180297852" />
                  <Point X="21.831283203125" Y="-1.201956176758" />
                  <Point X="21.799396484375" Y="-1.195474731445" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="21.070365234375" Y="-1.286238647461" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.22217578125" Y="-1.212879638672" />
                  <Point X="20.165923828125" Y="-0.992650146484" />
                  <Point X="20.122658203125" Y="-0.690153747559" />
                  <Point X="20.107576171875" Y="-0.584698364258" />
                  <Point X="20.6193515625" Y="-0.44756854248" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.49019921875" Y="-0.210823822021" />
                  <Point X="21.521623046875" Y="-0.192598495483" />
                  <Point X="21.534798828125" Y="-0.183384674072" />
                  <Point X="21.558515625" Y="-0.163609725952" />
                  <Point X="21.57454296875" Y="-0.146474822998" />
                  <Point X="21.585880859375" Y="-0.125934738159" />
                  <Point X="21.598591796875" Y="-0.094165161133" />
                  <Point X="21.6030703125" Y="-0.07973551178" />
                  <Point X="21.60928125" Y="-0.052139865875" />
                  <Point X="21.61159765625" Y="-0.030766374588" />
                  <Point X="21.60905078125" Y="-0.009419221878" />
                  <Point X="21.60151953125" Y="0.022430803299" />
                  <Point X="21.59684375" Y="0.036907051086" />
                  <Point X="21.585453125" Y="0.06442224884" />
                  <Point X="21.57387109375" Y="0.084825958252" />
                  <Point X="21.557642578125" Y="0.10176890564" />
                  <Point X="21.52996484375" Y="0.124293083191" />
                  <Point X="21.516638671875" Y="0.13337272644" />
                  <Point X="21.48917578125" Y="0.148848815918" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="20.8311328125" Y="0.328261871338" />
                  <Point X="20.10818359375" Y="0.521975402832" />
                  <Point X="20.139259765625" Y="0.731981811523" />
                  <Point X="20.17551171875" Y="0.976968444824" />
                  <Point X="20.262603515625" Y="1.298364990234" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.626630859375" Y="1.379798828125" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228027344" />
                  <Point X="21.296865234375" Y="1.305263793945" />
                  <Point X="21.3319609375" Y="1.316329589844" />
                  <Point X="21.358291015625" Y="1.324631103516" />
                  <Point X="21.37722265625" Y="1.332962036133" />
                  <Point X="21.395966796875" Y="1.343784179688" />
                  <Point X="21.4126484375" Y="1.35601574707" />
                  <Point X="21.426287109375" Y="1.371567993164" />
                  <Point X="21.438701171875" Y="1.389298095703" />
                  <Point X="21.448650390625" Y="1.407432983398" />
                  <Point X="21.462732421875" Y="1.441431762695" />
                  <Point X="21.473296875" Y="1.466937255859" />
                  <Point X="21.479083984375" Y="1.486788452148" />
                  <Point X="21.48284375" Y="1.508103759766" />
                  <Point X="21.484197265625" Y="1.528750244141" />
                  <Point X="21.481048828125" Y="1.549200195312" />
                  <Point X="21.4754453125" Y="1.570106811523" />
                  <Point X="21.46794921875" Y="1.589378295898" />
                  <Point X="21.45095703125" Y="1.622020263672" />
                  <Point X="21.438208984375" Y="1.646508056641" />
                  <Point X="21.426716796875" Y="1.663707763672" />
                  <Point X="21.4128046875" Y="1.680287231445" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="21.033056640625" Y="1.974516479492" />
                  <Point X="20.648140625" Y="2.269873535156" />
                  <Point X="20.777984375" Y="2.492325683594" />
                  <Point X="20.918853515625" Y="2.733665771484" />
                  <Point X="21.149544921875" Y="3.030188964844" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.420109375" Y="3.060158447266" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.902087890625" Y="2.821520019531" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.95943359375" Y="2.818762939453" />
                  <Point X="21.980892578125" Y="2.821587890625" />
                  <Point X="22.000984375" Y="2.826504150391" />
                  <Point X="22.019537109375" Y="2.83565234375" />
                  <Point X="22.037791015625" Y="2.847281005859" />
                  <Point X="22.053923828125" Y="2.860228759766" />
                  <Point X="22.088619140625" Y="2.894924072266" />
                  <Point X="22.1146484375" Y="2.920952148438" />
                  <Point X="22.127595703125" Y="2.937083984375" />
                  <Point X="22.139224609375" Y="2.955337890625" />
                  <Point X="22.14837109375" Y="2.973887207031" />
                  <Point X="22.1532890625" Y="2.993976318359" />
                  <Point X="22.156115234375" Y="3.015434814453" />
                  <Point X="22.15656640625" Y="3.03612109375" />
                  <Point X="22.1522890625" Y="3.085001220703" />
                  <Point X="22.14908203125" Y="3.121670654297" />
                  <Point X="22.145044921875" Y="3.141959472656" />
                  <Point X="22.13853515625" Y="3.162603271484" />
                  <Point X="22.130205078125" Y="3.181533203125" />
                  <Point X="21.968546875" Y="3.461531005859" />
                  <Point X="21.81666796875" Y="3.724595703125" />
                  <Point X="22.054041015625" Y="3.906587158203" />
                  <Point X="22.29937890625" Y="4.094685791016" />
                  <Point X="22.66271484375" Y="4.296547363281" />
                  <Point X="22.83296484375" Y="4.391134277344" />
                  <Point X="22.842546875" Y="4.378645507812" />
                  <Point X="22.9568046875" Y="4.229740722656" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.0592890625" Y="4.16107421875" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.11938671875" Y="4.132330078125" />
                  <Point X="23.140294921875" Y="4.126728515625" />
                  <Point X="23.160736328125" Y="4.123582519531" />
                  <Point X="23.181375" Y="4.124935546875" />
                  <Point X="23.202689453125" Y="4.128693847656" />
                  <Point X="23.222548828125" Y="4.134482421875" />
                  <Point X="23.279212890625" Y="4.157954101562" />
                  <Point X="23.32172265625" Y="4.175562011719" />
                  <Point X="23.33985546875" Y="4.185509765625" />
                  <Point X="23.3575859375" Y="4.197924316406" />
                  <Point X="23.373140625" Y="4.211565429688" />
                  <Point X="23.385373046875" Y="4.22825" />
                  <Point X="23.396193359375" Y="4.246994140625" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.42296484375" Y="4.324416015625" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583984375" />
                  <Point X="23.44272265625" Y="4.410145507812" />
                  <Point X="23.442271484375" Y="4.430827148438" />
                  <Point X="23.423892578125" Y="4.570426269531" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.732763671875" Y="4.720764160156" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.490837890625" Y="4.861359375" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.75568359375" Y="4.698384277344" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.229048828125" Y="4.595445800781" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.56672265625" Y="4.860781738281" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.208462890625" Y="4.743756835938" />
                  <Point X="26.4810234375" Y="4.677951660156" />
                  <Point X="26.71759375" Y="4.592145996094" />
                  <Point X="26.894650390625" Y="4.527926757812" />
                  <Point X="27.12401171875" Y="4.420662109375" />
                  <Point X="27.294572265625" Y="4.340896484375" />
                  <Point X="27.5161796875" Y="4.211787597656" />
                  <Point X="27.680982421875" Y="4.1157734375" />
                  <Point X="27.88995703125" Y="3.967161621094" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.639888671875" Y="3.403795166016" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.539935546875" />
                  <Point X="27.133078125" Y="2.516058349609" />
                  <Point X="27.120810546875" Y="2.470185546875" />
                  <Point X="27.111607421875" Y="2.435771972656" />
                  <Point X="27.108619140625" Y="2.417936035156" />
                  <Point X="27.107728515625" Y="2.380953369141" />
                  <Point X="27.11251171875" Y="2.341286376953" />
                  <Point X="27.116099609375" Y="2.311528564453" />
                  <Point X="27.121443359375" Y="2.289602050781" />
                  <Point X="27.1297109375" Y="2.267511962891" />
                  <Point X="27.140072265625" Y="2.247469726562" />
                  <Point X="27.1646171875" Y="2.211297363281" />
                  <Point X="27.18303125" Y="2.184161132813" />
                  <Point X="27.19446484375" Y="2.170329589844" />
                  <Point X="27.221599609375" Y="2.145592529297" />
                  <Point X="27.257771484375" Y="2.121047851562" />
                  <Point X="27.284908203125" Y="2.102635009766" />
                  <Point X="27.304955078125" Y="2.092271728516" />
                  <Point X="27.32704296875" Y="2.084006103516" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.388630859375" Y="2.073880371094" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.473205078125" Y="2.074171142578" />
                  <Point X="27.519078125" Y="2.086438232422" />
                  <Point X="27.5534921875" Y="2.095640869141" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.228220703125" Y="2.479467773438" />
                  <Point X="28.967328125" Y="2.90619140625" />
                  <Point X="29.02551953125" Y="2.825318359375" />
                  <Point X="29.12326953125" Y="2.689468017578" />
                  <Point X="29.239775390625" Y="2.496941162109" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.878951171875" Y="2.165806152344" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.2214296875" Y="1.660245605469" />
                  <Point X="28.20397265625" Y="1.641626708984" />
                  <Point X="28.170958984375" Y="1.598556518555" />
                  <Point X="28.14619140625" Y="1.566245239258" />
                  <Point X="28.13660546875" Y="1.55091027832" />
                  <Point X="28.1216328125" Y="1.517088378906" />
                  <Point X="28.109333984375" Y="1.473113525391" />
                  <Point X="28.100107421875" Y="1.440124023438" />
                  <Point X="28.09665234375" Y="1.417824829102" />
                  <Point X="28.0958359375" Y="1.394252807617" />
                  <Point X="28.097740234375" Y="1.371766723633" />
                  <Point X="28.1078359375" Y="1.322838989258" />
                  <Point X="28.11541015625" Y="1.286133911133" />
                  <Point X="28.120681640625" Y="1.268976074219" />
                  <Point X="28.136283203125" Y="1.235741210938" />
                  <Point X="28.1637421875" Y="1.194005371094" />
                  <Point X="28.18433984375" Y="1.162695678711" />
                  <Point X="28.19889453125" Y="1.145450317383" />
                  <Point X="28.216140625" Y="1.129359130859" />
                  <Point X="28.23434765625" Y="1.116034667969" />
                  <Point X="28.274138671875" Y="1.093635742188" />
                  <Point X="28.303990234375" Y="1.076832275391" />
                  <Point X="28.3205234375" Y="1.069501586914" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.409919921875" Y="1.052328125" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.09586328125" Y="1.126984375" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.803951171875" Y="1.105270385742" />
                  <Point X="29.845939453125" Y="0.932788146973" />
                  <Point X="29.882650390625" Y="0.69700579834" />
                  <Point X="29.890865234375" Y="0.644238769531" />
                  <Point X="29.459392578125" Y="0.528625488281" />
                  <Point X="28.716580078125" Y="0.32958972168" />
                  <Point X="28.7047890625" Y="0.325585357666" />
                  <Point X="28.681546875" Y="0.315067932129" />
                  <Point X="28.628689453125" Y="0.284515563965" />
                  <Point X="28.589037109375" Y="0.26159552002" />
                  <Point X="28.5743125" Y="0.251096191406" />
                  <Point X="28.54753125" Y="0.22557673645" />
                  <Point X="28.51581640625" Y="0.185165283203" />
                  <Point X="28.492025390625" Y="0.154849090576" />
                  <Point X="28.48030078125" Y="0.135566101074" />
                  <Point X="28.47052734375" Y="0.114101722717" />
                  <Point X="28.463681640625" Y="0.092604255676" />
                  <Point X="28.453109375" Y="0.037404319763" />
                  <Point X="28.4451796875" Y="-0.004006072998" />
                  <Point X="28.443484375" Y="-0.021876356125" />
                  <Point X="28.4451796875" Y="-0.05855286026" />
                  <Point X="28.45575" Y="-0.113752952576" />
                  <Point X="28.463681640625" Y="-0.155163345337" />
                  <Point X="28.470529296875" Y="-0.176667633057" />
                  <Point X="28.480302734375" Y="-0.198129730225" />
                  <Point X="28.492025390625" Y="-0.217409240723" />
                  <Point X="28.523740234375" Y="-0.257820709229" />
                  <Point X="28.54753125" Y="-0.288137023926" />
                  <Point X="28.56" Y="-0.301235473633" />
                  <Point X="28.589037109375" Y="-0.324155670166" />
                  <Point X="28.64189453125" Y="-0.35470803833" />
                  <Point X="28.681546875" Y="-0.377628082275" />
                  <Point X="28.692708984375" Y="-0.383137786865" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="29.27383203125" Y="-0.54146472168" />
                  <Point X="29.891474609375" Y="-0.706961730957" />
                  <Point X="29.878466796875" Y="-0.793236572266" />
                  <Point X="29.855025390625" Y="-0.948724182129" />
                  <Point X="29.80798828125" Y="-1.154843261719" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="29.2858359375" Y="-1.116853149414" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.270919921875" Y="-1.028057128906" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.1639765625" Y="-1.056596313477" />
                  <Point X="28.1361484375" Y="-1.073489013672" />
                  <Point X="28.1123984375" Y="-1.093960083008" />
                  <Point X="28.049693359375" Y="-1.169373535156" />
                  <Point X="28.002654296875" Y="-1.225947998047" />
                  <Point X="27.987931640625" Y="-1.250334472656" />
                  <Point X="27.97658984375" Y="-1.277719238281" />
                  <Point X="27.969759765625" Y="-1.305365966797" />
                  <Point X="27.9607734375" Y="-1.403029785156" />
                  <Point X="27.95403125" Y="-1.476296386719" />
                  <Point X="27.95634765625" Y="-1.507560791016" />
                  <Point X="27.964078125" Y="-1.539182495117" />
                  <Point X="27.976451171875" Y="-1.567996704102" />
                  <Point X="28.033861328125" Y="-1.657295898438" />
                  <Point X="28.076931640625" Y="-1.724287231445" />
                  <Point X="28.0869375" Y="-1.737243896484" />
                  <Point X="28.110630859375" Y="-1.760909423828" />
                  <Point X="28.62776171875" Y="-2.157718505859" />
                  <Point X="29.213123046875" Y="-2.606882324219" />
                  <Point X="29.190892578125" Y="-2.642854492188" />
                  <Point X="29.124814453125" Y="-2.749778808594" />
                  <Point X="29.028982421875" Y="-2.885944824219" />
                  <Point X="28.5681953125" Y="-2.619909423828" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.6307578125" Y="-2.13752734375" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.342263671875" Y="-2.189159179688" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246549072266" />
                  <Point X="27.22142578125" Y="-2.267508300781" />
                  <Point X="27.204533203125" Y="-2.290438476562" />
                  <Point X="27.15055078125" Y="-2.393009033203" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.100228515625" Y="-2.499735107422" />
                  <Point X="27.095271484375" Y="-2.531908935547" />
                  <Point X="27.09567578125" Y="-2.563259277344" />
                  <Point X="27.117974609375" Y="-2.686726074219" />
                  <Point X="27.134703125" Y="-2.779349609375" />
                  <Point X="27.13898828125" Y="-2.795140869141" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.484130859375" Y="-3.401655273438" />
                  <Point X="27.861287109375" Y="-4.05490625" />
                  <Point X="27.860267578125" Y="-4.055633789062" />
                  <Point X="27.781869140625" Y="-4.111630371094" />
                  <Point X="27.701765625" Y="-4.163480957031" />
                  <Point X="27.3440234375" Y="-3.697260498047" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.600154296875" Y="-2.822269287109" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.283921875" Y="-2.753371826172" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.00176171875" Y="-2.880966308594" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968861572266" />
                  <Point X="25.88725" Y="-2.996688476562" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.844876953125" Y="-3.167271972656" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627685547" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.913916015625" Y="-4.038441894531" />
                  <Point X="26.022064453125" Y="-4.859915039062" />
                  <Point X="25.9756796875" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94157421875" Y="-4.752636230469" />
                  <Point X="23.858755859375" Y="-4.731328613281" />
                  <Point X="23.8792265625" Y="-4.57583984375" />
                  <Point X="23.879923828125" Y="-4.568098144531" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688964844" />
                  <Point X="23.84208203125" Y="-4.323827148438" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.811875" Y="-4.178468261719" />
                  <Point X="23.80097265625" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.74979296875" Y="-4.070937744141" />
                  <Point X="23.738994140625" Y="-4.059779296875" />
                  <Point X="23.605716796875" Y="-3.942897949219" />
                  <Point X="23.505734375" Y="-3.855214599609" />
                  <Point X="23.49326171875" Y="-3.845964599609" />
                  <Point X="23.466978515625" Y="-3.82962109375" />
                  <Point X="23.45316796875" Y="-3.822527587891" />
                  <Point X="23.423470703125" Y="-3.810226318359" />
                  <Point X="23.4086875" Y="-3.805476318359" />
                  <Point X="23.378544921875" Y="-3.798447998047" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.186296875" Y="-3.784575683594" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812255859" />
                  <Point X="22.757169921875" Y="-3.914297363281" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629394531" />
                  <Point X="22.496798828125" Y="-4.162478027344" />
                  <Point X="22.4303515625" Y="-4.1213359375" />
                  <Point X="22.25240625" Y="-4.011157226562" />
                  <Point X="22.02815234375" Y="-3.838489257813" />
                  <Point X="22.019138671875" Y="-3.831549072266" />
                  <Point X="22.23316796875" Y="-3.460838623047" />
                  <Point X="22.658513671875" Y="-2.724119628906" />
                  <Point X="22.66515234375" Y="-2.710080566406" />
                  <Point X="22.676052734375" Y="-2.681116210938" />
                  <Point X="22.680314453125" Y="-2.666190917969" />
                  <Point X="22.6865859375" Y="-2.634665771484" />
                  <Point X="22.688361328125" Y="-2.619241699219" />
                  <Point X="22.689375" Y="-2.588309326172" />
                  <Point X="22.6853359375" Y="-2.557625244141" />
                  <Point X="22.676353515625" Y="-2.528008789062" />
                  <Point X="22.6706484375" Y="-2.513568115234" />
                  <Point X="22.65643359375" Y="-2.484739746094" />
                  <Point X="22.648451171875" Y="-2.471421875" />
                  <Point X="22.630423828125" Y="-2.44626171875" />
                  <Point X="22.62037890625" Y="-2.434419433594" />
                  <Point X="22.60303125" Y="-2.417070800781" />
                  <Point X="22.59119140625" Y="-2.407025634766" />
                  <Point X="22.56603515625" Y="-2.388997558594" />
                  <Point X="22.55271875" Y="-2.381014648438" />
                  <Point X="22.523888671875" Y="-2.366795898438" />
                  <Point X="22.5094453125" Y="-2.361088623047" />
                  <Point X="22.479826171875" Y="-2.352103027344" />
                  <Point X="22.449140625" Y="-2.3480625" />
                  <Point X="22.418205078125" Y="-2.349074707031" />
                  <Point X="22.402779296875" Y="-2.350849121094" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.76063671875" Y="-2.698014892578" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="21.136560546875" Y="-2.92528125" />
                  <Point X="20.99598046875" Y="-2.740588134766" />
                  <Point X="20.83520703125" Y="-2.470995849609" />
                  <Point X="20.818734375" Y="-2.443373535156" />
                  <Point X="21.207662109375" Y="-2.144938720703" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.963515625" Y="-1.563310913086" />
                  <Point X="21.984892578125" Y="-1.540392089844" />
                  <Point X="21.994630859375" Y="-1.528044189453" />
                  <Point X="22.012595703125" Y="-1.50091394043" />
                  <Point X="22.020162109375" Y="-1.487127685547" />
                  <Point X="22.03291796875" Y="-1.458498413086" />
                  <Point X="22.038107421875" Y="-1.443655639648" />
                  <Point X="22.0458125" Y="-1.413908447266" />
                  <Point X="22.048447265625" Y="-1.398808349609" />
                  <Point X="22.051251953125" Y="-1.368383666992" />
                  <Point X="22.051421875" Y="-1.353058837891" />
                  <Point X="22.04921484375" Y="-1.321387573242" />
                  <Point X="22.046919921875" Y="-1.306232543945" />
                  <Point X="22.039919921875" Y="-1.276483276367" />
                  <Point X="22.028224609375" Y="-1.24824597168" />
                  <Point X="22.012140625" Y="-1.222259033203" />
                  <Point X="22.003046875" Y="-1.209915283203" />
                  <Point X="21.982212890625" Y="-1.185958496094" />
                  <Point X="21.97125" Y="-1.175239257812" />
                  <Point X="21.94775" Y="-1.155707275391" />
                  <Point X="21.935212890625" Y="-1.14689453125" />
                  <Point X="21.90873046875" Y="-1.131308105469" />
                  <Point X="21.894568359375" Y="-1.124481689453" />
                  <Point X="21.865306640625" Y="-1.113257568359" />
                  <Point X="21.85020703125" Y="-1.108859741211" />
                  <Point X="21.8183203125" Y="-1.102378295898" />
                  <Point X="21.802703125" Y="-1.100532348633" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="21.05796484375" Y="-1.192051513672" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.314220703125" Y="-1.189368041992" />
                  <Point X="20.259240234375" Y="-0.974112854004" />
                  <Point X="20.216701171875" Y="-0.676702880859" />
                  <Point X="20.213548828125" Y="-0.654654602051" />
                  <Point X="20.643939453125" Y="-0.539331481934" />
                  <Point X="21.491712890625" Y="-0.312171325684" />
                  <Point X="21.50356640625" Y="-0.308140686035" />
                  <Point X="21.526640625" Y="-0.298556304932" />
                  <Point X="21.537861328125" Y="-0.293002410889" />
                  <Point X="21.56928515625" Y="-0.274777069092" />
                  <Point X="21.576064453125" Y="-0.27045111084" />
                  <Point X="21.59563671875" Y="-0.256349182129" />
                  <Point X="21.619353515625" Y="-0.236574142456" />
                  <Point X="21.627896484375" Y="-0.228505111694" />
                  <Point X="21.643923828125" Y="-0.211370239258" />
                  <Point X="21.657712890625" Y="-0.192384109497" />
                  <Point X="21.66905078125" Y="-0.171843978882" />
                  <Point X="21.674083984375" Y="-0.161224273682" />
                  <Point X="21.686794921875" Y="-0.12945463562" />
                  <Point X="21.689322265625" Y="-0.122325050354" />
                  <Point X="21.695751953125" Y="-0.100595344543" />
                  <Point X="21.701962890625" Y="-0.072999633789" />
                  <Point X="21.703728515625" Y="-0.062375774384" />
                  <Point X="21.706044921875" Y="-0.041002315521" />
                  <Point X="21.705927734375" Y="-0.019512037277" />
                  <Point X="21.703380859375" Y="0.001835116029" />
                  <Point X="21.701501953125" Y="0.012441589355" />
                  <Point X="21.693970703125" Y="0.044291625977" />
                  <Point X="21.691919921875" Y="0.051630180359" />
                  <Point X="21.684619140625" Y="0.073244110107" />
                  <Point X="21.673228515625" Y="0.100759407043" />
                  <Point X="21.6680703125" Y="0.111319511414" />
                  <Point X="21.65648828125" Y="0.131723205566" />
                  <Point X="21.6424765625" Y="0.150539016724" />
                  <Point X="21.626248046875" Y="0.167482025146" />
                  <Point X="21.617607421875" Y="0.175452819824" />
                  <Point X="21.5899296875" Y="0.197976913452" />
                  <Point X="21.58345703125" Y="0.202802108765" />
                  <Point X="21.56327734375" Y="0.216136108398" />
                  <Point X="21.535814453125" Y="0.231612060547" />
                  <Point X="21.525072265625" Y="0.236805389404" />
                  <Point X="21.503021484375" Y="0.245804962158" />
                  <Point X="21.491712890625" Y="0.249611343384" />
                  <Point X="20.855720703125" Y="0.420024871826" />
                  <Point X="20.2145546875" Y="0.591824768066" />
                  <Point X="20.233236328125" Y="0.718075317383" />
                  <Point X="20.26866796875" Y="0.957522583008" />
                  <Point X="20.354296875" Y="1.273518066406" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="20.61423046875" Y="1.285611450195" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.20470324707" />
                  <Point X="21.28485546875" Y="1.206589477539" />
                  <Point X="21.295109375" Y="1.208053588867" />
                  <Point X="21.3153984375" Y="1.212089233398" />
                  <Point X="21.32543359375" Y="1.214660766602" />
                  <Point X="21.360529296875" Y="1.2257265625" />
                  <Point X="21.386859375" Y="1.234028076172" />
                  <Point X="21.3965546875" Y="1.237677734375" />
                  <Point X="21.415486328125" Y="1.246008789062" />
                  <Point X="21.42472265625" Y="1.250690063477" />
                  <Point X="21.443466796875" Y="1.261512207031" />
                  <Point X="21.452140625" Y="1.267172119141" />
                  <Point X="21.468822265625" Y="1.279403686523" />
                  <Point X="21.48407421875" Y="1.293378662109" />
                  <Point X="21.497712890625" Y="1.308930908203" />
                  <Point X="21.504107421875" Y="1.317080200195" />
                  <Point X="21.516521484375" Y="1.334810302734" />
                  <Point X="21.521990234375" Y="1.343603881836" />
                  <Point X="21.531939453125" Y="1.361738769531" />
                  <Point X="21.536419921875" Y="1.371079711914" />
                  <Point X="21.550501953125" Y="1.405078491211" />
                  <Point X="21.56106640625" Y="1.430583984375" />
                  <Point X="21.5645" Y="1.440349243164" />
                  <Point X="21.570287109375" Y="1.460200439453" />
                  <Point X="21.572640625" Y="1.470286499023" />
                  <Point X="21.576400390625" Y="1.49160168457" />
                  <Point X="21.577640625" Y="1.501889160156" />
                  <Point X="21.578994140625" Y="1.522535522461" />
                  <Point X="21.578091796875" Y="1.543205932617" />
                  <Point X="21.574943359375" Y="1.563655761719" />
                  <Point X="21.572810546875" Y="1.573794555664" />
                  <Point X="21.56720703125" Y="1.594701171875" />
                  <Point X="21.563982421875" Y="1.604545654297" />
                  <Point X="21.556486328125" Y="1.623817138672" />
                  <Point X="21.55221484375" Y="1.633244140625" />
                  <Point X="21.53522265625" Y="1.665886108398" />
                  <Point X="21.522474609375" Y="1.690373901367" />
                  <Point X="21.51719921875" Y="1.699286254883" />
                  <Point X="21.50570703125" Y="1.716485961914" />
                  <Point X="21.499490234375" Y="1.72477331543" />
                  <Point X="21.485578125" Y="1.741352783203" />
                  <Point X="21.478498046875" Y="1.748912719727" />
                  <Point X="21.463556640625" Y="1.763215698242" />
                  <Point X="21.4556953125" Y="1.769958618164" />
                  <Point X="21.090888671875" Y="2.049885009766" />
                  <Point X="20.77238671875" Y="2.294280761719" />
                  <Point X="20.86003125" Y="2.444436035156" />
                  <Point X="20.99771875" Y="2.680323730469" />
                  <Point X="21.224525390625" Y="2.971854736328" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.372609375" Y="2.977885986328" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.89380859375" Y="2.726881591797" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.961505859375" Y="2.723785644531" />
                  <Point X="21.97183203125" Y="2.724575683594" />
                  <Point X="21.993291015625" Y="2.727400634766" />
                  <Point X="22.00347265625" Y="2.729310302734" />
                  <Point X="22.023564453125" Y="2.7342265625" />
                  <Point X="22.042998046875" Y="2.741299316406" />
                  <Point X="22.06155078125" Y="2.750447509766" />
                  <Point X="22.070580078125" Y="2.755529541016" />
                  <Point X="22.088833984375" Y="2.767158203125" />
                  <Point X="22.09725390625" Y="2.773191650391" />
                  <Point X="22.11338671875" Y="2.786139404297" />
                  <Point X="22.121099609375" Y="2.793053710938" />
                  <Point X="22.155794921875" Y="2.827749023438" />
                  <Point X="22.18182421875" Y="2.853777099609" />
                  <Point X="22.188736328125" Y="2.861489257812" />
                  <Point X="22.20168359375" Y="2.87762109375" />
                  <Point X="22.20771875" Y="2.886040771484" />
                  <Point X="22.21934765625" Y="2.904294677734" />
                  <Point X="22.2244296875" Y="2.91332421875" />
                  <Point X="22.233576171875" Y="2.931873535156" />
                  <Point X="22.240646484375" Y="2.951297607422" />
                  <Point X="22.245564453125" Y="2.97138671875" />
                  <Point X="22.2474765625" Y="2.981571533203" />
                  <Point X="22.250302734375" Y="3.003030029297" />
                  <Point X="22.251091796875" Y="3.01336328125" />
                  <Point X="22.25154296875" Y="3.034049560547" />
                  <Point X="22.251205078125" Y="3.044402587891" />
                  <Point X="22.246927734375" Y="3.093282714844" />
                  <Point X="22.243720703125" Y="3.129952148438" />
                  <Point X="22.242255859375" Y="3.140210449219" />
                  <Point X="22.23821875" Y="3.160499267578" />
                  <Point X="22.235646484375" Y="3.170529785156" />
                  <Point X="22.22913671875" Y="3.191173583984" />
                  <Point X="22.22548828125" Y="3.200866943359" />
                  <Point X="22.217158203125" Y="3.219796875" />
                  <Point X="22.2124765625" Y="3.229033447266" />
                  <Point X="22.050818359375" Y="3.50903125" />
                  <Point X="21.94061328125" Y="3.699914550781" />
                  <Point X="22.11184375" Y="3.8311953125" />
                  <Point X="22.351634765625" Y="4.015041503906" />
                  <Point X="22.7088515625" Y="4.213503417969" />
                  <Point X="22.8074765625" Y="4.268296875" />
                  <Point X="22.88143359375" Y="4.171912109375" />
                  <Point X="22.8881796875" Y="4.164047363281" />
                  <Point X="22.902482421875" Y="4.149106445313" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.934908203125" Y="4.121897460938" />
                  <Point X="22.952107421875" Y="4.110405273438" />
                  <Point X="22.96101953125" Y="4.105129394531" />
                  <Point X="23.015421875" Y="4.07680859375" />
                  <Point X="23.056236328125" Y="4.0555625" />
                  <Point X="23.06567578125" Y="4.051286132812" />
                  <Point X="23.084958984375" Y="4.043788085938" />
                  <Point X="23.094802734375" Y="4.040566162109" />
                  <Point X="23.1157109375" Y="4.034964599609" />
                  <Point X="23.12584375" Y="4.032833984375" />
                  <Point X="23.14628515625" Y="4.029687988281" />
                  <Point X="23.166951171875" Y="4.028786132812" />
                  <Point X="23.18758984375" Y="4.030139160156" />
                  <Point X="23.19787109375" Y="4.03137890625" />
                  <Point X="23.219185546875" Y="4.035137207031" />
                  <Point X="23.2292734375" Y="4.037489257812" />
                  <Point X="23.2491328125" Y="4.043277832031" />
                  <Point X="23.258904296875" Y="4.046714111328" />
                  <Point X="23.315568359375" Y="4.070185791016" />
                  <Point X="23.358078125" Y="4.087793701172" />
                  <Point X="23.367416015625" Y="4.092272460938" />
                  <Point X="23.385548828125" Y="4.102220214844" />
                  <Point X="23.39434375" Y="4.107689453125" />
                  <Point X="23.41207421875" Y="4.120104003906" />
                  <Point X="23.420224609375" Y="4.126499511719" />
                  <Point X="23.435779296875" Y="4.140140625" />
                  <Point X="23.449755859375" Y="4.15539453125" />
                  <Point X="23.46198828125" Y="4.172079101562" />
                  <Point X="23.4676484375" Y="4.180755371094" />
                  <Point X="23.47846875" Y="4.199499511719" />
                  <Point X="23.4831484375" Y="4.208733398438" />
                  <Point X="23.4914765625" Y="4.227660644531" />
                  <Point X="23.495125" Y="4.237354003906" />
                  <Point X="23.513568359375" Y="4.295848632813" />
                  <Point X="23.527404296875" Y="4.33973046875" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370050292969" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401864257812" />
                  <Point X="23.53769921875" Y="4.412217285156" />
                  <Point X="23.537248046875" Y="4.432898925781" />
                  <Point X="23.536458984375" Y="4.443227539062" />
                  <Point X="23.520736328125" Y="4.562655761719" />
                  <Point X="23.75841015625" Y="4.629291503906" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.501880859375" Y="4.767003417969" />
                  <Point X="24.63477734375" Y="4.782557128906" />
                  <Point X="24.663919921875" Y="4.673796386719" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.3208125" Y="4.570857910156" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.556828125" Y="4.766298339844" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.18616796875" Y="4.65141015625" />
                  <Point X="26.453591796875" Y="4.586844726562" />
                  <Point X="26.685201171875" Y="4.502838867188" />
                  <Point X="26.858265625" Y="4.440067382812" />
                  <Point X="27.083767578125" Y="4.334607910156" />
                  <Point X="27.25044921875" Y="4.25665625" />
                  <Point X="27.468357421875" Y="4.129702636719" />
                  <Point X="27.629435546875" Y="4.035858398438" />
                  <Point X="27.817783203125" Y="3.901915527344" />
                  <Point X="27.5576171875" Y="3.451295166016" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.06237890625" Y="2.593110595703" />
                  <Point X="27.0531875" Y="2.573448974609" />
                  <Point X="27.044185546875" Y="2.549571777344" />
                  <Point X="27.041302734375" Y="2.540601318359" />
                  <Point X="27.02903515625" Y="2.494728515625" />
                  <Point X="27.01983203125" Y="2.460314941406" />
                  <Point X="27.0179140625" Y="2.451469726562" />
                  <Point X="27.013646484375" Y="2.420223144531" />
                  <Point X="27.012755859375" Y="2.383240478516" />
                  <Point X="27.013412109375" Y="2.369580322266" />
                  <Point X="27.0181953125" Y="2.329913330078" />
                  <Point X="27.021783203125" Y="2.300155517578" />
                  <Point X="27.02380078125" Y="2.289034423828" />
                  <Point X="27.02914453125" Y="2.267107910156" />
                  <Point X="27.032470703125" Y="2.256302490234" />
                  <Point X="27.04073828125" Y="2.234212402344" />
                  <Point X="27.0453203125" Y="2.223884521484" />
                  <Point X="27.055681640625" Y="2.203842285156" />
                  <Point X="27.0614609375" Y="2.194127929688" />
                  <Point X="27.086005859375" Y="2.157955566406" />
                  <Point X="27.104419921875" Y="2.130819335937" />
                  <Point X="27.10980859375" Y="2.123633789062" />
                  <Point X="27.130462890625" Y="2.100124267578" />
                  <Point X="27.15759765625" Y="2.075387207031" />
                  <Point X="27.1682578125" Y="2.066981689453" />
                  <Point X="27.2044296875" Y="2.042437011719" />
                  <Point X="27.23156640625" Y="2.024024291992" />
                  <Point X="27.241283203125" Y="2.018244384766" />
                  <Point X="27.261330078125" Y="2.007881103516" />
                  <Point X="27.27166015625" Y="2.003297485352" />
                  <Point X="27.293748046875" Y="1.995031860352" />
                  <Point X="27.304548828125" Y="1.991707397461" />
                  <Point X="27.326470703125" Y="1.986365112305" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.3772578125" Y="1.979563598633" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.416044921875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975497436523" />
                  <Point X="27.4843125" Y="1.979822875977" />
                  <Point X="27.49774609375" Y="1.982395996094" />
                  <Point X="27.543619140625" Y="1.994662963867" />
                  <Point X="27.578033203125" Y="2.003865600586" />
                  <Point X="27.5839921875" Y="2.005670288086" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572753906" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.275720703125" Y="2.3971953125" />
                  <Point X="28.94040625" Y="2.780951416016" />
                  <Point X="28.94840625" Y="2.769832763672" />
                  <Point X="29.04394921875" Y="2.637048339844" />
                  <Point X="29.136884765625" Y="2.483471679688" />
                  <Point X="28.821119140625" Y="2.241174560547" />
                  <Point X="28.172953125" Y="1.743819824219" />
                  <Point X="28.168142578125" Y="1.739871337891" />
                  <Point X="28.152126953125" Y="1.725223632813" />
                  <Point X="28.134669921875" Y="1.706604736328" />
                  <Point X="28.12857421875" Y="1.699420166016" />
                  <Point X="28.095560546875" Y="1.656349975586" />
                  <Point X="28.07079296875" Y="1.624038574219" />
                  <Point X="28.065634765625" Y="1.616601074219" />
                  <Point X="28.049736328125" Y="1.589366210938" />
                  <Point X="28.034763671875" Y="1.555544311523" />
                  <Point X="28.03014453125" Y="1.54267590332" />
                  <Point X="28.017845703125" Y="1.498701049805" />
                  <Point X="28.008619140625" Y="1.465711547852" />
                  <Point X="28.006228515625" Y="1.454669921875" />
                  <Point X="28.0027734375" Y="1.432370727539" />
                  <Point X="28.001708984375" Y="1.42111328125" />
                  <Point X="28.000892578125" Y="1.397541137695" />
                  <Point X="28.001173828125" Y="1.386236206055" />
                  <Point X="28.003078125" Y="1.36375012207" />
                  <Point X="28.004701171875" Y="1.352568969727" />
                  <Point X="28.014796875" Y="1.303641235352" />
                  <Point X="28.02237109375" Y="1.266936157227" />
                  <Point X="28.024599609375" Y="1.258233764648" />
                  <Point X="28.034685546875" Y="1.228606689453" />
                  <Point X="28.050287109375" Y="1.195371826172" />
                  <Point X="28.056919921875" Y="1.183526000977" />
                  <Point X="28.08437890625" Y="1.141790161133" />
                  <Point X="28.1049765625" Y="1.11048046875" />
                  <Point X="28.111740234375" Y="1.101423339844" />
                  <Point X="28.126294921875" Y="1.084177978516" />
                  <Point X="28.1340859375" Y="1.075989746094" />
                  <Point X="28.15133203125" Y="1.0598984375" />
                  <Point X="28.16003515625" Y="1.052695800781" />
                  <Point X="28.1782421875" Y="1.039371337891" />
                  <Point X="28.18774609375" Y="1.033249633789" />
                  <Point X="28.227537109375" Y="1.010850708008" />
                  <Point X="28.257388671875" Y="0.994047241211" />
                  <Point X="28.265484375" Y="0.98998626709" />
                  <Point X="28.2946796875" Y="0.978084411621" />
                  <Point X="28.330275390625" Y="0.96802142334" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.39747265625" Y="0.958147094727" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="29.108263671875" Y="1.032797119141" />
                  <Point X="29.704703125" Y="1.111319824219" />
                  <Point X="29.711646484375" Y="1.082799438477" />
                  <Point X="29.7526875" Y="0.914207275391" />
                  <Point X="29.78387109375" Y="0.713921203613" />
                  <Point X="29.4348046875" Y="0.62038848877" />
                  <Point X="28.6919921875" Y="0.42135269165" />
                  <Point X="28.68603125" Y="0.419543792725" />
                  <Point X="28.665623046875" Y="0.412136291504" />
                  <Point X="28.642380859375" Y="0.401618835449" />
                  <Point X="28.634005859375" Y="0.397316650391" />
                  <Point X="28.5811484375" Y="0.366764373779" />
                  <Point X="28.54149609375" Y="0.343844207764" />
                  <Point X="28.5338828125" Y="0.338945587158" />
                  <Point X="28.50877734375" Y="0.319871917725" />
                  <Point X="28.48199609375" Y="0.294352478027" />
                  <Point X="28.472796875" Y="0.284227386475" />
                  <Point X="28.44108203125" Y="0.243815933228" />
                  <Point X="28.417291015625" Y="0.213499832153" />
                  <Point X="28.4108515625" Y="0.20420451355" />
                  <Point X="28.399126953125" Y="0.184921585083" />
                  <Point X="28.393841796875" Y="0.174933837891" />
                  <Point X="28.384068359375" Y="0.153469421387" />
                  <Point X="28.380005859375" Y="0.142927444458" />
                  <Point X="28.37316015625" Y="0.121430030823" />
                  <Point X="28.370376953125" Y="0.110474441528" />
                  <Point X="28.3598046875" Y="0.055274559021" />
                  <Point X="28.351875" Y="0.013864208221" />
                  <Point X="28.350603515625" Y="0.004966006279" />
                  <Point X="28.3485859375" Y="-0.026262935638" />
                  <Point X="28.35028125" Y="-0.062939350128" />
                  <Point X="28.351875" Y="-0.076419891357" />
                  <Point X="28.3624453125" Y="-0.131619918823" />
                  <Point X="28.370376953125" Y="-0.173030273438" />
                  <Point X="28.37316015625" Y="-0.183988250732" />
                  <Point X="28.3800078125" Y="-0.205492492676" />
                  <Point X="28.384072265625" Y="-0.216038772583" />
                  <Point X="28.393845703125" Y="-0.237500808716" />
                  <Point X="28.399130859375" Y="-0.247485748291" />
                  <Point X="28.410853515625" Y="-0.266765258789" />
                  <Point X="28.417291015625" Y="-0.276059997559" />
                  <Point X="28.449005859375" Y="-0.316471435547" />
                  <Point X="28.472796875" Y="-0.346787689209" />
                  <Point X="28.47872265625" Y="-0.353637878418" />
                  <Point X="28.501140625" Y="-0.375804077148" />
                  <Point X="28.530177734375" Y="-0.398724273682" />
                  <Point X="28.54149609375" Y="-0.40640435791" />
                  <Point X="28.594353515625" Y="-0.436956756592" />
                  <Point X="28.634005859375" Y="-0.459876800537" />
                  <Point X="28.639498046875" Y="-0.462815368652" />
                  <Point X="28.659154296875" Y="-0.472014831543" />
                  <Point X="28.683025390625" Y="-0.481027008057" />
                  <Point X="28.6919921875" Y="-0.483912841797" />
                  <Point X="29.249244140625" Y="-0.633227722168" />
                  <Point X="29.78487890625" Y="-0.776750854492" />
                  <Point X="29.784529296875" Y="-0.779073303223" />
                  <Point X="29.761619140625" Y="-0.931036682129" />
                  <Point X="29.727802734375" Y="-1.079219604492" />
                  <Point X="29.298236328125" Y="-1.02266595459" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042297363" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.2507421875" Y="-0.93522467041" />
                  <Point X="28.17291796875" Y="-0.952140075684" />
                  <Point X="28.157875" Y="-0.95674230957" />
                  <Point X="28.128755859375" Y="-0.968366088867" />
                  <Point X="28.1146796875" Y="-0.975387634277" />
                  <Point X="28.0868515625" Y="-0.992280456543" />
                  <Point X="28.074125" Y="-1.001530395508" />
                  <Point X="28.050375" Y="-1.022001586914" />
                  <Point X="28.0393515625" Y="-1.033222290039" />
                  <Point X="27.976646484375" Y="-1.108635864258" />
                  <Point X="27.929607421875" Y="-1.165210205078" />
                  <Point X="27.921326171875" Y="-1.176848510742" />
                  <Point X="27.906603515625" Y="-1.201234985352" />
                  <Point X="27.900162109375" Y="-1.213983276367" />
                  <Point X="27.8888203125" Y="-1.241368164062" />
                  <Point X="27.88436328125" Y="-1.254934692383" />
                  <Point X="27.877533203125" Y="-1.282581420898" />
                  <Point X="27.87516015625" Y="-1.296661499023" />
                  <Point X="27.866173828125" Y="-1.394325317383" />
                  <Point X="27.859431640625" Y="-1.467591918945" />
                  <Point X="27.859291015625" Y="-1.483315795898" />
                  <Point X="27.861607421875" Y="-1.514580200195" />
                  <Point X="27.864064453125" Y="-1.530120849609" />
                  <Point X="27.871794921875" Y="-1.561742553711" />
                  <Point X="27.87678515625" Y="-1.576666625977" />
                  <Point X="27.889158203125" Y="-1.605480712891" />
                  <Point X="27.896541015625" Y="-1.61937097168" />
                  <Point X="27.953951171875" Y="-1.708670166016" />
                  <Point X="27.997021484375" Y="-1.775661499023" />
                  <Point X="28.0017421875" Y="-1.782352539063" />
                  <Point X="28.019802734375" Y="-1.804458496094" />
                  <Point X="28.04349609375" Y="-1.828124023438" />
                  <Point X="28.052798828125" Y="-1.836277954102" />
                  <Point X="28.5699296875" Y="-2.233086914062" />
                  <Point X="29.087171875" Y="-2.629981201172" />
                  <Point X="29.045505859375" Y="-2.697402832031" />
                  <Point X="29.0012734375" Y="-2.760250976562" />
                  <Point X="28.6156953125" Y="-2.537636962891" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.647640625" Y="-2.044039672852" />
                  <Point X="27.555017578125" Y="-2.027311889648" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.03513659668" />
                  <Point X="27.415068359375" Y="-2.044960571289" />
                  <Point X="27.40058984375" Y="-2.051109130859" />
                  <Point X="27.29801953125" Y="-2.105091308594" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.208970703125" Y="-2.153169433594" />
                  <Point X="27.1860390625" Y="-2.1700625" />
                  <Point X="27.175208984375" Y="-2.179374267578" />
                  <Point X="27.15425" Y="-2.200333496094" />
                  <Point X="27.144939453125" Y="-2.211161621094" />
                  <Point X="27.128046875" Y="-2.234091796875" />
                  <Point X="27.12046484375" Y="-2.246193847656" />
                  <Point X="27.066482421875" Y="-2.348764404297" />
                  <Point X="27.025984375" Y="-2.425711914062" />
                  <Point X="27.0198359375" Y="-2.440193115234" />
                  <Point X="27.01001171875" Y="-2.469971679688" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517442871094" />
                  <Point X="27.000279296875" Y="-2.533134033203" />
                  <Point X="27.00068359375" Y="-2.564484375" />
                  <Point X="27.0021875" Y="-2.580143554688" />
                  <Point X="27.024486328125" Y="-2.703610351562" />
                  <Point X="27.04121484375" Y="-2.796233886719" />
                  <Point X="27.04301953125" Y="-2.804229248047" />
                  <Point X="27.05123828125" Y="-2.831542236328" />
                  <Point X="27.064072265625" Y="-2.862479980469" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.401859375" Y="-3.449155273438" />
                  <Point X="27.735896484375" Y="-4.027721923828" />
                  <Point X="27.72375390625" Y="-4.036081787109" />
                  <Point X="27.419392578125" Y="-3.639428222656" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.651529296875" Y="-2.742359130859" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.275216796875" Y="-2.658771484375" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.941025390625" Y="-2.807918457031" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883088378906" />
                  <Point X="25.83218359375" Y="-2.906838378906" />
                  <Point X="25.822935546875" Y="-2.919563720703" />
                  <Point X="25.80604296875" Y="-2.947390625" />
                  <Point X="25.799021484375" Y="-2.961466552734" />
                  <Point X="25.787396484375" Y="-2.990586669922" />
                  <Point X="25.78279296875" Y="-3.005630859375" />
                  <Point X="25.752044921875" Y="-3.147094238281" />
                  <Point X="25.728978515625" Y="-3.25321875" />
                  <Point X="25.7275859375" Y="-3.261287353516" />
                  <Point X="25.724724609375" Y="-3.289677490234" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520019531" />
                  <Point X="25.819728515625" Y="-4.050841796875" />
                  <Point X="25.833087890625" Y="-4.152312011719" />
                  <Point X="25.655068359375" Y="-3.487937255859" />
                  <Point X="25.652607421875" Y="-3.480122314453" />
                  <Point X="25.642146484375" Y="-3.453578613281" />
                  <Point X="25.6267890625" Y="-3.423815429688" />
                  <Point X="25.62041015625" Y="-3.413209716797" />
                  <Point X="25.526861328125" Y="-3.278424072266" />
                  <Point X="25.456681640625" Y="-3.177309326172" />
                  <Point X="25.446671875" Y="-3.165172119141" />
                  <Point X="25.424787109375" Y="-3.142716308594" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104936767578" />
                  <Point X="25.3452421875" Y="-3.090829345703" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.185896484375" Y="-3.040010498047" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.790255859375" Y="-3.051233398438" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.6670703125" Y="-3.090829589844" />
                  <Point X="24.6390703125" Y="-3.104937255859" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.587525390625" Y="-3.142717285156" />
                  <Point X="24.565640625" Y="-3.165173828125" />
                  <Point X="24.555630859375" Y="-3.177310791016" />
                  <Point X="24.462083984375" Y="-3.312096191406" />
                  <Point X="24.391904296875" Y="-3.413211181641" />
                  <Point X="24.38753125" Y="-3.420132080078" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.18554296875" Y="-4.128739746094" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.918383215505" Y="-2.712394203017" />
                  <Point X="29.019573055101" Y="-2.578110750381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.835492993511" Y="-2.664537429471" />
                  <Point X="28.944201645503" Y="-2.520276175779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.703196160289" Y="-4.009290265129" />
                  <Point X="27.715685736191" Y="-3.992716038103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.752602771516" Y="-2.616680655925" />
                  <Point X="28.868830235906" Y="-2.462441601177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.643181266003" Y="-3.931076906404" />
                  <Point X="27.664083487237" Y="-3.903338721955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.669712549522" Y="-2.568823882379" />
                  <Point X="28.793458826309" Y="-2.404607026574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.583166371718" Y="-3.852863547679" />
                  <Point X="27.612481238283" Y="-3.813961405807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.586822311675" Y="-2.52096712987" />
                  <Point X="28.718087416712" Y="-2.346772451972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.678179953321" Y="-1.072686622988" />
                  <Point X="29.751497037948" Y="-0.975391565498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.523151477432" Y="-3.774650188954" />
                  <Point X="27.560878989329" Y="-3.724584089658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.50393204417" Y="-2.473110416719" />
                  <Point X="28.642716007115" Y="-2.288937877369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.569963004464" Y="-1.058439551173" />
                  <Point X="29.782675551947" Y="-0.776160466542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.463136583147" Y="-3.696436830229" />
                  <Point X="27.509276740375" Y="-3.63520677351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.421041776665" Y="-2.425253703567" />
                  <Point X="28.567344597751" Y="-2.231103302456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.461746055608" Y="-1.044192479358" />
                  <Point X="29.683706045088" Y="-0.749641624711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.403121645031" Y="-3.618223529669" />
                  <Point X="27.457674491422" Y="-3.545829457362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.338151509161" Y="-2.377396990415" />
                  <Point X="28.491973194975" Y="-2.173268718802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.353529106751" Y="-1.029945407543" />
                  <Point X="29.58473653823" Y="-0.72312278288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.343106589077" Y="-3.540010385485" />
                  <Point X="27.406072242468" Y="-3.456452141213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.255261241656" Y="-2.329540277263" />
                  <Point X="28.416601792198" Y="-2.115434135149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.245312140118" Y="-1.015698359319" />
                  <Point X="29.485767031371" Y="-0.696603941048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.283091533123" Y="-3.461797241301" />
                  <Point X="27.354470174072" Y="-3.367074585455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.172370974151" Y="-2.281683564112" />
                  <Point X="28.341230389422" Y="-2.057599551495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.137095154911" Y="-1.001451335742" />
                  <Point X="29.386797524512" Y="-0.670085099217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.22307647717" Y="-3.383584097117" />
                  <Point X="27.302868121729" Y="-3.277697008396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.089480706646" Y="-2.23382685096" />
                  <Point X="28.265858986645" Y="-1.999764967841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.028878169705" Y="-0.987204312164" />
                  <Point X="29.287828017653" Y="-0.643566257386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.163061421216" Y="-3.305370952933" />
                  <Point X="27.251266069385" Y="-3.188319431338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.006590439142" Y="-2.185970137808" />
                  <Point X="28.190487583869" Y="-1.941930384187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.920661184499" Y="-0.972957288587" />
                  <Point X="29.188858472127" Y="-0.617047466868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.103046365262" Y="-3.227157808749" />
                  <Point X="27.199664017041" Y="-3.098941854279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.923700171637" Y="-2.138113424657" />
                  <Point X="28.115116181093" Y="-1.884095800533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.812444199293" Y="-0.95871026501" />
                  <Point X="29.089888901893" Y="-0.590528709138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.043031309309" Y="-3.148944664565" />
                  <Point X="27.148061964697" Y="-3.00956427722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.840547575999" Y="-2.090604832696" />
                  <Point X="28.040554841723" Y="-1.82518622643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.704227214086" Y="-0.944463241433" />
                  <Point X="28.99091933166" Y="-0.564009951407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.983016253355" Y="-3.070731520381" />
                  <Point X="27.096459912354" Y="-2.920186700161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.743621863889" Y="-2.061373783626" />
                  <Point X="27.979480658169" Y="-1.748378592043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.59601022888" Y="-0.930216217855" />
                  <Point X="28.891949761427" Y="-0.537491193677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.923001197401" Y="-2.992518376197" />
                  <Point X="27.049259866555" Y="-2.824967463111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.638918218592" Y="-2.042464400516" />
                  <Point X="27.924716661014" Y="-1.663197057472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.487793243674" Y="-0.915969194278" />
                  <Point X="28.792980191193" Y="-0.510972435946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.698833295356" Y="0.691135235082" />
                  <Point X="29.772249832737" Y="0.788562270835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.862986141448" Y="-2.914305232013" />
                  <Point X="27.023958483535" Y="-2.700687719021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.532583058607" Y="-2.025720110524" />
                  <Point X="27.8749976013" Y="-1.571320664795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.373108310969" Y="-0.910305426935" />
                  <Point X="28.69401062096" Y="-0.484453678216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.549785288775" Y="0.651197663182" />
                  <Point X="29.751537401332" Y="0.918931759402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.827327660422" Y="-4.130814642818" />
                  <Point X="25.829821818269" Y="-4.127504783563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.799021701576" Y="-2.841333097305" />
                  <Point X="27.001470206895" Y="-2.572674856677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.390494412102" Y="-2.056422299673" />
                  <Point X="27.862940119408" Y="-1.429465670295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.232367946675" Y="-0.939218385158" />
                  <Point X="28.605844199135" Y="-0.443598658333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.400737351581" Y="0.611260183362" />
                  <Point X="29.722492769267" Y="1.038244044231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.796125172415" Y="-4.014365929543" />
                  <Point X="25.81213016907" Y="-3.993126581612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.720996542309" Y="-2.78702016746" />
                  <Point X="27.101767728311" Y="-2.281719736855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.185665375996" Y="-2.170383797947" />
                  <Point X="27.892592830073" Y="-1.232259380753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.052253698008" Y="-1.020382252745" />
                  <Point X="28.524239008454" Y="-0.394036590637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.251689648578" Y="0.571323014324" />
                  <Point X="29.653529653684" Y="1.10458271222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.764922684408" Y="-3.897917216268" />
                  <Point X="25.794438891473" Y="-3.858747886528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.640864706822" Y="-2.735502891383" />
                  <Point X="28.456520166509" Y="-0.32604671576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.102641945575" Y="0.531385845286" />
                  <Point X="29.521476099642" Y="1.087197540558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.733720196401" Y="-3.781468502993" />
                  <Point X="25.776747613876" Y="-3.724369191445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.560732510712" Y="-2.68398609387" />
                  <Point X="28.398155615814" Y="-0.245643277119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.953594242572" Y="0.491448676248" />
                  <Point X="29.389422545599" Y="1.069812368896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.702517708394" Y="-3.665019789718" />
                  <Point X="25.759056336278" Y="-3.589990496362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.466990376562" Y="-2.650530294154" />
                  <Point X="28.362988492767" Y="-0.134455812243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.804546539569" Y="0.451511507211" />
                  <Point X="29.257368991557" Y="1.052427197234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.671315220387" Y="-3.548571076442" />
                  <Point X="25.741365058681" Y="-3.455611801279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.346790412868" Y="-2.652185220127" />
                  <Point X="28.350955921321" Y="0.007432239536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.650902749926" Y="0.405475125197" />
                  <Point X="29.125315437514" Y="1.035042025572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.63474845751" Y="-3.439240996355" />
                  <Point X="25.724740428081" Y="-3.319817617821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.218974326337" Y="-2.663947082472" />
                  <Point X="28.993261852264" Y="1.017656812496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.579657600406" Y="-3.354493219586" />
                  <Point X="25.758571737183" Y="-3.117066140861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.067801110069" Y="-2.70670490288" />
                  <Point X="28.861208262386" Y="1.000271593279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.522625666077" Y="-3.272321339298" />
                  <Point X="28.729154672508" Y="0.982886374061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.465593591097" Y="-3.190149645661" />
                  <Point X="28.59710108263" Y="0.965501154844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.398255095502" Y="-3.121655034129" />
                  <Point X="28.467429681223" Y="0.951277206502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.311459447653" Y="-3.078980935739" />
                  <Point X="28.357622423364" Y="0.96341386699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.932982547811" Y="-4.750425753991" />
                  <Point X="24.066403783116" Y="-4.573369794586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.215053683317" Y="-3.049059892669" />
                  <Point X="28.260556140658" Y="0.992458372578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.865177763538" Y="-4.682549928435" />
                  <Point X="24.132040921873" Y="-4.328410556085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.11864763141" Y="-3.019139231218" />
                  <Point X="28.177412344773" Y="1.039978642205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.878016308928" Y="-4.507656789851" />
                  <Point X="24.197677960659" Y="-4.083451450252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.015800175524" Y="-2.997766601563" />
                  <Point X="28.108266553902" Y="1.1060748919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.853674253702" Y="-4.38210397478" />
                  <Point X="24.263314558683" Y="-3.838492929329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.876797088723" Y="-3.024374114683" />
                  <Point X="28.052979841537" Y="1.190562759959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.876963011247" Y="2.284025358424" />
                  <Point X="29.0880581313" Y="2.56415804436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.828831533022" Y="-4.257215565206" />
                  <Point X="24.328951156706" Y="-3.593534408406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.721531441719" Y="-3.072562774109" />
                  <Point X="28.015751528333" Y="1.29901493311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.594935346121" Y="2.067617819272" />
                  <Point X="29.034220604396" Y="2.65056904648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.797144935349" Y="-4.141409287157" />
                  <Point X="28.004192888449" Y="1.441531913314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.312908280178" Y="1.851211075261" />
                  <Point X="28.976117713003" Y="2.731319718742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.739393610739" Y="-4.060192070016" />
                  <Point X="28.859262042398" Y="2.734102819595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.667800277453" Y="-3.997343818809" />
                  <Point X="28.648701987646" Y="2.612536002703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.59617865139" Y="-3.934533113385" />
                  <Point X="28.438141932894" Y="2.490969185811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.524557286847" Y="-3.871722060913" />
                  <Point X="28.227581838318" Y="2.36940231607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.445171197118" Y="-3.81921514679" />
                  <Point X="28.017021609371" Y="2.247835268014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.344506873727" Y="-3.794945402461" />
                  <Point X="27.806461380425" Y="2.126268219958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.231152659609" Y="-3.787515711909" />
                  <Point X="27.601378967292" Y="2.011970479012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.117798349815" Y="-3.780086148322" />
                  <Point X="27.455658955819" Y="1.976449305786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.99831002914" Y="-3.780796692112" />
                  <Point X="27.342235308202" Y="1.983786854974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.8006497736" Y="-3.88524489726" />
                  <Point X="27.247015144702" Y="2.015281243494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.480425951362" Y="-4.152340448913" />
                  <Point X="27.167482192573" Y="2.067593264629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.399316813145" Y="-4.102120097363" />
                  <Point X="27.100512602678" Y="2.13657743056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.318207744643" Y="-4.051899653299" />
                  <Point X="27.046146319893" Y="2.222286749926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.23819943322" Y="-4.000218455253" />
                  <Point X="27.016829723705" Y="2.341238126173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.162922861895" Y="-3.942258026012" />
                  <Point X="27.037559183907" Y="2.526602862396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.08764629057" Y="-3.884297596771" />
                  <Point X="27.368247136619" Y="3.123296411021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.054186232659" Y="-3.770844779948" />
                  <Point X="27.758009062542" Y="3.79838376989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.443947874781" Y="-3.095757797696" />
                  <Point X="27.752211838288" Y="3.948546406871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.688556178662" Y="-2.613295801299" />
                  <Point X="27.674762889877" Y="4.003623994348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.660406168289" Y="-2.492796313387" />
                  <Point X="27.595151701701" Y="4.055832192742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.600277258673" Y="-2.414734258115" />
                  <Point X="27.512489340263" Y="4.103991347461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.518926177918" Y="-2.364834975158" />
                  <Point X="27.429826990697" Y="4.152150517933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.411246314002" Y="-2.349875167554" />
                  <Point X="27.347164654729" Y="4.200309706449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.237353348719" Y="-2.422783113242" />
                  <Point X="27.26450231876" Y="4.248468894965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.02679358703" Y="-2.544349541227" />
                  <Point X="27.177446247303" Y="4.290797399555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.81623382534" Y="-2.665915969212" />
                  <Point X="27.089490215319" Y="4.331931616186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.605673647535" Y="-2.787482949401" />
                  <Point X="27.00153406912" Y="4.37306568125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.395113320437" Y="-2.909050127708" />
                  <Point X="26.913577914972" Y="4.414199735765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.200564680542" Y="-3.009369079428" />
                  <Point X="26.823594072323" Y="4.452642956755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.140789656828" Y="-2.930837401704" />
                  <Point X="26.730174481573" Y="4.486526786018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.081014748136" Y="-2.852305571339" />
                  <Point X="26.636754864963" Y="4.520410580965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.021239848201" Y="-2.773773729353" />
                  <Point X="21.889157078241" Y="-1.622008663634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.045729044284" Y="-1.414230646887" />
                  <Point X="26.543335224349" Y="4.554294344057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.965636525677" Y="-2.689706017167" />
                  <Point X="21.607129167045" Y="-1.838416529333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.036605844727" Y="-1.268481728209" />
                  <Point X="26.449631350118" Y="4.5878009164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.913086074883" Y="-2.601587007361" />
                  <Point X="21.325101255848" Y="-2.054824395033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.980845888549" Y="-1.184621875901" />
                  <Point X="26.348988787087" Y="4.612099537702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.860535624089" Y="-2.513467997555" />
                  <Point X="21.04307354185" Y="-2.27123199904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.903843054089" Y="-1.128952275215" />
                  <Point X="26.248346224057" Y="4.636398159005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.806011445619" Y="-1.10092339122" />
                  <Point X="26.147703557669" Y="4.660696643147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.680109987347" Y="-1.110144456047" />
                  <Point X="26.047060724202" Y="4.684994905568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.548056409181" Y="-1.127529659721" />
                  <Point X="25.946417890734" Y="4.709293167988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.416002831016" Y="-1.144914863396" />
                  <Point X="25.845775057267" Y="4.733591430408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.283949252851" Y="-1.16230006707" />
                  <Point X="25.73723165858" Y="4.747405288667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.151895674685" Y="-1.179685270744" />
                  <Point X="25.163089932545" Y="4.143349297662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.219809855661" Y="4.218619177916" />
                  <Point X="25.626979554432" Y="4.758951618191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.019842096784" Y="-1.197070474068" />
                  <Point X="25.000253058991" Y="4.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.295719512621" Y="4.477210508503" />
                  <Point X="25.516727480656" Y="4.77049798802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.887788519532" Y="-1.214455676529" />
                  <Point X="21.642605671531" Y="-0.212779483699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.650744051939" Y="-0.201979488123" />
                  <Point X="24.900387185769" Y="4.110442604741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.361356191705" Y="4.722169136996" />
                  <Point X="25.406475460012" Y="4.782044428358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.755734942281" Y="-1.231840878991" />
                  <Point X="21.437887563604" Y="-0.326593775309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.699559447603" Y="0.020656543316" />
                  <Point X="24.825468816963" Y="4.16887838478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.623681365029" Y="-1.249226081452" />
                  <Point X="21.288839828944" Y="-0.366530986357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.65984508961" Y="0.125809623603" />
                  <Point X="24.77502453937" Y="4.259792380826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.491627787778" Y="-1.266611283914" />
                  <Point X="21.139792094285" Y="-0.406468197404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.593242563926" Y="0.195280900195" />
                  <Point X="24.743694102734" Y="4.376071300536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.359574210526" Y="-1.283996486375" />
                  <Point X="20.990744359625" Y="-0.446405408452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.5101652424" Y="0.242889384277" />
                  <Point X="24.712492017903" Y="4.492520548844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.313640555151" Y="-1.187096692473" />
                  <Point X="20.841696624965" Y="-0.4863426195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.412310232447" Y="0.270887213455" />
                  <Point X="24.681289933072" Y="4.608969797151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.283527846268" Y="-1.069201793453" />
                  <Point X="20.692648890306" Y="-0.526279830547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.313340692708" Y="0.297406011653" />
                  <Point X="24.650087639531" Y="4.725418768492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.255569365557" Y="-0.948448137095" />
                  <Point X="20.543601083894" Y="-0.566217136813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.214371152968" Y="0.32392480985" />
                  <Point X="24.56833152797" Y="4.774780557415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.236592891127" Y="-0.815774955814" />
                  <Point X="20.39455324265" Y="-0.606154489303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.115401613229" Y="0.350443608047" />
                  <Point X="24.437873095367" Y="4.7595121834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.217616416697" Y="-0.683101774532" />
                  <Point X="20.245505401406" Y="-0.646091841793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.016432073489" Y="0.376962406244" />
                  <Point X="24.307414659674" Y="4.744243805284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.91746253375" Y="0.403481204441" />
                  <Point X="24.176956223981" Y="4.728975427167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.818492995658" Y="0.430000004825" />
                  <Point X="21.446645300749" Y="1.263586268484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.547126790842" Y="1.396929709581" />
                  <Point X="24.043014205227" Y="4.709084178188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.719523460298" Y="0.456518808834" />
                  <Point X="21.284734259883" Y="1.206578873547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.569830241876" Y="1.584914020116" />
                  <Point X="23.892198616712" Y="4.666800945837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.620553924938" Y="0.483037612843" />
                  <Point X="21.170182252371" Y="1.212419038579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.525691134812" Y="1.684195260062" />
                  <Point X="23.327337424197" Y="4.075060638782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.527725597952" Y="4.340984727078" />
                  <Point X="23.741382986909" Y="4.624517658694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.521584389579" Y="0.509556416853" />
                  <Point X="21.061965272043" Y="1.22666606863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.465141309046" Y="1.761698740735" />
                  <Point X="23.173855267242" Y="4.029238750591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.528927574126" Y="4.500435616742" />
                  <Point X="23.59056703269" Y="4.582233941036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.422614854219" Y="0.536075220862" />
                  <Point X="20.953748291715" Y="1.24091309868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.390252968" Y="1.820174368958" />
                  <Point X="22.130312692553" Y="2.802266794115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.238806689956" Y="2.946243191546" />
                  <Point X="23.070192753096" Y="4.049529761404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.323645318859" Y="0.562594024871" />
                  <Point X="20.845531311387" Y="1.255160128731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.31488157129" Y="1.878008960662" />
                  <Point X="21.952064787211" Y="2.723579647774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.245235619456" Y="3.112630482554" />
                  <Point X="22.984087832658" Y="4.093120486027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.224675783499" Y="0.58911282888" />
                  <Point X="20.737314331059" Y="1.269407158782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.239510174579" Y="1.935843552365" />
                  <Point X="21.839415470671" Y="2.731944769006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.213088653591" Y="3.227825831379" />
                  <Point X="22.905320288086" Y="4.146448237298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.240648192181" Y="0.768164744516" />
                  <Point X="20.629097350731" Y="1.283654188833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.164138777868" Y="1.993678144068" />
                  <Point X="21.744148579224" Y="2.763377147447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.161534484765" Y="3.317266952012" />
                  <Point X="22.84315153902" Y="4.22180333419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.271072067458" Y="0.966394404064" />
                  <Point X="20.520880363832" Y="1.297901210165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.088767381949" Y="2.051512736822" />
                  <Point X="21.661258382266" Y="2.811233954217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.109932114471" Y="3.406644107137" />
                  <Point X="22.72449227614" Y="4.222192987254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.33786827846" Y="1.212891783384" />
                  <Point X="20.412663375888" Y="1.312148230108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.01339601336" Y="2.109347365845" />
                  <Point X="21.578368185308" Y="2.859090760987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.058329744178" Y="3.496021262263" />
                  <Point X="22.51987451993" Y="4.10851186687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.938024644772" Y="2.167181994867" />
                  <Point X="21.49547798835" Y="2.906947567758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.006727917285" Y="3.585399138505" />
                  <Point X="22.301551048448" Y="3.976642648009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.862653276183" Y="2.22501662389" />
                  <Point X="21.412587791392" Y="2.954804374528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.955126182967" Y="3.674777137598" />
                  <Point X="22.019842934696" Y="3.760659167852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.787281907594" Y="2.282851252912" />
                  <Point X="21.329697551744" Y="3.002661124647" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.9998359375" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.700419921875" Y="-4.391298828125" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.370771484375" Y="-3.386758544922" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.129576171875" Y="-3.221471435547" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.846576171875" Y="-3.232694824219" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.618173828125" Y="-3.420429199219" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.3690703125" Y="-4.177915527344" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.0267578125" Y="-4.962717285156" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.73226171875" Y="-4.894971191406" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.663298828125" Y="-4.760313476562" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.655734375" Y="-4.360895507812" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.480439453125" Y="-4.085747558594" />
                  <Point X="23.38045703125" Y="-3.998064208984" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.17387109375" Y="-3.974168945312" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.862728515625" Y="-4.072276367188" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.643869140625" Y="-4.282922363281" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.330328125" Y="-4.282876953125" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.91223828125" Y="-3.989034179688" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.06862109375" Y="-3.365838623047" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597594482422" />
                  <Point X="22.4860234375" Y="-2.568766113281" />
                  <Point X="22.46867578125" Y="-2.551417480469" />
                  <Point X="22.439845703125" Y="-2.537198730469" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.85563671875" Y="-2.862559814453" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.985373046875" Y="-3.040357666016" />
                  <Point X="20.838302734375" Y="-2.847136962891" />
                  <Point X="20.6720234375" Y="-2.568312988281" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.091998046875" Y="-1.994201538086" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396014526367" />
                  <Point X="21.8618828125" Y="-1.366267211914" />
                  <Point X="21.85967578125" Y="-1.334595825195" />
                  <Point X="21.838841796875" Y="-1.310638916016" />
                  <Point X="21.812359375" Y="-1.295052490234" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.082765625" Y="-1.38042590332" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.130130859375" Y="-1.236390991211" />
                  <Point X="20.072607421875" Y="-1.011187744141" />
                  <Point X="20.028615234375" Y="-0.703604248047" />
                  <Point X="20.001603515625" Y="-0.514742248535" />
                  <Point X="20.594763671875" Y="-0.355805633545" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.4739609375" Y="-0.110420043945" />
                  <Point X="21.497677734375" Y="-0.09064516449" />
                  <Point X="21.510388671875" Y="-0.058875602722" />
                  <Point X="21.516599609375" Y="-0.031280040741" />
                  <Point X="21.509068359375" Y="0.000569912016" />
                  <Point X="21.497677734375" Y="0.028085084915" />
                  <Point X="21.47" Y="0.05060931778" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.806544921875" Y="0.236498886108" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.045283203125" Y="0.745887878418" />
                  <Point X="20.08235546875" Y="0.996414855957" />
                  <Point X="20.17091015625" Y="1.323211791992" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.63903125" Y="1.473986083984" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.303392578125" Y="1.406932495117" />
                  <Point X="21.32972265625" Y="1.415234008789" />
                  <Point X="21.348466796875" Y="1.426056152344" />
                  <Point X="21.360880859375" Y="1.443786132813" />
                  <Point X="21.374962890625" Y="1.47778503418" />
                  <Point X="21.38552734375" Y="1.503290527344" />
                  <Point X="21.389287109375" Y="1.524605834961" />
                  <Point X="21.38368359375" Y="1.545512451172" />
                  <Point X="21.36669140625" Y="1.578154418945" />
                  <Point X="21.353943359375" Y="1.602642211914" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.975224609375" Y="1.899147949219" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.6959375" Y="2.540215332031" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="21.074564453125" Y="3.088523193359" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.467609375" Y="3.142430908203" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.9103671875" Y="2.916158447266" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775146484" />
                  <Point X="21.986748046875" Y="2.927403808594" />
                  <Point X="22.021443359375" Y="2.962099121094" />
                  <Point X="22.04747265625" Y="2.988127197266" />
                  <Point X="22.0591015625" Y="3.006381103516" />
                  <Point X="22.061927734375" Y="3.027839599609" />
                  <Point X="22.057650390625" Y="3.076719726562" />
                  <Point X="22.054443359375" Y="3.113389160156" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.886275390625" Y="3.414030761719" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="21.99623828125" Y="3.981979003906" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.616578125" Y="4.379591308594" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.917916015625" Y="4.436478027344" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.10315625" Y="4.24533984375" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.16487890625" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.242857421875" Y="4.245722167969" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744628906" />
                  <Point X="23.31391796875" Y="4.294488769531" />
                  <Point X="23.332361328125" Y="4.352983398438" />
                  <Point X="23.346197265625" Y="4.396865234375" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.329705078125" Y="4.558025878906" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.7071171875" Y="4.812236816406" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.479794921875" Y="4.955715332031" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.847447265625" Y="4.722972167969" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.13728515625" Y="4.620033691406" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.5766171875" Y="4.955265136719" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.2307578125" Y="4.836103515625" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.749986328125" Y="4.681453125" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="27.164255859375" Y="4.506716308594" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.564001953125" Y="4.293873046875" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.945013671875" Y="4.044581054688" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.72216015625" Y="3.356295166016" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491515380859" />
                  <Point X="27.2125859375" Y="2.445642578125" />
                  <Point X="27.2033828125" Y="2.411229003906" />
                  <Point X="27.202044921875" Y="2.392326416016" />
                  <Point X="27.206828125" Y="2.352659423828" />
                  <Point X="27.210416015625" Y="2.322901611328" />
                  <Point X="27.21868359375" Y="2.300811523438" />
                  <Point X="27.243228515625" Y="2.264639160156" />
                  <Point X="27.261642578125" Y="2.237502929688" />
                  <Point X="27.27494140625" Y="2.224203369141" />
                  <Point X="27.31111328125" Y="2.199658691406" />
                  <Point X="27.33825" Y="2.181245849609" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.40000390625" Y="2.168197021484" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.494537109375" Y="2.178213378906" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.180720703125" Y="2.561740234375" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.102630859375" Y="2.880804443359" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.321052734375" Y="2.546125" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.936783203125" Y="2.090437744141" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583833374023" />
                  <Point X="28.246357421875" Y="1.540763061523" />
                  <Point X="28.22158984375" Y="1.508452026367" />
                  <Point X="28.21312109375" Y="1.491500976562" />
                  <Point X="28.200822265625" Y="1.447526000977" />
                  <Point X="28.191595703125" Y="1.414536499023" />
                  <Point X="28.190779296875" Y="1.390964477539" />
                  <Point X="28.200875" Y="1.342036743164" />
                  <Point X="28.20844921875" Y="1.305331665039" />
                  <Point X="28.215646484375" Y="1.287956298828" />
                  <Point X="28.24310546875" Y="1.246220581055" />
                  <Point X="28.263703125" Y="1.214910888672" />
                  <Point X="28.28094921875" Y="1.198819702148" />
                  <Point X="28.320740234375" Y="1.176420776367" />
                  <Point X="28.350591796875" Y="1.15961730957" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.4223671875" Y="1.146509155273" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.083462890625" Y="1.221171630859" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.896255859375" Y="1.127740966797" />
                  <Point X="29.93919140625" Y="0.951367431641" />
                  <Point X="29.97651953125" Y="0.71162109375" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="29.48398046875" Y="0.436862640381" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819198608" />
                  <Point X="28.67623046875" Y="0.202266799927" />
                  <Point X="28.636578125" Y="0.179346832275" />
                  <Point X="28.622265625" Y="0.166926086426" />
                  <Point X="28.59055078125" Y="0.126514572144" />
                  <Point X="28.566759765625" Y="0.096198410034" />
                  <Point X="28.556986328125" Y="0.074734046936" />
                  <Point X="28.5464140625" Y="0.019534093857" />
                  <Point X="28.538484375" Y="-0.021876363754" />
                  <Point X="28.538484375" Y="-0.040685844421" />
                  <Point X="28.5490546875" Y="-0.095885948181" />
                  <Point X="28.556986328125" Y="-0.137296401978" />
                  <Point X="28.566759765625" Y="-0.158758483887" />
                  <Point X="28.598474609375" Y="-0.199169845581" />
                  <Point X="28.622265625" Y="-0.229486160278" />
                  <Point X="28.636578125" Y="-0.241906906128" />
                  <Point X="28.689435546875" Y="-0.27245916748" />
                  <Point X="28.729087890625" Y="-0.295379272461" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.298419921875" Y="-0.449701843262" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.972404296875" Y="-0.807399108887" />
                  <Point X="29.948431640625" Y="-0.966413085938" />
                  <Point X="29.900607421875" Y="-1.175979370117" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.273435546875" Y="-1.211040527344" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.29109765625" Y="-1.120889648438" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.1854453125" Y="-1.154697631836" />
                  <Point X="28.122740234375" Y="-1.230111206055" />
                  <Point X="28.075701171875" Y="-1.286685668945" />
                  <Point X="28.064359375" Y="-1.31407043457" />
                  <Point X="28.055373046875" Y="-1.41173425293" />
                  <Point X="28.048630859375" Y="-1.485000732422" />
                  <Point X="28.056361328125" Y="-1.516622436523" />
                  <Point X="28.113771484375" Y="-1.605921630859" />
                  <Point X="28.156841796875" Y="-1.672912963867" />
                  <Point X="28.168462890625" Y="-1.685540771484" />
                  <Point X="28.68559375" Y="-2.082350097656" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.27170703125" Y="-2.692795898438" />
                  <Point X="29.204134765625" Y="-2.802136962891" />
                  <Point X="29.1052265625" Y="-2.942674072266" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.5206953125" Y="-2.702181884766" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.613875" Y="-2.231014892578" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.3865078125" Y="-2.273227050781" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334683105469" />
                  <Point X="27.234619140625" Y="-2.437253662109" />
                  <Point X="27.19412109375" Y="-2.514201171875" />
                  <Point X="27.1891640625" Y="-2.546375" />
                  <Point X="27.211462890625" Y="-2.669841796875" />
                  <Point X="27.22819140625" Y="-2.762465332031" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.56640234375" Y="-3.354155273438" />
                  <Point X="27.98667578125" Y="-4.082088867188" />
                  <Point X="27.915484375" Y="-4.132938964844" />
                  <Point X="27.8353203125" Y="-4.190197265625" />
                  <Point X="27.72471484375" Y="-4.261791015625" />
                  <Point X="27.679775390625" Y="-4.290878417969" />
                  <Point X="27.268654296875" Y="-3.755092773438" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.548779296875" Y="-2.902179443359" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.292626953125" Y="-2.847972167969" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.062498046875" Y="-2.954014160156" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045986328125" />
                  <Point X="25.937708984375" Y="-3.187449707031" />
                  <Point X="25.914642578125" Y="-3.29357421875" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="26.008103515625" Y="-4.026041992188" />
                  <Point X="26.127642578125" Y="-4.934028320312" />
                  <Point X="26.070205078125" Y="-4.946618652344" />
                  <Point X="25.994349609375" Y="-4.96324609375" />
                  <Point X="25.8921875" Y="-4.981805664062" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#174" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.110413401202" Y="4.767192917724" Z="1.45" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.45" />
                  <Point X="-0.532173715165" Y="5.036656325159" Z="1.45" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.45" />
                  <Point X="-1.31253717172" Y="4.891660490197" Z="1.45" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.45" />
                  <Point X="-1.726024460128" Y="4.582779778026" Z="1.45" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.45" />
                  <Point X="-1.721481873578" Y="4.399298544457" Z="1.45" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.45" />
                  <Point X="-1.782434438134" Y="4.32319602673" Z="1.45" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.45" />
                  <Point X="-1.879911916661" Y="4.320970563909" Z="1.45" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.45" />
                  <Point X="-2.048573885926" Y="4.498196206039" Z="1.45" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.45" />
                  <Point X="-2.413862410865" Y="4.454578872598" Z="1.45" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.45" />
                  <Point X="-3.040341486495" Y="4.052938631042" Z="1.45" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.45" />
                  <Point X="-3.163181617938" Y="3.420310911995" Z="1.45" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.45" />
                  <Point X="-2.998316501989" Y="3.103643560899" Z="1.45" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.45" />
                  <Point X="-3.020068178434" Y="3.028735389452" Z="1.45" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.45" />
                  <Point X="-3.091432904708" Y="2.997248197184" Z="1.45" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.45" />
                  <Point X="-3.513548436302" Y="3.217012330808" Z="1.45" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.45" />
                  <Point X="-3.971056153808" Y="3.150505524011" Z="1.45" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.45" />
                  <Point X="-4.353401833816" Y="2.596700549844" Z="1.45" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.45" />
                  <Point X="-4.061369071326" Y="1.89076017885" Z="1.45" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.45" />
                  <Point X="-3.683814780916" Y="1.586346381009" Z="1.45" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.45" />
                  <Point X="-3.677387175261" Y="1.528198836428" Z="1.45" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.45" />
                  <Point X="-3.717799240359" Y="1.485898511927" Z="1.45" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.45" />
                  <Point X="-4.360601007903" Y="1.554838431772" Z="1.45" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.45" />
                  <Point X="-4.883506315562" Y="1.367569163071" Z="1.45" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.45" />
                  <Point X="-5.010334640017" Y="0.784486982471" Z="1.45" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.45" />
                  <Point X="-4.212553204362" Y="0.219482703064" Z="1.45" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.45" />
                  <Point X="-3.564664913502" Y="0.040812652136" Z="1.45" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.45" />
                  <Point X="-3.544842517224" Y="0.01703068288" Z="1.45" />
                  <Point X="-3.539556741714" Y="0" Z="1.45" />
                  <Point X="-3.543522083911" Y="-0.012776268181" Z="1.45" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.45" />
                  <Point X="-3.560703681622" Y="-0.038063330998" Z="1.45" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.45" />
                  <Point X="-4.42433458441" Y="-0.276229353692" Z="1.45" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.45" />
                  <Point X="-5.027037064122" Y="-0.679403116116" Z="1.45" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.45" />
                  <Point X="-4.924471078486" Y="-1.217485021041" Z="1.45" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.45" />
                  <Point X="-3.916865251897" Y="-1.398718075741" Z="1.45" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.45" />
                  <Point X="-3.207807556751" Y="-1.313544207479" Z="1.45" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.45" />
                  <Point X="-3.195978713716" Y="-1.335200531747" Z="1.45" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.45" />
                  <Point X="-3.944596524994" Y="-1.923253955165" Z="1.45" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.45" />
                  <Point X="-4.377076967139" Y="-2.562642748307" Z="1.45" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.45" />
                  <Point X="-4.060569212888" Y="-3.039360822235" Z="1.45" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.45" />
                  <Point X="-3.1255197209" Y="-2.874581041347" Z="1.45" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.45" />
                  <Point X="-2.565403332216" Y="-2.562927136915" Z="1.45" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.45" />
                  <Point X="-2.980835971822" Y="-3.309558661592" Z="1.45" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.45" />
                  <Point X="-3.124421685192" Y="-3.99737139363" Z="1.45" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.45" />
                  <Point X="-2.702151904531" Y="-4.29410745636" Z="1.45" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.45" />
                  <Point X="-2.322620090899" Y="-4.28208021241" Z="1.45" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.45" />
                  <Point X="-2.115649197007" Y="-4.082569500476" Z="1.45" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.45" />
                  <Point X="-1.835555608559" Y="-3.992782020167" Z="1.45" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.45" />
                  <Point X="-1.558683344454" Y="-4.092059234343" Z="1.45" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.45" />
                  <Point X="-1.399461867872" Y="-4.339370239566" Z="1.45" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.45" />
                  <Point X="-1.392430105436" Y="-4.722507093224" Z="1.45" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.45" />
                  <Point X="-1.286353216104" Y="-4.912113851541" Z="1.45" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.45" />
                  <Point X="-0.988925803739" Y="-4.980521223371" Z="1.45" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.45" />
                  <Point X="-0.588789484917" Y="-4.159576342999" Z="1.45" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.45" />
                  <Point X="-0.346907545414" Y="-3.417658181435" Z="1.45" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.45" />
                  <Point X="-0.14476080182" Y="-3.249167779151" Z="1.45" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.45" />
                  <Point X="0.108598277541" Y="-3.237944266137" Z="1.45" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.45" />
                  <Point X="0.323538314018" Y="-3.383987574333" Z="1.45" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.45" />
                  <Point X="0.645965375485" Y="-4.372959686817" Z="1.45" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.45" />
                  <Point X="0.894968799574" Y="-4.999720231401" Z="1.45" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.45" />
                  <Point X="1.07475397009" Y="-4.964179177762" Z="1.45" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.45" />
                  <Point X="1.05151972279" Y="-3.988235694099" Z="1.45" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.45" />
                  <Point X="0.980412385291" Y="-3.166789233616" Z="1.45" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.45" />
                  <Point X="1.088307137311" Y="-2.961180712038" Z="1.45" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.45" />
                  <Point X="1.29105260291" Y="-2.866481736417" Z="1.45" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.45" />
                  <Point X="1.515582377218" Y="-2.912957433737" Z="1.45" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.45" />
                  <Point X="2.222828662518" Y="-3.754250683549" Z="1.45" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.45" />
                  <Point X="2.745727531938" Y="-4.27248561072" Z="1.45" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.45" />
                  <Point X="2.938387830492" Y="-4.142345982851" Z="1.45" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.45" />
                  <Point X="2.603546307486" Y="-3.297875164883" Z="1.45" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.45" />
                  <Point X="2.254509124679" Y="-2.629674944631" Z="1.45" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.45" />
                  <Point X="2.272708046473" Y="-2.429260696643" Z="1.45" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.45" />
                  <Point X="2.40363768173" Y="-2.286193263494" Z="1.45" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.45" />
                  <Point X="2.598831858882" Y="-2.248938884812" Z="1.45" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.45" />
                  <Point X="3.489538432224" Y="-2.714203192789" Z="1.45" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.45" />
                  <Point X="4.139957564265" Y="-2.940171593329" Z="1.45" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.45" />
                  <Point X="4.308083475974" Y="-2.687800953453" Z="1.45" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.45" />
                  <Point X="3.709874871127" Y="-2.011402457843" Z="1.45" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.45" />
                  <Point X="3.149673559002" Y="-1.547601616981" Z="1.45" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.45" />
                  <Point X="3.09900446709" Y="-1.385035999756" Z="1.45" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.45" />
                  <Point X="3.155031481031" Y="-1.230797686118" Z="1.45" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.45" />
                  <Point X="3.295560147133" Y="-1.138468680214" Z="1.45" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.45" />
                  <Point X="4.260752536141" Y="-1.229332812268" Z="1.45" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.45" />
                  <Point X="4.943197002997" Y="-1.155823117338" Z="1.45" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.45" />
                  <Point X="5.015689089902" Y="-0.783573006263" Z="1.45" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.45" />
                  <Point X="4.305203458724" Y="-0.370125520894" Z="1.45" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.45" />
                  <Point X="3.708299788668" Y="-0.197890543563" Z="1.45" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.45" />
                  <Point X="3.631650968095" Y="-0.137021931255" Z="1.45" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.45" />
                  <Point X="3.592006141511" Y="-0.055199987314" Z="1.45" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.45" />
                  <Point X="3.589365308914" Y="0.041410543892" Z="1.45" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.45" />
                  <Point X="3.623728470305" Y="0.12692680732" Z="1.45" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.45" />
                  <Point X="3.695095625684" Y="0.190258300679" Z="1.45" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.45" />
                  <Point X="4.490764358582" Y="0.419846411875" Z="1.45" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.45" />
                  <Point X="5.019767189758" Y="0.750593125501" Z="1.45" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.45" />
                  <Point X="4.938680522715" Y="1.170847664828" Z="1.45" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.45" />
                  <Point X="4.070780213962" Y="1.302023906434" Z="1.45" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.45" />
                  <Point X="3.422760910043" Y="1.227358215432" Z="1.45" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.45" />
                  <Point X="3.339109671713" Y="1.251272077084" Z="1.45" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.45" />
                  <Point X="3.278719456717" Y="1.304980750414" Z="1.45" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.45" />
                  <Point X="3.243687480458" Y="1.383421521138" Z="1.45" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.45" />
                  <Point X="3.242817928" Y="1.465338793899" Z="1.45" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.45" />
                  <Point X="3.279883381194" Y="1.54162472149" Z="1.45" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.45" />
                  <Point X="3.961063758608" Y="2.082050040905" Z="1.45" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.45" />
                  <Point X="4.35767258007" Y="2.603291217016" Z="1.45" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.45" />
                  <Point X="4.137059471475" Y="2.941287514197" Z="1.45" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.45" />
                  <Point X="3.14956389424" Y="2.636321616123" Z="1.45" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.45" />
                  <Point X="2.475464509555" Y="2.257796173533" Z="1.45" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.45" />
                  <Point X="2.399833807454" Y="2.249117420132" Z="1.45" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.45" />
                  <Point X="2.33303052952" Y="2.272313585552" Z="1.45" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.45" />
                  <Point X="2.278445014683" Y="2.323994330862" Z="1.45" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.45" />
                  <Point X="2.250312282627" Y="2.389924636378" Z="1.45" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.45" />
                  <Point X="2.254731769124" Y="2.464005066327" Z="1.45" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.45" />
                  <Point X="2.759303736736" Y="3.362575187715" Z="1.45" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.45" />
                  <Point X="2.967833706464" Y="4.116608173807" Z="1.45" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.45" />
                  <Point X="2.583015103816" Y="4.368355637755" Z="1.45" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.45" />
                  <Point X="2.17928067013" Y="4.583288154958" Z="1.45" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.45" />
                  <Point X="1.760878712868" Y="4.759737220333" Z="1.45" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.45" />
                  <Point X="1.236334352542" Y="4.915986996156" Z="1.45" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.45" />
                  <Point X="0.57566798298" Y="5.036273086503" Z="1.45" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.45" />
                  <Point X="0.082831160351" Y="4.664254588227" Z="1.45" />
                  <Point X="0" Y="4.355124473572" Z="1.45" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>