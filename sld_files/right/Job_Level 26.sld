<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#167" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1945" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.760640625" Y="-4.248995117188" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140625" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.461708984375" Y="-3.351166259766" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209020263672" />
                  <Point X="25.33049609375" Y="-3.189776611328" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.177685546875" Y="-3.136932617188" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.838365234375" Y="-3.135772460938" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.18977734375" />
                  <Point X="24.655560546875" Y="-3.209021484375" />
                  <Point X="24.63367578125" Y="-3.231477050781" />
                  <Point X="24.55301953125" Y="-3.3476875" />
                  <Point X="24.46994921875" Y="-3.467377441406" />
                  <Point X="24.4618125" Y="-3.481571777344" />
                  <Point X="24.449009765625" Y="-3.512524658203" />
                  <Point X="24.245765625" Y="-4.271043457031" />
                  <Point X="24.0834140625" Y="-4.876941894531" />
                  <Point X="24.0684609375" Y="-4.8740390625" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.77921875" Y="-4.808958007813" />
                  <Point X="23.753583984375" Y="-4.802362304688" />
                  <Point X="23.761283203125" Y="-4.743873046875" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.753673828125" Y="-4.366321289063" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.5614453125" Y="-4.030430419922" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295898438" />
                  <Point X="23.387115234375" Y="-3.897994628906" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.2044609375" Y="-3.880970214844" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894801513672" />
                  <Point X="22.83026171875" Y="-3.979714355469" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461425781" />
                  <Point X="22.550791015625" Y="-4.248166992188" />
                  <Point X="22.5198515625" Y="-4.288488769531" />
                  <Point X="22.41493359375" Y="-4.223526367188" />
                  <Point X="22.198287109375" Y="-4.089383789063" />
                  <Point X="22.002435546875" Y="-3.938585449219" />
                  <Point X="21.89527734375" Y="-3.856077392578" />
                  <Point X="22.209509765625" Y="-3.311811279297" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647654296875" />
                  <Point X="22.593412109375" Y="-2.616127197266" />
                  <Point X="22.594423828125" Y="-2.585190917969" />
                  <Point X="22.585439453125" Y="-2.555571044922" />
                  <Point X="22.571220703125" Y="-2.526741699219" />
                  <Point X="22.5531953125" Y="-2.501589355469" />
                  <Point X="22.5358515625" Y="-2.484244873047" />
                  <Point X="22.510701171875" Y="-2.466219726562" />
                  <Point X="22.481873046875" Y="-2.452" />
                  <Point X="22.45225390625" Y="-2.443012451172" />
                  <Point X="22.42131640625" Y="-2.444023925781" />
                  <Point X="22.389787109375" Y="-2.450294677734" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.706609375" Y="-2.838904296875" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="21.088298828125" Y="-3.018728271484" />
                  <Point X="20.917142578125" Y="-2.793863525391" />
                  <Point X="20.776728515625" Y="-2.558412597656" />
                  <Point X="20.693857421875" Y="-2.419450195312" />
                  <Point X="21.252390625" Y="-1.990872558594" />
                  <Point X="21.894044921875" Y="-1.498513305664" />
                  <Point X="21.91541796875" Y="-1.475599365234" />
                  <Point X="21.9333828125" Y="-1.44847253418" />
                  <Point X="21.946142578125" Y="-1.419838989258" />
                  <Point X="21.953849609375" Y="-1.390084594727" />
                  <Point X="21.956654296875" Y="-1.359647705078" />
                  <Point X="21.954443359375" Y="-1.327977661133" />
                  <Point X="21.94744140625" Y="-1.298233398438" />
                  <Point X="21.931357421875" Y="-1.272251098633" />
                  <Point X="21.9105234375" Y="-1.248295776367" />
                  <Point X="21.887025390625" Y="-1.228766113281" />
                  <Point X="21.86054296875" Y="-1.2131796875" />
                  <Point X="21.831279296875" Y="-1.201955444336" />
                  <Point X="21.79939453125" Y="-1.195474609375" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="20.942197265625" Y="-1.303112304688" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.232865234375" Y="-1.254729125977" />
                  <Point X="20.165921875" Y="-0.992649963379" />
                  <Point X="20.1287734375" Y="-0.732911682129" />
                  <Point X="20.107576171875" Y="-0.584698425293" />
                  <Point X="20.73618359375" Y="-0.41626361084" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.489671875" Y="-0.21112739563" />
                  <Point X="21.51891015625" Y="-0.194419067383" />
                  <Point X="21.532099609375" Y="-0.185325759888" />
                  <Point X="21.558001953125" Y="-0.16403427124" />
                  <Point X="21.574171875" Y="-0.146979553223" />
                  <Point X="21.585662109375" Y="-0.126477233887" />
                  <Point X="21.59764453125" Y="-0.097054794312" />
                  <Point X="21.60220703125" Y="-0.082671287537" />
                  <Point X="21.609146484375" Y="-0.052728359222" />
                  <Point X="21.611599609375" Y="-0.031333280563" />
                  <Point X="21.609169921875" Y="-0.009935461044" />
                  <Point X="21.6023671875" Y="0.019567884445" />
                  <Point X="21.59782421875" Y="0.033948051453" />
                  <Point X="21.585705078125" Y="0.063810077667" />
                  <Point X="21.5742421875" Y="0.084325500488" />
                  <Point X="21.55809375" Y="0.101399299622" />
                  <Point X="21.5326015625" Y="0.12240663147" />
                  <Point X="21.51942578125" Y="0.13151512146" />
                  <Point X="21.48977734375" Y="0.148507751465" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="20.71430078125" Y="0.359566955566" />
                  <Point X="20.108185546875" Y="0.521975341797" />
                  <Point X="20.13237109375" Y="0.685423461914" />
                  <Point X="20.17551171875" Y="0.976969116211" />
                  <Point X="20.25029296875" Y="1.252937988281" />
                  <Point X="20.29644921875" Y="1.423268066406" />
                  <Point X="20.71037890625" Y="1.368773193359" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228027344" />
                  <Point X="21.296865234375" Y="1.305263549805" />
                  <Point X="21.327125" Y="1.31480456543" />
                  <Point X="21.358291015625" Y="1.324631103516" />
                  <Point X="21.37722265625" Y="1.332961669922" />
                  <Point X="21.395966796875" Y="1.343783569336" />
                  <Point X="21.4126484375" Y="1.356014892578" />
                  <Point X="21.426287109375" Y="1.371566894531" />
                  <Point X="21.438701171875" Y="1.389296142578" />
                  <Point X="21.448650390625" Y="1.407433837891" />
                  <Point X="21.460791015625" Y="1.436746948242" />
                  <Point X="21.473296875" Y="1.466938110352" />
                  <Point X="21.479083984375" Y="1.486788085938" />
                  <Point X="21.48284375" Y="1.508103759766" />
                  <Point X="21.484197265625" Y="1.52874987793" />
                  <Point X="21.481048828125" Y="1.54919934082" />
                  <Point X="21.4754453125" Y="1.570106445312" />
                  <Point X="21.46794921875" Y="1.589378051758" />
                  <Point X="21.453298828125" Y="1.617521728516" />
                  <Point X="21.438208984375" Y="1.64650769043" />
                  <Point X="21.426716796875" Y="1.663707763672" />
                  <Point X="21.4128046875" Y="1.680287231445" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="20.96604296875" Y="2.025938720703" />
                  <Point X="20.648140625" Y="2.269873535156" />
                  <Point X="20.751212890625" Y="2.446462402344" />
                  <Point X="20.9188515625" Y="2.733665283203" />
                  <Point X="21.116935546875" Y="2.988275146484" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.471544921875" Y="3.030462158203" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.8953515625" Y="2.822109375" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.95943359375" Y="2.818762939453" />
                  <Point X="21.980892578125" Y="2.821587890625" />
                  <Point X="22.000984375" Y="2.826504150391" />
                  <Point X="22.019537109375" Y="2.835652832031" />
                  <Point X="22.037791015625" Y="2.847281738281" />
                  <Point X="22.053923828125" Y="2.860229003906" />
                  <Point X="22.083837890625" Y="2.890143066406" />
                  <Point X="22.1146484375" Y="2.920952392578" />
                  <Point X="22.127595703125" Y="2.937084716797" />
                  <Point X="22.139224609375" Y="2.955338867188" />
                  <Point X="22.14837109375" Y="2.973887939453" />
                  <Point X="22.1532890625" Y="2.993977050781" />
                  <Point X="22.156115234375" Y="3.015436035156" />
                  <Point X="22.15656640625" Y="3.03612109375" />
                  <Point X="22.15287890625" Y="3.078265136719" />
                  <Point X="22.14908203125" Y="3.121670654297" />
                  <Point X="22.145044921875" Y="3.141961669922" />
                  <Point X="22.13853515625" Y="3.162604492188" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.9388515625" Y="3.512966552734" />
                  <Point X="21.81666796875" Y="3.724595947266" />
                  <Point X="22.0074140625" Y="3.870840087891" />
                  <Point X="22.29937890625" Y="4.094685791016" />
                  <Point X="22.611357421875" Y="4.268013671875" />
                  <Point X="22.832962890625" Y="4.391133789062" />
                  <Point X="22.858291015625" Y="4.358125" />
                  <Point X="22.9568046875" Y="4.229740722656" />
                  <Point X="22.971109375" Y="4.214797851563" />
                  <Point X="22.987689453125" Y="4.200885742188" />
                  <Point X="23.00488671875" Y="4.18939453125" />
                  <Point X="23.05179296875" Y="4.1649765625" />
                  <Point X="23.100103515625" Y="4.139827636719" />
                  <Point X="23.1193828125" Y="4.132331054688" />
                  <Point X="23.1402890625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.12358203125" />
                  <Point X="23.181375" Y="4.124935058594" />
                  <Point X="23.20269140625" Y="4.128693847656" />
                  <Point X="23.222548828125" Y="4.134481933594" />
                  <Point X="23.271404296875" Y="4.15471875" />
                  <Point X="23.32172265625" Y="4.175561523437" />
                  <Point X="23.339857421875" Y="4.18551171875" />
                  <Point X="23.3575859375" Y="4.19792578125" />
                  <Point X="23.37313671875" Y="4.2115625" />
                  <Point X="23.3853671875" Y="4.228241210938" />
                  <Point X="23.396189453125" Y="4.246984375" />
                  <Point X="23.404521484375" Y="4.265922363281" />
                  <Point X="23.420421875" Y="4.316355957031" />
                  <Point X="23.43680078125" Y="4.368298828125" />
                  <Point X="23.4408359375" Y="4.388583007812" />
                  <Point X="23.44272265625" Y="4.410145507812" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.420515625" Y="4.5960703125" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.67240625" Y="4.703841796875" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.428578125" Y="4.854072265625" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.7708984375" Y="4.641597167969" />
                  <Point X="24.866095703125" Y="4.286315917969" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.244265625" Y="4.652232421875" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.514021484375" Y="4.86630078125" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.156951171875" Y="4.756192871094" />
                  <Point X="26.48102734375" Y="4.677950195312" />
                  <Point X="26.68394921875" Y="4.604350097656" />
                  <Point X="26.894640625" Y="4.527930664062" />
                  <Point X="27.091591796875" Y="4.435823242188" />
                  <Point X="27.294580078125" Y="4.340891601563" />
                  <Point X="27.484861328125" Y="4.230033691406" />
                  <Point X="27.68097265625" Y="4.115777832031" />
                  <Point X="27.860419921875" Y="3.988166748047" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.57204296875" Y="3.286284667969" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.5399375" />
                  <Point X="27.133078125" Y="2.516057373047" />
                  <Point X="27.122501953125" Y="2.476506347656" />
                  <Point X="27.111607421875" Y="2.435770996094" />
                  <Point X="27.108619140625" Y="2.417937011719" />
                  <Point X="27.107728515625" Y="2.380951904297" />
                  <Point X="27.111853515625" Y="2.346751464844" />
                  <Point X="27.116099609375" Y="2.311527099609" />
                  <Point X="27.121443359375" Y="2.289605224609" />
                  <Point X="27.1297109375" Y="2.267513671875" />
                  <Point X="27.140072265625" Y="2.247469970703" />
                  <Point X="27.161234375" Y="2.216282470703" />
                  <Point X="27.18303125" Y="2.184161376953" />
                  <Point X="27.194466796875" Y="2.170328125" />
                  <Point X="27.221599609375" Y="2.145592529297" />
                  <Point X="27.252787109375" Y="2.124430419922" />
                  <Point X="27.284908203125" Y="2.102635009766" />
                  <Point X="27.304953125" Y="2.092272216797" />
                  <Point X="27.327041015625" Y="2.084006347656" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.3831640625" Y="2.074539550781" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845458984" />
                  <Point X="27.47320703125" Y="2.074170898438" />
                  <Point X="27.5127578125" Y="2.084747558594" />
                  <Point X="27.553494140625" Y="2.095640625" />
                  <Point X="27.565283203125" Y="2.099637939453" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.34573046875" Y="2.5473125" />
                  <Point X="28.967328125" Y="2.906191650391" />
                  <Point X="29.006943359375" Y="2.851135498047" />
                  <Point X="29.12326953125" Y="2.689468017578" />
                  <Point X="29.22330859375" Y="2.524152587891" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.789626953125" Y="2.097265625" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.221427734375" Y="1.660243408203" />
                  <Point X="28.20397265625" Y="1.641627075195" />
                  <Point X="28.1755078125" Y="1.604492431641" />
                  <Point X="28.14619140625" Y="1.566245849609" />
                  <Point X="28.136607421875" Y="1.550911743164" />
                  <Point X="28.121630859375" Y="1.517088134766" />
                  <Point X="28.11102734375" Y="1.479173461914" />
                  <Point X="28.10010546875" Y="1.440123779297" />
                  <Point X="28.09665234375" Y="1.417825317383" />
                  <Point X="28.0958359375" Y="1.39425378418" />
                  <Point X="28.097740234375" Y="1.371766113281" />
                  <Point X="28.1064453125" Y="1.329581298828" />
                  <Point X="28.11541015625" Y="1.286133300781" />
                  <Point X="28.1206796875" Y="1.26898046875" />
                  <Point X="28.13628125" Y="1.235741455078" />
                  <Point X="28.159955078125" Y="1.199757446289" />
                  <Point X="28.184337890625" Y="1.162696166992" />
                  <Point X="28.198892578125" Y="1.145449951172" />
                  <Point X="28.21613671875" Y="1.129360229492" />
                  <Point X="28.23434765625" Y="1.116034667969" />
                  <Point X="28.26865625" Y="1.096722412109" />
                  <Point X="28.303990234375" Y="1.07683215332" />
                  <Point X="28.320521484375" Y="1.069501342773" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.402505859375" Y="1.053308105469" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.207490234375" Y="1.141680297852" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.79597265625" Y="1.138043823242" />
                  <Point X="29.845939453125" Y="0.932788818359" />
                  <Point X="29.8774609375" Y="0.730334899902" />
                  <Point X="29.890865234375" Y="0.644238647461" />
                  <Point X="29.357025390625" Y="0.501196350098" />
                  <Point X="28.716580078125" Y="0.329589874268" />
                  <Point X="28.704791015625" Y="0.325586120605" />
                  <Point X="28.681546875" Y="0.315067810059" />
                  <Point X="28.635974609375" Y="0.288725799561" />
                  <Point X="28.589037109375" Y="0.261595367432" />
                  <Point X="28.5743125" Y="0.251097106934" />
                  <Point X="28.54753125" Y="0.225576583862" />
                  <Point X="28.5201875" Y="0.190734161377" />
                  <Point X="28.492025390625" Y="0.154848937988" />
                  <Point X="28.48030078125" Y="0.135566558838" />
                  <Point X="28.47052734375" Y="0.114102180481" />
                  <Point X="28.463681640625" Y="0.092604560852" />
                  <Point X="28.45456640625" Y="0.04501159668" />
                  <Point X="28.4451796875" Y="-0.004005921364" />
                  <Point X="28.443484375" Y="-0.021876050949" />
                  <Point X="28.4451796875" Y="-0.058554073334" />
                  <Point X="28.454294921875" Y="-0.106147041321" />
                  <Point X="28.463681640625" Y="-0.155164703369" />
                  <Point X="28.47052734375" Y="-0.176662322998" />
                  <Point X="28.48030078125" Y="-0.198126693726" />
                  <Point X="28.492025390625" Y="-0.217409088135" />
                  <Point X="28.519369140625" Y="-0.252251358032" />
                  <Point X="28.54753125" Y="-0.288136871338" />
                  <Point X="28.56" Y="-0.301236694336" />
                  <Point X="28.589037109375" Y="-0.32415536499" />
                  <Point X="28.634609375" Y="-0.350497375488" />
                  <Point X="28.681546875" Y="-0.377627929688" />
                  <Point X="28.6927109375" Y="-0.38313885498" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="29.376197265625" Y="-0.568893859863" />
                  <Point X="29.891474609375" Y="-0.706961853027" />
                  <Point X="29.882921875" Y="-0.763688720703" />
                  <Point X="29.855025390625" Y="-0.948723754883" />
                  <Point X="29.81463671875" Y="-1.125709838867" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="29.1671171875" Y="-1.101223999023" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.285216796875" Y="-1.024949584961" />
                  <Point X="28.193095703125" Y="-1.044972412109" />
                  <Point X="28.163974609375" Y="-1.056597167969" />
                  <Point X="28.1361484375" Y="-1.073489379883" />
                  <Point X="28.1123984375" Y="-1.093960205078" />
                  <Point X="28.0583359375" Y="-1.158980957031" />
                  <Point X="28.002654296875" Y="-1.225948242188" />
                  <Point X="27.987935546875" Y="-1.250329223633" />
                  <Point X="27.976591796875" Y="-1.277714599609" />
                  <Point X="27.969759765625" Y="-1.305365966797" />
                  <Point X="27.96201171875" Y="-1.389570678711" />
                  <Point X="27.95403125" Y="-1.476296386719" />
                  <Point X="27.95634765625" Y="-1.507560913086" />
                  <Point X="27.964078125" Y="-1.539182495117" />
                  <Point X="27.976451171875" Y="-1.567996337891" />
                  <Point X="28.02594921875" Y="-1.644989135742" />
                  <Point X="28.076931640625" Y="-1.724286865234" />
                  <Point X="28.0869375" Y="-1.737244140625" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.722759765625" Y="-2.230612792969" />
                  <Point X="29.213123046875" Y="-2.6068828125" />
                  <Point X="29.203451171875" Y="-2.622533935547" />
                  <Point X="29.12480078125" Y="-2.749800537109" />
                  <Point X="29.041287109375" Y="-2.868462890625" />
                  <Point X="29.028982421875" Y="-2.885945556641" />
                  <Point X="28.4624609375" Y="-2.558864257812" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.647775390625" Y="-2.140600097656" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135176757812" />
                  <Point X="27.3563984375" Y="-2.181719482422" />
                  <Point X="27.26531640625" Y="-2.229655761719" />
                  <Point X="27.242384765625" Y="-2.246549072266" />
                  <Point X="27.22142578125" Y="-2.267508300781" />
                  <Point X="27.204533203125" Y="-2.290438720703" />
                  <Point X="27.157990234375" Y="-2.378874023438" />
                  <Point X="27.110052734375" Y="-2.469956787109" />
                  <Point X="27.100228515625" Y="-2.499734863281" />
                  <Point X="27.095271484375" Y="-2.531908203125" />
                  <Point X="27.09567578125" Y="-2.563259521484" />
                  <Point X="27.11490234375" Y="-2.669711181641" />
                  <Point X="27.134703125" Y="-2.779349853516" />
                  <Point X="27.13898828125" Y="-2.795139648438" />
                  <Point X="27.1518203125" Y="-2.826078613281" />
                  <Point X="27.545173828125" Y="-3.507388671875" />
                  <Point X="27.86128515625" Y="-4.054907226563" />
                  <Point X="27.781873046875" Y="-4.11162890625" />
                  <Point X="27.701765625" Y="-4.163480957031" />
                  <Point X="27.263337890625" Y="-3.592110351562" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.616935546875" Y="-2.833058105469" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.302275390625" Y="-2.751683105469" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.01593359375" Y="-2.869182861328" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968860839844" />
                  <Point X="25.88725" Y="-2.996687255859" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.849115234375" Y="-3.147776855469" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120361328" />
                  <Point X="25.931216796875" Y="-4.169846679688" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941578125" Y="-4.752637207031" />
                  <Point X="23.85875390625" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575836914062" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030761719" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688964844" />
                  <Point X="23.84684765625" Y="-4.347787109375" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.811875" Y="-4.178468261719" />
                  <Point X="23.80097265625" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.74979296875" Y="-4.070937744141" />
                  <Point X="23.738994140625" Y="-4.059779296875" />
                  <Point X="23.624083984375" Y="-3.959005615234" />
                  <Point X="23.505734375" Y="-3.85521484375" />
                  <Point X="23.49326171875" Y="-3.845965087891" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822527587891" />
                  <Point X="23.423470703125" Y="-3.810226318359" />
                  <Point X="23.4086875" Y="-3.805476318359" />
                  <Point X="23.378544921875" Y="-3.798447998047" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.210673828125" Y="-3.786173583984" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.946326171875" Y="-3.79549609375" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812011719" />
                  <Point X="22.777482421875" Y="-3.900724853516" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.619560546875" Y="-4.010130859375" />
                  <Point X="22.597244140625" Y="-4.032769287109" />
                  <Point X="22.589529296875" Y="-4.041628662109" />
                  <Point X="22.496796875" Y="-4.162477539063" />
                  <Point X="22.4649453125" Y="-4.142755859375" />
                  <Point X="22.252408203125" Y="-4.011156738281" />
                  <Point X="22.060392578125" Y="-3.863312744141" />
                  <Point X="22.01913671875" Y="-3.831546630859" />
                  <Point X="22.29178125" Y="-3.359311279297" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.710084960938" />
                  <Point X="22.67605078125" Y="-2.681119628906" />
                  <Point X="22.680314453125" Y="-2.666188964844" />
                  <Point X="22.6865859375" Y="-2.634661865234" />
                  <Point X="22.688361328125" Y="-2.619232421875" />
                  <Point X="22.689373046875" Y="-2.588296142578" />
                  <Point X="22.685333984375" Y="-2.557615966797" />
                  <Point X="22.676349609375" Y="-2.52799609375" />
                  <Point X="22.670640625" Y="-2.513549560547" />
                  <Point X="22.656421875" Y="-2.484720214844" />
                  <Point X="22.648439453125" Y="-2.471403320312" />
                  <Point X="22.6304140625" Y="-2.446250976562" />
                  <Point X="22.62037109375" Y="-2.434415527344" />
                  <Point X="22.60302734375" Y="-2.417071044922" />
                  <Point X="22.59119140625" Y="-2.407028320312" />
                  <Point X="22.566041015625" Y="-2.389003173828" />
                  <Point X="22.5527265625" Y="-2.381020751953" />
                  <Point X="22.5238984375" Y="-2.366801025391" />
                  <Point X="22.50945703125" Y="-2.361093017578" />
                  <Point X="22.479837890625" Y="-2.35210546875" />
                  <Point X="22.449150390625" Y="-2.348063232422" />
                  <Point X="22.418212890625" Y="-2.349074707031" />
                  <Point X="22.40278515625" Y="-2.350848876953" />
                  <Point X="22.371255859375" Y="-2.357119628906" />
                  <Point X="22.35632421875" Y="-2.361383056641" />
                  <Point X="22.32735546875" Y="-2.372285644531" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.659109375" Y="-2.756631835938" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="21.163892578125" Y="-2.961189941406" />
                  <Point X="20.99598046875" Y="-2.740588134766" />
                  <Point X="20.8583203125" Y="-2.50975390625" />
                  <Point X="20.818734375" Y="-2.443373779297" />
                  <Point X="21.31022265625" Y="-2.066241210938" />
                  <Point X="21.951876953125" Y="-1.573881835938" />
                  <Point X="21.963515625" Y="-1.563312011719" />
                  <Point X="21.984888671875" Y="-1.540398071289" />
                  <Point X="21.994623046875" Y="-1.528053710938" />
                  <Point X="22.012587890625" Y="-1.500926879883" />
                  <Point X="22.02015625" Y="-1.487140991211" />
                  <Point X="22.032916015625" Y="-1.458507446289" />
                  <Point X="22.038107421875" Y="-1.443659912109" />
                  <Point X="22.045814453125" Y="-1.413905517578" />
                  <Point X="22.04844921875" Y="-1.398801635742" />
                  <Point X="22.05125390625" Y="-1.368364746094" />
                  <Point X="22.051423828125" Y="-1.353031738281" />
                  <Point X="22.049212890625" Y="-1.321361694336" />
                  <Point X="22.046916015625" Y="-1.306209106445" />
                  <Point X="22.0399140625" Y="-1.27646496582" />
                  <Point X="22.028216796875" Y="-1.24823034668" />
                  <Point X="22.0121328125" Y="-1.222248046875" />
                  <Point X="22.003041015625" Y="-1.209908447266" />
                  <Point X="21.98220703125" Y="-1.185953369141" />
                  <Point X="21.97124609375" Y="-1.175235229492" />
                  <Point X="21.947748046875" Y="-1.155705566406" />
                  <Point X="21.9352109375" Y="-1.146893920898" />
                  <Point X="21.908728515625" Y="-1.131307495117" />
                  <Point X="21.894564453125" Y="-1.12448034668" />
                  <Point X="21.86530078125" Y="-1.113256103516" />
                  <Point X="21.850201171875" Y="-1.108859008789" />
                  <Point X="21.81831640625" Y="-1.102378173828" />
                  <Point X="21.802701171875" Y="-1.100532104492" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="20.929796875" Y="-1.208925048828" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.32491015625" Y="-1.231217285156" />
                  <Point X="20.259236328125" Y="-0.97411138916" />
                  <Point X="20.22281640625" Y="-0.719461425781" />
                  <Point X="20.213546875" Y="-0.654654663086" />
                  <Point X="20.760771484375" Y="-0.508026580811" />
                  <Point X="21.491712890625" Y="-0.312171325684" />
                  <Point X="21.50328515625" Y="-0.308256896973" />
                  <Point X="21.52583203125" Y="-0.298976013184" />
                  <Point X="21.536806640625" Y="-0.293609527588" />
                  <Point X="21.566044921875" Y="-0.27690133667" />
                  <Point X="21.572833984375" Y="-0.272632293701" />
                  <Point X="21.592423828125" Y="-0.258714508057" />
                  <Point X="21.618326171875" Y="-0.237423080444" />
                  <Point X="21.62694140625" Y="-0.229397293091" />
                  <Point X="21.643111328125" Y="-0.212342666626" />
                  <Point X="21.657044921875" Y="-0.193424316406" />
                  <Point X="21.66853515625" Y="-0.172922088623" />
                  <Point X="21.673646484375" Y="-0.162308914185" />
                  <Point X="21.68562890625" Y="-0.132886489868" />
                  <Point X="21.688197265625" Y="-0.125778747559" />
                  <Point X="21.69475390625" Y="-0.104119636536" />
                  <Point X="21.701693359375" Y="-0.074176727295" />
                  <Point X="21.70352734375" Y="-0.063550041199" />
                  <Point X="21.70598046875" Y="-0.042154884338" />
                  <Point X="21.7059921875" Y="-0.020615116119" />
                  <Point X="21.7035625" Y="0.000782717347" />
                  <Point X="21.701740234375" Y="0.011409106255" />
                  <Point X="21.6949375" Y="0.040912536621" />
                  <Point X="21.692955078125" Y="0.048185993195" />
                  <Point X="21.6858515625" Y="0.069672698975" />
                  <Point X="21.673732421875" Y="0.099534759521" />
                  <Point X="21.66863671875" Y="0.110148216248" />
                  <Point X="21.657173828125" Y="0.130663528442" />
                  <Point X="21.64326171875" Y="0.149604171753" />
                  <Point X="21.62711328125" Y="0.166678131104" />
                  <Point X="21.618509765625" Y="0.174713272095" />
                  <Point X="21.593017578125" Y="0.19572052002" />
                  <Point X="21.586623046875" Y="0.200551513672" />
                  <Point X="21.566666015625" Y="0.213937530518" />
                  <Point X="21.537017578125" Y="0.23093019104" />
                  <Point X="21.5259921875" Y="0.236334259033" />
                  <Point X="21.50333984375" Y="0.245674758911" />
                  <Point X="21.491712890625" Y="0.249611343384" />
                  <Point X="20.738888671875" Y="0.451329925537" />
                  <Point X="20.2145546875" Y="0.591824829102" />
                  <Point X="20.22634765625" Y="0.671517578125" />
                  <Point X="20.26866796875" Y="0.957523376465" />
                  <Point X="20.341986328125" Y="1.228091186523" />
                  <Point X="20.3664140625" Y="1.318237304688" />
                  <Point X="20.697978515625" Y="1.2745859375" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.20470324707" />
                  <Point X="21.28485546875" Y="1.206589477539" />
                  <Point X="21.295109375" Y="1.208053222656" />
                  <Point X="21.3153984375" Y="1.212088745117" />
                  <Point X="21.32543359375" Y="1.214660522461" />
                  <Point X="21.355693359375" Y="1.224201538086" />
                  <Point X="21.386859375" Y="1.234028076172" />
                  <Point X="21.396552734375" Y="1.237677246094" />
                  <Point X="21.415484375" Y="1.2460078125" />
                  <Point X="21.42472265625" Y="1.250689331055" />
                  <Point X="21.443466796875" Y="1.261511108398" />
                  <Point X="21.452140625" Y="1.267171020508" />
                  <Point X="21.468822265625" Y="1.27940234375" />
                  <Point X="21.48407421875" Y="1.293377197266" />
                  <Point X="21.497712890625" Y="1.308929199219" />
                  <Point X="21.504107421875" Y="1.317077392578" />
                  <Point X="21.516521484375" Y="1.334806518555" />
                  <Point X="21.5219921875" Y="1.343607421875" />
                  <Point X="21.53194140625" Y="1.361745117188" />
                  <Point X="21.536419921875" Y="1.37108215332" />
                  <Point X="21.548560546875" Y="1.400395141602" />
                  <Point X="21.56106640625" Y="1.430586425781" />
                  <Point X="21.5645" Y="1.440348388672" />
                  <Point X="21.570287109375" Y="1.460198486328" />
                  <Point X="21.572640625" Y="1.470286254883" />
                  <Point X="21.576400390625" Y="1.491601928711" />
                  <Point X="21.577640625" Y="1.501889160156" />
                  <Point X="21.578994140625" Y="1.52253527832" />
                  <Point X="21.578091796875" Y="1.543205932617" />
                  <Point X="21.574943359375" Y="1.563655395508" />
                  <Point X="21.572810546875" Y="1.573793212891" />
                  <Point X="21.56720703125" Y="1.594700317383" />
                  <Point X="21.563982421875" Y="1.604545043945" />
                  <Point X="21.556486328125" Y="1.623816772461" />
                  <Point X="21.55221484375" Y="1.633243530273" />
                  <Point X="21.537564453125" Y="1.661387207031" />
                  <Point X="21.522474609375" Y="1.690373168945" />
                  <Point X="21.51719921875" Y="1.69928515625" />
                  <Point X="21.50570703125" Y="1.716485229492" />
                  <Point X="21.499490234375" Y="1.72477331543" />
                  <Point X="21.485578125" Y="1.741352783203" />
                  <Point X="21.478498046875" Y="1.748912719727" />
                  <Point X="21.463556640625" Y="1.763215698242" />
                  <Point X="21.4556953125" Y="1.769958496094" />
                  <Point X="21.023875" Y="2.101307128906" />
                  <Point X="20.77238671875" Y="2.29428125" />
                  <Point X="20.833259765625" Y="2.398573242188" />
                  <Point X="20.99771484375" Y="2.680321777344" />
                  <Point X="21.191916015625" Y="2.929940917969" />
                  <Point X="21.273662109375" Y="3.035013427734" />
                  <Point X="21.424044921875" Y="2.948189697266" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.887072265625" Y="2.727470947266" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.961505859375" Y="2.723785644531" />
                  <Point X="21.97183203125" Y="2.724575683594" />
                  <Point X="21.993291015625" Y="2.727400634766" />
                  <Point X="22.00347265625" Y="2.729310302734" />
                  <Point X="22.023564453125" Y="2.7342265625" />
                  <Point X="22.043" Y="2.741300292969" />
                  <Point X="22.061552734375" Y="2.750448974609" />
                  <Point X="22.070580078125" Y="2.755530517578" />
                  <Point X="22.088833984375" Y="2.767159423828" />
                  <Point X="22.097251953125" Y="2.773191162109" />
                  <Point X="22.113384765625" Y="2.786138427734" />
                  <Point X="22.121099609375" Y="2.793053955078" />
                  <Point X="22.151013671875" Y="2.822968017578" />
                  <Point X="22.18182421875" Y="2.85377734375" />
                  <Point X="22.18873828125" Y="2.861490478516" />
                  <Point X="22.201685546875" Y="2.877622802734" />
                  <Point X="22.20771875" Y="2.886041992188" />
                  <Point X="22.21934765625" Y="2.904296142578" />
                  <Point X="22.2244296875" Y="2.913324707031" />
                  <Point X="22.233576171875" Y="2.931873779297" />
                  <Point X="22.240646484375" Y="2.951298339844" />
                  <Point X="22.245564453125" Y="2.971387451172" />
                  <Point X="22.2474765625" Y="2.981572509766" />
                  <Point X="22.250302734375" Y="3.003031494141" />
                  <Point X="22.251091796875" Y="3.013364501953" />
                  <Point X="22.25154296875" Y="3.034049560547" />
                  <Point X="22.251205078125" Y="3.044401611328" />
                  <Point X="22.247517578125" Y="3.086545654297" />
                  <Point X="22.243720703125" Y="3.129951171875" />
                  <Point X="22.242255859375" Y="3.140208496094" />
                  <Point X="22.23821875" Y="3.160499511719" />
                  <Point X="22.235646484375" Y="3.170533203125" />
                  <Point X="22.22913671875" Y="3.191176025391" />
                  <Point X="22.22548828125" Y="3.200870605469" />
                  <Point X="22.217158203125" Y="3.219799072266" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="22.021123046875" Y="3.560466552734" />
                  <Point X="21.94061328125" Y="3.699915039062" />
                  <Point X="22.065216796875" Y="3.795448486328" />
                  <Point X="22.3516328125" Y="4.015040527344" />
                  <Point X="22.657494140625" Y="4.184969238281" />
                  <Point X="22.807474609375" Y="4.268295898437" />
                  <Point X="22.881435546875" Y="4.171909179688" />
                  <Point X="22.8881796875" Y="4.164046875" />
                  <Point X="22.902484375" Y="4.149104003906" />
                  <Point X="22.910044921875" Y="4.142022949219" />
                  <Point X="22.926625" Y="4.128110839844" />
                  <Point X="22.934908203125" Y="4.121896972656" />
                  <Point X="22.95210546875" Y="4.110405761719" />
                  <Point X="22.96101953125" Y="4.105128417969" />
                  <Point X="23.00792578125" Y="4.080710693359" />
                  <Point X="23.056236328125" Y="4.055561767578" />
                  <Point X="23.065673828125" Y="4.051285644531" />
                  <Point X="23.084953125" Y="4.0437890625" />
                  <Point X="23.094794921875" Y="4.040568359375" />
                  <Point X="23.115701171875" Y="4.034966308594" />
                  <Point X="23.1258359375" Y="4.032834960938" />
                  <Point X="23.146279296875" Y="4.029687988281" />
                  <Point X="23.1669453125" Y="4.028785400391" />
                  <Point X="23.187587890625" Y="4.030138427734" />
                  <Point X="23.197873046875" Y="4.031378417969" />
                  <Point X="23.219189453125" Y="4.035137207031" />
                  <Point X="23.229275390625" Y="4.037489257812" />
                  <Point X="23.2491328125" Y="4.04327734375" />
                  <Point X="23.258904296875" Y="4.046713623047" />
                  <Point X="23.307759765625" Y="4.066950439453" />
                  <Point X="23.358078125" Y="4.087793212891" />
                  <Point X="23.367419921875" Y="4.092274658203" />
                  <Point X="23.3855546875" Y="4.102224609375" />
                  <Point X="23.39434765625" Y="4.107693359375" />
                  <Point X="23.412076171875" Y="4.120107421875" />
                  <Point X="23.420220703125" Y="4.126499023438" />
                  <Point X="23.435771484375" Y="4.140135742188" />
                  <Point X="23.44974609375" Y="4.155384765625" />
                  <Point X="23.4619765625" Y="4.172063476562" />
                  <Point X="23.467638671875" Y="4.18073828125" />
                  <Point X="23.4784609375" Y="4.199481445312" />
                  <Point X="23.483146484375" Y="4.208727050781" />
                  <Point X="23.491478515625" Y="4.227665039062" />
                  <Point X="23.495125" Y="4.237357421875" />
                  <Point X="23.511025390625" Y="4.287791015625" />
                  <Point X="23.527404296875" Y="4.339733886719" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370047851562" />
                  <Point X="23.535474609375" Y="4.380302246094" />
                  <Point X="23.537361328125" Y="4.401864746094" />
                  <Point X="23.53769921875" Y="4.412217285156" />
                  <Point X="23.537248046875" Y="4.4328984375" />
                  <Point X="23.536458984375" Y="4.443227050781" />
                  <Point X="23.520734375" Y="4.562655273438" />
                  <Point X="23.698052734375" Y="4.612369140625" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.43962109375" Y="4.759716308594" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.679134765625" Y="4.617009765625" />
                  <Point X="24.77433203125" Y="4.261728515625" />
                  <Point X="24.779564453125" Y="4.247106933594" />
                  <Point X="24.79233984375" Y="4.218912597656" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.247104980469" />
                  <Point X="25.23798046875" Y="4.2617265625" />
                  <Point X="25.336029296875" Y="4.627644042969" />
                  <Point X="25.378193359375" Y="4.785005859375" />
                  <Point X="25.504126953125" Y="4.771817382812" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.13465625" Y="4.663846191406" />
                  <Point X="26.45359765625" Y="4.586843261719" />
                  <Point X="26.651556640625" Y="4.51504296875" />
                  <Point X="26.858255859375" Y="4.440071777344" />
                  <Point X="27.05134765625" Y="4.349769042969" />
                  <Point X="27.25045703125" Y="4.256651367187" />
                  <Point X="27.4370390625" Y="4.147948730469" />
                  <Point X="27.629421875" Y="4.035865478516" />
                  <Point X="27.80536328125" Y="3.910746826172" />
                  <Point X="27.81778125" Y="3.901915283203" />
                  <Point X="27.489771484375" Y="3.333784667969" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.062380859375" Y="2.593116699219" />
                  <Point X="27.053185546875" Y="2.573447265625" />
                  <Point X="27.04418359375" Y="2.549567138672" />
                  <Point X="27.041302734375" Y="2.540598632812" />
                  <Point X="27.0307265625" Y="2.501047607422" />
                  <Point X="27.01983203125" Y="2.460312255859" />
                  <Point X="27.0179140625" Y="2.451470458984" />
                  <Point X="27.013646484375" Y="2.420224121094" />
                  <Point X="27.012755859375" Y="2.383239013672" />
                  <Point X="27.013412109375" Y="2.369576171875" />
                  <Point X="27.017537109375" Y="2.335375732422" />
                  <Point X="27.021783203125" Y="2.300151367188" />
                  <Point X="27.023802734375" Y="2.289028320312" />
                  <Point X="27.029146484375" Y="2.267106445312" />
                  <Point X="27.032470703125" Y="2.256307617188" />
                  <Point X="27.04073828125" Y="2.234216064453" />
                  <Point X="27.0453203125" Y="2.223888671875" />
                  <Point X="27.055681640625" Y="2.203844970703" />
                  <Point X="27.0614609375" Y="2.194128662109" />
                  <Point X="27.082623046875" Y="2.162941162109" />
                  <Point X="27.104419921875" Y="2.130820068359" />
                  <Point X="27.109810546875" Y="2.123632324219" />
                  <Point X="27.13046484375" Y="2.100123291016" />
                  <Point X="27.15759765625" Y="2.075387695312" />
                  <Point X="27.1682578125" Y="2.066981445312" />
                  <Point X="27.1994453125" Y="2.045819213867" />
                  <Point X="27.23156640625" Y="2.024023925781" />
                  <Point X="27.24128125" Y="2.018245239258" />
                  <Point X="27.261326171875" Y="2.007882324219" />
                  <Point X="27.27165625" Y="2.003298461914" />
                  <Point X="27.293744140625" Y="1.995032470703" />
                  <Point X="27.304548828125" Y="1.991707397461" />
                  <Point X="27.32647265625" Y="1.986364746094" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.371791015625" Y="1.98022277832" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.41604296875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975496948242" />
                  <Point X="27.484314453125" Y="1.979822509766" />
                  <Point X="27.49775" Y="1.982395751953" />
                  <Point X="27.53730078125" Y="1.992972412109" />
                  <Point X="27.578037109375" Y="2.003865356445" />
                  <Point X="27.584" Y="2.00567175293" />
                  <Point X="27.60440234375" Y="2.01306640625" />
                  <Point X="27.627654296875" Y="2.023573486328" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.39323046875" Y="2.465040039062" />
                  <Point X="28.94040625" Y="2.780951660156" />
                  <Point X="29.043951171875" Y="2.637047607422" />
                  <Point X="29.13688671875" Y="2.483471679688" />
                  <Point X="28.731794921875" Y="2.172634033203" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.168140625" Y="1.739869506836" />
                  <Point X="28.152125" Y="1.725222290039" />
                  <Point X="28.134669921875" Y="1.706605957031" />
                  <Point X="28.12857421875" Y="1.699421630859" />
                  <Point X="28.100109375" Y="1.662286987305" />
                  <Point X="28.07079296875" Y="1.624040405273" />
                  <Point X="28.0656328125" Y="1.616596435547" />
                  <Point X="28.0497421875" Y="1.589374389648" />
                  <Point X="28.034765625" Y="1.55555078125" />
                  <Point X="28.030140625" Y="1.542674926758" />
                  <Point X="28.019537109375" Y="1.504760131836" />
                  <Point X="28.008615234375" Y="1.465710449219" />
                  <Point X="28.006224609375" Y="1.454662231445" />
                  <Point X="28.002771484375" Y="1.432363647461" />
                  <Point X="28.001708984375" Y="1.421113647461" />
                  <Point X="28.000892578125" Y="1.397542114258" />
                  <Point X="28.001173828125" Y="1.386237670898" />
                  <Point X="28.003078125" Y="1.36375" />
                  <Point X="28.004701171875" Y="1.352566894531" />
                  <Point X="28.01340625" Y="1.310382080078" />
                  <Point X="28.02237109375" Y="1.266934082031" />
                  <Point X="28.024599609375" Y="1.258235107422" />
                  <Point X="28.034681640625" Y="1.228615234375" />
                  <Point X="28.050283203125" Y="1.195376342773" />
                  <Point X="28.056916015625" Y="1.183527832031" />
                  <Point X="28.08058984375" Y="1.147543701172" />
                  <Point X="28.10497265625" Y="1.110482421875" />
                  <Point X="28.111736328125" Y="1.101425537109" />
                  <Point X="28.126291015625" Y="1.084179321289" />
                  <Point X="28.13408203125" Y="1.075989990234" />
                  <Point X="28.151326171875" Y="1.059900268555" />
                  <Point X="28.160037109375" Y="1.052693359375" />
                  <Point X="28.178248046875" Y="1.039367675781" />
                  <Point X="28.187748046875" Y="1.033249267578" />
                  <Point X="28.222056640625" Y="1.013936889648" />
                  <Point X="28.257390625" Y="0.994046508789" />
                  <Point X="28.265478515625" Y="0.989988037109" />
                  <Point X="28.2946796875" Y="0.978083496094" />
                  <Point X="28.33027734375" Y="0.968020996094" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.39005859375" Y="0.95912713623" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="29.219890625" Y="1.047493041992" />
                  <Point X="29.704703125" Y="1.111319824219" />
                  <Point X="29.7526875" Y="0.914207702637" />
                  <Point X="29.783591796875" Y="0.715719787598" />
                  <Point X="29.78387109375" Y="0.713921081543" />
                  <Point X="29.3324375" Y="0.592959228516" />
                  <Point X="28.6919921875" Y="0.421352844238" />
                  <Point X="28.68603125" Y="0.419543792725" />
                  <Point X="28.665625" Y="0.412137023926" />
                  <Point X="28.642380859375" Y="0.401618682861" />
                  <Point X="28.634005859375" Y="0.397316040039" />
                  <Point X="28.58843359375" Y="0.370974121094" />
                  <Point X="28.54149609375" Y="0.343843597412" />
                  <Point X="28.53388671875" Y="0.338947967529" />
                  <Point X="28.508775390625" Y="0.319871490479" />
                  <Point X="28.481994140625" Y="0.294350982666" />
                  <Point X="28.472796875" Y="0.284226654053" />
                  <Point X="28.445453125" Y="0.249384246826" />
                  <Point X="28.417291015625" Y="0.213498947144" />
                  <Point X="28.410853515625" Y="0.204205551147" />
                  <Point X="28.39912890625" Y="0.184923080444" />
                  <Point X="28.393841796875" Y="0.174934280396" />
                  <Point X="28.384068359375" Y="0.153469863892" />
                  <Point X="28.380005859375" Y="0.142927734375" />
                  <Point X="28.37316015625" Y="0.121430183411" />
                  <Point X="28.370376953125" Y="0.110474594116" />
                  <Point X="28.36126171875" Y="0.06288167572" />
                  <Point X="28.351875" Y="0.013864208221" />
                  <Point X="28.350603515625" Y="0.004966303349" />
                  <Point X="28.3485859375" Y="-0.026262340546" />
                  <Point X="28.35028125" Y="-0.062940391541" />
                  <Point X="28.351875" Y="-0.076424201965" />
                  <Point X="28.360990234375" Y="-0.124017120361" />
                  <Point X="28.370376953125" Y="-0.173034744263" />
                  <Point X="28.37316015625" Y="-0.183990325928" />
                  <Point X="28.380005859375" Y="-0.205487884521" />
                  <Point X="28.384068359375" Y="-0.216029998779" />
                  <Point X="28.393841796875" Y="-0.237494415283" />
                  <Point X="28.39912890625" Y="-0.247483215332" />
                  <Point X="28.410853515625" Y="-0.266765716553" />
                  <Point X="28.417291015625" Y="-0.276059234619" />
                  <Point X="28.444634765625" Y="-0.310901489258" />
                  <Point X="28.472796875" Y="-0.346787109375" />
                  <Point X="28.47871875" Y="-0.353634155273" />
                  <Point X="28.501142578125" Y="-0.375807189941" />
                  <Point X="28.5301796875" Y="-0.398725891113" />
                  <Point X="28.54149609375" Y="-0.406403594971" />
                  <Point X="28.587068359375" Y="-0.432745666504" />
                  <Point X="28.634005859375" Y="-0.459876190186" />
                  <Point X="28.63949609375" Y="-0.462814483643" />
                  <Point X="28.659158203125" Y="-0.472016296387" />
                  <Point X="28.68302734375" Y="-0.481027313232" />
                  <Point X="28.6919921875" Y="-0.483912841797" />
                  <Point X="29.351609375" Y="-0.660656799316" />
                  <Point X="29.78487890625" Y="-0.776751159668" />
                  <Point X="29.761619140625" Y="-0.931036315918" />
                  <Point X="29.727802734375" Y="-1.079219848633" />
                  <Point X="29.179517578125" Y="-1.007036743164" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042297363" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.2650390625" Y="-0.932117126465" />
                  <Point X="28.17291796875" Y="-0.952139892578" />
                  <Point X="28.157875" Y="-0.956742492676" />
                  <Point X="28.12875390625" Y="-0.9683671875" />
                  <Point X="28.11467578125" Y="-0.975389465332" />
                  <Point X="28.086849609375" Y="-0.992281616211" />
                  <Point X="28.074125" Y="-1.001530395508" />
                  <Point X="28.050375" Y="-1.022001281738" />
                  <Point X="28.039349609375" Y="-1.033223266602" />
                  <Point X="27.985287109375" Y="-1.098243896484" />
                  <Point X="27.92960546875" Y="-1.165211303711" />
                  <Point X="27.921326171875" Y="-1.176850219727" />
                  <Point X="27.906607421875" Y="-1.201231201172" />
                  <Point X="27.90016796875" Y="-1.213973388672" />
                  <Point X="27.88882421875" Y="-1.241358642578" />
                  <Point X="27.884365234375" Y="-1.254927368164" />
                  <Point X="27.877533203125" Y="-1.282578857422" />
                  <Point X="27.87516015625" Y="-1.296661376953" />
                  <Point X="27.867412109375" Y="-1.380866210938" />
                  <Point X="27.859431640625" Y="-1.467591796875" />
                  <Point X="27.859291015625" Y="-1.483315795898" />
                  <Point X="27.861607421875" Y="-1.514580444336" />
                  <Point X="27.864064453125" Y="-1.53012097168" />
                  <Point X="27.871794921875" Y="-1.561742553711" />
                  <Point X="27.87678515625" Y="-1.576666870117" />
                  <Point X="27.889158203125" Y="-1.605480712891" />
                  <Point X="27.896541015625" Y="-1.619370239258" />
                  <Point X="27.9460390625" Y="-1.696363037109" />
                  <Point X="27.997021484375" Y="-1.775660766602" />
                  <Point X="28.001740234375" Y="-1.782350463867" />
                  <Point X="28.019802734375" Y="-1.804459350586" />
                  <Point X="28.04349609375" Y="-1.828124511719" />
                  <Point X="28.052798828125" Y="-1.836277587891" />
                  <Point X="28.664927734375" Y="-2.305981445313" />
                  <Point X="29.087171875" Y="-2.629981201172" />
                  <Point X="29.045470703125" Y="-2.697457275391" />
                  <Point X="29.001275390625" Y="-2.760252929688" />
                  <Point X="28.5099609375" Y="-2.476591796875" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.66466015625" Y="-2.047112426758" />
                  <Point X="27.555021484375" Y="-2.027311889648" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.035136474609" />
                  <Point X="27.415068359375" Y="-2.044960083008" />
                  <Point X="27.40058984375" Y="-2.051108642578" />
                  <Point X="27.312154296875" Y="-2.097651367188" />
                  <Point X="27.221072265625" Y="-2.145587646484" />
                  <Point X="27.208970703125" Y="-2.153169677734" />
                  <Point X="27.1860390625" Y="-2.170062988281" />
                  <Point X="27.175208984375" Y="-2.179374267578" />
                  <Point X="27.15425" Y="-2.200333496094" />
                  <Point X="27.144939453125" Y="-2.211162109375" />
                  <Point X="27.128046875" Y="-2.234092529297" />
                  <Point X="27.12046484375" Y="-2.246194335938" />
                  <Point X="27.073921875" Y="-2.334629638672" />
                  <Point X="27.025984375" Y="-2.425712402344" />
                  <Point X="27.0198359375" Y="-2.440192871094" />
                  <Point X="27.01001171875" Y="-2.469970947266" />
                  <Point X="27.0063359375" Y="-2.485268554688" />
                  <Point X="27.00137890625" Y="-2.517441894531" />
                  <Point X="27.000279296875" Y="-2.533133300781" />
                  <Point X="27.00068359375" Y="-2.564484619141" />
                  <Point X="27.0021875" Y="-2.58014453125" />
                  <Point X="27.0214140625" Y="-2.686596191406" />
                  <Point X="27.04121484375" Y="-2.796234863281" />
                  <Point X="27.04301953125" Y="-2.804231689453" />
                  <Point X="27.051236328125" Y="-2.831534912109" />
                  <Point X="27.064068359375" Y="-2.862473876953" />
                  <Point X="27.069546875" Y="-2.873578613281" />
                  <Point X="27.462900390625" Y="-3.554888671875" />
                  <Point X="27.73589453125" Y="-4.027725341797" />
                  <Point X="27.723755859375" Y="-4.036083007813" />
                  <Point X="27.33870703125" Y="-3.534278076172" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.668310546875" Y="-2.753147949219" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.2935703125" Y="-2.657082763672" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.955197265625" Y="-2.796135009766" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883087158203" />
                  <Point X="25.83218359375" Y="-2.906836425781" />
                  <Point X="25.822935546875" Y="-2.919562255859" />
                  <Point X="25.80604296875" Y="-2.947388671875" />
                  <Point X="25.79901953125" Y="-2.961466552734" />
                  <Point X="25.78739453125" Y="-2.990587890625" />
                  <Point X="25.78279296875" Y="-3.005631347656" />
                  <Point X="25.756283203125" Y="-3.127599609375" />
                  <Point X="25.728978515625" Y="-3.253219238281" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323170166016" />
                  <Point X="25.7255546875" Y="-3.335520507812" />
                  <Point X="25.83308984375" Y="-4.152324707031" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480121337891" />
                  <Point X="25.642146484375" Y="-3.453579589844" />
                  <Point X="25.6267890625" Y="-3.423815917969" />
                  <Point X="25.62041015625" Y="-3.413209960938" />
                  <Point X="25.53975390625" Y="-3.296999267578" />
                  <Point X="25.456681640625" Y="-3.177309570312" />
                  <Point X="25.446671875" Y="-3.165172851562" />
                  <Point X="25.424787109375" Y="-3.142716552734" />
                  <Point X="25.412912109375" Y="-3.132396972656" />
                  <Point X="25.38665625" Y="-3.113153320312" />
                  <Point X="25.373240234375" Y="-3.104936279297" />
                  <Point X="25.345240234375" Y="-3.090829101562" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.205845703125" Y="-3.046202148438" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.810205078125" Y="-3.045041748047" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.667068359375" Y="-3.090830078125" />
                  <Point X="24.639068359375" Y="-3.104938232422" />
                  <Point X="24.62565625" Y="-3.113154785156" />
                  <Point X="24.599400390625" Y="-3.132398925781" />
                  <Point X="24.587525390625" Y="-3.142716552734" />
                  <Point X="24.565640625" Y="-3.165172119141" />
                  <Point X="24.555630859375" Y="-3.177310058594" />
                  <Point X="24.474974609375" Y="-3.293520507812" />
                  <Point X="24.391904296875" Y="-3.413210449219" />
                  <Point X="24.38753125" Y="-3.420131835938" />
                  <Point X="24.374025390625" Y="-3.445261230469" />
                  <Point X="24.36122265625" Y="-3.476214111328" />
                  <Point X="24.35724609375" Y="-3.487936767578" />
                  <Point X="24.154001953125" Y="-4.246455566406" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.770946629331" Y="3.820795192757" />
                  <Point X="26.94338684312" Y="4.400258793382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.723265248953" Y="3.738208468798" />
                  <Point X="26.573287933266" Y="4.543431254012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.956312011052" Y="2.758846245079" />
                  <Point X="28.93182105057" Y="2.775995000227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.675583868575" Y="3.655621744839" />
                  <Point X="26.288859801675" Y="4.626616389899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.102274209587" Y="2.540668827439" />
                  <Point X="28.841043426139" Y="2.723584591221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.627902488196" Y="3.57303502088" />
                  <Point X="26.036070099389" Y="4.687648059089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.08032082029" Y="2.440067170181" />
                  <Point X="28.750265801708" Y="2.671174182216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.580221107818" Y="3.49044829692" />
                  <Point X="25.793519157237" Y="4.741510471251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.001294652834" Y="2.379428302417" />
                  <Point X="28.659488177277" Y="2.618763773211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.53253972744" Y="3.407861572961" />
                  <Point X="25.598763158321" Y="4.761906503872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.922268485378" Y="2.318789434654" />
                  <Point X="28.568710552846" Y="2.566353364206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.484858351347" Y="3.325274846001" />
                  <Point X="25.404007233159" Y="4.782302484848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.843242317921" Y="2.25815056689" />
                  <Point X="28.477932928416" Y="2.513942955201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.437177012561" Y="3.242688092919" />
                  <Point X="25.355496111794" Y="4.700296751783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.764216150465" Y="2.197511699127" />
                  <Point X="28.387155311548" Y="2.4615325409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.389495673774" Y="3.160101339837" />
                  <Point X="25.329330576816" Y="4.602644470683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.685189983022" Y="2.136872831354" />
                  <Point X="28.296377800137" Y="2.409122052758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.341814334988" Y="3.077514586755" />
                  <Point X="25.303164431833" Y="4.504992616714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.606163815588" Y="2.076233963574" />
                  <Point X="28.205600288725" Y="2.356711564616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.294132996202" Y="2.994927833672" />
                  <Point X="25.27699828685" Y="4.407340762745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.527137648155" Y="2.015595095795" />
                  <Point X="28.114822777314" Y="2.304301076474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.246451657415" Y="2.91234108059" />
                  <Point X="25.250832141866" Y="4.309688908776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.648465498685" Y="4.731470573098" />
                  <Point X="24.583995479414" Y="4.77661296658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.71533764658" Y="1.067634914064" />
                  <Point X="29.661139862067" Y="1.105584611335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.448111480721" Y="1.954956228015" />
                  <Point X="28.024045265902" Y="2.251890588332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.198770318629" Y="2.829754327508" />
                  <Point X="25.218537050141" Y="4.216328589517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.686716824116" Y="4.588713120752" />
                  <Point X="24.442086834961" Y="4.760004883231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.749371127817" Y="0.927830828019" />
                  <Point X="29.521725081586" Y="1.087230305633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.369085313288" Y="1.894317360236" />
                  <Point X="27.933267754491" Y="2.19948010019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.151088979843" Y="2.747167574426" />
                  <Point X="25.160248475336" Y="4.141169103055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.724968635986" Y="4.445955327798" />
                  <Point X="24.300178444822" Y="4.743396621808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.770979051892" Y="0.796727210764" />
                  <Point X="29.382310301104" Y="1.068875999932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.290059145854" Y="1.833678492456" />
                  <Point X="27.842490243079" Y="2.147069612048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.103407641056" Y="2.664580821343" />
                  <Point X="25.063443052521" Y="4.092979403917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.763220447856" Y="4.303197534844" />
                  <Point X="24.158270059181" Y="4.726788357237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.740288785555" Y="0.70224318067" />
                  <Point X="29.242895520623" Y="1.05052169423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.21103297842" Y="1.773039624677" />
                  <Point X="27.751712731668" Y="2.094659123905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.05682233548" Y="2.581226617546" />
                  <Point X="24.025099801858" Y="4.704061589348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.620500917818" Y="0.670145962713" />
                  <Point X="29.103480760564" Y="1.032167374229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.136894402576" Y="1.708978428423" />
                  <Point X="27.660935220256" Y="2.042248635763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.026779491908" Y="2.486289257152" />
                  <Point X="23.906828467074" Y="4.670902483585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.500713050081" Y="0.638048744756" />
                  <Point X="28.96406600454" Y="1.013813051402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.078399213141" Y="1.633963615082" />
                  <Point X="27.557838424669" Y="1.998464203266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.012910133124" Y="2.38002710079" />
                  <Point X="23.788557132291" Y="4.637743377823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.380925182344" Y="0.6059515268" />
                  <Point X="28.824651248516" Y="0.995458728575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.032754192567" Y="1.549951016637" />
                  <Point X="27.425190081371" Y="1.975371987242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.035488438436" Y="2.248244015278" />
                  <Point X="23.670285807225" Y="4.604584265255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.261137170689" Y="0.573854409615" />
                  <Point X="28.685236492493" Y="0.977104405748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.005930020068" Y="1.452759918495" />
                  <Point X="23.552014513836" Y="4.571425130508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.141349061162" Y="0.541757360962" />
                  <Point X="28.545821736469" Y="0.958750082922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.008300028044" Y="1.335126835112" />
                  <Point X="23.533106217417" Y="4.468691276262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.021560951635" Y="0.509660312308" />
                  <Point X="28.377235513298" Y="0.960821841292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.055457515224" Y="1.186133221173" />
                  <Point X="23.530873287595" Y="4.354281204624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.901772842108" Y="0.477563263654" />
                  <Point X="23.50183498363" Y="4.258640458025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.781984732581" Y="0.445466215001" />
                  <Point X="23.461255873086" Y="4.171080671189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.664611371739" Y="0.411678341115" />
                  <Point X="23.390008797702" Y="4.104994824516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.571277636078" Y="0.361057740462" />
                  <Point X="23.289510176747" Y="4.059391130556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.490009319417" Y="0.301988842473" />
                  <Point X="23.167536868913" Y="4.02882417423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.42915975752" Y="0.228622578477" />
                  <Point X="22.725194193202" Y="4.222582264302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.776712832138" Y="-0.83091782844" />
                  <Point X="29.646339857636" Y="-0.739629688915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.381261988485" Y="0.146187371487" />
                  <Point X="22.632842986743" Y="4.171273689295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.760578966529" Y="-0.935594360052" />
                  <Point X="29.378042730803" Y="-0.667739604159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.358110268389" Y="0.046424794488" />
                  <Point X="22.540491292309" Y="4.119965455972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.737759442196" Y="-1.035589543028" />
                  <Point X="29.109745728947" Y="-0.595849606912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.350426408493" Y="-0.064168494822" />
                  <Point X="22.448139597875" Y="4.068657222648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.61282438964" Y="-1.064082663374" />
                  <Point X="28.841448740749" Y="-0.523959619229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.378106730482" Y="-0.199524050871" />
                  <Point X="22.355787903441" Y="4.017348989325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.408844869836" Y="-1.0372282519" />
                  <Point X="22.276129403416" Y="3.957152885593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.204865350032" Y="-1.010373840425" />
                  <Point X="22.197068901217" Y="3.896538059275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.000885697825" Y="-0.98351933624" />
                  <Point X="22.118008399018" Y="3.835923232957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.79690602683" Y="-0.9566648189" />
                  <Point X="22.03894808198" Y="3.775308276988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.592926355835" Y="-0.929810301561" />
                  <Point X="21.959888137054" Y="3.714693060463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.39662664605" Y="-0.908333350953" />
                  <Point X="22.025604573845" Y="3.552704330106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.264983270539" Y="-0.932129252997" />
                  <Point X="22.137999386714" Y="3.358031048947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.14299193336" Y="-0.962683585041" />
                  <Point X="22.234371202685" Y="3.174577191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.055621128913" Y="-1.01747947508" />
                  <Point X="22.25098000048" Y="3.046973999651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.992664920492" Y="-1.089370649298" />
                  <Point X="22.236857612514" Y="2.94088901623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.931718974583" Y="-1.162669424482" />
                  <Point X="22.187151343178" Y="2.859720134783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.88688761206" Y="-1.247251752428" />
                  <Point X="22.119184612247" Y="2.791337366197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.870115907185" Y="-1.351481664177" />
                  <Point X="22.031167754194" Y="2.736993847762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.860090191926" Y="-1.46043516871" />
                  <Point X="21.8780079058" Y="2.728263942226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.883804784128" Y="-1.593013890868" />
                  <Point X="21.277279437608" Y="3.032924958139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.019334850924" Y="-1.803886651224" />
                  <Point X="21.215477416606" Y="2.960225613189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.07095884055" Y="-2.656215282055" />
                  <Point X="21.157069107027" Y="2.885149965918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.018651932859" Y="-2.735563176922" />
                  <Point X="21.098660935838" Y="2.810074221745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.357297403536" Y="-2.388451335993" />
                  <Point X="21.040252764649" Y="2.734998477573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.717926118899" Y="-2.056732328708" />
                  <Point X="20.984658123246" Y="2.657952678634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.507597154655" Y="-2.025431988373" />
                  <Point X="20.936604743345" Y="2.575626431546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.388058543644" Y="-2.057703737768" />
                  <Point X="20.888551363443" Y="2.493300184457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.293501928498" Y="-2.107468068987" />
                  <Point X="20.840497983541" Y="2.410973937369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.201257805812" Y="-2.158851624859" />
                  <Point X="20.792445373849" Y="2.328647150973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.133164484821" Y="-2.227145754132" />
                  <Point X="21.509778575399" Y="1.710391449908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.086609870778" Y="-2.310521448373" />
                  <Point X="21.577508785174" Y="1.546992660526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.042008977108" Y="-2.395265152347" />
                  <Point X="21.564437779541" Y="1.44017149127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.006198028971" Y="-2.486163642443" />
                  <Point X="21.526078690933" Y="1.35105722834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.006154115197" Y="-2.60210647962" />
                  <Point X="21.465873167239" Y="1.27724000394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.03013228554" Y="-2.734869761179" />
                  <Point X="21.369689966297" Y="1.228614620356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.074259901922" Y="-2.881741836745" />
                  <Point X="21.238237366161" Y="1.204685135956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.186653983051" Y="-3.076414605534" />
                  <Point X="21.036413635878" Y="1.230030047358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.29904806418" Y="-3.271087374323" />
                  <Point X="26.948303684706" Y="-3.02549351583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.407222271652" Y="-2.646624231625" />
                  <Point X="20.832434048" Y="1.256884506499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.411442145309" Y="-3.465760143112" />
                  <Point X="27.140624840082" Y="-3.276131824514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.260833428837" Y="-2.660095246309" />
                  <Point X="20.628454453196" Y="1.283738970489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.523837154496" Y="-3.660433561734" />
                  <Point X="27.332945995458" Y="-3.526770133198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.124058044831" Y="-2.680297677319" />
                  <Point X="20.424474845" Y="1.310593443857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.636232947396" Y="-3.855107529118" />
                  <Point X="27.525268332378" Y="-3.777409269208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.032303043317" Y="-2.732023719522" />
                  <Point X="20.347518183068" Y="1.248505492725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.726432915507" Y="-4.034239812668" />
                  <Point X="27.717590705784" Y="-4.028048430766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.956586427735" Y="-2.794979960457" />
                  <Point X="20.321103656115" Y="1.151027557683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.880868979944" Y="-2.857935618671" />
                  <Point X="21.680309939297" Y="0.083327486284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.412531595384" Y="0.270827901262" />
                  <Point X="20.294689124312" Y="1.053549626037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.81706473498" Y="-2.92923299131" />
                  <Point X="21.705081923524" Y="-0.04999162974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.14423471707" Y="0.342717812003" />
                  <Point X="20.268436398494" Y="0.955958396622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.779854349238" Y="-3.019151584646" />
                  <Point X="21.679463002551" Y="-0.148026654086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.875937838757" Y="0.414607722744" />
                  <Point X="20.252886857703" Y="0.850872716367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.757977031458" Y="-3.119806507754" />
                  <Point X="21.628179679266" Y="-0.22809127047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.607640915743" Y="0.486497664784" />
                  <Point X="20.237337316912" Y="0.745787036112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.736098880714" Y="-3.220460847613" />
                  <Point X="21.547442185403" Y="-0.287531854583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.339343946054" Y="0.558387639507" />
                  <Point X="20.221787487736" Y="0.640701557786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.725108542483" Y="-3.328738915868" />
                  <Point X="21.437680280195" Y="-0.326649327081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.741435275493" Y="-3.456144603328" />
                  <Point X="25.563959081143" Y="-3.331874434191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.116275693471" Y="-3.018403151412" />
                  <Point X="21.317892267993" Y="-0.358746443882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.758254042013" Y="-3.583894816362" />
                  <Point X="25.662887465178" Y="-3.517118420368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.933875778515" Y="-3.006658941924" />
                  <Point X="21.19810425579" Y="-0.390843560683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.775072808534" Y="-3.711645029395" />
                  <Point X="25.701139130825" Y="-3.659876110935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.819115269713" Y="-3.042276354504" />
                  <Point X="21.078316243588" Y="-0.422940677484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.791891575054" Y="-3.839395242428" />
                  <Point X="25.739390796471" Y="-3.802633801502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.704354705644" Y="-3.077893728386" />
                  <Point X="20.958528231385" Y="-0.455037794285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.808710341575" Y="-3.967145455462" />
                  <Point X="25.777642462117" Y="-3.94539149207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.607788472828" Y="-3.126250910164" />
                  <Point X="22.050193164737" Y="-1.33540339575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.720833279954" Y="-1.104783121641" />
                  <Point X="20.838740219182" Y="-0.487134911086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.825529108095" Y="-4.094895668495" />
                  <Point X="25.815894127764" Y="-4.088149182637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.542369284663" Y="-3.1964174874" />
                  <Point X="22.038270867783" Y="-1.443028899482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.581418499352" Y="-1.123137427257" />
                  <Point X="20.71895217687" Y="-0.519232006803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.48820181296" Y="-3.27446260132" />
                  <Point X="21.994426818784" Y="-1.528302551799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.442003718749" Y="-1.141491732874" />
                  <Point X="20.599164078418" Y="-0.551329063212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.434034636651" Y="-3.352507922077" />
                  <Point X="21.924211141172" Y="-1.595110590967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.302588938147" Y="-1.15984603849" />
                  <Point X="20.479375979966" Y="-0.583426119621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.381366213873" Y="-3.431602681355" />
                  <Point X="21.845185063302" Y="-1.65574952146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.163174157544" Y="-1.178200344107" />
                  <Point X="20.359587881515" Y="-0.615523176029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.347600819746" Y="-3.523933483789" />
                  <Point X="21.766158985433" Y="-1.716388451953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.023759376942" Y="-1.196554649724" />
                  <Point X="20.239799783063" Y="-0.647620232438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.321435058544" Y="-3.621585606485" />
                  <Point X="22.607271435519" Y="-2.421315315918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.542570265819" Y="-2.376011069163" />
                  <Point X="21.687132907563" Y="-1.777027382446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.884344614469" Y="-1.214908968035" />
                  <Point X="20.227940580524" Y="-0.755289915355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.295269297342" Y="-3.71923772918" />
                  <Point X="22.689165843888" Y="-2.594631983927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.356147069724" Y="-2.361449727892" />
                  <Point X="21.608106829693" Y="-1.837666312938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.744929889475" Y="-1.233263312589" />
                  <Point X="20.246372949424" Y="-0.884169984939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.26910353614" Y="-3.816889851876" />
                  <Point X="22.670002929634" Y="-2.697187552845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.259692934018" Y="-2.409885400911" />
                  <Point X="21.529080751823" Y="-1.898305243431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.60551516448" Y="-1.251617657143" />
                  <Point X="20.270136068016" Y="-1.016782685641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.242937774938" Y="-3.914541974572" />
                  <Point X="22.625209857346" Y="-2.781796691901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.168915293532" Y="-2.462295798675" />
                  <Point X="21.450054673954" Y="-1.958944173924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.466100439486" Y="-1.269972001697" />
                  <Point X="20.306212272031" Y="-1.158017101574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.216772013736" Y="-4.012194097267" />
                  <Point X="22.577528373834" Y="-2.864383343646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.078137653047" Y="-2.514706196439" />
                  <Point X="21.371028596084" Y="-2.019583104417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.190606252533" Y="-4.109846219963" />
                  <Point X="22.529846890323" Y="-2.94696999539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.987360012561" Y="-2.567116594202" />
                  <Point X="21.292002513097" Y="-2.080222031326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.164440491331" Y="-4.207498342658" />
                  <Point X="22.482165406811" Y="-3.029556647135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.896582372076" Y="-2.619526991966" />
                  <Point X="21.21297641303" Y="-2.140860946276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.138274500806" Y="-4.30515030478" />
                  <Point X="23.54547992306" Y="-3.890071072832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.443006749809" Y="-3.818318584458" />
                  <Point X="22.4344839233" Y="-3.11214329888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.805804731591" Y="-2.671937389729" />
                  <Point X="21.133950312963" Y="-2.201499861226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.112108358074" Y="-4.402802160326" />
                  <Point X="23.816636634705" Y="-4.195910632295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.233619303393" Y="-3.787677502003" />
                  <Point X="22.386802439788" Y="-3.194729950624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.715027091105" Y="-2.724347787493" />
                  <Point X="21.054924212896" Y="-2.262138776177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.085942215342" Y="-4.500454015871" />
                  <Point X="23.843439065167" Y="-4.330651482079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.051196755221" Y="-3.775917444566" />
                  <Point X="22.339120956277" Y="-3.277316602369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.624249492147" Y="-2.776758214334" />
                  <Point X="20.975898112829" Y="-2.322777691127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.059776072609" Y="-4.598105871416" />
                  <Point X="23.870241610795" Y="-4.465392412504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.926414894423" Y="-3.804517830936" />
                  <Point X="22.291439473664" Y="-3.359903254743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.533471959802" Y="-2.829168687818" />
                  <Point X="20.896872012762" Y="-2.383416606077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.033609929877" Y="-4.695757726962" />
                  <Point X="23.877801604931" Y="-4.586659563319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.839246244837" Y="-3.859455271333" />
                  <Point X="22.243758115581" Y="-3.442489994313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.442694427456" Y="-2.881579161302" />
                  <Point X="20.820069392356" Y="-2.445612418247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.952143806293" Y="-4.754688119052" />
                  <Point X="23.863820925314" Y="-4.692843771994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.754494504673" Y="-3.916085049926" />
                  <Point X="22.196076757497" Y="-3.525076733883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.35191689511" Y="-2.933989634786" />
                  <Point X="20.938816899489" Y="-2.644733903817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.669742850773" Y="-3.972714888922" />
                  <Point X="22.148395399414" Y="-3.607663473453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.261139362765" Y="-2.98640010827" />
                  <Point X="21.094003792403" Y="-2.869370521999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.594455462465" Y="-4.035971678029" />
                  <Point X="22.10071404133" Y="-3.690250213024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.536186544441" Y="-4.111144928317" />
                  <Point X="22.053032683247" Y="-3.772836952594" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.9998359375" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.668876953125" Y="-4.273583007813" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521543945312" />
                  <Point X="25.3836640625" Y="-3.405333251953" />
                  <Point X="25.300591796875" Y="-3.285643554688" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.149525390625" Y="-3.227663085938" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.866525390625" Y="-3.226503173828" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285644042969" />
                  <Point X="24.631064453125" Y="-3.401854492188" />
                  <Point X="24.547994140625" Y="-3.521544433594" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.337529296875" Y="-4.295631347656" />
                  <Point X="24.152255859375" Y="-4.987077148438" />
                  <Point X="24.050359375" Y="-4.967298339844" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.755546875" Y="-4.900961425781" />
                  <Point X="23.648412109375" Y="-4.873396484375" />
                  <Point X="23.667095703125" Y="-4.731473632812" />
                  <Point X="23.6908515625" Y="-4.5510390625" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.6605" Y="-4.38485546875" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.498806640625" Y="-4.101854980469" />
                  <Point X="23.38045703125" Y="-3.998064208984" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.198248046875" Y="-3.975766845703" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.883041015625" Y="-4.058703857422" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.62616015625" Y="-4.306" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.364921875" Y="-4.304297363281" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.944478515625" Y="-4.013858154297" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.12723828125" Y="-3.264311279297" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597592529297" />
                  <Point X="22.48601953125" Y="-2.568763183594" />
                  <Point X="22.46867578125" Y="-2.551418701172" />
                  <Point X="22.43984765625" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.754109375" Y="-2.921176757812" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.012705078125" Y="-3.076266357422" />
                  <Point X="20.838302734375" Y="-2.847135742188" />
                  <Point X="20.69513671875" Y="-2.607071533203" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="21.19455859375" Y="-1.91550402832" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396018066406" />
                  <Point X="21.861884765625" Y="-1.366263671875" />
                  <Point X="21.859673828125" Y="-1.33459362793" />
                  <Point X="21.83883984375" Y="-1.310638305664" />
                  <Point X="21.812357421875" Y="-1.295051879883" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.95459765625" Y="-1.397299560547" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.1408203125" Y="-1.278240600586" />
                  <Point X="20.072607421875" Y="-1.011187744141" />
                  <Point X="20.03473046875" Y="-0.746362304688" />
                  <Point X="20.001603515625" Y="-0.51474230957" />
                  <Point X="20.711595703125" Y="-0.324500701904" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.471775390625" Y="-0.111936950684" />
                  <Point X="21.497677734375" Y="-0.090645477295" />
                  <Point X="21.50966015625" Y="-0.061223060608" />
                  <Point X="21.516599609375" Y="-0.031280040741" />
                  <Point X="21.509796875" Y="-0.001776633501" />
                  <Point X="21.497677734375" Y="0.028085395813" />
                  <Point X="21.472185546875" Y="0.049092720032" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.689712890625" Y="0.267803955078" />
                  <Point X="20.001814453125" Y="0.45212600708" />
                  <Point X="20.03839453125" Y="0.699329650879" />
                  <Point X="20.08235546875" Y="0.996414489746" />
                  <Point X="20.158599609375" Y="1.277784545898" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.722779296875" Y="1.462960449219" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.298556640625" Y="1.405407592773" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056030273" />
                  <Point X="21.360880859375" Y="1.443785400391" />
                  <Point X="21.373021484375" Y="1.473098632812" />
                  <Point X="21.38552734375" Y="1.503289672852" />
                  <Point X="21.389287109375" Y="1.52460546875" />
                  <Point X="21.38368359375" Y="1.545512573242" />
                  <Point X="21.369033203125" Y="1.57365625" />
                  <Point X="21.353943359375" Y="1.602642211914" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.9082109375" Y="1.9505703125" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.669166015625" Y="2.494351806641" />
                  <Point X="20.83998828125" Y="2.7870078125" />
                  <Point X="21.041955078125" Y="3.046609375" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.519044921875" Y="3.112734619141" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.903630859375" Y="2.916747802734" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775146484" />
                  <Point X="21.986748046875" Y="2.927404052734" />
                  <Point X="22.016662109375" Y="2.957318115234" />
                  <Point X="22.04747265625" Y="2.988127441406" />
                  <Point X="22.0591015625" Y="3.006381591797" />
                  <Point X="22.061927734375" Y="3.027840576172" />
                  <Point X="22.058240234375" Y="3.069984619141" />
                  <Point X="22.054443359375" Y="3.113390136719" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.856580078125" Y="3.465466552734" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.949611328125" Y="3.946231689453" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.565220703125" Y="4.351058105469" />
                  <Point X="22.858451171875" Y="4.513971679688" />
                  <Point X="22.93366015625" Y="4.41595703125" />
                  <Point X="23.032173828125" Y="4.287572753906" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.09566015625" Y="4.249242675781" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.218491699219" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.235048828125" Y="4.242487304688" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.303095703125" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.294487304688" />
                  <Point X="23.329818359375" Y="4.344920898438" />
                  <Point X="23.346197265625" Y="4.396863769531" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.326328125" Y="4.583669921875" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.646759765625" Y="4.795314453125" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.41753515625" Y="4.948428222656" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.862662109375" Y="4.666184570312" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.152501953125" Y="4.676820800781" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.523916015625" Y="4.960784179688" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.17924609375" Y="4.848539550781" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.716341796875" Y="4.693657226562" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.1318359375" Y="4.521877929688" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.53268359375" Y="4.312118652344" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="27.9154765625" Y="4.065586425781" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.654314453125" Y="3.238784667969" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491516113281" />
                  <Point X="27.21427734375" Y="2.451965087891" />
                  <Point X="27.2033828125" Y="2.411229736328" />
                  <Point X="27.202044921875" Y="2.392327636719" />
                  <Point X="27.206169921875" Y="2.358127197266" />
                  <Point X="27.210416015625" Y="2.322902832031" />
                  <Point X="27.21868359375" Y="2.300811279297" />
                  <Point X="27.239845703125" Y="2.269623779297" />
                  <Point X="27.261642578125" Y="2.237502685547" />
                  <Point X="27.27494140625" Y="2.224203613281" />
                  <Point X="27.30612890625" Y="2.203041503906" />
                  <Point X="27.33825" Y="2.18124609375" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.394537109375" Y="2.168856201172" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946044922" />
                  <Point X="27.48821484375" Y="2.176522705078" />
                  <Point X="27.528951171875" Y="2.187415771484" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.29823046875" Y="2.629584960938" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.0840546875" Y="2.906621826172" />
                  <Point X="29.202595703125" Y="2.741876220703" />
                  <Point X="29.3045859375" Y="2.573336425781" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.847458984375" Y="2.021897094727" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583832763672" />
                  <Point X="28.25090625" Y="1.546697998047" />
                  <Point X="28.22158984375" Y="1.508451416016" />
                  <Point X="28.21312109375" Y="1.491501464844" />
                  <Point X="28.202517578125" Y="1.453586791992" />
                  <Point X="28.191595703125" Y="1.414536987305" />
                  <Point X="28.190779296875" Y="1.390965332031" />
                  <Point X="28.199484375" Y="1.348780517578" />
                  <Point X="28.20844921875" Y="1.305332519531" />
                  <Point X="28.215646484375" Y="1.287955200195" />
                  <Point X="28.2393203125" Y="1.251971191406" />
                  <Point X="28.263703125" Y="1.214909912109" />
                  <Point X="28.280947265625" Y="1.19882019043" />
                  <Point X="28.315255859375" Y="1.17950793457" />
                  <Point X="28.35058984375" Y="1.159617675781" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.414953125" Y="1.147489135742" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.19508984375" Y="1.235867553711" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.88827734375" Y="1.160514404297" />
                  <Point X="29.93919140625" Y="0.951367736816" />
                  <Point X="29.971330078125" Y="0.744950073242" />
                  <Point X="29.997859375" Y="0.574556335449" />
                  <Point X="29.38161328125" Y="0.40943347168" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819503784" />
                  <Point X="28.683515625" Y="0.206477523804" />
                  <Point X="28.636578125" Y="0.179346984863" />
                  <Point X="28.622265625" Y="0.166926544189" />
                  <Point X="28.594921875" Y="0.132084152222" />
                  <Point X="28.566759765625" Y="0.096198867798" />
                  <Point X="28.556986328125" Y="0.07473449707" />
                  <Point X="28.54787109375" Y="0.027141580582" />
                  <Point X="28.538484375" Y="-0.021876060486" />
                  <Point X="28.538484375" Y="-0.040684017181" />
                  <Point X="28.547599609375" Y="-0.088276939392" />
                  <Point X="28.556986328125" Y="-0.137294570923" />
                  <Point X="28.566759765625" Y="-0.15875894165" />
                  <Point X="28.594103515625" Y="-0.19360118103" />
                  <Point X="28.622265625" Y="-0.229486618042" />
                  <Point X="28.636578125" Y="-0.241907058716" />
                  <Point X="28.682150390625" Y="-0.268249023438" />
                  <Point X="28.729087890625" Y="-0.295379577637" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.40078515625" Y="-0.477130981445" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.976859375" Y="-0.777851013184" />
                  <Point X="29.948431640625" Y="-0.966413391113" />
                  <Point X="29.907255859375" Y="-1.146846069336" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.154716796875" Y="-1.195411254883" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.30539453125" Y="-1.117782104492" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.185447265625" Y="-1.154697143555" />
                  <Point X="28.131384765625" Y="-1.219717895508" />
                  <Point X="28.075703125" Y="-1.286685180664" />
                  <Point X="28.064359375" Y="-1.314070556641" />
                  <Point X="28.056611328125" Y="-1.398275268555" />
                  <Point X="28.048630859375" Y="-1.485000854492" />
                  <Point X="28.056361328125" Y="-1.516622436523" />
                  <Point X="28.105859375" Y="-1.593615234375" />
                  <Point X="28.156841796875" Y="-1.672912963867" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.780591796875" Y="-2.155244140625" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.284265625" Y="-2.672474853516" />
                  <Point X="29.2041328125" Y="-2.802140380859" />
                  <Point X="29.118974609375" Y="-2.923139404297" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.4149609375" Y="-2.64113671875" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.630890625" Y="-2.234087646484" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.400642578125" Y="-2.265787597656" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334683105469" />
                  <Point X="27.24205859375" Y="-2.423118408203" />
                  <Point X="27.19412109375" Y="-2.514201171875" />
                  <Point X="27.1891640625" Y="-2.546374511719" />
                  <Point X="27.208390625" Y="-2.652826171875" />
                  <Point X="27.22819140625" Y="-2.76246484375" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.627447265625" Y="-3.459888671875" />
                  <Point X="27.98667578125" Y="-4.082088867188" />
                  <Point X="27.930384765625" Y="-4.122295898438" />
                  <Point X="27.835314453125" Y="-4.190202148438" />
                  <Point X="27.7400859375" Y="-4.251841308594" />
                  <Point X="27.679775390625" Y="-4.29087890625" />
                  <Point X="27.18796875" Y="-3.649942626953" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.565560546875" Y="-2.912968261719" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.31098046875" Y="-2.846283447266" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.076669921875" Y="-2.942230712891" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045985839844" />
                  <Point X="25.941947265625" Y="-3.167954101562" />
                  <Point X="25.914642578125" Y="-3.293573730469" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="26.025404296875" Y="-4.157446777344" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.084302734375" Y="-4.943528808594" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.906388671875" Y="-4.979225585938" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#166" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.095197293765" Y="4.710405628776" Z="1.25" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.25" />
                  <Point X="-0.594435095944" Y="5.029369539894" Z="1.25" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.25" />
                  <Point X="-1.372896139994" Y="4.874737952206" Z="1.25" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.25" />
                  <Point X="-1.729400608057" Y="4.608424193418" Z="1.25" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.25" />
                  <Point X="-1.724023547081" Y="4.391237405672" Z="1.25" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.25" />
                  <Point X="-1.790243407028" Y="4.319961418715" Z="1.25" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.25" />
                  <Point X="-1.887409251817" Y="4.324873457334" Z="1.25" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.25" />
                  <Point X="-2.032827859162" Y="4.477675574947" Z="1.25" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.25" />
                  <Point X="-2.465219988803" Y="4.426045725093" Z="1.25" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.25" />
                  <Point X="-3.086966549432" Y="4.017191706924" Z="1.25" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.25" />
                  <Point X="-3.192878041622" Y="3.471746623644" Z="1.25" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.25" />
                  <Point X="-2.997727157535" Y="3.096907391148" Z="1.25" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.25" />
                  <Point X="-3.024849559858" Y="3.023954008028" Z="1.25" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.25" />
                  <Point X="-3.098169074459" Y="2.997837541638" Z="1.25" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.25" />
                  <Point X="-3.462112724653" Y="3.187315907124" Z="1.25" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.25" />
                  <Point X="-4.003664738454" Y="3.108591781226" Z="1.25" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.25" />
                  <Point X="-4.380171810196" Y="2.550837168573" Z="1.25" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.25" />
                  <Point X="-4.128384179555" Y="1.942182674226" Z="1.25" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.25" />
                  <Point X="-3.68147305988" Y="1.581847976161" Z="1.25" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.25" />
                  <Point X="-3.679327921298" Y="1.523513460724" Z="1.25" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.25" />
                  <Point X="-3.722635946158" Y="1.484373505948" Z="1.25" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.25" />
                  <Point X="-4.276852988936" Y="1.543812792576" Z="1.25" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.25" />
                  <Point X="-4.895816148582" Y="1.32214210658" Z="1.25" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.25" />
                  <Point X="-5.01722411104" Y="0.737928594605" Z="1.25" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.25" />
                  <Point X="-4.32938519961" Y="0.250787742322" Z="1.25" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.25" />
                  <Point X="-3.56247964762" Y="0.039295952821" Z="1.25" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.25" />
                  <Point X="-3.544114082748" Y="0.014683678824" Z="1.25" />
                  <Point X="-3.539556741714" Y="0" Z="1.25" />
                  <Point X="-3.544250518387" Y="-0.015123272237" Z="1.25" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.25" />
                  <Point X="-3.562888947503" Y="-0.039580030313" Z="1.25" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.25" />
                  <Point X="-4.307502589162" Y="-0.244924314434" Z="1.25" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.25" />
                  <Point X="-5.020921683795" Y="-0.722161210333" Z="1.25" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.25" />
                  <Point X="-4.913781440533" Y="-1.259334555074" Z="1.25" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.25" />
                  <Point X="-4.045034107071" Y="-1.4155918224" Z="1.25" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.25" />
                  <Point X="-3.205722314035" Y="-1.3147714906" Z="1.25" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.25" />
                  <Point X="-3.196585366983" Y="-1.337542844137" Z="1.25" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.25" />
                  <Point X="-3.842035909416" Y="-1.84455643025" Z="1.25" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.25" />
                  <Point X="-4.353963130338" Y="-2.601401124402" Z="1.25" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.25" />
                  <Point X="-4.033237244485" Y="-3.075269401662" Z="1.25" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.25" />
                  <Point X="-3.22704723309" Y="-2.933197977026" Z="1.25" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.25" />
                  <Point X="-2.564037306376" Y="-2.564293162756" Z="1.25" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.25" />
                  <Point X="-2.922219036142" Y="-3.208031149402" Z="1.25" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.25" />
                  <Point X="-3.092181485416" Y="-4.022195273636" Z="1.25" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.25" />
                  <Point X="-2.667556804991" Y="-4.315527882938" Z="1.25" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.25" />
                  <Point X="-2.340328396464" Y="-4.305158117465" Z="1.25" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.25" />
                  <Point X="-2.095336891191" Y="-4.068997231673" Z="1.25" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.25" />
                  <Point X="-1.811178509959" Y="-3.99437978136" Z="1.25" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.25" />
                  <Point X="-1.540316346542" Y="-4.10816668115" Z="1.25" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.25" />
                  <Point X="-1.394695927337" Y="-4.363330218285" Z="1.25" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.25" />
                  <Point X="-1.388633214414" Y="-4.693666855076" Z="1.25" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.25" />
                  <Point X="-1.263069970698" Y="-4.918104428187" Z="1.25" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.25" />
                  <Point X="-0.96532472008" Y="-4.985102276866" Z="1.25" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.25" />
                  <Point X="-0.620331320056" Y="-4.277292082717" Z="1.25" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.25" />
                  <Point X="-0.334015600283" Y="-3.399083346348" Z="1.25" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.25" />
                  <Point X="-0.124811267712" Y="-3.242976179971" Z="1.25" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.25" />
                  <Point X="0.128547811649" Y="-3.244135865317" Z="1.25" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.25" />
                  <Point X="0.336430259149" Y="-3.40256240942" Z="1.25" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.25" />
                  <Point X="0.614423540345" Y="-4.255243947098" Z="1.25" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.25" />
                  <Point X="0.909168963426" Y="-4.997140579883" Z="1.25" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.25" />
                  <Point X="1.088851829843" Y="-4.961088935014" Z="1.25" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.25" />
                  <Point X="1.068819501868" Y="-4.119640546499" Z="1.25" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.25" />
                  <Point X="0.984649726484" Y="-3.147294140346" Z="1.25" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.25" />
                  <Point X="1.102479085169" Y="-2.949397202119" Z="1.25" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.25" />
                  <Point X="1.30940588706" Y="-2.864792850515" Z="1.25" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.25" />
                  <Point X="1.532363750757" Y="-2.923746306385" Z="1.25" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.25" />
                  <Point X="2.142144190927" Y="-3.649100654432" Z="1.25" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.25" />
                  <Point X="2.761099729628" Y="-4.262535483382" Z="1.25" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.25" />
                  <Point X="2.953288541354" Y="-4.131702752042" Z="1.25" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.25" />
                  <Point X="2.664591639682" Y="-3.403608737561" Z="1.25" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.25" />
                  <Point X="2.251436228292" Y="-2.61265994904" Z="1.25" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.25" />
                  <Point X="2.280147344539" Y="-2.415125422069" Z="1.25" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.25" />
                  <Point X="2.417772956303" Y="-2.278753965428" Z="1.25" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.25" />
                  <Point X="2.615846854472" Y="-2.252011781199" Z="1.25" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.25" />
                  <Point X="3.383804859546" Y="-2.653157860593" Z="1.25" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.25" />
                  <Point X="4.153706168953" Y="-2.920636719824" Z="1.25" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.25" />
                  <Point X="4.320641452052" Y="-2.667480256054" Z="1.25" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.25" />
                  <Point X="3.80487217343" Y="-2.084296461447" Z="1.25" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.25" />
                  <Point X="3.141761722815" Y="-1.53529527576" Z="1.25" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.25" />
                  <Point X="3.100242967148" Y="-1.371576923461" Z="1.25" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.25" />
                  <Point X="3.163672746669" Y="-1.220404926234" Z="1.25" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.25" />
                  <Point X="3.309856551368" Y="-1.135361297611" Z="1.25" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.25" />
                  <Point X="4.142035457148" Y="-1.213703413492" Z="1.25" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.25" />
                  <Point X="4.949845164377" Y="-1.126689966942" Z="1.25" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.25" />
                  <Point X="5.020143950506" Y="-0.754024857348" Z="1.25" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.25" />
                  <Point X="4.407570579788" Y="-0.397554707055" Z="1.25" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.25" />
                  <Point X="3.701015537774" Y="-0.193680115979" Z="1.25" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.25" />
                  <Point X="3.627280417559" Y="-0.131452822812" Z="1.25" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.25" />
                  <Point X="3.590549291332" Y="-0.047592858761" Z="1.25" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.25" />
                  <Point X="3.590822159093" Y="0.049017672451" Z="1.25" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.25" />
                  <Point X="3.628099020842" Y="0.132495915763" Z="1.25" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.25" />
                  <Point X="3.702379876578" Y="0.194468728263" Z="1.25" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.25" />
                  <Point X="4.388397237518" Y="0.392417225714" Z="1.25" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.25" />
                  <Point X="5.014578037466" Y="0.783922203594" Z="1.25" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.25" />
                  <Point X="4.930701985644" Y="1.203621096212" Z="1.25" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.25" />
                  <Point X="4.182407306272" Y="1.316719896459" Z="1.25" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.25" />
                  <Point X="3.415346674088" Y="1.228338097034" Z="1.25" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.25" />
                  <Point X="3.333626044311" Y="1.254358884352" Z="1.25" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.25" />
                  <Point X="3.27493539712" Y="1.31073235467" Z="1.25" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.25" />
                  <Point X="3.242296223549" Y="1.39016426148" Z="1.25" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.25" />
                  <Point X="3.244512727434" Y="1.471398990648" Z="1.25" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.25" />
                  <Point X="3.284433158676" Y="1.547560264744" Z="1.25" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.25" />
                  <Point X="3.87173984237" Y="2.013509391663" Z="1.25" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.25" />
                  <Point X="4.341205815808" Y="2.630502718328" Z="1.25" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.25" />
                  <Point X="4.118482871714" Y="2.967104773675" Z="1.25" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.25" />
                  <Point X="3.267074387473" Y="2.704166327154" Z="1.25" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.25" />
                  <Point X="2.469142756356" Y="2.25610565437" Z="1.25" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.25" />
                  <Point X="2.394367282362" Y="2.249776581807" Z="1.25" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.25" />
                  <Point X="2.328045591192" Y="2.275696069408" Z="1.25" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.25" />
                  <Point X="2.275062530827" Y="2.328979269191" Z="1.25" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.25" />
                  <Point X="2.249653120951" Y="2.39539116147" Z="1.25" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.25" />
                  <Point X="2.256422288287" Y="2.470326819526" Z="1.25" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.25" />
                  <Point X="2.691459025705" Y="3.245064694482" Z="1.25" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.25" />
                  <Point X="2.938296009482" Y="4.137613752051" Z="1.25" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.25" />
                  <Point X="2.551697377451" Y="4.386601417652" Z="1.25" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.25" />
                  <Point X="2.146860867946" Y="4.598449812144" Z="1.25" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.25" />
                  <Point X="1.727233542421" Y="4.771940572235" Z="1.25" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.25" />
                  <Point X="1.184824200791" Y="4.928423134707" Z="1.25" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.25" />
                  <Point X="0.522966084445" Y="5.041792382826" Z="1.25" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.25" />
                  <Point X="0.098047267788" Y="4.721041877175" Z="1.25" />
                  <Point X="0" Y="4.355124473572" Z="1.25" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>