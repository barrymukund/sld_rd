<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#209" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3352" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004715820312" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.926236328125" Y="-4.867004394531" />
                  <Point X="25.5633046875" Y="-3.512525878906" />
                  <Point X="25.55772265625" Y="-3.497141357422" />
                  <Point X="25.542365234375" Y="-3.467376464844" />
                  <Point X="25.394025390625" Y="-3.253647705078" />
                  <Point X="25.37863671875" Y="-3.231476074219" />
                  <Point X="25.35675" Y="-3.209019775391" />
                  <Point X="25.330494140625" Y="-3.189776367188" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.07294921875" Y="-3.104426269531" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766357422" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.733630859375" Y="-3.168278564453" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.189776611328" />
                  <Point X="24.655560546875" Y="-3.209020507813" />
                  <Point X="24.63367578125" Y="-3.231476806641" />
                  <Point X="24.485337890625" Y="-3.445205322266" />
                  <Point X="24.46994921875" Y="-3.467377197266" />
                  <Point X="24.4618125" Y="-3.481572265625" />
                  <Point X="24.449009765625" Y="-3.512524169922" />
                  <Point X="24.411359375" Y="-3.653035400391" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.9445546875" Y="-4.84998828125" />
                  <Point X="23.92066796875" Y="-4.8453515625" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.51622265625" />
                  <Point X="23.72865234375" Y="-4.240530761719" />
                  <Point X="23.722962890625" Y="-4.211931152344" />
                  <Point X="23.712060546875" Y="-4.182963867188" />
                  <Point X="23.69598828125" Y="-4.155126953125" />
                  <Point X="23.67635546875" Y="-4.131203613281" />
                  <Point X="23.46501953125" Y="-3.945865966797" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.07648046875" Y="-3.872581787109" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.016580078125" Y="-3.873709228516" />
                  <Point X="22.98553125" Y="-3.882029785156" />
                  <Point X="22.957341796875" Y="-3.894802490234" />
                  <Point X="22.72362109375" Y="-4.050969970703" />
                  <Point X="22.699376953125" Y="-4.067170654297" />
                  <Point X="22.687216796875" Y="-4.076821044922" />
                  <Point X="22.664900390625" Y="-4.099459960938" />
                  <Point X="22.64376171875" Y="-4.127006835938" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.233310546875" Y="-4.111068847656" />
                  <Point X="22.198287109375" Y="-4.089383300781" />
                  <Point X="21.895279296875" Y="-3.856076904297" />
                  <Point X="21.901771484375" Y="-3.844830566406" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647654296875" />
                  <Point X="22.593412109375" Y="-2.616127441406" />
                  <Point X="22.59442578125" Y="-2.585193603516" />
                  <Point X="22.58544140625" Y="-2.555575927734" />
                  <Point X="22.571224609375" Y="-2.526746582031" />
                  <Point X="22.5531953125" Y="-2.501586669922" />
                  <Point X="22.5374765625" Y="-2.485868652344" />
                  <Point X="22.535865234375" Y="-2.484257324219" />
                  <Point X="22.5107421875" Y="-2.466248046875" />
                  <Point X="22.481908203125" Y="-2.452014404297" />
                  <Point X="22.45228125" Y="-2.443016601562" />
                  <Point X="22.4213359375" Y="-2.4440234375" />
                  <Point X="22.389796875" Y="-2.450292724609" />
                  <Point X="22.360818359375" Y="-2.461197021484" />
                  <Point X="22.23962890625" Y="-2.531165039062" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="20.944806640625" Y="-2.830207763672" />
                  <Point X="20.917140625" Y="-2.793860107422" />
                  <Point X="20.693857421875" Y="-2.419450439453" />
                  <Point X="20.713947265625" Y="-2.404034912109" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.915421875" Y="-1.475593139648" />
                  <Point X="21.93338671875" Y="-1.448461914063" />
                  <Point X="21.946142578125" Y="-1.419832885742" />
                  <Point X="21.953123046875" Y="-1.392881347656" />
                  <Point X="21.953841796875" Y="-1.390107666016" />
                  <Point X="21.95665234375" Y="-1.359676269531" />
                  <Point X="21.954447265625" Y="-1.328001464844" />
                  <Point X="21.947447265625" Y="-1.298251586914" />
                  <Point X="21.931365234375" Y="-1.272263549805" />
                  <Point X="21.91053125" Y="-1.248303466797" />
                  <Point X="21.88702734375" Y="-1.228765869141" />
                  <Point X="21.863033203125" Y="-1.21464453125" />
                  <Point X="21.86055078125" Y="-1.21318347168" />
                  <Point X="21.831294921875" Y="-1.201961547852" />
                  <Point X="21.79940234375" Y="-1.195476318359" />
                  <Point X="21.768072265625" Y="-1.194383666992" />
                  <Point X="21.615083984375" Y="-1.214525146484" />
                  <Point X="20.267900390625" Y="-1.391885498047" />
                  <Point X="20.176744140625" Y="-1.035020629883" />
                  <Point X="20.16592578125" Y="-0.992657043457" />
                  <Point X="20.107576171875" Y="-0.584698791504" />
                  <Point X="20.12281640625" Y="-0.58061517334" />
                  <Point X="21.467125" Y="-0.220408401489" />
                  <Point X="21.4825078125" Y="-0.214827835083" />
                  <Point X="21.5122734375" Y="-0.19946937561" />
                  <Point X="21.53741796875" Y="-0.182017730713" />
                  <Point X="21.54001953125" Y="-0.180212036133" />
                  <Point X="21.56247265625" Y="-0.15832963562" />
                  <Point X="21.581724609375" Y="-0.132064422607" />
                  <Point X="21.595833984375" Y="-0.104059860229" />
                  <Point X="21.60421484375" Y="-0.077054382324" />
                  <Point X="21.605083984375" Y="-0.074252784729" />
                  <Point X="21.609353515625" Y="-0.046095363617" />
                  <Point X="21.609353515625" Y="-0.016464664459" />
                  <Point X="21.605083984375" Y="0.011692755699" />
                  <Point X="21.596703125" Y="0.038698238373" />
                  <Point X="21.595833984375" Y="0.041499832153" />
                  <Point X="21.58173046875" Y="0.069496055603" />
                  <Point X="21.562486328125" Y="0.095756698608" />
                  <Point X="21.54002734375" Y="0.11764730072" />
                  <Point X="21.5148828125" Y="0.135098953247" />
                  <Point X="21.502982421875" Y="0.14213381958" />
                  <Point X="21.4848046875" Y="0.151164596558" />
                  <Point X="21.467125" Y="0.157848220825" />
                  <Point X="21.327669921875" Y="0.195215377808" />
                  <Point X="20.10818359375" Y="0.521975524902" />
                  <Point X="20.1685390625" Y="0.929854309082" />
                  <Point X="20.17551171875" Y="0.976967834473" />
                  <Point X="20.29644921875" Y="1.423268066406" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228149414" />
                  <Point X="21.29686328125" Y="1.305263549805" />
                  <Point X="21.352515625" Y="1.322810668945" />
                  <Point X="21.3582890625" Y="1.324630981445" />
                  <Point X="21.377224609375" Y="1.332962402344" />
                  <Point X="21.39596875" Y="1.343784667969" />
                  <Point X="21.412646484375" Y="1.356014160156" />
                  <Point X="21.426283203125" Y="1.371563354492" />
                  <Point X="21.43869921875" Y="1.389293701172" />
                  <Point X="21.44865234375" Y="1.407433105469" />
                  <Point X="21.470982421875" Y="1.461344848633" />
                  <Point X="21.473298828125" Y="1.4669375" />
                  <Point X="21.4790859375" Y="1.486796386719" />
                  <Point X="21.48284375" Y="1.508110717773" />
                  <Point X="21.484197265625" Y="1.52875" />
                  <Point X="21.48105078125" Y="1.549192871094" />
                  <Point X="21.47544921875" Y="1.570098510742" />
                  <Point X="21.467953125" Y="1.589375366211" />
                  <Point X="21.441009765625" Y="1.641135498047" />
                  <Point X="21.438197265625" Y="1.646534790039" />
                  <Point X="21.426703125" Y="1.663730102539" />
                  <Point X="21.412794921875" Y="1.680299316406" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="21.31787109375" Y="1.755970703125" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.891755859375" Y="2.687244628906" />
                  <Point X="20.9188515625" Y="2.733665527344" />
                  <Point X="21.24949609375" Y="3.158661621094" />
                  <Point X="21.793341796875" Y="2.844672119141" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.930716796875" Y="2.819015136719" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818763183594" />
                  <Point X="21.98089453125" Y="2.821588623047" />
                  <Point X="22.00098828125" Y="2.826505615234" />
                  <Point X="22.019541015625" Y="2.835655273438" />
                  <Point X="22.037794921875" Y="2.84728515625" />
                  <Point X="22.053921875" Y="2.860229492188" />
                  <Point X="22.108939453125" Y="2.915245605469" />
                  <Point X="22.114646484375" Y="2.920952880859" />
                  <Point X="22.12759375" Y="2.937083740234" />
                  <Point X="22.13922265625" Y="2.955336669922" />
                  <Point X="22.14837109375" Y="2.973886474609" />
                  <Point X="22.1532890625" Y="2.993976318359" />
                  <Point X="22.156115234375" Y="3.01543359375" />
                  <Point X="22.15656640625" Y="3.036119140625" />
                  <Point X="22.14978515625" Y="3.113627929688" />
                  <Point X="22.14908203125" Y="3.121668701172" />
                  <Point X="22.145044921875" Y="3.141958251953" />
                  <Point X="22.13853515625" Y="3.162602539062" />
                  <Point X="22.130205078125" Y="3.181533203125" />
                  <Point X="22.0947578125" Y="3.242929443359" />
                  <Point X="21.81666796875" Y="3.724595947266" />
                  <Point X="22.252197265625" Y="4.058511962891" />
                  <Point X="22.299376953125" Y="4.094685302734" />
                  <Point X="22.832962890625" Y="4.391133789062" />
                  <Point X="22.9568046875" Y="4.229740722656" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.18939453125" />
                  <Point X="23.091154296875" Y="4.144486328125" />
                  <Point X="23.100103515625" Y="4.139827636719" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.20268359375" Y="4.128692382812" />
                  <Point X="23.222544921875" Y="4.134481445312" />
                  <Point X="23.312396484375" Y="4.171700195312" />
                  <Point X="23.32171875" Y="4.175561035156" />
                  <Point X="23.33984765625" Y="4.185505859375" />
                  <Point X="23.357580078125" Y="4.197920410156" />
                  <Point X="23.37313671875" Y="4.211560546875" />
                  <Point X="23.385369140625" Y="4.228245117188" />
                  <Point X="23.39619140625" Y="4.246990722656" />
                  <Point X="23.404521484375" Y="4.265923339844" />
                  <Point X="23.433759765625" Y="4.358658203125" />
                  <Point X="23.436794921875" Y="4.368280273437" />
                  <Point X="23.440833984375" Y="4.388571289062" />
                  <Point X="23.44272265625" Y="4.410138671875" />
                  <Point X="23.442271484375" Y="4.430830078125" />
                  <Point X="23.438240234375" Y="4.461440429688" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.9892890625" Y="4.792685058594" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.705287109375" Y="4.886457519531" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.14621484375" Y="4.286313964844" />
                  <Point X="25.16437890625" Y="4.354098144531" />
                  <Point X="25.307419921875" Y="4.8879375" />
                  <Point X="25.790705078125" Y="4.837324707031" />
                  <Point X="25.84404296875" Y="4.83173828125" />
                  <Point X="26.427380859375" Y="4.690903320312" />
                  <Point X="26.4810234375" Y="4.677952148438" />
                  <Point X="26.8605859375" Y="4.540281738281" />
                  <Point X="26.894650390625" Y="4.527926269531" />
                  <Point X="27.261796875" Y="4.356225097656" />
                  <Point X="27.294576171875" Y="4.34089453125" />
                  <Point X="27.649279296875" Y="4.134243652344" />
                  <Point X="27.680978515625" Y="4.115775390625" />
                  <Point X="27.94326171875" Y="3.929253662109" />
                  <Point X="27.928228515625" Y="3.903215087891" />
                  <Point X="27.147583984375" Y="2.551098144531" />
                  <Point X="27.142076171875" Y="2.539927978516" />
                  <Point X="27.133078125" Y="2.516056152344" />
                  <Point X="27.113625" Y="2.443315917969" />
                  <Point X="27.11115625" Y="2.430729248047" />
                  <Point X="27.10780078125" Y="2.404281738281" />
                  <Point X="27.107728515625" Y="2.380950195312" />
                  <Point X="27.11530859375" Y="2.318089111328" />
                  <Point X="27.11609375" Y="2.311563964844" />
                  <Point X="27.121435546875" Y="2.289627929688" />
                  <Point X="27.129703125" Y="2.267527099609" />
                  <Point X="27.1400703125" Y="2.247471679688" />
                  <Point X="27.178990234375" Y="2.19011328125" />
                  <Point X="27.187017578125" Y="2.179869384766" />
                  <Point X="27.204361328125" Y="2.160616699219" />
                  <Point X="27.221603515625" Y="2.145590576172" />
                  <Point X="27.2789609375" Y="2.106670410156" />
                  <Point X="27.284912109375" Y="2.102633056641" />
                  <Point X="27.3049609375" Y="2.092269042969" />
                  <Point X="27.327044921875" Y="2.084005371094" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.41186328125" Y="2.071078857422" />
                  <Point X="27.425291015625" Y="2.070417724609" />
                  <Point X="27.450716796875" Y="2.070968017578" />
                  <Point X="27.473205078125" Y="2.074170654297" />
                  <Point X="27.5459453125" Y="2.093622558594" />
                  <Point X="27.55338671875" Y="2.095943847656" />
                  <Point X="27.57301953125" Y="2.102963623047" />
                  <Point X="27.58853515625" Y="2.110145263672" />
                  <Point X="27.72880078125" Y="2.191128173828" />
                  <Point X="28.967326171875" Y="2.906190917969" />
                  <Point X="29.10446875" Y="2.71559375" />
                  <Point X="29.1232734375" Y="2.689461425781" />
                  <Point X="29.26219921875" Y="2.459883056641" />
                  <Point X="29.258578125" Y="2.457104248047" />
                  <Point X="28.23078515625" Y="1.668451538086" />
                  <Point X="28.2214296875" Y="1.660245605469" />
                  <Point X="28.203974609375" Y="1.641628051758" />
                  <Point X="28.151623046875" Y="1.573331665039" />
                  <Point X="28.14478125" Y="1.563094360352" />
                  <Point X="28.1308828125" Y="1.539060302734" />
                  <Point X="28.121630859375" Y="1.517090209961" />
                  <Point X="28.10212890625" Y="1.447359375" />
                  <Point X="28.10010546875" Y="1.440125732422" />
                  <Point X="28.09665234375" Y="1.417827026367" />
                  <Point X="28.0958359375" Y="1.394256958008" />
                  <Point X="28.097740234375" Y="1.371769287109" />
                  <Point X="28.11375" Y="1.294184936523" />
                  <Point X="28.117078125" Y="1.28212890625" />
                  <Point X="28.1259375" Y="1.256699951172" />
                  <Point X="28.13628515625" Y="1.235740478516" />
                  <Point X="28.179814453125" Y="1.169576293945" />
                  <Point X="28.184330078125" Y="1.162710327148" />
                  <Point X="28.19888671875" Y="1.145459960938" />
                  <Point X="28.2161328125" Y="1.129365966797" />
                  <Point X="28.23434765625" Y="1.11603527832" />
                  <Point X="28.2974453125" Y="1.080517211914" />
                  <Point X="28.30915234375" Y="1.074942260742" />
                  <Point X="28.333673828125" Y="1.065259155273" />
                  <Point X="28.356119140625" Y="1.059438476562" />
                  <Point X="28.4414296875" Y="1.048163574219" />
                  <Point X="28.448802734375" Y="1.047480102539" />
                  <Point X="28.47073046875" Y="1.046307373047" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.621447265625" Y="1.064526367188" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.837859375" Y="0.965983276367" />
                  <Point X="29.84594140625" Y="0.932785827637" />
                  <Point X="29.8908671875" Y="0.644239013672" />
                  <Point X="28.716580078125" Y="0.329589782715" />
                  <Point X="28.70479296875" Y="0.325587219238" />
                  <Point X="28.681546875" Y="0.315068450928" />
                  <Point X="28.597732421875" Y="0.266621673584" />
                  <Point X="28.58787109375" Y="0.260070404053" />
                  <Point X="28.56486328125" Y="0.242623306274" />
                  <Point X="28.54753125" Y="0.225576187134" />
                  <Point X="28.4972421875" Y="0.161496047974" />
                  <Point X="28.492025390625" Y="0.15484854126" />
                  <Point X="28.480298828125" Y="0.135562362671" />
                  <Point X="28.470525390625" Y="0.114095405579" />
                  <Point X="28.463681640625" Y="0.092601127625" />
                  <Point X="28.44691796875" Y="0.005070747852" />
                  <Point X="28.44540625" Y="-0.006887602329" />
                  <Point X="28.44366796875" Y="-0.03476997757" />
                  <Point X="28.4451796875" Y="-0.058550674438" />
                  <Point X="28.461943359375" Y="-0.1460809021" />
                  <Point X="28.463681640625" Y="-0.15516116333" />
                  <Point X="28.470525390625" Y="-0.176655441284" />
                  <Point X="28.480298828125" Y="-0.198122390747" />
                  <Point X="28.492025390625" Y="-0.217408721924" />
                  <Point X="28.542314453125" Y="-0.281488708496" />
                  <Point X="28.5506796875" Y="-0.290810302734" />
                  <Point X="28.570208984375" Y="-0.309879272461" />
                  <Point X="28.589037109375" Y="-0.324155914307" />
                  <Point X="28.6728515625" Y="-0.372602722168" />
                  <Point X="28.679080078125" Y="-0.375901580811" />
                  <Point X="28.69985546875" Y="-0.385934295654" />
                  <Point X="28.716580078125" Y="-0.392149963379" />
                  <Point X="28.838771484375" Y="-0.424890869141" />
                  <Point X="29.891474609375" Y="-0.706961853027" />
                  <Point X="29.859533203125" Y="-0.918817504883" />
                  <Point X="29.855025390625" Y="-0.948726013184" />
                  <Point X="29.80117578125" Y="-1.18469934082" />
                  <Point X="29.7903828125" Y="-1.183278320313" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.21016015625" Y="-1.041263671875" />
                  <Point X="28.193095703125" Y="-1.04497265625" />
                  <Point X="28.1639765625" Y="-1.056596313477" />
                  <Point X="28.1361484375" Y="-1.073488891602" />
                  <Point X="28.1123984375" Y="-1.093960327148" />
                  <Point X="28.01296875" Y="-1.21354296875" />
                  <Point X="28.002654296875" Y="-1.225948242188" />
                  <Point X="27.987931640625" Y="-1.250333984375" />
                  <Point X="27.97658984375" Y="-1.277718505859" />
                  <Point X="27.969759765625" Y="-1.305365478516" />
                  <Point X="27.955509765625" Y="-1.460230224609" />
                  <Point X="27.95403125" Y="-1.476295654297" />
                  <Point X="27.956349609375" Y="-1.507564086914" />
                  <Point X="27.96408203125" Y="-1.539188354492" />
                  <Point X="27.976453125" Y="-1.567998901367" />
                  <Point X="28.06748828125" Y="-1.709599975586" />
                  <Point X="28.072169921875" Y="-1.716239624023" />
                  <Point X="28.093234375" Y="-1.7435546875" />
                  <Point X="28.110630859375" Y="-1.760909057617" />
                  <Point X="28.2240234375" Y="-1.847918945312" />
                  <Point X="29.213123046875" Y="-2.606882080078" />
                  <Point X="29.137521484375" Y="-2.729216552734" />
                  <Point X="29.12479296875" Y="-2.749813964844" />
                  <Point X="29.028982421875" Y="-2.885946044922" />
                  <Point X="29.0175625" Y="-2.879352294922" />
                  <Point X="27.800955078125" Y="-2.176943359375" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.558447265625" Y="-2.124467529297" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.50678125" Y="-2.120395507812" />
                  <Point X="27.474607421875" Y="-2.125354003906" />
                  <Point X="27.444833984375" Y="-2.135177490234" />
                  <Point X="27.2821875" Y="-2.220776611328" />
                  <Point X="27.26531640625" Y="-2.229656494141" />
                  <Point X="27.242388671875" Y="-2.246546386719" />
                  <Point X="27.2214296875" Y="-2.267503417969" />
                  <Point X="27.204533203125" Y="-2.290437011719" />
                  <Point X="27.11893359375" Y="-2.453082519531" />
                  <Point X="27.110052734375" Y="-2.469955078125" />
                  <Point X="27.100228515625" Y="-2.499734130859" />
                  <Point X="27.095271484375" Y="-2.531908935547" />
                  <Point X="27.09567578125" Y="-2.563259033203" />
                  <Point X="27.13103515625" Y="-2.759039550781" />
                  <Point X="27.134703125" Y="-2.779349365234" />
                  <Point X="27.13898828125" Y="-2.795139648438" />
                  <Point X="27.1518203125" Y="-2.826077880859" />
                  <Point X="27.224685546875" Y="-2.952286865234" />
                  <Point X="27.86128515625" Y="-4.054906494141" />
                  <Point X="27.796939453125" Y="-4.1008671875" />
                  <Point X="27.78184765625" Y="-4.111646972656" />
                  <Point X="27.701767578125" Y="-4.163481445312" />
                  <Point X="27.686931640625" Y="-4.144147949219" />
                  <Point X="26.758548828125" Y="-2.934254882812" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.52883203125" Y="-2.776416503906" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.205921875" Y="-2.760549560547" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397705078" />
                  <Point X="26.128978515625" Y="-2.780740966797" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="25.941529296875" Y="-2.931046386719" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968859375" />
                  <Point X="25.88725" Y="-2.996684570312" />
                  <Point X="25.875625" Y="-3.025807617188" />
                  <Point X="25.826869140625" Y="-3.250125" />
                  <Point X="25.821810546875" Y="-3.273395507812" />
                  <Point X="25.819724609375" Y="-3.289626708984" />
                  <Point X="25.8197421875" Y="-3.323120605469" />
                  <Point X="25.840392578125" Y="-3.479971923828" />
                  <Point X="26.022064453125" Y="-4.859915039062" />
                  <Point X="25.9899453125" Y="-4.866955566406" />
                  <Point X="25.975658203125" Y="-4.870087402344" />
                  <Point X="25.929318359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.96265625" Y="-4.756729003906" />
                  <Point X="23.941576171875" Y="-4.752637207031" />
                  <Point X="23.858755859375" Y="-4.731328613281" />
                  <Point X="23.8792265625" Y="-4.575838378906" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.509323730469" />
                  <Point X="23.876666015625" Y="-4.497688476562" />
                  <Point X="23.821826171875" Y="-4.221996582031" />
                  <Point X="23.81613671875" Y="-4.193396972656" />
                  <Point X="23.811875" Y="-4.178467773438" />
                  <Point X="23.80097265625" Y="-4.149500488281" />
                  <Point X="23.79433203125" Y="-4.135462402344" />
                  <Point X="23.778259765625" Y="-4.107625488281" />
                  <Point X="23.76942578125" Y="-4.094860595703" />
                  <Point X="23.74979296875" Y="-4.070937255859" />
                  <Point X="23.738994140625" Y="-4.059779052734" />
                  <Point X="23.527658203125" Y="-3.87444140625" />
                  <Point X="23.505734375" Y="-3.85521484375" />
                  <Point X="23.49326171875" Y="-3.845965332031" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.082693359375" Y="-3.77778515625" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.038068359375" Y="-3.776132324219" />
                  <Point X="23.007265625" Y="-3.779166748047" />
                  <Point X="22.991990234375" Y="-3.781947021484" />
                  <Point X="22.96094140625" Y="-3.790267578125" />
                  <Point X="22.94632421875" Y="-3.795498046875" />
                  <Point X="22.918134765625" Y="-3.808270751953" />
                  <Point X="22.9045625" Y="-3.815812988281" />
                  <Point X="22.670841796875" Y="-3.97198046875" />
                  <Point X="22.64659765625" Y="-3.988181152344" />
                  <Point X="22.640322265625" Y="-3.992756591797" />
                  <Point X="22.619560546875" Y="-4.010129638672" />
                  <Point X="22.597244140625" Y="-4.032768554688" />
                  <Point X="22.589533203125" Y="-4.041625488281" />
                  <Point X="22.56839453125" Y="-4.069172363281" />
                  <Point X="22.496798828125" Y="-4.162478515625" />
                  <Point X="22.283322265625" Y="-4.030298339844" />
                  <Point X="22.252408203125" Y="-4.011157226562" />
                  <Point X="22.01913671875" Y="-3.831546386719" />
                  <Point X="22.658513671875" Y="-2.724114746094" />
                  <Point X="22.6651484375" Y="-2.710084960938" />
                  <Point X="22.67605078125" Y="-2.681119628906" />
                  <Point X="22.680314453125" Y="-2.666188964844" />
                  <Point X="22.6865859375" Y="-2.634662109375" />
                  <Point X="22.688361328125" Y="-2.619238769531" />
                  <Point X="22.689375" Y="-2.588304931641" />
                  <Point X="22.6853359375" Y="-2.557616699219" />
                  <Point X="22.6763515625" Y="-2.527999023438" />
                  <Point X="22.67064453125" Y="-2.513559082031" />
                  <Point X="22.656427734375" Y="-2.484729736328" />
                  <Point X="22.6484453125" Y="-2.471411376953" />
                  <Point X="22.630416015625" Y="-2.446251464844" />
                  <Point X="22.620369140625" Y="-2.434409912109" />
                  <Point X="22.604650390625" Y="-2.418691894531" />
                  <Point X="22.591212890625" Y="-2.407046142578" />
                  <Point X="22.56608984375" Y="-2.389036865234" />
                  <Point X="22.55279296875" Y="-2.381062011719" />
                  <Point X="22.523958984375" Y="-2.366828369141" />
                  <Point X="22.509515625" Y="-2.361114013672" />
                  <Point X="22.479888671875" Y="-2.352116210938" />
                  <Point X="22.44919140625" Y="-2.348066894531" />
                  <Point X="22.41824609375" Y="-2.349073730469" />
                  <Point X="22.402814453125" Y="-2.350846435547" />
                  <Point X="22.371275390625" Y="-2.357115722656" />
                  <Point X="22.35633984375" Y="-2.361379150391" />
                  <Point X="22.327361328125" Y="-2.372283447266" />
                  <Point X="22.313318359375" Y="-2.378924316406" />
                  <Point X="22.19212890625" Y="-2.448892333984" />
                  <Point X="21.206912109375" Y="-3.017708007813" />
                  <Point X="21.020400390625" Y="-2.772669677734" />
                  <Point X="20.99598046875" Y="-2.740586425781" />
                  <Point X="20.818734375" Y="-2.443373535156" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.963517578125" Y="-1.563308837891" />
                  <Point X="21.98489453125" Y="-1.540388549805" />
                  <Point X="21.994630859375" Y="-1.528041503906" />
                  <Point X="22.012595703125" Y="-1.50091027832" />
                  <Point X="22.020162109375" Y="-1.487125732422" />
                  <Point X="22.03291796875" Y="-1.458496582031" />
                  <Point X="22.038107421875" Y="-1.443651977539" />
                  <Point X="22.0450859375" Y="-1.416711669922" />
                  <Point X="22.048439453125" Y="-1.398844360352" />
                  <Point X="22.05125" Y="-1.368412963867" />
                  <Point X="22.051423828125" Y="-1.353078613281" />
                  <Point X="22.04921875" Y="-1.321403808594" />
                  <Point X="22.046921875" Y="-1.306242675781" />
                  <Point X="22.039921875" Y="-1.276492797852" />
                  <Point X="22.02823046875" Y="-1.248260986328" />
                  <Point X="22.0121484375" Y="-1.222272949219" />
                  <Point X="22.0030546875" Y="-1.209928100586" />
                  <Point X="21.982220703125" Y="-1.185968017578" />
                  <Point X="21.971259765625" Y="-1.175247558594" />
                  <Point X="21.947755859375" Y="-1.155709960938" />
                  <Point X="21.935212890625" Y="-1.146892822266" />
                  <Point X="21.91121875" Y="-1.132771484375" />
                  <Point X="21.89457421875" Y="-1.124484863281" />
                  <Point X="21.865318359375" Y="-1.113262939453" />
                  <Point X="21.850224609375" Y="-1.108866821289" />
                  <Point X="21.81833203125" Y="-1.102381591797" />
                  <Point X="21.802712890625" Y="-1.100534057617" />
                  <Point X="21.7713828125" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196411133" />
                  <Point X="21.60268359375" Y="-1.120337890625" />
                  <Point X="20.33908203125" Y="-1.286694458008" />
                  <Point X="20.2687890625" Y="-1.011509033203" />
                  <Point X="20.2592421875" Y="-0.974122375488" />
                  <Point X="20.213548828125" Y="-0.654654724121" />
                  <Point X="21.491712890625" Y="-0.312171295166" />
                  <Point X="21.4995234375" Y="-0.309713348389" />
                  <Point X="21.526068359375" Y="-0.299251953125" />
                  <Point X="21.555833984375" Y="-0.2838934021" />
                  <Point X="21.56644140625" Y="-0.277513763428" />
                  <Point X="21.5915859375" Y="-0.260062164307" />
                  <Point X="21.60632421875" Y="-0.248246337891" />
                  <Point X="21.62877734375" Y="-0.226363845825" />
                  <Point X="21.63909375" Y="-0.214491699219" />
                  <Point X="21.658345703125" Y="-0.188226470947" />
                  <Point X="21.666564453125" Y="-0.17480909729" />
                  <Point X="21.680673828125" Y="-0.146804519653" />
                  <Point X="21.686564453125" Y="-0.132217330933" />
                  <Point X="21.6949453125" Y="-0.10521181488" />
                  <Point X="21.699009765625" Y="-0.088494857788" />
                  <Point X="21.703279296875" Y="-0.060337505341" />
                  <Point X="21.704353515625" Y="-0.046095420837" />
                  <Point X="21.704353515625" Y="-0.01646462059" />
                  <Point X="21.703279296875" Y="-0.002222533226" />
                  <Point X="21.699009765625" Y="0.025934818268" />
                  <Point X="21.695814453125" Y="0.039850231171" />
                  <Point X="21.68743359375" Y="0.066855758667" />
                  <Point X="21.68067578125" Y="0.084240470886" />
                  <Point X="21.666572265625" Y="0.112236717224" />
                  <Point X="21.658357421875" Y="0.125649635315" />
                  <Point X="21.63911328125" Y="0.151910263062" />
                  <Point X="21.628794921875" Y="0.16378717041" />
                  <Point X="21.6063359375" Y="0.185677841187" />
                  <Point X="21.5941953125" Y="0.195691741943" />
                  <Point X="21.56905078125" Y="0.213143341064" />
                  <Point X="21.5632265625" Y="0.216878540039" />
                  <Point X="21.54525" Y="0.227212875366" />
                  <Point X="21.527072265625" Y="0.236243652344" />
                  <Point X="21.5183984375" Y="0.240026702881" />
                  <Point X="21.491712890625" Y="0.249611099243" />
                  <Point X="21.3522578125" Y="0.286978302002" />
                  <Point X="20.2145546875" Y="0.591824890137" />
                  <Point X="20.262515625" Y="0.915948181152" />
                  <Point X="20.26866796875" Y="0.95752130127" />
                  <Point X="20.3664140625" Y="1.318237182617" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815551758" />
                  <Point X="21.252943359375" Y="1.204364257812" />
                  <Point X="21.263294921875" Y="1.204703369141" />
                  <Point X="21.28485546875" Y="1.20658984375" />
                  <Point X="21.295109375" Y="1.208053588867" />
                  <Point X="21.315396484375" Y="1.212088989258" />
                  <Point X="21.3254296875" Y="1.214660522461" />
                  <Point X="21.38108203125" Y="1.232207519531" />
                  <Point X="21.396548828125" Y="1.23767565918" />
                  <Point X="21.415484375" Y="1.246007080078" />
                  <Point X="21.4247265625" Y="1.250690673828" />
                  <Point X="21.443470703125" Y="1.261512939453" />
                  <Point X="21.452146484375" Y="1.267174316406" />
                  <Point X="21.46882421875" Y="1.279403930664" />
                  <Point X="21.4840703125" Y="1.29337512207" />
                  <Point X="21.49770703125" Y="1.308924194336" />
                  <Point X="21.504099609375" Y="1.3170703125" />
                  <Point X="21.516515625" Y="1.33480065918" />
                  <Point X="21.521984375" Y="1.343594604492" />
                  <Point X="21.5319375" Y="1.361733764648" />
                  <Point X="21.536421875" Y="1.371079467773" />
                  <Point X="21.558751953125" Y="1.424991088867" />
                  <Point X="21.564505859375" Y="1.440358886719" />
                  <Point X="21.57029296875" Y="1.460217773438" />
                  <Point X="21.572642578125" Y="1.470301757812" />
                  <Point X="21.576400390625" Y="1.491616210938" />
                  <Point X="21.577640625" Y="1.501894042969" />
                  <Point X="21.578994140625" Y="1.522533447266" />
                  <Point X="21.578091796875" Y="1.543201782227" />
                  <Point X="21.5749453125" Y="1.563644775391" />
                  <Point X="21.572814453125" Y="1.573780151367" />
                  <Point X="21.567212890625" Y="1.594685913086" />
                  <Point X="21.563990234375" Y="1.604529052734" />
                  <Point X="21.556494140625" Y="1.623805908203" />
                  <Point X="21.552220703125" Y="1.633239868164" />
                  <Point X="21.52527734375" Y="1.68499987793" />
                  <Point X="21.517177734375" Y="1.699328613281" />
                  <Point X="21.50568359375" Y="1.716523925781" />
                  <Point X="21.499466796875" Y="1.724807739258" />
                  <Point X="21.48555859375" Y="1.741376953125" />
                  <Point X="21.47848046875" Y="1.748931152344" />
                  <Point X="21.463548828125" Y="1.763221923828" />
                  <Point X="21.4556953125" Y="1.769958374023" />
                  <Point X="21.375703125" Y="1.831339111328" />
                  <Point X="20.77238671875" Y="2.294280517578" />
                  <Point X="20.973802734375" Y="2.639354980469" />
                  <Point X="20.99771484375" Y="2.680322509766" />
                  <Point X="21.273662109375" Y="3.035012695312" />
                  <Point X="21.745841796875" Y="2.762399658203" />
                  <Point X="21.755080078125" Y="2.757717773438" />
                  <Point X="21.774015625" Y="2.749385742188" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.9224375" Y="2.724376708984" />
                  <Point X="21.940830078125" Y="2.723334472656" />
                  <Point X="21.961509765625" Y="2.723785888672" />
                  <Point X="21.9718359375" Y="2.724576171875" />
                  <Point X="21.993294921875" Y="2.727401611328" />
                  <Point X="22.003474609375" Y="2.729311279297" />
                  <Point X="22.023568359375" Y="2.734228271484" />
                  <Point X="22.0430078125" Y="2.741303466797" />
                  <Point X="22.061560546875" Y="2.750453125" />
                  <Point X="22.070587890625" Y="2.755534912109" />
                  <Point X="22.088841796875" Y="2.767164794922" />
                  <Point X="22.097259765625" Y="2.773198486328" />
                  <Point X="22.11338671875" Y="2.786142822266" />
                  <Point X="22.121095703125" Y="2.793053466797" />
                  <Point X="22.17611328125" Y="2.848069580078" />
                  <Point X="22.188734375" Y="2.861487792969" />
                  <Point X="22.201681640625" Y="2.877618652344" />
                  <Point X="22.20771484375" Y="2.886038574219" />
                  <Point X="22.21934375" Y="2.904291503906" />
                  <Point X="22.224423828125" Y="2.913316650391" />
                  <Point X="22.233572265625" Y="2.931866455078" />
                  <Point X="22.240646484375" Y="2.951297607422" />
                  <Point X="22.245564453125" Y="2.971387451172" />
                  <Point X="22.2474765625" Y="2.981570800781" />
                  <Point X="22.250302734375" Y="3.003028076172" />
                  <Point X="22.251091796875" Y="3.013362060547" />
                  <Point X="22.25154296875" Y="3.034047607422" />
                  <Point X="22.251205078125" Y="3.044399169922" />
                  <Point X="22.244423828125" Y="3.121907958984" />
                  <Point X="22.242255859375" Y="3.140207763672" />
                  <Point X="22.23821875" Y="3.160497314453" />
                  <Point X="22.235646484375" Y="3.170527832031" />
                  <Point X="22.22913671875" Y="3.191172119141" />
                  <Point X="22.22548828125" Y="3.200864990234" />
                  <Point X="22.217158203125" Y="3.219795654297" />
                  <Point X="22.2124765625" Y="3.229033447266" />
                  <Point X="22.177029296875" Y="3.2904296875" />
                  <Point X="21.94061328125" Y="3.699914794922" />
                  <Point X="22.31" Y="3.983120117188" />
                  <Point X="22.3516328125" Y="4.015041015625" />
                  <Point X="22.807474609375" Y="4.268296386719" />
                  <Point X="22.881435546875" Y="4.171908203125" />
                  <Point X="22.8881796875" Y="4.164047363281" />
                  <Point X="22.902482421875" Y="4.149106445313" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93490625" Y="4.121898925781" />
                  <Point X="22.95210546875" Y="4.11040625" />
                  <Point X="22.96101953125" Y="4.105128417969" />
                  <Point X="23.047287109375" Y="4.060220458984" />
                  <Point X="23.06567578125" Y="4.051285400391" />
                  <Point X="23.08495703125" Y="4.043788330078" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031377929688" />
                  <Point X="23.219177734375" Y="4.035135253906" />
                  <Point X="23.229267578125" Y="4.037487548828" />
                  <Point X="23.24912890625" Y="4.043276611328" />
                  <Point X="23.258900390625" Y="4.046713134766" />
                  <Point X="23.348751953125" Y="4.083931884766" />
                  <Point X="23.367408203125" Y="4.092270019531" />
                  <Point X="23.385537109375" Y="4.10221484375" />
                  <Point X="23.39433203125" Y="4.107682617188" />
                  <Point X="23.412064453125" Y="4.120097167969" />
                  <Point X="23.4202109375" Y="4.126489746094" />
                  <Point X="23.435767578125" Y="4.140129882813" />
                  <Point X="23.449751953125" Y="4.155389648437" />
                  <Point X="23.461984375" Y="4.17207421875" />
                  <Point X="23.467642578125" Y="4.180746582031" />
                  <Point X="23.47846484375" Y="4.1994921875" />
                  <Point X="23.483146484375" Y="4.208731445312" />
                  <Point X="23.4914765625" Y="4.2276640625" />
                  <Point X="23.495125" Y="4.237356933594" />
                  <Point X="23.52436328125" Y="4.330091796875" />
                  <Point X="23.529966796875" Y="4.349733886719" />
                  <Point X="23.534005859375" Y="4.370024902344" />
                  <Point X="23.53547265625" Y="4.380283691406" />
                  <Point X="23.537361328125" Y="4.401851074219" />
                  <Point X="23.53769921875" Y="4.412209472656" />
                  <Point X="23.537248046875" Y="4.432900878906" />
                  <Point X="23.536458984375" Y="4.443233886719" />
                  <Point X="23.532427734375" Y="4.473844238281" />
                  <Point X="23.520736328125" Y="4.562655273438" />
                  <Point X="24.014935546875" Y="4.701212402344" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.634775390625" Y="4.782556640625" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.219974609375" Y="4.218915527344" />
                  <Point X="25.232748046875" Y="4.247107910156" />
                  <Point X="25.2379765625" Y="4.261724609375" />
                  <Point X="25.256140625" Y="4.329508789063" />
                  <Point X="25.378189453125" Y="4.785006347656" />
                  <Point X="25.780810546875" Y="4.742841308594" />
                  <Point X="25.827876953125" Y="4.737911621094" />
                  <Point X="26.4050859375" Y="4.598556640625" />
                  <Point X="26.453591796875" Y="4.586845703125" />
                  <Point X="26.828193359375" Y="4.450974609375" />
                  <Point X="26.858259765625" Y="4.440069335938" />
                  <Point X="27.221552734375" Y="4.270170410156" />
                  <Point X="27.25045703125" Y="4.25665234375" />
                  <Point X="27.60145703125" Y="4.052158447266" />
                  <Point X="27.629431640625" Y="4.035860595703" />
                  <Point X="27.81778125" Y="3.901916015625" />
                  <Point X="27.065310546875" Y="2.59859765625" />
                  <Point X="27.06237890625" Y="2.593111328125" />
                  <Point X="27.053181640625" Y="2.573435302734" />
                  <Point X="27.04418359375" Y="2.549563476562" />
                  <Point X="27.041302734375" Y="2.540599853516" />
                  <Point X="27.021849609375" Y="2.467859619141" />
                  <Point X="27.016912109375" Y="2.442686279297" />
                  <Point X="27.013556640625" Y="2.416238769531" />
                  <Point X="27.01280078125" Y="2.404575927734" />
                  <Point X="27.012728515625" Y="2.381244384766" />
                  <Point X="27.013412109375" Y="2.369577148438" />
                  <Point X="27.020990234375" Y="2.306739990234" />
                  <Point X="27.023791015625" Y="2.289086669922" />
                  <Point X="27.0291328125" Y="2.267150634766" />
                  <Point X="27.03245703125" Y="2.256342529297" />
                  <Point X="27.040724609375" Y="2.234241699219" />
                  <Point X="27.0453125" Y="2.223902832031" />
                  <Point X="27.0556796875" Y="2.203847412109" />
                  <Point X="27.061458984375" Y="2.194130859375" />
                  <Point X="27.10037890625" Y="2.136772460937" />
                  <Point X="27.11643359375" Y="2.116284667969" />
                  <Point X="27.13377734375" Y="2.097031982422" />
                  <Point X="27.141947265625" Y="2.088996826172" />
                  <Point X="27.159189453125" Y="2.073970703125" />
                  <Point X="27.16826171875" Y="2.066979736328" />
                  <Point X="27.225619140625" Y="2.028059692383" />
                  <Point X="27.241287109375" Y="2.018241943359" />
                  <Point X="27.2613359375" Y="2.007877929688" />
                  <Point X="27.27166796875" Y="2.003294189453" />
                  <Point X="27.293751953125" Y="1.995030517578" />
                  <Point X="27.304552734375" Y="1.991706420898" />
                  <Point X="27.32647265625" Y="1.986364868164" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.400490234375" Y="1.976762084961" />
                  <Point X="27.40719140625" Y="1.976193847656" />
                  <Point X="27.427345703125" Y="1.975439941406" />
                  <Point X="27.452771484375" Y="1.975990234375" />
                  <Point X="27.464111328125" Y="1.976916992188" />
                  <Point X="27.486599609375" Y="1.980119628906" />
                  <Point X="27.497748046875" Y="1.982395507812" />
                  <Point X="27.57048828125" Y="2.001847412109" />
                  <Point X="27.58537109375" Y="2.006489990234" />
                  <Point X="27.60500390625" Y="2.013509765625" />
                  <Point X="27.612923828125" Y="2.016751098633" />
                  <Point X="27.63603515625" Y="2.027872924805" />
                  <Point X="27.77630078125" Y="2.108855957031" />
                  <Point X="28.940404296875" Y="2.780951171875" />
                  <Point X="29.02735546875" Y="2.660108154297" />
                  <Point X="29.043955078125" Y="2.6370390625" />
                  <Point X="29.136884765625" Y="2.483471191406" />
                  <Point X="28.17294921875" Y="1.743817626953" />
                  <Point X="28.168140625" Y="1.739870849609" />
                  <Point X="28.152125" Y="1.725222290039" />
                  <Point X="28.134669921875" Y="1.706604736328" />
                  <Point X="28.128578125" Y="1.699422729492" />
                  <Point X="28.0762265625" Y="1.631126342773" />
                  <Point X="28.06254296875" Y="1.610651733398" />
                  <Point X="28.04864453125" Y="1.586617675781" />
                  <Point X="28.043330078125" Y="1.575930541992" />
                  <Point X="28.034078125" Y="1.553960327148" />
                  <Point X="28.030140625" Y="1.542677490234" />
                  <Point X="28.010638671875" Y="1.472946655273" />
                  <Point X="28.006224609375" Y="1.4546640625" />
                  <Point X="28.002771484375" Y="1.432365234375" />
                  <Point X="28.001708984375" Y="1.421115600586" />
                  <Point X="28.000892578125" Y="1.397545532227" />
                  <Point X="28.001173828125" Y="1.386240844727" />
                  <Point X="28.003078125" Y="1.363753173828" />
                  <Point X="28.004701171875" Y="1.35257019043" />
                  <Point X="28.0207109375" Y="1.274985961914" />
                  <Point X="28.0273671875" Y="1.250873779297" />
                  <Point X="28.0362265625" Y="1.225444824219" />
                  <Point X="28.04075390625" Y="1.21464465332" />
                  <Point X="28.0511015625" Y="1.193685180664" />
                  <Point X="28.056919921875" Y="1.183526733398" />
                  <Point X="28.10044921875" Y="1.117362548828" />
                  <Point X="28.1117265625" Y="1.101443603516" />
                  <Point X="28.126283203125" Y="1.084193237305" />
                  <Point X="28.134072265625" Y="1.076005004883" />
                  <Point X="28.151318359375" Y="1.059911010742" />
                  <Point X="28.16002734375" Y="1.052703613281" />
                  <Point X="28.1782421875" Y="1.039372924805" />
                  <Point X="28.187748046875" Y="1.03325" />
                  <Point X="28.250845703125" Y="0.997731933594" />
                  <Point X="28.274259765625" Y="0.986581970215" />
                  <Point X="28.29878125" Y="0.976898742676" />
                  <Point X="28.309826171875" Y="0.973301025391" />
                  <Point X="28.332271484375" Y="0.967480224609" />
                  <Point X="28.343671875" Y="0.965257385254" />
                  <Point X="28.428982421875" Y="0.95398260498" />
                  <Point X="28.443728515625" Y="0.952615722656" />
                  <Point X="28.46565625" Y="0.951442932129" />
                  <Point X="28.474408203125" Y="0.951378540039" />
                  <Point X="28.50060546875" Y="0.952797180176" />
                  <Point X="28.63384765625" Y="0.970339111328" />
                  <Point X="29.704703125" Y="1.111319702148" />
                  <Point X="29.7455546875" Y="0.943512573242" />
                  <Point X="29.752689453125" Y="0.914204467773" />
                  <Point X="29.783873046875" Y="0.713921325684" />
                  <Point X="28.6919921875" Y="0.421352722168" />
                  <Point X="28.686033203125" Y="0.419544891357" />
                  <Point X="28.66562890625" Y="0.412138702393" />
                  <Point X="28.6423828125" Y="0.401619934082" />
                  <Point X="28.634005859375" Y="0.397316833496" />
                  <Point X="28.55019140625" Y="0.34887008667" />
                  <Point X="28.53046875" Y="0.335767333984" />
                  <Point X="28.5074609375" Y="0.318320220947" />
                  <Point X="28.498248046875" Y="0.310352844238" />
                  <Point X="28.480916015625" Y="0.293305633545" />
                  <Point X="28.472796875" Y="0.284226257324" />
                  <Point X="28.4225078125" Y="0.22014616394" />
                  <Point X="28.4108515625" Y="0.204203979492" />
                  <Point X="28.399125" Y="0.184917770386" />
                  <Point X="28.393837890625" Y="0.17492616272" />
                  <Point X="28.384064453125" Y="0.153459213257" />
                  <Point X="28.38000390625" Y="0.142917541504" />
                  <Point X="28.37316015625" Y="0.121423248291" />
                  <Point X="28.370376953125" Y="0.11047063446" />
                  <Point X="28.35361328125" Y="0.022940216064" />
                  <Point X="28.35058984375" Y="-0.000976478875" />
                  <Point X="28.3488515625" Y="-0.028858879089" />
                  <Point X="28.348859375" Y="-0.040796863556" />
                  <Point X="28.35037109375" Y="-0.064577568054" />
                  <Point X="28.351875" Y="-0.076420135498" />
                  <Point X="28.368638671875" Y="-0.163950408936" />
                  <Point X="28.37316015625" Y="-0.183983291626" />
                  <Point X="28.38000390625" Y="-0.205477584839" />
                  <Point X="28.384064453125" Y="-0.216019256592" />
                  <Point X="28.393837890625" Y="-0.237486190796" />
                  <Point X="28.399125" Y="-0.247477661133" />
                  <Point X="28.4108515625" Y="-0.26676385498" />
                  <Point X="28.417291015625" Y="-0.276058898926" />
                  <Point X="28.467580078125" Y="-0.34013885498" />
                  <Point X="28.484310546875" Y="-0.358781799316" />
                  <Point X="28.50383984375" Y="-0.377850708008" />
                  <Point X="28.51280859375" Y="-0.385578063965" />
                  <Point X="28.53163671875" Y="-0.399854644775" />
                  <Point X="28.54149609375" Y="-0.406404296875" />
                  <Point X="28.625310546875" Y="-0.454851043701" />
                  <Point X="28.637767578125" Y="-0.461448730469" />
                  <Point X="28.65854296875" Y="-0.471481506348" />
                  <Point X="28.666759765625" Y="-0.47498336792" />
                  <Point X="28.6919921875" Y="-0.48391293335" />
                  <Point X="28.81418359375" Y="-0.516653808594" />
                  <Point X="29.78487890625" Y="-0.776751159668" />
                  <Point X="29.765595703125" Y="-0.904654541016" />
                  <Point X="29.761619140625" Y="-0.931040710449" />
                  <Point X="29.7278046875" Y="-1.079220092773" />
                  <Point X="28.436783203125" Y="-0.909253662109" />
                  <Point X="28.428625" Y="-0.908535522461" />
                  <Point X="28.40009765625" Y="-0.908042541504" />
                  <Point X="28.36672265625" Y="-0.910840820312" />
                  <Point X="28.354482421875" Y="-0.912676147461" />
                  <Point X="28.189982421875" Y="-0.948431274414" />
                  <Point X="28.157876953125" Y="-0.956742248535" />
                  <Point X="28.1287578125" Y="-0.968365905762" />
                  <Point X="28.1146796875" Y="-0.975387451172" />
                  <Point X="28.0868515625" Y="-0.992280090332" />
                  <Point X="28.074123046875" Y="-1.001530822754" />
                  <Point X="28.050373046875" Y="-1.022002258301" />
                  <Point X="28.0393515625" Y="-1.033223022461" />
                  <Point X="27.939921875" Y="-1.152805541992" />
                  <Point X="27.921326171875" Y="-1.17684777832" />
                  <Point X="27.906603515625" Y="-1.201233276367" />
                  <Point X="27.900162109375" Y="-1.213982299805" />
                  <Point X="27.8888203125" Y="-1.24136706543" />
                  <Point X="27.88436328125" Y="-1.254934082031" />
                  <Point X="27.877533203125" Y="-1.282581054688" />
                  <Point X="27.87516015625" Y="-1.296660766602" />
                  <Point X="27.86091015625" Y="-1.451525390625" />
                  <Point X="27.859291015625" Y="-1.483320068359" />
                  <Point X="27.861609375" Y="-1.514588500977" />
                  <Point X="27.864068359375" Y="-1.530127807617" />
                  <Point X="27.87180078125" Y="-1.561752075195" />
                  <Point X="27.8767890625" Y="-1.576671386719" />
                  <Point X="27.88916015625" Y="-1.605481933594" />
                  <Point X="27.89654296875" Y="-1.619373168945" />
                  <Point X="27.987578125" Y="-1.760974243164" />
                  <Point X="27.99694140625" Y="-1.774253540039" />
                  <Point X="28.018005859375" Y="-1.801568603516" />
                  <Point X="28.026140625" Y="-1.810811157227" />
                  <Point X="28.043537109375" Y="-1.828165649414" />
                  <Point X="28.052798828125" Y="-1.83627734375" />
                  <Point X="28.16619140625" Y="-1.923287231445" />
                  <Point X="29.087171875" Y="-2.629980957031" />
                  <Point X="29.056708984375" Y="-2.679274658203" />
                  <Point X="29.0454765625" Y="-2.697451660156" />
                  <Point X="29.001275390625" Y="-2.760253173828" />
                  <Point X="27.84845703125" Y="-2.094672363281" />
                  <Point X="27.841193359375" Y="-2.090886230469" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.57533203125" Y="-2.030979858398" />
                  <Point X="27.555021484375" Y="-2.027312011719" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.508005859375" Y="-2.025403442383" />
                  <Point X="27.492310546875" Y="-2.02650402832" />
                  <Point X="27.46013671875" Y="-2.031462524414" />
                  <Point X="27.444841796875" Y="-2.035137695313" />
                  <Point X="27.415068359375" Y="-2.044961181641" />
                  <Point X="27.40058984375" Y="-2.051109375" />
                  <Point X="27.237943359375" Y="-2.136708496094" />
                  <Point X="27.208970703125" Y="-2.153169433594" />
                  <Point X="27.18604296875" Y="-2.170059326172" />
                  <Point X="27.175216796875" Y="-2.179368164063" />
                  <Point X="27.1542578125" Y="-2.200325195313" />
                  <Point X="27.1449453125" Y="-2.211153808594" />
                  <Point X="27.128048828125" Y="-2.234087402344" />
                  <Point X="27.12046484375" Y="-2.246192382812" />
                  <Point X="27.034865234375" Y="-2.408837890625" />
                  <Point X="27.0198359375" Y="-2.440192138672" />
                  <Point X="27.01001171875" Y="-2.469971191406" />
                  <Point X="27.0063359375" Y="-2.485268554688" />
                  <Point X="27.00137890625" Y="-2.517443359375" />
                  <Point X="27.000279296875" Y="-2.533134033203" />
                  <Point X="27.00068359375" Y="-2.564484130859" />
                  <Point X="27.0021875" Y="-2.580143554688" />
                  <Point X="27.037546875" Y="-2.775924072266" />
                  <Point X="27.04121484375" Y="-2.796233886719" />
                  <Point X="27.04301953125" Y="-2.80423046875" />
                  <Point X="27.051236328125" Y="-2.831535644531" />
                  <Point X="27.064068359375" Y="-2.862473876953" />
                  <Point X="27.069546875" Y="-2.873577148438" />
                  <Point X="27.142412109375" Y="-2.999786132812" />
                  <Point X="27.73589453125" Y="-4.027725097656" />
                  <Point X="27.723755859375" Y="-4.036083984375" />
                  <Point X="26.833916015625" Y="-2.876420410156" />
                  <Point X="26.82865234375" Y="-2.870141113281" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.58020703125" Y="-2.696506347656" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.197216796875" Y="-2.66594921875" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670339355469" />
                  <Point X="26.133576171875" Y="-2.677171386719" />
                  <Point X="26.1200078125" Y="-2.681629394531" />
                  <Point X="26.092623046875" Y="-2.69297265625" />
                  <Point X="26.079876953125" Y="-2.699414306641" />
                  <Point X="26.05549609375" Y="-2.714134521484" />
                  <Point X="26.043861328125" Y="-2.722413085938" />
                  <Point X="25.88079296875" Y="-2.857998291016" />
                  <Point X="25.863876953125" Y="-2.872063476562" />
                  <Point X="25.85265625" Y="-2.883084960938" />
                  <Point X="25.832185546875" Y="-2.906832763672" />
                  <Point X="25.822935546875" Y="-2.919559082031" />
                  <Point X="25.80604296875" Y="-2.947384277344" />
                  <Point X="25.79901953125" Y="-2.961465820312" />
                  <Point X="25.78739453125" Y="-2.990588867188" />
                  <Point X="25.78279296875" Y="-3.005630371094" />
                  <Point X="25.734037109375" Y="-3.229947753906" />
                  <Point X="25.728978515625" Y="-3.253218261719" />
                  <Point X="25.7275859375" Y="-3.261286376953" />
                  <Point X="25.724724609375" Y="-3.289676513672" />
                  <Point X="25.7247421875" Y="-3.323170410156" />
                  <Point X="25.7255546875" Y="-3.335520996094" />
                  <Point X="25.746205078125" Y="-3.492372314453" />
                  <Point X="25.83308984375" Y="-4.152326171875" />
                  <Point X="25.65506640625" Y="-3.487936523438" />
                  <Point X="25.652607421875" Y="-3.480123535156" />
                  <Point X="25.6421484375" Y="-3.453581787109" />
                  <Point X="25.626791015625" Y="-3.423816894531" />
                  <Point X="25.62041015625" Y="-3.413209228516" />
                  <Point X="25.4720703125" Y="-3.19948046875" />
                  <Point X="25.456681640625" Y="-3.177308837891" />
                  <Point X="25.446669921875" Y="-3.165169189453" />
                  <Point X="25.424783203125" Y="-3.142712890625" />
                  <Point X="25.412908203125" Y="-3.132396240234" />
                  <Point X="25.38665234375" Y="-3.113152832031" />
                  <Point X="25.3732421875" Y="-3.104937255859" />
                  <Point X="25.345244140625" Y="-3.090830078125" />
                  <Point X="25.33065625" Y="-3.084938476562" />
                  <Point X="25.101109375" Y="-3.013695556641" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.063376953125" Y="-3.003109130859" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766357422" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109375" />
                  <Point X="24.935015625" Y="-3.006305419922" />
                  <Point X="24.705470703125" Y="-3.077548095703" />
                  <Point X="24.68165625" Y="-3.084938720703" />
                  <Point X="24.6670703125" Y="-3.090829101562" />
                  <Point X="24.6390703125" Y="-3.104936523438" />
                  <Point X="24.62565625" Y="-3.113153564453" />
                  <Point X="24.599400390625" Y="-3.132397460938" />
                  <Point X="24.587525390625" Y="-3.142716796875" />
                  <Point X="24.565640625" Y="-3.165173095703" />
                  <Point X="24.555630859375" Y="-3.177310058594" />
                  <Point X="24.40729296875" Y="-3.391038574219" />
                  <Point X="24.391904296875" Y="-3.413210449219" />
                  <Point X="24.387529296875" Y="-3.420133544922" />
                  <Point X="24.374025390625" Y="-3.445260742188" />
                  <Point X="24.36122265625" Y="-3.476212646484" />
                  <Point X="24.35724609375" Y="-3.487936035156" />
                  <Point X="24.319595703125" Y="-3.628447265625" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.764860013226" Y="0.708826798515" />
                  <Point X="29.607417065664" Y="1.098511768189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.068925788815" Y="2.431324448196" />
                  <Point X="28.930076526092" Y="2.774988432969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.672407936243" Y="0.684054338385" />
                  <Point X="29.510131006328" Y="1.08570383423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.990712640002" Y="2.371309404146" />
                  <Point X="28.846995568469" Y="2.72702163851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.579955859259" Y="0.659281878255" />
                  <Point X="29.412844946992" Y="1.07289590027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.912499491188" Y="2.311294360096" />
                  <Point X="28.763914610845" Y="2.67905484405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.487503782276" Y="0.634509418126" />
                  <Point X="29.315558887656" Y="1.060087966311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.834286342375" Y="2.251279316046" />
                  <Point X="28.680833653222" Y="2.631088049591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.395051705292" Y="0.609736957996" />
                  <Point X="29.21827282832" Y="1.047280032352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.756073193561" Y="2.191264271996" />
                  <Point X="28.597752695598" Y="2.583121255132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.302599628308" Y="0.584964497867" />
                  <Point X="29.120986768984" Y="1.034472098392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.677860044748" Y="2.131249227947" />
                  <Point X="28.514671737975" Y="2.535154460672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.775484194679" Y="-0.839065255982" />
                  <Point X="29.746110669022" Y="-0.766363228791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.210147551325" Y="0.560192037737" />
                  <Point X="29.023700709648" Y="1.021664164433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.599646895934" Y="2.071234183897" />
                  <Point X="28.431590780351" Y="2.487187666213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.74305378289" Y="-1.012396550557" />
                  <Point X="29.631210985367" Y="-0.735575912758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.117695474341" Y="0.535419577607" />
                  <Point X="28.926414650312" Y="1.008856230474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.521433747121" Y="2.011219139847" />
                  <Point X="28.348509822727" Y="2.439220871753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.782342478606" Y="3.840534222022" />
                  <Point X="27.73325684046" Y="3.962025439689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.664208703448" Y="-1.070847511415" />
                  <Point X="29.516311301712" Y="-0.704788596725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.025243397358" Y="0.510647117478" />
                  <Point X="28.829128590976" Y="0.996048296514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.443220598307" Y="1.951204095797" />
                  <Point X="28.265428865104" Y="2.391254077294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.722064129962" Y="3.736128989853" />
                  <Point X="27.592201248512" Y="4.057550900477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.555991718872" Y="-1.056600456018" />
                  <Point X="29.401411618057" Y="-0.674001280691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.932791320374" Y="0.485874657348" />
                  <Point X="28.73184253164" Y="0.983240362555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.365007449494" Y="1.891189051747" />
                  <Point X="28.18234790748" Y="2.343287282835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.661785781319" Y="3.631723757684" />
                  <Point X="27.458197741459" Y="4.135621838653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.447774734297" Y="-1.042353400622" />
                  <Point X="29.286511934403" Y="-0.643213964658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.840339243391" Y="0.461102197218" />
                  <Point X="28.634556472304" Y="0.970432428595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.28679430068" Y="1.831174007697" />
                  <Point X="28.099266949857" Y="2.295320488375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.601507432675" Y="3.527318525515" />
                  <Point X="27.324194234406" Y="4.213692776829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.339557749722" Y="-1.028106345226" />
                  <Point X="29.171612250748" Y="-0.612426648625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.747887166407" Y="0.436329737089" />
                  <Point X="28.537270493616" Y="0.957624295026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.208581151867" Y="1.771158963647" />
                  <Point X="28.016185992233" Y="2.247353693916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.541229084031" Y="3.422913293346" />
                  <Point X="27.193641137508" Y="4.283223650193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.231340765146" Y="-1.01385928983" />
                  <Point X="29.056712567093" Y="-0.581639332592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.656812074321" Y="0.408149119743" />
                  <Point X="28.436565006918" Y="0.953279740775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.132998231921" Y="1.704633874705" />
                  <Point X="27.93310503461" Y="2.199386899457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.480950735388" Y="3.318508061177" />
                  <Point X="27.06731033517" Y="4.342303977797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.123123780571" Y="-0.999612234434" />
                  <Point X="28.941812883438" Y="-0.550852016559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.57297966219" Y="0.362042240457" />
                  <Point X="28.327909837487" Y="0.968611341746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.066260989261" Y="1.616214966202" />
                  <Point X="27.850024076986" Y="2.151420104997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.420672386744" Y="3.214102829008" />
                  <Point X="26.940979532832" Y="4.401384305402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.014906795996" Y="-0.985365179038" />
                  <Point X="28.826913199783" Y="-0.520064700525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.493349393447" Y="0.305534691312" />
                  <Point X="28.20274378884" Y="1.024808802806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.015157014741" Y="1.489102361252" />
                  <Point X="27.766943133954" Y="2.103453274424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.3603940381" Y="3.109697596839" />
                  <Point X="26.816815612172" Y="4.455101412655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.90668981142" Y="-0.971118123642" />
                  <Point X="28.712013612512" Y="-0.489277623049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.424409012907" Y="0.222568740413" />
                  <Point X="27.683862305874" Y="2.055486159332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.300115689456" Y="3.00529236467" />
                  <Point X="26.696761666661" Y="4.498645974447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.798472826845" Y="-0.956871068245" />
                  <Point X="28.586604220857" Y="-0.432477866913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.369367765587" Y="0.105201227609" />
                  <Point X="27.599199568918" Y="2.011434406103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.239837340813" Y="2.900887132501" />
                  <Point X="26.57670772115" Y="4.542190536239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.69025584227" Y="-0.942624012849" />
                  <Point X="28.424824435192" Y="-0.285658226708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.362318685535" Y="-0.130951067469" />
                  <Point X="27.507425647045" Y="1.984983453176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.179558992169" Y="2.796481900332" />
                  <Point X="26.45665377564" Y="4.58573509803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.582038857694" Y="-0.928376957453" />
                  <Point X="27.408536405002" Y="1.976143535657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.119280643525" Y="2.692076668163" />
                  <Point X="26.342951961257" Y="4.613557583577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.473821873119" Y="-0.914129902057" />
                  <Point X="27.299111158347" Y="1.993381144642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.059388867366" Y="2.586714635521" />
                  <Point X="26.229416498072" Y="4.640968335458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.073529182084" Y="-2.652056958816" />
                  <Point X="29.054472388055" Y="-2.604889738446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.369923759904" Y="-0.910572428387" />
                  <Point X="27.166305576766" Y="2.068487113231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.016483668766" Y="2.439309348075" />
                  <Point X="26.115881034888" Y="4.66837908734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.009856530249" Y="-2.74806099578" />
                  <Point X="28.905974068527" Y="-2.490942880471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.27526923558" Y="-0.929893640059" />
                  <Point X="26.002345571703" Y="4.695789839222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.88525917377" Y="-2.69327109723" />
                  <Point X="28.757475748999" Y="-2.376996022496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.181215084995" Y="-0.950700828878" />
                  <Point X="25.888810108519" Y="4.723200591103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.751626572884" Y="-2.616118184033" />
                  <Point X="28.608977429471" Y="-2.263049164521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.093839539513" Y="-0.988038145388" />
                  <Point X="25.778308057176" Y="4.743103385215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.617993971998" Y="-2.538965270836" />
                  <Point X="28.460479109944" Y="-2.149102306546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.019352485437" Y="-1.057275597538" />
                  <Point X="25.671320373037" Y="4.754307815263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.484361371113" Y="-2.461812357639" />
                  <Point X="28.311980790416" Y="-2.035155448571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.950397846054" Y="-1.140206256561" />
                  <Point X="25.564332688897" Y="4.765512245312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.350728770227" Y="-2.384659444442" />
                  <Point X="28.163482462023" Y="-1.921208568654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.888815089167" Y="-1.241382965037" />
                  <Point X="25.457345004758" Y="4.77671667536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.217096169342" Y="-2.307506531244" />
                  <Point X="28.006827411014" Y="-1.787073091824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.862829559718" Y="-1.43066590316" />
                  <Point X="25.367561085543" Y="4.745340293014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.083463568456" Y="-2.230353618047" />
                  <Point X="25.326705271369" Y="4.59286260112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.94983096757" Y="-2.15320070485" />
                  <Point X="25.285849457194" Y="4.440384909226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.818146122637" Y="-2.080868656804" />
                  <Point X="25.244993070743" Y="4.287908633766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.704989700588" Y="-2.054396064654" />
                  <Point X="25.189348542183" Y="4.172034294427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.594464156875" Y="-2.034435124886" />
                  <Point X="25.111795648586" Y="4.110385061371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.489004822316" Y="-2.027013492791" />
                  <Point X="25.019545309974" Y="4.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.397035225392" Y="-2.052980132972" />
                  <Point X="24.908223893608" Y="4.107044074959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.312540860536" Y="-2.097448621774" />
                  <Point X="24.537440820389" Y="4.771165004509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.228182643485" Y="-2.142254088215" />
                  <Point X="24.439606185422" Y="4.759714842882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.150805840971" Y="-2.204339161996" />
                  <Point X="24.341771550455" Y="4.748264681255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.089231800749" Y="-2.305537444974" />
                  <Point X="24.243936915488" Y="4.736814519628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.643921210078" Y="-3.932041290175" />
                  <Point X="27.558302433971" Y="-3.720127383026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.031415126542" Y="-2.416035535178" />
                  <Point X="24.146102280521" Y="4.725364358002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.427513553523" Y="-3.650012924901" />
                  <Point X="27.217002971747" Y="-3.128980951441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.007788943838" Y="-2.611158061413" />
                  <Point X="24.049485949817" Y="4.710898787511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.211105896969" Y="-3.367984559628" />
                  <Point X="23.95745037081" Y="4.685095458714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.994698240415" Y="-3.085956194354" />
                  <Point X="23.865414939153" Y="4.659291765215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.790489287128" Y="-2.834120679167" />
                  <Point X="23.773379507495" Y="4.633488071716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.65075298755" Y="-2.741860581578" />
                  <Point X="23.681344075838" Y="4.607684378217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.516676023702" Y="-2.663607831454" />
                  <Point X="23.589308644181" Y="4.581880684718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.407348702317" Y="-2.646612596018" />
                  <Point X="23.533361219168" Y="4.466756040408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.308560743328" Y="-2.655703197892" />
                  <Point X="23.507598956266" Y="4.276920498188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.20977278434" Y="-2.664793799766" />
                  <Point X="23.452645332174" Y="4.159336110281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.114958956485" Y="-2.683720721364" />
                  <Point X="23.375489832702" Y="4.096703292252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.032086451074" Y="-2.732203453154" />
                  <Point X="23.28830523894" Y="4.058893353649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.955390366104" Y="-2.795973361978" />
                  <Point X="23.197003372179" Y="4.031274023319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.878694286951" Y="-2.859743285201" />
                  <Point X="23.090176633689" Y="4.042080098907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.809405176762" Y="-2.941846099931" />
                  <Point X="22.962568425752" Y="4.104322116317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.765276143836" Y="-3.086222291125" />
                  <Point X="22.796353949751" Y="4.262118000272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.729435935344" Y="-3.251114042705" />
                  <Point X="22.712676127135" Y="4.21562849851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.756720576851" Y="-3.572245280641" />
                  <Point X="22.628998304518" Y="4.169138996747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.806245413928" Y="-3.948422934251" />
                  <Point X="25.723728949214" Y="-3.744187517246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.547649246663" Y="-3.308374960308" />
                  <Point X="22.545320481902" Y="4.122649494985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.360374818541" Y="-3.098453865725" />
                  <Point X="22.461642659285" Y="4.076159993223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.241241262678" Y="-3.057187348251" />
                  <Point X="22.377964836669" Y="4.029670491461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.124090332653" Y="-3.020828001924" />
                  <Point X="22.298021962308" Y="3.973936668372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.012312092714" Y="-2.997766530196" />
                  <Point X="22.219793464309" Y="3.91395961489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.915720722292" Y="-3.012293879552" />
                  <Point X="22.14156496631" Y="3.853982561407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.824676465477" Y="-3.040550816874" />
                  <Point X="22.063336468311" Y="3.794005507925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.733632208662" Y="-3.068807754196" />
                  <Point X="22.247741472212" Y="3.083987726623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.134738905888" Y="3.363678892933" />
                  <Point X="21.985107970312" Y="3.734028454442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.644635509346" Y="-3.102132574162" />
                  <Point X="22.218449660127" Y="2.902888125184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.567057378511" Y="-3.163719342862" />
                  <Point X="22.149064938379" Y="2.821021957369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.50156243279" Y="-3.255213044186" />
                  <Point X="22.072556004972" Y="2.756788832169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.436801126673" Y="-3.348522567249" />
                  <Point X="21.982540513619" Y="2.725985610978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.373721711641" Y="-3.445994916824" />
                  <Point X="21.879201450684" Y="2.728159386652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.329716319978" Y="-3.590677130881" />
                  <Point X="21.76689955109" Y="2.752516961507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.288860078837" Y="-3.743153765997" />
                  <Point X="21.634465802655" Y="2.826702610763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.248003899915" Y="-3.895630555108" />
                  <Point X="21.500833256742" Y="2.9038553879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.207147720992" Y="-4.048107344219" />
                  <Point X="21.367200710828" Y="2.981008165037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.16629154207" Y="-4.20058413333" />
                  <Point X="21.253428383705" Y="3.009005175739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.125435363148" Y="-4.353060922441" />
                  <Point X="21.185989664194" Y="2.922322483369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.084579184225" Y="-4.505537711552" />
                  <Point X="21.118550944683" Y="2.835639790999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.043723005303" Y="-4.658014500664" />
                  <Point X="21.570842986577" Y="1.462578323761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.442590630584" Y="1.780014044" />
                  <Point X="21.051112225172" Y="2.74895709863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.982718829303" Y="-4.760623247084" />
                  <Point X="23.876756673092" Y="-4.498357707288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.67791906587" Y="-4.006217359689" />
                  <Point X="21.518632441827" Y="1.338204576238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.294092047752" Y="1.893961553674" />
                  <Point X="20.985108031125" Y="2.658723831144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.869543624293" Y="-4.734104165476" />
                  <Point X="23.861131542341" Y="-4.713283532027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.519231624862" Y="-3.86705154109" />
                  <Point X="21.446387950072" Y="1.26341658757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.145593716567" Y="2.007908440502" />
                  <Point X="20.924559008599" Y="2.554988540342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.390145948874" Y="-3.80115266193" />
                  <Point X="21.359309937418" Y="1.22534285147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.997095385382" Y="2.121855327331" />
                  <Point X="20.864009953444" Y="2.451253330302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.283563384844" Y="-3.790950939339" />
                  <Point X="21.265123390944" Y="1.204863353973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.848597054196" Y="2.235802214159" />
                  <Point X="20.803460898288" Y="2.347518120261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.178315471054" Y="-3.784052592011" />
                  <Point X="22.686559636032" Y="-2.566914189657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.647364757197" Y="-2.469903460331" />
                  <Point X="21.693651593335" Y="-0.109380546525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.561413178729" Y="0.217921014982" />
                  <Point X="21.159015897033" Y="1.21388923676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.073067583116" Y="-3.777154308667" />
                  <Point X="22.652100132236" Y="-2.735223305278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.499752148629" Y="-2.358148813909" />
                  <Point X="21.635383535631" Y="-0.218761423371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.440617157081" Y="0.263302279666" />
                  <Point X="21.050798924407" Y="1.228136262578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.974443036874" Y="-3.786649371283" />
                  <Point X="22.591821728312" Y="-2.839628400622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.394970923106" Y="-2.352405560573" />
                  <Point X="22.033330953348" Y="-1.457315225755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.899937588665" Y="-1.127155062496" />
                  <Point X="21.55857235469" Y="-0.28224645967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.325717397461" Y="0.294089783718" />
                  <Point X="20.942581951782" Y="1.242383288397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.888186109116" Y="-3.826755363815" />
                  <Point X="22.531543324387" Y="-2.944033495967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.305133591946" Y="-2.383649743715" />
                  <Point X="21.970618336215" Y="-1.55569543199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.786492650964" Y="-1.099968369049" />
                  <Point X="21.470498602226" Y="-0.31785565326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.210817733368" Y="0.324877051334" />
                  <Point X="20.834364979157" Y="1.256630314215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.807505912744" Y="-3.880664250886" />
                  <Point X="22.471264920463" Y="-3.048438591312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.222052472977" Y="-2.431616138831" />
                  <Point X="21.893578945229" Y="-1.618615628606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.68773751998" Y="-1.109140223085" />
                  <Point X="21.378046535528" Y="-0.342628138847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.095918069276" Y="0.35566431895" />
                  <Point X="20.726148006532" Y="1.270877340034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.726825716372" Y="-3.934573137958" />
                  <Point X="22.410986516539" Y="-3.152843686656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.138971469857" Y="-2.479582820684" />
                  <Point X="21.815365816113" Y="-1.678630721411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.590451509374" Y="-1.121948277656" />
                  <Point X="21.28559446883" Y="-0.367400624435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.981018405183" Y="0.386451586565" />
                  <Point X="20.617931033907" Y="1.285124365852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.646154503102" Y="-3.988504258986" />
                  <Point X="22.350708112614" Y="-3.257248782001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.055890531952" Y="-2.527549663948" />
                  <Point X="21.737152686998" Y="-1.738645814217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.493165465073" Y="-1.134756248827" />
                  <Point X="21.193142402133" Y="-0.392173110022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.86611874109" Y="0.417238854181" />
                  <Point X="20.509714061282" Y="1.299371391671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.573563980667" Y="-4.062435791667" />
                  <Point X="22.29042970869" Y="-3.361653877346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.972809594047" Y="-2.575516507212" />
                  <Point X="21.658939557883" Y="-1.798660907022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.395879420771" Y="-1.147564219998" />
                  <Point X="21.100690335435" Y="-0.416945595609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.751219076997" Y="0.448026121797" />
                  <Point X="20.401497088656" Y="1.313618417489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.506444313022" Y="-4.149908165116" />
                  <Point X="22.230151304766" Y="-3.46605897269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.889728656142" Y="-2.623483350476" />
                  <Point X="21.580726428768" Y="-1.858675999828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.29859337647" Y="-1.160372191169" />
                  <Point X="21.008238268737" Y="-0.441718081197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.636319412904" Y="0.478813389412" />
                  <Point X="20.338616354821" Y="1.215654314696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.379791078557" Y="-4.090029789992" />
                  <Point X="22.169872900841" Y="-3.570464068035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.806647718237" Y="-2.67145019374" />
                  <Point X="21.502513299653" Y="-1.918691092633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.201307332168" Y="-1.17318016234" />
                  <Point X="20.915786202039" Y="-0.466490566784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.521419748811" Y="0.509600657028" />
                  <Point X="20.297483831332" Y="1.063861502389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.242327831545" Y="-4.003395694928" />
                  <Point X="22.109594496917" Y="-3.674869163379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.723566780331" Y="-2.719417037004" />
                  <Point X="21.424300170538" Y="-1.978706185438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.104021287867" Y="-1.185988133512" />
                  <Point X="20.823334135341" Y="-0.491263052372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.406520084718" Y="0.540387924644" />
                  <Point X="20.260443016978" Y="0.901941354594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.093599835766" Y="-3.888880368283" />
                  <Point X="22.049316092993" Y="-3.779274258724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.640485842426" Y="-2.767383880268" />
                  <Point X="21.346087041423" Y="-2.038721278244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.006735243565" Y="-1.198796104683" />
                  <Point X="20.730882068643" Y="-0.516035537959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.291620420625" Y="0.571175192259" />
                  <Point X="20.232976850058" Y="0.716323122807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.557404904521" Y="-2.815350723532" />
                  <Point X="21.267873912308" Y="-2.098736371049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.909449199264" Y="-1.211604075854" />
                  <Point X="20.638430001945" Y="-0.540808023547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.474323966616" Y="-2.863317566796" />
                  <Point X="21.189660783193" Y="-2.158751463855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.812163154962" Y="-1.224412047025" />
                  <Point X="20.545977935247" Y="-0.565580509134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.391243028711" Y="-2.91128441006" />
                  <Point X="21.111447654078" Y="-2.21876655666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.714877110661" Y="-1.237220018196" />
                  <Point X="20.453525868549" Y="-0.590352994722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.308162090806" Y="-2.959251253324" />
                  <Point X="21.033234524963" Y="-2.278781649466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.617591066359" Y="-1.250027989367" />
                  <Point X="20.361073801851" Y="-0.615125480309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.2250811529" Y="-3.007218096588" />
                  <Point X="20.955021395848" Y="-2.338796742271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.520305022058" Y="-1.262835960538" />
                  <Point X="20.268621735153" Y="-0.639897965897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.036292070821" Y="-2.793548101908" />
                  <Point X="20.876808266733" Y="-2.398811835077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.423018977756" Y="-1.275643931709" />
                  <Point X="20.236250723211" Y="-0.813376280249" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.9998359375" Y="0.001625976562" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.83447265625" Y="-4.891590820312" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521543701172" />
                  <Point X="25.31598046875" Y="-3.307814941406" />
                  <Point X="25.300591796875" Y="-3.285643310547" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.0447890625" Y="-3.195156982422" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766357422" />
                  <Point X="24.761791015625" Y="-3.259009033203" />
                  <Point X="24.7379765625" Y="-3.266399658203" />
                  <Point X="24.711720703125" Y="-3.285643554688" />
                  <Point X="24.5633828125" Y="-3.499372070312" />
                  <Point X="24.547994140625" Y="-3.521543945312" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.503123046875" Y="-3.677623535156" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.926453125" Y="-4.943247558594" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534756835938" />
                  <Point X="23.635478515625" Y="-4.259064941406" />
                  <Point X="23.6297890625" Y="-4.230465332031" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.402380859375" Y="-4.017290527344" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.070267578125" Y="-3.967378417969" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791992188" />
                  <Point X="22.776400390625" Y="-4.129959472656" />
                  <Point X="22.75215625" Y="-4.14616015625" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.71912890625" Y="-4.184841308594" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.183298828125" Y="-4.191839355469" />
                  <Point X="22.144166015625" Y="-4.167609863281" />
                  <Point X="21.77521875" Y="-3.883532470703" />
                  <Point X="21.771419921875" Y="-3.880607421875" />
                  <Point X="21.8195" Y="-3.797330566406" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597592773438" />
                  <Point X="22.486021484375" Y="-2.568763427734" />
                  <Point X="22.470302734375" Y="-2.553045410156" />
                  <Point X="22.46869140625" Y="-2.551434082031" />
                  <Point X="22.439857421875" Y="-2.537200439453" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="22.28712890625" Y="-2.613437744141" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.869212890625" Y="-2.887745849609" />
                  <Point X="20.83830078125" Y="-2.847134277344" />
                  <Point X="20.5737890625" Y="-2.403590087891" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="20.656115234375" Y="-2.328666259766" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396013549805" />
                  <Point X="21.861158203125" Y="-1.369062133789" />
                  <Point X="21.861880859375" Y="-1.366273803711" />
                  <Point X="21.85967578125" Y="-1.334598999023" />
                  <Point X="21.838841796875" Y="-1.310638916016" />
                  <Point X="21.81484765625" Y="-1.296517456055" />
                  <Point X="21.812365234375" Y="-1.295056274414" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.627484375" Y="-1.308712402344" />
                  <Point X="20.19671875" Y="-1.497076416016" />
                  <Point X="20.08469921875" Y="-1.058531982422" />
                  <Point X="20.072609375" Y="-1.01119140625" />
                  <Point X="20.002625" Y="-0.5218828125" />
                  <Point X="20.001603515625" Y="-0.514742492676" />
                  <Point X="20.098228515625" Y="-0.488852142334" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.45810546875" Y="-0.121425048828" />
                  <Point X="21.48325" Y="-0.103973358154" />
                  <Point X="21.4858515625" Y="-0.102167739868" />
                  <Point X="21.505103515625" Y="-0.075902435303" />
                  <Point X="21.513484375" Y="-0.04889692688" />
                  <Point X="21.514353515625" Y="-0.046095401764" />
                  <Point X="21.514353515625" Y="-0.016464675903" />
                  <Point X="21.50597265625" Y="0.010540828705" />
                  <Point X="21.505103515625" Y="0.013342353821" />
                  <Point X="21.485859375" Y="0.03960294342" />
                  <Point X="21.46071484375" Y="0.057054634094" />
                  <Point X="21.46070703125" Y="0.057059352875" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="21.30308203125" Y="0.103452529907" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.0745625" Y="0.943760253906" />
                  <Point X="20.08235546875" Y="0.996415588379" />
                  <Point X="20.2232265625" Y="1.516277099609" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.2831015625" Y="1.520845092773" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.32394921875" Y="1.413413696289" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056396484" />
                  <Point X="21.3608828125" Y="1.443786743164" />
                  <Point X="21.383212890625" Y="1.497698486328" />
                  <Point X="21.385529296875" Y="1.503291137695" />
                  <Point X="21.389287109375" Y="1.52460534668" />
                  <Point X="21.383685546875" Y="1.545510864258" />
                  <Point X="21.3567421875" Y="1.597270996094" />
                  <Point X="21.353939453125" Y="1.60265246582" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.2600390625" Y="1.680602294922" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.809708984375" Y="2.735134277344" />
                  <Point X="20.83998828125" Y="2.7870078125" />
                  <Point X="21.213150390625" Y="3.26665625" />
                  <Point X="21.225330078125" Y="3.282310791016" />
                  <Point X="21.249005859375" Y="3.268640869141" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.93899609375" Y="2.913653564453" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775634766" />
                  <Point X="21.986748046875" Y="2.927405517578" />
                  <Point X="22.041765625" Y="2.982421630859" />
                  <Point X="22.04747265625" Y="2.98812890625" />
                  <Point X="22.0591015625" Y="3.006381835938" />
                  <Point X="22.061927734375" Y="3.027839111328" />
                  <Point X="22.055146484375" Y="3.105347900391" />
                  <Point X="22.054443359375" Y="3.113388671875" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="22.012486328125" Y="3.195429199219" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="22.19439453125" Y="4.133903808594" />
                  <Point X="22.24712109375" Y="4.174329589844" />
                  <Point X="22.83484765625" Y="4.500857421875" />
                  <Point X="22.858451171875" Y="4.513971679688" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.135021484375" Y="4.228752441406" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186189453125" Y="4.222249511719" />
                  <Point X="23.276041015625" Y="4.259468261719" />
                  <Point X="23.28536328125" Y="4.263329101563" />
                  <Point X="23.303095703125" Y="4.275743652344" />
                  <Point X="23.31391796875" Y="4.294489257812" />
                  <Point X="23.34316015625" Y="4.387236816406" />
                  <Point X="23.3461953125" Y="4.396858886719" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.344052734375" Y="4.449036621094" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.963642578125" Y="4.884157714844" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.74440625" Y="4.986684082031" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.78277734375" Y="4.964318359375" />
                  <Point X="24.83107421875" Y="4.977258789062" />
                  <Point X="24.78277734375" Y="4.964317871094" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.0726171875" Y="4.3786875" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.800599609375" Y="4.931808105469" />
                  <Point X="25.86020703125" Y="4.925564941406" />
                  <Point X="26.44967578125" Y="4.78325" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.892978515625" Y="4.629588867188" />
                  <Point X="26.9310390625" Y="4.615784179688" />
                  <Point X="27.302041015625" Y="4.442279785156" />
                  <Point X="27.3386953125" Y="4.425137207031" />
                  <Point X="27.6971015625" Y="4.216328613281" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="28.0687421875" Y="3.956592285156" />
                  <Point X="28.0105" Y="3.85571484375" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491512451172" />
                  <Point X="27.205400390625" Y="2.418772216797" />
                  <Point X="27.202044921875" Y="2.392324707031" />
                  <Point X="27.20962890625" Y="2.329438476563" />
                  <Point X="27.2104140625" Y="2.322913330078" />
                  <Point X="27.218681640625" Y="2.3008125" />
                  <Point X="27.2576015625" Y="2.243454101562" />
                  <Point X="27.257607421875" Y="2.243446533203" />
                  <Point X="27.2749453125" Y="2.224201416016" />
                  <Point X="27.332302734375" Y="2.18528125" />
                  <Point X="27.33825390625" Y="2.181243896484" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.423236328125" Y="2.165395507813" />
                  <Point X="27.448662109375" Y="2.165945800781" />
                  <Point X="27.52140234375" Y="2.185397705078" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="27.68130078125" Y="2.273400390625" />
                  <Point X="28.994248046875" Y="3.031430908203" />
                  <Point X="29.18158203125" Y="2.771079589844" />
                  <Point X="29.20258984375" Y="2.741886230469" />
                  <Point X="29.387513671875" Y="2.436295166016" />
                  <Point X="29.31641015625" Y="2.381735595703" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583833374023" />
                  <Point X="28.22701953125" Y="1.515536987305" />
                  <Point X="28.227017578125" Y="1.515532714844" />
                  <Point X="28.21312109375" Y="1.491502929688" />
                  <Point X="28.193619140625" Y="1.421772094727" />
                  <Point X="28.191595703125" Y="1.414538452148" />
                  <Point X="28.190779296875" Y="1.390968383789" />
                  <Point X="28.2067890625" Y="1.313384033203" />
                  <Point X="28.2156484375" Y="1.287954956055" />
                  <Point X="28.259185546875" Y="1.221780151367" />
                  <Point X="28.263701171875" Y="1.214914672852" />
                  <Point X="28.280947265625" Y="1.198820556641" />
                  <Point X="28.344044921875" Y="1.163302612305" />
                  <Point X="28.36856640625" Y="1.153619506836" />
                  <Point X="28.453876953125" Y="1.142344482422" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.609046875" Y="1.158713623047" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.9301640625" Y="0.988453857422" />
                  <Point X="29.939193359375" Y="0.951366027832" />
                  <Point X="29.997861328125" Y="0.574556518555" />
                  <Point X="29.919041015625" Y="0.553436767578" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819961548" />
                  <Point X="28.6452734375" Y="0.184373214722" />
                  <Point X="28.622265625" Y="0.166926239014" />
                  <Point X="28.5719765625" Y="0.102846038818" />
                  <Point X="28.566759765625" Y="0.096198562622" />
                  <Point X="28.556986328125" Y="0.074731613159" />
                  <Point X="28.54022265625" Y="-0.01279867363" />
                  <Point X="28.538484375" Y="-0.040681129456" />
                  <Point X="28.555248046875" Y="-0.128211410522" />
                  <Point X="28.556986328125" Y="-0.137291687012" />
                  <Point X="28.566759765625" Y="-0.158758636475" />
                  <Point X="28.617048828125" Y="-0.222838684082" />
                  <Point X="28.636578125" Y="-0.241907516479" />
                  <Point X="28.720392578125" Y="-0.290354278564" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="28.863359375" Y="-0.333127838135" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.953470703125" Y="-0.932980224609" />
                  <Point X="29.948431640625" Y="-0.966410705566" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.777982421875" Y="-1.277465576172" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.230337890625" Y="-1.134096069336" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.1854453125" Y="-1.154697631836" />
                  <Point X="28.086015625" Y="-1.274280273438" />
                  <Point X="28.075701171875" Y="-1.286685546875" />
                  <Point X="28.064359375" Y="-1.314070068359" />
                  <Point X="28.050109375" Y="-1.468934936523" />
                  <Point X="28.048630859375" Y="-1.485000366211" />
                  <Point X="28.05636328125" Y="-1.516624633789" />
                  <Point X="28.1473984375" Y="-1.658225708008" />
                  <Point X="28.168462890625" Y="-1.685540771484" />
                  <Point X="28.28185546875" Y="-1.77255065918" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.2183359375" Y="-2.779158203125" />
                  <Point X="29.204125" Y="-2.802154296875" />
                  <Point X="29.056689453125" Y="-3.011639160156" />
                  <Point X="28.9700625" Y="-2.961624511719" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.5415625" Y="-2.217955078125" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219245605469" />
                  <Point X="27.326431640625" Y="-2.304844726562" />
                  <Point X="27.309560546875" Y="-2.313724609375" />
                  <Point X="27.2886015625" Y="-2.334681640625" />
                  <Point X="27.203001953125" Y="-2.497327148438" />
                  <Point X="27.19412109375" Y="-2.514199707031" />
                  <Point X="27.1891640625" Y="-2.546374511719" />
                  <Point X="27.2245234375" Y="-2.742155029297" />
                  <Point X="27.22819140625" Y="-2.76246484375" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.306958984375" Y="-2.904787597656" />
                  <Point X="27.98667578125" Y="-4.082088378906" />
                  <Point X="27.85215625" Y="-4.178172363281" />
                  <Point X="27.835294921875" Y="-4.190215332031" />
                  <Point X="27.67977734375" Y="-4.290879394531" />
                  <Point X="27.6115625" Y="-4.20198046875" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.47745703125" Y="-2.856326660156" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.214626953125" Y="-2.855149902344" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509277344" />
                  <Point X="26.002265625" Y="-3.004094482422" />
                  <Point X="25.985349609375" Y="-3.018159667969" />
                  <Point X="25.96845703125" Y="-3.045984863281" />
                  <Point X="25.919701171875" Y="-3.270302246094" />
                  <Point X="25.914642578125" Y="-3.293572753906" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.934580078125" Y="-3.467571533203" />
                  <Point X="26.127642578125" Y="-4.934028320312" />
                  <Point X="26.010287109375" Y="-4.959752441406" />
                  <Point X="25.994330078125" Y="-4.963250488281" />
                  <Point X="25.860201171875" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#208" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.175081857811" Y="5.008538895752" Z="2.3" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.3" />
                  <Point X="-0.267562846851" Y="5.067625162536" Z="2.3" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.3" />
                  <Point X="-1.056011556553" Y="4.963581276655" Z="2.3" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.3" />
                  <Point X="-1.711675831429" Y="4.47379101261" Z="2.3" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.3" />
                  <Point X="-1.710679761189" Y="4.433558384292" Z="2.3" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.3" />
                  <Point X="-1.749246320335" Y="4.336943110796" Z="2.3" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.3" />
                  <Point X="-1.848048242248" Y="4.304383266851" Z="2.3" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.3" />
                  <Point X="-2.115494499671" Y="4.58540888818" Z="2.3" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.3" />
                  <Point X="-2.19559270463" Y="4.575844749492" Z="2.3" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.3" />
                  <Point X="-2.842184969013" Y="4.204863058547" Z="2.3" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.3" />
                  <Point X="-3.036971817282" Y="3.201709137485" Z="2.3" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.3" />
                  <Point X="-3.000821215918" Y="3.132272282341" Z="2.3" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.3" />
                  <Point X="-2.999747307384" Y="3.049056260502" Z="2.3" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.3" />
                  <Point X="-3.062804183266" Y="2.994743483255" Z="2.3" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.3" />
                  <Point X="-3.732150210812" Y="3.343222131464" Z="2.3" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.3" />
                  <Point X="-3.832469669063" Y="3.328638930847" Z="2.3" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.3" />
                  <Point X="-4.239629434203" Y="2.791619920246" Z="2.3" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.3" />
                  <Point X="-3.776554861353" Y="1.672214573506" Z="2.3" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.3" />
                  <Point X="-3.693767095323" Y="1.605464601615" Z="2.3" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.3" />
                  <Point X="-3.669139004608" Y="1.548111683173" Z="2.3" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.3" />
                  <Point X="-3.697243240715" Y="1.492379787339" Z="2.3" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.3" />
                  <Point X="-4.716530088512" Y="1.601697398357" Z="2.3" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.3" />
                  <Point X="-4.831189525224" Y="1.560634153161" Z="2.3" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.3" />
                  <Point X="-4.981054388172" Y="0.982360130902" Z="2.3" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.3" />
                  <Point X="-3.716017224558" Y="0.086436286216" Z="2.3" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.3" />
                  <Point X="-3.573952293499" Y="0.047258624225" Z="2.3" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.3" />
                  <Point X="-3.547938363747" Y="0.027005450117" Z="2.3" />
                  <Point X="-3.539556741714" Y="0" Z="2.3" />
                  <Point X="-3.540426237388" Y="-0.002801500944" Z="2.3" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.3" />
                  <Point X="-3.551416301624" Y="-0.031617358909" Z="2.3" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.3" />
                  <Point X="-4.920870564215" Y="-0.40927577054" Z="2.3" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.3" />
                  <Point X="-5.053027430509" Y="-0.497681215694" Z="2.3" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.3" />
                  <Point X="-4.969902039786" Y="-1.039624501403" Z="2.3" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.3" />
                  <Point X="-3.372147617408" Y="-1.327004652437" Z="2.3" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.3" />
                  <Point X="-3.216669838293" Y="-1.308328254216" Z="2.3" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.3" />
                  <Point X="-3.193400437328" Y="-1.32524570409" Z="2.3" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.3" />
                  <Point X="-4.380479141201" Y="-2.257718436053" Z="2.3" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.3" />
                  <Point X="-4.475310773542" Y="-2.397919649902" Z="2.3" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.3" />
                  <Point X="-4.176730078599" Y="-2.88674935967" Z="2.3" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.3" />
                  <Point X="-2.694027794091" Y="-2.62545906471" Z="2.3" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.3" />
                  <Point X="-2.571208942036" Y="-2.557121527095" Z="2.3" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.3" />
                  <Point X="-3.229957948458" Y="-3.741050588401" Z="2.3" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.3" />
                  <Point X="-3.26144253424" Y="-3.891869903608" Z="2.3" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.3" />
                  <Point X="-2.849181077579" Y="-4.203070643405" Z="2.3" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.3" />
                  <Point X="-2.247359792249" Y="-4.183999115925" Z="2.3" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.3" />
                  <Point X="-2.201976496723" Y="-4.140251642886" Z="2.3" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.3" />
                  <Point X="-1.939158277608" Y="-3.985991535096" Z="2.3" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.3" />
                  <Point X="-1.63674308558" Y="-4.023602585412" Z="2.3" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.3" />
                  <Point X="-1.419717115145" Y="-4.237540330011" Z="2.3" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.3" />
                  <Point X="-1.408566892278" Y="-4.845078105354" Z="2.3" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.3" />
                  <Point X="-1.385307009081" Y="-4.886653900794" Z="2.3" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.3" />
                  <Point X="-1.089230409292" Y="-4.961051746018" Z="2.3" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.3" />
                  <Point X="-0.454736685573" Y="-3.659284449193" Z="2.3" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.3" />
                  <Point X="-0.401698312223" Y="-3.496601230557" Z="2.3" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.3" />
                  <Point X="-0.229546321777" Y="-3.275482075668" Z="2.3" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.3" />
                  <Point X="0.023812757584" Y="-3.21162996962" Z="2.3" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.3" />
                  <Point X="0.268747547209" Y="-3.305044525211" Z="2.3" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.3" />
                  <Point X="0.780018174828" Y="-4.873251580623" Z="2.3" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.3" />
                  <Point X="0.834618103204" Y="-5.010683750354" Z="2.3" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.3" />
                  <Point X="1.014838066144" Y="-4.977312709439" Z="2.3" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.3" />
                  <Point X="0.977995661709" Y="-3.429765071398" Z="2.3" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.3" />
                  <Point X="0.962403685218" Y="-3.249643380016" Z="2.3" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.3" />
                  <Point X="1.028076358918" Y="-3.011260629196" Z="2.3" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.3" />
                  <Point X="1.213051145272" Y="-2.8736595015" Z="2.3" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.3" />
                  <Point X="1.444261539678" Y="-2.867104724982" Z="2.3" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.3" />
                  <Point X="2.565737666781" Y="-4.201138307297" Z="2.3" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.3" />
                  <Point X="2.680395691756" Y="-4.314773651907" Z="2.3" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.3" />
                  <Point X="2.875059809325" Y="-4.187579713786" Z="2.3" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.3" />
                  <Point X="2.344103645653" Y="-2.848507481002" Z="2.3" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.3" />
                  <Point X="2.267568934327" Y="-2.701988675889" Z="2.3" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.3" />
                  <Point X="2.241091029694" Y="-2.489335613581" Z="2.3" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.3" />
                  <Point X="2.343562764791" Y="-2.317810280273" Z="2.3" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.3" />
                  <Point X="2.526518127624" Y="-2.235879075164" Z="2.3" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.3" />
                  <Point X="3.938906116105" Y="-2.973645854622" Z="2.3" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.3" />
                  <Point X="4.081525994341" Y="-3.023194805725" Z="2.3" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.3" />
                  <Point X="4.254712077643" Y="-2.774163917398" Z="2.3" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.3" />
                  <Point X="3.306136336338" Y="-1.701602942527" Z="2.3" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.3" />
                  <Point X="3.183298862798" Y="-1.59990356717" Z="2.3" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.3" />
                  <Point X="3.093740841843" Y="-1.44223707401" Z="2.3" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.3" />
                  <Point X="3.118306102067" Y="-1.274966915628" Z="2.3" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.3" />
                  <Point X="3.234800429135" Y="-1.151675056278" Z="2.3" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.3" />
                  <Point X="4.765300121862" Y="-1.295757757065" Z="2.3" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.3" />
                  <Point X="4.914942317134" Y="-1.279639006519" Z="2.3" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.3" />
                  <Point X="4.996755932335" Y="-0.909152639149" Z="2.3" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.3" />
                  <Point X="3.870143194204" Y="-0.25355147971" Z="2.3" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.3" />
                  <Point X="3.73925785497" Y="-0.215784860795" Z="2.3" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.3" />
                  <Point X="3.650225807876" Y="-0.160690642134" Z="2.3" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.3" />
                  <Point X="3.598197754771" Y="-0.087530283664" Z="2.3" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.3" />
                  <Point X="3.583173695653" Y="0.009080247515" Z="2.3" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.3" />
                  <Point X="3.605153630524" Y="0.103258096441" Z="2.3" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.3" />
                  <Point X="3.664137559383" Y="0.172363983447" Z="2.3" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.3" />
                  <Point X="4.925824623103" Y="0.536420453059" Z="2.3" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.3" />
                  <Point X="5.041821087001" Y="0.608944543603" Z="2.3" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.3" />
                  <Point X="4.972589305268" Y="1.031560581445" Z="2.3" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.3" />
                  <Point X="3.596365071641" Y="1.239565948829" Z="2.3" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.3" />
                  <Point X="3.454271412851" Y="1.223193718625" Z="2.3" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.3" />
                  <Point X="3.36241508817" Y="1.238153146198" Z="2.3" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.3" />
                  <Point X="3.294801710001" Y="1.280536432323" Z="2.3" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.3" />
                  <Point X="3.249600322321" Y="1.354764874685" Z="2.3" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.3" />
                  <Point X="3.235615030407" Y="1.439582957712" Z="2.3" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.3" />
                  <Point X="3.260546826895" Y="1.516398662658" Z="2.3" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.3" />
                  <Point X="4.340690402616" Y="2.373347800183" Z="2.3" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.3" />
                  <Point X="4.427656328182" Y="2.487642336438" Z="2.3" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.3" />
                  <Point X="4.216010020457" Y="2.831564161416" Z="2.3" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.3" />
                  <Point X="2.650144297997" Y="2.347981594241" Z="2.3" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.3" />
                  <Point X="2.50233196065" Y="2.264980879976" Z="2.3" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.3" />
                  <Point X="2.423066539097" Y="2.246315983013" Z="2.3" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.3" />
                  <Point X="2.354216517416" Y="2.257938029164" Z="2.3" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.3" />
                  <Point X="2.292820571071" Y="2.302808342966" Z="2.3" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.3" />
                  <Point X="2.253113719746" Y="2.366691904736" Z="2.3" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.3" />
                  <Point X="2.247547062681" Y="2.437137615232" Z="2.3" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.3" />
                  <Point X="3.047643758618" Y="3.861994783958" Z="2.3" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.3" />
                  <Point X="3.093368918636" Y="4.027334466271" Z="2.3" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.3" />
                  <Point X="2.716115440868" Y="4.290811073191" Z="2.3" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.3" />
                  <Point X="2.317064829412" Y="4.518851111919" Z="2.3" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.3" />
                  <Point X="1.903870687266" Y="4.707872974747" Z="2.3" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.3" />
                  <Point X="1.455252497482" Y="4.863133407318" Z="2.3" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.3" />
                  <Point X="0.799651051753" Y="5.012816077128" Z="2.3" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.3" />
                  <Point X="0.018162703742" Y="4.422908610199" Z="2.3" />
                  <Point X="0" Y="4.355124473572" Z="2.3" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>