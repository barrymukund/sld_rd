<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#203" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3151" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.902580078125" Y="-4.778716796875" />
                  <Point X="25.5633046875" Y="-3.512525146484" />
                  <Point X="25.55772265625" Y="-3.497142333984" />
                  <Point X="25.542365234375" Y="-3.467376220703" />
                  <Point X="25.4036953125" Y="-3.267578613281" />
                  <Point X="25.37863671875" Y="-3.231475830078" />
                  <Point X="25.35675" Y="-3.209018798828" />
                  <Point X="25.330494140625" Y="-3.189775634766" />
                  <Point X="25.30249609375" Y="-3.175668945312" />
                  <Point X="25.087912109375" Y="-3.109069824219" />
                  <Point X="25.04913671875" Y="-3.097035644531" />
                  <Point X="25.0209765625" Y="-3.092766357422" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.748591796875" Y="-3.163634765625" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.681814453125" Y="-3.189778076172" />
                  <Point X="24.65555859375" Y="-3.209022705078" />
                  <Point X="24.63367578125" Y="-3.231477539062" />
                  <Point X="24.495005859375" Y="-3.431274902344" />
                  <Point X="24.46994921875" Y="-3.467377929688" />
                  <Point X="24.4618125" Y="-3.481571289062" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.387703125" Y="-3.741322509766" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.962255859375" Y="-4.853424316406" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.753583984375" Y="-4.802362304688" />
                  <Point X="23.7850390625" Y="-4.563438964844" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.7322265625" Y="-4.258501464844" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.18296484375" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.478794921875" Y="-3.957946533203" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.094763671875" Y="-3.873780273438" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.016580078125" Y="-3.873708984375" />
                  <Point X="22.98553125" Y="-3.882029052734" />
                  <Point X="22.957341796875" Y="-3.894802001953" />
                  <Point X="22.73885546875" Y="-4.040790283203" />
                  <Point X="22.699376953125" Y="-4.067170166016" />
                  <Point X="22.68721484375" Y="-4.076822021484" />
                  <Point X="22.664900390625" Y="-4.099460449219" />
                  <Point X="22.63048046875" Y="-4.144315429688" />
                  <Point X="22.519853515625" Y="-4.288489257813" />
                  <Point X="22.259255859375" Y="-4.127134765625" />
                  <Point X="22.19828515625" Y="-4.0893828125" />
                  <Point X="21.89527734375" Y="-3.856077392578" />
                  <Point X="21.945734375" Y="-3.768685058594" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647654785156" />
                  <Point X="22.593412109375" Y="-2.616128662109" />
                  <Point X="22.59442578125" Y="-2.585194335938" />
                  <Point X="22.58544140625" Y="-2.555576171875" />
                  <Point X="22.571224609375" Y="-2.526747314453" />
                  <Point X="22.553197265625" Y="-2.501590087891" />
                  <Point X="22.53850390625" Y="-2.486896484375" />
                  <Point X="22.535853515625" Y="-2.48424609375" />
                  <Point X="22.510705078125" Y="-2.466222412109" />
                  <Point X="22.481875" Y="-2.452000976562" />
                  <Point X="22.45225390625" Y="-2.443012939453" />
                  <Point X="22.42131640625" Y="-2.444023925781" />
                  <Point X="22.389787109375" Y="-2.450294433594" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="22.163484375" Y="-2.575128173828" />
                  <Point X="21.181978515625" Y="-3.141800537109" />
                  <Point X="20.965306640625" Y="-2.857139648438" />
                  <Point X="20.917134765625" Y="-2.793850830078" />
                  <Point X="20.693857421875" Y="-2.419449707031" />
                  <Point X="20.7908671875" Y="-2.34501171875" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.915421875" Y="-1.475592895508" />
                  <Point X="21.93338671875" Y="-1.448461547852" />
                  <Point X="21.94614453125" Y="-1.419830688477" />
                  <Point X="21.95266796875" Y="-1.394641357422" />
                  <Point X="21.9538359375" Y="-1.390139648438" />
                  <Point X="21.95665234375" Y="-1.35966796875" />
                  <Point X="21.9544453125" Y="-1.327995361328" />
                  <Point X="21.947447265625" Y="-1.29824987793" />
                  <Point X="21.931365234375" Y="-1.272265258789" />
                  <Point X="21.910533203125" Y="-1.248306518555" />
                  <Point X="21.88703125" Y="-1.228768188477" />
                  <Point X="21.8646015625" Y="-1.215567016602" />
                  <Point X="21.860548828125" Y="-1.213181518555" />
                  <Point X="21.831287109375" Y="-1.201957885742" />
                  <Point X="21.7993984375" Y="-1.195475219727" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="21.51895703125" Y="-1.227180419922" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.18476171875" Y="-1.066406860352" />
                  <Point X="20.16592578125" Y="-0.992653991699" />
                  <Point X="20.107578125" Y="-0.584698242188" />
                  <Point X="20.210439453125" Y="-0.557136230469" />
                  <Point X="21.467125" Y="-0.220408279419" />
                  <Point X="21.482505859375" Y="-0.214828155518" />
                  <Point X="21.512267578125" Y="-0.199471679688" />
                  <Point X="21.5357734375" Y="-0.183157546997" />
                  <Point X="21.540021484375" Y="-0.180209625244" />
                  <Point X="21.562474609375" Y="-0.158329956055" />
                  <Point X="21.581720703125" Y="-0.132074935913" />
                  <Point X="21.595833984375" Y="-0.104065368652" />
                  <Point X="21.60366796875" Y="-0.078820198059" />
                  <Point X="21.605083984375" Y="-0.074258293152" />
                  <Point X="21.6093515625" Y="-0.046102851868" />
                  <Point X="21.6093515625" Y="-0.01645728302" />
                  <Point X="21.605083984375" Y="0.011698309898" />
                  <Point X="21.59725" Y="0.036943473816" />
                  <Point X="21.595833984375" Y="0.04150522995" />
                  <Point X="21.581720703125" Y="0.06951449585" />
                  <Point X="21.562474609375" Y="0.095769676208" />
                  <Point X="21.540021484375" Y="0.117649650574" />
                  <Point X="21.516515625" Y="0.133963775635" />
                  <Point X="21.505720703125" Y="0.140441421509" />
                  <Point X="21.485908203125" Y="0.150607589722" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="21.240044921875" Y="0.218694366455" />
                  <Point X="20.108185546875" Y="0.521975463867" />
                  <Point X="20.163373046875" Y="0.894936218262" />
                  <Point X="20.17551171875" Y="0.976968444824" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.33351171875" Y="1.418388549805" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255017578125" Y="1.299342041016" />
                  <Point X="21.276578125" Y="1.301228637695" />
                  <Point X="21.29686328125" Y="1.305263793945" />
                  <Point X="21.348888671875" Y="1.321667114258" />
                  <Point X="21.3582890625" Y="1.324631225586" />
                  <Point X="21.37722265625" Y="1.332961791992" />
                  <Point X="21.39596875" Y="1.343784790039" />
                  <Point X="21.41265234375" Y="1.356017700195" />
                  <Point X="21.426291015625" Y="1.371571899414" />
                  <Point X="21.438705078125" Y="1.389303100586" />
                  <Point X="21.44865234375" Y="1.407434204102" />
                  <Point X="21.46952734375" Y="1.457831787109" />
                  <Point X="21.473298828125" Y="1.466938476562" />
                  <Point X="21.479087890625" Y="1.486805908203" />
                  <Point X="21.48284375" Y="1.508119628906" />
                  <Point X="21.4841953125" Y="1.528751464844" />
                  <Point X="21.48105078125" Y="1.549186889648" />
                  <Point X="21.475451171875" Y="1.570091674805" />
                  <Point X="21.467953125" Y="1.589375976562" />
                  <Point X="21.442765625" Y="1.637762451172" />
                  <Point X="21.43819921875" Y="1.646528930664" />
                  <Point X="21.4267109375" Y="1.663717041016" />
                  <Point X="21.41280078125" Y="1.680291870117" />
                  <Point X="21.39786328125" Y="1.694590332031" />
                  <Point X="21.267609375" Y="1.794537841797" />
                  <Point X="20.648140625" Y="2.269873535156" />
                  <Point X="20.871677734375" Y="2.652847412109" />
                  <Point X="20.9188515625" Y="2.7336640625" />
                  <Point X="21.24949609375" Y="3.158662353516" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.9256640625" Y="2.819457275391" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.00098828125" Y="2.826504882812" />
                  <Point X="22.0195390625" Y="2.835654541016" />
                  <Point X="22.03779296875" Y="2.847284179688" />
                  <Point X="22.053921875" Y="2.860229003906" />
                  <Point X="22.105353515625" Y="2.911659179688" />
                  <Point X="22.114646484375" Y="2.920952392578" />
                  <Point X="22.12759375" Y="2.937082763672" />
                  <Point X="22.13922265625" Y="2.955335449219" />
                  <Point X="22.14837109375" Y="2.973885253906" />
                  <Point X="22.153287109375" Y="2.993975341797" />
                  <Point X="22.15611328125" Y="3.015431884766" />
                  <Point X="22.15656640625" Y="3.036118408203" />
                  <Point X="22.1502265625" Y="3.108575195312" />
                  <Point X="22.14908203125" Y="3.12166796875" />
                  <Point X="22.145046875" Y="3.141955078125" />
                  <Point X="22.138537109375" Y="3.162600830078" />
                  <Point X="22.130205078125" Y="3.181533691406" />
                  <Point X="22.072484375" Y="3.281506591797" />
                  <Point X="21.81666796875" Y="3.724595947266" />
                  <Point X="22.217228515625" Y="4.031701416016" />
                  <Point X="22.29937890625" Y="4.094686279297" />
                  <Point X="22.832962890625" Y="4.391133789062" />
                  <Point X="22.956802734375" Y="4.229743164063" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.085533203125" Y="4.147413574219" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.1193828125" Y="4.132331054688" />
                  <Point X="23.1402890625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.12358203125" />
                  <Point X="23.181373046875" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222546875" Y="4.134481445312" />
                  <Point X="23.30654296875" Y="4.169274414063" />
                  <Point X="23.321720703125" Y="4.175561035156" />
                  <Point X="23.339849609375" Y="4.185506347656" />
                  <Point X="23.357580078125" Y="4.197919433594" />
                  <Point X="23.3731328125" Y="4.211557128906" />
                  <Point X="23.385365234375" Y="4.228237792969" />
                  <Point X="23.3961875" Y="4.246979980469" />
                  <Point X="23.404521484375" Y="4.265918945313" />
                  <Point X="23.431859375" Y="4.352627441406" />
                  <Point X="23.43680078125" Y="4.368295410156" />
                  <Point X="23.4408359375" Y="4.388581542969" />
                  <Point X="23.44272265625" Y="4.410146972656" />
                  <Point X="23.442271484375" Y="4.430827636719" />
                  <Point X="23.435708984375" Y="4.480671386719" />
                  <Point X="23.415798828125" Y="4.631897460938" />
                  <Point X="23.944021484375" Y="4.779992675781" />
                  <Point X="24.050369140625" Y="4.80980859375" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.866095703125" Y="4.28631640625" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.14621484375" Y="4.286314453125" />
                  <Point X="25.175791015625" Y="4.396689453125" />
                  <Point X="25.307419921875" Y="4.8879375" />
                  <Point X="25.7511796875" Y="4.841463867188" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.388748046875" Y="4.70023046875" />
                  <Point X="26.48102734375" Y="4.677950683594" />
                  <Point X="26.8353515625" Y="4.549435058594" />
                  <Point X="26.894646484375" Y="4.527928222656" />
                  <Point X="27.23748046875" Y="4.367596191406" />
                  <Point X="27.294576171875" Y="4.340893554688" />
                  <Point X="27.625791015625" Y="4.147927734375" />
                  <Point X="27.680978515625" Y="4.115775390625" />
                  <Point X="27.94326171875" Y="3.929254882812" />
                  <Point X="27.87734375" Y="3.815081542969" />
                  <Point X="27.14758203125" Y="2.55109765625" />
                  <Point X="27.142078125" Y="2.53993359375" />
                  <Point X="27.133078125" Y="2.516056152344" />
                  <Point X="27.11489453125" Y="2.448057128906" />
                  <Point X="27.112697265625" Y="2.437450195312" />
                  <Point X="27.108072265625" Y="2.406260009766" />
                  <Point X="27.107728515625" Y="2.380953125" />
                  <Point X="27.114818359375" Y="2.322153320312" />
                  <Point X="27.116099609375" Y="2.311528320312" />
                  <Point X="27.12144140625" Y="2.289604736328" />
                  <Point X="27.12970703125" Y="2.267517089844" />
                  <Point X="27.140072265625" Y="2.247470214844" />
                  <Point X="27.17645703125" Y="2.193850585938" />
                  <Point X="27.183201171875" Y="2.185061035156" />
                  <Point X="27.203078125" Y="2.162070556641" />
                  <Point X="27.2216015625" Y="2.145592041016" />
                  <Point X="27.275220703125" Y="2.109208740234" />
                  <Point X="27.28491015625" Y="2.102634521484" />
                  <Point X="27.304955078125" Y="2.092271484375" />
                  <Point X="27.327041015625" Y="2.084006347656" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.407763671875" Y="2.071573242188" />
                  <Point X="27.419318359375" Y="2.070890136719" />
                  <Point X="27.44884765625" Y="2.070946777344" />
                  <Point X="27.47320703125" Y="2.074171142578" />
                  <Point X="27.54120703125" Y="2.092354980469" />
                  <Point X="27.547251953125" Y="2.094188720703" />
                  <Point X="27.57162109375" Y="2.102475830078" />
                  <Point X="27.58853515625" Y="2.110145263672" />
                  <Point X="27.81693359375" Y="2.24201171875" />
                  <Point X="28.967326171875" Y="2.906190673828" />
                  <Point X="29.09053515625" Y="2.734957763672" />
                  <Point X="29.123279296875" Y="2.689451416016" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="29.191583984375" Y="2.405698730469" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.22142578125" Y="1.660241821289" />
                  <Point X="28.20397265625" Y="1.64162512207" />
                  <Point X="28.155033203125" Y="1.577780517578" />
                  <Point X="28.149244140625" Y="1.569321533203" />
                  <Point X="28.131935546875" Y="1.540839233398" />
                  <Point X="28.121630859375" Y="1.517090087891" />
                  <Point X="28.103400390625" Y="1.451904296875" />
                  <Point X="28.10010546875" Y="1.440125610352" />
                  <Point X="28.09665234375" Y="1.417818603516" />
                  <Point X="28.095837890625" Y="1.394241577148" />
                  <Point X="28.097740234375" Y="1.371764404297" />
                  <Point X="28.112705078125" Y="1.299237304688" />
                  <Point X="28.115392578125" Y="1.289088500977" />
                  <Point X="28.125291015625" Y="1.258612182617" />
                  <Point X="28.13628125" Y="1.235743530273" />
                  <Point X="28.176984375" Y="1.173877197266" />
                  <Point X="28.184337890625" Y="1.162698242188" />
                  <Point X="28.198888671875" Y="1.145456420898" />
                  <Point X="28.216134765625" Y="1.12936340332" />
                  <Point X="28.23434765625" Y="1.116034790039" />
                  <Point X="28.29333203125" Y="1.08283190918" />
                  <Point X="28.303220703125" Y="1.077997070312" />
                  <Point X="28.33185546875" Y="1.065999511719" />
                  <Point X="28.356119140625" Y="1.059438598633" />
                  <Point X="28.435869140625" Y="1.04889831543" />
                  <Point X="28.44173828125" Y="1.048307250977" />
                  <Point X="28.4692265625" Y="1.046399658203" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.70516796875" Y="1.075548461914" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.831875" Y="0.990563049316" />
                  <Point X="29.84594140625" Y="0.932783813477" />
                  <Point X="29.890865234375" Y="0.644238708496" />
                  <Point X="29.817677734375" Y="0.624627807617" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.704791015625" Y="0.32558605957" />
                  <Point X="28.681546875" Y="0.315068023682" />
                  <Point X="28.6031953125" Y="0.269779266357" />
                  <Point X="28.595037109375" Y="0.264489532471" />
                  <Point X="28.56656640625" Y="0.243883972168" />
                  <Point X="28.54753125" Y="0.225575637817" />
                  <Point X="28.50051953125" Y="0.165672180176" />
                  <Point X="28.492025390625" Y="0.154848007202" />
                  <Point X="28.480302734375" Y="0.135570327759" />
                  <Point X="28.470529296875" Y="0.114109291077" />
                  <Point X="28.463681640625" Y="0.092604850769" />
                  <Point X="28.44801171875" Y="0.010779978752" />
                  <Point X="28.44665234375" Y="0.000891140282" />
                  <Point X="28.4438203125" Y="-0.032706439972" />
                  <Point X="28.4451796875" Y="-0.058554367065" />
                  <Point X="28.460849609375" Y="-0.140379241943" />
                  <Point X="28.463681640625" Y="-0.155164825439" />
                  <Point X="28.470529296875" Y="-0.176669570923" />
                  <Point X="28.480302734375" Y="-0.198130462646" />
                  <Point X="28.492025390625" Y="-0.217407836914" />
                  <Point X="28.539037109375" Y="-0.277311279297" />
                  <Point X="28.545958984375" Y="-0.285191650391" />
                  <Point X="28.568765625" Y="-0.308437866211" />
                  <Point X="28.589037109375" Y="-0.324155609131" />
                  <Point X="28.667388671875" Y="-0.369444396973" />
                  <Point X="28.6722578125" Y="-0.372073242188" />
                  <Point X="28.69849609375" Y="-0.385264282227" />
                  <Point X="28.716580078125" Y="-0.392149963379" />
                  <Point X="28.915546875" Y="-0.445462677002" />
                  <Point X="29.891474609375" Y="-0.706961853027" />
                  <Point X="29.862875" Y="-0.896655395508" />
                  <Point X="29.855025390625" Y="-0.948725219727" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="29.701345703125" Y="-1.171556274414" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.2208828125" Y="-1.038933105469" />
                  <Point X="28.193095703125" Y="-1.04497265625" />
                  <Point X="28.1639765625" Y="-1.056596801758" />
                  <Point X="28.1361484375" Y="-1.073489868164" />
                  <Point X="28.1123984375" Y="-1.093960571289" />
                  <Point X="28.01944921875" Y="-1.205748657227" />
                  <Point X="28.002654296875" Y="-1.225948608398" />
                  <Point X="27.9879296875" Y="-1.250336547852" />
                  <Point X="27.976587890625" Y="-1.27772277832" />
                  <Point X="27.969759765625" Y="-1.305366943359" />
                  <Point X="27.9564375" Y="-1.450137451172" />
                  <Point X="27.95403125" Y="-1.476297241211" />
                  <Point X="27.956349609375" Y="-1.507568847656" />
                  <Point X="27.96408203125" Y="-1.539189453125" />
                  <Point X="27.976453125" Y="-1.567997558594" />
                  <Point X="28.0615546875" Y="-1.700369018555" />
                  <Point X="28.07693359375" Y="-1.724288452148" />
                  <Point X="28.0869375" Y="-1.73724206543" />
                  <Point X="28.110630859375" Y="-1.760909057617" />
                  <Point X="28.295271484375" Y="-1.902589599609" />
                  <Point X="29.213123046875" Y="-2.606882080078" />
                  <Point X="29.146939453125" Y="-2.7139765625" />
                  <Point X="29.12480078125" Y="-2.749799804688" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="28.938263671875" Y="-2.833568359375" />
                  <Point X="27.800955078125" Y="-2.176943359375" />
                  <Point X="27.7861328125" Y="-2.170012207031" />
                  <Point X="27.754224609375" Y="-2.159825439453" />
                  <Point X="27.571205078125" Y="-2.126772460938" />
                  <Point X="27.538134765625" Y="-2.120799804688" />
                  <Point X="27.506783203125" Y="-2.120395751953" />
                  <Point X="27.474609375" Y="-2.125353759766" />
                  <Point X="27.444833984375" Y="-2.135177246094" />
                  <Point X="27.2927890625" Y="-2.215196777344" />
                  <Point X="27.26531640625" Y="-2.22965625" />
                  <Point X="27.242388671875" Y="-2.246546875" />
                  <Point X="27.2214296875" Y="-2.267504394531" />
                  <Point X="27.204533203125" Y="-2.2904375" />
                  <Point X="27.124513671875" Y="-2.442481445312" />
                  <Point X="27.110052734375" Y="-2.469955566406" />
                  <Point X="27.100228515625" Y="-2.499734375" />
                  <Point X="27.095271484375" Y="-2.531908935547" />
                  <Point X="27.09567578125" Y="-2.563259033203" />
                  <Point X="27.12873046875" Y="-2.746278320312" />
                  <Point X="27.134703125" Y="-2.779349365234" />
                  <Point X="27.13898828125" Y="-2.795139648438" />
                  <Point X="27.1518203125" Y="-2.826078613281" />
                  <Point X="27.270470703125" Y="-3.031587646484" />
                  <Point X="27.86128515625" Y="-4.054906982422" />
                  <Point X="27.808115234375" Y="-4.092885009766" />
                  <Point X="27.781845703125" Y="-4.111646972656" />
                  <Point X="27.701767578125" Y="-4.163481933594" />
                  <Point X="27.62641796875" Y="-4.065285400391" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.54141796875" Y="-2.784508056641" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.219685546875" Y="-2.759282958984" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397705078" />
                  <Point X="26.128978515625" Y="-2.780740966797" />
                  <Point X="26.10459765625" Y="-2.7954609375" />
                  <Point X="25.952158203125" Y="-2.922208496094" />
                  <Point X="25.92461328125" Y="-2.945111328125" />
                  <Point X="25.904142578125" Y="-2.968860839844" />
                  <Point X="25.88725" Y="-2.996687011719" />
                  <Point X="25.875625" Y="-3.025808349609" />
                  <Point X="25.830046875" Y="-3.235504394531" />
                  <Point X="25.821810546875" Y="-3.273396240234" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120361328" />
                  <Point X="25.8533671875" Y="-3.578525146484" />
                  <Point X="26.022064453125" Y="-4.859915039062" />
                  <Point X="26.00051953125" Y="-4.864637695312" />
                  <Point X="25.97566796875" Y="-4.870085449219" />
                  <Point X="25.929318359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.980357421875" Y="-4.760165039062" />
                  <Point X="23.94157421875" Y="-4.75263671875" />
                  <Point X="23.858755859375" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575838867188" />
                  <Point X="23.879923828125" Y="-4.568099121094" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688964844" />
                  <Point X="23.825400390625" Y="-4.239967285156" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.811873046875" Y="-4.178467773438" />
                  <Point X="23.800970703125" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135463867188" />
                  <Point X="23.778259765625" Y="-4.107626464844" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.74979296875" Y="-4.070937744141" />
                  <Point X="23.738994140625" Y="-4.059779541016" />
                  <Point X="23.54143359375" Y="-3.886521972656" />
                  <Point X="23.505734375" Y="-3.85521484375" />
                  <Point X="23.49326171875" Y="-3.845965332031" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.1009765625" Y="-3.778983642578" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.007267578125" Y="-3.779166503906" />
                  <Point X="22.991990234375" Y="-3.781946533203" />
                  <Point X="22.96094140625" Y="-3.790266601562" />
                  <Point X="22.946322265625" Y="-3.795497558594" />
                  <Point X="22.9181328125" Y="-3.808270507812" />
                  <Point X="22.9045625" Y="-3.8158125" />
                  <Point X="22.686076171875" Y="-3.96180078125" />
                  <Point X="22.64659765625" Y="-3.988180664062" />
                  <Point X="22.640322265625" Y="-3.992755859375" />
                  <Point X="22.619556640625" Y="-4.0101328125" />
                  <Point X="22.5972421875" Y="-4.032771240234" />
                  <Point X="22.589533203125" Y="-4.041626464844" />
                  <Point X="22.55511328125" Y="-4.086481445312" />
                  <Point X="22.496798828125" Y="-4.162478515625" />
                  <Point X="22.309267578125" Y="-4.046364013672" />
                  <Point X="22.252404296875" Y="-4.011154785156" />
                  <Point X="22.01913671875" Y="-3.831547363281" />
                  <Point X="22.028005859375" Y="-3.816185791016" />
                  <Point X="22.658509765625" Y="-2.724120361328" />
                  <Point X="22.6651484375" Y="-2.710085449219" />
                  <Point X="22.67605078125" Y="-2.681120605469" />
                  <Point X="22.680314453125" Y="-2.666189941406" />
                  <Point X="22.6865859375" Y="-2.634663818359" />
                  <Point X="22.688361328125" Y="-2.619239990234" />
                  <Point X="22.689375" Y="-2.588305664062" />
                  <Point X="22.6853359375" Y="-2.557617919922" />
                  <Point X="22.6763515625" Y="-2.527999755859" />
                  <Point X="22.67064453125" Y="-2.513558837891" />
                  <Point X="22.656427734375" Y="-2.484729980469" />
                  <Point X="22.6484453125" Y="-2.471412109375" />
                  <Point X="22.63041796875" Y="-2.446254882812" />
                  <Point X="22.620373046875" Y="-2.434415527344" />
                  <Point X="22.6056796875" Y="-2.419721923828" />
                  <Point X="22.591193359375" Y="-2.407029296875" />
                  <Point X="22.566044921875" Y="-2.389005615234" />
                  <Point X="22.552732421875" Y="-2.381024169922" />
                  <Point X="22.52390234375" Y="-2.366802734375" />
                  <Point X="22.509458984375" Y="-2.36109375" />
                  <Point X="22.479837890625" Y="-2.352105712891" />
                  <Point X="22.449150390625" Y="-2.348063720703" />
                  <Point X="22.418212890625" Y="-2.349074707031" />
                  <Point X="22.40278515625" Y="-2.350848632812" />
                  <Point X="22.371255859375" Y="-2.357119140625" />
                  <Point X="22.35632421875" Y="-2.361383056641" />
                  <Point X="22.32735546875" Y="-2.372285888672" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="22.115984375" Y="-2.492855712891" />
                  <Point X="21.2069140625" Y="-3.017707519531" />
                  <Point X="21.040900390625" Y="-2.799601318359" />
                  <Point X="20.99597265625" Y="-2.740575683594" />
                  <Point X="20.818734375" Y="-2.443373291016" />
                  <Point X="20.84869921875" Y="-2.420380371094" />
                  <Point X="21.951876953125" Y="-1.573882080078" />
                  <Point X="21.96351953125" Y="-1.56330847168" />
                  <Point X="21.984896484375" Y="-1.540387939453" />
                  <Point X="21.994630859375" Y="-1.528041137695" />
                  <Point X="22.012595703125" Y="-1.500909912109" />
                  <Point X="22.020162109375" Y="-1.487128173828" />
                  <Point X="22.032919921875" Y="-1.458497314453" />
                  <Point X="22.038111328125" Y="-1.443647705078" />
                  <Point X="22.044634765625" Y="-1.418458374023" />
                  <Point X="22.04843359375" Y="-1.39888293457" />
                  <Point X="22.05125" Y="-1.368411254883" />
                  <Point X="22.051421875" Y="-1.353064086914" />
                  <Point X="22.04921484375" Y="-1.321391357422" />
                  <Point X="22.046919921875" Y="-1.306239257812" />
                  <Point X="22.039921875" Y="-1.276493774414" />
                  <Point X="22.028228515625" Y="-1.248254516602" />
                  <Point X="22.012146484375" Y="-1.222269897461" />
                  <Point X="22.0030546875" Y="-1.209931152344" />
                  <Point X="21.98222265625" Y="-1.185972290039" />
                  <Point X="21.971265625" Y="-1.175254272461" />
                  <Point X="21.947763671875" Y="-1.155715942383" />
                  <Point X="21.93521875" Y="-1.146895996094" />
                  <Point X="21.9127890625" Y="-1.133694824219" />
                  <Point X="21.8945703125" Y="-1.124482421875" />
                  <Point X="21.86530859375" Y="-1.113258789062" />
                  <Point X="21.850212890625" Y="-1.108862060547" />
                  <Point X="21.81832421875" Y="-1.102379394531" />
                  <Point X="21.802705078125" Y="-1.100532714844" />
                  <Point X="21.77137890625" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="21.506556640625" Y="-1.132993164062" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.276806640625" Y="-1.042895385742" />
                  <Point X="20.259240234375" Y="-0.974117004395" />
                  <Point X="20.21355078125" Y="-0.654653991699" />
                  <Point X="20.23502734375" Y="-0.648899047852" />
                  <Point X="21.491712890625" Y="-0.312171081543" />
                  <Point X="21.4995234375" Y="-0.30971270752" />
                  <Point X="21.52606640625" Y="-0.299252166748" />
                  <Point X="21.555828125" Y="-0.283895721436" />
                  <Point X="21.56643359375" Y="-0.277516387939" />
                  <Point X="21.589939453125" Y="-0.261202209473" />
                  <Point X="21.606322265625" Y="-0.248247924805" />
                  <Point X="21.628775390625" Y="-0.226368270874" />
                  <Point X="21.63909375" Y="-0.21449508667" />
                  <Point X="21.65833984375" Y="-0.188240112305" />
                  <Point X="21.66655859375" Y="-0.174822875977" />
                  <Point X="21.680671875" Y="-0.146813415527" />
                  <Point X="21.68656640625" Y="-0.132220870972" />
                  <Point X="21.694400390625" Y="-0.106975799561" />
                  <Point X="21.69901171875" Y="-0.088494987488" />
                  <Point X="21.703279296875" Y="-0.060339572906" />
                  <Point X="21.7043515625" Y="-0.046102840424" />
                  <Point X="21.7043515625" Y="-0.016457332611" />
                  <Point X="21.703279296875" Y="-0.00222059989" />
                  <Point X="21.69901171875" Y="0.025934963226" />
                  <Point X="21.69581640625" Y="0.039853939056" />
                  <Point X="21.687982421875" Y="0.065099021912" />
                  <Point X="21.680671875" Y="0.084253540039" />
                  <Point X="21.66655859375" Y="0.11226285553" />
                  <Point X="21.65833984375" Y="0.125679489136" />
                  <Point X="21.63909375" Y="0.151934616089" />
                  <Point X="21.628775390625" Y="0.163807647705" />
                  <Point X="21.606322265625" Y="0.185687606812" />
                  <Point X="21.5941875" Y="0.195694381714" />
                  <Point X="21.570681640625" Y="0.212008575439" />
                  <Point X="21.54908984375" Y="0.224963882446" />
                  <Point X="21.52927734375" Y="0.235129974365" />
                  <Point X="21.520078125" Y="0.239249511719" />
                  <Point X="21.501294921875" Y="0.246490112305" />
                  <Point X="21.491712890625" Y="0.249611190796" />
                  <Point X="21.2646328125" Y="0.310457244873" />
                  <Point X="20.2145546875" Y="0.591824951172" />
                  <Point X="20.257349609375" Y="0.881030273438" />
                  <Point X="20.26866796875" Y="0.95752130127" />
                  <Point X="20.3664140625" Y="1.318236938477" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232265625" Y="1.204815551758" />
                  <Point X="21.252947265625" Y="1.204364624023" />
                  <Point X="21.263298828125" Y="1.204703735352" />
                  <Point X="21.284859375" Y="1.206590209961" />
                  <Point X="21.29511328125" Y="1.208054199219" />
                  <Point X="21.3153984375" Y="1.212089355469" />
                  <Point X="21.3254296875" Y="1.214660522461" />
                  <Point X="21.377455078125" Y="1.231063842773" />
                  <Point X="21.396548828125" Y="1.237676025391" />
                  <Point X="21.415482421875" Y="1.246006469727" />
                  <Point X="21.42472265625" Y="1.250689208984" />
                  <Point X="21.44346875" Y="1.261512207031" />
                  <Point X="21.452142578125" Y="1.267172607422" />
                  <Point X="21.468826171875" Y="1.279405395508" />
                  <Point X="21.48408203125" Y="1.293385009766" />
                  <Point X="21.497720703125" Y="1.308939086914" />
                  <Point X="21.50411328125" Y="1.317086303711" />
                  <Point X="21.51652734375" Y="1.334817626953" />
                  <Point X="21.521994140625" Y="1.343608398438" />
                  <Point X="21.53194140625" Y="1.361739501953" />
                  <Point X="21.536421875" Y="1.371079833984" />
                  <Point X="21.557296875" Y="1.421477416992" />
                  <Point X="21.564505859375" Y="1.440362182617" />
                  <Point X="21.570294921875" Y="1.460229614258" />
                  <Point X="21.572646484375" Y="1.470319213867" />
                  <Point X="21.57640234375" Y="1.49163293457" />
                  <Point X="21.577640625" Y="1.501909545898" />
                  <Point X="21.5789921875" Y="1.522541625977" />
                  <Point X="21.57808984375" Y="1.543199707031" />
                  <Point X="21.5749453125" Y="1.563635253906" />
                  <Point X="21.57281640625" Y="1.573767211914" />
                  <Point X="21.567216796875" Y="1.594672119141" />
                  <Point X="21.563994140625" Y="1.604518432617" />
                  <Point X="21.55649609375" Y="1.623802734375" />
                  <Point X="21.552220703125" Y="1.633240844727" />
                  <Point X="21.527033203125" Y="1.681627319336" />
                  <Point X="21.52701953125" Y="1.681650268555" />
                  <Point X="21.517181640625" Y="1.699319213867" />
                  <Point X="21.505693359375" Y="1.716507446289" />
                  <Point X="21.49948046875" Y="1.724787597656" />
                  <Point X="21.4855703125" Y="1.741362426758" />
                  <Point X="21.4784921875" Y="1.748918823242" />
                  <Point X="21.4635546875" Y="1.763217407227" />
                  <Point X="21.4556953125" Y="1.769958862305" />
                  <Point X="21.32544140625" Y="1.86990637207" />
                  <Point X="20.77238671875" Y="2.294281005859" />
                  <Point X="20.953724609375" Y="2.604958007812" />
                  <Point X="20.99771484375" Y="2.680321044922" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.74584375" Y="2.762398681641" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774015625" Y="2.749385742188" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.917384765625" Y="2.724818847656" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310546875" />
                  <Point X="22.023568359375" Y="2.734227294922" />
                  <Point X="22.04301171875" Y="2.7413046875" />
                  <Point X="22.0615625" Y="2.750454345703" />
                  <Point X="22.070583984375" Y="2.755533691406" />
                  <Point X="22.088837890625" Y="2.767163330078" />
                  <Point X="22.097255859375" Y="2.773195068359" />
                  <Point X="22.113384765625" Y="2.786139892578" />
                  <Point X="22.121095703125" Y="2.793052978516" />
                  <Point X="22.17252734375" Y="2.844483154297" />
                  <Point X="22.188732421875" Y="2.861486083984" />
                  <Point X="22.2016796875" Y="2.877616455078" />
                  <Point X="22.20771484375" Y="2.886037109375" />
                  <Point X="22.21934375" Y="2.904289794922" />
                  <Point X="22.224423828125" Y="2.913315429688" />
                  <Point X="22.233572265625" Y="2.931865234375" />
                  <Point X="22.2406484375" Y="2.951305175781" />
                  <Point X="22.245564453125" Y="2.971395263672" />
                  <Point X="22.24747265625" Y="2.981569580078" />
                  <Point X="22.250298828125" Y="3.003026123047" />
                  <Point X="22.25108984375" Y="3.0133515625" />
                  <Point X="22.25154296875" Y="3.034038085938" />
                  <Point X="22.251205078125" Y="3.044399169922" />
                  <Point X="22.244865234375" Y="3.116855957031" />
                  <Point X="22.242255859375" Y="3.140200683594" />
                  <Point X="22.238220703125" Y="3.160487792969" />
                  <Point X="22.235650390625" Y="3.170522949219" />
                  <Point X="22.229140625" Y="3.191168701172" />
                  <Point X="22.225490234375" Y="3.200866943359" />
                  <Point X="22.217158203125" Y="3.219799804688" />
                  <Point X="22.2124765625" Y="3.229034423828" />
                  <Point X="22.154755859375" Y="3.329007324219" />
                  <Point X="21.94061328125" Y="3.699915039062" />
                  <Point X="22.27503125" Y="3.956309570312" />
                  <Point X="22.351634765625" Y="4.015041748047" />
                  <Point X="22.807474609375" Y="4.268296386719" />
                  <Point X="22.88143359375" Y="4.171910644531" />
                  <Point X="22.888177734375" Y="4.16405078125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93491015625" Y="4.121895996094" />
                  <Point X="22.952111328125" Y="4.110403320312" />
                  <Point X="22.9610234375" Y="4.105128417969" />
                  <Point X="23.04166796875" Y="4.063147460938" />
                  <Point X="23.065673828125" Y="4.051286865234" />
                  <Point X="23.084951171875" Y="4.043790283203" />
                  <Point X="23.094794921875" Y="4.040568359375" />
                  <Point X="23.115701171875" Y="4.034966308594" />
                  <Point X="23.1258359375" Y="4.032834960938" />
                  <Point X="23.146279296875" Y="4.029687988281" />
                  <Point X="23.166947265625" Y="4.028785400391" />
                  <Point X="23.187587890625" Y="4.030138427734" />
                  <Point X="23.197869140625" Y="4.031378417969" />
                  <Point X="23.21918359375" Y="4.03513671875" />
                  <Point X="23.22926953125" Y="4.037488037109" />
                  <Point X="23.24912890625" Y="4.043276123047" />
                  <Point X="23.25890234375" Y="4.046713134766" />
                  <Point X="23.3428984375" Y="4.081506103516" />
                  <Point X="23.367412109375" Y="4.092270996094" />
                  <Point X="23.385541015625" Y="4.102216308594" />
                  <Point X="23.394333984375" Y="4.107683105469" />
                  <Point X="23.412064453125" Y="4.120096191406" />
                  <Point X="23.420212890625" Y="4.126490722656" />
                  <Point X="23.435765625" Y="4.140128417969" />
                  <Point X="23.4497421875" Y="4.155377929688" />
                  <Point X="23.461974609375" Y="4.17205859375" />
                  <Point X="23.467634765625" Y="4.180732910156" />
                  <Point X="23.47845703125" Y="4.199475097656" />
                  <Point X="23.483140625" Y="4.208716796875" />
                  <Point X="23.491474609375" Y="4.227655761719" />
                  <Point X="23.495125" Y="4.237353027344" />
                  <Point X="23.522462890625" Y="4.324061523438" />
                  <Point X="23.529974609375" Y="4.34976171875" />
                  <Point X="23.534009765625" Y="4.370047851562" />
                  <Point X="23.535474609375" Y="4.380301757812" />
                  <Point X="23.537361328125" Y="4.4018671875" />
                  <Point X="23.53769921875" Y="4.412219238281" />
                  <Point X="23.537248046875" Y="4.432899902344" />
                  <Point X="23.536458984375" Y="4.443228515625" />
                  <Point X="23.529896484375" Y="4.493072265625" />
                  <Point X="23.520736328125" Y="4.562654296875" />
                  <Point X="23.96966796875" Y="4.68851953125" />
                  <Point X="24.068830078125" Y="4.716320800781" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.77433203125" Y="4.261728515625" />
                  <Point X="24.779564453125" Y="4.247107910156" />
                  <Point X="24.79233984375" Y="4.218913085938" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.219974609375" Y="4.218916015625" />
                  <Point X="25.232748046875" Y="4.247108886719" />
                  <Point X="25.2379765625" Y="4.261725585938" />
                  <Point X="25.267552734375" Y="4.372100585938" />
                  <Point X="25.378189453125" Y="4.785006347656" />
                  <Point X="25.74128515625" Y="4.74698046875" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.366453125" Y="4.607883789062" />
                  <Point X="26.453595703125" Y="4.586844238281" />
                  <Point X="26.802958984375" Y="4.460127929688" />
                  <Point X="26.85826171875" Y="4.440069335938" />
                  <Point X="27.197236328125" Y="4.281541992188" />
                  <Point X="27.250453125" Y="4.256653320312" />
                  <Point X="27.57796875" Y="4.065842529297" />
                  <Point X="27.6294296875" Y="4.035861572266" />
                  <Point X="27.81778125" Y="3.901916015625" />
                  <Point X="27.795072265625" Y="3.862581542969" />
                  <Point X="27.065310546875" Y="2.59859765625" />
                  <Point X="27.062375" Y="2.593105224609" />
                  <Point X="27.05318359375" Y="2.573440185547" />
                  <Point X="27.04418359375" Y="2.549562744141" />
                  <Point X="27.041302734375" Y="2.54059765625" />
                  <Point X="27.023119140625" Y="2.472598632813" />
                  <Point X="27.018724609375" Y="2.451384765625" />
                  <Point X="27.014099609375" Y="2.420194580078" />
                  <Point X="27.013080078125" Y="2.407550292969" />
                  <Point X="27.012736328125" Y="2.382243408203" />
                  <Point X="27.013412109375" Y="2.369580810547" />
                  <Point X="27.020501953125" Y="2.310781005859" />
                  <Point X="27.02380078125" Y="2.2890390625" />
                  <Point X="27.029142578125" Y="2.267115478516" />
                  <Point X="27.032466796875" Y="2.256308837891" />
                  <Point X="27.040732421875" Y="2.234221191406" />
                  <Point X="27.0453203125" Y="2.223884765625" />
                  <Point X="27.055685546875" Y="2.203837890625" />
                  <Point X="27.061462890625" Y="2.194127441406" />
                  <Point X="27.09784765625" Y="2.1405078125" />
                  <Point X="27.1113359375" Y="2.122928710938" />
                  <Point X="27.131212890625" Y="2.099938232422" />
                  <Point X="27.139935546875" Y="2.091091796875" />
                  <Point X="27.158458984375" Y="2.07461328125" />
                  <Point X="27.168259765625" Y="2.066981201172" />
                  <Point X="27.22187890625" Y="2.03059765625" />
                  <Point X="27.24128125" Y="2.018245239258" />
                  <Point X="27.261326171875" Y="2.007882202148" />
                  <Point X="27.271658203125" Y="2.003297607422" />
                  <Point X="27.293744140625" Y="1.995032470703" />
                  <Point X="27.304548828125" Y="1.991707519531" />
                  <Point X="27.32647265625" Y="1.986364746094" />
                  <Point X="27.337591796875" Y="1.984346923828" />
                  <Point X="27.396390625" Y="1.977256469727" />
                  <Point X="27.4195" Y="1.975890258789" />
                  <Point X="27.449029296875" Y="1.975946899414" />
                  <Point X="27.461314453125" Y="1.976768188477" />
                  <Point X="27.485673828125" Y="1.979992553711" />
                  <Point X="27.497748046875" Y="1.982395874023" />
                  <Point X="27.565748046875" Y="2.000579711914" />
                  <Point X="27.577837890625" Y="2.004247070313" />
                  <Point X="27.60220703125" Y="2.012534179688" />
                  <Point X="27.610853515625" Y="2.015954833984" />
                  <Point X="27.63603515625" Y="2.027872802734" />
                  <Point X="27.86443359375" Y="2.159739501953" />
                  <Point X="28.940404296875" Y="2.780950927734" />
                  <Point X="29.013421875" Y="2.679472167969" />
                  <Point X="29.0439609375" Y="2.637030761719" />
                  <Point X="29.136884765625" Y="2.483471435547" />
                  <Point X="29.133751953125" Y="2.481067138672" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.168140625" Y="1.739869995117" />
                  <Point X="28.152119140625" Y="1.725216186523" />
                  <Point X="28.134666015625" Y="1.706599609375" />
                  <Point X="28.128576171875" Y="1.699420166016" />
                  <Point X="28.07963671875" Y="1.635575561523" />
                  <Point X="28.06805859375" Y="1.618657348633" />
                  <Point X="28.05075" Y="1.590175048828" />
                  <Point X="28.04478515625" Y="1.578653198242" />
                  <Point X="28.03448046875" Y="1.554904174805" />
                  <Point X="28.030140625" Y="1.542676879883" />
                  <Point X="28.01191015625" Y="1.477491210938" />
                  <Point X="28.006224609375" Y="1.454658569336" />
                  <Point X="28.002771484375" Y="1.4323515625" />
                  <Point X="28.001708984375" Y="1.421098388672" />
                  <Point X="28.00089453125" Y="1.397521362305" />
                  <Point X="28.00117578125" Y="1.386229980469" />
                  <Point X="28.003078125" Y="1.363752807617" />
                  <Point X="28.00469921875" Y="1.352566894531" />
                  <Point X="28.0196640625" Y="1.280039916992" />
                  <Point X="28.0250390625" Y="1.259742431641" />
                  <Point X="28.0349375" Y="1.229266113281" />
                  <Point X="28.039666015625" Y="1.217462158203" />
                  <Point X="28.05065625" Y="1.19459362793" />
                  <Point X="28.05691796875" Y="1.183528442383" />
                  <Point X="28.09762109375" Y="1.121662231445" />
                  <Point X="28.111736328125" Y="1.101427978516" />
                  <Point X="28.126287109375" Y="1.084186279297" />
                  <Point X="28.134076171875" Y="1.075999511719" />
                  <Point X="28.151322265625" Y="1.059906494141" />
                  <Point X="28.16003125" Y="1.052699584961" />
                  <Point X="28.178244140625" Y="1.03937109375" />
                  <Point X="28.187748046875" Y="1.033249633789" />
                  <Point X="28.246732421875" Y="1.000046691895" />
                  <Point X="28.266509765625" Y="0.99037701416" />
                  <Point X="28.29514453125" Y="0.978379577637" />
                  <Point X="28.30705859375" Y="0.974293029785" />
                  <Point X="28.331322265625" Y="0.967732116699" />
                  <Point X="28.343671875" Y="0.96525769043" />
                  <Point X="28.423421875" Y="0.954717346191" />
                  <Point X="28.435162109375" Y="0.953535217285" />
                  <Point X="28.462650390625" Y="0.951627624512" />
                  <Point X="28.47215234375" Y="0.951444824219" />
                  <Point X="28.50060546875" Y="0.952797302246" />
                  <Point X="28.717568359375" Y="0.98136126709" />
                  <Point X="29.704703125" Y="1.111319702148" />
                  <Point X="29.7395703125" Y="0.968092651367" />
                  <Point X="29.752689453125" Y="0.914204101562" />
                  <Point X="29.78387109375" Y="0.713920654297" />
                  <Point X="28.6919921875" Y="0.421352661133" />
                  <Point X="28.68603125" Y="0.419543762207" />
                  <Point X="28.665626953125" Y="0.412137298584" />
                  <Point X="28.6423828125" Y="0.401619262695" />
                  <Point X="28.634005859375" Y="0.397316619873" />
                  <Point X="28.555654296875" Y="0.352027801514" />
                  <Point X="28.539337890625" Y="0.341448394775" />
                  <Point X="28.5108671875" Y="0.32084286499" />
                  <Point X="28.5007109375" Y="0.312353515625" />
                  <Point X="28.48167578125" Y="0.294045288086" />
                  <Point X="28.472796875" Y="0.284226043701" />
                  <Point X="28.42578515625" Y="0.224322570801" />
                  <Point X="28.41085546875" Y="0.204207351685" />
                  <Point X="28.3991328125" Y="0.184929626465" />
                  <Point X="28.393845703125" Y="0.174943069458" />
                  <Point X="28.384072265625" Y="0.153482070923" />
                  <Point X="28.3800078125" Y="0.142934005737" />
                  <Point X="28.37316015625" Y="0.121429618835" />
                  <Point X="28.370376953125" Y="0.110473136902" />
                  <Point X="28.35470703125" Y="0.028648370743" />
                  <Point X="28.35198828125" Y="0.008870678902" />
                  <Point X="28.34915625" Y="-0.024726858139" />
                  <Point X="28.348951171875" Y="-0.037695690155" />
                  <Point X="28.350310546875" Y="-0.063543586731" />
                  <Point X="28.351875" Y="-0.076422653198" />
                  <Point X="28.367544921875" Y="-0.158247558594" />
                  <Point X="28.37316015625" Y="-0.183989196777" />
                  <Point X="28.3800078125" Y="-0.205493881226" />
                  <Point X="28.384072265625" Y="-0.216042541504" />
                  <Point X="28.393845703125" Y="-0.237503387451" />
                  <Point X="28.3991328125" Y="-0.247490402222" />
                  <Point X="28.41085546875" Y="-0.266767822266" />
                  <Point X="28.417291015625" Y="-0.276058227539" />
                  <Point X="28.464302734375" Y="-0.335961730957" />
                  <Point X="28.478146484375" Y="-0.351722595215" />
                  <Point X="28.500953125" Y="-0.374968841553" />
                  <Point X="28.5105546875" Y="-0.383514068604" />
                  <Point X="28.530826171875" Y="-0.399231811523" />
                  <Point X="28.54149609375" Y="-0.406404205322" />
                  <Point X="28.61984765625" Y="-0.451692871094" />
                  <Point X="28.6295859375" Y="-0.456950561523" />
                  <Point X="28.65582421875" Y="-0.470141571045" />
                  <Point X="28.66469140625" Y="-0.474046203613" />
                  <Point X="28.6919921875" Y="-0.483912963867" />
                  <Point X="28.890958984375" Y="-0.537225646973" />
                  <Point X="29.78487890625" Y="-0.776751220703" />
                  <Point X="29.7689375" Y="-0.882492614746" />
                  <Point X="29.761619140625" Y="-0.931038208008" />
                  <Point X="29.7278046875" Y="-1.079219726563" />
                  <Point X="29.71374609375" Y="-1.077369018555" />
                  <Point X="28.436783203125" Y="-0.909253662109" />
                  <Point X="28.428625" Y="-0.908535644531" />
                  <Point X="28.40009765625" Y="-0.908042541504" />
                  <Point X="28.36672265625" Y="-0.910840942383" />
                  <Point X="28.354482421875" Y="-0.912676147461" />
                  <Point X="28.200705078125" Y="-0.946100708008" />
                  <Point X="28.17291796875" Y="-0.952140258789" />
                  <Point X="28.157875" Y="-0.956742797852" />
                  <Point X="28.128755859375" Y="-0.968366943359" />
                  <Point X="28.1146796875" Y="-0.97538861084" />
                  <Point X="28.0868515625" Y="-0.992281677246" />
                  <Point X="28.074125" Y="-1.001530761719" />
                  <Point X="28.050375" Y="-1.022001464844" />
                  <Point X="28.0393515625" Y="-1.033223144531" />
                  <Point X="27.94640234375" Y="-1.145011230469" />
                  <Point X="27.929607421875" Y="-1.165211181641" />
                  <Point X="27.921328125" Y="-1.176846435547" />
                  <Point X="27.906603515625" Y="-1.201234375" />
                  <Point X="27.900158203125" Y="-1.213986816406" />
                  <Point X="27.88881640625" Y="-1.241373168945" />
                  <Point X="27.884359375" Y="-1.254942382812" />
                  <Point X="27.87753125" Y="-1.282586547852" />
                  <Point X="27.87516015625" Y="-1.296661499023" />
                  <Point X="27.861837890625" Y="-1.441432006836" />
                  <Point X="27.859431640625" Y="-1.467591674805" />
                  <Point X="27.859291015625" Y="-1.483320922852" />
                  <Point X="27.861609375" Y="-1.514592529297" />
                  <Point X="27.864068359375" Y="-1.530135009766" />
                  <Point X="27.87180078125" Y="-1.561755615234" />
                  <Point X="27.876791015625" Y="-1.576675170898" />
                  <Point X="27.889162109375" Y="-1.605483276367" />
                  <Point X="27.89654296875" Y="-1.619371948242" />
                  <Point X="27.98164453125" Y="-1.751743408203" />
                  <Point X="27.9970234375" Y="-1.775662841797" />
                  <Point X="28.00174609375" Y="-1.782355224609" />
                  <Point X="28.019798828125" Y="-1.804454589844" />
                  <Point X="28.0434921875" Y="-1.828121582031" />
                  <Point X="28.052798828125" Y="-1.83627746582" />
                  <Point X="28.237439453125" Y="-1.977958129883" />
                  <Point X="29.087171875" Y="-2.629980712891" />
                  <Point X="29.066126953125" Y="-2.664034423828" />
                  <Point X="29.045478515625" Y="-2.697445800781" />
                  <Point X="29.001275390625" Y="-2.760252197266" />
                  <Point X="28.985763671875" Y="-2.751296142578" />
                  <Point X="27.848455078125" Y="-2.094671142578" />
                  <Point X="27.841197265625" Y="-2.090887451172" />
                  <Point X="27.815025390625" Y="-2.079512207031" />
                  <Point X="27.7831171875" Y="-2.069325439453" />
                  <Point X="27.771107421875" Y="-2.066337890625" />
                  <Point X="27.588087890625" Y="-2.033284667969" />
                  <Point X="27.555017578125" Y="-2.027312133789" />
                  <Point X="27.539359375" Y="-2.025807739258" />
                  <Point X="27.5080078125" Y="-2.025403686523" />
                  <Point X="27.492314453125" Y="-2.026504150391" />
                  <Point X="27.460140625" Y="-2.031462036133" />
                  <Point X="27.444845703125" Y="-2.03513684082" />
                  <Point X="27.4150703125" Y="-2.044960327148" />
                  <Point X="27.40058984375" Y="-2.051109130859" />
                  <Point X="27.248544921875" Y="-2.131128662109" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.208970703125" Y="-2.153170410156" />
                  <Point X="27.18604296875" Y="-2.170061035156" />
                  <Point X="27.175216796875" Y="-2.179369384766" />
                  <Point X="27.1542578125" Y="-2.200326904297" />
                  <Point X="27.144947265625" Y="-2.211153808594" />
                  <Point X="27.12805078125" Y="-2.234086914062" />
                  <Point X="27.12046484375" Y="-2.246193115234" />
                  <Point X="27.0404453125" Y="-2.398237060547" />
                  <Point X="27.025984375" Y="-2.425711181641" />
                  <Point X="27.0198359375" Y="-2.440192382812" />
                  <Point X="27.01001171875" Y="-2.469971191406" />
                  <Point X="27.0063359375" Y="-2.485268798828" />
                  <Point X="27.00137890625" Y="-2.517443359375" />
                  <Point X="27.000279296875" Y="-2.533134033203" />
                  <Point X="27.00068359375" Y="-2.564484130859" />
                  <Point X="27.0021875" Y="-2.580143554688" />
                  <Point X="27.0352421875" Y="-2.763162841797" />
                  <Point X="27.04121484375" Y="-2.796233886719" />
                  <Point X="27.04301953125" Y="-2.80423046875" />
                  <Point X="27.051236328125" Y="-2.831534912109" />
                  <Point X="27.064068359375" Y="-2.862473876953" />
                  <Point X="27.069546875" Y="-2.873578613281" />
                  <Point X="27.188197265625" Y="-3.079087646484" />
                  <Point X="27.73589453125" Y="-4.027724853516" />
                  <Point X="27.723755859375" Y="-4.036082763672" />
                  <Point X="27.701787109375" Y="-4.007452636719" />
                  <Point X="26.83391796875" Y="-2.876422363281" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.59279296875" Y="-2.704597900391" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.21098046875" Y="-2.664682617188" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670339355469" />
                  <Point X="26.133576171875" Y="-2.677171386719" />
                  <Point X="26.1200078125" Y="-2.681629394531" />
                  <Point X="26.092623046875" Y="-2.69297265625" />
                  <Point X="26.079876953125" Y="-2.6994140625" />
                  <Point X="26.05549609375" Y="-2.714134033203" />
                  <Point X="26.043861328125" Y="-2.722412597656" />
                  <Point X="25.891421875" Y="-2.84916015625" />
                  <Point X="25.863876953125" Y="-2.872062988281" />
                  <Point X="25.852654296875" Y="-2.883087402344" />
                  <Point X="25.83218359375" Y="-2.906836914062" />
                  <Point X="25.822935546875" Y="-2.919562011719" />
                  <Point X="25.80604296875" Y="-2.947388183594" />
                  <Point X="25.79901953125" Y="-2.961466308594" />
                  <Point X="25.78739453125" Y="-2.990587646484" />
                  <Point X="25.78279296875" Y="-3.005630859375" />
                  <Point X="25.73721484375" Y="-3.215326904297" />
                  <Point X="25.728978515625" Y="-3.25321875" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323170166016" />
                  <Point X="25.7255546875" Y="-3.335520507812" />
                  <Point X="25.7591796875" Y="-3.590925292969" />
                  <Point X="25.833087890625" Y="-4.152312011719" />
                  <Point X="25.655068359375" Y="-3.4879375" />
                  <Point X="25.652607421875" Y="-3.480119628906" />
                  <Point X="25.6421484375" Y="-3.453583984375" />
                  <Point X="25.626791015625" Y="-3.423817871094" />
                  <Point X="25.62041015625" Y="-3.413209228516" />
                  <Point X="25.481740234375" Y="-3.213411621094" />
                  <Point X="25.456681640625" Y="-3.177308837891" />
                  <Point X="25.446669921875" Y="-3.165170166016" />
                  <Point X="25.424783203125" Y="-3.142713134766" />
                  <Point X="25.412908203125" Y="-3.132394775391" />
                  <Point X="25.38665234375" Y="-3.113151611328" />
                  <Point X="25.373240234375" Y="-3.104936035156" />
                  <Point X="25.3452421875" Y="-3.090829345703" />
                  <Point X="25.33065625" Y="-3.084938232422" />
                  <Point X="25.116072265625" Y="-3.018339111328" />
                  <Point X="25.077296875" Y="-3.006304931641" />
                  <Point X="25.063376953125" Y="-3.003108886719" />
                  <Point X="25.035216796875" Y="-2.998839599609" />
                  <Point X="25.0209765625" Y="-2.997766357422" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.720431640625" Y="-3.072904052734" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.6670703125" Y="-3.090829833984" />
                  <Point X="24.639068359375" Y="-3.104938720703" />
                  <Point X="24.62565234375" Y="-3.11315625" />
                  <Point X="24.599396484375" Y="-3.132400878906" />
                  <Point X="24.587521484375" Y="-3.142719726562" />
                  <Point X="24.565638671875" Y="-3.165174560547" />
                  <Point X="24.555630859375" Y="-3.177310546875" />
                  <Point X="24.4169609375" Y="-3.377107910156" />
                  <Point X="24.391904296875" Y="-3.4132109375" />
                  <Point X="24.38753125" Y="-3.420129882812" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476214111328" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.295939453125" Y="-3.716734619141" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.279713152464" Y="0.998281707425" />
                  <Point X="20.287147402932" Y="0.57237378335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.308122926209" Y="-0.629313140391" />
                  <Point X="20.31816844742" Y="-1.204820665128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.369149069332" Y="1.317876869002" />
                  <Point X="20.382608351881" Y="0.546795088197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.402695076637" Y="-0.603972602369" />
                  <Point X="20.414438856377" Y="-1.27677329307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.464382386323" Y="1.305339199983" />
                  <Point X="20.47806930083" Y="0.521216393043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.497267227065" Y="-0.578632064346" />
                  <Point X="20.509235484471" Y="-1.264293071968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.559615703314" Y="1.292801530965" />
                  <Point X="20.573530249779" Y="0.49563769789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.591839377492" Y="-0.553291526324" />
                  <Point X="20.604032112566" Y="-1.251812850866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.654849020305" Y="1.280263861946" />
                  <Point X="20.668991198727" Y="0.470059002736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.68641152792" Y="-0.527950988301" />
                  <Point X="20.69882874066" Y="-1.239332629764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.750082337296" Y="1.267726192928" />
                  <Point X="20.764452147676" Y="0.444480307582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.780983678348" Y="-0.502610450279" />
                  <Point X="20.793625368755" Y="-1.226852408662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.825587271581" Y="2.38542681217" />
                  <Point X="20.827922055019" Y="2.251667158578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.845315654287" Y="1.255188523909" />
                  <Point X="20.859913096625" Y="0.418901612429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.875555828775" Y="-0.477269912256" />
                  <Point X="20.888421996849" Y="-1.21437218756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.908669718344" Y="-2.374363375069" />
                  <Point X="20.912622350945" Y="-2.600809545127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.917842851026" Y="2.543483612858" />
                  <Point X="20.924226404402" Y="2.177770084879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.940548971278" Y="1.242650854891" />
                  <Point X="20.955374045574" Y="0.393322917275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.970127979203" Y="-0.451929374234" />
                  <Point X="20.983218624944" Y="-1.201891966458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.002428409392" Y="-2.302419780427" />
                  <Point X="21.010407472673" Y="-2.759540009615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.010188946001" Y="2.696354782366" />
                  <Point X="21.020530753786" Y="2.103873011179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.035782288268" Y="1.230113185872" />
                  <Point X="21.050834994523" Y="0.367744222121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.064700129631" Y="-0.426588836211" />
                  <Point X="21.078015253038" Y="-1.189411745356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.096187100441" Y="-2.230476185784" />
                  <Point X="21.107651976979" Y="-2.887298522798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.103118443728" Y="2.8158028306" />
                  <Point X="21.11683510317" Y="2.02997593748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.131015605259" Y="1.217575516854" />
                  <Point X="21.146295943472" Y="0.342165526968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.159272280058" Y="-0.401248298189" />
                  <Point X="21.172811881133" Y="-1.176931524254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.189945791489" Y="-2.158532591142" />
                  <Point X="21.204896478008" Y="-3.015056848149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.196047941455" Y="2.935250878834" />
                  <Point X="21.213139452553" Y="1.95607886378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.226244763626" Y="1.205276095288" />
                  <Point X="21.241756892421" Y="0.316586831814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.253844430486" Y="-0.375907760166" />
                  <Point X="21.267608509228" Y="-1.164451303152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.283704482538" Y="-2.086588996499" />
                  <Point X="21.299028912562" Y="-2.964525004594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.289480467746" Y="3.025880439925" />
                  <Point X="21.309443801937" Y="1.882181790081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.321114734137" Y="1.213554532115" />
                  <Point X="21.337217841955" Y="0.291008103126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.348416580914" Y="-0.350567222144" />
                  <Point X="21.362405137322" Y="-1.151971082049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.377463173586" Y="-2.014645401857" />
                  <Point X="21.393095411377" Y="-2.910215705117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.385462213932" Y="2.970465291004" />
                  <Point X="21.405748151025" Y="1.808284733313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.415562050245" Y="1.246046823562" />
                  <Point X="21.432678791673" Y="0.265429363869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.442988731341" Y="-0.325226684121" />
                  <Point X="21.457201765417" Y="-1.139490860947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.471221864635" Y="-1.942701807214" />
                  <Point X="21.487161910193" Y="-2.855906405639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.481443960119" Y="2.915050142082" />
                  <Point X="21.502285319401" Y="1.721049468441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.509209467033" Y="1.324365316316" />
                  <Point X="21.528213827547" Y="0.235606231626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.537447313921" Y="-0.293379848428" />
                  <Point X="21.551998393618" Y="-1.127010645963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.564980555683" Y="-1.870758212572" />
                  <Point X="21.581228409009" Y="-2.801597106162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.577425706305" Y="2.85963499316" />
                  <Point X="21.624407247793" Y="0.168064283988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.631242540242" Y="-0.223529358137" />
                  <Point X="21.646795021935" Y="-1.114530437623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.658739246732" Y="-1.79881461793" />
                  <Point X="21.675294907824" Y="-2.747287806684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.673407452492" Y="2.804219844239" />
                  <Point X="21.741591650252" Y="-1.102050229283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.75249793778" Y="-1.726871023287" />
                  <Point X="21.76936140664" Y="-2.692978507206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.769343169766" Y="2.75144168997" />
                  <Point X="21.836676990911" Y="-1.106110339892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.846256628829" Y="-1.654927428645" />
                  <Point X="21.863427905455" Y="-2.638669207729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.864741953193" Y="2.729424455185" />
                  <Point X="21.932374154922" Y="-1.145221786922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.940015319877" Y="-1.582983834002" />
                  <Point X="21.957494404271" Y="-2.584359908251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.942787353456" Y="3.701581876004" />
                  <Point X="21.9428851336" Y="3.69598005532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.959855479404" Y="2.72374959533" />
                  <Point X="22.029229253868" Y="-1.250671281908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.032859247517" Y="-1.458633478784" />
                  <Point X="22.051560903086" Y="-2.530050608774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.072660431105" Y="-3.738841759392" />
                  <Point X="22.075029802823" Y="-3.874582974195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.036547075406" Y="3.773466410347" />
                  <Point X="22.040861752792" Y="3.526278708437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.054464922073" Y="2.746953662296" />
                  <Point X="22.145627401593" Y="-2.47574129161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.164886620799" Y="-3.579101220946" />
                  <Point X="22.171338641225" Y="-3.948737223612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.130306797356" Y="3.845350944689" />
                  <Point X="22.138838371984" Y="3.356577361555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.14820160488" Y="2.820158108257" />
                  <Point X="22.239693899429" Y="-2.421431936007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.257112810493" Y="-3.4193606825" />
                  <Point X="22.267606925261" Y="-4.020568114903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.224066519306" Y="3.917235479032" />
                  <Point X="22.237206481049" Y="3.164447574903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.240908354803" Y="2.952367369609" />
                  <Point X="22.333808132497" Y="-2.369857330001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.349339000187" Y="-3.259620144054" />
                  <Point X="22.363659518646" Y="-4.080042097105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.31782623052" Y="3.989120628415" />
                  <Point X="22.428454000072" Y="-2.348740044514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.441565189881" Y="-3.099879605609" />
                  <Point X="22.459712099687" Y="-4.13951537214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.411804739393" Y="4.048470868328" />
                  <Point X="22.523782932795" Y="-2.366755535156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.533791379575" Y="-2.940139067163" />
                  <Point X="22.553830051061" Y="-4.088153787753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.505906643045" Y="4.100751826069" />
                  <Point X="22.619971402674" Y="-2.434013876469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.626017569269" Y="-2.780398528717" />
                  <Point X="22.647093699202" Y="-3.987849203907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.600008546697" Y="4.15303278381" />
                  <Point X="22.741012760759" Y="-3.925093229534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.69411045035" Y="4.205313741551" />
                  <Point X="22.834931841271" Y="-3.862338341124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.788212354002" Y="4.257594699292" />
                  <Point X="22.928917259831" Y="-3.803383956892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.884790739511" Y="4.167998106498" />
                  <Point X="23.023481136408" Y="-3.77756941033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.981084894544" Y="4.09468506672" />
                  <Point X="23.118540387194" Y="-3.780134833142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.076933312765" Y="4.046908271828" />
                  <Point X="23.213663685318" Y="-3.78636952548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.17225804319" Y="4.029133530705" />
                  <Point X="23.308786983442" Y="-3.792604217818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.266907779846" Y="4.050029156684" />
                  <Point X="23.404007080868" Y="-3.804384538483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.361232286464" Y="4.089557199069" />
                  <Point X="23.499832400904" Y="-3.850838039205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.454973338102" Y="4.162511354838" />
                  <Point X="23.596309772333" Y="-3.934647539287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.542894848659" Y="4.568866795902" />
                  <Point X="23.692801319125" Y="-4.019269145315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.637446601575" Y="4.595375906564" />
                  <Point X="23.78970406194" Y="-4.12744815573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.731998354491" Y="4.621885017227" />
                  <Point X="23.895423976184" Y="-4.740762579034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.826550107408" Y="4.64839412789" />
                  <Point X="23.990812540849" Y="-4.762194381356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.921101860324" Y="4.674903238553" />
                  <Point X="24.081544731948" Y="-4.516862720705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.015653616696" Y="4.701412151235" />
                  <Point X="24.170748230959" Y="-4.18395234901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.110323092631" Y="4.721176914716" />
                  <Point X="24.25995172997" Y="-3.851041977315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.205143859695" Y="4.73227421516" />
                  <Point X="24.349155233097" Y="-3.51813184145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.299964626759" Y="4.743371515604" />
                  <Point X="24.441101012143" Y="-3.342326587733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.394785393824" Y="4.754468816048" />
                  <Point X="24.53378454107" Y="-3.208786996387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.489606160888" Y="4.765566116491" />
                  <Point X="24.627114142506" Y="-3.112260874321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.584426927953" Y="4.776663416935" />
                  <Point X="24.721436195715" Y="-3.072592276247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.682443693712" Y="4.604662074787" />
                  <Point X="24.815938708769" Y="-3.043262215772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.78386496328" Y="4.237616840066" />
                  <Point X="24.910441221824" Y="-3.013932155297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.880917626549" Y="4.120848892583" />
                  <Point X="25.005173520208" Y="-2.997766487587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.976515042459" Y="4.087452010458" />
                  <Point X="25.100462525426" Y="-3.013494532965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.071391813228" Y="4.095340860787" />
                  <Point X="25.195994531657" Y="-3.043144097073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.165526410307" Y="4.145748813365" />
                  <Point X="25.291526540018" Y="-3.07279378324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.25726034654" Y="4.333690533682" />
                  <Point X="25.387253145739" Y="-3.113591944706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.346463800604" Y="4.666603480431" />
                  <Point X="25.484068531358" Y="-3.216766264698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.439523661851" Y="4.778583007546" />
                  <Point X="25.58153421648" Y="-3.357196218287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.534712138925" Y="4.768614215651" />
                  <Point X="25.680486602064" Y="-3.582799184315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.629900615999" Y="4.758645423755" />
                  <Point X="25.766717537777" Y="-3.079590775352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.778159308079" Y="-3.735089356891" />
                  <Point X="25.782121885612" Y="-3.962105271721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.725089093073" Y="4.74867663186" />
                  <Point X="25.858206822152" Y="-2.87763295944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.820277564826" Y="4.738708144837" />
                  <Point X="25.951847330933" Y="-2.798918707207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.91567592394" Y="4.716715218879" />
                  <Point X="26.045505956827" Y="-2.721242383734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.011092498446" Y="4.693678733877" />
                  <Point X="26.139724648074" Y="-2.675652182809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.106509072952" Y="4.670642248874" />
                  <Point X="26.234509851057" Y="-2.662517417476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.201925647457" Y="4.647605763872" />
                  <Point X="26.329371951459" Y="-2.653788102371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.297342221963" Y="4.624569278869" />
                  <Point X="26.42425722742" Y="-2.646386514124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.392758799972" Y="4.601532593168" />
                  <Point X="26.519592840879" Y="-2.664784743878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.48824904989" Y="4.574275246581" />
                  <Point X="26.615557717446" Y="-2.719233432947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.58386889645" Y="4.539593313376" />
                  <Point X="26.71165053444" Y="-2.781011824193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.67948874301" Y="4.504911380172" />
                  <Point X="26.807848148938" Y="-2.84879406037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.775108589571" Y="4.470229446967" />
                  <Point X="26.904960953155" Y="-2.969007480456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.870751463254" Y="4.434228290722" />
                  <Point X="27.002187116072" Y="-3.095715216083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.966547936671" Y="4.389427411684" />
                  <Point X="27.083595922666" Y="-2.316247214945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.094065451906" Y="-2.916046143383" />
                  <Point X="27.099413278988" Y="-3.222422951709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.062344410088" Y="4.344626532647" />
                  <Point X="27.092014126885" Y="2.64484959573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.100886577759" Y="2.136547225614" />
                  <Point X="27.176206331603" Y="-2.178518582164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.192042062794" Y="-3.085747014491" />
                  <Point X="27.196639441904" Y="-3.349130687336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.158140883504" Y="4.299825653609" />
                  <Point X="27.184240319184" Y="2.804589984961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.197461196702" Y="2.047166419231" />
                  <Point X="27.270194724794" Y="-2.119734614483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.290018659071" Y="-3.25544704858" />
                  <Point X="27.293865604821" Y="-3.475838422962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.253944434834" Y="4.2546192812" />
                  <Point X="27.276466511482" Y="2.964330374191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.29338331234" Y="1.995167502105" />
                  <Point X="27.364344299904" Y="-2.070184752674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.387995255349" Y="-3.425147082669" />
                  <Point X="27.391091767737" Y="-3.602546158589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.349935066489" Y="4.19869508413" />
                  <Point X="27.368692703781" Y="3.124070763422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.388694221751" Y="1.978184566352" />
                  <Point X="27.458688951575" Y="-2.031810819628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.485971851626" Y="-3.594847116758" />
                  <Point X="27.488317930654" Y="-3.729253894215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.445925698144" Y="4.142770887059" />
                  <Point X="27.460918896079" Y="3.283811152652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.483681737037" Y="1.979728867584" />
                  <Point X="27.553622558393" Y="-2.027178104358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.583948447904" Y="-3.764547150848" />
                  <Point X="27.58554409357" Y="-3.855961629842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.541916329799" Y="4.086846689989" />
                  <Point X="27.553145088378" Y="3.443551541883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.578265701948" Y="2.004392554279" />
                  <Point X="27.648935434281" Y="-2.044273699548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.681925044181" Y="-3.934247184937" />
                  <Point X="27.682770256487" Y="-3.982669365468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.637926217857" Y="4.029819294341" />
                  <Point X="27.645371280676" Y="3.603291931114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.672502812022" Y="2.04892754132" />
                  <Point X="27.744250373854" Y="-2.061487523115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.73413493831" Y="3.961400798398" />
                  <Point X="27.737597472974" Y="3.763032320344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.766569304818" Y="2.103237185676" />
                  <Point X="27.839767173127" Y="-2.090265881211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.860635797613" Y="2.157546830032" />
                  <Point X="27.918907820326" Y="-1.180855115325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.927399943294" Y="-1.66736851435" />
                  <Point X="27.93573814927" Y="-2.145064014755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.954702296668" Y="2.211856115798" />
                  <Point X="28.011921206984" Y="-1.066213060756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.024896135626" Y="-1.809546224799" />
                  <Point X="28.031719893736" Y="-2.200479065082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.048768795987" Y="2.266165386478" />
                  <Point X="28.060294112558" Y="1.605880442298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.067959225412" Y="1.16674642101" />
                  <Point X="28.105448149767" Y="-0.980992616857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.121294623393" Y="-1.888836482879" />
                  <Point X="28.127701638201" Y="-2.255894115409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.142835295305" Y="2.320474657158" />
                  <Point X="28.153208183969" Y="1.726212263563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.165028222339" Y="1.049042718883" />
                  <Point X="28.199856798661" Y="-0.946285082238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.217598978746" Y="-1.962733898567" />
                  <Point X="28.223683382667" Y="-2.311309165735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.236901794623" Y="2.374783927838" />
                  <Point X="28.246924557149" Y="1.800580247272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.261019854902" Y="0.993061179848" />
                  <Point X="28.294512149586" Y="-0.925711097527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.313903327231" Y="-2.036630920773" />
                  <Point X="28.319665127132" Y="-2.366724216062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.330968293941" Y="2.429093198518" />
                  <Point X="28.340683248013" Y="1.872523852507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.356549345966" Y="0.963555709528" />
                  <Point X="28.371371610394" Y="0.114388749204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.37677814276" Y="-0.195351282636" />
                  <Point X="28.389234114388" Y="-0.90895341926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.410207673933" Y="-2.11052784088" />
                  <Point X="28.415646871598" Y="-2.422139266389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.42503479326" Y="2.483402469198" />
                  <Point X="28.434441938877" Y="1.944467457742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.451758829443" Y="0.952383461634" />
                  <Point X="28.463625546199" Y="0.272539713976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.474448599935" Y="-0.347512619254" />
                  <Point X="28.484363165203" Y="-0.915517683048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.506512020636" Y="-2.184424760988" />
                  <Point X="28.511628616064" Y="-2.477554316716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.519101292578" Y="2.537711739878" />
                  <Point X="28.52820062974" Y="2.016411062977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.546660242033" Y="0.958860583027" />
                  <Point X="28.557236583333" Y="0.352942395706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.570786549314" Y="-0.423334635425" />
                  <Point X="28.57959648281" Y="-0.928055387356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.602816367339" Y="-2.258321681095" />
                  <Point X="28.607610360529" Y="-2.532969367043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.613167791896" Y="2.592021010558" />
                  <Point X="28.621959320604" Y="2.088354668211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.641456868305" Y="0.971340908544" />
                  <Point X="28.651330721887" Y="0.405668215659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.666698858054" Y="-0.474771715659" />
                  <Point X="28.674829800417" Y="-0.940593091664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.699120714041" Y="-2.332218601202" />
                  <Point X="28.703592104995" Y="-2.588384417369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.707234291214" Y="2.646330281238" />
                  <Point X="28.715718011467" Y="2.160298273446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.73625349507" Y="0.983821205771" />
                  <Point X="28.745819665193" Y="0.435775686466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.762201261184" Y="-0.502725319271" />
                  <Point X="28.770063118024" Y="-0.953130795972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.795425060744" Y="-2.40611552131" />
                  <Point X="28.799573849461" Y="-2.643799467696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.801300790533" Y="2.700639551918" />
                  <Point X="28.809476702331" Y="2.232241878681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.831050123847" Y="0.996301387762" />
                  <Point X="28.840391815855" Y="0.461116211107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.857662207337" Y="-0.528303854239" />
                  <Point X="28.86529643563" Y="-0.96566850028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.891729407447" Y="-2.480012441417" />
                  <Point X="28.895555593926" Y="-2.699214518023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.895367289851" Y="2.754948822598" />
                  <Point X="28.903235393195" Y="2.304185483916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.925846752625" Y="1.008781569753" />
                  <Point X="28.934963966516" Y="0.486456735747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.953123155737" Y="-0.553882517959" />
                  <Point X="28.960529753237" Y="-0.978206204588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.988033754149" Y="-2.553909361525" />
                  <Point X="28.99153734079" Y="-2.754629705763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.991159143929" Y="2.710412585278" />
                  <Point X="28.996994084058" Y="2.376129089151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.020643381402" Y="1.021261751745" />
                  <Point X="29.029536117177" Y="0.511797260387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.048584105341" Y="-0.579461250643" />
                  <Point X="29.055763070844" Y="-0.990743908896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.084338100852" Y="-2.627806281632" />
                  <Point X="29.084452853914" Y="-2.634380480143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.088746334453" Y="2.563021591873" />
                  <Point X="29.090752774922" Y="2.448072694386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.115440010179" Y="1.033741933736" />
                  <Point X="29.124108267838" Y="0.537137785028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.144045054945" Y="-0.605039983328" />
                  <Point X="29.150996388451" Y="-1.003281613204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.210236638956" Y="1.046222115727" />
                  <Point X="29.2186804185" Y="0.562478309668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.239506004549" Y="-0.630618716012" />
                  <Point X="29.246229706058" Y="-1.015819317512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.305033267733" Y="1.058702297718" />
                  <Point X="29.313252569161" Y="0.587818834309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.334966954153" Y="-0.656197448696" />
                  <Point X="29.341463023665" Y="-1.02835702182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.399829896511" Y="1.071182479709" />
                  <Point X="29.407824719822" Y="0.613159358949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.430427903756" Y="-0.68177618138" />
                  <Point X="29.436696341272" Y="-1.040894726128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.494626525288" Y="1.0836626617" />
                  <Point X="29.502396870483" Y="0.638499883589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.52588885336" Y="-0.707354914064" />
                  <Point X="29.531929658879" Y="-1.053432430436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.589423154065" Y="1.096142843691" />
                  <Point X="29.596969021145" Y="0.66384040823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.621349802964" Y="-0.732933646749" />
                  <Point X="29.627162976486" Y="-1.065970134744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.684219782842" Y="1.108623025682" />
                  <Point X="29.691541171806" Y="0.68918093287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.716810752568" Y="-0.758512379433" />
                  <Point X="29.722396292575" Y="-1.078507752095" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.81081640625" Y="-4.803303710938" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521543212891" />
                  <Point X="25.325650390625" Y="-3.321745605469" />
                  <Point X="25.300591796875" Y="-3.285642822266" />
                  <Point X="25.2743359375" Y="-3.266399658203" />
                  <Point X="25.059751953125" Y="-3.199800537109" />
                  <Point X="25.0209765625" Y="-3.187766357422" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.776751953125" Y="-3.254365478516" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.28564453125" />
                  <Point X="24.57305078125" Y="-3.485441894531" />
                  <Point X="24.547994140625" Y="-3.521544921875" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.479466796875" Y="-3.765910400391" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.944154296875" Y="-4.94668359375" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.650771484375" Y="-4.87400390625" />
                  <Point X="23.650009765625" Y="-4.861254394531" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.639052734375" Y="-4.277035644531" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.41615625" Y="-4.02937109375" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.08855078125" Y="-3.968576904297" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791503906" />
                  <Point X="22.791634765625" Y="-4.119779785156" />
                  <Point X="22.75215625" Y="-4.146159667969" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.70584765625" Y="-4.202149414062" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.209244140625" Y="-4.207905273438" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.7993984375" Y="-3.902150634766" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="21.863462890625" Y="-3.721185058594" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597593505859" />
                  <Point X="22.486021484375" Y="-2.568764648438" />
                  <Point X="22.471328125" Y="-2.554071044922" />
                  <Point X="22.468677734375" Y="-2.551420654297" />
                  <Point X="22.43984765625" Y="-2.53719921875" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="22.210984375" Y="-2.657400634766" />
                  <Point X="21.15704296875" Y="-3.265893554688" />
                  <Point X="20.889712890625" Y="-2.914677978516" />
                  <Point X="20.838296875" Y="-2.847126464844" />
                  <Point X="20.591125" Y="-2.432658691406" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="20.73303515625" Y="-2.269643066406" />
                  <Point X="21.836212890625" Y="-1.423144897461" />
                  <Point X="21.854177734375" Y="-1.396013427734" />
                  <Point X="21.860701171875" Y="-1.37082421875" />
                  <Point X="21.8618828125" Y="-1.366271850586" />
                  <Point X="21.85967578125" Y="-1.334599243164" />
                  <Point X="21.83884375" Y="-1.310640380859" />
                  <Point X="21.8164140625" Y="-1.297439208984" />
                  <Point X="21.812361328125" Y="-1.295053710938" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.531357421875" Y="-1.321367675781" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.092716796875" Y="-1.089918212891" />
                  <Point X="20.072609375" Y="-1.011190368652" />
                  <Point X="20.007212890625" Y="-0.553950439453" />
                  <Point X="20.00160546875" Y="-0.514742126465" />
                  <Point X="20.1858515625" Y="-0.465373291016" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.4581015625" Y="-0.121427024841" />
                  <Point X="21.481607421875" Y="-0.105112861633" />
                  <Point X="21.48585546875" Y="-0.102165000916" />
                  <Point X="21.5051015625" Y="-0.075909889221" />
                  <Point X="21.512935546875" Y="-0.050664669037" />
                  <Point X="21.5143515625" Y="-0.046102855682" />
                  <Point X="21.5143515625" Y="-0.016457221985" />
                  <Point X="21.506517578125" Y="0.008787993431" />
                  <Point X="21.5051015625" Y="0.013349655151" />
                  <Point X="21.48585546875" Y="0.039604923248" />
                  <Point X="21.462349609375" Y="0.055919086456" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="21.21545703125" Y="0.126931373596" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.069396484375" Y="0.908842285156" />
                  <Point X="20.08235546875" Y="0.996414916992" />
                  <Point X="20.213994140625" Y="1.482206542969" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.345912109375" Y="1.512575805664" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866943359" />
                  <Point X="21.320322265625" Y="1.412270385742" />
                  <Point X="21.32972265625" Y="1.415234375" />
                  <Point X="21.34846875" Y="1.426057373047" />
                  <Point X="21.3608828125" Y="1.443788574219" />
                  <Point X="21.3817578125" Y="1.494186157227" />
                  <Point X="21.385529296875" Y="1.50329284668" />
                  <Point X="21.38928515625" Y="1.524606445312" />
                  <Point X="21.383685546875" Y="1.545511108398" />
                  <Point X="21.358498046875" Y="1.593897583008" />
                  <Point X="21.35394140625" Y="1.602646728516" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.20977734375" Y="1.719169311523" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.789630859375" Y="2.700736816406" />
                  <Point X="20.83998828125" Y="2.7870078125" />
                  <Point X="21.188693359375" Y="3.235220947266" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.287583984375" Y="3.246368408203" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.933943359375" Y="2.914095703125" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927405029297" />
                  <Point X="22.0381796875" Y="2.978835205078" />
                  <Point X="22.04747265625" Y="2.988128417969" />
                  <Point X="22.0591015625" Y="3.006381103516" />
                  <Point X="22.061927734375" Y="3.027837646484" />
                  <Point X="22.055587890625" Y="3.100294433594" />
                  <Point X="22.054443359375" Y="3.113387207031" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.990212890625" Y="3.234005859375" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="22.15942578125" Y="4.107093261719" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.796330078125" Y="4.479457519531" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.8628046875" Y="4.508300292969" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.1293984375" Y="4.2316796875" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.218491699219" />
                  <Point X="23.18619140625" Y="4.22225" />
                  <Point X="23.2701875" Y="4.25704296875" />
                  <Point X="23.285365234375" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275742675781" />
                  <Point X="23.31391796875" Y="4.294484863281" />
                  <Point X="23.341255859375" Y="4.381193359375" />
                  <Point X="23.346197265625" Y="4.396861328125" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.341521484375" Y="4.468270507812" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.918375" Y="4.871465820312" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.6977109375" Y="4.98121875" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.794189453125" Y="4.921727539062" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.084029296875" Y="4.421278320312" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.76107421875" Y="4.935947265625" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.41104296875" Y="4.792577148438" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.867744140625" Y="4.6387421875" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.277724609375" Y="4.453650390625" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.67361328125" Y="4.230012695313" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="28.048396484375" Y="3.971061523438" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.9596171875" Y="3.767581787109" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491514648438" />
                  <Point X="27.206669921875" Y="2.423515625" />
                  <Point X="27.206669921875" Y="2.423515380859" />
                  <Point X="27.202044921875" Y="2.392325439453" />
                  <Point X="27.209134765625" Y="2.333525634766" />
                  <Point X="27.210416015625" Y="2.322900634766" />
                  <Point X="27.218681640625" Y="2.300812988281" />
                  <Point X="27.25506640625" Y="2.247193359375" />
                  <Point X="27.274943359375" Y="2.224202880859" />
                  <Point X="27.3285625" Y="2.187819580078" />
                  <Point X="27.338251953125" Y="2.181245361328" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.41913671875" Y="2.165889892578" />
                  <Point X="27.448666015625" Y="2.165946533203" />
                  <Point X="27.516666015625" Y="2.184130371094" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="27.76943359375" Y="2.324283935547" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.1676484375" Y="2.790443359375" />
                  <Point X="29.20259765625" Y="2.741872070312" />
                  <Point X="29.378685546875" Y="2.450884521484" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="29.249416015625" Y="2.330330078125" />
                  <Point X="28.2886171875" Y="1.593082641602" />
                  <Point X="28.279369140625" Y="1.583830200195" />
                  <Point X="28.2304296875" Y="1.519985473633" />
                  <Point X="28.21312109375" Y="1.491503295898" />
                  <Point X="28.194890625" Y="1.426317626953" />
                  <Point X="28.191595703125" Y="1.414538818359" />
                  <Point X="28.19078125" Y="1.390961791992" />
                  <Point X="28.20574609375" Y="1.318434692383" />
                  <Point X="28.21564453125" Y="1.287958496094" />
                  <Point X="28.25634765625" Y="1.226092163086" />
                  <Point X="28.263701171875" Y="1.214913085938" />
                  <Point X="28.280947265625" Y="1.198819946289" />
                  <Point X="28.339931640625" Y="1.16561706543" />
                  <Point X="28.36856640625" Y="1.153619506836" />
                  <Point X="28.44831640625" Y="1.143079467773" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.692767578125" Y="1.169735717773" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.9241796875" Y="1.013033569336" />
                  <Point X="29.939193359375" Y="0.951366333008" />
                  <Point X="29.994681640625" Y="0.594969238281" />
                  <Point X="29.997859375" Y="0.574556335449" />
                  <Point X="29.842265625" Y="0.532864868164" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819503784" />
                  <Point X="28.650736328125" Y="0.187530685425" />
                  <Point X="28.622265625" Y="0.166925170898" />
                  <Point X="28.57525390625" Y="0.107021781921" />
                  <Point X="28.566759765625" Y="0.096197494507" />
                  <Point X="28.556986328125" Y="0.074736480713" />
                  <Point X="28.54131640625" Y="-0.00708838129" />
                  <Point X="28.538484375" Y="-0.040685997009" />
                  <Point X="28.554154296875" Y="-0.122511009216" />
                  <Point X="28.556986328125" Y="-0.137296554565" />
                  <Point X="28.566759765625" Y="-0.158757415771" />
                  <Point X="28.613771484375" Y="-0.218660812378" />
                  <Point X="28.636578125" Y="-0.241907058716" />
                  <Point X="28.7149296875" Y="-0.287195861816" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="28.940134765625" Y="-0.353699737549" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.9568125" Y="-0.910817932129" />
                  <Point X="29.948431640625" Y="-0.966412109375" />
                  <Point X="29.87733984375" Y="-1.277945068359" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.6889453125" Y="-1.265743530273" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.241060546875" Y="-1.13176550293" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.1854453125" Y="-1.154697998047" />
                  <Point X="28.09249609375" Y="-1.266486206055" />
                  <Point X="28.075701171875" Y="-1.286686035156" />
                  <Point X="28.064359375" Y="-1.314072387695" />
                  <Point X="28.051037109375" Y="-1.458842895508" />
                  <Point X="28.048630859375" Y="-1.485002685547" />
                  <Point X="28.05636328125" Y="-1.516623291016" />
                  <Point X="28.14146484375" Y="-1.648994628906" />
                  <Point X="28.15684375" Y="-1.67291394043" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.353103515625" Y="-1.827221069336" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.22775390625" Y="-2.763918212891" />
                  <Point X="29.204134765625" Y="-2.802138183594" />
                  <Point X="29.05710546875" Y="-3.011046386719" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.890763671875" Y="-2.915840576172" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312988281" />
                  <Point X="27.554322265625" Y="-2.220260009766" />
                  <Point X="27.521251953125" Y="-2.214287353516" />
                  <Point X="27.489078125" Y="-2.219245361328" />
                  <Point X="27.337033203125" Y="-2.299264892578" />
                  <Point X="27.309560546875" Y="-2.313724365234" />
                  <Point X="27.2886015625" Y="-2.334681884766" />
                  <Point X="27.20858203125" Y="-2.486725830078" />
                  <Point X="27.19412109375" Y="-2.514199951172" />
                  <Point X="27.1891640625" Y="-2.546374511719" />
                  <Point X="27.22221875" Y="-2.729393798828" />
                  <Point X="27.22819140625" Y="-2.76246484375" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.352744140625" Y="-2.984087646484" />
                  <Point X="27.98667578125" Y="-4.082088378906" />
                  <Point X="27.86333203125" Y="-4.170189941406" />
                  <Point X="27.835294921875" Y="-4.190215332031" />
                  <Point X="27.67977734375" Y="-4.290879882812" />
                  <Point X="27.551048828125" Y="-4.123117675781" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.49004296875" Y="-2.864418212891" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.228390625" Y="-2.853883300781" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509277344" />
                  <Point X="26.01289453125" Y="-2.995256835938" />
                  <Point X="25.985349609375" Y="-3.018159667969" />
                  <Point X="25.96845703125" Y="-3.045985839844" />
                  <Point X="25.92287890625" Y="-3.255681884766" />
                  <Point X="25.914642578125" Y="-3.293573730469" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.9475546875" Y="-3.566125" />
                  <Point X="26.127642578125" Y="-4.934028320312" />
                  <Point X="26.020861328125" Y="-4.957434570312" />
                  <Point X="25.99433984375" Y="-4.963248535156" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#202" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.163669777233" Y="4.965948429041" Z="2.15" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.15" />
                  <Point X="-0.314258882436" Y="5.062160073588" Z="2.15" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.15" />
                  <Point X="-1.101280782759" Y="4.950889373162" Z="2.15" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.15" />
                  <Point X="-1.714207942376" Y="4.493024324154" Z="2.15" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.15" />
                  <Point X="-1.712586016317" Y="4.427512530203" Z="2.15" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.15" />
                  <Point X="-1.755103047005" Y="4.334517154784" Z="2.15" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.15" />
                  <Point X="-1.853671243615" Y="4.30731043692" Z="2.15" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.15" />
                  <Point X="-2.103684979599" Y="4.570018414861" Z="2.15" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.15" />
                  <Point X="-2.234110888084" Y="4.554444888864" Z="2.15" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.15" />
                  <Point X="-2.877153766216" Y="4.178052865458" Z="2.15" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.15" />
                  <Point X="-3.059244135045" Y="3.240285921222" Z="2.15" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.15" />
                  <Point X="-3.000379207578" Y="3.127220155028" Z="2.15" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.15" />
                  <Point X="-3.003333343452" Y="3.045470224434" Z="2.15" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.15" />
                  <Point X="-3.067856310579" Y="2.995185491595" Z="2.15" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.15" />
                  <Point X="-3.693573427075" Y="3.320949813701" Z="2.15" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.15" />
                  <Point X="-3.856926107548" Y="3.297203623759" Z="2.15" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.15" />
                  <Point X="-4.259706916488" Y="2.757222384293" Z="2.15" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.15" />
                  <Point X="-3.826816192525" Y="1.710781445037" Z="2.15" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.15" />
                  <Point X="-3.692010804545" Y="1.602090797978" Z="2.15" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.15" />
                  <Point X="-3.670594564135" Y="1.544597651394" Z="2.15" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.15" />
                  <Point X="-3.700870770064" Y="1.491236032855" Z="2.15" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.15" />
                  <Point X="-4.653719074286" Y="1.59342816896" Z="2.15" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.15" />
                  <Point X="-4.84042189999" Y="1.526563860792" Z="2.15" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.15" />
                  <Point X="-4.986221491439" Y="0.947441340002" Z="2.15" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.15" />
                  <Point X="-3.803641220994" Y="0.10991506566" Z="2.15" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.15" />
                  <Point X="-3.572313344088" Y="0.046121099738" Z="2.15" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.15" />
                  <Point X="-3.54739203789" Y="0.025245197075" Z="2.15" />
                  <Point X="-3.539556741714" Y="0" Z="2.15" />
                  <Point X="-3.540972563245" Y="-0.004561753986" Z="2.15" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.15" />
                  <Point X="-3.553055251035" Y="-0.032754883395" Z="2.15" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.15" />
                  <Point X="-4.833246567779" Y="-0.385796991096" Z="2.15" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.15" />
                  <Point X="-5.048440895264" Y="-0.529749786357" Z="2.15" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.15" />
                  <Point X="-4.961884811321" Y="-1.071011651928" Z="2.15" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.15" />
                  <Point X="-3.468274258788" Y="-1.339659962431" Z="2.15" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.15" />
                  <Point X="-3.215105906256" Y="-1.309248716557" Z="2.15" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.15" />
                  <Point X="-3.193855427279" Y="-1.327002438382" Z="2.15" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.15" />
                  <Point X="-4.303558679517" Y="-2.198695292367" Z="2.15" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.15" />
                  <Point X="-4.457975395942" Y="-2.426988431973" Z="2.15" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.15" />
                  <Point X="-4.156231102297" Y="-2.913680794241" Z="2.15" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.15" />
                  <Point X="-2.770173428233" Y="-2.66942176647" Z="2.15" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.15" />
                  <Point X="-2.570184422656" Y="-2.558146046475" Z="2.15" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.15" />
                  <Point X="-3.185995246699" Y="-3.664904954259" Z="2.15" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.15" />
                  <Point X="-3.237262384408" Y="-3.910487813612" Z="2.15" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.15" />
                  <Point X="-2.823234752924" Y="-4.219135963338" Z="2.15" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.15" />
                  <Point X="-2.260641021423" Y="-4.201307544716" Z="2.15" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.15" />
                  <Point X="-2.186742267362" Y="-4.130072441285" Z="2.15" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.15" />
                  <Point X="-1.920875453658" Y="-3.987189855991" Z="2.15" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.15" />
                  <Point X="-1.622967837146" Y="-4.035683170517" Z="2.15" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.15" />
                  <Point X="-1.416142659744" Y="-4.255510314051" Z="2.15" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.15" />
                  <Point X="-1.405719224011" Y="-4.823447926743" Z="2.15" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.15" />
                  <Point X="-1.367844575027" Y="-4.891146833279" Z="2.15" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.15" />
                  <Point X="-1.071529596547" Y="-4.964487536139" Z="2.15" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.15" />
                  <Point X="-0.478393061928" Y="-3.747571253982" Z="2.15" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.15" />
                  <Point X="-0.392029353374" Y="-3.482670104242" Z="2.15" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.15" />
                  <Point X="-0.214584171196" Y="-3.270838376283" Z="2.15" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.15" />
                  <Point X="0.038774908164" Y="-3.216273669005" Z="2.15" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.15" />
                  <Point X="0.278416506058" Y="-3.318975651526" Z="2.15" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.15" />
                  <Point X="0.756361798474" Y="-4.784964775834" Z="2.15" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.15" />
                  <Point X="0.845268226093" Y="-5.008749011715" Z="2.15" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.15" />
                  <Point X="1.025411460958" Y="-4.974995027378" Z="2.15" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.15" />
                  <Point X="0.990970496017" Y="-3.528318710698" Z="2.15" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.15" />
                  <Point X="0.965581691113" Y="-3.235022060063" Z="2.15" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.15" />
                  <Point X="1.038705319811" Y="-3.002422996756" Z="2.15" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.15" />
                  <Point X="1.226816108384" Y="-2.872392837073" Z="2.15" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.15" />
                  <Point X="1.456847569832" Y="-2.875196379468" Z="2.15" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.15" />
                  <Point X="2.505224313087" Y="-4.122275785459" Z="2.15" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.15" />
                  <Point X="2.691924840023" Y="-4.307311056403" Z="2.15" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.15" />
                  <Point X="2.886235342472" Y="-4.17959729068" Z="2.15" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.15" />
                  <Point X="2.3898876448" Y="-2.92780766051" Z="2.15" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.15" />
                  <Point X="2.265264262037" Y="-2.689227429196" Z="2.15" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.15" />
                  <Point X="2.246670503243" Y="-2.478734157651" Z="2.15" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.15" />
                  <Point X="2.354164220721" Y="-2.312230806724" Z="2.15" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.15" />
                  <Point X="2.539279374316" Y="-2.238183747454" Z="2.15" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.15" />
                  <Point X="3.859605936597" Y="-2.927861855475" Z="2.15" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.15" />
                  <Point X="4.091837447857" Y="-3.008543650596" Z="2.15" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.15" />
                  <Point X="4.264130559702" Y="-2.758923394349" Z="2.15" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.15" />
                  <Point X="3.377384313066" Y="-1.75627344523" Z="2.15" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.15" />
                  <Point X="3.177364985657" Y="-1.590673811254" Z="2.15" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.15" />
                  <Point X="3.094669716887" Y="-1.432142766789" Z="2.15" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.15" />
                  <Point X="3.124787051296" Y="-1.267172345715" Z="2.15" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.15" />
                  <Point X="3.245522732311" Y="-1.149344519326" Z="2.15" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.15" />
                  <Point X="4.676262312617" Y="-1.284035707983" Z="2.15" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.15" />
                  <Point X="4.919928438168" Y="-1.257789143722" Z="2.15" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.15" />
                  <Point X="5.000097077788" Y="-0.886991527463" Z="2.15" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.15" />
                  <Point X="3.946918535001" Y="-0.274123369331" Z="2.15" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.15" />
                  <Point X="3.733794666799" Y="-0.212627040107" Z="2.15" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.15" />
                  <Point X="3.646947894974" Y="-0.156513810802" Z="2.15" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.15" />
                  <Point X="3.597105117137" Y="-0.081824937249" Z="2.15" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.15" />
                  <Point X="3.584266333288" Y="0.014785593935" Z="2.15" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.15" />
                  <Point X="3.608431543426" Y="0.107434927773" Z="2.15" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.15" />
                  <Point X="3.669600747553" Y="0.175521804135" Z="2.15" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.15" />
                  <Point X="4.849049282305" Y="0.515848563438" Z="2.15" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.15" />
                  <Point X="5.037929222781" Y="0.633941352173" Z="2.15" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.15" />
                  <Point X="4.966605402464" Y="1.056140654983" Z="2.15" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.15" />
                  <Point X="3.680085390874" Y="1.250587941347" Z="2.15" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.15" />
                  <Point X="3.448710735884" Y="1.223928629826" Z="2.15" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.15" />
                  <Point X="3.358302367619" Y="1.240468251649" Z="2.15" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.15" />
                  <Point X="3.291963665304" Y="1.284850135515" Z="2.15" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.15" />
                  <Point X="3.248556879639" Y="1.359821929941" Z="2.15" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.15" />
                  <Point X="3.236886129982" Y="1.444128105274" Z="2.15" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.15" />
                  <Point X="3.263959160006" Y="1.520850320099" Z="2.15" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.15" />
                  <Point X="4.273697465438" Y="2.321942313252" Z="2.15" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.15" />
                  <Point X="4.415306254986" Y="2.508050962422" Z="2.15" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.15" />
                  <Point X="4.202077570637" Y="2.850927106024" Z="2.15" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.15" />
                  <Point X="2.738277167922" Y="2.398865127514" Z="2.15" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.15" />
                  <Point X="2.497590645751" Y="2.263712990604" Z="2.15" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.15" />
                  <Point X="2.418966645278" Y="2.246810354269" Z="2.15" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.15" />
                  <Point X="2.350477813669" Y="2.260474892056" Z="2.15" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.15" />
                  <Point X="2.290283708179" Y="2.306547046713" Z="2.15" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.15" />
                  <Point X="2.25261934849" Y="2.370791798555" Z="2.15" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.15" />
                  <Point X="2.248814952054" Y="2.441878930131" Z="2.15" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.15" />
                  <Point X="2.996760225345" Y="3.773861914033" Z="2.15" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.15" />
                  <Point X="3.071215645899" Y="4.043088649953" Z="2.15" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.15" />
                  <Point X="2.692627146094" Y="4.304495408114" Z="2.15" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.15" />
                  <Point X="2.292749977774" Y="4.530222354808" Z="2.15" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.15" />
                  <Point X="1.878636809431" Y="4.717025488674" Z="2.15" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.15" />
                  <Point X="1.416619883669" Y="4.872460511231" Z="2.15" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.15" />
                  <Point X="0.760124627852" Y="5.01695554937" Z="2.15" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.15" />
                  <Point X="0.02957478432" Y="4.46549907691" Z="2.15" />
                  <Point X="0" Y="4.355124473572" Z="2.15" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>