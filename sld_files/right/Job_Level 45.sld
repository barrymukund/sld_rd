<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#205" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3218" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.91046484375" Y="-4.808145507812" />
                  <Point X="25.5633046875" Y="-3.512524902344" />
                  <Point X="25.55772265625" Y="-3.497139160156" />
                  <Point X="25.542365234375" Y="-3.467377197266" />
                  <Point X="25.400470703125" Y="-3.262936035156" />
                  <Point X="25.37863671875" Y="-3.231476806641" />
                  <Point X="25.35675390625" Y="-3.209022460938" />
                  <Point X="25.330498046875" Y="-3.189777832031" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.08292578125" Y="-3.107522216797" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766357422" />
                  <Point X="24.9913359375" Y="-3.092766357422" />
                  <Point X="24.96317578125" Y="-3.097035644531" />
                  <Point X="24.74360546875" Y="-3.165182373047" />
                  <Point X="24.70981640625" Y="-3.175668945312" />
                  <Point X="24.681814453125" Y="-3.189777832031" />
                  <Point X="24.65555859375" Y="-3.209022460938" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.491783203125" Y="-3.435918212891" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481571533203" />
                  <Point X="24.449009765625" Y="-3.512523925781" />
                  <Point X="24.395587890625" Y="-3.711893066406" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.95635546875" Y="-4.852278808594" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.753583984375" Y="-4.802362304688" />
                  <Point X="23.7850390625" Y="-4.563438964844" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.73103515625" Y="-4.252511230469" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.18296484375" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.474203125" Y="-3.953919677734" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.088669921875" Y="-3.873380859375" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.016580078125" Y="-3.873708984375" />
                  <Point X="22.98553125" Y="-3.882029296875" />
                  <Point X="22.957341796875" Y="-3.894802246094" />
                  <Point X="22.73377734375" Y="-4.044183349609" />
                  <Point X="22.699376953125" Y="-4.067170166016" />
                  <Point X="22.68721484375" Y="-4.076822021484" />
                  <Point X="22.664900390625" Y="-4.0994609375" />
                  <Point X="22.634908203125" Y="-4.138546386719" />
                  <Point X="22.519853515625" Y="-4.288489746094" />
                  <Point X="22.250607421875" Y="-4.121779785156" />
                  <Point X="22.198287109375" Y="-4.089384277344" />
                  <Point X="21.89527734375" Y="-3.856077636719" />
                  <Point X="21.931080078125" Y="-3.794066894531" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647654541016" />
                  <Point X="22.593412109375" Y="-2.616128173828" />
                  <Point X="22.59442578125" Y="-2.585194091797" />
                  <Point X="22.58544140625" Y="-2.555575927734" />
                  <Point X="22.571224609375" Y="-2.526747070312" />
                  <Point X="22.553197265625" Y="-2.50158984375" />
                  <Point X="22.5381875" Y="-2.486579589844" />
                  <Point X="22.535875" Y="-2.484265625" />
                  <Point X="22.5107109375" Y="-2.466226318359" />
                  <Point X="22.48187890625" Y="-2.452002685547" />
                  <Point X="22.4522578125" Y="-2.443013427734" />
                  <Point X="22.4213203125" Y="-2.444023925781" />
                  <Point X="22.3897890625" Y="-2.450294189453" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="22.188865234375" Y="-2.560473876953" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="20.95847265625" Y="-2.848162109375" />
                  <Point X="20.917138671875" Y="-2.793859375" />
                  <Point X="20.693857421875" Y="-2.419450927734" />
                  <Point X="20.765228515625" Y="-2.364686279297" />
                  <Point X="21.894044921875" Y="-1.498513549805" />
                  <Point X="21.915423828125" Y="-1.475591308594" />
                  <Point X="21.933388671875" Y="-1.448458740234" />
                  <Point X="21.946142578125" Y="-1.419833374023" />
                  <Point X="21.9528203125" Y="-1.394053100586" />
                  <Point X="21.95384765625" Y="-1.39008605957" />
                  <Point X="21.95665234375" Y="-1.359654418945" />
                  <Point X="21.954443359375" Y="-1.327985839844" />
                  <Point X="21.947443359375" Y="-1.298242797852" />
                  <Point X="21.93136328125" Y="-1.272260620117" />
                  <Point X="21.910533203125" Y="-1.248305053711" />
                  <Point X="21.887029296875" Y="-1.228768188477" />
                  <Point X="21.864078125" Y="-1.215260253906" />
                  <Point X="21.860546875" Y="-1.213181518555" />
                  <Point X="21.831287109375" Y="-1.201958496094" />
                  <Point X="21.7993984375" Y="-1.195475585938" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="21.551" Y="-1.222962036133" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.18208984375" Y="-1.055944580078" />
                  <Point X="20.16592578125" Y="-0.992653259277" />
                  <Point X="20.107576171875" Y="-0.584698425293" />
                  <Point X="20.181232421875" Y="-0.564962524414" />
                  <Point X="21.467125" Y="-0.220408279419" />
                  <Point X="21.482509765625" Y="-0.21482649231" />
                  <Point X="21.51226953125" Y="-0.19947076416" />
                  <Point X="21.536322265625" Y="-0.18277746582" />
                  <Point X="21.5400234375" Y="-0.180208724976" />
                  <Point X="21.562478515625" Y="-0.158324645996" />
                  <Point X="21.58172265625" Y="-0.132069473267" />
                  <Point X="21.59583203125" Y="-0.10406855011" />
                  <Point X="21.603849609375" Y="-0.078236717224" />
                  <Point X="21.605087890625" Y="-0.074247360229" />
                  <Point X="21.609357421875" Y="-0.046068092346" />
                  <Point X="21.6093515625" Y="-0.016439979553" />
                  <Point X="21.60508203125" Y="0.011703926086" />
                  <Point X="21.597068359375" Y="0.037521038055" />
                  <Point X="21.595859375" Y="0.041423721313" />
                  <Point X="21.581736328125" Y="0.069485961914" />
                  <Point X="21.562486328125" Y="0.095755561829" />
                  <Point X="21.5400234375" Y="0.117648742676" />
                  <Point X="21.515970703125" Y="0.134341888428" />
                  <Point X="21.504833984375" Y="0.140992736816" />
                  <Point X="21.48556640625" Y="0.150781539917" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="21.26925390625" Y="0.210867904663" />
                  <Point X="20.108185546875" Y="0.521975463867" />
                  <Point X="20.165095703125" Y="0.906575927734" />
                  <Point X="20.17551171875" Y="0.976968688965" />
                  <Point X="20.29644921875" Y="1.423267822266" />
                  <Point X="20.31257421875" Y="1.421144897461" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255017578125" Y="1.299341796875" />
                  <Point X="21.276578125" Y="1.301228271484" />
                  <Point X="21.29686328125" Y="1.305263671875" />
                  <Point X="21.35009765625" Y="1.322048461914" />
                  <Point X="21.3582890625" Y="1.324631225586" />
                  <Point X="21.37722265625" Y="1.332961303711" />
                  <Point X="21.395966796875" Y="1.343783081055" />
                  <Point X="21.4126484375" Y="1.356014160156" />
                  <Point X="21.426287109375" Y="1.371566162109" />
                  <Point X="21.438701171875" Y="1.389295288086" />
                  <Point X="21.448650390625" Y="1.407431762695" />
                  <Point X="21.470009765625" Y="1.459000610352" />
                  <Point X="21.473296875" Y="1.466935791016" />
                  <Point X="21.479083984375" Y="1.486788452148" />
                  <Point X="21.48284375" Y="1.508105957031" />
                  <Point X="21.484197265625" Y="1.528750732422" />
                  <Point X="21.481048828125" Y="1.549198974609" />
                  <Point X="21.4754453125" Y="1.570107421875" />
                  <Point X="21.46794921875" Y="1.589380615234" />
                  <Point X="21.44217578125" Y="1.638891601563" />
                  <Point X="21.438208984375" Y="1.646510253906" />
                  <Point X="21.426712890625" Y="1.663715454102" />
                  <Point X="21.41280078125" Y="1.680292480469" />
                  <Point X="21.39786328125" Y="1.694590332031" />
                  <Point X="21.28436328125" Y="1.781682006836" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.87837109375" Y="2.664313232422" />
                  <Point X="20.9188515625" Y="2.7336640625" />
                  <Point X="21.24949609375" Y="3.158662353516" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.92734765625" Y="2.819309814453" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.00098828125" Y="2.826505126953" />
                  <Point X="22.019541015625" Y="2.835655029297" />
                  <Point X="22.037794921875" Y="2.847284912109" />
                  <Point X="22.053921875" Y="2.860229248047" />
                  <Point X="22.106548828125" Y="2.912854736328" />
                  <Point X="22.114646484375" Y="2.920952636719" />
                  <Point X="22.127595703125" Y="2.937085449219" />
                  <Point X="22.139224609375" Y="2.955339355469" />
                  <Point X="22.14837109375" Y="2.973888427734" />
                  <Point X="22.1532890625" Y="2.993977783203" />
                  <Point X="22.156115234375" Y="3.015436035156" />
                  <Point X="22.15656640625" Y="3.036120361328" />
                  <Point X="22.150080078125" Y="3.110261230469" />
                  <Point X="22.14908203125" Y="3.121669921875" />
                  <Point X="22.145044921875" Y="3.141962158203" />
                  <Point X="22.13853515625" Y="3.162604736328" />
                  <Point X="22.130205078125" Y="3.181534179688" />
                  <Point X="22.079908203125" Y="3.268648193359" />
                  <Point X="21.81666796875" Y="3.724595947266" />
                  <Point X="22.228884765625" Y="4.040637939453" />
                  <Point X="22.29937890625" Y="4.094686279297" />
                  <Point X="22.832962890625" Y="4.391134277344" />
                  <Point X="22.9568046875" Y="4.229740722656" />
                  <Point X="22.971111328125" Y="4.214796875" />
                  <Point X="22.987693359375" Y="4.200883789062" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.08740625" Y="4.146437988281" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.119384765625" Y="4.132330078125" />
                  <Point X="23.1402890625" Y="4.126729003906" />
                  <Point X="23.16073046875" Y="4.123582519531" />
                  <Point X="23.181369140625" Y="4.124934570312" />
                  <Point X="23.20268359375" Y="4.128691894531" />
                  <Point X="23.222546875" Y="4.134481445312" />
                  <Point X="23.308494140625" Y="4.170083007812" />
                  <Point X="23.321720703125" Y="4.175561035156" />
                  <Point X="23.339853515625" Y="4.185508789062" />
                  <Point X="23.357583984375" Y="4.197923339844" />
                  <Point X="23.37313671875" Y="4.2115625" />
                  <Point X="23.385369140625" Y="4.228244628906" />
                  <Point X="23.39619140625" Y="4.246989746094" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.43249609375" Y="4.354645019531" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583984375" />
                  <Point X="23.44272265625" Y="4.410145507812" />
                  <Point X="23.442271484375" Y="4.430828125" />
                  <Point X="23.436552734375" Y="4.474260742188" />
                  <Point X="23.415798828125" Y="4.631897460938" />
                  <Point X="23.959111328125" Y="4.784223144531" />
                  <Point X="24.050369140625" Y="4.80980859375" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.866095703125" Y="4.286314941406" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.17198828125" Y="4.382492675781" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.764353515625" Y="4.840084472656" />
                  <Point X="25.84404296875" Y="4.83173828125" />
                  <Point X="26.401625" Y="4.69712109375" />
                  <Point X="26.48102734375" Y="4.677950195312" />
                  <Point X="26.843763671875" Y="4.546384277344" />
                  <Point X="26.894646484375" Y="4.527928710938" />
                  <Point X="27.2455859375" Y="4.363805664063" />
                  <Point X="27.294576171875" Y="4.340894042969" />
                  <Point X="27.63362109375" Y="4.143366210938" />
                  <Point X="27.68097265625" Y="4.115778320312" />
                  <Point X="27.94326171875" Y="3.929255126953" />
                  <Point X="27.8943046875" Y="3.844459228516" />
                  <Point X="27.14758203125" Y="2.55109765625" />
                  <Point X="27.142080078125" Y="2.539934814453" />
                  <Point X="27.133078125" Y="2.516058349609" />
                  <Point X="27.114470703125" Y="2.446479003906" />
                  <Point X="27.1121875" Y="2.435280761719" />
                  <Point X="27.107986328125" Y="2.405670166016" />
                  <Point X="27.107728515625" Y="2.380950927734" />
                  <Point X="27.114984375" Y="2.320784423828" />
                  <Point X="27.116099609375" Y="2.311532714844" />
                  <Point X="27.121439453125" Y="2.289619628906" />
                  <Point X="27.129705078125" Y="2.267525634766" />
                  <Point X="27.140072265625" Y="2.247470458984" />
                  <Point X="27.177302734375" Y="2.192604492187" />
                  <Point X="27.18442578125" Y="2.183380371094" />
                  <Point X="27.20345703125" Y="2.161635742187" />
                  <Point X="27.2216015625" Y="2.145591552734" />
                  <Point X="27.276466796875" Y="2.108362548828" />
                  <Point X="27.28491015625" Y="2.102634033203" />
                  <Point X="27.30495703125" Y="2.092270263672" />
                  <Point X="27.32704296875" Y="2.084005615234" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.409130859375" Y="2.071408447266" />
                  <Point X="27.42125" Y="2.070728027344" />
                  <Point X="27.449412109375" Y="2.07094921875" />
                  <Point X="27.47320703125" Y="2.074170898438" />
                  <Point X="27.542787109375" Y="2.09277734375" />
                  <Point X="27.54923828125" Y="2.094750244141" />
                  <Point X="27.57202734375" Y="2.102614990234" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="27.787556640625" Y="2.225050292969" />
                  <Point X="28.967326171875" Y="2.906190429688" />
                  <Point X="29.0951796875" Y="2.728503662109" />
                  <Point X="29.12328125" Y="2.689450439453" />
                  <Point X="29.26219921875" Y="2.459883544922" />
                  <Point X="29.213916015625" Y="2.422833496094" />
                  <Point X="28.23078515625" Y="1.668451171875" />
                  <Point X="28.221423828125" Y="1.660239013672" />
                  <Point X="28.20397265625" Y="1.641625854492" />
                  <Point X="28.153896484375" Y="1.576297363281" />
                  <Point X="28.147798828125" Y="1.567322387695" />
                  <Point X="28.131625" Y="1.540322753906" />
                  <Point X="28.121630859375" Y="1.517090209961" />
                  <Point X="28.1029765625" Y="1.450389282227" />
                  <Point X="28.10010546875" Y="1.440125610352" />
                  <Point X="28.09665234375" Y="1.417827270508" />
                  <Point X="28.0958359375" Y="1.394257202148" />
                  <Point X="28.097740234375" Y="1.371769165039" />
                  <Point X="28.1130546875" Y="1.297556274414" />
                  <Point X="28.11592578125" Y="1.286845947266" />
                  <Point X="28.125478515625" Y="1.258047485352" />
                  <Point X="28.136283203125" Y="1.235742797852" />
                  <Point X="28.177931640625" Y="1.172438476562" />
                  <Point X="28.18433984375" Y="1.162697265625" />
                  <Point X="28.198888671875" Y="1.145455200195" />
                  <Point X="28.2161328125" Y="1.129363769531" />
                  <Point X="28.23434765625" Y="1.116034912109" />
                  <Point X="28.294703125" Y="1.082060302734" />
                  <Point X="28.3051328125" Y="1.077000976562" />
                  <Point X="28.332396484375" Y="1.065774902344" />
                  <Point X="28.356119140625" Y="1.059438598633" />
                  <Point X="28.43772265625" Y="1.048653442383" />
                  <Point X="28.444021484375" Y="1.048033569336" />
                  <Point X="28.46965625" Y="1.04637097168" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.67726171875" Y="1.071874389648" />
                  <Point X="29.77683984375" Y="1.21663659668" />
                  <Point X="29.83387109375" Y="0.982370300293" />
                  <Point X="29.845939453125" Y="0.932791015625" />
                  <Point X="29.890865234375" Y="0.644238464355" />
                  <Point X="29.84326953125" Y="0.631485046387" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.7047890625" Y="0.325585601807" />
                  <Point X="28.681548828125" Y="0.315068481445" />
                  <Point X="28.601376953125" Y="0.268727050781" />
                  <Point X="28.5927265625" Y="0.263078033447" />
                  <Point X="28.56607421875" Y="0.243526351929" />
                  <Point X="28.54753125" Y="0.225576843262" />
                  <Point X="28.499427734375" Y="0.164281311035" />
                  <Point X="28.492025390625" Y="0.154849212646" />
                  <Point X="28.480298828125" Y="0.135564102173" />
                  <Point X="28.470525390625" Y="0.114097755432" />
                  <Point X="28.463681640625" Y="0.092602416992" />
                  <Point X="28.447646484375" Y="0.008875617027" />
                  <Point X="28.44623828125" Y="-0.001620838284" />
                  <Point X="28.443771484375" Y="-0.033309505463" />
                  <Point X="28.4451796875" Y="-0.058551937103" />
                  <Point X="28.46121484375" Y="-0.142278747559" />
                  <Point X="28.463681640625" Y="-0.155162399292" />
                  <Point X="28.470525390625" Y="-0.176657745361" />
                  <Point X="28.480298828125" Y="-0.198124084473" />
                  <Point X="28.492025390625" Y="-0.217409194946" />
                  <Point X="28.54012890625" Y="-0.278704864502" />
                  <Point X="28.547474609375" Y="-0.287016448975" />
                  <Point X="28.5691875" Y="-0.308867736816" />
                  <Point X="28.58903515625" Y="-0.324154998779" />
                  <Point X="28.669208984375" Y="-0.370496307373" />
                  <Point X="28.674458984375" Y="-0.373314758301" />
                  <Point X="28.698876953125" Y="-0.385454193115" />
                  <Point X="28.716580078125" Y="-0.392149963379" />
                  <Point X="28.889955078125" Y="-0.438605407715" />
                  <Point X="29.891474609375" Y="-0.706961853027" />
                  <Point X="29.86176171875" Y="-0.904042114258" />
                  <Point X="29.855025390625" Y="-0.948725158691" />
                  <Point X="29.80117578125" Y="-1.18469934082" />
                  <Point X="29.7310234375" Y="-1.175463623047" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.21730859375" Y="-1.039709960938" />
                  <Point X="28.193095703125" Y="-1.04497265625" />
                  <Point X="28.1639765625" Y="-1.056596557617" />
                  <Point X="28.1361484375" Y="-1.073489379883" />
                  <Point X="28.1123984375" Y="-1.093960571289" />
                  <Point X="28.0172890625" Y="-1.208347045898" />
                  <Point X="28.002654296875" Y="-1.225948608398" />
                  <Point X="27.987931640625" Y="-1.250334350586" />
                  <Point X="27.97658984375" Y="-1.27771875" />
                  <Point X="27.969759765625" Y="-1.305365722656" />
                  <Point X="27.95612890625" Y="-1.453501098633" />
                  <Point X="27.95403125" Y="-1.476296020508" />
                  <Point X="27.956349609375" Y="-1.50756628418" />
                  <Point X="27.96408203125" Y="-1.539187744141" />
                  <Point X="27.976453125" Y="-1.567996948242" />
                  <Point X="28.063533203125" Y="-1.703444946289" />
                  <Point X="28.07693359375" Y="-1.724287841797" />
                  <Point X="28.086935546875" Y="-1.73723840332" />
                  <Point X="28.110630859375" Y="-1.760909057617" />
                  <Point X="28.271521484375" Y="-1.884365966797" />
                  <Point X="29.213123046875" Y="-2.606882324219" />
                  <Point X="29.14380078125" Y="-2.719056396484" />
                  <Point X="29.124794921875" Y="-2.749808349609" />
                  <Point X="29.028982421875" Y="-2.885945556641" />
                  <Point X="28.9646953125" Y="-2.848829589844" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.566951171875" Y="-2.12600390625" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395507812" />
                  <Point X="27.474609375" Y="-2.125353515625" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.289255859375" Y="-2.217056396484" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246549804688" />
                  <Point X="27.22142578125" Y="-2.267509277344" />
                  <Point X="27.204533203125" Y="-2.290438720703" />
                  <Point X="27.12265234375" Y="-2.446016601562" />
                  <Point X="27.110052734375" Y="-2.469956787109" />
                  <Point X="27.100228515625" Y="-2.499735839844" />
                  <Point X="27.095271484375" Y="-2.531909912109" />
                  <Point X="27.09567578125" Y="-2.563260009766" />
                  <Point X="27.129498046875" Y="-2.750532958984" />
                  <Point X="27.134703125" Y="-2.779350341797" />
                  <Point X="27.13898828125" Y="-2.795143066406" />
                  <Point X="27.1518203125" Y="-2.826078369141" />
                  <Point X="27.255208984375" Y="-3.005154052734" />
                  <Point X="27.86128515625" Y="-4.054906738281" />
                  <Point X="27.804390625" Y="-4.095544921875" />
                  <Point X="27.7818515625" Y="-4.111643554688" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="27.64658984375" Y="-4.091572998047" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922177490234" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.53722265625" Y="-2.781811035156" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479990234375" Y="-2.751167236328" />
                  <Point X="26.448369140625" Y="-2.743435546875" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.21509765625" Y="-2.759705322266" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397705078" />
                  <Point X="26.128978515625" Y="-2.780740966797" />
                  <Point X="26.10459765625" Y="-2.7954609375" />
                  <Point X="25.948615234375" Y="-2.925154296875" />
                  <Point X="25.92461328125" Y="-2.945111328125" />
                  <Point X="25.904142578125" Y="-2.968859130859" />
                  <Point X="25.88725" Y="-2.996684082031" />
                  <Point X="25.875625" Y="-3.025807373047" />
                  <Point X="25.82898828125" Y="-3.240377197266" />
                  <Point X="25.821810546875" Y="-3.273395263672" />
                  <Point X="25.819724609375" Y="-3.289626464844" />
                  <Point X="25.8197421875" Y="-3.323120605469" />
                  <Point X="25.84904296875" Y="-3.545674316406" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="25.99699609375" Y="-4.865410644531" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.929318359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.97445703125" Y="-4.75901953125" />
                  <Point X="23.94157421875" Y="-4.75263671875" />
                  <Point X="23.858755859375" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575838867188" />
                  <Point X="23.879923828125" Y="-4.568099121094" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688964844" />
                  <Point X="23.824208984375" Y="-4.233977050781" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.811873046875" Y="-4.178467773438" />
                  <Point X="23.800970703125" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135463867188" />
                  <Point X="23.778259765625" Y="-4.107626464844" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.74979296875" Y="-4.070937744141" />
                  <Point X="23.738994140625" Y="-4.059779541016" />
                  <Point X="23.536841796875" Y="-3.882495117188" />
                  <Point X="23.505734375" Y="-3.85521484375" />
                  <Point X="23.49326171875" Y="-3.845965332031" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.0948828125" Y="-3.778584228516" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.007267578125" Y="-3.779166503906" />
                  <Point X="22.991990234375" Y="-3.781946533203" />
                  <Point X="22.96094140625" Y="-3.790266845703" />
                  <Point X="22.946322265625" Y="-3.795497802734" />
                  <Point X="22.9181328125" Y="-3.808270751953" />
                  <Point X="22.9045625" Y="-3.815812744141" />
                  <Point X="22.680998046875" Y="-3.965193847656" />
                  <Point X="22.64659765625" Y="-3.988180664062" />
                  <Point X="22.640322265625" Y="-3.992755859375" />
                  <Point X="22.619556640625" Y="-4.010133544922" />
                  <Point X="22.5972421875" Y="-4.032772460938" />
                  <Point X="22.589533203125" Y="-4.041627441406" />
                  <Point X="22.559541015625" Y="-4.080712890625" />
                  <Point X="22.49680078125" Y="-4.162479003906" />
                  <Point X="22.300619140625" Y="-4.041009033203" />
                  <Point X="22.252408203125" Y="-4.011157958984" />
                  <Point X="22.01913671875" Y="-3.831547363281" />
                  <Point X="22.658509765625" Y="-2.724120361328" />
                  <Point X="22.6651484375" Y="-2.710085205078" />
                  <Point X="22.67605078125" Y="-2.681120117188" />
                  <Point X="22.680314453125" Y="-2.666189453125" />
                  <Point X="22.6865859375" Y="-2.634663085938" />
                  <Point X="22.688361328125" Y="-2.619239501953" />
                  <Point X="22.689375" Y="-2.588305419922" />
                  <Point X="22.6853359375" Y="-2.557617675781" />
                  <Point X="22.6763515625" Y="-2.527999511719" />
                  <Point X="22.67064453125" Y="-2.51355859375" />
                  <Point X="22.656427734375" Y="-2.484729736328" />
                  <Point X="22.6484453125" Y="-2.471411865234" />
                  <Point X="22.63041796875" Y="-2.446254638672" />
                  <Point X="22.620373046875" Y="-2.434415771484" />
                  <Point X="22.605384765625" Y="-2.419426513672" />
                  <Point X="22.591224609375" Y="-2.407055419922" />
                  <Point X="22.566060546875" Y="-2.389016113281" />
                  <Point X="22.552740234375" Y="-2.381029541016" />
                  <Point X="22.523908203125" Y="-2.366805908203" />
                  <Point X="22.509466796875" Y="-2.361096679688" />
                  <Point X="22.479845703125" Y="-2.352107421875" />
                  <Point X="22.44915625" Y="-2.348063964844" />
                  <Point X="22.41821875" Y="-2.349074462891" />
                  <Point X="22.402791015625" Y="-2.350848388672" />
                  <Point X="22.371259765625" Y="-2.357118652344" />
                  <Point X="22.356328125" Y="-2.361382324219" />
                  <Point X="22.327357421875" Y="-2.372285400391" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="22.141365234375" Y="-2.478201416016" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="21.03406640625" Y="-2.790624023438" />
                  <Point X="20.995978515625" Y="-2.740584716797" />
                  <Point X="20.818734375" Y="-2.443374755859" />
                  <Point X="20.823060546875" Y="-2.440054931641" />
                  <Point X="21.951876953125" Y="-1.573882324219" />
                  <Point X="21.963517578125" Y="-1.563309204102" />
                  <Point X="21.984896484375" Y="-1.540386962891" />
                  <Point X="21.994634765625" Y="-1.528037841797" />
                  <Point X="22.012599609375" Y="-1.500905273438" />
                  <Point X="22.020166015625" Y="-1.487121704102" />
                  <Point X="22.032919921875" Y="-1.458496337891" />
                  <Point X="22.038107421875" Y="-1.443654541016" />
                  <Point X="22.04478515625" Y="-1.417874267578" />
                  <Point X="22.048447265625" Y="-1.3988046875" />
                  <Point X="22.051251953125" Y="-1.368373046875" />
                  <Point X="22.051421875" Y="-1.353043945312" />
                  <Point X="22.049212890625" Y="-1.321375366211" />
                  <Point X="22.046916015625" Y="-1.306222290039" />
                  <Point X="22.039916015625" Y="-1.276479248047" />
                  <Point X="22.028224609375" Y="-1.248248413086" />
                  <Point X="22.01214453125" Y="-1.222266235352" />
                  <Point X="22.003052734375" Y="-1.209925048828" />
                  <Point X="21.98222265625" Y="-1.185969482422" />
                  <Point X="21.971259765625" Y="-1.175248046875" />
                  <Point X="21.947755859375" Y="-1.155711181641" />
                  <Point X="21.93521484375" Y="-1.146895751953" />
                  <Point X="21.912263671875" Y="-1.133387817383" />
                  <Point X="21.894568359375" Y="-1.124482543945" />
                  <Point X="21.86530859375" Y="-1.113259521484" />
                  <Point X="21.850212890625" Y="-1.108862792969" />
                  <Point X="21.81832421875" Y="-1.102379882812" />
                  <Point X="21.80270703125" Y="-1.100533203125" />
                  <Point X="21.771380859375" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="21.538599609375" Y="-1.128774780273" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.274134765625" Y="-1.032433227539" />
                  <Point X="20.2592421875" Y="-0.974118225098" />
                  <Point X="20.213548828125" Y="-0.654654785156" />
                  <Point X="21.491712890625" Y="-0.312171325684" />
                  <Point X="21.499525390625" Y="-0.309712219238" />
                  <Point X="21.526072265625" Y="-0.299250213623" />
                  <Point X="21.55583203125" Y="-0.283894500732" />
                  <Point X="21.566435546875" Y="-0.277516052246" />
                  <Point X="21.59048828125" Y="-0.260822723389" />
                  <Point X="21.606328125" Y="-0.248243270874" />
                  <Point X="21.628783203125" Y="-0.226359146118" />
                  <Point X="21.639099609375" Y="-0.214485809326" />
                  <Point X="21.65834375" Y="-0.188230682373" />
                  <Point X="21.666560546875" Y="-0.174818511963" />
                  <Point X="21.680669921875" Y="-0.146817657471" />
                  <Point X="21.6865625" Y="-0.132228988647" />
                  <Point X="21.694580078125" Y="-0.106397285461" />
                  <Point X="21.699015625" Y="-0.088478713989" />
                  <Point X="21.70328515625" Y="-0.060299362183" />
                  <Point X="21.704357421875" Y="-0.046049251556" />
                  <Point X="21.7043515625" Y="-0.016421127319" />
                  <Point X="21.70327734375" Y="-0.002191226959" />
                  <Point X="21.6990078125" Y="0.025952749252" />
                  <Point X="21.6958125" Y="0.039866527557" />
                  <Point X="21.687798828125" Y="0.065683662415" />
                  <Point X="21.68071875" Y="0.08413117981" />
                  <Point X="21.666595703125" Y="0.112193565369" />
                  <Point X="21.658365234375" Y="0.125638290405" />
                  <Point X="21.639115234375" Y="0.151907836914" />
                  <Point X="21.62879296875" Y="0.163787857056" />
                  <Point X="21.606330078125" Y="0.185681045532" />
                  <Point X="21.594189453125" Y="0.195694366455" />
                  <Point X="21.57013671875" Y="0.212387390137" />
                  <Point X="21.54786328125" Y="0.225688995361" />
                  <Point X="21.528595703125" Y="0.235477752686" />
                  <Point X="21.519560546875" Y="0.23949130249" />
                  <Point X="21.491712890625" Y="0.249611190796" />
                  <Point X="21.293841796875" Y="0.302630767822" />
                  <Point X="20.2145546875" Y="0.591824829102" />
                  <Point X="20.259072265625" Y="0.892669921875" />
                  <Point X="20.26866796875" Y="0.957522583008" />
                  <Point X="20.3664140625" Y="1.318236938477" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.2529453125" Y="1.204364379883" />
                  <Point X="21.263298828125" Y="1.204703369141" />
                  <Point X="21.284859375" Y="1.20658984375" />
                  <Point X="21.29511328125" Y="1.208054077148" />
                  <Point X="21.3153984375" Y="1.212089477539" />
                  <Point X="21.3254296875" Y="1.214660766602" />
                  <Point X="21.3786640625" Y="1.23144543457" />
                  <Point X="21.396546875" Y="1.237675170898" />
                  <Point X="21.41548046875" Y="1.246005126953" />
                  <Point X="21.42472265625" Y="1.250688476562" />
                  <Point X="21.443466796875" Y="1.261510375977" />
                  <Point X="21.452140625" Y="1.267169921875" />
                  <Point X="21.468822265625" Y="1.279401000977" />
                  <Point X="21.48407421875" Y="1.293376464844" />
                  <Point X="21.497712890625" Y="1.308928466797" />
                  <Point X="21.504107421875" Y="1.317076416016" />
                  <Point X="21.516521484375" Y="1.334805541992" />
                  <Point X="21.5219921875" Y="1.343604125977" />
                  <Point X="21.53194140625" Y="1.361740600586" />
                  <Point X="21.536419921875" Y="1.371078491211" />
                  <Point X="21.557779296875" Y="1.422647338867" />
                  <Point X="21.5645" Y="1.440349609375" />
                  <Point X="21.570287109375" Y="1.460202270508" />
                  <Point X="21.572640625" Y="1.470287963867" />
                  <Point X="21.576400390625" Y="1.49160546875" />
                  <Point X="21.577640625" Y="1.501890869141" />
                  <Point X="21.578994140625" Y="1.522535522461" />
                  <Point X="21.57808984375" Y="1.543207641602" />
                  <Point X="21.57494140625" Y="1.563655761719" />
                  <Point X="21.572810546875" Y="1.573791503906" />
                  <Point X="21.56720703125" Y="1.594699829102" />
                  <Point X="21.563984375" Y="1.604543701172" />
                  <Point X="21.55648828125" Y="1.623816894531" />
                  <Point X="21.55221484375" Y="1.63324621582" />
                  <Point X="21.52644140625" Y="1.682757202148" />
                  <Point X="21.51719921875" Y="1.699289428711" />
                  <Point X="21.505703125" Y="1.716494506836" />
                  <Point X="21.499482421875" Y="1.724786254883" />
                  <Point X="21.4855703125" Y="1.74136328125" />
                  <Point X="21.478490234375" Y="1.748920898438" />
                  <Point X="21.463552734375" Y="1.76321875" />
                  <Point X="21.4556953125" Y="1.769958862305" />
                  <Point X="21.3421953125" Y="1.857050537109" />
                  <Point X="20.77238671875" Y="2.294280761719" />
                  <Point X="20.96041796875" Y="2.616423583984" />
                  <Point X="20.99771484375" Y="2.680321777344" />
                  <Point X="21.273662109375" Y="3.035013427734" />
                  <Point X="21.74584375" Y="2.762398681641" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774015625" Y="2.749385742188" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.919068359375" Y="2.724671386719" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.023568359375" Y="2.734227783203" />
                  <Point X="22.0430078125" Y="2.741303466797" />
                  <Point X="22.061560546875" Y="2.750453369141" />
                  <Point X="22.070587890625" Y="2.755534667969" />
                  <Point X="22.088841796875" Y="2.767164550781" />
                  <Point X="22.097259765625" Y="2.773198242188" />
                  <Point X="22.11338671875" Y="2.786142578125" />
                  <Point X="22.121095703125" Y="2.793053222656" />
                  <Point X="22.17372265625" Y="2.845678710938" />
                  <Point X="22.188732421875" Y="2.861486328125" />
                  <Point X="22.201681640625" Y="2.877619140625" />
                  <Point X="22.20771875" Y="2.886042236328" />
                  <Point X="22.21934765625" Y="2.904296142578" />
                  <Point X="22.2244296875" Y="2.913325195312" />
                  <Point X="22.233576171875" Y="2.931874267578" />
                  <Point X="22.240646484375" Y="2.951299072266" />
                  <Point X="22.245564453125" Y="2.971388427734" />
                  <Point X="22.2474765625" Y="2.981572998047" />
                  <Point X="22.250302734375" Y="3.00303125" />
                  <Point X="22.251091796875" Y="3.013364257812" />
                  <Point X="22.25154296875" Y="3.034048583984" />
                  <Point X="22.251205078125" Y="3.044399902344" />
                  <Point X="22.24471875" Y="3.118540771484" />
                  <Point X="22.242255859375" Y="3.140206787109" />
                  <Point X="22.23821875" Y="3.160499023438" />
                  <Point X="22.235646484375" Y="3.170533935547" />
                  <Point X="22.22913671875" Y="3.191176513672" />
                  <Point X="22.22548828125" Y="3.200869140625" />
                  <Point X="22.217158203125" Y="3.219798583984" />
                  <Point X="22.2124765625" Y="3.229035400391" />
                  <Point X="22.1621796875" Y="3.316149414062" />
                  <Point X="21.94061328125" Y="3.699915039062" />
                  <Point X="22.2866875" Y="3.96524609375" />
                  <Point X="22.351634765625" Y="4.015041748047" />
                  <Point X="22.807474609375" Y="4.268296875" />
                  <Point X="22.881435546875" Y="4.171908203125" />
                  <Point X="22.888181640625" Y="4.164044433594" />
                  <Point X="22.90248828125" Y="4.149100585938" />
                  <Point X="22.910048828125" Y="4.142020507812" />
                  <Point X="22.926630859375" Y="4.128107421875" />
                  <Point X="22.934916015625" Y="4.121893554688" />
                  <Point X="22.952111328125" Y="4.110404296875" />
                  <Point X="22.961021484375" Y="4.10512890625" />
                  <Point X="23.0435390625" Y="4.062172119141" />
                  <Point X="23.065673828125" Y="4.051287353516" />
                  <Point X="23.084953125" Y="4.043789794922" />
                  <Point X="23.094796875" Y="4.040566894531" />
                  <Point X="23.115701171875" Y="4.034965820313" />
                  <Point X="23.1258359375" Y="4.032834716797" />
                  <Point X="23.14627734375" Y="4.029688232422" />
                  <Point X="23.16694140625" Y="4.028785644531" />
                  <Point X="23.187580078125" Y="4.030137695312" />
                  <Point X="23.197861328125" Y="4.031377197266" />
                  <Point X="23.21917578125" Y="4.035134521484" />
                  <Point X="23.229267578125" Y="4.037487060547" />
                  <Point X="23.249130859375" Y="4.043276611328" />
                  <Point X="23.25890234375" Y="4.046713378906" />
                  <Point X="23.344849609375" Y="4.082314941406" />
                  <Point X="23.3674140625" Y="4.092271484375" />
                  <Point X="23.385546875" Y="4.102219238281" />
                  <Point X="23.394341796875" Y="4.107688476562" />
                  <Point X="23.412072265625" Y="4.120103027344" />
                  <Point X="23.420220703125" Y="4.126498046875" />
                  <Point X="23.4357734375" Y="4.140137207031" />
                  <Point X="23.449748046875" Y="4.155386230469" />
                  <Point X="23.46198046875" Y="4.172068359375" />
                  <Point X="23.467642578125" Y="4.180745605469" />
                  <Point X="23.47846484375" Y="4.199490722656" />
                  <Point X="23.483146484375" Y="4.208729003906" />
                  <Point X="23.4914765625" Y="4.227660644531" />
                  <Point X="23.495125" Y="4.237354003906" />
                  <Point X="23.523099609375" Y="4.326077636719" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370050292969" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401864257812" />
                  <Point X="23.53769921875" Y="4.412217285156" />
                  <Point X="23.537248046875" Y="4.432899902344" />
                  <Point X="23.536458984375" Y="4.443229492188" />
                  <Point X="23.530740234375" Y="4.486662109375" />
                  <Point X="23.520736328125" Y="4.562654296875" />
                  <Point X="23.9847578125" Y="4.69275" />
                  <Point X="24.068830078125" Y="4.716320800781" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.77433203125" Y="4.261727050781" />
                  <Point X="24.779564453125" Y="4.247104980469" />
                  <Point X="24.79233984375" Y="4.218911621094" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.247104980469" />
                  <Point X="25.23798046875" Y="4.2617265625" />
                  <Point X="25.263751953125" Y="4.357904296875" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.754458984375" Y="4.745601074219" />
                  <Point X="25.82787890625" Y="4.737911621094" />
                  <Point X="26.379330078125" Y="4.604774414062" />
                  <Point X="26.453595703125" Y="4.586843261719" />
                  <Point X="26.81137109375" Y="4.457077148438" />
                  <Point X="26.858263671875" Y="4.440068847656" />
                  <Point X="27.205341796875" Y="4.277751464844" />
                  <Point X="27.250453125" Y="4.256653808594" />
                  <Point X="27.585798828125" Y="4.061281005859" />
                  <Point X="27.629419921875" Y="4.035866210938" />
                  <Point X="27.81778125" Y="3.901915771484" />
                  <Point X="27.812033203125" Y="3.891959228516" />
                  <Point X="27.065310546875" Y="2.59859765625" />
                  <Point X="27.06237109375" Y="2.593096923828" />
                  <Point X="27.0531875" Y="2.573448974609" />
                  <Point X="27.044185546875" Y="2.549572509766" />
                  <Point X="27.041302734375" Y="2.5406015625" />
                  <Point X="27.0226953125" Y="2.471022216797" />
                  <Point X="27.01812890625" Y="2.448625732422" />
                  <Point X="27.013927734375" Y="2.419015136719" />
                  <Point X="27.0129921875" Y="2.406660888672" />
                  <Point X="27.012734375" Y="2.381941650391" />
                  <Point X="27.013412109375" Y="2.369576660156" />
                  <Point X="27.02066796875" Y="2.30941015625" />
                  <Point X="27.02380078125" Y="2.289041015625" />
                  <Point X="27.029140625" Y="2.267127929688" />
                  <Point X="27.032462890625" Y="2.256332275391" />
                  <Point X="27.040728515625" Y="2.23423828125" />
                  <Point X="27.045314453125" Y="2.223900878906" />
                  <Point X="27.055681640625" Y="2.203845703125" />
                  <Point X="27.061462890625" Y="2.194127929688" />
                  <Point X="27.098693359375" Y="2.139261962891" />
                  <Point X="27.112939453125" Y="2.120813720703" />
                  <Point X="27.131970703125" Y="2.099069091797" />
                  <Point X="27.14052734375" Y="2.090468017578" />
                  <Point X="27.158671875" Y="2.074423828125" />
                  <Point X="27.168259765625" Y="2.066980712891" />
                  <Point X="27.223125" Y="2.029751708984" />
                  <Point X="27.241283203125" Y="2.018244262695" />
                  <Point X="27.261330078125" Y="2.007880493164" />
                  <Point X="27.271662109375" Y="2.003295776367" />
                  <Point X="27.293748046875" Y="1.99503112793" />
                  <Point X="27.30455078125" Y="1.991706542969" />
                  <Point X="27.32647265625" Y="1.986364501953" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.3977578125" Y="1.977091674805" />
                  <Point X="27.42199609375" Y="1.975730957031" />
                  <Point X="27.450158203125" Y="1.975952148438" />
                  <Point X="27.462158203125" Y="1.976808105469" />
                  <Point X="27.485953125" Y="1.980029907227" />
                  <Point X="27.497748046875" Y="1.982395629883" />
                  <Point X="27.567328125" Y="2.001002075195" />
                  <Point X="27.58023046875" Y="2.004947631836" />
                  <Point X="27.60301953125" Y="2.01281237793" />
                  <Point X="27.611453125" Y="2.016182495117" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="27.835056640625" Y="2.142777832031" />
                  <Point X="28.940404296875" Y="2.780950439453" />
                  <Point X="29.01806640625" Y="2.673017822266" />
                  <Point X="29.043966796875" Y="2.637024169922" />
                  <Point X="29.13688671875" Y="2.483471435547" />
                  <Point X="28.172951171875" Y="1.743818969727" />
                  <Point X="28.16813671875" Y="1.739866577148" />
                  <Point X="28.15212109375" Y="1.725216064453" />
                  <Point X="28.134669921875" Y="1.706602905273" />
                  <Point X="28.12857421875" Y="1.699420288086" />
                  <Point X="28.078498046875" Y="1.634091796875" />
                  <Point X="28.066302734375" Y="1.616141845703" />
                  <Point X="28.05012890625" Y="1.589142211914" />
                  <Point X="28.044357421875" Y="1.577863525391" />
                  <Point X="28.03436328125" Y="1.554630981445" />
                  <Point X="28.030140625" Y="1.542677124023" />
                  <Point X="28.011486328125" Y="1.475976196289" />
                  <Point X="28.006224609375" Y="1.4546640625" />
                  <Point X="28.002771484375" Y="1.432365722656" />
                  <Point X="28.001708984375" Y="1.421115844727" />
                  <Point X="28.000892578125" Y="1.397545776367" />
                  <Point X="28.001173828125" Y="1.386241210938" />
                  <Point X="28.003078125" Y="1.363753051758" />
                  <Point X="28.004701171875" Y="1.352569702148" />
                  <Point X="28.020015625" Y="1.278356811523" />
                  <Point X="28.0257578125" Y="1.256936035156" />
                  <Point X="28.035310546875" Y="1.228137573242" />
                  <Point X="28.039982421875" Y="1.216631591797" />
                  <Point X="28.050787109375" Y="1.194326904297" />
                  <Point X="28.056919921875" Y="1.183528320312" />
                  <Point X="28.098568359375" Y="1.120224121094" />
                  <Point X="28.111734375" Y="1.101432495117" />
                  <Point X="28.126283203125" Y="1.084190429688" />
                  <Point X="28.13407421875" Y="1.075998779297" />
                  <Point X="28.151318359375" Y="1.059907470703" />
                  <Point X="28.16003125" Y="1.052697753906" />
                  <Point X="28.17824609375" Y="1.039368896484" />
                  <Point X="28.187748046875" Y="1.033249633789" />
                  <Point X="28.248103515625" Y="0.999275085449" />
                  <Point X="28.268962890625" Y="0.98915637207" />
                  <Point X="28.2962265625" Y="0.977930297852" />
                  <Point X="28.307880859375" Y="0.973992553711" />
                  <Point X="28.331603515625" Y="0.96765612793" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.425275390625" Y="0.95447253418" />
                  <Point X="28.437873046875" Y="0.953232727051" />
                  <Point X="28.4635078125" Y="0.951570068359" />
                  <Point X="28.472796875" Y="0.951422973633" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="28.689662109375" Y="0.977687133789" />
                  <Point X="29.704703125" Y="1.111319946289" />
                  <Point X="29.74156640625" Y="0.959899230957" />
                  <Point X="29.7526875" Y="0.914210021973" />
                  <Point X="29.78387109375" Y="0.713920532227" />
                  <Point X="28.6919921875" Y="0.42135269165" />
                  <Point X="28.68603125" Y="0.419544250488" />
                  <Point X="28.66562109375" Y="0.412135681152" />
                  <Point X="28.642380859375" Y="0.401618530273" />
                  <Point X="28.6340078125" Y="0.397316802979" />
                  <Point X="28.5538359375" Y="0.35097543335" />
                  <Point X="28.53653515625" Y="0.339677398682" />
                  <Point X="28.5098828125" Y="0.320125762939" />
                  <Point X="28.5" Y="0.311784912109" />
                  <Point X="28.48145703125" Y="0.293835418701" />
                  <Point X="28.472796875" Y="0.284226654053" />
                  <Point X="28.424693359375" Y="0.222931152344" />
                  <Point X="28.410853515625" Y="0.204206741333" />
                  <Point X="28.399126953125" Y="0.184921585083" />
                  <Point X="28.393837890625" Y="0.174928771973" />
                  <Point X="28.384064453125" Y="0.153462432861" />
                  <Point X="28.380001953125" Y="0.142918670654" />
                  <Point X="28.373158203125" Y="0.121423347473" />
                  <Point X="28.370376953125" Y="0.11047177124" />
                  <Point X="28.354341796875" Y="0.02674505806" />
                  <Point X="28.351525390625" Y="0.005752072811" />
                  <Point X="28.34905859375" Y="-0.025936559677" />
                  <Point X="28.348919921875" Y="-0.038601013184" />
                  <Point X="28.350328125" Y="-0.063843425751" />
                  <Point X="28.351875" Y="-0.076421379089" />
                  <Point X="28.36791015625" Y="-0.160148086548" />
                  <Point X="28.373158203125" Y="-0.183983337402" />
                  <Point X="28.380001953125" Y="-0.205478668213" />
                  <Point X="28.384064453125" Y="-0.21602243042" />
                  <Point X="28.393837890625" Y="-0.237488769531" />
                  <Point X="28.399126953125" Y="-0.247481582642" />
                  <Point X="28.410853515625" Y="-0.26676675415" />
                  <Point X="28.417291015625" Y="-0.276058929443" />
                  <Point X="28.46539453125" Y="-0.33735458374" />
                  <Point X="28.4800859375" Y="-0.353977905273" />
                  <Point X="28.501798828125" Y="-0.375829193115" />
                  <Point X="28.511216796875" Y="-0.38413067627" />
                  <Point X="28.531064453125" Y="-0.399417877197" />
                  <Point X="28.541494140625" Y="-0.406403900146" />
                  <Point X="28.62166796875" Y="-0.452745269775" />
                  <Point X="28.63216796875" Y="-0.458382080078" />
                  <Point X="28.6565859375" Y="-0.470521453857" />
                  <Point X="28.66526953125" Y="-0.474310882568" />
                  <Point X="28.6919921875" Y="-0.483912963867" />
                  <Point X="28.8653671875" Y="-0.530368469238" />
                  <Point X="29.784880859375" Y="-0.776751159668" />
                  <Point X="29.76782421875" Y="-0.889879394531" />
                  <Point X="29.761619140625" Y="-0.931037536621" />
                  <Point X="29.7278046875" Y="-1.079219970703" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042480469" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.197130859375" Y="-0.946877502441" />
                  <Point X="28.17291796875" Y="-0.952140197754" />
                  <Point X="28.157875" Y="-0.956742614746" />
                  <Point X="28.128755859375" Y="-0.968366394043" />
                  <Point X="28.1146796875" Y="-0.975388122559" />
                  <Point X="28.0868515625" Y="-0.992280883789" />
                  <Point X="28.074125" Y="-1.001531005859" />
                  <Point X="28.050375" Y="-1.022002197266" />
                  <Point X="28.0393515625" Y="-1.033223266602" />
                  <Point X="27.9442421875" Y="-1.147609741211" />
                  <Point X="27.929607421875" Y="-1.165211303711" />
                  <Point X="27.921326171875" Y="-1.176848022461" />
                  <Point X="27.906603515625" Y="-1.201233886719" />
                  <Point X="27.900162109375" Y="-1.213982666016" />
                  <Point X="27.8888203125" Y="-1.24136706543" />
                  <Point X="27.88436328125" Y="-1.254934204102" />
                  <Point X="27.877533203125" Y="-1.282581176758" />
                  <Point X="27.87516015625" Y="-1.296661010742" />
                  <Point X="27.861529296875" Y="-1.444796386719" />
                  <Point X="27.859431640625" Y="-1.467591308594" />
                  <Point X="27.859291015625" Y="-1.483319946289" />
                  <Point X="27.861609375" Y="-1.514590209961" />
                  <Point X="27.864068359375" Y="-1.530131835938" />
                  <Point X="27.87180078125" Y="-1.561753295898" />
                  <Point X="27.8767890625" Y="-1.576672241211" />
                  <Point X="27.88916015625" Y="-1.605481445313" />
                  <Point X="27.89654296875" Y="-1.619371582031" />
                  <Point X="27.983623046875" Y="-1.754819458008" />
                  <Point X="27.9970234375" Y="-1.775662475586" />
                  <Point X="28.00174609375" Y="-1.782356079102" />
                  <Point X="28.019794921875" Y="-1.804448608398" />
                  <Point X="28.043490234375" Y="-1.828119140625" />
                  <Point X="28.052798828125" Y="-1.83627734375" />
                  <Point X="28.213689453125" Y="-1.95973425293" />
                  <Point X="29.087169921875" Y="-2.629980224609" />
                  <Point X="29.062986328125" Y="-2.669114501953" />
                  <Point X="29.0454609375" Y="-2.697471679688" />
                  <Point X="29.001275390625" Y="-2.760252685547" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.583833984375" Y="-2.032516235352" />
                  <Point X="27.555017578125" Y="-2.027311889648" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403442383" />
                  <Point X="27.492314453125" Y="-2.02650378418" />
                  <Point X="27.460140625" Y="-2.031461914062" />
                  <Point X="27.444845703125" Y="-2.03513659668" />
                  <Point X="27.4150703125" Y="-2.044960083008" />
                  <Point X="27.40058984375" Y="-2.051108886719" />
                  <Point X="27.24501171875" Y="-2.13298828125" />
                  <Point X="27.221072265625" Y="-2.145587890625" />
                  <Point X="27.20896875" Y="-2.153170654297" />
                  <Point X="27.186037109375" Y="-2.170064453125" />
                  <Point X="27.175208984375" Y="-2.179375488281" />
                  <Point X="27.15425" Y="-2.200334960938" />
                  <Point X="27.14494140625" Y="-2.211161376953" />
                  <Point X="27.128048828125" Y="-2.234090820312" />
                  <Point X="27.12046484375" Y="-2.246193847656" />
                  <Point X="27.038583984375" Y="-2.401771728516" />
                  <Point X="27.025984375" Y="-2.425711914062" />
                  <Point X="27.0198359375" Y="-2.440193847656" />
                  <Point X="27.01001171875" Y="-2.469972900391" />
                  <Point X="27.0063359375" Y="-2.485270019531" />
                  <Point X="27.00137890625" Y="-2.517444091797" />
                  <Point X="27.000279296875" Y="-2.533135009766" />
                  <Point X="27.00068359375" Y="-2.564485107422" />
                  <Point X="27.0021875" Y="-2.580144287109" />
                  <Point X="27.036009765625" Y="-2.767417236328" />
                  <Point X="27.04121484375" Y="-2.796234619141" />
                  <Point X="27.043017578125" Y="-2.804227783203" />
                  <Point X="27.05123828125" Y="-2.831541992188" />
                  <Point X="27.0640703125" Y="-2.862477294922" />
                  <Point X="27.069546875" Y="-2.873578125" />
                  <Point X="27.172935546875" Y="-3.052653808594" />
                  <Point X="27.73589453125" Y="-4.027724853516" />
                  <Point X="27.72195703125" Y="-4.033740234375" />
                  <Point X="26.833916015625" Y="-2.876422363281" />
                  <Point X="26.828654296875" Y="-2.870144042969" />
                  <Point X="26.808830078125" Y="-2.849624023438" />
                  <Point X="26.783251953125" Y="-2.828003662109" />
                  <Point X="26.77330078125" Y="-2.820646728516" />
                  <Point X="26.58859765625" Y="-2.701900634766" />
                  <Point X="26.56017578125" Y="-2.683627929688" />
                  <Point X="26.546283203125" Y="-2.676245605469" />
                  <Point X="26.51747265625" Y="-2.663874511719" />
                  <Point X="26.5025546875" Y="-2.658885742188" />
                  <Point X="26.47093359375" Y="-2.651154052734" />
                  <Point X="26.45539453125" Y="-2.648695800781" />
                  <Point X="26.424125" Y="-2.646376953125" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.206392578125" Y="-2.665104980469" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670339355469" />
                  <Point X="26.133576171875" Y="-2.677171386719" />
                  <Point X="26.1200078125" Y="-2.681629394531" />
                  <Point X="26.092623046875" Y="-2.69297265625" />
                  <Point X="26.079876953125" Y="-2.6994140625" />
                  <Point X="26.05549609375" Y="-2.714134033203" />
                  <Point X="26.043861328125" Y="-2.722412597656" />
                  <Point X="25.88787890625" Y="-2.852105957031" />
                  <Point X="25.863876953125" Y="-2.872062988281" />
                  <Point X="25.85265625" Y="-2.883084716797" />
                  <Point X="25.832185546875" Y="-2.906832519531" />
                  <Point X="25.822935546875" Y="-2.91955859375" />
                  <Point X="25.80604296875" Y="-2.947383544922" />
                  <Point X="25.79901953125" Y="-2.961465576172" />
                  <Point X="25.78739453125" Y="-2.990588867188" />
                  <Point X="25.78279296875" Y="-3.005630126953" />
                  <Point X="25.73615625" Y="-3.220199951172" />
                  <Point X="25.728978515625" Y="-3.253218017578" />
                  <Point X="25.7275859375" Y="-3.261286132812" />
                  <Point X="25.724724609375" Y="-3.289676269531" />
                  <Point X="25.7247421875" Y="-3.323170410156" />
                  <Point X="25.7255546875" Y="-3.335520996094" />
                  <Point X="25.75485546875" Y="-3.558074707031" />
                  <Point X="25.833087890625" Y="-4.152315429688" />
                  <Point X="25.655068359375" Y="-3.487936279297" />
                  <Point X="25.652609375" Y="-3.480124755859" />
                  <Point X="25.642146484375" Y="-3.453576171875" />
                  <Point X="25.6267890625" Y="-3.423814208984" />
                  <Point X="25.62041015625" Y="-3.413209716797" />
                  <Point X="25.478515625" Y="-3.208768554688" />
                  <Point X="25.456681640625" Y="-3.177309326172" />
                  <Point X="25.446671875" Y="-3.165173095703" />
                  <Point X="25.4247890625" Y="-3.14271875" />
                  <Point X="25.412916015625" Y="-3.132400634766" />
                  <Point X="25.38666015625" Y="-3.113156005859" />
                  <Point X="25.373244140625" Y="-3.104938232422" />
                  <Point X="25.3452421875" Y="-3.090829589844" />
                  <Point X="25.33065625" Y="-3.084938720703" />
                  <Point X="25.1110859375" Y="-3.016791748047" />
                  <Point X="25.077296875" Y="-3.006305419922" />
                  <Point X="25.063376953125" Y="-3.003109375" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766357422" />
                  <Point X="24.9913359375" Y="-2.997766357422" />
                  <Point X="24.977095703125" Y="-2.998839599609" />
                  <Point X="24.948935546875" Y="-3.003108886719" />
                  <Point X="24.935015625" Y="-3.006304931641" />
                  <Point X="24.7154453125" Y="-3.074451660156" />
                  <Point X="24.68165625" Y="-3.084938232422" />
                  <Point X="24.6670703125" Y="-3.090829589844" />
                  <Point X="24.639068359375" Y="-3.104938476562" />
                  <Point X="24.62565234375" Y="-3.113156005859" />
                  <Point X="24.599396484375" Y="-3.132400634766" />
                  <Point X="24.587521484375" Y="-3.142719482422" />
                  <Point X="24.565638671875" Y="-3.165174316406" />
                  <Point X="24.555630859375" Y="-3.177310302734" />
                  <Point X="24.41373828125" Y="-3.381751220703" />
                  <Point X="24.391904296875" Y="-3.413210693359" />
                  <Point X="24.38753125" Y="-3.420130859375" />
                  <Point X="24.374025390625" Y="-3.445260498047" />
                  <Point X="24.36122265625" Y="-3.476212890625" />
                  <Point X="24.35724609375" Y="-3.487935546875" />
                  <Point X="24.30382421875" Y="-3.6873046875" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.16688209506" Y="-3.94530595286" />
                  <Point X="22.545930104468" Y="-4.098451289511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.8715639026" Y="-4.63404210981" />
                  <Point X="24.032701765643" Y="-4.699146032455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.04179283219" Y="-3.792305809536" />
                  <Point X="22.608556808555" Y="-4.021293319846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.880114316624" Y="-4.535035900763" />
                  <Point X="24.057474256566" Y="-4.606693967912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.089759627844" Y="-3.709224852395" />
                  <Point X="22.696940112483" Y="-3.954541692004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.862280424241" Y="-4.425369739977" />
                  <Point X="24.082246747489" Y="-4.51424190337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.137726423498" Y="-3.626143895254" />
                  <Point X="22.792500910849" Y="-3.890689960152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.840117979871" Y="-4.313954730669" />
                  <Point X="24.107019238412" Y="-4.421789838828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.185693219152" Y="-3.543062938113" />
                  <Point X="22.888061709215" Y="-3.826838228299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.817955318899" Y="-4.202539633847" />
                  <Point X="24.131791729335" Y="-4.329337774286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.233660014806" Y="-3.459981980972" />
                  <Point X="23.02045441548" Y="-3.777867553185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.75175943621" Y="-4.073333960644" />
                  <Point X="24.156564220259" Y="-4.236885709744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.124451642337" Y="-2.909371908024" />
                  <Point X="21.283355907536" Y="-2.973573398562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.28162681046" Y="-3.376901023831" />
                  <Point X="23.310865504868" Y="-3.792740449016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.540109909691" Y="-3.885361200691" />
                  <Point X="24.181336711182" Y="-4.144433645202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.011828733899" Y="-2.761408498831" />
                  <Point X="21.387761090413" Y="-2.913295030003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.329593606114" Y="-3.29382006669" />
                  <Point X="24.206109202105" Y="-4.051981580659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.926808174532" Y="-2.624597162557" />
                  <Point X="21.492166273291" Y="-2.853016661444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.377560401768" Y="-3.21073910955" />
                  <Point X="24.230881693028" Y="-3.959529516117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.84630869394" Y="-2.489612460678" />
                  <Point X="21.596571456168" Y="-2.792738292885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.425527197422" Y="-3.127658152409" />
                  <Point X="24.255654183951" Y="-3.867077451575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.876243540673" Y="-2.39924612327" />
                  <Point X="21.700976639046" Y="-2.732459924327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.473493993076" Y="-3.044577195268" />
                  <Point X="24.280426674874" Y="-3.774625387033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.963715617832" Y="-2.332126335916" />
                  <Point X="21.805381821923" Y="-2.672181555768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.52146078873" Y="-2.961496238127" />
                  <Point X="24.30519918901" Y="-3.682173331869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.05118769499" Y="-2.265006548562" />
                  <Point X="21.9097870048" Y="-2.611903187209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.569427584384" Y="-2.878415280986" />
                  <Point X="24.329972098162" Y="-3.589721436303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.138659772149" Y="-2.197886761208" />
                  <Point X="22.014192187678" Y="-2.55162481865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.617394380038" Y="-2.795334323845" />
                  <Point X="24.354745007314" Y="-3.497269540736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.815733308815" Y="-4.087547130181" />
                  <Point X="25.825057007258" Y="-4.091314148873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.226131849308" Y="-2.130766973854" />
                  <Point X="22.118597370555" Y="-2.491346450092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.66432156574" Y="-2.711833337019" />
                  <Point X="24.393741074655" Y="-3.410564174092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.784946099194" Y="-3.972647489519" />
                  <Point X="25.810810136179" Y="-3.983097238767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.313603926466" Y="-2.0636471865" />
                  <Point X="22.223002836467" Y="-2.431068195886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.688366317342" Y="-2.619087246706" />
                  <Point X="24.449279800815" Y="-3.330542475455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.754158889572" Y="-3.857747848857" />
                  <Point X="25.7965632651" Y="-3.874880328661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.401076003625" Y="-1.996527399146" />
                  <Point X="22.329300098924" Y="-2.371554277099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.668124348017" Y="-2.508448159681" />
                  <Point X="24.504818897402" Y="-3.250520926481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.72337167995" Y="-3.742848208195" />
                  <Point X="25.782316394022" Y="-3.766663418554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.488548080784" Y="-1.929407611792" />
                  <Point X="24.561025139755" Y="-3.170768921893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.692584470328" Y="-3.627948567533" />
                  <Point X="25.768069522943" Y="-3.658446508448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.576020157943" Y="-1.862287824438" />
                  <Point X="24.644684573368" Y="-3.102108726557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.661797260706" Y="-3.513048926871" />
                  <Point X="25.75382260164" Y="-3.55022957805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.33149908785" Y="-1.257007872962" />
                  <Point X="20.388780915807" Y="-1.28015123372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.663492235101" Y="-1.795168037084" />
                  <Point X="24.780136199557" Y="-3.054373935315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.601753116989" Y="-3.386328717547" />
                  <Point X="25.739575037766" Y="-3.442012388036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.302315105402" Y="-1.142755978124" />
                  <Point X="20.580053789418" Y="-1.254969690395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.75096431226" Y="-1.72804824973" />
                  <Point X="24.923560327705" Y="-3.00986024395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.502926221108" Y="-3.243939259239" />
                  <Point X="25.725444257582" Y="-3.333842381696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.273131358853" Y="-1.028504178596" />
                  <Point X="20.771326663028" Y="-1.229788147071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.838436389419" Y="-1.660928462376" />
                  <Point X="25.733056876756" Y="-3.234457278935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.251092365823" Y="-0.917139046866" />
                  <Point X="20.962599536639" Y="-1.204606603746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.925908466578" Y="-1.593808675022" />
                  <Point X="25.753529460679" Y="-3.140267939197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.612775546812" Y="-3.891452118276" />
                  <Point X="27.670737809514" Y="-3.914870392516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.235538395263" Y="-0.80839403429" />
                  <Point X="21.15387241025" Y="-1.179425060421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.999295763983" Y="-1.520998267263" />
                  <Point X="25.774001543399" Y="-3.046078396958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.498828859899" Y="-3.742953867861" />
                  <Point X="27.593584760441" Y="-3.781237736733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.219984424703" Y="-0.699649021713" />
                  <Point X="21.34514528386" Y="-1.154243517096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.04032006042" Y="-1.435112358366" />
                  <Point X="25.802232463764" Y="-2.955023628611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.384882172986" Y="-3.594455617447" />
                  <Point X="27.516431711367" Y="-3.64760508095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.302937043839" Y="-0.630703254792" />
                  <Point X="21.536418157471" Y="-1.129061973771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.050280136995" Y="-1.336675689959" />
                  <Point X="25.859977468284" Y="-2.875893324294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.270935486073" Y="-3.445957367033" />
                  <Point X="27.439278662294" Y="-3.513972425166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.455413974301" Y="-0.589847132979" />
                  <Point X="21.727690984919" Y="-1.103880411796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.00851621851" Y="-1.217341171043" />
                  <Point X="25.942432687629" Y="-2.806746594812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.15698879916" Y="-3.297459116619" />
                  <Point X="27.36212561322" Y="-3.380339769383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.607890904763" Y="-0.548991011166" />
                  <Point X="26.025364155552" Y="-2.737792282245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.043042112247" Y="-3.148960866204" />
                  <Point X="27.284972564146" Y="-3.2467071136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.760367835225" Y="-0.508134889353" />
                  <Point X="26.131009115894" Y="-2.678014816296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.929095425334" Y="-3.00046261579" />
                  <Point X="27.207819515073" Y="-3.113074457817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.912844765687" Y="-0.46727876754" />
                  <Point X="26.325522264145" Y="-2.654142428905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.808348340629" Y="-2.849216826322" />
                  <Point X="27.130666993509" Y="-2.979442015161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.065321696149" Y="-0.426422645727" />
                  <Point X="27.057889764222" Y="-2.847577305331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.217798626611" Y="-0.385566523914" />
                  <Point X="27.029942913545" Y="-2.733825244174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.370275557073" Y="-0.344710402101" />
                  <Point X="27.009981511596" Y="-2.623299513727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.518623096625" Y="-0.302185898064" />
                  <Point X="27.001390662779" Y="-2.517367784949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.615818219356" Y="-0.238994476117" />
                  <Point X="27.026361279211" Y="-2.424995768309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.674005800391" Y="-0.160042984318" />
                  <Point X="27.070830489869" Y="-2.340501695102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.701984145494" Y="-0.068886168941" />
                  <Point X="27.115299815912" Y="-2.256007668514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.220518215265" Y="0.632125720107" />
                  <Point X="20.528423218955" Y="0.507724023551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.696774018215" Y="0.035679659674" />
                  <Point X="27.176493674344" Y="-2.178270791626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.234824545317" Y="0.728806388125" />
                  <Point X="21.281384717667" Y="0.305968631581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.625605635681" Y="0.166894353223" />
                  <Point X="27.276716170589" Y="-2.116302507974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.249130875369" Y="0.825487056143" />
                  <Point X="27.38685148595" Y="-2.058339263216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.714854665" Y="-2.594887375544" />
                  <Point X="29.028479341239" Y="-2.721599969814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.263436829922" Y="0.922167875872" />
                  <Point X="27.570637817545" Y="-2.030132960576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.12370568104" Y="-2.253586882094" />
                  <Point X="29.080804967897" Y="-2.640280094713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.284541706351" Y="1.016101752856" />
                  <Point X="28.840572348046" Y="-2.440759015438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.309566654723" Y="1.108451817969" />
                  <Point X="28.558545603875" Y="-2.224352013851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.334591603094" Y="1.200801883081" />
                  <Point X="28.276518859705" Y="-2.007945012265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.359616551465" Y="1.293151948193" />
                  <Point X="28.016517424741" Y="-1.800436813231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.640410153368" Y="1.282164769532" />
                  <Point X="27.922712181654" Y="-1.660076234348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.01658689734" Y="1.232640299972" />
                  <Point X="27.865096719105" Y="-1.534337275911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.318857073033" Y="1.212976022259" />
                  <Point X="27.862803397221" Y="-1.430949913171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.446856111716" Y="1.263721854303" />
                  <Point X="27.871893511277" Y="-1.332161757091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.518126820495" Y="1.337387419377" />
                  <Point X="27.89053301362" Y="-1.237231804319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.558167787348" Y="1.423670619216" />
                  <Point X="27.938727213689" Y="-1.154242724526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.578686380142" Y="1.517841370164" />
                  <Point X="28.002497653647" Y="-1.077546854148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.553462493292" Y="1.630493282523" />
                  <Point X="28.072154675327" Y="-1.003229317166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.448825736749" Y="1.775230076907" />
                  <Point X="28.190074485009" Y="-0.948411212269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.166799014178" Y="1.991637069766" />
                  <Point X="28.355025193921" Y="-0.912594824085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.884772499211" Y="2.208043978748" />
                  <Point X="28.679417576938" Y="-0.94119705373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.801495384555" Y="2.344150917636" />
                  <Point X="29.055595007146" Y="-0.990721800547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.849888334492" Y="2.42705969727" />
                  <Point X="29.431772437354" Y="-1.040246547364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.89828128443" Y="2.509968476904" />
                  <Point X="29.732365552496" Y="-1.059233248632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.946674234367" Y="2.592877256538" />
                  <Point X="29.753772884444" Y="-0.96542157161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.995067332261" Y="2.675785976394" />
                  <Point X="28.472360481373" Y="-0.345236554105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.058984321091" Y="-0.582247970051" />
                  <Point X="29.770842967428" Y="-0.869857532259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.055046505908" Y="2.754013617791" />
                  <Point X="28.379995309614" Y="-0.205457801806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.115696215804" Y="2.831970344958" />
                  <Point X="28.355033179965" Y="-0.092911646221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.1763459257" Y="2.909927072125" />
                  <Point X="28.352188972001" Y="0.010698288943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.236995635596" Y="2.987883799292" />
                  <Point X="21.631047206697" Y="2.828676630236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.880011659118" Y="2.728088462157" />
                  <Point X="28.36954838223" Y="0.1061454325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.068536808962" Y="2.754380157945" />
                  <Point X="28.404854840259" Y="0.194341498069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.151403832011" Y="2.823360507931" />
                  <Point X="28.463933737133" Y="0.272932874893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.216345255124" Y="2.899583270404" />
                  <Point X="28.542573967084" Y="0.343620960141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.248463618475" Y="2.989067409834" />
                  <Point X="28.64721250593" Y="0.403805046769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.247031359664" Y="3.09210688051" />
                  <Point X="28.791431853174" Y="0.447997448764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.224202413131" Y="3.203791174172" />
                  <Point X="28.943908908093" Y="0.488853520293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.150763258199" Y="3.335923319322" />
                  <Point X="29.096385963011" Y="0.529709591822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.073610823571" Y="3.469555726853" />
                  <Point X="29.24886301793" Y="0.570565663351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.996458388944" Y="3.603188134384" />
                  <Point X="28.082840309897" Y="1.14413021787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.54269202167" Y="0.958338066319" />
                  <Point X="29.401340072849" Y="0.61142173488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.964783805599" Y="3.718446297302" />
                  <Point X="28.02193451117" Y="1.271198558416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.733964704623" Y="0.983519686674" />
                  <Point X="29.553817127767" Y="0.652277806409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.052303690449" Y="3.785546769095" />
                  <Point X="28.001541556306" Y="1.381898647557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.925237518013" Y="1.00870125433" />
                  <Point X="29.706294182686" Y="0.693133877938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.139823575299" Y="3.852647240888" />
                  <Point X="28.012583231771" Y="1.479898321647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.116510331403" Y="1.033882821985" />
                  <Point X="29.775508908696" Y="0.767630113971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.227343460149" Y="3.919747712681" />
                  <Point X="28.041300220837" Y="1.570756705491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.307783144793" Y="1.059064389641" />
                  <Point X="29.758485692397" Y="0.876968740358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.314862846383" Y="3.986848385928" />
                  <Point X="28.092601225275" Y="1.652490554841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.499055958183" Y="1.084245957296" />
                  <Point X="29.734447354704" Y="0.989141659766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.413546274541" Y="4.049438493451" />
                  <Point X="27.082562754011" Y="2.163033386888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.516948679051" Y="1.987530081039" />
                  <Point X="28.156433913347" Y="1.729161275349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.690328771573" Y="1.109427524952" />
                  <Point X="29.706782191183" Y="1.102779911925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.520320162901" Y="4.108759842873" />
                  <Point X="27.023768876549" Y="2.289248455856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.650305165005" Y="2.036111363882" />
                  <Point X="28.242212768267" Y="1.796965168893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.627094051261" Y="4.168081192296" />
                  <Point X="27.012882122595" Y="2.396107790522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.754710267497" Y="2.096389764919" />
                  <Point X="28.329685032625" Y="1.864084880614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.733867939621" Y="4.227402541719" />
                  <Point X="22.886853833556" Y="4.165592228386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.212611882066" Y="4.033977433511" />
                  <Point X="27.02838673031" Y="2.492304322938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.859115393956" Y="2.156668156272" />
                  <Point X="28.417157296983" Y="1.931204592334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.345672752818" Y="4.082678152649" />
                  <Point X="27.057628640202" Y="2.582950625003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.963520600455" Y="2.216946515287" />
                  <Point X="28.504629561342" Y="1.998324304055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.441524229842" Y="4.146412442701" />
                  <Point X="27.104499244957" Y="2.666474472015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.067925806954" Y="2.277224874302" />
                  <Point X="28.5921018257" Y="2.065444015775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.491813245677" Y="4.228555161986" />
                  <Point X="27.152466054878" Y="2.749555423392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.172331013453" Y="2.337503233317" />
                  <Point X="28.679574090058" Y="2.132563727496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.520945515439" Y="4.319245761539" />
                  <Point X="27.200432864799" Y="2.832636374769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.276736219952" Y="2.397781592333" />
                  <Point X="28.767046354416" Y="2.199683439216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.5376393494" Y="4.414961815363" />
                  <Point X="27.248399674719" Y="2.915717326146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.381141426451" Y="2.458059951348" />
                  <Point X="28.854518618774" Y="2.266803150937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.526075762833" Y="4.522094608155" />
                  <Point X="27.29636648464" Y="2.998798277522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.48554663295" Y="2.518338310363" />
                  <Point X="28.941990883132" Y="2.333922862657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.614335305714" Y="4.588896238706" />
                  <Point X="27.344333294561" Y="3.081879228899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.589951839449" Y="2.578616669378" />
                  <Point X="29.02946314749" Y="2.401042574378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.764046017296" Y="4.630869985492" />
                  <Point X="24.79484788527" Y="4.214398997191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.081246831427" Y="4.098686311892" />
                  <Point X="27.392300104482" Y="3.164960180276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.694357045948" Y="2.638895028393" />
                  <Point X="29.116935411849" Y="2.468162286098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.913756728879" Y="4.672843732279" />
                  <Point X="24.755275354574" Y="4.332848137969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.180031764907" Y="4.161235408603" />
                  <Point X="27.440266914403" Y="3.248041131652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.798762252447" Y="2.699173387408" />
                  <Point X="29.073538129575" Y="2.588156726821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.063467683803" Y="4.714817380749" />
                  <Point X="24.724488373855" Y="4.447747686148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.23094499956" Y="4.243125927115" />
                  <Point X="27.488233724324" Y="3.331122083029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.903167458946" Y="2.759451746423" />
                  <Point X="28.977476082361" Y="2.729429113757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.258425388894" Y="4.738510155518" />
                  <Point X="24.693701393137" Y="4.562647234327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.257569017092" Y="4.33482992635" />
                  <Point X="27.536200534244" Y="3.414203034406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.455063951551" Y="4.761523819748" />
                  <Point X="24.662914412419" Y="4.677546782506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.282341600653" Y="4.427281953464" />
                  <Point X="27.584167344165" Y="3.497283985782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.307114012564" Y="4.519734049929" />
                  <Point X="27.632134154086" Y="3.580364937159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.331886424476" Y="4.612186146394" />
                  <Point X="27.680100964007" Y="3.663445888536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.356658836387" Y="4.704638242859" />
                  <Point X="27.728067773928" Y="3.746526839913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.422938533571" Y="4.780320307511" />
                  <Point X="27.776034583849" Y="3.829607791289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.76527470556" Y="4.744468316529" />
                  <Point X="27.774519549005" Y="3.932680705654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.34279912665" Y="4.613594104903" />
                  <Point X="27.305196092685" Y="4.224760490961" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.818701171875" Y="-4.832732910156" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544677734" />
                  <Point X="25.32242578125" Y="-3.317103515625" />
                  <Point X="25.300591796875" Y="-3.285644287109" />
                  <Point X="25.2743359375" Y="-3.266399658203" />
                  <Point X="25.054765625" Y="-3.198252685547" />
                  <Point X="25.0209765625" Y="-3.187766357422" />
                  <Point X="24.9913359375" Y="-3.187766357422" />
                  <Point X="24.771765625" Y="-3.255913085938" />
                  <Point X="24.7379765625" Y="-3.266399658203" />
                  <Point X="24.711720703125" Y="-3.285644287109" />
                  <Point X="24.569828125" Y="-3.490085205078" />
                  <Point X="24.547994140625" Y="-3.521544677734" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.4873515625" Y="-3.736481445312" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.93825390625" Y="-4.945538085938" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.648412109375" Y="-4.873396484375" />
                  <Point X="23.649060546875" Y="-4.868464355469" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.637861328125" Y="-4.271045410156" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.411564453125" Y="-4.025344238281" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.08245703125" Y="-3.968177490234" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791748047" />
                  <Point X="22.786556640625" Y="-4.123172851562" />
                  <Point X="22.75215625" Y="-4.146159667969" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.710275390625" Y="-4.196379882812" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.200595703125" Y="-4.202550292969" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.791337890625" Y="-3.895944580078" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="21.84880859375" Y="-3.746566894531" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597593261719" />
                  <Point X="22.486021484375" Y="-2.568764404297" />
                  <Point X="22.470994140625" Y="-2.553736816406" />
                  <Point X="22.468681640625" Y="-2.551423095703" />
                  <Point X="22.439849609375" Y="-2.537199462891" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="22.236365234375" Y="-2.642746337891" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.88287890625" Y="-2.905700195312" />
                  <Point X="20.83830078125" Y="-2.847134277344" />
                  <Point X="20.585345703125" Y="-2.422969238281" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="20.707396484375" Y="-2.289317382812" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396012207031" />
                  <Point X="21.86085546875" Y="-1.370231933594" />
                  <Point X="21.8618828125" Y="-1.366264892578" />
                  <Point X="21.859673828125" Y="-1.334596191406" />
                  <Point X="21.83884375" Y="-1.310640625" />
                  <Point X="21.815892578125" Y="-1.29713269043" />
                  <Point X="21.812361328125" Y="-1.295054077148" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.563400390625" Y="-1.317149291992" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.090044921875" Y="-1.079455810547" />
                  <Point X="20.072609375" Y="-1.01118951416" />
                  <Point X="20.00568359375" Y="-0.543261230469" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="20.15664453125" Y="-0.473199523926" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.458103515625" Y="-0.121425354004" />
                  <Point X="21.48215625" Y="-0.104732116699" />
                  <Point X="21.485857421875" Y="-0.102163330078" />
                  <Point X="21.5051015625" Y="-0.075908210754" />
                  <Point X="21.513119140625" Y="-0.050076286316" />
                  <Point X="21.514357421875" Y="-0.046086883545" />
                  <Point X="21.5143515625" Y="-0.016458742142" />
                  <Point X="21.506337890625" Y="0.009358324051" />
                  <Point X="21.505107421875" Y="0.013333683014" />
                  <Point X="21.485857421875" Y="0.039603248596" />
                  <Point X="21.4618046875" Y="0.056296489716" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="21.244666015625" Y="0.119104988098" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.071119140625" Y="0.920482055664" />
                  <Point X="20.08235546875" Y="0.996415466309" />
                  <Point X="20.217072265625" Y="1.493563598633" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.324974609375" Y="1.515332275391" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866699219" />
                  <Point X="21.32153125" Y="1.412651489258" />
                  <Point X="21.32972265625" Y="1.41523425293" />
                  <Point X="21.348466796875" Y="1.426055908203" />
                  <Point X="21.360880859375" Y="1.44378503418" />
                  <Point X="21.382240234375" Y="1.495353881836" />
                  <Point X="21.38552734375" Y="1.50328918457" />
                  <Point X="21.389287109375" Y="1.524606567383" />
                  <Point X="21.38368359375" Y="1.545515014648" />
                  <Point X="21.35791015625" Y="1.595026000977" />
                  <Point X="21.353943359375" Y="1.60264465332" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.22653125" Y="1.706313476562" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.79632421875" Y="2.712202880859" />
                  <Point X="20.83998828125" Y="2.787007080078" />
                  <Point X="21.196845703125" Y="3.245699462891" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.274724609375" Y="3.253792724609" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.935626953125" Y="2.913948242188" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927405273438" />
                  <Point X="22.039375" Y="2.980030761719" />
                  <Point X="22.04747265625" Y="2.988128662109" />
                  <Point X="22.0591015625" Y="3.006382568359" />
                  <Point X="22.061927734375" Y="3.027840820312" />
                  <Point X="22.05544140625" Y="3.101981689453" />
                  <Point X="22.054443359375" Y="3.113390380859" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.99763671875" Y="3.221146972656" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="22.17108203125" Y="4.116029785156" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.80916796875" Y="4.486590820312" />
                  <Point X="22.858451171875" Y="4.513971679688" />
                  <Point X="22.8588671875" Y="4.513430664062" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.048755859375" Y="4.27366015625" />
                  <Point X="23.1312734375" Y="4.230703613281" />
                  <Point X="23.14397265625" Y="4.224093261719" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.18619140625" Y="4.222249511719" />
                  <Point X="23.272138671875" Y="4.257851074219" />
                  <Point X="23.285365234375" Y="4.263329101563" />
                  <Point X="23.303095703125" Y="4.275743652344" />
                  <Point X="23.31391796875" Y="4.294488769531" />
                  <Point X="23.341892578125" Y="4.383212402344" />
                  <Point X="23.346197265625" Y="4.396865234375" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.342365234375" Y="4.461859375" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.93346484375" Y="4.875696289062" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.713275390625" Y="4.983040527344" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.79038671875" Y="4.935923828125" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.080224609375" Y="4.407081054688" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.774248046875" Y="4.934567871094" />
                  <Point X="25.86020703125" Y="4.925564941406" />
                  <Point X="26.423919921875" Y="4.789467773438" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.87615625" Y="4.63569140625" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.285830078125" Y="4.449859863281" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.681443359375" Y="4.225451171875" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="28.05578125" Y="3.965810302734" />
                  <Point X="28.0687421875" Y="3.956593261719" />
                  <Point X="27.976578125" Y="3.796959472656" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491515136719" />
                  <Point X="27.20624609375" Y="2.421935791016" />
                  <Point X="27.202044921875" Y="2.392325195312" />
                  <Point X="27.20930078125" Y="2.332158691406" />
                  <Point X="27.210416015625" Y="2.322906982422" />
                  <Point X="27.218681640625" Y="2.300812988281" />
                  <Point X="27.255912109375" Y="2.245947021484" />
                  <Point X="27.274943359375" Y="2.224202392578" />
                  <Point X="27.32980859375" Y="2.186973388672" />
                  <Point X="27.338251953125" Y="2.181244873047" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.42050390625" Y="2.165725097656" />
                  <Point X="27.448666015625" Y="2.165946289062" />
                  <Point X="27.51824609375" Y="2.184552734375" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="27.740056640625" Y="2.307322753906" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.17229296875" Y="2.783989501953" />
                  <Point X="29.20259765625" Y="2.741873291016" />
                  <Point X="29.382802734375" Y="2.444081787109" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="29.271748046875" Y="2.347465087891" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583831542969" />
                  <Point X="28.229294921875" Y="1.518502929688" />
                  <Point X="28.21312109375" Y="1.491503295898" />
                  <Point X="28.194466796875" Y="1.424802490234" />
                  <Point X="28.191595703125" Y="1.414538696289" />
                  <Point X="28.190779296875" Y="1.39096862793" />
                  <Point X="28.20609375" Y="1.316755737305" />
                  <Point X="28.215646484375" Y="1.28795703125" />
                  <Point X="28.257294921875" Y="1.224652832031" />
                  <Point X="28.263703125" Y="1.214911621094" />
                  <Point X="28.280947265625" Y="1.19882019043" />
                  <Point X="28.341302734375" Y="1.164845581055" />
                  <Point X="28.36856640625" Y="1.153619506836" />
                  <Point X="28.450169921875" Y="1.142834472656" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.664861328125" Y="1.166061645508" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.92617578125" Y="1.004841308594" />
                  <Point X="29.93919140625" Y="0.951370727539" />
                  <Point X="29.995978515625" Y="0.58663659668" />
                  <Point X="29.997859375" Y="0.574556213379" />
                  <Point X="29.867857421875" Y="0.539722106934" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.72908984375" Y="0.232820114136" />
                  <Point X="28.64891796875" Y="0.186478652954" />
                  <Point X="28.622265625" Y="0.166927001953" />
                  <Point X="28.574162109375" Y="0.105631439209" />
                  <Point X="28.566759765625" Y="0.096199325562" />
                  <Point X="28.556986328125" Y="0.074732978821" />
                  <Point X="28.540951171875" Y="-0.008993789673" />
                  <Point X="28.538484375" Y="-0.040682498932" />
                  <Point X="28.55451953125" Y="-0.124409263611" />
                  <Point X="28.556986328125" Y="-0.137293060303" />
                  <Point X="28.566759765625" Y="-0.158759399414" />
                  <Point X="28.61486328125" Y="-0.220054946899" />
                  <Point X="28.636576171875" Y="-0.241906143188" />
                  <Point X="28.71675" Y="-0.288247467041" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="28.91454296875" Y="-0.346842346191" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.95569921875" Y="-0.918204528809" />
                  <Point X="29.948431640625" Y="-0.966412536621" />
                  <Point X="29.875677734375" Y="-1.285228393555" />
                  <Point X="29.874548828125" Y="-1.290178710938" />
                  <Point X="29.718623046875" Y="-1.269650878906" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.237486328125" Y="-1.132542358398" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.1854453125" Y="-1.154697875977" />
                  <Point X="28.0903359375" Y="-1.269084350586" />
                  <Point X="28.075701171875" Y="-1.286686035156" />
                  <Point X="28.064359375" Y="-1.31407043457" />
                  <Point X="28.050728515625" Y="-1.462205810547" />
                  <Point X="28.048630859375" Y="-1.485000732422" />
                  <Point X="28.05636328125" Y="-1.516622436523" />
                  <Point X="28.143443359375" Y="-1.65207043457" />
                  <Point X="28.15684375" Y="-1.672913085938" />
                  <Point X="28.168462890625" Y="-1.685540771484" />
                  <Point X="28.329353515625" Y="-1.808997680664" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.224615234375" Y="-2.768997802734" />
                  <Point X="29.20412890625" Y="-2.802145751953" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.9171953125" Y="-2.931102050781" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.550068359375" Y="-2.219491455078" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219245117188" />
                  <Point X="27.3335" Y="-2.301124511719" />
                  <Point X="27.309560546875" Y="-2.313724121094" />
                  <Point X="27.2886015625" Y="-2.33468359375" />
                  <Point X="27.206720703125" Y="-2.490261474609" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.1891640625" Y="-2.546375732422" />
                  <Point X="27.222986328125" Y="-2.733648681641" />
                  <Point X="27.22819140625" Y="-2.762466064453" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.337482421875" Y="-2.957654296875" />
                  <Point X="27.98667578125" Y="-4.082088378906" />
                  <Point X="27.859607421875" Y="-4.172850097656" />
                  <Point X="27.83530078125" Y="-4.1902109375" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="27.571220703125" Y="-4.149405273438" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467529297" />
                  <Point X="26.48584765625" Y="-2.861721435547" />
                  <Point X="26.45742578125" Y="-2.843448730469" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.223802734375" Y="-2.854305664062" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509277344" />
                  <Point X="26.0093515625" Y="-2.998202636719" />
                  <Point X="25.985349609375" Y="-3.018159667969" />
                  <Point X="25.96845703125" Y="-3.045984619141" />
                  <Point X="25.9218203125" Y="-3.260554443359" />
                  <Point X="25.914642578125" Y="-3.293572509766" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.94323046875" Y="-3.533273925781" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.017337890625" Y="-4.958207519531" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#204" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.167473804092" Y="4.980145251278" Z="2.2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.2" />
                  <Point X="-0.298693537241" Y="5.063981769904" Z="2.2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.2" />
                  <Point X="-1.08619104069" Y="4.95512000766" Z="2.2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.2" />
                  <Point X="-1.713363905394" Y="4.486613220306" Z="2.2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.2" />
                  <Point X="-1.711950597941" Y="4.4295278149" Z="2.2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.2" />
                  <Point X="-1.753150804782" Y="4.335325806788" Z="2.2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.2" />
                  <Point X="-1.851796909826" Y="4.306334713563" Z="2.2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.2" />
                  <Point X="-2.10762148629" Y="4.575148572634" Z="2.2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.2" />
                  <Point X="-2.221271493599" Y="4.56157817574" Z="2.2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.2" />
                  <Point X="-2.865497500482" Y="4.186989596487" Z="2.2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.2" />
                  <Point X="-3.051820029124" Y="3.227426993309" Z="2.2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.2" />
                  <Point X="-3.000526543691" Y="3.128904197465" Z="2.2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.2" />
                  <Point X="-3.002137998096" Y="3.04666556979" Z="2.2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.2" />
                  <Point X="-3.066172268141" Y="2.995038155482" Z="2.2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.2" />
                  <Point X="-3.706432354988" Y="3.328373919622" Z="2.2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.2" />
                  <Point X="-3.848773961386" Y="3.307682059455" Z="2.2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.2" />
                  <Point X="-4.253014422393" Y="2.768688229611" Z="2.2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.2" />
                  <Point X="-3.810062415468" Y="1.697925821193" Z="2.2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.2" />
                  <Point X="-3.692596234804" Y="1.60321539919" Z="2.2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.2" />
                  <Point X="-3.670109377626" Y="1.54576899532" Z="2.2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.2" />
                  <Point X="-3.699661593614" Y="1.491617284349" Z="2.2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.2" />
                  <Point X="-4.674656079028" Y="1.596184578759" Z="2.2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.2" />
                  <Point X="-4.837344441734" Y="1.537920624915" Z="2.2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.2" />
                  <Point X="-4.984499123684" Y="0.959080936969" Z="2.2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.2" />
                  <Point X="-3.774433222182" Y="0.102088805845" Z="2.2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.2" />
                  <Point X="-3.572859660558" Y="0.046500274567" Z="2.2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.2" />
                  <Point X="-3.547574146509" Y="0.025831948089" Z="2.2" />
                  <Point X="-3.539556741714" Y="0" Z="2.2" />
                  <Point X="-3.540790454626" Y="-0.003975002972" Z="2.2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.2" />
                  <Point X="-3.552508934565" Y="-0.032375708567" Z="2.2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.2" />
                  <Point X="-4.862454566591" Y="-0.393623250911" Z="2.2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.2" />
                  <Point X="-5.049969740346" Y="-0.519060262802" Z="2.2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.2" />
                  <Point X="-4.964557220809" Y="-1.06054926842" Z="2.2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.2" />
                  <Point X="-3.436232044995" Y="-1.335441525766" Z="2.2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.2" />
                  <Point X="-3.215627216935" Y="-1.308941895776" Z="2.2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.2" />
                  <Point X="-3.193703763962" Y="-1.326416860285" Z="2.2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.2" />
                  <Point X="-4.329198833412" Y="-2.218369673595" Z="2.2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.2" />
                  <Point X="-4.463753855142" Y="-2.417298837949" Z="2.2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.2" />
                  <Point X="-4.163064094397" Y="-2.904703649384" Z="2.2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.2" />
                  <Point X="-2.744791550186" Y="-2.65476753255" Z="2.2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.2" />
                  <Point X="-2.570525929116" Y="-2.557804540015" Z="2.2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.2" />
                  <Point X="-3.200649480618" Y="-3.690286832306" Z="2.2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.2" />
                  <Point X="-3.245322434352" Y="-3.904281843611" Z="2.2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.2" />
                  <Point X="-2.831883527809" Y="-4.213780856694" Z="2.2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.2" />
                  <Point X="-2.256213945031" Y="-4.195538068452" Z="2.2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.2" />
                  <Point X="-2.191820343815" Y="-4.133465508485" Z="2.2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.2" />
                  <Point X="-1.926969728308" Y="-3.986790415692" Z="2.2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.2" />
                  <Point X="-1.627559586624" Y="-4.031656308815" Z="2.2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.2" />
                  <Point X="-1.417334144878" Y="-4.249520319371" Z="2.2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.2" />
                  <Point X="-1.406668446767" Y="-4.83065798628" Z="2.2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.2" />
                  <Point X="-1.373665386378" Y="-4.889649189117" Z="2.2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.2" />
                  <Point X="-1.077429867462" Y="-4.963342272766" Z="2.2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.2" />
                  <Point X="-0.470507603143" Y="-3.718142319052" Z="2.2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.2" />
                  <Point X="-0.395252339657" Y="-3.487313813014" Z="2.2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.2" />
                  <Point X="-0.219571554723" Y="-3.272386276078" Z="2.2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.2" />
                  <Point X="0.033787524638" Y="-3.21472576921" Z="2.2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.2" />
                  <Point X="0.275193519775" Y="-3.314331942754" Z="2.2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.2" />
                  <Point X="0.764247257259" Y="-4.814393710763" Z="2.2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.2" />
                  <Point X="0.84171818513" Y="-5.009393924595" Z="2.2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.2" />
                  <Point X="1.02188699602" Y="-4.975767588065" Z="2.2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.2" />
                  <Point X="0.986645551248" Y="-3.495467497598" Z="2.2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.2" />
                  <Point X="0.964522355815" Y="-3.239895833381" Z="2.2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.2" />
                  <Point X="1.035162332847" Y="-3.005368874236" Z="2.2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.2" />
                  <Point X="1.222227787347" Y="-2.872815058549" Z="2.2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.2" />
                  <Point X="1.452652226447" Y="-2.872499161306" Z="2.2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.2" />
                  <Point X="2.525395430985" Y="-4.148563292738" Z="2.2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.2" />
                  <Point X="2.688081790601" Y="-4.309798588238" Z="2.2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.2" />
                  <Point X="2.882510164756" Y="-4.182258098382" Z="2.2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.2" />
                  <Point X="2.374626311751" Y="-2.901374267341" Z="2.2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.2" />
                  <Point X="2.266032486134" Y="-2.693481178094" Z="2.2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.2" />
                  <Point X="2.244810678727" Y="-2.482267976294" Z="2.2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.2" />
                  <Point X="2.350630402078" Y="-2.31409063124" Z="2.2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.2" />
                  <Point X="2.535025625419" Y="-2.237415523357" Z="2.2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.2" />
                  <Point X="3.886039329766" Y="-2.943123188524" Z="2.2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.2" />
                  <Point X="4.088400296685" Y="-3.013427368972" Z="2.2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.2" />
                  <Point X="4.260991065682" Y="-2.764003568699" Z="2.2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.2" />
                  <Point X="3.35363498749" Y="-1.738049944329" Z="2.2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.2" />
                  <Point X="3.179342944704" Y="-1.593750396559" Z="2.2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.2" />
                  <Point X="3.094360091872" Y="-1.435507535862" Z="2.2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.2" />
                  <Point X="3.122626734887" Y="-1.269770535686" Z="2.2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.2" />
                  <Point X="3.241948631253" Y="-1.150121364976" Z="2.2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.2" />
                  <Point X="4.705941582365" Y="-1.287943057677" Z="2.2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.2" />
                  <Point X="4.918266397823" Y="-1.265072431321" Z="2.2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.2" />
                  <Point X="4.998983362637" Y="-0.894378564692" Z="2.2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.2" />
                  <Point X="3.921326754736" Y="-0.26726607279" Z="2.2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.2" />
                  <Point X="3.735615729523" Y="-0.213679647003" Z="2.2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.2" />
                  <Point X="3.648040532608" Y="-0.157906087913" Z="2.2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.2" />
                  <Point X="3.597469329681" Y="-0.083726719387" Z="2.2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.2" />
                  <Point X="3.583902120743" Y="0.012883811795" Z="2.2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.2" />
                  <Point X="3.607338905792" Y="0.106042650662" Z="2.2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.2" />
                  <Point X="3.66777968483" Y="0.174469197239" Z="2.2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.2" />
                  <Point X="4.874641062571" Y="0.522705859978" Z="2.2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.2" />
                  <Point X="5.039226510854" Y="0.62560908265" Z="2.2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.2" />
                  <Point X="4.968600036732" Y="1.047947297137" Z="2.2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.2" />
                  <Point X="3.652178617796" Y="1.246913943841" Z="2.2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.2" />
                  <Point X="3.450564294873" Y="1.223683659425" Z="2.2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.2" />
                  <Point X="3.359673274469" Y="1.239696549832" Z="2.2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.2" />
                  <Point X="3.292909680203" Y="1.283412234451" Z="2.2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.2" />
                  <Point X="3.248904693866" Y="1.358136244856" Z="2.2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.2" />
                  <Point X="3.236462430124" Y="1.442613056087" Z="2.2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.2" />
                  <Point X="3.262821715636" Y="1.519366434285" Z="2.2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.2" />
                  <Point X="4.296028444498" Y="2.339077475562" Z="2.2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.2" />
                  <Point X="4.419422946051" Y="2.501248087094" Z="2.2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.2" />
                  <Point X="4.206721720577" Y="2.844472791155" Z="2.2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.2" />
                  <Point X="2.708899544614" Y="2.381903949757" Z="2.2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.2" />
                  <Point X="2.499171084051" Y="2.264135620395" Z="2.2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.2" />
                  <Point X="2.420333276551" Y="2.24664556385" Z="2.2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.2" />
                  <Point X="2.351724048252" Y="2.259629271092" Z="2.2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.2" />
                  <Point X="2.291129329143" Y="2.305300812131" Z="2.2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.2" />
                  <Point X="2.252784138909" Y="2.369425167282" Z="2.2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.2" />
                  <Point X="2.248392322263" Y="2.440298491831" Z="2.2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.2" />
                  <Point X="3.013721403103" Y="3.803239537341" Z="2.2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.2" />
                  <Point X="3.078600070145" Y="4.037837255393" Z="2.2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.2" />
                  <Point X="2.700456577686" Y="4.29993396314" Z="2.2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.2" />
                  <Point X="2.30085492832" Y="4.526431940512" Z="2.2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.2" />
                  <Point X="1.887048102043" Y="4.713974650699" Z="2.2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.2" />
                  <Point X="1.429497421607" Y="4.869351476594" Z="2.2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.2" />
                  <Point X="0.773300102485" Y="5.015575725289" Z="2.2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.2" />
                  <Point X="0.025770757461" Y="4.451302254673" Z="2.2" />
                  <Point X="0" Y="4.355124473572" Z="2.2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>