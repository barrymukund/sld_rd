<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#131" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="739" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004715820312" />
                  <Width Value="9.78389453125" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.618703125" Y="-3.719274658203" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467377441406" />
                  <Point X="25.51972265625" Y="-3.434753417969" />
                  <Point X="25.37863671875" Y="-3.231477050781" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669677734" />
                  <Point X="25.267458984375" Y="-3.164794921875" />
                  <Point X="25.04913671875" Y="-3.097036376953" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097036132812" />
                  <Point X="24.928138671875" Y="-3.107910644531" />
                  <Point X="24.70981640625" Y="-3.175669433594" />
                  <Point X="24.68181640625" Y="-3.18977734375" />
                  <Point X="24.655560546875" Y="-3.209021484375" />
                  <Point X="24.63367578125" Y="-3.231477050781" />
                  <Point X="24.611033203125" Y="-3.264100830078" />
                  <Point X="24.46994921875" Y="-3.467377441406" />
                  <Point X="24.4618125" Y="-3.481571777344" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.103826171875" Y="-4.800764160156" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.9206640625" Y="-4.845351074219" />
                  <Point X="23.883990234375" Y="-4.835915527344" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.778369140625" Y="-4.614093261719" />
                  <Point X="23.7850390625" Y="-4.563439453125" />
                  <Point X="23.78580078125" Y="-4.547930175781" />
                  <Point X="23.7834921875" Y="-4.51622265625" />
                  <Point X="23.77512109375" Y="-4.474140625" />
                  <Point X="23.722962890625" Y="-4.211931152344" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131205566406" />
                  <Point X="23.64409765625" Y="-4.102915039062" />
                  <Point X="23.443095703125" Y="-3.926640380859" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.314158203125" Y="-3.88816015625" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801513672" />
                  <Point X="22.921666015625" Y="-3.918639160156" />
                  <Point X="22.699376953125" Y="-4.067169433594" />
                  <Point X="22.687212890625" Y="-4.076824462891" />
                  <Point X="22.6648984375" Y="-4.099462402344" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.19828515625" Y="-4.089383056641" />
                  <Point X="22.147517578125" Y="-4.050293212891" />
                  <Point X="21.89527734375" Y="-3.856077392578" />
                  <Point X="22.473287109375" Y="-2.854937255859" />
                  <Point X="22.57623828125" Y="-2.676619384766" />
                  <Point X="22.587138671875" Y="-2.647663085938" />
                  <Point X="22.593412109375" Y="-2.616142089844" />
                  <Point X="22.5942265625" Y="-2.583754638672" />
                  <Point X="22.584109375" Y="-2.552977539063" />
                  <Point X="22.5674921875" Y="-2.521744140625" />
                  <Point X="22.550798828125" Y="-2.499189941406" />
                  <Point X="22.535849609375" Y="-2.484240722656" />
                  <Point X="22.510689453125" Y="-2.466212402344" />
                  <Point X="22.481859375" Y="-2.451995361328" />
                  <Point X="22.4522421875" Y="-2.443011230469" />
                  <Point X="22.421310546875" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450295166016" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.249736328125" Y="-3.102680419922" />
                  <Point X="21.1819765625" Y="-3.141801513672" />
                  <Point X="20.91715234375" Y="-2.793877929688" />
                  <Point X="20.880744140625" Y="-2.732825927734" />
                  <Point X="20.693859375" Y="-2.419448974609" />
                  <Point X="21.7139140625" Y="-1.636733642578" />
                  <Point X="21.894044921875" Y="-1.498513305664" />
                  <Point X="21.916931640625" Y="-1.475884399414" />
                  <Point X="21.935494140625" Y="-1.446796264648" />
                  <Point X="21.94327734375" Y="-1.427609619141" />
                  <Point X="21.947208984375" Y="-1.415716674805" />
                  <Point X="21.95384765625" Y="-1.390083374023" />
                  <Point X="21.957962890625" Y="-1.358598266602" />
                  <Point X="21.95726171875" Y="-1.335733154297" />
                  <Point X="21.9511171875" Y="-1.313698364258" />
                  <Point X="21.93911328125" Y="-1.284715942383" />
                  <Point X="21.926916015625" Y="-1.263502441406" />
                  <Point X="21.909705078125" Y="-1.246108276367" />
                  <Point X="21.893541015625" Y="-1.233523681641" />
                  <Point X="21.883365234375" Y="-1.226611206055" />
                  <Point X="21.860544921875" Y="-1.213180297852" />
                  <Point X="21.831283203125" Y="-1.201956665039" />
                  <Point X="21.799396484375" Y="-1.195474975586" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="20.3654375" Y="-1.379044189453" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.165921875" Y="-0.992644958496" />
                  <Point X="20.15629296875" Y="-0.925322570801" />
                  <Point X="20.107578125" Y="-0.584698425293" />
                  <Point X="21.261927734375" Y="-0.275390899658" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.485654296875" Y="-0.213296936035" />
                  <Point X="21.50505859375" Y="-0.203413406372" />
                  <Point X="21.516107421875" Y="-0.196807495117" />
                  <Point X="21.5400234375" Y="-0.180209365845" />
                  <Point X="21.563978515625" Y="-0.15868321228" />
                  <Point X="21.58393359375" Y="-0.130455505371" />
                  <Point X="21.592638671875" Y="-0.111594490051" />
                  <Point X="21.597115234375" Y="-0.099940483093" />
                  <Point X="21.6050859375" Y="-0.074255729675" />
                  <Point X="21.60928515625" Y="-0.042501937866" />
                  <Point X="21.608005859375" Y="-0.008741326332" />
                  <Point X="21.603806640625" Y="0.015817751884" />
                  <Point X="21.5958359375" Y="0.041502658844" />
                  <Point X="21.582517578125" Y="0.070829620361" />
                  <Point X="21.561453125" Y="0.098496459961" />
                  <Point X="21.545794921875" Y="0.112678840637" />
                  <Point X="21.536185546875" Y="0.120313140869" />
                  <Point X="21.51226953125" Y="0.136911270142" />
                  <Point X="21.49807421875" Y="0.14504737854" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="20.188556640625" Y="0.500439666748" />
                  <Point X="20.10818359375" Y="0.521975769043" />
                  <Point X="20.17551171875" Y="0.976964477539" />
                  <Point X="20.194900390625" Y="1.048515869141" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="21.087244140625" Y="1.319157836914" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228027344" />
                  <Point X="21.2968671875" Y="1.305264282227" />
                  <Point X="21.305361328125" Y="1.307942626953" />
                  <Point X="21.35829296875" Y="1.324631835938" />
                  <Point X="21.37722265625" Y="1.332962158203" />
                  <Point X="21.395966796875" Y="1.343784301758" />
                  <Point X="21.4126484375" Y="1.356015991211" />
                  <Point X="21.426287109375" Y="1.371568481445" />
                  <Point X="21.438701171875" Y="1.389298583984" />
                  <Point X="21.448650390625" Y="1.407435180664" />
                  <Point X="21.45205859375" Y="1.415664428711" />
                  <Point X="21.473296875" Y="1.466939331055" />
                  <Point X="21.479083984375" Y="1.486788085938" />
                  <Point X="21.48284375" Y="1.508103027344" />
                  <Point X="21.484197265625" Y="1.52875" />
                  <Point X="21.481048828125" Y="1.549200195312" />
                  <Point X="21.4754453125" Y="1.570106445312" />
                  <Point X="21.467947265625" Y="1.589381225586" />
                  <Point X="21.463833984375" Y="1.597281982422" />
                  <Point X="21.43820703125" Y="1.646510986328" />
                  <Point X="21.42671875" Y="1.663704956055" />
                  <Point X="21.412806640625" Y="1.680285400391" />
                  <Point X="21.39786328125" Y="1.694590454102" />
                  <Point X="20.664474609375" Y="2.257340087891" />
                  <Point X="20.648140625" Y="2.269874023438" />
                  <Point X="20.918853515625" Y="2.733666748047" />
                  <Point X="20.970197265625" Y="2.7996640625" />
                  <Point X="21.24949609375" Y="3.158662353516" />
                  <Point X="21.703005859375" Y="2.896828369141" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.812275390625" Y="2.836340820312" />
                  <Point X="21.83291796875" Y="2.82983203125" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.8650390625" Y="2.824761230469" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.019537109375" Y="2.835653564453" />
                  <Point X="22.037791015625" Y="2.847282714844" />
                  <Point X="22.053919921875" Y="2.8602265625" />
                  <Point X="22.062318359375" Y="2.868624267578" />
                  <Point X="22.11464453125" Y="2.920949951172" />
                  <Point X="22.127595703125" Y="2.937085693359" />
                  <Point X="22.139224609375" Y="2.955340087891" />
                  <Point X="22.14837109375" Y="2.973888916016" />
                  <Point X="22.1532890625" Y="2.993977783203" />
                  <Point X="22.156115234375" Y="3.015436523438" />
                  <Point X="22.15656640625" Y="3.036121337891" />
                  <Point X="22.15553125" Y="3.047952636719" />
                  <Point X="22.14908203125" Y="3.121670898438" />
                  <Point X="22.145044921875" Y="3.141963378906" />
                  <Point X="22.13853515625" Y="3.162605224609" />
                  <Point X="22.130205078125" Y="3.181532470703" />
                  <Point X="21.816666015625" Y="3.724596191406" />
                  <Point X="22.299373046875" Y="4.094683105469" />
                  <Point X="22.380248046875" Y="4.139614746094" />
                  <Point X="22.832962890625" Y="4.391133789062" />
                  <Point X="22.9291484375" Y="4.265781738281" />
                  <Point X="22.9568046875" Y="4.229740234375" />
                  <Point X="22.971109375" Y="4.214797851563" />
                  <Point X="22.987689453125" Y="4.200885742188" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.0180546875" Y="4.182540039063" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222548828125" Y="4.134482421875" />
                  <Point X="23.236263671875" Y="4.140163574219" />
                  <Point X="23.32172265625" Y="4.175562011719" />
                  <Point X="23.339853515625" Y="4.185508789062" />
                  <Point X="23.357583984375" Y="4.197922851562" />
                  <Point X="23.373140625" Y="4.211563476563" />
                  <Point X="23.385373046875" Y="4.228248535156" />
                  <Point X="23.396193359375" Y="4.2469921875" />
                  <Point X="23.4045234375" Y="4.265927734375" />
                  <Point X="23.408986328125" Y="4.2800859375" />
                  <Point X="23.436802734375" Y="4.368304199219" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410145996094" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.41580078125" Y="4.631898925781" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.148400390625" Y="4.821281738281" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.83937109375" Y="4.386054199219" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183885742188" />
                  <Point X="25.067130859375" Y="4.194217773438" />
                  <Point X="25.094423828125" Y="4.208806152344" />
                  <Point X="25.11558203125" Y="4.231394042969" />
                  <Point X="25.13344140625" Y="4.258120117188" />
                  <Point X="25.146216796875" Y="4.286314453125" />
                  <Point X="25.307419921875" Y="4.8879375" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="25.92515625" Y="4.812155761719" />
                  <Point X="26.48102734375" Y="4.677951171875" />
                  <Point X="26.532544921875" Y="4.659265136719" />
                  <Point X="26.894642578125" Y="4.527930175781" />
                  <Point X="26.945703125" Y="4.50405078125" />
                  <Point X="27.2945703125" Y="4.340897460937" />
                  <Point X="27.3439296875" Y="4.312139648438" />
                  <Point X="27.680990234375" Y="4.115767089844" />
                  <Point X="27.727501953125" Y="4.082691650391" />
                  <Point X="27.943263671875" Y="3.929254638672" />
                  <Point X="27.2667421875" Y="2.757487304688" />
                  <Point X="27.147583984375" Y="2.55109765625" />
                  <Point X="27.140015625" Y="2.534482910156" />
                  <Point X="27.132044921875" Y="2.511297607422" />
                  <Point X="27.130109375" Y="2.504956054688" />
                  <Point X="27.111607421875" Y="2.435772949219" />
                  <Point X="27.108384765625" Y="2.411828125" />
                  <Point X="27.108205078125" Y="2.38332421875" />
                  <Point X="27.10888671875" Y="2.371353759766" />
                  <Point X="27.116099609375" Y="2.311530029297" />
                  <Point X="27.12144140625" Y="2.289608398438" />
                  <Point X="27.12970703125" Y="2.267518798828" />
                  <Point X="27.140072265625" Y="2.247467773438" />
                  <Point X="27.146013671875" Y="2.238712402344" />
                  <Point X="27.18303125" Y="2.184159179688" />
                  <Point X="27.1991875" Y="2.165917236328" />
                  <Point X="27.2212421875" Y="2.146676269531" />
                  <Point X="27.23035546875" Y="2.139650390625" />
                  <Point X="27.28491015625" Y="2.102633789063" />
                  <Point X="27.304955078125" Y="2.092271728516" />
                  <Point X="27.32704296875" Y="2.084006103516" />
                  <Point X="27.34896484375" Y="2.078663330078" />
                  <Point X="27.35856640625" Y="2.077505615234" />
                  <Point X="27.418388671875" Y="2.070291748047" />
                  <Point X="27.443259765625" Y="2.070572265625" />
                  <Point X="27.473265625" Y="2.074879150391" />
                  <Point X="27.48430859375" Y="2.077140136719" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.874529296875" Y="2.852613769531" />
                  <Point X="28.967326171875" Y="2.906190673828" />
                  <Point X="29.12327734375" Y="2.68945703125" />
                  <Point X="29.149205078125" Y="2.646606933594" />
                  <Point X="29.26219921875" Y="2.459883300781" />
                  <Point X="28.387669921875" Y="1.788832641602" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.21716015625" Y="1.655683959961" />
                  <Point X="28.199923828125" Y="1.636009277344" />
                  <Point X="28.195982421875" Y="1.631202636719" />
                  <Point X="28.14619140625" Y="1.56624621582" />
                  <Point X="28.133837890625" Y="1.544848144531" />
                  <Point X="28.122392578125" Y="1.517253540039" />
                  <Point X="28.11865625" Y="1.50644519043" />
                  <Point X="28.100107421875" Y="1.440124633789" />
                  <Point X="28.09665234375" Y="1.417816162109" />
                  <Point X="28.095837890625" Y="1.394237915039" />
                  <Point X="28.097744140625" Y="1.371750610352" />
                  <Point X="28.10018359375" Y="1.359927001953" />
                  <Point X="28.115408203125" Y="1.28613684082" />
                  <Point X="28.123607421875" Y="1.262590698242" />
                  <Point X="28.137451171875" Y="1.235111328125" />
                  <Point X="28.142927734375" Y="1.225640136719" />
                  <Point X="28.184337890625" Y="1.162696533203" />
                  <Point X="28.198892578125" Y="1.145450317383" />
                  <Point X="28.21613671875" Y="1.129360473633" />
                  <Point X="28.2343515625" Y="1.116032226562" />
                  <Point X="28.243984375" Y="1.110610595703" />
                  <Point X="28.303994140625" Y="1.076829711914" />
                  <Point X="28.327634765625" Y="1.067432739258" />
                  <Point X="28.3586328125" Y="1.059713745117" />
                  <Point X="28.369140625" Y="1.057717651367" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.7098125" Y="1.207812133789" />
                  <Point X="29.77683984375" Y="1.21663659668" />
                  <Point X="29.8459375" Y="0.932803955078" />
                  <Point X="29.854109375" Y="0.880315490723" />
                  <Point X="29.890865234375" Y="0.644238769531" />
                  <Point X="28.896373046875" Y="0.377764953613" />
                  <Point X="28.716580078125" Y="0.329589874268" />
                  <Point X="28.698775390625" Y="0.32284387207" />
                  <Point X="28.67390234375" Y="0.310441375732" />
                  <Point X="28.66875390625" Y="0.307673187256" />
                  <Point X="28.589037109375" Y="0.261595825195" />
                  <Point X="28.569080078125" Y="0.246197158813" />
                  <Point X="28.547091796875" Y="0.223995681763" />
                  <Point X="28.53985546875" Y="0.215795669556" />
                  <Point X="28.492025390625" Y="0.154849243164" />
                  <Point X="28.48030078125" Y="0.135567611694" />
                  <Point X="28.47052734375" Y="0.114103851318" />
                  <Point X="28.463681640625" Y="0.092603042603" />
                  <Point X="28.461123046875" Y="0.079242149353" />
                  <Point X="28.4451796875" Y="-0.004007439137" />
                  <Point X="28.443783203125" Y="-0.029407131195" />
                  <Point X="28.446341796875" Y="-0.061577281952" />
                  <Point X="28.44773828125" Y="-0.071914360046" />
                  <Point X="28.463681640625" Y="-0.155164260864" />
                  <Point X="28.47052734375" Y="-0.176664001465" />
                  <Point X="28.48030078125" Y="-0.198127761841" />
                  <Point X="28.4920234375" Y="-0.217406814575" />
                  <Point X="28.49969921875" Y="-0.227188186646" />
                  <Point X="28.547529296875" Y="-0.288134460449" />
                  <Point X="28.566201171875" Y="-0.306178833008" />
                  <Point X="28.593306640625" Y="-0.325994171143" />
                  <Point X="28.601830078125" Y="-0.33155090332" />
                  <Point X="28.681546875" Y="-0.377628234863" />
                  <Point X="28.692708984375" Y="-0.383138244629" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="29.836849609375" Y="-0.692325378418" />
                  <Point X="29.89147265625" Y="-0.706961853027" />
                  <Point X="29.8550234375" Y="-0.948727416992" />
                  <Point X="29.844552734375" Y="-0.994608215332" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="28.632890625" Y="-1.030891601562" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.34955078125" Y="-1.010966369629" />
                  <Point X="28.193095703125" Y="-1.044972412109" />
                  <Point X="28.1639765625" Y="-1.056595947266" />
                  <Point X="28.1361484375" Y="-1.07348840332" />
                  <Point X="28.1123984375" Y="-1.093958862305" />
                  <Point X="28.097220703125" Y="-1.112212158203" />
                  <Point X="28.002654296875" Y="-1.225946899414" />
                  <Point X="27.987931640625" Y="-1.250334838867" />
                  <Point X="27.97658984375" Y="-1.277720214844" />
                  <Point X="27.969759765625" Y="-1.305363647461" />
                  <Point X="27.967583984375" Y="-1.329002685547" />
                  <Point X="27.95403125" Y="-1.476294067383" />
                  <Point X="27.95634765625" Y="-1.507561767578" />
                  <Point X="27.964078125" Y="-1.539182739258" />
                  <Point X="27.97644921875" Y="-1.567993408203" />
                  <Point X="27.99034375" Y="-1.589607543945" />
                  <Point X="28.0769296875" Y="-1.724283935547" />
                  <Point X="28.0869375" Y="-1.737243286133" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="29.150248046875" Y="-2.558635742188" />
                  <Point X="29.213123046875" Y="-2.606881835938" />
                  <Point X="29.124814453125" Y="-2.749781005859" />
                  <Point X="29.103154296875" Y="-2.780556152344" />
                  <Point X="29.028982421875" Y="-2.885944824219" />
                  <Point X="27.98666015625" Y="-2.28416015625" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159824951172" />
                  <Point X="27.72433984375" Y="-2.154427978516" />
                  <Point X="27.538134765625" Y="-2.120799316406" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.4200078125" Y="-2.148242919922" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246549804688" />
                  <Point X="27.22142578125" Y="-2.267509521484" />
                  <Point X="27.204533203125" Y="-2.290437988281" />
                  <Point X="27.191466796875" Y="-2.315264404297" />
                  <Point X="27.110052734375" Y="-2.469956054688" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531908203125" />
                  <Point X="27.095677734375" Y="-2.563262451172" />
                  <Point X="27.101076171875" Y="-2.593146728516" />
                  <Point X="27.134705078125" Y="-2.779352783203" />
                  <Point X="27.13898828125" Y="-2.795139648438" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.819880859375" Y="-3.983189941406" />
                  <Point X="27.86128515625" Y="-4.054905273438" />
                  <Point X="27.781841796875" Y="-4.111649414062" />
                  <Point X="27.757640625" Y="-4.127314941406" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="26.9002578125" Y="-3.118935302734" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900556884766" />
                  <Point X="26.692451171875" Y="-2.881607666016" />
                  <Point X="26.50880078125" Y="-2.763538085938" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.384865234375" Y="-2.744083007813" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.07970703125" Y="-2.816156982422" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968861083984" />
                  <Point X="25.88725" Y="-2.996687744141" />
                  <Point X="25.875625" Y="-3.025810791016" />
                  <Point X="25.86818359375" Y="-3.060051025391" />
                  <Point X="25.821810546875" Y="-3.273398681641" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323119873047" />
                  <Point X="26.00906640625" Y="-4.76116796875" />
                  <Point X="26.02206640625" Y="-4.859915039062" />
                  <Point X="25.975671875" Y="-4.870084960938" />
                  <Point X="25.953310546875" Y="-4.874146972656" />
                  <Point X="25.92931640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941578125" Y="-4.752637695312" />
                  <Point X="23.907662109375" Y="-4.743911621094" />
                  <Point X="23.858755859375" Y="-4.731328125" />
                  <Point X="23.872556640625" Y="-4.626492675781" />
                  <Point X="23.8792265625" Y="-4.575838867188" />
                  <Point X="23.879923828125" Y="-4.568099609375" />
                  <Point X="23.88055078125" Y="-4.541031738281" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497687988281" />
                  <Point X="23.868294921875" Y="-4.455605957031" />
                  <Point X="23.81613671875" Y="-4.193396484375" />
                  <Point X="23.811873046875" Y="-4.178467285156" />
                  <Point X="23.800970703125" Y="-4.149500488281" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.769423828125" Y="-4.094858886719" />
                  <Point X="23.749791015625" Y="-4.070937011719" />
                  <Point X="23.738994140625" Y="-4.059781982422" />
                  <Point X="23.706736328125" Y="-4.031491455078" />
                  <Point X="23.505734375" Y="-3.855216796875" />
                  <Point X="23.493263671875" Y="-3.845967529297" />
                  <Point X="23.46698046875" Y="-3.829622558594" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.32037109375" Y="-3.793363525391" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.79549609375" />
                  <Point X="22.918134765625" Y="-3.808269042969" />
                  <Point X="22.9045625" Y="-3.815811767578" />
                  <Point X="22.86888671875" Y="-3.839649414062" />
                  <Point X="22.64659765625" Y="-3.9881796875" />
                  <Point X="22.64031640625" Y="-3.992760009766" />
                  <Point X="22.619556640625" Y="-4.010134521484" />
                  <Point X="22.5972421875" Y="-4.032772460938" />
                  <Point X="22.589529296875" Y="-4.041629638672" />
                  <Point X="22.496798828125" Y="-4.162478515625" />
                  <Point X="22.252404296875" Y="-4.011155517578" />
                  <Point X="22.205474609375" Y="-3.975020996094" />
                  <Point X="22.01913671875" Y="-3.831546875" />
                  <Point X="22.55555859375" Y="-2.902437255859" />
                  <Point X="22.658509765625" Y="-2.724119384766" />
                  <Point X="22.665146484375" Y="-2.710088623047" />
                  <Point X="22.676046875" Y="-2.681132324219" />
                  <Point X="22.680310546875" Y="-2.666206787109" />
                  <Point X="22.686583984375" Y="-2.634685791016" />
                  <Point X="22.6883828125" Y="-2.618530273438" />
                  <Point X="22.689197265625" Y="-2.586142822266" />
                  <Point X="22.684474609375" Y="-2.554087646484" />
                  <Point X="22.674357421875" Y="-2.523310546875" />
                  <Point X="22.667978515625" Y="-2.508356689453" />
                  <Point X="22.651361328125" Y="-2.477123291016" />
                  <Point X="22.6438515625" Y="-2.465227050781" />
                  <Point X="22.627158203125" Y="-2.442672851562" />
                  <Point X="22.617974609375" Y="-2.432014892578" />
                  <Point X="22.603025390625" Y="-2.417065673828" />
                  <Point X="22.591181640625" Y="-2.407018554688" />
                  <Point X="22.566021484375" Y="-2.388990234375" />
                  <Point X="22.552705078125" Y="-2.381009033203" />
                  <Point X="22.523875" Y="-2.366791992188" />
                  <Point X="22.509435546875" Y="-2.3610859375" />
                  <Point X="22.479818359375" Y="-2.352101806641" />
                  <Point X="22.4491328125" Y="-2.348062011719" />
                  <Point X="22.418201171875" Y="-2.349074951172" />
                  <Point X="22.40277734375" Y="-2.350849609375" />
                  <Point X="22.371251953125" Y="-2.357120605469" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372285888672" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.206912109375" Y="-3.017708496094" />
                  <Point X="20.995998046875" Y="-2.740610839844" />
                  <Point X="20.962337890625" Y="-2.684168212891" />
                  <Point X="20.818736328125" Y="-2.443371826172" />
                  <Point X="21.77174609375" Y="-1.712102172852" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.960837890625" Y="-1.566067871094" />
                  <Point X="21.983724609375" Y="-1.543439086914" />
                  <Point X="21.997015625" Y="-1.526989135742" />
                  <Point X="22.015578125" Y="-1.497901000977" />
                  <Point X="22.02352734375" Y="-1.482507324219" />
                  <Point X="22.031310546875" Y="-1.463320678711" />
                  <Point X="22.039173828125" Y="-1.439534545898" />
                  <Point X="22.0458125" Y="-1.413901245117" />
                  <Point X="22.048046875" Y="-1.402395507812" />
                  <Point X="22.052162109375" Y="-1.370910400391" />
                  <Point X="22.05291796875" Y="-1.355686523438" />
                  <Point X="22.052216796875" Y="-1.332821289062" />
                  <Point X="22.04876953125" Y="-1.310215454102" />
                  <Point X="22.042625" Y="-1.288180664062" />
                  <Point X="22.03888671875" Y="-1.277346069336" />
                  <Point X="22.0268828125" Y="-1.248363647461" />
                  <Point X="22.021470703125" Y="-1.237362670898" />
                  <Point X="22.0092734375" Y="-1.216149291992" />
                  <Point X="21.9944453125" Y="-1.196683959961" />
                  <Point X="21.977234375" Y="-1.179289794922" />
                  <Point X="21.96806640625" Y="-1.171148071289" />
                  <Point X="21.95190234375" Y="-1.158563476562" />
                  <Point X="21.93155078125" Y="-1.144738769531" />
                  <Point X="21.90873046875" Y="-1.131307861328" />
                  <Point X="21.89456640625" Y="-1.124481201172" />
                  <Point X="21.8653046875" Y="-1.113257446289" />
                  <Point X="21.85020703125" Y="-1.108860595703" />
                  <Point X="21.8183203125" Y="-1.10237890625" />
                  <Point X="21.802703125" Y="-1.100532592773" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="20.353037109375" Y="-1.284856933594" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.259236328125" Y="-0.974107299805" />
                  <Point X="20.2503359375" Y="-0.911871887207" />
                  <Point X="20.213548828125" Y="-0.654654663086" />
                  <Point X="21.286515625" Y="-0.367153869629" />
                  <Point X="21.491712890625" Y="-0.312171295166" />
                  <Point X="21.5011640625" Y="-0.309100616455" />
                  <Point X="21.528771484375" Y="-0.297948699951" />
                  <Point X="21.54817578125" Y="-0.288065124512" />
                  <Point X="21.5702734375" Y="-0.274853302002" />
                  <Point X="21.594189453125" Y="-0.258255096436" />
                  <Point X="21.603521484375" Y="-0.250871368408" />
                  <Point X="21.6274765625" Y="-0.22934513855" />
                  <Point X="21.641552734375" Y="-0.213522445679" />
                  <Point X="21.6615078125" Y="-0.185294815063" />
                  <Point X="21.670189453125" Y="-0.170266067505" />
                  <Point X="21.67889453125" Y="-0.151404937744" />
                  <Point X="21.68784765625" Y="-0.128097015381" />
                  <Point X="21.695818359375" Y="-0.102412307739" />
                  <Point X="21.699265625" Y="-0.086710456848" />
                  <Point X="21.70346484375" Y="-0.054956588745" />
                  <Point X="21.704216796875" Y="-0.038904727936" />
                  <Point X="21.7029375" Y="-0.00514400959" />
                  <Point X="21.701646484375" Y="0.007269861698" />
                  <Point X="21.697447265625" Y="0.031828895569" />
                  <Point X="21.6945390625" Y="0.043974205017" />
                  <Point X="21.686568359375" Y="0.069659057617" />
                  <Point X="21.682333984375" Y="0.080784370422" />
                  <Point X="21.669015625" Y="0.110111373901" />
                  <Point X="21.658103515625" Y="0.128377716064" />
                  <Point X="21.6370390625" Y="0.15604460144" />
                  <Point X="21.625228515625" Y="0.16890776062" />
                  <Point X="21.6095703125" Y="0.183090240479" />
                  <Point X="21.5903515625" Y="0.198358856201" />
                  <Point X="21.566435546875" Y="0.214957061768" />
                  <Point X="21.559509765625" Y="0.219332962036" />
                  <Point X="21.534384765625" Y="0.232834594727" />
                  <Point X="21.503435546875" Y="0.245635482788" />
                  <Point X="21.491712890625" Y="0.249611297607" />
                  <Point X="20.2145546875" Y="0.591825012207" />
                  <Point X="20.26866796875" Y="0.957518432617" />
                  <Point X="20.28659375" Y="1.023669250488" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="21.07484375" Y="1.224970581055" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815307617" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.20470324707" />
                  <Point X="21.28485546875" Y="1.206589477539" />
                  <Point X="21.295109375" Y="1.208053588867" />
                  <Point X="21.315400390625" Y="1.21208984375" />
                  <Point X="21.3339296875" Y="1.21733996582" />
                  <Point X="21.386861328125" Y="1.234029174805" />
                  <Point X="21.39655859375" Y="1.237679199219" />
                  <Point X="21.41548828125" Y="1.246009399414" />
                  <Point X="21.42472265625" Y="1.250690185547" />
                  <Point X="21.443466796875" Y="1.261512329102" />
                  <Point X="21.452142578125" Y="1.267172485352" />
                  <Point X="21.46882421875" Y="1.279404052734" />
                  <Point X="21.48407421875" Y="1.293379394531" />
                  <Point X="21.497712890625" Y="1.308931884766" />
                  <Point X="21.504107421875" Y="1.317080688477" />
                  <Point X="21.516521484375" Y="1.334810913086" />
                  <Point X="21.5219921875" Y="1.343607666016" />
                  <Point X="21.53194140625" Y="1.361744262695" />
                  <Point X="21.539828125" Y="1.379313720703" />
                  <Point X="21.56106640625" Y="1.430588623047" />
                  <Point X="21.5645" Y="1.440348266602" />
                  <Point X="21.570287109375" Y="1.460197021484" />
                  <Point X="21.572640625" Y="1.470285644531" />
                  <Point X="21.576400390625" Y="1.491600585938" />
                  <Point X="21.577640625" Y="1.501888671875" />
                  <Point X="21.578994140625" Y="1.522535644531" />
                  <Point X="21.578091796875" Y="1.543205444336" />
                  <Point X="21.574943359375" Y="1.563655761719" />
                  <Point X="21.572810546875" Y="1.573794921875" />
                  <Point X="21.56720703125" Y="1.594701293945" />
                  <Point X="21.563982421875" Y="1.604547973633" />
                  <Point X="21.556484375" Y="1.623822631836" />
                  <Point X="21.54809765625" Y="1.641151489258" />
                  <Point X="21.522470703125" Y="1.690380493164" />
                  <Point X="21.517197265625" Y="1.6992890625" />
                  <Point X="21.505708984375" Y="1.716483032227" />
                  <Point X="21.499494140625" Y="1.724768310547" />
                  <Point X="21.48558203125" Y="1.741348876953" />
                  <Point X="21.4785" Y="1.74891015625" />
                  <Point X="21.463556640625" Y="1.763215209961" />
                  <Point X="21.4556953125" Y="1.769958984375" />
                  <Point X="20.77238671875" Y="2.294281005859" />
                  <Point X="20.99771484375" Y="2.680319091797" />
                  <Point X="21.0451796875" Y="2.741330810547" />
                  <Point X="21.273662109375" Y="3.035012939453" />
                  <Point X="21.655505859375" Y="2.814555908203" />
                  <Point X="21.74584375" Y="2.762398681641" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774013671875" Y="2.749386474609" />
                  <Point X="21.78370703125" Y="2.745738037109" />
                  <Point X="21.804349609375" Y="2.739229248047" />
                  <Point X="21.814384765625" Y="2.736657226562" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.856759765625" Y="2.730122802734" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.02356640625" Y="2.734227294922" />
                  <Point X="22.043005859375" Y="2.741302978516" />
                  <Point X="22.061556640625" Y="2.750451904297" />
                  <Point X="22.070580078125" Y="2.755531738281" />
                  <Point X="22.088833984375" Y="2.767160888672" />
                  <Point X="22.097251953125" Y="2.77319140625" />
                  <Point X="22.113380859375" Y="2.786135253906" />
                  <Point X="22.129490234375" Y="2.801446289062" />
                  <Point X="22.18181640625" Y="2.853771972656" />
                  <Point X="22.188732421875" Y="2.861484863281" />
                  <Point X="22.20168359375" Y="2.877620605469" />
                  <Point X="22.20771875" Y="2.886043457031" />
                  <Point X="22.21934765625" Y="2.904297851562" />
                  <Point X="22.2244296875" Y="2.913325683594" />
                  <Point X="22.233576171875" Y="2.931874511719" />
                  <Point X="22.240646484375" Y="2.951299072266" />
                  <Point X="22.245564453125" Y="2.971387939453" />
                  <Point X="22.2474765625" Y="2.981573242188" />
                  <Point X="22.250302734375" Y="3.003031982422" />
                  <Point X="22.251091796875" Y="3.013364990234" />
                  <Point X="22.25154296875" Y="3.034049804688" />
                  <Point X="22.250169921875" Y="3.056232910156" />
                  <Point X="22.243720703125" Y="3.129951171875" />
                  <Point X="22.242255859375" Y="3.140207519531" />
                  <Point X="22.23821875" Y="3.1605" />
                  <Point X="22.235646484375" Y="3.170536132812" />
                  <Point X="22.22913671875" Y="3.191177978516" />
                  <Point X="22.225486328125" Y="3.200873535156" />
                  <Point X="22.21715625" Y="3.21980078125" />
                  <Point X="22.2124765625" Y="3.229032714844" />
                  <Point X="21.940611328125" Y="3.699915283203" />
                  <Point X="22.351626953125" Y="4.015037597656" />
                  <Point X="22.426384765625" Y="4.0565703125" />
                  <Point X="22.807474609375" Y="4.268295898437" />
                  <Point X="22.853779296875" Y="4.207949707031" />
                  <Point X="22.881435546875" Y="4.171908203125" />
                  <Point X="22.888181640625" Y="4.164045410156" />
                  <Point X="22.902486328125" Y="4.149103027344" />
                  <Point X="22.910044921875" Y="4.142022949219" />
                  <Point X="22.926625" Y="4.128110839844" />
                  <Point X="22.93491015625" Y="4.121895996094" />
                  <Point X="22.952107421875" Y="4.110405273438" />
                  <Point X="22.9741875" Y="4.098274414062" />
                  <Point X="23.056236328125" Y="4.0555625" />
                  <Point X="23.065673828125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.0437890625" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.229271484375" Y="4.037488525391" />
                  <Point X="23.2491328125" Y="4.043277587891" />
                  <Point X="23.272619140625" Y="4.052395751953" />
                  <Point X="23.358078125" Y="4.087794189453" />
                  <Point X="23.367416015625" Y="4.092272705078" />
                  <Point X="23.385546875" Y="4.102219238281" />
                  <Point X="23.39433984375" Y="4.1076875" />
                  <Point X="23.4120703125" Y="4.1201015625" />
                  <Point X="23.420216796875" Y="4.126493164063" />
                  <Point X="23.4357734375" Y="4.140133789063" />
                  <Point X="23.449755859375" Y="4.155393554688" />
                  <Point X="23.46198828125" Y="4.172078613281" />
                  <Point X="23.4676484375" Y="4.180752929688" />
                  <Point X="23.47846875" Y="4.199496582031" />
                  <Point X="23.483150390625" Y="4.208737792969" />
                  <Point X="23.49148046875" Y="4.227673339844" />
                  <Point X="23.499591796875" Y="4.251525878906" />
                  <Point X="23.527408203125" Y="4.339744140625" />
                  <Point X="23.529978515625" Y="4.3497734375" />
                  <Point X="23.53401171875" Y="4.370052734375" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401865234375" />
                  <Point X="23.53769921875" Y="4.412218261719" />
                  <Point X="23.537248046875" Y="4.432898925781" />
                  <Point X="23.536458984375" Y="4.443226074219" />
                  <Point X="23.520736328125" Y="4.562655761719" />
                  <Point X="24.068826171875" Y="4.7163203125" />
                  <Point X="24.159443359375" Y="4.72692578125" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.747607421875" Y="4.361466308594" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.021630859375" Y="4.085113037109" />
                  <Point X="25.05216796875" Y="4.090154541016" />
                  <Point X="25.06723046875" Y="4.093927246094" />
                  <Point X="25.09766796875" Y="4.104259277344" />
                  <Point X="25.1119140625" Y="4.110435058594" />
                  <Point X="25.13920703125" Y="4.1250234375" />
                  <Point X="25.1637578125" Y="4.143860839844" />
                  <Point X="25.184916015625" Y="4.166448730469" />
                  <Point X="25.1945703125" Y="4.178611816406" />
                  <Point X="25.2124296875" Y="4.205337890625" />
                  <Point X="25.21997265625" Y="4.218911132812" />
                  <Point X="25.232748046875" Y="4.24710546875" />
                  <Point X="25.23798046875" Y="4.261727050781" />
                  <Point X="25.37819140625" Y="4.785006347656" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="25.902861328125" Y="4.719809082031" />
                  <Point X="26.453599609375" Y="4.58684375" />
                  <Point X="26.50015234375" Y="4.569958496094" />
                  <Point X="26.85825390625" Y="4.440073242188" />
                  <Point X="26.905458984375" Y="4.417996582031" />
                  <Point X="27.25044140625" Y="4.256660644531" />
                  <Point X="27.29610546875" Y="4.230055175781" />
                  <Point X="27.62944921875" Y="4.035848144531" />
                  <Point X="27.672447265625" Y="4.005271240234" />
                  <Point X="27.81778515625" Y="3.901916259766" />
                  <Point X="27.184470703125" Y="2.804987304688" />
                  <Point X="27.0653125" Y="2.59859765625" />
                  <Point X="27.061130859375" Y="2.590478759766" />
                  <Point X="27.05017578125" Y="2.565368164062" />
                  <Point X="27.042205078125" Y="2.542182861328" />
                  <Point X="27.038333984375" Y="2.529499755859" />
                  <Point X="27.01983203125" Y="2.460316650391" />
                  <Point X="27.01745703125" Y="2.448444335938" />
                  <Point X="27.014234375" Y="2.424499511719" />
                  <Point X="27.01338671875" Y="2.412427001953" />
                  <Point X="27.01320703125" Y="2.383923095703" />
                  <Point X="27.0145703125" Y="2.359982177734" />
                  <Point X="27.021783203125" Y="2.300158447266" />
                  <Point X="27.02380078125" Y="2.289038818359" />
                  <Point X="27.029142578125" Y="2.2671171875" />
                  <Point X="27.032466796875" Y="2.256315185547" />
                  <Point X="27.040732421875" Y="2.234225585938" />
                  <Point X="27.04531640625" Y="2.223893554688" />
                  <Point X="27.055681640625" Y="2.203842529297" />
                  <Point X="27.067404296875" Y="2.185368164062" />
                  <Point X="27.104421875" Y="2.130814941406" />
                  <Point X="27.1119140625" Y="2.121172851563" />
                  <Point X="27.1280703125" Y="2.102930908203" />
                  <Point X="27.136734375" Y="2.094331054688" />
                  <Point X="27.1587890625" Y="2.075090087891" />
                  <Point X="27.177015625" Y="2.061038330078" />
                  <Point X="27.2315703125" Y="2.024021850586" />
                  <Point X="27.24128515625" Y="2.018242797852" />
                  <Point X="27.261330078125" Y="2.007880737305" />
                  <Point X="27.27166015625" Y="2.003297485352" />
                  <Point X="27.293748046875" Y="1.995031860352" />
                  <Point X="27.304548828125" Y="1.991707763672" />
                  <Point X="27.326470703125" Y="1.986364990234" />
                  <Point X="27.347193359375" Y="1.983188720703" />
                  <Point X="27.407015625" Y="1.975974853516" />
                  <Point X="27.4194609375" Y="1.975297729492" />
                  <Point X="27.44433203125" Y="1.975578125" />
                  <Point X="27.4567578125" Y="1.976536010742" />
                  <Point X="27.486763671875" Y="1.980842895508" />
                  <Point X="27.508849609375" Y="1.985364868164" />
                  <Point X="27.578033203125" Y="2.003865478516" />
                  <Point X="27.583994140625" Y="2.005670532227" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572753906" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.922029296875" Y="2.770341308594" />
                  <Point X="28.94040625" Y="2.780951416016" />
                  <Point X="29.04394921875" Y="2.637051025391" />
                  <Point X="29.06792578125" Y="2.597426513672" />
                  <Point X="29.136884765625" Y="2.483470947266" />
                  <Point X="28.329837890625" Y="1.864201171875" />
                  <Point X="28.172953125" Y="1.743819824219" />
                  <Point X="28.165826171875" Y="1.737772583008" />
                  <Point X="28.145703125" Y="1.71828503418" />
                  <Point X="28.128466796875" Y="1.698610351562" />
                  <Point X="28.120583984375" Y="1.688997070313" />
                  <Point X="28.07079296875" Y="1.624040771484" />
                  <Point X="28.06391796875" Y="1.613744262695" />
                  <Point X="28.051564453125" Y="1.592346191406" />
                  <Point X="28.0460859375" Y="1.581244506836" />
                  <Point X="28.034640625" Y="1.553650024414" />
                  <Point X="28.02716796875" Y="1.532033203125" />
                  <Point X="28.008619140625" Y="1.465712646484" />
                  <Point X="28.0062265625" Y="1.454664672852" />
                  <Point X="28.002771484375" Y="1.432356201172" />
                  <Point X="28.001708984375" Y="1.421095581055" />
                  <Point X="28.00089453125" Y="1.397517456055" />
                  <Point X="28.001177734375" Y="1.386213500977" />
                  <Point X="28.003083984375" Y="1.363726196289" />
                  <Point X="28.007142578125" Y="1.340730834961" />
                  <Point X="28.0223671875" Y="1.266940551758" />
                  <Point X="28.02569140625" Y="1.254895874023" />
                  <Point X="28.033890625" Y="1.231349731445" />
                  <Point X="28.038765625" Y="1.219848510742" />
                  <Point X="28.052609375" Y="1.192369140625" />
                  <Point X="28.0635625" Y="1.173426635742" />
                  <Point X="28.10497265625" Y="1.110483154297" />
                  <Point X="28.111736328125" Y="1.10142590332" />
                  <Point X="28.126291015625" Y="1.0841796875" />
                  <Point X="28.13408203125" Y="1.075990600586" />
                  <Point X="28.151326171875" Y="1.059900756836" />
                  <Point X="28.160037109375" Y="1.052693237305" />
                  <Point X="28.178251953125" Y="1.039364990234" />
                  <Point X="28.197388671875" Y="1.027822631836" />
                  <Point X="28.2573984375" Y="0.994041687012" />
                  <Point X="28.26890234375" Y="0.988548339844" />
                  <Point X="28.29254296875" Y="0.979151367188" />
                  <Point X="28.3046796875" Y="0.975247924805" />
                  <Point X="28.335677734375" Y="0.967528869629" />
                  <Point X="28.356693359375" Y="0.963536560059" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199768066" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="29.704703125" Y="1.111319702148" />
                  <Point X="29.752685546875" Y="0.914221435547" />
                  <Point X="29.760240234375" Y="0.86570111084" />
                  <Point X="29.78387109375" Y="0.713921142578" />
                  <Point X="28.87178515625" Y="0.469527923584" />
                  <Point X="28.6919921875" Y="0.421352874756" />
                  <Point X="28.682919921875" Y="0.418427093506" />
                  <Point X="28.6563828125" Y="0.407860900879" />
                  <Point X="28.631509765625" Y="0.395458465576" />
                  <Point X="28.621212890625" Y="0.389921966553" />
                  <Point X="28.54149609375" Y="0.343844573975" />
                  <Point X="28.53100390625" Y="0.336809204102" />
                  <Point X="28.511046875" Y="0.321410552979" />
                  <Point X="28.50158203125" Y="0.313047393799" />
                  <Point X="28.47959375" Y="0.290845947266" />
                  <Point X="28.46512109375" Y="0.274446014404" />
                  <Point X="28.417291015625" Y="0.213499511719" />
                  <Point X="28.410853515625" Y="0.204207168579" />
                  <Point X="28.39912890625" Y="0.184925582886" />
                  <Point X="28.393841796875" Y="0.174936203003" />
                  <Point X="28.384068359375" Y="0.15347253418" />
                  <Point X="28.380005859375" Y="0.142925506592" />
                  <Point X="28.37316015625" Y="0.121424690247" />
                  <Point X="28.367818359375" Y="0.097109840393" />
                  <Point X="28.351875" Y="0.013860244751" />
                  <Point X="28.350322265625" Y="0.00120783329" />
                  <Point X="28.34892578125" Y="-0.024191810608" />
                  <Point X="28.34908203125" Y="-0.036939044952" />
                  <Point X="28.351640625" Y="-0.069109199524" />
                  <Point X="28.35443359375" Y="-0.089783233643" />
                  <Point X="28.370376953125" Y="-0.173033126831" />
                  <Point X="28.37316015625" Y="-0.183987228394" />
                  <Point X="28.380005859375" Y="-0.205487014771" />
                  <Point X="28.384068359375" Y="-0.216032684326" />
                  <Point X="28.393841796875" Y="-0.237496353149" />
                  <Point X="28.39912890625" Y="-0.247484558105" />
                  <Point X="28.4108515625" Y="-0.26676361084" />
                  <Point X="28.424962890625" Y="-0.2858359375" />
                  <Point X="28.47279296875" Y="-0.346782287598" />
                  <Point X="28.48151171875" Y="-0.356447692871" />
                  <Point X="28.50018359375" Y="-0.374492126465" />
                  <Point X="28.51013671875" Y="-0.382870910645" />
                  <Point X="28.5372421875" Y="-0.402686187744" />
                  <Point X="28.5542890625" Y="-0.413799743652" />
                  <Point X="28.634005859375" Y="-0.45987701416" />
                  <Point X="28.63949609375" Y="-0.462814544678" />
                  <Point X="28.65915625" Y="-0.472015930176" />
                  <Point X="28.68302734375" Y="-0.481027526855" />
                  <Point X="28.6919921875" Y="-0.483912872314" />
                  <Point X="29.78487890625" Y="-0.776751037598" />
                  <Point X="29.7616171875" Y="-0.931036621094" />
                  <Point X="29.75193359375" Y="-0.973471191406" />
                  <Point X="29.727802734375" Y="-1.079219604492" />
                  <Point X="28.645291015625" Y="-0.936704406738" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535644531" />
                  <Point X="28.40009765625" Y="-0.908042358398" />
                  <Point X="28.36672265625" Y="-0.910840942383" />
                  <Point X="28.354482421875" Y="-0.912676269531" />
                  <Point X="28.329373046875" Y="-0.918133850098" />
                  <Point X="28.17291796875" Y="-0.952139953613" />
                  <Point X="28.157876953125" Y="-0.956741882324" />
                  <Point X="28.1287578125" Y="-0.968365539551" />
                  <Point X="28.1146796875" Y="-0.975387084961" />
                  <Point X="28.0868515625" Y="-0.992279541016" />
                  <Point X="28.074125" Y="-1.001528930664" />
                  <Point X="28.050375" Y="-1.021999328613" />
                  <Point X="28.0393515625" Y="-1.033220092773" />
                  <Point X="28.024173828125" Y="-1.051473388672" />
                  <Point X="27.929607421875" Y="-1.165208129883" />
                  <Point X="27.92132421875" Y="-1.176849609375" />
                  <Point X="27.9066015625" Y="-1.201237670898" />
                  <Point X="27.900162109375" Y="-1.21398425293" />
                  <Point X="27.8888203125" Y="-1.241369628906" />
                  <Point X="27.88436328125" Y="-1.254933105469" />
                  <Point X="27.877533203125" Y="-1.282576538086" />
                  <Point X="27.87516015625" Y="-1.296656494141" />
                  <Point X="27.872984375" Y="-1.320295532227" />
                  <Point X="27.859431640625" Y="-1.467587036133" />
                  <Point X="27.859291015625" Y="-1.483312744141" />
                  <Point X="27.861607421875" Y="-1.514580444336" />
                  <Point X="27.864064453125" Y="-1.530122314453" />
                  <Point X="27.871794921875" Y="-1.561743286133" />
                  <Point X="27.87678515625" Y="-1.576665649414" />
                  <Point X="27.88915625" Y="-1.605476318359" />
                  <Point X="27.896537109375" Y="-1.619364624023" />
                  <Point X="27.910431640625" Y="-1.640978759766" />
                  <Point X="27.997017578125" Y="-1.775655151367" />
                  <Point X="28.001740234375" Y="-1.782348754883" />
                  <Point X="28.01980078125" Y="-1.804457397461" />
                  <Point X="28.043494140625" Y="-1.828123291016" />
                  <Point X="28.052798828125" Y="-1.836277709961" />
                  <Point X="29.087171875" Y="-2.629980224609" />
                  <Point X="29.045490234375" Y="-2.697428955078" />
                  <Point X="29.025466796875" Y="-2.725878173828" />
                  <Point X="29.001275390625" Y="-2.760251464844" />
                  <Point X="28.03416015625" Y="-2.201887695312" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513671875" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337158203" />
                  <Point X="27.74122265625" Y="-2.060940185547" />
                  <Point X="27.555017578125" Y="-2.027311523438" />
                  <Point X="27.539359375" Y="-2.025807250977" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.03513659668" />
                  <Point X="27.415068359375" Y="-2.044960571289" />
                  <Point X="27.40058984375" Y="-2.051109130859" />
                  <Point X="27.375763671875" Y="-2.064175048828" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.20896875" Y="-2.153170654297" />
                  <Point X="27.186037109375" Y="-2.170064453125" />
                  <Point X="27.175208984375" Y="-2.179375732422" />
                  <Point X="27.15425" Y="-2.200335449219" />
                  <Point X="27.14494140625" Y="-2.21116015625" />
                  <Point X="27.128048828125" Y="-2.234088623047" />
                  <Point X="27.12046484375" Y="-2.246192382812" />
                  <Point X="27.1073984375" Y="-2.271018798828" />
                  <Point X="27.025984375" Y="-2.425710449219" />
                  <Point X="27.0198359375" Y="-2.440193359375" />
                  <Point X="27.01001171875" Y="-2.46997265625" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517441894531" />
                  <Point X="27.000279296875" Y="-2.533138916016" />
                  <Point X="27.000685546875" Y="-2.564493164062" />
                  <Point X="27.00219140625" Y="-2.580150390625" />
                  <Point X="27.00758984375" Y="-2.610034667969" />
                  <Point X="27.04121875" Y="-2.796240722656" />
                  <Point X="27.04301953125" Y="-2.804228271484" />
                  <Point X="27.05123828125" Y="-2.831539794922" />
                  <Point X="27.064072265625" Y="-2.862478759766" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.735896484375" Y="-4.027722167969" />
                  <Point X="27.723755859375" Y="-4.036082275391" />
                  <Point X="26.975626953125" Y="-3.061102783203" />
                  <Point X="26.83391796875" Y="-2.876422607422" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849626220703" />
                  <Point X="26.78325390625" Y="-2.828004882812" />
                  <Point X="26.77330078125" Y="-2.820646484375" />
                  <Point X="26.743826171875" Y="-2.801697265625" />
                  <Point X="26.56017578125" Y="-2.683627685547" />
                  <Point X="26.546283203125" Y="-2.676244628906" />
                  <Point X="26.517470703125" Y="-2.663873291016" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.37616015625" Y="-2.649482666016" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="26.018970703125" Y="-2.743109130859" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883087646484" />
                  <Point X="25.83218359375" Y="-2.906837158203" />
                  <Point X="25.822935546875" Y="-2.919562744141" />
                  <Point X="25.80604296875" Y="-2.947389404297" />
                  <Point X="25.79901953125" Y="-2.961468994141" />
                  <Point X="25.78739453125" Y="-2.990592041016" />
                  <Point X="25.78279296875" Y="-3.005635498047" />
                  <Point X="25.7753515625" Y="-3.039875732422" />
                  <Point X="25.728978515625" Y="-3.253223388672" />
                  <Point X="25.7275859375" Y="-3.261287597656" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323169677734" />
                  <Point X="25.7255546875" Y="-3.335520019531" />
                  <Point X="25.83308984375" Y="-4.152326660156" />
                  <Point X="25.710466796875" Y="-3.694687011719" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480122314453" />
                  <Point X="25.642146484375" Y="-3.453577880859" />
                  <Point X="25.6267890625" Y="-3.423815185547" />
                  <Point X="25.62041015625" Y="-3.413210693359" />
                  <Point X="25.597767578125" Y="-3.380586669922" />
                  <Point X="25.456681640625" Y="-3.177310302734" />
                  <Point X="25.446671875" Y="-3.165173339844" />
                  <Point X="25.424787109375" Y="-3.142717041016" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.373240234375" Y="-3.104936523438" />
                  <Point X="25.345240234375" Y="-3.090829345703" />
                  <Point X="25.33065625" Y="-3.084939453125" />
                  <Point X="25.295619140625" Y="-3.074064697266" />
                  <Point X="25.077296875" Y="-3.006306152344" />
                  <Point X="25.06337890625" Y="-3.003109863281" />
                  <Point X="25.03521875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.998840087891" />
                  <Point X="24.948935546875" Y="-3.003109619141" />
                  <Point X="24.935015625" Y="-3.006305664062" />
                  <Point X="24.899978515625" Y="-3.017180175781" />
                  <Point X="24.68165625" Y="-3.084938964844" />
                  <Point X="24.6670703125" Y="-3.090830078125" />
                  <Point X="24.6390703125" Y="-3.104937988281" />
                  <Point X="24.62565625" Y="-3.113154785156" />
                  <Point X="24.599400390625" Y="-3.132398925781" />
                  <Point X="24.587525390625" Y="-3.142716552734" />
                  <Point X="24.565640625" Y="-3.165172119141" />
                  <Point X="24.555630859375" Y="-3.177310058594" />
                  <Point X="24.53298828125" Y="-3.209933837891" />
                  <Point X="24.391904296875" Y="-3.413210449219" />
                  <Point X="24.38753125" Y="-3.420131835938" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213623047" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.662772052697" Y="-3.956606719281" />
                  <Point X="27.689520408403" Y="-3.947396521801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.60178824602" Y="-3.87713116317" />
                  <Point X="27.641131328372" Y="-3.863584253525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.540804439342" Y="-3.79765560706" />
                  <Point X="27.592742248341" Y="-3.779771985248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.479820632664" Y="-3.71818005095" />
                  <Point X="27.54435316831" Y="-3.695959716972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.418836825987" Y="-3.63870449484" />
                  <Point X="27.495964088279" Y="-3.612147448696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.913169507742" Y="-4.745328590806" />
                  <Point X="24.031218739985" Y="-4.704680980417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.816388969518" Y="-4.089997575967" />
                  <Point X="25.824515628165" Y="-4.08719934299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.357853019309" Y="-3.55922893873" />
                  <Point X="27.447575008248" Y="-3.528335180419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.868096351137" Y="-4.660374558531" />
                  <Point X="24.060877163128" Y="-4.59399480165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.791741326769" Y="-3.998010475255" />
                  <Point X="25.811861547928" Y="-3.991082527524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.296869212631" Y="-3.47975338262" />
                  <Point X="27.399185928217" Y="-3.444522912143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.880210349936" Y="-4.555729409524" />
                  <Point X="24.090535586271" Y="-4.483308622882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.76709368402" Y="-3.906023374543" />
                  <Point X="25.79920746769" Y="-3.894965712058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.235885405954" Y="-3.40027782651" />
                  <Point X="27.350796848186" Y="-3.360710643866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.868993492814" Y="-4.459117718453" />
                  <Point X="24.120194009414" Y="-4.372622444115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.742446041271" Y="-3.814036273831" />
                  <Point X="25.786553387452" Y="-3.798848896592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.174901599276" Y="-3.3208022704" />
                  <Point X="27.302407768155" Y="-3.27689837559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.927149537741" Y="-2.717454919856" />
                  <Point X="29.061757327023" Y="-2.671105741043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.850288544928" Y="-4.365084383802" />
                  <Point X="24.149852432556" Y="-4.261936265347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.717798398522" Y="-3.72204917312" />
                  <Point X="25.773899307214" Y="-3.702732081126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.113917792598" Y="-3.24132671429" />
                  <Point X="27.254018688124" Y="-3.193086107314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.818137463398" Y="-2.654516822522" />
                  <Point X="29.025912407028" Y="-2.582974172081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.831583614004" Y="-4.271051043312" />
                  <Point X="24.179510855699" Y="-4.15125008658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.693150626631" Y="-3.630062116875" />
                  <Point X="25.761245226976" Y="-3.60661526566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.052933985921" Y="-3.16185115818" />
                  <Point X="27.205629608092" Y="-3.109273839037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.709125389055" Y="-2.591578725188" />
                  <Point X="28.935530061104" Y="-2.513621344823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.811505430009" Y="-4.177490551773" />
                  <Point X="24.209169278842" Y="-4.040563907812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.668502800062" Y="-3.538075079457" />
                  <Point X="25.748591146738" Y="-3.510498450194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.991950179243" Y="-3.08237560207" />
                  <Point X="27.157240528061" Y="-3.025461570761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.600113314712" Y="-2.528640627853" />
                  <Point X="28.84514771518" Y="-2.444268517566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.767278665509" Y="-4.092245083324" />
                  <Point X="24.238827701985" Y="-3.929877729045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.639122572956" Y="-3.447717538222" />
                  <Point X="25.7359370665" Y="-3.414381634728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.9309666856" Y="-3.002899938173" />
                  <Point X="27.10885144803" Y="-2.941649302485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.491101240368" Y="-2.465702530519" />
                  <Point X="28.754765369256" Y="-2.374915690309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.6912824487" Y="-4.017938714564" />
                  <Point X="24.268486125128" Y="-3.819191550278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.587066130966" Y="-3.365168043936" />
                  <Point X="25.724739350051" Y="-3.317763352994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.86998330637" Y="-2.923424234881" />
                  <Point X="27.061936279814" Y="-2.857329525671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.382089166025" Y="-2.402764433184" />
                  <Point X="28.664383023332" Y="-2.305562863051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.609014903816" Y="-3.945791737233" />
                  <Point X="24.298144548271" Y="-3.70850537151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.530782163186" Y="-3.284074203515" />
                  <Point X="25.737763888286" Y="-3.212804680117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.804107989564" Y="-2.845632960779" />
                  <Point X="27.035735109312" Y="-2.765877347463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.273077091682" Y="-2.33982633585" />
                  <Point X="28.574000677409" Y="-2.236210035794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.526747358931" Y="-3.873644759901" />
                  <Point X="24.327802971414" Y="-3.597819192743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.474498195406" Y="-3.202980363095" />
                  <Point X="25.761369525494" Y="-3.104202642685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.707581816626" Y="-2.778395622814" />
                  <Point X="27.018651799465" Y="-2.671285638057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.164065017339" Y="-2.276888238516" />
                  <Point X="28.483618331485" Y="-2.166857208537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.460817608718" Y="-4.140199842014" />
                  <Point X="22.532952767963" Y="-4.115361714797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.421226194866" Y="-3.809504645762" />
                  <Point X="24.357526227251" Y="-3.487110690288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.404849545332" Y="-3.126488351831" />
                  <Point X="25.785966947644" Y="-2.99525910631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.605808374424" Y="-2.712965064551" />
                  <Point X="27.001849663921" Y="-2.576597112574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.055052942996" Y="-2.213950141181" />
                  <Point X="28.393235985561" Y="-2.09750438128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.356537706116" Y="-4.075632327278" />
                  <Point X="22.684957135985" Y="-3.962548448833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.199343616594" Y="-3.785430979657" />
                  <Point X="24.426935133847" Y="-3.362737322426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.279673730962" Y="-3.069115876522" />
                  <Point X="25.872304080857" Y="-2.865056882579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.484142306672" Y="-2.654384086565" />
                  <Point X="27.009136683475" Y="-2.47361402581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.946040999772" Y="-2.151011998699" />
                  <Point X="28.302853639637" Y="-2.028151554022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.252277628241" Y="-4.011057986341" />
                  <Point X="24.518567301748" Y="-3.23071187204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.126205221795" Y="-3.021485357286" />
                  <Point X="27.063504640353" Y="-2.354419672266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.835742903612" Y="-2.088516714187" />
                  <Point X="28.212471293714" Y="-1.958798726765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.162109614046" Y="-3.941631358752" />
                  <Point X="27.130385750694" Y="-2.230916694455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.664352067718" Y="-2.047057346937" />
                  <Point X="28.12208894779" Y="-1.889445899508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.071941432486" Y="-3.87220478879" />
                  <Point X="28.034493784516" Y="-1.819133368301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.049138341616" Y="-3.779582557932" />
                  <Point X="27.973808204637" Y="-1.739555124469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.121540394616" Y="-3.654178567113" />
                  <Point X="27.920919789158" Y="-1.657292101629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.635178675643" Y="-1.067025430685" />
                  <Point X="29.738720914258" Y="-1.031372978788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.193942447616" Y="-3.528774576293" />
                  <Point X="27.87539048136" Y="-1.572495134805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.424090026589" Y="-1.039235116693" />
                  <Point X="29.762892805143" Y="-0.922575964578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.266344500616" Y="-3.403370585474" />
                  <Point X="27.859342575837" Y="-1.477546907099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.213001377535" Y="-1.011444802702" />
                  <Point X="29.778870807036" Y="-0.816600332608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.338746553616" Y="-3.277966594654" />
                  <Point X="27.868035530254" Y="-1.37407971814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.001912728481" Y="-0.98365448871" />
                  <Point X="29.682484991036" Y="-0.749314665874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.411148606616" Y="-3.152562603835" />
                  <Point X="27.880838944195" Y="-1.269197184463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.790824079427" Y="-0.955864174719" />
                  <Point X="29.518386080167" Y="-0.705344487484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.483550659616" Y="-3.027158613015" />
                  <Point X="27.945075403999" Y="-1.146604832859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.579735605308" Y="-0.928073800493" />
                  <Point X="29.354287169299" Y="-0.661374309094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.555952709106" Y="-2.901754623404" />
                  <Point X="28.073803123485" Y="-1.001806359732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.295293014824" Y="-0.925541274079" />
                  <Point X="29.190188258431" Y="-0.617404130704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.628354117171" Y="-2.776350854654" />
                  <Point X="29.026089347563" Y="-0.573433952314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.682066871066" Y="-2.657382105589" />
                  <Point X="28.861990436695" Y="-0.529463773924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.684753841439" Y="-2.555982942781" />
                  <Point X="28.697891525827" Y="-0.485493595533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.1747445065" Y="-2.975446888413" />
                  <Point X="21.435807682318" Y="-2.885555628165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.646115430306" Y="-2.468813249955" />
                  <Point X="28.576695320279" Y="-0.426750831017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.11414945948" Y="-2.895837471617" />
                  <Point X="21.866984915484" Y="-2.636615435852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.57255160363" Y="-2.393669342106" />
                  <Point X="28.483498037501" Y="-0.358367264248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.053554412461" Y="-2.816228054822" />
                  <Point X="22.29816214865" Y="-2.387675243538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.406282993821" Y="-2.350446250974" />
                  <Point X="28.420370980187" Y="-0.279629688514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.99350517649" Y="-2.736430700211" />
                  <Point X="28.376438388985" Y="-0.194282928076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.943794263145" Y="-2.653073575645" />
                  <Point X="28.356518166689" Y="-0.100668045962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.894083332507" Y="-2.569716457033" />
                  <Point X="28.350124140513" Y="-0.002395721022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.844372401868" Y="-2.486359338422" />
                  <Point X="28.369496562418" Y="0.104748703489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.933770303991" Y="-2.355103207438" />
                  <Point X="28.426087755872" Y="0.224708578777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.171298029298" Y="-2.17284188798" />
                  <Point X="28.63446856421" Y="0.39693380988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.752375492401" Y="0.437532421054" />
                  <Point X="29.772229244173" Y="0.788696229307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.408825754604" Y="-1.990580568523" />
                  <Point X="29.757382055193" Y="0.884057896873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.646353479911" Y="-1.808319249065" />
                  <Point X="29.737255946249" Y="0.977601886529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.883879686524" Y="-1.626058452535" />
                  <Point X="29.714687902387" Y="1.070305050562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.025888416813" Y="-1.476686960656" />
                  <Point X="29.441290653111" Y="1.076640792951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.052351238835" Y="-1.367101115595" />
                  <Point X="28.968861317577" Y="1.014444292112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.036681833663" Y="-1.272022559767" />
                  <Point X="28.497228450047" Y="0.952522037199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.986628064752" Y="-1.18878348984" />
                  <Point X="28.288005789019" Y="0.980954862394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.888422219275" Y="-1.122124509511" />
                  <Point X="28.174281364214" Y="1.042270367341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.60127495638" Y="-1.120523276494" />
                  <Point X="28.10046901058" Y="1.117328700495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.128845567619" Y="-1.182719795661" />
                  <Point X="28.04876523476" Y="1.199999627482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.656416178857" Y="-1.244916314828" />
                  <Point X="28.0176584662" Y="1.289762672819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.331337656706" Y="-1.25637586178" />
                  <Point X="28.001313772739" Y="1.384608708243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.307747870511" Y="-1.164024511846" />
                  <Point X="28.015392456299" Y="1.489930352464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.284158084316" Y="-1.071673161911" />
                  <Point X="28.059139764362" Y="1.60546772335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.260568298122" Y="-0.979321811977" />
                  <Point X="28.176052320772" Y="1.746197909575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.246314775236" Y="-0.88375572878" />
                  <Point X="28.413580861659" Y="1.928459509859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.232619461108" Y="-0.787997438895" />
                  <Point X="28.651107670694" Y="2.110720513819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.218924146979" Y="-0.692239149009" />
                  <Point X="20.729865553067" Y="-0.51630791412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.651955723276" Y="-0.198806806574" />
                  <Point X="28.888634479729" Y="2.292981517779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.699912345493" Y="-0.081820052592" />
                  <Point X="29.126161288764" Y="2.475242521739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.69971179351" Y="0.018584856535" />
                  <Point X="27.390254940137" Y="1.977995996535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.785172540959" Y="2.113977031472" />
                  <Point X="29.088839887236" Y="2.562865697339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.669652657025" Y="0.108708630524" />
                  <Point X="27.231774091533" Y="2.023900628896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.216349798633" Y="2.362917232225" />
                  <Point X="29.03770298943" Y="2.64573181608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.604824631414" Y="0.186860515904" />
                  <Point X="27.13893800226" Y="2.092408564562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.647527056307" Y="2.611857432977" />
                  <Point X="28.979762620034" Y="2.726255311685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.493504172595" Y="0.249003772721" />
                  <Point X="27.076819202596" Y="2.171493311246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.329613616284" Y="0.293045693339" />
                  <Point X="27.032360967119" Y="2.256659077846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.165514735885" Y="0.33701588222" />
                  <Point X="27.01560928057" Y="2.351364974311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.001415855486" Y="0.380986071102" />
                  <Point X="27.018323031597" Y="2.452773358438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.837316975087" Y="0.424956259983" />
                  <Point X="27.04972615551" Y="2.564060285857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.673218094688" Y="0.468926448865" />
                  <Point X="27.116693581846" Y="2.687592984648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.509119214289" Y="0.512896637746" />
                  <Point X="27.189094973479" Y="2.812996747741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.345020333891" Y="0.556866826628" />
                  <Point X="27.26149731267" Y="2.938400837104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.217765500052" Y="0.613523438125" />
                  <Point X="27.333899651862" Y="3.063804926467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.233431277087" Y="0.719391562455" />
                  <Point X="27.406301991054" Y="3.18920901583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.249097054122" Y="0.825259686784" />
                  <Point X="27.478704330245" Y="3.314613105193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.264762831156" Y="0.931127811114" />
                  <Point X="21.106167628543" Y="1.220846716808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.534972161265" Y="1.368495958128" />
                  <Point X="27.551106669437" Y="3.440017194557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.291210976915" Y="1.040708602731" />
                  <Point X="20.895079014143" Y="1.248637042732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.574829320653" Y="1.482693843405" />
                  <Point X="27.623509008628" Y="3.56542128392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.321238530438" Y="1.151521883282" />
                  <Point X="20.683990399791" Y="1.276427368672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.570681172624" Y="1.581739486208" />
                  <Point X="27.69591134782" Y="3.690825373283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.351266083961" Y="1.262335163832" />
                  <Point X="20.472901785439" Y="1.304217694613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.533403954964" Y="1.669377875534" />
                  <Point X="27.768313687012" Y="3.816229462646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.476935220887" Y="1.750408095816" />
                  <Point X="27.787635783542" Y="3.923356558741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.389479149454" Y="1.820768520185" />
                  <Point X="27.692441614559" Y="3.991052542449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.2990968976" Y="1.890121379833" />
                  <Point X="27.59278159232" Y="4.057210809564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.208714645745" Y="1.959474239481" />
                  <Point X="27.484387311803" Y="4.120361630372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.118332393891" Y="2.028827099129" />
                  <Point X="27.375993031286" Y="4.18351245118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.027950142037" Y="2.098179958777" />
                  <Point X="27.267599730035" Y="4.246663609176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.937567890182" Y="2.167532818425" />
                  <Point X="27.146291536102" Y="4.305367812999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.847185638328" Y="2.236885678072" />
                  <Point X="27.022553882861" Y="4.363235486898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.785041638166" Y="2.315961747529" />
                  <Point X="21.971716040358" Y="2.724566512188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.081088685265" Y="2.762226533968" />
                  <Point X="26.89881632213" Y="4.421103192649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.858439297509" Y="2.441708553105" />
                  <Point X="21.764350091117" Y="2.753638654521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.223543628764" Y="2.911751669377" />
                  <Point X="26.762731936062" Y="4.474719545501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.931836956852" Y="2.56745535868" />
                  <Point X="21.653250581425" Y="2.815857990224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.251275220481" Y="3.021774386878" />
                  <Point X="26.620625402025" Y="4.526262306516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.008653123467" Y="2.694379250705" />
                  <Point X="21.544238391084" Y="2.878796047617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.244595789047" Y="3.119948438907" />
                  <Point X="26.47851909206" Y="4.577805144685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.115421181583" Y="2.831616406045" />
                  <Point X="21.435226200742" Y="2.94173410501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.220529695524" Y="3.212135783076" />
                  <Point X="24.872685847763" Y="4.125346381048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.233654245501" Y="4.249637767914" />
                  <Point X="26.312150756984" Y="4.620993897654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.222191132716" Y="2.968854213202" />
                  <Point X="21.326214010401" Y="3.004672162403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.173560545039" Y="3.296436972304" />
                  <Point X="24.802374948232" Y="4.201610361537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.264509608651" Y="4.360736086177" />
                  <Point X="26.140622843297" Y="4.662406065235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.125171345125" Y="3.380249199302" />
                  <Point X="24.766800712085" Y="4.289835134423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.294167556183" Y="4.471422101179" />
                  <Point X="25.96909492961" Y="4.703818232815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.076782145212" Y="3.464061426299" />
                  <Point X="24.742153094723" Y="4.381822243876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.323825503715" Y="4.582108116181" />
                  <Point X="25.788339109595" Y="4.742052977434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.028392945298" Y="3.547873653297" />
                  <Point X="24.717505365602" Y="4.473809314847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.353483451248" Y="4.692794131182" />
                  <Point X="25.564593370092" Y="4.76548510568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.980003745385" Y="3.631685880294" />
                  <Point X="23.139097924831" Y="4.030794012681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.42227843305" Y="4.128300881206" />
                  <Point X="24.692857636482" Y="4.565796385819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.984840135459" Y="3.733825147658" />
                  <Point X="23.00169333563" Y="4.083955783139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.50096071426" Y="4.255867328016" />
                  <Point X="24.668209907361" Y="4.657783456791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.222724797384" Y="3.916209370249" />
                  <Point X="22.901646093087" Y="4.14998071961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.533514027072" Y="4.367550297234" />
                  <Point X="24.64356217824" Y="4.749770527762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.569525820521" Y="4.136096503546" />
                  <Point X="22.837984440658" Y="4.228534219484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.533208219628" Y="4.467918963999" />
                  <Point X="24.350283828761" Y="4.749260658369" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.526939453125" Y="-3.743862304688" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.441677734375" Y="-3.488920166016" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.239298828125" Y="-3.255525146484" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.956298828125" Y="-3.198641113281" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285644042969" />
                  <Point X="24.689078125" Y="-3.318267822266" />
                  <Point X="24.547994140625" Y="-3.521544433594" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.19558984375" Y="-4.825352050781" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.8603203125" Y="-4.927919433594" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.684181640625" Y="-4.601693359375" />
                  <Point X="23.6908515625" Y="-4.551039550781" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.681947265625" Y="-4.492675292969" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.581458984375" Y="-4.174338378906" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.3079453125" Y="-3.982956787109" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.9744453125" Y="-3.99762890625" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294921875" />
                  <Point X="22.54647265625" Y="-4.409851074219" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.520599609375" Y="-4.400688964844" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="22.089560546875" Y="-4.125565429687" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.391015625" Y="-2.807437255859" />
                  <Point X="22.493966796875" Y="-2.629119384766" />
                  <Point X="22.500240234375" Y="-2.597598388672" />
                  <Point X="22.483623046875" Y="-2.566364990234" />
                  <Point X="22.468673828125" Y="-2.551415771484" />
                  <Point X="22.43984375" Y="-2.537198730469" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.297236328125" Y="-3.184952880859" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.13569921875" Y="-3.237854736328" />
                  <Point X="20.838302734375" Y="-2.847136962891" />
                  <Point X="20.799150390625" Y="-2.781483886719" />
                  <Point X="20.56898046875" Y="-2.395526367188" />
                  <Point X="21.65608203125" Y="-1.561364990234" />
                  <Point X="21.836212890625" Y="-1.42314465332" />
                  <Point X="21.8474609375" Y="-1.411085693359" />
                  <Point X="21.855244140625" Y="-1.391898803711" />
                  <Point X="21.8618828125" Y="-1.36626550293" />
                  <Point X="21.86334765625" Y="-1.35005065918" />
                  <Point X="21.85134375" Y="-1.321068359375" />
                  <Point X="21.8351796875" Y="-1.308483642578" />
                  <Point X="21.812359375" Y="-1.295052734375" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.377837890625" Y="-1.473231445312" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.188923828125" Y="-1.466563354492" />
                  <Point X="20.072607421875" Y="-1.011187561035" />
                  <Point X="20.06225" Y="-0.93877355957" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="21.23733984375" Y="-0.183627975464" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.46194140625" Y="-0.118761795044" />
                  <Point X="21.485857421875" Y="-0.102163635254" />
                  <Point X="21.497677734375" Y="-0.090645118713" />
                  <Point X="21.5063828125" Y="-0.071784034729" />
                  <Point X="21.514353515625" Y="-0.046099205017" />
                  <Point X="21.51307421875" Y="-0.012338674545" />
                  <Point X="21.505103515625" Y="0.013346157074" />
                  <Point X="21.497677734375" Y="0.028085039139" />
                  <Point X="21.48201953125" Y="0.042267414093" />
                  <Point X="21.458103515625" Y="0.058865581512" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.16396875" Y="0.408676696777" />
                  <Point X="20.001814453125" Y="0.452126098633" />
                  <Point X="20.007390625" Y="0.489816741943" />
                  <Point X="20.08235546875" Y="0.996414855957" />
                  <Point X="20.10320703125" Y="1.073363037109" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="21.09964453125" Y="1.413345092773" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.276791015625" Y="1.398545043945" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056274414" />
                  <Point X="21.360880859375" Y="1.443786376953" />
                  <Point X="21.3642890625" Y="1.45201550293" />
                  <Point X="21.38552734375" Y="1.503290527344" />
                  <Point X="21.389287109375" Y="1.52460546875" />
                  <Point X="21.38368359375" Y="1.54551171875" />
                  <Point X="21.3795703125" Y="1.553412475586" />
                  <Point X="21.353943359375" Y="1.602641479492" />
                  <Point X="21.34003125" Y="1.619221923828" />
                  <Point X="20.606642578125" Y="2.181971679688" />
                  <Point X="20.52389453125" Y="2.245466308594" />
                  <Point X="20.548701171875" Y="2.287966796875" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="20.895216796875" Y="2.857997558594" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.750505859375" Y="2.979100830078" />
                  <Point X="21.84084375" Y="2.926943603516" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.873318359375" Y="2.919399658203" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404541016" />
                  <Point X="21.995146484375" Y="2.935802246094" />
                  <Point X="22.04747265625" Y="2.988127929688" />
                  <Point X="22.0591015625" Y="3.006382324219" />
                  <Point X="22.061927734375" Y="3.027841064453" />
                  <Point X="22.060892578125" Y="3.039672363281" />
                  <Point X="22.054443359375" Y="3.113390625" />
                  <Point X="22.04793359375" Y="3.134032470703" />
                  <Point X="21.7229453125" Y="3.696926757812" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.739798828125" Y="3.785370605469" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.334111328125" Y="4.222659179687" />
                  <Point X="22.858451171875" Y="4.513971679688" />
                  <Point X="23.004517578125" Y="4.323614257813" />
                  <Point X="23.032173828125" Y="4.287572753906" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.061921875" Y="4.266805664062" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.199908203125" Y="4.227931640625" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.294487792969" />
                  <Point X="23.318380859375" Y="4.308645996094" />
                  <Point X="23.346197265625" Y="4.396864257812" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.31113671875" Y="4.6990703125" />
                  <Point X="23.31086328125" Y="4.701141113281" />
                  <Point X="23.375142578125" Y="4.719163085938" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.137357421875" Y="4.915637695312" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.931134765625" Y="4.410642089844" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176269531" />
                  <Point X="25.054453125" Y="4.31090234375" />
                  <Point X="25.22097265625" Y="4.932362792969" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.2867578125" Y="4.98562109375" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="25.947451171875" Y="4.904502441406" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.5649375" Y="4.748571777344" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="26.985947265625" Y="4.59010546875" />
                  <Point X="27.33869921875" Y="4.425134765625" />
                  <Point X="27.39175390625" Y="4.394224609375" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="27.782556640625" Y="4.160111816406" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.349013671875" Y="2.709987304688" />
                  <Point X="27.22985546875" Y="2.50359765625" />
                  <Point X="27.221884765625" Y="2.480412353516" />
                  <Point X="27.2033828125" Y="2.411229248047" />
                  <Point X="27.203203125" Y="2.382725341797" />
                  <Point X="27.210416015625" Y="2.322901611328" />
                  <Point X="27.218681640625" Y="2.300812011719" />
                  <Point X="27.224623046875" Y="2.292056640625" />
                  <Point X="27.261640625" Y="2.237503417969" />
                  <Point X="27.2836953125" Y="2.218262451172" />
                  <Point X="27.33825" Y="2.181245849609" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.369939453125" Y="2.171822509766" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.459767578125" Y="2.168915527344" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.827029296875" Y="2.934886230469" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.000458984375" Y="3.022799316406" />
                  <Point X="29.20259765625" Y="2.741874755859" />
                  <Point X="29.230484375" Y="2.695788085938" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.445501953125" Y="1.713464111328" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.271380859375" Y="1.573408203125" />
                  <Point X="28.22158984375" Y="1.508451782227" />
                  <Point X="28.21014453125" Y="1.480857177734" />
                  <Point X="28.191595703125" Y="1.414536621094" />
                  <Point X="28.19078125" Y="1.390958374023" />
                  <Point X="28.193224609375" Y="1.379123291016" />
                  <Point X="28.20844921875" Y="1.305333007812" />
                  <Point X="28.22229296875" Y="1.277853637695" />
                  <Point X="28.263703125" Y="1.21491003418" />
                  <Point X="28.280947265625" Y="1.19882019043" />
                  <Point X="28.290580078125" Y="1.19339855957" />
                  <Point X="28.35058984375" Y="1.159617675781" />
                  <Point X="28.381587890625" Y="1.15189855957" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.697412109375" Y="1.301999511719" />
                  <Point X="29.848974609375" Y="1.321953125" />
                  <Point X="29.852373046875" Y="1.307994628906" />
                  <Point X="29.93919140625" Y="0.951367431641" />
                  <Point X="29.947978515625" Y="0.894930786133" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="28.9209609375" Y="0.286002075195" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.716294921875" Y="0.225424362183" />
                  <Point X="28.636578125" Y="0.179346984863" />
                  <Point X="28.61458984375" Y="0.157145462036" />
                  <Point X="28.566759765625" Y="0.096199020386" />
                  <Point X="28.556986328125" Y="0.07473526001" />
                  <Point X="28.554427734375" Y="0.06137443161" />
                  <Point X="28.538484375" Y="-0.021875299454" />
                  <Point X="28.54104296875" Y="-0.054045455933" />
                  <Point X="28.556986328125" Y="-0.137295333862" />
                  <Point X="28.566759765625" Y="-0.158759094238" />
                  <Point X="28.574435546875" Y="-0.168540481567" />
                  <Point X="28.622265625" Y="-0.22948677063" />
                  <Point X="28.64937109375" Y="-0.249302047729" />
                  <Point X="28.729087890625" Y="-0.295379425049" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.8614375" Y="-0.60056237793" />
                  <Point X="29.998068359375" Y="-0.637172546387" />
                  <Point X="29.99690625" Y="-0.644884338379" />
                  <Point X="29.948431640625" Y="-0.966412902832" />
                  <Point X="29.937171875" Y="-1.015747009277" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.620490234375" Y="-1.125078857422" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.369728515625" Y="-1.103798828125" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.1854453125" Y="-1.154697387695" />
                  <Point X="28.170267578125" Y="-1.172950805664" />
                  <Point X="28.075701171875" Y="-1.286685424805" />
                  <Point X="28.064359375" Y="-1.314070800781" />
                  <Point X="28.06218359375" Y="-1.337709838867" />
                  <Point X="28.048630859375" Y="-1.485001220703" />
                  <Point X="28.056361328125" Y="-1.516622192383" />
                  <Point X="28.070255859375" Y="-1.538236328125" />
                  <Point X="28.156841796875" Y="-1.672912719727" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="29.208080078125" Y="-2.483267089844" />
                  <Point X="29.33907421875" Y="-2.583783691406" />
                  <Point X="29.204134765625" Y="-2.802138671875" />
                  <Point X="29.18084375" Y="-2.835232421875" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="27.93916015625" Y="-2.366432617188" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.70745703125" Y="-2.247915771484" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.464251953125" Y="-2.232310791016" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.33468359375" />
                  <Point X="27.27553515625" Y="-2.359510009766" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.1891640625" Y="-2.546374511719" />
                  <Point X="27.1945625" Y="-2.576258789062" />
                  <Point X="27.22819140625" Y="-2.76246484375" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.90215234375" Y="-3.935689941406" />
                  <Point X="27.98667578125" Y="-4.082087646484" />
                  <Point X="27.83529296875" Y="-4.190216308594" />
                  <Point X="27.80926171875" Y="-4.207065917969" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="26.824888671875" Y="-3.176767578125" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.641076171875" Y="-2.961518066406" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.3935703125" Y="-2.838683349609" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.140443359375" Y="-2.889204833984" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045986083984" />
                  <Point X="25.961015625" Y="-3.080226318359" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310719726562" />
                  <Point X="26.10325390625" Y="-4.748768066406" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.9702890625" Y="-4.9676171875" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#130" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.026724810296" Y="4.45486282851" Z="0.35" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.35" />
                  <Point X="-0.874611309453" Y="4.996579006201" Z="0.35" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.35" />
                  <Point X="-1.64451149723" Y="4.79858653125" Z="0.35" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.35" />
                  <Point X="-1.744593273738" Y="4.723824062682" Z="0.35" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.35" />
                  <Point X="-1.735461077846" Y="4.35496228114" Z="0.35" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.35" />
                  <Point X="-1.825383767052" Y="4.305405682645" Z="0.35" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.35" />
                  <Point X="-1.92114726002" Y="4.342436477748" Z="0.35" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.35" />
                  <Point X="-1.961970738725" Y="4.385332735032" Z="0.35" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.35" />
                  <Point X="-2.696329089521" Y="4.297646561323" Z="0.35" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.35" />
                  <Point X="-3.296779332648" Y="3.85633054839" Z="0.35" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.35" />
                  <Point X="-3.326511948198" Y="3.703207326067" Z="0.35" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.35" />
                  <Point X="-2.995075107491" Y="3.066594627268" Z="0.35" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.35" />
                  <Point X="-3.046365776264" Y="3.002437791622" Z="0.35" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.35" />
                  <Point X="-3.128481838338" Y="3.000489591682" Z="0.35" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.35" />
                  <Point X="-3.230652022231" Y="3.053682000547" Z="0.35" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.35" />
                  <Point X="-4.150403369361" Y="2.919979938693" Z="0.35" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.35" />
                  <Point X="-4.500636703905" Y="2.344451952853" Z="0.35" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.35" />
                  <Point X="-4.429952166585" Y="2.173583903414" Z="0.35" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.35" />
                  <Point X="-3.670935315214" Y="1.561605154344" Z="0.35" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.35" />
                  <Point X="-3.68806127846" Y="1.502429270054" Z="0.35" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.35" />
                  <Point X="-3.744401122251" Y="1.477510979042" Z="0.35" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.35" />
                  <Point X="-3.899986903585" Y="1.494197416192" Z="0.35" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.35" />
                  <Point X="-4.951210397175" Y="1.117720352367" Z="0.35" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.35" />
                  <Point X="-5.048226730641" Y="0.528415849207" Z="0.35" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.35" />
                  <Point X="-4.855129178227" Y="0.391660418985" Z="0.35" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.35" />
                  <Point X="-3.552645951152" Y="0.032470805903" Z="0.35" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.35" />
                  <Point X="-3.540836127606" Y="0.004122160573" Z="0.35" />
                  <Point X="-3.539556741714" Y="0" Z="0.35" />
                  <Point X="-3.547528473529" Y="-0.025684790488" Z="0.35" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.35" />
                  <Point X="-3.572722643971" Y="-0.046405177231" Z="0.35" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.35" />
                  <Point X="-3.781758610545" Y="-0.104051637771" Z="0.35" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.35" />
                  <Point X="-4.993402472326" Y="-0.91457263431" Z="0.35" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.35" />
                  <Point X="-4.865678069745" Y="-1.44765745822" Z="0.35" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.35" />
                  <Point X="-4.621793955354" Y="-1.49152368237" Z="0.35" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.35" />
                  <Point X="-3.196338721814" Y="-1.320294264643" Z="0.35" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.35" />
                  <Point X="-3.199315306688" Y="-1.348083249892" Z="0.35" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.35" />
                  <Point X="-3.380513139315" Y="-1.490417568134" Z="0.35" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.35" />
                  <Point X="-4.249950864735" Y="-2.775813816832" Z="0.35" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.35" />
                  <Point X="-3.910243386673" Y="-3.236858009084" Z="0.35" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.35" />
                  <Point X="-3.683921037946" Y="-3.196974187582" Z="0.35" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.35" />
                  <Point X="-2.557890190096" Y="-2.570440279036" Z="0.35" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.35" />
                  <Point X="-2.658442825586" Y="-2.751157344546" Z="0.35" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.35" />
                  <Point X="-2.947100586424" Y="-4.133902733659" Z="0.35" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.35" />
                  <Point X="-2.511878857058" Y="-4.411919802538" Z="0.35" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.35" />
                  <Point X="-2.420015771506" Y="-4.409008690214" Z="0.35" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.35" />
                  <Point X="-2.003931515021" Y="-4.007922022062" Z="0.35" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.35" />
                  <Point X="-1.70148156626" Y="-4.001569706729" Z="0.35" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.35" />
                  <Point X="-1.457664855938" Y="-4.180650191783" Z="0.35" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.35" />
                  <Point X="-1.373249194929" Y="-4.47115012252" Z="0.35" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.35" />
                  <Point X="-1.371547204816" Y="-4.563885783409" Z="0.35" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.35" />
                  <Point X="-1.15829536637" Y="-4.945062023096" Z="0.35" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.35" />
                  <Point X="-0.859119843613" Y="-5.005717017592" Z="0.35" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.35" />
                  <Point X="-0.762269578185" Y="-4.807012911453" Z="0.35" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.35" />
                  <Point X="-0.276001847191" Y="-3.315496588454" Z="0.35" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.35" />
                  <Point X="-0.035038364228" Y="-3.215113983659" Z="0.35" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.35" />
                  <Point X="0.218320715133" Y="-3.271998061629" Z="0.35" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.35" />
                  <Point X="0.394444012241" Y="-3.486149167314" Z="0.35" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.35" />
                  <Point X="0.472485282216" Y="-3.725523118363" Z="0.35" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.35" />
                  <Point X="0.973069700759" Y="-4.98553214805" Z="0.35" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.35" />
                  <Point X="1.152292198727" Y="-4.947182842649" Z="0.35" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.35" />
                  <Point X="1.146668507719" Y="-4.710962382299" Z="0.35" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.35" />
                  <Point X="1.003717761855" Y="-3.059566220629" Z="0.35" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.35" />
                  <Point X="1.166252850527" Y="-2.896371407481" Z="0.35" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.35" />
                  <Point X="1.391995665737" Y="-2.857192863956" Z="0.35" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.35" />
                  <Point X="1.607879931681" Y="-2.972296233301" Z="0.35" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.35" />
                  <Point X="1.779064068766" Y="-3.175925523405" Z="0.35" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.35" />
                  <Point X="2.830274619233" Y="-4.217759910361" Z="0.35" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.35" />
                  <Point X="3.020341740237" Y="-4.083808213405" Z="0.35" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.35" />
                  <Point X="2.939295634563" Y="-3.879409814613" Z="0.35" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.35" />
                  <Point X="2.237608194546" Y="-2.536092468884" Z="0.35" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.35" />
                  <Point X="2.313624185834" Y="-2.351516686487" Z="0.35" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.35" />
                  <Point X="2.481381691885" Y="-2.245277124133" Z="0.35" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.35" />
                  <Point X="2.692414334628" Y="-2.265839814944" Z="0.35" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.35" />
                  <Point X="2.908003782494" Y="-2.378453865711" Z="0.35" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.35" />
                  <Point X="4.215574890049" Y="-2.832729789052" Z="0.35" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.35" />
                  <Point X="4.377152344402" Y="-2.57603711776" Z="0.35" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.35" />
                  <Point X="4.232360033794" Y="-2.412319477663" Z="0.35" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.35" />
                  <Point X="3.106158459972" Y="-1.479916740266" Z="0.35" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.35" />
                  <Point X="3.105816217409" Y="-1.311011080133" Z="0.35" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.35" />
                  <Point X="3.202558442043" Y="-1.173637506752" Z="0.35" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.35" />
                  <Point X="3.374190370424" Y="-1.121378075896" Z="0.35" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.35" />
                  <Point X="3.607808601679" Y="-1.143371119" Z="0.35" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.35" />
                  <Point X="4.979761890586" Y="-0.995590790162" Z="0.35" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.35" />
                  <Point X="5.040190823224" Y="-0.621058187234" Z="0.35" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.35" />
                  <Point X="4.868222624575" Y="-0.52098604478" Z="0.35" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.35" />
                  <Point X="3.668236408749" Y="-0.174733191851" Z="0.35" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.35" />
                  <Point X="3.607612940144" Y="-0.106391834823" Z="0.35" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.35" />
                  <Point X="3.583993465527" Y="-0.013360780272" Z="0.35" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.35" />
                  <Point X="3.597377984898" Y="0.083249750967" Z="0.35" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.35" />
                  <Point X="3.647766498257" Y="0.157556903752" Z="0.35" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.35" />
                  <Point X="3.735159005603" Y="0.213415652391" Z="0.35" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.35" />
                  <Point X="3.927745192732" Y="0.268985887989" Z="0.35" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.35" />
                  <Point X="4.99122685215" Y="0.933903055015" Z="0.35" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.35" />
                  <Point X="4.894798568825" Y="1.35110153744" Z="0.35" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.35" />
                  <Point X="4.684729221671" Y="1.38285185157" Z="0.35" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.35" />
                  <Point X="3.381982612291" Y="1.232747564241" Z="0.35" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.35" />
                  <Point X="3.308949721004" Y="1.268249517055" Z="0.35" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.35" />
                  <Point X="3.257907128937" Y="1.336614573825" Z="0.35" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.35" />
                  <Point X="3.236035567458" Y="1.420506593018" Z="0.35" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.35" />
                  <Point X="3.252139324885" Y="1.498669876022" Z="0.35" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.35" />
                  <Point X="3.304907157347" Y="1.57427020939" Z="0.35" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.35" />
                  <Point X="3.469782219302" Y="1.705076470074" Z="0.35" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.35" />
                  <Point X="4.267105376629" Y="2.752954474234" Z="0.35" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.35" />
                  <Point X="4.034888172792" Y="3.083282441325" Z="0.35" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.35" />
                  <Point X="3.795871607025" Y="3.009467526794" Z="0.35" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.35" />
                  <Point X="2.440694866961" Y="2.248498318137" Z="0.35" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.35" />
                  <Point X="2.369767919447" Y="2.252742809346" Z="0.35" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.35" />
                  <Point X="2.305613368714" Y="2.290917246761" Z="0.35" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.35" />
                  <Point X="2.259841353474" Y="2.351411491669" Z="0.35" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.35" />
                  <Point X="2.246686893413" Y="2.419990524386" Z="0.35" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.35" />
                  <Point X="2.264029624521" Y="2.498774708921" Z="0.35" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.35" />
                  <Point X="2.386157826066" Y="2.71626747493" Z="0.35" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.35" />
                  <Point X="2.805376373065" Y="4.232138854149" Z="0.35" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.35" />
                  <Point X="2.410767608807" Y="4.468707427189" Z="0.35" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.35" />
                  <Point X="2.000971758119" Y="4.666677269479" Z="0.35" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.35" />
                  <Point X="1.57583027541" Y="4.826855655796" Z="0.35" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.35" />
                  <Point X="0.953028517914" Y="4.984385758182" Z="0.35" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.35" />
                  <Point X="0.285807541039" Y="5.066629216282" Z="0.35" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.35" />
                  <Point X="0.166519751257" Y="4.976584677441" Z="0.35" />
                  <Point X="0" Y="4.355124473572" Z="0.35" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>