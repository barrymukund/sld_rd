<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#189" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2682" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.847380859375" Y="-4.572713378906" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140625" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.426255859375" Y="-3.300085449219" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209020263672" />
                  <Point X="25.33049609375" Y="-3.189776611328" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.12282421875" Y="-3.119905517578" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.78350390625" Y="-3.152799560547" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.189777587891" />
                  <Point X="24.655560546875" Y="-3.209021972656" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.51756640625" Y="-3.398768554688" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481571533203" />
                  <Point X="24.449009765625" Y="-3.512524658203" />
                  <Point X="24.332505859375" Y="-3.947325195312" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="24.00355859375" Y="-4.861440917969" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.75358203125" Y="-4.802362304688" />
                  <Point X="23.7850390625" Y="-4.563437011719" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.740568359375" Y="-4.300431152344" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182963867188" />
                  <Point X="23.69598828125" Y="-4.155126953125" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.5109375" Y="-3.986134765625" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.137423828125" Y="-3.876576171875" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.77440234375" Y="-4.017038330078" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.664900390625" Y="-4.0994609375" />
                  <Point X="22.599490234375" Y="-4.184702636719" />
                  <Point X="22.519853515625" Y="-4.288489257813" />
                  <Point X="22.319796875" Y="-4.164620605469" />
                  <Point X="22.19828515625" Y="-4.0893828125" />
                  <Point X="21.913775390625" Y="-3.870320068359" />
                  <Point X="21.89527734375" Y="-3.856077636719" />
                  <Point X="22.048314453125" Y="-3.591011962891" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647655761719" />
                  <Point X="22.593412109375" Y="-2.616131347656" />
                  <Point X="22.59442578125" Y="-2.585198730469" />
                  <Point X="22.585443359375" Y="-2.55558203125" />
                  <Point X="22.571228515625" Y="-2.526753662109" />
                  <Point X="22.553201171875" Y="-2.501593505859" />
                  <Point X="22.535853515625" Y="-2.484244873047" />
                  <Point X="22.510697265625" Y="-2.466217529297" />
                  <Point X="22.4818671875" Y="-2.451998291016" />
                  <Point X="22.45225" Y="-2.443012207031" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.985810546875" Y="-2.677707763672" />
                  <Point X="21.181978515625" Y="-3.141800537109" />
                  <Point X="21.01313671875" Y="-2.919979248047" />
                  <Point X="20.917134765625" Y="-2.793849365234" />
                  <Point X="20.713166015625" Y="-2.451827392578" />
                  <Point X="20.693857421875" Y="-2.419449951172" />
                  <Point X="20.970349609375" Y="-2.207291015625" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.915419921875" Y="-1.475596191406" />
                  <Point X="21.933384765625" Y="-1.448467163086" />
                  <Point X="21.946142578125" Y="-1.419839111328" />
                  <Point X="21.951607421875" Y="-1.398743408203" />
                  <Point X="21.953849609375" Y="-1.390086914062" />
                  <Point X="21.956654296875" Y="-1.359652709961" />
                  <Point X="21.9544453125" Y="-1.327984985352" />
                  <Point X="21.9474453125" Y="-1.298239624023" />
                  <Point X="21.931361328125" Y="-1.272255981445" />
                  <Point X="21.910529296875" Y="-1.248301025391" />
                  <Point X="21.887029296875" Y="-1.228767578125" />
                  <Point X="21.868248046875" Y="-1.217714233398" />
                  <Point X="21.860546875" Y="-1.213180908203" />
                  <Point X="21.8312890625" Y="-1.201958862305" />
                  <Point X="21.799400390625" Y="-1.195475585938" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="21.294662109375" Y="-1.256709594727" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.20346875" Y="-1.139643188477" />
                  <Point X="20.165921875" Y="-0.992650634766" />
                  <Point X="20.11195703125" Y="-0.615327331543" />
                  <Point X="20.107576171875" Y="-0.584698547363" />
                  <Point X="20.414896484375" Y="-0.502352478027" />
                  <Point X="21.467125" Y="-0.220408432007" />
                  <Point X="21.482505859375" Y="-0.214828155518" />
                  <Point X="21.51226953125" Y="-0.199470916748" />
                  <Point X="21.531951171875" Y="-0.185811157227" />
                  <Point X="21.5400234375" Y="-0.180208877563" />
                  <Point X="21.56248046875" Y="-0.158323898315" />
                  <Point X="21.581724609375" Y="-0.132067810059" />
                  <Point X="21.59583203125" Y="-0.1040677948" />
                  <Point X="21.602392578125" Y="-0.082929924011" />
                  <Point X="21.605083984375" Y="-0.074255256653" />
                  <Point X="21.609353515625" Y="-0.046089191437" />
                  <Point X="21.6093515625" Y="-0.016452730179" />
                  <Point X="21.60508203125" Y="0.011700131416" />
                  <Point X="21.59852734375" Y="0.032821990967" />
                  <Point X="21.595837890625" Y="0.041491268158" />
                  <Point X="21.581728515625" Y="0.069497955322" />
                  <Point X="21.562482421875" Y="0.095759658813" />
                  <Point X="21.5400234375" Y="0.117648887634" />
                  <Point X="21.520341796875" Y="0.131308807373" />
                  <Point X="21.51146875" Y="0.136771286011" />
                  <Point X="21.487830078125" Y="0.149592712402" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="21.03558984375" Y="0.273478088379" />
                  <Point X="20.10818359375" Y="0.521975463867" />
                  <Point X="20.15131640625" Y="0.813459106445" />
                  <Point X="20.17551171875" Y="0.976968261719" />
                  <Point X="20.284146484375" Y="1.377862426758" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.4800703125" Y="1.39909375" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228149414" />
                  <Point X="21.29686328125" Y="1.305263427734" />
                  <Point X="21.340423828125" Y="1.318998046875" />
                  <Point X="21.3582890625" Y="1.324630981445" />
                  <Point X="21.377224609375" Y="1.332962524414" />
                  <Point X="21.39596875" Y="1.343784790039" />
                  <Point X="21.412646484375" Y="1.356014526367" />
                  <Point X="21.426283203125" Y="1.371563842773" />
                  <Point X="21.43869921875" Y="1.389294311523" />
                  <Point X="21.44865234375" Y="1.407432983398" />
                  <Point X="21.466130859375" Y="1.449631103516" />
                  <Point X="21.473298828125" Y="1.466937011719" />
                  <Point X="21.479087890625" Y="1.486805908203" />
                  <Point X="21.48284375" Y="1.508121948242" />
                  <Point X="21.484193359375" Y="1.528755737305" />
                  <Point X="21.481048828125" Y="1.549193115234" />
                  <Point X="21.475447265625" Y="1.570099975586" />
                  <Point X="21.46794921875" Y="1.58937890625" />
                  <Point X="21.446859375" Y="1.629893066406" />
                  <Point X="21.438208984375" Y="1.646508666992" />
                  <Point X="21.42671484375" Y="1.663711669922" />
                  <Point X="21.412802734375" Y="1.680289916992" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="21.150333984375" Y="1.884526855469" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.82483203125" Y="2.572586669922" />
                  <Point X="20.9188515625" Y="2.733665039062" />
                  <Point X="21.206609375" Y="3.103537841797" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.330095703125" Y="3.112127441406" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.913875" Y="2.820488525391" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.00098828125" Y="2.826505126953" />
                  <Point X="22.019541015625" Y="2.835655029297" />
                  <Point X="22.037794921875" Y="2.847284912109" />
                  <Point X="22.053921875" Y="2.860228759766" />
                  <Point X="22.096986328125" Y="2.903291503906" />
                  <Point X="22.114646484375" Y="2.920952148438" />
                  <Point X="22.12759375" Y="2.937083984375" />
                  <Point X="22.13922265625" Y="2.955337158203" />
                  <Point X="22.14837109375" Y="2.97388671875" />
                  <Point X="22.1532890625" Y="2.993976318359" />
                  <Point X="22.156115234375" Y="3.01543359375" />
                  <Point X="22.15656640625" Y="3.036120117188" />
                  <Point X="22.1512578125" Y="3.096788574219" />
                  <Point X="22.14908203125" Y="3.121669677734" />
                  <Point X="22.145044921875" Y="3.141958251953" />
                  <Point X="22.13853515625" Y="3.162602539062" />
                  <Point X="22.130205078125" Y="3.181533447266" />
                  <Point X="22.020515625" Y="3.371518798828" />
                  <Point X="21.81666796875" Y="3.724595703125" />
                  <Point X="22.135634765625" Y="3.969144287109" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.75258984375" Y="4.34648046875" />
                  <Point X="22.83296484375" Y="4.391134277344" />
                  <Point X="22.9568046875" Y="4.2297421875" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.07241015625" Y="4.154244140625" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.16073046875" Y="4.123583007812" />
                  <Point X="23.181369140625" Y="4.124935058594" />
                  <Point X="23.202685546875" Y="4.128692871094" />
                  <Point X="23.222548828125" Y="4.134481933594" />
                  <Point X="23.29287890625" Y="4.163614257812" />
                  <Point X="23.32172265625" Y="4.175561523437" />
                  <Point X="23.339859375" Y="4.185512207031" />
                  <Point X="23.357587890625" Y="4.197926757812" />
                  <Point X="23.37313671875" Y="4.211563476563" />
                  <Point X="23.3853671875" Y="4.2282421875" />
                  <Point X="23.396189453125" Y="4.246985839844" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.427412109375" Y="4.338522949219" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410145996094" />
                  <Point X="23.442271484375" Y="4.430827148438" />
                  <Point X="23.42980078125" Y="4.525548828125" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.838392578125" Y="4.750378417969" />
                  <Point X="24.050369140625" Y="4.80980859375" />
                  <Point X="24.599796875" Y="4.874110839844" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.7290546875" Y="4.79776171875" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.202419921875" Y="4.496067871094" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.658951171875" Y="4.851123046875" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.29860546875" Y="4.721993652344" />
                  <Point X="26.4810234375" Y="4.677952148438" />
                  <Point X="26.77647265625" Y="4.570790039062" />
                  <Point X="26.894650390625" Y="4.527926269531" />
                  <Point X="27.18074609375" Y="4.39412890625" />
                  <Point X="27.294572265625" Y="4.340895996094" />
                  <Point X="27.570986328125" Y="4.179857421875" />
                  <Point X="27.680982421875" Y="4.1157734375" />
                  <Point X="27.9416484375" Y="3.930401611328" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.7586171875" Y="3.609438476562" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142078125" Y="2.53993359375" />
                  <Point X="27.133078125" Y="2.516058349609" />
                  <Point X="27.1178515625" Y="2.459122314453" />
                  <Point X="27.111607421875" Y="2.435771972656" />
                  <Point X="27.108619140625" Y="2.417936523438" />
                  <Point X="27.107728515625" Y="2.380954101562" />
                  <Point X="27.113666015625" Y="2.331720703125" />
                  <Point X="27.116099609375" Y="2.311529296875" />
                  <Point X="27.12144140625" Y="2.289606933594" />
                  <Point X="27.129708984375" Y="2.267514892578" />
                  <Point X="27.140072265625" Y="2.247469482422" />
                  <Point X="27.170537109375" Y="2.202573486328" />
                  <Point X="27.18303125" Y="2.184160888672" />
                  <Point X="27.19446875" Y="2.170327392578" />
                  <Point X="27.2216015625" Y="2.145592285156" />
                  <Point X="27.26649609375" Y="2.115128417969" />
                  <Point X="27.28491015625" Y="2.102634765625" />
                  <Point X="27.30495703125" Y="2.092270996094" />
                  <Point X="27.32704296875" Y="2.084006103516" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.398197265625" Y="2.072726806641" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845458984" />
                  <Point X="27.47320703125" Y="2.074170898438" />
                  <Point X="27.530142578125" Y="2.089396484375" />
                  <Point X="27.553494140625" Y="2.095640625" />
                  <Point X="27.565283203125" Y="2.099637939453" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.022578125" Y="2.360739501953" />
                  <Point X="28.967326171875" Y="2.906190185547" />
                  <Point X="29.058025390625" Y="2.780138183594" />
                  <Point X="29.123279296875" Y="2.689451904297" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="29.035267578125" Y="2.285752441406" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.22142578125" Y="1.660242553711" />
                  <Point X="28.20397265625" Y="1.641626831055" />
                  <Point X="28.16299609375" Y="1.588169311523" />
                  <Point X="28.14619140625" Y="1.566245483398" />
                  <Point X="28.136609375" Y="1.550916137695" />
                  <Point X="28.121630859375" Y="1.517089111328" />
                  <Point X="28.1063671875" Y="1.462508789062" />
                  <Point X="28.10010546875" Y="1.440124633789" />
                  <Point X="28.09665234375" Y="1.417827148438" />
                  <Point X="28.0958359375" Y="1.39425378418" />
                  <Point X="28.097740234375" Y="1.371766845703" />
                  <Point X="28.110271484375" Y="1.311039306641" />
                  <Point X="28.11541015625" Y="1.286133911133" />
                  <Point X="28.1206796875" Y="1.26898046875" />
                  <Point X="28.13628125" Y="1.235741577148" />
                  <Point X="28.170361328125" Y="1.183940429688" />
                  <Point X="28.184337890625" Y="1.162695922852" />
                  <Point X="28.198892578125" Y="1.145450195312" />
                  <Point X="28.21613671875" Y="1.129360473633" />
                  <Point X="28.23434765625" Y="1.116034790039" />
                  <Point X="28.283736328125" Y="1.088233764648" />
                  <Point X="28.303990234375" Y="1.076832397461" />
                  <Point X="28.320521484375" Y="1.069501586914" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.42289453125" Y="1.05061340332" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.900515625" Y="1.101266357422" />
                  <Point X="29.77683984375" Y="1.21663659668" />
                  <Point X="29.8179140625" Y="1.047916870117" />
                  <Point X="29.845939453125" Y="0.932788513184" />
                  <Point X="29.8908671875" Y="0.644239074707" />
                  <Point X="29.63853515625" Y="0.576626647949" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.704791015625" Y="0.325586517334" />
                  <Point X="28.681546875" Y="0.315068023682" />
                  <Point X="28.615943359375" Y="0.27714730835" />
                  <Point X="28.589037109375" Y="0.26159564209" />
                  <Point X="28.574310546875" Y="0.251094772339" />
                  <Point X="28.54753125" Y="0.225576385498" />
                  <Point X="28.50816796875" Y="0.175419082642" />
                  <Point X="28.492025390625" Y="0.154848754883" />
                  <Point X="28.48030078125" Y="0.135567443848" />
                  <Point X="28.47052734375" Y="0.114104133606" />
                  <Point X="28.463681640625" Y="0.092603782654" />
                  <Point X="28.450560546875" Y="0.024091234207" />
                  <Point X="28.4451796875" Y="-0.004006679058" />
                  <Point X="28.443484375" Y="-0.021875743866" />
                  <Point X="28.4451796875" Y="-0.058553455353" />
                  <Point X="28.45830078125" Y="-0.127065856934" />
                  <Point X="28.463681640625" Y="-0.155163925171" />
                  <Point X="28.47052734375" Y="-0.176664260864" />
                  <Point X="28.48030078125" Y="-0.198127578735" />
                  <Point X="28.492025390625" Y="-0.217408737183" />
                  <Point X="28.531388671875" Y="-0.267566040039" />
                  <Point X="28.54753125" Y="-0.288136535645" />
                  <Point X="28.55999609375" Y="-0.301231323242" />
                  <Point X="28.58903515625" Y="-0.324155303955" />
                  <Point X="28.654640625" Y="-0.362075897217" />
                  <Point X="28.681544921875" Y="-0.377627868652" />
                  <Point X="28.692708984375" Y="-0.383138763428" />
                  <Point X="28.716580078125" Y="-0.392149963379" />
                  <Point X="29.094689453125" Y="-0.493463806152" />
                  <Point X="29.891474609375" Y="-0.706961914062" />
                  <Point X="29.870671875" Y="-0.844945922852" />
                  <Point X="29.855025390625" Y="-0.94872454834" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="29.49358984375" Y="-1.144204833984" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.24590234375" Y="-1.033494995117" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.163974609375" Y="-1.056597167969" />
                  <Point X="28.1361484375" Y="-1.073489379883" />
                  <Point X="28.1123984375" Y="-1.093960083008" />
                  <Point X="28.034572265625" Y="-1.187560913086" />
                  <Point X="28.002654296875" Y="-1.225947998047" />
                  <Point X="27.987935546875" Y="-1.250329589844" />
                  <Point X="27.976591796875" Y="-1.277715209961" />
                  <Point X="27.969759765625" Y="-1.305365722656" />
                  <Point X="27.95860546875" Y="-1.426583007813" />
                  <Point X="27.95403125" Y="-1.476296142578" />
                  <Point X="27.956349609375" Y="-1.507566772461" />
                  <Point X="27.96408203125" Y="-1.539188110352" />
                  <Point X="27.976453125" Y="-1.567996704102" />
                  <Point X="28.047708984375" Y="-1.67883203125" />
                  <Point X="28.07693359375" Y="-1.724287353516" />
                  <Point X="28.086935546875" Y="-1.737238647461" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.461517578125" Y="-2.030154174805" />
                  <Point X="29.213123046875" Y="-2.606882324219" />
                  <Point X="29.168916015625" Y="-2.678415771484" />
                  <Point X="29.12480078125" Y="-2.749798583984" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="28.753228515625" Y="-2.726738769531" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.600984375" Y="-2.132149658203" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.31752734375" Y="-2.202177978516" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246549560547" />
                  <Point X="27.22142578125" Y="-2.267509033203" />
                  <Point X="27.204533203125" Y="-2.290438476562" />
                  <Point X="27.13753125" Y="-2.417745849609" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.100228515625" Y="-2.499735595703" />
                  <Point X="27.095271484375" Y="-2.531909667969" />
                  <Point X="27.09567578125" Y="-2.563259521484" />
                  <Point X="27.1233515625" Y="-2.716502441406" />
                  <Point X="27.134703125" Y="-2.779349853516" />
                  <Point X="27.13898828125" Y="-2.795142333984" />
                  <Point X="27.1518203125" Y="-2.826078369141" />
                  <Point X="27.377298828125" Y="-3.21662109375" />
                  <Point X="27.86128515625" Y="-4.054907226563" />
                  <Point X="27.83419140625" Y="-4.074259277344" />
                  <Point X="27.781865234375" Y="-4.111634277344" />
                  <Point X="27.701765625" Y="-4.163481445312" />
                  <Point X="27.485220703125" Y="-3.881273193359" />
                  <Point X="26.758548828125" Y="-2.934255371094" />
                  <Point X="26.74750390625" Y="-2.922177490234" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.57078515625" Y="-2.803388671875" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479990234375" Y="-2.751167236328" />
                  <Point X="26.448369140625" Y="-2.743435546875" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.2518046875" Y="-2.756327392578" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.156365234375" Y="-2.769396972656" />
                  <Point X="26.12898046875" Y="-2.780739746094" />
                  <Point X="26.10459765625" Y="-2.7954609375" />
                  <Point X="25.9769609375" Y="-2.901587158203" />
                  <Point X="25.92461328125" Y="-2.945111328125" />
                  <Point X="25.904142578125" Y="-2.968860595703" />
                  <Point X="25.88725" Y="-2.996687011719" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.837462890625" Y="-3.201388427734" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627197266" />
                  <Point X="25.8197421875" Y="-3.323120361328" />
                  <Point X="25.883642578125" Y="-3.808483642578" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94157421875" Y="-4.752636230469" />
                  <Point X="23.85875390625" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575837890625" />
                  <Point X="23.879923828125" Y="-4.56809765625" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497689453125" />
                  <Point X="23.8337421875" Y="-4.281897460938" />
                  <Point X="23.81613671875" Y="-4.193397949219" />
                  <Point X="23.811875" Y="-4.17846875" />
                  <Point X="23.80097265625" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135462402344" />
                  <Point X="23.778259765625" Y="-4.107625488281" />
                  <Point X="23.769423828125" Y="-4.094859863281" />
                  <Point X="23.749791015625" Y="-4.070937011719" />
                  <Point X="23.738994140625" Y="-4.059779785156" />
                  <Point X="23.573576171875" Y="-3.914710449219" />
                  <Point X="23.505734375" Y="-3.855215332031" />
                  <Point X="23.493263671875" Y="-3.845965820312" />
                  <Point X="23.46698046875" Y="-3.829621582031" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.14363671875" Y="-3.781779541016" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812255859" />
                  <Point X="22.721623046875" Y="-3.938048828125" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.61955859375" Y="-4.010133056641" />
                  <Point X="22.597244140625" Y="-4.032770996094" />
                  <Point X="22.589533203125" Y="-4.041627441406" />
                  <Point X="22.524123046875" Y="-4.126869140625" />
                  <Point X="22.496798828125" Y="-4.162479492188" />
                  <Point X="22.369806640625" Y="-4.083849853516" />
                  <Point X="22.252412109375" Y="-4.011161621094" />
                  <Point X="22.01913671875" Y="-3.831547119141" />
                  <Point X="22.1305859375" Y="-3.638512207031" />
                  <Point X="22.658509765625" Y="-2.724119873047" />
                  <Point X="22.6651484375" Y="-2.710086425781" />
                  <Point X="22.67605078125" Y="-2.681122558594" />
                  <Point X="22.680314453125" Y="-2.666191894531" />
                  <Point X="22.6865859375" Y="-2.634667480469" />
                  <Point X="22.688361328125" Y="-2.619242919922" />
                  <Point X="22.689375" Y="-2.588310302734" />
                  <Point X="22.6853359375" Y="-2.557626464844" />
                  <Point X="22.676353515625" Y="-2.528009765625" />
                  <Point X="22.6706484375" Y="-2.513568847656" />
                  <Point X="22.65643359375" Y="-2.484740478516" />
                  <Point X="22.648451171875" Y="-2.471422607422" />
                  <Point X="22.630423828125" Y="-2.446262451172" />
                  <Point X="22.62037890625" Y="-2.434420166016" />
                  <Point X="22.60303125" Y="-2.417071533203" />
                  <Point X="22.591189453125" Y="-2.407025390625" />
                  <Point X="22.566033203125" Y="-2.388998046875" />
                  <Point X="22.55271875" Y="-2.381016845703" />
                  <Point X="22.523888671875" Y="-2.366797607422" />
                  <Point X="22.50944921875" Y="-2.361090576172" />
                  <Point X="22.47983203125" Y="-2.352104492188" />
                  <Point X="22.44914453125" Y="-2.348062988281" />
                  <Point X="22.41820703125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.938310546875" Y="-2.595435302734" />
                  <Point X="21.2069140625" Y="-3.017707519531" />
                  <Point X="21.08873046875" Y="-2.862440673828" />
                  <Point X="20.99597265625" Y="-2.740573242188" />
                  <Point X="20.818734375" Y="-2.443373535156" />
                  <Point X="21.028181640625" Y="-2.282659667969" />
                  <Point X="21.951876953125" Y="-1.573882080078" />
                  <Point X="21.963517578125" Y="-1.563310302734" />
                  <Point X="21.984892578125" Y="-1.540393066406" />
                  <Point X="21.994626953125" Y="-1.528047363281" />
                  <Point X="22.012591796875" Y="-1.500918334961" />
                  <Point X="22.020158203125" Y="-1.487137084961" />
                  <Point X="22.032916015625" Y="-1.458508789062" />
                  <Point X="22.038107421875" Y="-1.443662475586" />
                  <Point X="22.043572265625" Y="-1.422566772461" />
                  <Point X="22.04844921875" Y="-1.39880480957" />
                  <Point X="22.05125390625" Y="-1.368370483398" />
                  <Point X="22.051423828125" Y="-1.353041992188" />
                  <Point X="22.04921484375" Y="-1.321374389648" />
                  <Point X="22.046919921875" Y="-1.306223022461" />
                  <Point X="22.039919921875" Y="-1.276477539062" />
                  <Point X="22.02822265625" Y="-1.248238525391" />
                  <Point X="22.012138671875" Y="-1.222254882812" />
                  <Point X="22.003046875" Y="-1.209916259766" />
                  <Point X="21.98221484375" Y="-1.185961303711" />
                  <Point X="21.971255859375" Y="-1.175243896484" />
                  <Point X="21.947755859375" Y="-1.155710327148" />
                  <Point X="21.93521484375" Y="-1.14689453125" />
                  <Point X="21.91643359375" Y="-1.135841064453" />
                  <Point X="21.894568359375" Y="-1.124481689453" />
                  <Point X="21.865310546875" Y="-1.113259643555" />
                  <Point X="21.850216796875" Y="-1.10886340332" />
                  <Point X="21.818328125" Y="-1.102380126953" />
                  <Point X="21.802708984375" Y="-1.100533203125" />
                  <Point X="21.771380859375" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="21.28226171875" Y="-1.162522338867" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.295513671875" Y="-1.116131713867" />
                  <Point X="20.259236328125" Y="-0.974112609863" />
                  <Point X="20.213548828125" Y="-0.654654724121" />
                  <Point X="20.439484375" Y="-0.594115356445" />
                  <Point X="21.491712890625" Y="-0.312171325684" />
                  <Point X="21.499525390625" Y="-0.309712524414" />
                  <Point X="21.52606640625" Y="-0.299252441406" />
                  <Point X="21.555830078125" Y="-0.283895233154" />
                  <Point X="21.566435546875" Y="-0.277516052246" />
                  <Point X="21.5861171875" Y="-0.263856262207" />
                  <Point X="21.606326171875" Y="-0.248244918823" />
                  <Point X="21.628783203125" Y="-0.226360046387" />
                  <Point X="21.639103515625" Y="-0.214483886719" />
                  <Point X="21.65834765625" Y="-0.188227706909" />
                  <Point X="21.666564453125" Y="-0.174813308716" />
                  <Point X="21.680671875" Y="-0.146813201904" />
                  <Point X="21.6865625" Y="-0.132227798462" />
                  <Point X="21.693123046875" Y="-0.111089904785" />
                  <Point X="21.69901171875" Y="-0.088493125916" />
                  <Point X="21.70328125" Y="-0.060327007294" />
                  <Point X="21.704353515625" Y="-0.046082988739" />
                  <Point X="21.7043515625" Y="-0.016446540833" />
                  <Point X="21.70327734375" Y="-0.002208318472" />
                  <Point X="21.6990078125" Y="0.025944574356" />
                  <Point X="21.695814453125" Y="0.039856571198" />
                  <Point X="21.689259765625" Y="0.060978408813" />
                  <Point X="21.6806796875" Y="0.084233283997" />
                  <Point X="21.6665703125" Y="0.112240081787" />
                  <Point X="21.658353515625" Y="0.125653747559" />
                  <Point X="21.639107421875" Y="0.151915557861" />
                  <Point X="21.6287890625" Y="0.163792160034" />
                  <Point X="21.606330078125" Y="0.185681503296" />
                  <Point X="21.594189453125" Y="0.195693634033" />
                  <Point X="21.5745078125" Y="0.209353561401" />
                  <Point X="21.55676171875" Y="0.220278671265" />
                  <Point X="21.533123046875" Y="0.233100082397" />
                  <Point X="21.523015625" Y="0.237836837769" />
                  <Point X="21.502310546875" Y="0.246092391968" />
                  <Point X="21.491712890625" Y="0.249611190796" />
                  <Point X="21.060177734375" Y="0.365240997314" />
                  <Point X="20.2145546875" Y="0.591824768066" />
                  <Point X="20.24529296875" Y="0.79955279541" />
                  <Point X="20.26866796875" Y="0.957522033691" />
                  <Point X="20.366416015625" Y="1.318236938477" />
                  <Point X="20.467669921875" Y="1.304906494141" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.204703369141" />
                  <Point X="21.28485546875" Y="1.20658972168" />
                  <Point X="21.295109375" Y="1.208053588867" />
                  <Point X="21.315396484375" Y="1.212088745117" />
                  <Point X="21.3254296875" Y="1.21466027832" />
                  <Point X="21.368990234375" Y="1.228394897461" />
                  <Point X="21.396548828125" Y="1.23767590332" />
                  <Point X="21.415484375" Y="1.246007568359" />
                  <Point X="21.4247265625" Y="1.250690795898" />
                  <Point X="21.443470703125" Y="1.261513061523" />
                  <Point X="21.452146484375" Y="1.267174926758" />
                  <Point X="21.46882421875" Y="1.279404907227" />
                  <Point X="21.4840703125" Y="1.293375854492" />
                  <Point X="21.49770703125" Y="1.308925170898" />
                  <Point X="21.504099609375" Y="1.317071044922" />
                  <Point X="21.516515625" Y="1.334801635742" />
                  <Point X="21.521984375" Y="1.34359362793" />
                  <Point X="21.5319375" Y="1.361732299805" />
                  <Point X="21.536421875" Y="1.371078979492" />
                  <Point X="21.553900390625" Y="1.413277099609" />
                  <Point X="21.564505859375" Y="1.440362548828" />
                  <Point X="21.570294921875" Y="1.460231567383" />
                  <Point X="21.572646484375" Y="1.470320922852" />
                  <Point X="21.57640234375" Y="1.491636962891" />
                  <Point X="21.577640625" Y="1.501921630859" />
                  <Point X="21.578990234375" Y="1.522555297852" />
                  <Point X="21.578087890625" Y="1.543202636719" />
                  <Point X="21.574943359375" Y="1.563640014648" />
                  <Point X="21.5728125" Y="1.573779174805" />
                  <Point X="21.5672109375" Y="1.594686035156" />
                  <Point X="21.563986328125" Y="1.60453515625" />
                  <Point X="21.55648828125" Y="1.623814086914" />
                  <Point X="21.55221484375" Y="1.633244384766" />
                  <Point X="21.531125" Y="1.673758544922" />
                  <Point X="21.51719921875" Y="1.699286132812" />
                  <Point X="21.505705078125" Y="1.716489257812" />
                  <Point X="21.499486328125" Y="1.724779785156" />
                  <Point X="21.48557421875" Y="1.741358154297" />
                  <Point X="21.478494140625" Y="1.748917358398" />
                  <Point X="21.4635546875" Y="1.763217529297" />
                  <Point X="21.4556953125" Y="1.769958496094" />
                  <Point X="21.208166015625" Y="1.959895263672" />
                  <Point X="20.77238671875" Y="2.294280517578" />
                  <Point X="20.90687890625" Y="2.524697021484" />
                  <Point X="20.997716796875" Y="2.680322998047" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.282595703125" Y="3.029855224609" />
                  <Point X="21.74584375" Y="2.762398925781" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774015625" Y="2.749385742188" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.905595703125" Y="2.725850097656" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.023568359375" Y="2.734227783203" />
                  <Point X="22.0430078125" Y="2.741303466797" />
                  <Point X="22.061560546875" Y="2.750453369141" />
                  <Point X="22.070587890625" Y="2.755534667969" />
                  <Point X="22.088841796875" Y="2.767164550781" />
                  <Point X="22.097259765625" Y="2.773197265625" />
                  <Point X="22.11338671875" Y="2.786141113281" />
                  <Point X="22.121095703125" Y="2.793052246094" />
                  <Point X="22.16416015625" Y="2.836114990234" />
                  <Point X="22.1818203125" Y="2.853775634766" />
                  <Point X="22.188734375" Y="2.861489257812" />
                  <Point X="22.201681640625" Y="2.87762109375" />
                  <Point X="22.20771484375" Y="2.886039306641" />
                  <Point X="22.21934375" Y="2.904292480469" />
                  <Point X="22.224423828125" Y="2.913316650391" />
                  <Point X="22.233572265625" Y="2.931866210938" />
                  <Point X="22.240646484375" Y="2.951297607422" />
                  <Point X="22.245564453125" Y="2.971387207031" />
                  <Point X="22.2474765625" Y="2.981570800781" />
                  <Point X="22.250302734375" Y="3.003028076172" />
                  <Point X="22.251091796875" Y="3.013362060547" />
                  <Point X="22.25154296875" Y="3.034048583984" />
                  <Point X="22.251205078125" Y="3.044401123047" />
                  <Point X="22.245896484375" Y="3.105069580078" />
                  <Point X="22.243720703125" Y="3.129950683594" />
                  <Point X="22.242255859375" Y="3.140209716797" />
                  <Point X="22.23821875" Y="3.160498291016" />
                  <Point X="22.235646484375" Y="3.170527832031" />
                  <Point X="22.22913671875" Y="3.191172119141" />
                  <Point X="22.225490234375" Y="3.200864501953" />
                  <Point X="22.21716015625" Y="3.219795410156" />
                  <Point X="22.2124765625" Y="3.229033935547" />
                  <Point X="22.102787109375" Y="3.419019287109" />
                  <Point X="21.94061328125" Y="3.699914550781" />
                  <Point X="22.1934375" Y="3.893752441406" />
                  <Point X="22.351634765625" Y="4.015041992188" />
                  <Point X="22.7987265625" Y="4.263436523438" />
                  <Point X="22.807474609375" Y="4.268297363281" />
                  <Point X="22.881435546875" Y="4.17191015625" />
                  <Point X="22.88817578125" Y="4.164052246094" />
                  <Point X="22.902478515625" Y="4.149109863281" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.934908203125" Y="4.121897460938" />
                  <Point X="22.952107421875" Y="4.110405273438" />
                  <Point X="22.96101953125" Y="4.105129394531" />
                  <Point X="23.02854296875" Y="4.069978271484" />
                  <Point X="23.056236328125" Y="4.055562255859" />
                  <Point X="23.065673828125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.0437890625" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834716797" />
                  <Point X="23.146279296875" Y="4.029688720703" />
                  <Point X="23.16694140625" Y="4.028786132812" />
                  <Point X="23.187580078125" Y="4.030138183594" />
                  <Point X="23.197861328125" Y="4.031377685547" />
                  <Point X="23.219177734375" Y="4.035135498047" />
                  <Point X="23.229267578125" Y="4.037487548828" />
                  <Point X="23.249130859375" Y="4.043276611328" />
                  <Point X="23.258904296875" Y="4.046713623047" />
                  <Point X="23.329234375" Y="4.075845947266" />
                  <Point X="23.358078125" Y="4.087793212891" />
                  <Point X="23.36741796875" Y="4.0922734375" />
                  <Point X="23.3855546875" Y="4.102224121094" />
                  <Point X="23.3943515625" Y="4.107694824219" />
                  <Point X="23.412080078125" Y="4.120109375" />
                  <Point X="23.420228515625" Y="4.12650390625" />
                  <Point X="23.43577734375" Y="4.140140625" />
                  <Point X="23.44974609375" Y="4.155385742188" />
                  <Point X="23.4619765625" Y="4.172064453125" />
                  <Point X="23.467638671875" Y="4.180740234375" />
                  <Point X="23.4784609375" Y="4.199483886719" />
                  <Point X="23.48314453125" Y="4.208724121094" />
                  <Point X="23.4914765625" Y="4.227659667969" />
                  <Point X="23.495125" Y="4.237354980469" />
                  <Point X="23.518015625" Y="4.309956542969" />
                  <Point X="23.527404296875" Y="4.339731445312" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370049316406" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401865234375" />
                  <Point X="23.53769921875" Y="4.412217773438" />
                  <Point X="23.537248046875" Y="4.432898925781" />
                  <Point X="23.536458984375" Y="4.443227539062" />
                  <Point X="23.52398828125" Y="4.53794921875" />
                  <Point X="23.520736328125" Y="4.562654785156" />
                  <Point X="23.8640390625" Y="4.658905273438" />
                  <Point X="24.068830078125" Y="4.716320800781" />
                  <Point X="24.61083984375" Y="4.779754882812" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.637291015625" Y="4.773174316406" />
                  <Point X="24.77433203125" Y="4.261728027344" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.29418359375" Y="4.471479980469" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.649056640625" Y="4.756639648438" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.276310546875" Y="4.629646972656" />
                  <Point X="26.453591796875" Y="4.586845214844" />
                  <Point X="26.744080078125" Y="4.481482910156" />
                  <Point X="26.858265625" Y="4.440066894531" />
                  <Point X="27.140501953125" Y="4.308074707031" />
                  <Point X="27.2504453125" Y="4.256657226562" />
                  <Point X="27.5231640625" Y="4.097771972656" />
                  <Point X="27.629435546875" Y="4.035858398438" />
                  <Point X="27.81778125" Y="3.901916503906" />
                  <Point X="27.67634375" Y="3.656938232422" />
                  <Point X="27.065310546875" Y="2.59859765625" />
                  <Point X="27.062380859375" Y="2.593116699219" />
                  <Point X="27.05318359375" Y="2.573442871094" />
                  <Point X="27.04418359375" Y="2.549567626953" />
                  <Point X="27.041302734375" Y="2.540602050781" />
                  <Point X="27.026076171875" Y="2.483666015625" />
                  <Point X="27.01983203125" Y="2.460315673828" />
                  <Point X="27.0179140625" Y="2.451470214844" />
                  <Point X="27.013646484375" Y="2.420223632812" />
                  <Point X="27.012755859375" Y="2.383241210938" />
                  <Point X="27.013412109375" Y="2.369579589844" />
                  <Point X="27.019349609375" Y="2.320346191406" />
                  <Point X="27.021783203125" Y="2.300154785156" />
                  <Point X="27.02380078125" Y="2.289038818359" />
                  <Point X="27.029142578125" Y="2.267116455078" />
                  <Point X="27.032466796875" Y="2.256310058594" />
                  <Point X="27.040734375" Y="2.234218017578" />
                  <Point X="27.0453203125" Y="2.223886474609" />
                  <Point X="27.05568359375" Y="2.203841064453" />
                  <Point X="27.0614609375" Y="2.194127197266" />
                  <Point X="27.09192578125" Y="2.149231201172" />
                  <Point X="27.104419921875" Y="2.130818603516" />
                  <Point X="27.10981640625" Y="2.123626220703" />
                  <Point X="27.130466796875" Y="2.100121826172" />
                  <Point X="27.157599609375" Y="2.07538671875" />
                  <Point X="27.168259765625" Y="2.066981933594" />
                  <Point X="27.213154296875" Y="2.036518066406" />
                  <Point X="27.231568359375" Y="2.024024291992" />
                  <Point X="27.241283203125" Y="2.018244995117" />
                  <Point X="27.261330078125" Y="2.007881225586" />
                  <Point X="27.271662109375" Y="2.00329675293" />
                  <Point X="27.293748046875" Y="1.995031860352" />
                  <Point X="27.304548828125" Y="1.991707397461" />
                  <Point X="27.326470703125" Y="1.986365112305" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.38682421875" Y="1.97841003418" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.41604296875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975496948242" />
                  <Point X="27.484314453125" Y="1.979822509766" />
                  <Point X="27.49775" Y="1.982395751953" />
                  <Point X="27.554685546875" Y="1.997621337891" />
                  <Point X="27.578037109375" Y="2.003865356445" />
                  <Point X="27.584" Y="2.00567175293" />
                  <Point X="27.60440234375" Y="2.01306640625" />
                  <Point X="27.627654296875" Y="2.023573486328" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.070078125" Y="2.278467041016" />
                  <Point X="28.940404296875" Y="2.780949951172" />
                  <Point X="28.980912109375" Y="2.724652587891" />
                  <Point X="29.043962890625" Y="2.637028320312" />
                  <Point X="29.136884765625" Y="2.483471435547" />
                  <Point X="28.977435546875" Y="2.361120849609" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.168142578125" Y="1.739872802734" />
                  <Point X="28.15212109375" Y="1.72521875" />
                  <Point X="28.13466796875" Y="1.706602905273" />
                  <Point X="28.12857421875" Y="1.699421142578" />
                  <Point X="28.08759765625" Y="1.645963623047" />
                  <Point X="28.07079296875" Y="1.624039794922" />
                  <Point X="28.065634765625" Y="1.616599853516" />
                  <Point X="28.049744140625" Y="1.589379760742" />
                  <Point X="28.034765625" Y="1.555552734375" />
                  <Point X="28.030140625" Y="1.542674682617" />
                  <Point X="28.014876953125" Y="1.488094238281" />
                  <Point X="28.008615234375" Y="1.465710205078" />
                  <Point X="28.006224609375" Y="1.454663452148" />
                  <Point X="28.002771484375" Y="1.432366088867" />
                  <Point X="28.001708984375" Y="1.421115356445" />
                  <Point X="28.000892578125" Y="1.397541992188" />
                  <Point X="28.001173828125" Y="1.386237426758" />
                  <Point X="28.003078125" Y="1.363750488281" />
                  <Point X="28.004701171875" Y="1.352567871094" />
                  <Point X="28.017232421875" Y="1.291840332031" />
                  <Point X="28.02237109375" Y="1.266934936523" />
                  <Point X="28.02459765625" Y="1.258236694336" />
                  <Point X="28.034681640625" Y="1.228615112305" />
                  <Point X="28.050283203125" Y="1.195376342773" />
                  <Point X="28.056916015625" Y="1.183527587891" />
                  <Point X="28.09099609375" Y="1.13172644043" />
                  <Point X="28.10497265625" Y="1.110482055664" />
                  <Point X="28.11173828125" Y="1.101424316406" />
                  <Point X="28.12629296875" Y="1.084178588867" />
                  <Point X="28.13408203125" Y="1.075990234375" />
                  <Point X="28.151326171875" Y="1.059900512695" />
                  <Point X="28.160037109375" Y="1.052693847656" />
                  <Point X="28.178248046875" Y="1.039368164063" />
                  <Point X="28.187748046875" Y="1.033249389648" />
                  <Point X="28.23713671875" Y="1.005448425293" />
                  <Point X="28.257390625" Y="0.994046936035" />
                  <Point X="28.265478515625" Y="0.989988342285" />
                  <Point X="28.2946796875" Y="0.978083984375" />
                  <Point X="28.33027734375" Y="0.968021179199" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.410447265625" Y="0.956432434082" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="28.912916015625" Y="1.007079101562" />
                  <Point X="29.704703125" Y="1.111319946289" />
                  <Point X="29.725609375" Y="1.025445800781" />
                  <Point X="29.7526875" Y="0.914207885742" />
                  <Point X="29.783873046875" Y="0.713921203613" />
                  <Point X="29.613947265625" Y="0.668389648438" />
                  <Point X="28.6919921875" Y="0.42135269165" />
                  <Point X="28.686033203125" Y="0.419544830322" />
                  <Point X="28.665625" Y="0.412137176514" />
                  <Point X="28.642380859375" Y="0.401618682861" />
                  <Point X="28.634005859375" Y="0.397316192627" />
                  <Point X="28.56840234375" Y="0.359395507812" />
                  <Point X="28.54149609375" Y="0.343843902588" />
                  <Point X="28.5338828125" Y="0.338945281982" />
                  <Point X="28.5087734375" Y="0.319869537354" />
                  <Point X="28.481994140625" Y="0.294351135254" />
                  <Point X="28.472796875" Y="0.284226959229" />
                  <Point X="28.43343359375" Y="0.23406968689" />
                  <Point X="28.417291015625" Y="0.213499389648" />
                  <Point X="28.410853515625" Y="0.204207336426" />
                  <Point X="28.39912890625" Y="0.184926040649" />
                  <Point X="28.393841796875" Y="0.174936798096" />
                  <Point X="28.384068359375" Y="0.153473434448" />
                  <Point X="28.380005859375" Y="0.14292640686" />
                  <Point X="28.37316015625" Y="0.121426017761" />
                  <Point X="28.370376953125" Y="0.110472808838" />
                  <Point X="28.357255859375" Y="0.041960327148" />
                  <Point X="28.351875" Y="0.013862423897" />
                  <Point X="28.350603515625" Y="0.004966006279" />
                  <Point X="28.3485859375" Y="-0.026262191772" />
                  <Point X="28.35028125" Y="-0.062939796448" />
                  <Point X="28.351875" Y="-0.076422569275" />
                  <Point X="28.36499609375" Y="-0.144934906006" />
                  <Point X="28.370376953125" Y="-0.173032958984" />
                  <Point X="28.37316015625" Y="-0.183986160278" />
                  <Point X="28.380005859375" Y="-0.205486541748" />
                  <Point X="28.384068359375" Y="-0.216033569336" />
                  <Point X="28.393841796875" Y="-0.237496948242" />
                  <Point X="28.399130859375" Y="-0.247486495972" />
                  <Point X="28.41085546875" Y="-0.26676763916" />
                  <Point X="28.417291015625" Y="-0.276059387207" />
                  <Point X="28.456654296875" Y="-0.326216644287" />
                  <Point X="28.472796875" Y="-0.346787109375" />
                  <Point X="28.478720703125" Y="-0.353636230469" />
                  <Point X="28.5011328125" Y="-0.375797241211" />
                  <Point X="28.530171875" Y="-0.398721130371" />
                  <Point X="28.541494140625" Y="-0.406404205322" />
                  <Point X="28.607099609375" Y="-0.444324890137" />
                  <Point X="28.63400390625" Y="-0.459876800537" />
                  <Point X="28.639494140625" Y="-0.462814483643" />
                  <Point X="28.659158203125" Y="-0.472016906738" />
                  <Point X="28.683029296875" Y="-0.481028198242" />
                  <Point X="28.6919921875" Y="-0.483912963867" />
                  <Point X="29.0701015625" Y="-0.58522668457" />
                  <Point X="29.784880859375" Y="-0.776751220703" />
                  <Point X="29.776734375" Y="-0.830783508301" />
                  <Point X="29.761619140625" Y="-0.931035949707" />
                  <Point X="29.7278046875" Y="-1.079219848633" />
                  <Point X="29.505990234375" Y="-1.050017578125" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042480469" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.225724609375" Y="-0.940662536621" />
                  <Point X="28.17291796875" Y="-0.952140075684" />
                  <Point X="28.157875" Y="-0.956742492676" />
                  <Point X="28.12875390625" Y="-0.968367004395" />
                  <Point X="28.11467578125" Y="-0.975389465332" />
                  <Point X="28.086849609375" Y="-0.992281616211" />
                  <Point X="28.074125" Y="-1.001530273438" />
                  <Point X="28.050375" Y="-1.022000976562" />
                  <Point X="28.039349609375" Y="-1.033223022461" />
                  <Point X="27.9615234375" Y="-1.126823852539" />
                  <Point X="27.92960546875" Y="-1.1652109375" />
                  <Point X="27.92132421875" Y="-1.176850952148" />
                  <Point X="27.90660546875" Y="-1.201232543945" />
                  <Point X="27.90016796875" Y="-1.213973876953" />
                  <Point X="27.88882421875" Y="-1.241359619141" />
                  <Point X="27.884365234375" Y="-1.254927368164" />
                  <Point X="27.877533203125" Y="-1.282577758789" />
                  <Point X="27.87516015625" Y="-1.296660522461" />
                  <Point X="27.864005859375" Y="-1.417877807617" />
                  <Point X="27.859431640625" Y="-1.467591064453" />
                  <Point X="27.859291015625" Y="-1.483320068359" />
                  <Point X="27.861609375" Y="-1.514590698242" />
                  <Point X="27.864068359375" Y="-1.530132446289" />
                  <Point X="27.87180078125" Y="-1.56175378418" />
                  <Point X="27.876791015625" Y="-1.576673461914" />
                  <Point X="27.889162109375" Y="-1.605481933594" />
                  <Point X="27.89654296875" Y="-1.61937097168" />
                  <Point X="27.967798828125" Y="-1.730206420898" />
                  <Point X="27.9970234375" Y="-1.775661621094" />
                  <Point X="28.001744140625" Y="-1.782353515625" />
                  <Point X="28.019794921875" Y="-1.804448974609" />
                  <Point X="28.043490234375" Y="-1.828119628906" />
                  <Point X="28.052798828125" Y="-1.836277587891" />
                  <Point X="28.403685546875" Y="-2.105522705078" />
                  <Point X="29.087171875" Y="-2.629981201172" />
                  <Point X="29.0454921875" Y="-2.697422119141" />
                  <Point X="29.0012734375" Y="-2.760251464844" />
                  <Point X="28.800728515625" Y="-2.644466308594" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.617869140625" Y="-2.038662109375" />
                  <Point X="27.555021484375" Y="-2.027311889648" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.03513659668" />
                  <Point X="27.415068359375" Y="-2.044960571289" />
                  <Point X="27.40058984375" Y="-2.051109130859" />
                  <Point X="27.273283203125" Y="-2.118110107422" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.208970703125" Y="-2.153170166016" />
                  <Point X="27.1860390625" Y="-2.170063720703" />
                  <Point X="27.175208984375" Y="-2.179375244141" />
                  <Point X="27.15425" Y="-2.200334716797" />
                  <Point X="27.14494140625" Y="-2.211161132813" />
                  <Point X="27.128048828125" Y="-2.234090576172" />
                  <Point X="27.12046484375" Y="-2.246193603516" />
                  <Point X="27.053462890625" Y="-2.373500976562" />
                  <Point X="27.025984375" Y="-2.425711669922" />
                  <Point X="27.0198359375" Y="-2.440193603516" />
                  <Point X="27.01001171875" Y="-2.46997265625" />
                  <Point X="27.0063359375" Y="-2.485269775391" />
                  <Point X="27.00137890625" Y="-2.517443847656" />
                  <Point X="27.000279296875" Y="-2.533134765625" />
                  <Point X="27.00068359375" Y="-2.564484619141" />
                  <Point X="27.0021875" Y="-2.580143554688" />
                  <Point X="27.02986328125" Y="-2.733386474609" />
                  <Point X="27.04121484375" Y="-2.796233886719" />
                  <Point X="27.043017578125" Y="-2.804227783203" />
                  <Point X="27.05123828125" Y="-2.831540527344" />
                  <Point X="27.0640703125" Y="-2.8624765625" />
                  <Point X="27.069546875" Y="-2.873578125" />
                  <Point X="27.295025390625" Y="-3.264120849609" />
                  <Point X="27.73589453125" Y="-4.027725097656" />
                  <Point X="27.723755859375" Y="-4.036083496094" />
                  <Point X="27.56058984375" Y="-3.823441162109" />
                  <Point X="26.83391796875" Y="-2.876423339844" />
                  <Point X="26.828654296875" Y="-2.870145019531" />
                  <Point X="26.808830078125" Y="-2.849624023438" />
                  <Point X="26.783251953125" Y="-2.828003662109" />
                  <Point X="26.77330078125" Y="-2.820646728516" />
                  <Point X="26.62216015625" Y="-2.723478271484" />
                  <Point X="26.56017578125" Y="-2.683627929688" />
                  <Point X="26.546283203125" Y="-2.676245605469" />
                  <Point X="26.51747265625" Y="-2.663874511719" />
                  <Point X="26.5025546875" Y="-2.658885742188" />
                  <Point X="26.47093359375" Y="-2.651154052734" />
                  <Point X="26.45539453125" Y="-2.648695800781" />
                  <Point X="26.424125" Y="-2.646376953125" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.243099609375" Y="-2.661727050781" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670339111328" />
                  <Point X="26.133578125" Y="-2.677170410156" />
                  <Point X="26.12001171875" Y="-2.681627929688" />
                  <Point X="26.092626953125" Y="-2.692970703125" />
                  <Point X="26.07987890625" Y="-2.699412841797" />
                  <Point X="26.05549609375" Y="-2.714134033203" />
                  <Point X="26.043861328125" Y="-2.722413085938" />
                  <Point X="25.916224609375" Y="-2.828539306641" />
                  <Point X="25.863876953125" Y="-2.872063476562" />
                  <Point X="25.852654296875" Y="-2.883086914062" />
                  <Point X="25.83218359375" Y="-2.906836181641" />
                  <Point X="25.822935546875" Y="-2.919562011719" />
                  <Point X="25.80604296875" Y="-2.947388427734" />
                  <Point X="25.79901953125" Y="-2.961466552734" />
                  <Point X="25.78739453125" Y="-2.990588134766" />
                  <Point X="25.78279296875" Y="-3.005631591797" />
                  <Point X="25.744630859375" Y="-3.181211425781" />
                  <Point X="25.728978515625" Y="-3.253219482422" />
                  <Point X="25.7275859375" Y="-3.261286865234" />
                  <Point X="25.724724609375" Y="-3.289677001953" />
                  <Point X="25.7247421875" Y="-3.323170166016" />
                  <Point X="25.7255546875" Y="-3.335520507812" />
                  <Point X="25.789455078125" Y="-3.820883789062" />
                  <Point X="25.83308984375" Y="-4.152320800781" />
                  <Point X="25.655068359375" Y="-3.487937255859" />
                  <Point X="25.652607421875" Y="-3.480121337891" />
                  <Point X="25.642146484375" Y="-3.453579589844" />
                  <Point X="25.6267890625" Y="-3.423815917969" />
                  <Point X="25.62041015625" Y="-3.413209960938" />
                  <Point X="25.50430078125" Y="-3.245918457031" />
                  <Point X="25.456681640625" Y="-3.177309570312" />
                  <Point X="25.446671875" Y="-3.165172851562" />
                  <Point X="25.424787109375" Y="-3.142716552734" />
                  <Point X="25.412912109375" Y="-3.132396972656" />
                  <Point X="25.38665625" Y="-3.113153320312" />
                  <Point X="25.373240234375" Y="-3.104936279297" />
                  <Point X="25.345240234375" Y="-3.090829101562" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.150984375" Y="-3.029175048828" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.75534375" Y="-3.062068847656" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.667068359375" Y="-3.090830322266" />
                  <Point X="24.639068359375" Y="-3.104938720703" />
                  <Point X="24.62565625" Y="-3.113155273438" />
                  <Point X="24.599400390625" Y="-3.132399658203" />
                  <Point X="24.58752734375" Y="-3.142716796875" />
                  <Point X="24.565642578125" Y="-3.165172119141" />
                  <Point X="24.555630859375" Y="-3.177310302734" />
                  <Point X="24.439521484375" Y="-3.3446015625" />
                  <Point X="24.391904296875" Y="-3.413210693359" />
                  <Point X="24.38753125" Y="-3.420130859375" />
                  <Point X="24.374025390625" Y="-3.445261230469" />
                  <Point X="24.36122265625" Y="-3.476214355469" />
                  <Point X="24.35724609375" Y="-3.487937011719" />
                  <Point X="24.2407421875" Y="-3.922737548828" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.361492240558" Y="4.19196156717" />
                  <Point X="25.611480342633" Y="4.760574901448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.709299463616" Y="3.979063238604" />
                  <Point X="25.366233411483" Y="4.740371548516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.789589411856" Y="3.853086541708" />
                  <Point X="25.341611689705" Y="4.648482719575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.741028456496" Y="3.76897604127" />
                  <Point X="25.316989967927" Y="4.556593890633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.636038698049" Y="4.777848370391" />
                  <Point X="24.62505225869" Y="4.78141808093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.692467501136" Y="3.684865540832" />
                  <Point X="25.292368254852" Y="4.464705058863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.665356063556" Y="4.668433669594" />
                  <Point X="24.399035709733" Y="4.754966398058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.643906397489" Y="3.600755088575" />
                  <Point X="25.267746651117" Y="4.372816191567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.694673523143" Y="4.559018938227" />
                  <Point X="24.173019502564" Y="4.728514604133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.595345220131" Y="3.516644660268" />
                  <Point X="25.243125047382" Y="4.280927324271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.72399098273" Y="4.449604206861" />
                  <Point X="23.979876534943" Y="4.691381647199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.546784042773" Y="3.432534231961" />
                  <Point X="25.204563350948" Y="4.193567867659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.753308442318" Y="4.340189475495" />
                  <Point X="23.814847993249" Y="4.645113759533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.498222865415" Y="3.348423803655" />
                  <Point X="25.127443258613" Y="4.118736793332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.787713630428" Y="4.229121640923" />
                  <Point X="23.649820343626" Y="4.598845582016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.941351089355" Y="2.779634108204" />
                  <Point X="28.93928688007" Y="2.780304810458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.449661688057" Y="3.264313375348" />
                  <Point X="23.523728007839" Y="4.539926554155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.035157394707" Y="2.649265680662" />
                  <Point X="28.828578408249" Y="2.71638726219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.401100510699" Y="3.180202947041" />
                  <Point X="23.537032940617" Y="4.435714608136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.112139611716" Y="2.524363730793" />
                  <Point X="28.717869936428" Y="2.652469713922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.352539333341" Y="3.096092518734" />
                  <Point X="23.527182000175" Y="4.339026461409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.075509931921" Y="2.436376523923" />
                  <Point X="28.607161464606" Y="2.588552165654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.303978155983" Y="3.011982090427" />
                  <Point X="23.498613688477" Y="4.248419957265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.984057700324" Y="2.36620224393" />
                  <Point X="28.496452992785" Y="2.524634617386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.255416978626" Y="2.927871662121" />
                  <Point X="23.455089711845" Y="4.162672843229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.892605015278" Y="2.296028111273" />
                  <Point X="28.385744520964" Y="2.460717069118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.206855801268" Y="2.843761233814" />
                  <Point X="23.366171115627" Y="4.091675335198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.801152294835" Y="2.225853990116" />
                  <Point X="28.275036049142" Y="2.39679952085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.15829462391" Y="2.759650805507" />
                  <Point X="23.227084060758" Y="4.036978547514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.913997272037" Y="4.1387066118" />
                  <Point X="22.699605177493" Y="4.208366826034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.709699574391" Y="2.15567986896" />
                  <Point X="28.164327577321" Y="2.332881972582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.109733446552" Y="2.6755403772" />
                  <Point X="22.586159259484" Y="4.145338727949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.618246853948" Y="2.085505747803" />
                  <Point X="28.053619094031" Y="2.26896442804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.061536933895" Y="2.591311462149" />
                  <Point X="22.472713341475" Y="4.082310629865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.526794133504" Y="2.015331626647" />
                  <Point X="27.94291054507" Y="2.205046904836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.030819674732" Y="2.501403193363" />
                  <Point X="22.359267423466" Y="4.019282531781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.435341413061" Y="1.94515750549" />
                  <Point X="27.832201996109" Y="2.141129381632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.013332750961" Y="2.40719612802" />
                  <Point X="22.266285876156" Y="3.949605156585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.343888692617" Y="1.874983384334" />
                  <Point X="27.721493447148" Y="2.077211858428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.021230431383" Y="2.304741104794" />
                  <Point X="22.174780357915" Y="3.879448190473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.252435972174" Y="1.804809263177" />
                  <Point X="27.607365460532" Y="2.014405377869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.063503704558" Y="2.191116774413" />
                  <Point X="22.08327431968" Y="3.809291393317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.162040568603" Y="1.734291598944" />
                  <Point X="27.420157886463" Y="1.975343894665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.195946750312" Y="2.048194508915" />
                  <Point X="21.991768281445" Y="3.739134596162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.095346644117" Y="1.656072857326" />
                  <Point X="21.971916136641" Y="3.645696037718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.725336108782" Y="1.026568264301" />
                  <Point X="29.533760610586" Y="1.088814916981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.042629698506" Y="1.573312719977" />
                  <Point X="22.042903165014" Y="3.52274204272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.751740010907" Y="0.918100205141" />
                  <Point X="29.314980565238" Y="1.060011951555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.013427336344" Y="1.482912231318" />
                  <Point X="22.113890457807" Y="3.399787961807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.768481586737" Y="0.812771626105" />
                  <Point X="29.09620051989" Y="1.031208986128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.001154582654" Y="1.387010979416" />
                  <Point X="22.184879176736" Y="3.276833417514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.773686186468" Y="0.711191637839" />
                  <Point X="28.877420493022" Y="1.002406014697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.01943119267" Y="1.281183637539" />
                  <Point X="22.238406994623" Y="3.159552263883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.605202327412" Y="0.666046450841" />
                  <Point X="28.65864056158" Y="0.973603012261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.068975975997" Y="1.165196650287" />
                  <Point X="22.250205037437" Y="3.055829936093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.436718295292" Y="0.620901320075" />
                  <Point X="28.399680136477" Y="0.957855443619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.215199860123" Y="1.017796718973" />
                  <Point X="22.242403724468" Y="2.958475825031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.268234263171" Y="0.575756189309" />
                  <Point X="22.197987687765" Y="2.873018558881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.099750231051" Y="0.530611058543" />
                  <Point X="22.124915763607" Y="2.796872154979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.93126619893" Y="0.485465927777" />
                  <Point X="22.015993564613" Y="2.732374211487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.517058769347" Y="2.894487953605" />
                  <Point X="21.235506808386" Y="2.985969731234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.76278216681" Y="0.440320797011" />
                  <Point X="21.173474938977" Y="2.906236196096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.617346705967" Y="0.387686731467" />
                  <Point X="21.111443069569" Y="2.826502660959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.511729181607" Y="0.322115034096" />
                  <Point X="21.049411200161" Y="2.746769125821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.441935827459" Y="0.244903358222" />
                  <Point X="20.989549450886" Y="2.666330475909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.388171194132" Y="0.162483635248" />
                  <Point X="20.940539787027" Y="2.5823657697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.362787194632" Y="0.070842485353" />
                  <Point X="20.891530167033" Y="2.498401049238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.348701740586" Y="-0.0244697845" />
                  <Point X="20.842520643235" Y="2.41443629752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.361875196476" Y="-0.128639011089" />
                  <Point X="20.793511119438" Y="2.330471545802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.394748277082" Y="-0.239209033756" />
                  <Point X="20.900852363195" Y="2.195705350185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.78286459463" Y="-0.790124277293" />
                  <Point X="29.538644999647" Y="-0.710772520677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.497920069017" Y="-0.372620492354" />
                  <Point X="21.126637274884" Y="2.022454473962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.768507546631" Y="-0.88534830092" />
                  <Point X="21.352421085529" Y="1.849203955491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.750580040693" Y="-0.97941221244" />
                  <Point X="21.519526997317" Y="1.695019042091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.729359520377" Y="-1.072406158727" />
                  <Point X="21.571627596984" Y="1.578201619771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.248830923179" Y="-1.016161864197" />
                  <Point X="21.573923202496" Y="1.477566821023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.731986838206" Y="-0.948117952411" />
                  <Point X="21.543279284889" Y="1.38763472212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.331114185291" Y="-0.9177554431" />
                  <Point X="21.493358031931" Y="1.303966209164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.151504263402" Y="-0.959285553142" />
                  <Point X="21.393309580438" Y="1.236585010329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.047161759883" Y="-1.025271529897" />
                  <Point X="21.153497656683" Y="1.214615716446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.980827433403" Y="-1.10360711199" />
                  <Point X="20.636654684582" Y="1.282659266639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.917634284136" Y="-1.182963324429" />
                  <Point X="20.354545858374" Y="1.274433069452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.880450607743" Y="-1.270770526893" />
                  <Point X="20.3296679891" Y="1.182627467877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.868702113693" Y="-1.366842121078" />
                  <Point X="20.304790119827" Y="1.090821866302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.859777586321" Y="-1.463831277658" />
                  <Point X="20.279912250554" Y="0.999016264726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.874004644899" Y="-1.568342840512" />
                  <Point X="20.260939395333" Y="0.905292007779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.942200999553" Y="-1.690390090653" />
                  <Point X="20.246836692022" Y="0.809985342552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.036176481522" Y="-1.82081348701" />
                  <Point X="20.23273384006" Y="0.714678725625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.255837034623" Y="-1.992074438501" />
                  <Point X="21.631946300749" Y="0.16015812663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.721337393811" Y="0.456032896059" />
                  <Point X="20.218630969826" Y="0.619372114634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.481621480802" Y="-2.16532516347" />
                  <Point X="21.695898631827" Y="0.03948984334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.70740569803" Y="-2.338575814049" />
                  <Point X="21.702924294548" Y="-0.06268184416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.933189915259" Y="-2.511826464627" />
                  <Point X="28.104779053825" Y="-2.242659459174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.457057504341" Y="-2.032201970072" />
                  <Point X="21.676979918888" Y="-0.154140916804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.07082292469" Y="-2.656435051546" />
                  <Point X="28.500486964706" Y="-2.471121664677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.327007677458" Y="-2.089835131129" />
                  <Point X="21.619540988625" Y="-0.235366788334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.016467371951" Y="-2.738662773164" />
                  <Point X="28.89619407908" Y="-2.699583611379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.210861139684" Y="-2.15198574466" />
                  <Point X="21.518234289769" Y="-0.302339157818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.13347196227" Y="-2.226729387945" />
                  <Point X="21.354347973294" Y="-0.348978176955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.08621796507" Y="-2.311264544832" />
                  <Point X="21.185864058514" Y="-0.394123345847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.041323557203" Y="-2.396566378768" />
                  <Point X="21.017380143735" Y="-0.439268514739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.006376648838" Y="-2.48510035122" />
                  <Point X="20.848896228956" Y="-0.484413683632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.002856046813" Y="-2.583845349582" />
                  <Point X="20.680412314177" Y="-0.529558852524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.022020648669" Y="-2.689961217498" />
                  <Point X="20.511928399398" Y="-0.574704021417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.041186601422" Y="-2.796077524347" />
                  <Point X="21.883623137861" Y="-1.120283570465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.845564309373" Y="-1.107917507474" />
                  <Point X="20.343444532458" Y="-0.619849205853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.092005958517" Y="-2.91247864572" />
                  <Point X="26.736438008154" Y="-2.796947615298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.303239494666" Y="-2.656192885887" />
                  <Point X="22.037277254776" Y="-1.270097730761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.583953311198" Y="-1.122803852718" />
                  <Point X="20.216980150628" Y="-0.678647348627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.162993039871" Y="-3.035432657933" />
                  <Point X="26.887159305732" Y="-2.945808844825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.099826149762" Y="-2.689988794954" />
                  <Point X="22.050702961172" Y="-1.374348918507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.365173263629" Y="-1.151606817423" />
                  <Point X="20.231962022316" Y="-0.783404165128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.233980121224" Y="-3.158386670145" />
                  <Point X="26.989263208226" Y="-3.078873325111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.001245496991" Y="-2.757846910504" />
                  <Point X="22.029042876551" Y="-1.467200041695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.146393193241" Y="-1.180409774713" />
                  <Point X="20.246943894004" Y="-0.888160981628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.304967336412" Y="-3.281340725843" />
                  <Point X="27.09136711072" Y="-3.211937805396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.914865561579" Y="-2.829669279431" />
                  <Point X="21.976052237066" Y="-1.549871250513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.927613108927" Y="-1.209212727479" />
                  <Point X="20.264231274637" Y="-0.993666903395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.37595537338" Y="-3.404295048554" />
                  <Point X="27.193471013214" Y="-3.345002285681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.834973338954" Y="-2.903599634028" />
                  <Point X="21.889598923078" Y="-1.621669777296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.708833024613" Y="-1.238015680244" />
                  <Point X="20.292056292279" Y="-1.102596710977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.446943410349" Y="-3.527249371264" />
                  <Point X="27.295574915708" Y="-3.478066765966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.788298728902" Y="-2.98832304521" />
                  <Point X="21.798146333302" Y="-1.691843940909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.490052940299" Y="-1.26681863301" />
                  <Point X="20.31988122613" Y="-1.211526491335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.517931447317" Y="-3.650203693975" />
                  <Point X="27.397678818202" Y="-3.611131246251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.766391327561" Y="-3.081093810324" />
                  <Point X="21.706693743526" Y="-1.762018104522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.588919484285" Y="-3.773158016685" />
                  <Point X="27.499782720696" Y="-3.744195726537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.746112667007" Y="-3.174393785399" />
                  <Point X="21.61524115375" Y="-1.832192268135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.659907521254" Y="-3.896112339396" />
                  <Point X="27.601886595151" Y="-3.877260197712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.726905135556" Y="-3.268041791417" />
                  <Point X="25.459308560147" Y="-3.181094393423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.928032734004" Y="-3.008472413376" />
                  <Point X="21.523788563974" Y="-1.902366431747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.730895558222" Y="-4.019066662106" />
                  <Point X="27.703990428322" Y="-4.010324655472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.729951979769" Y="-3.368920682416" />
                  <Point X="25.548824623312" Y="-3.310068836777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.770797595448" Y="-3.057272531222" />
                  <Point X="21.432335974198" Y="-1.97254059536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.743690528319" Y="-3.47327351874" />
                  <Point X="25.633901885251" Y="-3.437601026185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.629019420868" Y="-3.111094921108" />
                  <Point X="22.64485244916" Y="-2.466399991385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.340256149786" Y="-2.367430654319" />
                  <Point X="21.340883384422" Y="-2.042714758973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.757429076869" Y="-3.577626355065" />
                  <Point X="25.671630751719" Y="-3.54974878932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.550064801084" Y="-3.185329921334" />
                  <Point X="22.688336042416" Y="-2.5804175786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.22504962512" Y="-2.429886696623" />
                  <Point X="21.249430794646" Y="-2.112888922586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.771167625419" Y="-3.681979191389" />
                  <Point X="25.700948434147" Y="-3.659163593091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.493493825222" Y="-3.266837808344" />
                  <Point X="22.677307144864" Y="-2.67672298386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.114341054042" Y="-2.49380421264" />
                  <Point X="21.15797820487" Y="-2.183063086199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.784906173969" Y="-3.786332027713" />
                  <Point X="25.730266116574" Y="-3.768578396862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.436922908989" Y="-3.348345714729" />
                  <Point X="22.635959942125" Y="-2.763177374608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.003632482964" Y="-2.557721728658" />
                  <Point X="21.066525615093" Y="-2.253237249812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.798644613051" Y="-3.890684828469" />
                  <Point X="25.759583799001" Y="-3.877993200633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.382015452231" Y="-3.430394111861" />
                  <Point X="22.587398828315" Y="-2.847287823563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.892923978496" Y="-2.621639266319" />
                  <Point X="20.975072803659" Y="-2.323411341404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.812382997944" Y="-3.995037611618" />
                  <Point X="25.788901481428" Y="-3.987408004404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.3487920488" Y="-3.519488085012" />
                  <Point X="22.538837714505" Y="-2.931398272518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.782215569896" Y="-2.685556835129" />
                  <Point X="20.88361983219" Y="-2.393585380997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.826121382838" Y="-4.099390394767" />
                  <Point X="25.818219163856" Y="-4.096822808175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.324170602786" Y="-3.611377003555" />
                  <Point X="22.490276600695" Y="-3.015508721473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.671507161297" Y="-2.749474403939" />
                  <Point X="20.840198849282" Y="-2.479365959723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.299549156772" Y="-3.703265922098" />
                  <Point X="22.441715486885" Y="-3.099619170428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.560798752697" Y="-2.813391972749" />
                  <Point X="20.914085699722" Y="-2.603262164026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.274927710758" Y="-3.795154840641" />
                  <Point X="22.393154373075" Y="-3.183729619382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.450090344097" Y="-2.877309541559" />
                  <Point X="20.987972550161" Y="-2.727158368329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.250306264743" Y="-3.887043759184" />
                  <Point X="22.344593259265" Y="-3.267840068337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.339381935498" Y="-2.941227110369" />
                  <Point X="21.086045515176" Y="-2.858913117633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.225684605465" Y="-3.978932608433" />
                  <Point X="22.296032145455" Y="-3.351950517292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.228673526898" Y="-3.005144679179" />
                  <Point X="21.187059907791" Y="-2.991623594699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.201062810726" Y="-4.070821413669" />
                  <Point X="23.476012926106" Y="-3.835238425404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.353898203779" Y="-3.79556094692" />
                  <Point X="22.247471031645" Y="-3.436060966247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.176441015987" Y="-4.162710218904" />
                  <Point X="23.667979786305" Y="-3.997501150609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.000054830052" Y="-3.780479176717" />
                  <Point X="22.198909917836" Y="-3.520171415201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.151819221248" Y="-4.254599024139" />
                  <Point X="23.795998764194" Y="-4.13898594932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.870801058841" Y="-3.838370991941" />
                  <Point X="22.150348804026" Y="-3.604281864156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.12719742651" Y="-4.346487829375" />
                  <Point X="23.827200359586" Y="-4.249012873519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.770218143515" Y="-3.90557853295" />
                  <Point X="22.101787551848" Y="-3.688392268153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.102575631771" Y="-4.43837663461" />
                  <Point X="23.848443154889" Y="-4.355803987419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.669635818383" Y="-3.972786265724" />
                  <Point X="22.053226204716" Y="-3.772502641296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.077953837032" Y="-4.530265439846" />
                  <Point X="23.86968524817" Y="-4.462594873218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.586449365551" Y="-4.045646260042" />
                  <Point X="22.086024092259" Y="-3.883048232257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.053332042293" Y="-4.622154245081" />
                  <Point X="23.87997640905" Y="-4.565827585387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.525096580926" Y="-4.125600443201" />
                  <Point X="22.340219914101" Y="-4.065530372776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.028710247554" Y="-4.714043050316" />
                  <Point X="23.867908930144" Y="-4.66179553511" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.7556171875" Y="-4.597301269531" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521543945312" />
                  <Point X="25.3482109375" Y="-3.354252441406" />
                  <Point X="25.300591796875" Y="-3.285643554688" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.0946640625" Y="-3.210635986328" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.8116640625" Y="-3.243530273438" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285644287109" />
                  <Point X="24.595611328125" Y="-3.452935546875" />
                  <Point X="24.547994140625" Y="-3.521544677734" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.42426953125" Y="-3.971912841797" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.98545703125" Y="-4.954700195312" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.691517578125" Y="-4.884487304688" />
                  <Point X="23.648412109375" Y="-4.873396484375" />
                  <Point X="23.656654296875" Y="-4.810783691406" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534756835938" />
                  <Point X="23.64739453125" Y="-4.31896484375" />
                  <Point X="23.6297890625" Y="-4.230465332031" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.448298828125" Y="-4.057559082031" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.1312109375" Y="-3.971372802734" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.827181640625" Y="-4.096027832031" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.674857421875" Y="-4.242536132812" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.26978515625" Y="-4.245391113281" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.855818359375" Y="-3.945592529297" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="21.96604296875" Y="-3.543511962891" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597595214844" />
                  <Point X="22.4860234375" Y="-2.568766845703" />
                  <Point X="22.46867578125" Y="-2.551418212891" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="22.033310546875" Y="-2.759980224609" />
                  <Point X="21.15704296875" Y="-3.265893554688" />
                  <Point X="20.93754296875" Y="-2.977517822266" />
                  <Point X="20.838296875" Y="-2.847126464844" />
                  <Point X="20.63157421875" Y="-2.500485839844" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="20.912517578125" Y="-2.131922363281" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.39601574707" />
                  <Point X="21.859642578125" Y="-1.374920043945" />
                  <Point X="21.861884765625" Y="-1.366263427734" />
                  <Point X="21.85967578125" Y="-1.334595703125" />
                  <Point X="21.83884375" Y="-1.31064074707" />
                  <Point X="21.8200625" Y="-1.299587402344" />
                  <Point X="21.812361328125" Y="-1.295054199219" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.3070625" Y="-1.350896850586" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.111423828125" Y="-1.163154541016" />
                  <Point X="20.072607421875" Y="-1.011188476563" />
                  <Point X="20.0179140625" Y="-0.628777526855" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="20.39030859375" Y="-0.410589538574" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.458103515625" Y="-0.121426116943" />
                  <Point X="21.47778515625" Y="-0.107766227722" />
                  <Point X="21.485857421875" Y="-0.102164085388" />
                  <Point X="21.5051015625" Y="-0.075907905579" />
                  <Point X="21.511662109375" Y="-0.054769985199" />
                  <Point X="21.514353515625" Y="-0.046095401764" />
                  <Point X="21.5143515625" Y="-0.016459047318" />
                  <Point X="21.50779296875" Y="0.004673182964" />
                  <Point X="21.505103515625" Y="0.013342202187" />
                  <Point X="21.485857421875" Y="0.039604011536" />
                  <Point X="21.46617578125" Y="0.053263896942" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="21.011001953125" Y="0.181715133667" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.05733984375" Y="0.82736517334" />
                  <Point X="20.08235546875" Y="0.996415161133" />
                  <Point X="20.192453125" Y="1.402709472656" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.492470703125" Y="1.493281005859" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.311857421875" Y="1.409601196289" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056518555" />
                  <Point X="21.3608828125" Y="1.443786987305" />
                  <Point X="21.378361328125" Y="1.485985107422" />
                  <Point X="21.385529296875" Y="1.503291137695" />
                  <Point X="21.38928515625" Y="1.524606933594" />
                  <Point X="21.38368359375" Y="1.545513671875" />
                  <Point X="21.36259375" Y="1.586027954102" />
                  <Point X="21.353943359375" Y="1.602643432617" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.092501953125" Y="1.809158447266" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.74278515625" Y="2.620476318359" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="21.13162890625" Y="3.161872070312" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.377595703125" Y="3.194399658203" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.922154296875" Y="2.915126953125" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927405273438" />
                  <Point X="22.0298125" Y="2.970468017578" />
                  <Point X="22.04747265625" Y="2.988128662109" />
                  <Point X="22.0591015625" Y="3.006381835938" />
                  <Point X="22.061927734375" Y="3.027839111328" />
                  <Point X="22.056619140625" Y="3.088507568359" />
                  <Point X="22.054443359375" Y="3.113388671875" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.938244140625" Y="3.324018310547" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="22.07783203125" Y="4.044536132813" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.706453125" Y="4.429524414063" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.890359375" Y="4.472389160156" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.11627734375" Y="4.238509765625" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.22225" />
                  <Point X="23.2565234375" Y="4.251382324219" />
                  <Point X="23.2853671875" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.294487792969" />
                  <Point X="23.33680859375" Y="4.367089355469" />
                  <Point X="23.346197265625" Y="4.396864257812" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.33561328125" Y="4.5131484375" />
                  <Point X="23.31086328125" Y="4.701141113281" />
                  <Point X="23.81274609375" Y="4.8418515625" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.58875390625" Y="4.968466796875" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.820818359375" Y="4.822349609375" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.11065625" Y="4.520655761719" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.668845703125" Y="4.945606445312" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.320900390625" Y="4.814340332031" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.808865234375" Y="4.660097167969" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="27.220990234375" Y="4.480183105469" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.61880859375" Y="4.261942871094" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.996705078125" Y="4.007821289062" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.840888671875" Y="3.561938476562" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491514648438" />
                  <Point X="27.209626953125" Y="2.434578613281" />
                  <Point X="27.2033828125" Y="2.411228271484" />
                  <Point X="27.202044921875" Y="2.392328613281" />
                  <Point X="27.207982421875" Y="2.343095214844" />
                  <Point X="27.210416015625" Y="2.322903808594" />
                  <Point X="27.21868359375" Y="2.300811767578" />
                  <Point X="27.2491484375" Y="2.255915771484" />
                  <Point X="27.261642578125" Y="2.237503173828" />
                  <Point X="27.274943359375" Y="2.224202636719" />
                  <Point X="27.319837890625" Y="2.193738769531" />
                  <Point X="27.338251953125" Y="2.181245117188" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.4095703125" Y="2.167043457031" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946044922" />
                  <Point X="27.505599609375" Y="2.181171630859" />
                  <Point X="27.528951171875" Y="2.187415771484" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="27.975078125" Y="2.443011962891" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.135138671875" Y="2.835623779297" />
                  <Point X="29.20259765625" Y="2.741873046875" />
                  <Point X="29.349869140625" Y="2.498504882812" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="29.093099609375" Y="2.210383789062" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583832519531" />
                  <Point X="28.23839453125" Y="1.530375" />
                  <Point X="28.22158984375" Y="1.508451171875" />
                  <Point X="28.21312109375" Y="1.491503540039" />
                  <Point X="28.197857421875" Y="1.436923217773" />
                  <Point X="28.191595703125" Y="1.4145390625" />
                  <Point X="28.190779296875" Y="1.390965698242" />
                  <Point X="28.203310546875" Y="1.330238037109" />
                  <Point X="28.20844921875" Y="1.305332763672" />
                  <Point X="28.215646484375" Y="1.287955566406" />
                  <Point X="28.2497265625" Y="1.236154541016" />
                  <Point X="28.263703125" Y="1.21491003418" />
                  <Point X="28.280947265625" Y="1.19882019043" />
                  <Point X="28.3303359375" Y="1.171019165039" />
                  <Point X="28.35058984375" Y="1.159617797852" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.435341796875" Y="1.144794433594" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.888115234375" Y="1.195453613281" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.91021875" Y="1.070387817383" />
                  <Point X="29.93919140625" Y="0.951367797852" />
                  <Point X="29.9856015625" Y="0.653295471191" />
                  <Point X="29.997859375" Y="0.57455645752" />
                  <Point X="29.663123046875" Y="0.484863677979" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819656372" />
                  <Point X="28.663484375" Y="0.194899047852" />
                  <Point X="28.636578125" Y="0.179347290039" />
                  <Point X="28.622265625" Y="0.16692578125" />
                  <Point X="28.58290234375" Y="0.116768486023" />
                  <Point X="28.566759765625" Y="0.096198104858" />
                  <Point X="28.556986328125" Y="0.074734802246" />
                  <Point X="28.543865234375" Y="0.006222248077" />
                  <Point X="28.538484375" Y="-0.02187575531" />
                  <Point X="28.538484375" Y="-0.040684322357" />
                  <Point X="28.55160546875" Y="-0.109196723938" />
                  <Point X="28.556986328125" Y="-0.137294876099" />
                  <Point X="28.566759765625" Y="-0.158758178711" />
                  <Point X="28.606123046875" Y="-0.208915481567" />
                  <Point X="28.622265625" Y="-0.229485855103" />
                  <Point X="28.636576171875" Y="-0.241906448364" />
                  <Point X="28.702181640625" Y="-0.279827056885" />
                  <Point X="28.7290859375" Y="-0.295378967285" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.11927734375" Y="-0.401700775146" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.964609375" Y="-0.859108154297" />
                  <Point X="29.948431640625" Y="-0.966414672852" />
                  <Point X="29.88897265625" Y="-1.226962646484" />
                  <Point X="29.874546875" Y="-1.290178588867" />
                  <Point X="29.481189453125" Y="-1.238392089844" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.266080078125" Y="-1.126327392578" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.185447265625" Y="-1.154697143555" />
                  <Point X="28.10762109375" Y="-1.248297973633" />
                  <Point X="28.075703125" Y="-1.286685180664" />
                  <Point X="28.064359375" Y="-1.314070800781" />
                  <Point X="28.053205078125" Y="-1.435288085938" />
                  <Point X="28.048630859375" Y="-1.485001220703" />
                  <Point X="28.05636328125" Y="-1.516622436523" />
                  <Point X="28.127619140625" Y="-1.627457763672" />
                  <Point X="28.15684375" Y="-1.672913085938" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.519349609375" Y="-1.954785644531" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.24973046875" Y="-2.728357177734" />
                  <Point X="29.204134765625" Y="-2.802136962891" />
                  <Point X="29.081166015625" Y="-2.976860107422" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.705728515625" Y="-2.809011230469" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.584099609375" Y="-2.225637207031" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.361771484375" Y="-2.286245849609" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334683349609" />
                  <Point X="27.221599609375" Y="-2.461990722656" />
                  <Point X="27.19412109375" Y="-2.514201416016" />
                  <Point X="27.1891640625" Y="-2.546375488281" />
                  <Point X="27.21683984375" Y="-2.699618408203" />
                  <Point X="27.22819140625" Y="-2.762465820312" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.459572265625" Y="-3.169121337891" />
                  <Point X="27.98667578125" Y="-4.082088867188" />
                  <Point X="27.889408203125" Y="-4.151564453125" />
                  <Point X="27.835314453125" Y="-4.190202148438" />
                  <Point X="27.6978125" Y="-4.279204101562" />
                  <Point X="27.679775390625" Y="-4.29087890625" />
                  <Point X="27.4098515625" Y="-3.939105224609" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467529297" />
                  <Point X="26.51941015625" Y="-2.883299072266" />
                  <Point X="26.45742578125" Y="-2.843448730469" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.260509765625" Y="-2.850927734375" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868508789062" />
                  <Point X="26.037697265625" Y="-2.974635009766" />
                  <Point X="25.985349609375" Y="-3.018159179688" />
                  <Point X="25.96845703125" Y="-3.045985595703" />
                  <Point X="25.930294921875" Y="-3.221565429688" />
                  <Point X="25.914642578125" Y="-3.293573486328" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.977830078125" Y="-3.796083496094" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.04553515625" Y="-4.952026855469" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.867337890625" Y="-4.986319824219" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#188" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.137041589218" Y="4.866570673383" Z="1.8" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.8" />
                  <Point X="-0.4232162988" Y="5.049408199373" Z="1.8" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.8" />
                  <Point X="-1.206908977239" Y="4.921274931679" Z="1.8" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.8" />
                  <Point X="-1.720116201252" Y="4.53790205109" Z="1.8" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.8" />
                  <Point X="-1.717033944948" Y="4.41340553733" Z="1.8" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.8" />
                  <Point X="-1.76876874257" Y="4.328856590757" Z="1.8" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.8" />
                  <Point X="-1.866791580138" Y="4.314140500414" Z="1.8" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.8" />
                  <Point X="-2.076129432762" Y="4.53410731045" Z="1.8" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.8" />
                  <Point X="-2.323986649474" Y="4.504511880731" Z="1.8" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.8" />
                  <Point X="-2.958747626355" Y="4.11549574825" Z="1.8" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.8" />
                  <Point X="-3.111212876491" Y="3.330298416608" Z="1.8" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.8" />
                  <Point X="-2.999347854783" Y="3.115431857963" Z="1.8" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.8" />
                  <Point X="-3.011700760943" Y="3.037102806943" Z="1.8" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.8" />
                  <Point X="-3.079644607643" Y="2.99621684439" Z="1.8" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.8" />
                  <Point X="-3.603560931689" Y="3.268981072254" Z="1.8" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.8" />
                  <Point X="-3.913991130678" Y="3.223854573885" Z="1.8" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.8" />
                  <Point X="-4.306554375152" Y="2.676961467068" Z="1.8" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.8" />
                  <Point X="-3.944092631926" Y="1.800770811944" Z="1.8" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.8" />
                  <Point X="-3.687912792731" Y="1.594218589494" Z="1.8" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.8" />
                  <Point X="-3.673990869698" Y="1.536398243911" Z="1.8" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.8" />
                  <Point X="-3.709335005212" Y="1.488567272391" Z="1.8" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.8" />
                  <Point X="-4.507160041094" Y="1.574133300366" Z="1.8" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.8" />
                  <Point X="-4.861964107776" Y="1.447066511932" Z="1.8" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.8" />
                  <Point X="-4.998278065728" Y="0.865964161236" Z="1.8" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.8" />
                  <Point X="-4.008097212678" Y="0.164698884362" Z="1.8" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.8" />
                  <Point X="-3.568489128795" Y="0.043466875937" Z="1.8" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.8" />
                  <Point X="-3.546117277557" Y="0.021137939978" Z="1.8" />
                  <Point X="-3.539556741714" Y="0" Z="1.8" />
                  <Point X="-3.542247323578" Y="-0.008669011083" Z="1.8" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.8" />
                  <Point X="-3.556879466328" Y="-0.035409107197" Z="1.8" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.8" />
                  <Point X="-4.628790576095" Y="-0.331013172394" Z="1.8" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.8" />
                  <Point X="-5.037738979693" Y="-0.604576451236" Z="1.8" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.8" />
                  <Point X="-4.943177944904" Y="-1.144248336485" Z="1.8" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.8" />
                  <Point X="-3.692569755343" Y="-1.369189019086" Z="1.8" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.8" />
                  <Point X="-3.211456731504" Y="-1.311396462018" Z="1.8" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.8" />
                  <Point X="-3.194917070497" Y="-1.331101485065" Z="1.8" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.8" />
                  <Point X="-4.124077602256" Y="-2.060974623766" Z="1.8" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.8" />
                  <Point X="-4.417526181541" Y="-2.49481559014" Z="1.8" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.8" />
                  <Point X="-4.108400157592" Y="-2.976520808238" Z="1.8" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.8" />
                  <Point X="-2.947846574567" Y="-2.772001403908" Z="1.8" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.8" />
                  <Point X="-2.567793877436" Y="-2.560536591695" Z="1.8" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.8" />
                  <Point X="-3.08341560926" Y="-3.487231807926" Z="1.8" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.8" />
                  <Point X="-3.1808420348" Y="-3.953929603621" Z="1.8" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.8" />
                  <Point X="-2.762693328728" Y="-4.256621709849" Z="1.8" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.8" />
                  <Point X="-2.291630556161" Y="-4.241693878563" Z="1.8" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.8" />
                  <Point X="-2.151195732184" Y="-4.10632097088" Z="1.8" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.8" />
                  <Point X="-1.878215531108" Y="-3.989985938079" Z="1.8" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.8" />
                  <Point X="-1.5908255908" Y="-4.06387120243" Z="1.8" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.8" />
                  <Point X="-1.407802263808" Y="-4.297440276808" Z="1.8" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.8" />
                  <Point X="-1.399074664723" Y="-4.772977509984" Z="1.8" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.8" />
                  <Point X="-1.327098895566" Y="-4.90163034241" Z="1.8" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.8" />
                  <Point X="-1.030227700143" Y="-4.972504379755" Z="1.8" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.8" />
                  <Point X="-0.533591273422" Y="-3.95357379849" Z="1.8" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.8" />
                  <Point X="-0.369468449394" Y="-3.450164142838" Z="1.8" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.8" />
                  <Point X="-0.179672486508" Y="-3.260003077717" Z="1.8" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.8" />
                  <Point X="0.073686592853" Y="-3.227108967571" Z="1.8" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.8" />
                  <Point X="0.300977410038" Y="-3.351481612929" Z="1.8" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.8" />
                  <Point X="0.701163586979" Y="-4.578962231325" Z="1.8" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.8" />
                  <Point X="0.870118512834" Y="-5.004234621558" Z="1.8" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.8" />
                  <Point X="1.050082715524" Y="-4.96958710257" Z="1.8" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.8" />
                  <Point X="1.021245109404" Y="-3.758277202398" Z="1.8" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.8" />
                  <Point X="0.972997038202" Y="-3.20090564684" Z="1.8" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.8" />
                  <Point X="1.063506228561" Y="-2.981801854397" Z="1.8" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.8" />
                  <Point X="1.258934355647" Y="-2.869437286745" Z="1.8" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.8" />
                  <Point X="1.486214973525" Y="-2.894076906603" Z="1.8" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.8" />
                  <Point X="2.364026487802" Y="-3.938263234504" Z="1.8" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.8" />
                  <Point X="2.718826185981" Y="-4.289898333562" Z="1.8" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.8" />
                  <Point X="2.912311586482" Y="-4.160971636765" Z="1.8" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.8" />
                  <Point X="2.496716976143" Y="-3.112841412697" Z="1.8" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.8" />
                  <Point X="2.259886693358" Y="-2.659451186913" Z="1.8" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.8" />
                  <Point X="2.259689274858" Y="-2.453997427147" Z="1.8" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.8" />
                  <Point X="2.378900951225" Y="-2.299212035109" Z="1.8" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.8" />
                  <Point X="2.569055616599" Y="-2.243561316133" Z="1.8" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.8" />
                  <Point X="3.67457218441" Y="-2.821032524132" Z="1.8" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.8" />
                  <Point X="4.115897506061" Y="-2.974357621963" Z="1.8" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.8" />
                  <Point X="4.286107017838" Y="-2.723362173901" Z="1.8" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.8" />
                  <Point X="3.543629592096" Y="-1.883837951537" Z="1.8" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.8" />
                  <Point X="3.16351927233" Y="-1.569137714118" Z="1.8" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.8" />
                  <Point X="3.096837091988" Y="-1.408589383272" Z="1.8" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.8" />
                  <Point X="3.139909266164" Y="-1.248985015917" Z="1.8" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.8" />
                  <Point X="3.270541439722" Y="-1.14390659977" Z="1.8" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.8" />
                  <Point X="4.468507424379" Y="-1.256684260125" Z="1.8" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.8" />
                  <Point X="4.931562720583" Y="-1.20680613053" Z="1.8" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.8" />
                  <Point X="5.007893083845" Y="-0.835282266863" Z="1.8" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.8" />
                  <Point X="4.126060996863" Y="-0.322124445112" Z="1.8" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.8" />
                  <Point X="3.721047227734" Y="-0.205258791835" Z="1.8" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.8" />
                  <Point X="3.639299431535" Y="-0.146767871028" Z="1.8" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.8" />
                  <Point X="3.594555629324" Y="-0.068512462281" Z="1.8" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.8" />
                  <Point X="3.586815821101" Y="0.028098068913" Z="1.8" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.8" />
                  <Point X="3.616080006866" Y="0.117180867547" Z="1.8" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.8" />
                  <Point X="3.682348186619" Y="0.182890052407" Z="1.8" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.8" />
                  <Point X="4.669906820444" Y="0.467847487656" Z="1.8" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.8" />
                  <Point X="5.02884820627" Y="0.692267238837" Z="1.8" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.8" />
                  <Point X="4.95264296259" Y="1.113494159906" Z="1.8" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.8" />
                  <Point X="3.875432802418" Y="1.276305923891" Z="1.8" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.8" />
                  <Point X="3.435735822964" Y="1.225643422629" Z="1.8" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.8" />
                  <Point X="3.348706019666" Y="1.245870164367" Z="1.8" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.8" />
                  <Point X="3.28534156101" Y="1.294915442964" Z="1.8" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.8" />
                  <Point X="3.246122180048" Y="1.37162172554" Z="1.8" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.8" />
                  <Point X="3.239852028991" Y="1.454733449587" Z="1.8" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.8" />
                  <Point X="3.2719212706" Y="1.531237520794" Z="1.8" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.8" />
                  <Point X="4.117380612023" Y="2.201996177078" Z="1.8" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.8" />
                  <Point X="4.386489417528" Y="2.555671089719" Z="1.8" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.8" />
                  <Point X="4.169568521056" Y="2.89610731011" Z="1.8" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.8" />
                  <Point X="2.943920531081" Y="2.517593371819" Z="1.8" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.8" />
                  <Point X="2.486527577653" Y="2.260754582069" Z="1.8" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.8" />
                  <Point X="2.409400226366" Y="2.247963887201" Z="1.8" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.8" />
                  <Point X="2.341754171595" Y="2.266394238804" Z="1.8" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.8" />
                  <Point X="2.284364361431" Y="2.315270688787" Z="1.8" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.8" />
                  <Point X="2.251465815558" Y="2.380358217466" Z="1.8" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.8" />
                  <Point X="2.251773360589" Y="2.452941998229" Z="1.8" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.8" />
                  <Point X="2.878031981041" Y="3.568218550874" Z="1.8" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.8" />
                  <Point X="3.019524676182" Y="4.07984841188" Z="1.8" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.8" />
                  <Point X="2.637821124955" Y="4.336425522934" Z="1.8" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.8" />
                  <Point X="2.236015323952" Y="4.556755254883" Z="1.8" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.8" />
                  <Point X="1.819757761149" Y="4.738381354503" Z="1.8" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.8" />
                  <Point X="1.326477118106" Y="4.894223753694" Z="1.8" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.8" />
                  <Point X="0.667896305416" Y="5.026614317936" Z="1.8" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.8" />
                  <Point X="0.056202972335" Y="4.564876832568" Z="1.8" />
                  <Point X="0" Y="4.355124473572" Z="1.8" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>