<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#177" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2280" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.800068359375" Y="-4.396139648438" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140625" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.44559375" Y="-3.327947753906" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209020263672" />
                  <Point X="25.33049609375" Y="-3.189776611328" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.152748046875" Y="-3.129192871094" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.813427734375" Y="-3.143511962891" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.189776611328" />
                  <Point X="24.655560546875" Y="-3.209020263672" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.53690625" Y="-3.370906494141" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481572265625" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.28519140625" Y="-4.1238984375" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="24.038958984375" Y="-4.8683125" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.75358203125" Y="-4.802362792969" />
                  <Point X="23.756537109375" Y="-4.779922363281" />
                  <Point X="23.7850390625" Y="-4.5634375" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223632812" />
                  <Point X="23.747716796875" Y="-4.336371582031" />
                  <Point X="23.722962890625" Y="-4.211932128906" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.538486328125" Y="-4.010295898437" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295898438" />
                  <Point X="23.387115234375" Y="-3.897994628906" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.173990234375" Y="-3.878972900391" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.80487109375" Y="-3.996679931641" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.572927734375" Y="-4.2193203125" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.371689453125" Y="-4.196750976562" />
                  <Point X="22.19828515625" Y="-4.0893828125" />
                  <Point X="21.96213671875" Y="-3.907555908203" />
                  <Point X="21.895279296875" Y="-3.856078125" />
                  <Point X="22.136240234375" Y="-3.438720214844" />
                  <Point X="22.576240234375" Y="-2.676619384766" />
                  <Point X="22.587140625" Y="-2.647655761719" />
                  <Point X="22.593412109375" Y="-2.616131347656" />
                  <Point X="22.59442578125" Y="-2.585198974609" />
                  <Point X="22.585443359375" Y="-2.555582275391" />
                  <Point X="22.571228515625" Y="-2.526753662109" />
                  <Point X="22.553201171875" Y="-2.501593261719" />
                  <Point X="22.535853515625" Y="-2.484244628906" />
                  <Point X="22.510697265625" Y="-2.466216796875" />
                  <Point X="22.4818671875" Y="-2.451997802734" />
                  <Point X="22.452248046875" Y="-2.443011962891" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.83351953125" Y="-2.765633056641" />
                  <Point X="21.1819765625" Y="-3.141801513672" />
                  <Point X="21.0541328125" Y="-2.973842529297" />
                  <Point X="20.917140625" Y="-2.793861816406" />
                  <Point X="20.7478359375" Y="-2.50996484375" />
                  <Point X="20.693857421875" Y="-2.419450439453" />
                  <Point X="21.124189453125" Y="-2.089244628906" />
                  <Point X="21.894044921875" Y="-1.498513305664" />
                  <Point X="21.915423828125" Y="-1.475591186523" />
                  <Point X="21.933388671875" Y="-1.448458496094" />
                  <Point X="21.946142578125" Y="-1.419832763672" />
                  <Point X="21.95384765625" Y="-1.390085449219" />
                  <Point X="21.95665234375" Y="-1.359654663086" />
                  <Point X="21.954443359375" Y="-1.327984741211" />
                  <Point X="21.94744140625" Y="-1.298239746094" />
                  <Point X="21.931359375" Y="-1.272256469727" />
                  <Point X="21.91052734375" Y="-1.24830065918" />
                  <Point X="21.887029296875" Y="-1.228767211914" />
                  <Point X="21.860546875" Y="-1.213180664062" />
                  <Point X="21.831283203125" Y="-1.201956665039" />
                  <Point X="21.799396484375" Y="-1.195474975586" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="21.102408203125" Y="-1.282020385742" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.21950390625" Y="-1.202417724609" />
                  <Point X="20.165921875" Y="-0.99265057373" />
                  <Point X="20.121130859375" Y="-0.679464355469" />
                  <Point X="20.107578125" Y="-0.584698364258" />
                  <Point X="20.59014453125" Y="-0.455394927979" />
                  <Point X="21.467125" Y="-0.220408432007" />
                  <Point X="21.490318359375" Y="-0.210755172729" />
                  <Point X="21.522291015625" Y="-0.19215007019" />
                  <Point X="21.535490234375" Y="-0.182884780884" />
                  <Point X="21.558658203125" Y="-0.163489929199" />
                  <Point X="21.574634765625" Y="-0.146345352173" />
                  <Point X="21.5859296875" Y="-0.125811309814" />
                  <Point X="21.598822265625" Y="-0.093456283569" />
                  <Point X="21.6032890625" Y="-0.078987014771" />
                  <Point X="21.609318359375" Y="-0.051976680756" />
                  <Point X="21.61159765625" Y="-0.030623823166" />
                  <Point X="21.6090234375" Y="-0.009304466248" />
                  <Point X="21.601310546875" Y="0.023133892059" />
                  <Point X="21.596591796875" Y="0.037667362213" />
                  <Point X="21.5853828125" Y="0.064594367981" />
                  <Point X="21.573775390625" Y="0.084953781128" />
                  <Point X="21.5575390625" Y="0.101852523804" />
                  <Point X="21.52931640625" Y="0.124754959106" />
                  <Point X="21.515927734375" Y="0.133845077515" />
                  <Point X="21.489009765625" Y="0.148942596436" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="20.860341796875" Y="0.320435638428" />
                  <Point X="20.108185546875" Y="0.521975463867" />
                  <Point X="20.140982421875" Y="0.743621398926" />
                  <Point X="20.17551171875" Y="0.976968078613" />
                  <Point X="20.265681640625" Y="1.309721923828" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.605693359375" Y="1.382555175781" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228149414" />
                  <Point X="21.296865234375" Y="1.305263671875" />
                  <Point X="21.333169921875" Y="1.31671081543" />
                  <Point X="21.358291015625" Y="1.324631225586" />
                  <Point X="21.3772265625" Y="1.332963378906" />
                  <Point X="21.395970703125" Y="1.343786254883" />
                  <Point X="21.4126484375" Y="1.356016235352" />
                  <Point X="21.42628515625" Y="1.371565917969" />
                  <Point X="21.438701171875" Y="1.389297241211" />
                  <Point X="21.448650390625" Y="1.407432373047" />
                  <Point X="21.46321875" Y="1.442602539063" />
                  <Point X="21.473296875" Y="1.466936767578" />
                  <Point X="21.479087890625" Y="1.486806152344" />
                  <Point X="21.48284375" Y="1.508120117188" />
                  <Point X="21.484193359375" Y="1.528755615234" />
                  <Point X="21.481048828125" Y="1.549194580078" />
                  <Point X="21.475447265625" Y="1.570099731445" />
                  <Point X="21.46794921875" Y="1.589378662109" />
                  <Point X="21.45037109375" Y="1.623145141602" />
                  <Point X="21.438208984375" Y="1.646508300781" />
                  <Point X="21.42671875" Y="1.663705444336" />
                  <Point X="21.412806640625" Y="1.680285644531" />
                  <Point X="21.39786328125" Y="1.694590332031" />
                  <Point X="21.049810546875" Y="1.961660644531" />
                  <Point X="20.648140625" Y="2.269873535156" />
                  <Point X="20.78467578125" Y="2.503791748047" />
                  <Point X="20.9188515625" Y="2.733665039062" />
                  <Point X="21.1576953125" Y="3.040667236328" />
                  <Point X="21.24949609375" Y="3.158662353516" />
                  <Point X="21.40725" Y="3.067582763672" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.903771484375" Y="2.821372558594" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.980892578125" Y="2.821587890625" />
                  <Point X="22.000984375" Y="2.826504150391" />
                  <Point X="22.01953515625" Y="2.83565234375" />
                  <Point X="22.0377890625" Y="2.847281005859" />
                  <Point X="22.053921875" Y="2.860228759766" />
                  <Point X="22.0898125" Y="2.896119384766" />
                  <Point X="22.114646484375" Y="2.920952148438" />
                  <Point X="22.12758984375" Y="2.937078857422" />
                  <Point X="22.139220703125" Y="2.955333251953" />
                  <Point X="22.14837109375" Y="2.973885009766" />
                  <Point X="22.1532890625" Y="2.993977294922" />
                  <Point X="22.156115234375" Y="3.015436523438" />
                  <Point X="22.15656640625" Y="3.036120605469" />
                  <Point X="22.152142578125" Y="3.086684814453" />
                  <Point X="22.14908203125" Y="3.121670166016" />
                  <Point X="22.145044921875" Y="3.141962402344" />
                  <Point X="22.13853515625" Y="3.162604736328" />
                  <Point X="22.130205078125" Y="3.181533203125" />
                  <Point X="21.975970703125" Y="3.448672119141" />
                  <Point X="21.81666796875" Y="3.724595458984" />
                  <Point X="22.065697265625" Y="3.915523681641" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.6755546875" Y="4.303680175781" />
                  <Point X="22.83296484375" Y="4.391133789062" />
                  <Point X="22.838609375" Y="4.383776855469" />
                  <Point X="22.9568046875" Y="4.229741699219" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.18939453125" />
                  <Point X="23.0611640625" Y="4.160098144531" />
                  <Point X="23.100103515625" Y="4.139827636719" />
                  <Point X="23.1193828125" Y="4.132331054688" />
                  <Point X="23.1402890625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.12358203125" />
                  <Point X="23.181373046875" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222546875" Y="4.134481933594" />
                  <Point X="23.281162109375" Y="4.158762207031" />
                  <Point X="23.321720703125" Y="4.175561523437" />
                  <Point X="23.339853515625" Y="4.185509277344" />
                  <Point X="23.357583984375" Y="4.197923828125" />
                  <Point X="23.373134765625" Y="4.211561523438" />
                  <Point X="23.3853671875" Y="4.2282421875" />
                  <Point X="23.396189453125" Y="4.246985839844" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.423599609375" Y="4.326431152344" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410145996094" />
                  <Point X="23.442271484375" Y="4.430827148438" />
                  <Point X="23.424736328125" Y="4.564015625" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.747853515625" Y="4.724994628906" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.506404296875" Y="4.863180664062" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.75187890625" Y="4.712581054688" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.225244140625" Y="4.581249023438" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.5798984375" Y="4.859401855469" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.22133984375" Y="4.740647949219" />
                  <Point X="26.48102734375" Y="4.677950195312" />
                  <Point X="26.726005859375" Y="4.589095703125" />
                  <Point X="26.894642578125" Y="4.527930175781" />
                  <Point X="27.1321171875" Y="4.41687109375" />
                  <Point X="27.294576171875" Y="4.340894042969" />
                  <Point X="27.524009765625" Y="4.207226074219" />
                  <Point X="27.680982421875" Y="4.1157734375" />
                  <Point X="27.897341796875" Y="3.961909912109" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.656849609375" Y="3.433172851562" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.539937988281" />
                  <Point X="27.133078125" Y="2.516057861328" />
                  <Point X="27.120388671875" Y="2.468604492188" />
                  <Point X="27.111607421875" Y="2.435771484375" />
                  <Point X="27.108619140625" Y="2.4179375" />
                  <Point X="27.107728515625" Y="2.380952636719" />
                  <Point X="27.112677734375" Y="2.339919189453" />
                  <Point X="27.116099609375" Y="2.311527832031" />
                  <Point X="27.12144140625" Y="2.289607177734" />
                  <Point X="27.129708984375" Y="2.267514892578" />
                  <Point X="27.140072265625" Y="2.247469726562" />
                  <Point X="27.165462890625" Y="2.210051025391" />
                  <Point X="27.18303125" Y="2.184161132813" />
                  <Point X="27.19446484375" Y="2.170329589844" />
                  <Point X="27.221599609375" Y="2.145592529297" />
                  <Point X="27.259017578125" Y="2.120202392578" />
                  <Point X="27.284908203125" Y="2.102635009766" />
                  <Point X="27.304955078125" Y="2.092271728516" />
                  <Point X="27.32704296875" Y="2.084006103516" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.389998046875" Y="2.073715576172" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.47320703125" Y="2.074171142578" />
                  <Point X="27.52066015625" Y="2.086860839844" />
                  <Point X="27.553494140625" Y="2.095640869141" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.19884375" Y="2.462506591797" />
                  <Point X="28.967328125" Y="2.90619140625" />
                  <Point X="29.0301640625" Y="2.818864257813" />
                  <Point X="29.123271484375" Y="2.689467773438" />
                  <Point X="29.243890625" Y="2.490138427734" />
                  <Point X="29.26219921875" Y="2.459883300781" />
                  <Point X="28.90128125" Y="2.182941650391" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.22142578125" Y="1.6602421875" />
                  <Point X="28.203974609375" Y="1.641627075195" />
                  <Point X="28.169822265625" Y="1.597072998047" />
                  <Point X="28.146193359375" Y="1.566245849609" />
                  <Point X="28.13660546875" Y="1.550910888672" />
                  <Point X="28.1216328125" Y="1.517088623047" />
                  <Point X="28.10891015625" Y="1.471598754883" />
                  <Point X="28.100107421875" Y="1.440124267578" />
                  <Point X="28.09665234375" Y="1.417824707031" />
                  <Point X="28.0958359375" Y="1.394252563477" />
                  <Point X="28.09773828125" Y="1.371766845703" />
                  <Point X="28.108181640625" Y="1.321153320312" />
                  <Point X="28.115408203125" Y="1.286134155273" />
                  <Point X="28.1206796875" Y="1.268976806641" />
                  <Point X="28.136283203125" Y="1.235741333008" />
                  <Point X="28.1646875" Y="1.192567749023" />
                  <Point X="28.18433984375" Y="1.162695922852" />
                  <Point X="28.198890625" Y="1.145452026367" />
                  <Point X="28.216134765625" Y="1.129361328125" />
                  <Point X="28.234345703125" Y="1.116034667969" />
                  <Point X="28.2755078125" Y="1.092864013672" />
                  <Point X="28.30398828125" Y="1.07683215332" />
                  <Point X="28.3205234375" Y="1.069500366211" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.4117734375" Y="1.052083251953" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.06795703125" Y="1.123310302734" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.8059453125" Y="1.097077148437" />
                  <Point X="29.845939453125" Y="0.932788513184" />
                  <Point X="29.883947265625" Y="0.688673583984" />
                  <Point X="29.890865234375" Y="0.644238647461" />
                  <Point X="29.484984375" Y="0.535482971191" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.7047890625" Y="0.325585754395" />
                  <Point X="28.681546875" Y="0.315067901611" />
                  <Point X="28.626869140625" Y="0.283463012695" />
                  <Point X="28.589037109375" Y="0.261595489502" />
                  <Point X="28.574314453125" Y="0.251097351074" />
                  <Point X="28.54753125" Y="0.225576538086" />
                  <Point X="28.514724609375" Y="0.183772872925" />
                  <Point X="28.492025390625" Y="0.154848754883" />
                  <Point X="28.48030078125" Y="0.135567443848" />
                  <Point X="28.47052734375" Y="0.114103523254" />
                  <Point X="28.463681640625" Y="0.092603782654" />
                  <Point X="28.45274609375" Y="0.035502075195" />
                  <Point X="28.4451796875" Y="-0.004006527424" />
                  <Point X="28.443484375" Y="-0.02187528801" />
                  <Point X="28.4451796875" Y="-0.058553455353" />
                  <Point X="28.456115234375" Y="-0.115655166626" />
                  <Point X="28.463681640625" Y="-0.155163925171" />
                  <Point X="28.47052734375" Y="-0.176663650513" />
                  <Point X="28.48030078125" Y="-0.198127578735" />
                  <Point X="28.492025390625" Y="-0.217408889771" />
                  <Point X="28.52483203125" Y="-0.259212554932" />
                  <Point X="28.54753125" Y="-0.288136535645" />
                  <Point X="28.560001953125" Y="-0.301237243652" />
                  <Point X="28.589037109375" Y="-0.324155609131" />
                  <Point X="28.64371484375" Y="-0.355760498047" />
                  <Point X="28.681546875" Y="-0.377627868652" />
                  <Point X="28.692708984375" Y="-0.383138305664" />
                  <Point X="28.716580078125" Y="-0.392149963379" />
                  <Point X="29.248240234375" Y="-0.53460748291" />
                  <Point X="29.891474609375" Y="-0.706961853027" />
                  <Point X="29.877353515625" Y="-0.800623901367" />
                  <Point X="29.855025390625" Y="-0.948723754883" />
                  <Point X="29.806326171875" Y="-1.162126464844" />
                  <Point X="29.80117578125" Y="-1.184698974609" />
                  <Point X="29.315513671875" Y="-1.120760620117" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.267345703125" Y="-1.028834106445" />
                  <Point X="28.193095703125" Y="-1.04497265625" />
                  <Point X="28.1639765625" Y="-1.056596313477" />
                  <Point X="28.1361484375" Y="-1.073488891602" />
                  <Point X="28.1123984375" Y="-1.093959960937" />
                  <Point X="28.047533203125" Y="-1.171971679688" />
                  <Point X="28.002654296875" Y="-1.225947998047" />
                  <Point X="27.987931640625" Y="-1.250335205078" />
                  <Point X="27.97658984375" Y="-1.277720458984" />
                  <Point X="27.969759765625" Y="-1.305365722656" />
                  <Point X="27.960462890625" Y="-1.406394287109" />
                  <Point X="27.95403125" Y="-1.476296020508" />
                  <Point X="27.95634765625" Y="-1.507562011719" />
                  <Point X="27.964078125" Y="-1.539182739258" />
                  <Point X="27.976451171875" Y="-1.567996704102" />
                  <Point X="28.03583984375" Y="-1.660372314453" />
                  <Point X="28.076931640625" Y="-1.724287353516" />
                  <Point X="28.0869375" Y="-1.737243286133" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.604013671875" Y="-2.139495361328" />
                  <Point X="29.213123046875" Y="-2.6068828125" />
                  <Point X="29.18775390625" Y="-2.647934814453" />
                  <Point X="29.124806640625" Y="-2.749792724609" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="28.59462890625" Y="-2.635170898438" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.62650390625" Y="-2.136759033203" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.33873046875" Y="-2.191018798828" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246548828125" />
                  <Point X="27.22142578125" Y="-2.2675078125" />
                  <Point X="27.204533203125" Y="-2.290438476562" />
                  <Point X="27.14869140625" Y="-2.396542724609" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.100228515625" Y="-2.499735107422" />
                  <Point X="27.095271484375" Y="-2.531909423828" />
                  <Point X="27.09567578125" Y="-2.563259277344" />
                  <Point X="27.1187421875" Y="-2.690979736328" />
                  <Point X="27.134703125" Y="-2.779349609375" />
                  <Point X="27.13898828125" Y="-2.795141601562" />
                  <Point X="27.1518203125" Y="-2.826078369141" />
                  <Point X="27.4688671875" Y="-3.375221679688" />
                  <Point X="27.86128515625" Y="-4.054907470703" />
                  <Point X="27.85654296875" Y="-4.058294677734" />
                  <Point X="27.781865234375" Y="-4.111634277344" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="27.364193359375" Y="-3.723547607422" />
                  <Point X="26.758548828125" Y="-2.934254882812" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.595958984375" Y="-2.819572021484" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.279333984375" Y="-2.753794189453" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.156365234375" Y="-2.769396972656" />
                  <Point X="26.12898046875" Y="-2.780739746094" />
                  <Point X="26.10459765625" Y="-2.7954609375" />
                  <Point X="25.99821875" Y="-2.883912109375" />
                  <Point X="25.92461328125" Y="-2.945111328125" />
                  <Point X="25.904142578125" Y="-2.968860839844" />
                  <Point X="25.88725" Y="-2.9966875" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.843818359375" Y="-3.172145751953" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120361328" />
                  <Point X="25.909591796875" Y="-4.005591064453" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94158203125" Y="-4.752638183594" />
                  <Point X="23.858755859375" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575840332031" />
                  <Point X="23.879923828125" Y="-4.568098144531" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.509324707031" />
                  <Point X="23.876666015625" Y="-4.497689941406" />
                  <Point X="23.840890625" Y="-4.317837890625" />
                  <Point X="23.81613671875" Y="-4.1933984375" />
                  <Point X="23.811875" Y="-4.178469238281" />
                  <Point X="23.80097265625" Y="-4.149501464844" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.74979296875" Y="-4.070937744141" />
                  <Point X="23.738994140625" Y="-4.059779296875" />
                  <Point X="23.601125" Y="-3.93887109375" />
                  <Point X="23.505734375" Y="-3.855214599609" />
                  <Point X="23.49326171875" Y="-3.845964599609" />
                  <Point X="23.466978515625" Y="-3.82962109375" />
                  <Point X="23.45316796875" Y="-3.822527587891" />
                  <Point X="23.423470703125" Y="-3.810226318359" />
                  <Point X="23.4086875" Y="-3.805476318359" />
                  <Point X="23.378544921875" Y="-3.798447998047" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.180203125" Y="-3.784176269531" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812255859" />
                  <Point X="22.752091796875" Y="-3.917690429688" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629638672" />
                  <Point X="22.49755859375" Y="-4.161487792969" />
                  <Point X="22.496798828125" Y="-4.162478515625" />
                  <Point X="22.421701171875" Y="-4.11598046875" />
                  <Point X="22.25240625" Y="-4.011156494141" />
                  <Point X="22.02009375" Y="-3.832283447266" />
                  <Point X="22.019138671875" Y="-3.831548339844" />
                  <Point X="22.218513671875" Y="-3.486220214844" />
                  <Point X="22.658513671875" Y="-2.724119384766" />
                  <Point X="22.66515234375" Y="-2.710081054688" />
                  <Point X="22.676052734375" Y="-2.681117431641" />
                  <Point X="22.680314453125" Y="-2.666191894531" />
                  <Point X="22.6865859375" Y="-2.634667480469" />
                  <Point X="22.688361328125" Y="-2.619242919922" />
                  <Point X="22.689375" Y="-2.588310546875" />
                  <Point X="22.6853359375" Y="-2.557626708984" />
                  <Point X="22.676353515625" Y="-2.528010009766" />
                  <Point X="22.6706484375" Y="-2.513569335938" />
                  <Point X="22.65643359375" Y="-2.484740722656" />
                  <Point X="22.648453125" Y="-2.471423095703" />
                  <Point X="22.63042578125" Y="-2.446262695312" />
                  <Point X="22.62037890625" Y="-2.434419921875" />
                  <Point X="22.60303125" Y="-2.417071289062" />
                  <Point X="22.59119140625" Y="-2.407025878906" />
                  <Point X="22.56603515625" Y="-2.388998046875" />
                  <Point X="22.55271875" Y="-2.381015625" />
                  <Point X="22.523888671875" Y="-2.366796630859" />
                  <Point X="22.509447265625" Y="-2.361089355469" />
                  <Point X="22.479828125" Y="-2.352103515625" />
                  <Point X="22.449140625" Y="-2.348062744141" />
                  <Point X="22.418205078125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.78601953125" Y="-2.683360595703" />
                  <Point X="21.206912109375" Y="-3.017708496094" />
                  <Point X="21.1297265625" Y="-2.916303955078" />
                  <Point X="20.995978515625" Y="-2.740584716797" />
                  <Point X="20.829427734375" Y="-2.461306396484" />
                  <Point X="20.818734375" Y="-2.443374023438" />
                  <Point X="21.182021484375" Y="-2.16461328125" />
                  <Point X="21.951876953125" Y="-1.573881835938" />
                  <Point X="21.963517578125" Y="-1.563309082031" />
                  <Point X="21.984896484375" Y="-1.540386962891" />
                  <Point X="21.994634765625" Y="-1.528037597656" />
                  <Point X="22.012599609375" Y="-1.500904907227" />
                  <Point X="22.020166015625" Y="-1.48712097168" />
                  <Point X="22.032919921875" Y="-1.458495117188" />
                  <Point X="22.038107421875" Y="-1.443653198242" />
                  <Point X="22.0458125" Y="-1.413905883789" />
                  <Point X="22.048447265625" Y="-1.398804321289" />
                  <Point X="22.051251953125" Y="-1.368373535156" />
                  <Point X="22.051421875" Y="-1.353044433594" />
                  <Point X="22.049212890625" Y="-1.321374511719" />
                  <Point X="22.046916015625" Y="-1.306216796875" />
                  <Point X="22.0399140625" Y="-1.276471801758" />
                  <Point X="22.028220703125" Y="-1.248242431641" />
                  <Point X="22.012138671875" Y="-1.222259155273" />
                  <Point X="22.003044921875" Y="-1.20991796875" />
                  <Point X="21.982212890625" Y="-1.185962158203" />
                  <Point X="21.971255859375" Y="-1.175245849609" />
                  <Point X="21.9477578125" Y="-1.155712280273" />
                  <Point X="21.935216796875" Y="-1.146895141602" />
                  <Point X="21.908734375" Y="-1.13130859375" />
                  <Point X="21.89456640625" Y="-1.124481079102" />
                  <Point X="21.865302734375" Y="-1.113257080078" />
                  <Point X="21.85020703125" Y="-1.108860595703" />
                  <Point X="21.8183203125" Y="-1.10237890625" />
                  <Point X="21.802703125" Y="-1.100532470703" />
                  <Point X="21.77137890625" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="21.0900078125" Y="-1.187833129883" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.311548828125" Y="-1.17890637207" />
                  <Point X="20.259236328125" Y="-0.974112609863" />
                  <Point X="20.215173828125" Y="-0.666014526367" />
                  <Point X="20.213548828125" Y="-0.654654663086" />
                  <Point X="20.614732421875" Y="-0.547157836914" />
                  <Point X="21.491712890625" Y="-0.312171417236" />
                  <Point X="21.50362890625" Y="-0.30811505127" />
                  <Point X="21.526822265625" Y="-0.298461700439" />
                  <Point X="21.538099609375" Y="-0.29286517334" />
                  <Point X="21.570072265625" Y="-0.27425994873" />
                  <Point X="21.57687109375" Y="-0.269905609131" />
                  <Point X="21.596470703125" Y="-0.255729217529" />
                  <Point X="21.619638671875" Y="-0.236334365845" />
                  <Point X="21.628158203125" Y="-0.228255828857" />
                  <Point X="21.644134765625" Y="-0.211111282349" />
                  <Point X="21.657873046875" Y="-0.192131408691" />
                  <Point X="21.66916796875" Y="-0.171597366333" />
                  <Point X="21.674181640625" Y="-0.160977233887" />
                  <Point X="21.68707421875" Y="-0.128622177124" />
                  <Point X="21.689595703125" Y="-0.121478767395" />
                  <Point X="21.6960078125" Y="-0.099683670044" />
                  <Point X="21.702037109375" Y="-0.07267339325" />
                  <Point X="21.70378125" Y="-0.062060085297" />
                  <Point X="21.706060546875" Y="-0.040707290649" />
                  <Point X="21.705912109375" Y="-0.019235742569" />
                  <Point X="21.703337890625" Y="0.002083614111" />
                  <Point X="21.701447265625" Y="0.012671061516" />
                  <Point X="21.693734375" Y="0.045109489441" />
                  <Point X="21.69166796875" Y="0.052471076965" />
                  <Point X="21.684296875" Y="0.07417640686" />
                  <Point X="21.673087890625" Y="0.10110345459" />
                  <Point X="21.667912109375" Y="0.111646461487" />
                  <Point X="21.6563046875" Y="0.132005859375" />
                  <Point X="21.642279296875" Y="0.150772918701" />
                  <Point X="21.62604296875" Y="0.167671630859" />
                  <Point X="21.617400390625" Y="0.175619689941" />
                  <Point X="21.589177734375" Y="0.198522186279" />
                  <Point X="21.5826796875" Y="0.20335168457" />
                  <Point X="21.562400390625" Y="0.216702316284" />
                  <Point X="21.535482421875" Y="0.231799880981" />
                  <Point X="21.524818359375" Y="0.236935836792" />
                  <Point X="21.50293359375" Y="0.245841629028" />
                  <Point X="21.491712890625" Y="0.249611297607" />
                  <Point X="20.8849296875" Y="0.412198608398" />
                  <Point X="20.2145546875" Y="0.591824951172" />
                  <Point X="20.234958984375" Y="0.729715637207" />
                  <Point X="20.26866796875" Y="0.957520019531" />
                  <Point X="20.357375" Y="1.28487487793" />
                  <Point X="20.366416015625" Y="1.318237060547" />
                  <Point X="20.59329296875" Y="1.288367919922" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815551758" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.204703369141" />
                  <Point X="21.28485546875" Y="1.206589599609" />
                  <Point X="21.295109375" Y="1.208053344727" />
                  <Point X="21.3153984375" Y="1.212088867188" />
                  <Point X="21.32543359375" Y="1.214660766602" />
                  <Point X="21.36173828125" Y="1.226107910156" />
                  <Point X="21.386859375" Y="1.234028320312" />
                  <Point X="21.396552734375" Y="1.237677124023" />
                  <Point X="21.41548828125" Y="1.246009399414" />
                  <Point X="21.42473046875" Y="1.250692749023" />
                  <Point X="21.443474609375" Y="1.261515625" />
                  <Point X="21.4521484375" Y="1.267177001953" />
                  <Point X="21.468826171875" Y="1.279406982422" />
                  <Point X="21.484072265625" Y="1.293378295898" />
                  <Point X="21.497708984375" Y="1.308927978516" />
                  <Point X="21.504103515625" Y="1.317074951172" />
                  <Point X="21.51651953125" Y="1.334806274414" />
                  <Point X="21.521990234375" Y="1.343603393555" />
                  <Point X="21.531939453125" Y="1.361738647461" />
                  <Point X="21.53641796875" Y="1.371076538086" />
                  <Point X="21.550986328125" Y="1.406246704102" />
                  <Point X="21.561064453125" Y="1.430580932617" />
                  <Point X="21.564501953125" Y="1.440354614258" />
                  <Point X="21.57029296875" Y="1.460223999023" />
                  <Point X="21.572646484375" Y="1.470319702148" />
                  <Point X="21.57640234375" Y="1.491633666992" />
                  <Point X="21.577640625" Y="1.501920043945" />
                  <Point X="21.578990234375" Y="1.522555664062" />
                  <Point X="21.578087890625" Y="1.543201416016" />
                  <Point X="21.574943359375" Y="1.563640380859" />
                  <Point X="21.5728125" Y="1.573782470703" />
                  <Point X="21.5672109375" Y="1.594687866211" />
                  <Point X="21.563986328125" Y="1.60453503418" />
                  <Point X="21.55648828125" Y="1.623813842773" />
                  <Point X="21.55221484375" Y="1.633245605469" />
                  <Point X="21.53463671875" Y="1.667011962891" />
                  <Point X="21.522474609375" Y="1.690375244141" />
                  <Point X="21.51719921875" Y="1.699285766602" />
                  <Point X="21.505708984375" Y="1.716482910156" />
                  <Point X="21.499494140625" Y="1.72476940918" />
                  <Point X="21.48558203125" Y="1.741349609375" />
                  <Point X="21.4785" Y="1.748911254883" />
                  <Point X="21.463556640625" Y="1.763215942383" />
                  <Point X="21.4556953125" Y="1.769958862305" />
                  <Point X="21.107642578125" Y="2.037029174805" />
                  <Point X="20.77238671875" Y="2.294281005859" />
                  <Point X="20.86672265625" Y="2.45590234375" />
                  <Point X="20.99771484375" Y="2.680322509766" />
                  <Point X="21.23267578125" Y="2.982333007812" />
                  <Point X="21.273662109375" Y="3.035013671875" />
                  <Point X="21.35975" Y="2.985310302734" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.8954921875" Y="2.726734130859" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.99329296875" Y="2.727400634766" />
                  <Point X="22.00347265625" Y="2.729310302734" />
                  <Point X="22.023564453125" Y="2.7342265625" />
                  <Point X="22.043001953125" Y="2.741301269531" />
                  <Point X="22.061552734375" Y="2.750449462891" />
                  <Point X="22.070578125" Y="2.755529541016" />
                  <Point X="22.08883203125" Y="2.767158203125" />
                  <Point X="22.097251953125" Y="2.773191650391" />
                  <Point X="22.113384765625" Y="2.786139404297" />
                  <Point X="22.12109765625" Y="2.793053710938" />
                  <Point X="22.15698828125" Y="2.828944335938" />
                  <Point X="22.181822265625" Y="2.853777099609" />
                  <Point X="22.188734375" Y="2.861488525391" />
                  <Point X="22.201677734375" Y="2.877615234375" />
                  <Point X="22.207708984375" Y="2.886030517578" />
                  <Point X="22.21933984375" Y="2.904284912109" />
                  <Point X="22.224419921875" Y="2.913309570313" />
                  <Point X="22.2335703125" Y="2.931861328125" />
                  <Point X="22.240646484375" Y="2.951298828125" />
                  <Point X="22.245564453125" Y="2.971391113281" />
                  <Point X="22.2474765625" Y="2.981572998047" />
                  <Point X="22.250302734375" Y="3.003032226562" />
                  <Point X="22.251091796875" Y="3.013364746094" />
                  <Point X="22.25154296875" Y="3.034048828125" />
                  <Point X="22.251205078125" Y="3.044400390625" />
                  <Point X="22.24678125" Y="3.094964599609" />
                  <Point X="22.243720703125" Y="3.129949951172" />
                  <Point X="22.242255859375" Y="3.14020703125" />
                  <Point X="22.23821875" Y="3.160499267578" />
                  <Point X="22.235646484375" Y="3.170534423828" />
                  <Point X="22.22913671875" Y="3.191176757812" />
                  <Point X="22.22548828125" Y="3.200870849609" />
                  <Point X="22.217158203125" Y="3.219799316406" />
                  <Point X="22.2124765625" Y="3.229033691406" />
                  <Point X="22.0582421875" Y="3.496172607422" />
                  <Point X="21.94061328125" Y="3.699914306641" />
                  <Point X="22.1235" Y="3.840131835938" />
                  <Point X="22.351634765625" Y="4.015041748047" />
                  <Point X="22.72169140625" Y="4.220635742188" />
                  <Point X="22.8074765625" Y="4.268296386719" />
                  <Point X="22.88143359375" Y="4.171913574219" />
                  <Point X="22.888177734375" Y="4.164050292969" />
                  <Point X="22.90248046875" Y="4.149108398438" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93490625" Y="4.121898925781" />
                  <Point X="22.95210546875" Y="4.11040625" />
                  <Point X="22.96101953125" Y="4.105128417969" />
                  <Point X="23.017296875" Y="4.075832275391" />
                  <Point X="23.056236328125" Y="4.055561767578" />
                  <Point X="23.065673828125" Y="4.051285644531" />
                  <Point X="23.084953125" Y="4.0437890625" />
                  <Point X="23.094794921875" Y="4.040568359375" />
                  <Point X="23.115701171875" Y="4.034966308594" />
                  <Point X="23.1258359375" Y="4.032834960938" />
                  <Point X="23.146279296875" Y="4.029687988281" />
                  <Point X="23.166947265625" Y="4.028785400391" />
                  <Point X="23.187587890625" Y="4.030138427734" />
                  <Point X="23.197869140625" Y="4.031378417969" />
                  <Point X="23.21918359375" Y="4.03513671875" />
                  <Point X="23.229271484375" Y="4.037488769531" />
                  <Point X="23.249130859375" Y="4.04327734375" />
                  <Point X="23.25890234375" Y="4.046713867188" />
                  <Point X="23.317517578125" Y="4.070994140625" />
                  <Point X="23.358076171875" Y="4.087793457031" />
                  <Point X="23.3674140625" Y="4.092271972656" />
                  <Point X="23.385546875" Y="4.102219726562" />
                  <Point X="23.394341796875" Y="4.107688964844" />
                  <Point X="23.412072265625" Y="4.120103515625" />
                  <Point X="23.42022265625" Y="4.126499023438" />
                  <Point X="23.4357734375" Y="4.14013671875" />
                  <Point X="23.449744140625" Y="4.155382324219" />
                  <Point X="23.4619765625" Y="4.172062988281" />
                  <Point X="23.467638671875" Y="4.180740234375" />
                  <Point X="23.4784609375" Y="4.199483886719" />
                  <Point X="23.48314453125" Y="4.208724121094" />
                  <Point X="23.4914765625" Y="4.227659667969" />
                  <Point X="23.495125" Y="4.237354980469" />
                  <Point X="23.514203125" Y="4.297864746094" />
                  <Point X="23.527404296875" Y="4.339731445312" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370049316406" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401865234375" />
                  <Point X="23.53769921875" Y="4.412217773438" />
                  <Point X="23.537248046875" Y="4.432898925781" />
                  <Point X="23.536458984375" Y="4.443227539062" />
                  <Point X="23.520736328125" Y="4.562655273438" />
                  <Point X="23.7735" Y="4.633521972656" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.517447265625" Y="4.768824707031" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.660115234375" Y="4.687993164062" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.3170078125" Y="4.556661132812" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.57000390625" Y="4.764918457031" />
                  <Point X="25.827876953125" Y="4.737912109375" />
                  <Point X="26.199044921875" Y="4.648301269531" />
                  <Point X="26.453595703125" Y="4.58684375" />
                  <Point X="26.69361328125" Y="4.499788574219" />
                  <Point X="26.8582578125" Y="4.440070800781" />
                  <Point X="27.091873046875" Y="4.330816894531" />
                  <Point X="27.250453125" Y="4.256653808594" />
                  <Point X="27.4761875" Y="4.125141113281" />
                  <Point X="27.62943359375" Y="4.035859130859" />
                  <Point X="27.817783203125" Y="3.901915527344" />
                  <Point X="27.574578125" Y="3.480672851563" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.0623828125" Y="2.593118164062" />
                  <Point X="27.053185546875" Y="2.573447753906" />
                  <Point X="27.04418359375" Y="2.549567626953" />
                  <Point X="27.041302734375" Y="2.540599365234" />
                  <Point X="27.02861328125" Y="2.493145996094" />
                  <Point X="27.01983203125" Y="2.460312988281" />
                  <Point X="27.0179140625" Y="2.451470947266" />
                  <Point X="27.013646484375" Y="2.420224609375" />
                  <Point X="27.012755859375" Y="2.383239746094" />
                  <Point X="27.013412109375" Y="2.369576660156" />
                  <Point X="27.018361328125" Y="2.328543212891" />
                  <Point X="27.021783203125" Y="2.300151855469" />
                  <Point X="27.02380078125" Y="2.289035644531" />
                  <Point X="27.029142578125" Y="2.267114990234" />
                  <Point X="27.032466796875" Y="2.256310546875" />
                  <Point X="27.040734375" Y="2.234218261719" />
                  <Point X="27.0453203125" Y="2.223885986328" />
                  <Point X="27.05568359375" Y="2.203840820312" />
                  <Point X="27.0614609375" Y="2.194127929688" />
                  <Point X="27.0868515625" Y="2.156709228516" />
                  <Point X="27.104419921875" Y="2.130819335937" />
                  <Point X="27.10980859375" Y="2.123633789062" />
                  <Point X="27.130462890625" Y="2.100124267578" />
                  <Point X="27.15759765625" Y="2.075387207031" />
                  <Point X="27.1682578125" Y="2.066981689453" />
                  <Point X="27.20567578125" Y="2.041591552734" />
                  <Point X="27.23156640625" Y="2.024024169922" />
                  <Point X="27.241283203125" Y="2.018244384766" />
                  <Point X="27.261330078125" Y="2.007881103516" />
                  <Point X="27.27166015625" Y="2.003297363281" />
                  <Point X="27.293748046875" Y="1.995031860352" />
                  <Point X="27.304548828125" Y="1.991707519531" />
                  <Point X="27.326470703125" Y="1.986364990234" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.378625" Y="1.979398925781" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.416044921875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975497314453" />
                  <Point X="27.484314453125" Y="1.979822753906" />
                  <Point X="27.49775" Y="1.982395996094" />
                  <Point X="27.545203125" Y="1.995085693359" />
                  <Point X="27.578037109375" Y="2.003865722656" />
                  <Point X="27.584" Y="2.005671875" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.24634375" Y="2.380234130859" />
                  <Point X="28.94040625" Y="2.780951416016" />
                  <Point X="28.95305078125" Y="2.763378417969" />
                  <Point X="29.04395703125" Y="2.637041503906" />
                  <Point X="29.136884765625" Y="2.483471679688" />
                  <Point X="28.84344921875" Y="2.258310302734" />
                  <Point X="28.172953125" Y="1.743820068359" />
                  <Point X="28.168142578125" Y="1.739871459961" />
                  <Point X="28.152119140625" Y="1.725215576172" />
                  <Point X="28.13466796875" Y="1.706600341797" />
                  <Point X="28.128578125" Y="1.699421875" />
                  <Point X="28.09442578125" Y="1.654867797852" />
                  <Point X="28.070796875" Y="1.624040649414" />
                  <Point X="28.065642578125" Y="1.616609130859" />
                  <Point X="28.049736328125" Y="1.589366577148" />
                  <Point X="28.034763671875" Y="1.555544067383" />
                  <Point X="28.03014453125" Y="1.542676391602" />
                  <Point X="28.017421875" Y="1.497186401367" />
                  <Point X="28.008619140625" Y="1.465712036133" />
                  <Point X="28.006228515625" Y="1.454669921875" />
                  <Point X="28.0027734375" Y="1.432370361328" />
                  <Point X="28.001708984375" Y="1.421113037109" />
                  <Point X="28.000892578125" Y="1.397540893555" />
                  <Point X="28.001173828125" Y="1.386243896484" />
                  <Point X="28.003076171875" Y="1.363758178711" />
                  <Point X="28.004697265625" Y="1.352569213867" />
                  <Point X="28.015140625" Y="1.301955810547" />
                  <Point X="28.0223671875" Y="1.266936645508" />
                  <Point X="28.02459765625" Y="1.258233276367" />
                  <Point X="28.034685546875" Y="1.228603881836" />
                  <Point X="28.0502890625" Y="1.195368530273" />
                  <Point X="28.056919921875" Y="1.183526977539" />
                  <Point X="28.08532421875" Y="1.140353393555" />
                  <Point X="28.1049765625" Y="1.110481689453" />
                  <Point X="28.111734375" Y="1.101430175781" />
                  <Point X="28.12628515625" Y="1.084186279297" />
                  <Point X="28.134078125" Y="1.075994018555" />
                  <Point X="28.151322265625" Y="1.059903320313" />
                  <Point X="28.16003125" Y="1.052696655273" />
                  <Point X="28.1782421875" Y="1.039369995117" />
                  <Point X="28.187744140625" Y="1.033249633789" />
                  <Point X="28.22890625" Y="1.010078857422" />
                  <Point X="28.25738671875" Y="0.994047058105" />
                  <Point X="28.26548046875" Y="0.989986694336" />
                  <Point X="28.29468359375" Y="0.978082275391" />
                  <Point X="28.330279296875" Y="0.968020690918" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.399326171875" Y="0.957902282715" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199768066" />
                  <Point X="28.46571875" Y="0.951222839355" />
                  <Point X="28.49121875" Y="0.952032409668" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="29.080357421875" Y="1.029123046875" />
                  <Point X="29.704703125" Y="1.111319702148" />
                  <Point X="29.713640625" Y="1.074606689453" />
                  <Point X="29.7526875" Y="0.914209899902" />
                  <Point X="29.78387109375" Y="0.713921081543" />
                  <Point X="29.460396484375" Y="0.62724597168" />
                  <Point X="28.6919921875" Y="0.421352752686" />
                  <Point X="28.686033203125" Y="0.419544616699" />
                  <Point X="28.665623046875" Y="0.412136047363" />
                  <Point X="28.642380859375" Y="0.401618164062" />
                  <Point X="28.634005859375" Y="0.39731640625" />
                  <Point X="28.579328125" Y="0.365711456299" />
                  <Point X="28.54149609375" Y="0.343843994141" />
                  <Point X="28.5338828125" Y="0.338944946289" />
                  <Point X="28.508779296875" Y="0.319873809814" />
                  <Point X="28.48199609375" Y="0.294353027344" />
                  <Point X="28.472796875" Y="0.284226287842" />
                  <Point X="28.439990234375" Y="0.242422698975" />
                  <Point X="28.417291015625" Y="0.21349861145" />
                  <Point X="28.410853515625" Y="0.204207305908" />
                  <Point X="28.39912890625" Y="0.184926010132" />
                  <Point X="28.393841796875" Y="0.174935882568" />
                  <Point X="28.384068359375" Y="0.153471908569" />
                  <Point X="28.380005859375" Y="0.142926528931" />
                  <Point X="28.37316015625" Y="0.121426742554" />
                  <Point X="28.370376953125" Y="0.110472488403" />
                  <Point X="28.35944140625" Y="0.05337084198" />
                  <Point X="28.351875" Y="0.013862125397" />
                  <Point X="28.350603515625" Y="0.004966303349" />
                  <Point X="28.3485859375" Y="-0.026261590958" />
                  <Point X="28.35028125" Y="-0.062939785004" />
                  <Point X="28.351875" Y="-0.076422111511" />
                  <Point X="28.362810546875" Y="-0.133523910522" />
                  <Point X="28.370376953125" Y="-0.173032623291" />
                  <Point X="28.37316015625" Y="-0.183986877441" />
                  <Point X="28.380005859375" Y="-0.20548664856" />
                  <Point X="28.384068359375" Y="-0.216032043457" />
                  <Point X="28.393841796875" Y="-0.237496017456" />
                  <Point X="28.39912890625" Y="-0.24748614502" />
                  <Point X="28.410853515625" Y="-0.266767425537" />
                  <Point X="28.417291015625" Y="-0.276058746338" />
                  <Point X="28.45009765625" Y="-0.317862335205" />
                  <Point X="28.472796875" Y="-0.3467862854" />
                  <Point X="28.47872265625" Y="-0.353636749268" />
                  <Point X="28.501142578125" Y="-0.375806243896" />
                  <Point X="28.530177734375" Y="-0.398724639893" />
                  <Point X="28.54149609375" Y="-0.40640411377" />
                  <Point X="28.596173828125" Y="-0.438009063721" />
                  <Point X="28.634005859375" Y="-0.459876403809" />
                  <Point X="28.639494140625" Y="-0.462812896729" />
                  <Point X="28.65915625" Y="-0.472015930176" />
                  <Point X="28.68302734375" Y="-0.481027526855" />
                  <Point X="28.6919921875" Y="-0.483912872314" />
                  <Point X="29.22365234375" Y="-0.626370422363" />
                  <Point X="29.784880859375" Y="-0.776751098633" />
                  <Point X="29.783416015625" Y="-0.786461181641" />
                  <Point X="29.761619140625" Y="-0.931035583496" />
                  <Point X="29.727802734375" Y="-1.079219604492" />
                  <Point X="29.3279140625" Y="-1.026573364258" />
                  <Point X="28.436783203125" Y="-0.909253540039" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042419434" />
                  <Point X="28.36672265625" Y="-0.910840820312" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.24716796875" Y="-0.936001647949" />
                  <Point X="28.17291796875" Y="-0.952140197754" />
                  <Point X="28.157876953125" Y="-0.95674230957" />
                  <Point X="28.1287578125" Y="-0.968365905762" />
                  <Point X="28.1146796875" Y="-0.975387451172" />
                  <Point X="28.0868515625" Y="-0.992280090332" />
                  <Point X="28.074125" Y="-1.001530395508" />
                  <Point X="28.050375" Y="-1.022001403809" />
                  <Point X="28.0393515625" Y="-1.033222290039" />
                  <Point X="27.974486328125" Y="-1.111234008789" />
                  <Point X="27.929607421875" Y="-1.165210327148" />
                  <Point X="27.921326171875" Y="-1.176849609375" />
                  <Point X="27.906603515625" Y="-1.201236816406" />
                  <Point X="27.900162109375" Y="-1.21398449707" />
                  <Point X="27.8888203125" Y="-1.241369750977" />
                  <Point X="27.88436328125" Y="-1.254934692383" />
                  <Point X="27.877533203125" Y="-1.282579956055" />
                  <Point X="27.87516015625" Y="-1.296660522461" />
                  <Point X="27.86586328125" Y="-1.397688964844" />
                  <Point X="27.859431640625" Y="-1.467590698242" />
                  <Point X="27.859291015625" Y="-1.483315063477" />
                  <Point X="27.861607421875" Y="-1.514580932617" />
                  <Point X="27.864064453125" Y="-1.530122680664" />
                  <Point X="27.871794921875" Y="-1.561743286133" />
                  <Point X="27.87678515625" Y="-1.576666992188" />
                  <Point X="27.889158203125" Y="-1.605480957031" />
                  <Point X="27.896541015625" Y="-1.619371459961" />
                  <Point X="27.9559296875" Y="-1.711746948242" />
                  <Point X="27.997021484375" Y="-1.775661987305" />
                  <Point X="28.001744140625" Y="-1.782354614258" />
                  <Point X="28.01980078125" Y="-1.804457397461" />
                  <Point X="28.043494140625" Y="-1.828123291016" />
                  <Point X="28.052798828125" Y="-1.836277709961" />
                  <Point X="28.546181640625" Y="-2.214864013672" />
                  <Point X="29.087171875" Y="-2.629981201172" />
                  <Point X="29.045474609375" Y="-2.697451171875" />
                  <Point X="29.001275390625" Y="-2.760252441406" />
                  <Point X="28.64212890625" Y="-2.5528984375" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.64338671875" Y="-2.043271362305" />
                  <Point X="27.555017578125" Y="-2.027311889648" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.03513659668" />
                  <Point X="27.415068359375" Y="-2.044960571289" />
                  <Point X="27.40058984375" Y="-2.051109130859" />
                  <Point X="27.294486328125" Y="-2.106950927734" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.208970703125" Y="-2.153168945313" />
                  <Point X="27.1860390625" Y="-2.170061767578" />
                  <Point X="27.175208984375" Y="-2.179373779297" />
                  <Point X="27.15425" Y="-2.200332763672" />
                  <Point X="27.144939453125" Y="-2.211161865234" />
                  <Point X="27.128046875" Y="-2.234092529297" />
                  <Point X="27.12046484375" Y="-2.246194091797" />
                  <Point X="27.064623046875" Y="-2.352298339844" />
                  <Point X="27.025984375" Y="-2.425712158203" />
                  <Point X="27.0198359375" Y="-2.440193115234" />
                  <Point X="27.01001171875" Y="-2.469971679688" />
                  <Point X="27.0063359375" Y="-2.485269287109" />
                  <Point X="27.00137890625" Y="-2.517443603516" />
                  <Point X="27.000279296875" Y="-2.533134521484" />
                  <Point X="27.00068359375" Y="-2.564484375" />
                  <Point X="27.0021875" Y="-2.580143310547" />
                  <Point X="27.02525390625" Y="-2.707863769531" />
                  <Point X="27.04121484375" Y="-2.796233642578" />
                  <Point X="27.04301953125" Y="-2.804228271484" />
                  <Point X="27.05123828125" Y="-2.8315390625" />
                  <Point X="27.0640703125" Y="-2.862475830078" />
                  <Point X="27.069546875" Y="-2.873578125" />
                  <Point X="27.38659375" Y="-3.422721435547" />
                  <Point X="27.73589453125" Y="-4.027725341797" />
                  <Point X="27.723755859375" Y="-4.036082763672" />
                  <Point X="27.4395625" Y="-3.665715087891" />
                  <Point X="26.83391796875" Y="-2.876422363281" />
                  <Point X="26.82865234375" Y="-2.870141113281" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.647333984375" Y="-2.739661865234" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.27062890625" Y="-2.659193847656" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670339111328" />
                  <Point X="26.133578125" Y="-2.677170410156" />
                  <Point X="26.12001171875" Y="-2.681627929688" />
                  <Point X="26.092626953125" Y="-2.692970703125" />
                  <Point X="26.07987890625" Y="-2.699412841797" />
                  <Point X="26.05549609375" Y="-2.714134033203" />
                  <Point X="26.043861328125" Y="-2.722413085938" />
                  <Point X="25.937482421875" Y="-2.810864257813" />
                  <Point X="25.863876953125" Y="-2.872063476562" />
                  <Point X="25.852654296875" Y="-2.883087402344" />
                  <Point X="25.83218359375" Y="-2.906836914062" />
                  <Point X="25.822935546875" Y="-2.9195625" />
                  <Point X="25.80604296875" Y="-2.947389160156" />
                  <Point X="25.79901953125" Y="-2.961466552734" />
                  <Point X="25.78739453125" Y="-2.990587646484" />
                  <Point X="25.78279296875" Y="-3.005631347656" />
                  <Point X="25.750986328125" Y="-3.151968505859" />
                  <Point X="25.728978515625" Y="-3.253219238281" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323170166016" />
                  <Point X="25.7255546875" Y="-3.335520507812" />
                  <Point X="25.815404296875" Y="-4.017991210938" />
                  <Point X="25.833087890625" Y="-4.152317871094" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480121337891" />
                  <Point X="25.642146484375" Y="-3.453579589844" />
                  <Point X="25.6267890625" Y="-3.423815917969" />
                  <Point X="25.62041015625" Y="-3.413209960938" />
                  <Point X="25.523638671875" Y="-3.273780761719" />
                  <Point X="25.456681640625" Y="-3.177309570312" />
                  <Point X="25.446671875" Y="-3.165172851562" />
                  <Point X="25.424787109375" Y="-3.142716552734" />
                  <Point X="25.412912109375" Y="-3.132396972656" />
                  <Point X="25.38665625" Y="-3.113153320312" />
                  <Point X="25.3732421875" Y="-3.104936523438" />
                  <Point X="25.3452421875" Y="-3.090829101562" />
                  <Point X="25.33065625" Y="-3.084938476562" />
                  <Point X="25.180908203125" Y="-3.038462158203" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.063376953125" Y="-3.003109130859" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.785267578125" Y="-3.05278125" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.6670703125" Y="-3.090829101562" />
                  <Point X="24.6390703125" Y="-3.104936523438" />
                  <Point X="24.62565625" Y="-3.113153320312" />
                  <Point X="24.599400390625" Y="-3.132396972656" />
                  <Point X="24.5875234375" Y="-3.142717529297" />
                  <Point X="24.565638671875" Y="-3.165174560547" />
                  <Point X="24.555630859375" Y="-3.177311035156" />
                  <Point X="24.458861328125" Y="-3.316740234375" />
                  <Point X="24.391904296875" Y="-3.413211425781" />
                  <Point X="24.387529296875" Y="-3.4201328125" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213134766" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.193427734375" Y="-4.099310546875" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.060865883485" Y="2.425140245888" />
                  <Point X="28.876680223504" Y="2.744159167002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.984847001346" Y="2.366808812088" />
                  <Point X="28.794407815993" Y="2.696659156871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.767043104383" Y="0.822005420146" />
                  <Point X="29.60740315652" Y="1.098509920761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.908828119206" Y="2.308477378289" />
                  <Point X="28.712135408482" Y="2.649159146741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.728339826765" Y="0.699041463399" />
                  <Point X="29.505455597555" Y="1.085088272597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.832809234894" Y="2.250145948253" />
                  <Point X="28.629863000971" Y="2.601659136611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.633339878088" Y="0.673586201224" />
                  <Point X="29.40350803859" Y="1.071666624432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.756790337229" Y="2.191814541342" />
                  <Point X="28.547590593461" Y="2.554159126481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.793623112746" Y="3.860069110333" />
                  <Point X="27.735804240115" Y="3.960214335366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.538339929411" Y="0.648130939049" />
                  <Point X="29.301560479624" Y="1.058244976268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.680771439565" Y="2.133483134432" />
                  <Point X="28.46531818595" Y="2.50665911635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.73877478013" Y="3.765069209133" />
                  <Point X="27.558612115768" Y="4.077120097437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.4433399705" Y="0.622675694599" />
                  <Point X="29.199612920659" Y="1.044823328104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.604752541901" Y="2.075151727522" />
                  <Point X="28.383045778439" Y="2.45915910622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.683926447515" Y="3.670069307934" />
                  <Point X="27.393315456002" Y="4.173422310473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.348339964825" Y="0.597220531149" />
                  <Point X="29.097665361694" Y="1.031401679939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.528733644237" Y="2.016820320612" />
                  <Point X="28.300773370928" Y="2.41165909609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.629078114899" Y="3.575069406734" />
                  <Point X="27.230058419093" Y="4.266191793092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.253339959149" Y="0.571765367698" />
                  <Point X="28.995717804907" Y="1.017980028001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.452714746572" Y="1.958488913702" />
                  <Point X="28.218500956329" Y="2.364159098237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.574229782846" Y="3.480069504561" />
                  <Point X="27.079787314736" Y="4.336468980747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.158339953474" Y="0.546310204248" />
                  <Point X="28.893770248567" Y="1.00455837529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.376695848908" Y="1.900157506792" />
                  <Point X="28.136228527873" Y="2.316659124385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.519381538803" Y="3.38506944995" />
                  <Point X="26.929516604848" Y="4.406745485164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.063339947798" Y="0.520855040797" />
                  <Point X="28.791822692226" Y="0.991136722579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.300676951244" Y="1.841826099882" />
                  <Point X="28.053956099417" Y="2.269159150533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.46453329476" Y="3.290069395339" />
                  <Point X="26.785302074035" Y="4.466532379721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.767887699378" Y="-0.889457461299" />
                  <Point X="29.687797864026" Y="-0.750737797298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.968339942122" Y="0.495399877347" />
                  <Point X="28.689875135886" Y="0.977715069869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.22465805358" Y="1.783494692971" />
                  <Point X="27.971683670961" Y="2.221659176681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.409685050716" Y="3.195069340728" />
                  <Point X="26.646549589858" Y="4.516858731993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.739119480737" Y="-1.029629444968" />
                  <Point X="29.558025458762" Y="-0.71596539796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.873339936447" Y="0.469944713896" />
                  <Point X="28.587927579545" Y="0.964293417158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.149946817248" Y="1.722898350195" />
                  <Point X="27.889411242505" Y="2.174159202828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.354836806673" Y="3.100069286117" />
                  <Point X="26.507797371262" Y="4.567184624264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.652316125348" Y="-1.069281623167" />
                  <Point X="29.428253053498" Y="-0.681192998623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.778339930771" Y="0.444489550446" />
                  <Point X="28.485416281491" Y="0.951848193737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.085928302105" Y="1.643781671047" />
                  <Point X="27.807138814049" Y="2.126659228976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.299988562629" Y="3.005069231506" />
                  <Point X="26.37592371156" Y="4.605596503047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.533595699013" Y="-1.053651812859" />
                  <Point X="29.298480648235" Y="-0.646420599286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.683563018401" Y="0.418647978056" />
                  <Point X="28.36998561669" Y="0.961779969925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.031854575668" Y="1.54744011259" />
                  <Point X="27.724866385593" Y="2.079159255124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.245140318586" Y="2.910069176895" />
                  <Point X="26.248459595788" Y="4.636370827707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.414875272679" Y="-1.038022002552" />
                  <Point X="29.168708264916" Y="-0.61164823796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.598148591265" Y="0.376590105554" />
                  <Point X="28.234087591507" Y="1.007162254189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.001334562264" Y="1.410302326454" />
                  <Point X="27.642593957137" Y="2.031659281272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.190292074543" Y="2.815069122284" />
                  <Point X="26.120995786702" Y="4.667144621169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.296154836355" Y="-1.022392174941" />
                  <Point X="29.038935911486" Y="-0.5768759284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.517411161542" Y="0.326431435908" />
                  <Point X="27.55283492356" Y="1.997126487866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.135443830499" Y="2.720069067673" />
                  <Point X="25.993532171786" Y="4.69791807832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.177434372677" Y="-1.006762299954" />
                  <Point X="28.909163558055" Y="-0.54210361884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.449339197083" Y="0.254335536922" />
                  <Point X="27.455113522723" Y="1.976384919103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.080595586456" Y="2.625069013063" />
                  <Point X="25.86606855687" Y="4.728691535471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.058713908999" Y="-0.991132424966" />
                  <Point X="28.779391204624" Y="-0.50733130928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.390156645653" Y="0.166842722919" />
                  <Point X="27.341061748232" Y="1.983928387214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.034454125691" Y="2.514988367442" />
                  <Point X="25.746104167882" Y="4.746475952296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.939993445322" Y="-0.975502549978" />
                  <Point X="28.645657481257" Y="-0.465697705724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.356086653772" Y="0.035853679871" />
                  <Point X="27.19317664493" Y="2.050072899815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.014653555666" Y="2.359283960745" />
                  <Point X="25.629348034891" Y="4.758703506734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.821272981644" Y="-0.95987267499" />
                  <Point X="28.452028926937" Y="-0.320323211846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.373038373715" Y="-0.183507560347" />
                  <Point X="25.512591885317" Y="4.770931089891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.702552517966" Y="-0.944242800002" />
                  <Point X="25.395835718603" Y="4.783158702736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.583832054289" Y="-0.928612925014" />
                  <Point X="25.348674999401" Y="4.674843464517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.465111590611" Y="-0.912983050027" />
                  <Point X="25.313902359628" Y="4.545071443316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.355177693365" Y="-0.912571954522" />
                  <Point X="25.279129908557" Y="4.415299095275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.257688104502" Y="-0.933715033403" />
                  <Point X="25.244357457486" Y="4.285526747234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.16077449918" Y="-0.955855745041" />
                  <Point X="25.195545155525" Y="4.180072134264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.051220600905" Y="-2.688153634629" />
                  <Point X="28.962325733085" Y="-2.534183207032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.076465877227" Y="-0.999828928302" />
                  <Point X="25.122718564168" Y="4.116211490637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.974087732704" Y="-2.744555587972" />
                  <Point X="28.765377881007" Y="-2.383059520792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.00789281546" Y="-1.07105690129" />
                  <Point X="25.030163383194" Y="4.086521766587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.640380105537" Y="4.761646207431" />
                  <Point X="24.628717218648" Y="4.781846920084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.809542634403" Y="-2.649555117578" />
                  <Point X="28.568430028929" Y="-2.231935834552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.943151184015" Y="-1.148921106262" />
                  <Point X="24.908749927644" Y="4.106816040323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.735378934167" Y="4.407103409584" />
                  <Point X="24.52596360669" Y="4.769821396657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.644997536102" Y="-2.554554647184" />
                  <Point X="28.371482307324" Y="-2.080812374296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.888098399799" Y="-1.243566886903" />
                  <Point X="24.423210122875" Y="4.757795651279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.480452707315" Y="-2.459554643602" />
                  <Point X="28.174534602334" Y="-1.92968894282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.866072380231" Y="-1.395416701922" />
                  <Point X="24.32045665064" Y="4.745769885844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.315907883309" Y="-2.364554648302" />
                  <Point X="24.217703178405" Y="4.733744120409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.151363059304" Y="-2.269554653002" />
                  <Point X="24.11494970617" Y="4.721718354974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.986818235298" Y="-2.174554657702" />
                  <Point X="24.016792049448" Y="4.701732403569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.824668896464" Y="-2.083703764427" />
                  <Point X="23.922378133908" Y="4.675262102227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.697244377809" Y="-2.052998023987" />
                  <Point X="23.827964218368" Y="4.648791800884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.574778378837" Y="-2.030880691567" />
                  <Point X="23.733550362713" Y="4.622321395815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.464985909782" Y="-2.030714556875" />
                  <Point X="23.639136588703" Y="4.595850849335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.374877148985" Y="-2.064641604969" />
                  <Point X="23.544722814693" Y="4.569380302855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.290744782609" Y="-2.108920071844" />
                  <Point X="23.53662166228" Y="4.393411910434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.207304138488" Y="-2.15439663681" />
                  <Point X="23.502860178643" Y="4.261888515433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.136777601176" Y="-2.222241090904" />
                  <Point X="23.452491785121" Y="4.159129132109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.082527822026" Y="-2.318277717117" />
                  <Point X="23.378032103238" Y="4.098097084245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.03021628902" Y="-2.417671484129" />
                  <Point X="23.29045551575" Y="4.059784183329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.000578527665" Y="-2.556337375639" />
                  <Point X="23.197205267965" Y="4.03129835031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.054985171476" Y="-2.840572446989" />
                  <Point X="23.078949373711" Y="4.046123567452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.367353632487" Y="-3.571610492143" />
                  <Point X="22.917494026988" Y="4.13577243113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.924280502601" Y="-2.994185319712" />
                  <Point X="22.749788918475" Y="4.236246199764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.679618086706" Y="-2.760417584679" />
                  <Point X="22.666733442176" Y="4.19010250456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.513395040909" Y="-2.662510823969" />
                  <Point X="22.583677878788" Y="4.1439589602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.395166853717" Y="-2.647733596866" />
                  <Point X="22.5006223154" Y="4.097815415839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.291004353919" Y="-2.657318854973" />
                  <Point X="22.417566752012" Y="4.051671871479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.186841807587" Y="-2.666904032479" />
                  <Point X="22.335957973936" Y="4.003022421449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.092292450306" Y="-2.693139741846" />
                  <Point X="22.25991984442" Y="3.944724325084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.013886378788" Y="-2.747336442355" />
                  <Point X="22.183881714904" Y="3.886426228718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.939769645058" Y="-2.808962493844" />
                  <Point X="22.107843554343" Y="3.828128186123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.865652252832" Y="-2.870587404784" />
                  <Point X="22.031805274056" Y="3.769830350901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.803379456578" Y="-2.952727757742" />
                  <Point X="21.955766993768" Y="3.71153251568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.766775067319" Y="-3.079327095767" />
                  <Point X="22.250865867466" Y="3.010406273179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.736772586184" Y="-3.217361274087" />
                  <Point X="22.21047305714" Y="2.890368672923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.73449294277" Y="-3.40341281587" />
                  <Point X="22.143834311477" Y="2.815790366164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.766895971102" Y="-3.649536507261" />
                  <Point X="22.069333749206" Y="2.75482912521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.799298999434" Y="-3.895660198652" />
                  <Point X="25.734021906131" Y="-3.78259695648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.331402624601" Y="-3.085239904762" />
                  <Point X="21.976731881491" Y="2.725220264969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.831700867546" Y="-4.141781880481" />
                  <Point X="25.82902112594" Y="-4.137140431868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.197709532425" Y="-3.043676676494" />
                  <Point X="21.864600928305" Y="2.729436772988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.064776872456" Y="-3.003430555441" />
                  <Point X="21.730889033982" Y="2.771032567531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.95441511694" Y="-3.002278387676" />
                  <Point X="21.566344009901" Y="2.866032909373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.860411548807" Y="-3.029459431576" />
                  <Point X="21.401798985819" Y="2.961033251215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.767384332135" Y="-3.058331565814" />
                  <Point X="21.25972918455" Y="3.017105365235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.674676688844" Y="-3.087757217383" />
                  <Point X="21.196761014855" Y="2.936169434405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.593641901955" Y="-3.137400849312" />
                  <Point X="21.133793220499" Y="2.855232853471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.52907689944" Y="-3.215570984564" />
                  <Point X="21.070825426143" Y="2.774296272536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.469194557806" Y="-3.301851726379" />
                  <Point X="21.007857631787" Y="2.693359691602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.409311509316" Y="-3.388131243881" />
                  <Point X="21.578495192734" Y="1.514986443334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.411872621014" Y="1.803585203241" />
                  <Point X="20.951450154659" Y="2.601060307914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.357243389657" Y="-3.487946615177" />
                  <Point X="21.542879260296" Y="1.386675047876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.214925030493" Y="1.954708436451" />
                  <Point X="20.896302306478" Y="2.506579182892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.322470804712" Y="-3.617718731343" />
                  <Point X="21.485866437077" Y="1.295424154374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.017977188197" Y="2.105832105749" />
                  <Point X="20.841154605937" Y="2.412097802148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.287698219768" Y="-3.747490847508" />
                  <Point X="21.406885160668" Y="1.24222373796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.821029044658" Y="2.256956296815" />
                  <Point X="20.786007076202" Y="2.317616125562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.252925634823" Y="-3.877262963674" />
                  <Point X="21.314670570444" Y="1.211944093428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.218153049879" Y="-4.00703507984" />
                  <Point X="21.207539228641" Y="1.207501020513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.183380493014" Y="-4.136807244641" />
                  <Point X="21.088818802052" Y="1.223130831261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.148608005251" Y="-4.26657952913" />
                  <Point X="21.706027352688" Y="-0.035905737306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.559122614802" Y="0.218540732586" />
                  <Point X="20.970098375464" Y="1.238760642009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.113835517488" Y="-4.396351813619" />
                  <Point X="21.67164413717" Y="-0.166352261101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.420465427262" Y="0.268702026239" />
                  <Point X="20.851377948875" Y="1.254390452757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.079063029725" Y="-4.526124098108" />
                  <Point X="21.607984505206" Y="-0.246090544148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.290692978621" Y="0.303474500708" />
                  <Point X="20.732657522286" Y="1.270020263505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.044290541962" Y="-4.655896382597" />
                  <Point X="23.836568790535" Y="-4.296111755288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.660254404552" Y="-3.99072628066" />
                  <Point X="21.528145364306" Y="-0.297805095676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.160920529979" Y="0.338246975178" />
                  <Point X="20.613937095698" Y="1.285650074253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.996615381435" Y="-4.763320582305" />
                  <Point X="23.880077333298" Y="-4.561470761917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.453566464443" Y="-3.82273226708" />
                  <Point X="22.049226941193" Y="-1.390344861734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.896179106451" Y="-1.125258235971" />
                  <Point X="21.435447505913" Y="-0.327247695188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.031148081338" Y="0.373019449648" />
                  <Point X="20.495216593398" Y="1.301280016135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.870138857661" Y="-4.734256817163" />
                  <Point X="23.860555582994" Y="-4.717658098537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.327171142702" Y="-3.793809147985" />
                  <Point X="22.678933442749" Y="-2.671028516285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.497981723704" Y="-2.357610945182" />
                  <Point X="22.007665088243" Y="-1.508357620768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.771581342711" Y="-1.099448578664" />
                  <Point X="21.340447517358" Y="-0.352702888292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.901375632696" Y="0.407791924118" />
                  <Point X="20.376496075162" Y="1.316909985621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.213160204171" Y="-3.786336409831" />
                  <Point X="22.629201266011" Y="-2.774889859403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.386276463778" Y="-2.354131759517" />
                  <Point X="21.940735023248" Y="-1.582431347641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.668911338314" Y="-1.111618914636" />
                  <Point X="21.245447528803" Y="-0.378158081395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.771603226077" Y="0.442564325803" />
                  <Point X="20.33435059518" Y="1.199908098259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.099149368183" Y="-3.778863849286" />
                  <Point X="22.574352935552" Y="-2.869889764338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.296500223246" Y="-2.388634749604" />
                  <Point X="21.864716125936" Y="-1.640762755162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.566963792101" Y="-1.125040584888" />
                  <Point X="21.150447540247" Y="-0.403613274499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.641830825556" Y="0.477336716925" />
                  <Point X="20.299310391611" Y="1.070599511149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.991334391012" Y="-3.782122831008" />
                  <Point X="22.519504605093" Y="-2.964889669273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.214227787636" Y="-2.436134711064" />
                  <Point X="21.788697228624" Y="-1.699094162683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.465016245888" Y="-1.138462255139" />
                  <Point X="21.055447551692" Y="-0.429068467602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.512058425036" Y="0.512109108047" />
                  <Point X="20.265859227849" Y="0.938538626356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.902055555124" Y="-3.81748735121" />
                  <Point X="22.464656274634" Y="-3.059889574207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.131955352025" Y="-2.483634672524" />
                  <Point X="21.712678331313" Y="-1.757425570204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.363068699674" Y="-1.151883925391" />
                  <Point X="20.960447563136" Y="-0.454523660706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.382286024515" Y="0.546881499169" />
                  <Point X="20.243480019004" Y="0.78730055311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.822896565933" Y="-3.870379960056" />
                  <Point X="22.409807944175" Y="-3.154889479142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.049682916415" Y="-2.531134633984" />
                  <Point X="21.636659434001" Y="-1.815756977725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.261121153461" Y="-1.165305595643" />
                  <Point X="20.865447574581" Y="-0.479978853809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.252513623994" Y="0.581653890291" />
                  <Point X="20.221100731213" Y="0.6360626166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.743737592053" Y="-3.92327259542" />
                  <Point X="22.354959613716" Y="-3.249889384076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.967410480804" Y="-2.578634595444" />
                  <Point X="21.56064053669" Y="-1.874088385246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.159173607248" Y="-1.178727265894" />
                  <Point X="20.770447586026" Y="-0.505434046913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.664578747933" Y="-3.976165455535" />
                  <Point X="22.300111283257" Y="-3.344889289011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.885138045194" Y="-2.626134556905" />
                  <Point X="21.484621639378" Y="-1.932419792767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.057226056245" Y="-1.19214892785" />
                  <Point X="20.67544759747" Y="-0.530889240016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.591422487031" Y="-4.039455094762" />
                  <Point X="22.245262952798" Y="-3.439889193946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.802865609583" Y="-2.673634518365" />
                  <Point X="21.408602742067" Y="-1.990751200288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.955278495136" Y="-1.205570572301" />
                  <Point X="20.58044760505" Y="-0.556344426426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.528728694974" Y="-4.1208662616" />
                  <Point X="22.190414670606" Y="-3.534889182481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.720593195311" Y="-2.721134516784" />
                  <Point X="21.332583844755" Y="-2.049082607809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.853330934027" Y="-1.218992216753" />
                  <Point X="20.485447605785" Y="-0.58179960098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.413156889656" Y="-4.110690022868" />
                  <Point X="22.135566434362" Y="-3.629889250602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.638320786534" Y="-2.76863452472" />
                  <Point X="21.256564947443" Y="-2.10741401533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.751383372918" Y="-1.232413861205" />
                  <Point X="20.39044760652" Y="-0.607254775535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.240863328492" Y="-4.002268821114" />
                  <Point X="22.080718198119" Y="-3.724889318722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.556048377756" Y="-2.816134532656" />
                  <Point X="21.180546050897" Y="-2.165745424177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.649435811809" Y="-1.245835505656" />
                  <Point X="20.295447607256" Y="-0.632709950089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.043375257834" Y="-3.850209448846" />
                  <Point X="22.025869961875" Y="-3.819889386843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.473775968978" Y="-2.863634540592" />
                  <Point X="21.104527193025" Y="-2.224076900009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.5474882507" Y="-1.259257150108" />
                  <Point X="20.218530545537" Y="-0.689485691224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.391503560201" Y="-2.911134548528" />
                  <Point X="21.028508335153" Y="-2.282408375841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.445540689592" Y="-1.27267879456" />
                  <Point X="20.254650528436" Y="-0.942047336773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.309231151423" Y="-2.958634556464" />
                  <Point X="20.952489477281" Y="-2.340739851674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.343593128483" Y="-1.286100439012" />
                  <Point X="20.335230370497" Y="-1.271615717288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.226958742645" Y="-3.0061345644" />
                  <Point X="20.876470619409" Y="-2.399071327506" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.7083046875" Y="-4.420727539062" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521543945312" />
                  <Point X="25.367548828125" Y="-3.382114746094" />
                  <Point X="25.300591796875" Y="-3.285643554688" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.124587890625" Y="-3.219923583984" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.841587890625" Y="-3.234242675781" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643554688" />
                  <Point X="24.614951171875" Y="-3.425072753906" />
                  <Point X="24.547994140625" Y="-3.521543945312" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.376955078125" Y="-4.148486328125" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.020857421875" Y="-4.961571777344" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.72644140625" Y="-4.893473632812" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.662349609375" Y="-4.7675234375" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.65454296875" Y="-4.354905273438" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.47584765625" Y="-4.081720703125" />
                  <Point X="23.38045703125" Y="-3.998064208984" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.16777734375" Y="-3.97376953125" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.857650390625" Y="-4.075669433594" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.648296875" Y="-4.277152832031" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.321677734375" Y="-4.277521484375" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.9041796875" Y="-3.982828369141" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.053966796875" Y="-3.391220458984" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597595214844" />
                  <Point X="22.4860234375" Y="-2.568766601562" />
                  <Point X="22.46867578125" Y="-2.55141796875" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.88101953125" Y="-2.847905517578" />
                  <Point X="21.157041015625" Y="-3.26589453125" />
                  <Point X="20.9785390625" Y="-3.031380859375" />
                  <Point X="20.838302734375" Y="-2.847137207031" />
                  <Point X="20.666244140625" Y="-2.558623535156" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="21.066357421875" Y="-2.013875976562" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396012084961" />
                  <Point X="21.8618828125" Y="-1.366264892578" />
                  <Point X="21.859673828125" Y="-1.334594970703" />
                  <Point X="21.838841796875" Y="-1.310639282227" />
                  <Point X="21.812359375" Y="-1.295052734375" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.11480859375" Y="-1.376207519531" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.127458984375" Y="-1.225928710938" />
                  <Point X="20.072607421875" Y="-1.011188232422" />
                  <Point X="20.027087890625" Y="-0.692914428711" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="20.565556640625" Y="-0.363632019043" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.474509765625" Y="-0.110040214539" />
                  <Point X="21.497677734375" Y="-0.09064540863" />
                  <Point X="21.5105703125" Y="-0.058290412903" />
                  <Point X="21.516599609375" Y="-0.031280040741" />
                  <Point X="21.50888671875" Y="0.001158297658" />
                  <Point X="21.497677734375" Y="0.028085329056" />
                  <Point X="21.469455078125" Y="0.050987781525" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.83575390625" Y="0.228672653198" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.047005859375" Y="0.757527526855" />
                  <Point X="20.08235546875" Y="0.996414855957" />
                  <Point X="20.17398828125" Y="1.334568725586" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.61809375" Y="1.476742431641" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.3046015625" Y="1.407313720703" />
                  <Point X="21.32972265625" Y="1.415234008789" />
                  <Point X="21.348466796875" Y="1.426056884766" />
                  <Point X="21.3608828125" Y="1.443788208008" />
                  <Point X="21.375451171875" Y="1.478958251953" />
                  <Point X="21.385529296875" Y="1.503292602539" />
                  <Point X="21.38928515625" Y="1.524606567383" />
                  <Point X="21.38368359375" Y="1.54551171875" />
                  <Point X="21.36610546875" Y="1.579278320312" />
                  <Point X="21.353943359375" Y="1.602641479492" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.991978515625" Y="1.886292236328" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.70262890625" Y="2.551681152344" />
                  <Point X="20.83998828125" Y="2.787007080078" />
                  <Point X="21.08271484375" Y="3.099001220703" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.45475" Y="3.149855224609" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.91205078125" Y="2.916010986328" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.9684921875" Y="2.915775146484" />
                  <Point X="21.98674609375" Y="2.927403808594" />
                  <Point X="22.02263671875" Y="2.963294433594" />
                  <Point X="22.047470703125" Y="2.988127197266" />
                  <Point X="22.0591015625" Y="3.006381591797" />
                  <Point X="22.061927734375" Y="3.027840820312" />
                  <Point X="22.05750390625" Y="3.078405029297" />
                  <Point X="22.054443359375" Y="3.113390380859" />
                  <Point X="22.04793359375" Y="3.134032714844" />
                  <Point X="21.89369921875" Y="3.401171630859" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="22.00789453125" Y="3.990915527344" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.62941796875" Y="4.386724609375" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.913978515625" Y="4.441608398438" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.10503125" Y="4.244364257813" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.218491699219" />
                  <Point X="23.18619140625" Y="4.22225" />
                  <Point X="23.244806640625" Y="4.246530273438" />
                  <Point X="23.285365234375" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.294487792969" />
                  <Point X="23.33299609375" Y="4.354997558594" />
                  <Point X="23.346197265625" Y="4.396864257812" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.330548828125" Y="4.551615234375" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.72220703125" Y="4.816467285156" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.495361328125" Y="4.957536621094" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.843642578125" Y="4.737168945312" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.13348046875" Y="4.605836914062" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.58979296875" Y="4.953885253906" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.243634765625" Y="4.832994628906" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.7583984375" Y="4.678402832031" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.172361328125" Y="4.50292578125" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.57183203125" Y="4.289311523438" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.9523984375" Y="4.039329589844" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.73912109375" Y="3.385672851562" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491516357422" />
                  <Point X="27.2121640625" Y="2.444062988281" />
                  <Point X="27.2033828125" Y="2.411229980469" />
                  <Point X="27.202044921875" Y="2.392328613281" />
                  <Point X="27.206994140625" Y="2.351295166016" />
                  <Point X="27.210416015625" Y="2.322903808594" />
                  <Point X="27.21868359375" Y="2.300811523438" />
                  <Point X="27.24407421875" Y="2.263392822266" />
                  <Point X="27.261642578125" Y="2.237502929688" />
                  <Point X="27.27494140625" Y="2.224203369141" />
                  <Point X="27.312359375" Y="2.198813232422" />
                  <Point X="27.33825" Y="2.181245849609" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.40137109375" Y="2.168032226562" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.4961171875" Y="2.178635986328" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.15134375" Y="2.544779052734" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.107275390625" Y="2.874350341797" />
                  <Point X="29.202595703125" Y="2.741877197266" />
                  <Point X="29.32516796875" Y="2.539321777344" />
                  <Point X="29.387513671875" Y="2.436295654297" />
                  <Point X="28.95911328125" Y="2.107572998047" />
                  <Point X="28.2886171875" Y="1.593082641602" />
                  <Point X="28.27937109375" Y="1.583832275391" />
                  <Point X="28.24521875" Y="1.539278198242" />
                  <Point X="28.22158984375" Y="1.508451171875" />
                  <Point X="28.21312109375" Y="1.491500854492" />
                  <Point X="28.2003984375" Y="1.446010864258" />
                  <Point X="28.191595703125" Y="1.414536376953" />
                  <Point X="28.190779296875" Y="1.390964233398" />
                  <Point X="28.20122265625" Y="1.340350830078" />
                  <Point X="28.20844921875" Y="1.305331542969" />
                  <Point X="28.215646484375" Y="1.287955688477" />
                  <Point X="28.24405078125" Y="1.244782104492" />
                  <Point X="28.263703125" Y="1.21491027832" />
                  <Point X="28.280947265625" Y="1.198819702148" />
                  <Point X="28.322109375" Y="1.175649047852" />
                  <Point X="28.35058984375" Y="1.15961730957" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.424220703125" Y="1.146264282227" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.055556640625" Y="1.217497558594" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.89825" Y="1.119547485352" />
                  <Point X="29.93919140625" Y="0.951367675781" />
                  <Point X="29.97781640625" Y="0.703288635254" />
                  <Point X="29.997859375" Y="0.574556335449" />
                  <Point X="29.509572265625" Y="0.443720031738" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819351196" />
                  <Point X="28.67441015625" Y="0.20121446228" />
                  <Point X="28.636578125" Y="0.179346984863" />
                  <Point X="28.622265625" Y="0.166926696777" />
                  <Point X="28.589458984375" Y="0.125123008728" />
                  <Point X="28.566759765625" Y="0.096199020386" />
                  <Point X="28.556986328125" Y="0.074735107422" />
                  <Point X="28.54605078125" Y="0.017633401871" />
                  <Point X="28.538484375" Y="-0.021875299454" />
                  <Point X="28.538484375" Y="-0.040684780121" />
                  <Point X="28.549419921875" Y="-0.097786483765" />
                  <Point X="28.556986328125" Y="-0.137295181274" />
                  <Point X="28.566759765625" Y="-0.158759094238" />
                  <Point X="28.59956640625" Y="-0.200562774658" />
                  <Point X="28.622265625" Y="-0.22948677063" />
                  <Point X="28.636578125" Y="-0.241907058716" />
                  <Point X="28.691255859375" Y="-0.273511962891" />
                  <Point X="28.729087890625" Y="-0.295379425049" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.272828125" Y="-0.442844573975" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.971291015625" Y="-0.814786193848" />
                  <Point X="29.948431640625" Y="-0.966413330078" />
                  <Point X="29.8989453125" Y="-1.183262695312" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.30311328125" Y="-1.214947998047" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.2875234375" Y="-1.121666503906" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.1854453125" Y="-1.154697631836" />
                  <Point X="28.120580078125" Y="-1.232709350586" />
                  <Point X="28.075701171875" Y="-1.286685668945" />
                  <Point X="28.064359375" Y="-1.314071044922" />
                  <Point X="28.0550625" Y="-1.415099609375" />
                  <Point X="28.048630859375" Y="-1.485001342773" />
                  <Point X="28.056361328125" Y="-1.516622070312" />
                  <Point X="28.11575" Y="-1.608997802734" />
                  <Point X="28.156841796875" Y="-1.672912719727" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.661845703125" Y="-2.064126708984" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.268568359375" Y="-2.697875732422" />
                  <Point X="29.204134765625" Y="-2.802139160156" />
                  <Point X="29.1017890625" Y="-2.947557861328" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.54712890625" Y="-2.717443359375" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.60962109375" Y="-2.230246582031" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.382974609375" Y="-2.275086669922" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334682861328" />
                  <Point X="27.232759765625" Y="-2.440787109375" />
                  <Point X="27.19412109375" Y="-2.514200927734" />
                  <Point X="27.1891640625" Y="-2.546375244141" />
                  <Point X="27.21223046875" Y="-2.674095703125" />
                  <Point X="27.22819140625" Y="-2.762465576172" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.551140625" Y="-3.327721923828" />
                  <Point X="27.98667578125" Y="-4.082088867188" />
                  <Point X="27.911759765625" Y="-4.135599609375" />
                  <Point X="27.835314453125" Y="-4.190202148438" />
                  <Point X="27.72087109375" Y="-4.264279296875" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="27.28882421875" Y="-3.781380126953" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.544583984375" Y="-2.899482177734" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.2880390625" Y="-2.84839453125" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868508789062" />
                  <Point X="26.058955078125" Y="-2.956959960938" />
                  <Point X="25.985349609375" Y="-3.018159179688" />
                  <Point X="25.96845703125" Y="-3.045985839844" />
                  <Point X="25.936650390625" Y="-3.192322998047" />
                  <Point X="25.914642578125" Y="-3.293573730469" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="26.003779296875" Y="-3.993190917969" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.066681640625" Y="-4.947391601562" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.888638671875" Y="-4.982450195312" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#176" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.114217428061" Y="4.781389739961" Z="1.5" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.5" />
                  <Point X="-0.51660836997" Y="5.038478021476" Z="1.5" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.5" />
                  <Point X="-1.297447429651" Y="4.895891124694" Z="1.5" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.5" />
                  <Point X="-1.725180423146" Y="4.576368674178" Z="1.5" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.5" />
                  <Point X="-1.720846455202" Y="4.401313829153" Z="1.5" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.5" />
                  <Point X="-1.780482195911" Y="4.324004678734" Z="1.5" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.5" />
                  <Point X="-1.878037582872" Y="4.319994840552" Z="1.5" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.5" />
                  <Point X="-2.052510392617" Y="4.503326363812" Z="1.5" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.5" />
                  <Point X="-2.401023016381" Y="4.461712159474" Z="1.5" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.5" />
                  <Point X="-3.028685220761" Y="4.061875362072" Z="1.5" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.5" />
                  <Point X="-3.155757512017" Y="3.407451984082" Z="1.5" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.5" />
                  <Point X="-2.998463838102" Y="3.105327603337" Z="1.5" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.5" />
                  <Point X="-3.018872833079" Y="3.029930734808" Z="1.5" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.5" />
                  <Point X="-3.08974886227" Y="2.997100861071" Z="1.5" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.5" />
                  <Point X="-3.526407364215" Y="3.224436436729" Z="1.5" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.5" />
                  <Point X="-3.962904007647" Y="3.160983959707" Z="1.5" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.5" />
                  <Point X="-4.346709339722" Y="2.608166395162" Z="1.5" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.5" />
                  <Point X="-4.044615294269" Y="1.877904555007" Z="1.5" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.5" />
                  <Point X="-3.684400211176" Y="1.587470982221" Z="1.5" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.5" />
                  <Point X="-3.676901988752" Y="1.529370180355" Z="1.5" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.5" />
                  <Point X="-3.716590063909" Y="1.486279763422" Z="1.5" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.5" />
                  <Point X="-4.381538012644" Y="1.557594841571" Z="1.5" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.5" />
                  <Point X="-4.880428857307" Y="1.378925927194" Z="1.5" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.5" />
                  <Point X="-5.008612272262" Y="0.796126579437" Z="1.5" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.5" />
                  <Point X="-4.18334520555" Y="0.211656443249" Z="1.5" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.5" />
                  <Point X="-3.565211229972" Y="0.041191826964" Z="1.5" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.5" />
                  <Point X="-3.545024625843" Y="0.017617433894" Z="1.5" />
                  <Point X="-3.539556741714" Y="0" Z="1.5" />
                  <Point X="-3.543339975292" Y="-0.012189517167" Z="1.5" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.5" />
                  <Point X="-3.560157365151" Y="-0.037684156169" Z="1.5" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.5" />
                  <Point X="-4.453542583222" Y="-0.284055613507" Z="1.5" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.5" />
                  <Point X="-5.028565909203" Y="-0.668713592562" Z="1.5" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.5" />
                  <Point X="-4.927143487974" Y="-1.207022637533" Z="1.5" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.5" />
                  <Point X="-3.884823038104" Y="-1.394499639076" Z="1.5" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.5" />
                  <Point X="-3.20832886743" Y="-1.313237386699" Z="1.5" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.5" />
                  <Point X="-3.195827050399" Y="-1.33461495365" Z="1.5" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.5" />
                  <Point X="-3.970236678889" Y="-1.942928336394" Z="1.5" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.5" />
                  <Point X="-4.382855426339" Y="-2.552953154283" Z="1.5" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.5" />
                  <Point X="-4.067402204988" Y="-3.030383677378" Z="1.5" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.5" />
                  <Point X="-3.100137842852" Y="-2.859926807427" Z="1.5" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.5" />
                  <Point X="-2.565744838676" Y="-2.562585630455" Z="1.5" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.5" />
                  <Point X="-2.995490205741" Y="-3.33494053964" Z="1.5" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.5" />
                  <Point X="-3.132481735136" Y="-3.991165423629" Z="1.5" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.5" />
                  <Point X="-2.710800679417" Y="-4.288752349716" Z="1.5" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.5" />
                  <Point X="-2.318193014508" Y="-4.276310736146" Z="1.5" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.5" />
                  <Point X="-2.120727273461" Y="-4.085962567676" Z="1.5" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.5" />
                  <Point X="-1.841649883209" Y="-3.992382579868" Z="1.5" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.5" />
                  <Point X="-1.563275093932" Y="-4.088032372641" Z="1.5" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.5" />
                  <Point X="-1.400653353005" Y="-4.333380244887" Z="1.5" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.5" />
                  <Point X="-1.393379328191" Y="-4.729717152762" Z="1.5" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.5" />
                  <Point X="-1.292174027456" Y="-4.910616207379" Z="1.5" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.5" />
                  <Point X="-0.994826074654" Y="-4.979375959997" Z="1.5" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.5" />
                  <Point X="-0.580904026132" Y="-4.130147408069" Z="1.5" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.5" />
                  <Point X="-0.350130531697" Y="-3.422301890207" Z="1.5" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.5" />
                  <Point X="-0.149748185347" Y="-3.250715678946" Z="1.5" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.5" />
                  <Point X="0.103610894014" Y="-3.236396366342" Z="1.5" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.5" />
                  <Point X="0.320315327735" Y="-3.379343865561" Z="1.5" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.5" />
                  <Point X="0.653850834269" Y="-4.402388621747" Z="1.5" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.5" />
                  <Point X="0.891418758611" Y="-5.000365144281" Z="1.5" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.5" />
                  <Point X="1.071229505152" Y="-4.964951738448" Z="1.5" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.5" />
                  <Point X="1.047194778021" Y="-3.955384480999" Z="1.5" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.5" />
                  <Point X="0.979353049992" Y="-3.171663006934" Z="1.5" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.5" />
                  <Point X="1.084764150347" Y="-2.964126589518" Z="1.5" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.5" />
                  <Point X="1.286464281873" Y="-2.866903957892" Z="1.5" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.5" />
                  <Point X="1.511387033833" Y="-2.910260215575" Z="1.5" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.5" />
                  <Point X="2.242999780416" Y="-3.780538190829" Z="1.5" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.5" />
                  <Point X="2.741884482515" Y="-4.274973142554" Z="1.5" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.5" />
                  <Point X="2.934662652776" Y="-4.145006790553" Z="1.5" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.5" />
                  <Point X="2.588284974437" Y="-3.271441771714" Z="1.5" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.5" />
                  <Point X="2.255277348776" Y="-2.633928693528" Z="1.5" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.5" />
                  <Point X="2.270848221957" Y="-2.432794515286" Z="1.5" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.5" />
                  <Point X="2.400103863086" Y="-2.28805308801" Z="1.5" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.5" />
                  <Point X="2.594578109985" Y="-2.248170660715" Z="1.5" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.5" />
                  <Point X="3.515971825393" Y="-2.729464525838" Z="1.5" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.5" />
                  <Point X="4.136520413093" Y="-2.945055311705" Z="1.5" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.5" />
                  <Point X="4.304943981954" Y="-2.692881127803" Z="1.5" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.5" />
                  <Point X="3.686125545551" Y="-1.993178956942" Z="1.5" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.5" />
                  <Point X="3.151651518049" Y="-1.550678202286" Z="1.5" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.5" />
                  <Point X="3.098694842075" Y="-1.38840076883" Z="1.5" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.5" />
                  <Point X="3.152871164621" Y="-1.23339587609" Z="1.5" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.5" />
                  <Point X="3.291986046074" Y="-1.139245525865" Z="1.5" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.5" />
                  <Point X="4.290431805889" Y="-1.233240161961" Z="1.5" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.5" />
                  <Point X="4.941534962652" Y="-1.163106404936" Z="1.5" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.5" />
                  <Point X="5.014575374751" Y="-0.790960043491" Z="1.5" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.5" />
                  <Point X="4.279611678459" Y="-0.363268224354" Z="1.5" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.5" />
                  <Point X="3.710120851392" Y="-0.198943150459" Z="1.5" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.5" />
                  <Point X="3.63274360573" Y="-0.138414208365" Z="1.5" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.5" />
                  <Point X="3.592370354055" Y="-0.057101769452" Z="1.5" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.5" />
                  <Point X="3.589001096369" Y="0.039508761752" Z="1.5" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.5" />
                  <Point X="3.622635832671" Y="0.12553453021" Z="1.5" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.5" />
                  <Point X="3.69327456296" Y="0.189205693783" Z="1.5" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.5" />
                  <Point X="4.516356138848" Y="0.426703708415" Z="1.5" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.5" />
                  <Point X="5.021064477831" Y="0.742260855977" Z="1.5" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.5" />
                  <Point X="4.940675156983" Y="1.162654306982" Z="1.5" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.5" />
                  <Point X="4.042873440884" Y="1.298349908928" Z="1.5" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.5" />
                  <Point X="3.424614469031" Y="1.227113245031" Z="1.5" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.5" />
                  <Point X="3.340480578563" Y="1.250500375268" Z="1.5" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.5" />
                  <Point X="3.279665471616" Y="1.30354284935" Z="1.5" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.5" />
                  <Point X="3.244035294685" Y="1.381735836052" Z="1.5" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.5" />
                  <Point X="3.242394228142" Y="1.463823744711" Z="1.5" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.5" />
                  <Point X="3.278745936824" Y="1.540140835676" Z="1.5" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.5" />
                  <Point X="3.983394737667" Y="2.099185203215" Z="1.5" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.5" />
                  <Point X="4.361789271135" Y="2.596488341688" Z="1.5" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.5" />
                  <Point X="4.141703621415" Y="2.934833199327" Z="1.5" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.5" />
                  <Point X="3.120186270931" Y="2.619360438365" Z="1.5" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.5" />
                  <Point X="2.477044947855" Y="2.258218803324" Z="1.5" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.5" />
                  <Point X="2.401200438728" Y="2.248952629713" Z="1.5" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.5" />
                  <Point X="2.334276764102" Y="2.271467964588" Z="1.5" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.5" />
                  <Point X="2.279290635647" Y="2.32274809628" Z="1.5" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.5" />
                  <Point X="2.250477073045" Y="2.388558005105" Z="1.5" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.5" />
                  <Point X="2.254309139334" Y="2.462424628027" Z="1.5" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.5" />
                  <Point X="2.776264914494" Y="3.391952811024" Z="1.5" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.5" />
                  <Point X="2.975218130709" Y="4.111356779246" Z="1.5" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.5" />
                  <Point X="2.590844535407" Y="4.36379419278" Z="1.5" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.5" />
                  <Point X="2.187385620676" Y="4.579497740662" Z="1.5" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.5" />
                  <Point X="1.769290005479" Y="4.756686382357" Z="1.5" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.5" />
                  <Point X="1.24921189048" Y="4.912877961519" Z="1.5" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.5" />
                  <Point X="0.588843457614" Y="5.034893262422" Z="1.5" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.5" />
                  <Point X="0.079027133492" Y="4.65005776599" Z="1.5" />
                  <Point X="0" Y="4.355124473572" Z="1.5" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>