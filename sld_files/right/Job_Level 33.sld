<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#181" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2414" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999524414062" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.81583984375" Y="-4.454998046875" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497139648438" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.439146484375" Y="-3.31866015625" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209021484375" />
                  <Point X="25.33049609375" Y="-3.18977734375" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.1427734375" Y="-3.126097167969" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.803453125" Y="-3.146607910156" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.18977734375" />
                  <Point X="24.655560546875" Y="-3.209021484375" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.530458984375" Y="-3.380193847656" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481571777344" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.300962890625" Y="-4.065040527344" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="24.02716015625" Y="-4.866021972656" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.75358203125" Y="-4.802362304688" />
                  <Point X="23.754638671875" Y="-4.794342773438" />
                  <Point X="23.7850390625" Y="-4.5634375" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.745333984375" Y="-4.324391113281" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182963867188" />
                  <Point X="23.69598828125" Y="-4.155126953125" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.5293046875" Y="-4.002242431641" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.16180078125" Y="-3.878174072266" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.79471484375" Y="-4.003466064453" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.58178125" Y="-4.20778125" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.354392578125" Y="-4.186041015625" />
                  <Point X="22.19828515625" Y="-4.0893828125" />
                  <Point X="21.946015625" Y="-3.895143798828" />
                  <Point X="21.895279296875" Y="-3.856078369141" />
                  <Point X="22.106931640625" Y="-3.489484130859" />
                  <Point X="22.576240234375" Y="-2.676619384766" />
                  <Point X="22.587140625" Y="-2.647654296875" />
                  <Point X="22.593412109375" Y="-2.616127441406" />
                  <Point X="22.59442578125" Y="-2.585193603516" />
                  <Point X="22.58544140625" Y="-2.555575927734" />
                  <Point X="22.571224609375" Y="-2.526746582031" />
                  <Point X="22.553197265625" Y="-2.501588378906" />
                  <Point X="22.535853515625" Y="-2.484244628906" />
                  <Point X="22.510703125" Y="-2.466220214844" />
                  <Point X="22.481873046875" Y="-2.451999755859" />
                  <Point X="22.45225390625" Y="-2.443012695312" />
                  <Point X="22.42131640625" Y="-2.444023925781" />
                  <Point X="22.389787109375" Y="-2.450294433594" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.884283203125" Y="-2.736324707031" />
                  <Point X="21.181978515625" Y="-3.141800537109" />
                  <Point X="21.04046875" Y="-2.955887939453" />
                  <Point X="20.9171328125" Y="-2.793849365234" />
                  <Point X="20.736279296875" Y="-2.4905859375" />
                  <Point X="20.693857421875" Y="-2.419450439453" />
                  <Point X="21.07291015625" Y="-2.128593505859" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.915421875" Y="-1.475594360352" />
                  <Point X="21.93338671875" Y="-1.448463867188" />
                  <Point X="21.946142578125" Y="-1.419834838867" />
                  <Point X="21.95384765625" Y="-1.390087646484" />
                  <Point X="21.95665234375" Y="-1.359662963867" />
                  <Point X="21.9544453125" Y="-1.3279921875" />
                  <Point X="21.9474453125" Y="-1.298242797852" />
                  <Point X="21.931359375" Y="-1.272255859375" />
                  <Point X="21.910525390625" Y="-1.248299194336" />
                  <Point X="21.887029296875" Y="-1.228767822266" />
                  <Point X="21.860546875" Y="-1.213181152344" />
                  <Point X="21.83128515625" Y="-1.20195715332" />
                  <Point X="21.7993984375" Y="-1.195475097656" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="21.1664921875" Y="-1.273583251953" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.214158203125" Y="-1.181492553711" />
                  <Point X="20.165921875" Y="-0.992650512695" />
                  <Point X="20.118072265625" Y="-0.658085327148" />
                  <Point X="20.107576171875" Y="-0.584698486328" />
                  <Point X="20.531728515625" Y="-0.471047393799" />
                  <Point X="21.467125" Y="-0.220408432007" />
                  <Point X="21.482505859375" Y="-0.214828460693" />
                  <Point X="21.512271484375" Y="-0.199470611572" />
                  <Point X="21.529767578125" Y="-0.18732723999" />
                  <Point X="21.540025390625" Y="-0.1802084198" />
                  <Point X="21.562482421875" Y="-0.158321914673" />
                  <Point X="21.581728515625" Y="-0.132061584473" />
                  <Point X="21.5958359375" Y="-0.104060661316" />
                  <Point X="21.6050859375" Y="-0.074253738403" />
                  <Point X="21.609353515625" Y="-0.046096931458" />
                  <Point X="21.609353515625" Y="-0.01646320343" />
                  <Point X="21.6050859375" Y="0.011693604469" />
                  <Point X="21.5958359375" Y="0.041500526428" />
                  <Point X="21.581728515625" Y="0.069501594543" />
                  <Point X="21.562482421875" Y="0.095761787415" />
                  <Point X="21.540025390625" Y="0.117648284912" />
                  <Point X="21.522529296875" Y="0.129791671753" />
                  <Point X="21.5144765625" Y="0.134804244995" />
                  <Point X="21.48865234375" Y="0.149142196655" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="20.9187578125" Y="0.304783172607" />
                  <Point X="20.108185546875" Y="0.521975402832" />
                  <Point X="20.144427734375" Y="0.766900756836" />
                  <Point X="20.17551171875" Y="0.97696875" />
                  <Point X="20.2718359375" Y="1.332435302734" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.563818359375" Y="1.388068115234" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341674805" />
                  <Point X="21.276576171875" Y="1.301227783203" />
                  <Point X="21.296865234375" Y="1.305263427734" />
                  <Point X="21.335587890625" Y="1.317473022461" />
                  <Point X="21.358291015625" Y="1.324630859375" />
                  <Point X="21.37722265625" Y="1.332961303711" />
                  <Point X="21.395966796875" Y="1.343783203125" />
                  <Point X="21.4126484375" Y="1.356014892578" />
                  <Point X="21.426287109375" Y="1.371567382813" />
                  <Point X="21.438701171875" Y="1.389297241211" />
                  <Point X="21.448650390625" Y="1.407432983398" />
                  <Point X="21.4641875" Y="1.444945800781" />
                  <Point X="21.473296875" Y="1.466937255859" />
                  <Point X="21.479083984375" Y="1.486788330078" />
                  <Point X="21.48284375" Y="1.508104492188" />
                  <Point X="21.484197265625" Y="1.528750244141" />
                  <Point X="21.481048828125" Y="1.54919934082" />
                  <Point X="21.4754453125" Y="1.570106811523" />
                  <Point X="21.467951171875" Y="1.589378417969" />
                  <Point X="21.449203125" Y="1.62539440918" />
                  <Point X="21.4382109375" Y="1.646508300781" />
                  <Point X="21.42671484375" Y="1.66371081543" />
                  <Point X="21.412802734375" Y="1.680289306641" />
                  <Point X="21.39786328125" Y="1.694590332031" />
                  <Point X="21.083318359375" Y="1.93594934082" />
                  <Point X="20.648140625" Y="2.269873535156" />
                  <Point X="20.798060546875" Y="2.526723388672" />
                  <Point X="20.9188515625" Y="2.733664550781" />
                  <Point X="21.174" Y="3.061624267578" />
                  <Point X="21.24949609375" Y="3.158662353516" />
                  <Point X="21.38153125" Y="3.082431152344" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.907138671875" Y="2.821077880859" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.95943359375" Y="2.818762939453" />
                  <Point X="21.980890625" Y="2.821587646484" />
                  <Point X="22.000982421875" Y="2.82650390625" />
                  <Point X="22.01953515625" Y="2.835652099609" />
                  <Point X="22.0377890625" Y="2.847280761719" />
                  <Point X="22.053921875" Y="2.860228515625" />
                  <Point X="22.092203125" Y="2.898510009766" />
                  <Point X="22.114646484375" Y="2.920951904297" />
                  <Point X="22.12758984375" Y="2.937078613281" />
                  <Point X="22.139220703125" Y="2.955333007812" />
                  <Point X="22.14837109375" Y="2.973884521484" />
                  <Point X="22.1532890625" Y="2.993976806641" />
                  <Point X="22.156115234375" Y="3.015435791016" />
                  <Point X="22.15656640625" Y="3.036120605469" />
                  <Point X="22.15184765625" Y="3.090052734375" />
                  <Point X="22.14908203125" Y="3.121670166016" />
                  <Point X="22.145044921875" Y="3.141961181641" />
                  <Point X="22.13853515625" Y="3.162604248047" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.9908203125" Y="3.422954101562" />
                  <Point X="21.81666796875" Y="3.724595703125" />
                  <Point X="22.089009765625" Y="3.933397216797" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.701232421875" Y="4.317947265625" />
                  <Point X="22.83296484375" Y="4.391133789062" />
                  <Point X="22.9568046875" Y="4.229741699219" />
                  <Point X="22.971111328125" Y="4.214796875" />
                  <Point X="22.987693359375" Y="4.200883789062" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.0649140625" Y="4.158146484375" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.11938671875" Y="4.132330078125" />
                  <Point X="23.14029296875" Y="4.126728515625" />
                  <Point X="23.160736328125" Y="4.12358203125" />
                  <Point X="23.181376953125" Y="4.124935546875" />
                  <Point X="23.20269140625" Y="4.128694335938" />
                  <Point X="23.222548828125" Y="4.134481933594" />
                  <Point X="23.2850703125" Y="4.160379394531" />
                  <Point X="23.32172265625" Y="4.175561523437" />
                  <Point X="23.33985546875" Y="4.185509765625" />
                  <Point X="23.3575859375" Y="4.197924316406" />
                  <Point X="23.373140625" Y="4.211565429688" />
                  <Point X="23.385373046875" Y="4.22825" />
                  <Point X="23.396193359375" Y="4.246994140625" />
                  <Point X="23.404521484375" Y="4.265921875" />
                  <Point X="23.42487109375" Y="4.330462402344" />
                  <Point X="23.43680078125" Y="4.368298339844" />
                  <Point X="23.4408359375" Y="4.388583984375" />
                  <Point X="23.44272265625" Y="4.410145019531" />
                  <Point X="23.442271484375" Y="4.430827148438" />
                  <Point X="23.426423828125" Y="4.551193359375" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.778033203125" Y="4.733456054688" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.53753515625" Y="4.86682421875" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.744271484375" Y="4.740974609375" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.21763671875" Y="4.55285546875" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.60625" Y="4.856642089844" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.24709375" Y="4.7344296875" />
                  <Point X="26.48102734375" Y="4.677950195312" />
                  <Point X="26.742828125" Y="4.582994140625" />
                  <Point X="26.894646484375" Y="4.527928222656" />
                  <Point X="27.148326171875" Y="4.409290527344" />
                  <Point X="27.294576171875" Y="4.340893554688" />
                  <Point X="27.53966796875" Y="4.198103515625" />
                  <Point X="27.68097265625" Y="4.115777832031" />
                  <Point X="27.912111328125" Y="3.951407226562" />
                  <Point X="27.94326171875" Y="3.929254150391" />
                  <Point X="27.690771484375" Y="3.491927978516" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.539936767578" />
                  <Point X="27.133078125" Y="2.516058105469" />
                  <Point X="27.11954296875" Y="2.465443847656" />
                  <Point X="27.111607421875" Y="2.435771728516" />
                  <Point X="27.108619140625" Y="2.417936279297" />
                  <Point X="27.107728515625" Y="2.380953369141" />
                  <Point X="27.113005859375" Y="2.337186523438" />
                  <Point X="27.116099609375" Y="2.311528564453" />
                  <Point X="27.121443359375" Y="2.289601074219" />
                  <Point X="27.1297109375" Y="2.267511474609" />
                  <Point X="27.140072265625" Y="2.247469482422" />
                  <Point X="27.167154296875" Y="2.207558349609" />
                  <Point X="27.18303125" Y="2.184160888672" />
                  <Point X="27.194470703125" Y="2.170325439453" />
                  <Point X="27.2216015625" Y="2.145592529297" />
                  <Point X="27.26151171875" Y="2.118510986328" />
                  <Point X="27.28491015625" Y="2.102635009766" />
                  <Point X="27.304955078125" Y="2.092271972656" />
                  <Point X="27.327041015625" Y="2.084006591797" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.39273046875" Y="2.073385986328" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845458984" />
                  <Point X="27.47320703125" Y="2.074170898438" />
                  <Point X="27.523822265625" Y="2.087705810547" />
                  <Point X="27.553494140625" Y="2.095640625" />
                  <Point X="27.5652890625" Y="2.099639648438" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.140087890625" Y="2.428584228516" />
                  <Point X="28.967326171875" Y="2.906190429688" />
                  <Point X="29.03944921875" Y="2.805955566406" />
                  <Point X="29.123279296875" Y="2.689452392578" />
                  <Point X="29.252125" Y="2.476532470703" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.945943359375" Y="2.217211914062" />
                  <Point X="28.23078515625" Y="1.668451171875" />
                  <Point X="28.221431640625" Y="1.660246582031" />
                  <Point X="28.20397265625" Y="1.641626831055" />
                  <Point X="28.167546875" Y="1.594105102539" />
                  <Point X="28.14619140625" Y="1.566245727539" />
                  <Point X="28.13660546875" Y="1.550908813477" />
                  <Point X="28.1216328125" Y="1.517088500977" />
                  <Point X="28.1080625" Y="1.468568237305" />
                  <Point X="28.100107421875" Y="1.440124023438" />
                  <Point X="28.09665234375" Y="1.417824707031" />
                  <Point X="28.0958359375" Y="1.394255249023" />
                  <Point X="28.097740234375" Y="1.371766357422" />
                  <Point X="28.108880859375" Y="1.317781616211" />
                  <Point X="28.11541015625" Y="1.286133544922" />
                  <Point X="28.1206796875" Y="1.268981689453" />
                  <Point X="28.136283203125" Y="1.235741577148" />
                  <Point X="28.166580078125" Y="1.189692016602" />
                  <Point X="28.18433984375" Y="1.162696166992" />
                  <Point X="28.198890625" Y="1.145454101562" />
                  <Point X="28.216134765625" Y="1.129362915039" />
                  <Point X="28.23434765625" Y="1.116034667969" />
                  <Point X="28.278251953125" Y="1.091320556641" />
                  <Point X="28.303990234375" Y="1.076832397461" />
                  <Point X="28.3205234375" Y="1.069500976563" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.41548046875" Y="1.051593261719" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.012142578125" Y="1.115962402344" />
                  <Point X="29.77683984375" Y="1.21663659668" />
                  <Point X="29.809935546875" Y="1.080690063477" />
                  <Point X="29.845939453125" Y="0.93278894043" />
                  <Point X="29.88654296875" Y="0.672008972168" />
                  <Point X="29.8908671875" Y="0.644238891602" />
                  <Point X="29.53616796875" Y="0.549197387695" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.7047890625" Y="0.325585296631" />
                  <Point X="28.681546875" Y="0.315067901611" />
                  <Point X="28.6232265625" Y="0.281357818604" />
                  <Point X="28.589037109375" Y="0.261595489502" />
                  <Point X="28.574314453125" Y="0.251097213745" />
                  <Point X="28.54753125" Y="0.225576538086" />
                  <Point X="28.5125390625" Y="0.18098828125" />
                  <Point X="28.492025390625" Y="0.154848907471" />
                  <Point X="28.48030078125" Y="0.135567443848" />
                  <Point X="28.47052734375" Y="0.114103675842" />
                  <Point X="28.463681640625" Y="0.092603935242" />
                  <Point X="28.452017578125" Y="0.031698511124" />
                  <Point X="28.4451796875" Y="-0.004006679058" />
                  <Point X="28.443484375" Y="-0.021875440598" />
                  <Point X="28.4451796875" Y="-0.058553455353" />
                  <Point X="28.45684375" Y="-0.119458877563" />
                  <Point X="28.463681640625" Y="-0.155164077759" />
                  <Point X="28.47052734375" Y="-0.176663803101" />
                  <Point X="28.48030078125" Y="-0.198127578735" />
                  <Point X="28.492025390625" Y="-0.217409042358" />
                  <Point X="28.527017578125" Y="-0.261997314453" />
                  <Point X="28.54753125" Y="-0.28813684082" />
                  <Point X="28.560001953125" Y="-0.301237091064" />
                  <Point X="28.589037109375" Y="-0.324155456543" />
                  <Point X="28.647357421875" Y="-0.357865661621" />
                  <Point X="28.681546875" Y="-0.377627868652" />
                  <Point X="28.692708984375" Y="-0.383137695312" />
                  <Point X="28.716580078125" Y="-0.392149963379" />
                  <Point X="29.197056640625" Y="-0.520892944336" />
                  <Point X="29.89147265625" Y="-0.706961730957" />
                  <Point X="29.875125" Y="-0.815397644043" />
                  <Point X="29.855025390625" Y="-0.948725036621" />
                  <Point X="29.803001953125" Y="-1.176693115234" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="29.374873046875" Y="-1.128575561523" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.260197265625" Y="-1.030387695312" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.1639765625" Y="-1.056596313477" />
                  <Point X="28.1361484375" Y="-1.073488891602" />
                  <Point X="28.1123984375" Y="-1.093960327148" />
                  <Point X="28.043212890625" Y="-1.177168212891" />
                  <Point X="28.002654296875" Y="-1.225947998047" />
                  <Point X="27.987931640625" Y="-1.250335205078" />
                  <Point X="27.97658984375" Y="-1.277720581055" />
                  <Point X="27.969759765625" Y="-1.305365844727" />
                  <Point X="27.95984375" Y="-1.413124145508" />
                  <Point X="27.95403125" Y="-1.476296142578" />
                  <Point X="27.956349609375" Y="-1.507567138672" />
                  <Point X="27.96408203125" Y="-1.539188232422" />
                  <Point X="27.976453125" Y="-1.567996337891" />
                  <Point X="28.039796875" Y="-1.666525512695" />
                  <Point X="28.07693359375" Y="-1.724286987305" />
                  <Point X="28.086935546875" Y="-1.73723840332" />
                  <Point X="28.110630859375" Y="-1.760909301758" />
                  <Point X="28.556513671875" Y="-2.103048095703" />
                  <Point X="29.213123046875" Y="-2.606882568359" />
                  <Point X="29.181474609375" Y="-2.658094970703" />
                  <Point X="29.124802734375" Y="-2.749796875" />
                  <Point X="29.028982421875" Y="-2.885945068359" />
                  <Point X="28.64749609375" Y="-2.665693603516" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.61799609375" Y="-2.13522265625" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135176757812" />
                  <Point X="27.331662109375" Y="-2.19473828125" />
                  <Point X="27.26531640625" Y="-2.229655761719" />
                  <Point X="27.242384765625" Y="-2.246549560547" />
                  <Point X="27.22142578125" Y="-2.267509033203" />
                  <Point X="27.204533203125" Y="-2.290438476562" />
                  <Point X="27.144970703125" Y="-2.403610351562" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531908935547" />
                  <Point X="27.09567578125" Y="-2.563259277344" />
                  <Point X="27.120279296875" Y="-2.699487304688" />
                  <Point X="27.134703125" Y="-2.779349609375" />
                  <Point X="27.13898828125" Y="-2.795140869141" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.43834765625" Y="-3.322354980469" />
                  <Point X="27.861287109375" Y="-4.05490625" />
                  <Point X="27.849091796875" Y="-4.063616210938" />
                  <Point X="27.781865234375" Y="-4.111634277344" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="27.40453515625" Y="-3.776122802734" />
                  <Point X="26.758548828125" Y="-2.934254882812" />
                  <Point X="26.74750390625" Y="-2.922177490234" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.58756640625" Y="-2.814177490234" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479990234375" Y="-2.751167236328" />
                  <Point X="26.448369140625" Y="-2.743435546875" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.270158203125" Y="-2.754638427734" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.156365234375" Y="-2.769396972656" />
                  <Point X="26.12898046875" Y="-2.780739746094" />
                  <Point X="26.10459765625" Y="-2.7954609375" />
                  <Point X="25.9911328125" Y="-2.889803710938" />
                  <Point X="25.92461328125" Y="-2.945111328125" />
                  <Point X="25.904142578125" Y="-2.968861328125" />
                  <Point X="25.88725" Y="-2.996688232422" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.84169921875" Y="-3.181893310547" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.90094140625" Y="-3.939888427734" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941568359375" Y="-4.752634765625" />
                  <Point X="23.85875390625" Y="-4.731328125" />
                  <Point X="23.879224609375" Y="-4.575847167969" />
                  <Point X="23.879923828125" Y="-4.568098144531" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497689453125" />
                  <Point X="23.8385078125" Y="-4.305857421875" />
                  <Point X="23.81613671875" Y="-4.193397949219" />
                  <Point X="23.811875" Y="-4.17846875" />
                  <Point X="23.80097265625" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135462402344" />
                  <Point X="23.778259765625" Y="-4.107625488281" />
                  <Point X="23.769423828125" Y="-4.094859863281" />
                  <Point X="23.749791015625" Y="-4.070937011719" />
                  <Point X="23.738994140625" Y="-4.059779785156" />
                  <Point X="23.591943359375" Y="-3.930818115234" />
                  <Point X="23.505734375" Y="-3.855215332031" />
                  <Point X="23.493263671875" Y="-3.845965820312" />
                  <Point X="23.46698046875" Y="-3.829621582031" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.168013671875" Y="-3.783377441406" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812255859" />
                  <Point X="22.741935546875" Y="-3.9244765625" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629150391" />
                  <Point X="22.506412109375" Y="-4.149948730469" />
                  <Point X="22.496796875" Y="-4.162478515625" />
                  <Point X="22.404404296875" Y="-4.105270507813" />
                  <Point X="22.25240625" Y="-4.011156738281" />
                  <Point X="22.019138671875" Y="-3.831548583984" />
                  <Point X="22.189205078125" Y="-3.536983886719" />
                  <Point X="22.658513671875" Y="-2.724119140625" />
                  <Point X="22.66515234375" Y="-2.710079589844" />
                  <Point X="22.676052734375" Y="-2.681114501953" />
                  <Point X="22.680314453125" Y="-2.666188964844" />
                  <Point X="22.6865859375" Y="-2.634662109375" />
                  <Point X="22.688361328125" Y="-2.619238769531" />
                  <Point X="22.689375" Y="-2.588304931641" />
                  <Point X="22.6853359375" Y="-2.557616699219" />
                  <Point X="22.6763515625" Y="-2.527999023438" />
                  <Point X="22.67064453125" Y="-2.513559082031" />
                  <Point X="22.656427734375" Y="-2.484729736328" />
                  <Point X="22.6484453125" Y="-2.471412841797" />
                  <Point X="22.63041796875" Y="-2.446254638672" />
                  <Point X="22.620373046875" Y="-2.434413330078" />
                  <Point X="22.603029296875" Y="-2.417069580078" />
                  <Point X="22.591193359375" Y="-2.407026855469" />
                  <Point X="22.56604296875" Y="-2.389002441406" />
                  <Point X="22.552728515625" Y="-2.381020751953" />
                  <Point X="22.5238984375" Y="-2.366800292969" />
                  <Point X="22.50945703125" Y="-2.361092285156" />
                  <Point X="22.479837890625" Y="-2.352105224609" />
                  <Point X="22.449150390625" Y="-2.348063476562" />
                  <Point X="22.418212890625" Y="-2.349074707031" />
                  <Point X="22.40278515625" Y="-2.350848632812" />
                  <Point X="22.371255859375" Y="-2.357119140625" />
                  <Point X="22.35632421875" Y="-2.361383056641" />
                  <Point X="22.32735546875" Y="-2.372285888672" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.836783203125" Y="-2.654052246094" />
                  <Point X="21.2069140625" Y="-3.017707763672" />
                  <Point X="21.1160625" Y="-2.898349365234" />
                  <Point X="20.995970703125" Y="-2.740572509766" />
                  <Point X="20.818734375" Y="-2.443374267578" />
                  <Point X="21.1307421875" Y="-2.203962158203" />
                  <Point X="21.951876953125" Y="-1.573882080078" />
                  <Point X="21.963517578125" Y="-1.563310668945" />
                  <Point X="21.98489453125" Y="-1.540391479492" />
                  <Point X="21.994630859375" Y="-1.528043701172" />
                  <Point X="22.012595703125" Y="-1.500913330078" />
                  <Point X="22.020162109375" Y="-1.487127563477" />
                  <Point X="22.03291796875" Y="-1.458498535156" />
                  <Point X="22.038107421875" Y="-1.443655517578" />
                  <Point X="22.0458125" Y="-1.413908203125" />
                  <Point X="22.048447265625" Y="-1.398808227539" />
                  <Point X="22.051251953125" Y="-1.368383544922" />
                  <Point X="22.051421875" Y="-1.353058837891" />
                  <Point X="22.04921484375" Y="-1.321387939453" />
                  <Point X="22.046919921875" Y="-1.306233032227" />
                  <Point X="22.039919921875" Y="-1.276483642578" />
                  <Point X="22.02822265625" Y="-1.248241821289" />
                  <Point X="22.01213671875" Y="-1.222254882812" />
                  <Point X="22.00304296875" Y="-1.209915405273" />
                  <Point X="21.982208984375" Y="-1.185958618164" />
                  <Point X="21.97125390625" Y="-1.175243652344" />
                  <Point X="21.9477578125" Y="-1.155712402344" />
                  <Point X="21.935216796875" Y="-1.146895874023" />
                  <Point X="21.908734375" Y="-1.131309204102" />
                  <Point X="21.894568359375" Y="-1.124482421875" />
                  <Point X="21.865306640625" Y="-1.113258422852" />
                  <Point X="21.8502109375" Y="-1.108861206055" />
                  <Point X="21.81832421875" Y="-1.102379150391" />
                  <Point X="21.802705078125" Y="-1.100532714844" />
                  <Point X="21.77137890625" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="21.154091796875" Y="-1.179395996094" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.306203125" Y="-1.157980957031" />
                  <Point X="20.259236328125" Y="-0.974110961914" />
                  <Point X="20.213548828125" Y="-0.654654663086" />
                  <Point X="20.55631640625" Y="-0.562810302734" />
                  <Point X="21.491712890625" Y="-0.312171356201" />
                  <Point X="21.4995234375" Y="-0.309713134766" />
                  <Point X="21.52606640625" Y="-0.299253204346" />
                  <Point X="21.55583203125" Y="-0.28389541626" />
                  <Point X="21.566439453125" Y="-0.27751473999" />
                  <Point X="21.583935546875" Y="-0.265371368408" />
                  <Point X="21.606330078125" Y="-0.248242279053" />
                  <Point X="21.628787109375" Y="-0.226355773926" />
                  <Point X="21.639107421875" Y="-0.214479629517" />
                  <Point X="21.658353515625" Y="-0.188219299316" />
                  <Point X="21.666568359375" Y="-0.174805938721" />
                  <Point X="21.68067578125" Y="-0.146805084229" />
                  <Point X="21.686568359375" Y="-0.132217453003" />
                  <Point X="21.695818359375" Y="-0.102410545349" />
                  <Point X="21.699013671875" Y="-0.088489784241" />
                  <Point X="21.70328125" Y="-0.060333034515" />
                  <Point X="21.704353515625" Y="-0.046096897125" />
                  <Point X="21.704353515625" Y="-0.016463129044" />
                  <Point X="21.70328125" Y="-0.002227139235" />
                  <Point X="21.699013671875" Y="0.025929613113" />
                  <Point X="21.695818359375" Y="0.039850372314" />
                  <Point X="21.686568359375" Y="0.069657287598" />
                  <Point X="21.68067578125" Y="0.084244766235" />
                  <Point X="21.666568359375" Y="0.112245765686" />
                  <Point X="21.658353515625" Y="0.125659568787" />
                  <Point X="21.639107421875" Y="0.151919754028" />
                  <Point X="21.628787109375" Y="0.163795608521" />
                  <Point X="21.606330078125" Y="0.185682113647" />
                  <Point X="21.594193359375" Y="0.195692459106" />
                  <Point X="21.576697265625" Y="0.207835830688" />
                  <Point X="21.560591796875" Y="0.217861190796" />
                  <Point X="21.534767578125" Y="0.232199142456" />
                  <Point X="21.52426953125" Y="0.237212631226" />
                  <Point X="21.5027421875" Y="0.245918655396" />
                  <Point X="21.491712890625" Y="0.249611190796" />
                  <Point X="20.943345703125" Y="0.396546173096" />
                  <Point X="20.2145546875" Y="0.591824829102" />
                  <Point X="20.238404296875" Y="0.752994812012" />
                  <Point X="20.26866796875" Y="0.95752130127" />
                  <Point X="20.363529296875" Y="1.307588256836" />
                  <Point X="20.3664140625" Y="1.318236938477" />
                  <Point X="20.55141796875" Y="1.293880859375" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815551758" />
                  <Point X="21.252943359375" Y="1.204364257812" />
                  <Point X="21.263294921875" Y="1.204703125" />
                  <Point X="21.28485546875" Y="1.206589233398" />
                  <Point X="21.295109375" Y="1.208053100586" />
                  <Point X="21.3153984375" Y="1.212088745117" />
                  <Point X="21.32543359375" Y="1.214660522461" />
                  <Point X="21.36415625" Y="1.226870239258" />
                  <Point X="21.386859375" Y="1.234028076172" />
                  <Point X="21.396552734375" Y="1.237676757812" />
                  <Point X="21.415484375" Y="1.246007202148" />
                  <Point X="21.42472265625" Y="1.250688842773" />
                  <Point X="21.443466796875" Y="1.261510742188" />
                  <Point X="21.452142578125" Y="1.267171386719" />
                  <Point X="21.46882421875" Y="1.279403076172" />
                  <Point X="21.48407421875" Y="1.293378295898" />
                  <Point X="21.497712890625" Y="1.308930786133" />
                  <Point X="21.504107421875" Y="1.317079101562" />
                  <Point X="21.516521484375" Y="1.334808837891" />
                  <Point X="21.521990234375" Y="1.343604736328" />
                  <Point X="21.531939453125" Y="1.361740600586" />
                  <Point X="21.536419921875" Y="1.371080444336" />
                  <Point X="21.55195703125" Y="1.408593261719" />
                  <Point X="21.56106640625" Y="1.430584716797" />
                  <Point X="21.5645" Y="1.440348999023" />
                  <Point X="21.570287109375" Y="1.460200073242" />
                  <Point X="21.572640625" Y="1.470286865234" />
                  <Point X="21.576400390625" Y="1.491603027344" />
                  <Point X="21.577640625" Y="1.501889770508" />
                  <Point X="21.578994140625" Y="1.522535522461" />
                  <Point X="21.578091796875" Y="1.543206542969" />
                  <Point X="21.574943359375" Y="1.563655761719" />
                  <Point X="21.572810546875" Y="1.573792724609" />
                  <Point X="21.56720703125" Y="1.594700195312" />
                  <Point X="21.563986328125" Y="1.604537719727" />
                  <Point X="21.5564921875" Y="1.623809326172" />
                  <Point X="21.55221875" Y="1.633243286133" />
                  <Point X="21.533470703125" Y="1.669259277344" />
                  <Point X="21.522478515625" Y="1.690373168945" />
                  <Point X="21.517197265625" Y="1.69929296875" />
                  <Point X="21.505701171875" Y="1.716495483398" />
                  <Point X="21.499486328125" Y="1.724778564453" />
                  <Point X="21.48557421875" Y="1.741356933594" />
                  <Point X="21.47849609375" Y="1.748914916992" />
                  <Point X="21.463556640625" Y="1.763215820313" />
                  <Point X="21.4556953125" Y="1.769958862305" />
                  <Point X="21.141150390625" Y="2.011317871094" />
                  <Point X="20.77238671875" Y="2.294281005859" />
                  <Point X="20.880107421875" Y="2.478833984375" />
                  <Point X="20.99771484375" Y="2.680321533203" />
                  <Point X="21.24898046875" Y="3.003290039062" />
                  <Point X="21.273662109375" Y="3.035013671875" />
                  <Point X="21.33403125" Y="3.000158935547" />
                  <Point X="21.74584375" Y="2.762398925781" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774015625" Y="2.749385742188" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.898859375" Y="2.726439453125" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.961505859375" Y="2.723785644531" />
                  <Point X="21.97183203125" Y="2.724575683594" />
                  <Point X="21.9932890625" Y="2.727400390625" />
                  <Point X="22.003470703125" Y="2.729310058594" />
                  <Point X="22.0235625" Y="2.734226318359" />
                  <Point X="22.04299609375" Y="2.741299072266" />
                  <Point X="22.061548828125" Y="2.750447265625" />
                  <Point X="22.070578125" Y="2.755529296875" />
                  <Point X="22.08883203125" Y="2.767157958984" />
                  <Point X="22.097251953125" Y="2.77319140625" />
                  <Point X="22.113384765625" Y="2.786139160156" />
                  <Point X="22.12109765625" Y="2.793053466797" />
                  <Point X="22.15937890625" Y="2.831334960938" />
                  <Point X="22.181822265625" Y="2.853776855469" />
                  <Point X="22.188734375" Y="2.86148828125" />
                  <Point X="22.201677734375" Y="2.877614990234" />
                  <Point X="22.207708984375" Y="2.886030273438" />
                  <Point X="22.21933984375" Y="2.904284667969" />
                  <Point X="22.224419921875" Y="2.913308837891" />
                  <Point X="22.2335703125" Y="2.931860351562" />
                  <Point X="22.240646484375" Y="2.951298339844" />
                  <Point X="22.245564453125" Y="2.971390625" />
                  <Point X="22.2474765625" Y="2.981572265625" />
                  <Point X="22.250302734375" Y="3.00303125" />
                  <Point X="22.251091796875" Y="3.013364257812" />
                  <Point X="22.25154296875" Y="3.034049072266" />
                  <Point X="22.251205078125" Y="3.044400878906" />
                  <Point X="22.246486328125" Y="3.098333007812" />
                  <Point X="22.243720703125" Y="3.129950439453" />
                  <Point X="22.242255859375" Y="3.140208007812" />
                  <Point X="22.23821875" Y="3.160499023438" />
                  <Point X="22.235646484375" Y="3.170532470703" />
                  <Point X="22.22913671875" Y="3.191175537109" />
                  <Point X="22.22548828125" Y="3.200869873047" />
                  <Point X="22.217158203125" Y="3.219798583984" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="22.073091796875" Y="3.470454101562" />
                  <Point X="21.94061328125" Y="3.699914550781" />
                  <Point X="22.1468125" Y="3.858005371094" />
                  <Point X="22.35163671875" Y="4.015042236328" />
                  <Point X="22.747369140625" Y="4.234903320312" />
                  <Point X="22.8074765625" Y="4.268296875" />
                  <Point X="22.881435546875" Y="4.171909667969" />
                  <Point X="22.8881796875" Y="4.164047851562" />
                  <Point X="22.902486328125" Y="4.149103027344" />
                  <Point X="22.910048828125" Y="4.142020507812" />
                  <Point X="22.926630859375" Y="4.128107421875" />
                  <Point X="22.934916015625" Y="4.121893554688" />
                  <Point X="22.952111328125" Y="4.110404296875" />
                  <Point X="22.961021484375" Y="4.10512890625" />
                  <Point X="23.021046875" Y="4.073880859375" />
                  <Point X="23.05623828125" Y="4.055562011719" />
                  <Point X="23.06567578125" Y="4.051286132812" />
                  <Point X="23.08495703125" Y="4.043788574219" />
                  <Point X="23.09480078125" Y="4.040566894531" />
                  <Point X="23.11570703125" Y="4.034965332031" />
                  <Point X="23.125841796875" Y="4.032834228516" />
                  <Point X="23.14628515625" Y="4.029687744141" />
                  <Point X="23.166953125" Y="4.028785644531" />
                  <Point X="23.18759375" Y="4.030139160156" />
                  <Point X="23.197875" Y="4.031379150391" />
                  <Point X="23.219189453125" Y="4.035137939453" />
                  <Point X="23.2292734375" Y="4.037489257812" />
                  <Point X="23.249130859375" Y="4.043276855469" />
                  <Point X="23.258904296875" Y="4.046713378906" />
                  <Point X="23.32142578125" Y="4.072610839844" />
                  <Point X="23.358078125" Y="4.08779296875" />
                  <Point X="23.36741796875" Y="4.092272949219" />
                  <Point X="23.38555078125" Y="4.102221191406" />
                  <Point X="23.39434375" Y="4.107689453125" />
                  <Point X="23.41207421875" Y="4.120104003906" />
                  <Point X="23.420224609375" Y="4.126499511719" />
                  <Point X="23.435779296875" Y="4.140140625" />
                  <Point X="23.449755859375" Y="4.15539453125" />
                  <Point X="23.46198828125" Y="4.172079101562" />
                  <Point X="23.4676484375" Y="4.180755371094" />
                  <Point X="23.47846875" Y="4.199499511719" />
                  <Point X="23.4831484375" Y="4.208734375" />
                  <Point X="23.4914765625" Y="4.227662109375" />
                  <Point X="23.495125" Y="4.237354980469" />
                  <Point X="23.515474609375" Y="4.301895507813" />
                  <Point X="23.527404296875" Y="4.339731445312" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370049804688" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401863769531" />
                  <Point X="23.53769921875" Y="4.412216796875" />
                  <Point X="23.537248046875" Y="4.432898925781" />
                  <Point X="23.536458984375" Y="4.443228027344" />
                  <Point X="23.520734375" Y="4.562655273438" />
                  <Point X="23.8036796875" Y="4.641983398438" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.548578125" Y="4.772468261719" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.6525078125" Y="4.71638671875" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.309400390625" Y="4.528267578125" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.59635546875" Y="4.762158691406" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.224798828125" Y="4.642083007812" />
                  <Point X="26.453595703125" Y="4.58684375" />
                  <Point X="26.710435546875" Y="4.493687011719" />
                  <Point X="26.85826171875" Y="4.440069335938" />
                  <Point X="27.10808203125" Y="4.323236328125" />
                  <Point X="27.25044921875" Y="4.256654785156" />
                  <Point X="27.491845703125" Y="4.116018066406" />
                  <Point X="27.629419921875" Y="4.035865478516" />
                  <Point X="27.81778125" Y="3.901915283203" />
                  <Point X="27.6085" Y="3.539427978516" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.062380859375" Y="2.593114257812" />
                  <Point X="27.0531875" Y="2.573448242188" />
                  <Point X="27.044185546875" Y="2.549569580078" />
                  <Point X="27.041302734375" Y="2.540600341797" />
                  <Point X="27.027767578125" Y="2.489986083984" />
                  <Point X="27.01983203125" Y="2.460313964844" />
                  <Point X="27.0179140625" Y="2.451469970703" />
                  <Point X="27.013646484375" Y="2.420223388672" />
                  <Point X="27.012755859375" Y="2.383240478516" />
                  <Point X="27.013412109375" Y="2.369580810547" />
                  <Point X="27.018689453125" Y="2.325813964844" />
                  <Point X="27.021783203125" Y="2.300156005859" />
                  <Point X="27.02380078125" Y="2.289035400391" />
                  <Point X="27.02914453125" Y="2.267107910156" />
                  <Point X="27.032470703125" Y="2.256301025391" />
                  <Point X="27.04073828125" Y="2.234211425781" />
                  <Point X="27.045322265625" Y="2.223883544922" />
                  <Point X="27.05568359375" Y="2.203841552734" />
                  <Point X="27.0614609375" Y="2.194127441406" />
                  <Point X="27.08854296875" Y="2.154216308594" />
                  <Point X="27.104419921875" Y="2.130818847656" />
                  <Point X="27.10981640625" Y="2.123625244141" />
                  <Point X="27.130470703125" Y="2.100119384766" />
                  <Point X="27.1576015625" Y="2.075386474609" />
                  <Point X="27.168259765625" Y="2.066981933594" />
                  <Point X="27.208169921875" Y="2.039900512695" />
                  <Point X="27.231568359375" Y="2.024024536133" />
                  <Point X="27.24128125" Y="2.018245727539" />
                  <Point X="27.261326171875" Y="2.00788269043" />
                  <Point X="27.271658203125" Y="2.003298461914" />
                  <Point X="27.293744140625" Y="1.995033203125" />
                  <Point X="27.304546875" Y="1.991708007812" />
                  <Point X="27.326470703125" Y="1.986364990234" />
                  <Point X="27.337591796875" Y="1.984346923828" />
                  <Point X="27.381357421875" Y="1.979069213867" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.41604296875" Y="1.975320800781" />
                  <Point X="27.44757421875" Y="1.975497070312" />
                  <Point X="27.484314453125" Y="1.979822509766" />
                  <Point X="27.497748046875" Y="1.982395507812" />
                  <Point X="27.54836328125" Y="1.995930419922" />
                  <Point X="27.57803515625" Y="2.003865234375" />
                  <Point X="27.583998046875" Y="2.005671142578" />
                  <Point X="27.604412109375" Y="2.013069458008" />
                  <Point X="27.627658203125" Y="2.023574829102" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.187587890625" Y="2.346311767578" />
                  <Point X="28.940404296875" Y="2.780950439453" />
                  <Point X="28.9623359375" Y="2.750469970703" />
                  <Point X="29.0439609375" Y="2.637031738281" />
                  <Point X="29.136884765625" Y="2.483471923828" />
                  <Point X="28.888111328125" Y="2.292580566406" />
                  <Point X="28.172953125" Y="1.743819824219" />
                  <Point X="28.168138671875" Y="1.739869262695" />
                  <Point X="28.152130859375" Y="1.725226928711" />
                  <Point X="28.134671875" Y="1.706607177734" />
                  <Point X="28.12857421875" Y="1.699420288086" />
                  <Point X="28.0921484375" Y="1.65189855957" />
                  <Point X="28.07079296875" Y="1.624039306641" />
                  <Point X="28.0656328125" Y="1.616597167969" />
                  <Point X="28.04973828125" Y="1.589366333008" />
                  <Point X="28.034765625" Y="1.555545898438" />
                  <Point X="28.03014453125" Y="1.542676513672" />
                  <Point X="28.01657421875" Y="1.49415625" />
                  <Point X="28.008619140625" Y="1.465712036133" />
                  <Point X="28.006228515625" Y="1.454669799805" />
                  <Point X="28.0027734375" Y="1.432370483398" />
                  <Point X="28.001708984375" Y="1.42111340332" />
                  <Point X="28.000892578125" Y="1.397543945312" />
                  <Point X="28.001173828125" Y="1.386239746094" />
                  <Point X="28.003078125" Y="1.363750610352" />
                  <Point X="28.004701171875" Y="1.352566162109" />
                  <Point X="28.015841796875" Y="1.298581420898" />
                  <Point X="28.02237109375" Y="1.26693347168" />
                  <Point X="28.024599609375" Y="1.258233886719" />
                  <Point X="28.03468359375" Y="1.22861340332" />
                  <Point X="28.050287109375" Y="1.195373291016" />
                  <Point X="28.056919921875" Y="1.183526733398" />
                  <Point X="28.087216796875" Y="1.137477050781" />
                  <Point X="28.1049765625" Y="1.110481445312" />
                  <Point X="28.11173828125" Y="1.101426513672" />
                  <Point X="28.1262890625" Y="1.084184448242" />
                  <Point X="28.134078125" Y="1.075997192383" />
                  <Point X="28.151322265625" Y="1.05990612793" />
                  <Point X="28.16003125" Y="1.052698486328" />
                  <Point X="28.178244140625" Y="1.039370239258" />
                  <Point X="28.187748046875" Y="1.033249511719" />
                  <Point X="28.23165234375" Y="1.008535339355" />
                  <Point X="28.257390625" Y="0.994047119141" />
                  <Point X="28.26548046875" Y="0.989987915039" />
                  <Point X="28.294681640625" Y="0.978083374023" />
                  <Point X="28.33027734375" Y="0.968020996094" />
                  <Point X="28.343671875" Y="0.96525769043" />
                  <Point X="28.403033203125" Y="0.95741217041" />
                  <Point X="28.437833984375" Y="0.952813049316" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797302246" />
                  <Point X="29.02454296875" Y="1.021775085449" />
                  <Point X="29.704703125" Y="1.111319946289" />
                  <Point X="29.717630859375" Y="1.058218994141" />
                  <Point X="29.7526875" Y="0.914209228516" />
                  <Point X="29.783873046875" Y="0.713920959473" />
                  <Point X="29.511580078125" Y="0.640960327148" />
                  <Point X="28.6919921875" Y="0.421352813721" />
                  <Point X="28.686029296875" Y="0.419543457031" />
                  <Point X="28.665623046875" Y="0.412136260986" />
                  <Point X="28.642380859375" Y="0.401618804932" />
                  <Point X="28.634005859375" Y="0.397316619873" />
                  <Point X="28.575685546875" Y="0.363606567383" />
                  <Point X="28.54149609375" Y="0.343844177246" />
                  <Point X="28.533880859375" Y="0.338944702148" />
                  <Point X="28.508779296875" Y="0.319873840332" />
                  <Point X="28.48199609375" Y="0.294353210449" />
                  <Point X="28.472796875" Y="0.284226654053" />
                  <Point X="28.4378046875" Y="0.239638305664" />
                  <Point X="28.417291015625" Y="0.213498962402" />
                  <Point X="28.410853515625" Y="0.204207199097" />
                  <Point X="28.39912890625" Y="0.184925765991" />
                  <Point X="28.393841796875" Y="0.174936080933" />
                  <Point X="28.384068359375" Y="0.153472259521" />
                  <Point X="28.380005859375" Y="0.142926727295" />
                  <Point X="28.37316015625" Y="0.121426940918" />
                  <Point X="28.370376953125" Y="0.110472694397" />
                  <Point X="28.358712890625" Y="0.049567337036" />
                  <Point X="28.351875" Y="0.013862178802" />
                  <Point X="28.350603515625" Y="0.004966207981" />
                  <Point X="28.3485859375" Y="-0.026261835098" />
                  <Point X="28.35028125" Y="-0.062939880371" />
                  <Point X="28.351875" Y="-0.076422203064" />
                  <Point X="28.3635390625" Y="-0.137327713013" />
                  <Point X="28.370376953125" Y="-0.173032867432" />
                  <Point X="28.37316015625" Y="-0.183987121582" />
                  <Point X="28.380005859375" Y="-0.205486755371" />
                  <Point X="28.384068359375" Y="-0.216032440186" />
                  <Point X="28.393841796875" Y="-0.237496261597" />
                  <Point X="28.39912890625" Y="-0.247485946655" />
                  <Point X="28.410853515625" Y="-0.26676739502" />
                  <Point X="28.417291015625" Y="-0.276059143066" />
                  <Point X="28.452283203125" Y="-0.320647338867" />
                  <Point X="28.472796875" Y="-0.346786987305" />
                  <Point X="28.47872265625" Y="-0.353638336182" />
                  <Point X="28.501142578125" Y="-0.375806182861" />
                  <Point X="28.530177734375" Y="-0.39872442627" />
                  <Point X="28.54149609375" Y="-0.406404052734" />
                  <Point X="28.59981640625" Y="-0.440114257812" />
                  <Point X="28.634005859375" Y="-0.459876495361" />
                  <Point X="28.63949609375" Y="-0.462814758301" />
                  <Point X="28.659154296875" Y="-0.472014526367" />
                  <Point X="28.683025390625" Y="-0.481026855469" />
                  <Point X="28.6919921875" Y="-0.483912963867" />
                  <Point X="29.17246875" Y="-0.612655883789" />
                  <Point X="29.78487890625" Y="-0.776751098633" />
                  <Point X="29.7811875" Y="-0.801235595703" />
                  <Point X="29.761619140625" Y="-0.931035400391" />
                  <Point X="29.727802734375" Y="-1.079219848633" />
                  <Point X="29.3872734375" Y="-1.034388305664" />
                  <Point X="28.436783203125" Y="-0.909253662109" />
                  <Point X="28.428625" Y="-0.908535644531" />
                  <Point X="28.40009765625" Y="-0.908042541504" />
                  <Point X="28.36672265625" Y="-0.910840942383" />
                  <Point X="28.354482421875" Y="-0.912676147461" />
                  <Point X="28.24001953125" Y="-0.937555175781" />
                  <Point X="28.17291796875" Y="-0.952140136719" />
                  <Point X="28.157875" Y="-0.956742370605" />
                  <Point X="28.128755859375" Y="-0.968366027832" />
                  <Point X="28.1146796875" Y="-0.975387573242" />
                  <Point X="28.0868515625" Y="-0.992280029297" />
                  <Point X="28.074123046875" Y="-1.001530944824" />
                  <Point X="28.050373046875" Y="-1.022002380371" />
                  <Point X="28.0393515625" Y="-1.03322277832" />
                  <Point X="27.970166015625" Y="-1.116430664063" />
                  <Point X="27.929607421875" Y="-1.165210449219" />
                  <Point X="27.921326171875" Y="-1.176849609375" />
                  <Point X="27.906603515625" Y="-1.201236816406" />
                  <Point X="27.900162109375" Y="-1.213984619141" />
                  <Point X="27.8888203125" Y="-1.241369995117" />
                  <Point X="27.88436328125" Y="-1.254934814453" />
                  <Point X="27.877533203125" Y="-1.282580078125" />
                  <Point X="27.87516015625" Y="-1.296660766602" />
                  <Point X="27.865244140625" Y="-1.404418945312" />
                  <Point X="27.859431640625" Y="-1.467590942383" />
                  <Point X="27.859291015625" Y="-1.483319946289" />
                  <Point X="27.861609375" Y="-1.514590942383" />
                  <Point X="27.864068359375" Y="-1.53013293457" />
                  <Point X="27.87180078125" Y="-1.56175402832" />
                  <Point X="27.876791015625" Y="-1.576673950195" />
                  <Point X="27.889162109375" Y="-1.605482055664" />
                  <Point X="27.89654296875" Y="-1.619370361328" />
                  <Point X="27.95988671875" Y="-1.717899536133" />
                  <Point X="27.9970234375" Y="-1.775660888672" />
                  <Point X="28.001744140625" Y="-1.782352783203" />
                  <Point X="28.019794921875" Y="-1.804448120117" />
                  <Point X="28.043490234375" Y="-1.828119018555" />
                  <Point X="28.052798828125" Y="-1.836277832031" />
                  <Point X="28.498681640625" Y="-2.178416503906" />
                  <Point X="29.087171875" Y="-2.629980712891" />
                  <Point X="29.04546875" Y="-2.697459716797" />
                  <Point X="29.001275390625" Y="-2.760251953125" />
                  <Point X="28.69499609375" Y="-2.583421142578" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.63487890625" Y="-2.041734985352" />
                  <Point X="27.555017578125" Y="-2.027311889648" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503295898" />
                  <Point X="27.460140625" Y="-2.03146105957" />
                  <Point X="27.44484375" Y="-2.035136352539" />
                  <Point X="27.415068359375" Y="-2.044960083008" />
                  <Point X="27.40058984375" Y="-2.051108642578" />
                  <Point X="27.28741796875" Y="-2.110670166016" />
                  <Point X="27.221072265625" Y="-2.145587646484" />
                  <Point X="27.20896875" Y="-2.153170410156" />
                  <Point X="27.186037109375" Y="-2.170064208984" />
                  <Point X="27.175208984375" Y="-2.179375244141" />
                  <Point X="27.15425" Y="-2.200334716797" />
                  <Point X="27.14494140625" Y="-2.211161132813" />
                  <Point X="27.128048828125" Y="-2.234090576172" />
                  <Point X="27.12046484375" Y="-2.246193603516" />
                  <Point X="27.06090234375" Y="-2.359365478516" />
                  <Point X="27.025984375" Y="-2.425711669922" />
                  <Point X="27.0198359375" Y="-2.440193359375" />
                  <Point X="27.01001171875" Y="-2.469972167969" />
                  <Point X="27.0063359375" Y="-2.485269287109" />
                  <Point X="27.00137890625" Y="-2.517442871094" />
                  <Point X="27.000279296875" Y="-2.533134033203" />
                  <Point X="27.00068359375" Y="-2.564484375" />
                  <Point X="27.0021875" Y="-2.580143554688" />
                  <Point X="27.026791015625" Y="-2.716371582031" />
                  <Point X="27.04121484375" Y="-2.796233886719" />
                  <Point X="27.04301953125" Y="-2.804229248047" />
                  <Point X="27.05123828125" Y="-2.831542236328" />
                  <Point X="27.064072265625" Y="-2.862479980469" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.356076171875" Y="-3.369854980469" />
                  <Point X="27.735896484375" Y="-4.027723388672" />
                  <Point X="27.723755859375" Y="-4.036082763672" />
                  <Point X="27.479904296875" Y="-3.718290283203" />
                  <Point X="26.83391796875" Y="-2.876422363281" />
                  <Point X="26.828654296875" Y="-2.870143066406" />
                  <Point X="26.808830078125" Y="-2.849624023438" />
                  <Point X="26.783251953125" Y="-2.828003662109" />
                  <Point X="26.77330078125" Y="-2.820646728516" />
                  <Point X="26.63894140625" Y="-2.734267089844" />
                  <Point X="26.56017578125" Y="-2.683627929688" />
                  <Point X="26.546283203125" Y="-2.676245605469" />
                  <Point X="26.51747265625" Y="-2.663874511719" />
                  <Point X="26.5025546875" Y="-2.658885742188" />
                  <Point X="26.47093359375" Y="-2.651154052734" />
                  <Point X="26.45539453125" Y="-2.648695800781" />
                  <Point X="26.424125" Y="-2.646376953125" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.261453125" Y="-2.660038085938" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670339111328" />
                  <Point X="26.133578125" Y="-2.677170410156" />
                  <Point X="26.12001171875" Y="-2.681627929688" />
                  <Point X="26.092626953125" Y="-2.692970703125" />
                  <Point X="26.07987890625" Y="-2.699412841797" />
                  <Point X="26.05549609375" Y="-2.714134033203" />
                  <Point X="26.043861328125" Y="-2.722413085938" />
                  <Point X="25.930396484375" Y="-2.816755859375" />
                  <Point X="25.863876953125" Y="-2.872063476562" />
                  <Point X="25.852654296875" Y="-2.883088134766" />
                  <Point X="25.83218359375" Y="-2.906838134766" />
                  <Point X="25.822935546875" Y="-2.919563476563" />
                  <Point X="25.80604296875" Y="-2.947390380859" />
                  <Point X="25.799021484375" Y="-2.961466552734" />
                  <Point X="25.787396484375" Y="-2.990586914062" />
                  <Point X="25.78279296875" Y="-3.005631103516" />
                  <Point X="25.7488671875" Y="-3.161715820312" />
                  <Point X="25.728978515625" Y="-3.253218994141" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520019531" />
                  <Point X="25.80675390625" Y="-3.952288330078" />
                  <Point X="25.833087890625" Y="-4.152313964844" />
                  <Point X="25.655068359375" Y="-3.487937255859" />
                  <Point X="25.652607421875" Y="-3.480123291016" />
                  <Point X="25.642146484375" Y="-3.453577392578" />
                  <Point X="25.6267890625" Y="-3.423814697266" />
                  <Point X="25.62041015625" Y="-3.413209472656" />
                  <Point X="25.51719140625" Y="-3.264492675781" />
                  <Point X="25.456681640625" Y="-3.177309082031" />
                  <Point X="25.446669921875" Y="-3.165170898438" />
                  <Point X="25.42478515625" Y="-3.142715820312" />
                  <Point X="25.412912109375" Y="-3.132398925781" />
                  <Point X="25.38665625" Y="-3.113154785156" />
                  <Point X="25.3732421875" Y="-3.104937988281" />
                  <Point X="25.3452421875" Y="-3.090830078125" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.17093359375" Y="-3.035366699219" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.77529296875" Y="-3.055877197266" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.667068359375" Y="-3.090830078125" />
                  <Point X="24.639068359375" Y="-3.104938232422" />
                  <Point X="24.62565625" Y="-3.113154785156" />
                  <Point X="24.599400390625" Y="-3.132398925781" />
                  <Point X="24.587525390625" Y="-3.142717041016" />
                  <Point X="24.565640625" Y="-3.165172851562" />
                  <Point X="24.555630859375" Y="-3.177310546875" />
                  <Point X="24.4524140625" Y="-3.326027099609" />
                  <Point X="24.391904296875" Y="-3.4132109375" />
                  <Point X="24.38753125" Y="-3.420131591797" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213623047" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.20919921875" Y="-4.040452636719" />
                  <Point X="24.014572265625" Y="-4.766805664062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.461647401044" Y="1.305699307387" />
                  <Point X="20.253922442758" Y="0.581276287514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.913050233233" Y="2.535272312894" />
                  <Point X="20.831041407671" Y="2.249273550105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.556880738763" Y="1.293161673423" />
                  <Point X="20.345699384657" Y="0.556684768845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.086016471311" Y="2.793821518401" />
                  <Point X="20.912046464613" Y="2.18711600425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.652114062934" Y="1.280623992211" />
                  <Point X="20.437476326557" Y="0.532093250176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.242532503485" Y="2.995002038237" />
                  <Point X="20.993051521556" Y="2.124958458394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.747347387105" Y="1.268086310999" />
                  <Point X="20.529253268457" Y="0.507501731508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.341588538461" Y="2.9957957339" />
                  <Point X="21.074056578499" Y="2.062800912539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.842580711277" Y="1.255548629787" />
                  <Point X="20.621030210357" Y="0.482910212839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.301604320738" Y="-0.631060248355" />
                  <Point X="20.240600537714" Y="-0.8438057224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.426379611418" Y="2.946841594979" />
                  <Point X="21.155061640594" Y="2.000643384653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.937814035448" Y="1.243010948575" />
                  <Point X="20.712807152256" Y="0.45831869417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.408658053182" Y="-0.602375267024" />
                  <Point X="20.279413790868" Y="-1.053103574202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.511170684376" Y="2.897887456058" />
                  <Point X="21.236066727541" Y="1.938485943433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.033047359619" Y="1.230473267364" />
                  <Point X="20.804584094156" Y="0.433727175501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.515711785626" Y="-0.573690285692" />
                  <Point X="20.32597470979" Y="-1.235382104495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.595961757333" Y="2.848933317137" />
                  <Point X="21.317071814487" Y="1.876328502214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.12828068379" Y="1.217935586152" />
                  <Point X="20.896361036056" Y="0.409135656832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.622765518901" Y="-0.545005301464" />
                  <Point X="20.412875404304" Y="-1.276979118728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.680752830291" Y="2.799979178216" />
                  <Point X="21.398076901434" Y="1.814171060995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.22353843235" Y="1.205483082907" />
                  <Point X="20.988137963403" Y="0.384544087411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.729819252684" Y="-0.516320315465" />
                  <Point X="20.515581071587" Y="-1.263457642644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.064973541006" Y="3.795260282924" />
                  <Point X="22.005437849088" Y="3.587634651007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.766075646247" Y="2.752879447508" />
                  <Point X="21.478258630693" Y="1.749142230281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.324963955265" Y="1.214540165031" />
                  <Point X="21.079914875484" Y="0.359952464754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.836872986466" Y="-0.487635329466" />
                  <Point X="20.61828673887" Y="-1.249936166559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.191651481299" Y="3.892383010156" />
                  <Point X="22.071470530922" Y="3.473262227938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.858339098512" Y="2.729984592116" />
                  <Point X="21.54682484101" Y="1.643605271041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.436029786014" Y="1.257216995939" />
                  <Point X="21.171691787566" Y="0.335360842098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.943926720249" Y="-0.458950343467" />
                  <Point X="20.720992406153" Y="-1.236414690474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.318329457319" Y="3.989505861984" />
                  <Point X="22.137503330595" Y="3.358890215824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.955351520321" Y="2.723651361701" />
                  <Point X="21.263468699648" Y="0.310769219441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.050980454031" Y="-0.430265357468" />
                  <Point X="20.823698073435" Y="-1.22289321439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.438284147284" Y="4.063181828914" />
                  <Point X="22.203536133234" Y="3.244518214053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.061924140676" Y="2.750658505785" />
                  <Point X="21.35524561173" Y="0.286177596785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.158034187814" Y="-0.401580371469" />
                  <Point X="20.926403740718" Y="-1.209371738305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.555840502319" Y="4.128493807964" />
                  <Point X="22.249750138846" Y="3.061029853268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.194643749278" Y="2.868851034342" />
                  <Point X="21.447022523811" Y="0.261585974128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.265087921597" Y="-0.37289538547" />
                  <Point X="21.029109408001" Y="-1.19585026222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.673396857353" Y="4.193805787015" />
                  <Point X="21.537059540288" Y="0.230926614407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.372141655379" Y="-0.344210399471" />
                  <Point X="21.131815075284" Y="-1.182328786136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.790952980739" Y="4.259116958212" />
                  <Point X="21.619291991422" Y="0.173049500784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.479195389162" Y="-0.315525413472" />
                  <Point X="21.234520735989" Y="-1.168807332987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.883236876716" Y="-2.393879737707" />
                  <Point X="20.852708723719" Y="-2.500344059412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.869307433713" Y="4.187715657791" />
                  <Point X="21.687558400378" Y="0.066467009943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.594784790122" Y="-0.25707301847" />
                  <Point X="21.337226394874" Y="-1.155285886192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.009944490123" Y="-2.296653528028" />
                  <Point X="20.91944735445" Y="-2.6122545461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.946955098635" Y="4.113849494509" />
                  <Point X="21.439932053758" Y="-1.141764439397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.136652106079" Y="-2.19942730946" />
                  <Point X="20.986185985181" Y="-2.724165032788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.03259847399" Y="4.067867687279" />
                  <Point X="21.542637712642" Y="-1.128242992602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.263359774133" Y="-2.102200909204" />
                  <Point X="21.057231199259" Y="-2.821056678509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.121634896308" Y="4.033718841038" />
                  <Point X="21.645343371526" Y="-1.114721545806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.390067442187" Y="-2.004974508948" />
                  <Point X="21.129016363544" Y="-2.915367811188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.220990696348" Y="4.035557941716" />
                  <Point X="21.74804903041" Y="-1.101200099011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.516775110241" Y="-1.907748108693" />
                  <Point X="21.200801875207" Y="-3.009677732419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.331659394201" Y="4.076849805626" />
                  <Point X="21.844985296084" Y="-1.107798917432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.643482778295" Y="-1.810521708437" />
                  <Point X="21.315265645073" Y="-2.955150879551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.574078869337" Y="4.577611233221" />
                  <Point X="23.536171402914" Y="4.445412187289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.455100534655" Y="4.16268447035" />
                  <Point X="21.932980318318" Y="-1.145579557366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.770190446349" Y="-1.713295308181" />
                  <Point X="21.433701461581" Y="-2.886771853855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.681547063763" Y="4.607741615258" />
                  <Point X="22.010470700275" Y="-1.219994231535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.896898114404" Y="-1.616068907925" />
                  <Point X="21.552137278089" Y="-2.818392828159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.78901525819" Y="4.637871997294" />
                  <Point X="21.670573094597" Y="-2.750013802463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.896483406192" Y="4.668002217428" />
                  <Point X="21.789008911105" Y="-2.681634776767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.003951546859" Y="4.698132411978" />
                  <Point X="21.90744469899" Y="-2.613255850889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.109355416908" Y="4.721063639364" />
                  <Point X="22.025880467524" Y="-2.544876992497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.211615652609" Y="4.733031710917" />
                  <Point X="22.144316236059" Y="-2.476498134106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.31387588831" Y="4.74499978247" />
                  <Point X="22.262752004593" Y="-2.408119275715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.416136124011" Y="4.756967854023" />
                  <Point X="22.376503780604" Y="-2.356075440501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.518396359713" Y="4.768935925576" />
                  <Point X="22.47659320769" Y="-2.351677878266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.084916069859" Y="-3.717618386062" />
                  <Point X="22.046259302875" Y="-3.852430553594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.620656599264" Y="4.780904010556" />
                  <Point X="22.564913209844" Y="-2.388325178536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.281260617646" Y="-3.3775393256" />
                  <Point X="22.127214185478" Y="-3.914763078167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.675924936965" Y="4.628991858279" />
                  <Point X="22.642354151083" Y="-2.462912272977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.477604601413" Y="-3.037462232112" />
                  <Point X="22.208169068081" Y="-3.97709560274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.723664454762" Y="4.450823590727" />
                  <Point X="22.290472176969" Y="-4.034726303492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.77140397256" Y="4.272655323175" />
                  <Point X="22.374399617728" Y="-4.086692285815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.836876356288" Y="4.156328908398" />
                  <Point X="22.458326972795" Y="-4.138658566983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.920230732512" Y="4.10236441254" />
                  <Point X="22.582262692478" Y="-4.051099099513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.014112496623" Y="4.08511328125" />
                  <Point X="22.711584269601" Y="-3.944756915016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.121702484001" Y="4.115668405785" />
                  <Point X="22.833835938932" Y="-3.86307042907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.411461448139" Y="4.78152225109" />
                  <Point X="22.952694261839" Y="-3.793217948455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.507408579386" Y="4.771473910985" />
                  <Point X="23.056441327716" Y="-3.776064683871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.603355713814" Y="4.761425581972" />
                  <Point X="23.153446648436" Y="-3.782422678725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.699302888653" Y="4.75137739389" />
                  <Point X="23.250451952965" Y="-3.788780730042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.795250063492" Y="4.741329205808" />
                  <Point X="23.347457254634" Y="-3.795138791335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.88887588522" Y="4.723185497354" />
                  <Point X="23.439996703544" Y="-3.817071132044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.981305480464" Y="4.700870051387" />
                  <Point X="23.523436075895" Y="-3.870739211181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.073735075709" Y="4.678554605419" />
                  <Point X="23.602406089437" Y="-3.939993796787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.166164670953" Y="4.656239159452" />
                  <Point X="23.681375847596" Y="-4.009249273017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.258594249938" Y="4.633923656779" />
                  <Point X="23.759220816925" Y="-4.082427354061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.351023800712" Y="4.611608055726" />
                  <Point X="23.820270393627" Y="-4.214177929941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.443453351487" Y="4.589292454673" />
                  <Point X="23.860748749964" Y="-4.41766887685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.533291042066" Y="4.55793796294" />
                  <Point X="23.868889134573" Y="-4.733935733447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.622809271566" Y="4.525469378023" />
                  <Point X="23.961259688966" Y="-4.756457079335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.712327499105" Y="4.493000786269" />
                  <Point X="24.482819106681" Y="-3.282218984135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.80184563585" Y="4.460531877877" />
                  <Point X="24.63107842701" Y="-3.109833040448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.890485305377" Y="4.424999390221" />
                  <Point X="24.742456154416" Y="-3.066068496633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.977627796523" Y="4.384245621054" />
                  <Point X="24.850939046047" Y="-3.03239944491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.064770287669" Y="4.343491851887" />
                  <Point X="24.95858588106" Y="-3.001646069115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.151912705547" Y="4.302737827204" />
                  <Point X="25.057260572128" Y="-3.002182277702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.239055051024" Y="4.261983550031" />
                  <Point X="25.148564276568" Y="-3.028424171525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.324058556466" Y="4.213770251222" />
                  <Point X="25.239316229443" Y="-3.056590251721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.408740334008" Y="4.164434953891" />
                  <Point X="25.330068151378" Y="-3.084756439823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.493422104946" Y="4.115099633529" />
                  <Point X="27.045555935521" Y="2.553204685369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.032461960766" Y="2.507540568683" />
                  <Point X="25.414771935291" Y="-3.134014991819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.578103527748" Y="4.065763099072" />
                  <Point X="27.248266170819" Y="2.9154835364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.048300934704" Y="2.218121883706" />
                  <Point X="25.488174702989" Y="-3.222684870992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.66176362112" Y="4.012864765609" />
                  <Point X="27.444610888068" Y="3.255563187844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.117519217507" Y="2.114858971471" />
                  <Point X="25.558109874262" Y="-3.323447696024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.743852879829" Y="3.954488280655" />
                  <Point X="27.640955683066" Y="3.595643110432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.197022641014" Y="2.047464607483" />
                  <Point X="25.627669355894" Y="-3.425520706535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.282069428459" Y="1.999402250957" />
                  <Point X="25.89187400537" Y="-2.848785347285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.733971939412" Y="-3.399455292817" />
                  <Point X="25.680927296747" Y="-3.584443945818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.375277699283" Y="1.979802369454" />
                  <Point X="26.021641434452" Y="-2.740888292224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.76506919545" Y="-3.63566202441" />
                  <Point X="25.728667131809" Y="-3.762611106935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.47375546387" Y="1.97857939661" />
                  <Point X="26.139134337463" Y="-2.675797596677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.796166451487" Y="-3.871868756003" />
                  <Point X="25.776406966872" Y="-3.940778268053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.580005629572" Y="2.004462007678" />
                  <Point X="26.241967584661" Y="-2.661831196554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.827263743682" Y="-4.1080753615" />
                  <Point X="25.824146801934" Y="-4.118945429171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.695369975349" Y="2.062129541982" />
                  <Point X="26.343474466705" Y="-2.652490381425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.813805772622" Y="2.130508500599" />
                  <Point X="26.443640921692" Y="-2.647824190978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.932241569895" Y="2.198887459216" />
                  <Point X="26.535631206258" Y="-2.67167169535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.050677367169" Y="2.267266417834" />
                  <Point X="26.620002182651" Y="-2.722090885097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.169113164442" Y="2.335645376451" />
                  <Point X="26.703447542548" Y="-2.775738083182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.287548952369" Y="2.404024302474" />
                  <Point X="28.058016067904" Y="1.603548005852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.001160117442" Y="1.405267742995" />
                  <Point X="26.786501468815" Y="-2.830750372564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.405984738568" Y="2.472403222472" />
                  <Point X="28.203869204727" Y="1.76754259043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.043778251383" Y="1.209239087407" />
                  <Point X="26.861810418699" Y="-2.912772604451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.524420524768" Y="2.54078214247" />
                  <Point X="28.330576840015" Y="1.864768876414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.111704797359" Y="1.101471353505" />
                  <Point X="27.169219748277" Y="-2.18536461977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.022990304106" Y="-2.695327295487" />
                  <Point X="26.933753943068" Y="-3.006531469887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.642856310967" Y="2.609161062468" />
                  <Point X="28.457284475302" Y="1.961995162399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.190523030169" Y="1.031687445581" />
                  <Point X="27.289830575979" Y="-2.109400428618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.070322652202" Y="-2.874915532537" />
                  <Point X="27.005697467437" Y="-3.100290335324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.761292097167" Y="2.677539982466" />
                  <Point X="28.583992110589" Y="2.059221448383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.276147382721" Y="0.985639297955" />
                  <Point X="27.406037263763" Y="-2.048795298633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.13635543015" Y="-2.989287620414" />
                  <Point X="27.077640991806" Y="-3.19404920076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.879727883366" Y="2.745918902464" />
                  <Point X="28.710699745877" Y="2.156447734367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.368201863413" Y="0.962015672078" />
                  <Point X="27.511560151356" Y="-2.025449007745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.202388208099" Y="-3.103659708291" />
                  <Point X="27.149584516175" Y="-3.287808066196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.974867464032" Y="2.733054298797" />
                  <Point X="28.837407381164" Y="2.253674020352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.463958273838" Y="0.951302209422" />
                  <Point X="27.607154428561" Y="-2.036727896133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.268420986047" Y="-3.218031796168" />
                  <Point X="27.221528040544" Y="-3.381566931633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.045454262426" Y="2.6345639676" />
                  <Point X="28.964115083037" Y="2.350900538547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.565671734975" Y="0.961363451463" />
                  <Point X="27.969720065766" Y="-1.116967007567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.860396052909" Y="-1.498225149063" />
                  <Point X="27.701116927813" Y="-2.053697470527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.334453763996" Y="-3.332403884045" />
                  <Point X="27.293471564913" Y="-3.475325797069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.11250857907" Y="2.523754408525" />
                  <Point X="29.09082282933" Y="2.448127211655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.668377402756" Y="0.974884929284" />
                  <Point X="28.468916517935" Y="0.27928215858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.357899526769" Y="-0.107880099923" />
                  <Point X="28.107979512228" Y="-0.979454768439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.915867484893" Y="-1.649429027402" />
                  <Point X="27.794428597439" Y="-2.072936757553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.400486529609" Y="-3.446776014939" />
                  <Point X="27.365415089282" Y="-3.569084662505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.771083070536" Y="0.988406407106" />
                  <Point X="28.595150842049" Y="0.374857812342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.41107578227" Y="-0.267088209881" />
                  <Point X="28.217413532608" Y="-0.942468736582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.984212946209" Y="-1.755735829901" />
                  <Point X="27.881546571174" Y="-2.113776029095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.466519289217" Y="-3.561148166778" />
                  <Point X="27.437358613651" Y="-3.662843527942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.873788738317" Y="1.001927884927" />
                  <Point X="28.708586507886" Y="0.425799240369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.483680674908" Y="-0.358540610064" />
                  <Point X="28.322810927614" Y="-0.919560090356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.05865731437" Y="-1.840773216574" />
                  <Point X="27.966337646506" Y="-2.162730159734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.532552048825" Y="-3.675520318616" />
                  <Point X="27.509302185161" Y="-3.756602228976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.976494406098" Y="1.015449362748" />
                  <Point X="28.815640217835" Y="0.45448414325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.564904667737" Y="-0.419934635748" />
                  <Point X="28.424819445122" Y="-0.908469864463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.139662381205" Y="-1.902930727931" />
                  <Point X="28.051128721838" Y="-2.211684290374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.598584808433" Y="-3.789892470454" />
                  <Point X="27.581245824897" Y="-3.85036069208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.07920007254" Y="1.028970835903" />
                  <Point X="28.922693927784" Y="0.483169046131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.650024587837" Y="-0.467741948394" />
                  <Point X="28.520271393871" Y="-0.920245111166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.22066744804" Y="-1.965088239289" />
                  <Point X="28.13591979717" Y="-2.260638421013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.664617568041" Y="-3.904264622293" />
                  <Point X="27.653189464633" Y="-3.944119155184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.181905737807" Y="1.042492304956" />
                  <Point X="29.029747637733" Y="0.511853949012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.740489850962" Y="-0.496907834566" />
                  <Point X="28.6155047051" Y="-0.932782837514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.301672514875" Y="-2.027245750646" />
                  <Point X="28.220710872502" Y="-2.309592551653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.730650327649" Y="-4.018636774131" />
                  <Point X="27.726113197027" Y="-4.034459628995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.284611403073" Y="1.056013774009" />
                  <Point X="29.136801347682" Y="0.540538851893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.832266802215" Y="-0.52149932062" />
                  <Point X="28.710738016328" Y="-0.945320563862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.38267758171" Y="-2.089403262003" />
                  <Point X="28.305501947835" Y="-2.358546682292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.38731706834" Y="1.069535243061" />
                  <Point X="29.243855057631" Y="0.569223754774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.924043753467" Y="-0.546090806673" />
                  <Point X="28.805971327557" Y="-0.95785829021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.463682648545" Y="-2.151560773361" />
                  <Point X="28.390293023167" Y="-2.407500812931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.490022733606" Y="1.083056712114" />
                  <Point X="29.35090876758" Y="0.597908657655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.015820704719" Y="-0.570682292726" />
                  <Point X="28.901204638785" Y="-0.970396016558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.544687737948" Y="-2.213718206012" />
                  <Point X="28.475084098499" Y="-2.456454943571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.592728398873" Y="1.096578181167" />
                  <Point X="29.457962477529" Y="0.626593560536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.107597655971" Y="-0.59527377878" />
                  <Point X="28.996437950014" Y="-0.982933742907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.62569284452" Y="-2.275875578788" />
                  <Point X="28.559875173831" Y="-2.50540907421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.695434064139" Y="1.11009965022" />
                  <Point X="29.565016192606" Y="0.655278481302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.199374596679" Y="-0.619865301605" />
                  <Point X="29.091671261243" Y="-0.995471469255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.706697951093" Y="-2.338032951564" />
                  <Point X="28.644666249163" Y="-2.554363204849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.745985866156" Y="0.941738983276" />
                  <Point X="29.67206991283" Y="0.683963420013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.291151511964" Y="-0.644456913089" />
                  <Point X="29.186904572471" Y="-1.008009195603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.787703057665" Y="-2.40019032434" />
                  <Point X="28.729457312134" Y="-2.6033173786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.782330085176" Y="0.723830586172" />
                  <Point X="29.779123633053" Y="0.712648358725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.382928427249" Y="-0.669048524574" />
                  <Point X="29.2821378837" Y="-1.020546921951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.868708164238" Y="-2.462347697116" />
                  <Point X="28.81424835705" Y="-2.652271615313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.474705342535" Y="-0.693640136058" />
                  <Point X="29.377371194929" Y="-1.033084648299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.94971327081" Y="-2.524505069892" />
                  <Point X="28.899039401966" Y="-2.701225852027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.56648225782" Y="-0.718231747542" />
                  <Point X="29.47260451114" Y="-1.04562235727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.030718377383" Y="-2.586662442668" />
                  <Point X="28.983830446882" Y="-2.75018008874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.658259173106" Y="-0.742823359026" />
                  <Point X="29.56783782793" Y="-1.058160064225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.750036088391" Y="-0.76741497051" />
                  <Point X="29.663071144719" Y="-1.070697771179" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.724076171875" Y="-4.479585449219" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544433594" />
                  <Point X="25.3611015625" Y="-3.372827636719" />
                  <Point X="25.300591796875" Y="-3.285644042969" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.11461328125" Y="-3.216827636719" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.83161328125" Y="-3.237338623047" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285644042969" />
                  <Point X="24.60850390625" Y="-3.434360595703" />
                  <Point X="24.547994140625" Y="-3.521544433594" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.3927265625" Y="-4.089628417969" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.00905859375" Y="-4.95928125" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.71480078125" Y="-4.890478027344" />
                  <Point X="23.648412109375" Y="-4.873396484375" />
                  <Point X="23.660451171875" Y="-4.781943847656" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534756835938" />
                  <Point X="23.65216015625" Y="-4.342924804687" />
                  <Point X="23.6297890625" Y="-4.230465332031" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.466666015625" Y="-4.073666748047" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.155587890625" Y="-3.972970703125" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.847494140625" Y="-4.082455566406" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.657150390625" Y="-4.265613769531" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.304380859375" Y="-4.266811523438" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.88805859375" Y="-3.970416259766" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.024658203125" Y="-3.441984375" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597592773438" />
                  <Point X="22.486021484375" Y="-2.568763427734" />
                  <Point X="22.468677734375" Y="-2.551419677734" />
                  <Point X="22.43984765625" Y="-2.53719921875" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.931783203125" Y="-2.818597167969" />
                  <Point X="21.15704296875" Y="-3.265893554688" />
                  <Point X="20.964875" Y="-3.013426513672" />
                  <Point X="20.838294921875" Y="-2.847125244141" />
                  <Point X="20.6546875" Y="-2.539244384766" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="21.015078125" Y="-2.053224853516" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396014404297" />
                  <Point X="21.8618828125" Y="-1.366267089844" />
                  <Point X="21.85967578125" Y="-1.334596435547" />
                  <Point X="21.838841796875" Y="-1.310639770508" />
                  <Point X="21.812359375" Y="-1.295053100586" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.178892578125" Y="-1.367770507812" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.12211328125" Y="-1.20500390625" />
                  <Point X="20.072607421875" Y="-1.011188415527" />
                  <Point X="20.024029296875" Y="-0.671535583496" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="20.507140625" Y="-0.379284484863" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.458103515625" Y="-0.121426269531" />
                  <Point X="21.475599609375" Y="-0.109282974243" />
                  <Point X="21.485857421875" Y="-0.102164085388" />
                  <Point X="21.505103515625" Y="-0.075903800964" />
                  <Point X="21.514353515625" Y="-0.046096923828" />
                  <Point X="21.514353515625" Y="-0.016463153839" />
                  <Point X="21.505103515625" Y="0.013343723297" />
                  <Point X="21.485857421875" Y="0.039604011536" />
                  <Point X="21.468361328125" Y="0.051747299194" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.894169921875" Y="0.213020202637" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.050451171875" Y="0.780806945801" />
                  <Point X="20.08235546875" Y="0.996414794922" />
                  <Point X="20.180142578125" Y="1.357282104492" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.57621875" Y="1.482255371094" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866333008" />
                  <Point X="21.30701953125" Y="1.408075927734" />
                  <Point X="21.32972265625" Y="1.415233764648" />
                  <Point X="21.348466796875" Y="1.426055664062" />
                  <Point X="21.360880859375" Y="1.443785522461" />
                  <Point X="21.37641796875" Y="1.481298339844" />
                  <Point X="21.38552734375" Y="1.503289794922" />
                  <Point X="21.389287109375" Y="1.524605957031" />
                  <Point X="21.38368359375" Y="1.545513427734" />
                  <Point X="21.364935546875" Y="1.581529296875" />
                  <Point X="21.353943359375" Y="1.602643188477" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.025486328125" Y="1.860580932617" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.716013671875" Y="2.574612792969" />
                  <Point X="20.83998828125" Y="2.7870078125" />
                  <Point X="21.09901953125" Y="3.119958496094" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.42903125" Y="3.164703369141" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.91541796875" Y="2.915716308594" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.9684921875" Y="2.915774902344" />
                  <Point X="21.98674609375" Y="2.927403564453" />
                  <Point X="22.02502734375" Y="2.965685058594" />
                  <Point X="22.047470703125" Y="2.988126953125" />
                  <Point X="22.0591015625" Y="3.006381347656" />
                  <Point X="22.061927734375" Y="3.027840332031" />
                  <Point X="22.057208984375" Y="3.081772460938" />
                  <Point X="22.054443359375" Y="3.113389892578" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.908548828125" Y="3.375454101562" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="22.03120703125" Y="4.0087890625" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.655095703125" Y="4.400991210938" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.90610546875" Y="4.451868652344" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.048755859375" Y="4.27366015625" />
                  <Point X="23.10878125" Y="4.242412109375" />
                  <Point X="23.14397265625" Y="4.224093261719" />
                  <Point X="23.16487890625" Y="4.218491699219" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.24871484375" Y="4.248147949219" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744628906" />
                  <Point X="23.31391796875" Y="4.294488769531" />
                  <Point X="23.334267578125" Y="4.359029296875" />
                  <Point X="23.346197265625" Y="4.396865234375" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.332236328125" Y="4.538792480469" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.75238671875" Y="4.824928710938" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.5264921875" Y="4.961180175781" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.83603515625" Y="4.7655625" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.125873046875" Y="4.577443359375" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.61614453125" Y="4.951125488281" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.269388671875" Y="4.826776367188" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.775220703125" Y="4.672301269531" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.1885703125" Y="4.495344726562" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.587490234375" Y="4.280188476563" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="27.96716796875" Y="4.028826904297" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.77304296875" Y="3.444427978516" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491515869141" />
                  <Point X="27.211318359375" Y="2.440901611328" />
                  <Point X="27.2033828125" Y="2.411229492188" />
                  <Point X="27.202044921875" Y="2.392325927734" />
                  <Point X="27.207322265625" Y="2.348559082031" />
                  <Point X="27.210416015625" Y="2.322901123047" />
                  <Point X="27.21868359375" Y="2.300811523438" />
                  <Point X="27.245765625" Y="2.260900390625" />
                  <Point X="27.261642578125" Y="2.237502929688" />
                  <Point X="27.274943359375" Y="2.224203125" />
                  <Point X="27.314853515625" Y="2.197121582031" />
                  <Point X="27.338251953125" Y="2.181245605469" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.404103515625" Y="2.167702636719" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.448666015625" Y="2.165946289062" />
                  <Point X="27.49928125" Y="2.179481201172" />
                  <Point X="27.528953125" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.092587890625" Y="2.510856689453" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.1165625" Y="2.861441162109" />
                  <Point X="29.20259765625" Y="2.741873291016" />
                  <Point X="29.33340234375" Y="2.525716308594" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="29.003775390625" Y="2.141843261719" />
                  <Point X="28.2886171875" Y="1.593082641602" />
                  <Point X="28.27937109375" Y="1.583833496094" />
                  <Point X="28.2429453125" Y="1.536311645508" />
                  <Point X="28.22158984375" Y="1.508452392578" />
                  <Point X="28.21312109375" Y="1.491500488281" />
                  <Point X="28.19955078125" Y="1.442980224609" />
                  <Point X="28.191595703125" Y="1.414536010742" />
                  <Point X="28.190779296875" Y="1.390966552734" />
                  <Point X="28.201919921875" Y="1.336981811523" />
                  <Point X="28.20844921875" Y="1.305333740234" />
                  <Point X="28.215646484375" Y="1.287956420898" />
                  <Point X="28.245943359375" Y="1.241906860352" />
                  <Point X="28.263703125" Y="1.214911010742" />
                  <Point X="28.280947265625" Y="1.198819824219" />
                  <Point X="28.3248515625" Y="1.174105712891" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.427927734375" Y="1.145774291992" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.9997421875" Y="1.210149658203" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.902240234375" Y="1.103161010742" />
                  <Point X="29.93919140625" Y="0.951367980957" />
                  <Point X="29.980412109375" Y="0.686624450684" />
                  <Point X="29.997859375" Y="0.57455645752" />
                  <Point X="29.560755859375" Y="0.457434539795" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819198608" />
                  <Point X="28.670767578125" Y="0.199109161377" />
                  <Point X="28.636578125" Y="0.179346832275" />
                  <Point X="28.622265625" Y="0.166926544189" />
                  <Point X="28.5872734375" Y="0.122338218689" />
                  <Point X="28.566759765625" Y="0.096198867798" />
                  <Point X="28.556986328125" Y="0.074735107422" />
                  <Point X="28.545322265625" Y="0.013829734802" />
                  <Point X="28.538484375" Y="-0.021875450134" />
                  <Point X="28.538484375" Y="-0.040684627533" />
                  <Point X="28.5501484375" Y="-0.101590003967" />
                  <Point X="28.556986328125" Y="-0.137295181274" />
                  <Point X="28.566759765625" Y="-0.15875894165" />
                  <Point X="28.601751953125" Y="-0.203347106934" />
                  <Point X="28.622265625" Y="-0.229486618042" />
                  <Point X="28.636578125" Y="-0.241906906128" />
                  <Point X="28.6948984375" Y="-0.275617095947" />
                  <Point X="28.729087890625" Y="-0.295379272461" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.22164453125" Y="-0.429129943848" />
                  <Point X="29.998068359375" Y="-0.637172424316" />
                  <Point X="29.9690625" Y="-0.82956048584" />
                  <Point X="29.948431640625" Y="-0.966413391113" />
                  <Point X="29.89562109375" Y="-1.197829345703" />
                  <Point X="29.874546875" Y="-1.290178588867" />
                  <Point X="29.36247265625" Y="-1.222762817383" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.280375" Y="-1.123220214844" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.1854453125" Y="-1.154697631836" />
                  <Point X="28.116259765625" Y="-1.237905639648" />
                  <Point X="28.075701171875" Y="-1.286685546875" />
                  <Point X="28.064359375" Y="-1.314071044922" />
                  <Point X="28.054443359375" Y="-1.421829223633" />
                  <Point X="28.048630859375" Y="-1.485001342773" />
                  <Point X="28.05636328125" Y="-1.516622436523" />
                  <Point X="28.11970703125" Y="-1.615151489258" />
                  <Point X="28.15684375" Y="-1.672913085938" />
                  <Point X="28.168462890625" Y="-1.685540771484" />
                  <Point X="28.614345703125" Y="-2.0276796875" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.2622890625" Y="-2.708036132812" />
                  <Point X="29.2041328125" Y="-2.802140136719" />
                  <Point X="29.0949140625" Y="-2.957325439453" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.59999609375" Y="-2.747966064453" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.60111328125" Y="-2.228710205078" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.37590625" Y="-2.278806396484" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334683349609" />
                  <Point X="27.2290390625" Y="-2.447855224609" />
                  <Point X="27.19412109375" Y="-2.514201416016" />
                  <Point X="27.1891640625" Y="-2.546375" />
                  <Point X="27.213767578125" Y="-2.682603027344" />
                  <Point X="27.22819140625" Y="-2.762465332031" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.520619140625" Y="-3.274854980469" />
                  <Point X="27.98667578125" Y="-4.082088867188" />
                  <Point X="27.90430859375" Y="-4.140921386719" />
                  <Point X="27.835314453125" Y="-4.190202148438" />
                  <Point X="27.713185546875" Y="-4.26925390625" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="27.329166015625" Y="-3.833955322266" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467529297" />
                  <Point X="26.53619140625" Y="-2.894087890625" />
                  <Point X="26.45742578125" Y="-2.843448730469" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.27886328125" Y="-2.849238769531" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868508789062" />
                  <Point X="26.051869140625" Y="-2.9628515625" />
                  <Point X="25.985349609375" Y="-3.018159179688" />
                  <Point X="25.96845703125" Y="-3.045986083984" />
                  <Point X="25.93453125" Y="-3.202070800781" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.99512890625" Y="-3.927488525391" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.0596328125" Y="-4.948936523438" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.8815390625" Y="-4.983740234375" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#180" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.12182548178" Y="4.809783384435" Z="1.6" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.6" />
                  <Point X="-0.48547767958" Y="5.042121414108" Z="1.6" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.6" />
                  <Point X="-1.267267945514" Y="4.904352393689" Z="1.6" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.6" />
                  <Point X="-1.723492349181" Y="4.563546466482" Z="1.6" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.6" />
                  <Point X="-1.719575618451" Y="4.405344398545" Z="1.6" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.6" />
                  <Point X="-1.776577711464" Y="4.325621982742" Z="1.6" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.6" />
                  <Point X="-1.874288915294" Y="4.318043393839" Z="1.6" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.6" />
                  <Point X="-2.060383405998" Y="4.513586679358" Z="1.6" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.6" />
                  <Point X="-2.375344227412" Y="4.475978733226" Z="1.6" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.6" />
                  <Point X="-3.005372689292" Y="4.079748824131" Z="1.6" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.6" />
                  <Point X="-3.140909300175" Y="3.381734128258" Z="1.6" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.6" />
                  <Point X="-2.998758510329" Y="3.108695688212" Z="1.6" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.6" />
                  <Point X="-3.016482142367" Y="3.03232142552" Z="1.6" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.6" />
                  <Point X="-3.086380777394" Y="2.996806188844" Z="1.6" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.6" />
                  <Point X="-3.552125220039" Y="3.239284648571" Z="1.6" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.6" />
                  <Point X="-3.946599715324" Y="3.1819408311" Z="1.6" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.6" />
                  <Point X="-4.333324351532" Y="2.631098085797" Z="1.6" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.6" />
                  <Point X="-4.011107740154" Y="1.852193307319" Z="1.6" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.6" />
                  <Point X="-3.685571071694" Y="1.589720184646" Z="1.6" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.6" />
                  <Point X="-3.675931615734" Y="1.531712868207" Z="1.6" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.6" />
                  <Point X="-3.71417171101" Y="1.487042266412" Z="1.6" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.6" />
                  <Point X="-4.423412022128" Y="1.56310766117" Z="1.6" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.6" />
                  <Point X="-4.874273940796" Y="1.40163945544" Z="1.6" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.6" />
                  <Point X="-5.005167536751" Y="0.81940577337" Z="1.6" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.6" />
                  <Point X="-4.124929207926" Y="0.19600392362" Z="1.6" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.6" />
                  <Point X="-3.566303862913" Y="0.041950176622" Z="1.6" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.6" />
                  <Point X="-3.545388843081" Y="0.018790935922" Z="1.6" />
                  <Point X="-3.539556741714" Y="0" Z="1.6" />
                  <Point X="-3.542975758054" Y="-0.011016015139" Z="1.6" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.6" />
                  <Point X="-3.55906473221" Y="-0.036925806512" Z="1.6" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.6" />
                  <Point X="-4.511958580846" Y="-0.299708133136" Z="1.6" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.6" />
                  <Point X="-5.031623599367" Y="-0.647334545453" Z="1.6" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.6" />
                  <Point X="-4.932488306951" Y="-1.186097870517" Z="1.6" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.6" />
                  <Point X="-3.820738610517" Y="-1.386062765746" Z="1.6" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.6" />
                  <Point X="-3.209371488788" Y="-1.312623745139" Z="1.6" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.6" />
                  <Point X="-3.195523723765" Y="-1.333443797455" Z="1.6" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.6" />
                  <Point X="-4.021516986678" Y="-1.982277098851" Z="1.6" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.6" />
                  <Point X="-4.39441234474" Y="-2.533573966236" Z="1.6" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.6" />
                  <Point X="-4.08106818919" Y="-3.012429387665" Z="1.6" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.6" />
                  <Point X="-3.049374086757" Y="-2.830618339587" Z="1.6" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.6" />
                  <Point X="-2.566427851596" Y="-2.561902617535" Z="1.6" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.6" />
                  <Point X="-3.024798673581" Y="-3.385704295735" Z="1.6" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.6" />
                  <Point X="-3.148601835024" Y="-3.978753483626" Z="1.6" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.6" />
                  <Point X="-2.728098229187" Y="-4.278042136427" Z="1.6" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.6" />
                  <Point X="-2.309338861726" Y="-4.264771783619" Z="1.6" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.6" />
                  <Point X="-2.130883426369" Y="-4.092748702078" Z="1.6" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.6" />
                  <Point X="-1.853838432508" Y="-3.991583699272" Z="1.6" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.6" />
                  <Point X="-1.572458592888" Y="-4.079978649237" Z="1.6" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.6" />
                  <Point X="-1.403036323273" Y="-4.321400255527" Z="1.6" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.6" />
                  <Point X="-1.395277773702" Y="-4.744137271836" Z="1.6" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.6" />
                  <Point X="-1.303815650159" Y="-4.907620919056" Z="1.6" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.6" />
                  <Point X="-1.006626616484" Y="-4.97708543325" Z="1.6" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.6" />
                  <Point X="-0.565133108562" Y="-4.071289538209" Z="1.6" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.6" />
                  <Point X="-0.356576504263" Y="-3.431589307751" Z="1.6" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.6" />
                  <Point X="-0.1597229524" Y="-3.253811478536" Z="1.6" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.6" />
                  <Point X="0.09363612696" Y="-3.233300566752" Z="1.6" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.6" />
                  <Point X="0.313869355169" Y="-3.370056448017" Z="1.6" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.6" />
                  <Point X="0.669621751839" Y="-4.461246491606" Z="1.6" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.6" />
                  <Point X="0.884318676686" Y="-5.00165497004" Z="1.6" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.6" />
                  <Point X="1.064180575276" Y="-4.966496859822" Z="1.6" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.6" />
                  <Point X="1.038544888482" Y="-3.889682054799" Z="1.6" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.6" />
                  <Point X="0.977234379396" Y="-3.181410553569" Z="1.6" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.6" />
                  <Point X="1.077678176419" Y="-2.970018344478" Z="1.6" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.6" />
                  <Point X="1.277287639797" Y="-2.867748400843" Z="1.6" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.6" />
                  <Point X="1.502996347064" Y="-2.904865779251" Z="1.6" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.6" />
                  <Point X="2.283342016211" Y="-3.833113205387" Z="1.6" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.6" />
                  <Point X="2.73419838367" Y="-4.279948206223" Z="1.6" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.6" />
                  <Point X="2.927212297344" Y="-4.150328405957" Z="1.6" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.6" />
                  <Point X="2.557762308339" Y="-3.218574985375" Z="1.6" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.6" />
                  <Point X="2.25681379697" Y="-2.642436191323" Z="1.6" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.6" />
                  <Point X="2.267128572924" Y="-2.439862152573" Z="1.6" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.6" />
                  <Point X="2.393036225799" Y="-2.291772737043" Z="1.6" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.6" />
                  <Point X="2.586070612189" Y="-2.246634212521" Z="1.6" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.6" />
                  <Point X="3.568838611732" Y="-2.759987191936" Z="1.6" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.6" />
                  <Point X="4.129646110749" Y="-2.954822748458" Z="1.6" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.6" />
                  <Point X="4.298664993916" Y="-2.703041476502" Z="1.6" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.6" />
                  <Point X="3.638626894399" Y="-1.95673195514" Z="1.6" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.6" />
                  <Point X="3.155607436142" Y="-1.556831372897" Z="1.6" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.6" />
                  <Point X="3.098075592046" Y="-1.395130306977" Z="1.6" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.6" />
                  <Point X="3.148550531802" Y="-1.238592256032" Z="1.6" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.6" />
                  <Point X="3.284837843957" Y="-1.140799217167" Z="1.6" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.6" />
                  <Point X="4.349790345386" Y="-1.241054861349" Z="1.6" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.6" />
                  <Point X="4.938210881963" Y="-1.177672980134" Z="1.6" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.6" />
                  <Point X="5.012347944449" Y="-0.805734117949" Z="1.6" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.6" />
                  <Point X="4.228428117927" Y="-0.349553631273" Z="1.6" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.6" />
                  <Point X="3.713762976839" Y="-0.201048364251" Z="1.6" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.6" />
                  <Point X="3.634928880998" Y="-0.141198762586" Z="1.6" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.6" />
                  <Point X="3.593098779145" Y="-0.060905333728" Z="1.6" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.6" />
                  <Point X="3.588272671279" Y="0.035705197472" Z="1.6" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.6" />
                  <Point X="3.620450557402" Y="0.122749975989" Z="1.6" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.6" />
                  <Point X="3.689632437513" Y="0.187100479991" Z="1.6" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.6" />
                  <Point X="4.56753969938" Y="0.440418301495" Z="1.6" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.6" />
                  <Point X="5.023659053977" Y="0.725596316931" Z="1.6" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.6" />
                  <Point X="4.944664425519" Y="1.14626759129" Z="1.6" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.6" />
                  <Point X="3.987059894729" Y="1.291001913915" Z="1.6" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.6" />
                  <Point X="3.428321587009" Y="1.226623304231" Z="1.6" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.6" />
                  <Point X="3.343222392264" Y="1.248956971634" Z="1.6" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.6" />
                  <Point X="3.281557501414" Y="1.300667047221" Z="1.6" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.6" />
                  <Point X="3.244730923139" Y="1.378364465881" Z="1.6" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.6" />
                  <Point X="3.241546828425" Y="1.460793646336" Z="1.6" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.6" />
                  <Point X="3.276471048083" Y="1.537173064049" Z="1.6" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.6" />
                  <Point X="4.028056695786" Y="2.133455527836" Z="1.6" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.6" />
                  <Point X="4.370022653266" Y="2.582882591032" Z="1.6" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.6" />
                  <Point X="4.150991921295" Y="2.921924569588" Z="1.6" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.6" />
                  <Point X="3.061431024315" Y="2.58543808285" Z="1.6" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.6" />
                  <Point X="2.480205824454" Y="2.259064062905" Z="1.6" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.6" />
                  <Point X="2.403933701274" Y="2.248623048876" Z="1.6" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.6" />
                  <Point X="2.336769233266" Y="2.26977672266" Z="1.6" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.6" />
                  <Point X="2.280981877575" Y="2.320255627116" Z="1.6" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.6" />
                  <Point X="2.250806653883" Y="2.385824742559" Z="1.6" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.6" />
                  <Point X="2.253463879752" Y="2.459263751428" Z="1.6" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.6" />
                  <Point X="2.81018727001" Y="3.45070805764" Z="1.6" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.6" />
                  <Point X="2.9899869792" Y="4.100853990124" Z="1.6" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.6" />
                  <Point X="2.60650339859" Y="4.354671302832" Z="1.6" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.6" />
                  <Point X="2.203595521768" Y="4.571916912069" Z="1.6" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.6" />
                  <Point X="1.786112590703" Y="4.750584706406" Z="1.6" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.6" />
                  <Point X="1.274966966355" Y="4.906659892244" Z="1.6" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.6" />
                  <Point X="0.615194406881" Y="5.03213361426" Z="1.6" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.6" />
                  <Point X="0.071419079773" Y="4.621664121516" Z="1.6" />
                  <Point X="0" Y="4.355124473572" Z="1.6" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>