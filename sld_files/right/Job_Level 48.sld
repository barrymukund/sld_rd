<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#211" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3419" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.5633046875" Y="-3.512525634766" />
                  <Point X="25.55772265625" Y="-3.497142333984" />
                  <Point X="25.542365234375" Y="-3.467375976562" />
                  <Point X="25.390802734375" Y="-3.249003662109" />
                  <Point X="25.37863671875" Y="-3.231475585938" />
                  <Point X="25.35675" Y="-3.209018798828" />
                  <Point X="25.330494140625" Y="-3.189775634766" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.067962890625" Y="-3.102878417969" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766357422" />
                  <Point X="24.9913359375" Y="-3.092766357422" />
                  <Point X="24.96317578125" Y="-3.097035644531" />
                  <Point X="24.728642578125" Y="-3.169826171875" />
                  <Point X="24.70981640625" Y="-3.175668945312" />
                  <Point X="24.68181640625" Y="-3.189776367188" />
                  <Point X="24.655560546875" Y="-3.209020019531" />
                  <Point X="24.63367578125" Y="-3.2314765625" />
                  <Point X="24.482115234375" Y="-3.449848632812" />
                  <Point X="24.46994921875" Y="-3.467376953125" />
                  <Point X="24.4618125" Y="-3.481572753906" />
                  <Point X="24.449009765625" Y="-3.512525146484" />
                  <Point X="24.41924609375" Y="-3.623607421875" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.938654296875" Y="-4.848843261719" />
                  <Point X="23.92066796875" Y="-4.845352050781" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516222167969" />
                  <Point X="23.7274609375" Y="-4.234540527344" />
                  <Point X="23.722962890625" Y="-4.211930664062" />
                  <Point X="23.712060546875" Y="-4.182962890625" />
                  <Point X="23.69598828125" Y="-4.155126464844" />
                  <Point X="23.67635546875" Y="-4.131203613281" />
                  <Point X="23.460427734375" Y="-3.941839111328" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.07038671875" Y="-3.872182373047" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.016580078125" Y="-3.873708984375" />
                  <Point X="22.985529296875" Y="-3.882029785156" />
                  <Point X="22.95733984375" Y="-3.894802734375" />
                  <Point X="22.718541015625" Y="-4.054363037109" />
                  <Point X="22.699375" Y="-4.067170654297" />
                  <Point X="22.687208984375" Y="-4.076826416016" />
                  <Point X="22.664900390625" Y="-4.0994609375" />
                  <Point X="22.648189453125" Y="-4.12123828125" />
                  <Point X="22.519853515625" Y="-4.288490234375" />
                  <Point X="22.22466015625" Y="-4.10571484375" />
                  <Point X="22.1982890625" Y="-4.089386230469" />
                  <Point X="21.895279296875" Y="-3.856078125" />
                  <Point X="22.576240234375" Y="-2.676619384766" />
                  <Point X="22.587140625" Y="-2.647654541016" />
                  <Point X="22.593412109375" Y="-2.616128173828" />
                  <Point X="22.59442578125" Y="-2.585194091797" />
                  <Point X="22.58544140625" Y="-2.555575927734" />
                  <Point X="22.571224609375" Y="-2.526747070312" />
                  <Point X="22.55319921875" Y="-2.501591308594" />
                  <Point X="22.537140625" Y="-2.485531738281" />
                  <Point X="22.5358515625" Y="-2.484242675781" />
                  <Point X="22.510689453125" Y="-2.466212402344" />
                  <Point X="22.481859375" Y="-2.451995361328" />
                  <Point X="22.4522421875" Y="-2.443011230469" />
                  <Point X="22.421310546875" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450295166016" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="22.26501171875" Y="-2.516511230469" />
                  <Point X="21.181978515625" Y="-3.14180078125" />
                  <Point X="20.937974609375" Y="-2.821231201172" />
                  <Point X="20.91713671875" Y="-2.793853515625" />
                  <Point X="20.693857421875" Y="-2.419448730469" />
                  <Point X="21.894044921875" Y="-1.498513061523" />
                  <Point X="21.915421875" Y="-1.475594726562" />
                  <Point X="21.93338671875" Y="-1.448464599609" />
                  <Point X="21.946142578125" Y="-1.419836181641" />
                  <Point X="21.9532734375" Y="-1.392310302734" />
                  <Point X="21.95384765625" Y="-1.390094848633" />
                  <Point X="21.956654296875" Y="-1.359645874023" />
                  <Point X="21.954443359375" Y="-1.327977050781" />
                  <Point X="21.94744140625" Y="-1.298234130859" />
                  <Point X="21.931357421875" Y="-1.272252807617" />
                  <Point X="21.910525390625" Y="-1.248298339844" />
                  <Point X="21.88702734375" Y="-1.228765625" />
                  <Point X="21.86251171875" Y="-1.214337402344" />
                  <Point X="21.8605546875" Y="-1.213185668945" />
                  <Point X="21.8313046875" Y="-1.20196484375" />
                  <Point X="21.799408203125" Y="-1.195477172852" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="21.647125" Y="-1.210306762695" />
                  <Point X="20.267900390625" Y="-1.391885375977" />
                  <Point X="20.174072265625" Y="-1.024558105469" />
                  <Point X="20.16592578125" Y="-0.992656982422" />
                  <Point X="20.107578125" Y="-0.584697998047" />
                  <Point X="21.467125" Y="-0.220408218384" />
                  <Point X="21.48250390625" Y="-0.21482901001" />
                  <Point X="21.512267578125" Y="-0.199472229004" />
                  <Point X="21.537951171875" Y="-0.181647064209" />
                  <Point X="21.540013671875" Y="-0.180216552734" />
                  <Point X="21.562474609375" Y="-0.158330200195" />
                  <Point X="21.581720703125" Y="-0.132074874878" />
                  <Point X="21.595833984375" Y="-0.104066215515" />
                  <Point X="21.604396484375" Y="-0.076474067688" />
                  <Point X="21.605083984375" Y="-0.074259292603" />
                  <Point X="21.6093515625" Y="-0.046103248596" />
                  <Point X="21.6093515625" Y="-0.016456920624" />
                  <Point X="21.605083984375" Y="0.011699280739" />
                  <Point X="21.596521484375" Y="0.039291427612" />
                  <Point X="21.595833984375" Y="0.041506202698" />
                  <Point X="21.581720703125" Y="0.069514862061" />
                  <Point X="21.562474609375" Y="0.095770042419" />
                  <Point X="21.540021484375" Y="0.117650314331" />
                  <Point X="21.514330078125" Y="0.135481124878" />
                  <Point X="21.50201171875" Y="0.142722290039" />
                  <Point X="21.467125" Y="0.157848205566" />
                  <Point X="21.356876953125" Y="0.187389175415" />
                  <Point X="20.10818359375" Y="0.521975524902" />
                  <Point X="20.17026171875" Y="0.941493713379" />
                  <Point X="20.17551171875" Y="0.976969665527" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301227905273" />
                  <Point X="21.29686328125" Y="1.305263183594" />
                  <Point X="21.353724609375" Y="1.323191650391" />
                  <Point X="21.3582890625" Y="1.324630981445" />
                  <Point X="21.37722265625" Y="1.332961303711" />
                  <Point X="21.395966796875" Y="1.343783081055" />
                  <Point X="21.4126484375" Y="1.356014892578" />
                  <Point X="21.426287109375" Y="1.371567504883" />
                  <Point X="21.438701171875" Y="1.389297363281" />
                  <Point X="21.4486484375" Y="1.407430175781" />
                  <Point X="21.47146484375" Y="1.462512939453" />
                  <Point X="21.47330078125" Y="1.466946411133" />
                  <Point X="21.479091796875" Y="1.4868203125" />
                  <Point X="21.482845703125" Y="1.508129638672" />
                  <Point X="21.4841953125" Y="1.528765869141" />
                  <Point X="21.481048828125" Y="1.549205078125" />
                  <Point X="21.475447265625" Y="1.570104980469" />
                  <Point X="21.467951171875" Y="1.589376342773" />
                  <Point X="21.440421875" Y="1.642261230469" />
                  <Point X="21.43819140625" Y="1.646544189453" />
                  <Point X="21.426705078125" Y="1.663725830078" />
                  <Point X="21.412796875" Y="1.680296508789" />
                  <Point X="21.39786328125" Y="1.694590209961" />
                  <Point X="21.334625" Y="1.743114990234" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.89844921875" Y="2.6987109375" />
                  <Point X="20.918849609375" Y="2.733663574219" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.812275390625" Y="2.836340576172" />
                  <Point X="21.832916015625" Y="2.829832275391" />
                  <Point X="21.853205078125" Y="2.825796630859" />
                  <Point X="21.9323984375" Y="2.818868164062" />
                  <Point X="21.93875390625" Y="2.818312011719" />
                  <Point X="21.959431640625" Y="2.818762939453" />
                  <Point X="21.980890625" Y="2.821587646484" />
                  <Point X="22.000982421875" Y="2.826503662109" />
                  <Point X="22.019533203125" Y="2.835651611328" />
                  <Point X="22.0377890625" Y="2.84728125" />
                  <Point X="22.053921875" Y="2.860229003906" />
                  <Point X="22.110134765625" Y="2.916440673828" />
                  <Point X="22.114646484375" Y="2.920952392578" />
                  <Point X="22.127595703125" Y="2.937086425781" />
                  <Point X="22.139224609375" Y="2.955341064453" />
                  <Point X="22.14837109375" Y="2.973889892578" />
                  <Point X="22.1532890625" Y="2.993978515625" />
                  <Point X="22.156115234375" Y="3.015437744141" />
                  <Point X="22.15656640625" Y="3.036123291016" />
                  <Point X="22.14963671875" Y="3.115316162109" />
                  <Point X="22.14908203125" Y="3.121660644531" />
                  <Point X="22.145048828125" Y="3.141937255859" />
                  <Point X="22.1385390625" Y="3.162590820312" />
                  <Point X="22.130205078125" Y="3.181533691406" />
                  <Point X="22.102181640625" Y="3.230070800781" />
                  <Point X="21.81666796875" Y="3.724595214844" />
                  <Point X="22.26385546875" Y="4.067448486328" />
                  <Point X="22.29938671875" Y="4.094691650391" />
                  <Point X="22.83296484375" Y="4.391133789062" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.97111328125" Y="4.214795898438" />
                  <Point X="22.987697265625" Y="4.200881835938" />
                  <Point X="23.004890625" Y="4.189393554688" />
                  <Point X="23.09303125" Y="4.143509765625" />
                  <Point X="23.100107421875" Y="4.139826660156" />
                  <Point X="23.119390625" Y="4.132328613281" />
                  <Point X="23.140294921875" Y="4.126728027344" />
                  <Point X="23.16073828125" Y="4.12358203125" />
                  <Point X="23.181376953125" Y="4.124935058594" />
                  <Point X="23.202689453125" Y="4.128693359375" />
                  <Point X="23.222546875" Y="4.134481445312" />
                  <Point X="23.3143515625" Y="4.172508789063" />
                  <Point X="23.321720703125" Y="4.175561035156" />
                  <Point X="23.339853515625" Y="4.185509277344" />
                  <Point X="23.357583984375" Y="4.197923828125" />
                  <Point X="23.37313671875" Y="4.2115625" />
                  <Point X="23.3853671875" Y="4.228244140625" />
                  <Point X="23.396189453125" Y="4.246988769531" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.43440234375" Y="4.360690917969" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583984375" />
                  <Point X="23.44272265625" Y="4.410145019531" />
                  <Point X="23.44226953125" Y="4.43083203125" />
                  <Point X="23.43908203125" Y="4.455031738281" />
                  <Point X="23.415796875" Y="4.631897460938" />
                  <Point X="24.00437890625" Y="4.796915527344" />
                  <Point X="24.05036328125" Y="4.809808105469" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.14621484375" Y="4.286314453125" />
                  <Point X="25.16057421875" Y="4.339901855469" />
                  <Point X="25.307419921875" Y="4.8879375" />
                  <Point X="25.803880859375" Y="4.835944824219" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.4402578125" Y="4.687793945312" />
                  <Point X="26.48102734375" Y="4.677950683594" />
                  <Point X="26.868998046875" Y="4.537231445312" />
                  <Point X="26.89464453125" Y="4.527929199219" />
                  <Point X="27.269900390625" Y="4.352434570312" />
                  <Point X="27.294576171875" Y="4.34089453125" />
                  <Point X="27.657109375" Y="4.129682128906" />
                  <Point X="27.680978515625" Y="4.115776367188" />
                  <Point X="27.94326171875" Y="3.929254394531" />
                  <Point X="27.147583984375" Y="2.551098144531" />
                  <Point X="27.14208203125" Y="2.539941894531" />
                  <Point X="27.133078125" Y="2.516060546875" />
                  <Point X="27.113203125" Y="2.441739746094" />
                  <Point X="27.1106328125" Y="2.428324951172" />
                  <Point X="27.10769921875" Y="2.403453125" />
                  <Point X="27.107728515625" Y="2.380951416016" />
                  <Point X="27.115478515625" Y="2.316685058594" />
                  <Point X="27.116099609375" Y="2.311535888672" />
                  <Point X="27.1214375" Y="2.289626464844" />
                  <Point X="27.129703125" Y="2.267529296875" />
                  <Point X="27.1400703125" Y="2.247471679688" />
                  <Point X="27.1798359375" Y="2.188866943359" />
                  <Point X="27.188400390625" Y="2.178033447266" />
                  <Point X="27.20489453125" Y="2.160029785156" />
                  <Point X="27.221599609375" Y="2.145593505859" />
                  <Point X="27.280203125" Y="2.105827636719" />
                  <Point X="27.284923828125" Y="2.102625244141" />
                  <Point X="27.30496484375" Y="2.092266601563" />
                  <Point X="27.327046875" Y="2.084004394531" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.41323046875" Y="2.0709140625" />
                  <Point X="27.42742578125" Y="2.070272705078" />
                  <Point X="27.451484375" Y="2.070987548828" />
                  <Point X="27.473205078125" Y="2.074170410156" />
                  <Point X="27.547525390625" Y="2.094044921875" />
                  <Point X="27.555591796875" Y="2.096591796875" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="27.699423828125" Y="2.174166748047" />
                  <Point X="28.967326171875" Y="2.906190917969" />
                  <Point X="29.10911328125" Y="2.709139404297" />
                  <Point X="29.123275390625" Y="2.689458496094" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.221421875" Y="1.66023840332" />
                  <Point X="28.20397265625" Y="1.641625488281" />
                  <Point X="28.150484375" Y="1.571845214844" />
                  <Point X="28.14320703125" Y="1.56084375" />
                  <Point X="28.1304453125" Y="1.538295410156" />
                  <Point X="28.1216328125" Y="1.51708996582" />
                  <Point X="28.10170703125" Y="1.445844360352" />
                  <Point X="28.100107421875" Y="1.440125610352" />
                  <Point X="28.09665234375" Y="1.417826660156" />
                  <Point X="28.0958359375" Y="1.394256591797" />
                  <Point X="28.097740234375" Y="1.371769165039" />
                  <Point X="28.11409765625" Y="1.292499389648" />
                  <Point X="28.117708984375" Y="1.279642333984" />
                  <Point X="28.12621875" Y="1.255902099609" />
                  <Point X="28.136283203125" Y="1.235743530273" />
                  <Point X="28.18076953125" Y="1.168125610352" />
                  <Point X="28.18433984375" Y="1.162698242188" />
                  <Point X="28.19888671875" Y="1.145458862305" />
                  <Point X="28.21612890625" Y="1.129367797852" />
                  <Point X="28.234345703125" Y="1.116035888672" />
                  <Point X="28.298814453125" Y="1.079746337891" />
                  <Point X="28.3112890625" Y="1.073872802734" />
                  <Point X="28.334443359375" Y="1.064960449219" />
                  <Point X="28.35612109375" Y="1.059438476562" />
                  <Point X="28.44328515625" Y="1.047918457031" />
                  <Point X="28.451345703125" Y="1.047200805664" />
                  <Point X="28.47141796875" Y="1.046273071289" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.593541015625" Y="1.060852416992" />
                  <Point X="29.776841796875" Y="1.21663671875" />
                  <Point X="29.83985546875" Y="0.957789306641" />
                  <Point X="29.8459375" Y="0.93280645752" />
                  <Point X="29.890865234375" Y="0.64423828125" />
                  <Point X="28.716580078125" Y="0.32958972168" />
                  <Point X="28.704787109375" Y="0.325584747314" />
                  <Point X="28.681544921875" Y="0.315066894531" />
                  <Point X="28.595908203125" Y="0.265567749023" />
                  <Point X="28.585310546875" Y="0.258451965332" />
                  <Point X="28.564130859375" Y="0.242063156128" />
                  <Point X="28.54753515625" Y="0.225579483032" />
                  <Point X="28.496154296875" Y="0.160106994629" />
                  <Point X="28.492029296875" Y="0.154851699829" />
                  <Point X="28.480310546875" Y="0.135581161499" />
                  <Point X="28.470533203125" Y="0.114115577698" />
                  <Point X="28.463681640625" Y="0.092605209351" />
                  <Point X="28.4465546875" Y="0.003173215866" />
                  <Point X="28.4449921875" Y="-0.009676596642" />
                  <Point X="28.4436171875" Y="-0.035667507172" />
                  <Point X="28.4451796875" Y="-0.058554763794" />
                  <Point X="28.462306640625" Y="-0.147986907959" />
                  <Point X="28.463681640625" Y="-0.155165374756" />
                  <Point X="28.470533203125" Y="-0.176675735474" />
                  <Point X="28.480310546875" Y="-0.198141326904" />
                  <Point X="28.492029296875" Y="-0.217411865234" />
                  <Point X="28.54341015625" Y="-0.282884216309" />
                  <Point X="28.5523984375" Y="-0.292809936523" />
                  <Point X="28.570830078125" Y="-0.310480926514" />
                  <Point X="28.58903515625" Y="-0.32415447998" />
                  <Point X="28.674671875" Y="-0.373653778076" />
                  <Point X="28.681533203125" Y="-0.377254394531" />
                  <Point X="28.70048828125" Y="-0.386236450195" />
                  <Point X="28.716580078125" Y="-0.392150054932" />
                  <Point X="28.8131796875" Y="-0.418033630371" />
                  <Point X="29.891474609375" Y="-0.706961914062" />
                  <Point X="29.858419921875" Y="-0.926204406738" />
                  <Point X="29.855025390625" Y="-0.948726074219" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="28.4243828125" Y="-1.003440795898" />
                  <Point X="28.40803515625" Y="-1.002710205078" />
                  <Point X="28.37466015625" Y="-1.005508789063" />
                  <Point X="28.2065859375" Y="-1.042040283203" />
                  <Point X="28.193095703125" Y="-1.04497265625" />
                  <Point X="28.163978515625" Y="-1.056595947266" />
                  <Point X="28.136150390625" Y="-1.07348828125" />
                  <Point X="28.112396484375" Y="-1.093960205078" />
                  <Point X="28.010806640625" Y="-1.216141235352" />
                  <Point X="28.00265234375" Y="-1.225948242188" />
                  <Point X="27.9879296875" Y="-1.250337768555" />
                  <Point X="27.976587890625" Y="-1.277725952148" />
                  <Point X="27.969759765625" Y="-1.305368164062" />
                  <Point X="27.95519921875" Y="-1.46359777832" />
                  <Point X="27.95403125" Y="-1.476298461914" />
                  <Point X="27.956349609375" Y="-1.507570068359" />
                  <Point X="27.96408203125" Y="-1.539190429688" />
                  <Point X="27.976453125" Y="-1.567998657227" />
                  <Point X="28.069466796875" Y="-1.712676269531" />
                  <Point X="28.07473828125" Y="-1.720072631836" />
                  <Point X="28.09382421875" Y="-1.744311767578" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.2002734375" Y="-1.8296953125" />
                  <Point X="29.213125" Y="-2.606882324219" />
                  <Point X="29.134384765625" Y="-2.734295166016" />
                  <Point X="29.12482421875" Y="-2.749763427734" />
                  <Point X="29.02898046875" Y="-2.885944580078" />
                  <Point X="27.800955078125" Y="-2.176942871094" />
                  <Point X="27.78612890625" Y="-2.170011474609" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.55419140625" Y="-2.12369921875" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506779296875" Y="-2.120395751953" />
                  <Point X="27.474607421875" Y="-2.125354003906" />
                  <Point X="27.444833984375" Y="-2.135177246094" />
                  <Point X="27.278654296875" Y="-2.222636230469" />
                  <Point X="27.26531640625" Y="-2.22965625" />
                  <Point X="27.24238671875" Y="-2.246547363281" />
                  <Point X="27.2214296875" Y="-2.267503173828" />
                  <Point X="27.20453515625" Y="-2.290435791016" />
                  <Point X="27.117076171875" Y="-2.456614990234" />
                  <Point X="27.1100546875" Y="-2.469953857422" />
                  <Point X="27.100232421875" Y="-2.499728515625" />
                  <Point X="27.0952734375" Y="-2.531906738281" />
                  <Point X="27.09567578125" Y="-2.563260742188" />
                  <Point X="27.131802734375" Y="-2.763294921875" />
                  <Point X="27.13366015625" Y="-2.771485351562" />
                  <Point X="27.142462890625" Y="-2.803653320313" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.209427734375" Y="-2.925854248047" />
                  <Point X="27.861287109375" Y="-4.054905273438" />
                  <Point X="27.79321484375" Y="-4.103526855469" />
                  <Point X="27.781853515625" Y="-4.111641601562" />
                  <Point X="27.701765625" Y="-4.16348046875" />
                  <Point X="26.758548828125" Y="-2.934256347656" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.52463671875" Y="-2.773719238281" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.20133203125" Y="-2.760971923828" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397949219" />
                  <Point X="26.128978515625" Y="-2.780741455078" />
                  <Point X="26.10459765625" Y="-2.795461425781" />
                  <Point X="25.937986328125" Y="-2.933992431641" />
                  <Point X="25.92461328125" Y="-2.945111816406" />
                  <Point X="25.90414453125" Y="-2.968856689453" />
                  <Point X="25.887251953125" Y="-2.996679443359" />
                  <Point X="25.875625" Y="-3.025805664062" />
                  <Point X="25.825810546875" Y="-3.254996826172" />
                  <Point X="25.824466796875" Y="-3.2626875" />
                  <Point X="25.81975390625" Y="-3.298233886719" />
                  <Point X="25.8197421875" Y="-3.32312109375" />
                  <Point X="25.836068359375" Y="-3.44712109375" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="25.986423828125" Y="-4.867728027344" />
                  <Point X="25.975673828125" Y="-4.870084960938" />
                  <Point X="25.929318359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.956755859375" Y="-4.755583984375" />
                  <Point X="23.941576171875" Y="-4.752637207031" />
                  <Point X="23.858755859375" Y="-4.731328613281" />
                  <Point X="23.8792265625" Y="-4.575838378906" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.509323242188" />
                  <Point X="23.876666015625" Y="-4.497687988281" />
                  <Point X="23.820634765625" Y="-4.216006347656" />
                  <Point X="23.81613671875" Y="-4.193396484375" />
                  <Point X="23.811875" Y="-4.178467773438" />
                  <Point X="23.80097265625" Y="-4.1495" />
                  <Point X="23.79433203125" Y="-4.1354609375" />
                  <Point X="23.778259765625" Y="-4.107624511719" />
                  <Point X="23.769423828125" Y="-4.094859375" />
                  <Point X="23.749791015625" Y="-4.070936523438" />
                  <Point X="23.738994140625" Y="-4.059779052734" />
                  <Point X="23.52306640625" Y="-3.870414550781" />
                  <Point X="23.505734375" Y="-3.85521484375" />
                  <Point X="23.49326171875" Y="-3.845965332031" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.076599609375" Y="-3.777385742188" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.007267578125" Y="-3.779166503906" />
                  <Point X="22.991990234375" Y="-3.781946533203" />
                  <Point X="22.960939453125" Y="-3.790267333984" />
                  <Point X="22.9463203125" Y="-3.795498291016" />
                  <Point X="22.918130859375" Y="-3.808271240234" />
                  <Point X="22.904560546875" Y="-3.815813232422" />
                  <Point X="22.66576171875" Y="-3.975373535156" />
                  <Point X="22.646595703125" Y="-3.988181152344" />
                  <Point X="22.64031640625" Y="-3.992758789062" />
                  <Point X="22.619548828125" Y="-4.010140136719" />
                  <Point X="22.597240234375" Y="-4.032774658203" />
                  <Point X="22.589533203125" Y="-4.041627197266" />
                  <Point X="22.572822265625" Y="-4.063404541016" />
                  <Point X="22.49680078125" Y="-4.162479492188" />
                  <Point X="22.274671875" Y="-4.024944091797" />
                  <Point X="22.252408203125" Y="-4.011158935547" />
                  <Point X="22.01913671875" Y="-3.831547607422" />
                  <Point X="22.65851171875" Y="-2.724119384766" />
                  <Point X="22.66515234375" Y="-2.710079833984" />
                  <Point X="22.676052734375" Y="-2.681114990234" />
                  <Point X="22.680314453125" Y="-2.666189453125" />
                  <Point X="22.6865859375" Y="-2.634663085938" />
                  <Point X="22.688361328125" Y="-2.619239501953" />
                  <Point X="22.689375" Y="-2.588305419922" />
                  <Point X="22.6853359375" Y="-2.557617675781" />
                  <Point X="22.6763515625" Y="-2.527999511719" />
                  <Point X="22.67064453125" Y="-2.51355859375" />
                  <Point X="22.656427734375" Y="-2.484729736328" />
                  <Point X="22.648447265625" Y="-2.471413574219" />
                  <Point X="22.630421875" Y="-2.4462578125" />
                  <Point X="22.620376953125" Y="-2.434418212891" />
                  <Point X="22.604318359375" Y="-2.418358642578" />
                  <Point X="22.591185546875" Y="-2.407021240234" />
                  <Point X="22.5660234375" Y="-2.388990966797" />
                  <Point X="22.552705078125" Y="-2.381009033203" />
                  <Point X="22.523875" Y="-2.366791992188" />
                  <Point X="22.509435546875" Y="-2.3610859375" />
                  <Point X="22.479818359375" Y="-2.352101806641" />
                  <Point X="22.4491328125" Y="-2.348062011719" />
                  <Point X="22.418201171875" Y="-2.349074951172" />
                  <Point X="22.40277734375" Y="-2.350849609375" />
                  <Point X="22.371251953125" Y="-2.357120605469" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372285888672" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="22.21751171875" Y="-2.434238769531" />
                  <Point X="21.2069140625" Y="-3.017707763672" />
                  <Point X="21.013568359375" Y="-2.763692871094" />
                  <Point X="20.995974609375" Y="-2.740578613281" />
                  <Point X="20.818734375" Y="-2.443372314453" />
                  <Point X="21.951876953125" Y="-1.573881591797" />
                  <Point X="21.963515625" Y="-1.563311401367" />
                  <Point X="21.984892578125" Y="-1.540393066406" />
                  <Point X="21.994630859375" Y="-1.528044555664" />
                  <Point X="22.012595703125" Y="-1.500914428711" />
                  <Point X="22.020162109375" Y="-1.48712890625" />
                  <Point X="22.03291796875" Y="-1.458500610352" />
                  <Point X="22.038107421875" Y="-1.44366027832" />
                  <Point X="22.04523828125" Y="-1.416134521484" />
                  <Point X="22.048447265625" Y="-1.398814575195" />
                  <Point X="22.05125390625" Y="-1.368365600586" />
                  <Point X="22.051423828125" Y="-1.353029663086" />
                  <Point X="22.049212890625" Y="-1.321360839844" />
                  <Point X="22.046916015625" Y="-1.306207641602" />
                  <Point X="22.0399140625" Y="-1.276464599609" />
                  <Point X="22.028216796875" Y="-1.248229736328" />
                  <Point X="22.0121328125" Y="-1.222248413086" />
                  <Point X="22.003041015625" Y="-1.209912353516" />
                  <Point X="21.982208984375" Y="-1.185957885742" />
                  <Point X="21.97125390625" Y="-1.175242431641" />
                  <Point X="21.947755859375" Y="-1.155709716797" />
                  <Point X="21.935212890625" Y="-1.146892456055" />
                  <Point X="21.910697265625" Y="-1.132464233398" />
                  <Point X="21.894580078125" Y="-1.12448828125" />
                  <Point X="21.865330078125" Y="-1.113267578125" />
                  <Point X="21.850240234375" Y="-1.10887097168" />
                  <Point X="21.81834375" Y="-1.102383300781" />
                  <Point X="21.802720703125" Y="-1.100534912109" />
                  <Point X="21.771384765625" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="21.634724609375" Y="-1.116119506836" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.2661171875" Y="-1.001046691895" />
                  <Point X="20.2592421875" Y="-0.974121032715" />
                  <Point X="20.21355078125" Y="-0.654654052734" />
                  <Point X="21.491712890625" Y="-0.312171234131" />
                  <Point X="21.4995234375" Y="-0.309713012695" />
                  <Point X="21.526064453125" Y="-0.299253814697" />
                  <Point X="21.555828125" Y="-0.283897064209" />
                  <Point X="21.56643359375" Y="-0.277517578125" />
                  <Point X="21.59209375" Y="-0.259708709717" />
                  <Point X="21.6063125" Y="-0.248256408691" />
                  <Point X="21.6287734375" Y="-0.226370040894" />
                  <Point X="21.63909375" Y="-0.214494934082" />
                  <Point X="21.65833984375" Y="-0.188239654541" />
                  <Point X="21.66655859375" Y="-0.174823913574" />
                  <Point X="21.680671875" Y="-0.146815353394" />
                  <Point X="21.68656640625" Y="-0.132222366333" />
                  <Point X="21.69512890625" Y="-0.104630233765" />
                  <Point X="21.69901171875" Y="-0.088495727539" />
                  <Point X="21.703279296875" Y="-0.06033972168" />
                  <Point X="21.7043515625" Y="-0.046103286743" />
                  <Point X="21.7043515625" Y="-0.016456888199" />
                  <Point X="21.703279296875" Y="-0.00222059989" />
                  <Point X="21.69901171875" Y="0.025935705185" />
                  <Point X="21.69581640625" Y="0.039855426788" />
                  <Point X="21.68725390625" Y="0.067447563171" />
                  <Point X="21.680671875" Y="0.084255317688" />
                  <Point X="21.66655859375" Y="0.112263893127" />
                  <Point X="21.65833984375" Y="0.125679786682" />
                  <Point X="21.63909375" Y="0.151934906006" />
                  <Point X="21.628775390625" Y="0.163807495117" />
                  <Point X="21.606322265625" Y="0.185687759399" />
                  <Point X="21.5941875" Y="0.195695281982" />
                  <Point X="21.56849609375" Y="0.213526153564" />
                  <Point X="21.56247265625" Y="0.217379211426" />
                  <Point X="21.539802734375" Y="0.229882553101" />
                  <Point X="21.504916015625" Y="0.245008346558" />
                  <Point X="21.491712890625" Y="0.249611190796" />
                  <Point X="21.38146484375" Y="0.279152069092" />
                  <Point X="20.2145546875" Y="0.591824890137" />
                  <Point X="20.26423828125" Y="0.927587585449" />
                  <Point X="20.26866796875" Y="0.957521911621" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="21.221935546875" Y="1.20560546875" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.20470324707" />
                  <Point X="21.28485546875" Y="1.206589355469" />
                  <Point X="21.295109375" Y="1.208053222656" />
                  <Point X="21.315396484375" Y="1.212088500977" />
                  <Point X="21.3254296875" Y="1.21466027832" />
                  <Point X="21.382291015625" Y="1.232588623047" />
                  <Point X="21.396546875" Y="1.237675170898" />
                  <Point X="21.41548046875" Y="1.246005737305" />
                  <Point X="21.42472265625" Y="1.250688598633" />
                  <Point X="21.443466796875" Y="1.261510375977" />
                  <Point X="21.452142578125" Y="1.267171508789" />
                  <Point X="21.46882421875" Y="1.279403320312" />
                  <Point X="21.48407421875" Y="1.293378662109" />
                  <Point X="21.497712890625" Y="1.308931274414" />
                  <Point X="21.504107421875" Y="1.317079223633" />
                  <Point X="21.516521484375" Y="1.334809082031" />
                  <Point X="21.5219921875" Y="1.343606079102" />
                  <Point X="21.531939453125" Y="1.361738891602" />
                  <Point X="21.536416015625" Y="1.371074707031" />
                  <Point X="21.559232421875" Y="1.426157470703" />
                  <Point X="21.5645078125" Y="1.440369873047" />
                  <Point X="21.570298828125" Y="1.460243774414" />
                  <Point X="21.572650390625" Y="1.470338623047" />
                  <Point X="21.576404296875" Y="1.491647949219" />
                  <Point X="21.577642578125" Y="1.501929931641" />
                  <Point X="21.5789921875" Y="1.522566040039" />
                  <Point X="21.57808984375" Y="1.543220214844" />
                  <Point X="21.574943359375" Y="1.563659423828" />
                  <Point X="21.572810546875" Y="1.573798828125" />
                  <Point X="21.567208984375" Y="1.594698730469" />
                  <Point X="21.563984375" Y="1.604544067383" />
                  <Point X="21.55648828125" Y="1.623815429688" />
                  <Point X="21.552216796875" Y="1.633241333008" />
                  <Point X="21.5246875" Y="1.686126342773" />
                  <Point X="21.51716796875" Y="1.699342163086" />
                  <Point X="21.505681640625" Y="1.716523803711" />
                  <Point X="21.499470703125" Y="1.724800415039" />
                  <Point X="21.4855625" Y="1.74137097168" />
                  <Point X="21.478486328125" Y="1.74892590332" />
                  <Point X="21.463552734375" Y="1.763219726562" />
                  <Point X="21.4556953125" Y="1.769958618164" />
                  <Point X="21.39245703125" Y="1.818483398438" />
                  <Point X="20.77238671875" Y="2.294280517578" />
                  <Point X="20.98049609375" Y="2.650821289063" />
                  <Point X="20.99771484375" Y="2.680321533203" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.75508203125" Y="2.757716552734" />
                  <Point X="21.774013671875" Y="2.749386230469" />
                  <Point X="21.78370703125" Y="2.745737792969" />
                  <Point X="21.80434765625" Y="2.739229492188" />
                  <Point X="21.8143828125" Y="2.736657470703" />
                  <Point X="21.834671875" Y="2.732621826172" />
                  <Point X="21.84492578125" Y="2.731158203125" />
                  <Point X="21.924119140625" Y="2.724229736328" />
                  <Point X="21.94082421875" Y="2.723334472656" />
                  <Point X="21.961501953125" Y="2.723785400391" />
                  <Point X="21.971830078125" Y="2.724575439453" />
                  <Point X="21.9932890625" Y="2.727400146484" />
                  <Point X="22.00346875" Y="2.729309814453" />
                  <Point X="22.023560546875" Y="2.734225830078" />
                  <Point X="22.042998046875" Y="2.741300292969" />
                  <Point X="22.061548828125" Y="2.750448242188" />
                  <Point X="22.07057421875" Y="2.755528076172" />
                  <Point X="22.088830078125" Y="2.767157714844" />
                  <Point X="22.097251953125" Y="2.773191894531" />
                  <Point X="22.113384765625" Y="2.786139648438" />
                  <Point X="22.121095703125" Y="2.793053222656" />
                  <Point X="22.17730859375" Y="2.849264892578" />
                  <Point X="22.188734375" Y="2.861488769531" />
                  <Point X="22.20168359375" Y="2.877622802734" />
                  <Point X="22.20771875" Y="2.886044677734" />
                  <Point X="22.21934765625" Y="2.904299316406" />
                  <Point X="22.2244296875" Y="2.913326660156" />
                  <Point X="22.233576171875" Y="2.931875488281" />
                  <Point X="22.240646484375" Y="2.951299804688" />
                  <Point X="22.245564453125" Y="2.971388427734" />
                  <Point X="22.2474765625" Y="2.98157421875" />
                  <Point X="22.250302734375" Y="3.003033447266" />
                  <Point X="22.251091796875" Y="3.013366210938" />
                  <Point X="22.25154296875" Y="3.034051757812" />
                  <Point X="22.251205078125" Y="3.044404541016" />
                  <Point X="22.244275390625" Y="3.123597412109" />
                  <Point X="22.242255859375" Y="3.140193847656" />
                  <Point X="22.23822265625" Y="3.160470458984" />
                  <Point X="22.235654296875" Y="3.170495117188" />
                  <Point X="22.22914453125" Y="3.191148681641" />
                  <Point X="22.22549609375" Y="3.20084765625" />
                  <Point X="22.217162109375" Y="3.219790527344" />
                  <Point X="22.2124765625" Y="3.229034423828" />
                  <Point X="22.184453125" Y="3.277571533203" />
                  <Point X="21.94061328125" Y="3.699914306641" />
                  <Point X="22.321658203125" Y="3.992056640625" />
                  <Point X="22.351642578125" Y="4.015047607422" />
                  <Point X="22.8074765625" Y="4.268295898437" />
                  <Point X="22.881435546875" Y="4.171909179688" />
                  <Point X="22.88818359375" Y="4.164043945312" />
                  <Point X="22.9024921875" Y="4.149098632812" />
                  <Point X="22.910052734375" Y="4.142018554687" />
                  <Point X="22.92663671875" Y="4.128104492188" />
                  <Point X="22.93491796875" Y="4.121892089844" />
                  <Point X="22.952111328125" Y="4.110403808594" />
                  <Point X="22.9610234375" Y="4.105127929688" />
                  <Point X="23.0491640625" Y="4.059243896484" />
                  <Point X="23.0656796875" Y="4.051284667969" />
                  <Point X="23.084962890625" Y="4.043786621094" />
                  <Point X="23.094806640625" Y="4.040564941406" />
                  <Point X="23.1157109375" Y="4.034964355469" />
                  <Point X="23.125845703125" Y="4.032833251953" />
                  <Point X="23.1462890625" Y="4.029687255859" />
                  <Point X="23.166953125" Y="4.028785644531" />
                  <Point X="23.187591796875" Y="4.030138671875" />
                  <Point X="23.197875" Y="4.031378662109" />
                  <Point X="23.2191875" Y="4.035136962891" />
                  <Point X="23.2292734375" Y="4.037488769531" />
                  <Point X="23.249130859375" Y="4.043276855469" />
                  <Point X="23.25890234375" Y="4.046713134766" />
                  <Point X="23.35070703125" Y="4.084740478516" />
                  <Point X="23.367416015625" Y="4.092272460938" />
                  <Point X="23.385548828125" Y="4.102220703125" />
                  <Point X="23.394341796875" Y="4.107688964844" />
                  <Point X="23.412072265625" Y="4.120103515625" />
                  <Point X="23.420220703125" Y="4.126497558594" />
                  <Point X="23.4357734375" Y="4.140136230469" />
                  <Point X="23.449751953125" Y="4.155391113281" />
                  <Point X="23.461982421875" Y="4.172072753906" />
                  <Point X="23.467638671875" Y="4.180744140625" />
                  <Point X="23.4784609375" Y="4.199488769531" />
                  <Point X="23.483140625" Y="4.208722167969" />
                  <Point X="23.49147265625" Y="4.227654785156" />
                  <Point X="23.495125" Y="4.237354003906" />
                  <Point X="23.525005859375" Y="4.332123535156" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370050292969" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401863769531" />
                  <Point X="23.53769921875" Y="4.412225585938" />
                  <Point X="23.53724609375" Y="4.432912597656" />
                  <Point X="23.536455078125" Y="4.443237792969" />
                  <Point X="23.533267578125" Y="4.4674375" />
                  <Point X="23.520732421875" Y="4.562654785156" />
                  <Point X="24.030025390625" Y="4.705442871094" />
                  <Point X="24.0688203125" Y="4.716319824219" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.219974609375" Y="4.218916015625" />
                  <Point X="25.232748046875" Y="4.247108886719" />
                  <Point X="25.2379765625" Y="4.261725585938" />
                  <Point X="25.2523359375" Y="4.315312988281" />
                  <Point X="25.378189453125" Y="4.785006347656" />
                  <Point X="25.793986328125" Y="4.741461425781" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.417962890625" Y="4.595447265625" />
                  <Point X="26.453595703125" Y="4.586844238281" />
                  <Point X="26.83660546875" Y="4.447924316406" />
                  <Point X="26.858259765625" Y="4.440069824219" />
                  <Point X="27.22965625" Y="4.266380371094" />
                  <Point X="27.250453125" Y="4.256654296875" />
                  <Point X="27.609287109375" Y="4.047596923828" />
                  <Point X="27.6294296875" Y="4.035861572266" />
                  <Point X="27.817783203125" Y="3.901916015625" />
                  <Point X="27.0653125" Y="2.598598144531" />
                  <Point X="27.0623828125" Y="2.593117431641" />
                  <Point X="27.053189453125" Y="2.573456542969" />
                  <Point X="27.044185546875" Y="2.549575195312" />
                  <Point X="27.041302734375" Y="2.540603271484" />
                  <Point X="27.021427734375" Y="2.466282470703" />
                  <Point X="27.019900390625" Y="2.459616943359" />
                  <Point X="27.016287109375" Y="2.439452880859" />
                  <Point X="27.013353515625" Y="2.414581054688" />
                  <Point X="27.01269921875" Y="2.403329345703" />
                  <Point X="27.012728515625" Y="2.380827636719" />
                  <Point X="27.013412109375" Y="2.369577636719" />
                  <Point X="27.021162109375" Y="2.305311279297" />
                  <Point X="27.023798828125" Y="2.289048339844" />
                  <Point X="27.02913671875" Y="2.267138916016" />
                  <Point X="27.032458984375" Y="2.256343261719" />
                  <Point X="27.040724609375" Y="2.23424609375" />
                  <Point X="27.045310546875" Y="2.223908935547" />
                  <Point X="27.055677734375" Y="2.203851318359" />
                  <Point X="27.061458984375" Y="2.194130859375" />
                  <Point X="27.101224609375" Y="2.135526123047" />
                  <Point X="27.105310546875" Y="2.129951171875" />
                  <Point X="27.118353515625" Y="2.113859130859" />
                  <Point X="27.13484765625" Y="2.09585546875" />
                  <Point X="27.14277734375" Y="2.088151123047" />
                  <Point X="27.159482421875" Y="2.07371484375" />
                  <Point X="27.1682578125" Y="2.066982910156" />
                  <Point X="27.226861328125" Y="2.027217041016" />
                  <Point X="27.241302734375" Y="2.018231811523" />
                  <Point X="27.26134375" Y="2.007873168945" />
                  <Point X="27.271673828125" Y="2.003290771484" />
                  <Point X="27.293755859375" Y="1.995028564453" />
                  <Point X="27.304556640625" Y="1.991705078125" />
                  <Point X="27.326474609375" Y="1.986364257812" />
                  <Point X="27.337591796875" Y="1.984346923828" />
                  <Point X="27.401857421875" Y="1.976597167969" />
                  <Point X="27.408943359375" Y="1.976010864258" />
                  <Point X="27.430248046875" Y="1.975314575195" />
                  <Point X="27.454306640625" Y="1.976029541016" />
                  <Point X="27.4652578125" Y="1.976991333008" />
                  <Point X="27.486978515625" Y="1.980174194336" />
                  <Point X="27.497748046875" Y="1.982395263672" />
                  <Point X="27.572068359375" Y="2.002269775391" />
                  <Point X="27.591736328125" Y="2.008736450195" />
                  <Point X="27.6246796875" Y="2.022289672852" />
                  <Point X="27.63603515625" Y="2.027872680664" />
                  <Point X="27.746923828125" Y="2.091894287109" />
                  <Point X="28.940404296875" Y="2.780950927734" />
                  <Point X="29.032" Y="2.653653564453" />
                  <Point X="29.043958984375" Y="2.637034423828" />
                  <Point X="29.136884765625" Y="2.483471679688" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.168140625" Y="1.739869995117" />
                  <Point X="28.152115234375" Y="1.725212036133" />
                  <Point X="28.134666015625" Y="1.706599121094" />
                  <Point X="28.12857421875" Y="1.699419677734" />
                  <Point X="28.0750859375" Y="1.629639404297" />
                  <Point X="28.06053125" Y="1.607636474609" />
                  <Point X="28.04776953125" Y="1.585088134766" />
                  <Point X="28.04271875" Y="1.574752441406" />
                  <Point X="28.03390625" Y="1.55354699707" />
                  <Point X="28.03014453125" Y="1.542677490234" />
                  <Point X="28.01021875" Y="1.471432006836" />
                  <Point X="28.006228515625" Y="1.454671630859" />
                  <Point X="28.0027734375" Y="1.432372680664" />
                  <Point X="28.001708984375" Y="1.421115234375" />
                  <Point X="28.000892578125" Y="1.397545166016" />
                  <Point X="28.001173828125" Y="1.386240478516" />
                  <Point X="28.003078125" Y="1.363753051758" />
                  <Point X="28.004701171875" Y="1.3525703125" />
                  <Point X="28.02105859375" Y="1.273300537109" />
                  <Point X="28.02263671875" Y="1.266809814453" />
                  <Point X="28.02828125" Y="1.247586425781" />
                  <Point X="28.036791015625" Y="1.223846191406" />
                  <Point X="28.04122265625" Y="1.213466796875" />
                  <Point X="28.051287109375" Y="1.193308227539" />
                  <Point X="28.056919921875" Y="1.183529174805" />
                  <Point X="28.10140625" Y="1.115911376953" />
                  <Point X="28.111734375" Y="1.101432617188" />
                  <Point X="28.12628125" Y="1.084193237305" />
                  <Point X="28.1340703125" Y="1.076005249023" />
                  <Point X="28.1513125" Y="1.05991418457" />
                  <Point X="28.1600234375" Y="1.052705200195" />
                  <Point X="28.178240234375" Y="1.039373168945" />
                  <Point X="28.18774609375" Y="1.033250366211" />
                  <Point X="28.25221484375" Y="0.996960876465" />
                  <Point X="28.258345703125" Y="0.993796813965" />
                  <Point X="28.2771640625" Y="0.985213684082" />
                  <Point X="28.300318359375" Y="0.976301391602" />
                  <Point X="28.3109921875" Y="0.97290032959" />
                  <Point X="28.332669921875" Y="0.967378234863" />
                  <Point X="28.343673828125" Y="0.965257385254" />
                  <Point X="28.430837890625" Y="0.953737487793" />
                  <Point X="28.446958984375" Y="0.952302062988" />
                  <Point X="28.46703125" Y="0.951374389648" />
                  <Point X="28.475439453125" Y="0.951358337402" />
                  <Point X="28.50060546875" Y="0.952797302246" />
                  <Point X="28.60594140625" Y="0.966665161133" />
                  <Point X="29.704705078125" Y="1.111319946289" />
                  <Point X="29.74755078125" Y="0.935318786621" />
                  <Point X="29.752685546875" Y="0.914225280762" />
                  <Point X="29.78387109375" Y="0.713920776367" />
                  <Point X="28.6919921875" Y="0.421352661133" />
                  <Point X="28.68603125" Y="0.419543914795" />
                  <Point X="28.66562109375" Y="0.412135070801" />
                  <Point X="28.64237890625" Y="0.401617156982" />
                  <Point X="28.63400390625" Y="0.397315734863" />
                  <Point X="28.5483671875" Y="0.347816589355" />
                  <Point X="28.527171875" Y="0.33358505249" />
                  <Point X="28.5059921875" Y="0.317196258545" />
                  <Point X="28.49718359375" Y="0.309465484619" />
                  <Point X="28.480587890625" Y="0.292981719971" />
                  <Point X="28.47280078125" Y="0.284228881836" />
                  <Point X="28.421419921875" Y="0.218756347656" />
                  <Point X="28.410859375" Y="0.204212402344" />
                  <Point X="28.399140625" Y="0.184941818237" />
                  <Point X="28.393857421875" Y="0.174960006714" />
                  <Point X="28.384080078125" Y="0.153494400024" />
                  <Point X="28.380013671875" Y="0.142947982788" />
                  <Point X="28.373162109375" Y="0.121437644958" />
                  <Point X="28.370376953125" Y="0.110473731995" />
                  <Point X="28.35325" Y="0.021041704178" />
                  <Point X="28.350125" Y="-0.004657866478" />
                  <Point X="28.34875" Y="-0.030648736954" />
                  <Point X="28.348837890625" Y="-0.042138027191" />
                  <Point X="28.350400390625" Y="-0.065025352478" />
                  <Point X="28.351875" Y="-0.076423240662" />
                  <Point X="28.369001953125" Y="-0.165855422974" />
                  <Point X="28.373162109375" Y="-0.183997817993" />
                  <Point X="28.380013671875" Y="-0.205508148193" />
                  <Point X="28.384080078125" Y="-0.216054580688" />
                  <Point X="28.393857421875" Y="-0.237520187378" />
                  <Point X="28.399140625" Y="-0.247501998901" />
                  <Point X="28.410859375" Y="-0.266772583008" />
                  <Point X="28.417294921875" Y="-0.27606137085" />
                  <Point X="28.46867578125" Y="-0.341533752441" />
                  <Point X="28.4729921875" Y="-0.346651733398" />
                  <Point X="28.48665234375" Y="-0.361385162354" />
                  <Point X="28.505083984375" Y="-0.379056121826" />
                  <Point X="28.51377734375" Y="-0.386441345215" />
                  <Point X="28.531982421875" Y="-0.400114929199" />
                  <Point X="28.541494140625" Y="-0.406403167725" />
                  <Point X="28.627130859375" Y="-0.45590246582" />
                  <Point X="28.640853515625" Y="-0.46310369873" />
                  <Point X="28.65980859375" Y="-0.472085723877" />
                  <Point X="28.66771875" Y="-0.475405944824" />
                  <Point X="28.6919921875" Y="-0.483913116455" />
                  <Point X="28.788591796875" Y="-0.509796691895" />
                  <Point X="29.78487890625" Y="-0.776751220703" />
                  <Point X="29.764482421875" Y="-0.912041503906" />
                  <Point X="29.761619140625" Y="-0.931040100098" />
                  <Point X="29.7278046875" Y="-1.079219848633" />
                  <Point X="28.436783203125" Y="-0.909253479004" />
                  <Point X="28.428625" Y="-0.908535522461" />
                  <Point X="28.40009765625" Y="-0.908042358398" />
                  <Point X="28.36672265625" Y="-0.910840942383" />
                  <Point X="28.354482421875" Y="-0.912676269531" />
                  <Point X="28.186408203125" Y="-0.949207763672" />
                  <Point X="28.157875" Y="-0.956742675781" />
                  <Point X="28.1287578125" Y="-0.968366027832" />
                  <Point X="28.11468359375" Y="-0.97538684082" />
                  <Point X="28.08685546875" Y="-0.992279174805" />
                  <Point X="28.074130859375" Y="-1.001526000977" />
                  <Point X="28.050376953125" Y="-1.021997924805" />
                  <Point X="28.03934765625" Y="-1.033223022461" />
                  <Point X="27.9377578125" Y="-1.155404052734" />
                  <Point X="27.921322265625" Y="-1.176853271484" />
                  <Point X="27.906599609375" Y="-1.201242797852" />
                  <Point X="27.900158203125" Y="-1.213990234375" />
                  <Point X="27.88881640625" Y="-1.241378540039" />
                  <Point X="27.884359375" Y="-1.254943969727" />
                  <Point X="27.87753125" Y="-1.282586181641" />
                  <Point X="27.87516015625" Y="-1.296662841797" />
                  <Point X="27.860599609375" Y="-1.454892456055" />
                  <Point X="27.859291015625" Y="-1.483322143555" />
                  <Point X="27.861609375" Y="-1.51459375" />
                  <Point X="27.864068359375" Y="-1.530136352539" />
                  <Point X="27.87180078125" Y="-1.561756713867" />
                  <Point X="27.876791015625" Y="-1.576676025391" />
                  <Point X="27.889162109375" Y="-1.605484130859" />
                  <Point X="27.89654296875" Y="-1.619373168945" />
                  <Point X="27.989556640625" Y="-1.76405078125" />
                  <Point X="28.000099609375" Y="-1.778843505859" />
                  <Point X="28.019185546875" Y="-1.803082641602" />
                  <Point X="28.0270703125" Y="-1.81190637207" />
                  <Point X="28.043876953125" Y="-1.82850378418" />
                  <Point X="28.052798828125" Y="-1.83627734375" />
                  <Point X="28.14244140625" Y="-1.905063476562" />
                  <Point X="29.087173828125" Y="-2.629980957031" />
                  <Point X="29.053572265625" Y="-2.684353027344" />
                  <Point X="29.045505859375" Y="-2.697403076172" />
                  <Point X="29.0012734375" Y="-2.760251464844" />
                  <Point X="27.848455078125" Y="-2.094670410156" />
                  <Point X="27.841189453125" Y="-2.090883300781" />
                  <Point X="27.8150234375" Y="-2.079512207031" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771109375" Y="-2.066337646484" />
                  <Point X="27.571076171875" Y="-2.030211547852" />
                  <Point X="27.539357421875" Y="-2.025807495117" />
                  <Point X="27.508001953125" Y="-2.025403686523" />
                  <Point X="27.49230859375" Y="-2.026504272461" />
                  <Point X="27.46013671875" Y="-2.031462402344" />
                  <Point X="27.444841796875" Y="-2.035137451172" />
                  <Point X="27.415068359375" Y="-2.044960693359" />
                  <Point X="27.40058984375" Y="-2.051109130859" />
                  <Point X="27.23441015625" Y="-2.138568115234" />
                  <Point X="27.20897265625" Y="-2.153168945313" />
                  <Point X="27.18604296875" Y="-2.170060058594" />
                  <Point X="27.175212890625" Y="-2.179370361328" />
                  <Point X="27.154255859375" Y="-2.200326171875" />
                  <Point X="27.144943359375" Y="-2.21115625" />
                  <Point X="27.128048828125" Y="-2.234088867188" />
                  <Point X="27.120466796875" Y="-2.24619140625" />
                  <Point X="27.0330078125" Y="-2.412370605469" />
                  <Point X="27.019837890625" Y="-2.440192138672" />
                  <Point X="27.010015625" Y="-2.469966796875" />
                  <Point X="27.006341796875" Y="-2.485258789062" />
                  <Point X="27.0013828125" Y="-2.517437011719" />
                  <Point X="27.00028125" Y="-2.533125732422" />
                  <Point X="27.00068359375" Y="-2.564479736328" />
                  <Point X="27.0021875" Y="-2.580145019531" />
                  <Point X="27.038314453125" Y="-2.780179199219" />
                  <Point X="27.042029296875" Y="-2.796560058594" />
                  <Point X="27.05083203125" Y="-2.828728027344" />
                  <Point X="27.05479296875" Y="-2.840243408203" />
                  <Point X="27.06415234375" Y="-2.862668701172" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.12715625" Y="-2.973354248047" />
                  <Point X="27.735896484375" Y="-4.027721923828" />
                  <Point X="27.723755859375" Y="-4.036082763672" />
                  <Point X="26.83391796875" Y="-2.876424072266" />
                  <Point X="26.82865625" Y="-2.870146728516" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.57601171875" Y="-2.693809082031" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.192626953125" Y="-2.666371582031" />
                  <Point X="26.161224609375" Y="-2.670339355469" />
                  <Point X="26.13357421875" Y="-2.677171630859" />
                  <Point X="26.1200078125" Y="-2.681629882812" />
                  <Point X="26.092623046875" Y="-2.692973388672" />
                  <Point X="26.079876953125" Y="-2.699414550781" />
                  <Point X="26.05549609375" Y="-2.714134521484" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.87725" Y="-2.860944335938" />
                  <Point X="25.852658203125" Y="-2.883084228516" />
                  <Point X="25.832189453125" Y="-2.906829101562" />
                  <Point X="25.822939453125" Y="-2.919553466797" />
                  <Point X="25.806046875" Y="-2.947376220703" />
                  <Point X="25.799021484375" Y="-2.961458740234" />
                  <Point X="25.78739453125" Y="-2.990584960938" />
                  <Point X="25.78279296875" Y="-3.005628662109" />
                  <Point X="25.732978515625" Y="-3.234819824219" />
                  <Point X="25.730291015625" Y="-3.250201171875" />
                  <Point X="25.725578125" Y="-3.285747558594" />
                  <Point X="25.72475390625" Y="-3.298189208984" />
                  <Point X="25.7247421875" Y="-3.323076416016" />
                  <Point X="25.7255546875" Y="-3.335521972656" />
                  <Point X="25.741880859375" Y="-3.459521972656" />
                  <Point X="25.833087890625" Y="-4.152314941406" />
                  <Point X="25.655068359375" Y="-3.487937744141" />
                  <Point X="25.652607421875" Y="-3.48012109375" />
                  <Point X="25.6421484375" Y="-3.453584472656" />
                  <Point X="25.626791015625" Y="-3.423818115234" />
                  <Point X="25.62041015625" Y="-3.413208740234" />
                  <Point X="25.46884765625" Y="-3.194836425781" />
                  <Point X="25.456681640625" Y="-3.177308349609" />
                  <Point X="25.446669921875" Y="-3.165169433594" />
                  <Point X="25.424783203125" Y="-3.142712646484" />
                  <Point X="25.412908203125" Y="-3.132394775391" />
                  <Point X="25.38665234375" Y="-3.113151611328" />
                  <Point X="25.373240234375" Y="-3.104935546875" />
                  <Point X="25.3452421875" Y="-3.090829101562" />
                  <Point X="25.33065625" Y="-3.084938720703" />
                  <Point X="25.096123046875" Y="-3.012147949219" />
                  <Point X="25.063376953125" Y="-3.003109375" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766357422" />
                  <Point X="24.9913359375" Y="-2.997766357422" />
                  <Point X="24.977095703125" Y="-2.998839599609" />
                  <Point X="24.948935546875" Y="-3.003108886719" />
                  <Point X="24.935015625" Y="-3.006304931641" />
                  <Point X="24.700482421875" Y="-3.079095458984" />
                  <Point X="24.6670703125" Y="-3.090828857422" />
                  <Point X="24.6390703125" Y="-3.104936279297" />
                  <Point X="24.62565625" Y="-3.113153076172" />
                  <Point X="24.599400390625" Y="-3.132396728516" />
                  <Point X="24.587525390625" Y="-3.142716552734" />
                  <Point X="24.565640625" Y="-3.165173095703" />
                  <Point X="24.555630859375" Y="-3.177309814453" />
                  <Point X="24.4040703125" Y="-3.395681884766" />
                  <Point X="24.391904296875" Y="-3.413210205078" />
                  <Point X="24.38752734375" Y="-3.420135009766" />
                  <Point X="24.374025390625" Y="-3.44526171875" />
                  <Point X="24.36122265625" Y="-3.476214111328" />
                  <Point X="24.35724609375" Y="-3.487937988281" />
                  <Point X="24.327482421875" Y="-3.599020263672" />
                  <Point X="24.014572265625" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.076876524887" Y="4.67779624829" />
                  <Point X="25.352849010084" Y="4.690434195571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.656203055073" Y="4.702594195947" />
                  <Point X="24.057159715406" Y="4.713050536336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.484408422955" Y="4.575668281385" />
                  <Point X="25.327508567042" Y="4.595862043485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.681781545909" Y="4.607133250565" />
                  <Point X="23.738127390869" Y="4.623604795111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.759612555869" Y="4.475850104212" />
                  <Point X="25.302168124001" Y="4.5012898914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.707360036745" Y="4.511672305182" />
                  <Point X="23.524726558249" Y="4.532315249336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.993654310657" Y="4.376750419022" />
                  <Point X="25.276827680959" Y="4.406717739314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.73293852758" Y="4.4162113598" />
                  <Point X="23.536926228061" Y="4.437087832143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.204698723606" Y="4.278052153927" />
                  <Point X="25.251487195318" Y="4.312145587972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.758517018416" Y="4.320750414417" />
                  <Point X="23.527852987404" Y="4.342231734984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.38212732213" Y="4.179940655056" />
                  <Point X="25.219294940936" Y="4.217693034698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.789492779524" Y="4.225195259332" />
                  <Point X="23.498397002146" Y="4.247731419955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.814090717228" Y="4.259676030589" />
                  <Point X="22.792635330949" Y="4.260050535749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.550250825017" Y="4.081991577233" />
                  <Point X="25.137489631007" Y="4.12410648053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.868175108712" Y="4.128807383003" />
                  <Point X="23.448105920648" Y="4.153594782883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.888841483647" Y="4.163356779943" />
                  <Point X="22.626823646336" Y="4.167930318306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.701892845258" Y="3.984330184759" />
                  <Point X="23.294043904087" Y="4.061269474221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.036642318609" Y="4.065762435608" />
                  <Point X="22.461011961723" Y="4.075810100862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.809424745039" Y="3.887438737303" />
                  <Point X="22.310399302341" Y="3.983424583447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.755115442577" Y="3.793372238539" />
                  <Point X="22.189229390667" Y="3.890525140958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.700806140115" Y="3.699305739776" />
                  <Point X="22.068059478992" Y="3.79762569847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.646496837653" Y="3.605239241012" />
                  <Point X="21.946889567318" Y="3.704726255981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.592187535192" Y="3.511172742249" />
                  <Point X="21.993158038626" Y="3.608904165646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.53787823273" Y="3.417106243485" />
                  <Point X="22.048573154796" Y="3.512922420031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.483568930268" Y="3.323039744722" />
                  <Point X="22.103988270966" Y="3.416940674416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.429259627806" Y="3.228973245958" />
                  <Point X="22.159403387136" Y="3.320958928801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.374950325345" Y="3.134906747195" />
                  <Point X="22.214530549201" Y="3.224982209444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.320641022883" Y="3.040840248431" />
                  <Point X="22.243561882729" Y="3.129460994468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.266331720421" Y="2.946773749668" />
                  <Point X="22.25153462654" Y="3.034307358543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.212022417959" Y="2.852707250904" />
                  <Point X="22.236372385778" Y="2.939557545276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.414137155556" Y="2.953909714606" />
                  <Point X="21.213291720613" Y="2.957415484713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.979336318436" Y="2.726844200859" />
                  <Point X="28.850581400297" Y="2.729091626315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.157713115497" Y="2.758640752141" />
                  <Point X="22.173680979082" Y="2.845637356686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.583837258744" Y="2.855933117122" />
                  <Point X="21.140361606112" Y="2.863674013433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.047832007281" Y="2.630634132998" />
                  <Point X="28.690841034675" Y="2.636865433604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.103403813036" Y="2.664574253377" />
                  <Point X="22.065222690455" Y="2.752516031992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.75464673873" Y="2.757937155394" />
                  <Point X="21.067431491611" Y="2.769932542153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.105942131049" Y="2.534605345851" />
                  <Point X="28.531100669054" Y="2.544639240893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.052058177148" Y="2.570456023621" />
                  <Point X="20.995295874349" Y="2.676177202871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.080281975697" Y="2.440038774364" />
                  <Point X="28.371360303432" Y="2.452413048183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.024008006608" Y="2.475931170005" />
                  <Point X="20.940396407473" Y="2.582121005466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.959210974066" Y="2.347137605395" />
                  <Point X="28.211619937811" Y="2.360186855472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.012728143319" Y="2.381113589587" />
                  <Point X="20.885496723527" Y="2.48806481185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.838139972434" Y="2.254236436425" />
                  <Point X="28.05187957219" Y="2.267960662762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.024567709686" Y="2.285892458023" />
                  <Point X="20.830597039581" Y="2.394008618233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.717068970803" Y="2.161335267455" />
                  <Point X="27.892139206568" Y="2.175734470051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.064134817519" Y="2.190187340423" />
                  <Point X="20.775697355635" Y="2.299952424617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.595997969171" Y="2.068434098485" />
                  <Point X="27.732398833778" Y="2.083508277466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.136856713518" Y="2.093903503842" />
                  <Point X="20.891453309561" Y="2.202917425761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.47492696754" Y="1.975532929516" />
                  <Point X="27.533533896697" Y="1.99196500669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.290611007258" Y="1.996205241498" />
                  <Point X="21.018160790633" Y="2.105691267288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.353855965908" Y="1.882631760546" />
                  <Point X="21.144868271705" Y="2.008465108815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.232784964277" Y="1.789730591576" />
                  <Point X="21.271575752777" Y="1.911238950342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.126392332362" Y="1.69657321071" />
                  <Point X="21.398283197465" Y="1.814012792504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.057769356084" Y="1.602756558052" />
                  <Point X="21.505225510672" Y="1.717131636319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.020555544096" Y="1.508391656392" />
                  <Point X="21.557503813734" Y="1.62120464398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.001452508016" Y="1.413710629963" />
                  <Point X="21.578850133942" Y="1.525817571411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.011728159432" Y="1.318516796637" />
                  <Point X="21.561071981635" Y="1.43111341905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.037127125448" Y="1.223058984871" />
                  <Point X="21.517793419215" Y="1.336854378002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.707732486294" Y="1.098883988664" />
                  <Point X="29.621656782906" Y="1.100386445656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.094077767319" Y="1.127050436556" />
                  <Point X="21.410277749419" Y="1.243716599835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.859417462064" Y="1.253331901918" />
                  <Point X="20.351229853785" Y="1.262202349616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.730961504916" Y="1.003464053472" />
                  <Point X="28.984435011436" Y="1.016494721886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.192970058434" Y="1.030309794029" />
                  <Point X="20.325604171563" Y="1.167635176399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.753646417681" Y="0.908053615682" />
                  <Point X="20.299978489341" Y="1.073068003181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.768479596951" Y="0.812780230411" />
                  <Point X="20.27435280712" Y="0.978500829964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.783312776222" Y="0.71750684514" />
                  <Point X="20.257755388202" Y="0.883776067825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.463489976191" Y="0.628074901715" />
                  <Point X="20.243732094295" Y="0.789006374166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.130578131758" Y="0.538871428411" />
                  <Point X="20.229708800387" Y="0.694236680508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.797666287324" Y="0.449667955107" />
                  <Point X="20.21568550648" Y="0.599466986849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.567156481656" Y="0.358677047567" />
                  <Point X="20.563275756967" Y="0.498385305295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.458154105383" Y="0.265565219958" />
                  <Point X="20.942583692502" Y="0.396749989488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.392372060075" Y="0.171698978666" />
                  <Point X="21.321891628038" Y="0.295114673681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.364000883436" Y="0.077179728232" />
                  <Point X="21.594618623488" Y="0.195339735104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.349441345875" Y="-0.017580605258" />
                  <Point X="21.673266107352" Y="0.098952467002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.358833581888" Y="-0.112759018512" />
                  <Point X="21.702422992502" Y="0.003429060515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.381036553503" Y="-0.208161043987" />
                  <Point X="21.698285557908" Y="-0.09151319146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.439372428351" Y="-0.304193771635" />
                  <Point X="21.659800180547" Y="-0.185855897864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.533085238392" Y="-0.400844005983" />
                  <Point X="21.563649880834" Y="-0.279192059303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.750752929073" Y="-0.49965788082" />
                  <Point X="21.278753280933" Y="-0.369233641818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.13006116168" Y="-0.601293201813" />
                  <Point X="20.94584167561" Y="-0.458437119296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.509369032267" Y="-0.702928516486" />
                  <Point X="20.612930070288" Y="-0.547640596774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.780969289192" Y="-0.802683787769" />
                  <Point X="20.280018464965" Y="-0.636844074252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.766682418833" Y="-0.897448880683" />
                  <Point X="20.224454151351" Y="-0.730888666715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.747678121592" Y="-0.992131630605" />
                  <Point X="28.96215690393" Y="-0.978420306748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.139540931666" Y="-0.964061491542" />
                  <Point X="20.238077500015" Y="-0.826140934314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.019592569527" Y="-1.056982256257" />
                  <Point X="20.25170084868" Y="-0.921393201914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.94172107781" Y="-1.150637475477" />
                  <Point X="20.270123081768" Y="-1.016729234353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.887722028739" Y="-1.244709389733" />
                  <Point X="21.924558454157" Y="-1.140621982361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.504326246691" Y="-1.133286801895" />
                  <Point X="20.29450194593" Y="-1.112169240174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.871224110796" Y="-1.339435888669" />
                  <Point X="22.021467983375" Y="-1.23732801565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.867106056564" Y="-1.217178553267" />
                  <Point X="20.318880810092" Y="-1.207609245995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.862494745857" Y="-1.434297988201" />
                  <Point X="22.050014353884" Y="-1.332840765565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.863942008501" Y="-1.529337721429" />
                  <Point X="22.042237065458" Y="-1.427719483655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.90015032462" Y="-1.624984211102" />
                  <Point X="21.998651174864" Y="-1.521973160269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.961928685192" Y="-1.721077027561" />
                  <Point X="21.897989889892" Y="-1.615230582168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.032556552222" Y="-1.81732431273" />
                  <Point X="21.776918711244" Y="-1.708131748048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.154701326811" Y="-1.914470828865" />
                  <Point X="21.655847532595" Y="-1.801032913928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.281409141339" Y="-2.011696993159" />
                  <Point X="21.534776353947" Y="-1.893934079808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.408116955866" Y="-2.108923157452" />
                  <Point X="27.856463372271" Y="-2.099294008333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.326607650862" Y="-2.090045342313" />
                  <Point X="21.413705175298" Y="-1.986835245688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.534824770393" Y="-2.206149321746" />
                  <Point X="28.026163547823" Y="-2.197270607079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.172218142924" Y="-2.182364934592" />
                  <Point X="21.292633996649" Y="-2.079736411568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.66153258492" Y="-2.30337548604" />
                  <Point X="28.195863723375" Y="-2.295247205826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.104673264291" Y="-2.276200405514" />
                  <Point X="21.171562818001" Y="-2.172637577448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.788240399447" Y="-2.400601650333" />
                  <Point X="28.365563898927" Y="-2.393223804573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.055122985909" Y="-2.370349973352" />
                  <Point X="21.050491639352" Y="-2.265538743327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.914948213975" Y="-2.497827814627" />
                  <Point X="28.535264074478" Y="-2.49120040332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.011783437688" Y="-2.464607949888" />
                  <Point X="22.562613620567" Y="-2.386947401853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.307146367314" Y="-2.382488204361" />
                  <Point X="20.929420460704" Y="-2.358439909207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.041656028502" Y="-2.59505397892" />
                  <Point X="28.70496425003" Y="-2.589177002066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.000618762501" Y="-2.559427540922" />
                  <Point X="22.655743157291" Y="-2.483587455128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.147405898363" Y="-2.474714395268" />
                  <Point X="20.823645806809" Y="-2.45160807692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.049950002526" Y="-2.69021322194" />
                  <Point X="28.874664425582" Y="-2.687153600813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.01565322209" Y="-2.654704439554" />
                  <Point X="22.68817234753" Y="-2.579167979913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.987665493239" Y="-2.566940587289" />
                  <Point X="20.880904113607" Y="-2.547621995547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.03286747332" Y="-2.750019386591" />
                  <Point X="26.653133717664" Y="-2.743391109231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.031677701287" Y="-2.732543554115" />
                  <Point X="22.678082483957" Y="-2.674006331853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.827925088115" Y="-2.65916677931" />
                  <Point X="20.938162420406" Y="-2.643635914174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.056967872771" Y="-2.845454531792" />
                  <Point X="26.798561428912" Y="-2.840944030537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.919753388344" Y="-2.825604379129" />
                  <Point X="22.633041738807" Y="-2.768234613886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.668184682991" Y="-2.751392971331" />
                  <Point X="20.995420727204" Y="-2.739649832801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.108691203079" Y="-2.941371837045" />
                  <Point X="26.880700518201" Y="-2.937392244838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.823387753327" Y="-2.918936781878" />
                  <Point X="22.578732335014" Y="-2.862301110881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.508444277867" Y="-2.843619163352" />
                  <Point X="21.068560641726" Y="-2.835940965922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.164106330093" Y="-3.03735358285" />
                  <Point X="26.954597482934" Y="-3.033696592319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.781144327326" Y="-3.013213891298" />
                  <Point X="25.045307914034" Y="-3.000369818928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.975087150743" Y="-2.999144110945" />
                  <Point X="22.524422931221" Y="-2.956367607876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.348703872743" Y="-2.935845355373" />
                  <Point X="21.141855556658" Y="-2.932234804585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.21952148754" Y="-3.133335329185" />
                  <Point X="27.028494447667" Y="-3.130000939801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.760571085564" Y="-3.107869255192" />
                  <Point X="25.365371564864" Y="-3.100971021898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.672622615151" Y="-3.088879044002" />
                  <Point X="22.470113527428" Y="-3.050434104871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.274936644986" Y="-3.22931707552" />
                  <Point X="27.1023914124" Y="-3.226305287283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.739997843803" Y="-3.202524619085" />
                  <Point X="25.470923916558" Y="-3.197827916214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.552516502053" Y="-3.181797055164" />
                  <Point X="22.415804123635" Y="-3.144500601865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.330351802433" Y="-3.325298821856" />
                  <Point X="27.176288377133" Y="-3.322609634764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.724814531583" Y="-3.297274064549" />
                  <Point X="25.537677940826" Y="-3.294007583206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.487361296323" Y="-3.275674237981" />
                  <Point X="22.361494719842" Y="-3.23856709886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.385766959879" Y="-3.421280568191" />
                  <Point X="27.250185341865" Y="-3.418913982246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.733047646307" Y="-3.392432245265" />
                  <Point X="25.604431965094" Y="-3.390187250198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.422206090593" Y="-3.369551420799" />
                  <Point X="22.307185316049" Y="-3.332633595855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.441182117326" Y="-3.517262314527" />
                  <Point X="27.324082306598" Y="-3.515218329727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.745585998514" Y="-3.487665574181" />
                  <Point X="25.654482016165" Y="-3.486075348254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.36644327104" Y="-3.463592548327" />
                  <Point X="22.252875912256" Y="-3.42670009285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.496597274772" Y="-3.613244060862" />
                  <Point X="27.397979271331" Y="-3.611522677209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.758123579538" Y="-3.582898889636" />
                  <Point X="25.680148454324" Y="-3.581537828762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.338441812505" Y="-3.558118252215" />
                  <Point X="22.198566508463" Y="-3.520766589844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.552012432219" Y="-3.709225807198" />
                  <Point X="27.471876236064" Y="-3.707827024691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.770661160563" Y="-3.678132205091" />
                  <Point X="25.705727167002" Y="-3.676998778017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.313101424867" Y="-3.652690405267" />
                  <Point X="22.14425710467" Y="-3.614833086839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.607427589665" Y="-3.805207553533" />
                  <Point X="27.545773200797" Y="-3.804131372172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.783198741587" Y="-3.773365520546" />
                  <Point X="25.731305879681" Y="-3.772459727272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.28776067267" Y="-3.747262551957" />
                  <Point X="22.089947700877" Y="-3.708899583834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.662842747112" Y="-3.901189299868" />
                  <Point X="27.61967016553" Y="-3.900435719654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.795736322611" Y="-3.868598836001" />
                  <Point X="25.756884592359" Y="-3.867920676527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.262419920473" Y="-3.841834698646" />
                  <Point X="23.463610986005" Y="-3.82789143683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.901176900036" Y="-3.818074113341" />
                  <Point X="22.035638297084" Y="-3.802966080829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.718257904558" Y="-3.997171046204" />
                  <Point X="27.693567130263" Y="-3.996740067135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.808273903635" Y="-3.963832151456" />
                  <Point X="25.782463305038" Y="-3.963381625781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.237079168276" Y="-3.936406845335" />
                  <Point X="23.5853439462" Y="-3.925030764718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.762597867108" Y="-3.910669678488" />
                  <Point X="22.107035543645" Y="-3.899226795567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.82081148466" Y="-4.059065466911" />
                  <Point X="25.808042017716" Y="-4.058842575036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.211738416079" Y="-4.030978992024" />
                  <Point X="23.69588683588" Y="-4.021974769199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.627686592906" Y="-4.003329264601" />
                  <Point X="22.233298631146" Y="-3.996445197122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.186397663882" Y="-4.125551138714" />
                  <Point X="23.784560471243" Y="-4.118537044426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.547092409946" Y="-4.096936959069" />
                  <Point X="22.38641553125" Y="-4.094132333719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.161056911685" Y="-4.220123285403" />
                  <Point X="23.82027040115" Y="-4.214174834735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.135716159488" Y="-4.314695432092" />
                  <Point X="23.839236286139" Y="-4.309520356653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.110375407291" Y="-4.409267578781" />
                  <Point X="23.858202125441" Y="-4.404865877774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.085034655094" Y="-4.503839725471" />
                  <Point X="23.877007470424" Y="-4.500208597455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.059693902897" Y="-4.59841187216" />
                  <Point X="23.876675279891" Y="-4.595217270212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.0343531507" Y="-4.692984018849" />
                  <Point X="23.864195051559" Y="-4.690013898181" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001625976562" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.842357421875" Y="-4.92101953125" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521543212891" />
                  <Point X="25.3127578125" Y="-3.303170898438" />
                  <Point X="25.300591796875" Y="-3.285642822266" />
                  <Point X="25.2743359375" Y="-3.266399658203" />
                  <Point X="25.039802734375" Y="-3.193608886719" />
                  <Point X="25.0209765625" Y="-3.187766357422" />
                  <Point X="24.9913359375" Y="-3.187766357422" />
                  <Point X="24.756802734375" Y="-3.260556884766" />
                  <Point X="24.7379765625" Y="-3.266399658203" />
                  <Point X="24.711720703125" Y="-3.285643310547" />
                  <Point X="24.56016015625" Y="-3.504015380859" />
                  <Point X="24.547994140625" Y="-3.521543701172" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.511009765625" Y="-3.648194580078" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.920552734375" Y="-4.942102539062" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534756347656" />
                  <Point X="23.634287109375" Y="-4.253074707031" />
                  <Point X="23.6297890625" Y="-4.23046484375" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.3977890625" Y="-4.013263671875" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.064173828125" Y="-3.966979003906" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.010119140625" Y="-3.973792236328" />
                  <Point X="22.7713203125" Y="-4.133352539062" />
                  <Point X="22.752154296875" Y="-4.14616015625" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.723556640625" Y="-4.179071777344" />
                  <Point X="22.54290625" Y="-4.414500488281" />
                  <Point X="22.1746484375" Y="-4.186485351562" />
                  <Point X="22.144169921875" Y="-4.167613769531" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="21.804845703125" Y="-3.822712402344" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597593261719" />
                  <Point X="22.486021484375" Y="-2.568764404297" />
                  <Point X="22.469962890625" Y="-2.552704833984" />
                  <Point X="22.468673828125" Y="-2.551415771484" />
                  <Point X="22.43984375" Y="-2.537198730469" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="22.31251171875" Y="-2.598783691406" />
                  <Point X="21.15704296875" Y="-3.265893798828" />
                  <Point X="20.862380859375" Y="-2.87876953125" />
                  <Point X="20.838298828125" Y="-2.847130126953" />
                  <Point X="20.568982421875" Y="-2.395525878906" />
                  <Point X="20.630474609375" Y="-2.348340576172" />
                  <Point X="21.836212890625" Y="-1.423144897461" />
                  <Point X="21.854177734375" Y="-1.396014770508" />
                  <Point X="21.861310546875" Y="-1.368477783203" />
                  <Point X="21.861884765625" Y="-1.366262084961" />
                  <Point X="21.859673828125" Y="-1.334593261719" />
                  <Point X="21.838841796875" Y="-1.310638793945" />
                  <Point X="21.814326171875" Y="-1.296210571289" />
                  <Point X="21.812369140625" Y="-1.29505871582" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.659525390625" Y="-1.304494018555" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.08202734375" Y="-1.048069458008" />
                  <Point X="20.072609375" Y="-1.011192993164" />
                  <Point X="20.00160546875" Y="-0.514741943359" />
                  <Point X="20.06901953125" Y="-0.496678314209" />
                  <Point X="21.442537109375" Y="-0.128645339966" />
                  <Point X="21.4581015625" Y="-0.12142742157" />
                  <Point X="21.48379296875" Y="-0.103596504211" />
                  <Point X="21.48585546875" Y="-0.102165397644" />
                  <Point X="21.5051015625" Y="-0.075910133362" />
                  <Point X="21.5136640625" Y="-0.048317913055" />
                  <Point X="21.5143515625" Y="-0.046103252411" />
                  <Point X="21.5143515625" Y="-0.016456857681" />
                  <Point X="21.5057890625" Y="0.01113535881" />
                  <Point X="21.5051015625" Y="0.013350020409" />
                  <Point X="21.48585546875" Y="0.039605285645" />
                  <Point X="21.4601640625" Y="0.057436050415" />
                  <Point X="21.442537109375" Y="0.066085227966" />
                  <Point X="21.3322890625" Y="0.095626205444" />
                  <Point X="20.001814453125" Y="0.452126068115" />
                  <Point X="20.07628515625" Y="0.955399780273" />
                  <Point X="20.08235546875" Y="0.996417358398" />
                  <Point X="20.2263046875" Y="1.527634521484" />
                  <Point X="20.226484375" Y="1.528298706055" />
                  <Point X="20.2621640625" Y="1.5236015625" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866455078" />
                  <Point X="21.325158203125" Y="1.413794799805" />
                  <Point X="21.32972265625" Y="1.415234008789" />
                  <Point X="21.348466796875" Y="1.426055786133" />
                  <Point X="21.360880859375" Y="1.443785644531" />
                  <Point X="21.383697265625" Y="1.498868408203" />
                  <Point X="21.385533203125" Y="1.503301879883" />
                  <Point X="21.389287109375" Y="1.524611328125" />
                  <Point X="21.383685546875" Y="1.545511230469" />
                  <Point X="21.35615625" Y="1.598396118164" />
                  <Point X="21.353939453125" Y="1.602651245117" />
                  <Point X="21.34003125" Y="1.619221801758" />
                  <Point X="21.27679296875" Y="1.667746582031" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.81640234375" Y="2.746600585938" />
                  <Point X="20.839986328125" Y="2.787006103516" />
                  <Point X="21.221302734375" Y="3.277134765625" />
                  <Point X="21.2361484375" Y="3.276064941406" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861484375" Y="2.920435058594" />
                  <Point X="21.940677734375" Y="2.913506591797" />
                  <Point X="21.947033203125" Y="2.912950439453" />
                  <Point X="21.9684921875" Y="2.915775146484" />
                  <Point X="21.986748046875" Y="2.927404785156" />
                  <Point X="22.0429609375" Y="2.983616455078" />
                  <Point X="22.04747265625" Y="2.988128173828" />
                  <Point X="22.0591015625" Y="3.0063828125" />
                  <Point X="22.061927734375" Y="3.027842041016" />
                  <Point X="22.054998046875" Y="3.107034912109" />
                  <Point X="22.054443359375" Y="3.113379394531" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="22.01991015625" Y="3.182570068359" />
                  <Point X="21.69272265625" Y="3.749276123047" />
                  <Point X="22.206052734375" Y="4.142840332031" />
                  <Point X="22.247130859375" Y="4.1743359375" />
                  <Point X="22.8476875" Y="4.507990722656" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.0487578125" Y="4.273659179688" />
                  <Point X="23.1368984375" Y="4.227775390625" />
                  <Point X="23.143974609375" Y="4.224092285156" />
                  <Point X="23.16487890625" Y="4.218491699219" />
                  <Point X="23.18619140625" Y="4.22225" />
                  <Point X="23.27799609375" Y="4.26027734375" />
                  <Point X="23.285365234375" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.294488769531" />
                  <Point X="23.343798828125" Y="4.389258300781" />
                  <Point X="23.346197265625" Y="4.396865234375" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.344896484375" Y="4.442625976562" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.978732421875" Y="4.888388183594" />
                  <Point X="24.03190625" Y="4.903296386719" />
                  <Point X="24.75997265625" Y="4.988505859375" />
                  <Point X="24.778974609375" Y="4.978514160156" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.0688125" Y="4.364490722656" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.813775390625" Y="4.930428222656" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.462552734375" Y="4.780140625" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.901390625" Y="4.626538574219" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.31014453125" Y="4.438488769531" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.704931640625" Y="4.211767089844" />
                  <Point X="27.73252734375" Y="4.195689941406" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="28.0274609375" Y="3.885092285156" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491517822266" />
                  <Point X="27.204978515625" Y="2.417197021484" />
                  <Point X="27.202044921875" Y="2.392325195312" />
                  <Point X="27.209794921875" Y="2.328058837891" />
                  <Point X="27.210416015625" Y="2.322909667969" />
                  <Point X="27.218681640625" Y="2.3008125" />
                  <Point X="27.258447265625" Y="2.242207763672" />
                  <Point X="27.258455078125" Y="2.242198242188" />
                  <Point X="27.27494140625" Y="2.224204101562" />
                  <Point X="27.333544921875" Y="2.184438232422" />
                  <Point X="27.338255859375" Y="2.181242431641" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.424603515625" Y="2.165230712891" />
                  <Point X="27.448662109375" Y="2.165945556641" />
                  <Point X="27.522982421875" Y="2.185820068359" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="27.651923828125" Y="2.256439208984" />
                  <Point X="28.994248046875" Y="3.031430908203" />
                  <Point X="29.1862265625" Y="2.764625244141" />
                  <Point X="29.202591796875" Y="2.741883789062" />
                  <Point X="29.387513671875" Y="2.436295654297" />
                  <Point X="29.338740234375" Y="2.398870605469" />
                  <Point X="28.2886171875" Y="1.593082763672" />
                  <Point X="28.27937109375" Y="1.583831298828" />
                  <Point X="28.2258828125" Y="1.514051025391" />
                  <Point X="28.21312109375" Y="1.491502685547" />
                  <Point X="28.1931953125" Y="1.420256958008" />
                  <Point X="28.191595703125" Y="1.414538208008" />
                  <Point X="28.190779296875" Y="1.390968017578" />
                  <Point X="28.20713671875" Y="1.311698120117" />
                  <Point X="28.215646484375" Y="1.287957763672" />
                  <Point X="28.2601328125" Y="1.22033984375" />
                  <Point X="28.263703125" Y="1.214912475586" />
                  <Point X="28.2809453125" Y="1.198821411133" />
                  <Point X="28.3454140625" Y="1.162531860352" />
                  <Point X="28.368568359375" Y="1.153619506836" />
                  <Point X="28.455732421875" Y="1.142099487305" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.581140625" Y="1.155039672852" />
                  <Point X="29.848978515625" Y="1.321953491211" />
                  <Point X="29.93216015625" Y="0.98025970459" />
                  <Point X="29.939189453125" Y="0.951386962891" />
                  <Point X="29.997859375" Y="0.574556274414" />
                  <Point X="29.9446328125" Y="0.560293945312" />
                  <Point X="28.74116796875" Y="0.237826919556" />
                  <Point X="28.7290859375" Y="0.232818191528" />
                  <Point X="28.64344921875" Y="0.183318954468" />
                  <Point X="28.62226953125" Y="0.166930099487" />
                  <Point X="28.570888671875" Y="0.101457588196" />
                  <Point X="28.566763671875" Y="0.096202270508" />
                  <Point X="28.556986328125" Y="0.074736686707" />
                  <Point X="28.539859375" Y="-0.014695351601" />
                  <Point X="28.538484375" Y="-0.04068624115" />
                  <Point X="28.555611328125" Y="-0.130118438721" />
                  <Point X="28.556986328125" Y="-0.137296798706" />
                  <Point X="28.566763671875" Y="-0.158762374878" />
                  <Point X="28.61814453125" Y="-0.224234741211" />
                  <Point X="28.636576171875" Y="-0.241905792236" />
                  <Point X="28.722212890625" Y="-0.291405029297" />
                  <Point X="28.74116796875" Y="-0.300387023926" />
                  <Point X="28.837767578125" Y="-0.32627053833" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.952357421875" Y="-0.940366943359" />
                  <Point X="29.948431640625" Y="-0.966411560059" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.807662109375" Y="-1.281372924805" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341308594" />
                  <Point X="28.226763671875" Y="-1.134872802734" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.1854453125" Y="-1.154697387695" />
                  <Point X="28.08385546875" Y="-1.276878417969" />
                  <Point X="28.075701171875" Y="-1.286685424805" />
                  <Point X="28.064359375" Y="-1.314073486328" />
                  <Point X="28.049798828125" Y="-1.472303100586" />
                  <Point X="28.048630859375" Y="-1.48500378418" />
                  <Point X="28.05636328125" Y="-1.516624145508" />
                  <Point X="28.149376953125" Y="-1.661301757812" />
                  <Point X="28.168462890625" Y="-1.685540893555" />
                  <Point X="28.25810546875" Y="-1.754327148438" />
                  <Point X="29.339076171875" Y="-2.583784423828" />
                  <Point X="29.21519921875" Y="-2.784236816406" />
                  <Point X="29.20414453125" Y="-2.802122070312" />
                  <Point X="29.056689453125" Y="-3.011638183594" />
                  <Point X="28.99649609375" Y="-2.976885986328" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.73733984375" Y="-2.253312744141" />
                  <Point X="27.537306640625" Y="-2.217186767578" />
                  <Point X="27.52125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219245361328" />
                  <Point X="27.3228984375" Y="-2.306704345703" />
                  <Point X="27.309560546875" Y="-2.313724365234" />
                  <Point X="27.288603515625" Y="-2.334680175781" />
                  <Point X="27.20114453125" Y="-2.500859375" />
                  <Point X="27.194123046875" Y="-2.514198242188" />
                  <Point X="27.1891640625" Y="-2.546376464844" />
                  <Point X="27.225291015625" Y="-2.746410644531" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.29169921875" Y="-2.878354248047" />
                  <Point X="27.98667578125" Y="-4.082087890625" />
                  <Point X="27.848431640625" Y="-4.18083203125" />
                  <Point X="27.835294921875" Y="-4.19021484375" />
                  <Point X="27.679775390625" Y="-4.290878417969" />
                  <Point X="27.631734375" Y="-4.228268066406" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.47326171875" Y="-2.853629394531" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.210037109375" Y="-2.855572265625" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509521484" />
                  <Point X="25.99872265625" Y="-3.007040527344" />
                  <Point X="25.985349609375" Y="-3.018159912109" />
                  <Point X="25.96845703125" Y="-3.045982666016" />
                  <Point X="25.918642578125" Y="-3.275173828125" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.930255859375" Y="-3.434720214844" />
                  <Point X="26.127642578125" Y="-4.934028320312" />
                  <Point X="26.006765625" Y="-4.960524902344" />
                  <Point X="25.994345703125" Y="-4.963247558594" />
                  <Point X="25.860201171875" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#210" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.17888588467" Y="5.022735717989" Z="2.35" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.35" />
                  <Point X="-0.251997501656" Y="5.069446858853" Z="2.35" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.35" />
                  <Point X="-1.040921814484" Y="4.967811911153" Z="2.35" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.35" />
                  <Point X="-1.710831794447" Y="4.467379908761" Z="2.35" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.35" />
                  <Point X="-1.710044342814" Y="4.435573668988" Z="2.35" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.35" />
                  <Point X="-1.747294078111" Y="4.337751762799" Z="2.35" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.35" />
                  <Point X="-1.846173908459" Y="4.303407543494" Z="2.35" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.35" />
                  <Point X="-2.119431006362" Y="4.590539045953" Z="2.35" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.35" />
                  <Point X="-2.182753310146" Y="4.582978036368" Z="2.35" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.35" />
                  <Point X="-2.830528703279" Y="4.213799789576" Z="2.35" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.35" />
                  <Point X="-3.029547711361" Y="3.188850209572" Z="2.35" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.35" />
                  <Point X="-3.000968552032" Y="3.133956324779" Z="2.35" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.35" />
                  <Point X="-2.998551962028" Y="3.050251605858" Z="2.35" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.35" />
                  <Point X="-3.061120140828" Y="2.994596147141" Z="2.35" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.35" />
                  <Point X="-3.745009138725" Y="3.350646237385" Z="2.35" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.35" />
                  <Point X="-3.824317522902" Y="3.339117366544" Z="2.35" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.35" />
                  <Point X="-4.232936940108" Y="2.803085765564" Z="2.35" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.35" />
                  <Point X="-3.759801084296" Y="1.659358949662" Z="2.35" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.35" />
                  <Point X="-3.694352525582" Y="1.606589202827" Z="2.35" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.35" />
                  <Point X="-3.668653818099" Y="1.549283027099" Z="2.35" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.35" />
                  <Point X="-3.696034064265" Y="1.492761038834" Z="2.35" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.35" />
                  <Point X="-4.737467093253" Y="1.604453808156" Z="2.35" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.35" />
                  <Point X="-4.828112066969" Y="1.571990917284" Z="2.35" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.35" />
                  <Point X="-4.979332020417" Y="0.993999727868" Z="2.35" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.35" />
                  <Point X="-3.686809225746" Y="0.078610026401" Z="2.35" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.35" />
                  <Point X="-3.57449860997" Y="0.047637799053" Z="2.35" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.35" />
                  <Point X="-3.548120472366" Y="0.027592201131" Z="2.35" />
                  <Point X="-3.539556741714" Y="0" Z="2.35" />
                  <Point X="-3.540244128769" Y="-0.00221474993" Z="2.35" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.35" />
                  <Point X="-3.550869985154" Y="-0.03123818408" Z="2.35" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.35" />
                  <Point X="-4.950078563027" Y="-0.417102030355" Z="2.35" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.35" />
                  <Point X="-5.054556275591" Y="-0.48699169214" Z="2.35" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.35" />
                  <Point X="-4.972574449274" Y="-1.029162117895" Z="2.35" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.35" />
                  <Point X="-3.340105403614" Y="-1.322786215772" Z="2.35" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.35" />
                  <Point X="-3.217191148972" Y="-1.308021433436" Z="2.35" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.35" />
                  <Point X="-3.193248774011" Y="-1.324660125993" Z="2.35" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.35" />
                  <Point X="-4.406119295095" Y="-2.277392817281" Z="2.35" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.35" />
                  <Point X="-4.481089232743" Y="-2.388230055878" Z="2.35" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.35" />
                  <Point X="-4.183563070699" Y="-2.877772214814" Z="2.35" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.35" />
                  <Point X="-2.668645916043" Y="-2.610804830791" Z="2.35" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.35" />
                  <Point X="-2.571550448496" Y="-2.556780020635" Z="2.35" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.35" />
                  <Point X="-3.244612182378" Y="-3.766432466449" Z="2.35" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.35" />
                  <Point X="-3.269502584184" Y="-3.885663933607" Z="2.35" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.35" />
                  <Point X="-2.857829852465" Y="-4.19771553676" Z="2.35" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.35" />
                  <Point X="-2.242932715858" Y="-4.178229639661" Z="2.35" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.35" />
                  <Point X="-2.207054573177" Y="-4.143644710087" Z="2.35" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.35" />
                  <Point X="-1.945252552258" Y="-3.985592094797" Z="2.35" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.35" />
                  <Point X="-1.641334835058" Y="-4.01957572371" Z="2.35" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.35" />
                  <Point X="-1.420908600279" Y="-4.231550335332" Z="2.35" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.35" />
                  <Point X="-1.409516115033" Y="-4.852288164892" Z="2.35" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.35" />
                  <Point X="-1.391127820433" Y="-4.885156256632" Z="2.35" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.35" />
                  <Point X="-1.095130680207" Y="-4.959906482645" Z="2.35" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.35" />
                  <Point X="-0.446851226788" Y="-3.629855514263" Z="2.35" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.35" />
                  <Point X="-0.404921298506" Y="-3.501244939329" Z="2.35" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.35" />
                  <Point X="-0.234533705304" Y="-3.277029975463" Z="2.35" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.35" />
                  <Point X="0.018825374057" Y="-3.210082069825" Z="2.35" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.35" />
                  <Point X="0.265524560927" Y="-3.300400816439" Z="2.35" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.35" />
                  <Point X="0.787903633613" Y="-4.902680515553" Z="2.35" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.35" />
                  <Point X="0.831068062241" Y="-5.011328663234" Z="2.35" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.35" />
                  <Point X="1.011313601206" Y="-4.978085270126" Z="2.35" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.35" />
                  <Point X="0.973670716939" Y="-3.396913858298" Z="2.35" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.35" />
                  <Point X="0.96134434992" Y="-3.254517153334" Z="2.35" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.35" />
                  <Point X="1.024533371954" Y="-3.014206506676" Z="2.35" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.35" />
                  <Point X="1.208462824234" Y="-2.874081722975" Z="2.35" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.35" />
                  <Point X="1.440066196293" Y="-2.86440750682" Z="2.35" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.35" />
                  <Point X="2.585908784678" Y="-4.227425814576" Z="2.35" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.35" />
                  <Point X="2.676552642333" Y="-4.317261183741" Z="2.35" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.35" />
                  <Point X="2.871334631609" Y="-4.190240521488" Z="2.35" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.35" />
                  <Point X="2.328842312604" Y="-2.822074087832" Z="2.35" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.35" />
                  <Point X="2.268337158424" Y="-2.706242424786" Z="2.35" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.35" />
                  <Point X="2.239231205178" Y="-2.492869432224" Z="2.35" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.35" />
                  <Point X="2.340028946148" Y="-2.31967010479" Z="2.35" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.35" />
                  <Point X="2.522264378726" Y="-2.235110851067" Z="2.35" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.35" />
                  <Point X="3.965339509275" Y="-2.988907187671" Z="2.35" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.35" />
                  <Point X="4.078088843168" Y="-3.028078524101" Z="2.35" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.35" />
                  <Point X="4.251572583624" Y="-2.779244091748" Z="2.35" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.35" />
                  <Point X="3.282387010763" Y="-1.683379441626" Z="2.35" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.35" />
                  <Point X="3.185276821845" Y="-1.602980152475" Z="2.35" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.35" />
                  <Point X="3.093431216829" Y="-1.445601843084" Z="2.35" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.35" />
                  <Point X="3.116145785658" Y="-1.2775651056" Z="2.35" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.35" />
                  <Point X="3.231226328077" Y="-1.152451901929" Z="2.35" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.35" />
                  <Point X="4.79497939161" Y="-1.299665106759" Z="2.35" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.35" />
                  <Point X="4.913280276789" Y="-1.286922294118" Z="2.35" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.35" />
                  <Point X="4.995642217184" Y="-0.916539676377" Z="2.35" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.35" />
                  <Point X="3.844551413938" Y="-0.24669418317" Z="2.35" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.35" />
                  <Point X="3.741078917693" Y="-0.216837467691" Z="2.35" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.35" />
                  <Point X="3.65131844551" Y="-0.162082919244" Z="2.35" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.35" />
                  <Point X="3.598561967316" Y="-0.089432065802" Z="2.35" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.35" />
                  <Point X="3.582809483109" Y="0.007178465376" Z="2.35" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.35" />
                  <Point X="3.60406099289" Y="0.101865819331" Z="2.35" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.35" />
                  <Point X="3.662316496659" Y="0.171311376551" Z="2.35" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.35" />
                  <Point X="4.951416403369" Y="0.543277749599" Z="2.35" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.35" />
                  <Point X="5.043118375074" Y="0.60061227408" Z="2.35" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.35" />
                  <Point X="4.974583939535" Y="1.023367223599" Z="2.35" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.35" />
                  <Point X="3.568458298563" Y="1.235891951323" Z="2.35" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.35" />
                  <Point X="3.456124971839" Y="1.222948748224" Z="2.35" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.35" />
                  <Point X="3.363785995021" Y="1.237381444382" Z="2.35" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.35" />
                  <Point X="3.2957477249" Y="1.279098531259" Z="2.35" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.35" />
                  <Point X="3.249948136548" Y="1.353079189599" Z="2.35" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.35" />
                  <Point X="3.235191330549" Y="1.438067908525" Z="2.35" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.35" />
                  <Point X="3.259409382524" Y="1.514914776844" Z="2.35" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.35" />
                  <Point X="4.363021381676" Y="2.390482962493" Z="2.35" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.35" />
                  <Point X="4.431773019248" Y="2.48083946111" Z="2.35" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.35" />
                  <Point X="4.220654170397" Y="2.825109846546" Z="2.35" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.35" />
                  <Point X="2.620766674689" Y="2.331020416483" Z="2.35" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.35" />
                  <Point X="2.50391239895" Y="2.265403509767" Z="2.35" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.35" />
                  <Point X="2.42443317037" Y="2.246151192594" Z="2.35" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.35" />
                  <Point X="2.355462751998" Y="2.2570924082" Z="2.35" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.35" />
                  <Point X="2.293666192035" Y="2.301562108384" Z="2.35" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.35" />
                  <Point X="2.253278510165" Y="2.365325273462" Z="2.35" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.35" />
                  <Point X="2.247124432891" Y="2.435557176932" Z="2.35" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.35" />
                  <Point X="3.064604936376" Y="3.891372407266" Z="2.35" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.35" />
                  <Point X="3.100753342881" Y="4.02208307171" Z="2.35" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.35" />
                  <Point X="2.72394487246" Y="4.286249628217" Z="2.35" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.35" />
                  <Point X="2.325169779958" Y="4.515060697623" Z="2.35" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.35" />
                  <Point X="1.912281979878" Y="4.704822136772" Z="2.35" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.35" />
                  <Point X="1.46813003542" Y="4.860024372681" Z="2.35" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.35" />
                  <Point X="0.812826526386" Y="5.011436253047" Z="2.35" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.35" />
                  <Point X="0.014358676883" Y="4.408711787962" Z="2.35" />
                  <Point X="0" Y="4.355124473572" Z="2.35" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>