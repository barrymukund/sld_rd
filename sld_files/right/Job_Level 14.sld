<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#143" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1141" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.78389453125" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.666015625" Y="-3.895848144531" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140625" />
                  <Point X="25.542365234375" Y="-3.467377197266" />
                  <Point X="25.500384765625" Y="-3.406890869141" />
                  <Point X="25.37863671875" Y="-3.231476806641" />
                  <Point X="25.356751953125" Y="-3.209020263672" />
                  <Point X="25.33049609375" Y="-3.189776611328" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.237533203125" Y="-3.155507080078" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.963177734375" Y="-3.097035888672" />
                  <Point X="24.89821484375" Y="-3.117197753906" />
                  <Point X="24.709818359375" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.18977734375" />
                  <Point X="24.655560546875" Y="-3.209021484375" />
                  <Point X="24.63367578125" Y="-3.231477050781" />
                  <Point X="24.5916953125" Y="-3.291963134766" />
                  <Point X="24.46994921875" Y="-3.467377441406" />
                  <Point X="24.4618125" Y="-3.481571777344" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.151138671875" Y="-4.624190429688" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.920658203125" Y="-4.845349609375" />
                  <Point X="23.849068359375" Y="-4.8269296875" />
                  <Point X="23.753583984375" Y="-4.802362304688" />
                  <Point X="23.772673828125" Y="-4.657352539062" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.516223632812" />
                  <Point X="23.76797265625" Y="-4.438201660156" />
                  <Point X="23.722962890625" Y="-4.211932128906" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204589844" />
                  <Point X="23.616546875" Y="-4.078752929688" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.277591796875" Y="-3.885763427734" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894802001953" />
                  <Point X="22.89119921875" Y="-3.938998046875" />
                  <Point X="22.699376953125" Y="-4.067170166016" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.5198515625" Y="-4.288489746094" />
                  <Point X="22.518716796875" Y="-4.287787597656" />
                  <Point X="22.19829296875" Y="-4.089388671875" />
                  <Point X="22.09915625" Y="-4.013056884766" />
                  <Point X="21.895279296875" Y="-3.856077392578" />
                  <Point X="22.385361328125" Y="-3.007228759766" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647653076172" />
                  <Point X="22.593412109375" Y="-2.616124023438" />
                  <Point X="22.59442578125" Y="-2.585186279297" />
                  <Point X="22.5854375" Y="-2.555565429688" />
                  <Point X="22.571216796875" Y="-2.526735351562" />
                  <Point X="22.553193359375" Y="-2.501586181641" />
                  <Point X="22.5358515625" Y="-2.484243896484" />
                  <Point X="22.510697265625" Y="-2.466217529297" />
                  <Point X="22.4818671875" Y="-2.451998291016" />
                  <Point X="22.45225" Y="-2.443012207031" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.40202734375" Y="-3.014755126953" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="21.170294921875" Y="-3.126454101562" />
                  <Point X="20.917138671875" Y="-2.793860595703" />
                  <Point X="20.8460703125" Y="-2.674688476562" />
                  <Point X="20.693857421875" Y="-2.419450439453" />
                  <Point X="21.560072265625" Y="-1.754780029297" />
                  <Point X="21.894044921875" Y="-1.498513305664" />
                  <Point X="21.916931640625" Y="-1.475884399414" />
                  <Point X="21.936181640625" Y="-1.445054199219" />
                  <Point X="21.944873046875" Y="-1.422354980469" />
                  <Point X="21.94812109375" Y="-1.412201904297" />
                  <Point X="21.953849609375" Y="-1.39008190918" />
                  <Point X="21.957962890625" Y="-1.358598266602" />
                  <Point X="21.95726171875" Y="-1.335732666016" />
                  <Point X="21.9511171875" Y="-1.313697265625" />
                  <Point X="21.93911328125" Y="-1.284715820312" />
                  <Point X="21.9263515625" Y="-1.262768920898" />
                  <Point X="21.908232421875" Y="-1.244985595703" />
                  <Point X="21.888939453125" Y="-1.230559936523" />
                  <Point X="21.88023828125" Y="-1.224772094727" />
                  <Point X="21.860546875" Y="-1.213182006836" />
                  <Point X="21.83128515625" Y="-1.201957397461" />
                  <Point X="21.7993984375" Y="-1.195475097656" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="20.55769140625" Y="-1.353733642578" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.264935546875" Y="-1.380278076172" />
                  <Point X="20.165921875" Y="-0.992650695801" />
                  <Point X="20.14712109375" Y="-0.861186584473" />
                  <Point X="20.107578125" Y="-0.584698303223" />
                  <Point X="21.0866796875" Y="-0.322348449707" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.487416015625" Y="-0.212376022339" />
                  <Point X="21.5101015625" Y="-0.200216705322" />
                  <Point X="21.519390625" Y="-0.194530181885" />
                  <Point X="21.54002734375" Y="-0.180207092285" />
                  <Point X="21.563984375" Y="-0.15867729187" />
                  <Point X="21.584708984375" Y="-0.128735580444" />
                  <Point X="21.59450390625" Y="-0.106357574463" />
                  <Point X="21.598205078125" Y="-0.096424255371" />
                  <Point X="21.605083984375" Y="-0.074259979248" />
                  <Point X="21.61052734375" Y="-0.045519985199" />
                  <Point X="21.609654296875" Y="-0.012158127785" />
                  <Point X="21.605037109375" Y="0.010307911873" />
                  <Point X="21.602712890625" Y="0.019342475891" />
                  <Point X="21.595833984375" Y="0.041506755829" />
                  <Point X="21.582517578125" Y="0.07083265686" />
                  <Point X="21.55999609375" Y="0.099789558411" />
                  <Point X="21.541060546875" Y="0.11624621582" />
                  <Point X="21.53291015625" Y="0.122585899353" />
                  <Point X="21.5122734375" Y="0.136908981323" />
                  <Point X="21.498076171875" Y="0.145046920776" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="20.3638046875" Y="0.453482116699" />
                  <Point X="20.10818359375" Y="0.521975769043" />
                  <Point X="20.111701171875" Y="0.545749084473" />
                  <Point X="20.17551171875" Y="0.976966308594" />
                  <Point X="20.213365234375" Y="1.116656616211" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.961623046875" Y="1.335696289062" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255017578125" Y="1.299341796875" />
                  <Point X="21.276578125" Y="1.301228271484" />
                  <Point X="21.29686328125" Y="1.305263549805" />
                  <Point X="21.31261328125" Y="1.310229614258" />
                  <Point X="21.3582890625" Y="1.324631103516" />
                  <Point X="21.37722265625" Y="1.332961547852" />
                  <Point X="21.395966796875" Y="1.343783325195" />
                  <Point X="21.4126484375" Y="1.356014770508" />
                  <Point X="21.426287109375" Y="1.371567016602" />
                  <Point X="21.438701171875" Y="1.389296508789" />
                  <Point X="21.44865234375" Y="1.40743737793" />
                  <Point X="21.454970703125" Y="1.422694580078" />
                  <Point X="21.473298828125" Y="1.466941650391" />
                  <Point X="21.479083984375" Y="1.486788085938" />
                  <Point X="21.48284375" Y="1.508103637695" />
                  <Point X="21.484197265625" Y="1.528749755859" />
                  <Point X="21.481048828125" Y="1.54919934082" />
                  <Point X="21.4754453125" Y="1.570106445312" />
                  <Point X="21.467951171875" Y="1.589376342773" />
                  <Point X="21.460326171875" Y="1.604024658203" />
                  <Point X="21.4382109375" Y="1.646506103516" />
                  <Point X="21.426716796875" Y="1.663707763672" />
                  <Point X="21.4128046875" Y="1.680287231445" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="20.76499609375" Y="2.180206542969" />
                  <Point X="20.648140625" Y="2.269873046875" />
                  <Point X="20.670904296875" Y="2.308872070312" />
                  <Point X="20.918853515625" Y="2.733668212891" />
                  <Point X="21.019109375" Y="2.862534423828" />
                  <Point X="21.24949609375" Y="3.158662353516" />
                  <Point X="21.6258515625" Y="2.941372802734" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.875142578125" Y="2.823877197266" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.019537109375" Y="2.835653076172" />
                  <Point X="22.037791015625" Y="2.847281982422" />
                  <Point X="22.053921875" Y="2.860228027344" />
                  <Point X="22.0694921875" Y="2.875797851562" />
                  <Point X="22.114646484375" Y="2.920951416016" />
                  <Point X="22.127595703125" Y="2.937084960938" />
                  <Point X="22.139224609375" Y="2.955339111328" />
                  <Point X="22.14837109375" Y="2.973888183594" />
                  <Point X="22.1532890625" Y="2.993977050781" />
                  <Point X="22.156115234375" Y="3.015435546875" />
                  <Point X="22.15656640625" Y="3.036123535156" />
                  <Point X="22.154646484375" Y="3.058059082031" />
                  <Point X="22.14908203125" Y="3.121673095703" />
                  <Point X="22.145044921875" Y="3.141961425781" />
                  <Point X="22.13853515625" Y="3.162604248047" />
                  <Point X="22.130205078125" Y="3.181532714844" />
                  <Point X="21.84976171875" Y="3.6672734375" />
                  <Point X="21.81666796875" Y="3.724595947266" />
                  <Point X="21.8675390625" Y="3.763599609375" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.457283203125" Y="4.182414550781" />
                  <Point X="22.832962890625" Y="4.391134277344" />
                  <Point X="22.905529296875" Y="4.296562988281" />
                  <Point X="22.9568046875" Y="4.229740722656" />
                  <Point X="22.971109375" Y="4.214797851563" />
                  <Point X="22.987689453125" Y="4.200885742188" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.029302734375" Y="4.176685546875" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.22255078125" Y="4.134482910156" />
                  <Point X="23.247978515625" Y="4.145016113281" />
                  <Point X="23.321724609375" Y="4.1755625" />
                  <Point X="23.339857421875" Y="4.18551171875" />
                  <Point X="23.3575859375" Y="4.19792578125" />
                  <Point X="23.37313671875" Y="4.2115625" />
                  <Point X="23.3853671875" Y="4.2282421875" />
                  <Point X="23.396189453125" Y="4.246985839844" />
                  <Point X="23.404521484375" Y="4.265924316406" />
                  <Point X="23.412796875" Y="4.292174316406" />
                  <Point X="23.43680078125" Y="4.36830078125" />
                  <Point X="23.4408359375" Y="4.388583007812" />
                  <Point X="23.44272265625" Y="4.41014453125" />
                  <Point X="23.442271484375" Y="4.430826171875" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.491328125" Y="4.65307421875" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.24179296875" Y="4.832211914062" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.816546875" Y="4.471234863281" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183885742188" />
                  <Point X="25.067130859375" Y="4.194217773438" />
                  <Point X="25.094423828125" Y="4.208806640625" />
                  <Point X="25.11558203125" Y="4.23139453125" />
                  <Point X="25.13344140625" Y="4.25812109375" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.289912109375" Y="4.822594238281" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.355916015625" Y="4.882858886719" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.002421875" Y="4.793501464844" />
                  <Point X="26.4810234375" Y="4.677951660156" />
                  <Point X="26.583013671875" Y="4.640959472656" />
                  <Point X="26.894650390625" Y="4.527926757812" />
                  <Point X="26.99433203125" Y="4.48130859375" />
                  <Point X="27.294576171875" Y="4.34089453125" />
                  <Point X="27.390908203125" Y="4.284770996094" />
                  <Point X="27.68097265625" Y="4.115777832031" />
                  <Point X="27.771806640625" Y="4.05118359375" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.368509765625" Y="2.933753173828" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.539934326172" />
                  <Point X="27.133080078125" Y="2.516060546875" />
                  <Point X="27.12757421875" Y="2.495474609375" />
                  <Point X="27.111609375" Y="2.435774169922" />
                  <Point X="27.10840625" Y="2.409136230469" />
                  <Point X="27.10921484375" Y="2.372433105469" />
                  <Point X="27.109875" Y="2.363153076172" />
                  <Point X="27.116099609375" Y="2.311529052734" />
                  <Point X="27.12144140625" Y="2.289607421875" />
                  <Point X="27.12970703125" Y="2.267518310547" />
                  <Point X="27.140072265625" Y="2.247467529297" />
                  <Point X="27.151087890625" Y="2.231234863281" />
                  <Point X="27.18303125" Y="2.184158935547" />
                  <Point X="27.2012578125" Y="2.164162109375" />
                  <Point X="27.230791015625" Y="2.139847412109" />
                  <Point X="27.237833984375" Y="2.134577392578" />
                  <Point X="27.28491015625" Y="2.102634521484" />
                  <Point X="27.304953125" Y="2.092272216797" />
                  <Point X="27.327041015625" Y="2.084006347656" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.366765625" Y="2.076517089844" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.47320703125" Y="2.074171142578" />
                  <Point X="27.49379296875" Y="2.079676269531" />
                  <Point X="27.553494140625" Y="2.095640869141" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.698263671875" Y="2.750846679688" />
                  <Point X="28.967328125" Y="2.90619140625" />
                  <Point X="29.1232734375" Y="2.689463867188" />
                  <Point X="29.173908203125" Y="2.605787597656" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.52165625" Y="1.891643676758" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.2214296875" Y="1.660245605469" />
                  <Point X="28.203970703125" Y="1.64162512207" />
                  <Point X="28.18915625" Y="1.622296875" />
                  <Point X="28.146189453125" Y="1.566243774414" />
                  <Point X="28.13660546875" Y="1.550909423828" />
                  <Point X="28.1216328125" Y="1.517089599609" />
                  <Point X="28.11611328125" Y="1.49735559082" />
                  <Point X="28.100107421875" Y="1.440125" />
                  <Point X="28.09665234375" Y="1.417824462891" />
                  <Point X="28.0958359375" Y="1.39425378418" />
                  <Point X="28.097740234375" Y="1.371764526367" />
                  <Point X="28.102271484375" Y="1.349807739258" />
                  <Point X="28.11541015625" Y="1.286131713867" />
                  <Point X="28.1206796875" Y="1.268979248047" />
                  <Point X="28.136283203125" Y="1.235740600586" />
                  <Point X="28.14860546875" Y="1.217011474609" />
                  <Point X="28.18433984375" Y="1.162695068359" />
                  <Point X="28.198892578125" Y="1.145450805664" />
                  <Point X="28.21613671875" Y="1.129360717773" />
                  <Point X="28.23434765625" Y="1.116034057617" />
                  <Point X="28.252205078125" Y="1.105982177734" />
                  <Point X="28.303990234375" Y="1.076831665039" />
                  <Point X="28.3205234375" Y="1.069501098633" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.380263671875" Y="1.056247680664" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.54237109375" Y="1.185768310547" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.84594140625" Y="0.932788391113" />
                  <Point X="29.86189453125" Y="0.830322692871" />
                  <Point X="29.890865234375" Y="0.644238891602" />
                  <Point X="29.049923828125" Y="0.418908813477" />
                  <Point X="28.716580078125" Y="0.329589874268" />
                  <Point X="28.7047890625" Y="0.325585357666" />
                  <Point X="28.681548828125" Y="0.31506854248" />
                  <Point X="28.657828125" Y="0.301357940674" />
                  <Point X="28.5890390625" Y="0.261596130371" />
                  <Point X="28.5743125" Y="0.251096954346" />
                  <Point X="28.547533203125" Y="0.225577346802" />
                  <Point X="28.53330078125" Y="0.207442337036" />
                  <Point X="28.49202734375" Y="0.154849700928" />
                  <Point X="28.48030078125" Y="0.135567306519" />
                  <Point X="28.47052734375" Y="0.114103546143" />
                  <Point X="28.463681640625" Y="0.092604255676" />
                  <Point X="28.4589375" Y="0.067832977295" />
                  <Point X="28.4451796875" Y="-0.004006072998" />
                  <Point X="28.443484375" Y="-0.021875444412" />
                  <Point X="28.4451796875" Y="-0.058553920746" />
                  <Point X="28.449923828125" Y="-0.08332535553" />
                  <Point X="28.463681640625" Y="-0.155164398193" />
                  <Point X="28.47052734375" Y="-0.176663696289" />
                  <Point X="28.48030078125" Y="-0.198127456665" />
                  <Point X="28.49202734375" Y="-0.217409851074" />
                  <Point X="28.506259765625" Y="-0.235544708252" />
                  <Point X="28.547533203125" Y="-0.288137481689" />
                  <Point X="28.56" Y="-0.301236541748" />
                  <Point X="28.5890390625" Y="-0.324156280518" />
                  <Point X="28.612759765625" Y="-0.337866760254" />
                  <Point X="28.681548828125" Y="-0.377628540039" />
                  <Point X="28.692708984375" Y="-0.383137786865" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="29.683298828125" Y="-0.651181518555" />
                  <Point X="29.89147265625" Y="-0.706961791992" />
                  <Point X="29.855021484375" Y="-0.948746704102" />
                  <Point X="29.83458203125" Y="-1.038310668945" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="28.810966796875" Y="-1.054335571289" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.32810546875" Y="-1.015627380371" />
                  <Point X="28.193095703125" Y="-1.044972412109" />
                  <Point X="28.163974609375" Y="-1.056597167969" />
                  <Point X="28.1361484375" Y="-1.073489379883" />
                  <Point X="28.1123984375" Y="-1.093960449219" />
                  <Point X="28.084259765625" Y="-1.127802856445" />
                  <Point X="28.002654296875" Y="-1.225948486328" />
                  <Point X="27.987935546875" Y="-1.250329589844" />
                  <Point X="27.976591796875" Y="-1.277715209961" />
                  <Point X="27.969759765625" Y="-1.305365234375" />
                  <Point X="27.9657265625" Y="-1.349192626953" />
                  <Point X="27.95403125" Y="-1.476295776367" />
                  <Point X="27.95634765625" Y="-1.507562011719" />
                  <Point X="27.964078125" Y="-1.539182617188" />
                  <Point X="27.976451171875" Y="-1.567996337891" />
                  <Point X="28.00221484375" Y="-1.6080703125" />
                  <Point X="28.076931640625" Y="-1.724287109375" />
                  <Point X="28.0869375" Y="-1.737242431641" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="29.007751953125" Y="-2.449294921875" />
                  <Point X="29.213125" Y="-2.606882568359" />
                  <Point X="29.124806640625" Y="-2.749793457031" />
                  <Point X="29.082533203125" Y="-2.809858642578" />
                  <Point X="29.028982421875" Y="-2.885945556641" />
                  <Point X="28.14526171875" Y="-2.375728271484" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.6988203125" Y="-2.149818847656" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135176757812" />
                  <Point X="27.3988046875" Y="-2.159401611328" />
                  <Point X="27.26531640625" Y="-2.229655761719" />
                  <Point X="27.242384765625" Y="-2.246549072266" />
                  <Point X="27.22142578125" Y="-2.267508300781" />
                  <Point X="27.204533203125" Y="-2.290439208984" />
                  <Point X="27.18030859375" Y="-2.33646875" />
                  <Point X="27.110052734375" Y="-2.469957275391" />
                  <Point X="27.100228515625" Y="-2.499734863281" />
                  <Point X="27.095271484375" Y="-2.531908203125" />
                  <Point X="27.095677734375" Y="-2.563260498047" />
                  <Point X="27.105685546875" Y="-2.618667236328" />
                  <Point X="27.134705078125" Y="-2.779350830078" />
                  <Point X="27.13898828125" Y="-2.795139648438" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.7283125" Y="-3.824589599609" />
                  <Point X="27.86128515625" Y="-4.054905761719" />
                  <Point X="27.7818515625" Y="-4.111643554688" />
                  <Point X="27.73458203125" Y="-4.142240234375" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="27.02128515625" Y="-3.276660400391" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900556884766" />
                  <Point X="26.667279296875" Y="-2.865424560547" />
                  <Point X="26.50880078125" Y="-2.763538085938" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.3573359375" Y="-2.746616455078" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.05844921875" Y="-2.833832275391" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968860839844" />
                  <Point X="25.88725" Y="-2.996687255859" />
                  <Point X="25.875625" Y="-3.025810302734" />
                  <Point X="25.861828125" Y="-3.089293212891" />
                  <Point X="25.821810546875" Y="-3.273398193359" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.983115234375" Y="-4.564061523438" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="25.97569140625" Y="-4.870081054688" />
                  <Point X="25.9320078125" Y="-4.878016601562" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94156640625" Y="-4.752635253906" />
                  <Point X="23.872740234375" Y="-4.734926269531" />
                  <Point X="23.858755859375" Y="-4.731328125" />
                  <Point X="23.866861328125" Y="-4.669751953125" />
                  <Point X="23.8792265625" Y="-4.575837890625" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030761719" />
                  <Point X="23.8782421875" Y="-4.509324707031" />
                  <Point X="23.876666015625" Y="-4.497689941406" />
                  <Point X="23.861146484375" Y="-4.41966796875" />
                  <Point X="23.81613671875" Y="-4.1933984375" />
                  <Point X="23.811875" Y="-4.178469238281" />
                  <Point X="23.80097265625" Y="-4.149501464844" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.769423828125" Y="-4.094860351562" />
                  <Point X="23.749791015625" Y="-4.0709375" />
                  <Point X="23.738994140625" Y="-4.059780273438" />
                  <Point X="23.679185546875" Y="-4.007328613281" />
                  <Point X="23.505734375" Y="-3.855215332031" />
                  <Point X="23.493263671875" Y="-3.845965820312" />
                  <Point X="23.46698046875" Y="-3.829621582031" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.2838046875" Y="-3.790966796875" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.94632421875" Y="-3.795496582031" />
                  <Point X="22.9181328125" Y="-3.808270507812" />
                  <Point X="22.9045625" Y="-3.815812988281" />
                  <Point X="22.838419921875" Y="-3.860009033203" />
                  <Point X="22.64659765625" Y="-3.988181152344" />
                  <Point X="22.640318359375" Y="-3.992758789062" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629394531" />
                  <Point X="22.496796875" Y="-4.162479003906" />
                  <Point X="22.2524140625" Y="-4.011161865234" />
                  <Point X="22.15711328125" Y="-3.937784423828" />
                  <Point X="22.01913671875" Y="-3.831547119141" />
                  <Point X="22.4676328125" Y="-3.054728759766" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.710083740234" />
                  <Point X="22.67605078125" Y="-2.6811171875" />
                  <Point X="22.680314453125" Y="-2.666186523438" />
                  <Point X="22.6865859375" Y="-2.634657470703" />
                  <Point X="22.688361328125" Y="-2.619235107422" />
                  <Point X="22.689375" Y="-2.588297363281" />
                  <Point X="22.68533203125" Y="-2.557601074219" />
                  <Point X="22.67634375" Y="-2.527980224609" />
                  <Point X="22.67063671875" Y="-2.513540283203" />
                  <Point X="22.656416015625" Y="-2.484710205078" />
                  <Point X="22.648435546875" Y="-2.471396240234" />
                  <Point X="22.630412109375" Y="-2.446247070312" />
                  <Point X="22.620369140625" Y="-2.434411865234" />
                  <Point X="22.60302734375" Y="-2.417069580078" />
                  <Point X="22.591189453125" Y="-2.407024902344" />
                  <Point X="22.56603515625" Y="-2.388998535156" />
                  <Point X="22.55271875" Y="-2.381016845703" />
                  <Point X="22.523888671875" Y="-2.366797607422" />
                  <Point X="22.50944921875" Y="-2.361090576172" />
                  <Point X="22.47983203125" Y="-2.352104492188" />
                  <Point X="22.44914453125" Y="-2.348062988281" />
                  <Point X="22.41820703125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.35452734375" Y="-2.932482666016" />
                  <Point X="21.206912109375" Y="-3.017708496094" />
                  <Point X="20.995984375" Y="-2.740594482422" />
                  <Point X="20.9276640625" Y="-2.626030517578" />
                  <Point X="20.818734375" Y="-2.443373046875" />
                  <Point X="21.617904296875" Y="-1.83014855957" />
                  <Point X="21.951876953125" Y="-1.573881835938" />
                  <Point X="21.960837890625" Y="-1.566067749023" />
                  <Point X="21.983724609375" Y="-1.543438964844" />
                  <Point X="21.997513671875" Y="-1.526198852539" />
                  <Point X="22.016763671875" Y="-1.495368652344" />
                  <Point X="22.024900390625" Y="-1.479024169922" />
                  <Point X="22.033591796875" Y="-1.456324951172" />
                  <Point X="22.040087890625" Y="-1.436018798828" />
                  <Point X="22.04581640625" Y="-1.413898803711" />
                  <Point X="22.048048828125" Y="-1.402388916016" />
                  <Point X="22.052162109375" Y="-1.370905151367" />
                  <Point X="22.05291796875" Y="-1.355686523438" />
                  <Point X="22.052216796875" Y="-1.332820922852" />
                  <Point X="22.048771484375" Y="-1.310215454102" />
                  <Point X="22.042626953125" Y="-1.288180297852" />
                  <Point X="22.03888671875" Y="-1.27734387207" />
                  <Point X="22.0268828125" Y="-1.248362426758" />
                  <Point X="22.02123828125" Y="-1.236961547852" />
                  <Point X="22.0084765625" Y="-1.215014648438" />
                  <Point X="21.99289453125" Y="-1.194968505859" />
                  <Point X="21.974775390625" Y="-1.177184936523" />
                  <Point X="21.96512109375" Y="-1.16890222168" />
                  <Point X="21.945828125" Y="-1.1544765625" />
                  <Point X="21.92842578125" Y="-1.142900878906" />
                  <Point X="21.908734375" Y="-1.131310791016" />
                  <Point X="21.8945703125" Y="-1.124483886719" />
                  <Point X="21.86530859375" Y="-1.113259277344" />
                  <Point X="21.8502109375" Y="-1.108861572266" />
                  <Point X="21.81832421875" Y="-1.102379272461" />
                  <Point X="21.802705078125" Y="-1.100532714844" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="20.545291015625" Y="-1.259546386719" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.259236328125" Y="-0.974111328125" />
                  <Point X="20.2411640625" Y="-0.847737487793" />
                  <Point X="20.213548828125" Y="-0.654654541016" />
                  <Point X="21.111267578125" Y="-0.414111328125" />
                  <Point X="21.491712890625" Y="-0.312171203613" />
                  <Point X="21.502091796875" Y="-0.308739349365" />
                  <Point X="21.5223828125" Y="-0.30070703125" />
                  <Point X="21.532294921875" Y="-0.296106872559" />
                  <Point X="21.55498046875" Y="-0.283947601318" />
                  <Point X="21.57355859375" Y="-0.272574371338" />
                  <Point X="21.5941953125" Y="-0.258251281738" />
                  <Point X="21.60352734375" Y="-0.250866226196" />
                  <Point X="21.627484375" Y="-0.229336425781" />
                  <Point X="21.64209765625" Y="-0.212744766235" />
                  <Point X="21.662822265625" Y="-0.18280305481" />
                  <Point X="21.67173828125" Y="-0.166828186035" />
                  <Point X="21.681533203125" Y="-0.144450180054" />
                  <Point X="21.688935546875" Y="-0.124583473206" />
                  <Point X="21.695814453125" Y="-0.102419197083" />
                  <Point X="21.698423828125" Y="-0.091938766479" />
                  <Point X="21.7038671875" Y="-0.063198673248" />
                  <Point X="21.705494140625" Y="-0.043034713745" />
                  <Point X="21.70462109375" Y="-0.009672898293" />
                  <Point X="21.702708984375" Y="0.006966469288" />
                  <Point X="21.698091796875" Y="0.029432447433" />
                  <Point X="21.693443359375" Y="0.047501716614" />
                  <Point X="21.686564453125" Y="0.069665985107" />
                  <Point X="21.682333984375" Y="0.080784912109" />
                  <Point X="21.669017578125" Y="0.110110870361" />
                  <Point X="21.657505859375" Y="0.129156143188" />
                  <Point X="21.634984375" Y="0.158113082886" />
                  <Point X="21.622314453125" Y="0.171494033813" />
                  <Point X="21.60337890625" Y="0.187950744629" />
                  <Point X="21.587078125" Y="0.200630203247" />
                  <Point X="21.56644140625" Y="0.214953292847" />
                  <Point X="21.559517578125" Y="0.219328887939" />
                  <Point X="21.534384765625" Y="0.23283454895" />
                  <Point X="21.50343359375" Y="0.245635879517" />
                  <Point X="21.491712890625" Y="0.249611236572" />
                  <Point X="20.388392578125" Y="0.545245117188" />
                  <Point X="20.214552734375" Y="0.591825195312" />
                  <Point X="20.26866796875" Y="0.957517089844" />
                  <Point X="20.30505859375" Y="1.091809448242" />
                  <Point X="20.3664140625" Y="1.318236938477" />
                  <Point X="20.94922265625" Y="1.241509155273" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.2529453125" Y="1.204364379883" />
                  <Point X="21.263298828125" Y="1.204703369141" />
                  <Point X="21.284859375" Y="1.20658984375" />
                  <Point X="21.29511328125" Y="1.208053955078" />
                  <Point X="21.3153984375" Y="1.212089233398" />
                  <Point X="21.341181640625" Y="1.219626708984" />
                  <Point X="21.386857421875" Y="1.234028198242" />
                  <Point X="21.396548828125" Y="1.23767565918" />
                  <Point X="21.415482421875" Y="1.246006103516" />
                  <Point X="21.42472265625" Y="1.250688842773" />
                  <Point X="21.443466796875" Y="1.261510620117" />
                  <Point X="21.452140625" Y="1.267171020508" />
                  <Point X="21.468822265625" Y="1.27940246582" />
                  <Point X="21.48407421875" Y="1.293377685547" />
                  <Point X="21.497712890625" Y="1.308929931641" />
                  <Point X="21.504107421875" Y="1.31707800293" />
                  <Point X="21.516521484375" Y="1.334807495117" />
                  <Point X="21.5219921875" Y="1.343606933594" />
                  <Point X="21.531943359375" Y="1.361747802734" />
                  <Point X="21.5427421875" Y="1.386346313477" />
                  <Point X="21.5610703125" Y="1.430593505859" />
                  <Point X="21.56450390625" Y="1.440355957031" />
                  <Point X="21.5702890625" Y="1.460202392578" />
                  <Point X="21.572640625" Y="1.470286132812" />
                  <Point X="21.576400390625" Y="1.49160168457" />
                  <Point X="21.577640625" Y="1.501889038086" />
                  <Point X="21.578994140625" Y="1.52253515625" />
                  <Point X="21.578091796875" Y="1.543205688477" />
                  <Point X="21.574943359375" Y="1.563655151367" />
                  <Point X="21.572810546875" Y="1.573793212891" />
                  <Point X="21.56720703125" Y="1.594700317383" />
                  <Point X="21.563984375" Y="1.604540039062" />
                  <Point X="21.556490234375" Y="1.623810058594" />
                  <Point X="21.54459375" Y="1.647888793945" />
                  <Point X="21.522478515625" Y="1.690370239258" />
                  <Point X="21.51719921875" Y="1.699286376953" />
                  <Point X="21.505705078125" Y="1.716488037109" />
                  <Point X="21.499490234375" Y="1.72477331543" />
                  <Point X="21.485578125" Y="1.741352783203" />
                  <Point X="21.478498046875" Y="1.748912841797" />
                  <Point X="21.463556640625" Y="1.763215698242" />
                  <Point X="21.4556953125" Y="1.769958618164" />
                  <Point X="20.822828125" Y="2.255575195312" />
                  <Point X="20.77238671875" Y="2.294280029297" />
                  <Point X="20.99771875" Y="2.680328125" />
                  <Point X="21.09408984375" Y="2.804200439453" />
                  <Point X="21.273662109375" Y="3.035013427734" />
                  <Point X="21.5783515625" Y="2.859100341797" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.86686328125" Y="2.729238769531" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.02356640625" Y="2.734227294922" />
                  <Point X="22.04300390625" Y="2.741302001953" />
                  <Point X="22.0615546875" Y="2.750450439453" />
                  <Point X="22.070580078125" Y="2.755530761719" />
                  <Point X="22.088833984375" Y="2.767159667969" />
                  <Point X="22.097251953125" Y="2.773192138672" />
                  <Point X="22.1133828125" Y="2.786138183594" />
                  <Point X="22.121095703125" Y="2.793051757812" />
                  <Point X="22.136666015625" Y="2.808621582031" />
                  <Point X="22.1818203125" Y="2.853775146484" />
                  <Point X="22.188734375" Y="2.861486816406" />
                  <Point X="22.20168359375" Y="2.877620361328" />
                  <Point X="22.20771875" Y="2.886042236328" />
                  <Point X="22.21934765625" Y="2.904296386719" />
                  <Point X="22.2244296875" Y="2.913324951172" />
                  <Point X="22.233576171875" Y="2.931874023438" />
                  <Point X="22.240646484375" Y="2.951298339844" />
                  <Point X="22.245564453125" Y="2.971387207031" />
                  <Point X="22.2474765625" Y="2.981572265625" />
                  <Point X="22.250302734375" Y="3.003030761719" />
                  <Point X="22.251091796875" Y="3.013364257812" />
                  <Point X="22.25154296875" Y="3.034052246094" />
                  <Point X="22.251205078125" Y="3.044406738281" />
                  <Point X="22.24928515625" Y="3.066342285156" />
                  <Point X="22.243720703125" Y="3.129956298828" />
                  <Point X="22.242255859375" Y="3.140213378906" />
                  <Point X="22.23821875" Y="3.160501708984" />
                  <Point X="22.235646484375" Y="3.170532958984" />
                  <Point X="22.22913671875" Y="3.19117578125" />
                  <Point X="22.22548828125" Y="3.200870361328" />
                  <Point X="22.217158203125" Y="3.219798828125" />
                  <Point X="22.2124765625" Y="3.229032714844" />
                  <Point X="21.940611328125" Y="3.699915771484" />
                  <Point X="22.351630859375" Y="4.015039306641" />
                  <Point X="22.503419921875" Y="4.099370605469" />
                  <Point X="22.807474609375" Y="4.268296386719" />
                  <Point X="22.83016015625" Y="4.238730957031" />
                  <Point X="22.881435546875" Y="4.171908691406" />
                  <Point X="22.8881796875" Y="4.164046875" />
                  <Point X="22.902484375" Y="4.149104003906" />
                  <Point X="22.910044921875" Y="4.142022949219" />
                  <Point X="22.926625" Y="4.128110839844" />
                  <Point X="22.9349140625" Y="4.121894042969" />
                  <Point X="22.95211328125" Y="4.110402832031" />
                  <Point X="22.9610234375" Y="4.105128417969" />
                  <Point X="22.9854375" Y="4.092419189453" />
                  <Point X="23.056240234375" Y="4.055561279297" />
                  <Point X="23.06567578125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.043789550781" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.229271484375" Y="4.037488525391" />
                  <Point X="23.249134765625" Y="4.043278076172" />
                  <Point X="23.258908203125" Y="4.046715087891" />
                  <Point X="23.2843359375" Y="4.057248291016" />
                  <Point X="23.35808203125" Y="4.087794677734" />
                  <Point X="23.367421875" Y="4.092275878906" />
                  <Point X="23.3855546875" Y="4.102225097656" />
                  <Point X="23.39434765625" Y="4.107693359375" />
                  <Point X="23.412076171875" Y="4.120107421875" />
                  <Point X="23.420220703125" Y="4.126499023438" />
                  <Point X="23.435771484375" Y="4.140135742188" />
                  <Point X="23.449748046875" Y="4.15538671875" />
                  <Point X="23.461978515625" Y="4.17206640625" />
                  <Point X="23.467638671875" Y="4.180740234375" />
                  <Point X="23.4784609375" Y="4.199483886719" />
                  <Point X="23.483146484375" Y="4.208729003906" />
                  <Point X="23.491478515625" Y="4.227667480469" />
                  <Point X="23.495125" Y="4.237360839844" />
                  <Point X="23.503400390625" Y="4.263610839844" />
                  <Point X="23.527404296875" Y="4.339737304688" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370045898437" />
                  <Point X="23.535474609375" Y="4.380301757812" />
                  <Point X="23.537361328125" Y="4.40186328125" />
                  <Point X="23.53769921875" Y="4.412216308594" />
                  <Point X="23.537248046875" Y="4.432897949219" />
                  <Point X="23.536458984375" Y="4.4432265625" />
                  <Point X="23.520734375" Y="4.562655761719" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.2528359375" Y="4.737855957031" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.724783203125" Y="4.446646972656" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.021630859375" Y="4.085113037109" />
                  <Point X="25.05216796875" Y="4.090154541016" />
                  <Point X="25.06723046875" Y="4.093927246094" />
                  <Point X="25.09766796875" Y="4.104259277344" />
                  <Point X="25.1119140625" Y="4.110436035156" />
                  <Point X="25.13920703125" Y="4.125024902344" />
                  <Point X="25.1637578125" Y="4.143861328125" />
                  <Point X="25.184916015625" Y="4.16644921875" />
                  <Point X="25.1945703125" Y="4.178612792969" />
                  <Point X="25.2124296875" Y="4.205339355469" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.24710546875" />
                  <Point X="25.23798046875" Y="4.261727050781" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="25.980126953125" Y="4.701154785156" />
                  <Point X="26.453591796875" Y="4.586844726562" />
                  <Point X="26.55062109375" Y="4.55165234375" />
                  <Point X="26.858265625" Y="4.440067871094" />
                  <Point X="26.954087890625" Y="4.395254394531" />
                  <Point X="27.250453125" Y="4.256654296875" />
                  <Point X="27.3430859375" Y="4.202686035156" />
                  <Point X="27.629427734375" Y="4.035861328125" />
                  <Point X="27.716751953125" Y="3.973763427734" />
                  <Point X="27.817783203125" Y="3.901915771484" />
                  <Point X="27.28623828125" Y="2.981253173828" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.062376953125" Y="2.593106933594" />
                  <Point X="27.0531875" Y="2.573445556641" />
                  <Point X="27.0441875" Y="2.549571777344" />
                  <Point X="27.041306640625" Y="2.540606201172" />
                  <Point X="27.03580078125" Y="2.520020263672" />
                  <Point X="27.0198359375" Y="2.460319824219" />
                  <Point X="27.0172890625" Y="2.447115966797" />
                  <Point X="27.0140859375" Y="2.420478027344" />
                  <Point X="27.0134296875" Y="2.407043945312" />
                  <Point X="27.01423828125" Y="2.370340820313" />
                  <Point X="27.01555859375" Y="2.351780761719" />
                  <Point X="27.021783203125" Y="2.300156738281" />
                  <Point X="27.02380078125" Y="2.289037841797" />
                  <Point X="27.029142578125" Y="2.267116210938" />
                  <Point X="27.032466796875" Y="2.256313476562" />
                  <Point X="27.040732421875" Y="2.234224365234" />
                  <Point X="27.04531640625" Y="2.223892578125" />
                  <Point X="27.055681640625" Y="2.203841796875" />
                  <Point X="27.072478515625" Y="2.177890136719" />
                  <Point X="27.104421875" Y="2.130814208984" />
                  <Point X="27.1128203125" Y="2.120163574219" />
                  <Point X="27.131046875" Y="2.100166748047" />
                  <Point X="27.140875" Y="2.090820556641" />
                  <Point X="27.170408203125" Y="2.066505859375" />
                  <Point X="27.184494140625" Y="2.055965820313" />
                  <Point X="27.2315703125" Y="2.024023071289" />
                  <Point X="27.24128125" Y="2.018245605469" />
                  <Point X="27.26132421875" Y="2.007883422852" />
                  <Point X="27.27165625" Y="2.003298339844" />
                  <Point X="27.293744140625" Y="1.995032470703" />
                  <Point X="27.304548828125" Y="1.991707519531" />
                  <Point X="27.32647265625" Y="1.986364746094" />
                  <Point X="27.355392578125" Y="1.982200317383" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.416044921875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975497314453" />
                  <Point X="27.484314453125" Y="1.979822753906" />
                  <Point X="27.49775" Y="1.982395996094" />
                  <Point X="27.5183359375" Y="1.987901245117" />
                  <Point X="27.578037109375" Y="2.003865844727" />
                  <Point X="27.584" Y="2.005671875" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.745763671875" Y="2.66857421875" />
                  <Point X="28.94040625" Y="2.780951416016" />
                  <Point X="29.043958984375" Y="2.637037597656" />
                  <Point X="29.092630859375" Y="2.556604492188" />
                  <Point X="29.13688671875" Y="2.483471679688" />
                  <Point X="28.46382421875" Y="1.967012207031" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.168142578125" Y="1.739871337891" />
                  <Point X="28.152126953125" Y="1.725224487305" />
                  <Point X="28.13466796875" Y="1.706604125977" />
                  <Point X="28.1285703125" Y="1.699416625977" />
                  <Point X="28.113755859375" Y="1.680088378906" />
                  <Point X="28.0707890625" Y="1.62403527832" />
                  <Point X="28.06562890625" Y="1.61659375" />
                  <Point X="28.04973828125" Y="1.58936730957" />
                  <Point X="28.034765625" Y="1.555547485352" />
                  <Point X="28.03014453125" Y="1.542678710938" />
                  <Point X="28.024625" Y="1.522944702148" />
                  <Point X="28.008619140625" Y="1.465714111328" />
                  <Point X="28.0062265625" Y="1.454670043945" />
                  <Point X="28.002771484375" Y="1.432369506836" />
                  <Point X="28.001708984375" Y="1.421112915039" />
                  <Point X="28.000892578125" Y="1.397542236328" />
                  <Point X="28.001173828125" Y="1.38623828125" />
                  <Point X="28.003078125" Y="1.363749023438" />
                  <Point X="28.004701171875" Y="1.352563842773" />
                  <Point X="28.009232421875" Y="1.330607055664" />
                  <Point X="28.02237109375" Y="1.266931030273" />
                  <Point X="28.024599609375" Y="1.258233032227" />
                  <Point X="28.03468359375" Y="1.22860949707" />
                  <Point X="28.050287109375" Y="1.19537097168" />
                  <Point X="28.056919921875" Y="1.183525634766" />
                  <Point X="28.0692421875" Y="1.164796508789" />
                  <Point X="28.1049765625" Y="1.110480102539" />
                  <Point X="28.11173828125" Y="1.101425292969" />
                  <Point X="28.126291015625" Y="1.084181030273" />
                  <Point X="28.13408203125" Y="1.075991577148" />
                  <Point X="28.151326171875" Y="1.059901489258" />
                  <Point X="28.160033203125" Y="1.052696166992" />
                  <Point X="28.178244140625" Y="1.039369384766" />
                  <Point X="28.187748046875" Y="1.033248413086" />
                  <Point X="28.20560546875" Y="1.023196533203" />
                  <Point X="28.257390625" Y="0.994045959473" />
                  <Point X="28.265484375" Y="0.989985473633" />
                  <Point X="28.294681640625" Y="0.978083557129" />
                  <Point X="28.33027734375" Y="0.968021240234" />
                  <Point X="28.343671875" Y="0.965257751465" />
                  <Point X="28.36781640625" Y="0.962066650391" />
                  <Point X="28.437833984375" Y="0.952812927246" />
                  <Point X="28.444033203125" Y="0.952199707031" />
                  <Point X="28.46571875" Y="0.951222839355" />
                  <Point X="28.49121875" Y="0.952032348633" />
                  <Point X="28.50060546875" Y="0.952797180176" />
                  <Point X="29.554771484375" Y="1.091581054688" />
                  <Point X="29.704703125" Y="1.111319824219" />
                  <Point X="29.752689453125" Y="0.914207519531" />
                  <Point X="29.768025390625" Y="0.815708068848" />
                  <Point X="29.783873046875" Y="0.713921386719" />
                  <Point X="29.0253359375" Y="0.510671722412" />
                  <Point X="28.6919921875" Y="0.421352813721" />
                  <Point X="28.686029296875" Y="0.419543640137" />
                  <Point X="28.665623046875" Y="0.41213583374" />
                  <Point X="28.6423828125" Y="0.401619110107" />
                  <Point X="28.6340078125" Y="0.397317687988" />
                  <Point X="28.610287109375" Y="0.383607208252" />
                  <Point X="28.541498046875" Y="0.343845397949" />
                  <Point X="28.533890625" Y="0.338949920654" />
                  <Point X="28.5087734375" Y="0.319870178223" />
                  <Point X="28.481994140625" Y="0.294350585938" />
                  <Point X="28.47280078125" Y="0.28422833252" />
                  <Point X="28.458568359375" Y="0.266093231201" />
                  <Point X="28.417294921875" Y="0.213500656128" />
                  <Point X="28.410859375" Y="0.204212310791" />
                  <Point X="28.3991328125" Y="0.184929840088" />
                  <Point X="28.393841796875" Y="0.17493598938" />
                  <Point X="28.384068359375" Y="0.153472183228" />
                  <Point X="28.380005859375" Y="0.142927093506" />
                  <Point X="28.37316015625" Y="0.121427757263" />
                  <Point X="28.370376953125" Y="0.110473655701" />
                  <Point X="28.3656328125" Y="0.085702392578" />
                  <Point X="28.351875" Y="0.013863312721" />
                  <Point X="28.350603515625" Y="0.004966600418" />
                  <Point X="28.3485859375" Y="-0.026261734009" />
                  <Point X="28.35028125" Y="-0.062940216064" />
                  <Point X="28.351875" Y="-0.076423278809" />
                  <Point X="28.356619140625" Y="-0.101194694519" />
                  <Point X="28.370376953125" Y="-0.173033782959" />
                  <Point X="28.37316015625" Y="-0.183987869263" />
                  <Point X="28.380005859375" Y="-0.205487213135" />
                  <Point X="28.384068359375" Y="-0.216032302856" />
                  <Point X="28.393841796875" Y="-0.237496109009" />
                  <Point X="28.3991328125" Y="-0.247489959717" />
                  <Point X="28.410859375" Y="-0.26677243042" />
                  <Point X="28.417294921875" Y="-0.276061065674" />
                  <Point X="28.43152734375" Y="-0.294196014404" />
                  <Point X="28.47280078125" Y="-0.346788757324" />
                  <Point X="28.47871875" Y="-0.353631347656" />
                  <Point X="28.501142578125" Y="-0.375807647705" />
                  <Point X="28.530181640625" Y="-0.398727508545" />
                  <Point X="28.5415" Y="-0.406405670166" />
                  <Point X="28.565220703125" Y="-0.420116119385" />
                  <Point X="28.634009765625" Y="-0.459877929688" />
                  <Point X="28.63949609375" Y="-0.462814300537" />
                  <Point X="28.659154296875" Y="-0.472014923096" />
                  <Point X="28.683025390625" Y="-0.481026977539" />
                  <Point X="28.6919921875" Y="-0.483912780762" />
                  <Point X="29.6587109375" Y="-0.742944396973" />
                  <Point X="29.78487890625" Y="-0.776751220703" />
                  <Point X="29.761615234375" Y="-0.931056762695" />
                  <Point X="29.741962890625" Y="-1.017174072266" />
                  <Point X="29.7278046875" Y="-1.079219848633" />
                  <Point X="28.8233671875" Y="-0.960148376465" />
                  <Point X="28.436783203125" Y="-0.909253662109" />
                  <Point X="28.428625" Y="-0.908535705566" />
                  <Point X="28.40009765625" Y="-0.908042419434" />
                  <Point X="28.36672265625" Y="-0.910840942383" />
                  <Point X="28.354482421875" Y="-0.912676147461" />
                  <Point X="28.307927734375" Y="-0.92279486084" />
                  <Point X="28.17291796875" Y="-0.952139831543" />
                  <Point X="28.157875" Y="-0.956742370605" />
                  <Point X="28.12875390625" Y="-0.968367248535" />
                  <Point X="28.11467578125" Y="-0.975389526367" />
                  <Point X="28.086849609375" Y="-0.992281677246" />
                  <Point X="28.074125" Y="-1.001530761719" />
                  <Point X="28.050375" Y="-1.022001953125" />
                  <Point X="28.039349609375" Y="-1.033223754883" />
                  <Point X="28.0112109375" Y="-1.067066040039" />
                  <Point X="27.92960546875" Y="-1.165211669922" />
                  <Point X="27.921326171875" Y="-1.176850585938" />
                  <Point X="27.906607421875" Y="-1.201231689453" />
                  <Point X="27.90016796875" Y="-1.213973999023" />
                  <Point X="27.88882421875" Y="-1.241359619141" />
                  <Point X="27.884365234375" Y="-1.254927124023" />
                  <Point X="27.877533203125" Y="-1.282577026367" />
                  <Point X="27.87516015625" Y="-1.296659667969" />
                  <Point X="27.871126953125" Y="-1.340487060547" />
                  <Point X="27.859431640625" Y="-1.467590209961" />
                  <Point X="27.859291015625" Y="-1.483314697266" />
                  <Point X="27.861607421875" Y="-1.514580932617" />
                  <Point X="27.864064453125" Y="-1.530122802734" />
                  <Point X="27.871794921875" Y="-1.561743530273" />
                  <Point X="27.87678515625" Y="-1.576667114258" />
                  <Point X="27.889158203125" Y="-1.605480834961" />
                  <Point X="27.896541015625" Y="-1.619370849609" />
                  <Point X="27.9223046875" Y="-1.659444824219" />
                  <Point X="27.997021484375" Y="-1.775661743164" />
                  <Point X="28.00174609375" Y="-1.782356201172" />
                  <Point X="28.01980078125" Y="-1.804455322266" />
                  <Point X="28.043494140625" Y="-1.828122070312" />
                  <Point X="28.052798828125" Y="-1.836277709961" />
                  <Point X="28.949919921875" Y="-2.524663574219" />
                  <Point X="29.087173828125" Y="-2.629981445312" />
                  <Point X="29.045486328125" Y="-2.697437744141" />
                  <Point X="29.004845703125" Y="-2.755182128906" />
                  <Point X="29.00127734375" Y="-2.760252929688" />
                  <Point X="28.19276171875" Y="-2.293455810547" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.715705078125" Y="-2.056331298828" />
                  <Point X="27.555021484375" Y="-2.027312011719" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.035136352539" />
                  <Point X="27.415068359375" Y="-2.044960083008" />
                  <Point X="27.40058984375" Y="-2.051108642578" />
                  <Point X="27.354560546875" Y="-2.075333496094" />
                  <Point X="27.221072265625" Y="-2.145587646484" />
                  <Point X="27.208970703125" Y="-2.153169677734" />
                  <Point X="27.1860390625" Y="-2.170062988281" />
                  <Point X="27.175208984375" Y="-2.179374267578" />
                  <Point X="27.15425" Y="-2.200333496094" />
                  <Point X="27.144939453125" Y="-2.211162841797" />
                  <Point X="27.128046875" Y="-2.23409375" />
                  <Point X="27.12046484375" Y="-2.2461953125" />
                  <Point X="27.096240234375" Y="-2.292224853516" />
                  <Point X="27.025984375" Y="-2.425713378906" />
                  <Point X="27.0198359375" Y="-2.440192871094" />
                  <Point X="27.01001171875" Y="-2.469970458984" />
                  <Point X="27.0063359375" Y="-2.485268554688" />
                  <Point X="27.00137890625" Y="-2.517441894531" />
                  <Point X="27.000279296875" Y="-2.533139160156" />
                  <Point X="27.000685546875" Y="-2.564491455078" />
                  <Point X="27.00219140625" Y="-2.580146484375" />
                  <Point X="27.01219921875" Y="-2.635553222656" />
                  <Point X="27.04121875" Y="-2.796236816406" />
                  <Point X="27.04301953125" Y="-2.804223632812" />
                  <Point X="27.05123828125" Y="-2.831539794922" />
                  <Point X="27.064072265625" Y="-2.862478759766" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.646041015625" Y="-3.872089599609" />
                  <Point X="27.735896484375" Y="-4.027723632812" />
                  <Point X="27.7237578125" Y="-4.036082763672" />
                  <Point X="27.096654296875" Y="-3.218828125" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849626220703" />
                  <Point X="26.78325390625" Y="-2.828004882812" />
                  <Point X="26.77330078125" Y="-2.820646484375" />
                  <Point X="26.718654296875" Y="-2.785514160156" />
                  <Point X="26.56017578125" Y="-2.683627685547" />
                  <Point X="26.546283203125" Y="-2.676244628906" />
                  <Point X="26.517470703125" Y="-2.663873291016" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.348630859375" Y="-2.652016113281" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.997712890625" Y="-2.760784423828" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883087158203" />
                  <Point X="25.83218359375" Y="-2.906836425781" />
                  <Point X="25.822935546875" Y="-2.919562255859" />
                  <Point X="25.80604296875" Y="-2.947388671875" />
                  <Point X="25.79901953125" Y="-2.961468505859" />
                  <Point X="25.78739453125" Y="-2.990591552734" />
                  <Point X="25.78279296875" Y="-3.005634765625" />
                  <Point X="25.76899609375" Y="-3.069117675781" />
                  <Point X="25.728978515625" Y="-3.25322265625" />
                  <Point X="25.7275859375" Y="-3.261287597656" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520019531" />
                  <Point X="25.833087890625" Y="-4.152318847656" />
                  <Point X="25.757779296875" Y="-3.871260498047" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480121337891" />
                  <Point X="25.642146484375" Y="-3.453579345703" />
                  <Point X="25.6267890625" Y="-3.423815917969" />
                  <Point X="25.62041015625" Y="-3.413210449219" />
                  <Point X="25.5784296875" Y="-3.352724121094" />
                  <Point X="25.456681640625" Y="-3.177310058594" />
                  <Point X="25.446671875" Y="-3.165173339844" />
                  <Point X="25.424787109375" Y="-3.142716796875" />
                  <Point X="25.412912109375" Y="-3.132396972656" />
                  <Point X="25.38665625" Y="-3.113153320312" />
                  <Point X="25.3732421875" Y="-3.104936523438" />
                  <Point X="25.3452421875" Y="-3.090829101562" />
                  <Point X="25.33065625" Y="-3.084938476562" />
                  <Point X="25.265693359375" Y="-3.064776367188" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.063376953125" Y="-3.003109130859" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.998840087891" />
                  <Point X="24.9489375" Y="-3.003109375" />
                  <Point X="24.93501953125" Y="-3.006305175781" />
                  <Point X="24.870056640625" Y="-3.026467041016" />
                  <Point X="24.68166015625" Y="-3.084938476562" />
                  <Point X="24.66707421875" Y="-3.090828857422" />
                  <Point X="24.639072265625" Y="-3.104937011719" />
                  <Point X="24.62565625" Y="-3.113154785156" />
                  <Point X="24.599400390625" Y="-3.132398925781" />
                  <Point X="24.587525390625" Y="-3.142716552734" />
                  <Point X="24.565640625" Y="-3.165172119141" />
                  <Point X="24.555630859375" Y="-3.177310058594" />
                  <Point X="24.513650390625" Y="-3.237796142578" />
                  <Point X="24.391904296875" Y="-3.413210449219" />
                  <Point X="24.38753125" Y="-3.420131835938" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213623047" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.059375" Y="-4.599602539062" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.92936022028" Y="-2.718731523032" />
                  <Point X="29.010968044749" Y="-2.571507110487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.84707529185" Y="-2.671224256194" />
                  <Point X="28.934762298003" Y="-2.513032709577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.76479036342" Y="-2.623716989356" />
                  <Point X="28.858556698791" Y="-2.45455804251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.628188702619" Y="-1.066105153619" />
                  <Point X="29.783496108343" Y="-0.785923176946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.682505434991" Y="-2.576209722518" />
                  <Point X="28.782351099578" Y="-2.396083375444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.526957530519" Y="-1.052777815145" />
                  <Point X="29.693529482899" Y="-0.752274058364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.600220506561" Y="-2.52870245568" />
                  <Point X="28.706145500366" Y="-2.337608708378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.425726358419" Y="-1.039450476671" />
                  <Point X="29.59895736466" Y="-0.726933468719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.684445812621" Y="-3.984850529351" />
                  <Point X="27.697522933113" Y="-3.961258779483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.517935578131" Y="-2.481195188841" />
                  <Point X="28.629939901154" Y="-2.279134041312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.324495186319" Y="-1.026123138197" />
                  <Point X="29.504385219207" Y="-0.701592928169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.621382973257" Y="-3.902665695883" />
                  <Point X="27.642107814867" Y="-3.865277091897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.435650649701" Y="-2.433687922003" />
                  <Point X="28.553734301941" Y="-2.220659374245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.223264014219" Y="-1.012795799723" />
                  <Point X="29.409813073755" Y="-0.676252387619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.558320133892" Y="-3.820480862415" />
                  <Point X="27.586692745091" Y="-3.769295316871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.353365721271" Y="-2.386180655165" />
                  <Point X="28.477528702729" Y="-2.162184707179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.122032842119" Y="-0.999468461249" />
                  <Point X="29.315240928303" Y="-0.650911847069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.495257294527" Y="-3.738296028948" />
                  <Point X="27.531277675314" Y="-3.673313541846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.271080792842" Y="-2.338673388327" />
                  <Point X="28.401323103516" Y="-2.103710040113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.020801670019" Y="-0.986141122774" />
                  <Point X="29.22066878285" Y="-0.625571306519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.432194455163" Y="-3.65611119548" />
                  <Point X="27.475862605537" Y="-3.57733176682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.188795861761" Y="-2.291166126271" />
                  <Point X="28.325117504304" Y="-2.045235373047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.919570497919" Y="-0.9728137843" />
                  <Point X="29.126096637398" Y="-0.600230765968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.369131615798" Y="-3.573926362012" />
                  <Point X="27.42044753576" Y="-3.481349991794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.106510878332" Y="-2.243658958653" />
                  <Point X="28.248911905091" Y="-1.98676070598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.818339325552" Y="-0.959486446307" />
                  <Point X="29.031524491946" Y="-0.574890225418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.739304861991" Y="0.701979362387" />
                  <Point X="29.775551399886" Y="0.767369847712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.306068776433" Y="-3.491741528545" />
                  <Point X="27.365032465984" Y="-3.385368216769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.024225894903" Y="-2.196151791036" />
                  <Point X="28.172706305879" Y="-1.928286038914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.71710814808" Y="-0.946159117526" />
                  <Point X="28.936952346494" Y="-0.549549684868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.611739316743" Y="0.667798234097" />
                  <Point X="29.751357947868" Y="0.919676912172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.243005937068" Y="-3.409556695077" />
                  <Point X="27.309617396207" Y="-3.289386441743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.941940911475" Y="-2.148644623419" />
                  <Point X="28.096500706666" Y="-1.869811371848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.615876970607" Y="-0.932831788745" />
                  <Point X="28.842380201041" Y="-0.524209144318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.484173771495" Y="0.633617105806" />
                  <Point X="29.718211406503" Y="1.055832175892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.179943097704" Y="-3.327371861609" />
                  <Point X="27.25420232643" Y="-3.193404666718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.859655928046" Y="-2.101137455801" />
                  <Point X="28.022574016961" Y="-1.807225443201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.514645793134" Y="-0.919504459964" />
                  <Point X="28.747808055589" Y="-0.498868603767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.356608226246" Y="0.599435977515" />
                  <Point X="29.635284173547" Y="1.102180694692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.116880258339" Y="-3.245187028142" />
                  <Point X="27.198787256653" Y="-3.097422891692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.770398500629" Y="-2.06620891011" />
                  <Point X="27.961810226575" Y="-1.720893015584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.412264055257" Y="-0.908252797097" />
                  <Point X="28.655121022288" Y="-0.470127230873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.229042680998" Y="0.565254849225" />
                  <Point X="29.518115005741" Y="1.086755127789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.053817508322" Y="-3.163002033487" />
                  <Point X="27.143372186876" Y="-3.001441116666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.671663964921" Y="-2.048377520357" />
                  <Point X="27.903481864708" Y="-1.630166958614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.293893790211" Y="-0.92584520078" />
                  <Point X="28.572039290818" Y="-0.42405743477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.101477135749" Y="0.531073720934" />
                  <Point X="29.400945791037" Y="1.071329476279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.990754800491" Y="-3.080816962726" />
                  <Point X="27.0879571171" Y="-2.905459341641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.572929410562" Y="-2.030546164251" />
                  <Point X="27.861292184947" Y="-1.510325948419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.170246778424" Y="-0.952957107576" />
                  <Point X="28.494052603178" Y="-0.368795936284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.973911630018" Y="0.496892663934" />
                  <Point X="29.283776576333" Y="1.05590382477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.92769209266" Y="-2.998631891964" />
                  <Point X="27.040890130963" Y="-2.794417225063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.464145662704" Y="-2.03084403312" />
                  <Point X="27.876928863823" Y="-1.286163425727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.972708607136" Y="-1.113372194803" />
                  <Point X="28.428748252275" Y="-0.290654896676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.846346182799" Y="0.462711712492" />
                  <Point X="29.166607361628" Y="1.04047817326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.86462938483" Y="-2.916446821203" />
                  <Point X="27.014197625891" Y="-2.646618571657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.321106600755" Y="-2.09294012448" />
                  <Point X="28.375384275886" Y="-0.190972851228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.718780735579" Y="0.428530761049" />
                  <Point X="29.049438146924" Y="1.025052521751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.798085441376" Y="-2.840542065753" />
                  <Point X="27.019590858365" Y="-2.440935715454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.150615694604" Y="-2.204560653752" />
                  <Point X="28.349315626771" Y="-0.042048731882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.573461460162" Y="0.3623210557" />
                  <Point X="28.93226893222" Y="1.009626870241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.719623779979" Y="-2.786137442607" />
                  <Point X="28.815099717516" Y="0.994201218732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.63954324408" Y="-2.734653346372" />
                  <Point X="28.697930502811" Y="0.978775567222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.804415414699" Y="-4.045310625066" />
                  <Point X="25.816200911475" Y="-4.024049026063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.559428676459" Y="-2.683230644988" />
                  <Point X="28.580761288107" Y="0.963349915713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.769020115134" Y="-3.91321222853" />
                  <Point X="25.795354449268" Y="-3.865703832148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.46877925292" Y="-2.650813326776" />
                  <Point X="28.465427769236" Y="0.95123594715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.733624694929" Y="-3.781114049633" />
                  <Point X="25.774507987061" Y="-3.707358638233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.360077813921" Y="-2.650962706532" />
                  <Point X="28.363154244943" Y="0.96268283249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.698229218582" Y="-3.649015972019" />
                  <Point X="25.753661524854" Y="-3.549013444318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.245620950483" Y="-2.661495146828" />
                  <Point X="28.268898306345" Y="0.988593825307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.068387330253" Y="2.430910204252" />
                  <Point X="29.116341742843" Y="2.51742225464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.662833742234" Y="-3.516917894405" />
                  <Point X="25.732815062648" Y="-3.390668250403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.12714146351" Y="-2.679284592083" />
                  <Point X="28.185746623497" Y="1.034537425782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.879374685454" Y="2.285875573949" />
                  <Point X="29.059651860452" Y="2.61110420683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.615568176596" Y="-3.406234024732" />
                  <Point X="25.747424889444" Y="-3.168358217902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.952516212537" Y="-2.79836367685" />
                  <Point X="28.11322682155" Y="1.099661447131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.690362040655" Y="2.140840943646" />
                  <Point X="28.999589075226" Y="2.698701281232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.555179353004" Y="-3.319225139113" />
                  <Point X="28.053992644393" Y="1.188753370061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.501349395856" Y="1.995806313343" />
                  <Point X="28.934753318539" Y="2.777687687185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.494790243351" Y="-3.23221676956" />
                  <Point X="28.013239644343" Y="1.311186219064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.312337341212" Y="1.850772747707" />
                  <Point X="28.775012700644" Y="2.68546119131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.43179578048" Y="-3.149908581632" />
                  <Point X="28.027297931315" Y="1.532501247384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.096964194789" Y="1.658182513622" />
                  <Point X="28.615272258747" Y="2.593235012944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.353593164342" Y="-3.095036628469" />
                  <Point X="28.455531856299" Y="2.501008905746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.26232717235" Y="-3.06373162919" />
                  <Point X="28.295791453851" Y="2.408782798549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.169652057427" Y="-3.034968754971" />
                  <Point X="28.136051051403" Y="2.316556691351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.076964237359" Y="-3.006228801441" />
                  <Point X="27.976310648955" Y="2.224330584153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.994591470896" Y="-4.762927753881" />
                  <Point X="24.035280659685" Y="-4.689522514182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.972014163" Y="-2.99961054024" />
                  <Point X="27.816570246507" Y="2.132104476955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.897905189143" Y="-4.741401216178" />
                  <Point X="24.136917213596" Y="-4.31021210998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.844031411727" Y="-3.034544328122" />
                  <Point X="27.656829844059" Y="2.039878369758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.878655456462" Y="-4.580175445947" />
                  <Point X="24.238553248681" Y="-3.930902641766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.712843554118" Y="-3.075260280897" />
                  <Point X="27.519584954997" Y="1.988235242987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.860581616415" Y="-4.416828309249" />
                  <Point X="24.340189283766" Y="-3.551593173552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.516031536644" Y="-3.234365351966" />
                  <Point X="27.404348738775" Y="1.97629681305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.831896450146" Y="-4.2726245118" />
                  <Point X="27.304312678531" Y="1.991780190385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.796623049375" Y="-4.140306204017" />
                  <Point X="27.218487305811" Y="2.03290032665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.73471894364" Y="-4.056030959747" />
                  <Point X="27.141629878572" Y="2.090199064828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.661630356113" Y="-3.991933054745" />
                  <Point X="27.077509430026" Y="2.170475920825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.588541528028" Y="-3.927835583724" />
                  <Point X="27.027101763859" Y="2.275491291093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.515452699942" Y="-3.863738112703" />
                  <Point X="27.019249235105" Y="2.457278161486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.434064128089" Y="-3.814613775794" />
                  <Point X="27.741483217962" Y="3.956175964305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.336633876869" Y="-3.794429394538" />
                  <Point X="27.663574815955" Y="4.011578693812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.231823139775" Y="-3.787559762258" />
                  <Point X="27.583309789842" Y="4.06272996089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.127012399504" Y="-3.780690135707" />
                  <Point X="27.501206077555" Y="4.110564150303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.019929797822" Y="-3.777919055634" />
                  <Point X="27.419102365267" Y="4.158398339716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.881919606739" Y="-3.830942823799" />
                  <Point X="27.336998645982" Y="4.206232516505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.709404795924" Y="-3.946214573735" />
                  <Point X="27.254894839313" Y="4.254066535648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.48496852509" Y="-4.15515511707" />
                  <Point X="27.168861710374" Y="4.294811869773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.404104006947" Y="-4.105085362244" />
                  <Point X="27.082603852402" Y="4.335151781989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.323239488803" Y="-4.055015607417" />
                  <Point X="26.99634599443" Y="4.375491694205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.242963023272" Y="-4.003884977594" />
                  <Point X="26.910088259981" Y="4.415831829262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.166835247452" Y="-3.945269913412" />
                  <Point X="26.822162462757" Y="4.453162699413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.090707728659" Y="-3.88665438554" />
                  <Point X="26.731726149943" Y="4.48596447955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.688595673586" Y="-2.612082773326" />
                  <Point X="26.641289837128" Y="4.518766259687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.653384365712" Y="-2.479652446991" />
                  <Point X="26.550853524314" Y="4.551568039824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.586776726217" Y="-2.403862602243" />
                  <Point X="26.460416906336" Y="4.584369269431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.502958611975" Y="-2.359121275826" />
                  <Point X="26.365023413946" Y="4.608228060881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.398447594031" Y="-2.351710935885" />
                  <Point X="26.269225297774" Y="4.631356891707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.256588930063" Y="-2.411677532918" />
                  <Point X="26.173427181602" Y="4.654485722532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.096848533942" Y="-2.503903628703" />
                  <Point X="26.07762906543" Y="4.677614553357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.93710813782" Y="-2.596129724488" />
                  <Point X="25.981830949258" Y="4.700743384182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.777367741698" Y="-2.688355820273" />
                  <Point X="25.886032716691" Y="4.723872005027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.617627345576" Y="-2.780581916058" />
                  <Point X="25.787538447157" Y="4.74213684643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.457886949454" Y="-2.872808011843" />
                  <Point X="25.684879292558" Y="4.752888036282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.298146492691" Y="-2.965034217028" />
                  <Point X="25.582220137959" Y="4.763639226135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.179958687691" Y="-2.982297454074" />
                  <Point X="21.97001999332" Y="-1.556989129127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.042729901215" Y="-1.425816983003" />
                  <Point X="25.111401991906" Y="4.110214013871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.277835822549" Y="4.410468592444" />
                  <Point X="25.47956098336" Y="4.774390415988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.117109546237" Y="-2.899727099371" />
                  <Point X="21.778141397352" Y="-1.707194072204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.029274784856" Y="-1.254137448203" />
                  <Point X="24.989021548605" Y="4.085387057109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.054260404783" Y="-2.817156744667" />
                  <Point X="21.589128653209" Y="-1.852228881728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.96700628641" Y="-1.170519585785" />
                  <Point X="24.895684659356" Y="4.11295605884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.991888264622" Y="-2.733725856852" />
                  <Point X="21.400116167691" Y="-1.99726322468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.885775657189" Y="-1.121110312825" />
                  <Point X="24.821109054957" Y="4.174371314391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.935594642446" Y="-2.639329032309" />
                  <Point X="21.211103682174" Y="-2.142297567631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.78883127876" Y="-1.100049393851" />
                  <Point X="24.769959107458" Y="4.278047573689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.879300644253" Y="-2.544932886121" />
                  <Point X="21.022091196657" Y="-2.287331910582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.67418443303" Y="-1.110924571275" />
                  <Point X="24.734563654787" Y="4.410145694016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.823006584399" Y="-2.45053685117" />
                  <Point X="20.833078711139" Y="-2.432366253534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.557015226295" Y="-1.126350208409" />
                  <Point X="24.699168278553" Y="4.542243952237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.439846019559" Y="-1.141775845542" />
                  <Point X="24.663772931503" Y="4.67434226311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.322676812824" Y="-1.157201482675" />
                  <Point X="24.613775797982" Y="4.780098253875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.205507606088" Y="-1.172627119809" />
                  <Point X="24.49762189802" Y="4.766504278648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.088338399353" Y="-1.188052756942" />
                  <Point X="21.612099668331" Y="-0.243162415345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.69972115868" Y="-0.085089062367" />
                  <Point X="24.381467998059" Y="4.752910303421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.971169192618" Y="-1.203478394075" />
                  <Point X="21.460609086729" Y="-0.320505451763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.682624589043" Y="0.080021116822" />
                  <Point X="24.265314098098" Y="4.739316328195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.853999985882" Y="-1.218904031209" />
                  <Point X="21.333043545704" Y="-0.354686572435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.623825579832" Y="0.169898103506" />
                  <Point X="24.149160116136" Y="4.725722205036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.736830779147" Y="-1.234329668342" />
                  <Point X="21.205478004679" Y="-0.388867693107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.546487995901" Y="0.226330616084" />
                  <Point X="24.029166880894" Y="4.705201885614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.619661572411" Y="-1.249755305475" />
                  <Point X="21.077912465139" Y="-0.4230488111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.456068205865" Y="0.259162204101" />
                  <Point X="23.900561981977" Y="4.66914571367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.502492367045" Y="-1.265180940139" />
                  <Point X="20.950346929792" Y="-0.457229921527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.361496059205" Y="0.284502742473" />
                  <Point X="23.771957083059" Y="4.633089541725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.385323164056" Y="-1.280606570513" />
                  <Point X="20.822781394446" Y="-0.491411031954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.266923912546" Y="0.309843280845" />
                  <Point X="23.362161526574" Y="4.089751995192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.537498569955" Y="4.40606839472" />
                  <Point X="23.643352184142" Y="4.597033369781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.318340271053" Y="-1.205493701013" />
                  <Point X="20.695215859099" Y="-0.525592142381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.172351765886" Y="0.335183819216" />
                  <Point X="23.223875536243" Y="4.036230672015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.284076043863" Y="-1.071354795897" />
                  <Point X="20.567650323753" Y="-0.559773252809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.077779619226" Y="0.360524357588" />
                  <Point X="23.114704401557" Y="4.03523393881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.253109320895" Y="-0.93126703569" />
                  <Point X="20.440084788406" Y="-0.593954363236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.983207472566" Y="0.38586489596" />
                  <Point X="21.489988046915" Y="1.300121253529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.557404728904" Y="1.42174416734" />
                  <Point X="23.02606165498" Y="4.071271398091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.230832731618" Y="-0.775501859306" />
                  <Point X="20.31251925306" Y="-0.628135473663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.888635325906" Y="0.411205434331" />
                  <Point X="21.335893653306" Y="1.218080815903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.558264355369" Y="1.619248181799" />
                  <Point X="22.942641700006" Y="4.116731022839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.794063179246" Y="0.436545972703" />
                  <Point X="21.220467052465" Y="1.205798923022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.504463870449" Y="1.71814274501" />
                  <Point X="22.084365482371" Y="2.764312946277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.249625792517" Y="3.062450437832" />
                  <Point X="22.871665910115" Y="4.184640515673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.699491032586" Y="0.461886511075" />
                  <Point X="21.119235923769" Y="1.219126339799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.433856212867" Y="1.78671636611" />
                  <Point X="21.95318150196" Y="2.723603988153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.222056766297" Y="3.208667805228" />
                  <Point X="22.808603182062" Y="4.266825549952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.604918885927" Y="0.487227049446" />
                  <Point X="21.018004795073" Y="1.232453756576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.357650616213" Y="1.845191037791" />
                  <Point X="21.848573298364" Y="2.730839000538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.167708191176" Y="3.306573587543" />
                  <Point X="22.653328935029" Y="4.182656600405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.510346739267" Y="0.512567587818" />
                  <Point X="20.916773639971" Y="1.245781125716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.281445019559" Y="1.903665709473" />
                  <Point X="21.754903545146" Y="2.757807499773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.112293027444" Y="3.40255519307" />
                  <Point X="22.496374326284" Y="4.095456198084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.415774592607" Y="0.53790812619" />
                  <Point X="20.815542428897" Y="1.259108393879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.205239422904" Y="1.962140381154" />
                  <Point X="21.672350005046" Y="2.80483017833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.056877863712" Y="3.498536798596" />
                  <Point X="22.336933966949" Y="4.00377138299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.32120241192" Y="0.563248603175" />
                  <Point X="20.714311217823" Y="1.272435662042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.12903382625" Y="2.020615052835" />
                  <Point X="21.590065109783" Y="2.852337505003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.001462699981" Y="3.594518404123" />
                  <Point X="22.148037963382" Y="3.858947179041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.226630217366" Y="0.588589055144" />
                  <Point X="20.613080006749" Y="1.285762930205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.052828229596" Y="2.079089724516" />
                  <Point X="21.50778022453" Y="2.899844849733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.946047536249" Y="3.69050000965" />
                  <Point X="21.959141959816" Y="3.714122975092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.249058665806" Y="0.825004254471" />
                  <Point X="20.511848795675" Y="1.299090198369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.976622632941" Y="2.137564396197" />
                  <Point X="21.425495340938" Y="2.947352197461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.321054666172" Y="1.150841684584" />
                  <Point X="20.410617584601" Y="1.312417466532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.900417036287" Y="2.196039067878" />
                  <Point X="21.343210457346" Y="2.994859545189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.824211439633" Y="2.25451373956" />
                  <Point X="21.215187379835" Y="2.959853006847" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.574251953125" Y="-3.920435791016" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521543945312" />
                  <Point X="25.42233984375" Y="-3.461057617188" />
                  <Point X="25.300591796875" Y="-3.285643554688" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.209373046875" Y="-3.246237792969" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.926373046875" Y="-3.207928466797" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285644042969" />
                  <Point X="24.669740234375" Y="-3.346130126953" />
                  <Point X="24.547994140625" Y="-3.521544433594" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.24290234375" Y="-4.648778320312" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.121162109375" Y="-4.981041503906" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.825396484375" Y="-4.918933105469" />
                  <Point X="23.648412109375" Y="-4.873396484375" />
                  <Point X="23.678486328125" Y="-4.644953125" />
                  <Point X="23.6908515625" Y="-4.5510390625" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.674798828125" Y="-4.456735351562" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.553908203125" Y="-4.150177246094" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.27137890625" Y="-3.980560058594" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.943978515625" Y="-4.017987060547" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.57303515625" Y="-4.375233886719" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.46870703125" Y="-4.36855859375" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="22.04119921875" Y="-4.088329589844" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.30308984375" Y="-2.959728759766" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597590576172" />
                  <Point X="22.486017578125" Y="-2.568760498047" />
                  <Point X="22.46867578125" Y="-2.551418212891" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.44952734375" Y="-3.097027587891" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.094701171875" Y="-3.183991943359" />
                  <Point X="20.83830078125" Y="-2.847135742188" />
                  <Point X="20.764478515625" Y="-2.723346679688" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="21.502240234375" Y="-1.679411499023" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.847462890625" Y="-1.411084228516" />
                  <Point X="21.856154296875" Y="-1.388385009766" />
                  <Point X="21.8618828125" Y="-1.366265014648" />
                  <Point X="21.86334765625" Y="-1.350050537109" />
                  <Point X="21.85134375" Y="-1.321068969727" />
                  <Point X="21.83205078125" Y="-1.306643310547" />
                  <Point X="21.812359375" Y="-1.295053222656" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.570091796875" Y="-1.447920898438" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.172890625" Y="-1.403788818359" />
                  <Point X="20.072607421875" Y="-1.011187805176" />
                  <Point X="20.053078125" Y="-0.874636352539" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="21.062091796875" Y="-0.230585510254" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.46522265625" Y="-0.116485984802" />
                  <Point X="21.485859375" Y="-0.102162872314" />
                  <Point X="21.4976796875" Y="-0.090643028259" />
                  <Point X="21.507474609375" Y="-0.068264976501" />
                  <Point X="21.514353515625" Y="-0.046100727081" />
                  <Point X="21.516599609375" Y="-0.031282693863" />
                  <Point X="21.511982421875" Y="-0.008816726685" />
                  <Point X="21.505103515625" Y="0.013347525597" />
                  <Point X="21.497677734375" Y="0.028085069656" />
                  <Point X="21.4787421875" Y="0.044541702271" />
                  <Point X="21.45810546875" Y="0.058864818573" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.339216796875" Y="0.361719177246" />
                  <Point X="20.001814453125" Y="0.452126098633" />
                  <Point X="20.017724609375" Y="0.55965423584" />
                  <Point X="20.08235546875" Y="0.996414916992" />
                  <Point X="20.121671875" Y="1.14150378418" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.9740234375" Y="1.429883544922" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866699219" />
                  <Point X="21.284046875" Y="1.400832641602" />
                  <Point X="21.32972265625" Y="1.41523425293" />
                  <Point X="21.348466796875" Y="1.426056030273" />
                  <Point X="21.360880859375" Y="1.443785522461" />
                  <Point X="21.36719921875" Y="1.459042724609" />
                  <Point X="21.38552734375" Y="1.503289794922" />
                  <Point X="21.389287109375" Y="1.52460546875" />
                  <Point X="21.38368359375" Y="1.545512451172" />
                  <Point X="21.37605859375" Y="1.560160888672" />
                  <Point X="21.353943359375" Y="1.602642211914" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.7071640625" Y="2.104837890625" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.588857421875" Y="2.356761962891" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="20.94412890625" Y="2.920868164062" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.6733515625" Y="3.023645263672" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.883421875" Y="2.918515625" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404296875" />
                  <Point X="22.002318359375" Y="2.942974121094" />
                  <Point X="22.04747265625" Y="2.988127685547" />
                  <Point X="22.0591015625" Y="3.006381835938" />
                  <Point X="22.061927734375" Y="3.027840332031" />
                  <Point X="22.0600078125" Y="3.049775878906" />
                  <Point X="22.054443359375" Y="3.113389892578" />
                  <Point X="22.04793359375" Y="3.134032714844" />
                  <Point X="21.767490234375" Y="3.6197734375" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.809736328125" Y="3.838990966797" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.411146484375" Y="4.265458496094" />
                  <Point X="22.858451171875" Y="4.513971679688" />
                  <Point X="22.9808984375" Y="4.354395019531" />
                  <Point X="23.032173828125" Y="4.287572753906" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.07316796875" Y="4.260951660156" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.21162109375" Y="4.232783691406" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.303095703125" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.294487792969" />
                  <Point X="23.322193359375" Y="4.320737792969" />
                  <Point X="23.346197265625" Y="4.396864257812" />
                  <Point X="23.348083984375" Y="4.41842578125" />
                  <Point X="23.31619921875" Y="4.660603027344" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.465681640625" Y="4.744546875" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.23075" Y="4.926567871094" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.908310546875" Y="4.495822753906" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176269531" />
                  <Point X="25.054453125" Y="4.310902832031" />
                  <Point X="25.1981484375" Y="4.847182128906" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.365810546875" Y="4.977342285156" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.024716796875" Y="4.885848144531" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.61540625" Y="4.730266601562" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="27.034576171875" Y="4.567362792969" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.43873046875" Y="4.366855957031" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="27.82686328125" Y="4.128603515625" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.45078125" Y="2.886253173828" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491514892578" />
                  <Point X="27.21934765625" Y="2.470928955078" />
                  <Point X="27.2033828125" Y="2.411228515625" />
                  <Point X="27.20419140625" Y="2.374525390625" />
                  <Point X="27.210416015625" Y="2.322901367188" />
                  <Point X="27.218681640625" Y="2.300812255859" />
                  <Point X="27.229697265625" Y="2.284579589844" />
                  <Point X="27.261640625" Y="2.237503662109" />
                  <Point X="27.291173828125" Y="2.213188964844" />
                  <Point X="27.33825" Y="2.18124609375" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.378138671875" Y="2.170833740234" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.46925" Y="2.171451416016" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.650763671875" Y="2.833119140625" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.02832421875" Y="2.984073486328" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.255185546875" Y="2.654970947266" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.57948828125" Y="1.816275146484" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583833374023" />
                  <Point X="28.264556640625" Y="1.564505249023" />
                  <Point X="28.22158984375" Y="1.508452026367" />
                  <Point X="28.21312109375" Y="1.491500610352" />
                  <Point X="28.2076015625" Y="1.471766479492" />
                  <Point X="28.191595703125" Y="1.414536010742" />
                  <Point X="28.190779296875" Y="1.390965209961" />
                  <Point X="28.195310546875" Y="1.369008422852" />
                  <Point X="28.20844921875" Y="1.305332397461" />
                  <Point X="28.215646484375" Y="1.287955566406" />
                  <Point X="28.22796875" Y="1.26922644043" />
                  <Point X="28.263703125" Y="1.21491003418" />
                  <Point X="28.280947265625" Y="1.198819946289" />
                  <Point X="28.2988046875" Y="1.188768188477" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.3927109375" Y="1.150428710938" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.529970703125" Y="1.279955566406" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.864341796875" Y="1.258834594727" />
                  <Point X="29.939193359375" Y="0.951366943359" />
                  <Point X="29.955763671875" Y="0.8449375" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="29.07451171875" Y="0.327145874023" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819198608" />
                  <Point X="28.7053671875" Y="0.219108657837" />
                  <Point X="28.636578125" Y="0.179346832275" />
                  <Point X="28.622265625" Y="0.166926391602" />
                  <Point X="28.608033203125" Y="0.148791397095" />
                  <Point X="28.566759765625" Y="0.09619871521" />
                  <Point X="28.556986328125" Y="0.074734954834" />
                  <Point X="28.5522421875" Y="0.0499635849" />
                  <Point X="28.538484375" Y="-0.021875450134" />
                  <Point X="28.538484375" Y="-0.040684627533" />
                  <Point X="28.543228515625" Y="-0.065456001282" />
                  <Point X="28.556986328125" Y="-0.137295028687" />
                  <Point X="28.566759765625" Y="-0.158758789063" />
                  <Point X="28.5809921875" Y="-0.176893783569" />
                  <Point X="28.622265625" Y="-0.229486465454" />
                  <Point X="28.636578125" Y="-0.241906906128" />
                  <Point X="28.660298828125" Y="-0.255617446899" />
                  <Point X="28.729087890625" Y="-0.295379272461" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.70788671875" Y="-0.559418579102" />
                  <Point X="29.998068359375" Y="-0.637172546387" />
                  <Point X="29.990224609375" Y="-0.689206298828" />
                  <Point X="29.948431640625" Y="-0.966412841797" />
                  <Point X="29.927201171875" Y="-1.059446533203" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.79856640625" Y="-1.148522827148" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.348283203125" Y="-1.108459960938" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.185447265625" Y="-1.154697143555" />
                  <Point X="28.15730859375" Y="-1.188539672852" />
                  <Point X="28.075703125" Y="-1.286685180664" />
                  <Point X="28.064359375" Y="-1.314070800781" />
                  <Point X="28.060326171875" Y="-1.35789831543" />
                  <Point X="28.048630859375" Y="-1.485001220703" />
                  <Point X="28.056361328125" Y="-1.516621826172" />
                  <Point X="28.082125" Y="-1.556695678711" />
                  <Point X="28.156841796875" Y="-1.672912475586" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="29.065583984375" Y="-2.373926269531" />
                  <Point X="29.339076171875" Y="-2.583784179688" />
                  <Point X="29.321939453125" Y="-2.611512939453" />
                  <Point X="29.204134765625" Y="-2.802138427734" />
                  <Point X="29.160220703125" Y="-2.864534912109" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.09776171875" Y="-2.458000732422" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.681935546875" Y="-2.243306396484" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.443048828125" Y="-2.243469726562" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334683105469" />
                  <Point X="27.264376953125" Y="-2.380712646484" />
                  <Point X="27.19412109375" Y="-2.514201171875" />
                  <Point X="27.1891640625" Y="-2.546374511719" />
                  <Point X="27.199171875" Y="-2.60178125" />
                  <Point X="27.22819140625" Y="-2.76246484375" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.810583984375" Y="-3.777089599609" />
                  <Point X="27.98667578125" Y="-4.082089111328" />
                  <Point X="27.975087890625" Y="-4.090366210938" />
                  <Point X="27.835314453125" Y="-4.190202148438" />
                  <Point X="27.786203125" Y="-4.221991210938" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="26.945916015625" Y="-3.334492675781" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.615904296875" Y="-2.945334960938" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.366041015625" Y="-2.841216796875" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.119185546875" Y="-2.906880126953" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045985839844" />
                  <Point X="25.95466015625" Y="-3.10946875" />
                  <Point X="25.914642578125" Y="-3.293573730469" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="26.077302734375" Y="-4.551661621094" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.12659765625" Y="-4.9342578125" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.94898828125" Y="-4.971486816406" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#142" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.049548971452" Y="4.540043761932" Z="0.65" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.65" />
                  <Point X="-0.781219238283" Y="5.007509184099" Z="0.65" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.65" />
                  <Point X="-1.553973044818" Y="4.823970338236" Z="0.65" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.65" />
                  <Point X="-1.739529051844" Y="4.685357439594" Z="0.65" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.65" />
                  <Point X="-1.731648567591" Y="4.367053989318" Z="0.65" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.65" />
                  <Point X="-1.813670313711" Y="4.310257594668" Z="0.65" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.65" />
                  <Point X="-1.909901257286" Y="4.33658213761" Z="0.65" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.65" />
                  <Point X="-1.985589778871" Y="4.41611368167" Z="0.65" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.65" />
                  <Point X="-2.619292722615" Y="4.34044628258" Z="0.65" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.65" />
                  <Point X="-3.226841738242" Y="3.909950934568" Z="0.65" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.65" />
                  <Point X="-3.281967312673" Y="3.626053758592" Z="0.65" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.65" />
                  <Point X="-2.995959124172" Y="3.076698881895" Z="0.65" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.65" />
                  <Point X="-3.039193704129" Y="3.009609863758" Z="0.65" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.65" />
                  <Point X="-3.118377583712" Y="2.999605575001" Z="0.65" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.65" />
                  <Point X="-3.307805589705" Y="3.098226636073" Z="0.65" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.65" />
                  <Point X="-4.101490492392" Y="2.982850552871" Z="0.65" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.65" />
                  <Point X="-4.460481739335" Y="2.413247024759" Z="0.65" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.65" />
                  <Point X="-4.329429504242" Y="2.096450160351" Z="0.65" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.65" />
                  <Point X="-3.674447896769" Y="1.568352761616" Z="0.65" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.65" />
                  <Point X="-3.685150159406" Y="1.50945733361" Z="0.65" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.65" />
                  <Point X="-3.737146063554" Y="1.47979848801" Z="0.65" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.65" />
                  <Point X="-4.025608932035" Y="1.510735874986" Z="0.65" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.65" />
                  <Point X="-4.932745647644" Y="1.185860937105" Z="0.65" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.65" />
                  <Point X="-5.037892524107" Y="0.598253431006" Z="0.65" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.65" />
                  <Point X="-4.679881185355" Y="0.344702860097" Z="0.65" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.65" />
                  <Point X="-3.555923849975" Y="0.034745854875" Z="0.65" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.65" />
                  <Point X="-3.54192877932" Y="0.007642666657" Z="0.65" />
                  <Point X="-3.539556741714" Y="0" Z="0.65" />
                  <Point X="-3.546435821815" Y="-0.022164284404" Z="0.65" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.65" />
                  <Point X="-3.569444745149" Y="-0.044130128259" Z="0.65" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.65" />
                  <Point X="-3.957006603418" Y="-0.151009196659" Z="0.65" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.65" />
                  <Point X="-5.002575542816" Y="-0.850435492984" Z="0.65" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.65" />
                  <Point X="-4.881712526674" Y="-1.384883157171" Z="0.65" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.65" />
                  <Point X="-4.429540672593" Y="-1.46621306238" Z="0.65" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.65" />
                  <Point X="-3.199466585888" Y="-1.318453339962" Z="0.65" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.65" />
                  <Point X="-3.198405326786" Y="-1.344569781307" Z="0.65" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.65" />
                  <Point X="-3.534354062682" Y="-1.608463855506" Z="0.65" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.65" />
                  <Point X="-4.284621619936" Y="-2.717676252689" Z="0.65" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.65" />
                  <Point X="-3.951241339277" Y="-3.182995139943" Z="0.65" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.65" />
                  <Point X="-3.531629769661" Y="-3.109048784063" Z="0.65" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.65" />
                  <Point X="-2.559939228856" Y="-2.568391240276" Z="0.65" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.65" />
                  <Point X="-2.746368229105" Y="-2.903448612831" Z="0.65" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.65" />
                  <Point X="-2.995460886088" Y="-4.096666913651" Z="0.65" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.65" />
                  <Point X="-2.563771506369" Y="-4.379789162672" Z="0.65" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.65" />
                  <Point X="-2.393453313158" Y="-4.374391832631" Z="0.65" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.65" />
                  <Point X="-2.034399973745" Y="-4.028280425266" Z="0.65" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.65" />
                  <Point X="-1.738047214159" Y="-3.99917306494" Z="0.65" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.65" />
                  <Point X="-1.485215352806" Y="-4.156489021572" Z="0.65" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.65" />
                  <Point X="-1.380398105731" Y="-4.435210154441" Z="0.65" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.65" />
                  <Point X="-1.377242541349" Y="-4.607146140631" Z="0.65" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.65" />
                  <Point X="-1.193220234479" Y="-4.936076158126" Z="0.65" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.65" />
                  <Point X="-0.894521469102" Y="-4.99884543735" Z="0.65" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.65" />
                  <Point X="-0.714956825476" Y="-4.630439301874" Z="0.65" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.65" />
                  <Point X="-0.295339764888" Y="-3.343358841085" Z="0.65" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.65" />
                  <Point X="-0.06496266539" Y="-3.224401382429" Z="0.65" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.65" />
                  <Point X="0.188396413972" Y="-3.262710662859" Z="0.65" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.65" />
                  <Point X="0.375106094544" Y="-3.458286914683" Z="0.65" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.65" />
                  <Point X="0.519798034926" Y="-3.902096727941" Z="0.65" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.65" />
                  <Point X="0.951769454981" Y="-4.989401625327" Z="0.65" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.65" />
                  <Point X="1.131145409099" Y="-4.951818206771" Z="0.65" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.65" />
                  <Point X="1.120718839102" Y="-4.513855103699" Z="0.65" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.65" />
                  <Point X="0.997361750065" Y="-3.088808860535" Z="0.65" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.65" />
                  <Point X="1.144994928741" Y="-2.91404667236" Z="0.65" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.65" />
                  <Point X="1.364465739511" Y="-2.859726192809" Z="0.65" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.65" />
                  <Point X="1.582707871373" Y="-2.956112924329" Z="0.65" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.65" />
                  <Point X="1.900090776153" Y="-3.333650567081" Z="0.65" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.65" />
                  <Point X="2.807216322698" Y="-4.232685101368" Z="0.65" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.65" />
                  <Point X="2.997990673943" Y="-4.099773059617" Z="0.65" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.65" />
                  <Point X="2.847727636269" Y="-3.720809455595" Z="0.65" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.65" />
                  <Point X="2.242217539128" Y="-2.56161496227" Z="0.65" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.65" />
                  <Point X="2.302465238736" Y="-2.372719598348" Z="0.65" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.65" />
                  <Point X="2.460178780024" Y="-2.256436071231" Z="0.65" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.65" />
                  <Point X="2.666891841243" Y="-2.261230470363" Z="0.65" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.65" />
                  <Point X="3.066604141511" Y="-2.470021864005" Z="0.65" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.65" />
                  <Point X="4.194951983017" Y="-2.86203209931" Z="0.65" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.65" />
                  <Point X="4.358315380285" Y="-2.606518163858" Z="0.65" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.65" />
                  <Point X="4.089864080339" Y="-2.302978472258" Z="0.65" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.65" />
                  <Point X="3.118026214253" Y="-1.498376252097" Z="0.65" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.65" />
                  <Point X="3.103958467322" Y="-1.331199694576" Z="0.65" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.65" />
                  <Point X="3.189596543585" Y="-1.18922664658" Z="0.65" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.65" />
                  <Point X="3.352745764072" Y="-1.126039149801" Z="0.65" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.65" />
                  <Point X="3.785884220169" Y="-1.166815217164" Z="0.65" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.65" />
                  <Point X="4.969789648516" Y="-1.039290515755" Z="0.65" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.65" />
                  <Point X="5.033508532318" Y="-0.665380410605" Z="0.65" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.65" />
                  <Point X="4.714671942979" Y="-0.479842265538" Z="0.65" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.65" />
                  <Point X="3.679162785091" Y="-0.181048833227" Z="0.65" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.65" />
                  <Point X="3.614168765949" Y="-0.114745497486" Z="0.65" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.65" />
                  <Point X="3.586178740795" Y="-0.024771473102" Z="0.65" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.65" />
                  <Point X="3.595192709629" Y="0.071839058128" Z="0.65" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.65" />
                  <Point X="3.641210672452" Y="0.149203241089" Z="0.65" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.65" />
                  <Point X="3.724232629262" Y="0.207100011015" Z="0.65" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.65" />
                  <Point X="4.081295874327" Y="0.310129667231" Z="0.65" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.65" />
                  <Point X="4.999010580588" Y="0.883909437875" Z="0.65" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.65" />
                  <Point X="4.906766374431" Y="1.301941390364" Z="0.65" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.65" />
                  <Point X="4.517288583205" Y="1.360807866533" Z="0.65" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.65" />
                  <Point X="3.393103966223" Y="1.231277741839" Z="0.65" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.65" />
                  <Point X="3.317175162106" Y="1.263619306154" Z="0.65" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.65" />
                  <Point X="3.263583218331" Y="1.32798716744" Z="0.65" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.65" />
                  <Point X="3.238122452822" Y="1.410392482505" Z="0.65" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.65" />
                  <Point X="3.249597125735" Y="1.489579580898" Z="0.65" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.65" />
                  <Point X="3.298082491123" Y="1.565366894508" Z="0.65" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.65" />
                  <Point X="3.603768093658" Y="1.807887443937" Z="0.65" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.65" />
                  <Point X="4.291805523022" Y="2.712137222266" Z="0.65" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.65" />
                  <Point X="4.062753072433" Y="3.044556552108" Z="0.65" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.65" />
                  <Point X="3.619605867174" Y="2.907700460247" Z="0.65" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.65" />
                  <Point X="2.45017749676" Y="2.251034096881" Z="0.65" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.65" />
                  <Point X="2.377967707085" Y="2.251754066833" Z="0.65" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.65" />
                  <Point X="2.313090776206" Y="2.285843520977" Z="0.65" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.65" />
                  <Point X="2.264915079258" Y="2.343934084176" Z="0.65" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.65" />
                  <Point X="2.247675635926" Y="2.411790736747" Z="0.65" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.65" />
                  <Point X="2.261493845776" Y="2.489292079122" Z="0.65" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.65" />
                  <Point X="2.487924892612" Y="2.892533214781" Z="0.65" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.65" />
                  <Point X="2.849682918537" Y="4.200630486783" Z="0.65" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.65" />
                  <Point X="2.457744198355" Y="4.441338757343" Z="0.65" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.65" />
                  <Point X="2.049601461394" Y="4.643934783701" Z="0.65" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.65" />
                  <Point X="1.62629803108" Y="4.808550627943" Z="0.65" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.65" />
                  <Point X="1.03029374554" Y="4.965731550357" Z="0.65" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.65" />
                  <Point X="0.364860388841" Y="5.058350271797" Z="0.65" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.65" />
                  <Point X="0.143695590101" Y="4.891403744019" Z="0.65" />
                  <Point X="0" Y="4.355124473572" Z="0.65" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>