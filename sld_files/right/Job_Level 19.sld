<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#153" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1476" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.78389453125" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.705443359375" Y="-4.042992919922" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467377197266" />
                  <Point X="25.48426953125" Y="-3.383672363281" />
                  <Point X="25.37863671875" Y="-3.231476806641" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.21259765625" Y="-3.147767822266" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097036132812" />
                  <Point X="24.87327734375" Y="-3.1249375" />
                  <Point X="24.70981640625" Y="-3.175669433594" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231477783203" />
                  <Point X="24.57558203125" Y="-3.315182373047" />
                  <Point X="24.46994921875" Y="-3.467378173828" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.19056640625" Y="-4.477045898438" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.9206640625" Y="-4.845351074219" />
                  <Point X="23.819962890625" Y="-4.819441894531" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.767927734375" Y="-4.693402832031" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.762013671875" Y="-4.408250976562" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182965332031" />
                  <Point X="23.69598828125" Y="-4.155127929687" />
                  <Point X="23.67635546875" Y="-4.131204589844" />
                  <Point X="23.593587890625" Y="-4.058618408203" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295898438" />
                  <Point X="23.387115234375" Y="-3.897994628906" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.24712109375" Y="-3.883766113281" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.86580859375" Y="-3.955963134766" />
                  <Point X="22.699376953125" Y="-4.067169921875" />
                  <Point X="22.68721484375" Y="-4.076821533203" />
                  <Point X="22.6648984375" Y="-4.099461425781" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.475474609375" Y="-4.26101171875" />
                  <Point X="22.198283203125" Y="-4.089382080078" />
                  <Point X="22.058857421875" Y="-3.982027587891" />
                  <Point X="21.89527734375" Y="-3.856077392578" />
                  <Point X="22.31208984375" Y="-3.134137939453" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647654052734" />
                  <Point X="22.593412109375" Y="-2.616126708984" />
                  <Point X="22.594423828125" Y="-2.585190429688" />
                  <Point X="22.585439453125" Y="-2.555570800781" />
                  <Point X="22.571220703125" Y="-2.526741455078" />
                  <Point X="22.553197265625" Y="-2.501590087891" />
                  <Point X="22.535853515625" Y="-2.484245117188" />
                  <Point X="22.510697265625" Y="-2.466216796875" />
                  <Point X="22.4818671875" Y="-2.451997802734" />
                  <Point X="22.452248046875" Y="-2.443011962891" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.5289375" Y="-2.941483886719" />
                  <Point X="21.1819765625" Y="-3.141801513672" />
                  <Point X="21.13612890625" Y="-3.081567871094" />
                  <Point X="20.917142578125" Y="-2.79386328125" />
                  <Point X="20.817177734375" Y="-2.626239501953" />
                  <Point X="20.693857421875" Y="-2.419450195312" />
                  <Point X="21.43187109375" Y="-1.853151977539" />
                  <Point X="21.894044921875" Y="-1.498513305664" />
                  <Point X="21.915421875" Y="-1.475593139648" />
                  <Point X="21.93338671875" Y="-1.448461669922" />
                  <Point X="21.946142578125" Y="-1.419833984375" />
                  <Point X="21.95384765625" Y="-1.390086669922" />
                  <Point X="21.95665234375" Y="-1.359656005859" />
                  <Point X="21.954443359375" Y="-1.327985229492" />
                  <Point X="21.947443359375" Y="-1.298241088867" />
                  <Point X="21.931361328125" Y="-1.272258056641" />
                  <Point X="21.910529296875" Y="-1.248301513672" />
                  <Point X="21.88702734375" Y="-1.228766723633" />
                  <Point X="21.860544921875" Y="-1.213180297852" />
                  <Point X="21.831283203125" Y="-1.201956176758" />
                  <Point X="21.799396484375" Y="-1.195474731445" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="20.71790234375" Y="-1.332641357422" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.251572265625" Y="-1.327965698242" />
                  <Point X="20.165921875" Y="-0.992649475098" />
                  <Point X="20.1394765625" Y="-0.807738769531" />
                  <Point X="20.107578125" Y="-0.584698242188" />
                  <Point X="20.940638671875" Y="-0.361479736328" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.48851171875" Y="-0.211779632568" />
                  <Point X="21.51392578125" Y="-0.197725219727" />
                  <Point X="21.5221171875" Y="-0.19263659668" />
                  <Point X="21.5400234375" Y="-0.180209365845" />
                  <Point X="21.563982421875" Y="-0.158677139282" />
                  <Point X="21.58517578125" Y="-0.127646934509" />
                  <Point X="21.5958828125" Y="-0.10232951355" />
                  <Point X="21.599115234375" Y="-0.093486351013" />
                  <Point X="21.60508203125" Y="-0.074261795044" />
                  <Point X="21.610525390625" Y="-0.045532279968" />
                  <Point X="21.60942578125" Y="-0.011076621056" />
                  <Point X="21.603896484375" Y="0.014324285507" />
                  <Point X="21.60180078125" Y="0.022278406143" />
                  <Point X="21.59583203125" Y="0.041509033203" />
                  <Point X="21.582515625" Y="0.070833267212" />
                  <Point X="21.559078125" Y="0.100575378418" />
                  <Point X="21.537412109375" Y="0.118927749634" />
                  <Point X="21.53017578125" Y="0.124484031677" />
                  <Point X="21.51226953125" Y="0.136911270142" />
                  <Point X="21.49807421875" Y="0.14504737854" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="20.509845703125" Y="0.414350860596" />
                  <Point X="20.108185546875" Y="0.521975402832" />
                  <Point X="20.120314453125" Y="0.603946166992" />
                  <Point X="20.17551171875" Y="0.976968688965" />
                  <Point X="20.228751953125" Y="1.173440673828" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.8569375" Y="1.349478393555" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228027344" />
                  <Point X="21.296865234375" Y="1.305263916016" />
                  <Point X="21.31866015625" Y="1.312136230469" />
                  <Point X="21.358291015625" Y="1.324631469727" />
                  <Point X="21.377224609375" Y="1.332962768555" />
                  <Point X="21.39596875" Y="1.34378527832" />
                  <Point X="21.4126484375" Y="1.356014892578" />
                  <Point X="21.42628515625" Y="1.371564208984" />
                  <Point X="21.438701171875" Y="1.389294799805" />
                  <Point X="21.448650390625" Y="1.407430908203" />
                  <Point X="21.457396484375" Y="1.428544799805" />
                  <Point X="21.473296875" Y="1.466935180664" />
                  <Point X="21.479087890625" Y="1.486805541992" />
                  <Point X="21.48284375" Y="1.508120239258" />
                  <Point X="21.484193359375" Y="1.528755126953" />
                  <Point X="21.481048828125" Y="1.549193481445" />
                  <Point X="21.475447265625" Y="1.570099243164" />
                  <Point X="21.46794921875" Y="1.589378662109" />
                  <Point X="21.457396484375" Y="1.609649902344" />
                  <Point X="21.438208984375" Y="1.646508056641" />
                  <Point X="21.42671875" Y="1.663705810547" />
                  <Point X="21.412806640625" Y="1.680285888672" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="20.848765625" Y="2.115928222656" />
                  <Point X="20.648140625" Y="2.269873779297" />
                  <Point X="20.704365234375" Y="2.366201660156" />
                  <Point X="20.9188515625" Y="2.733664794922" />
                  <Point X="21.05987109375" Y="2.914926025391" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.561556640625" Y="2.978493408203" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.8835625" Y="2.823140625" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.95943359375" Y="2.818762939453" />
                  <Point X="21.980892578125" Y="2.821587890625" />
                  <Point X="22.000984375" Y="2.826504394531" />
                  <Point X="22.019537109375" Y="2.835653320313" />
                  <Point X="22.037791015625" Y="2.847282470703" />
                  <Point X="22.053921875" Y="2.860228759766" />
                  <Point X="22.07546875" Y="2.881775390625" />
                  <Point X="22.114646484375" Y="2.920952148438" />
                  <Point X="22.127595703125" Y="2.937084960938" />
                  <Point X="22.139224609375" Y="2.955339111328" />
                  <Point X="22.14837109375" Y="2.973888183594" />
                  <Point X="22.1532890625" Y="2.993977294922" />
                  <Point X="22.156115234375" Y="3.015436035156" />
                  <Point X="22.15656640625" Y="3.036121826172" />
                  <Point X="22.15391015625" Y="3.066477539062" />
                  <Point X="22.14908203125" Y="3.121671386719" />
                  <Point X="22.145044921875" Y="3.141961669922" />
                  <Point X="22.13853515625" Y="3.162604492188" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.8868828125" Y="3.602979003906" />
                  <Point X="21.81666796875" Y="3.724595947266" />
                  <Point X="21.9258203125" Y="3.808282958984" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.52148046875" Y="4.218081054688" />
                  <Point X="22.83296484375" Y="4.391134277344" />
                  <Point X="22.88584765625" Y="4.322214355469" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.038671875" Y="4.171807128906" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222548828125" Y="4.134482421875" />
                  <Point X="23.25773828125" Y="4.149059082031" />
                  <Point X="23.32172265625" Y="4.175562011719" />
                  <Point X="23.339853515625" Y="4.185508789062" />
                  <Point X="23.357583984375" Y="4.197922851562" />
                  <Point X="23.373140625" Y="4.211563964844" />
                  <Point X="23.385373046875" Y="4.228249511719" />
                  <Point X="23.396193359375" Y="4.246993652344" />
                  <Point X="23.404521484375" Y="4.265922851563" />
                  <Point X="23.415974609375" Y="4.302249511719" />
                  <Point X="23.43680078125" Y="4.368299316406" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410145019531" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.56677734375" Y="4.674227050781" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.319619140625" Y="4.8413203125" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.79752734375" Y="4.542219238281" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183885742188" />
                  <Point X="25.067130859375" Y="4.194217773438" />
                  <Point X="25.094423828125" Y="4.208806640625" />
                  <Point X="25.11558203125" Y="4.23139453125" />
                  <Point X="25.13344140625" Y="4.25812109375" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.270892578125" Y="4.751610351562" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.42179296875" Y="4.875959472656" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.06680859375" Y="4.777956054688" />
                  <Point X="26.48102734375" Y="4.677950195312" />
                  <Point X="26.6250703125" Y="4.625706054688" />
                  <Point X="26.894640625" Y="4.527930664062" />
                  <Point X="27.034857421875" Y="4.462355957031" />
                  <Point X="27.294578125" Y="4.340893066406" />
                  <Point X="27.430056640625" Y="4.261963378906" />
                  <Point X="27.680982421875" Y="4.1157734375" />
                  <Point X="27.808728515625" Y="4.024926757812" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.45331640625" Y="3.080641357422" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142078125" Y="2.53993359375" />
                  <Point X="27.133080078125" Y="2.516059814453" />
                  <Point X="27.1254609375" Y="2.487571777344" />
                  <Point X="27.111609375" Y="2.4357734375" />
                  <Point X="27.108619140625" Y="2.417935791016" />
                  <Point X="27.107728515625" Y="2.380952392578" />
                  <Point X="27.11069921875" Y="2.356318359375" />
                  <Point X="27.116099609375" Y="2.311527587891" />
                  <Point X="27.12144140625" Y="2.289607666016" />
                  <Point X="27.12970703125" Y="2.267518554688" />
                  <Point X="27.140072265625" Y="2.247468017578" />
                  <Point X="27.15531640625" Y="2.225004150391" />
                  <Point X="27.18303125" Y="2.184159423828" />
                  <Point X="27.194466796875" Y="2.170328125" />
                  <Point X="27.221599609375" Y="2.145593261719" />
                  <Point X="27.2440625" Y="2.130350585938" />
                  <Point X="27.284908203125" Y="2.102635742188" />
                  <Point X="27.304955078125" Y="2.092271728516" />
                  <Point X="27.32704296875" Y="2.084006103516" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.37359765625" Y="2.075693115234" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.473205078125" Y="2.074171142578" />
                  <Point X="27.501693359375" Y="2.081789306641" />
                  <Point X="27.5534921875" Y="2.095640869141" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.551375" Y="2.666040771484" />
                  <Point X="28.967328125" Y="2.90619140625" />
                  <Point X="28.97443359375" Y="2.896315673828" />
                  <Point X="29.12326953125" Y="2.689468017578" />
                  <Point X="29.1944921875" Y="2.571772705078" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.633310546875" Y="1.977319458008" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.2214296875" Y="1.660245117188" />
                  <Point X="28.20397265625" Y="1.641626220703" />
                  <Point X="28.183470703125" Y="1.614878662109" />
                  <Point X="28.14619140625" Y="1.566244873047" />
                  <Point X="28.13660546875" Y="1.550908813477" />
                  <Point X="28.1216328125" Y="1.517090087891" />
                  <Point X="28.113994140625" Y="1.489780395508" />
                  <Point X="28.100107421875" Y="1.440125488281" />
                  <Point X="28.09665234375" Y="1.41782409668" />
                  <Point X="28.0958359375" Y="1.394253173828" />
                  <Point X="28.09773828125" Y="1.371767211914" />
                  <Point X="28.1040078125" Y="1.341381958008" />
                  <Point X="28.115408203125" Y="1.286134399414" />
                  <Point X="28.1206796875" Y="1.268977294922" />
                  <Point X="28.136283203125" Y="1.235740600586" />
                  <Point X="28.1533359375" Y="1.209821533203" />
                  <Point X="28.18433984375" Y="1.16269519043" />
                  <Point X="28.198892578125" Y="1.145451416016" />
                  <Point X="28.21613671875" Y="1.129361083984" />
                  <Point X="28.234349609375" Y="1.116033813477" />
                  <Point X="28.2590625" Y="1.102123535156" />
                  <Point X="28.3039921875" Y="1.076831298828" />
                  <Point X="28.320521484375" Y="1.069501708984" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.38953125" Y="1.055023071289" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.402837890625" Y="1.16739831543" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.782009765625" Y="1.195397827148" />
                  <Point X="29.84594140625" Y="0.932788757324" />
                  <Point X="29.868380859375" Y="0.788661376953" />
                  <Point X="29.890865234375" Y="0.644238891602" />
                  <Point X="29.1778828125" Y="0.453195373535" />
                  <Point X="28.716580078125" Y="0.329589874268" />
                  <Point X="28.704791015625" Y="0.325586120605" />
                  <Point X="28.681544921875" Y="0.315067504883" />
                  <Point X="28.648720703125" Y="0.296093841553" />
                  <Point X="28.58903515625" Y="0.261595062256" />
                  <Point X="28.5743125" Y="0.251096801758" />
                  <Point X="28.54753125" Y="0.225576583862" />
                  <Point X="28.5278359375" Y="0.200480163574" />
                  <Point X="28.492025390625" Y="0.154848937988" />
                  <Point X="28.48030078125" Y="0.135568222046" />
                  <Point X="28.47052734375" Y="0.114104911804" />
                  <Point X="28.463681640625" Y="0.092602737427" />
                  <Point X="28.4571171875" Y="0.058322551727" />
                  <Point X="28.4451796875" Y="-0.004007742882" />
                  <Point X="28.443484375" Y="-0.021874988556" />
                  <Point X="28.4451796875" Y="-0.058552253723" />
                  <Point X="28.451744140625" Y="-0.092832748413" />
                  <Point X="28.463681640625" Y="-0.155162734985" />
                  <Point X="28.47052734375" Y="-0.176664901733" />
                  <Point X="28.48030078125" Y="-0.198128219604" />
                  <Point X="28.492025390625" Y="-0.217409240723" />
                  <Point X="28.511720703125" Y="-0.242505508423" />
                  <Point X="28.54753125" Y="-0.288136871338" />
                  <Point X="28.560001953125" Y="-0.301237457275" />
                  <Point X="28.589037109375" Y="-0.324155822754" />
                  <Point X="28.62186328125" Y="-0.343129486084" />
                  <Point X="28.681546875" Y="-0.377628234863" />
                  <Point X="28.692708984375" Y="-0.383137786865" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="29.55533984375" Y="-0.616895141602" />
                  <Point X="29.89147265625" Y="-0.706961791992" />
                  <Point X="29.890716796875" Y="-0.711980102539" />
                  <Point X="29.855021484375" Y="-0.948747192383" />
                  <Point X="29.826271484375" Y="-1.074726806641" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="28.95936328125" Y="-1.073872436523" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.310234375" Y="-1.01951184082" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.1639765625" Y="-1.056596313477" />
                  <Point X="28.1361484375" Y="-1.073489013672" />
                  <Point X="28.1123984375" Y="-1.093959716797" />
                  <Point X="28.07345703125" Y="-1.14079309082" />
                  <Point X="28.002654296875" Y="-1.225947631836" />
                  <Point X="27.987931640625" Y="-1.250335449219" />
                  <Point X="27.97658984375" Y="-1.277720947266" />
                  <Point X="27.969759765625" Y="-1.305364624023" />
                  <Point X="27.964177734375" Y="-1.366016113281" />
                  <Point X="27.95403125" Y="-1.476294921875" />
                  <Point X="27.95634765625" Y="-1.507562011719" />
                  <Point X="27.964078125" Y="-1.539182983398" />
                  <Point X="27.976451171875" Y="-1.567995727539" />
                  <Point X="28.012103515625" Y="-1.623452758789" />
                  <Point X="28.076931640625" Y="-1.724286254883" />
                  <Point X="28.0869375" Y="-1.737243408203" />
                  <Point X="28.110630859375" Y="-1.760909423828" />
                  <Point X="28.88900390625" Y="-2.358177246094" />
                  <Point X="29.213125" Y="-2.6068828125" />
                  <Point X="29.124822265625" Y="-2.749768310547" />
                  <Point X="29.065345703125" Y="-2.83427734375" />
                  <Point X="29.028982421875" Y="-2.885945068359" />
                  <Point X="28.277427734375" Y="-2.452034912109" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.677548828125" Y="-2.145977783203" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135176513672" />
                  <Point X="27.381134765625" Y="-2.168700439453" />
                  <Point X="27.26531640625" Y="-2.229655517578" />
                  <Point X="27.242384765625" Y="-2.246549804688" />
                  <Point X="27.22142578125" Y="-2.267509521484" />
                  <Point X="27.20453515625" Y="-2.290437744141" />
                  <Point X="27.171009765625" Y="-2.354136230469" />
                  <Point X="27.1100546875" Y="-2.469955810547" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531908691406" />
                  <Point X="27.09567578125" Y="-2.563259033203" />
                  <Point X="27.1095234375" Y="-2.639934570312" />
                  <Point X="27.134703125" Y="-2.779349365234" />
                  <Point X="27.13898828125" Y="-2.795140869141" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.652005859375" Y="-3.692422607422" />
                  <Point X="27.86128515625" Y="-4.054904785156" />
                  <Point X="27.78184765625" Y="-4.111645996094" />
                  <Point X="27.7153671875" Y="-4.154677734375" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="27.122140625" Y="-3.408097900391" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.646302734375" Y="-2.851938720703" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.33439453125" Y="-2.748727539062" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.040734375" Y="-2.848561767578" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968860839844" />
                  <Point X="25.88725" Y="-2.996687255859" />
                  <Point X="25.875625" Y="-3.025809570312" />
                  <Point X="25.85653125" Y="-3.113661376953" />
                  <Point X="25.821810546875" Y="-3.273397460938" />
                  <Point X="25.819724609375" Y="-3.289627197266" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.9614921875" Y="-4.399805175781" />
                  <Point X="26.02206640625" Y="-4.859915527344" />
                  <Point X="25.97569140625" Y="-4.870080566406" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941572265625" Y="-4.752636230469" />
                  <Point X="23.85875390625" Y="-4.731328613281" />
                  <Point X="23.862115234375" Y="-4.705801757812" />
                  <Point X="23.8792265625" Y="-4.575837402344" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030761719" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688476562" />
                  <Point X="23.8551875" Y="-4.389716308594" />
                  <Point X="23.81613671875" Y="-4.193396972656" />
                  <Point X="23.811873046875" Y="-4.178467285156" />
                  <Point X="23.800970703125" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135464355469" />
                  <Point X="23.778259765625" Y="-4.107626953125" />
                  <Point X="23.76942578125" Y="-4.094861572266" />
                  <Point X="23.74979296875" Y="-4.070938232422" />
                  <Point X="23.738994140625" Y="-4.059780273438" />
                  <Point X="23.6562265625" Y="-3.987194091797" />
                  <Point X="23.505734375" Y="-3.855215332031" />
                  <Point X="23.49326171875" Y="-3.845965087891" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822527587891" />
                  <Point X="23.423470703125" Y="-3.810226318359" />
                  <Point X="23.4086875" Y="-3.805476318359" />
                  <Point X="23.378544921875" Y="-3.798447998047" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.253333984375" Y="-3.788969482422" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.808270019531" />
                  <Point X="22.9045625" Y="-3.8158125" />
                  <Point X="22.813029296875" Y="-3.876973876953" />
                  <Point X="22.64659765625" Y="-3.988180664062" />
                  <Point X="22.640322265625" Y="-3.992754882812" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032771484375" />
                  <Point X="22.589529296875" Y="-4.04162890625" />
                  <Point X="22.496796875" Y="-4.162478515625" />
                  <Point X="22.252404296875" Y="-4.011156005859" />
                  <Point X="22.116814453125" Y="-3.906755371094" />
                  <Point X="22.01913671875" Y="-3.831546630859" />
                  <Point X="22.394361328125" Y="-3.181637939453" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.710084716797" />
                  <Point X="22.67605078125" Y="-2.681119140625" />
                  <Point X="22.680314453125" Y="-2.666188476562" />
                  <Point X="22.6865859375" Y="-2.634661132812" />
                  <Point X="22.688361328125" Y="-2.619231933594" />
                  <Point X="22.689373046875" Y="-2.588295654297" />
                  <Point X="22.685333984375" Y="-2.557615234375" />
                  <Point X="22.676349609375" Y="-2.527995605469" />
                  <Point X="22.670640625" Y="-2.513549316406" />
                  <Point X="22.656421875" Y="-2.484719970703" />
                  <Point X="22.64844140625" Y="-2.471405517578" />
                  <Point X="22.63041796875" Y="-2.446254150391" />
                  <Point X="22.620375" Y="-2.434417236328" />
                  <Point X="22.60303125" Y="-2.417072265625" />
                  <Point X="22.59119140625" Y="-2.407027099609" />
                  <Point X="22.56603515625" Y="-2.388998779297" />
                  <Point X="22.55271875" Y="-2.381015625" />
                  <Point X="22.523888671875" Y="-2.366796630859" />
                  <Point X="22.509447265625" Y="-2.361089355469" />
                  <Point X="22.479828125" Y="-2.352103515625" />
                  <Point X="22.449140625" Y="-2.348062744141" />
                  <Point X="22.418205078125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.4814375" Y="-2.859211425781" />
                  <Point X="21.206912109375" Y="-3.017708740234" />
                  <Point X="20.99598046875" Y="-2.740587402344" />
                  <Point X="20.89876953125" Y="-2.577580810547" />
                  <Point X="20.818734375" Y="-2.443373535156" />
                  <Point X="21.489703125" Y="-1.928520629883" />
                  <Point X="21.951876953125" Y="-1.573881835938" />
                  <Point X="21.963517578125" Y="-1.563308959961" />
                  <Point X="21.98489453125" Y="-1.540388793945" />
                  <Point X="21.994630859375" Y="-1.528041137695" />
                  <Point X="22.012595703125" Y="-1.500909667969" />
                  <Point X="22.020162109375" Y="-1.487126953125" />
                  <Point X="22.03291796875" Y="-1.458499145508" />
                  <Point X="22.038107421875" Y="-1.443654663086" />
                  <Point X="22.0458125" Y="-1.413907348633" />
                  <Point X="22.048447265625" Y="-1.398805541992" />
                  <Point X="22.051251953125" Y="-1.36837487793" />
                  <Point X="22.051421875" Y="-1.353046142578" />
                  <Point X="22.049212890625" Y="-1.321375366211" />
                  <Point X="22.04691796875" Y="-1.306222412109" />
                  <Point X="22.03991796875" Y="-1.276478271484" />
                  <Point X="22.02822265625" Y="-1.248243530273" />
                  <Point X="22.012140625" Y="-1.222260498047" />
                  <Point X="22.003048828125" Y="-1.209920654297" />
                  <Point X="21.982216796875" Y="-1.185964111328" />
                  <Point X="21.97125390625" Y="-1.175243774414" />
                  <Point X="21.947751953125" Y="-1.155709106445" />
                  <Point X="21.935212890625" Y="-1.14689440918" />
                  <Point X="21.90873046875" Y="-1.131308105469" />
                  <Point X="21.894568359375" Y="-1.124481689453" />
                  <Point X="21.865306640625" Y="-1.113257568359" />
                  <Point X="21.85020703125" Y="-1.108859741211" />
                  <Point X="21.8183203125" Y="-1.102378417969" />
                  <Point X="21.802703125" Y="-1.100532348633" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="20.705501953125" Y="-1.238454223633" />
                  <Point X="20.339080078125" Y="-1.286694458008" />
                  <Point X="20.259236328125" Y="-0.974107727051" />
                  <Point X="20.23351953125" Y="-0.794289001465" />
                  <Point X="20.213548828125" Y="-0.654654418945" />
                  <Point X="20.9652265625" Y="-0.453242675781" />
                  <Point X="21.491712890625" Y="-0.312171203613" />
                  <Point X="21.502669921875" Y="-0.308508117676" />
                  <Point X="21.524056640625" Y="-0.299879364014" />
                  <Point X="21.534486328125" Y="-0.294914031982" />
                  <Point X="21.559900390625" Y="-0.280859649658" />
                  <Point X="21.576283203125" Y="-0.270682403564" />
                  <Point X="21.594189453125" Y="-0.258255157471" />
                  <Point X="21.603525390625" Y="-0.250867553711" />
                  <Point X="21.627484375" Y="-0.229335372925" />
                  <Point X="21.642431640625" Y="-0.212256973267" />
                  <Point X="21.663625" Y="-0.181226760864" />
                  <Point X="21.672673828125" Y="-0.164650405884" />
                  <Point X="21.683380859375" Y="-0.139333099365" />
                  <Point X="21.689845703125" Y="-0.121646682739" />
                  <Point X="21.6958125" Y="-0.102422172546" />
                  <Point X="21.698421875" Y="-0.091946784973" />
                  <Point X="21.703865234375" Y="-0.063217250824" />
                  <Point X="21.7054765625" Y="-0.042502048492" />
                  <Point X="21.704376953125" Y="-0.008046369553" />
                  <Point X="21.702251953125" Y="0.009129823685" />
                  <Point X="21.69672265625" Y="0.03453080368" />
                  <Point X="21.69253125" Y="0.050438949585" />
                  <Point X="21.6865625" Y="0.069669700623" />
                  <Point X="21.682330078125" Y="0.080789070129" />
                  <Point X="21.669013671875" Y="0.110113250732" />
                  <Point X="21.6571328125" Y="0.129632781982" />
                  <Point X="21.6336953125" Y="0.15937487793" />
                  <Point X="21.62048046875" Y="0.173064682007" />
                  <Point X="21.598814453125" Y="0.19141708374" />
                  <Point X="21.584341796875" Y="0.202529754639" />
                  <Point X="21.566435546875" Y="0.214957000732" />
                  <Point X="21.559509765625" Y="0.219333053589" />
                  <Point X="21.534384765625" Y="0.23283454895" />
                  <Point X="21.503435546875" Y="0.2456355896" />
                  <Point X="21.491712890625" Y="0.249611236572" />
                  <Point X="20.53443359375" Y="0.506113769531" />
                  <Point X="20.2145546875" Y="0.591825073242" />
                  <Point X="20.26866796875" Y="0.957520019531" />
                  <Point X="20.3204453125" Y="1.148593505859" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="20.844537109375" Y="1.255291137695" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.20470324707" />
                  <Point X="21.28485546875" Y="1.206589477539" />
                  <Point X="21.295111328125" Y="1.208053710938" />
                  <Point X="21.315400390625" Y="1.212089477539" />
                  <Point X="21.32543359375" Y="1.214661254883" />
                  <Point X="21.347228515625" Y="1.221533569336" />
                  <Point X="21.386859375" Y="1.234028808594" />
                  <Point X="21.396552734375" Y="1.237677246094" />
                  <Point X="21.415486328125" Y="1.246008666992" />
                  <Point X="21.4247265625" Y="1.25069140625" />
                  <Point X="21.443470703125" Y="1.261513916016" />
                  <Point X="21.452142578125" Y="1.267172119141" />
                  <Point X="21.468822265625" Y="1.279401733398" />
                  <Point X="21.484072265625" Y="1.293376220703" />
                  <Point X="21.497708984375" Y="1.30892565918" />
                  <Point X="21.504103515625" Y="1.317071777344" />
                  <Point X="21.51651953125" Y="1.334802368164" />
                  <Point X="21.5219921875" Y="1.343602905273" />
                  <Point X="21.53194140625" Y="1.361739013672" />
                  <Point X="21.53641796875" Y="1.371074462891" />
                  <Point X="21.5451640625" Y="1.392188232422" />
                  <Point X="21.561064453125" Y="1.430578735352" />
                  <Point X="21.564501953125" Y="1.440354248047" />
                  <Point X="21.57029296875" Y="1.460224609375" />
                  <Point X="21.572646484375" Y="1.470319580078" />
                  <Point X="21.57640234375" Y="1.491634155273" />
                  <Point X="21.577640625" Y="1.501920043945" />
                  <Point X="21.578990234375" Y="1.522554931641" />
                  <Point X="21.578087890625" Y="1.543201293945" />
                  <Point X="21.574943359375" Y="1.563639648438" />
                  <Point X="21.5728125" Y="1.573780761719" />
                  <Point X="21.5672109375" Y="1.594686523438" />
                  <Point X="21.563986328125" Y="1.604533569336" />
                  <Point X="21.55648828125" Y="1.623812988281" />
                  <Point X="21.55221484375" Y="1.633245361328" />
                  <Point X="21.541662109375" Y="1.653516601562" />
                  <Point X="21.522474609375" Y="1.690374755859" />
                  <Point X="21.517201171875" Y="1.699284301758" />
                  <Point X="21.5057109375" Y="1.716481933594" />
                  <Point X="21.499494140625" Y="1.724769897461" />
                  <Point X="21.48558203125" Y="1.741350219727" />
                  <Point X="21.478498046875" Y="1.748912597656" />
                  <Point X="21.4635546875" Y="1.763216796875" />
                  <Point X="21.4556953125" Y="1.769958618164" />
                  <Point X="20.90659765625" Y="2.191296630859" />
                  <Point X="20.772384765625" Y="2.294281494141" />
                  <Point X="20.786412109375" Y="2.318312744141" />
                  <Point X="20.99771484375" Y="2.680320556641" />
                  <Point X="21.1348515625" Y="2.856591796875" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.514056640625" Y="2.896220947266" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.875283203125" Y="2.728502197266" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.961505859375" Y="2.723785644531" />
                  <Point X="21.97183203125" Y="2.724575683594" />
                  <Point X="21.993291015625" Y="2.727400634766" />
                  <Point X="22.00347265625" Y="2.729310546875" />
                  <Point X="22.023564453125" Y="2.734227050781" />
                  <Point X="22.043" Y="2.741301025391" />
                  <Point X="22.061552734375" Y="2.750449951172" />
                  <Point X="22.070580078125" Y="2.755531494141" />
                  <Point X="22.088833984375" Y="2.767160644531" />
                  <Point X="22.09725390625" Y="2.773193115234" />
                  <Point X="22.113384765625" Y="2.786139404297" />
                  <Point X="22.121095703125" Y="2.793053222656" />
                  <Point X="22.142642578125" Y="2.814599853516" />
                  <Point X="22.1818203125" Y="2.853776611328" />
                  <Point X="22.188732421875" Y="2.861485839844" />
                  <Point X="22.201681640625" Y="2.877618652344" />
                  <Point X="22.20771875" Y="2.886042236328" />
                  <Point X="22.21934765625" Y="2.904296386719" />
                  <Point X="22.2244296875" Y="2.913324951172" />
                  <Point X="22.233576171875" Y="2.931874023438" />
                  <Point X="22.240646484375" Y="2.951298583984" />
                  <Point X="22.245564453125" Y="2.971387695312" />
                  <Point X="22.2474765625" Y="2.981572753906" />
                  <Point X="22.250302734375" Y="3.003031494141" />
                  <Point X="22.251091796875" Y="3.013364501953" />
                  <Point X="22.25154296875" Y="3.034050292969" />
                  <Point X="22.251205078125" Y="3.044403076172" />
                  <Point X="22.248548828125" Y="3.074758789062" />
                  <Point X="22.243720703125" Y="3.129952636719" />
                  <Point X="22.242255859375" Y="3.140209960938" />
                  <Point X="22.23821875" Y="3.160500244141" />
                  <Point X="22.235646484375" Y="3.170533203125" />
                  <Point X="22.22913671875" Y="3.191176025391" />
                  <Point X="22.22548828125" Y="3.200870605469" />
                  <Point X="22.217158203125" Y="3.219799072266" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="21.969154296875" Y="3.650479003906" />
                  <Point X="21.94061328125" Y="3.699915527344" />
                  <Point X="21.983623046875" Y="3.732891601562" />
                  <Point X="22.3516328125" Y="4.015040527344" />
                  <Point X="22.5676171875" Y="4.135037109375" />
                  <Point X="22.807474609375" Y="4.268296386719" />
                  <Point X="22.810478515625" Y="4.2643828125" />
                  <Point X="22.881435546875" Y="4.171909667969" />
                  <Point X="22.8881796875" Y="4.164048828125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.934908203125" Y="4.121897460938" />
                  <Point X="22.952107421875" Y="4.110405273438" />
                  <Point X="22.96101953125" Y="4.105129394531" />
                  <Point X="22.9948046875" Y="4.087541503906" />
                  <Point X="23.056236328125" Y="4.0555625" />
                  <Point X="23.065673828125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.0437890625" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.229271484375" Y="4.037488525391" />
                  <Point X="23.2491328125" Y="4.043277587891" />
                  <Point X="23.258904296875" Y="4.046714599609" />
                  <Point X="23.29409375" Y="4.061291259766" />
                  <Point X="23.358078125" Y="4.087794189453" />
                  <Point X="23.367416015625" Y="4.092272705078" />
                  <Point X="23.385546875" Y="4.102219238281" />
                  <Point X="23.39433984375" Y="4.1076875" />
                  <Point X="23.4120703125" Y="4.1201015625" />
                  <Point X="23.420216796875" Y="4.126494140625" />
                  <Point X="23.4357734375" Y="4.140135253906" />
                  <Point X="23.4497578125" Y="4.155395507812" />
                  <Point X="23.461990234375" Y="4.172081054688" />
                  <Point X="23.4676484375" Y="4.180754882812" />
                  <Point X="23.47846875" Y="4.199499023438" />
                  <Point X="23.483150390625" Y="4.208736328125" />
                  <Point X="23.491478515625" Y="4.227665527344" />
                  <Point X="23.495125" Y="4.237357421875" />
                  <Point X="23.506578125" Y="4.273684082031" />
                  <Point X="23.527404296875" Y="4.339733886719" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370048339844" />
                  <Point X="23.535474609375" Y="4.380302246094" />
                  <Point X="23.537361328125" Y="4.401863769531" />
                  <Point X="23.53769921875" Y="4.412216796875" />
                  <Point X="23.537248046875" Y="4.4328984375" />
                  <Point X="23.536458984375" Y="4.443227050781" />
                  <Point X="23.520736328125" Y="4.562654785156" />
                  <Point X="23.592423828125" Y="4.58275390625" />
                  <Point X="24.068826171875" Y="4.7163203125" />
                  <Point X="24.330662109375" Y="4.746964355469" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.705763671875" Y="4.517631347656" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.021630859375" Y="4.085113037109" />
                  <Point X="25.05216796875" Y="4.090154541016" />
                  <Point X="25.06723046875" Y="4.093927246094" />
                  <Point X="25.09766796875" Y="4.104259277344" />
                  <Point X="25.1119140625" Y="4.110436035156" />
                  <Point X="25.13920703125" Y="4.125024902344" />
                  <Point X="25.1637578125" Y="4.143861328125" />
                  <Point X="25.184916015625" Y="4.16644921875" />
                  <Point X="25.1945703125" Y="4.178612792969" />
                  <Point X="25.2124296875" Y="4.205339355469" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.24710546875" />
                  <Point X="25.23798046875" Y="4.261727050781" />
                  <Point X="25.36265625" Y="4.727022460938" />
                  <Point X="25.378193359375" Y="4.785005859375" />
                  <Point X="25.4118984375" Y="4.781476074219" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.044513671875" Y="4.685609375" />
                  <Point X="26.453603515625" Y="4.586841796875" />
                  <Point X="26.5926796875" Y="4.536398925781" />
                  <Point X="26.85825" Y="4.440074707031" />
                  <Point X="26.99461328125" Y="4.376301757813" />
                  <Point X="27.250451171875" Y="4.256655273438" />
                  <Point X="27.382234375" Y="4.179877929687" />
                  <Point X="27.62943359375" Y="4.035859130859" />
                  <Point X="27.753671875" Y="3.947507324219" />
                  <Point X="27.81778125" Y="3.901916503906" />
                  <Point X="27.37104296875" Y="3.128141357422" />
                  <Point X="27.065310546875" Y="2.598597900391" />
                  <Point X="27.062380859375" Y="2.593116699219" />
                  <Point X="27.053181640625" Y="2.573438476562" />
                  <Point X="27.04418359375" Y="2.549564697266" />
                  <Point X="27.041306640625" Y="2.540604980469" />
                  <Point X="27.0336875" Y="2.512116943359" />
                  <Point X="27.0198359375" Y="2.460318603516" />
                  <Point X="27.017916015625" Y="2.451479736328" />
                  <Point X="27.013646484375" Y="2.420222900391" />
                  <Point X="27.012755859375" Y="2.383239501953" />
                  <Point X="27.013412109375" Y="2.369578369141" />
                  <Point X="27.0163828125" Y="2.344944335938" />
                  <Point X="27.021783203125" Y="2.300153564453" />
                  <Point X="27.02380078125" Y="2.289034667969" />
                  <Point X="27.029142578125" Y="2.267114746094" />
                  <Point X="27.032466796875" Y="2.256313720703" />
                  <Point X="27.040732421875" Y="2.234224609375" />
                  <Point X="27.04531640625" Y="2.223892333984" />
                  <Point X="27.055681640625" Y="2.203841796875" />
                  <Point X="27.061462890625" Y="2.194123535156" />
                  <Point X="27.07670703125" Y="2.171659667969" />
                  <Point X="27.104421875" Y="2.130814941406" />
                  <Point X="27.109814453125" Y="2.123625244141" />
                  <Point X="27.13046484375" Y="2.100122314453" />
                  <Point X="27.15759765625" Y="2.075387451172" />
                  <Point X="27.1682578125" Y="2.066983154297" />
                  <Point X="27.190720703125" Y="2.051740478516" />
                  <Point X="27.23156640625" Y="2.024025512695" />
                  <Point X="27.241279296875" Y="2.018246459961" />
                  <Point X="27.261326171875" Y="2.007882324219" />
                  <Point X="27.27166015625" Y="2.003297485352" />
                  <Point X="27.293748046875" Y="1.995031860352" />
                  <Point X="27.304548828125" Y="1.991707519531" />
                  <Point X="27.326470703125" Y="1.986364990234" />
                  <Point X="27.337591796875" Y="1.984346923828" />
                  <Point X="27.362224609375" Y="1.981376464844" />
                  <Point X="27.407015625" Y="1.975975341797" />
                  <Point X="27.416044921875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975497436523" />
                  <Point X="27.4843125" Y="1.979822998047" />
                  <Point X="27.49774609375" Y="1.982395874023" />
                  <Point X="27.526234375" Y="1.990014038086" />
                  <Point X="27.578033203125" Y="2.003865722656" />
                  <Point X="27.5839921875" Y="2.005670288086" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.598875" Y="2.583768310547" />
                  <Point X="28.94040625" Y="2.780951171875" />
                  <Point X="29.043951171875" Y="2.637046875" />
                  <Point X="29.11321484375" Y="2.522588623047" />
                  <Point X="29.136884765625" Y="2.483471679688" />
                  <Point X="28.575478515625" Y="2.052687988281" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.168140625" Y="1.739869384766" />
                  <Point X="28.152126953125" Y="1.725223144531" />
                  <Point X="28.134669921875" Y="1.706604248047" />
                  <Point X="28.12857421875" Y="1.699419189453" />
                  <Point X="28.108072265625" Y="1.67267175293" />
                  <Point X="28.07079296875" Y="1.624037841797" />
                  <Point X="28.0656328125" Y="1.616598022461" />
                  <Point X="28.04973828125" Y="1.589367797852" />
                  <Point X="28.034765625" Y="1.555549072266" />
                  <Point X="28.03014453125" Y="1.542679931641" />
                  <Point X="28.022505859375" Y="1.515370117188" />
                  <Point X="28.008619140625" Y="1.465715332031" />
                  <Point X="28.0062265625" Y="1.454670043945" />
                  <Point X="28.002771484375" Y="1.432368652344" />
                  <Point X="28.001708984375" Y="1.421112548828" />
                  <Point X="28.000892578125" Y="1.397541625977" />
                  <Point X="28.001173828125" Y="1.386244506836" />
                  <Point X="28.003076171875" Y="1.363758666992" />
                  <Point X="28.004697265625" Y="1.352569946289" />
                  <Point X="28.010966796875" Y="1.322184570312" />
                  <Point X="28.0223671875" Y="1.266937011719" />
                  <Point X="28.02459765625" Y="1.258233154297" />
                  <Point X="28.034685546875" Y="1.228605712891" />
                  <Point X="28.0502890625" Y="1.195368896484" />
                  <Point X="28.056919921875" Y="1.183525512695" />
                  <Point X="28.07397265625" Y="1.157606445312" />
                  <Point X="28.1049765625" Y="1.110480102539" />
                  <Point X="28.11173828125" Y="1.101424316406" />
                  <Point X="28.126291015625" Y="1.084180541992" />
                  <Point X="28.13408203125" Y="1.075992797852" />
                  <Point X="28.151326171875" Y="1.05990234375" />
                  <Point X="28.160037109375" Y="1.052694824219" />
                  <Point X="28.17825" Y="1.039367431641" />
                  <Point X="28.187751953125" Y="1.033247436523" />
                  <Point X="28.21246484375" Y="1.019337097168" />
                  <Point X="28.25739453125" Y="0.994044921875" />
                  <Point X="28.265482421875" Y="0.989986633301" />
                  <Point X="28.2946796875" Y="0.97808416748" />
                  <Point X="28.33027734375" Y="0.968021240234" />
                  <Point X="28.343671875" Y="0.965257568359" />
                  <Point X="28.377083984375" Y="0.96084197998" />
                  <Point X="28.437833984375" Y="0.952812927246" />
                  <Point X="28.444033203125" Y="0.952199707031" />
                  <Point X="28.46571875" Y="0.951222839355" />
                  <Point X="28.49121875" Y="0.952032348633" />
                  <Point X="28.50060546875" Y="0.952797180176" />
                  <Point X="29.41523828125" Y="1.07321105957" />
                  <Point X="29.704703125" Y="1.111319824219" />
                  <Point X="29.752689453125" Y="0.914213684082" />
                  <Point X="29.77451171875" Y="0.774046691895" />
                  <Point X="29.783873046875" Y="0.713921386719" />
                  <Point X="29.153294921875" Y="0.544958251953" />
                  <Point X="28.6919921875" Y="0.421352813721" />
                  <Point X="28.68603125" Y="0.419543792725" />
                  <Point X="28.665626953125" Y="0.412137908936" />
                  <Point X="28.642380859375" Y="0.401619262695" />
                  <Point X="28.634001953125" Y="0.397315460205" />
                  <Point X="28.601177734375" Y="0.378341674805" />
                  <Point X="28.5414921875" Y="0.343842895508" />
                  <Point X="28.533880859375" Y="0.338944274902" />
                  <Point X="28.508775390625" Y="0.319871643066" />
                  <Point X="28.481994140625" Y="0.294351318359" />
                  <Point X="28.472796875" Y="0.28422668457" />
                  <Point X="28.4531015625" Y="0.259130249023" />
                  <Point X="28.417291015625" Y="0.213499008179" />
                  <Point X="28.41085546875" Y="0.204208602905" />
                  <Point X="28.399130859375" Y="0.184927902222" />
                  <Point X="28.393841796875" Y="0.174937484741" />
                  <Point X="28.384068359375" Y="0.153474258423" />
                  <Point X="28.38000390625" Y="0.142925018311" />
                  <Point X="28.373158203125" Y="0.121422706604" />
                  <Point X="28.370376953125" Y="0.110470092773" />
                  <Point X="28.3638125" Y="0.076189788818" />
                  <Point X="28.351875" Y="0.013859597206" />
                  <Point X="28.350603515625" Y="0.004965857506" />
                  <Point X="28.3485859375" Y="-0.026261436462" />
                  <Point X="28.35028125" Y="-0.062938732147" />
                  <Point X="28.351875" Y="-0.076419418335" />
                  <Point X="28.358439453125" Y="-0.110699867249" />
                  <Point X="28.370376953125" Y="-0.173029907227" />
                  <Point X="28.373158203125" Y="-0.183982818604" />
                  <Point X="28.38000390625" Y="-0.205484985352" />
                  <Point X="28.384068359375" Y="-0.216034225464" />
                  <Point X="28.393841796875" Y="-0.23749760437" />
                  <Point X="28.399130859375" Y="-0.24748727417" />
                  <Point X="28.41085546875" Y="-0.266768432617" />
                  <Point X="28.417291015625" Y="-0.276059570312" />
                  <Point X="28.436986328125" Y="-0.301155883789" />
                  <Point X="28.472796875" Y="-0.346787261963" />
                  <Point X="28.47872265625" Y="-0.353637420654" />
                  <Point X="28.501142578125" Y="-0.37580645752" />
                  <Point X="28.530177734375" Y="-0.398724853516" />
                  <Point X="28.54149609375" Y="-0.406404937744" />
                  <Point X="28.574322265625" Y="-0.425378570557" />
                  <Point X="28.634005859375" Y="-0.459877349854" />
                  <Point X="28.639498046875" Y="-0.462815917969" />
                  <Point X="28.659154296875" Y="-0.472014923096" />
                  <Point X="28.683025390625" Y="-0.481026977539" />
                  <Point X="28.6919921875" Y="-0.483912780762" />
                  <Point X="29.530751953125" Y="-0.70865802002" />
                  <Point X="29.78487890625" Y="-0.776751403809" />
                  <Point X="29.761615234375" Y="-0.931057373047" />
                  <Point X="29.73365234375" Y="-1.053590087891" />
                  <Point X="29.727802734375" Y="-1.079219848633" />
                  <Point X="28.971763671875" Y="-0.979685119629" />
                  <Point X="28.436783203125" Y="-0.909253662109" />
                  <Point X="28.428625" Y="-0.908535705566" />
                  <Point X="28.40009765625" Y="-0.908042419434" />
                  <Point X="28.36672265625" Y="-0.910840942383" />
                  <Point X="28.354482421875" Y="-0.912676147461" />
                  <Point X="28.290056640625" Y="-0.926679260254" />
                  <Point X="28.17291796875" Y="-0.952140014648" />
                  <Point X="28.157875" Y="-0.956742248535" />
                  <Point X="28.128755859375" Y="-0.968366027832" />
                  <Point X="28.1146796875" Y="-0.975387695312" />
                  <Point X="28.0868515625" Y="-0.992280334473" />
                  <Point X="28.074125" Y="-1.001529907227" />
                  <Point X="28.050375" Y="-1.022000610352" />
                  <Point X="28.0393515625" Y="-1.033221801758" />
                  <Point X="28.00041015625" Y="-1.080055175781" />
                  <Point X="27.929607421875" Y="-1.165209716797" />
                  <Point X="27.92132421875" Y="-1.176850097656" />
                  <Point X="27.9066015625" Y="-1.201237915039" />
                  <Point X="27.900162109375" Y="-1.213985107422" />
                  <Point X="27.8888203125" Y="-1.241370483398" />
                  <Point X="27.88436328125" Y="-1.254933959961" />
                  <Point X="27.877533203125" Y="-1.282577636719" />
                  <Point X="27.87516015625" Y="-1.296658081055" />
                  <Point X="27.869578125" Y="-1.357309692383" />
                  <Point X="27.859431640625" Y="-1.467588378906" />
                  <Point X="27.859291015625" Y="-1.483313720703" />
                  <Point X="27.861607421875" Y="-1.514580810547" />
                  <Point X="27.864064453125" Y="-1.530122680664" />
                  <Point X="27.871794921875" Y="-1.561743530273" />
                  <Point X="27.876787109375" Y="-1.576668579102" />
                  <Point X="27.88916015625" Y="-1.605481445313" />
                  <Point X="27.896541015625" Y="-1.619369140625" />
                  <Point X="27.932193359375" Y="-1.674826171875" />
                  <Point X="27.997021484375" Y="-1.775659545898" />
                  <Point X="28.0017421875" Y="-1.782350219727" />
                  <Point X="28.01980078125" Y="-1.80445715332" />
                  <Point X="28.043494140625" Y="-1.828123291016" />
                  <Point X="28.052798828125" Y="-1.836277954102" />
                  <Point X="28.831171875" Y="-2.433545654297" />
                  <Point X="29.087173828125" Y="-2.629981445312" />
                  <Point X="29.045498046875" Y="-2.697417236328" />
                  <Point X="29.001275390625" Y="-2.760252197266" />
                  <Point X="28.324927734375" Y="-2.369762451172" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.694431640625" Y="-2.052490234375" />
                  <Point X="27.555017578125" Y="-2.027312011719" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.444845703125" Y="-2.035136108398" />
                  <Point X="27.4150703125" Y="-2.044959594727" />
                  <Point X="27.40058984375" Y="-2.051108154297" />
                  <Point X="27.336890625" Y="-2.084632080078" />
                  <Point X="27.221072265625" Y="-2.145587158203" />
                  <Point X="27.20896875" Y="-2.153170898438" />
                  <Point X="27.186037109375" Y="-2.170065185547" />
                  <Point X="27.175208984375" Y="-2.179375732422" />
                  <Point X="27.15425" Y="-2.200335449219" />
                  <Point X="27.144939453125" Y="-2.2111640625" />
                  <Point X="27.128048828125" Y="-2.234092285156" />
                  <Point X="27.12046875" Y="-2.246191894531" />
                  <Point X="27.086943359375" Y="-2.309890380859" />
                  <Point X="27.02598828125" Y="-2.425709960938" />
                  <Point X="27.01983984375" Y="-2.440187988281" />
                  <Point X="27.010013671875" Y="-2.469967529297" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517442382812" />
                  <Point X="27.000279296875" Y="-2.533133789062" />
                  <Point X="27.00068359375" Y="-2.564484130859" />
                  <Point X="27.0021875" Y="-2.580143066406" />
                  <Point X="27.01603515625" Y="-2.656818603516" />
                  <Point X="27.04121484375" Y="-2.796233398438" />
                  <Point X="27.04301953125" Y="-2.804228759766" />
                  <Point X="27.05123828125" Y="-2.831542236328" />
                  <Point X="27.064072265625" Y="-2.862479980469" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.569734375" Y="-3.739922607422" />
                  <Point X="27.735896484375" Y="-4.027723144531" />
                  <Point X="27.7237578125" Y="-4.036082763672" />
                  <Point X="27.197509765625" Y="-3.350265380859" />
                  <Point X="26.83391796875" Y="-2.876422607422" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.697677734375" Y="-2.772028564453" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.325689453125" Y="-2.654127197266" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.979998046875" Y="-2.775513916016" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883087158203" />
                  <Point X="25.83218359375" Y="-2.906836425781" />
                  <Point X="25.822935546875" Y="-2.919562255859" />
                  <Point X="25.80604296875" Y="-2.947388671875" />
                  <Point X="25.79901953125" Y="-2.961467529297" />
                  <Point X="25.78739453125" Y="-2.99058984375" />
                  <Point X="25.78279296875" Y="-3.005633300781" />
                  <Point X="25.76369921875" Y="-3.093485107422" />
                  <Point X="25.728978515625" Y="-3.253221191406" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677001953" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520263672" />
                  <Point X="25.83308984375" Y="-4.152324707031" />
                  <Point X="25.79720703125" Y="-4.018405273438" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480122314453" />
                  <Point X="25.642146484375" Y="-3.453578125" />
                  <Point X="25.6267890625" Y="-3.423815185547" />
                  <Point X="25.62041015625" Y="-3.413210205078" />
                  <Point X="25.562314453125" Y="-3.329505371094" />
                  <Point X="25.456681640625" Y="-3.177309814453" />
                  <Point X="25.446671875" Y="-3.165172607422" />
                  <Point X="25.424787109375" Y="-3.142716552734" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104936767578" />
                  <Point X="25.3452421875" Y="-3.090829345703" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.2407578125" Y="-3.057037353516" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.998840087891" />
                  <Point X="24.948935546875" Y="-3.003109619141" />
                  <Point X="24.935015625" Y="-3.006305664062" />
                  <Point X="24.8451171875" Y="-3.03420703125" />
                  <Point X="24.68165625" Y="-3.084938964844" />
                  <Point X="24.6670703125" Y="-3.090829345703" />
                  <Point X="24.6390703125" Y="-3.104936767578" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.5875234375" Y="-3.142718017578" />
                  <Point X="24.565638671875" Y="-3.165175048828" />
                  <Point X="24.555630859375" Y="-3.177311767578" />
                  <Point X="24.497537109375" Y="-3.261016357422" />
                  <Point X="24.391904296875" Y="-3.413212158203" />
                  <Point X="24.38753125" Y="-3.420131347656" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.098802734375" Y="-4.452458007812" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.666131972092" Y="-3.960983571788" />
                  <Point X="27.688134761756" Y="-3.944997609362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.608279680545" Y="-3.885589264077" />
                  <Point X="27.640373039138" Y="-3.862272074193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.550427388998" Y="-3.810194956366" />
                  <Point X="27.592611316519" Y="-3.779546539023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.99808493309" Y="-2.758410184595" />
                  <Point X="29.007266760975" Y="-2.751739196152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.492575097451" Y="-3.734800648655" />
                  <Point X="27.544849647312" Y="-3.696820965049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.908026589695" Y="-2.706414943211" />
                  <Point X="29.051210319916" Y="-2.602385873887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.434722805904" Y="-3.659406340944" />
                  <Point X="27.497088027205" Y="-3.6140953554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.817968246299" Y="-2.654419701827" />
                  <Point X="28.972604463399" Y="-2.542069913735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.376870514357" Y="-3.584012033233" />
                  <Point X="27.449326407099" Y="-3.531369745751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.727909902904" Y="-2.602424460443" />
                  <Point X="28.893998606881" Y="-2.481753953583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.31901822281" Y="-3.508617725521" />
                  <Point X="27.401564786993" Y="-3.448644136102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.637851559508" Y="-2.550429219059" />
                  <Point X="28.815392823123" Y="-2.421437940568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.261165931264" Y="-3.43322341781" />
                  <Point X="27.353803166887" Y="-3.365918526453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.547793216113" Y="-2.498433977675" />
                  <Point X="28.736787329066" Y="-2.361121717072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.203313639717" Y="-3.357829110099" />
                  <Point X="27.306041546781" Y="-3.283192916804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.457734872717" Y="-2.446438736292" />
                  <Point X="28.658181835009" Y="-2.300805493577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.145461516963" Y="-3.282434679753" />
                  <Point X="27.258279926675" Y="-3.200467307155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.367676529322" Y="-2.394443494908" />
                  <Point X="28.579576340952" Y="-2.240489270082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.82550403699" Y="-4.124013466249" />
                  <Point X="25.829025715019" Y="-4.121454817391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.087609413031" Y="-3.207040235731" />
                  <Point X="27.210518306569" Y="-3.117741697506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.277618186546" Y="-2.342448253073" />
                  <Point X="28.500970846895" Y="-2.180173046586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.799167414487" Y="-4.025721684679" />
                  <Point X="25.814915755778" Y="-4.014279844986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.0297573091" Y="-3.13164579171" />
                  <Point X="27.162756686463" Y="-3.035016087857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.187559844332" Y="-2.290453010831" />
                  <Point X="28.422365352838" Y="-2.119856823091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.772830323271" Y="-3.927430243649" />
                  <Point X="25.800805796537" Y="-3.907104872581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.971905205168" Y="-3.056251347688" />
                  <Point X="27.114995066356" Y="-2.952290478208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.097501502117" Y="-2.238457768589" />
                  <Point X="28.343759858782" Y="-2.059540599596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.698386055899" Y="-1.07534705784" />
                  <Point X="29.734709807396" Y="-1.0489563076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.74649319436" Y="-3.829138830006" />
                  <Point X="25.786695837296" Y="-3.799929900175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.914053101236" Y="-2.980856903667" />
                  <Point X="27.067480802525" Y="-2.869385153707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.007443159903" Y="-2.186462526347" />
                  <Point X="28.265154364725" Y="-1.9992243761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.561556546987" Y="-1.057333057288" />
                  <Point X="29.764844874699" Y="-0.909635441758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.72015606545" Y="-3.730847416363" />
                  <Point X="25.772585878055" Y="-3.69275492777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.856200997304" Y="-2.905462459645" />
                  <Point X="27.037192808842" Y="-2.773964211343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.917384817688" Y="-2.134467284105" />
                  <Point X="28.186548870668" Y="-1.938908152605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.424727038076" Y="-1.039319056736" />
                  <Point X="29.784726189148" Y="-0.777764363436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.693818936539" Y="-3.632556002719" />
                  <Point X="25.758475918814" Y="-3.585579955364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.791543895683" Y="-2.835012135848" />
                  <Point X="27.018444587597" Y="-2.670159133539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.825270988136" Y="-2.08396544083" />
                  <Point X="28.107943376611" Y="-1.87859192911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.287897529164" Y="-1.021305056184" />
                  <Point X="29.667709174281" Y="-0.745355743375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.980213442958" Y="-4.760136812167" />
                  <Point X="24.025097279523" Y="-4.727526796082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.667481807628" Y="-3.534264589076" />
                  <Point X="25.744365959573" Y="-3.478404982959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.707721839706" Y="-2.778485966438" />
                  <Point X="27.000787076234" Y="-2.565561608621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.70447320439" Y="-2.054303710148" />
                  <Point X="28.031825667538" Y="-1.816468224024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.151068020252" Y="-1.003291055632" />
                  <Point X="29.549632443119" Y="-0.713717052269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.858808180356" Y="-4.730916440709" />
                  <Point X="24.064168350283" Y="-4.581713543698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.635266245399" Y="-3.440244107237" />
                  <Point X="25.730256000332" Y="-3.371230010553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.621974393684" Y="-2.723358674778" />
                  <Point X="27.023460688729" Y="-2.431661807016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.57502667523" Y="-2.030925660823" />
                  <Point X="27.974526981964" Y="-1.740671698029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.014238511341" Y="-0.98527705508" />
                  <Point X="29.43155563346" Y="-0.682078418195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.875904562427" Y="-4.601068734197" />
                  <Point X="24.103239359588" Y="-4.435900335964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.583683782995" Y="-3.36029450201" />
                  <Point X="25.728680322288" Y="-3.2549483498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.533029720815" Y="-2.670554304395" />
                  <Point X="27.123270911556" Y="-2.241718977541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.346293958058" Y="-2.079683249532" />
                  <Point X="27.923068287091" Y="-1.660632170427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.877408947284" Y="-0.967263094593" />
                  <Point X="29.31347880886" Y="-0.650439794976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.874128541141" Y="-4.484932631329" />
                  <Point X="24.142309889148" Y="-4.290087476784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.529503920834" Y="-3.282232018169" />
                  <Point X="25.758899583042" Y="-3.115566313835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.403925403247" Y="-2.646927623794" />
                  <Point X="27.876840256837" Y="-1.576792342531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.740579358404" Y="-0.949249152141" />
                  <Point X="29.19540198426" Y="-0.618801171758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.853719144225" Y="-4.382334468297" />
                  <Point X="24.181380418708" Y="-4.144274617605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.475323947559" Y="-3.204169615057" />
                  <Point X="25.794789537867" Y="-2.972064277464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.218862120865" Y="-2.663957510955" />
                  <Point X="27.859391805512" Y="-1.472042926604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.603749769523" Y="-0.93123520969" />
                  <Point X="29.077325159659" Y="-0.587162548539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.833310730116" Y="-4.279735591214" />
                  <Point X="24.220450948268" Y="-3.998461758425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.412699915137" Y="-3.132242180024" />
                  <Point X="27.870573691746" Y="-1.346492352849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.466920180643" Y="-0.913221267239" />
                  <Point X="28.959248335059" Y="-0.555523925321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.811700806545" Y="-4.178009661853" />
                  <Point X="24.259521477827" Y="-3.852648899246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.320515520354" Y="-3.08179160539" />
                  <Point X="27.90528297085" Y="-1.203848127601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.285371587664" Y="-0.927697583075" />
                  <Point X="28.841171510459" Y="-0.523885302102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.767593587993" Y="-4.09262897406" />
                  <Point X="24.298592007387" Y="-3.706836040066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.207268793031" Y="-3.046643711085" />
                  <Point X="28.723094685859" Y="-0.492246678883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.699207534079" Y="-4.024887892689" />
                  <Point X="24.337662536947" Y="-3.561023180887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.094021416997" Y="-3.011496288097" />
                  <Point X="28.618421949158" Y="-0.450869415757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.625977452793" Y="-3.96066620321" />
                  <Point X="24.404538921765" Y="-3.395008185335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.941633394143" Y="-3.004786209596" />
                  <Point X="28.529406746915" Y="-0.398116287963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.552747175794" Y="-3.896444655925" />
                  <Point X="24.581122784757" Y="-3.149286041249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.649164023696" Y="-3.099851187502" />
                  <Point X="28.460317807004" Y="-0.33088588316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.475664110567" Y="-3.835022323138" />
                  <Point X="28.403528526747" Y="-0.25471925254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.70921346696" Y="0.693916384701" />
                  <Point X="29.779083979676" Y="0.744680283643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.366782222292" Y="-3.796703187637" />
                  <Point X="28.36841809866" Y="-0.162802013858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.453155333653" Y="0.625305719075" />
                  <Point X="29.762659672835" Y="0.850173784092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.218908652993" Y="-3.786713166638" />
                  <Point X="28.350085106299" Y="-0.058695254612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.197097200347" Y="0.556695053449" />
                  <Point X="29.74314416841" Y="0.953421398035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.070658803929" Y="-3.776996528892" />
                  <Point X="28.362147244076" Y="0.067494859324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.941039497594" Y="0.488084700638" />
                  <Point X="29.718852844073" Y="1.053199175705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.432910395076" Y="-4.122921412229" />
                  <Point X="28.438263539986" Y="0.240223043239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.684125153677" Y="0.41885196159" />
                  <Point X="29.622292204067" Y="1.100470222072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.345651224472" Y="-4.068892452768" />
                  <Point X="29.424900335903" Y="1.07448309303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.258392053868" Y="-4.014863493308" />
                  <Point X="29.227508398377" Y="1.048495913595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.179322163253" Y="-3.954884673662" />
                  <Point X="29.03011645728" Y="1.022508731565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.100855565163" Y="-3.89446753634" />
                  <Point X="28.832724516184" Y="0.996521549536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.022388726963" Y="-3.834050573467" />
                  <Point X="28.635332575087" Y="0.970534367506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.131079718288" Y="-3.637655487997" />
                  <Point X="28.448214197138" Y="0.952011366017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.247862714859" Y="-3.435381216577" />
                  <Point X="28.314690727309" Y="0.972427344562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.36464571143" Y="-3.233106945158" />
                  <Point X="28.215376695381" Y="1.017697934601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.481428914175" Y="-3.030832523945" />
                  <Point X="28.134044019888" Y="1.076032744801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.598212187287" Y="-2.828558051607" />
                  <Point X="28.077440918085" Y="1.152334641987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.683734402741" Y="-2.648996067128" />
                  <Point X="28.031930841359" Y="1.236696093655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.678571489482" Y="-2.535320685316" />
                  <Point X="28.007969032748" Y="1.336713278514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.63354029855" Y="-2.450611302753" />
                  <Point X="28.005912972048" Y="1.452645920837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.560930802881" Y="-2.385938731431" />
                  <Point X="28.062453697898" Y="1.611151620594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.45108634172" Y="-2.348318946068" />
                  <Point X="29.1260715215" Y="2.501341660848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.99200134115" Y="-2.564437265088" />
                  <Point X="29.076713371635" Y="2.58290732373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.206766438592" Y="-3.017517358409" />
                  <Point X="29.02529435547" Y="2.6629756796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.149214025686" Y="-2.941905176112" />
                  <Point X="28.969808175216" Y="2.740089067792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.091661612781" Y="-2.866292993814" />
                  <Point X="28.570398418128" Y="2.56732735103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.034109199875" Y="-2.790680811517" />
                  <Point X="27.783317477149" Y="2.112906032288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.979490790252" Y="-2.712936951058" />
                  <Point X="27.432450580852" Y="1.975412768322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.930631835687" Y="-2.631008601561" />
                  <Point X="27.296615531135" Y="1.994149285771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.881773023842" Y="-2.549080148372" />
                  <Point X="27.202892211132" Y="2.043481765785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.832914479543" Y="-2.467151500798" />
                  <Point X="27.126086420651" Y="2.105105550466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.020150972212" Y="-1.487147240212" />
                  <Point X="27.069891078032" Y="2.181703702041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.051020581751" Y="-1.347292698196" />
                  <Point X="27.028643636576" Y="2.269162139514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.02744832931" Y="-1.246992484213" />
                  <Point X="27.013136208334" Y="2.375321791258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.968140342626" Y="-1.172655800927" />
                  <Point X="27.032214773609" Y="2.506609638167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.880336403381" Y="-1.119022639052" />
                  <Point X="27.11452325354" Y="2.683836707115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.742179847003" Y="-1.101972794922" />
                  <Point X="27.231306676756" Y="2.886111288509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.544787910767" Y="-1.12795997342" />
                  <Point X="27.348090099972" Y="3.088385869904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.347395974531" Y="-1.153947151918" />
                  <Point X="27.464872965896" Y="3.290660046403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.150004038294" Y="-1.179934330417" />
                  <Point X="27.581655695496" Y="3.492934123856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.952612102058" Y="-1.205921508915" />
                  <Point X="27.698438425096" Y="3.695208201309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.755220165822" Y="-1.231908687414" />
                  <Point X="27.815221154695" Y="3.897482278763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.557828357957" Y="-1.257895772645" />
                  <Point X="27.737894318899" Y="3.958727501863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.360436593312" Y="-1.283882826475" />
                  <Point X="27.656217174444" Y="4.016812040713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.31651815818" Y="-1.198364979499" />
                  <Point X="27.569149991789" Y="4.070980487583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.291219036525" Y="-1.099319409441" />
                  <Point X="21.306229180057" Y="-0.361871373808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.70172200392" Y="-0.07452901775" />
                  <Point X="27.479452910232" Y="4.123238201056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.265919914869" Y="-1.000273839382" />
                  <Point X="21.050171376391" Y="-0.430481799936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.695675677641" Y="0.038504526932" />
                  <Point X="27.389755828675" Y="4.175495914529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.248042318416" Y="-0.895836215641" />
                  <Point X="20.794113342188" Y="-0.49909239356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.657852306448" Y="0.128450697071" />
                  <Point X="27.300058825681" Y="4.227753685082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.232829266087" Y="-0.789462687277" />
                  <Point X="20.53805519354" Y="-0.567703070333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.591074422054" Y="0.19736018199" />
                  <Point X="27.206503375335" Y="4.277208129542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.217615656199" Y="-0.683089564003" />
                  <Point X="20.281997044891" Y="-0.636313747106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.498295489609" Y="0.247378799728" />
                  <Point X="27.108173367273" Y="4.323193654768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.380689898968" Y="0.279359794459" />
                  <Point X="27.009843359211" Y="4.369179179994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.262613080255" Y="0.310998421955" />
                  <Point X="26.911513649557" Y="4.415164922027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.144536261542" Y="0.342637049451" />
                  <Point X="26.808841335139" Y="4.457995577016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.026459442829" Y="0.374275676947" />
                  <Point X="26.701036418237" Y="4.497097178021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.908382624117" Y="0.405914304443" />
                  <Point X="26.593231501334" Y="4.536198779025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.790305805404" Y="0.437552931939" />
                  <Point X="26.485425852459" Y="4.575299848221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.672228986691" Y="0.469191559436" />
                  <Point X="26.36810107297" Y="4.607484864196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.554152167978" Y="0.500830186932" />
                  <Point X="26.24678961749" Y="4.636773390518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.436075319182" Y="0.532468792571" />
                  <Point X="21.424387291851" Y="1.250519471651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.48529754543" Y="1.294773361268" />
                  <Point X="26.125478162009" Y="4.666061916839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.317998464354" Y="0.564107393828" />
                  <Point X="21.204165074038" Y="1.207945122661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.573726202034" Y="1.476446998848" />
                  <Point X="26.004166718896" Y="4.695350452146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.216967469431" Y="0.608130537232" />
                  <Point X="21.067335544493" Y="1.225959108223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.568454958664" Y="1.590043674227" />
                  <Point X="25.002623526416" Y="4.085113187038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.235264139011" Y="4.25413648583" />
                  <Point X="25.8828553006" Y="4.724639005484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.236436627072" Y="0.739702166105" />
                  <Point X="20.930506014949" Y="1.243973093784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.528635656673" Y="1.678539715758" />
                  <Point X="24.88625750386" Y="4.117994780699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.275182063972" Y="4.400565013806" />
                  <Point X="25.750634713381" Y="4.746001583654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.255905784713" Y="0.871273794978" />
                  <Point X="20.793676448578" Y="1.261987052589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.472186930726" Y="1.754953773567" />
                  <Point X="24.814580455466" Y="4.183344814621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.314252577674" Y="4.546377861464" />
                  <Point X="25.609372947957" Y="4.760795361355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.282317068089" Y="1.007889173432" />
                  <Point X="20.656846819959" Y="1.280000966169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.395154757695" Y="1.816413081698" />
                  <Point X="24.772133705911" Y="4.269931903756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.353323091376" Y="4.692190709122" />
                  <Point X="25.468111182534" Y="4.775589139056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.321937928452" Y="1.154101871345" />
                  <Point X="20.52001719134" Y="1.298014879749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.316549246202" Y="1.876729292526" />
                  <Point X="24.745796873419" Y="4.36822353276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.361557257499" Y="1.300313456691" />
                  <Point X="20.383187562721" Y="1.316028793328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.23794373471" Y="1.937045503353" />
                  <Point X="24.719460040926" Y="4.466515161764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.159338223217" Y="1.997361714181" />
                  <Point X="24.693123084655" Y="4.564806700837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.080732711724" Y="2.057677925008" />
                  <Point X="22.006010446117" Y="2.729931549261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.191045099072" Y="2.864367093787" />
                  <Point X="24.666785994266" Y="4.663098142468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.002127200232" Y="2.117994135835" />
                  <Point X="21.845951518719" Y="2.731068389382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.251358970319" Y="3.02561414414" />
                  <Point X="24.640448903876" Y="4.761389584098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.923521688739" Y="2.178310346663" />
                  <Point X="21.735594555166" Y="2.768315819961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.242745342741" Y="3.136782435246" />
                  <Point X="24.483607601091" Y="4.764864165339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.844916039667" Y="2.238626457534" />
                  <Point X="21.645536234392" Y="2.820311077781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.211181358236" Y="3.231276316012" />
                  <Point X="24.290949925932" Y="4.742316628853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.781582106654" Y="2.310038119596" />
                  <Point X="21.555477913618" Y="2.872306335601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.163419676494" Y="3.31400188088" />
                  <Point X="23.148353224135" Y="4.0295979905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.513195688143" Y="4.294671556625" />
                  <Point X="24.098291907888" Y="4.719768843245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.900594482405" Y="2.5139321298" />
                  <Point X="21.465419627492" Y="2.924301618593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.115657994752" Y="3.396727445748" />
                  <Point X="23.036562678545" Y="4.065803862763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.5373194581" Y="4.429624959297" />
                  <Point X="23.845895145568" Y="4.65381831935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.036367481287" Y="2.730003445505" />
                  <Point X="21.375361370874" Y="2.976296923024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.067896313011" Y="3.479453010616" />
                  <Point X="22.943887360312" Y="4.115897760632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.524058876165" Y="4.537417040437" />
                  <Point X="23.582711907192" Y="4.580030961875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.246499987609" Y="3.000100105727" />
                  <Point X="21.285303114255" Y="3.028292227455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.020134631269" Y="3.562178575485" />
                  <Point X="22.87353285199" Y="4.182208676163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.972372949528" Y="3.644904140353" />
                  <Point X="22.815680748391" Y="4.257603120425" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.6136796875" Y="-4.067580566406" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.406224609375" Y="-3.437839355469" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.1844375" Y="-3.238498291016" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.9014375" Y="-3.21566796875" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.653626953125" Y="-3.369348388672" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.282330078125" Y="-4.501633789062" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.091662109375" Y="-4.975314941406" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.796291015625" Y="-4.9114453125" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.673740234375" Y="-4.681003417969" />
                  <Point X="23.6908515625" Y="-4.5510390625" />
                  <Point X="23.690318359375" Y="-4.5347578125" />
                  <Point X="23.66883984375" Y="-4.426785644531" />
                  <Point X="23.6297890625" Y="-4.230466308594" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.53094921875" Y="-4.13004296875" />
                  <Point X="23.38045703125" Y="-3.998064208984" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.240908203125" Y="-3.978562744141" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.918587890625" Y="-4.034952392578" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157293945313" />
                  <Point X="22.595171875" Y="-4.346386230469" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.425462890625" Y="-4.341782714844" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="22.000900390625" Y="-4.057299804688" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.229818359375" Y="-3.086637939453" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597592285156" />
                  <Point X="22.48601953125" Y="-2.568762939453" />
                  <Point X="22.46867578125" Y="-2.55141796875" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.5764375" Y="-3.023756347656" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.06053515625" Y="-3.139106201172" />
                  <Point X="20.838302734375" Y="-2.847135742188" />
                  <Point X="20.7355859375" Y="-2.674898681641" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="21.3740390625" Y="-1.777783447266" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396013427734" />
                  <Point X="21.8618828125" Y="-1.366266113281" />
                  <Point X="21.859673828125" Y="-1.334595458984" />
                  <Point X="21.838841796875" Y="-1.310638916016" />
                  <Point X="21.812359375" Y="-1.295052490234" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.730302734375" Y="-1.426828613281" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.15952734375" Y="-1.351477294922" />
                  <Point X="20.072607421875" Y="-1.011188171387" />
                  <Point X="20.04543359375" Y="-0.821188903809" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="20.91605078125" Y="-0.269716796875" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.467951171875" Y="-0.114590919495" />
                  <Point X="21.485857421875" Y="-0.102163635254" />
                  <Point X="21.497677734375" Y="-0.090643539429" />
                  <Point X="21.508384765625" Y="-0.065326057434" />
                  <Point X="21.5143515625" Y="-0.046101486206" />
                  <Point X="21.516599609375" Y="-0.031283159256" />
                  <Point X="21.5110703125" Y="-0.005882251263" />
                  <Point X="21.5051015625" Y="0.013348286629" />
                  <Point X="21.49767578125" Y="0.028085977554" />
                  <Point X="21.476009765625" Y="0.046438289642" />
                  <Point X="21.458103515625" Y="0.058865581512" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.4852578125" Y="0.322587860107" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.026337890625" Y="0.617852355957" />
                  <Point X="20.08235546875" Y="0.996414916992" />
                  <Point X="20.13705859375" Y="1.198287475586" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.869337890625" Y="1.443665771484" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.290091796875" Y="1.402738891602" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056640625" />
                  <Point X="21.3608828125" Y="1.443787231445" />
                  <Point X="21.36962890625" Y="1.464901245117" />
                  <Point X="21.385529296875" Y="1.503291503906" />
                  <Point X="21.38928515625" Y="1.524606201172" />
                  <Point X="21.38368359375" Y="1.545511962891" />
                  <Point X="21.373130859375" Y="1.565783325195" />
                  <Point X="21.353943359375" Y="1.602641601562" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.79093359375" Y="2.040559692383" />
                  <Point X="20.52389453125" Y="2.245466308594" />
                  <Point X="20.622318359375" Y="2.414091064453" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="20.984890625" Y="2.973260253906" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.609056640625" Y="3.060765869141" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.891841796875" Y="2.917779052734" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775146484" />
                  <Point X="21.986748046875" Y="2.927404296875" />
                  <Point X="22.008294921875" Y="2.948950927734" />
                  <Point X="22.04747265625" Y="2.988127685547" />
                  <Point X="22.0591015625" Y="3.006381835938" />
                  <Point X="22.061927734375" Y="3.027840576172" />
                  <Point X="22.059271484375" Y="3.058196289062" />
                  <Point X="22.054443359375" Y="3.113390136719" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.804611328125" Y="3.555479003906" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.868017578125" Y="3.883674560547" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.47534375" Y="4.301125" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.961216796875" Y="4.380046386719" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.0825390625" Y="4.256072753906" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.2213828125" Y="4.236827148438" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.29448828125" />
                  <Point X="23.32537109375" Y="4.330814941406" />
                  <Point X="23.346197265625" Y="4.396864746094" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.320419921875" Y="4.628547851562" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.541130859375" Y="4.765699707031" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.308576171875" Y="4.935676269531" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.889291015625" Y="4.566807128906" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176269531" />
                  <Point X="25.054453125" Y="4.310902832031" />
                  <Point X="25.17912890625" Y="4.776198242188" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.4316875" Y="4.970442871094" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.089103515625" Y="4.870302734375" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.657462890625" Y="4.715013183594" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.0751015625" Y="4.548410644531" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.47787890625" Y="4.344048828125" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.86378515625" Y="4.102346191406" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.535587890625" Y="3.033141357422" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491514648438" />
                  <Point X="27.217234375" Y="2.463026611328" />
                  <Point X="27.2033828125" Y="2.411228271484" />
                  <Point X="27.202044921875" Y="2.392326416016" />
                  <Point X="27.205015625" Y="2.367692382812" />
                  <Point X="27.210416015625" Y="2.322901611328" />
                  <Point X="27.218681640625" Y="2.3008125" />
                  <Point X="27.23392578125" Y="2.278348632812" />
                  <Point X="27.261640625" Y="2.23750390625" />
                  <Point X="27.27494140625" Y="2.224203369141" />
                  <Point X="27.297404296875" Y="2.208960693359" />
                  <Point X="27.33825" Y="2.181245849609" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.384970703125" Y="2.170009765625" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.47715234375" Y="2.173564453125" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.503875" Y="2.748313232422" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.051544921875" Y="2.951801757812" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.27576953125" Y="2.620956542969" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.691142578125" Y="1.901950927734" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583833251953" />
                  <Point X="28.258869140625" Y="1.557085693359" />
                  <Point X="28.22158984375" Y="1.508451904297" />
                  <Point X="28.21312109375" Y="1.491500244141" />
                  <Point X="28.205482421875" Y="1.464190551758" />
                  <Point X="28.191595703125" Y="1.414535644531" />
                  <Point X="28.190779296875" Y="1.390964599609" />
                  <Point X="28.197048828125" Y="1.360579345703" />
                  <Point X="28.20844921875" Y="1.305331787109" />
                  <Point X="28.215646484375" Y="1.287955810547" />
                  <Point X="28.23269921875" Y="1.262036865234" />
                  <Point X="28.263703125" Y="1.214910400391" />
                  <Point X="28.280947265625" Y="1.19882019043" />
                  <Point X="28.30566015625" Y="1.184909912109" />
                  <Point X="28.35058984375" Y="1.159617797852" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.401978515625" Y="1.149203979492" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.3904375" Y="1.261585571289" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.874314453125" Y="1.217867797852" />
                  <Point X="29.939193359375" Y="0.951367004395" />
                  <Point X="29.96225" Y="0.803276184082" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="29.202470703125" Y="0.361432434082" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819503784" />
                  <Point X="28.696263671875" Y="0.21384588623" />
                  <Point X="28.636578125" Y="0.179347137451" />
                  <Point X="28.622265625" Y="0.166926544189" />
                  <Point X="28.6025703125" Y="0.141830093384" />
                  <Point X="28.566759765625" Y="0.096198867798" />
                  <Point X="28.556986328125" Y="0.074735565186" />
                  <Point X="28.550421875" Y="0.040455253601" />
                  <Point X="28.538484375" Y="-0.021874994278" />
                  <Point X="28.538484375" Y="-0.040685081482" />
                  <Point X="28.545048828125" Y="-0.074965545654" />
                  <Point X="28.556986328125" Y="-0.137295639038" />
                  <Point X="28.566759765625" Y="-0.15875894165" />
                  <Point X="28.586455078125" Y="-0.183855239868" />
                  <Point X="28.622265625" Y="-0.229486618042" />
                  <Point X="28.636578125" Y="-0.24190675354" />
                  <Point X="28.669404296875" Y="-0.260880523682" />
                  <Point X="28.729087890625" Y="-0.295379272461" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.579927734375" Y="-0.525132202148" />
                  <Point X="29.998068359375" Y="-0.637172546387" />
                  <Point X="29.98465625" Y="-0.726141479492" />
                  <Point X="29.948431640625" Y="-0.966413208008" />
                  <Point X="29.918890625" Y="-1.095862915039" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.946962890625" Y="-1.168059570312" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.330412109375" Y="-1.112344360352" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.1854453125" Y="-1.154697631836" />
                  <Point X="28.14650390625" Y="-1.201531005859" />
                  <Point X="28.075701171875" Y="-1.286685546875" />
                  <Point X="28.064359375" Y="-1.314071166992" />
                  <Point X="28.05877734375" Y="-1.37472253418" />
                  <Point X="28.048630859375" Y="-1.485001464844" />
                  <Point X="28.056361328125" Y="-1.516622314453" />
                  <Point X="28.092013671875" Y="-1.572079101562" />
                  <Point X="28.156841796875" Y="-1.672912841797" />
                  <Point X="28.168462890625" Y="-1.685540771484" />
                  <Point X="28.9468359375" Y="-2.282808837891" />
                  <Point X="29.339076171875" Y="-2.583784179688" />
                  <Point X="29.3062421875" Y="-2.636913574219" />
                  <Point X="29.2041328125" Y="-2.802140380859" />
                  <Point X="29.14303515625" Y="-2.888953369141" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.229927734375" Y="-2.534307373047" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.660666015625" Y="-2.239465332031" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.42537890625" Y="-2.252768798828" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.33468359375" />
                  <Point X="27.255076171875" Y="-2.398382080078" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.1891640625" Y="-2.546375" />
                  <Point X="27.20301171875" Y="-2.623050537109" />
                  <Point X="27.22819140625" Y="-2.762465332031" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.73427734375" Y="-3.644922607422" />
                  <Point X="27.98667578125" Y="-4.082087646484" />
                  <Point X="27.956458984375" Y="-4.103669921875" />
                  <Point X="27.83529296875" Y="-4.190215820312" />
                  <Point X="27.76698828125" Y="-4.234428710938" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="27.046771484375" Y="-3.465930175781" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.594927734375" Y="-2.931848876953" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.343099609375" Y="-2.843327880859" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.101470703125" Y="-2.921609619141" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045985839844" />
                  <Point X="25.94936328125" Y="-3.133837646484" />
                  <Point X="25.914642578125" Y="-3.293573730469" />
                  <Point X="25.9139296875" Y="-3.310719970703" />
                  <Point X="26.0556796875" Y="-4.387405273438" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.108974609375" Y="-4.938120605469" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.93123828125" Y="-4.974711425781" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#152" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.068569105749" Y="4.611027873117" Z="0.9" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.9" />
                  <Point X="-0.703392512309" Y="5.01661766568" Z="0.9" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.9" />
                  <Point X="-1.478524334475" Y="4.845123510723" Z="0.9" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.9" />
                  <Point X="-1.735308866933" Y="4.653301920354" Z="0.9" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.9" />
                  <Point X="-1.728471475712" Y="4.377130412799" Z="0.9" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.9" />
                  <Point X="-1.803909102593" Y="4.314300854688" Z="0.9" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.9" />
                  <Point X="-1.90052958834" Y="4.331703520828" Z="0.9" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.9" />
                  <Point X="-2.005272312325" Y="4.441764470535" Z="0.9" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.9" />
                  <Point X="-2.555095750193" Y="4.376112716961" Z="0.9" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.9" />
                  <Point X="-3.168560409571" Y="3.954634589716" Z="0.9" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.9" />
                  <Point X="-3.244846783068" Y="3.561759119031" Z="0.9" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.9" />
                  <Point X="-2.99669580474" Y="3.085119094084" Z="0.9" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.9" />
                  <Point X="-3.033216977349" Y="3.015586590537" Z="0.9" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.9" />
                  <Point X="-3.109957371523" Y="2.998868894433" Z="0.9" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.9" />
                  <Point X="-3.372100229266" Y="3.135347165678" Z="0.9" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.9" />
                  <Point X="-4.060729761585" Y="3.035242731352" Z="0.9" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.9" />
                  <Point X="-4.427019268861" Y="2.470576251348" Z="0.9" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.9" />
                  <Point X="-4.245660618956" Y="2.032172041132" Z="0.9" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.9" />
                  <Point X="-3.677375048065" Y="1.573975767677" Z="0.9" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.9" />
                  <Point X="-3.682724226861" Y="1.515314053241" Z="0.9" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.9" />
                  <Point X="-3.731100181305" Y="1.481704745484" Z="0.9" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.9" />
                  <Point X="-4.130293955744" Y="1.524517923982" Z="0.9" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.9" />
                  <Point X="-4.917358356369" Y="1.242644757719" Z="0.9" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.9" />
                  <Point X="-5.029280685329" Y="0.656451415839" Z="0.9" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.9" />
                  <Point X="-4.533841191295" Y="0.305571561024" Z="0.9" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.9" />
                  <Point X="-3.558655432327" Y="0.036641729019" Z="0.9" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.9" />
                  <Point X="-3.542839322415" Y="0.010576421727" Z="0.9" />
                  <Point X="-3.539556741714" Y="0" Z="0.9" />
                  <Point X="-3.54552527872" Y="-0.019230529334" Z="0.9" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.9" />
                  <Point X="-3.566713162796" Y="-0.042234254115" Z="0.9" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.9" />
                  <Point X="-4.103046597478" Y="-0.190140495732" Z="0.9" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.9" />
                  <Point X="-5.010219768224" Y="-0.796987875213" Z="0.9" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.9" />
                  <Point X="-4.895074574115" Y="-1.332571239631" Z="0.9" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.9" />
                  <Point X="-4.269329603626" Y="-1.445120879055" Z="0.9" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.9" />
                  <Point X="-3.202073139283" Y="-1.316919236061" Z="0.9" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.9" />
                  <Point X="-3.197647010202" Y="-1.341641890819" Z="0.9" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.9" />
                  <Point X="-3.662554832155" Y="-1.70683576165" Z="0.9" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.9" />
                  <Point X="-4.313513915937" Y="-2.669228282569" Z="0.9" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.9" />
                  <Point X="-3.985406299781" Y="-3.138109415659" Z="0.9" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.9" />
                  <Point X="-3.404720379423" Y="-3.035777614465" Z="0.9" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.9" />
                  <Point X="-2.561646761156" Y="-2.566683707976" Z="0.9" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.9" />
                  <Point X="-2.819639398704" Y="-3.030358003069" Z="0.9" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.9" />
                  <Point X="-3.035761135808" Y="-4.065637063645" Z="0.9" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.9" />
                  <Point X="-2.607015380794" Y="-4.353013629449" Z="0.9" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.9" />
                  <Point X="-2.371317931202" Y="-4.345544451312" Z="0.9" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.9" />
                  <Point X="-2.059790356014" Y="-4.045245761269" Z="0.9" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.9" />
                  <Point X="-1.768518587409" Y="-3.997175863448" Z="0.9" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.9" />
                  <Point X="-1.508174100196" Y="-4.136354713063" Z="0.9" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.9" />
                  <Point X="-1.3863555314" Y="-4.405260181043" Z="0.9" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.9" />
                  <Point X="-1.381988655126" Y="-4.643196438317" Z="0.9" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.9" />
                  <Point X="-1.222324291237" Y="-4.928587937318" Z="0.9" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.9" />
                  <Point X="-0.924022823676" Y="-4.993119120482" Z="0.9" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.9" />
                  <Point X="-0.675529531551" Y="-4.483294627226" Z="0.9" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.9" />
                  <Point X="-0.311454696303" Y="-3.366577384944" Z="0.9" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.9" />
                  <Point X="-0.089899583024" Y="-3.232140881405" Z="0.9" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.9" />
                  <Point X="0.163459496337" Y="-3.254971163883" Z="0.9" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.9" />
                  <Point X="0.358991163129" Y="-3.435068370823" Z="0.9" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.9" />
                  <Point X="0.55922532885" Y="-4.04924140259" Z="0.9" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.9" />
                  <Point X="0.934019250167" Y="-4.992626189725" Z="0.9" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.9" />
                  <Point X="1.113523084409" Y="-4.955681010205" Z="0.9" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.9" />
                  <Point X="1.099094115255" Y="-4.349599038199" Z="0.9" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.9" />
                  <Point X="0.992065073573" Y="-3.113177727123" Z="0.9" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.9" />
                  <Point X="1.127279993919" Y="-2.928776059759" Z="0.9" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.9" />
                  <Point X="1.341524134323" Y="-2.861837300187" Z="0.9" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.9" />
                  <Point X="1.56173115445" Y="-2.942626833519" Z="0.9" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.9" />
                  <Point X="2.000946365642" Y="-3.465088103477" Z="0.9" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.9" />
                  <Point X="2.788001075585" Y="-4.24512276054" Z="0.9" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.9" />
                  <Point X="2.979364785364" Y="-4.113077098128" Z="0.9" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.9" />
                  <Point X="2.771420971024" Y="-3.588642489748" Z="0.9" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.9" />
                  <Point X="2.246058659613" Y="-2.582883706757" Z="0.9" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.9" />
                  <Point X="2.293166116154" Y="-2.390388691565" Z="0.9" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.9" />
                  <Point X="2.442509686807" Y="-2.265735193814" Z="0.9" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.9" />
                  <Point X="2.645623096755" Y="-2.257389349878" Z="0.9" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.9" />
                  <Point X="3.198771107359" Y="-2.54632852925" Z="0.9" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.9" />
                  <Point X="4.177766227157" Y="-2.886450691191" Z="0.9" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.9" />
                  <Point X="4.342617910188" Y="-2.631919035607" Z="0.9" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.9" />
                  <Point X="3.97111745246" Y="-2.211860967753" Z="0.9" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.9" />
                  <Point X="3.127916009487" Y="-1.513759178623" Z="0.9" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.9" />
                  <Point X="3.102410342249" Y="-1.348023539944" Z="0.9" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.9" />
                  <Point X="3.178794961537" Y="-1.202217596435" Z="0.9" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.9" />
                  <Point X="3.334875258779" Y="-1.129923378055" Z="0.9" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.9" />
                  <Point X="3.93428056891" Y="-1.186351965634" Z="0.9" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.9" />
                  <Point X="4.961479446792" Y="-1.07570695375" Z="0.9" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.9" />
                  <Point X="5.027939956563" Y="-0.702315596748" Z="0.9" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.9" />
                  <Point X="4.58671304165" Y="-0.445555782837" Z="0.9" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.9" />
                  <Point X="3.688268098709" Y="-0.186311867707" Z="0.9" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.9" />
                  <Point X="3.61963195412" Y="-0.121706883039" Z="0.9" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.9" />
                  <Point X="3.587999803519" Y="-0.034280383793" Z="0.9" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.9" />
                  <Point X="3.593371646906" Y="0.062330147429" Z="0.9" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.9" />
                  <Point X="3.635747484281" Y="0.142241855536" Z="0.9" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.9" />
                  <Point X="3.715127315644" Y="0.201836976535" Z="0.9" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.9" />
                  <Point X="4.209254775657" Y="0.344416149932" Z="0.9" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.9" />
                  <Point X="5.005497020954" Y="0.842248090258" Z="0.9" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.9" />
                  <Point X="4.91673954577" Y="1.260974601134" Z="0.9" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.9" />
                  <Point X="4.377754717816" Y="1.342437879002" Z="0.9" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.9" />
                  <Point X="3.402371761167" Y="1.230052889837" Z="0.9" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.9" />
                  <Point X="3.324029696358" Y="1.25976079707" Z="0.9" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.9" />
                  <Point X="3.268313292827" Y="1.32079766212" Z="0.9" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.9" />
                  <Point X="3.239861523958" Y="1.401964057078" Z="0.9" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.9" />
                  <Point X="3.247478626443" Y="1.482004334961" Z="0.9" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.9" />
                  <Point X="3.29239526927" Y="1.55794746544" Z="0.9" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.9" />
                  <Point X="3.715422988955" Y="1.893563255489" Z="0.9" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.9" />
                  <Point X="4.31238897835" Y="2.678122845625" Z="0.9" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.9" />
                  <Point X="4.085973822133" Y="3.012284977761" Z="0.9" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.9" />
                  <Point X="3.472717750632" Y="2.822894571458" Z="0.9" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.9" />
                  <Point X="2.458079688258" Y="2.253147245835" Z="0.9" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.9" />
                  <Point X="2.384800863451" Y="2.250930114739" Z="0.9" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.9" />
                  <Point X="2.319321949117" Y="2.281615416156" Z="0.9" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.9" />
                  <Point X="2.269143184079" Y="2.337702911265" Z="0.9" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.9" />
                  <Point X="2.24849958802" Y="2.404957580382" Z="0.9" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.9" />
                  <Point X="2.259380696823" Y="2.481389887624" Z="0.9" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.9" />
                  <Point X="2.572730781401" Y="3.039421331323" Z="0.9" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.9" />
                  <Point X="2.886605039764" Y="4.174373513978" Z="0.9" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.9" />
                  <Point X="2.496891356311" Y="4.418531532472" Z="0.9" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.9" />
                  <Point X="2.090126214124" Y="4.624982712218" Z="0.9" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.9" />
                  <Point X="1.668354494139" Y="4.793296438064" Z="0.9" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.9" />
                  <Point X="1.094681435228" Y="4.950186377169" Z="0.9" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.9" />
                  <Point X="0.43073776201" Y="5.051451151392" Z="0.9" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.9" />
                  <Point X="0.124675455804" Y="4.820419632834" Z="0.9" />
                  <Point X="0" Y="4.355124473572" Z="0.9" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>