<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#125" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="538" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.595046875" Y="-3.630987792969" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.54236328125" Y="-3.467375" />
                  <Point X="25.529388671875" Y="-3.448682128906" />
                  <Point X="25.378634765625" Y="-3.231474609375" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.282419921875" Y="-3.169438476562" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.963177734375" Y="-3.097035644531" />
                  <Point X="24.9431015625" Y="-3.103266357422" />
                  <Point X="24.709818359375" Y="-3.175668945312" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.633673828125" Y="-3.231479736328" />
                  <Point X="24.620701171875" Y="-3.250172363281" />
                  <Point X="24.469947265625" Y="-3.467380126953" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.901455078125" Y="-4.840408691406" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.781216796875" Y="-4.592463867188" />
                  <Point X="23.7850390625" Y="-4.563440429688" />
                  <Point X="23.78580078125" Y="-4.547931152344" />
                  <Point X="23.7834921875" Y="-4.516221191406" />
                  <Point X="23.7786953125" Y="-4.492109375" />
                  <Point X="23.722962890625" Y="-4.2119296875" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.676357421875" Y="-4.131206542969" />
                  <Point X="23.657875" Y="-4.114996582031" />
                  <Point X="23.44309765625" Y="-3.926641601562" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.33244140625" Y="-3.889358398438" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894801269531" />
                  <Point X="22.936900390625" Y="-3.908459716797" />
                  <Point X="22.699376953125" Y="-4.067169433594" />
                  <Point X="22.687212890625" Y="-4.076824462891" />
                  <Point X="22.6648984375" Y="-4.099462402344" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.198287109375" Y="-4.089384765625" />
                  <Point X="22.171697265625" Y="-4.068910888672" />
                  <Point X="21.895279296875" Y="-3.856077392578" />
                  <Point X="22.51725" Y="-2.778791503906" />
                  <Point X="22.57623828125" Y="-2.676619384766" />
                  <Point X="22.587140625" Y="-2.647653320312" />
                  <Point X="22.593412109375" Y="-2.616125" />
                  <Point X="22.594306640625" Y="-2.584325683594" />
                  <Point X="22.584654296875" Y="-2.554014160156" />
                  <Point X="22.569064453125" Y="-2.523813964844" />
                  <Point X="22.551826171875" Y="-2.500218261719" />
                  <Point X="22.535853515625" Y="-2.484244384766" />
                  <Point X="22.5106953125" Y="-2.46621484375" />
                  <Point X="22.481865234375" Y="-2.451996826172" />
                  <Point X="22.452248046875" Y="-2.443011962891" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.1819765625" Y="-3.141801513672" />
                  <Point X="20.917140625" Y="-2.793861328125" />
                  <Point X="20.898076171875" Y="-2.761894287109" />
                  <Point X="20.693857421875" Y="-2.419450195312" />
                  <Point X="21.790833984375" Y="-1.577710449219" />
                  <Point X="21.894044921875" Y="-1.498513183594" />
                  <Point X="21.91659375" Y="-1.473780029297" />
                  <Point X="21.935169921875" Y="-1.444291259766" />
                  <Point X="21.94675390625" Y="-1.417473876953" />
                  <Point X="21.95384765625" Y="-1.390083618164" />
                  <Point X="21.95665234375" Y="-1.359660522461" />
                  <Point X="21.9544453125" Y="-1.327994384766" />
                  <Point X="21.94702734375" Y="-1.297256347656" />
                  <Point X="21.92993359375" Y="-1.270655395508" />
                  <Point X="21.907001953125" Y="-1.245459594727" />
                  <Point X="21.8849296875" Y="-1.227531005859" />
                  <Point X="21.860544921875" Y="-1.21317956543" />
                  <Point X="21.83128125" Y="-1.201956054688" />
                  <Point X="21.79939453125" Y="-1.195474731445" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="20.269310546875" Y="-1.391699584961" />
                  <Point X="20.267900390625" Y="-1.391885375977" />
                  <Point X="20.165921875" Y="-0.992649047852" />
                  <Point X="20.160880859375" Y="-0.957393615723" />
                  <Point X="20.107578125" Y="-0.584698303223" />
                  <Point X="21.34955078125" Y="-0.251912139893" />
                  <Point X="21.467125" Y="-0.220408340454" />
                  <Point X="21.484498046875" Y="-0.213876495361" />
                  <Point X="21.51446875" Y="-0.197944580078" />
                  <Point X="21.5400234375" Y="-0.180208786011" />
                  <Point X="21.56375390625" Y="-0.156543289185" />
                  <Point X="21.583732421875" Y="-0.127925041199" />
                  <Point X="21.59656640625" Y="-0.101703704834" />
                  <Point X="21.605083984375" Y="-0.074258636475" />
                  <Point X="21.609328125" Y="-0.043926670074" />
                  <Point X="21.608595703125" Y="-0.011924245834" />
                  <Point X="21.6043515625" Y="0.014060599327" />
                  <Point X="21.595833984375" Y="0.041505516052" />
                  <Point X="21.580306640625" Y="0.071395469666" />
                  <Point X="21.558861328125" Y="0.099178123474" />
                  <Point X="21.53782421875" Y="0.119175140381" />
                  <Point X="21.51226953125" Y="0.13691078186" />
                  <Point X="21.498076171875" Y="0.145046905518" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="20.10818359375" Y="0.521975402832" />
                  <Point X="20.17551171875" Y="0.976966491699" />
                  <Point X="20.18566796875" Y="1.014445617676" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="21.1500546875" Y="1.310888549805" />
                  <Point X="21.2343359375" Y="1.29979309082" />
                  <Point X="21.255017578125" Y="1.299341796875" />
                  <Point X="21.276578125" Y="1.301228149414" />
                  <Point X="21.296865234375" Y="1.305263916016" />
                  <Point X="21.301732421875" Y="1.306798461914" />
                  <Point X="21.358291015625" Y="1.324631347656" />
                  <Point X="21.377224609375" Y="1.332962524414" />
                  <Point X="21.39596875" Y="1.343784790039" />
                  <Point X="21.4126484375" Y="1.356016357422" />
                  <Point X="21.426287109375" Y="1.371568725586" />
                  <Point X="21.438701171875" Y="1.389298828125" />
                  <Point X="21.4486484375" Y="1.407431396484" />
                  <Point X="21.4506015625" Y="1.412146606445" />
                  <Point X="21.473294921875" Y="1.466935668945" />
                  <Point X="21.47908203125" Y="1.486784667969" />
                  <Point X="21.482841796875" Y="1.508094482422" />
                  <Point X="21.484197265625" Y="1.528733154297" />
                  <Point X="21.481052734375" Y="1.54917578125" />
                  <Point X="21.475455078125" Y="1.570078125" />
                  <Point X="21.467958984375" Y="1.589363891602" />
                  <Point X="21.46559765625" Y="1.593900512695" />
                  <Point X="21.438212890625" Y="1.646503417969" />
                  <Point X="21.426716796875" Y="1.663708007812" />
                  <Point X="21.4128046875" Y="1.680287475586" />
                  <Point X="21.39786328125" Y="1.694590454102" />
                  <Point X="20.648140625" Y="2.269874023438" />
                  <Point X="20.918845703125" Y="2.73365625" />
                  <Point X="20.9457421875" Y="2.768229980469" />
                  <Point X="21.24949609375" Y="3.158661865234" />
                  <Point X="21.74158203125" Y="2.874556152344" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.812275390625" Y="2.836340820312" />
                  <Point X="21.83291796875" Y="2.82983203125" />
                  <Point X="21.85320703125" Y="2.825796142578" />
                  <Point X="21.859986328125" Y="2.825203125" />
                  <Point X="21.938755859375" Y="2.818311523438" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.019537109375" Y="2.835653564453" />
                  <Point X="22.037791015625" Y="2.847282714844" />
                  <Point X="22.05391796875" Y="2.860224365234" />
                  <Point X="22.05873046875" Y="2.865036132812" />
                  <Point X="22.114642578125" Y="2.920947753906" />
                  <Point X="22.12759765625" Y="2.937090087891" />
                  <Point X="22.1392265625" Y="2.955346679688" />
                  <Point X="22.148375" Y="2.973898193359" />
                  <Point X="22.1532890625" Y="2.993989746094" />
                  <Point X="22.15611328125" Y="3.015451171875" />
                  <Point X="22.156564453125" Y="3.036114257812" />
                  <Point X="22.15597265625" Y="3.0428984375" />
                  <Point X="22.14908203125" Y="3.121668945312" />
                  <Point X="22.145044921875" Y="3.141961669922" />
                  <Point X="22.13853515625" Y="3.162604492188" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.81666796875" Y="3.724596191406" />
                  <Point X="22.299376953125" Y="4.094684570313" />
                  <Point X="22.34173046875" Y="4.11821484375" />
                  <Point X="22.83296484375" Y="4.391133789062" />
                  <Point X="22.940958984375" Y="4.250392578125" />
                  <Point X="22.9568046875" Y="4.229741699219" />
                  <Point X="22.971107421875" Y="4.214801269531" />
                  <Point X="22.9876875" Y="4.200888183594" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.012431640625" Y="4.185467285156" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.2225546875" Y="4.134484863281" />
                  <Point X="23.230412109375" Y="4.137740234375" />
                  <Point X="23.321728515625" Y="4.175564453125" />
                  <Point X="23.33986328125" Y="4.185514648437" />
                  <Point X="23.35759375" Y="4.197931640625" />
                  <Point X="23.373142578125" Y="4.211570800781" />
                  <Point X="23.385373046875" Y="4.228252441406" />
                  <Point X="23.3961953125" Y="4.247" />
                  <Point X="23.40452734375" Y="4.265938964844" />
                  <Point X="23.407078125" Y="4.274032226563" />
                  <Point X="23.43680078125" Y="4.368296386719" />
                  <Point X="23.4408359375" Y="4.388583007812" />
                  <Point X="23.44272265625" Y="4.410143066406" />
                  <Point X="23.442271484375" Y="4.430824707031" />
                  <Point X="23.415798828125" Y="4.6318984375" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.101705078125" Y="4.815816894531" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.850783203125" Y="4.343463378906" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.03669140625" Y="4.183885253906" />
                  <Point X="25.06712890625" Y="4.194216796875" />
                  <Point X="25.094423828125" Y="4.208805664062" />
                  <Point X="25.11558203125" Y="4.231393554688" />
                  <Point X="25.13344140625" Y="4.258119628906" />
                  <Point X="25.146216796875" Y="4.286313964844" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.844041015625" Y="4.831738769531" />
                  <Point X="25.8865234375" Y="4.821482421875" />
                  <Point X="26.481025390625" Y="4.677951660156" />
                  <Point X="26.5073125" Y="4.668416992188" />
                  <Point X="26.894646484375" Y="4.527928222656" />
                  <Point X="26.921388671875" Y="4.515421875" />
                  <Point X="27.294576171875" Y="4.340894042969" />
                  <Point X="27.320443359375" Y="4.325823730469" />
                  <Point X="27.680978515625" Y="4.115774902344" />
                  <Point X="27.705349609375" Y="4.098444335938" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.215859375" Y="2.669354492188" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.140568359375" Y="2.536046875" />
                  <Point X="27.131376953125" Y="2.509696777344" />
                  <Point X="27.111607421875" Y="2.435772216797" />
                  <Point X="27.108419921875" Y="2.413850341797" />
                  <Point X="27.10774609375" Y="2.389439697266" />
                  <Point X="27.108392578125" Y="2.375445556641" />
                  <Point X="27.116099609375" Y="2.311529052734" />
                  <Point X="27.121443359375" Y="2.289602050781" />
                  <Point X="27.1297109375" Y="2.267512207031" />
                  <Point X="27.14007421875" Y="2.247467529297" />
                  <Point X="27.143478515625" Y="2.242450927734" />
                  <Point X="27.183033203125" Y="2.184158935547" />
                  <Point X="27.197625" Y="2.167312988281" />
                  <Point X="27.215939453125" Y="2.150609130859" />
                  <Point X="27.2266171875" Y="2.142187744141" />
                  <Point X="27.28491015625" Y="2.102634277344" />
                  <Point X="27.304953125" Y="2.092272216797" />
                  <Point X="27.327041015625" Y="2.084006347656" />
                  <Point X="27.348958984375" Y="2.078664306641" />
                  <Point X="27.354458984375" Y="2.078000732422" />
                  <Point X="27.4183828125" Y="2.070292724609" />
                  <Point X="27.441107421875" Y="2.070288574219" />
                  <Point X="27.466373046875" Y="2.073327636719" />
                  <Point X="27.4795703125" Y="2.075872558594" />
                  <Point X="27.553494140625" Y="2.095640869141" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.962662109375" Y="2.903497314453" />
                  <Point X="28.967328125" Y="2.906191162109" />
                  <Point X="29.123279296875" Y="2.689453125" />
                  <Point X="29.13685546875" Y="2.667016601562" />
                  <Point X="29.26219921875" Y="2.459883544922" />
                  <Point X="28.32067578125" Y="1.737427124023" />
                  <Point X="28.23078515625" Y="1.668451171875" />
                  <Point X="28.2182890625" Y="1.65694934082" />
                  <Point X="28.20446484375" Y="1.64172644043" />
                  <Point X="28.19939453125" Y="1.635654174805" />
                  <Point X="28.14619140625" Y="1.56624621582" />
                  <Point X="28.134681640625" Y="1.546817871094" />
                  <Point X="28.124505859375" Y="1.523767333984" />
                  <Point X="28.119923828125" Y="1.510987304688" />
                  <Point X="28.10010546875" Y="1.440121459961" />
                  <Point X="28.09665234375" Y="1.417823974609" />
                  <Point X="28.0958359375" Y="1.394253662109" />
                  <Point X="28.097740234375" Y="1.371764404297" />
                  <Point X="28.099140625" Y="1.364978637695" />
                  <Point X="28.11541015625" Y="1.286131469727" />
                  <Point X="28.122640625" Y="1.264563964844" />
                  <Point X="28.133646484375" Y="1.241399169922" />
                  <Point X="28.14008984375" Y="1.229953979492" />
                  <Point X="28.184337890625" Y="1.162696655273" />
                  <Point X="28.198892578125" Y="1.145450683594" />
                  <Point X="28.21613671875" Y="1.129360717773" />
                  <Point X="28.234353515625" Y="1.11603112793" />
                  <Point X="28.239873046875" Y="1.112924804688" />
                  <Point X="28.30399609375" Y="1.076828857422" />
                  <Point X="28.3254375" Y="1.06800769043" />
                  <Point X="28.350875" Y="1.061023681641" />
                  <Point X="28.363580078125" Y="1.058452392578" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046174926758" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.776837890625" Y="1.216636352539" />
                  <Point X="29.84594140625" Y="0.93278326416" />
                  <Point X="29.85021875" Y="0.905313659668" />
                  <Point X="29.890865234375" Y="0.644238891602" />
                  <Point X="28.81959765625" Y="0.357193237305" />
                  <Point X="28.716580078125" Y="0.32958984375" />
                  <Point X="28.700318359375" Y="0.323595489502" />
                  <Point X="28.680908203125" Y="0.314350860596" />
                  <Point X="28.674216796875" Y="0.310830841064" />
                  <Point X="28.589037109375" Y="0.261595672607" />
                  <Point X="28.570669921875" Y="0.247764663696" />
                  <Point X="28.551958984375" Y="0.229739868164" />
                  <Point X="28.5431328125" Y="0.219972000122" />
                  <Point X="28.492025390625" Y="0.154848770142" />
                  <Point X="28.48030078125" Y="0.135566986084" />
                  <Point X="28.47052734375" Y="0.114102912903" />
                  <Point X="28.46368359375" Y="0.09261151886" />
                  <Point X="28.462216796875" Y="0.084955970764" />
                  <Point X="28.445181640625" Y="-0.00399896574" />
                  <Point X="28.443630859375" Y="-0.02713318634" />
                  <Point X="28.44509765625" Y="-0.053597080231" />
                  <Point X="28.446646484375" Y="-0.066207824707" />
                  <Point X="28.463681640625" Y="-0.155162918091" />
                  <Point X="28.47052734375" Y="-0.176663116455" />
                  <Point X="28.48030078125" Y="-0.198127029419" />
                  <Point X="28.49202734375" Y="-0.217410476685" />
                  <Point X="28.49642578125" Y="-0.223014892578" />
                  <Point X="28.547533203125" Y="-0.288138122559" />
                  <Point X="28.564322265625" Y="-0.304770111084" />
                  <Point X="28.58596484375" Y="-0.321427734375" />
                  <Point X="28.5963671875" Y="-0.328392791748" />
                  <Point X="28.681546875" Y="-0.377628112793" />
                  <Point X="28.692708984375" Y="-0.383138275146" />
                  <Point X="28.716580078125" Y="-0.392149902344" />
                  <Point X="29.891474609375" Y="-0.706962341309" />
                  <Point X="29.8550234375" Y="-0.948734558105" />
                  <Point X="29.849541015625" Y="-0.972756469727" />
                  <Point X="29.801177734375" Y="-1.18469921875" />
                  <Point X="28.543853515625" Y="-1.019169555664" />
                  <Point X="28.4243828125" Y="-1.003440917969" />
                  <Point X="28.40049609375" Y="-1.003324951172" />
                  <Point X="28.36896484375" Y="-1.007165405273" />
                  <Point X="28.3602734375" Y="-1.008635925293" />
                  <Point X="28.193095703125" Y="-1.044972412109" />
                  <Point X="28.1627265625" Y="-1.055693359375" />
                  <Point X="28.130712890625" Y="-1.076807250977" />
                  <Point X="28.109517578125" Y="-1.098040039062" />
                  <Point X="28.103705078125" Y="-1.104418457031" />
                  <Point X="28.00265625" Y="-1.225947753906" />
                  <Point X="27.98662890625" Y="-1.250415527344" />
                  <Point X="27.974333984375" Y="-1.283385253906" />
                  <Point X="27.9694921875" Y="-1.311482299805" />
                  <Point X="27.968513671875" Y="-1.31891015625" />
                  <Point X="27.95403125" Y="-1.476296020508" />
                  <Point X="27.955111328125" Y="-1.508485595703" />
                  <Point X="27.965748046875" Y="-1.545636352539" />
                  <Point X="27.98021875" Y="-1.573184814453" />
                  <Point X="27.984412109375" Y="-1.580382080078" />
                  <Point X="28.076931640625" Y="-1.724288208008" />
                  <Point X="28.0869375" Y="-1.737243286133" />
                  <Point X="28.110630859375" Y="-1.760909301758" />
                  <Point X="29.213125" Y="-2.606882568359" />
                  <Point X="29.124822265625" Y="-2.749766845703" />
                  <Point X="29.11346484375" Y="-2.765905029297" />
                  <Point X="29.028982421875" Y="-2.885944824219" />
                  <Point X="27.907361328125" Y="-2.238376220703" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7782734375" Y="-2.167514648437" />
                  <Point X="27.745037109375" Y="-2.15851953125" />
                  <Point X="27.737103515625" Y="-2.156732910156" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.505974609375" Y="-2.119082275391" />
                  <Point X="27.467712890625" Y="-2.126591552734" />
                  <Point X="27.43781640625" Y="-2.139248535156" />
                  <Point X="27.430609375" Y="-2.142663330078" />
                  <Point X="27.26531640625" Y="-2.229655761719" />
                  <Point X="27.24114453125" Y="-2.246129638672" />
                  <Point X="27.216853515625" Y="-2.272389648438" />
                  <Point X="27.2004765625" Y="-2.298683349609" />
                  <Point X="27.197046875" Y="-2.304663330078" />
                  <Point X="27.110052734375" Y="-2.469956298828" />
                  <Point X="27.098732421875" Y="-2.500111816406" />
                  <Point X="27.094408203125" Y="-2.539170898438" />
                  <Point X="27.09771484375" Y="-2.572795166016" />
                  <Point X="27.09876953125" Y="-2.580381347656" />
                  <Point X="27.134703125" Y="-2.779348632812" />
                  <Point X="27.138986328125" Y="-2.795139160156" />
                  <Point X="27.151822265625" Y="-2.826078857422" />
                  <Point X="27.86128515625" Y="-4.054905517578" />
                  <Point X="27.78185546875" Y="-4.111640136719" />
                  <Point X="27.769169921875" Y="-4.1198515625" />
                  <Point X="27.701765625" Y="-4.163480957031" />
                  <Point X="26.839744140625" Y="-3.040072998047" />
                  <Point X="26.758548828125" Y="-2.934255371094" />
                  <Point X="26.747505859375" Y="-2.922178955078" />
                  <Point X="26.721923828125" Y="-2.900556152344" />
                  <Point X="26.70503515625" Y="-2.889698730469" />
                  <Point X="26.508798828125" Y="-2.763537353516" />
                  <Point X="26.479744140625" Y="-2.749644287109" />
                  <Point X="26.44095703125" Y="-2.741946289062" />
                  <Point X="26.40603125" Y="-2.74242578125" />
                  <Point X="26.398630859375" Y="-2.74281640625" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15537890625" Y="-2.76853515625" />
                  <Point X="26.12247265625" Y="-2.783798583984" />
                  <Point X="26.09537890625" Y="-2.803404296875" />
                  <Point X="26.0903359375" Y="-2.807319335938" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968861083984" />
                  <Point X="25.88725" Y="-2.9966875" />
                  <Point X="25.875623046875" Y="-3.025811279297" />
                  <Point X="25.871359375" Y="-3.045430175781" />
                  <Point X="25.82180859375" Y="-3.273399169922" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323119873047" />
                  <Point X="26.022041015625" Y="-4.859721679688" />
                  <Point X="26.02206640625" Y="-4.859915039062" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.963958984375" Y="-4.872212402344" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94157421875" Y="-4.75263671875" />
                  <Point X="23.925125" Y="-4.748404785156" />
                  <Point X="23.858755859375" Y="-4.731328613281" />
                  <Point X="23.875404296875" Y="-4.60486328125" />
                  <Point X="23.8792265625" Y="-4.57583984375" />
                  <Point X="23.879923828125" Y="-4.568100585938" />
                  <Point X="23.88055078125" Y="-4.541033203125" />
                  <Point X="23.8782421875" Y="-4.509323242188" />
                  <Point X="23.876666015625" Y="-4.497685058594" />
                  <Point X="23.871869140625" Y="-4.473573242188" />
                  <Point X="23.81613671875" Y="-4.193393554688" />
                  <Point X="23.811873046875" Y="-4.178464355469" />
                  <Point X="23.800970703125" Y="-4.149499023438" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.749794921875" Y="-4.070940185547" />
                  <Point X="23.738998046875" Y="-4.059784179688" />
                  <Point X="23.720515625" Y="-4.04357421875" />
                  <Point X="23.50573828125" Y="-3.855219238281" />
                  <Point X="23.493265625" Y="-3.84596875" />
                  <Point X="23.46698046875" Y="-3.829622558594" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.338654296875" Y="-3.794561767578" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.946326171875" Y="-3.795495849609" />
                  <Point X="22.918134765625" Y="-3.808269042969" />
                  <Point X="22.9045625" Y="-3.815811523438" />
                  <Point X="22.88412109375" Y="-3.829469970703" />
                  <Point X="22.64659765625" Y="-3.9881796875" />
                  <Point X="22.64031640625" Y="-3.992760009766" />
                  <Point X="22.619556640625" Y="-4.010134521484" />
                  <Point X="22.5972421875" Y="-4.032772460938" />
                  <Point X="22.589529296875" Y="-4.041629638672" />
                  <Point X="22.496798828125" Y="-4.162478515625" />
                  <Point X="22.2524140625" Y="-4.011162109375" />
                  <Point X="22.22965625" Y="-3.993639160156" />
                  <Point X="22.019138671875" Y="-3.831546386719" />
                  <Point X="22.599521484375" Y="-2.826291503906" />
                  <Point X="22.658509765625" Y="-2.724119384766" />
                  <Point X="22.6651484375" Y="-2.710083984375" />
                  <Point X="22.67605078125" Y="-2.681117919922" />
                  <Point X="22.680314453125" Y="-2.666187255859" />
                  <Point X="22.6865859375" Y="-2.634658935547" />
                  <Point X="22.688375" Y="-2.618796386719" />
                  <Point X="22.68926953125" Y="-2.586997070312" />
                  <Point X="22.684828125" Y="-2.555500244141" />
                  <Point X="22.67517578125" Y="-2.525188720703" />
                  <Point X="22.6690703125" Y="-2.510437255859" />
                  <Point X="22.65348046875" Y="-2.480237060547" />
                  <Point X="22.6457734375" Y="-2.467772460938" />
                  <Point X="22.62853515625" Y="-2.444176757812" />
                  <Point X="22.61900390625" Y="-2.433045654297" />
                  <Point X="22.60303125" Y="-2.417071777344" />
                  <Point X="22.59119140625" Y="-2.407026123047" />
                  <Point X="22.566033203125" Y="-2.388996582031" />
                  <Point X="22.55271484375" Y="-2.381012695312" />
                  <Point X="22.523884765625" Y="-2.366794677734" />
                  <Point X="22.509443359375" Y="-2.361087890625" />
                  <Point X="22.479826171875" Y="-2.352103027344" />
                  <Point X="22.449140625" Y="-2.348062744141" />
                  <Point X="22.418205078125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.206912109375" Y="-3.017708496094" />
                  <Point X="20.995982421875" Y="-2.740590087891" />
                  <Point X="20.97966796875" Y="-2.713234619141" />
                  <Point X="20.818734375" Y="-2.443373291016" />
                  <Point X="21.848666015625" Y="-1.653078979492" />
                  <Point X="21.951876953125" Y="-1.573881713867" />
                  <Point X="21.964248046875" Y="-1.562516723633" />
                  <Point X="21.986796875" Y="-1.537783569336" />
                  <Point X="21.996974609375" Y="-1.524415161133" />
                  <Point X="22.01555078125" Y="-1.494926391602" />
                  <Point X="22.022380859375" Y="-1.481963012695" />
                  <Point X="22.03396484375" Y="-1.455145629883" />
                  <Point X="22.03871875" Y="-1.441291870117" />
                  <Point X="22.0458125" Y="-1.413901611328" />
                  <Point X="22.048447265625" Y="-1.3988046875" />
                  <Point X="22.051251953125" Y="-1.368381591797" />
                  <Point X="22.051421875" Y="-1.353055297852" />
                  <Point X="22.04921484375" Y="-1.321389038086" />
                  <Point X="22.046794921875" Y="-1.305708007812" />
                  <Point X="22.039376953125" Y="-1.274969970703" />
                  <Point X="22.02694921875" Y="-1.245898925781" />
                  <Point X="22.00985546875" Y="-1.219297973633" />
                  <Point X="22.00019140625" Y="-1.206711181641" />
                  <Point X="21.977259765625" Y="-1.181515380859" />
                  <Point X="21.9668984375" Y="-1.171720336914" />
                  <Point X="21.944826171875" Y="-1.153791625977" />
                  <Point X="21.933115234375" Y="-1.145658203125" />
                  <Point X="21.90873046875" Y="-1.131306884766" />
                  <Point X="21.894564453125" Y="-1.124479492188" />
                  <Point X="21.86530078125" Y="-1.113255981445" />
                  <Point X="21.850203125" Y="-1.108859741211" />
                  <Point X="21.81831640625" Y="-1.102378417969" />
                  <Point X="21.802701171875" Y="-1.100532348633" />
                  <Point X="21.77137890625" Y="-1.09944128418" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.259236328125" Y="-0.974109069824" />
                  <Point X="20.254923828125" Y="-0.943946716309" />
                  <Point X="20.213548828125" Y="-0.654654541016" />
                  <Point X="21.374138671875" Y="-0.343675018311" />
                  <Point X="21.491712890625" Y="-0.312171264648" />
                  <Point X="21.50055859375" Y="-0.309331085205" />
                  <Point X="21.52908984375" Y="-0.297760955811" />
                  <Point X="21.559060546875" Y="-0.28182901001" />
                  <Point X="21.568634765625" Y="-0.275989776611" />
                  <Point X="21.594189453125" Y="-0.25825402832" />
                  <Point X="21.60710546875" Y="-0.247475891113" />
                  <Point X="21.6308359375" Y="-0.223810516357" />
                  <Point X="21.641650390625" Y="-0.210922988892" />
                  <Point X="21.66162890625" Y="-0.182304763794" />
                  <Point X="21.669060546875" Y="-0.169688461304" />
                  <Point X="21.68189453125" Y="-0.143467224121" />
                  <Point X="21.687296875" Y="-0.129861984253" />
                  <Point X="21.695814453125" Y="-0.102416992188" />
                  <Point X="21.69916796875" Y="-0.087423027039" />
                  <Point X="21.703412109375" Y="-0.057091175079" />
                  <Point X="21.704302734375" Y="-0.041752998352" />
                  <Point X="21.7035703125" Y="-0.009750630379" />
                  <Point X="21.702353515625" Y="0.003389266014" />
                  <Point X="21.698109375" Y="0.029374191284" />
                  <Point X="21.69508203125" Y="0.042219070435" />
                  <Point X="21.686564453125" Y="0.069664070129" />
                  <Point X="21.68013671875" Y="0.085299789429" />
                  <Point X="21.664609375" Y="0.115189781189" />
                  <Point X="21.655509765625" Y="0.129443756104" />
                  <Point X="21.634064453125" Y="0.157226425171" />
                  <Point X="21.6243125" Y="0.168033828735" />
                  <Point X="21.603275390625" Y="0.188030883789" />
                  <Point X="21.591990234375" Y="0.197220535278" />
                  <Point X="21.566435546875" Y="0.21495614624" />
                  <Point X="21.559515625" Y="0.219329681396" />
                  <Point X="21.534384765625" Y="0.232834442139" />
                  <Point X="21.50343359375" Y="0.245635925293" />
                  <Point X="21.491712890625" Y="0.249611297607" />
                  <Point X="20.2145546875" Y="0.591824768066" />
                  <Point X="20.26866796875" Y="0.957518615723" />
                  <Point X="20.277361328125" Y="0.989598327637" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="21.137654296875" Y="1.216701416016" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815673828" />
                  <Point X="21.2529453125" Y="1.204364379883" />
                  <Point X="21.263296875" Y="1.204703369141" />
                  <Point X="21.284857421875" Y="1.206589599609" />
                  <Point X="21.29511328125" Y="1.208053833008" />
                  <Point X="21.315400390625" Y="1.21208972168" />
                  <Point X="21.330298828125" Y="1.216194946289" />
                  <Point X="21.386857421875" Y="1.234027832031" />
                  <Point X="21.396552734375" Y="1.237677124023" />
                  <Point X="21.415486328125" Y="1.246008300781" />
                  <Point X="21.4247265625" Y="1.250690795898" />
                  <Point X="21.443470703125" Y="1.261513061523" />
                  <Point X="21.4521484375" Y="1.26717590332" />
                  <Point X="21.468828125" Y="1.279407470703" />
                  <Point X="21.48407421875" Y="1.293379516602" />
                  <Point X="21.497712890625" Y="1.308931762695" />
                  <Point X="21.504107421875" Y="1.317080932617" />
                  <Point X="21.516521484375" Y="1.334811035156" />
                  <Point X="21.5219921875" Y="1.343607055664" />
                  <Point X="21.531939453125" Y="1.361739624023" />
                  <Point X="21.538369140625" Y="1.375791503906" />
                  <Point X="21.5610625" Y="1.430580322266" />
                  <Point X="21.564498046875" Y="1.440344848633" />
                  <Point X="21.57028515625" Y="1.460193725586" />
                  <Point X="21.57263671875" Y="1.470278442383" />
                  <Point X="21.576396484375" Y="1.491588256836" />
                  <Point X="21.57763671875" Y="1.501868652344" />
                  <Point X="21.5789921875" Y="1.522507324219" />
                  <Point X="21.57809375" Y="1.543176269531" />
                  <Point X="21.57494921875" Y="1.563618896484" />
                  <Point X="21.572818359375" Y="1.573750854492" />
                  <Point X="21.567220703125" Y="1.594653198242" />
                  <Point X="21.564001953125" Y="1.604494873047" />
                  <Point X="21.556505859375" Y="1.623780639648" />
                  <Point X="21.549865234375" Y="1.637762451172" />
                  <Point X="21.52248046875" Y="1.690365478516" />
                  <Point X="21.517201171875" Y="1.699283691406" />
                  <Point X="21.505705078125" Y="1.71648828125" />
                  <Point X="21.499490234375" Y="1.72477355957" />
                  <Point X="21.485578125" Y="1.741353027344" />
                  <Point X="21.478498046875" Y="1.748912719727" />
                  <Point X="21.463556640625" Y="1.763215698242" />
                  <Point X="21.4556953125" Y="1.769958862305" />
                  <Point X="20.77238671875" Y="2.29428125" />
                  <Point X="20.997708984375" Y="2.680313232422" />
                  <Point X="21.020724609375" Y="2.709897705078" />
                  <Point X="21.273662109375" Y="3.035012695312" />
                  <Point X="21.69408203125" Y="2.792283691406" />
                  <Point X="21.74584375" Y="2.762398681641" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774013671875" Y="2.749386474609" />
                  <Point X="21.78370703125" Y="2.745738037109" />
                  <Point X="21.804349609375" Y="2.739229248047" />
                  <Point X="21.8143828125" Y="2.736657470703" />
                  <Point X="21.834671875" Y="2.732621582031" />
                  <Point X="21.85170703125" Y="2.730564453125" />
                  <Point X="21.9304765625" Y="2.723672851562" />
                  <Point X="21.940830078125" Y="2.723334228516" />
                  <Point X="21.961509765625" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.02356640625" Y="2.734227294922" />
                  <Point X="22.043005859375" Y="2.741302978516" />
                  <Point X="22.061556640625" Y="2.750451904297" />
                  <Point X="22.070580078125" Y="2.755531738281" />
                  <Point X="22.088833984375" Y="2.767160888672" />
                  <Point X="22.09725" Y="2.773190185547" />
                  <Point X="22.113376953125" Y="2.786131835938" />
                  <Point X="22.125900390625" Y="2.797855957031" />
                  <Point X="22.1818125" Y="2.853767578125" />
                  <Point X="22.188732421875" Y="2.861486572266" />
                  <Point X="22.2016875" Y="2.87762890625" />
                  <Point X="22.20772265625" Y="2.886052246094" />
                  <Point X="22.2193515625" Y="2.904308837891" />
                  <Point X="22.2244296875" Y="2.913329833984" />
                  <Point X="22.233578125" Y="2.931881347656" />
                  <Point X="22.240654296875" Y="2.951328125" />
                  <Point X="22.245568359375" Y="2.971419677734" />
                  <Point X="22.2474765625" Y="2.981594970703" />
                  <Point X="22.25030078125" Y="3.003056396484" />
                  <Point X="22.25108984375" Y="3.013377441406" />
                  <Point X="22.251541015625" Y="3.034040527344" />
                  <Point X="22.250611328125" Y="3.051177246094" />
                  <Point X="22.243720703125" Y="3.129947753906" />
                  <Point X="22.242255859375" Y="3.140205322266" />
                  <Point X="22.23821875" Y="3.160498046875" />
                  <Point X="22.235646484375" Y="3.170533203125" />
                  <Point X="22.22913671875" Y="3.191176025391" />
                  <Point X="22.22548828125" Y="3.200870605469" />
                  <Point X="22.217158203125" Y="3.219799072266" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="21.94061328125" Y="3.699915039062" />
                  <Point X="22.351630859375" Y="4.0150390625" />
                  <Point X="22.3878671875" Y="4.035170410156" />
                  <Point X="22.8074765625" Y="4.268295898437" />
                  <Point X="22.86558984375" Y="4.192560546875" />
                  <Point X="22.881435546875" Y="4.171909667969" />
                  <Point X="22.888181640625" Y="4.164046875" />
                  <Point X="22.902484375" Y="4.149106445313" />
                  <Point X="22.910041015625" Y="4.142028808594" />
                  <Point X="22.92662109375" Y="4.128115722656" />
                  <Point X="22.934904296875" Y="4.121900878906" />
                  <Point X="22.952103515625" Y="4.110407714844" />
                  <Point X="22.968564453125" Y="4.101201660156" />
                  <Point X="23.056236328125" Y="4.0555625" />
                  <Point X="23.065673828125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.0437890625" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.229275390625" Y="4.037489501953" />
                  <Point X="23.249142578125" Y="4.043281005859" />
                  <Point X="23.2667734375" Y="4.049974609375" />
                  <Point X="23.35808984375" Y="4.087798828125" />
                  <Point X="23.36742578125" Y="4.092277587891" />
                  <Point X="23.385560546875" Y="4.102227539063" />
                  <Point X="23.394359375" Y="4.10769921875" />
                  <Point X="23.41208984375" Y="4.120116210938" />
                  <Point X="23.420240234375" Y="4.126514160156" />
                  <Point X="23.4357890625" Y="4.140153320312" />
                  <Point X="23.4497578125" Y="4.155399414062" />
                  <Point X="23.46198828125" Y="4.172081054688" />
                  <Point X="23.4676484375" Y="4.1807578125" />
                  <Point X="23.478470703125" Y="4.199505371094" />
                  <Point X="23.48315234375" Y="4.208744140625" />
                  <Point X="23.491484375" Y="4.227683105469" />
                  <Point X="23.497681640625" Y="4.245463867188" />
                  <Point X="23.527404296875" Y="4.339728027344" />
                  <Point X="23.529974609375" Y="4.349763183594" />
                  <Point X="23.534009765625" Y="4.370049804688" />
                  <Point X="23.535474609375" Y="4.380301269531" />
                  <Point X="23.537361328125" Y="4.401861328125" />
                  <Point X="23.53769921875" Y="4.41221484375" />
                  <Point X="23.537248046875" Y="4.432896484375" />
                  <Point X="23.536458984375" Y="4.443225097656" />
                  <Point X="23.520734375" Y="4.562655273438" />
                  <Point X="24.068826171875" Y="4.7163203125" />
                  <Point X="24.112748046875" Y="4.7214609375" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.75901953125" Y="4.318875488281" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.021630859375" Y="4.085113037109" />
                  <Point X="25.052166015625" Y="4.090154052734" />
                  <Point X="25.0672265625" Y="4.093926269531" />
                  <Point X="25.0976640625" Y="4.1042578125" />
                  <Point X="25.11191015625" Y="4.11043359375" />
                  <Point X="25.139205078125" Y="4.125022460938" />
                  <Point X="25.1637578125" Y="4.143860351562" />
                  <Point X="25.184916015625" Y="4.166448242188" />
                  <Point X="25.1945703125" Y="4.178611328125" />
                  <Point X="25.2124296875" Y="4.205337402344" />
                  <Point X="25.21997265625" Y="4.218910644531" />
                  <Point X="25.232748046875" Y="4.247104980469" />
                  <Point X="25.23798046875" Y="4.261726074219" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.827876953125" Y="4.737912109375" />
                  <Point X="25.864228515625" Y="4.729135742188" />
                  <Point X="26.45359765625" Y="4.586844238281" />
                  <Point X="26.474919921875" Y="4.579110351562" />
                  <Point X="26.85825390625" Y="4.440072753906" />
                  <Point X="26.88114453125" Y="4.4293671875" />
                  <Point X="27.25045703125" Y="4.256651367187" />
                  <Point X="27.27262109375" Y="4.243738769531" />
                  <Point X="27.629431640625" Y="4.035860107422" />
                  <Point X="27.650294921875" Y="4.021023681641" />
                  <Point X="27.817783203125" Y="3.901915527344" />
                  <Point X="27.133587890625" Y="2.716854492188" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.061478515625" Y="2.591233642578" />
                  <Point X="27.050869140625" Y="2.567335693359" />
                  <Point X="27.041677734375" Y="2.540985595703" />
                  <Point X="27.0396015625" Y="2.534239990234" />
                  <Point X="27.01983203125" Y="2.460315429688" />
                  <Point X="27.017595703125" Y="2.449441650391" />
                  <Point X="27.014408203125" Y="2.427519775391" />
                  <Point X="27.01345703125" Y="2.416471679688" />
                  <Point X="27.012783203125" Y="2.392061035156" />
                  <Point X="27.01284765625" Y="2.385055664062" />
                  <Point X="27.014076171875" Y="2.364072753906" />
                  <Point X="27.021783203125" Y="2.30015625" />
                  <Point X="27.02380078125" Y="2.289035400391" />
                  <Point X="27.02914453125" Y="2.267108398438" />
                  <Point X="27.032470703125" Y="2.256302246094" />
                  <Point X="27.04073828125" Y="2.234212402344" />
                  <Point X="27.045322265625" Y="2.223882568359" />
                  <Point X="27.055685546875" Y="2.203837890625" />
                  <Point X="27.064869140625" Y="2.189106445312" />
                  <Point X="27.104423828125" Y="2.130814453125" />
                  <Point X="27.1112265625" Y="2.121959960938" />
                  <Point X="27.125818359375" Y="2.105114013672" />
                  <Point X="27.133607421875" Y="2.097122558594" />
                  <Point X="27.151921875" Y="2.080418701172" />
                  <Point X="27.157109375" Y="2.076016845703" />
                  <Point X="27.17327734375" Y="2.063575927734" />
                  <Point X="27.2315703125" Y="2.024022583008" />
                  <Point X="27.24128125" Y="2.018244995117" />
                  <Point X="27.26132421875" Y="2.00788293457" />
                  <Point X="27.27165625" Y="2.003298339844" />
                  <Point X="27.293744140625" Y="1.995032470703" />
                  <Point X="27.304544921875" Y="1.991708251953" />
                  <Point X="27.326462890625" Y="1.986366210938" />
                  <Point X="27.343080078125" Y="1.983684692383" />
                  <Point X="27.40700390625" Y="1.97597668457" />
                  <Point X="27.418365234375" Y="1.975292724609" />
                  <Point X="27.44108984375" Y="1.975288574219" />
                  <Point X="27.452453125" Y="1.975968505859" />
                  <Point X="27.47771875" Y="1.979007568359" />
                  <Point X="27.484361328125" Y="1.980046142578" />
                  <Point X="27.50411328125" Y="1.984097290039" />
                  <Point X="27.578037109375" Y="2.003865600586" />
                  <Point X="27.584" Y="2.005671875" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.94040625" Y="2.780951171875" />
                  <Point X="29.04395703125" Y="2.637037841797" />
                  <Point X="29.055576171875" Y="2.617835449219" />
                  <Point X="29.136884765625" Y="2.483471191406" />
                  <Point X="28.26284375" Y="1.812795654297" />
                  <Point X="28.172953125" Y="1.743819702148" />
                  <Point X="28.16644921875" Y="1.738349487305" />
                  <Point X="28.1479609375" Y="1.720815917969" />
                  <Point X="28.13413671875" Y="1.705593017578" />
                  <Point X="28.12399609375" Y="1.693448608398" />
                  <Point X="28.07079296875" Y="1.624040649414" />
                  <Point X="28.06445703125" Y="1.614666992188" />
                  <Point X="28.052947265625" Y="1.595238769531" />
                  <Point X="28.0477734375" Y="1.585183959961" />
                  <Point X="28.03759765625" Y="1.562133422852" />
                  <Point X="28.035080078125" Y="1.555829345703" />
                  <Point X="28.02843359375" Y="1.536573242188" />
                  <Point X="28.008615234375" Y="1.465707519531" />
                  <Point X="28.006224609375" Y="1.454660400391" />
                  <Point X="28.002771484375" Y="1.432362915039" />
                  <Point X="28.001708984375" Y="1.421112548828" />
                  <Point X="28.000892578125" Y="1.397542236328" />
                  <Point X="28.001173828125" Y="1.38623815918" />
                  <Point X="28.003078125" Y="1.363748779297" />
                  <Point X="28.0061015625" Y="1.345777954102" />
                  <Point X="28.02237109375" Y="1.266930786133" />
                  <Point X="28.025337890625" Y="1.255934692383" />
                  <Point X="28.032568359375" Y="1.23436706543" />
                  <Point X="28.03683203125" Y="1.223795776367" />
                  <Point X="28.047837890625" Y="1.200630859375" />
                  <Point X="28.060724609375" Y="1.177740600586" />
                  <Point X="28.10497265625" Y="1.110483276367" />
                  <Point X="28.111736328125" Y="1.101425537109" />
                  <Point X="28.126291015625" Y="1.08417956543" />
                  <Point X="28.13408203125" Y="1.075991210938" />
                  <Point X="28.151326171875" Y="1.059901245117" />
                  <Point X="28.160037109375" Y="1.052693237305" />
                  <Point X="28.17825390625" Y="1.039363647461" />
                  <Point X="28.193279296875" Y="1.030135253906" />
                  <Point X="28.25740234375" Y="0.994039306641" />
                  <Point X="28.2678515625" Y="0.988973388672" />
                  <Point X="28.28929296875" Y="0.980152160645" />
                  <Point X="28.30028515625" Y="0.976397766113" />
                  <Point X="28.32572265625" Y="0.969413696289" />
                  <Point X="28.3511328125" Y="0.964271362305" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.44403125" Y="0.952199768066" />
                  <Point X="28.46571875" Y="0.951222717285" />
                  <Point X="28.49121875" Y="0.952032409668" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="29.704701171875" Y="1.111319580078" />
                  <Point X="29.752689453125" Y="0.914204101562" />
                  <Point X="29.756349609375" Y="0.89069708252" />
                  <Point X="29.78387109375" Y="0.713921264648" />
                  <Point X="28.795009765625" Y="0.448956176758" />
                  <Point X="28.6919921875" Y="0.421352752686" />
                  <Point X="28.68372265625" Y="0.418726745605" />
                  <Point X="28.65946875" Y="0.409364379883" />
                  <Point X="28.64005859375" Y="0.400119750977" />
                  <Point X="28.62667578125" Y="0.393079620361" />
                  <Point X="28.54149609375" Y="0.343844421387" />
                  <Point X="28.531890625" Y="0.337485168457" />
                  <Point X="28.5135234375" Y="0.323654174805" />
                  <Point X="28.50476171875" Y="0.31618258667" />
                  <Point X="28.48605078125" Y="0.298157775879" />
                  <Point X="28.4683984375" Y="0.278621887207" />
                  <Point X="28.417291015625" Y="0.21349861145" />
                  <Point X="28.410853515625" Y="0.20420640564" />
                  <Point X="28.39912890625" Y="0.184924667358" />
                  <Point X="28.393841796875" Y="0.174935134888" />
                  <Point X="28.384068359375" Y="0.15347102356" />
                  <Point X="28.380005859375" Y="0.142928604126" />
                  <Point X="28.373162109375" Y="0.121437141418" />
                  <Point X="28.3689140625" Y="0.102832679749" />
                  <Point X="28.35187890625" Y="0.01387773037" />
                  <Point X="28.35039453125" Y="0.002354999542" />
                  <Point X="28.34884375" Y="-0.020779190063" />
                  <Point X="28.34877734375" Y="-0.032390651703" />
                  <Point X="28.350244140625" Y="-0.058854587555" />
                  <Point X="28.353341796875" Y="-0.084075889587" />
                  <Point X="28.370376953125" Y="-0.173030990601" />
                  <Point X="28.37316015625" Y="-0.18398538208" />
                  <Point X="28.380005859375" Y="-0.205485610962" />
                  <Point X="28.384068359375" Y="-0.216031600952" />
                  <Point X="28.393841796875" Y="-0.237495422363" />
                  <Point X="28.399130859375" Y="-0.247487625122" />
                  <Point X="28.410857421875" Y="-0.266770996094" />
                  <Point X="28.421693359375" Y="-0.281666442871" />
                  <Point X="28.47280078125" Y="-0.346789703369" />
                  <Point X="28.480673828125" Y="-0.355628295898" />
                  <Point X="28.497462890625" Y="-0.372260253906" />
                  <Point X="28.50637890625" Y="-0.380053436279" />
                  <Point X="28.528021484375" Y="-0.396711090088" />
                  <Point X="28.548826171875" Y="-0.41064151001" />
                  <Point X="28.634005859375" Y="-0.459876708984" />
                  <Point X="28.639494140625" Y="-0.462813934326" />
                  <Point X="28.65915625" Y="-0.472015930176" />
                  <Point X="28.68302734375" Y="-0.481027526855" />
                  <Point X="28.6919921875" Y="-0.483912872314" />
                  <Point X="29.784880859375" Y="-0.776751586914" />
                  <Point X="29.761619140625" Y="-0.931041137695" />
                  <Point X="29.756921875" Y="-0.951618530273" />
                  <Point X="29.7278046875" Y="-1.079219604492" />
                  <Point X="28.55625390625" Y="-0.924982299805" />
                  <Point X="28.436783203125" Y="-0.909253723145" />
                  <Point X="28.42484375" Y="-0.908442077637" />
                  <Point X="28.40095703125" Y="-0.90832598877" />
                  <Point X="28.389009765625" Y="-0.909021850586" />
                  <Point X="28.357478515625" Y="-0.912862243652" />
                  <Point X="28.340095703125" Y="-0.915803344727" />
                  <Point X="28.17291796875" Y="-0.952139892578" />
                  <Point X="28.161470703125" Y="-0.955390563965" />
                  <Point X="28.1311015625" Y="-0.966111450195" />
                  <Point X="28.110421875" Y="-0.976388305664" />
                  <Point X="28.078408203125" Y="-0.997502075195" />
                  <Point X="28.063478515625" Y="-1.00969140625" />
                  <Point X="28.042283203125" Y="-1.030924316406" />
                  <Point X="28.030658203125" Y="-1.043681030273" />
                  <Point X="27.929609375" Y="-1.165210327148" />
                  <Point X="27.9231875" Y="-1.173892578125" />
                  <Point X="27.90716015625" Y="-1.198360473633" />
                  <Point X="27.8976171875" Y="-1.217221557617" />
                  <Point X="27.885322265625" Y="-1.25019128418" />
                  <Point X="27.880712890625" Y="-1.267252197266" />
                  <Point X="27.87587109375" Y="-1.295349121094" />
                  <Point X="27.8739140625" Y="-1.310205200195" />
                  <Point X="27.859431640625" Y="-1.467591064453" />
                  <Point X="27.859083984375" Y="-1.479481811523" />
                  <Point X="27.8601640625" Y="-1.511671386719" />
                  <Point X="27.86378125" Y="-1.534634643555" />
                  <Point X="27.87441796875" Y="-1.57178527832" />
                  <Point X="27.88164453125" Y="-1.589814086914" />
                  <Point X="27.896115234375" Y="-1.617362426758" />
                  <Point X="27.904501953125" Y="-1.631757324219" />
                  <Point X="27.997021484375" Y="-1.775663574219" />
                  <Point X="28.00174609375" Y="-1.782358032227" />
                  <Point X="28.01980078125" Y="-1.80445715332" />
                  <Point X="28.043494140625" Y="-1.828123291016" />
                  <Point X="28.052798828125" Y="-1.836277832031" />
                  <Point X="29.087173828125" Y="-2.629980957031" />
                  <Point X="29.04550390625" Y="-2.697406982422" />
                  <Point X="29.035775390625" Y="-2.711230224609" />
                  <Point X="29.001275390625" Y="-2.760251220703" />
                  <Point X="27.954861328125" Y="-2.156103759766" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.837419921875" Y="-2.089220214844" />
                  <Point X="27.81473828125" Y="-2.079791748047" />
                  <Point X="27.803091796875" Y="-2.075813720703" />
                  <Point X="27.76985546875" Y="-2.066818603516" />
                  <Point X="27.75398828125" Y="-2.063245361328" />
                  <Point X="27.555021484375" Y="-2.027311889648" />
                  <Point X="27.543201171875" Y="-2.025934570313" />
                  <Point X="27.5110390625" Y="-2.024217407227" />
                  <Point X="27.4876796875" Y="-2.025860717773" />
                  <Point X="27.44941796875" Y="-2.033369873047" />
                  <Point X="27.43067578125" Y="-2.039108520508" />
                  <Point X="27.400779296875" Y="-2.051765625" />
                  <Point X="27.386365234375" Y="-2.058595214844" />
                  <Point X="27.221072265625" Y="-2.145587646484" />
                  <Point X="27.211814453125" Y="-2.151153564453" />
                  <Point X="27.187642578125" Y="-2.167627441406" />
                  <Point X="27.17140625" Y="-2.181619873047" />
                  <Point X="27.147115234375" Y="-2.207879882812" />
                  <Point X="27.13621484375" Y="-2.222164550781" />
                  <Point X="27.119837890625" Y="-2.248458251953" />
                  <Point X="27.112978515625" Y="-2.260418212891" />
                  <Point X="27.025984375" Y="-2.425711181641" />
                  <Point X="27.02111328125" Y="-2.436568603516" />
                  <Point X="27.00979296875" Y="-2.466724121094" />
                  <Point X="27.00430859375" Y="-2.489658203125" />
                  <Point X="26.999984375" Y="-2.528717285156" />
                  <Point X="26.999865234375" Y="-2.548468505859" />
                  <Point X="27.003171875" Y="-2.582092773438" />
                  <Point X="27.00528125" Y="-2.597265136719" />
                  <Point X="27.04121484375" Y="-2.796232421875" />
                  <Point X="27.043015625" Y="-2.80421875" />
                  <Point X="27.05123828125" Y="-2.831543212891" />
                  <Point X="27.06407421875" Y="-2.862482910156" />
                  <Point X="27.06955078125" Y="-2.873578857422" />
                  <Point X="27.735896484375" Y="-4.027722900391" />
                  <Point X="27.723755859375" Y="-4.036082763672" />
                  <Point X="26.91511328125" Y="-2.982240722656" />
                  <Point X="26.83391796875" Y="-2.876423095703" />
                  <Point X="26.82865625" Y="-2.870146972656" />
                  <Point X="26.80883203125" Y="-2.849624267578" />
                  <Point X="26.78325" Y="-2.828001464844" />
                  <Point X="26.773296875" Y="-2.820645019531" />
                  <Point X="26.756408203125" Y="-2.809787597656" />
                  <Point X="26.560171875" Y="-2.683626220703" />
                  <Point X="26.54978125" Y="-2.677831542969" />
                  <Point X="26.5207265625" Y="-2.663938476562" />
                  <Point X="26.49823828125" Y="-2.656461669922" />
                  <Point X="26.459451171875" Y="-2.648763671875" />
                  <Point X="26.43965234375" Y="-2.646955322266" />
                  <Point X="26.4047265625" Y="-2.647434814453" />
                  <Point X="26.38992578125" Y="-2.648216064453" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.164625" Y="-2.669565185547" />
                  <Point X="26.135990234375" Y="-2.675534667969" />
                  <Point X="26.115404296875" Y="-2.682354736328" />
                  <Point X="26.082498046875" Y="-2.697618164062" />
                  <Point X="26.066779296875" Y="-2.706835449219" />
                  <Point X="26.039685546875" Y="-2.726441162109" />
                  <Point X="26.029599609375" Y="-2.734271240234" />
                  <Point X="25.863876953125" Y="-2.872063476562" />
                  <Point X="25.852654296875" Y="-2.883087646484" />
                  <Point X="25.83218359375" Y="-2.906837158203" />
                  <Point X="25.822935546875" Y="-2.9195625" />
                  <Point X="25.80604296875" Y="-2.947388916016" />
                  <Point X="25.799021484375" Y="-2.961464355469" />
                  <Point X="25.78739453125" Y="-2.990588134766" />
                  <Point X="25.7827890625" Y="-3.005636474609" />
                  <Point X="25.778525390625" Y="-3.025255371094" />
                  <Point X="25.728974609375" Y="-3.253224365234" />
                  <Point X="25.72758203125" Y="-3.261298828125" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323169677734" />
                  <Point X="25.7255546875" Y="-3.335520019531" />
                  <Point X="25.83308984375" Y="-4.152324707031" />
                  <Point X="25.686810546875" Y="-3.606400146484" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480122314453" />
                  <Point X="25.64214453125" Y="-3.453576416016" />
                  <Point X="25.62678515625" Y="-3.423811279297" />
                  <Point X="25.62040625" Y="-3.413205810547" />
                  <Point X="25.607431640625" Y="-3.394512939453" />
                  <Point X="25.456677734375" Y="-3.177305419922" />
                  <Point X="25.446669921875" Y="-3.165170166016" />
                  <Point X="25.424787109375" Y="-3.142716308594" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104936767578" />
                  <Point X="25.3452421875" Y="-3.090829345703" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.310580078125" Y="-3.078708007812" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.9489375" Y="-3.003108886719" />
                  <Point X="24.93501953125" Y="-3.0063046875" />
                  <Point X="24.914943359375" Y="-3.012535400391" />
                  <Point X="24.68166015625" Y="-3.084937988281" />
                  <Point X="24.66707421875" Y="-3.090828369141" />
                  <Point X="24.639072265625" Y="-3.104936279297" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.5875234375" Y="-3.142718017578" />
                  <Point X="24.56563671875" Y="-3.165177001953" />
                  <Point X="24.555626953125" Y="-3.177315673828" />
                  <Point X="24.542654296875" Y="-3.196008300781" />
                  <Point X="24.391900390625" Y="-3.413216064453" />
                  <Point X="24.38752734375" Y="-3.420136962891" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.136177478616" Y="4.663479052241" />
                  <Point X="25.352958724812" Y="4.690829653805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.652804057701" Y="4.715279593543" />
                  <Point X="24.196179079832" Y="4.73122528914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.543618385645" Y="4.554192995537" />
                  <Point X="25.327724090248" Y="4.596652959953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.678515159474" Y="4.619323835377" />
                  <Point X="23.828707554996" Y="4.648999770845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.833618822883" Y="4.449008050407" />
                  <Point X="25.302489455685" Y="4.502476266101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.704226261246" Y="4.52336807721" />
                  <Point X="23.527208690688" Y="4.564470436479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.059254487524" Y="4.346070772665" />
                  <Point X="25.277254821121" Y="4.40829957225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.729937363019" Y="4.427412319043" />
                  <Point X="23.533037853539" Y="4.469208970918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.272943963196" Y="4.243550665034" />
                  <Point X="25.252020186558" Y="4.314122878398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.755648464791" Y="4.331456560876" />
                  <Point X="23.534588064026" Y="4.374096929667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.446508148264" Y="4.142431763427" />
                  <Point X="25.220540693215" Y="4.220164259821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.784878746544" Y="4.235377910237" />
                  <Point X="23.508554952059" Y="4.279948119261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.620072333331" Y="4.04131286182" />
                  <Point X="25.142850465713" Y="4.127819355639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.856426991603" Y="4.137821483755" />
                  <Point X="23.470795098135" Y="4.186208815707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.853934621633" Y="4.207750058216" />
                  <Point X="22.707690498039" Y="4.212857015545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.762423886863" Y="3.941283929324" />
                  <Point X="23.371680593384" Y="4.094612063773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.954284702744" Y="4.109187849456" />
                  <Point X="22.54671132726" Y="4.123420625353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.785172211642" Y="3.845431633609" />
                  <Point X="22.385732082887" Y="4.03398423773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.73137513386" Y="3.752252362253" />
                  <Point X="22.258166954114" Y="3.943381003479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.677578056079" Y="3.659073090898" />
                  <Point X="22.13958367857" Y="3.852464116001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.623780978298" Y="3.565893819542" />
                  <Point X="22.021000403027" Y="3.761547228524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.569983900516" Y="3.472714548186" />
                  <Point X="21.958654601471" Y="3.66866648518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.516186822735" Y="3.37953527683" />
                  <Point X="22.014665446435" Y="3.571652636666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.462389744954" Y="3.286356005474" />
                  <Point X="22.0706762914" Y="3.474638788151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.408592667173" Y="3.193176734118" />
                  <Point X="22.126687136364" Y="3.377624939637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.354795589391" Y="3.099997462762" />
                  <Point X="22.182697981329" Y="3.280611091122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.30099851161" Y="3.006818191407" />
                  <Point X="22.231446734139" Y="3.183850840454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.247201433829" Y="2.913638920051" />
                  <Point X="22.247369452632" Y="3.088236900163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.956273649831" Y="2.758898896443" />
                  <Point X="28.905294048493" Y="2.76067914335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.193404356048" Y="2.820459648695" />
                  <Point X="22.24899350013" Y="2.993122280467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.288099213371" Y="3.026677448361" />
                  <Point X="21.267730712015" Y="3.027388732101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.026434112737" Y="2.661390932382" />
                  <Point X="28.750039354113" Y="2.671042850036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.139607278266" Y="2.727280377339" />
                  <Point X="22.216105566961" Y="2.899212845692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.4633443586" Y="2.925499846331" />
                  <Point X="21.195732297929" Y="2.934845065415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.088044049963" Y="2.564181559257" />
                  <Point X="28.594784659733" Y="2.581406556723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.085810258767" Y="2.634101103948" />
                  <Point X="22.135030650772" Y="2.806986137443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.638589503828" Y="2.824322244302" />
                  <Point X="21.123733883843" Y="2.842301398729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.116874628189" Y="2.468116866572" />
                  <Point X="28.439529965354" Y="2.491770263409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.041555548263" Y="2.540588605784" />
                  <Point X="21.051735469757" Y="2.749757732042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.998385211121" Y="2.377196701484" />
                  <Point X="28.284275270974" Y="2.402133970096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.017150956344" Y="2.446382926204" />
                  <Point X="20.984136600752" Y="2.657060429856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.879895794053" Y="2.286276536396" />
                  <Point X="28.129020576594" Y="2.312497676782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.015606787458" Y="2.351378943062" />
                  <Point X="20.929760767678" Y="2.563901369081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.761406376984" Y="2.195356371309" />
                  <Point X="27.973765882214" Y="2.222861383469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.032686904468" Y="2.255724585524" />
                  <Point X="20.875384934604" Y="2.470742308305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.642916959916" Y="2.104436206221" />
                  <Point X="27.818511187834" Y="2.133225090156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.085416744356" Y="2.158825312232" />
                  <Point X="20.821009101529" Y="2.377583247529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.524427542848" Y="2.013516041133" />
                  <Point X="27.663256493455" Y="2.043588796842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.177746975123" Y="2.060543162818" />
                  <Point X="20.786119470506" Y="2.283743713584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.405938125779" Y="1.922595876046" />
                  <Point X="20.915907646099" Y="2.184153503913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.287448708711" Y="1.831675710958" />
                  <Point X="21.045695821692" Y="2.084563294242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.169295965004" Y="1.740743788977" />
                  <Point X="21.175483997285" Y="1.984973084571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.089520060396" Y="1.648471718245" />
                  <Point X="21.305272172878" Y="1.8853828749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.034904624746" Y="1.555321024575" />
                  <Point X="21.435060348471" Y="1.785792665229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.007643065932" Y="1.461215112478" />
                  <Point X="21.52390334605" Y="1.687632292681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.002860047515" Y="1.366324232453" />
                  <Point X="21.568191584496" Y="1.591027806606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.02161160417" Y="1.270611506958" />
                  <Point X="21.576888450236" Y="1.495666198654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.063116967077" Y="1.174104201038" />
                  <Point X="21.549050823119" Y="1.401580403306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.726701238122" Y="1.020952651471" />
                  <Point X="29.166808076107" Y="1.040504551521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.133514232066" Y="1.076587967666" />
                  <Point X="21.497188305483" Y="1.308333575621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.750041766053" Y="0.925079675567" />
                  <Point X="28.596141211532" Y="0.965374770847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.303170659791" Y="0.975605527953" />
                  <Point X="21.338603981769" Y="1.218813555526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.043275084571" Y="1.229126667869" />
                  <Point X="20.348838615793" Y="1.253376923722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.765882012194" Y="0.829468615274" />
                  <Point X="20.323321798289" Y="1.159210083916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.780762076278" Y="0.733891085278" />
                  <Point X="20.297804980786" Y="1.06504324411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.535591168747" Y="0.647394735317" />
                  <Point X="20.272287831596" Y="0.970876415886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.221734523993" Y="0.563296944154" />
                  <Point X="20.256659213816" Y="0.876364272537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.907877879239" Y="0.47919915299" />
                  <Point X="20.242665400465" Y="0.781795040559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.62824699962" Y="0.393906171772" />
                  <Point X="20.228671587114" Y="0.687225808581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.49171678192" Y="0.303616005325" />
                  <Point X="20.214677773763" Y="0.592656576603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.415707171619" Y="0.211212412697" />
                  <Point X="20.618890823671" Y="0.483483239153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.372301996374" Y="0.117670248108" />
                  <Point X="21.026814963835" Y="0.374180307576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.353676171157" Y="0.023262769548" />
                  <Point X="21.434739103998" Y="0.264877375998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.351825549382" Y="-0.071730512024" />
                  <Point X="21.62881670975" Y="0.163042129956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.369298330712" Y="-0.167398581701" />
                  <Point X="21.687724216239" Y="0.065927127792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.409078461273" Y="-0.263845641179" />
                  <Point X="21.704026885395" Y="-0.029700080668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.486716818893" Y="-0.361614739078" />
                  <Point X="21.68904329262" Y="-0.124234748787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.637882606435" Y="-0.461951471408" />
                  <Point X="21.636176246803" Y="-0.217446497574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.013781393422" Y="-0.570136053009" />
                  <Point X="21.504106259656" Y="-0.307892418705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.421705356354" Y="-0.679438978397" />
                  <Point X="21.193412017673" Y="-0.392100643407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.783316948882" Y="-0.787124640174" />
                  <Point X="20.879555222929" Y="-0.476198429332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.769060445586" Y="-0.881684698817" />
                  <Point X="20.565698428185" Y="-0.560296215258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.751330117155" Y="-0.976123448813" />
                  <Point X="28.653514564102" Y="-0.93778688494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.296338790143" Y="-0.92531403207" />
                  <Point X="20.251841633441" Y="-0.644394001184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.729810409835" Y="-1.070429870782" />
                  <Point X="29.636213259581" Y="-1.067161386273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.06101986245" Y="-1.012154420747" />
                  <Point X="20.225545339728" Y="-0.738533621081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.980180617758" Y="-1.104389358826" />
                  <Point X="20.23920890545" Y="-0.834068670018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.90809713162" Y="-1.196930054731" />
                  <Point X="20.252872471171" Y="-0.929603718956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.876639607334" Y="-1.290889440485" />
                  <Point X="20.272322796471" Y="-1.02534084599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.866975433049" Y="-1.385609866791" />
                  <Point X="21.975525502815" Y="-1.179875901805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.32341856453" Y="-1.157103825729" />
                  <Point X="20.296822599816" Y="-1.121254304684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.859114567011" Y="-1.480393266008" />
                  <Point X="22.039910825857" Y="-1.277182193538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.752751835867" Y="-1.232233611149" />
                  <Point X="20.321322403162" Y="-1.217167763378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.876125493869" Y="-1.576045207372" />
                  <Point X="22.050860987342" Y="-1.372622488311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.931030644674" Y="-1.673020444196" />
                  <Point X="22.028881890504" Y="-1.466912868045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.993548420858" Y="-1.770261519755" />
                  <Point X="21.966724661972" Y="-1.559800196504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.095276203884" Y="-1.868871838926" />
                  <Point X="21.851587250909" Y="-1.650837416221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.225064831668" Y="-1.968462064387" />
                  <Point X="21.733097808183" Y="-1.741757580412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.354853459453" Y="-2.068052289849" />
                  <Point X="27.642951809092" Y="-2.043192136416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.440435772921" Y="-2.036120120599" />
                  <Point X="21.614608339667" Y="-1.832677743703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.484642087238" Y="-2.167642515311" />
                  <Point X="27.942027097735" Y="-2.14869398234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.260397718461" Y="-2.124890959908" />
                  <Point X="21.49611887115" Y="-1.923597906995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.614430715022" Y="-2.267232740773" />
                  <Point X="28.117271940725" Y="-2.249871573815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.141084975969" Y="-2.215782373838" />
                  <Point X="21.377629402633" Y="-2.014518070286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.744219342807" Y="-2.366822966235" />
                  <Point X="28.292516656152" Y="-2.351049160836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.087427410619" Y="-2.308966517075" />
                  <Point X="21.259139934116" Y="-2.105438233577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.874007970592" Y="-2.466413191697" />
                  <Point X="28.467761371579" Y="-2.452226747856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.038301062905" Y="-2.402308893919" />
                  <Point X="21.140650465599" Y="-2.196358396868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.003796598376" Y="-2.566003417159" />
                  <Point X="28.643006087006" Y="-2.553404334877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.00358937506" Y="-2.496154641778" />
                  <Point X="21.022160997082" Y="-2.287278560159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.066610262565" Y="-2.663254825355" />
                  <Point X="28.818250802433" Y="-2.654581921897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.004443922866" Y="-2.591242389953" />
                  <Point X="22.623464714604" Y="-2.438255224873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.234105256045" Y="-2.424658492971" />
                  <Point X="20.903671528565" Y="-2.37819872345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.004174124631" Y="-2.756132414083" />
                  <Point X="28.99349551786" Y="-2.755759508918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.021468414062" Y="-2.686894804994" />
                  <Point X="26.533067347374" Y="-2.669839463925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.258961808606" Y="-2.660267487589" />
                  <Point X="22.678373494468" Y="-2.535230588426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.078850578841" Y="-2.514294786885" />
                  <Point X="20.835128069108" Y="-2.470863039811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.0387448784" Y="-2.782556019131" />
                  <Point X="26.695401729871" Y="-2.770566212185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.014543173757" Y="-2.746790107491" />
                  <Point X="22.687044706978" Y="-2.630591300548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.923595901637" Y="-2.603931080798" />
                  <Point X="20.893022140045" Y="-2.567942652025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.072562239838" Y="-2.878794854123" />
                  <Point X="26.828768954153" Y="-2.87028140499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.904825143829" Y="-2.838016576167" />
                  <Point X="22.658207919728" Y="-2.724642204456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.768341224433" Y="-2.693567374711" />
                  <Point X="20.950916210981" Y="-2.66502226424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.128573242241" Y="-2.975808708136" />
                  <Point X="26.904164844202" Y="-2.967972194196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.816603079576" Y="-2.929993700505" />
                  <Point X="22.604411574688" Y="-2.8178215014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.613086547229" Y="-2.783203668624" />
                  <Point X="21.012453022533" Y="-2.762229083759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.184584244645" Y="-3.072822562148" />
                  <Point X="26.97911354062" Y="-3.065647367056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.778856140654" Y="-3.023733455061" />
                  <Point X="22.550614676578" Y="-2.91100077903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.457831870025" Y="-2.872839962538" />
                  <Point X="21.086782374514" Y="-2.859882628635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.240595247048" Y="-3.16983641616" />
                  <Point X="27.054062448031" Y="-3.163322547283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.758350282646" Y="-3.118075281428" />
                  <Point X="25.372581605752" Y="-3.104603942385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.694581552214" Y="-3.0809276588" />
                  <Point X="22.496817723167" Y="-3.004180054729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.302577192821" Y="-2.962476256451" />
                  <Point X="21.161111726495" Y="-2.95753617351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.296606249452" Y="-3.266850270173" />
                  <Point X="27.129011355442" Y="-3.260997727511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.737844375108" Y="-3.212417106066" />
                  <Point X="25.474668594493" Y="-3.203226805296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.560582460922" Y="-3.17130621413" />
                  <Point X="22.443020769756" Y="-3.097359330428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.352617251855" Y="-3.363864124185" />
                  <Point X="27.203960262854" Y="-3.358672907739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.724733710043" Y="-3.307017178262" />
                  <Point X="25.542282743306" Y="-3.300645850109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.495403302699" Y="-3.264088014478" />
                  <Point X="22.389223816345" Y="-3.190538606127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.408628254259" Y="-3.460877978197" />
                  <Point X="27.278909170265" Y="-3.456348087967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.734361153165" Y="-3.402411282692" />
                  <Point X="25.609897035012" Y="-3.398064899912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.4309891855" Y="-3.356896530648" />
                  <Point X="22.335426862934" Y="-3.283717881826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.464639256662" Y="-3.55789183221" />
                  <Point X="27.353858077676" Y="-3.554023268194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.746933655994" Y="-3.497908230874" />
                  <Point X="25.656897678544" Y="-3.49476410526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.372107300334" Y="-3.449898236617" />
                  <Point X="22.281629909522" Y="-3.376897157525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.520650259066" Y="-3.654905686222" />
                  <Point X="27.428806985087" Y="-3.651698448422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.759506158823" Y="-3.593405179056" />
                  <Point X="25.682609018511" Y="-3.590719871744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.342247058364" Y="-3.543913400698" />
                  <Point X="22.227832956111" Y="-3.470076433224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.576661261469" Y="-3.751919540234" />
                  <Point X="27.503755892499" Y="-3.74937362865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.772078661652" Y="-3.688902127237" />
                  <Point X="25.708320188442" Y="-3.686675632291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.317012361467" Y="-3.638090092373" />
                  <Point X="22.1740360027" Y="-3.563255708923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.632672263873" Y="-3.848933394247" />
                  <Point X="27.57870479991" Y="-3.847048808878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.78465116448" Y="-3.784399075419" />
                  <Point X="25.734031325161" Y="-3.782631391678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.29177766457" Y="-3.732266784048" />
                  <Point X="22.120239049289" Y="-3.656434984622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.688683266276" Y="-3.945947248259" />
                  <Point X="27.653653707321" Y="-3.944723989105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.797223667309" Y="-3.8798960236" />
                  <Point X="25.759742461879" Y="-3.878587151065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.266542967673" Y="-3.826443475723" />
                  <Point X="23.321644486275" Y="-3.793446893661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.992041662665" Y="-3.781936909434" />
                  <Point X="22.066442095878" Y="-3.74961426032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.809796170138" Y="-3.975392971782" />
                  <Point X="25.785453598598" Y="-3.974542910453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.241308270776" Y="-3.920620167398" />
                  <Point X="23.552901660526" Y="-3.896580478845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.821888429869" Y="-3.871052934322" />
                  <Point X="22.034748330118" Y="-3.84356539634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.822368672966" Y="-4.070889919964" />
                  <Point X="25.811164735317" Y="-4.07049866984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.216073573879" Y="-4.014796859073" />
                  <Point X="23.66578937926" Y="-3.995580511557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.686691361002" Y="-3.961389655352" />
                  <Point X="22.164069690589" Y="-3.943139304468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.190838876982" Y="-4.108973550748" />
                  <Point X="23.768915815622" Y="-4.094239672779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.580991415501" Y="-4.052756438628" />
                  <Point X="22.303967420084" Y="-4.043082547541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.165604180085" Y="-4.203150242422" />
                  <Point X="23.815430837624" Y="-4.190921919848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.509954502623" Y="-4.145333681677" />
                  <Point X="22.466667585978" Y="-4.143822069239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.140369483188" Y="-4.297326934097" />
                  <Point X="23.834687438014" Y="-4.28665228186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.11513478629" Y="-4.391503625772" />
                  <Point X="23.853728308219" Y="-4.382375110408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.089900089393" Y="-4.485680317447" />
                  <Point X="23.872769297811" Y="-4.478097943125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.064665392496" Y="-4.579857009122" />
                  <Point X="23.879447365595" Y="-4.573389053099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.039430695599" Y="-4.674033700797" />
                  <Point X="23.867090674518" Y="-4.668015454646" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999837890625" Y="0.001370849609" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.977974121094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.503283203125" Y="-3.655575439453" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.451345703125" Y="-3.502851318359" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.254259765625" Y="-3.260168945312" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.971259765625" Y="-3.193997314453" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.698748046875" Y="-3.304336425781" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.17193359375" Y="-4.913638671875" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.877783203125" Y="-4.932412109375" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.687029296875" Y="-4.580063964844" />
                  <Point X="23.6908515625" Y="-4.551040527344" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.685521484375" Y="-4.510645507812" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.595234375" Y="-4.186418945312" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.326228515625" Y="-3.984155029297" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.9896796875" Y="-3.987449462891" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294921875" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="22.113740234375" Y="-4.14418359375" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.434978515625" Y="-2.731291503906" />
                  <Point X="22.493966796875" Y="-2.629119384766" />
                  <Point X="22.50023828125" Y="-2.597591064453" />
                  <Point X="22.4846484375" Y="-2.567390869141" />
                  <Point X="22.46867578125" Y="-2.551416992188" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.22108984375" Y="-3.228915771484" />
                  <Point X="21.157041015625" Y="-3.26589453125" />
                  <Point X="21.156197265625" Y="-3.264786376953" />
                  <Point X="20.83830078125" Y="-2.847135498047" />
                  <Point X="20.816484375" Y="-2.810552978516" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.733001953125" Y="-1.502341918945" />
                  <Point X="21.836212890625" Y="-1.42314465332" />
                  <Point X="21.8547890625" Y="-1.393656005859" />
                  <Point X="21.8618828125" Y="-1.36626574707" />
                  <Point X="21.85967578125" Y="-1.334599609375" />
                  <Point X="21.836744140625" Y="-1.309403808594" />
                  <Point X="21.812359375" Y="-1.295052368164" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.2817109375" Y="-1.48588684082" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.072607421875" Y="-1.011187805176" />
                  <Point X="20.066837890625" Y="-0.970842102051" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="21.324962890625" Y="-0.160149139404" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.460302734375" Y="-0.119899169922" />
                  <Point X="21.485857421875" Y="-0.102163482666" />
                  <Point X="21.5058359375" Y="-0.07354524231" />
                  <Point X="21.514353515625" Y="-0.046100269318" />
                  <Point X="21.51362109375" Y="-0.014097898483" />
                  <Point X="21.505103515625" Y="0.01334706974" />
                  <Point X="21.483658203125" Y="0.041129737854" />
                  <Point X="21.458103515625" Y="0.058865428925" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.076345703125" Y="0.432155395508" />
                  <Point X="20.001814453125" Y="0.452125946045" />
                  <Point X="20.002224609375" Y="0.454897979736" />
                  <Point X="20.08235546875" Y="0.996414550781" />
                  <Point X="20.093974609375" Y="1.039292724609" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="21.162455078125" Y="1.405075927734" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866699219" />
                  <Point X="21.2731640625" Y="1.397401367188" />
                  <Point X="21.32972265625" Y="1.41523425293" />
                  <Point X="21.348466796875" Y="1.426056518555" />
                  <Point X="21.360880859375" Y="1.443786621094" />
                  <Point X="21.362833984375" Y="1.448501831055" />
                  <Point X="21.38552734375" Y="1.503290893555" />
                  <Point X="21.389287109375" Y="1.524600708008" />
                  <Point X="21.383689453125" Y="1.545502929688" />
                  <Point X="21.381328125" Y="1.550039672852" />
                  <Point X="21.353943359375" Y="1.602642456055" />
                  <Point X="21.34003125" Y="1.619221923828" />
                  <Point X="20.556380859375" Y="2.220538574219" />
                  <Point X="20.52389453125" Y="2.245465820312" />
                  <Point X="20.528625" Y="2.253569335938" />
                  <Point X="20.83998828125" Y="2.787007568359" />
                  <Point X="20.870759765625" Y="2.826562255859" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.78908203125" Y="2.956828613281" />
                  <Point X="21.84084375" Y="2.926943603516" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.868265625" Y="2.919841796875" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404541016" />
                  <Point X="21.991560546875" Y="2.932216308594" />
                  <Point X="22.04747265625" Y="2.988127929688" />
                  <Point X="22.0591015625" Y="3.006384521484" />
                  <Point X="22.06192578125" Y="3.027845947266" />
                  <Point X="22.061333984375" Y="3.034619628906" />
                  <Point X="22.054443359375" Y="3.113390136719" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.700673828125" Y="3.735504150391" />
                  <Point X="21.704830078125" Y="3.758560302734" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.29559375" Y="4.201259277344" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="23.016328125" Y="4.308224609375" />
                  <Point X="23.032173828125" Y="4.287573730469" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.056298828125" Y="4.269732910156" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.19405078125" Y="4.225505859375" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275747070312" />
                  <Point X="23.313919921875" Y="4.294494628906" />
                  <Point X="23.316474609375" Y="4.302600585938" />
                  <Point X="23.346197265625" Y="4.396864746094" />
                  <Point X="23.348083984375" Y="4.418424804688" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.329875" Y="4.706471191406" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.090662109375" Y="4.910172851562" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.942546875" Y="4.368051269531" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.28417578125" />
                  <Point X="25.054453125" Y="4.310901855469" />
                  <Point X="25.232384765625" Y="4.974952636719" />
                  <Point X="25.24723046875" Y="4.989760742188" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="25.908818359375" Y="4.913829101562" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.539705078125" Y="4.757724121094" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="26.9616328125" Y="4.6014765625" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.368265625" Y="4.407909179688" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="27.760404296875" Y="4.175865722656" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.298130859375" Y="2.621854492188" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.22315234375" Y="2.485153564453" />
                  <Point X="27.2033828125" Y="2.411229003906" />
                  <Point X="27.202708984375" Y="2.386818359375" />
                  <Point X="27.210416015625" Y="2.322901855469" />
                  <Point X="27.21868359375" Y="2.300812011719" />
                  <Point X="27.222087890625" Y="2.295795410156" />
                  <Point X="27.261642578125" Y="2.237503417969" />
                  <Point X="27.27995703125" Y="2.220799560547" />
                  <Point X="27.33825" Y="2.18124609375" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.365837890625" Y="2.172316650391" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.45502734375" Y="2.167647705078" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.915162109375" Y="2.985769775391" />
                  <Point X="28.99425" Y="3.031430908203" />
                  <Point X="29.20259765625" Y="2.741874755859" />
                  <Point X="29.218134765625" Y="2.716196777344" />
                  <Point X="29.387513671875" Y="2.436295654297" />
                  <Point X="28.3785078125" Y="1.66205859375" />
                  <Point X="28.2886171875" Y="1.593082641602" />
                  <Point X="28.27479296875" Y="1.577859741211" />
                  <Point X="28.22158984375" Y="1.508451782227" />
                  <Point X="28.2114140625" Y="1.485401245117" />
                  <Point X="28.191595703125" Y="1.414535400391" />
                  <Point X="28.190779296875" Y="1.390965087891" />
                  <Point X="28.1921796875" Y="1.384179321289" />
                  <Point X="28.20844921875" Y="1.30533215332" />
                  <Point X="28.219455078125" Y="1.282167358398" />
                  <Point X="28.263703125" Y="1.21491003418" />
                  <Point X="28.280947265625" Y="1.198819946289" />
                  <Point X="28.286466796875" Y="1.195713500977" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.37602734375" Y="1.152633422852" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.7811328125" Y="1.313021484375" />
                  <Point X="29.848974609375" Y="1.32195300293" />
                  <Point X="29.939193359375" Y="0.951366516113" />
                  <Point X="29.944087890625" Y="0.919927734375" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="28.844185546875" Y="0.265430328369" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.7217578125" Y="0.22858215332" />
                  <Point X="28.636578125" Y="0.179346984863" />
                  <Point X="28.6178671875" Y="0.161322113037" />
                  <Point X="28.566759765625" Y="0.096198867798" />
                  <Point X="28.556986328125" Y="0.074734802246" />
                  <Point X="28.55551953125" Y="0.067079246521" />
                  <Point X="28.538484375" Y="-0.02187575531" />
                  <Point X="28.539951171875" Y="-0.048339725494" />
                  <Point X="28.556986328125" Y="-0.137294876099" />
                  <Point X="28.566759765625" Y="-0.15875894165" />
                  <Point X="28.571158203125" Y="-0.164363357544" />
                  <Point X="28.622265625" Y="-0.229486618042" />
                  <Point X="28.643908203125" Y="-0.246144104004" />
                  <Point X="28.729087890625" Y="-0.295379425049" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.938212890625" Y="-0.621134277344" />
                  <Point X="29.9980703125" Y="-0.637172668457" />
                  <Point X="29.948431640625" Y="-0.966412963867" />
                  <Point X="29.942158203125" Y="-0.993897155762" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.531453125" Y="-1.113356811523" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.380451171875" Y="-1.101468505859" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.197947265625" Y="-1.143923095703" />
                  <Point X="28.176751953125" Y="-1.165155883789" />
                  <Point X="28.075703125" Y="-1.286685180664" />
                  <Point X="28.067955078125" Y="-1.299517944336" />
                  <Point X="28.06311328125" Y="-1.327615234375" />
                  <Point X="28.048630859375" Y="-1.485000976562" />
                  <Point X="28.0498515625" Y="-1.501458374023" />
                  <Point X="28.064322265625" Y="-1.529006713867" />
                  <Point X="28.156841796875" Y="-1.672912719727" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="29.279328125" Y="-2.5379375" />
                  <Point X="29.339076171875" Y="-2.583784179688" />
                  <Point X="29.2041328125" Y="-2.802140380859" />
                  <Point X="29.191154296875" Y="-2.820581298828" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="27.859861328125" Y="-2.320648681641" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.72021875" Y="-2.250220458984" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.50475" Y="-2.214074462891" />
                  <Point X="27.474853515625" Y="-2.226731445312" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2974921875" Y="-2.322614746094" />
                  <Point X="27.281115234375" Y="-2.348908447266" />
                  <Point X="27.19412109375" Y="-2.514201416016" />
                  <Point X="27.188951171875" Y="-2.529873291016" />
                  <Point X="27.1922578125" Y="-2.563497558594" />
                  <Point X="27.22819140625" Y="-2.76246484375" />
                  <Point X="27.23409375" Y="-2.778578857422" />
                  <Point X="27.947935546875" Y="-4.014990234375" />
                  <Point X="27.986673828125" Y="-4.082087646484" />
                  <Point X="27.83529296875" Y="-4.190216308594" />
                  <Point X="27.820791015625" Y="-4.199603027344" />
                  <Point X="27.679775390625" Y="-4.29087890625" />
                  <Point X="26.764375" Y="-3.097905273438" />
                  <Point X="26.6831796875" Y="-2.992087646484" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.653662109375" Y="-2.969609863281" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.44226171875" Y="-2.836937255859" />
                  <Point X="26.4073359375" Y="-2.837416748047" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.178166015625" Y="-2.86076171875" />
                  <Point X="26.151072265625" Y="-2.880367431641" />
                  <Point X="25.985349609375" Y="-3.018159667969" />
                  <Point X="25.96845703125" Y="-3.045986083984" />
                  <Point X="25.964193359375" Y="-3.065604980469" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310719726562" />
                  <Point X="26.116228515625" Y="-4.847321777344" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.980939453125" Y="-4.965682617188" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#124" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.015312729718" Y="4.412272361799" Z="0.2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.2" />
                  <Point X="-0.921307345037" Y="4.991113917252" Z="0.2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.2" />
                  <Point X="-1.689780723436" Y="4.785894627758" Z="0.2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.2" />
                  <Point X="-1.747125384685" Y="4.743057374226" Z="0.2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.2" />
                  <Point X="-1.737367332973" Y="4.348916427052" Z="0.2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.2" />
                  <Point X="-1.831240493722" Y="4.302979726633" Z="0.2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.2" />
                  <Point X="-1.926770261387" Y="4.345363647817" Z="0.2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.2" />
                  <Point X="-1.950161218653" Y="4.369942261713" Z="0.2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.2" />
                  <Point X="-2.734847272975" Y="4.276246700695" Z="0.2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.2" />
                  <Point X="-3.33174812985" Y="3.829520355301" Z="0.2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.2" />
                  <Point X="-3.348784265961" Y="3.741784109804" Z="0.2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.2" />
                  <Point X="-2.994633099151" Y="3.061542499955" Z="0.2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.2" />
                  <Point X="-3.049951812332" Y="2.998851755555" Z="0.2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.2" />
                  <Point X="-3.133533965652" Y="3.000931600022" Z="0.2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.2" />
                  <Point X="-3.192075238493" Y="3.031409682785" Z="0.2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.2" />
                  <Point X="-4.174859807845" Y="2.888544631605" Z="0.2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.2" />
                  <Point X="-4.520714186189" Y="2.310054416899" Z="0.2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.2" />
                  <Point X="-4.480213497757" Y="2.212150774946" Z="0.2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.2" />
                  <Point X="-3.669179024437" Y="1.558231350708" Z="0.2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.2" />
                  <Point X="-3.689516837987" Y="1.498915238275" Z="0.2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.2" />
                  <Point X="-3.7480286516" Y="1.476367224557" Z="0.2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.2" />
                  <Point X="-3.83717588936" Y="1.485928186794" Z="0.2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.2" />
                  <Point X="-4.960442771941" Y="1.083650059998" Z="0.2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.2" />
                  <Point X="-5.053393833907" Y="0.493497058308" Z="0.2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.2" />
                  <Point X="-4.942753174663" Y="0.415139198428" Z="0.2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.2" />
                  <Point X="-3.551007001741" Y="0.031333281416" Z="0.2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.2" />
                  <Point X="-3.540289801749" Y="0.002361907531" Z="0.2" />
                  <Point X="-3.539556741714" Y="0" Z="0.2" />
                  <Point X="-3.548074799386" Y="-0.02744504353" Z="0.2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.2" />
                  <Point X="-3.574361593383" Y="-0.047542701717" Z="0.2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.2" />
                  <Point X="-3.694134614109" Y="-0.080572858328" Z="0.2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.2" />
                  <Point X="-4.988815937082" Y="-0.946641204972" Z="0.2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.2" />
                  <Point X="-4.85766084128" Y="-1.479044608744" Z="0.2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.2" />
                  <Point X="-4.717920596735" Y="-1.504178992364" Z="0.2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.2" />
                  <Point X="-3.194774789778" Y="-1.321214726984" Z="0.2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.2" />
                  <Point X="-3.199770296639" Y="-1.349839984184" Z="0.2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.2" />
                  <Point X="-3.303592677632" Y="-1.431394424448" Z="0.2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.2" />
                  <Point X="-4.232615487134" Y="-2.804882598903" Z="0.2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.2" />
                  <Point X="-3.889744410372" Y="-3.263789443654" Z="0.2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.2" />
                  <Point X="-3.760066672089" Y="-3.240936889342" Z="0.2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.2" />
                  <Point X="-2.556865670716" Y="-2.571464798416" Z="0.2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.2" />
                  <Point X="-2.614480123827" Y="-2.675011710403" Z="0.2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.2" />
                  <Point X="-2.922920436592" Y="-4.152520643663" Z="0.2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.2" />
                  <Point X="-2.485932532402" Y="-4.427985122472" Z="0.2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.2" />
                  <Point X="-2.433297000679" Y="-4.426317119006" Z="0.2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.2" />
                  <Point X="-1.98869728566" Y="-3.99774282046" Z="0.2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.2" />
                  <Point X="-1.68319874231" Y="-4.002768027624" Z="0.2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.2" />
                  <Point X="-1.443889607504" Y="-4.192730776888" Z="0.2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.2" />
                  <Point X="-1.369674739528" Y="-4.489120106559" Z="0.2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.2" />
                  <Point X="-1.36869953655" Y="-4.542255604798" Z="0.2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.2" />
                  <Point X="-1.140832932315" Y="-4.94955495558" Z="0.2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.2" />
                  <Point X="-0.841419030868" Y="-5.009152807713" Z="0.2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.2" />
                  <Point X="-0.78592595454" Y="-4.895299716242" Z="0.2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.2" />
                  <Point X="-0.266332888343" Y="-3.301565462138" Z="0.2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.2" />
                  <Point X="-0.020076213648" Y="-3.210470284273" Z="0.2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.2" />
                  <Point X="0.233282865714" Y="-3.276641761015" Z="0.2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.2" />
                  <Point X="0.404112971089" Y="-3.50008029363" Z="0.2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.2" />
                  <Point X="0.448828905861" Y="-3.637236313573" Z="0.2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.2" />
                  <Point X="0.983719823648" Y="-4.983597409411" Z="0.2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.2" />
                  <Point X="1.162865593541" Y="-4.944865160589" Z="0.2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.2" />
                  <Point X="1.159643342027" Y="-4.809516021599" Z="0.2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.2" />
                  <Point X="1.00689576775" Y="-3.044944900676" Z="0.2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.2" />
                  <Point X="1.17688181142" Y="-2.887533775041" Z="0.2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.2" />
                  <Point X="1.405760628849" Y="-2.85592619953" Z="0.2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.2" />
                  <Point X="1.620465961836" Y="-2.980387887787" Z="0.2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.2" />
                  <Point X="1.718550715073" Y="-3.097063001567" Z="0.2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.2" />
                  <Point X="2.8418037675" Y="-4.210297314857" Z="0.2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.2" />
                  <Point X="3.031517273384" Y="-4.075825790299" Z="0.2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.2" />
                  <Point X="2.98507963371" Y="-3.958709994121" Z="0.2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.2" />
                  <Point X="2.235303522256" Y="-2.523331222192" Z="0.2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.2" />
                  <Point X="2.319203659383" Y="-2.340915230557" Z="0.2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.2" />
                  <Point X="2.491983147815" Y="-2.239697650584" Z="0.2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.2" />
                  <Point X="2.705175581321" Y="-2.268144487235" Z="0.2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.2" />
                  <Point X="2.828703602986" Y="-2.332669866564" Z="0.2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.2" />
                  <Point X="4.225886343566" Y="-2.818078633924" Z="0.2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.2" />
                  <Point X="4.38657082646" Y="-2.560796594711" Z="0.2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.2" />
                  <Point X="4.303608010521" Y="-2.466989980366" Z="0.2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.2" />
                  <Point X="3.100224582831" Y="-1.47068698435" Z="0.2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.2" />
                  <Point X="3.106745092452" Y="-1.300916772912" Z="0.2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.2" />
                  <Point X="3.209039391271" Y="-1.165842936839" Z="0.2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.2" />
                  <Point X="3.3849126736" Y="-1.119047538944" Z="0.2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.2" />
                  <Point X="3.518770792434" Y="-1.131649069918" Z="0.2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.2" />
                  <Point X="4.984748011621" Y="-0.973740927365" Z="0.2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.2" />
                  <Point X="5.043531968677" Y="-0.598897075548" Z="0.2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.2" />
                  <Point X="4.944997965373" Y="-0.5415579344" Z="0.2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.2" />
                  <Point X="3.662773220578" Y="-0.171575371163" Z="0.2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.2" />
                  <Point X="3.604335027241" Y="-0.102215003491" Z="0.2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.2" />
                  <Point X="3.582900827892" Y="-0.007655433858" Z="0.2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.2" />
                  <Point X="3.598470622532" Y="0.088955097386" Z="0.2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.2" />
                  <Point X="3.651044411159" Y="0.161733735084" Z="0.2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.2" />
                  <Point X="3.740622193774" Y="0.216573473079" Z="0.2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.2" />
                  <Point X="3.850969851934" Y="0.248413998369" Z="0.2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.2" />
                  <Point X="4.98733498793" Y="0.958899863585" Z="0.2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.2" />
                  <Point X="4.888814666021" Y="1.375681610978" Z="0.2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.2" />
                  <Point X="4.768449540904" Y="1.393873844088" Z="0.2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.2" />
                  <Point X="3.376421935325" Y="1.233482475443" Z="0.2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.2" />
                  <Point X="3.304837000452" Y="1.270564622505" Z="0.2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.2" />
                  <Point X="3.255069084239" Y="1.340928277018" Z="0.2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.2" />
                  <Point X="3.234992124776" Y="1.425563648275" Z="0.2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.2" />
                  <Point X="3.25341042446" Y="1.503215023585" Z="0.2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.2" />
                  <Point X="3.308319490458" Y="1.578721866831" Z="0.2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.2" />
                  <Point X="3.402789282124" Y="1.653670983142" Z="0.2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.2" />
                  <Point X="4.254755303433" Y="2.773363100219" Z="0.2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.2" />
                  <Point X="4.020955722971" Y="3.102645385934" Z="0.2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.2" />
                  <Point X="3.88400447695" Y="3.060351060067" Z="0.2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.2" />
                  <Point X="2.435953552062" Y="2.247230428764" Z="0.2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.2" />
                  <Point X="2.365668025627" Y="2.253237180602" Z="0.2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.2" />
                  <Point X="2.301874664967" Y="2.293454109653" Z="0.2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.2" />
                  <Point X="2.257304490582" Y="2.355150195415" Z="0.2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.2" />
                  <Point X="2.246192522156" Y="2.424090418205" Z="0.2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.2" />
                  <Point X="2.265297513893" Y="2.50351602382" Z="0.2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.2" />
                  <Point X="2.335274292792" Y="2.628134605005" Z="0.2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.2" />
                  <Point X="2.783223100329" Y="4.247893037832" Z="0.2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.2" />
                  <Point X="2.387279314033" Y="4.482391762112" Z="0.2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.2" />
                  <Point X="1.976656906481" Y="4.678048512368" Z="0.2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.2" />
                  <Point X="1.550596397575" Y="4.836008169723" Z="0.2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.2" />
                  <Point X="0.914395904101" Y="4.993712862095" Z="0.2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.2" />
                  <Point X="0.246281117138" Y="5.070768688525" Z="0.2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.2" />
                  <Point X="0.177931831835" Y="5.019175144152" Z="0.2" />
                  <Point X="0" Y="4.355124473572" Z="0.2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>