<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#137" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="940" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004715576172" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.642359375" Y="-3.807561279297" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.54236328125" Y="-3.467376464844" />
                  <Point X="25.51005078125" Y="-3.420821289063" />
                  <Point X="25.378634765625" Y="-3.231476074219" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.25249609375" Y="-3.160151123047" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097036132812" />
                  <Point X="24.91317578125" Y="-3.112554443359" />
                  <Point X="24.70981640625" Y="-3.175669433594" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231478027344" />
                  <Point X="24.601365234375" Y="-3.278032958984" />
                  <Point X="24.46994921875" Y="-3.467378417969" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.127482421875" Y="-4.712477539062" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.9206640625" Y="-4.845351074219" />
                  <Point X="23.866529296875" Y="-4.831422851562" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.775521484375" Y="-4.63572265625" />
                  <Point X="23.7850390625" Y="-4.563438964844" />
                  <Point X="23.78580078125" Y="-4.547930175781" />
                  <Point X="23.7834921875" Y="-4.516223632812" />
                  <Point X="23.771546875" Y="-4.456171386719" />
                  <Point X="23.722962890625" Y="-4.211932128906" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131205078125" />
                  <Point X="23.630322265625" Y="-4.090833740234" />
                  <Point X="23.443095703125" Y="-3.926639892578" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.295875" Y="-3.886961669922" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801513672" />
                  <Point X="22.906431640625" Y="-3.928818359375" />
                  <Point X="22.699376953125" Y="-4.067169433594" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.5198515625" Y="-4.288489746094" />
                  <Point X="22.1982890625" Y="-4.089386230469" />
                  <Point X="22.123337890625" Y="-4.031675537109" />
                  <Point X="21.89527734375" Y="-3.856077392578" />
                  <Point X="22.42932421875" Y="-2.931083007812" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647655273438" />
                  <Point X="22.593412109375" Y="-2.616129882812" />
                  <Point X="22.59442578125" Y="-2.585197753906" />
                  <Point X="22.585443359375" Y="-2.555581298828" />
                  <Point X="22.571228515625" Y="-2.526752685547" />
                  <Point X="22.553201171875" Y="-2.501592285156" />
                  <Point X="22.535853515625" Y="-2.484243652344" />
                  <Point X="22.5106953125" Y="-2.46621484375" />
                  <Point X="22.481865234375" Y="-2.451996826172" />
                  <Point X="22.452248046875" Y="-2.443011962891" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.325880859375" Y="-3.058717773438" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="20.917142578125" Y="-2.793860839844" />
                  <Point X="20.86340625" Y="-2.703756347656" />
                  <Point X="20.693857421875" Y="-2.419449951172" />
                  <Point X="21.6369921875" Y="-1.695756958008" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.91693359375" Y="-1.475880493164" />
                  <Point X="21.9358671875" Y="-1.445860961914" />
                  <Point X="21.94410546875" Y="-1.424918212891" />
                  <Point X="21.947666015625" Y="-1.413959350586" />
                  <Point X="21.953849609375" Y="-1.390082641602" />
                  <Point X="21.957962890625" Y="-1.358597900391" />
                  <Point X="21.95726171875" Y="-1.335732788086" />
                  <Point X="21.9511171875" Y="-1.313697631836" />
                  <Point X="21.93911328125" Y="-1.284715698242" />
                  <Point X="21.926611328125" Y="-1.26310534668" />
                  <Point X="21.908912109375" Y="-1.245498046875" />
                  <Point X="21.89118359375" Y="-1.231992797852" />
                  <Point X="21.881802734375" Y="-1.22569152832" />
                  <Point X="21.860546875" Y="-1.213180908203" />
                  <Point X="21.831283203125" Y="-1.201956787109" />
                  <Point X="21.799396484375" Y="-1.195474975586" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="20.461564453125" Y="-1.366388916016" />
                  <Point X="20.2678984375" Y="-1.391885498047" />
                  <Point X="20.165921875" Y="-0.992646728516" />
                  <Point X="20.15170703125" Y="-0.893254882812" />
                  <Point X="20.107578125" Y="-0.584698364258" />
                  <Point X="21.174302734375" Y="-0.298869781494" />
                  <Point X="21.467125" Y="-0.220408279419" />
                  <Point X="21.48661328125" Y="-0.212802215576" />
                  <Point X="21.50765625" Y="-0.201781326294" />
                  <Point X="21.517744140625" Y="-0.195670394897" />
                  <Point X="21.540021484375" Y="-0.180209625244" />
                  <Point X="21.563986328125" Y="-0.158673614502" />
                  <Point X="21.58436328125" Y="-0.129513626099" />
                  <Point X="21.59361328125" Y="-0.108886680603" />
                  <Point X="21.59766015625" Y="-0.098174682617" />
                  <Point X="21.60508203125" Y="-0.074261932373" />
                  <Point X="21.610525390625" Y="-0.045538040161" />
                  <Point X="21.609814453125" Y="-0.01295730114" />
                  <Point X="21.6057421875" Y="0.007752975464" />
                  <Point X="21.6032578125" Y="0.017585195541" />
                  <Point X="21.59583203125" Y="0.041509784698" />
                  <Point X="21.582517578125" Y="0.070831130981" />
                  <Point X="21.560658203125" Y="0.099208946228" />
                  <Point X="21.54336328125" Y="0.114524894714" />
                  <Point X="21.534548828125" Y="0.121447898865" />
                  <Point X="21.5122734375" Y="0.136908508301" />
                  <Point X="21.498076171875" Y="0.145046447754" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="20.276181640625" Y="0.47696081543" />
                  <Point X="20.10818359375" Y="0.521975524902" />
                  <Point X="20.17551171875" Y="0.976967102051" />
                  <Point X="20.2041328125" Y="1.08258605957" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="21.02443359375" Y="1.327427124023" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228149414" />
                  <Point X="21.2968671875" Y="1.305264282227" />
                  <Point X="21.30898828125" Y="1.309086425781" />
                  <Point X="21.35829296875" Y="1.324631835938" />
                  <Point X="21.37722265625" Y="1.332961914062" />
                  <Point X="21.395966796875" Y="1.343783935547" />
                  <Point X="21.4126484375" Y="1.356015625" />
                  <Point X="21.426287109375" Y="1.371567993164" />
                  <Point X="21.438701171875" Y="1.389297607422" />
                  <Point X="21.44865234375" Y="1.407436401367" />
                  <Point X="21.453515625" Y="1.41917956543" />
                  <Point X="21.473298828125" Y="1.466940917969" />
                  <Point X="21.479083984375" Y="1.486788085938" />
                  <Point X="21.48284375" Y="1.508103271484" />
                  <Point X="21.484197265625" Y="1.52874987793" />
                  <Point X="21.481048828125" Y="1.549199829102" />
                  <Point X="21.4754453125" Y="1.570106445312" />
                  <Point X="21.46794921875" Y="1.589378173828" />
                  <Point X="21.462080078125" Y="1.600652709961" />
                  <Point X="21.438208984375" Y="1.64650793457" />
                  <Point X="21.42671875" Y="1.663706054688" />
                  <Point X="21.412806640625" Y="1.680286132812" />
                  <Point X="21.39786328125" Y="1.694590454102" />
                  <Point X="20.714736328125" Y="2.218773193359" />
                  <Point X="20.648140625" Y="2.269873779297" />
                  <Point X="20.650826171875" Y="2.274474609375" />
                  <Point X="20.918853515625" Y="2.733665771484" />
                  <Point X="20.994654296875" Y="2.831098876953" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.664427734375" Y="2.919100585938" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.87008984375" Y="2.824319335938" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.019537109375" Y="2.835653076172" />
                  <Point X="22.037791015625" Y="2.847281982422" />
                  <Point X="22.053921875" Y="2.860227783203" />
                  <Point X="22.06590625" Y="2.872211669922" />
                  <Point X="22.114646484375" Y="2.920951171875" />
                  <Point X="22.127595703125" Y="2.9370859375" />
                  <Point X="22.139224609375" Y="2.955340576172" />
                  <Point X="22.14837109375" Y="2.973889160156" />
                  <Point X="22.1532890625" Y="2.993977783203" />
                  <Point X="22.156115234375" Y="3.015436767578" />
                  <Point X="22.15656640625" Y="3.036118164062" />
                  <Point X="22.15508984375" Y="3.053001464844" />
                  <Point X="22.14908203125" Y="3.121667724609" />
                  <Point X="22.145044921875" Y="3.141963378906" />
                  <Point X="22.13853515625" Y="3.16260546875" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.827490234375" Y="3.705850585938" />
                  <Point X="21.81666796875" Y="3.724596435547" />
                  <Point X="21.8325703125" Y="3.736789306641" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.418765625" Y="4.161015136719" />
                  <Point X="22.83296484375" Y="4.391134277344" />
                  <Point X="22.91733984375" Y="4.281173339844" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.004888671875" Y="4.189394042969" />
                  <Point X="23.0236796875" Y="4.179612304688" />
                  <Point X="23.10010546875" Y="4.139827148438" />
                  <Point X="23.1193828125" Y="4.132331054688" />
                  <Point X="23.1402890625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.12358203125" />
                  <Point X="23.18137109375" Y="4.124934570312" />
                  <Point X="23.2026875" Y="4.128692871094" />
                  <Point X="23.222552734375" Y="4.134483398438" />
                  <Point X="23.242123046875" Y="4.142590820312" />
                  <Point X="23.3217265625" Y="4.175562988281" />
                  <Point X="23.339859375" Y="4.185512207031" />
                  <Point X="23.357587890625" Y="4.197926757812" />
                  <Point X="23.37313671875" Y="4.211563476563" />
                  <Point X="23.3853671875" Y="4.2282421875" />
                  <Point X="23.396189453125" Y="4.246985839844" />
                  <Point X="23.4045234375" Y="4.26592578125" />
                  <Point X="23.410892578125" Y="4.286129882813" />
                  <Point X="23.436802734375" Y="4.368302246094" />
                  <Point X="23.4408359375" Y="4.388583007812" />
                  <Point X="23.44272265625" Y="4.410145019531" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.415798828125" Y="4.6318984375" />
                  <Point X="23.44605859375" Y="4.640382324219" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.19509765625" Y="4.826747070312" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.827958984375" Y="4.42864453125" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183885742188" />
                  <Point X="25.067130859375" Y="4.194217773438" />
                  <Point X="25.094423828125" Y="4.208806640625" />
                  <Point X="25.11558203125" Y="4.23139453125" />
                  <Point X="25.13344140625" Y="4.25812109375" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.30132421875" Y="4.865185058594" />
                  <Point X="25.307421875" Y="4.887937011719" />
                  <Point X="25.316388671875" Y="4.886998046875" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="25.9637890625" Y="4.802828613281" />
                  <Point X="26.4810234375" Y="4.677952148438" />
                  <Point X="26.557779296875" Y="4.650112304688" />
                  <Point X="26.894650390625" Y="4.527926269531" />
                  <Point X="26.970017578125" Y="4.4926796875" />
                  <Point X="27.294572265625" Y="4.340895996094" />
                  <Point X="27.367419921875" Y="4.298455078125" />
                  <Point X="27.680984375" Y="4.115770996094" />
                  <Point X="27.74965625" Y="4.066937255859" />
                  <Point X="27.943263671875" Y="3.929254638672" />
                  <Point X="27.317626953125" Y="2.845620361328" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.1396640625" Y="2.533438964844" />
                  <Point X="27.130423828125" Y="2.505511230469" />
                  <Point X="27.12883984375" Y="2.500211914062" />
                  <Point X="27.111607421875" Y="2.435770019531" />
                  <Point X="27.10838671875" Y="2.410312011719" />
                  <Point X="27.108701171875" Y="2.377708984375" />
                  <Point X="27.109380859375" Y="2.367253173828" />
                  <Point X="27.116099609375" Y="2.311529296875" />
                  <Point X="27.12144140625" Y="2.289607421875" />
                  <Point X="27.12970703125" Y="2.267518310547" />
                  <Point X="27.140072265625" Y="2.247467285156" />
                  <Point X="27.14855078125" Y="2.234973388672" />
                  <Point X="27.18303125" Y="2.184158691406" />
                  <Point X="27.20035546875" Y="2.1649140625" />
                  <Point X="27.226150390625" Y="2.143136474609" />
                  <Point X="27.23409375" Y="2.137114746094" />
                  <Point X="27.284908203125" Y="2.102635009766" />
                  <Point X="27.304953125" Y="2.092272705078" />
                  <Point X="27.327041015625" Y="2.084006591797" />
                  <Point X="27.34896484375" Y="2.078663330078" />
                  <Point X="27.362666015625" Y="2.077011230469" />
                  <Point X="27.418388671875" Y="2.070291748047" />
                  <Point X="27.444810546875" Y="2.070808105469" />
                  <Point X="27.47955859375" Y="2.0763828125" />
                  <Point X="27.489052734375" Y="2.078408203125" />
                  <Point X="27.553494140625" Y="2.095640869141" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.786396484375" Y="2.801730224609" />
                  <Point X="28.967328125" Y="2.906191162109" />
                  <Point X="29.123275390625" Y="2.689457763672" />
                  <Point X="29.16155859375" Y="2.6261953125" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.454662109375" Y="1.84023828125" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.21644140625" Y="1.654854492188" />
                  <Point X="28.19579296875" Y="1.630728393555" />
                  <Point X="28.1925703125" Y="1.626751342773" />
                  <Point X="28.14619140625" Y="1.566246459961" />
                  <Point X="28.13325390625" Y="1.543403930664" />
                  <Point X="28.120537109375" Y="1.511263916016" />
                  <Point X="28.117384765625" Y="1.501899536133" />
                  <Point X="28.100107421875" Y="1.440124023438" />
                  <Point X="28.09665234375" Y="1.417824829102" />
                  <Point X="28.0958359375" Y="1.394254272461" />
                  <Point X="28.097740234375" Y="1.371761474609" />
                  <Point X="28.101228515625" Y="1.354861694336" />
                  <Point X="28.11541015625" Y="1.286128540039" />
                  <Point X="28.124326171875" Y="1.261193237305" />
                  <Point X="28.1410078125" Y="1.229400634766" />
                  <Point X="28.145765625" Y="1.221326782227" />
                  <Point X="28.184337890625" Y="1.162696655273" />
                  <Point X="28.198892578125" Y="1.145451049805" />
                  <Point X="28.21613671875" Y="1.129360717773" />
                  <Point X="28.23434765625" Y="1.116034301758" />
                  <Point X="28.248091796875" Y="1.108297729492" />
                  <Point X="28.303990234375" Y="1.07683203125" />
                  <Point X="28.3291875" Y="1.067059692383" />
                  <Point X="28.365748046875" Y="1.058605834961" />
                  <Point X="28.374703125" Y="1.056982788086" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.626091796875" Y="1.196790283203" />
                  <Point X="29.77683984375" Y="1.21663659668" />
                  <Point X="29.84594140625" Y="0.932790527344" />
                  <Point X="29.858001953125" Y="0.855320251465" />
                  <Point X="29.890865234375" Y="0.644238708496" />
                  <Point X="28.9731484375" Y="0.398336883545" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.704791015625" Y="0.32558605957" />
                  <Point X="28.681544921875" Y="0.315066680908" />
                  <Point X="28.6632890625" Y="0.304513763428" />
                  <Point X="28.58903515625" Y="0.261594268799" />
                  <Point X="28.56797265625" Y="0.245061416626" />
                  <Point X="28.542705078125" Y="0.218682525635" />
                  <Point X="28.536576171875" Y="0.211617294312" />
                  <Point X="28.492025390625" Y="0.154847854614" />
                  <Point X="28.48030078125" Y="0.135566680908" />
                  <Point X="28.47052734375" Y="0.114102615356" />
                  <Point X="28.46368359375" Y="0.092607887268" />
                  <Point X="28.46003125" Y="0.073541801453" />
                  <Point X="28.445181640625" Y="-0.004002580643" />
                  <Point X="28.443921875" Y="-0.030989843369" />
                  <Point X="28.447572265625" Y="-0.068865180969" />
                  <Point X="28.448830078125" Y="-0.077621208191" />
                  <Point X="28.463681640625" Y="-0.155165740967" />
                  <Point X="28.47052734375" Y="-0.176664718628" />
                  <Point X="28.48030078125" Y="-0.198128326416" />
                  <Point X="28.4920234375" Y="-0.217406005859" />
                  <Point X="28.5029765625" Y="-0.231364196777" />
                  <Point X="28.547529296875" Y="-0.288133636475" />
                  <Point X="28.560001953125" Y="-0.301239074707" />
                  <Point X="28.5890390625" Y="-0.324156219482" />
                  <Point X="28.607296875" Y="-0.334709136963" />
                  <Point X="28.681548828125" Y="-0.37762878418" />
                  <Point X="28.692708984375" Y="-0.383137695312" />
                  <Point X="28.716580078125" Y="-0.392149810791" />
                  <Point X="29.76007421875" Y="-0.671753417969" />
                  <Point X="29.891474609375" Y="-0.706961853027" />
                  <Point X="29.855025390625" Y="-0.948726135254" />
                  <Point X="29.839568359375" Y="-1.016461120605" />
                  <Point X="29.801177734375" Y="-1.18469921875" />
                  <Point X="28.7219296875" Y="-1.042613647461" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.338828125" Y="-1.013296875" />
                  <Point X="28.193095703125" Y="-1.044972412109" />
                  <Point X="28.163974609375" Y="-1.056597412109" />
                  <Point X="28.1361484375" Y="-1.073489868164" />
                  <Point X="28.112400390625" Y="-1.093959960937" />
                  <Point X="28.0907421875" Y="-1.12000793457" />
                  <Point X="28.00265625" Y="-1.225947998047" />
                  <Point X="27.987935546875" Y="-1.250329223633" />
                  <Point X="27.976591796875" Y="-1.277714355469" />
                  <Point X="27.969759765625" Y="-1.3053671875" />
                  <Point X="27.96665625" Y="-1.339100341797" />
                  <Point X="27.95403125" Y="-1.476297363281" />
                  <Point X="27.95634765625" Y="-1.507561401367" />
                  <Point X="27.964078125" Y="-1.539182373047" />
                  <Point X="27.976451171875" Y="-1.567996948242" />
                  <Point X="27.99628125" Y="-1.598841064453" />
                  <Point X="28.076931640625" Y="-1.724287475586" />
                  <Point X="28.0869375" Y="-1.737241943359" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="29.079" Y="-2.503965332031" />
                  <Point X="29.213123046875" Y="-2.606882324219" />
                  <Point X="29.1248125" Y="-2.749784179688" />
                  <Point X="29.09284375" Y="-2.795206298828" />
                  <Point X="29.028982421875" Y="-2.885945068359" />
                  <Point X="28.0659609375" Y="-2.329944091797" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.71158203125" Y="-2.152123535156" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.40940625" Y="-2.153822509766" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246549804688" />
                  <Point X="27.22142578125" Y="-2.267509521484" />
                  <Point X="27.20453515625" Y="-2.290437255859" />
                  <Point X="27.185888671875" Y="-2.325865234375" />
                  <Point X="27.1100546875" Y="-2.469955322266" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531908203125" />
                  <Point X="27.095677734375" Y="-2.563261230469" />
                  <Point X="27.103380859375" Y="-2.605906738281" />
                  <Point X="27.134705078125" Y="-2.7793515625" />
                  <Point X="27.138986328125" Y="-2.795139160156" />
                  <Point X="27.151822265625" Y="-2.826078857422" />
                  <Point X="27.774095703125" Y="-3.903889892578" />
                  <Point X="27.86128515625" Y="-4.054904785156" />
                  <Point X="27.781841796875" Y="-4.111648925781" />
                  <Point X="27.746111328125" Y="-4.13477734375" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="26.960771484375" Y="-3.197797851562" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900556884766" />
                  <Point X="26.679865234375" Y="-2.873516113281" />
                  <Point X="26.50880078125" Y="-2.763538085938" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.371099609375" Y="-2.745349609375" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.069078125" Y="-2.824994628906" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968860839844" />
                  <Point X="25.88725" Y="-2.996687255859" />
                  <Point X="25.875625" Y="-3.025810302734" />
                  <Point X="25.865005859375" Y="-3.074671875" />
                  <Point X="25.821810546875" Y="-3.273398193359" />
                  <Point X="25.819724609375" Y="-3.289627197266" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.996091796875" Y="-4.662614746094" />
                  <Point X="26.02206640625" Y="-4.859915527344" />
                  <Point X="25.975689453125" Y="-4.870081054688" />
                  <Point X="25.942658203125" Y="-4.87608203125" />
                  <Point X="25.92931640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941572265625" Y="-4.752636230469" />
                  <Point X="23.890201171875" Y="-4.739419433594" />
                  <Point X="23.858755859375" Y="-4.731328613281" />
                  <Point X="23.869708984375" Y="-4.648122070312" />
                  <Point X="23.8792265625" Y="-4.575838378906" />
                  <Point X="23.879923828125" Y="-4.568099121094" />
                  <Point X="23.88055078125" Y="-4.54103125" />
                  <Point X="23.8782421875" Y="-4.509324707031" />
                  <Point X="23.876666015625" Y="-4.497689941406" />
                  <Point X="23.864720703125" Y="-4.437637695313" />
                  <Point X="23.81613671875" Y="-4.1933984375" />
                  <Point X="23.811875" Y="-4.178469238281" />
                  <Point X="23.80097265625" Y="-4.149501464844" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.769423828125" Y="-4.094859619141" />
                  <Point X="23.749791015625" Y="-4.070937255859" />
                  <Point X="23.738994140625" Y="-4.05978125" />
                  <Point X="23.6929609375" Y="-4.019409912109" />
                  <Point X="23.505734375" Y="-3.855216064453" />
                  <Point X="23.493263671875" Y="-3.845966308594" />
                  <Point X="23.46698046875" Y="-3.829621826172" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.302087890625" Y="-3.792165039062" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.79549609375" />
                  <Point X="22.918134765625" Y="-3.808269042969" />
                  <Point X="22.9045625" Y="-3.815811767578" />
                  <Point X="22.85365234375" Y="-3.849828613281" />
                  <Point X="22.64659765625" Y="-3.9881796875" />
                  <Point X="22.640314453125" Y="-3.992760253906" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629394531" />
                  <Point X="22.496796875" Y="-4.162479003906" />
                  <Point X="22.252408203125" Y="-4.011159179687" />
                  <Point X="22.181294921875" Y="-3.956403320312" />
                  <Point X="22.01913671875" Y="-3.831546875" />
                  <Point X="22.511595703125" Y="-2.978583007812" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.7100859375" />
                  <Point X="22.67605078125" Y="-2.681121582031" />
                  <Point X="22.680314453125" Y="-2.666190917969" />
                  <Point X="22.6865859375" Y="-2.634665527344" />
                  <Point X="22.688361328125" Y="-2.619241455078" />
                  <Point X="22.689375" Y="-2.588309326172" />
                  <Point X="22.6853359375" Y="-2.557625244141" />
                  <Point X="22.676353515625" Y="-2.528008789062" />
                  <Point X="22.6706484375" Y="-2.513568359375" />
                  <Point X="22.65643359375" Y="-2.484739746094" />
                  <Point X="22.648453125" Y="-2.471422119141" />
                  <Point X="22.63042578125" Y="-2.44626171875" />
                  <Point X="22.62037890625" Y="-2.434418945313" />
                  <Point X="22.60303125" Y="-2.4170703125" />
                  <Point X="22.591189453125" Y="-2.407024169922" />
                  <Point X="22.56603125" Y="-2.388995361328" />
                  <Point X="22.55271484375" Y="-2.381012695312" />
                  <Point X="22.523884765625" Y="-2.366794677734" />
                  <Point X="22.509443359375" Y="-2.361087890625" />
                  <Point X="22.479826171875" Y="-2.352103027344" />
                  <Point X="22.449140625" Y="-2.348062744141" />
                  <Point X="22.418205078125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.278380859375" Y="-2.9764453125" />
                  <Point X="21.206912109375" Y="-3.017708007813" />
                  <Point X="20.995984375" Y="-2.740591064453" />
                  <Point X="20.944998046875" Y="-2.655096679688" />
                  <Point X="20.818734375" Y="-2.443373046875" />
                  <Point X="21.69482421875" Y="-1.771125488281" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.960841796875" Y="-1.566064941406" />
                  <Point X="21.98373046875" Y="-1.543432006836" />
                  <Point X="21.997287109375" Y="-1.526559936523" />
                  <Point X="22.016220703125" Y="-1.496540283203" />
                  <Point X="22.0242734375" Y="-1.480637329102" />
                  <Point X="22.03251171875" Y="-1.459694580078" />
                  <Point X="22.0396328125" Y="-1.437776733398" />
                  <Point X="22.04581640625" Y="-1.413900024414" />
                  <Point X="22.048048828125" Y="-1.402389160156" />
                  <Point X="22.052162109375" Y="-1.370904418945" />
                  <Point X="22.05291796875" Y="-1.355686035156" />
                  <Point X="22.052216796875" Y="-1.332820922852" />
                  <Point X="22.048771484375" Y="-1.310215576172" />
                  <Point X="22.042626953125" Y="-1.288180297852" />
                  <Point X="22.03888671875" Y="-1.277344726562" />
                  <Point X="22.0268828125" Y="-1.248362792969" />
                  <Point X="22.02134375" Y="-1.237143798828" />
                  <Point X="22.008841796875" Y="-1.215533325195" />
                  <Point X="21.993611328125" Y="-1.195755493164" />
                  <Point X="21.975912109375" Y="-1.178148193359" />
                  <Point X="21.96648046875" Y="-1.169927368164" />
                  <Point X="21.948751953125" Y="-1.156422241211" />
                  <Point X="21.929990234375" Y="-1.143819824219" />
                  <Point X="21.908734375" Y="-1.131309204102" />
                  <Point X="21.894568359375" Y="-1.124481567383" />
                  <Point X="21.8653046875" Y="-1.113257324219" />
                  <Point X="21.85020703125" Y="-1.108860595703" />
                  <Point X="21.8183203125" Y="-1.10237890625" />
                  <Point X="21.802703125" Y="-1.100532592773" />
                  <Point X="21.77137890625" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="20.4491640625" Y="-1.272201660156" />
                  <Point X="20.339080078125" Y="-1.286694458008" />
                  <Point X="20.25923828125" Y="-0.974110595703" />
                  <Point X="20.24575" Y="-0.879804992676" />
                  <Point X="20.213548828125" Y="-0.654654541016" />
                  <Point X="21.198890625" Y="-0.390632751465" />
                  <Point X="21.491712890625" Y="-0.312171173096" />
                  <Point X="21.5016640625" Y="-0.308906829834" />
                  <Point X="21.52115234375" Y="-0.301300750732" />
                  <Point X="21.530689453125" Y="-0.296958862305" />
                  <Point X="21.551732421875" Y="-0.285938049316" />
                  <Point X="21.571908203125" Y="-0.273716339111" />
                  <Point X="21.594185546875" Y="-0.258255554199" />
                  <Point X="21.60351953125" Y="-0.250870025635" />
                  <Point X="21.627484375" Y="-0.229333984375" />
                  <Point X="21.641857421875" Y="-0.213089645386" />
                  <Point X="21.662234375" Y="-0.1839296875" />
                  <Point X="21.671046875" Y="-0.168385971069" />
                  <Point X="21.680296875" Y="-0.147759048462" />
                  <Point X="21.688390625" Y="-0.126334915161" />
                  <Point X="21.6958125" Y="-0.102422233582" />
                  <Point X="21.698419921875" Y="-0.091950271606" />
                  <Point X="21.70386328125" Y="-0.063226371765" />
                  <Point X="21.705501953125" Y="-0.043465621948" />
                  <Point X="21.704791015625" Y="-0.010884814262" />
                  <Point X="21.703029296875" Y="0.005371555328" />
                  <Point X="21.69895703125" Y="0.026081855774" />
                  <Point X="21.69398828125" Y="0.045746295929" />
                  <Point X="21.6865625" Y="0.069670860291" />
                  <Point X="21.68233203125" Y="0.080788154602" />
                  <Point X="21.669017578125" Y="0.110109512329" />
                  <Point X="21.65777734375" Y="0.128804046631" />
                  <Point X="21.63591796875" Y="0.157181945801" />
                  <Point X="21.623640625" Y="0.170329711914" />
                  <Point X="21.606345703125" Y="0.185645751953" />
                  <Point X="21.588716796875" Y="0.199491897583" />
                  <Point X="21.56644140625" Y="0.214952392578" />
                  <Point X="21.559517578125" Y="0.219328445435" />
                  <Point X="21.53438671875" Y="0.23283366394" />
                  <Point X="21.503435546875" Y="0.245635437012" />
                  <Point X="21.491712890625" Y="0.249611251831" />
                  <Point X="20.30076953125" Y="0.568723754883" />
                  <Point X="20.2145546875" Y="0.591825012207" />
                  <Point X="20.26866796875" Y="0.957518859863" />
                  <Point X="20.295826171875" Y="1.057738647461" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="21.012033203125" Y="1.233239868164" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815551758" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.204703491211" />
                  <Point X="21.28485546875" Y="1.20658972168" />
                  <Point X="21.295109375" Y="1.208053588867" />
                  <Point X="21.315400390625" Y="1.21208972168" />
                  <Point X="21.33755859375" Y="1.21848425293" />
                  <Point X="21.38686328125" Y="1.234029541016" />
                  <Point X="21.396556640625" Y="1.237678588867" />
                  <Point X="21.415486328125" Y="1.246008666992" />
                  <Point X="21.42472265625" Y="1.250689697266" />
                  <Point X="21.443466796875" Y="1.26151171875" />
                  <Point X="21.452142578125" Y="1.267172119141" />
                  <Point X="21.46882421875" Y="1.279403808594" />
                  <Point X="21.48407421875" Y="1.29337878418" />
                  <Point X="21.497712890625" Y="1.308931152344" />
                  <Point X="21.504107421875" Y="1.317079223633" />
                  <Point X="21.516521484375" Y="1.334808837891" />
                  <Point X="21.521990234375" Y="1.343604125977" />
                  <Point X="21.53194140625" Y="1.361742919922" />
                  <Point X="21.541287109375" Y="1.382830322266" />
                  <Point X="21.5610703125" Y="1.430591674805" />
                  <Point X="21.56450390625" Y="1.440356201172" />
                  <Point X="21.5702890625" Y="1.460203369141" />
                  <Point X="21.572640625" Y="1.470285766602" />
                  <Point X="21.576400390625" Y="1.491601074219" />
                  <Point X="21.577640625" Y="1.501888793945" />
                  <Point X="21.578994140625" Y="1.522535400391" />
                  <Point X="21.578091796875" Y="1.543205566406" />
                  <Point X="21.574943359375" Y="1.563655517578" />
                  <Point X="21.572810546875" Y="1.573794189453" />
                  <Point X="21.56720703125" Y="1.594700805664" />
                  <Point X="21.563982421875" Y="1.604544921875" />
                  <Point X="21.556486328125" Y="1.623816650391" />
                  <Point X="21.546345703125" Y="1.644518798828" />
                  <Point X="21.522474609375" Y="1.690374023438" />
                  <Point X="21.517201171875" Y="1.699283325195" />
                  <Point X="21.5057109375" Y="1.716481445313" />
                  <Point X="21.499494140625" Y="1.724770263672" />
                  <Point X="21.48558203125" Y="1.741350341797" />
                  <Point X="21.478498046875" Y="1.748912597656" />
                  <Point X="21.4635546875" Y="1.763216918945" />
                  <Point X="21.4556953125" Y="1.769958984375" />
                  <Point X="20.772568359375" Y="2.294141601562" />
                  <Point X="20.77238671875" Y="2.294281738281" />
                  <Point X="20.99771484375" Y="2.680319335938" />
                  <Point X="21.069634765625" Y="2.772765136719" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.616927734375" Y="2.836828125" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.861810546875" Y="2.729680908203" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.02356640625" Y="2.734227294922" />
                  <Point X="22.04300390625" Y="2.741302001953" />
                  <Point X="22.0615546875" Y="2.750450439453" />
                  <Point X="22.070580078125" Y="2.755530761719" />
                  <Point X="22.088833984375" Y="2.767159667969" />
                  <Point X="22.097251953125" Y="2.773191650391" />
                  <Point X="22.1133828125" Y="2.786137451172" />
                  <Point X="22.133080078125" Y="2.80503515625" />
                  <Point X="22.1818203125" Y="2.853774658203" />
                  <Point X="22.188736328125" Y="2.861489257812" />
                  <Point X="22.201685546875" Y="2.877624023438" />
                  <Point X="22.20771875" Y="2.886044189453" />
                  <Point X="22.21934765625" Y="2.904298828125" />
                  <Point X="22.2244296875" Y="2.913325683594" />
                  <Point X="22.233576171875" Y="2.931874267578" />
                  <Point X="22.240646484375" Y="2.951299072266" />
                  <Point X="22.245564453125" Y="2.971387695312" />
                  <Point X="22.2474765625" Y="2.981573242188" />
                  <Point X="22.250302734375" Y="3.003032226562" />
                  <Point X="22.251091796875" Y="3.013364746094" />
                  <Point X="22.25154296875" Y="3.034046142578" />
                  <Point X="22.249728515625" Y="3.061278320313" />
                  <Point X="22.243720703125" Y="3.129944580078" />
                  <Point X="22.242255859375" Y="3.140201416016" />
                  <Point X="22.23821875" Y="3.160497070312" />
                  <Point X="22.235646484375" Y="3.170535888672" />
                  <Point X="22.22913671875" Y="3.191177978516" />
                  <Point X="22.225486328125" Y="3.200873291016" />
                  <Point X="22.21715625" Y="3.21980078125" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="21.94061328125" Y="3.699915771484" />
                  <Point X="22.351630859375" Y="4.015038330078" />
                  <Point X="22.464904296875" Y="4.077971191406" />
                  <Point X="22.8074765625" Y="4.268295898437" />
                  <Point X="22.841970703125" Y="4.223341308594" />
                  <Point X="22.881435546875" Y="4.171909179688" />
                  <Point X="22.8881796875" Y="4.164048828125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93491015625" Y="4.121896972656" />
                  <Point X="22.952111328125" Y="4.110403808594" />
                  <Point X="22.9610234375" Y="4.105127441406" />
                  <Point X="22.979814453125" Y="4.095345703125" />
                  <Point X="23.056240234375" Y="4.055560546875" />
                  <Point X="23.06567578125" Y="4.051285644531" />
                  <Point X="23.084953125" Y="4.043789550781" />
                  <Point X="23.094794921875" Y="4.040568359375" />
                  <Point X="23.115701171875" Y="4.034966308594" />
                  <Point X="23.1258359375" Y="4.032834960938" />
                  <Point X="23.146279296875" Y="4.029687988281" />
                  <Point X="23.1669453125" Y="4.028785400391" />
                  <Point X="23.187583984375" Y="4.030137939453" />
                  <Point X="23.197865234375" Y="4.031377685547" />
                  <Point X="23.219181640625" Y="4.035135986328" />
                  <Point X="23.2292734375" Y="4.037488525391" />
                  <Point X="23.249138671875" Y="4.043279052734" />
                  <Point X="23.258912109375" Y="4.046716552734" />
                  <Point X="23.278482421875" Y="4.054823974609" />
                  <Point X="23.3580859375" Y="4.087796142578" />
                  <Point X="23.367423828125" Y="4.092276367188" />
                  <Point X="23.385556640625" Y="4.102225585938" />
                  <Point X="23.3943515625" Y="4.107694824219" />
                  <Point X="23.412080078125" Y="4.120109375" />
                  <Point X="23.420228515625" Y="4.12650390625" />
                  <Point X="23.43577734375" Y="4.140140625" />
                  <Point X="23.44974609375" Y="4.155385742188" />
                  <Point X="23.4619765625" Y="4.172064453125" />
                  <Point X="23.467638671875" Y="4.180740234375" />
                  <Point X="23.4784609375" Y="4.199483886719" />
                  <Point X="23.48314453125" Y="4.208724121094" />
                  <Point X="23.491478515625" Y="4.2276640625" />
                  <Point X="23.49512890625" Y="4.237363769531" />
                  <Point X="23.501498046875" Y="4.257567871094" />
                  <Point X="23.527408203125" Y="4.339740234375" />
                  <Point X="23.529978515625" Y="4.349772460938" />
                  <Point X="23.53401171875" Y="4.370053222656" />
                  <Point X="23.535474609375" Y="4.380301757812" />
                  <Point X="23.537361328125" Y="4.401863769531" />
                  <Point X="23.53769921875" Y="4.412216796875" />
                  <Point X="23.537248046875" Y="4.4328984375" />
                  <Point X="23.536458984375" Y="4.443227050781" />
                  <Point X="23.520734375" Y="4.562655761719" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.206140625" Y="4.732391113281" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.7361953125" Y="4.404056640625" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.021630859375" Y="4.085113037109" />
                  <Point X="25.05216796875" Y="4.090154541016" />
                  <Point X="25.06723046875" Y="4.093927246094" />
                  <Point X="25.09766796875" Y="4.104259277344" />
                  <Point X="25.1119140625" Y="4.110436035156" />
                  <Point X="25.13920703125" Y="4.125024902344" />
                  <Point X="25.1637578125" Y="4.143861328125" />
                  <Point X="25.184916015625" Y="4.16644921875" />
                  <Point X="25.1945703125" Y="4.178612792969" />
                  <Point X="25.2124296875" Y="4.205339355469" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.24710546875" />
                  <Point X="25.23798046875" Y="4.261727050781" />
                  <Point X="25.37819140625" Y="4.785005859375" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="25.941494140625" Y="4.710481933594" />
                  <Point X="26.453591796875" Y="4.586845703125" />
                  <Point X="26.52538671875" Y="4.560805175781" />
                  <Point X="26.85826171875" Y="4.440068847656" />
                  <Point X="26.9297734375" Y="4.406625" />
                  <Point X="27.25044921875" Y="4.256655273438" />
                  <Point X="27.31959765625" Y="4.216369628906" />
                  <Point X="27.6294375" Y="4.03585546875" />
                  <Point X="27.6946015625" Y="3.989516601562" />
                  <Point X="27.817783203125" Y="3.901916015625" />
                  <Point X="27.23535546875" Y="2.893120361328" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.06090234375" Y="2.589973876953" />
                  <Point X="27.04947265625" Y="2.563280029297" />
                  <Point X="27.040232421875" Y="2.535352294922" />
                  <Point X="27.037064453125" Y="2.524753662109" />
                  <Point X="27.01983203125" Y="2.460311767578" />
                  <Point X="27.017359375" Y="2.447693359375" />
                  <Point X="27.014138671875" Y="2.422235351562" />
                  <Point X="27.013390625" Y="2.409395751953" />
                  <Point X="27.013705078125" Y="2.376792724609" />
                  <Point X="27.015064453125" Y="2.355881103516" />
                  <Point X="27.021783203125" Y="2.300157226562" />
                  <Point X="27.02380078125" Y="2.289038330078" />
                  <Point X="27.029142578125" Y="2.267116455078" />
                  <Point X="27.032466796875" Y="2.256313476562" />
                  <Point X="27.040732421875" Y="2.234224365234" />
                  <Point X="27.04531640625" Y="2.223893066406" />
                  <Point X="27.055681640625" Y="2.203842041016" />
                  <Point X="27.06994140625" Y="2.181628417969" />
                  <Point X="27.104421875" Y="2.130813720703" />
                  <Point X="27.11242578125" Y="2.120598876953" />
                  <Point X="27.12975" Y="2.101354248047" />
                  <Point X="27.1390703125" Y="2.092324462891" />
                  <Point X="27.164865234375" Y="2.070546875" />
                  <Point X="27.180751953125" Y="2.058503417969" />
                  <Point X="27.23156640625" Y="2.024023681641" />
                  <Point X="27.24128125" Y="2.018244384766" />
                  <Point X="27.261326171875" Y="2.007882080078" />
                  <Point X="27.27165625" Y="2.003299194336" />
                  <Point X="27.293744140625" Y="1.995033081055" />
                  <Point X="27.304546875" Y="1.991708251953" />
                  <Point X="27.326470703125" Y="1.986364868164" />
                  <Point X="27.35129296875" Y="1.982694335938" />
                  <Point X="27.407015625" Y="1.975974975586" />
                  <Point X="27.420244140625" Y="1.975309936523" />
                  <Point X="27.446666015625" Y="1.975826171875" />
                  <Point X="27.459859375" Y="1.97700769043" />
                  <Point X="27.494607421875" Y="1.982582275391" />
                  <Point X="27.513595703125" Y="1.986633056641" />
                  <Point X="27.578037109375" Y="2.003865722656" />
                  <Point X="27.584" Y="2.005671875" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572753906" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.833896484375" Y="2.719457763672" />
                  <Point X="28.94040625" Y="2.780950927734" />
                  <Point X="29.043953125" Y="2.637043212891" />
                  <Point X="29.08028125" Y="2.577010742188" />
                  <Point X="29.136884765625" Y="2.483471435547" />
                  <Point X="28.396830078125" Y="1.915606811523" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.1654296875" Y="1.737397338867" />
                  <Point X="28.144265625" Y="1.716626098633" />
                  <Point X="28.1236171875" Y="1.6925" />
                  <Point X="28.117171875" Y="1.684545898438" />
                  <Point X="28.07079296875" Y="1.624040893555" />
                  <Point X="28.063529296875" Y="1.613064575195" />
                  <Point X="28.050591796875" Y="1.590222045898" />
                  <Point X="28.04491796875" Y="1.578355957031" />
                  <Point X="28.032201171875" Y="1.546215942383" />
                  <Point X="28.025896484375" Y="1.527487182617" />
                  <Point X="28.008619140625" Y="1.465711669922" />
                  <Point X="28.006228515625" Y="1.454669921875" />
                  <Point X="28.0027734375" Y="1.432370849609" />
                  <Point X="28.001708984375" Y="1.42111340332" />
                  <Point X="28.000892578125" Y="1.39754284668" />
                  <Point X="28.001173828125" Y="1.386239990234" />
                  <Point X="28.003078125" Y="1.363747192383" />
                  <Point X="28.008189453125" Y="1.335657592773" />
                  <Point X="28.02237109375" Y="1.266924438477" />
                  <Point X="28.02595703125" Y="1.254143188477" />
                  <Point X="28.034873046875" Y="1.229207763672" />
                  <Point X="28.040203125" Y="1.217053710938" />
                  <Point X="28.056884765625" Y="1.185260986328" />
                  <Point X="28.066400390625" Y="1.16911340332" />
                  <Point X="28.10497265625" Y="1.110483276367" />
                  <Point X="28.11173828125" Y="1.101424804688" />
                  <Point X="28.12629296875" Y="1.084179199219" />
                  <Point X="28.13408203125" Y="1.07599230957" />
                  <Point X="28.151326171875" Y="1.059901977539" />
                  <Point X="28.16003515625" Y="1.052695556641" />
                  <Point X="28.17824609375" Y="1.039369140625" />
                  <Point X="28.2014921875" Y="1.025512207031" />
                  <Point X="28.257390625" Y="0.994046569824" />
                  <Point X="28.269638671875" Y="0.988260070801" />
                  <Point X="28.2948359375" Y="0.978487670898" />
                  <Point X="28.30778515625" Y="0.97450177002" />
                  <Point X="28.344345703125" Y="0.966047973633" />
                  <Point X="28.362255859375" Y="0.962801757812" />
                  <Point X="28.437833984375" Y="0.952812927246" />
                  <Point X="28.444033203125" Y="0.952199707031" />
                  <Point X="28.46571875" Y="0.951222961426" />
                  <Point X="28.49121875" Y="0.952032348633" />
                  <Point X="28.50060546875" Y="0.952797180176" />
                  <Point X="29.6384921875" Y="1.102603149414" />
                  <Point X="29.704703125" Y="1.111319946289" />
                  <Point X="29.752689453125" Y="0.914207580566" />
                  <Point X="29.7641328125" Y="0.840706726074" />
                  <Point X="29.783873046875" Y="0.713921142578" />
                  <Point X="28.948560546875" Y="0.490099853516" />
                  <Point X="28.6919921875" Y="0.42135269165" />
                  <Point X="28.68603125" Y="0.419543823242" />
                  <Point X="28.665625" Y="0.412136749268" />
                  <Point X="28.64237890625" Y="0.401617370605" />
                  <Point X="28.634001953125" Y="0.397313995361" />
                  <Point X="28.61574609375" Y="0.386761016846" />
                  <Point X="28.5414921875" Y="0.343841583252" />
                  <Point X="28.530376953125" Y="0.33632244873" />
                  <Point X="28.509314453125" Y="0.319789642334" />
                  <Point X="28.4993671875" Y="0.310775939941" />
                  <Point X="28.474099609375" Y="0.284397033691" />
                  <Point X="28.461841796875" Y="0.270266571045" />
                  <Point X="28.417291015625" Y="0.213497085571" />
                  <Point X="28.41085546875" Y="0.204206665039" />
                  <Point X="28.399130859375" Y="0.184925521851" />
                  <Point X="28.393841796875" Y="0.174934799194" />
                  <Point X="28.384068359375" Y="0.153470825195" />
                  <Point X="28.380005859375" Y="0.142924255371" />
                  <Point X="28.373162109375" Y="0.121429519653" />
                  <Point X="28.366728515625" Y="0.091415283203" />
                  <Point X="28.35187890625" Y="0.013870851517" />
                  <Point X="28.35028515625" Y="0.00042716977" />
                  <Point X="28.349025390625" Y="-0.02656006813" />
                  <Point X="28.349359375" Y="-0.040103626251" />
                  <Point X="28.353009765625" Y="-0.077978973389" />
                  <Point X="28.355525390625" Y="-0.09549105835" />
                  <Point X="28.370376953125" Y="-0.173035644531" />
                  <Point X="28.37316015625" Y="-0.183989593506" />
                  <Point X="28.380005859375" Y="-0.205488632202" />
                  <Point X="28.384068359375" Y="-0.216033569336" />
                  <Point X="28.393841796875" Y="-0.237497238159" />
                  <Point X="28.399130859375" Y="-0.247487670898" />
                  <Point X="28.410853515625" Y="-0.266765380859" />
                  <Point X="28.428240234375" Y="-0.290010742188" />
                  <Point X="28.47279296875" Y="-0.34678024292" />
                  <Point X="28.478712890625" Y="-0.353626831055" />
                  <Point X="28.501146484375" Y="-0.37581149292" />
                  <Point X="28.53018359375" Y="-0.398728668213" />
                  <Point X="28.5415" Y="-0.406405639648" />
                  <Point X="28.5597578125" Y="-0.416958618164" />
                  <Point X="28.634009765625" Y="-0.459878234863" />
                  <Point X="28.639498046875" Y="-0.462815612793" />
                  <Point X="28.659154296875" Y="-0.472014770508" />
                  <Point X="28.683025390625" Y="-0.481026824951" />
                  <Point X="28.6919921875" Y="-0.483912780762" />
                  <Point X="29.735486328125" Y="-0.763516418457" />
                  <Point X="29.784880859375" Y="-0.776751403809" />
                  <Point X="29.761619140625" Y="-0.931038574219" />
                  <Point X="29.74694921875" Y="-0.995325561523" />
                  <Point X="29.7278046875" Y="-1.079219726563" />
                  <Point X="28.734330078125" Y="-0.948426452637" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042480469" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.318650390625" Y="-0.920464355469" />
                  <Point X="28.17291796875" Y="-0.952139892578" />
                  <Point X="28.157875" Y="-0.956742614746" />
                  <Point X="28.12875390625" Y="-0.968367614746" />
                  <Point X="28.11467578125" Y="-0.975390014648" />
                  <Point X="28.086849609375" Y="-0.992282531738" />
                  <Point X="28.074123046875" Y="-1.001532348633" />
                  <Point X="28.050375" Y="-1.022002502441" />
                  <Point X="28.039353515625" Y="-1.03322265625" />
                  <Point X="28.0176953125" Y="-1.059270751953" />
                  <Point X="27.929609375" Y="-1.165210693359" />
                  <Point X="27.921330078125" Y="-1.176845458984" />
                  <Point X="27.906609375" Y="-1.201226806641" />
                  <Point X="27.90016796875" Y="-1.213973022461" />
                  <Point X="27.88882421875" Y="-1.241358154297" />
                  <Point X="27.884365234375" Y="-1.254928344727" />
                  <Point X="27.877533203125" Y="-1.282581176758" />
                  <Point X="27.87516015625" Y="-1.296663818359" />
                  <Point X="27.872056640625" Y="-1.330396972656" />
                  <Point X="27.859431640625" Y="-1.467594116211" />
                  <Point X="27.859291015625" Y="-1.483316894531" />
                  <Point X="27.861607421875" Y="-1.514580932617" />
                  <Point X="27.864064453125" Y="-1.530121948242" />
                  <Point X="27.871794921875" Y="-1.561742919922" />
                  <Point X="27.87678515625" Y="-1.576666015625" />
                  <Point X="27.889158203125" Y="-1.60548059082" />
                  <Point X="27.896541015625" Y="-1.619372070312" />
                  <Point X="27.91637109375" Y="-1.650216186523" />
                  <Point X="27.997021484375" Y="-1.775662719727" />
                  <Point X="28.001748046875" Y="-1.782359008789" />
                  <Point X="28.019798828125" Y="-1.804454101562" />
                  <Point X="28.0434921875" Y="-1.828121337891" />
                  <Point X="28.052798828125" Y="-1.836277709961" />
                  <Point X="29.02116796875" Y="-2.579333984375" />
                  <Point X="29.087171875" Y="-2.629980712891" />
                  <Point X="29.045486328125" Y="-2.697435791016" />
                  <Point X="29.01515625" Y="-2.740528564453" />
                  <Point X="29.001275390625" Y="-2.760251708984" />
                  <Point X="28.1134609375" Y="-2.247671630859" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.728466796875" Y="-2.058635986328" />
                  <Point X="27.555021484375" Y="-2.027312011719" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.03513659668" />
                  <Point X="27.415068359375" Y="-2.044960571289" />
                  <Point X="27.40058984375" Y="-2.051109130859" />
                  <Point X="27.365162109375" Y="-2.069754638672" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.20896875" Y="-2.153170654297" />
                  <Point X="27.186037109375" Y="-2.170064453125" />
                  <Point X="27.175208984375" Y="-2.179375732422" />
                  <Point X="27.15425" Y="-2.200335449219" />
                  <Point X="27.144939453125" Y="-2.211163085938" />
                  <Point X="27.128048828125" Y="-2.234090820312" />
                  <Point X="27.12046875" Y="-2.246190917969" />
                  <Point X="27.101822265625" Y="-2.281618896484" />
                  <Point X="27.02598828125" Y="-2.425708984375" />
                  <Point X="27.019837890625" Y="-2.440187744141" />
                  <Point X="27.01001171875" Y="-2.469967773438" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517441894531" />
                  <Point X="27.000279296875" Y="-2.533139160156" />
                  <Point X="27.000685546875" Y="-2.5644921875" />
                  <Point X="27.00219140625" Y="-2.580147949219" />
                  <Point X="27.00989453125" Y="-2.622793457031" />
                  <Point X="27.04121875" Y="-2.79623828125" />
                  <Point X="27.043015625" Y="-2.804215576172" />
                  <Point X="27.05123828125" Y="-2.831543212891" />
                  <Point X="27.06407421875" Y="-2.862482910156" />
                  <Point X="27.06955078125" Y="-2.873578857422" />
                  <Point X="27.69182421875" Y="-3.951389892578" />
                  <Point X="27.73589453125" Y="-4.027722412109" />
                  <Point X="27.723755859375" Y="-4.036081787109" />
                  <Point X="27.036140625" Y="-3.139965332031" />
                  <Point X="26.83391796875" Y="-2.876422607422" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849626220703" />
                  <Point X="26.78325390625" Y="-2.828004882812" />
                  <Point X="26.77330078125" Y="-2.820646484375" />
                  <Point X="26.731240234375" Y="-2.793605712891" />
                  <Point X="26.56017578125" Y="-2.683627685547" />
                  <Point X="26.546283203125" Y="-2.676244628906" />
                  <Point X="26.517470703125" Y="-2.663873291016" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.36239453125" Y="-2.650749267578" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="26.008341796875" Y="-2.751946777344" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883087158203" />
                  <Point X="25.83218359375" Y="-2.906836425781" />
                  <Point X="25.822935546875" Y="-2.919562255859" />
                  <Point X="25.80604296875" Y="-2.947388671875" />
                  <Point X="25.79901953125" Y="-2.961468505859" />
                  <Point X="25.78739453125" Y="-2.990591552734" />
                  <Point X="25.78279296875" Y="-3.005634765625" />
                  <Point X="25.772173828125" Y="-3.054496337891" />
                  <Point X="25.728978515625" Y="-3.25322265625" />
                  <Point X="25.7275859375" Y="-3.261287353516" />
                  <Point X="25.724724609375" Y="-3.289677001953" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520263672" />
                  <Point X="25.83308984375" Y="-4.152327636719" />
                  <Point X="25.734123046875" Y="-3.782973632812" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480122314453" />
                  <Point X="25.64214453125" Y="-3.453574707031" />
                  <Point X="25.62678515625" Y="-3.423811035156" />
                  <Point X="25.62040625" Y="-3.413208740234" />
                  <Point X="25.58809375" Y="-3.366653564453" />
                  <Point X="25.456677734375" Y="-3.177308349609" />
                  <Point X="25.446671875" Y="-3.165173828125" />
                  <Point X="25.4247890625" Y="-3.142718505859" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104936767578" />
                  <Point X="25.3452421875" Y="-3.090829345703" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.28065625" Y="-3.069420654297" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.998840087891" />
                  <Point X="24.948935546875" Y="-3.003109619141" />
                  <Point X="24.935015625" Y="-3.006305664062" />
                  <Point X="24.885015625" Y="-3.021823974609" />
                  <Point X="24.68165625" Y="-3.084938964844" />
                  <Point X="24.6670703125" Y="-3.090829345703" />
                  <Point X="24.6390703125" Y="-3.104936767578" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.5875234375" Y="-3.142718505859" />
                  <Point X="24.565638671875" Y="-3.16517578125" />
                  <Point X="24.555630859375" Y="-3.177312255859" />
                  <Point X="24.5233203125" Y="-3.2238671875" />
                  <Point X="24.391904296875" Y="-3.413212646484" />
                  <Point X="24.38753125" Y="-3.420131103516" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.03571875" Y="-4.687889648438" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.684062651602" Y="4.199730171459" />
                  <Point X="21.988845921341" Y="3.616374069509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.286899122151" Y="3.027370769202" />
                  <Point X="21.231646955495" Y="2.981008696539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.849030390441" Y="4.214140847789" />
                  <Point X="22.037078561432" Y="3.532832367533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.374451495331" Y="2.97682224076" />
                  <Point X="20.975273327777" Y="2.641871987591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.910461235212" Y="4.141673754492" />
                  <Point X="22.085311201523" Y="3.449290665558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.462003868511" Y="2.926273712317" />
                  <Point X="20.833401452415" Y="2.398813656814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.579645469792" Y="4.579172306432" />
                  <Point X="23.524637062839" Y="4.533014772446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.994149807808" Y="4.087883112405" />
                  <Point X="22.133543841614" Y="3.365748963582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.549556241691" Y="2.875725183874" />
                  <Point X="20.816383997474" Y="2.260520624163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.80159958363" Y="4.641400229004" />
                  <Point X="23.537533303767" Y="4.419822310965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.088148522289" Y="4.04274370657" />
                  <Point X="22.181776481704" Y="3.282207261606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.637108638989" Y="2.82517667567" />
                  <Point X="20.89358241582" Y="2.201284096038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.023553697467" Y="4.703628151577" />
                  <Point X="23.504929619564" Y="4.268450879089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.229895499828" Y="4.037669850657" />
                  <Point X="22.227203837018" Y="3.196311646209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.724661116805" Y="2.774628235027" />
                  <Point X="20.970780834166" Y="2.142047567913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.205542381623" Y="4.732321096844" />
                  <Point X="22.247289761627" Y="3.089152045654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.824746501438" Y="2.734596151872" />
                  <Point X="21.047979252513" Y="2.082811039789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.377290871654" Y="4.752421498998" />
                  <Point X="22.243192949415" Y="2.961700719551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.95960739182" Y="2.723744182765" />
                  <Point X="21.125177670859" Y="2.023574511664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.54903935938" Y="4.772521899218" />
                  <Point X="21.202376089205" Y="1.964337983539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.42343636464" Y="1.310729947947" />
                  <Point X="20.347000370082" Y="1.246592533105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.64836361717" Y="4.731851154809" />
                  <Point X="21.279574507552" Y="1.905101455415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.551186512616" Y="1.29391135751" />
                  <Point X="20.30350682482" Y="1.08608342283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.675493116738" Y="4.630601815404" />
                  <Point X="21.356772925898" Y="1.84586492729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.678936660592" Y="1.277092767073" />
                  <Point X="20.264497821436" Y="0.929337289992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.702622616306" Y="4.529352475999" />
                  <Point X="21.433971344244" Y="1.786628399165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.806686808568" Y="1.260274176636" />
                  <Point X="20.243545428238" Y="0.7877424521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.729752115874" Y="4.428103136594" />
                  <Point X="21.50279713434" Y="1.720366401764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.934436956544" Y="1.243455586199" />
                  <Point X="20.222593035039" Y="0.646147614208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.756881616672" Y="4.326853798221" />
                  <Point X="21.550381139549" Y="1.636280430497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.062187093203" Y="1.226636986266" />
                  <Point X="20.283599535033" Y="0.573324453366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.442636541374" Y="4.77825681013" />
                  <Point X="25.357166416559" Y="4.706538859921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.787848366336" Y="4.228824293956" />
                  <Point X="21.578415503185" Y="1.535790362198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.189937212351" Y="1.209818371639" />
                  <Point X="20.395621392347" Y="0.543308260035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.574031317682" Y="4.764496425982" />
                  <Point X="25.314299351233" Y="4.54655542873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.842319627895" Y="4.150517416954" />
                  <Point X="21.539729137241" Y="1.379314954317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.35437869605" Y="1.223787467475" />
                  <Point X="20.50764325788" Y="0.513292073602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.705426093989" Y="4.750736041833" />
                  <Point X="25.271432285906" Y="4.386571997538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.929127565983" Y="4.0993442333" />
                  <Point X="20.619665123414" Y="0.483275887168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.835689563876" Y="4.736026378885" />
                  <Point X="25.220314030642" Y="4.219664995913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.0726635047" Y="4.095771494052" />
                  <Point X="20.731686988947" Y="0.453259700734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.950460722337" Y="4.708317123133" />
                  <Point X="20.843708854481" Y="0.423243514301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.065231829344" Y="4.680607824206" />
                  <Point X="20.955730720014" Y="0.393227327867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.180002936351" Y="4.652898525278" />
                  <Point X="21.067752585548" Y="0.363211141434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.294774043358" Y="4.625189226351" />
                  <Point X="21.179774451081" Y="0.333194955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.409545150364" Y="4.597479927424" />
                  <Point X="21.291796316615" Y="0.303178768566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.517179345408" Y="4.5637820483" />
                  <Point X="21.403818182148" Y="0.273162582133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.337749878524" Y="-0.621374938248" />
                  <Point X="20.222607261311" Y="-0.717991065884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.620368678297" Y="4.526354486983" />
                  <Point X="21.513731335667" Y="0.241376976226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.554879621132" Y="-0.563195143795" />
                  <Point X="20.23844332115" Y="-0.8287167264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.723558006985" Y="4.488926922139" />
                  <Point X="21.600540789051" Y="0.190205064056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.772009363739" Y="-0.505015349342" />
                  <Point X="20.254279736297" Y="-0.939442088778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.826747335672" Y="4.451499357295" />
                  <Point X="21.663606229647" Y="0.119109559514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.989139106346" Y="-0.44683555489" />
                  <Point X="20.277156237248" Y="-1.044260117754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.924179619247" Y="4.409241058021" />
                  <Point X="21.699180354465" Y="0.024946102041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.206268849581" Y="-0.38865575991" />
                  <Point X="20.303241647237" Y="-1.14638555234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.019080734109" Y="4.364858856014" />
                  <Point X="21.695978901892" Y="-0.101753928119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.423398610667" Y="-0.330475949951" />
                  <Point X="20.329327057225" Y="-1.248510986926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.113981856264" Y="4.320476660126" />
                  <Point X="20.448835885934" Y="-1.27224486532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.208882978419" Y="4.276094464238" />
                  <Point X="20.624133353299" Y="-1.249166517595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.299472549658" Y="4.228094447566" />
                  <Point X="20.799430820801" Y="-1.226088169754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.386701553907" Y="4.177274580373" />
                  <Point X="20.974728288304" Y="-1.203009821913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.473930442986" Y="4.126454616541" />
                  <Point X="21.150025755806" Y="-1.179931474073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.561159332064" Y="4.075634652708" />
                  <Point X="21.325323223308" Y="-1.156853126232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.646817284599" Y="4.023496516601" />
                  <Point X="21.50062069081" Y="-1.133774778391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.72681445026" Y="3.966608416315" />
                  <Point X="21.675918158312" Y="-1.11069643055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.806810416416" Y="3.909719309526" />
                  <Point X="21.83063992728" Y="-1.104883143761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.697952592264" Y="3.694363056943" />
                  <Point X="21.931123538655" Y="-1.144581075003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.55907221081" Y="3.4538148876" />
                  <Point X="22.003157139852" Y="-1.208151399293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.420191829355" Y="3.213266718257" />
                  <Point X="22.045082331255" Y="-1.296985679136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.281311447901" Y="2.972718548914" />
                  <Point X="22.043671152825" Y="-1.422183490923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.911450220955" Y="-2.372229657266" />
                  <Point X="20.82137929129" Y="-2.447808141128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.142430479837" Y="2.732169887348" />
                  <Point X="20.870670548023" Y="-2.53046155827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.035337127524" Y="2.518294202434" />
                  <Point X="20.919961804756" Y="-2.613114975411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.013745988282" Y="2.376163392972" />
                  <Point X="20.969253324558" Y="-2.695768171812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.029643322114" Y="2.265489147442" />
                  <Point X="21.022349177945" Y="-2.775229153304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.072682149279" Y="2.177589318955" />
                  <Point X="21.079952266924" Y="-2.850908115074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.129695131481" Y="2.101415198806" />
                  <Point X="21.137555355903" Y="-2.926587076843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.205962222004" Y="2.041397193849" />
                  <Point X="21.195158444882" Y="-3.002266038613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.297224728997" Y="1.993961837321" />
                  <Point X="21.584025328342" Y="-2.799981672611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.422850716083" Y="1.975360864264" />
                  <Point X="22.057811636224" Y="-2.526441448897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.628804347797" Y="2.024162788188" />
                  <Point X="22.416788345981" Y="-2.349237916626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.100815492119" Y="2.296213472814" />
                  <Point X="22.5363390115" Y="-2.372936689768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.574602901061" Y="2.569754620427" />
                  <Point X="22.616036475225" Y="-2.430076269837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.95308728612" Y="2.76332703585" />
                  <Point X="22.668798778714" Y="-2.509817132926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.008726679404" Y="2.686000337747" />
                  <Point X="22.688422844769" Y="-2.617364278823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.06221366206" Y="2.60686755268" />
                  <Point X="22.611073632099" Y="-2.806281667133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.11198588506" Y="2.524617714156" />
                  <Point X="28.27249371522" Y="1.820200144067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.13205238883" Y="1.702355878891" />
                  <Point X="22.472193487861" Y="-3.046829637428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.011929201767" Y="1.477546864443" />
                  <Point X="22.333312742725" Y="-3.287378111936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.005862956795" Y="1.348442988038" />
                  <Point X="22.194431997589" Y="-3.527926586444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.029492143802" Y="1.244256537654" />
                  <Point X="22.055551252453" Y="-3.768475060952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.073988372882" Y="1.157579614578" />
                  <Point X="22.076000309998" Y="-3.875329956794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.12985058166" Y="1.080439880873" />
                  <Point X="22.153072135073" Y="-3.934672709286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.207891281227" Y="1.02191011061" />
                  <Point X="22.230143538256" Y="-3.994015815787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.301493848988" Y="0.976438298209" />
                  <Point X="22.312882368099" Y="-4.048603386669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.423404727669" Y="0.954719979061" />
                  <Point X="22.397923640075" Y="-4.101258979205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.581617606297" Y="0.963462654678" />
                  <Point X="22.81294327469" Y="-3.877029849355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.540258359626" Y="-4.105839661013" />
                  <Point X="22.482964912051" Y="-4.153914571742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.756915150229" Y="0.986541066651" />
                  <Point X="23.079278784253" Y="-3.777561513998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.932212694161" Y="1.009619478624" />
                  <Point X="23.216364656979" Y="-3.786546501241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.107510238092" Y="1.032697890597" />
                  <Point X="23.353450409548" Y="-3.795531589307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.282807782024" Y="1.05577630257" />
                  <Point X="23.463033305433" Y="-3.827594314273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.458105325956" Y="1.078854714542" />
                  <Point X="28.66110322269" Y="0.410090543644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.405766186981" Y="0.195837331155" />
                  <Point X="23.541024992591" Y="-3.88616521083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.633402869888" Y="1.101933126515" />
                  <Point X="28.8834598332" Y="0.472656201027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.354784752471" Y="0.029045135774" />
                  <Point X="23.613290713854" Y="-3.949540763259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.719575938324" Y="1.050227223971" />
                  <Point X="29.100589417863" Y="0.530835862948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.355378735694" Y="-0.094470145609" />
                  <Point X="24.855939922291" Y="-3.030847963263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.38385218725" Y="-3.426976607619" />
                  <Point X="23.685556435116" Y="-4.012916315687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.744645531392" Y="0.947249417782" />
                  <Point X="29.317719054264" Y="0.589015568284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.378078687096" Y="-0.199436317246" />
                  <Point X="25.04085959501" Y="-2.999695626573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.327733242082" Y="-3.598079686298" />
                  <Point X="23.755668546319" Y="-4.078098961522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.764286938282" Y="0.839716822572" />
                  <Point X="29.534848690665" Y="0.647195273619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.424264578106" Y="-0.284695445621" />
                  <Point X="25.152835854613" Y="-3.029750080926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.284865554629" Y="-3.758063639516" />
                  <Point X="23.805147718328" Y="-4.160594699026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.781364483234" Y="0.730032891756" />
                  <Point X="29.751978327067" Y="0.705374978955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.483852240427" Y="-0.358709152631" />
                  <Point X="25.260724370557" Y="-3.063234559476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.241997867176" Y="-3.918047592734" />
                  <Point X="23.830113111198" Y="-4.263659939562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.561219848222" Y="-0.417803713952" />
                  <Point X="25.364230460082" Y="-3.100396330417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.199130179723" Y="-4.078031545952" />
                  <Point X="23.851253290719" Y="-4.36993491521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.649687453061" Y="-0.467584271847" />
                  <Point X="25.441342669126" Y="-3.159705196736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.15626249227" Y="-4.238015499169" />
                  <Point X="23.872393327973" Y="-4.476210010233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.757199142136" Y="-0.501384945683" />
                  <Point X="28.236473181" Y="-0.938325907617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.896011527217" Y="-1.224007155736" />
                  <Point X="26.175134140495" Y="-2.667994736236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.786260668763" Y="-2.994298322941" />
                  <Point X="25.497706985151" Y="-3.236423612434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.113394804817" Y="-4.397999452387" />
                  <Point X="23.876465682439" Y="-4.59680659159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.869221002097" Y="-0.531401136792" />
                  <Point X="28.419948406358" Y="-0.908385606175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.868294733078" Y="-1.371277999962" />
                  <Point X="26.341150462609" Y="-2.652704194067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.752066153313" Y="-3.147004620729" />
                  <Point X="25.552101164964" Y="-3.314795168702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.070527117364" Y="-4.557983405605" />
                  <Point X="23.862725200543" Y="-4.732349917367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.981242862059" Y="-0.561417327902" />
                  <Point X="28.549087466054" Y="-0.9240387613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.860653318256" Y="-1.501703600808" />
                  <Point X="26.486310441729" Y="-2.654914201613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.724726856547" Y="-3.29395870705" />
                  <Point X="25.606495575807" Y="-3.393166531112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.027659029937" Y="-4.717967694441" />
                  <Point X="23.977931673122" Y="-4.759693901204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.09326472202" Y="-0.591433519011" />
                  <Point X="28.676837549784" Y="-0.940857405647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.887996008823" Y="-1.602774051724" />
                  <Point X="27.274784014878" Y="-2.117320009677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.119889068631" Y="-2.247292301944" />
                  <Point X="26.582658741407" Y="-2.698082071375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.735247540162" Y="-3.409144497795" />
                  <Point X="25.652123320839" Y="-3.478893999571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.205286581981" Y="-0.621449710121" />
                  <Point X="28.804587658848" Y="-0.957676028735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.938405441607" Y="-1.684489207753" />
                  <Point X="27.531755035423" Y="-2.0257094136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.012961061296" Y="-2.461029245948" />
                  <Point X="26.666338488783" Y="-2.751880118701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.749950122825" Y="-3.520821258592" />
                  <Point X="25.679679087481" Y="-3.579785658431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.317308441942" Y="-0.65146590123" />
                  <Point X="28.932337788643" Y="-0.974494634427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.990195727386" Y="-1.765045690545" />
                  <Point X="27.655922148887" Y="-2.045534526975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.004376708079" Y="-2.592246066052" />
                  <Point X="26.750018273472" Y="-2.805678134718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.764652705488" Y="-3.632498019389" />
                  <Point X="25.706808694976" Y="-3.681034907274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.429330301903" Y="-0.681482092339" />
                  <Point X="29.060087918439" Y="-0.991313240119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.052955345856" Y="-1.83639781032" />
                  <Point X="27.777136927918" Y="-2.067836943083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.023827050455" Y="-2.699938983426" />
                  <Point X="26.825220422462" Y="-2.866589731724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.77935528815" Y="-3.744174780186" />
                  <Point X="25.733938302471" Y="-3.782284156118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.541352161864" Y="-0.711498283449" />
                  <Point X="29.187838048234" Y="-1.008131845812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.130153800655" Y="-1.895634307857" />
                  <Point X="27.874814619946" Y="-2.109889320216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.043889120561" Y="-2.807118600285" />
                  <Point X="26.883808837353" Y="-2.941441906884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.794057870813" Y="-3.855851540982" />
                  <Point X="25.761067656818" Y="-3.883533617378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.653374021825" Y="-0.741514474558" />
                  <Point X="29.31558817803" Y="-1.024950451504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.207352255454" Y="-1.954870805395" />
                  <Point X="27.962367146637" Y="-2.160437719847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.083560348211" Y="-2.897844180282" />
                  <Point X="26.941696131754" Y="-3.016882391989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.808760453476" Y="-3.967528301779" />
                  <Point X="25.78819700943" Y="-3.984783080094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.765396018572" Y="-0.771530550891" />
                  <Point X="29.443338307825" Y="-1.041769057196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.284550710253" Y="-2.014107302932" />
                  <Point X="28.049919673329" Y="-2.210986119478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.131793057253" Y="-2.981385824401" />
                  <Point X="26.999583426155" Y="-3.092322877094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.823463036139" Y="-4.079205062576" />
                  <Point X="25.815326362041" Y="-4.086032542811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.767198633745" Y="-0.89403166965" />
                  <Point X="29.571088437621" Y="-1.058587662889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.361749165053" Y="-2.073343800469" />
                  <Point X="28.137472182312" Y="-2.261534533968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.180025766294" Y="-3.064927468521" />
                  <Point X="27.057470788041" Y="-3.167763305571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.735741051193" Y="-1.044441408054" />
                  <Point X="29.698838567416" Y="-1.075406268581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.438947619852" Y="-2.132580298006" />
                  <Point X="28.225024644436" Y="-2.312082987777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.228258475336" Y="-3.14846911264" />
                  <Point X="27.11535826559" Y="-3.243203636997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.516146074651" Y="-2.191816795543" />
                  <Point X="28.31257710656" Y="-2.362631441587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.276491184377" Y="-3.232010756759" />
                  <Point X="27.173245743139" Y="-3.318643968422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.59334452945" Y="-2.25105329308" />
                  <Point X="28.400129568684" Y="-2.413179895397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.324723893419" Y="-3.315552400878" />
                  <Point X="27.231133220688" Y="-3.394084299848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.670542984249" Y="-2.310289790617" />
                  <Point X="28.487682030808" Y="-2.463728349207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.37295660246" Y="-3.399094044997" />
                  <Point X="27.289020698237" Y="-3.469524631273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.747741439049" Y="-2.369526288154" />
                  <Point X="28.575234492931" Y="-2.514276803017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.421189311502" Y="-3.482635689116" />
                  <Point X="27.346908175786" Y="-3.544964962699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.824939893848" Y="-2.428762785691" />
                  <Point X="28.662786955055" Y="-2.564825256826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.469422020543" Y="-3.566177333235" />
                  <Point X="27.404795653335" Y="-3.620405294124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.902138348647" Y="-2.487999283228" />
                  <Point X="28.750339417179" Y="-2.615373710636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.517654729585" Y="-3.649718977355" />
                  <Point X="27.462683130883" Y="-3.69584562555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.979336803446" Y="-2.547235780765" />
                  <Point X="28.837891879303" Y="-2.665922164446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.565887438626" Y="-3.733260621474" />
                  <Point X="27.520570608432" Y="-3.771285956976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.056535219566" Y="-2.606472310759" />
                  <Point X="28.925444341427" Y="-2.716470618256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.614120147668" Y="-3.816802265593" />
                  <Point X="27.578458085981" Y="-3.846726288401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.662352856709" Y="-3.900343909712" />
                  <Point X="27.63634556353" Y="-3.922166619827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.710585504503" Y="-3.983885605224" />
                  <Point X="27.694233041079" Y="-3.997606951252" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.550595703125" Y="-3.832148925781" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.4320078125" Y="-3.474989013672" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.2243359375" Y="-3.250881591797" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.9413359375" Y="-3.203284912109" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.67941015625" Y="-3.332198730469" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.21924609375" Y="-4.737065429688" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.13886328125" Y="-4.984477050781" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.842857421875" Y="-4.923426269531" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.681333984375" Y="-4.623323242188" />
                  <Point X="23.6908515625" Y="-4.551039550781" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.678373046875" Y="-4.474705078125" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.56768359375" Y="-4.1622578125" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.289662109375" Y="-3.981758300781" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.9592109375" Y="-4.007808105469" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.55975390625" Y="-4.392542480469" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.494654296875" Y="-4.384624023438" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="22.065380859375" Y="-4.106947753906" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.347052734375" Y="-2.883583007812" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597594238281" />
                  <Point X="22.4860234375" Y="-2.568765625" />
                  <Point X="22.46867578125" Y="-2.551416992188" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.373380859375" Y="-3.140990234375" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.11519921875" Y="-3.210923339844" />
                  <Point X="20.838302734375" Y="-2.847135742188" />
                  <Point X="20.781814453125" Y="-2.752415527344" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.57916015625" Y="-1.620388427734" />
                  <Point X="21.836212890625" Y="-1.423144897461" />
                  <Point X="21.8474609375" Y="-1.411084838867" />
                  <Point X="21.85569921875" Y="-1.390141967773" />
                  <Point X="21.8618828125" Y="-1.366265258789" />
                  <Point X="21.86334765625" Y="-1.350050537109" />
                  <Point X="21.85134375" Y="-1.321068603516" />
                  <Point X="21.833615234375" Y="-1.307563354492" />
                  <Point X="21.812359375" Y="-1.295052856445" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.47396484375" Y="-1.460576171875" />
                  <Point X="20.19671875" Y="-1.497076416016" />
                  <Point X="20.18090625" Y="-1.435176269531" />
                  <Point X="20.072607421875" Y="-1.011188049316" />
                  <Point X="20.0576640625" Y="-0.90670501709" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="21.14971484375" Y="-0.207106811523" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.463580078125" Y="-0.117624572754" />
                  <Point X="21.485857421875" Y="-0.102163780212" />
                  <Point X="21.4976796875" Y="-0.090641342163" />
                  <Point X="21.5069296875" Y="-0.070014373779" />
                  <Point X="21.5143515625" Y="-0.046101638794" />
                  <Point X="21.516599609375" Y="-0.031286182404" />
                  <Point X="21.51252734375" Y="-0.010575950623" />
                  <Point X="21.5051015625" Y="0.013348590851" />
                  <Point X="21.49767578125" Y="0.028088119507" />
                  <Point X="21.480380859375" Y="0.043404026031" />
                  <Point X="21.45810546875" Y="0.058864665985" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.25159375" Y="0.385197845459" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.01255859375" Y="0.524735656738" />
                  <Point X="20.08235546875" Y="0.996414733887" />
                  <Point X="20.112439453125" Y="1.107433349609" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="21.036833984375" Y="1.421614379883" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.28041796875" Y="1.399688720703" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056152344" />
                  <Point X="21.360880859375" Y="1.443786010742" />
                  <Point X="21.365744140625" Y="1.455529052734" />
                  <Point X="21.38552734375" Y="1.503290283203" />
                  <Point X="21.389287109375" Y="1.52460546875" />
                  <Point X="21.38368359375" Y="1.545512084961" />
                  <Point X="21.377814453125" Y="1.556786621094" />
                  <Point X="21.353943359375" Y="1.602641845703" />
                  <Point X="21.34003125" Y="1.619221923828" />
                  <Point X="20.656904296875" Y="2.143404785156" />
                  <Point X="20.52389453125" Y="2.245466308594" />
                  <Point X="20.568779296875" Y="2.322364257812" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="20.919673828125" Y="2.889432861328" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.711927734375" Y="3.001373046875" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.878369140625" Y="2.918957763672" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404296875" />
                  <Point X="21.998732421875" Y="2.939388183594" />
                  <Point X="22.04747265625" Y="2.988127685547" />
                  <Point X="22.0591015625" Y="3.006382324219" />
                  <Point X="22.061927734375" Y="3.027841308594" />
                  <Point X="22.060451171875" Y="3.044724609375" />
                  <Point X="22.054443359375" Y="3.113390869141" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.74521875" Y="3.658350585938" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="21.774767578125" Y="3.812180664062" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.37262890625" Y="4.244059082031" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.992708984375" Y="4.339005371094" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.067544921875" Y="4.26387890625" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.218491699219" />
                  <Point X="23.186193359375" Y="4.22225" />
                  <Point X="23.205763671875" Y="4.230357421875" />
                  <Point X="23.2853671875" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.294487792969" />
                  <Point X="23.320287109375" Y="4.314691894531" />
                  <Point X="23.346197265625" Y="4.396864257812" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.31366796875" Y="4.679836425781" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.420412109375" Y="4.731854980469" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.1840546875" Y="4.921103027344" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.91972265625" Y="4.453232421875" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176269531" />
                  <Point X="25.054453125" Y="4.310902832031" />
                  <Point X="25.209560546875" Y="4.889772949219" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.326283203125" Y="4.981481445312" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="25.986083984375" Y="4.895175292969" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.590171875" Y="4.739419433594" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="27.01026171875" Y="4.578733886719" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.4152421875" Y="4.380540527344" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="27.8047109375" Y="4.144357421875" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.3998984375" Y="2.798120361328" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.220615234375" Y="2.475670166016" />
                  <Point X="27.2033828125" Y="2.411228271484" />
                  <Point X="27.203697265625" Y="2.378625244141" />
                  <Point X="27.210416015625" Y="2.322901367188" />
                  <Point X="27.218681640625" Y="2.300812255859" />
                  <Point X="27.22716015625" Y="2.288318359375" />
                  <Point X="27.261640625" Y="2.237503662109" />
                  <Point X="27.287435546875" Y="2.215726074219" />
                  <Point X="27.33825" Y="2.181246337891" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.3740390625" Y="2.171328125" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.464509765625" Y="2.170183349609" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.738896484375" Y="2.884002685547" />
                  <Point X="28.994248046875" Y="3.031430908203" />
                  <Point X="29.014392578125" Y="3.003436523438" />
                  <Point X="29.202595703125" Y="2.741876220703" />
                  <Point X="29.2428359375" Y="2.675379638672" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.512494140625" Y="1.764869750977" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.26796875" Y="1.568956787109" />
                  <Point X="28.22158984375" Y="1.508451904297" />
                  <Point X="28.208873046875" Y="1.476311889648" />
                  <Point X="28.191595703125" Y="1.414536376953" />
                  <Point X="28.190779296875" Y="1.390965698242" />
                  <Point X="28.194267578125" Y="1.374066040039" />
                  <Point X="28.20844921875" Y="1.305332763672" />
                  <Point X="28.225130859375" Y="1.273540161133" />
                  <Point X="28.263703125" Y="1.21491003418" />
                  <Point X="28.280947265625" Y="1.198819824219" />
                  <Point X="28.29469140625" Y="1.191083251953" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.387150390625" Y="1.151163696289" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.61369140625" Y="1.290977539062" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.858357421875" Y="1.283414672852" />
                  <Point X="29.939193359375" Y="0.951367370605" />
                  <Point X="29.95187109375" Y="0.869934143066" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="28.997736328125" Y="0.306573974609" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819503784" />
                  <Point X="28.71083203125" Y="0.222266586304" />
                  <Point X="28.636578125" Y="0.179346984863" />
                  <Point X="28.611310546875" Y="0.152968048096" />
                  <Point X="28.566759765625" Y="0.096198562622" />
                  <Point X="28.556986328125" Y="0.07473449707" />
                  <Point X="28.553333984375" Y="0.055668399811" />
                  <Point X="28.538484375" Y="-0.021876060486" />
                  <Point X="28.542134765625" Y="-0.059751335144" />
                  <Point X="28.556986328125" Y="-0.137295791626" />
                  <Point X="28.566759765625" Y="-0.158759399414" />
                  <Point X="28.577712890625" Y="-0.172717590332" />
                  <Point X="28.622265625" Y="-0.229487075806" />
                  <Point X="28.636578125" Y="-0.24190675354" />
                  <Point X="28.6548359375" Y="-0.252459671021" />
                  <Point X="28.729087890625" Y="-0.295379272461" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.784662109375" Y="-0.579990478516" />
                  <Point X="29.998068359375" Y="-0.637172424316" />
                  <Point X="29.993564453125" Y="-0.667045410156" />
                  <Point X="29.948431640625" Y="-0.966412475586" />
                  <Point X="29.9321875" Y="-1.037596679688" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.709529296875" Y="-1.13680090332" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.359005859375" Y="-1.106129394531" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.185447265625" Y="-1.154697265625" />
                  <Point X="28.1637890625" Y="-1.180745239258" />
                  <Point X="28.075703125" Y="-1.286685424805" />
                  <Point X="28.064359375" Y="-1.314070556641" />
                  <Point X="28.061255859375" Y="-1.347803710938" />
                  <Point X="28.048630859375" Y="-1.485000854492" />
                  <Point X="28.056361328125" Y="-1.516621826172" />
                  <Point X="28.07619140625" Y="-1.547465942383" />
                  <Point X="28.156841796875" Y="-1.672912353516" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="29.13683203125" Y="-2.428596679688" />
                  <Point X="29.339076171875" Y="-2.583783935547" />
                  <Point X="29.331357421875" Y="-2.596272460938" />
                  <Point X="29.204134765625" Y="-2.802138183594" />
                  <Point X="29.17053125" Y="-2.849883544922" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.0184609375" Y="-2.412216552734" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.694697265625" Y="-2.245611083984" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.453650390625" Y="-2.237890380859" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.33468359375" />
                  <Point X="27.269955078125" Y="-2.370111572266" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.1891640625" Y="-2.546374511719" />
                  <Point X="27.1968671875" Y="-2.589020019531" />
                  <Point X="27.22819140625" Y="-2.76246484375" />
                  <Point X="27.23409375" Y="-2.778578857422" />
                  <Point X="27.8563671875" Y="-3.856389892578" />
                  <Point X="27.98667578125" Y="-4.082087646484" />
                  <Point X="27.986259765625" Y="-4.082384033203" />
                  <Point X="27.83529296875" Y="-4.190215820312" />
                  <Point X="27.797732421875" Y="-4.214528320312" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="26.88540234375" Y="-3.255630126953" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.628490234375" Y="-2.953426513672" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.3798046875" Y="-2.839949951172" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.129814453125" Y="-2.898042480469" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045985839844" />
                  <Point X="25.957837890625" Y="-3.094847412109" />
                  <Point X="25.914642578125" Y="-3.293573730469" />
                  <Point X="25.9139296875" Y="-3.310719970703" />
                  <Point X="26.090279296875" Y="-4.65021484375" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="25.994349609375" Y="-4.96324609375" />
                  <Point X="25.959638671875" Y="-4.969552246094" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#136" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.038136890874" Y="4.497453295221" Z="0.5" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.5" />
                  <Point X="-0.827915273868" Y="5.00204409515" Z="0.5" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.5" />
                  <Point X="-1.599242271024" Y="4.811278434743" Z="0.5" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.5" />
                  <Point X="-1.742061162791" Y="4.704590751138" Z="0.5" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.5" />
                  <Point X="-1.733554822718" Y="4.361008135229" Z="0.5" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.5" />
                  <Point X="-1.819527040381" Y="4.307831638657" Z="0.5" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.5" />
                  <Point X="-1.915524258653" Y="4.339509307679" Z="0.5" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.5" />
                  <Point X="-1.973780258798" Y="4.400723208351" Z="0.5" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.5" />
                  <Point X="-2.657810906068" Y="4.319046421952" Z="0.5" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.5" />
                  <Point X="-3.261810535445" Y="3.883140741479" Z="0.5" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.5" />
                  <Point X="-3.304239630436" Y="3.66463054233" Z="0.5" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.5" />
                  <Point X="-2.995517115832" Y="3.071646754582" Z="0.5" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.5" />
                  <Point X="-3.042779740196" Y="3.00602382769" Z="0.5" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.5" />
                  <Point X="-3.123429711025" Y="3.000047583341" Z="0.5" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.5" />
                  <Point X="-3.269228805968" Y="3.07595431831" Z="0.5" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.5" />
                  <Point X="-4.125946930877" Y="2.951415245782" Z="0.5" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.5" />
                  <Point X="-4.48055922162" Y="2.378849488806" Z="0.5" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.5" />
                  <Point X="-4.379690835413" Y="2.135017031883" Z="0.5" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.5" />
                  <Point X="-3.672691605992" Y="1.56497895798" Z="0.5" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.5" />
                  <Point X="-3.686605718933" Y="1.505943301832" Z="0.5" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.5" />
                  <Point X="-3.740773592902" Y="1.478654733526" Z="0.5" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.5" />
                  <Point X="-3.96279791781" Y="1.502466645589" Z="0.5" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.5" />
                  <Point X="-4.94197802241" Y="1.151790644736" Z="0.5" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.5" />
                  <Point X="-5.043059627374" Y="0.563334640107" Z="0.5" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.5" />
                  <Point X="-4.767505181791" Y="0.368181639541" Z="0.5" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.5" />
                  <Point X="-3.554284900563" Y="0.033608330389" Z="0.5" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.5" />
                  <Point X="-3.541382453463" Y="0.005882413615" Z="0.5" />
                  <Point X="-3.539556741714" Y="0" Z="0.5" />
                  <Point X="-3.546982147672" Y="-0.023924537446" Z="0.5" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.5" />
                  <Point X="-3.57108369456" Y="-0.045267652745" Z="0.5" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.5" />
                  <Point X="-3.869382606982" Y="-0.127530417215" Z="0.5" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.5" />
                  <Point X="-4.997989007571" Y="-0.882504063647" Z="0.5" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.5" />
                  <Point X="-4.87369529821" Y="-1.416270307696" Z="0.5" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.5" />
                  <Point X="-4.525667313974" Y="-1.478868372375" Z="0.5" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.5" />
                  <Point X="-3.197902653851" Y="-1.319373802303" Z="0.5" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.5" />
                  <Point X="-3.198860316737" Y="-1.346326515599" Z="0.5" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.5" />
                  <Point X="-3.457433600999" Y="-1.54944071182" Z="0.5" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.5" />
                  <Point X="-4.267286242335" Y="-2.74674503476" Z="0.5" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.5" />
                  <Point X="-3.930742362975" Y="-3.209926574513" Z="0.5" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.5" />
                  <Point X="-3.607775403804" Y="-3.153011485823" Z="0.5" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.5" />
                  <Point X="-2.558914709476" Y="-2.569415759656" Z="0.5" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.5" />
                  <Point X="-2.702405527345" Y="-2.827302978688" Z="0.5" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.5" />
                  <Point X="-2.971280736256" Y="-4.115284823655" Z="0.5" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.5" />
                  <Point X="-2.537825181713" Y="-4.395854482605" Z="0.5" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.5" />
                  <Point X="-2.406734542332" Y="-4.391700261423" Z="0.5" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.5" />
                  <Point X="-2.019165744383" Y="-4.018101223664" Z="0.5" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.5" />
                  <Point X="-1.71976439021" Y="-4.000371385834" Z="0.5" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.5" />
                  <Point X="-1.471440104372" Y="-4.168569606677" Z="0.5" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.5" />
                  <Point X="-1.37682365033" Y="-4.453180138481" Z="0.5" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.5" />
                  <Point X="-1.374394873083" Y="-4.58551596202" Z="0.5" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.5" />
                  <Point X="-1.175757800425" Y="-4.940569090611" Z="0.5" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.5" />
                  <Point X="-0.876820656357" Y="-5.002281227471" Z="0.5" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.5" />
                  <Point X="-0.73861320183" Y="-4.718726106664" Z="0.5" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.5" />
                  <Point X="-0.28567080604" Y="-3.329427714769" Z="0.5" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.5" />
                  <Point X="-0.050000514809" Y="-3.219757683044" Z="0.5" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.5" />
                  <Point X="0.203358564552" Y="-3.267354362244" Z="0.5" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.5" />
                  <Point X="0.384775053392" Y="-3.472218040999" Z="0.5" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.5" />
                  <Point X="0.496141658571" Y="-3.813809923152" Z="0.5" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.5" />
                  <Point X="0.96241957787" Y="-4.987466886689" Z="0.5" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.5" />
                  <Point X="1.141718803913" Y="-4.94950052471" Z="0.5" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.5" />
                  <Point X="1.133693673411" Y="-4.612408742999" Z="0.5" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.5" />
                  <Point X="1.00053975596" Y="-3.074187540582" Z="0.5" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.5" />
                  <Point X="1.155623889634" Y="-2.90520903992" Z="0.5" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.5" />
                  <Point X="1.378230702624" Y="-2.858459528383" Z="0.5" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.5" />
                  <Point X="1.595293901527" Y="-2.964204578815" Z="0.5" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.5" />
                  <Point X="1.83957742246" Y="-3.254788045243" Z="0.5" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.5" />
                  <Point X="2.818745470965" Y="-4.225222505864" Z="0.5" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.5" />
                  <Point X="3.00916620709" Y="-4.091790636511" Z="0.5" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.5" />
                  <Point X="2.893511635416" Y="-3.800109635104" Z="0.5" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.5" />
                  <Point X="2.239912866837" Y="-2.548853715577" Z="0.5" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.5" />
                  <Point X="2.308044712285" Y="-2.362118142417" Z="0.5" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.5" />
                  <Point X="2.470780235955" Y="-2.250856597682" Z="0.5" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.5" />
                  <Point X="2.679653087935" Y="-2.263535142654" Z="0.5" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.5" />
                  <Point X="2.987303962003" Y="-2.424237864858" Z="0.5" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.5" />
                  <Point X="4.205263436533" Y="-2.847380944181" Z="0.5" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.5" />
                  <Point X="4.367733862343" Y="-2.591277640809" Z="0.5" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.5" />
                  <Point X="4.161112057066" Y="-2.35764897496" Z="0.5" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.5" />
                  <Point X="3.112092337112" Y="-1.489146496182" Z="0.5" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.5" />
                  <Point X="3.104887342365" Y="-1.321105387354" Z="0.5" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.5" />
                  <Point X="3.196077492814" Y="-1.181432076666" Z="0.5" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.5" />
                  <Point X="3.363468067248" Y="-1.123708612849" Z="0.5" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.5" />
                  <Point X="3.696846410924" Y="-1.155093168082" Z="0.5" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.5" />
                  <Point X="4.974775769551" Y="-1.017440652959" Z="0.5" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.5" />
                  <Point X="5.036849677771" Y="-0.64321929892" Z="0.5" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.5" />
                  <Point X="4.791447283777" Y="-0.500414155159" Z="0.5" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.5" />
                  <Point X="3.67369959692" Y="-0.177891012539" Z="0.5" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.5" />
                  <Point X="3.610890853046" Y="-0.110568666154" Z="0.5" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.5" />
                  <Point X="3.585086103161" Y="-0.019066126687" Z="0.5" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.5" />
                  <Point X="3.596285347263" Y="0.077544404547" Z="0.5" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.5" />
                  <Point X="3.644488585354" Y="0.153380072421" Z="0.5" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.5" />
                  <Point X="3.729695817433" Y="0.210257831703" Z="0.5" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.5" />
                  <Point X="4.004520533529" Y="0.28955777761" Z="0.5" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.5" />
                  <Point X="4.995118716369" Y="0.908906246445" Z="0.5" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.5" />
                  <Point X="4.900782471628" Y="1.326521463902" Z="0.5" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.5" />
                  <Point X="4.601008902438" Y="1.371829859051" Z="0.5" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.5" />
                  <Point X="3.387543289257" Y="1.23201265304" Z="0.5" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.5" />
                  <Point X="3.313062441555" Y="1.265934411604" Z="0.5" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.5" />
                  <Point X="3.260745173634" Y="1.332300870633" Z="0.5" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.5" />
                  <Point X="3.23707901014" Y="1.415449537762" Z="0.5" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.5" />
                  <Point X="3.25086822531" Y="1.49412472846" Z="0.5" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.5" />
                  <Point X="3.301494824235" Y="1.569818551949" Z="0.5" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.5" />
                  <Point X="3.53677515648" Y="1.756481957005" Z="0.5" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.5" />
                  <Point X="4.279455449826" Y="2.73254584825" Z="0.5" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.5" />
                  <Point X="4.048820622612" Y="3.063919496717" Z="0.5" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.5" />
                  <Point X="3.707738737099" Y="2.958583993521" Z="0.5" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.5" />
                  <Point X="2.445436181861" Y="2.249766207509" Z="0.5" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.5" />
                  <Point X="2.373867813266" Y="2.25224843809" Z="0.5" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.5" />
                  <Point X="2.30935207246" Y="2.288380383869" Z="0.5" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.5" />
                  <Point X="2.262378216366" Y="2.347672787922" Z="0.5" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.5" />
                  <Point X="2.247181264669" Y="2.415890630567" Z="0.5" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.5" />
                  <Point X="2.262761735149" Y="2.494033394021" Z="0.5" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.5" />
                  <Point X="2.437041359339" Y="2.804400344856" Z="0.5" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.5" />
                  <Point X="2.827529645801" Y="4.216384670466" Z="0.5" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.5" />
                  <Point X="2.434255903581" Y="4.455023092266" Z="0.5" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.5" />
                  <Point X="2.025286609756" Y="4.65530602659" Z="0.5" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.5" />
                  <Point X="1.601064153245" Y="4.817703141869" Z="0.5" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.5" />
                  <Point X="0.991661131727" Y="4.975058654269" Z="0.5" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.5" />
                  <Point X="0.32533396494" Y="5.062489744039" Z="0.5" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.5" />
                  <Point X="0.155107670679" Y="4.93399421073" Z="0.5" />
                  <Point X="0" Y="4.355124473572" Z="0.5" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>