<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#207" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3285" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.918349609375" Y="-4.837573730469" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.397248046875" Y="-3.258291992188" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209021240234" />
                  <Point X="25.33049609375" Y="-3.189777099609" />
                  <Point X="25.30249609375" Y="-3.175668945312" />
                  <Point X="25.0779375" Y="-3.105974121094" />
                  <Point X="25.04913671875" Y="-3.097035644531" />
                  <Point X="25.0209765625" Y="-3.092766357422" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.7386171875" Y="-3.16673046875" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.18977734375" />
                  <Point X="24.655560546875" Y="-3.209021484375" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.488560546875" Y="-3.440562011719" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481571777344" />
                  <Point X="24.449009765625" Y="-3.512524658203" />
                  <Point X="24.403474609375" Y="-3.68246484375" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.950455078125" Y="-4.851133789062" />
                  <Point X="23.920666015625" Y="-4.8453515625" />
                  <Point X="23.75358203125" Y="-4.802362792969" />
                  <Point X="23.7850390625" Y="-4.563438964844" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.51622265625" />
                  <Point X="23.72984375" Y="-4.246520996094" />
                  <Point X="23.722962890625" Y="-4.211931152344" />
                  <Point X="23.712060546875" Y="-4.182963867188" />
                  <Point X="23.69598828125" Y="-4.155126953125" />
                  <Point X="23.67635546875" Y="-4.131203613281" />
                  <Point X="23.469611328125" Y="-3.949892822266" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.082576171875" Y="-3.872981201172" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.016580078125" Y="-3.873708984375" />
                  <Point X="22.98553125" Y="-3.882029296875" />
                  <Point X="22.957341796875" Y="-3.894802246094" />
                  <Point X="22.72869921875" Y="-4.047576416016" />
                  <Point X="22.699376953125" Y="-4.067170166016" />
                  <Point X="22.68721484375" Y="-4.076822021484" />
                  <Point X="22.664900390625" Y="-4.099459472656" />
                  <Point X="22.639333984375" Y="-4.132775878906" />
                  <Point X="22.5198515625" Y="-4.288488769531" />
                  <Point X="22.241958984375" Y="-4.116424316406" />
                  <Point X="22.198291015625" Y="-4.089386474609" />
                  <Point X="21.895279296875" Y="-3.856076904297" />
                  <Point X="21.91642578125" Y="-3.819448974609" />
                  <Point X="22.57623828125" Y="-2.676619873047" />
                  <Point X="22.587140625" Y="-2.647654296875" />
                  <Point X="22.593412109375" Y="-2.616127441406" />
                  <Point X="22.59442578125" Y="-2.585193603516" />
                  <Point X="22.58544140625" Y="-2.555575683594" />
                  <Point X="22.571224609375" Y="-2.526746582031" />
                  <Point X="22.553197265625" Y="-2.501588623047" />
                  <Point X="22.537833984375" Y="-2.486225341797" />
                  <Point X="22.53587890625" Y="-2.484270263672" />
                  <Point X="22.5107265625" Y="-2.466236816406" />
                  <Point X="22.481892578125" Y="-2.452008056641" />
                  <Point X="22.45226953125" Y="-2.443014892578" />
                  <Point X="22.421328125" Y="-2.444023681641" />
                  <Point X="22.38979296875" Y="-2.450293457031" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="22.214248046875" Y="-2.545819580078" />
                  <Point X="21.181978515625" Y="-3.14180078125" />
                  <Point X="20.951640625" Y="-2.839185302734" />
                  <Point X="20.917134765625" Y="-2.793851318359" />
                  <Point X="20.693857421875" Y="-2.419449951172" />
                  <Point X="20.739587890625" Y="-2.384360595703" />
                  <Point X="21.894044921875" Y="-1.498513793945" />
                  <Point X="21.915419921875" Y="-1.475596069336" />
                  <Point X="21.933384765625" Y="-1.448467041016" />
                  <Point X="21.946142578125" Y="-1.419838989258" />
                  <Point X="21.95297265625" Y="-1.393473022461" />
                  <Point X="21.953853515625" Y="-1.390069091797" />
                  <Point X="21.956654296875" Y="-1.359626953125" />
                  <Point X="21.954439453125" Y="-1.327961547852" />
                  <Point X="21.947435546875" Y="-1.298224487305" />
                  <Point X="21.93135546875" Y="-1.272248291016" />
                  <Point X="21.910525390625" Y="-1.24829699707" />
                  <Point X="21.88702734375" Y="-1.228766235352" />
                  <Point X="21.8635546875" Y="-1.214951538086" />
                  <Point X="21.860548828125" Y="-1.213182617188" />
                  <Point X="21.83129296875" Y="-1.201960205078" />
                  <Point X="21.79940234375" Y="-1.195475952148" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="21.583041015625" Y="-1.218743530273" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.17941796875" Y="-1.045481933594" />
                  <Point X="20.165923828125" Y="-0.992650878906" />
                  <Point X="20.107576171875" Y="-0.584698303223" />
                  <Point X="20.1520234375" Y="-0.572788818359" />
                  <Point X="21.467125" Y="-0.220408462524" />
                  <Point X="21.4825078125" Y="-0.214827133179" />
                  <Point X="21.5122734375" Y="-0.199468673706" />
                  <Point X="21.536857421875" Y="-0.182405807495" />
                  <Point X="21.54000390625" Y="-0.180223022461" />
                  <Point X="21.56246484375" Y="-0.158339859009" />
                  <Point X="21.58171875" Y="-0.13207800293" />
                  <Point X="21.595833984375" Y="-0.104066452026" />
                  <Point X="21.604033203125" Y="-0.077647789001" />
                  <Point X="21.6051015625" Y="-0.074200775146" />
                  <Point X="21.60935546875" Y="-0.046084190369" />
                  <Point X="21.609353515625" Y="-0.016453187943" />
                  <Point X="21.605083984375" Y="0.011699375153" />
                  <Point X="21.596884765625" Y="0.038118034363" />
                  <Point X="21.595833984375" Y="0.041505542755" />
                  <Point X="21.581736328125" Y="0.069486732483" />
                  <Point X="21.5624921875" Y="0.095750419617" />
                  <Point X="21.540025390625" Y="0.117646942139" />
                  <Point X="21.51542578125" Y="0.13471925354" />
                  <Point X="21.50392578125" Y="0.141553924561" />
                  <Point X="21.485201171875" Y="0.150965835571" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="21.2984609375" Y="0.203041778564" />
                  <Point X="20.108185546875" Y="0.521975402832" />
                  <Point X="20.166818359375" Y="0.918215881348" />
                  <Point X="20.17551171875" Y="0.976971069336" />
                  <Point X="20.296451171875" Y="1.423267944336" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255017578125" Y="1.299341796875" />
                  <Point X="21.276578125" Y="1.301228271484" />
                  <Point X="21.29686328125" Y="1.305263793945" />
                  <Point X="21.351306640625" Y="1.3224296875" />
                  <Point X="21.3582890625" Y="1.324631103516" />
                  <Point X="21.37722265625" Y="1.332961791992" />
                  <Point X="21.395966796875" Y="1.343783935547" />
                  <Point X="21.4126484375" Y="1.35601550293" />
                  <Point X="21.426287109375" Y="1.371567626953" />
                  <Point X="21.438701171875" Y="1.389297363281" />
                  <Point X="21.448650390625" Y="1.407430908203" />
                  <Point X="21.47049609375" Y="1.460171264648" />
                  <Point X="21.47330078125" Y="1.466941040039" />
                  <Point X="21.479091796875" Y="1.486817016602" />
                  <Point X="21.482845703125" Y="1.508130249023" />
                  <Point X="21.484193359375" Y="1.528762695312" />
                  <Point X="21.481046875" Y="1.549198486328" />
                  <Point X="21.4754453125" Y="1.570102294922" />
                  <Point X="21.46794921875" Y="1.58937902832" />
                  <Point X="21.44158984375" Y="1.640014770508" />
                  <Point X="21.438208984375" Y="1.646508911133" />
                  <Point X="21.426716796875" Y="1.663709350586" />
                  <Point X="21.4128046875" Y="1.680288330078" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="21.3011171875" Y="1.768826293945" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.885064453125" Y="2.675779296875" />
                  <Point X="20.918849609375" Y="2.733663574219" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.79334375" Y="2.844671386719" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.92903125" Y="2.819162597656" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.00098828125" Y="2.826505126953" />
                  <Point X="22.019541015625" Y="2.835655029297" />
                  <Point X="22.037794921875" Y="2.847284912109" />
                  <Point X="22.053921875" Y="2.860229248047" />
                  <Point X="22.107744140625" Y="2.914050048828" />
                  <Point X="22.114646484375" Y="2.920952636719" />
                  <Point X="22.127595703125" Y="2.937086425781" />
                  <Point X="22.139224609375" Y="2.955340820312" />
                  <Point X="22.14837109375" Y="2.973889648438" />
                  <Point X="22.1532890625" Y="2.993978515625" />
                  <Point X="22.156115234375" Y="3.015437255859" />
                  <Point X="22.15656640625" Y="3.036122802734" />
                  <Point X="22.149931640625" Y="3.111947509766" />
                  <Point X="22.14908203125" Y="3.121665527344" />
                  <Point X="22.145046875" Y="3.141948974609" />
                  <Point X="22.138537109375" Y="3.162597412109" />
                  <Point X="22.13020703125" Y="3.181532958984" />
                  <Point X="22.0873359375" Y="3.255788085938" />
                  <Point X="21.816669921875" Y="3.724596923828" />
                  <Point X="22.240541015625" Y="4.049574951172" />
                  <Point X="22.29937890625" Y="4.094686523438" />
                  <Point X="22.83296484375" Y="4.391134765625" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.18939453125" />
                  <Point X="23.089279296875" Y="4.145462402344" />
                  <Point X="23.100103515625" Y="4.139827636719" />
                  <Point X="23.1193828125" Y="4.132331054688" />
                  <Point X="23.1402890625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.12358203125" />
                  <Point X="23.181373046875" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222546875" Y="4.134481445312" />
                  <Point X="23.310447265625" Y="4.170891601563" />
                  <Point X="23.321720703125" Y="4.175561035156" />
                  <Point X="23.3398515625" Y="4.185507324219" />
                  <Point X="23.35758203125" Y="4.197920898438" />
                  <Point X="23.373134765625" Y="4.21155859375" />
                  <Point X="23.385365234375" Y="4.228239257812" />
                  <Point X="23.3961875" Y="4.246981933594" />
                  <Point X="23.404521484375" Y="4.265919433594" />
                  <Point X="23.433130859375" Y="4.356658691406" />
                  <Point X="23.43680078125" Y="4.368295898438" />
                  <Point X="23.4408359375" Y="4.38858203125" />
                  <Point X="23.44272265625" Y="4.410145996094" />
                  <Point X="23.442271484375" Y="4.430828613281" />
                  <Point X="23.437396484375" Y="4.467850097656" />
                  <Point X="23.415798828125" Y="4.6318984375" />
                  <Point X="23.97419921875" Y="4.788454589844" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.866095703125" Y="4.286315917969" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.16818359375" Y="4.368296386719" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.777529296875" Y="4.838704589844" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.414501953125" Y="4.694012207031" />
                  <Point X="26.48102734375" Y="4.677950683594" />
                  <Point X="26.85217578125" Y="4.543333007813" />
                  <Point X="26.894642578125" Y="4.527928710938" />
                  <Point X="27.25369140625" Y="4.360015136719" />
                  <Point X="27.294580078125" Y="4.340893554688" />
                  <Point X="27.641451171875" Y="4.1388046875" />
                  <Point X="27.680978515625" Y="4.115775878906" />
                  <Point X="27.94326171875" Y="3.929253662109" />
                  <Point X="27.911267578125" Y="3.873837402344" />
                  <Point X="27.147583984375" Y="2.551098144531" />
                  <Point X="27.142078125" Y="2.539932861328" />
                  <Point X="27.133080078125" Y="2.516061279297" />
                  <Point X="27.11405078125" Y="2.444901367188" />
                  <Point X="27.1116796875" Y="2.433059326172" />
                  <Point X="27.1078984375" Y="2.405028808594" />
                  <Point X="27.107728515625" Y="2.380955566406" />
                  <Point X="27.1151484375" Y="2.319422607422" />
                  <Point X="27.116099609375" Y="2.311530761719" />
                  <Point X="27.12144140625" Y="2.289611328125" />
                  <Point X="27.12970703125" Y="2.267521240234" />
                  <Point X="27.140072265625" Y="2.247470947266" />
                  <Point X="27.1781484375" Y="2.191358886719" />
                  <Point X="27.18569140625" Y="2.181657226562" />
                  <Point X="27.203876953125" Y="2.161157714844" />
                  <Point X="27.2216015625" Y="2.145591308594" />
                  <Point X="27.277712890625" Y="2.107516845703" />
                  <Point X="27.28491015625" Y="2.102633789063" />
                  <Point X="27.304958984375" Y="2.092269775391" />
                  <Point X="27.327044921875" Y="2.084005371094" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.410498046875" Y="2.071243652344" />
                  <Point X="27.42323828125" Y="2.070570068359" />
                  <Point X="27.45003125" Y="2.070955810547" />
                  <Point X="27.47320703125" Y="2.074170898438" />
                  <Point X="27.5443671875" Y="2.093200195312" />
                  <Point X="27.55127734375" Y="2.095333007812" />
                  <Point X="27.57248828125" Y="2.102775146484" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="27.7581796875" Y="2.208089111328" />
                  <Point X="28.967326171875" Y="2.906190673828" />
                  <Point X="29.09982421875" Y="2.722049560547" />
                  <Point X="29.123279296875" Y="2.689452392578" />
                  <Point X="29.26219921875" Y="2.459884277344" />
                  <Point X="29.23624609375" Y="2.439969238281" />
                  <Point X="28.23078515625" Y="1.668451538086" />
                  <Point X="28.221431640625" Y="1.660246704102" />
                  <Point X="28.20397265625" Y="1.641627319336" />
                  <Point X="28.152759765625" Y="1.574814819336" />
                  <Point X="28.1463125" Y="1.565250976562" />
                  <Point X="28.131275390625" Y="1.539733032227" />
                  <Point X="28.121630859375" Y="1.517090087891" />
                  <Point X="28.102552734375" Y="1.448874389648" />
                  <Point X="28.10010546875" Y="1.440125488281" />
                  <Point X="28.09665234375" Y="1.417827026367" />
                  <Point X="28.0958359375" Y="1.394257202148" />
                  <Point X="28.097740234375" Y="1.37176953125" />
                  <Point X="28.11340234375" Y="1.295870849609" />
                  <Point X="28.116486328125" Y="1.284525268555" />
                  <Point X="28.125693359375" Y="1.25741003418" />
                  <Point X="28.136283203125" Y="1.235740966797" />
                  <Point X="28.178876953125" Y="1.170998901367" />
                  <Point X="28.18433984375" Y="1.162695678711" />
                  <Point X="28.198892578125" Y="1.145452270508" />
                  <Point X="28.216134765625" Y="1.129362915039" />
                  <Point X="28.23434765625" Y="1.11603503418" />
                  <Point X="28.29607421875" Y="1.081288818359" />
                  <Point X="28.30710546875" Y="1.075983764648" />
                  <Point X="28.332998046875" Y="1.065529174805" />
                  <Point X="28.356119140625" Y="1.059438476562" />
                  <Point X="28.439576171875" Y="1.048408691406" />
                  <Point X="28.446369140625" Y="1.047758178711" />
                  <Point X="28.470150390625" Y="1.046340087891" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.64935546875" Y="1.068200439453" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.835865234375" Y="0.974176879883" />
                  <Point X="29.845939453125" Y="0.932790161133" />
                  <Point X="29.8908671875" Y="0.644238830566" />
                  <Point X="29.868861328125" Y="0.638342468262" />
                  <Point X="28.716580078125" Y="0.329589874268" />
                  <Point X="28.704787109375" Y="0.325584899902" />
                  <Point X="28.681546875" Y="0.315067657471" />
                  <Point X="28.599552734375" Y="0.267673522949" />
                  <Point X="28.59033203125" Y="0.261603118896" />
                  <Point X="28.565501953125" Y="0.243101852417" />
                  <Point X="28.547529296875" Y="0.225574462891" />
                  <Point X="28.49833203125" Y="0.162886703491" />
                  <Point X="28.4920234375" Y="0.154846817017" />
                  <Point X="28.480298828125" Y="0.135564727783" />
                  <Point X="28.47052734375" Y="0.11410521698" />
                  <Point X="28.463681640625" Y="0.092605018616" />
                  <Point X="28.447283203125" Y="0.006976568699" />
                  <Point X="28.44582421875" Y="-0.00420142746" />
                  <Point X="28.443720703125" Y="-0.033995754242" />
                  <Point X="28.4451796875" Y="-0.058554679871" />
                  <Point X="28.461578125" Y="-0.144182983398" />
                  <Point X="28.463681640625" Y="-0.155165161133" />
                  <Point X="28.47052734375" Y="-0.176665512085" />
                  <Point X="28.480298828125" Y="-0.19812487793" />
                  <Point X="28.4920234375" Y="-0.217406814575" />
                  <Point X="28.541220703125" Y="-0.280094573975" />
                  <Point X="28.54904296875" Y="-0.28888079834" />
                  <Point X="28.569666015625" Y="-0.309343505859" />
                  <Point X="28.589037109375" Y="-0.324155212402" />
                  <Point X="28.67103125" Y="-0.371549346924" />
                  <Point X="28.6767265625" Y="-0.374588348389" />
                  <Point X="28.699322265625" Y="-0.3856746521" />
                  <Point X="28.716580078125" Y="-0.392150024414" />
                  <Point X="28.86436328125" Y="-0.431748199463" />
                  <Point X="29.891474609375" Y="-0.706961975098" />
                  <Point X="29.8606484375" Y="-0.911428894043" />
                  <Point X="29.855025390625" Y="-0.948726074219" />
                  <Point X="29.801177734375" Y="-1.18469934082" />
                  <Point X="29.760703125" Y="-1.17937097168" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710327148" />
                  <Point X="28.37466015625" Y="-1.005508850098" />
                  <Point X="28.213734375" Y="-1.040486572266" />
                  <Point X="28.193095703125" Y="-1.044972412109" />
                  <Point X="28.1639765625" Y="-1.056596313477" />
                  <Point X="28.1361484375" Y="-1.073489013672" />
                  <Point X="28.1123984375" Y="-1.093960327148" />
                  <Point X="28.01512890625" Y="-1.210944824219" />
                  <Point X="28.002654296875" Y="-1.225948242188" />
                  <Point X="27.9879296875" Y="-1.250337036133" />
                  <Point X="27.976587890625" Y="-1.277724121094" />
                  <Point X="27.969759765625" Y="-1.305367431641" />
                  <Point X="27.955818359375" Y="-1.456867675781" />
                  <Point X="27.95403125" Y="-1.476297851562" />
                  <Point X="27.956349609375" Y="-1.507565307617" />
                  <Point X="27.964080078125" Y="-1.539183959961" />
                  <Point X="27.976451171875" Y="-1.567996704102" />
                  <Point X="28.065509765625" Y="-1.706521240234" />
                  <Point X="28.076931640625" Y="-1.724287231445" />
                  <Point X="28.0869375" Y="-1.737241943359" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.2477734375" Y="-1.866142578125" />
                  <Point X="29.213125" Y="-2.6068828125" />
                  <Point X="29.140662109375" Y="-2.724136962891" />
                  <Point X="29.1248125" Y="-2.749785400391" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="28.99112890625" Y="-2.864090576172" />
                  <Point X="27.800955078125" Y="-2.176942871094" />
                  <Point X="27.78612890625" Y="-2.170011474609" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.56269921875" Y="-2.125235595703" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.50678125" Y="-2.120395507812" />
                  <Point X="27.474609375" Y="-2.125353271484" />
                  <Point X="27.444833984375" Y="-2.135176757812" />
                  <Point X="27.28572265625" Y="-2.218916015625" />
                  <Point X="27.26531640625" Y="-2.229655761719" />
                  <Point X="27.242384765625" Y="-2.246548828125" />
                  <Point X="27.22142578125" Y="-2.2675078125" />
                  <Point X="27.204533203125" Y="-2.290437988281" />
                  <Point X="27.12079296875" Y="-2.449549560547" />
                  <Point X="27.110052734375" Y="-2.469956054688" />
                  <Point X="27.100228515625" Y="-2.499735839844" />
                  <Point X="27.095271484375" Y="-2.531911865234" />
                  <Point X="27.09567578125" Y="-2.563260986328" />
                  <Point X="27.130265625" Y="-2.754787597656" />
                  <Point X="27.134703125" Y="-2.779351318359" />
                  <Point X="27.13898828125" Y="-2.795146484375" />
                  <Point X="27.1518203125" Y="-2.826078125" />
                  <Point X="27.239947265625" Y="-2.978720458984" />
                  <Point X="27.86128515625" Y="-4.054905761719" />
                  <Point X="27.800662109375" Y="-4.098206542969" />
                  <Point X="27.78182421875" Y="-4.111661132813" />
                  <Point X="27.701767578125" Y="-4.163481933594" />
                  <Point X="27.666759765625" Y="-4.117860351563" />
                  <Point X="26.758548828125" Y="-2.934254882812" />
                  <Point X="26.74750390625" Y="-2.922177490234" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.53302734375" Y="-2.779113769531" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479990234375" Y="-2.751167236328" />
                  <Point X="26.448369140625" Y="-2.743435546875" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.210509765625" Y="-2.760127441406" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397949219" />
                  <Point X="26.128978515625" Y="-2.780741455078" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="25.945072265625" Y="-2.928100341797" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968861328125" />
                  <Point X="25.88725" Y="-2.996687744141" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.827927734375" Y="-3.245252197266" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627685547" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.844716796875" Y="-3.512822509766" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="25.99347265625" Y="-4.866183105469" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.968556640625" Y="-4.757874511719" />
                  <Point X="23.94157421875" Y="-4.752637207031" />
                  <Point X="23.85875390625" Y="-4.731328613281" />
                  <Point X="23.8792265625" Y="-4.57583984375" />
                  <Point X="23.879923828125" Y="-4.568099121094" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.509323730469" />
                  <Point X="23.876666015625" Y="-4.497688476562" />
                  <Point X="23.823017578125" Y="-4.227986816406" />
                  <Point X="23.81613671875" Y="-4.193396972656" />
                  <Point X="23.811875" Y="-4.178467773438" />
                  <Point X="23.80097265625" Y="-4.149500488281" />
                  <Point X="23.79433203125" Y="-4.135462402344" />
                  <Point X="23.778259765625" Y="-4.107625488281" />
                  <Point X="23.76942578125" Y="-4.094860595703" />
                  <Point X="23.74979296875" Y="-4.070937255859" />
                  <Point X="23.738994140625" Y="-4.059779052734" />
                  <Point X="23.53225" Y="-3.878468261719" />
                  <Point X="23.505734375" Y="-3.85521484375" />
                  <Point X="23.49326171875" Y="-3.845965332031" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.0887890625" Y="-3.778184570312" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.007267578125" Y="-3.779166503906" />
                  <Point X="22.991990234375" Y="-3.781946533203" />
                  <Point X="22.96094140625" Y="-3.790266845703" />
                  <Point X="22.946322265625" Y="-3.795497802734" />
                  <Point X="22.9181328125" Y="-3.808270751953" />
                  <Point X="22.9045625" Y="-3.815812744141" />
                  <Point X="22.675919921875" Y="-3.968586914062" />
                  <Point X="22.64659765625" Y="-3.988180664062" />
                  <Point X="22.640322265625" Y="-3.992755859375" />
                  <Point X="22.61955859375" Y="-4.010131347656" />
                  <Point X="22.597244140625" Y="-4.032768798828" />
                  <Point X="22.589533203125" Y="-4.041624511719" />
                  <Point X="22.563966796875" Y="-4.074940917969" />
                  <Point X="22.496796875" Y="-4.162477539063" />
                  <Point X="22.291970703125" Y="-4.035653808594" />
                  <Point X="22.252412109375" Y="-4.011160400391" />
                  <Point X="22.019138671875" Y="-3.831546630859" />
                  <Point X="22.65851171875" Y="-2.724118652344" />
                  <Point X="22.6651484375" Y="-2.710084960938" />
                  <Point X="22.67605078125" Y="-2.681119384766" />
                  <Point X="22.680314453125" Y="-2.666188964844" />
                  <Point X="22.6865859375" Y="-2.634662109375" />
                  <Point X="22.688361328125" Y="-2.619238769531" />
                  <Point X="22.689375" Y="-2.588304931641" />
                  <Point X="22.6853359375" Y="-2.557616943359" />
                  <Point X="22.6763515625" Y="-2.527999023438" />
                  <Point X="22.67064453125" Y="-2.51355859375" />
                  <Point X="22.656427734375" Y="-2.484729492188" />
                  <Point X="22.6484453125" Y="-2.471412353516" />
                  <Point X="22.63041796875" Y="-2.446254394531" />
                  <Point X="22.620373046875" Y="-2.434413574219" />
                  <Point X="22.605009765625" Y="-2.419050292969" />
                  <Point X="22.591234375" Y="-2.407063720703" />
                  <Point X="22.56608203125" Y="-2.389030273438" />
                  <Point X="22.552765625" Y="-2.381044921875" />
                  <Point X="22.523931640625" Y="-2.366816162109" />
                  <Point X="22.509490234375" Y="-2.361104736328" />
                  <Point X="22.4798671875" Y="-2.352111572266" />
                  <Point X="22.449173828125" Y="-2.348065429688" />
                  <Point X="22.418232421875" Y="-2.34907421875" />
                  <Point X="22.402802734375" Y="-2.350847412109" />
                  <Point X="22.371267578125" Y="-2.3571171875" />
                  <Point X="22.356333984375" Y="-2.361380859375" />
                  <Point X="22.327359375" Y="-2.372284667969" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="22.166748046875" Y="-2.463547119141" />
                  <Point X="21.2069140625" Y="-3.017707763672" />
                  <Point X="21.027234375" Y="-2.781646972656" />
                  <Point X="20.99597265625" Y="-2.740575195312" />
                  <Point X="20.818734375" Y="-2.443374267578" />
                  <Point X="21.951876953125" Y="-1.573883056641" />
                  <Point X="21.963517578125" Y="-1.563309936523" />
                  <Point X="21.984892578125" Y="-1.540392211914" />
                  <Point X="21.994626953125" Y="-1.528047363281" />
                  <Point X="22.012591796875" Y="-1.500918334961" />
                  <Point X="22.020158203125" Y="-1.48713684082" />
                  <Point X="22.032916015625" Y="-1.458508789062" />
                  <Point X="22.038107421875" Y="-1.443662353516" />
                  <Point X="22.0449375" Y="-1.417296264648" />
                  <Point X="22.048453125" Y="-1.398772705078" />
                  <Point X="22.05125390625" Y="-1.368330566406" />
                  <Point X="22.051421875" Y="-1.352998413086" />
                  <Point X="22.04920703125" Y="-1.321333007812" />
                  <Point X="22.04691015625" Y="-1.306182373047" />
                  <Point X="22.03990625" Y="-1.2764453125" />
                  <Point X="22.0282109375" Y="-1.248221801758" />
                  <Point X="22.012130859375" Y="-1.222245483398" />
                  <Point X="22.0030390625" Y="-1.20990637207" />
                  <Point X="21.982208984375" Y="-1.185955078125" />
                  <Point X="21.97125" Y="-1.17523815918" />
                  <Point X="21.947751953125" Y="-1.155707397461" />
                  <Point X="21.935212890625" Y="-1.146893554688" />
                  <Point X="21.911740234375" Y="-1.133078735352" />
                  <Point X="21.894572265625" Y="-1.12448449707" />
                  <Point X="21.86531640625" Y="-1.113262207031" />
                  <Point X="21.85022265625" Y="-1.108865112305" />
                  <Point X="21.81833203125" Y="-1.102380859375" />
                  <Point X="21.802712890625" Y="-1.100533691406" />
                  <Point X="21.7713828125" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="21.570640625" Y="-1.124556274414" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.271462890625" Y="-1.021970825195" />
                  <Point X="20.259240234375" Y="-0.974114624023" />
                  <Point X="20.213548828125" Y="-0.654654541016" />
                  <Point X="21.491712890625" Y="-0.312171478271" />
                  <Point X="21.49952734375" Y="-0.309711914062" />
                  <Point X="21.526068359375" Y="-0.299251251221" />
                  <Point X="21.555833984375" Y="-0.283892700195" />
                  <Point X="21.56644140625" Y="-0.277512786865" />
                  <Point X="21.5910078125" Y="-0.260462158203" />
                  <Point X="21.606298828125" Y="-0.248267654419" />
                  <Point X="21.628759765625" Y="-0.226384414673" />
                  <Point X="21.639080078125" Y="-0.2145103302" />
                  <Point X="21.658333984375" Y="-0.188248519897" />
                  <Point X="21.666556640625" Y="-0.174828323364" />
                  <Point X="21.680671875" Y="-0.146816772461" />
                  <Point X="21.686564453125" Y="-0.132225418091" />
                  <Point X="21.694763671875" Y="-0.105806655884" />
                  <Point X="21.699033203125" Y="-0.088412124634" />
                  <Point X="21.703287109375" Y="-0.060295497894" />
                  <Point X="21.70435546875" Y="-0.046077934265" />
                  <Point X="21.704353515625" Y="-0.016446987152" />
                  <Point X="21.703279296875" Y="-0.00220861578" />
                  <Point X="21.699009765625" Y="0.02594383049" />
                  <Point X="21.695814453125" Y="0.039858352661" />
                  <Point X="21.687615234375" Y="0.06627696228" />
                  <Point X="21.680673828125" Y="0.08425038147" />
                  <Point X="21.666576171875" Y="0.112231460571" />
                  <Point X="21.6583671875" Y="0.125636062622" />
                  <Point X="21.639123046875" Y="0.151899810791" />
                  <Point X="21.628798828125" Y="0.163783401489" />
                  <Point X="21.60633203125" Y="0.185680007935" />
                  <Point X="21.594189453125" Y="0.19569303894" />
                  <Point X="21.56958984375" Y="0.212765350342" />
                  <Point X="21.5639609375" Y="0.216385055542" />
                  <Point X="21.546591796875" Y="0.226434341431" />
                  <Point X="21.5278671875" Y="0.235846328735" />
                  <Point X="21.519005859375" Y="0.239748123169" />
                  <Point X="21.491712890625" Y="0.249611343384" />
                  <Point X="21.323048828125" Y="0.294804748535" />
                  <Point X="20.2145546875" Y="0.591824829102" />
                  <Point X="20.260794921875" Y="0.904309936523" />
                  <Point X="20.26866796875" Y="0.957524414062" />
                  <Point X="20.366416015625" Y="1.318237060547" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.2529453125" Y="1.204364379883" />
                  <Point X="21.263298828125" Y="1.204703369141" />
                  <Point X="21.284859375" Y="1.20658984375" />
                  <Point X="21.29511328125" Y="1.208054321289" />
                  <Point X="21.3153984375" Y="1.21208972168" />
                  <Point X="21.3254296875" Y="1.214660766602" />
                  <Point X="21.379873046875" Y="1.231826538086" />
                  <Point X="21.396548828125" Y="1.237676025391" />
                  <Point X="21.415482421875" Y="1.246006713867" />
                  <Point X="21.42472265625" Y="1.250689819336" />
                  <Point X="21.443466796875" Y="1.261511962891" />
                  <Point X="21.452140625" Y="1.267171875" />
                  <Point X="21.468822265625" Y="1.279403320312" />
                  <Point X="21.48407421875" Y="1.293378051758" />
                  <Point X="21.497712890625" Y="1.308930175781" />
                  <Point X="21.504107421875" Y="1.317079101562" />
                  <Point X="21.516521484375" Y="1.334808837891" />
                  <Point X="21.52198828125" Y="1.343600585938" />
                  <Point X="21.5319375" Y="1.361734008789" />
                  <Point X="21.536419921875" Y="1.371076049805" />
                  <Point X="21.558265625" Y="1.42381640625" />
                  <Point X="21.5645078125" Y="1.44036706543" />
                  <Point X="21.570298828125" Y="1.460243164062" />
                  <Point X="21.57265234375" Y="1.470338378906" />
                  <Point X="21.57640625" Y="1.491651489258" />
                  <Point X="21.57764453125" Y="1.501938354492" />
                  <Point X="21.5789921875" Y="1.522570800781" />
                  <Point X="21.578087890625" Y="1.543219482422" />
                  <Point X="21.57494140625" Y="1.563655273438" />
                  <Point X="21.57280859375" Y="1.573788085938" />
                  <Point X="21.56720703125" Y="1.594691894531" />
                  <Point X="21.563986328125" Y="1.604533081055" />
                  <Point X="21.556490234375" Y="1.623809692383" />
                  <Point X="21.55221484375" Y="1.633245239258" />
                  <Point X="21.52585546875" Y="1.683880981445" />
                  <Point X="21.517201171875" Y="1.699285644531" />
                  <Point X="21.505708984375" Y="1.716486083984" />
                  <Point X="21.499490234375" Y="1.724776000977" />
                  <Point X="21.485578125" Y="1.741354980469" />
                  <Point X="21.478494140625" Y="1.748916381836" />
                  <Point X="21.463552734375" Y="1.763218139648" />
                  <Point X="21.4556953125" Y="1.769958496094" />
                  <Point X="21.35894921875" Y="1.844194824219" />
                  <Point X="20.77238671875" Y="2.294280517578" />
                  <Point X="20.967111328125" Y="2.627889648438" />
                  <Point X="20.99771484375" Y="2.680321289062" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.74584375" Y="2.762398925781" />
                  <Point X="21.75508203125" Y="2.757717285156" />
                  <Point X="21.774015625" Y="2.749385986328" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.920751953125" Y="2.724524169922" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.023568359375" Y="2.734227783203" />
                  <Point X="22.0430078125" Y="2.741303466797" />
                  <Point X="22.061560546875" Y="2.750453369141" />
                  <Point X="22.070587890625" Y="2.755534667969" />
                  <Point X="22.088841796875" Y="2.767164550781" />
                  <Point X="22.097259765625" Y="2.773198242188" />
                  <Point X="22.11338671875" Y="2.786142578125" />
                  <Point X="22.121095703125" Y="2.793053222656" />
                  <Point X="22.17491796875" Y="2.846874023438" />
                  <Point X="22.188734375" Y="2.861488525391" />
                  <Point X="22.20168359375" Y="2.877622314453" />
                  <Point X="22.20771875" Y="2.886044189453" />
                  <Point X="22.21934765625" Y="2.904298583984" />
                  <Point X="22.2244296875" Y="2.913326416016" />
                  <Point X="22.233576171875" Y="2.931875244141" />
                  <Point X="22.240646484375" Y="2.951299804688" />
                  <Point X="22.245564453125" Y="2.971388671875" />
                  <Point X="22.2474765625" Y="2.981573974609" />
                  <Point X="22.250302734375" Y="3.003032714844" />
                  <Point X="22.251091796875" Y="3.013365722656" />
                  <Point X="22.25154296875" Y="3.034051269531" />
                  <Point X="22.251205078125" Y="3.044403808594" />
                  <Point X="22.2445703125" Y="3.120228515625" />
                  <Point X="22.242255859375" Y="3.140201416016" />
                  <Point X="22.238220703125" Y="3.160484863281" />
                  <Point X="22.235650390625" Y="3.170513427734" />
                  <Point X="22.229140625" Y="3.191161865234" />
                  <Point X="22.225494140625" Y="3.2008515625" />
                  <Point X="22.2171640625" Y="3.219787109375" />
                  <Point X="22.21248046875" Y="3.229032958984" />
                  <Point X="22.169609375" Y="3.303288085938" />
                  <Point X="21.9406171875" Y="3.699916748047" />
                  <Point X="22.29834375" Y="3.974183105469" />
                  <Point X="22.351634765625" Y="4.015042480469" />
                  <Point X="22.8074765625" Y="4.268296875" />
                  <Point X="22.881435546875" Y="4.171909179688" />
                  <Point X="22.8881796875" Y="4.164048828125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93490625" Y="4.121898925781" />
                  <Point X="22.95210546875" Y="4.11040625" />
                  <Point X="22.96101953125" Y="4.105128417969" />
                  <Point X="23.045412109375" Y="4.061196533203" />
                  <Point X="23.065673828125" Y="4.051285644531" />
                  <Point X="23.084953125" Y="4.0437890625" />
                  <Point X="23.094794921875" Y="4.040568359375" />
                  <Point X="23.115701171875" Y="4.034966308594" />
                  <Point X="23.1258359375" Y="4.032834960938" />
                  <Point X="23.146279296875" Y="4.029687988281" />
                  <Point X="23.166947265625" Y="4.028785400391" />
                  <Point X="23.187587890625" Y="4.030138427734" />
                  <Point X="23.197869140625" Y="4.031378417969" />
                  <Point X="23.21918359375" Y="4.03513671875" />
                  <Point X="23.22926953125" Y="4.037488037109" />
                  <Point X="23.24912890625" Y="4.043276123047" />
                  <Point X="23.25890234375" Y="4.046713134766" />
                  <Point X="23.346802734375" Y="4.083123291016" />
                  <Point X="23.367412109375" Y="4.092270751953" />
                  <Point X="23.38554296875" Y="4.102216796875" />
                  <Point X="23.394337890625" Y="4.107685058594" />
                  <Point X="23.412068359375" Y="4.120098632812" />
                  <Point X="23.42021484375" Y="4.1264921875" />
                  <Point X="23.435767578125" Y="4.140129882813" />
                  <Point X="23.449748046875" Y="4.155385253906" />
                  <Point X="23.461978515625" Y="4.172065917969" />
                  <Point X="23.467634765625" Y="4.180735351563" />
                  <Point X="23.47845703125" Y="4.199478027344" />
                  <Point X="23.483140625" Y="4.208715820313" />
                  <Point X="23.491474609375" Y="4.227653320312" />
                  <Point X="23.495125" Y="4.237353027344" />
                  <Point X="23.523734375" Y="4.328092285156" />
                  <Point X="23.529974609375" Y="4.349762207031" />
                  <Point X="23.534009765625" Y="4.370048339844" />
                  <Point X="23.535474609375" Y="4.380301757812" />
                  <Point X="23.537361328125" Y="4.401865722656" />
                  <Point X="23.53769921875" Y="4.412217773438" />
                  <Point X="23.537248046875" Y="4.432900390625" />
                  <Point X="23.536458984375" Y="4.443230957031" />
                  <Point X="23.531583984375" Y="4.480252441406" />
                  <Point X="23.520734375" Y="4.562655761719" />
                  <Point X="23.999845703125" Y="4.696981933594" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.77433203125" Y="4.261728027344" />
                  <Point X="24.779564453125" Y="4.247106933594" />
                  <Point X="24.79233984375" Y="4.218912597656" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.259947265625" Y="4.343708496094" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.767634765625" Y="4.744221191406" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.39220703125" Y="4.601665527344" />
                  <Point X="26.453595703125" Y="4.586844238281" />
                  <Point X="26.819783203125" Y="4.454025878906" />
                  <Point X="26.85825390625" Y="4.440071289062" />
                  <Point X="27.213447265625" Y="4.273960449219" />
                  <Point X="27.2504609375" Y="4.256650390625" />
                  <Point X="27.59362890625" Y="4.056719726562" />
                  <Point X="27.6294296875" Y="4.035862548828" />
                  <Point X="27.81778125" Y="3.901915771484" />
                  <Point X="27.065310546875" Y="2.59859765625" />
                  <Point X="27.062380859375" Y="2.593114013672" />
                  <Point X="27.05318359375" Y="2.573440429688" />
                  <Point X="27.044185546875" Y="2.549568847656" />
                  <Point X="27.0413046875" Y="2.540603515625" />
                  <Point X="27.022275390625" Y="2.469443603516" />
                  <Point X="27.017533203125" Y="2.445759521484" />
                  <Point X="27.013751953125" Y="2.417729003906" />
                  <Point X="27.012900390625" Y="2.405699462891" />
                  <Point X="27.01273046875" Y="2.381626220703" />
                  <Point X="27.013412109375" Y="2.369582519531" />
                  <Point X="27.02083203125" Y="2.308049560547" />
                  <Point X="27.02380078125" Y="2.289037353516" />
                  <Point X="27.029142578125" Y="2.267117919922" />
                  <Point X="27.032466796875" Y="2.256318847656" />
                  <Point X="27.040732421875" Y="2.234228759766" />
                  <Point X="27.04531640625" Y="2.223894775391" />
                  <Point X="27.055681640625" Y="2.203844482422" />
                  <Point X="27.061462890625" Y="2.194128173828" />
                  <Point X="27.0995390625" Y="2.138016113281" />
                  <Point X="27.114625" Y="2.118612792969" />
                  <Point X="27.132810546875" Y="2.09811328125" />
                  <Point X="27.1411875" Y="2.089777587891" />
                  <Point X="27.158912109375" Y="2.074211181641" />
                  <Point X="27.168259765625" Y="2.06698046875" />
                  <Point X="27.22437109375" Y="2.028905883789" />
                  <Point X="27.24128515625" Y="2.018242797852" />
                  <Point X="27.261333984375" Y="2.007878662109" />
                  <Point X="27.271666015625" Y="2.003295043945" />
                  <Point X="27.293751953125" Y="1.995030517578" />
                  <Point X="27.304552734375" Y="1.991706542969" />
                  <Point X="27.32647265625" Y="1.986364746094" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.399125" Y="1.976926879883" />
                  <Point X="27.42460546875" Y="1.975579956055" />
                  <Point X="27.4513984375" Y="1.975965820312" />
                  <Point X="27.4630859375" Y="1.976856933594" />
                  <Point X="27.48626171875" Y="1.980072021484" />
                  <Point X="27.49775" Y="1.982395751953" />
                  <Point X="27.56891015625" Y="2.001425048828" />
                  <Point X="27.58273046875" Y="2.005690673828" />
                  <Point X="27.60394140625" Y="2.0131328125" />
                  <Point X="27.61213671875" Y="2.016444702148" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="27.8056796875" Y="2.125816650391" />
                  <Point X="28.940404296875" Y="2.780950683594" />
                  <Point X="29.0227109375" Y="2.666563720703" />
                  <Point X="29.043962890625" Y="2.637028320312" />
                  <Point X="29.136884765625" Y="2.483472412109" />
                  <Point X="28.172951171875" Y="1.743819335938" />
                  <Point X="28.168138671875" Y="1.739868652344" />
                  <Point X="28.152130859375" Y="1.725227661133" />
                  <Point X="28.134671875" Y="1.706608276367" />
                  <Point X="28.12857421875" Y="1.699421142578" />
                  <Point X="28.077361328125" Y="1.632608642578" />
                  <Point X="28.064466796875" Y="1.613481201172" />
                  <Point X="28.0494296875" Y="1.587963256836" />
                  <Point X="28.043873046875" Y="1.5769609375" />
                  <Point X="28.034228515625" Y="1.554317993164" />
                  <Point X="28.030140625" Y="1.542677246094" />
                  <Point X="28.0110625" Y="1.474461547852" />
                  <Point X="28.006224609375" Y="1.454663818359" />
                  <Point X="28.002771484375" Y="1.432365356445" />
                  <Point X="28.001708984375" Y="1.421115600586" />
                  <Point X="28.000892578125" Y="1.397545776367" />
                  <Point X="28.001173828125" Y="1.386241088867" />
                  <Point X="28.003078125" Y="1.363753417969" />
                  <Point X="28.004701171875" Y="1.3525703125" />
                  <Point X="28.02036328125" Y="1.276671630859" />
                  <Point X="28.02653125" Y="1.25398059082" />
                  <Point X="28.03573828125" Y="1.226865356445" />
                  <Point X="28.04033984375" Y="1.215697509766" />
                  <Point X="28.0509296875" Y="1.194028442383" />
                  <Point X="28.05691796875" Y="1.183526977539" />
                  <Point X="28.09951171875" Y="1.118784912109" />
                  <Point X="28.111740234375" Y="1.101424072266" />
                  <Point X="28.12629296875" Y="1.084180664062" />
                  <Point X="28.134080078125" Y="1.075995239258" />
                  <Point X="28.151322265625" Y="1.059905883789" />
                  <Point X="28.160033203125" Y="1.052697753906" />
                  <Point X="28.17824609375" Y="1.039369750977" />
                  <Point X="28.187748046875" Y="1.033249755859" />
                  <Point X="28.249474609375" Y="0.998503417969" />
                  <Point X="28.271537109375" Y="0.987893371582" />
                  <Point X="28.2974296875" Y="0.977438781738" />
                  <Point X="28.308798828125" Y="0.973663146973" />
                  <Point X="28.331919921875" Y="0.96757244873" />
                  <Point X="28.343671875" Y="0.965257507324" />
                  <Point X="28.42712890625" Y="0.954227600098" />
                  <Point X="28.44071484375" Y="0.952926574707" />
                  <Point X="28.46449609375" Y="0.951508544922" />
                  <Point X="28.4735390625" Y="0.951400512695" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="28.661755859375" Y="0.974013183594" />
                  <Point X="29.704703125" Y="1.111319824219" />
                  <Point X="29.743560546875" Y="0.951705932617" />
                  <Point X="29.7526875" Y="0.914210144043" />
                  <Point X="29.783873046875" Y="0.713921142578" />
                  <Point X="28.6919921875" Y="0.421352966309" />
                  <Point X="28.68603125" Y="0.4195440979" />
                  <Point X="28.665619140625" Y="0.412134796143" />
                  <Point X="28.64237890625" Y="0.401617645264" />
                  <Point X="28.634005859375" Y="0.397316192627" />
                  <Point X="28.55201171875" Y="0.349922149658" />
                  <Point X="28.5335703125" Y="0.337781433105" />
                  <Point X="28.508740234375" Y="0.31928012085" />
                  <Point X="28.499173828125" Y="0.31111416626" />
                  <Point X="28.481201171875" Y="0.293586761475" />
                  <Point X="28.472794921875" Y="0.284225158691" />
                  <Point X="28.42359765625" Y="0.221537368774" />
                  <Point X="28.4108515625" Y="0.204203918457" />
                  <Point X="28.399126953125" Y="0.184921890259" />
                  <Point X="28.39383984375" Y="0.174933242798" />
                  <Point X="28.384068359375" Y="0.153473724365" />
                  <Point X="28.380005859375" Y="0.142927597046" />
                  <Point X="28.37316015625" Y="0.121427505493" />
                  <Point X="28.370376953125" Y="0.110473403931" />
                  <Point X="28.353978515625" Y="0.024845062256" />
                  <Point X="28.351060546875" Y="0.002489056826" />
                  <Point X="28.34895703125" Y="-0.02730522728" />
                  <Point X="28.348888671875" Y="-0.039629486084" />
                  <Point X="28.35034765625" Y="-0.064188529968" />
                  <Point X="28.351875" Y="-0.076423164368" />
                  <Point X="28.3682734375" Y="-0.162051498413" />
                  <Point X="28.37316015625" Y="-0.183987350464" />
                  <Point X="28.380005859375" Y="-0.205487731934" />
                  <Point X="28.384068359375" Y="-0.216034317017" />
                  <Point X="28.39383984375" Y="-0.237493682861" />
                  <Point X="28.399126953125" Y="-0.247482330322" />
                  <Point X="28.4108515625" Y="-0.266764221191" />
                  <Point X="28.4172890625" Y="-0.276057617188" />
                  <Point X="28.466486328125" Y="-0.338745269775" />
                  <Point X="28.482130859375" Y="-0.3563175354" />
                  <Point X="28.50275390625" Y="-0.37678024292" />
                  <Point X="28.5119609375" Y="-0.384810333252" />
                  <Point X="28.53133203125" Y="-0.399621948242" />
                  <Point X="28.54149609375" Y="-0.406403747559" />
                  <Point X="28.623490234375" Y="-0.453797973633" />
                  <Point X="28.634880859375" Y="-0.45987588501" />
                  <Point X="28.6574765625" Y="-0.470962280273" />
                  <Point X="28.66594921875" Y="-0.474619598389" />
                  <Point X="28.6919921875" Y="-0.483912963867" />
                  <Point X="28.839775390625" Y="-0.523511169434" />
                  <Point X="29.784880859375" Y="-0.776751342773" />
                  <Point X="29.7667109375" Y="-0.897266418457" />
                  <Point X="29.761619140625" Y="-0.931040161133" />
                  <Point X="29.7278046875" Y="-1.079219970703" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428623046875" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042602539" />
                  <Point X="28.36672265625" Y="-0.910841003418" />
                  <Point X="28.354482421875" Y="-0.912676391602" />
                  <Point X="28.193556640625" Y="-0.947654052734" />
                  <Point X="28.17291796875" Y="-0.952139892578" />
                  <Point X="28.157875" Y="-0.95674230957" />
                  <Point X="28.128755859375" Y="-0.968366271973" />
                  <Point X="28.1146796875" Y="-0.975387634277" />
                  <Point X="28.0868515625" Y="-0.992280456543" />
                  <Point X="28.074125" Y="-1.001530883789" />
                  <Point X="28.050375" Y="-1.022002197266" />
                  <Point X="28.0393515625" Y="-1.033223022461" />
                  <Point X="27.94208203125" Y="-1.150207519531" />
                  <Point X="27.921326171875" Y="-1.17684753418" />
                  <Point X="27.9066015625" Y="-1.201236206055" />
                  <Point X="27.900158203125" Y="-1.21398840332" />
                  <Point X="27.88881640625" Y="-1.241375488281" />
                  <Point X="27.884359375" Y="-1.254942993164" />
                  <Point X="27.87753125" Y="-1.282586303711" />
                  <Point X="27.87516015625" Y="-1.296662109375" />
                  <Point X="27.86121875" Y="-1.448162353516" />
                  <Point X="27.859291015625" Y="-1.483322265625" />
                  <Point X="27.861609375" Y="-1.51458996582" />
                  <Point X="27.864068359375" Y="-1.530127441406" />
                  <Point X="27.871798828125" Y="-1.56174597168" />
                  <Point X="27.876787109375" Y="-1.576664550781" />
                  <Point X="27.889158203125" Y="-1.605477294922" />
                  <Point X="27.896541015625" Y="-1.619371582031" />
                  <Point X="27.985599609375" Y="-1.757896118164" />
                  <Point X="27.997021484375" Y="-1.775662109375" />
                  <Point X="28.00174609375" Y="-1.782358032227" />
                  <Point X="28.019798828125" Y="-1.804454101562" />
                  <Point X="28.0434921875" Y="-1.828121337891" />
                  <Point X="28.052798828125" Y="-1.836277587891" />
                  <Point X="28.18994140625" Y="-1.941511108398" />
                  <Point X="29.087173828125" Y="-2.629981201172" />
                  <Point X="29.059849609375" Y="-2.674194580078" />
                  <Point X="29.0454921875" Y="-2.697428222656" />
                  <Point X="29.001275390625" Y="-2.760252441406" />
                  <Point X="27.848455078125" Y="-2.094670410156" />
                  <Point X="27.841189453125" Y="-2.090883300781" />
                  <Point X="27.8150234375" Y="-2.079512207031" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771109375" Y="-2.066337646484" />
                  <Point X="27.579583984375" Y="-2.031747924805" />
                  <Point X="27.55501953125" Y="-2.027311889648" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.508005859375" Y="-2.025403442383" />
                  <Point X="27.4923125" Y="-2.02650378418" />
                  <Point X="27.460140625" Y="-2.031461547852" />
                  <Point X="27.444845703125" Y="-2.035136474609" />
                  <Point X="27.4150703125" Y="-2.044959838867" />
                  <Point X="27.40058984375" Y="-2.051108642578" />
                  <Point X="27.241478515625" Y="-2.134847900391" />
                  <Point X="27.221072265625" Y="-2.145587646484" />
                  <Point X="27.208970703125" Y="-2.153169189453" />
                  <Point X="27.1860390625" Y="-2.170062255859" />
                  <Point X="27.175208984375" Y="-2.179373779297" />
                  <Point X="27.15425" Y="-2.200332763672" />
                  <Point X="27.144939453125" Y="-2.211161132813" />
                  <Point X="27.128046875" Y="-2.234091308594" />
                  <Point X="27.12046484375" Y="-2.246193115234" />
                  <Point X="27.036724609375" Y="-2.4053046875" />
                  <Point X="27.025984375" Y="-2.425711181641" />
                  <Point X="27.0198359375" Y="-2.440193603516" />
                  <Point X="27.01001171875" Y="-2.469973388672" />
                  <Point X="27.0063359375" Y="-2.485270751953" />
                  <Point X="27.00137890625" Y="-2.517446777344" />
                  <Point X="27.000279296875" Y="-2.533136962891" />
                  <Point X="27.00068359375" Y="-2.564486083984" />
                  <Point X="27.0021875" Y="-2.580145019531" />
                  <Point X="27.03677734375" Y="-2.771671630859" />
                  <Point X="27.04121484375" Y="-2.796235351563" />
                  <Point X="27.043017578125" Y="-2.804225341797" />
                  <Point X="27.051240234375" Y="-2.831549072266" />
                  <Point X="27.064072265625" Y="-2.862480712891" />
                  <Point X="27.069546875" Y="-2.873577636719" />
                  <Point X="27.157673828125" Y="-3.026219970703" />
                  <Point X="27.73589453125" Y="-4.027723144531" />
                  <Point X="27.72375390625" Y="-4.036083740234" />
                  <Point X="26.833916015625" Y="-2.876421386719" />
                  <Point X="26.828654296875" Y="-2.870143066406" />
                  <Point X="26.808830078125" Y="-2.849624023438" />
                  <Point X="26.783251953125" Y="-2.828003662109" />
                  <Point X="26.77330078125" Y="-2.820646728516" />
                  <Point X="26.58440234375" Y="-2.699203369141" />
                  <Point X="26.56017578125" Y="-2.683627929688" />
                  <Point X="26.546283203125" Y="-2.676245605469" />
                  <Point X="26.51747265625" Y="-2.663874511719" />
                  <Point X="26.5025546875" Y="-2.658885742188" />
                  <Point X="26.47093359375" Y="-2.651154052734" />
                  <Point X="26.45539453125" Y="-2.648695800781" />
                  <Point X="26.424125" Y="-2.646376953125" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.2018046875" Y="-2.665527099609" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.161224609375" Y="-2.670339355469" />
                  <Point X="26.13357421875" Y="-2.677171630859" />
                  <Point X="26.1200078125" Y="-2.681629882812" />
                  <Point X="26.092623046875" Y="-2.692973388672" />
                  <Point X="26.079876953125" Y="-2.6994140625" />
                  <Point X="26.05549609375" Y="-2.714133789062" />
                  <Point X="26.043861328125" Y="-2.722412841797" />
                  <Point X="25.8843359375" Y="-2.855052001953" />
                  <Point X="25.863876953125" Y="-2.872063232422" />
                  <Point X="25.852654296875" Y="-2.883087890625" />
                  <Point X="25.83218359375" Y="-2.906837646484" />
                  <Point X="25.822935546875" Y="-2.919562744141" />
                  <Point X="25.80604296875" Y="-2.947389160156" />
                  <Point X="25.79901953125" Y="-2.961466552734" />
                  <Point X="25.78739453125" Y="-2.990587402344" />
                  <Point X="25.78279296875" Y="-3.005630859375" />
                  <Point X="25.735095703125" Y="-3.225074462891" />
                  <Point X="25.728978515625" Y="-3.25321875" />
                  <Point X="25.7275859375" Y="-3.261287353516" />
                  <Point X="25.724724609375" Y="-3.289677490234" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520019531" />
                  <Point X="25.750529296875" Y="-3.525222412109" />
                  <Point X="25.833087890625" Y="-4.152317382812" />
                  <Point X="25.655068359375" Y="-3.487938476562" />
                  <Point X="25.652607421875" Y="-3.480122314453" />
                  <Point X="25.642146484375" Y="-3.453578613281" />
                  <Point X="25.6267890625" Y="-3.423815429688" />
                  <Point X="25.62041015625" Y="-3.413209716797" />
                  <Point X="25.47529296875" Y="-3.204124755859" />
                  <Point X="25.456681640625" Y="-3.177309326172" />
                  <Point X="25.446669921875" Y="-3.165171386719" />
                  <Point X="25.42478515625" Y="-3.142716064453" />
                  <Point X="25.412912109375" Y="-3.132398681641" />
                  <Point X="25.38665625" Y="-3.113154541016" />
                  <Point X="25.373244140625" Y="-3.104937988281" />
                  <Point X="25.345244140625" Y="-3.090829833984" />
                  <Point X="25.33065625" Y="-3.084938232422" />
                  <Point X="25.10609765625" Y="-3.015243408203" />
                  <Point X="25.077296875" Y="-3.006304931641" />
                  <Point X="25.063376953125" Y="-3.003108886719" />
                  <Point X="25.035216796875" Y="-2.998839599609" />
                  <Point X="25.0209765625" Y="-2.997766357422" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.71045703125" Y="-3.075999755859" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.667068359375" Y="-3.090830078125" />
                  <Point X="24.639068359375" Y="-3.104938232422" />
                  <Point X="24.62565625" Y="-3.113154785156" />
                  <Point X="24.599400390625" Y="-3.132398925781" />
                  <Point X="24.587525390625" Y="-3.142717041016" />
                  <Point X="24.565640625" Y="-3.165172851562" />
                  <Point X="24.555630859375" Y="-3.177310546875" />
                  <Point X="24.410515625" Y="-3.386395263672" />
                  <Point X="24.391904296875" Y="-3.4132109375" />
                  <Point X="24.38753125" Y="-3.420131591797" />
                  <Point X="24.374025390625" Y="-3.445261230469" />
                  <Point X="24.36122265625" Y="-3.476214111328" />
                  <Point X="24.35724609375" Y="-3.487937011719" />
                  <Point X="24.3117109375" Y="-3.657877197266" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.667063811046" Y="-3.96220357631" />
                  <Point X="27.686718773783" Y="-3.942548613573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.91742567074" Y="-2.711841716616" />
                  <Point X="29.037444675839" Y="-2.591822711517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.608732528774" Y="-3.886184570156" />
                  <Point X="27.637543016315" Y="-3.857374082615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.832251043673" Y="-2.662666055258" />
                  <Point X="28.961425729315" Y="-2.533491369615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.550401246502" Y="-3.810165564003" />
                  <Point X="27.588367258848" Y="-3.772199551657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.747076416605" Y="-2.6134903939" />
                  <Point X="28.885406782791" Y="-2.475160027714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.49206996423" Y="-3.734146557849" />
                  <Point X="27.539191501381" Y="-3.687025020699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.661901789538" Y="-2.564314732541" />
                  <Point X="28.809387836268" Y="-2.416828685812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.433738681958" Y="-3.658127551696" />
                  <Point X="27.490015743913" Y="-3.601850489741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.576727162471" Y="-2.515139071183" />
                  <Point X="28.733368889744" Y="-2.35849734391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.375407399686" Y="-3.582108545542" />
                  <Point X="27.440839986446" Y="-3.516675958783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.491552535404" Y="-2.465963409825" />
                  <Point X="28.65734994322" Y="-2.300166002008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.317076117414" Y="-3.506089539389" />
                  <Point X="27.391664228979" Y="-3.431501427824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.406377908336" Y="-2.416787748467" />
                  <Point X="28.581330996696" Y="-2.241834660107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.258744835142" Y="-3.430070533235" />
                  <Point X="27.342488471511" Y="-3.346326896866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.321203281269" Y="-2.367612087109" />
                  <Point X="28.505312050173" Y="-2.183503318205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.623347465355" Y="-1.065467903023" />
                  <Point X="29.762301539515" Y="-0.926513828863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.20041355287" Y="-3.354051527082" />
                  <Point X="27.293312714044" Y="-3.261152365908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.236028654202" Y="-2.31843642575" />
                  <Point X="28.429293103649" Y="-2.125171976303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.504627033618" Y="-1.049838046334" />
                  <Point X="29.779228327964" Y="-0.775236751988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.142082270598" Y="-3.278032520928" />
                  <Point X="27.244136956577" Y="-3.17597783495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.150854027135" Y="-2.269260764392" />
                  <Point X="28.353274157125" Y="-2.066840634401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.385906601882" Y="-1.034208189645" />
                  <Point X="29.673269589634" Y="-0.746845201892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.083750988327" Y="-3.202013514775" />
                  <Point X="27.194961199109" Y="-3.090803303992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.065679400067" Y="-2.220085103034" />
                  <Point X="28.277255210602" Y="-2.0085092925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.267186170145" Y="-1.018578332956" />
                  <Point X="29.567310851304" Y="-0.718453651797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.025419706055" Y="-3.125994508621" />
                  <Point X="27.145785572673" Y="-3.005628642003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.980504773" Y="-2.170909441676" />
                  <Point X="28.201236264078" Y="-1.950177950598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.148465738408" Y="-1.002948476268" />
                  <Point X="29.461352112974" Y="-0.690062101701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.967088423783" Y="-3.049975502468" />
                  <Point X="27.096610357211" Y="-2.92045356904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.895330145933" Y="-2.121733780318" />
                  <Point X="28.125217425587" Y="-1.891846500663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.029745306672" Y="-0.987318619579" />
                  <Point X="29.355393374644" Y="-0.661670551606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.811386619562" Y="-4.071327018263" />
                  <Point X="25.821141107785" Y="-4.06157253004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.908757141511" Y="-2.973956496314" />
                  <Point X="27.051222730524" Y="-2.831490907301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.806062457492" Y="-2.076651180333" />
                  <Point X="28.049407860887" Y="-1.833305776938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.911024874935" Y="-0.97168876289" />
                  <Point X="29.249434636315" Y="-0.63327900151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.782995095666" Y="-3.965368253734" />
                  <Point X="25.805511268892" Y="-3.942852080508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.850425859239" Y="-2.897937490161" />
                  <Point X="27.027585830523" Y="-2.720777518876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.695653184034" Y="-2.052710165366" />
                  <Point X="27.987504420369" Y="-1.760858929031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.792304443199" Y="-0.956058906201" />
                  <Point X="29.143475897985" Y="-0.604887451415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.754603571769" Y="-3.859409489205" />
                  <Point X="25.789881429999" Y="-3.824131630975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.784746286891" Y="-2.829266774083" />
                  <Point X="27.007033785858" Y="-2.606979275116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.581854989791" Y="-2.032158071183" />
                  <Point X="27.934930017981" Y="-1.679083042993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.673584011462" Y="-0.940429049512" />
                  <Point X="29.037517159655" Y="-0.576495901319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.726212047872" Y="-3.753450724676" />
                  <Point X="25.774251591106" Y="-3.705411181443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.703738097436" Y="-2.775924675113" />
                  <Point X="27.010170405717" Y="-2.469492366832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.444369039051" Y="-2.035293733498" />
                  <Point X="27.88466058949" Y="-1.595002183059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.554863579726" Y="-0.924799192823" />
                  <Point X="28.931558421325" Y="-0.548104351224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.697820523976" Y="-3.647491960148" />
                  <Point X="25.758621752213" Y="-3.58669073191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.621961940254" Y="-2.723350543869" />
                  <Point X="27.859477334662" Y="-1.485835149461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.436117462049" Y="-0.909195022074" />
                  <Point X="28.82559966992" Y="-0.519712814203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.669429000079" Y="-3.541533195619" />
                  <Point X="25.742991960452" Y="-3.467970235245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.538191236949" Y="-2.672770958749" />
                  <Point X="27.871193376114" Y="-1.339768819584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.28267916978" Y="-0.928283025918" />
                  <Point X="28.71964083386" Y="-0.491321361838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.635641045783" Y="-3.440970861489" />
                  <Point X="25.727362219296" Y="-3.349249687977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.429813140017" Y="-2.646798767256" />
                  <Point X="28.019432255902" Y="-1.05717965137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.0776251206" Y="-0.998986786672" />
                  <Point X="28.623061658769" Y="-0.453550248503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.582980533186" Y="-3.359281085661" />
                  <Point X="25.740069251567" Y="-3.20219236728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.284328497999" Y="-2.657933120848" />
                  <Point X="28.538114309683" Y="-0.404147309164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.609444964401" Y="0.667183345554" />
                  <Point X="29.766669896849" Y="0.824408278002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.527937004845" Y="-3.279974325577" />
                  <Point X="25.77738091406" Y="-3.030530416361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.129352261417" Y="-2.678559069005" />
                  <Point X="28.467748435345" Y="-0.340162895076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.425919153434" Y="0.618007823013" />
                  <Point X="29.746703838977" Y="0.938792508556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.92514966096" Y="-4.748411381036" />
                  <Point X="24.054036536835" Y="-4.619524505162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.472893487759" Y="-3.200667554237" />
                  <Point X="28.409318327142" Y="-0.264242714854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.242393342467" Y="0.568832300471" />
                  <Point X="29.72040060924" Y="1.046839567244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.866467653536" Y="-4.672743100035" />
                  <Point X="24.10321261974" Y="-4.43599813383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.409392071455" Y="-3.129818682115" />
                  <Point X="28.36989231632" Y="-0.169318437251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.0588675315" Y="0.519656777929" />
                  <Point X="29.64231733043" Y="1.10310657686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.879415961513" Y="-4.525444503632" />
                  <Point X="24.152388702646" Y="-4.252471762499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.322464617625" Y="-3.08239584752" />
                  <Point X="28.349805081474" Y="-0.055055383671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.875341720533" Y="0.470481255388" />
                  <Point X="29.487597829028" Y="1.082737363883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.859436748312" Y="-4.411073428407" />
                  <Point X="24.201564785551" Y="-4.068945391168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.219935606928" Y="-3.050574569792" />
                  <Point X="28.367864568908" Y="0.097354392189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.691806924198" Y="0.421296747478" />
                  <Point X="29.332878327627" Y="1.062368150907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.837146100795" Y="-4.2990137875" />
                  <Point X="24.250740868457" Y="-3.885419019837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.11740659623" Y="-3.018753292064" />
                  <Point X="29.178158826225" Y="1.04199893793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.814421433125" Y="-4.187388166744" />
                  <Point X="24.299916951363" Y="-3.701892648506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.004043102971" Y="-2.997766496898" />
                  <Point X="29.023439324823" Y="1.021629724954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.770723517876" Y="-4.096735793567" />
                  <Point X="24.349092480303" Y="-3.51836683114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.82791371162" Y="-3.039545599824" />
                  <Point X="28.868719823421" Y="1.001260511978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.704010221184" Y="-4.029098801834" />
                  <Point X="24.555250619522" Y="-3.177858403495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.604304676414" Y="-3.128804346604" />
                  <Point X="28.714000322019" Y="0.980891299001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.632432378311" Y="-3.966326356282" />
                  <Point X="28.559280757196" Y="0.960522022604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.560854535437" Y="-3.90355391073" />
                  <Point X="28.41962745798" Y="0.955219011813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.487608335613" Y="-3.842449822128" />
                  <Point X="28.304987145469" Y="0.974928987728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.393721522716" Y="-3.8019863466" />
                  <Point X="28.214115302345" Y="1.018407433029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.271215978106" Y="-3.790141602784" />
                  <Point X="28.135773046004" Y="1.074415465113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.145129904793" Y="-3.781877387672" />
                  <Point X="28.078194364913" Y="1.151187072448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.014170458589" Y="-3.778486545451" />
                  <Point X="28.031627841368" Y="1.238970837328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.496199226796" Y="-4.162107488819" />
                  <Point X="22.499988370512" Y="-4.158318345102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.717508247528" Y="-3.940798468086" />
                  <Point X="28.005757594988" Y="1.347450879374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.413224815906" Y="-4.110731611283" />
                  <Point X="28.01597185575" Y="1.492015428561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.580497088069" Y="2.05654066088" />
                  <Point X="29.088080012321" Y="2.564123585132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.330250405017" Y="-4.059355733747" />
                  <Point X="29.036711813545" Y="2.647105674782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.247713354606" Y="-4.007542495732" />
                  <Point X="28.980492841472" Y="2.725236991134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.171807990922" Y="-3.949097570991" />
                  <Point X="28.849198537828" Y="2.728292975916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.095902627238" Y="-3.890652646249" />
                  <Point X="28.531322027889" Y="2.544766754402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.019997263554" Y="-3.832207721508" />
                  <Point X="28.213445517951" Y="2.361240532889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.200588120643" Y="-3.517266575994" />
                  <Point X="27.895569008012" Y="2.177714311376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.384113487971" Y="-3.19939092024" />
                  <Point X="27.592689253837" Y="2.009184845626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.567638855299" Y="-2.881515264486" />
                  <Point X="27.424735955065" Y="1.975581835279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.68742423193" Y="-2.62737959943" />
                  <Point X="27.306126784611" Y="1.991322953252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.669406154342" Y="-2.511047388592" />
                  <Point X="27.215427881898" Y="2.034974338963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.616031363583" Y="-2.430071890926" />
                  <Point X="27.138527620728" Y="2.092424366219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.537996293584" Y="-2.373756672499" />
                  <Point X="27.079419240911" Y="2.167666274828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.428668715548" Y="-2.348733962111" />
                  <Point X="27.03280844969" Y="2.255405772032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.196931914837" Y="-2.446120474396" />
                  <Point X="27.013370478155" Y="2.370318088922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.879055598818" Y="-2.62964650199" />
                  <Point X="27.038384099449" Y="2.529681998642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.561179219041" Y="-2.813172593341" />
                  <Point X="27.191477982646" Y="2.817126170264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.243302839265" Y="-2.996698684692" />
                  <Point X="27.375003681005" Y="3.135002157049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.155495931111" Y="-2.95015530442" />
                  <Point X="27.558529379364" Y="3.452878143833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.097430818168" Y="-2.873870128937" />
                  <Point X="27.742055077723" Y="3.770754130617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.039365705226" Y="-2.797584953454" />
                  <Point X="27.77166332824" Y="3.93471266956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.983290750695" Y="-2.719309619559" />
                  <Point X="27.693148847651" Y="3.990548477397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.933100939746" Y="-2.635149142083" />
                  <Point X="27.613432457276" Y="4.045182375446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.882911128796" Y="-2.550988664607" />
                  <Point X="21.557072543401" Y="-1.876827250003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.04980339565" Y="-1.384096397754" />
                  <Point X="27.528540320646" Y="4.094640527242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.832721317847" Y="-2.466828187132" />
                  <Point X="20.979650799595" Y="-2.319898705383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.034983627102" Y="-1.264565877876" />
                  <Point X="27.443648345608" Y="4.14409884063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.980710006458" Y="-1.184489210095" />
                  <Point X="27.35875637057" Y="4.193557154017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.902430580225" Y="-1.128418347903" />
                  <Point X="27.273864395532" Y="4.243015467404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.796192281423" Y="-1.100306358279" />
                  <Point X="27.184157045213" Y="4.287658405511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.647742725558" Y="-1.114405625719" />
                  <Point X="27.092616769208" Y="4.330468417932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.493023257087" Y="-1.134774805764" />
                  <Point X="27.001076493203" Y="4.373278430352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.338303735243" Y="-1.155144039182" />
                  <Point X="26.909536217199" Y="4.416088442773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.183584213399" Y="-1.175513272601" />
                  <Point X="26.814895986949" Y="4.455798500949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.028864691555" Y="-1.19588250602" />
                  <Point X="26.716305168187" Y="4.491557970613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.874145169711" Y="-1.216251739439" />
                  <Point X="26.617714349425" Y="4.527317440276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.719425647866" Y="-1.236620972858" />
                  <Point X="26.519123530664" Y="4.56307690994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.564706126022" Y="-1.256990206276" />
                  <Point X="21.520088049913" Y="-0.301608282385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.684595607334" Y="-0.137100724964" />
                  <Point X="26.417302878485" Y="4.595606546187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.409986604178" Y="-1.277359439695" />
                  <Point X="21.332518572275" Y="-0.354827471598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.700890260745" Y="0.013544216872" />
                  <Point X="26.309080849439" Y="4.621734805566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.324273953042" Y="-1.228721802406" />
                  <Point X="21.148992658532" Y="-0.404003096915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.666063829438" Y="0.11306807399" />
                  <Point X="26.200858777475" Y="4.647863022027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.29693874841" Y="-1.121706718612" />
                  <Point X="20.96546674479" Y="-0.453178722232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.605232320273" Y="0.186586853251" />
                  <Point X="26.092636705511" Y="4.673991238489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.269603714138" Y="-1.014691464459" />
                  <Point X="20.781940831047" Y="-0.50235434755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.522503313053" Y="0.238208134456" />
                  <Point X="25.984414633547" Y="4.700119454951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.24880314496" Y="-0.901141745212" />
                  <Point X="20.598414917304" Y="-0.551529972867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.419031228682" Y="0.269086338511" />
                  <Point X="25.876192561584" Y="4.726247671412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.231991893222" Y="-0.783602708524" />
                  <Point X="20.414889003561" Y="-0.600705598184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.313072499276" Y="0.297477897531" />
                  <Point X="25.145274354113" Y="4.129679752367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.195300280647" Y="4.179705678901" />
                  <Point X="25.760557025893" Y="4.744962424147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.215180641484" Y="-0.666063671836" />
                  <Point X="20.231363089819" Y="-0.649881223502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.207113765266" Y="0.325869451946" />
                  <Point X="24.969804331479" Y="4.088560018159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.272756227811" Y="4.39151191449" />
                  <Point X="25.638943044295" Y="4.757698730975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.101155031256" Y="0.354261006361" />
                  <Point X="24.872433798454" Y="4.125539773559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.321932238825" Y="4.575038213931" />
                  <Point X="25.517329062698" Y="4.770435037803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.995196297245" Y="0.382652560776" />
                  <Point X="24.807093212887" Y="4.194549476417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.37110824984" Y="4.758564513371" />
                  <Point X="25.395715081101" Y="4.783171344632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.889237563235" Y="0.411044115191" />
                  <Point X="24.767060262923" Y="4.288866814879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.783278829225" Y="0.439435669606" />
                  <Point X="24.73866885373" Y="4.394825694112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.677320095215" Y="0.467827224022" />
                  <Point X="24.710277444537" Y="4.500784573344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.571361361204" Y="0.496218778437" />
                  <Point X="21.2814325949" Y="1.206290012132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.577683916224" Y="1.502541333457" />
                  <Point X="24.681886035344" Y="4.606743452577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.465402627194" Y="0.524610332852" />
                  <Point X="21.155185685671" Y="1.214393391329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.558761445144" Y="1.617969150802" />
                  <Point X="24.653494626151" Y="4.712702331809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.359443893184" Y="0.553001887267" />
                  <Point X="21.036465263277" Y="1.23002325736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.512605710479" Y="1.706163704562" />
                  <Point X="24.582930757104" Y="4.776488751187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.253485159174" Y="0.581393441682" />
                  <Point X="20.917744840883" Y="1.245653123392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.447974577208" Y="1.775882859717" />
                  <Point X="24.430772563564" Y="4.758680846073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.229315102777" Y="0.691573673711" />
                  <Point X="20.799024418489" Y="1.261282989423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.371955852846" Y="1.83421442378" />
                  <Point X="24.278614370024" Y="4.740872940958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.252648467863" Y="0.849257327223" />
                  <Point X="20.680303996095" Y="1.276912855455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.295936994324" Y="1.892545853684" />
                  <Point X="24.126456176484" Y="4.723065035844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.284318200534" Y="1.015277348319" />
                  <Point X="20.561583573701" Y="1.292542721486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.219918108111" Y="1.950877255896" />
                  <Point X="21.997168489564" Y="2.728127637349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.246751243464" Y="2.977710391249" />
                  <Point X="23.356442946737" Y="4.087402094522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.487372219253" Y="4.218331367038" />
                  <Point X="23.952845585682" Y="4.683804733467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.334258300115" Y="1.199567736325" />
                  <Point X="20.442863151307" Y="1.308172587518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.143899221897" Y="2.009208658108" />
                  <Point X="21.864165434606" Y="2.729474870817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.245402983881" Y="3.110712420092" />
                  <Point X="23.163621215631" Y="4.028930651842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.537388491265" Y="4.402697927476" />
                  <Point X="23.766153252921" Y="4.631462689132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.067880335684" Y="2.067540060319" />
                  <Point X="21.757148331795" Y="2.756808056431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.218069404144" Y="3.21772912878" />
                  <Point X="23.056240279291" Y="4.055900003927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.525651352157" Y="4.525311076793" />
                  <Point X="23.579460920161" Y="4.579120644797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.99186144947" Y="2.125871462531" />
                  <Point X="21.67138037301" Y="2.805390386071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.169488110502" Y="3.303498123564" />
                  <Point X="22.967661055126" Y="4.101671068187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.915842563256" Y="2.184202864743" />
                  <Point X="21.586205710071" Y="2.854566011558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.120312693064" Y="3.388672994551" />
                  <Point X="22.891852113133" Y="4.16021241462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.839823677043" Y="2.242534266955" />
                  <Point X="21.501031047133" Y="2.903741637045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.071137275625" Y="3.473847865537" />
                  <Point X="22.832705912862" Y="4.235416502775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.793651893597" Y="2.330712771934" />
                  <Point X="21.415856384195" Y="2.952917262533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.021961858187" Y="3.559022736524" />
                  <Point X="22.635927709392" Y="4.17298858773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.982018814796" Y="2.653429981559" />
                  <Point X="21.330681721257" Y="3.00209288802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.972786440748" Y="3.644197607511" />
                  <Point X="22.317326174011" Y="3.988737340774" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.9998359375" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.8265859375" Y="-4.862161621094" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.319203125" Y="-3.312459228516" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399658203" />
                  <Point X="25.04977734375" Y="-3.196704833984" />
                  <Point X="25.0209765625" Y="-3.187766357422" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.76677734375" Y="-3.257461181641" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285644042969" />
                  <Point X="24.56660546875" Y="-3.494728759766" />
                  <Point X="24.547994140625" Y="-3.521544433594" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.49523828125" Y="-3.707052490234" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.932353515625" Y="-4.944393066406" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.64841015625" Y="-4.873396972656" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534756835938" />
                  <Point X="23.636669921875" Y="-4.265055175781" />
                  <Point X="23.6297890625" Y="-4.230465332031" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.40697265625" Y="-4.021317382812" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.07636328125" Y="-3.967777832031" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791748047" />
                  <Point X="22.781478515625" Y="-4.126565917969" />
                  <Point X="22.75215625" Y="-4.146159667969" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.714701171875" Y="-4.190610839844" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.191947265625" Y="-4.197194824219" />
                  <Point X="22.144169921875" Y="-4.167612792969" />
                  <Point X="21.783279296875" Y="-3.88973828125" />
                  <Point X="21.771419921875" Y="-3.880607177734" />
                  <Point X="21.834154296875" Y="-3.771948730469" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597592773438" />
                  <Point X="22.486021484375" Y="-2.568763671875" />
                  <Point X="22.470658203125" Y="-2.553400390625" />
                  <Point X="22.4686875" Y="-2.551428710938" />
                  <Point X="22.439853515625" Y="-2.537199951172" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="22.261748046875" Y="-2.628092041016" />
                  <Point X="21.15704296875" Y="-3.265893798828" />
                  <Point X="20.876046875" Y="-2.896723632812" />
                  <Point X="20.838296875" Y="-2.847126953125" />
                  <Point X="20.579568359375" Y="-2.413279296875" />
                  <Point X="20.56898046875" Y="-2.395526367188" />
                  <Point X="20.681755859375" Y="-2.308991699219" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.39601574707" />
                  <Point X="21.8610078125" Y="-1.369649780273" />
                  <Point X="21.86188671875" Y="-1.366255493164" />
                  <Point X="21.859671875" Y="-1.334590209961" />
                  <Point X="21.838841796875" Y="-1.310638916016" />
                  <Point X="21.815369140625" Y="-1.29682421875" />
                  <Point X="21.81236328125" Y="-1.295055297852" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.59544140625" Y="-1.312930786133" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.087373046875" Y="-1.068993041992" />
                  <Point X="20.072607421875" Y="-1.011188293457" />
                  <Point X="20.004154296875" Y="-0.532571838379" />
                  <Point X="20.001603515625" Y="-0.514742248535" />
                  <Point X="20.127435546875" Y="-0.481025848389" />
                  <Point X="21.442537109375" Y="-0.128645431519" />
                  <Point X="21.45810546875" Y="-0.121424766541" />
                  <Point X="21.482703125" Y="-0.104352302551" />
                  <Point X="21.485849609375" Y="-0.102169281006" />
                  <Point X="21.505103515625" Y="-0.075907463074" />
                  <Point X="21.513302734375" Y="-0.049488822937" />
                  <Point X="21.51435546875" Y="-0.046090393066" />
                  <Point X="21.514353515625" Y="-0.016459506989" />
                  <Point X="21.506154296875" Y="0.009959137917" />
                  <Point X="21.50510546875" Y="0.01333733654" />
                  <Point X="21.485861328125" Y="0.039600975037" />
                  <Point X="21.46126171875" Y="0.056673442841" />
                  <Point X="21.442537109375" Y="0.066085334778" />
                  <Point X="21.273873046875" Y="0.111278778076" />
                  <Point X="20.001814453125" Y="0.452125976563" />
                  <Point X="20.072841796875" Y="0.932122009277" />
                  <Point X="20.08235546875" Y="0.996417785645" />
                  <Point X="20.220150390625" Y="1.504921142578" />
                  <Point X="20.226484375" Y="1.528298706055" />
                  <Point X="20.3040390625" Y="1.518088623047" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866699219" />
                  <Point X="21.322740234375" Y="1.413032714844" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056152344" />
                  <Point X="21.360880859375" Y="1.443785888672" />
                  <Point X="21.3827265625" Y="1.496526123047" />
                  <Point X="21.38553125" Y="1.503295776367" />
                  <Point X="21.38928515625" Y="1.524609008789" />
                  <Point X="21.38368359375" Y="1.545512817383" />
                  <Point X="21.35732421875" Y="1.59614855957" />
                  <Point X="21.353943359375" Y="1.602642700195" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.24328515625" Y="1.693457885742" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.803017578125" Y="2.723668945313" />
                  <Point X="20.839986328125" Y="2.787006103516" />
                  <Point X="21.204998046875" Y="3.256177734375" />
                  <Point X="21.225330078125" Y="3.282310791016" />
                  <Point X="21.261865234375" Y="3.261216796875" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.937310546875" Y="2.913801025391" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927405273438" />
                  <Point X="22.0405703125" Y="2.981226074219" />
                  <Point X="22.04747265625" Y="2.988128662109" />
                  <Point X="22.0591015625" Y="3.006383056641" />
                  <Point X="22.061927734375" Y="3.027841796875" />
                  <Point X="22.05529296875" Y="3.103666503906" />
                  <Point X="22.054443359375" Y="3.113384521484" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="22.0050625" Y="3.208288085938" />
                  <Point X="21.69272265625" Y="3.749277099609" />
                  <Point X="22.18273828125" Y="4.124966796875" />
                  <Point X="22.247123046875" Y="4.174331054688" />
                  <Point X="22.8220078125" Y="4.493724121094" />
                  <Point X="22.858453125" Y="4.513972167969" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.133146484375" Y="4.229728515625" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.218491699219" />
                  <Point X="23.18619140625" Y="4.22225" />
                  <Point X="23.274091796875" Y="4.25866015625" />
                  <Point X="23.285365234375" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275743164062" />
                  <Point X="23.31391796875" Y="4.294485839844" />
                  <Point X="23.34252734375" Y="4.385225097656" />
                  <Point X="23.346197265625" Y="4.396862304687" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.343208984375" Y="4.455447753906" />
                  <Point X="23.31086328125" Y="4.701141113281" />
                  <Point X="23.948552734375" Y="4.879927246094" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.728841796875" Y="4.984862304688" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.78658203125" Y="4.95012109375" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.076419921875" Y="4.392884277344" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.787423828125" Y="4.933187988281" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.436796875" Y="4.786358886719" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.884568359375" Y="4.632640136719" />
                  <Point X="26.931033203125" Y="4.615786132813" />
                  <Point X="27.293935546875" Y="4.446069824219" />
                  <Point X="27.338697265625" Y="4.42513671875" />
                  <Point X="27.6892734375" Y="4.220889648438" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="28.0631640625" Y="3.96055859375" />
                  <Point X="28.0687421875" Y="3.956592285156" />
                  <Point X="27.9935390625" Y="3.826337158203" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.22485546875" Y="2.491519042969" />
                  <Point X="27.205826171875" Y="2.420359130859" />
                  <Point X="27.202044921875" Y="2.392328613281" />
                  <Point X="27.20946484375" Y="2.330795654297" />
                  <Point X="27.210416015625" Y="2.322903808594" />
                  <Point X="27.218681640625" Y="2.300813720703" />
                  <Point X="27.2567578125" Y="2.244701660156" />
                  <Point X="27.274943359375" Y="2.224202148438" />
                  <Point X="27.3310546875" Y="2.186127685547" />
                  <Point X="27.338251953125" Y="2.181244628906" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.42187109375" Y="2.165560302734" />
                  <Point X="27.4486640625" Y="2.165946044922" />
                  <Point X="27.51982421875" Y="2.184975341797" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="27.7106796875" Y="2.290361572266" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.1769375" Y="2.777535400391" />
                  <Point X="29.20259765625" Y="2.741873046875" />
                  <Point X="29.386919921875" Y="2.437279541016" />
                  <Point X="29.387515625" Y="2.436296630859" />
                  <Point X="29.294078125" Y="2.364600341797" />
                  <Point X="28.2886171875" Y="1.593082641602" />
                  <Point X="28.27937109375" Y="1.583833496094" />
                  <Point X="28.228158203125" Y="1.517020996094" />
                  <Point X="28.21312109375" Y="1.491502929688" />
                  <Point X="28.19404296875" Y="1.423287231445" />
                  <Point X="28.191595703125" Y="1.414538452148" />
                  <Point X="28.190779296875" Y="1.39096862793" />
                  <Point X="28.20644140625" Y="1.315069946289" />
                  <Point X="28.2156484375" Y="1.287954833984" />
                  <Point X="28.2582421875" Y="1.223212768555" />
                  <Point X="28.263705078125" Y="1.214909545898" />
                  <Point X="28.280947265625" Y="1.1988203125" />
                  <Point X="28.342673828125" Y="1.16407409668" />
                  <Point X="28.36856640625" Y="1.153619506836" />
                  <Point X="28.4520234375" Y="1.14258972168" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.636955078125" Y="1.162387695312" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.928169921875" Y="0.996647644043" />
                  <Point X="29.93919140625" Y="0.951368652344" />
                  <Point X="29.99727734375" Y="0.578305786133" />
                  <Point X="29.997861328125" Y="0.574556640625" />
                  <Point X="29.89344921875" Y="0.546579467773" />
                  <Point X="28.74116796875" Y="0.237826904297" />
                  <Point X="28.729087890625" Y="0.232819091797" />
                  <Point X="28.64709375" Y="0.185424972534" />
                  <Point X="28.622263671875" Y="0.166923675537" />
                  <Point X="28.57306640625" Y="0.104235946655" />
                  <Point X="28.5667578125" Y="0.096196144104" />
                  <Point X="28.556986328125" Y="0.07473664093" />
                  <Point X="28.540587890625" Y="-0.010891898155" />
                  <Point X="28.538484375" Y="-0.040686157227" />
                  <Point X="28.5548828125" Y="-0.126314544678" />
                  <Point X="28.556986328125" Y="-0.137296737671" />
                  <Point X="28.5667578125" Y="-0.158756088257" />
                  <Point X="28.615955078125" Y="-0.221443969727" />
                  <Point X="28.636578125" Y="-0.241906646729" />
                  <Point X="28.718572265625" Y="-0.289300750732" />
                  <Point X="28.74116796875" Y="-0.300386993408" />
                  <Point X="28.888951171875" Y="-0.339985137939" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.9545859375" Y="-0.925591186523" />
                  <Point X="29.948431640625" Y="-0.966413513184" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.748302734375" Y="-1.273558227539" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341308594" />
                  <Point X="28.233912109375" Y="-1.133319091797" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.1854453125" Y="-1.154697631836" />
                  <Point X="28.08817578125" Y="-1.271682250977" />
                  <Point X="28.075701171875" Y="-1.286685668945" />
                  <Point X="28.064359375" Y="-1.314072753906" />
                  <Point X="28.05041796875" Y="-1.465572875977" />
                  <Point X="28.048630859375" Y="-1.485003173828" />
                  <Point X="28.056361328125" Y="-1.516621826172" />
                  <Point X="28.145419921875" Y="-1.655146362305" />
                  <Point X="28.156841796875" Y="-1.672912353516" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.30560546875" Y="-1.790774047852" />
                  <Point X="29.339076171875" Y="-2.583784179688" />
                  <Point X="29.221474609375" Y="-2.774079101562" />
                  <Point X="29.2041328125" Y="-2.802141357422" />
                  <Point X="29.056689453125" Y="-3.011638183594" />
                  <Point X="28.94362890625" Y="-2.94636328125" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.73733984375" Y="-2.253312744141" />
                  <Point X="27.545814453125" Y="-2.218723144531" />
                  <Point X="27.52125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.329966796875" Y="-2.302984130859" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334682861328" />
                  <Point X="27.204861328125" Y="-2.493794433594" />
                  <Point X="27.19412109375" Y="-2.514200927734" />
                  <Point X="27.1891640625" Y="-2.546376953125" />
                  <Point X="27.22375390625" Y="-2.737903564453" />
                  <Point X="27.22819140625" Y="-2.762467285156" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.322220703125" Y="-2.931220947266" />
                  <Point X="27.98667578125" Y="-4.082087646484" />
                  <Point X="27.85587890625" Y="-4.17551171875" />
                  <Point X="27.835279296875" Y="-4.190224609375" />
                  <Point X="27.67977734375" Y="-4.290879882812" />
                  <Point X="27.591390625" Y="-4.175692871094" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467529297" />
                  <Point X="26.48165234375" Y="-2.859024169922" />
                  <Point X="26.45742578125" Y="-2.843448730469" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.21921484375" Y="-2.854727783203" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509521484" />
                  <Point X="26.00580859375" Y="-3.001148681641" />
                  <Point X="25.985349609375" Y="-3.018159912109" />
                  <Point X="25.96845703125" Y="-3.045986328125" />
                  <Point X="25.920759765625" Y="-3.265429931641" />
                  <Point X="25.914642578125" Y="-3.29357421875" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.938904296875" Y="-3.500422607422" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.013814453125" Y="-4.958979980469" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#206" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.171277830952" Y="4.994342073515" Z="2.25" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.25" />
                  <Point X="-0.283128192046" Y="5.06580346622" Z="2.25" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.25" />
                  <Point X="-1.071101298622" Y="4.959350642157" Z="2.25" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.25" />
                  <Point X="-1.712519868411" Y="4.480202116458" Z="2.25" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.25" />
                  <Point X="-1.711315179565" Y="4.431543099596" Z="2.25" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.25" />
                  <Point X="-1.751198562558" Y="4.336134458792" Z="2.25" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.25" />
                  <Point X="-1.849922576037" Y="4.305358990207" Z="2.25" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.25" />
                  <Point X="-2.111557992981" Y="4.580278730407" Z="2.25" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.25" />
                  <Point X="-2.208432099115" Y="4.568711462616" Z="2.25" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.25" />
                  <Point X="-2.853841234747" Y="4.195926327517" Z="2.25" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.25" />
                  <Point X="-3.044395923203" Y="3.214568065397" Z="2.25" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.25" />
                  <Point X="-3.000673879805" Y="3.130588239903" Z="2.25" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.25" />
                  <Point X="-3.00094265274" Y="3.047860915146" Z="2.25" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.25" />
                  <Point X="-3.064488225704" Y="2.994890819368" Z="2.25" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.25" />
                  <Point X="-3.7192912829" Y="3.335798025543" Z="2.25" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.25" />
                  <Point X="-3.840621815225" Y="3.318160495151" Z="2.25" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.25" />
                  <Point X="-4.246321928298" Y="2.780154074928" Z="2.25" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.25" />
                  <Point X="-3.793308638411" Y="1.685070197349" Z="2.25" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.25" />
                  <Point X="-3.693181665063" Y="1.604340000402" Z="2.25" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.25" />
                  <Point X="-3.669624191117" Y="1.546940339246" Z="2.25" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.25" />
                  <Point X="-3.698452417165" Y="1.491998535844" Z="2.25" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.25" />
                  <Point X="-4.69559308377" Y="1.598940988558" Z="2.25" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.25" />
                  <Point X="-4.834266983479" Y="1.549277389038" Z="2.25" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.25" />
                  <Point X="-4.982776755928" Y="0.970720533935" Z="2.25" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.25" />
                  <Point X="-3.74522522337" Y="0.09426254603" Z="2.25" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.25" />
                  <Point X="-3.573405977029" Y="0.046879449396" Z="2.25" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.25" />
                  <Point X="-3.547756255128" Y="0.026418699103" Z="2.25" />
                  <Point X="-3.539556741714" Y="0" Z="2.25" />
                  <Point X="-3.540608346007" Y="-0.003388251958" Z="2.25" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.25" />
                  <Point X="-3.551962618094" Y="-0.031996533738" Z="2.25" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.25" />
                  <Point X="-4.891662565403" Y="-0.401449510726" Z="2.25" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.25" />
                  <Point X="-5.051498585428" Y="-0.508370739248" Z="2.25" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.25" />
                  <Point X="-4.967229630298" Y="-1.050086884911" Z="2.25" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.25" />
                  <Point X="-3.404189831201" Y="-1.331223089101" Z="2.25" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.25" />
                  <Point X="-3.216148527614" Y="-1.308635074996" Z="2.25" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.25" />
                  <Point X="-3.193552100645" Y="-1.325831282188" Z="2.25" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.25" />
                  <Point X="-4.354838987306" Y="-2.238044054824" Z="2.25" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.25" />
                  <Point X="-4.469532314342" Y="-2.407609243926" Z="2.25" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.25" />
                  <Point X="-4.169897086498" Y="-2.895726504527" Z="2.25" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.25" />
                  <Point X="-2.719409672138" Y="-2.64011329863" Z="2.25" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.25" />
                  <Point X="-2.570867435576" Y="-2.557463033555" Z="2.25" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.25" />
                  <Point X="-3.215303714538" Y="-3.715668710354" Z="2.25" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.25" />
                  <Point X="-3.253382484296" Y="-3.898075873609" Z="2.25" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.25" />
                  <Point X="-2.840532302694" Y="-4.208425750049" Z="2.25" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.25" />
                  <Point X="-2.25178686864" Y="-4.189768592189" Z="2.25" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.25" />
                  <Point X="-2.196898420269" Y="-4.136858575686" Z="2.25" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.25" />
                  <Point X="-1.933064002958" Y="-3.986390975394" Z="2.25" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.25" />
                  <Point X="-1.632151336102" Y="-4.027629447113" Z="2.25" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.25" />
                  <Point X="-1.418525630012" Y="-4.243530324691" Z="2.25" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.25" />
                  <Point X="-1.407617669522" Y="-4.837868045817" Z="2.25" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.25" />
                  <Point X="-1.37948619773" Y="-4.888151544955" Z="2.25" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.25" />
                  <Point X="-1.083330138377" Y="-4.962197009392" Z="2.25" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.25" />
                  <Point X="-0.462622144358" Y="-3.688713384123" Z="2.25" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.25" />
                  <Point X="-0.39847532594" Y="-3.491957521785" Z="2.25" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.25" />
                  <Point X="-0.22455893825" Y="-3.273934175873" Z="2.25" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.25" />
                  <Point X="0.028800141111" Y="-3.213177869415" Z="2.25" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.25" />
                  <Point X="0.271970533492" Y="-3.309688233982" Z="2.25" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.25" />
                  <Point X="0.772132716044" Y="-4.843822645693" Z="2.25" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.25" />
                  <Point X="0.838168144167" Y="-5.010038837474" Z="2.25" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.25" />
                  <Point X="1.018362531082" Y="-4.976540148752" Z="2.25" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.25" />
                  <Point X="0.982320606478" Y="-3.462616284498" Z="2.25" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.25" />
                  <Point X="0.963463020517" Y="-3.244769606698" Z="2.25" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.25" />
                  <Point X="1.031619345882" Y="-3.008314751716" Z="2.25" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.25" />
                  <Point X="1.217639466309" Y="-2.873237280024" Z="2.25" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.25" />
                  <Point X="1.448456883062" Y="-2.869801943144" Z="2.25" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.25" />
                  <Point X="2.545566548883" Y="-4.174850800018" Z="2.25" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.25" />
                  <Point X="2.684238741178" Y="-4.312286120072" Z="2.25" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.25" />
                  <Point X="2.87878498704" Y="-4.184918906084" Z="2.25" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.25" />
                  <Point X="2.359364978702" Y="-2.874940874171" Z="2.25" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.25" />
                  <Point X="2.26680071023" Y="-2.697734926991" Z="2.25" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.25" />
                  <Point X="2.24295085421" Y="-2.485801794938" Z="2.25" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.25" />
                  <Point X="2.347096583435" Y="-2.315950455757" Z="2.25" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.25" />
                  <Point X="2.530771876521" Y="-2.23664729926" Z="2.25" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.25" />
                  <Point X="3.912472722936" Y="-2.958384521573" Z="2.25" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.25" />
                  <Point X="4.084963145513" Y="-3.018311087349" Z="2.25" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.25" />
                  <Point X="4.257851571663" Y="-2.769083743048" Z="2.25" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.25" />
                  <Point X="3.329885661914" Y="-1.719826443428" Z="2.25" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.25" />
                  <Point X="3.181320903751" Y="-1.596826981865" Z="2.25" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.25" />
                  <Point X="3.094050466858" Y="-1.438872304936" Z="2.25" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.25" />
                  <Point X="3.120466418477" Y="-1.272368725657" Z="2.25" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.25" />
                  <Point X="3.238374530194" Y="-1.150898210627" Z="2.25" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.25" />
                  <Point X="4.735620852114" Y="-1.291850407371" Z="2.25" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.25" />
                  <Point X="4.916604357478" Y="-1.27235571892" Z="2.25" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.25" />
                  <Point X="4.997869647486" Y="-0.90176560192" Z="2.25" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.25" />
                  <Point X="3.89573497447" Y="-0.26040877625" Z="2.25" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.25" />
                  <Point X="3.737436792246" Y="-0.214732253899" Z="2.25" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.25" />
                  <Point X="3.649133170242" Y="-0.159298365023" Z="2.25" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.25" />
                  <Point X="3.597833542226" Y="-0.085628501526" Z="2.25" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.25" />
                  <Point X="3.583537908198" Y="0.010982029655" Z="2.25" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.25" />
                  <Point X="3.606246268158" Y="0.104650373552" Z="2.25" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.25" />
                  <Point X="3.665958622106" Y="0.173416590343" Z="2.25" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.25" />
                  <Point X="4.900232842837" Y="0.529563156519" Z="2.25" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.25" />
                  <Point X="5.040523798928" Y="0.617276813127" Z="2.25" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.25" />
                  <Point X="4.970594671" Y="1.039753939291" Z="2.25" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.25" />
                  <Point X="3.624271844719" Y="1.243239946335" Z="2.25" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.25" />
                  <Point X="3.452417853862" Y="1.223438689025" Z="2.25" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.25" />
                  <Point X="3.36104418132" Y="1.238924848015" Z="2.25" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.25" />
                  <Point X="3.293855695102" Y="1.281974333387" Z="2.25" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.25" />
                  <Point X="3.249252508093" Y="1.35645055977" Z="2.25" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.25" />
                  <Point X="3.236038730266" Y="1.4410980069" Z="2.25" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.25" />
                  <Point X="3.261684271265" Y="1.517882548471" Z="2.25" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.25" />
                  <Point X="4.318359423557" Y="2.356212637873" Z="2.25" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.25" />
                  <Point X="4.423539637117" Y="2.494445211766" Z="2.25" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.25" />
                  <Point X="4.211365870517" Y="2.838018476285" Z="2.25" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.25" />
                  <Point X="2.679521921305" Y="2.364942771999" Z="2.25" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.25" />
                  <Point X="2.50075152235" Y="2.264558250185" Z="2.25" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.25" />
                  <Point X="2.421699907824" Y="2.246480773431" Z="2.25" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.25" />
                  <Point X="2.352970282834" Y="2.258783650128" Z="2.25" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.25" />
                  <Point X="2.291974950107" Y="2.304054577548" Z="2.25" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.25" />
                  <Point X="2.252948929327" Y="2.368058536009" Z="2.25" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.25" />
                  <Point X="2.247969692472" Y="2.438718053532" Z="2.25" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.25" />
                  <Point X="3.030682580861" Y="3.83261716065" Z="2.25" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.25" />
                  <Point X="3.08598449439" Y="4.032585860832" Z="2.25" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.25" />
                  <Point X="2.708286009277" Y="4.295372518166" Z="2.25" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.25" />
                  <Point X="2.308959878866" Y="4.522641526216" Z="2.25" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.25" />
                  <Point X="1.895459394655" Y="4.710923812723" Z="2.25" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.25" />
                  <Point X="1.442374959544" Y="4.866242441956" Z="2.25" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.25" />
                  <Point X="0.786475577119" Y="5.014195901209" Z="2.25" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.25" />
                  <Point X="0.021966730601" Y="4.437105432436" Z="2.25" />
                  <Point X="0" Y="4.355124473572" Z="2.25" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>