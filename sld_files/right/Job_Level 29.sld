<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#173" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2146" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.784296875" Y="-4.337282226563" />
                  <Point X="25.5633046875" Y="-3.512524414063" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.4520390625" Y="-3.337235107422" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.16272265625" Y="-3.132288574219" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.82340234375" Y="-3.140416259766" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.5433515625" Y="-3.361618896484" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.269421875" Y="-4.182756347656" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="24.050759765625" Y="-4.870603027344" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.761755859375" Y="-4.804465332031" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.758435546875" Y="-4.765502929688" />
                  <Point X="23.7850390625" Y="-4.563437988281" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.750099609375" Y="-4.348351074219" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.547669921875" Y="-4.018349609375" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295898438" />
                  <Point X="23.387115234375" Y="-3.897994628906" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.186177734375" Y="-3.879771728516" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.81502734375" Y="-3.989893798828" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461425781" />
                  <Point X="22.564072265625" Y="-4.230858886719" />
                  <Point X="22.5198515625" Y="-4.288488769531" />
                  <Point X="22.38898828125" Y="-4.207461425781" />
                  <Point X="22.198287109375" Y="-4.089384277344" />
                  <Point X="21.978255859375" Y="-3.919967773438" />
                  <Point X="21.895279296875" Y="-3.856078369141" />
                  <Point X="22.165548828125" Y="-3.387956787109" />
                  <Point X="22.576240234375" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647655029297" />
                  <Point X="22.593412109375" Y="-2.616129150391" />
                  <Point X="22.59442578125" Y="-2.585194824219" />
                  <Point X="22.58544140625" Y="-2.555576416016" />
                  <Point X="22.571224609375" Y="-2.526747558594" />
                  <Point X="22.55319921875" Y="-2.501591552734" />
                  <Point X="22.5358515625" Y="-2.484242919922" />
                  <Point X="22.510693359375" Y="-2.466214355469" />
                  <Point X="22.481865234375" Y="-2.451997070313" />
                  <Point X="22.452248046875" Y="-2.443011962891" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.782755859375" Y="-2.794941650391" />
                  <Point X="21.1819765625" Y="-3.141801513672" />
                  <Point X="21.067798828125" Y="-2.991796386719" />
                  <Point X="20.917140625" Y="-2.793860595703" />
                  <Point X="20.75939453125" Y="-2.529344238281" />
                  <Point X="20.693857421875" Y="-2.419449951172" />
                  <Point X="21.175470703125" Y="-2.049895751953" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.91541796875" Y="-1.475598754883" />
                  <Point X="21.9333828125" Y="-1.448471679688" />
                  <Point X="21.946142578125" Y="-1.419838500977" />
                  <Point X="21.953849609375" Y="-1.390083862305" />
                  <Point X="21.956654296875" Y="-1.359646850586" />
                  <Point X="21.954443359375" Y="-1.327977539062" />
                  <Point X="21.94744140625" Y="-1.298234985352" />
                  <Point X="21.931359375" Y="-1.27225378418" />
                  <Point X="21.91052734375" Y="-1.248298828125" />
                  <Point X="21.887029296875" Y="-1.228766845703" />
                  <Point X="21.860546875" Y="-1.213180419922" />
                  <Point X="21.83128125" Y="-1.201956054688" />
                  <Point X="21.79939453125" Y="-1.195474731445" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="21.03832421875" Y="-1.29045703125" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.22484765625" Y="-1.223341918945" />
                  <Point X="20.165921875" Y="-0.992650268555" />
                  <Point X="20.1241875" Y="-0.700843444824" />
                  <Point X="20.107576171875" Y="-0.584698486328" />
                  <Point X="20.648560546875" Y="-0.43974230957" />
                  <Point X="21.467125" Y="-0.220408432007" />
                  <Point X="21.49007421875" Y="-0.210896179199" />
                  <Point X="21.520951171875" Y="-0.193050796509" />
                  <Point X="21.5341171875" Y="-0.18387689209" />
                  <Point X="21.558380859375" Y="-0.163721862793" />
                  <Point X="21.57444921875" Y="-0.146601470947" />
                  <Point X="21.585828125" Y="-0.126062675476" />
                  <Point X="21.598357421875" Y="-0.094878707886" />
                  <Point X="21.602853515625" Y="-0.080476837158" />
                  <Point X="21.60924609375" Y="-0.052296051025" />
                  <Point X="21.611599609375" Y="-0.030907636642" />
                  <Point X="21.609078125" Y="-0.009538374901" />
                  <Point X="21.601728515625" Y="0.021723304749" />
                  <Point X="21.59709375" Y="0.036153862" />
                  <Point X="21.585521484375" Y="0.064257087708" />
                  <Point X="21.573966796875" Y="0.084697891235" />
                  <Point X="21.55775" Y="0.101680091858" />
                  <Point X="21.530619140625" Y="0.123825996399" />
                  <Point X="21.51734375" Y="0.132905029297" />
                  <Point X="21.489333984375" Y="0.148759689331" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="20.80192578125" Y="0.336088256836" />
                  <Point X="20.10818359375" Y="0.521975524902" />
                  <Point X="20.137537109375" Y="0.720342346191" />
                  <Point X="20.17551171875" Y="0.97696862793" />
                  <Point X="20.259525390625" Y="1.287008422852" />
                  <Point X="20.29644921875" Y="1.423268066406" />
                  <Point X="20.64756640625" Y="1.377042358398" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228149414" />
                  <Point X="21.296865234375" Y="1.305263671875" />
                  <Point X="21.330751953125" Y="1.315948364258" />
                  <Point X="21.358291015625" Y="1.324631225586" />
                  <Point X="21.377224609375" Y="1.332962768555" />
                  <Point X="21.39596875" Y="1.34378527832" />
                  <Point X="21.4126484375" Y="1.356015014648" />
                  <Point X="21.42628515625" Y="1.371564453125" />
                  <Point X="21.438701171875" Y="1.389295288086" />
                  <Point X="21.448650390625" Y="1.407432250977" />
                  <Point X="21.462248046875" Y="1.440259765625" />
                  <Point X="21.473296875" Y="1.466936645508" />
                  <Point X="21.479087890625" Y="1.486806152344" />
                  <Point X="21.48284375" Y="1.508121337891" />
                  <Point X="21.484193359375" Y="1.528755859375" />
                  <Point X="21.481048828125" Y="1.549193725586" />
                  <Point X="21.475447265625" Y="1.570099975586" />
                  <Point X="21.467951171875" Y="1.589377807617" />
                  <Point X="21.451544921875" Y="1.620895263672" />
                  <Point X="21.4382109375" Y="1.646507446289" />
                  <Point X="21.426716796875" Y="1.663709716797" />
                  <Point X="21.4128046875" Y="1.680288574219" />
                  <Point X="21.39786328125" Y="1.694590332031" />
                  <Point X="21.016302734375" Y="1.987372070312" />
                  <Point X="20.648140625" Y="2.269873535156" />
                  <Point X="20.771291015625" Y="2.480859863281" />
                  <Point X="20.9188515625" Y="2.733664794922" />
                  <Point X="21.141392578125" Y="3.019710449219" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.432966796875" Y="3.052734619141" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.90040234375" Y="2.821667236328" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.95943359375" Y="2.818762939453" />
                  <Point X="21.980892578125" Y="2.821587890625" />
                  <Point X="22.000984375" Y="2.826504150391" />
                  <Point X="22.019537109375" Y="2.83565234375" />
                  <Point X="22.037791015625" Y="2.847281005859" />
                  <Point X="22.053923828125" Y="2.860228759766" />
                  <Point X="22.087423828125" Y="2.893728759766" />
                  <Point X="22.1146484375" Y="2.920952148438" />
                  <Point X="22.127595703125" Y="2.937085449219" />
                  <Point X="22.139224609375" Y="2.955340087891" />
                  <Point X="22.14837109375" Y="2.973888916016" />
                  <Point X="22.1532890625" Y="2.993977539062" />
                  <Point X="22.156115234375" Y="3.015436767578" />
                  <Point X="22.15656640625" Y="3.036120361328" />
                  <Point X="22.1524375" Y="3.08331640625" />
                  <Point X="22.14908203125" Y="3.121669921875" />
                  <Point X="22.145044921875" Y="3.141962646484" />
                  <Point X="22.13853515625" Y="3.162604980469" />
                  <Point X="22.130205078125" Y="3.181533203125" />
                  <Point X="21.961123046875" Y="3.474390136719" />
                  <Point X="21.81666796875" Y="3.724595703125" />
                  <Point X="22.0423828125" Y="3.897650390625" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.649875" Y="4.2894140625" />
                  <Point X="22.83296484375" Y="4.391133789062" />
                  <Point X="22.846482421875" Y="4.373516113281" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.18939453125" />
                  <Point X="23.057416015625" Y="4.162049316406" />
                  <Point X="23.100103515625" Y="4.139827636719" />
                  <Point X="23.1193828125" Y="4.132331054688" />
                  <Point X="23.1402890625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.12358203125" />
                  <Point X="23.181373046875" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222546875" Y="4.134481933594" />
                  <Point X="23.2772578125" Y="4.157145019531" />
                  <Point X="23.321720703125" Y="4.175561523437" />
                  <Point X="23.3398515625" Y="4.185508300781" />
                  <Point X="23.35758203125" Y="4.197922363281" />
                  <Point X="23.373134765625" Y="4.211560546875" />
                  <Point X="23.3853671875" Y="4.228241699219" />
                  <Point X="23.396189453125" Y="4.246985351563" />
                  <Point X="23.404521484375" Y="4.265921875" />
                  <Point X="23.422328125" Y="4.322401367188" />
                  <Point X="23.43680078125" Y="4.368298339844" />
                  <Point X="23.4408359375" Y="4.388583007812" />
                  <Point X="23.44272265625" Y="4.410145996094" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.423048828125" Y="4.576836914062" />
                  <Point X="23.415798828125" Y="4.6318984375" />
                  <Point X="23.717673828125" Y="4.716533691406" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.4752734375" Y="4.859537597656" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.759486328125" Y="4.6841875" />
                  <Point X="24.866095703125" Y="4.286315917969" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.232853515625" Y="4.609642089844" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.553548828125" Y="4.862161621094" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.195583984375" Y="4.746865722656" />
                  <Point X="26.48102734375" Y="4.677950195312" />
                  <Point X="26.70918359375" Y="4.595197265625" />
                  <Point X="26.894646484375" Y="4.527928222656" />
                  <Point X="27.11590625" Y="4.424452148438" />
                  <Point X="27.294576171875" Y="4.340893554688" />
                  <Point X="27.508349609375" Y="4.216349609375" />
                  <Point X="27.680978515625" Y="4.115775878906" />
                  <Point X="27.882572265625" Y="3.972413085938" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.622927734375" Y="3.374417480469" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142078125" Y="2.539933105469" />
                  <Point X="27.133078125" Y="2.516058837891" />
                  <Point X="27.121232421875" Y="2.471766357422" />
                  <Point X="27.111607421875" Y="2.435772460938" />
                  <Point X="27.108619140625" Y="2.417936035156" />
                  <Point X="27.107728515625" Y="2.380952636719" />
                  <Point X="27.11234765625" Y="2.34265234375" />
                  <Point X="27.116099609375" Y="2.311527832031" />
                  <Point X="27.121443359375" Y="2.289605224609" />
                  <Point X="27.1297109375" Y="2.267513671875" />
                  <Point X="27.140072265625" Y="2.247469726562" />
                  <Point X="27.163771484375" Y="2.212543457031" />
                  <Point X="27.18303125" Y="2.184161132813" />
                  <Point X="27.194466796875" Y="2.170328125" />
                  <Point X="27.221599609375" Y="2.145592773438" />
                  <Point X="27.256525390625" Y="2.121893798828" />
                  <Point X="27.284908203125" Y="2.102635253906" />
                  <Point X="27.304953125" Y="2.092272216797" />
                  <Point X="27.327041015625" Y="2.084006347656" />
                  <Point X="27.34896484375" Y="2.078663330078" />
                  <Point X="27.387265625" Y="2.074044921875" />
                  <Point X="27.418388671875" Y="2.070291748047" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.473205078125" Y="2.074171142578" />
                  <Point X="27.517498046875" Y="2.086015625" />
                  <Point X="27.5534921875" Y="2.095640869141" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.25759765625" Y="2.496428955078" />
                  <Point X="28.967326171875" Y="2.906190673828" />
                  <Point X="29.020873046875" Y="2.831772705078" />
                  <Point X="29.12328125" Y="2.689450927734" />
                  <Point X="29.235658203125" Y="2.503744140625" />
                  <Point X="29.26219921875" Y="2.459883544922" />
                  <Point X="28.85662109375" Y="2.148670898438" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.221427734375" Y="1.660243408203" />
                  <Point X="28.20397265625" Y="1.641626953125" />
                  <Point X="28.172095703125" Y="1.600040527344" />
                  <Point X="28.14619140625" Y="1.566245605469" />
                  <Point X="28.13660546875" Y="1.550911010742" />
                  <Point X="28.121630859375" Y="1.517088500977" />
                  <Point X="28.109755859375" Y="1.474628540039" />
                  <Point X="28.10010546875" Y="1.440124023438" />
                  <Point X="28.09665234375" Y="1.417824951172" />
                  <Point X="28.0958359375" Y="1.394253051758" />
                  <Point X="28.097740234375" Y="1.371766845703" />
                  <Point X="28.10748828125" Y="1.324524414062" />
                  <Point X="28.11541015625" Y="1.286133911133" />
                  <Point X="28.1206796875" Y="1.268978759766" />
                  <Point X="28.13628125" Y="1.235741699219" />
                  <Point X="28.16279296875" Y="1.195443969727" />
                  <Point X="28.184337890625" Y="1.162696411133" />
                  <Point X="28.19889453125" Y="1.14544934082" />
                  <Point X="28.216138671875" Y="1.129359863281" />
                  <Point X="28.23434765625" Y="1.116034301758" />
                  <Point X="28.27276953125" Y="1.094407104492" />
                  <Point X="28.303990234375" Y="1.07683190918" />
                  <Point X="28.320521484375" Y="1.069501953125" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.40806640625" Y="1.052573120117" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.12376953125" Y="1.130658325195" />
                  <Point X="29.77683984375" Y="1.21663659668" />
                  <Point X="29.80195703125" Y="1.113463623047" />
                  <Point X="29.845939453125" Y="0.932788146973" />
                  <Point X="29.881353515625" Y="0.70533807373" />
                  <Point X="29.890865234375" Y="0.644238708496" />
                  <Point X="29.43380078125" Y="0.521768249512" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.704791015625" Y="0.32558605957" />
                  <Point X="28.681546875" Y="0.315067749023" />
                  <Point X="28.63051171875" Y="0.285568023682" />
                  <Point X="28.589037109375" Y="0.261595336914" />
                  <Point X="28.574314453125" Y="0.251097961426" />
                  <Point X="28.54753125" Y="0.225576385498" />
                  <Point X="28.51691015625" Y="0.186557159424" />
                  <Point X="28.492025390625" Y="0.154848602295" />
                  <Point X="28.48030078125" Y="0.135567596436" />
                  <Point X="28.47052734375" Y="0.114103675842" />
                  <Point X="28.463681640625" Y="0.092603782654" />
                  <Point X="28.453474609375" Y="0.03930563736" />
                  <Point X="28.4451796875" Y="-0.004006679058" />
                  <Point X="28.443484375" Y="-0.02187528801" />
                  <Point X="28.4451796875" Y="-0.058553302765" />
                  <Point X="28.45538671875" Y="-0.111851455688" />
                  <Point X="28.463681640625" Y="-0.155163925171" />
                  <Point X="28.47052734375" Y="-0.176663803101" />
                  <Point X="28.48030078125" Y="-0.198127731323" />
                  <Point X="28.492025390625" Y="-0.217408584595" />
                  <Point X="28.522646484375" Y="-0.256427825928" />
                  <Point X="28.54753125" Y="-0.288136535645" />
                  <Point X="28.560001953125" Y="-0.301237854004" />
                  <Point X="28.589037109375" Y="-0.324155609131" />
                  <Point X="28.640072265625" Y="-0.353655151367" />
                  <Point X="28.681546875" Y="-0.377627868652" />
                  <Point X="28.6927109375" Y="-0.383138763428" />
                  <Point X="28.716580078125" Y="-0.392149963379" />
                  <Point X="29.299423828125" Y="-0.548322021484" />
                  <Point X="29.891474609375" Y="-0.706961730957" />
                  <Point X="29.879580078125" Y="-0.785849365234" />
                  <Point X="29.855025390625" Y="-0.948724365234" />
                  <Point X="29.809650390625" Y="-1.147559814453" />
                  <Point X="29.80117578125" Y="-1.184698974609" />
                  <Point X="29.25615625" Y="-1.112945922852" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.274494140625" Y="-1.027280395508" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.163974609375" Y="-1.056597045898" />
                  <Point X="28.1361484375" Y="-1.073489013672" />
                  <Point X="28.1123984375" Y="-1.093960327148" />
                  <Point X="28.05185546875" Y="-1.166775268555" />
                  <Point X="28.002654296875" Y="-1.225947998047" />
                  <Point X="27.987935546875" Y="-1.250329956055" />
                  <Point X="27.976591796875" Y="-1.277716186523" />
                  <Point X="27.969759765625" Y="-1.305365722656" />
                  <Point X="27.96108203125" Y="-1.399665039062" />
                  <Point X="27.95403125" Y="-1.476296020508" />
                  <Point X="27.956349609375" Y="-1.507567382812" />
                  <Point X="27.96408203125" Y="-1.539188354492" />
                  <Point X="27.976453125" Y="-1.567996337891" />
                  <Point X="28.031884765625" Y="-1.654218994141" />
                  <Point X="28.07693359375" Y="-1.724286987305" />
                  <Point X="28.086935546875" Y="-1.737238647461" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.65151171875" Y="-2.175942138672" />
                  <Point X="29.213125" Y="-2.606883056641" />
                  <Point X="29.19403515625" Y="-2.6377734375" />
                  <Point X="29.1248125" Y="-2.749781738281" />
                  <Point X="29.030974609375" Y="-2.883114013672" />
                  <Point X="29.028982421875" Y="-2.885945068359" />
                  <Point X="28.54176171875" Y="-2.604648193359" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.635013671875" Y="-2.138295410156" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135176757812" />
                  <Point X="27.345796875" Y="-2.187299072266" />
                  <Point X="27.26531640625" Y="-2.229655761719" />
                  <Point X="27.242384765625" Y="-2.246549560547" />
                  <Point X="27.22142578125" Y="-2.267509033203" />
                  <Point X="27.204533203125" Y="-2.290438476562" />
                  <Point X="27.15241015625" Y="-2.389475097656" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.100228515625" Y="-2.499735107422" />
                  <Point X="27.095271484375" Y="-2.531908203125" />
                  <Point X="27.09567578125" Y="-2.563259521484" />
                  <Point X="27.11720703125" Y="-2.682472412109" />
                  <Point X="27.134703125" Y="-2.779349853516" />
                  <Point X="27.13898828125" Y="-2.795139648438" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.499392578125" Y="-3.428088623047" />
                  <Point X="27.861287109375" Y="-4.054906005859" />
                  <Point X="27.781865234375" Y="-4.111634277344" />
                  <Point X="27.701765625" Y="-4.163481445312" />
                  <Point X="27.3238515625" Y="-3.670972900391" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.604349609375" Y="-2.824966552734" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.288509765625" Y="-2.752949707031" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.0053046875" Y="-2.878020507812" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968860839844" />
                  <Point X="25.88725" Y="-2.996687255859" />
                  <Point X="25.875625" Y="-3.025808837891" />
                  <Point X="25.8459375" Y="-3.1623984375" />
                  <Point X="25.821810546875" Y="-3.273396728516" />
                  <Point X="25.819724609375" Y="-3.289627197266" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.9182421875" Y="-4.071293212891" />
                  <Point X="26.02206640625" Y="-4.859916015625" />
                  <Point X="25.97569140625" Y="-4.870081054688" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94157421875" Y="-4.752636230469" />
                  <Point X="23.85875390625" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575834960938" />
                  <Point X="23.879923828125" Y="-4.568098144531" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688964844" />
                  <Point X="23.8432734375" Y="-4.329816894531" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.811875" Y="-4.178468261719" />
                  <Point X="23.80097265625" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.74979296875" Y="-4.070937744141" />
                  <Point X="23.738994140625" Y="-4.059779296875" />
                  <Point X="23.61030859375" Y="-3.946924804688" />
                  <Point X="23.505734375" Y="-3.855214599609" />
                  <Point X="23.49326171875" Y="-3.845964599609" />
                  <Point X="23.466978515625" Y="-3.82962109375" />
                  <Point X="23.45316796875" Y="-3.822527587891" />
                  <Point X="23.423470703125" Y="-3.810226318359" />
                  <Point X="23.4086875" Y="-3.805476318359" />
                  <Point X="23.378544921875" Y="-3.798447998047" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.192390625" Y="-3.784975097656" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812255859" />
                  <Point X="22.762248046875" Y="-3.910904296875" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.619560546875" Y="-4.010130859375" />
                  <Point X="22.597244140625" Y="-4.032769287109" />
                  <Point X="22.589529296875" Y="-4.041628662109" />
                  <Point X="22.496796875" Y="-4.162477539063" />
                  <Point X="22.439" Y="-4.126690917969" />
                  <Point X="22.25240625" Y="-4.011157470703" />
                  <Point X="22.036212890625" Y="-3.8446953125" />
                  <Point X="22.01913671875" Y="-3.831547851562" />
                  <Point X="22.2478203125" Y="-3.435456787109" />
                  <Point X="22.65851171875" Y="-2.724119628906" />
                  <Point X="22.66515234375" Y="-2.710080322266" />
                  <Point X="22.676052734375" Y="-2.681115722656" />
                  <Point X="22.680314453125" Y="-2.666190429688" />
                  <Point X="22.6865859375" Y="-2.634664550781" />
                  <Point X="22.688361328125" Y="-2.619240478516" />
                  <Point X="22.689375" Y="-2.588306152344" />
                  <Point X="22.6853359375" Y="-2.557618652344" />
                  <Point X="22.6763515625" Y="-2.528000244141" />
                  <Point X="22.67064453125" Y="-2.513559082031" />
                  <Point X="22.656427734375" Y="-2.484730224609" />
                  <Point X="22.648447265625" Y="-2.471414550781" />
                  <Point X="22.630421875" Y="-2.446258544922" />
                  <Point X="22.620376953125" Y="-2.434418212891" />
                  <Point X="22.603029296875" Y="-2.417069580078" />
                  <Point X="22.5911875" Y="-2.407023193359" />
                  <Point X="22.566029296875" Y="-2.388994628906" />
                  <Point X="22.552712890625" Y="-2.381012451172" />
                  <Point X="22.523884765625" Y="-2.366795166016" />
                  <Point X="22.5094453125" Y="-2.361088378906" />
                  <Point X="22.479828125" Y="-2.352103271484" />
                  <Point X="22.449140625" Y="-2.348062744141" />
                  <Point X="22.418205078125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.735255859375" Y="-2.712669189453" />
                  <Point X="21.206912109375" Y="-3.017708740234" />
                  <Point X="21.143392578125" Y="-2.934258056641" />
                  <Point X="20.99598046875" Y="-2.7405859375" />
                  <Point X="20.840986328125" Y="-2.480685791016" />
                  <Point X="20.818734375" Y="-2.443373291016" />
                  <Point X="21.233302734375" Y="-2.125264404297" />
                  <Point X="21.951876953125" Y="-1.573882080078" />
                  <Point X="21.963515625" Y="-1.563311035156" />
                  <Point X="21.984888671875" Y="-1.540396362305" />
                  <Point X="21.994623046875" Y="-1.528052734375" />
                  <Point X="22.012587890625" Y="-1.50092565918" />
                  <Point X="22.02015625" Y="-1.487140625" />
                  <Point X="22.032916015625" Y="-1.458507446289" />
                  <Point X="22.038107421875" Y="-1.443659301758" />
                  <Point X="22.045814453125" Y="-1.413904785156" />
                  <Point X="22.04844921875" Y="-1.39880090332" />
                  <Point X="22.05125390625" Y="-1.368363891602" />
                  <Point X="22.051423828125" Y="-1.353030517578" />
                  <Point X="22.049212890625" Y="-1.321361206055" />
                  <Point X="22.046916015625" Y="-1.306207763672" />
                  <Point X="22.0399140625" Y="-1.276465454102" />
                  <Point X="22.02821875" Y="-1.248234985352" />
                  <Point X="22.01213671875" Y="-1.222253662109" />
                  <Point X="22.003044921875" Y="-1.20991394043" />
                  <Point X="21.982212890625" Y="-1.185959106445" />
                  <Point X="21.97125390625" Y="-1.175241821289" />
                  <Point X="21.947755859375" Y="-1.155709838867" />
                  <Point X="21.93521484375" Y="-1.14689465332" />
                  <Point X="21.908732421875" Y="-1.131308227539" />
                  <Point X="21.89456640625" Y="-1.12448046875" />
                  <Point X="21.86530078125" Y="-1.113256103516" />
                  <Point X="21.850203125" Y="-1.108859741211" />
                  <Point X="21.81831640625" Y="-1.102378295898" />
                  <Point X="21.802701171875" Y="-1.100532348633" />
                  <Point X="21.77137890625" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="21.025923828125" Y="-1.196269775391" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.316892578125" Y="-1.199830078125" />
                  <Point X="20.259236328125" Y="-0.974111633301" />
                  <Point X="20.21823046875" Y="-0.687393310547" />
                  <Point X="20.213548828125" Y="-0.654654602051" />
                  <Point X="20.6731484375" Y="-0.531505310059" />
                  <Point X="21.491712890625" Y="-0.312171325684" />
                  <Point X="21.5035" Y="-0.308168334961" />
                  <Point X="21.52644921875" Y="-0.298656158447" />
                  <Point X="21.537611328125" Y="-0.293147155762" />
                  <Point X="21.56848828125" Y="-0.275301849365" />
                  <Point X="21.57526171875" Y="-0.270995361328" />
                  <Point X="21.5948203125" Y="-0.256953613281" />
                  <Point X="21.619083984375" Y="-0.236798568726" />
                  <Point X="21.627650390625" Y="-0.228735031128" />
                  <Point X="21.64371875" Y="-0.211614562988" />
                  <Point X="21.657548828125" Y="-0.192640029907" />
                  <Point X="21.668927734375" Y="-0.172101242065" />
                  <Point X="21.673978515625" Y="-0.161480499268" />
                  <Point X="21.6865078125" Y="-0.130296585083" />
                  <Point X="21.689041015625" Y="-0.12318914032" />
                  <Point X="21.6955" Y="-0.101492874146" />
                  <Point X="21.701892578125" Y="-0.073312042236" />
                  <Point X="21.70367578125" Y="-0.062686840057" />
                  <Point X="21.706029296875" Y="-0.041298370361" />
                  <Point X="21.7059453125" Y="-0.019775247574" />
                  <Point X="21.703423828125" Y="0.001594049692" />
                  <Point X="21.701556640625" Y="0.012203347206" />
                  <Point X="21.69420703125" Y="0.043464984894" />
                  <Point X="21.692177734375" Y="0.050773517609" />
                  <Point X="21.6849375" Y="0.072326065063" />
                  <Point X="21.673365234375" Y="0.100429321289" />
                  <Point X="21.66822265625" Y="0.111006217957" />
                  <Point X="21.65666796875" Y="0.131446914673" />
                  <Point X="21.642671875" Y="0.150306869507" />
                  <Point X="21.626455078125" Y="0.167289108276" />
                  <Point X="21.617822265625" Y="0.175275222778" />
                  <Point X="21.59069140625" Y="0.197421066284" />
                  <Point X="21.584248046875" Y="0.202241500854" />
                  <Point X="21.564140625" Y="0.215579376221" />
                  <Point X="21.536130859375" Y="0.231434020996" />
                  <Point X="21.525314453125" Y="0.236682342529" />
                  <Point X="21.50310546875" Y="0.2457709198" />
                  <Point X="21.491712890625" Y="0.249611190796" />
                  <Point X="20.826513671875" Y="0.427851226807" />
                  <Point X="20.2145546875" Y="0.591824951172" />
                  <Point X="20.231513671875" Y="0.706436096191" />
                  <Point X="20.26866796875" Y="0.957523254395" />
                  <Point X="20.35121875" Y="1.262161621094" />
                  <Point X="20.3664140625" Y="1.318237182617" />
                  <Point X="20.635166015625" Y="1.282855102539" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.204703369141" />
                  <Point X="21.28485546875" Y="1.20658972168" />
                  <Point X="21.295109375" Y="1.208053344727" />
                  <Point X="21.3153984375" Y="1.212088989258" />
                  <Point X="21.32543359375" Y="1.214660766602" />
                  <Point X="21.3593203125" Y="1.225345458984" />
                  <Point X="21.386859375" Y="1.234028320312" />
                  <Point X="21.3965546875" Y="1.237677612305" />
                  <Point X="21.41548828125" Y="1.246009155273" />
                  <Point X="21.4247265625" Y="1.25069140625" />
                  <Point X="21.443470703125" Y="1.261513916016" />
                  <Point X="21.452142578125" Y="1.267172241211" />
                  <Point X="21.468822265625" Y="1.279401977539" />
                  <Point X="21.484072265625" Y="1.293376586914" />
                  <Point X="21.497708984375" Y="1.308926025391" />
                  <Point X="21.504103515625" Y="1.317072387695" />
                  <Point X="21.51651953125" Y="1.334803344727" />
                  <Point X="21.5219921875" Y="1.343605102539" />
                  <Point X="21.53194140625" Y="1.36174206543" />
                  <Point X="21.53641796875" Y="1.371077148438" />
                  <Point X="21.550015625" Y="1.403904663086" />
                  <Point X="21.561064453125" Y="1.430581542969" />
                  <Point X="21.564501953125" Y="1.440354492188" />
                  <Point X="21.57029296875" Y="1.460224121094" />
                  <Point X="21.572646484375" Y="1.47032043457" />
                  <Point X="21.57640234375" Y="1.491635742188" />
                  <Point X="21.577640625" Y="1.501921020508" />
                  <Point X="21.578990234375" Y="1.522555541992" />
                  <Point X="21.578087890625" Y="1.543202392578" />
                  <Point X="21.574943359375" Y="1.563640258789" />
                  <Point X="21.5728125" Y="1.573780395508" />
                  <Point X="21.5672109375" Y="1.594686767578" />
                  <Point X="21.56398828125" Y="1.604529052734" />
                  <Point X="21.5564921875" Y="1.623806884766" />
                  <Point X="21.55221875" Y="1.633242431641" />
                  <Point X="21.5358125" Y="1.664759765625" />
                  <Point X="21.522478515625" Y="1.690371948242" />
                  <Point X="21.517201171875" Y="1.699286499023" />
                  <Point X="21.50570703125" Y="1.716488769531" />
                  <Point X="21.499490234375" Y="1.724776611328" />
                  <Point X="21.485578125" Y="1.74135546875" />
                  <Point X="21.478494140625" Y="1.748916625977" />
                  <Point X="21.463552734375" Y="1.763218383789" />
                  <Point X="21.4556953125" Y="1.769958862305" />
                  <Point X="21.074134765625" Y="2.062740722656" />
                  <Point X="20.77238671875" Y="2.294281005859" />
                  <Point X="20.853337890625" Y="2.432970458984" />
                  <Point X="20.99771484375" Y="2.680322265625" />
                  <Point X="21.216373046875" Y="2.961376220703" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.385466796875" Y="2.970462402344" />
                  <Point X="21.74584375" Y="2.762398925781" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774015625" Y="2.749385742188" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.892123046875" Y="2.727028808594" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.961505859375" Y="2.723785644531" />
                  <Point X="21.97183203125" Y="2.724575683594" />
                  <Point X="21.993291015625" Y="2.727400634766" />
                  <Point X="22.00347265625" Y="2.729310302734" />
                  <Point X="22.023564453125" Y="2.7342265625" />
                  <Point X="22.042998046875" Y="2.741299316406" />
                  <Point X="22.06155078125" Y="2.750447509766" />
                  <Point X="22.070580078125" Y="2.755529541016" />
                  <Point X="22.088833984375" Y="2.767158203125" />
                  <Point X="22.09725390625" Y="2.773191650391" />
                  <Point X="22.11338671875" Y="2.786139404297" />
                  <Point X="22.121099609375" Y="2.793053710938" />
                  <Point X="22.154599609375" Y="2.826553710938" />
                  <Point X="22.18182421875" Y="2.853777099609" />
                  <Point X="22.188740234375" Y="2.861492431641" />
                  <Point X="22.2016875" Y="2.877625732422" />
                  <Point X="22.20771875" Y="2.886043701172" />
                  <Point X="22.21934765625" Y="2.904298339844" />
                  <Point X="22.2244296875" Y="2.913325683594" />
                  <Point X="22.233576171875" Y="2.931874511719" />
                  <Point X="22.240646484375" Y="2.951298828125" />
                  <Point X="22.245564453125" Y="2.971387451172" />
                  <Point X="22.2474765625" Y="2.981573242188" />
                  <Point X="22.250302734375" Y="3.003032470703" />
                  <Point X="22.251091796875" Y="3.013364990234" />
                  <Point X="22.25154296875" Y="3.034048583984" />
                  <Point X="22.251205078125" Y="3.044399658203" />
                  <Point X="22.247076171875" Y="3.091595703125" />
                  <Point X="22.243720703125" Y="3.12994921875" />
                  <Point X="22.242255859375" Y="3.140206298828" />
                  <Point X="22.23821875" Y="3.160499023438" />
                  <Point X="22.235646484375" Y="3.170534667969" />
                  <Point X="22.22913671875" Y="3.191177001953" />
                  <Point X="22.225486328125" Y="3.200871582031" />
                  <Point X="22.21715625" Y="3.219799804688" />
                  <Point X="22.2124765625" Y="3.229033447266" />
                  <Point X="22.04339453125" Y="3.521890380859" />
                  <Point X="21.94061328125" Y="3.699915039062" />
                  <Point X="22.100185546875" Y="3.822258789062" />
                  <Point X="22.3516328125" Y="4.015041015625" />
                  <Point X="22.69601171875" Y="4.206370117188" />
                  <Point X="22.807474609375" Y="4.268296386719" />
                  <Point X="22.88143359375" Y="4.171911621094" />
                  <Point X="22.8881796875" Y="4.164048828125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93490625" Y="4.121898925781" />
                  <Point X="22.95210546875" Y="4.11040625" />
                  <Point X="22.96101953125" Y="4.105128417969" />
                  <Point X="23.013548828125" Y="4.077783447266" />
                  <Point X="23.056236328125" Y="4.055561767578" />
                  <Point X="23.065673828125" Y="4.051285644531" />
                  <Point X="23.084953125" Y="4.0437890625" />
                  <Point X="23.094794921875" Y="4.040568359375" />
                  <Point X="23.115701171875" Y="4.034966308594" />
                  <Point X="23.1258359375" Y="4.032834960938" />
                  <Point X="23.146279296875" Y="4.029687988281" />
                  <Point X="23.166947265625" Y="4.028785400391" />
                  <Point X="23.187587890625" Y="4.030138427734" />
                  <Point X="23.197869140625" Y="4.031378417969" />
                  <Point X="23.21918359375" Y="4.03513671875" />
                  <Point X="23.229271484375" Y="4.037488769531" />
                  <Point X="23.249130859375" Y="4.04327734375" />
                  <Point X="23.25890234375" Y="4.046713867188" />
                  <Point X="23.31361328125" Y="4.069376953125" />
                  <Point X="23.358076171875" Y="4.087793457031" />
                  <Point X="23.3674140625" Y="4.092272216797" />
                  <Point X="23.385544921875" Y="4.10221875" />
                  <Point X="23.394337890625" Y="4.107687011719" />
                  <Point X="23.412068359375" Y="4.120101074219" />
                  <Point X="23.420216796875" Y="4.126494628906" />
                  <Point X="23.43576953125" Y="4.1401328125" />
                  <Point X="23.449744140625" Y="4.155382324219" />
                  <Point X="23.4619765625" Y="4.172063476562" />
                  <Point X="23.467638671875" Y="4.180739746094" />
                  <Point X="23.4784609375" Y="4.199483398437" />
                  <Point X="23.48314453125" Y="4.208725097656" />
                  <Point X="23.4914765625" Y="4.227661621094" />
                  <Point X="23.495125" Y="4.237356445312" />
                  <Point X="23.512931640625" Y="4.2938359375" />
                  <Point X="23.527404296875" Y="4.339732910156" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370048339844" />
                  <Point X="23.535474609375" Y="4.380302246094" />
                  <Point X="23.537361328125" Y="4.401865234375" />
                  <Point X="23.53769921875" Y="4.412218261719" />
                  <Point X="23.537248046875" Y="4.432898925781" />
                  <Point X="23.536458984375" Y="4.4432265625" />
                  <Point X="23.520736328125" Y="4.562655761719" />
                  <Point X="23.7433203125" Y="4.625060546875" />
                  <Point X="24.068826171875" Y="4.7163203125" />
                  <Point X="24.48631640625" Y="4.765181640625" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.66772265625" Y="4.659600097656" />
                  <Point X="24.77433203125" Y="4.261728515625" />
                  <Point X="24.779564453125" Y="4.247106933594" />
                  <Point X="24.79233984375" Y="4.218912597656" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.247104980469" />
                  <Point X="25.23798046875" Y="4.2617265625" />
                  <Point X="25.3246171875" Y="4.585053710938" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.543654296875" Y="4.767678222656" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.1732890625" Y="4.654519042969" />
                  <Point X="26.45359765625" Y="4.586843261719" />
                  <Point X="26.676791015625" Y="4.505890136719" />
                  <Point X="26.85826171875" Y="4.440069335938" />
                  <Point X="27.075662109375" Y="4.338397949219" />
                  <Point X="27.25044921875" Y="4.256654785156" />
                  <Point X="27.46052734375" Y="4.134264160156" />
                  <Point X="27.6294296875" Y="4.035861572266" />
                  <Point X="27.81778125" Y="3.901916259766" />
                  <Point X="27.540654296875" Y="3.421917480469" />
                  <Point X="27.065310546875" Y="2.598597900391" />
                  <Point X="27.062380859375" Y="2.593115234375" />
                  <Point X="27.053185546875" Y="2.573443603516" />
                  <Point X="27.044185546875" Y="2.549569335938" />
                  <Point X="27.041302734375" Y="2.540603271484" />
                  <Point X="27.02945703125" Y="2.496310791016" />
                  <Point X="27.01983203125" Y="2.460316894531" />
                  <Point X="27.0179140625" Y="2.451469726562" />
                  <Point X="27.013646484375" Y="2.420223144531" />
                  <Point X="27.012755859375" Y="2.383239746094" />
                  <Point X="27.013412109375" Y="2.369577636719" />
                  <Point X="27.01803125" Y="2.33127734375" />
                  <Point X="27.021783203125" Y="2.300152832031" />
                  <Point X="27.023802734375" Y="2.289029785156" />
                  <Point X="27.029146484375" Y="2.267107177734" />
                  <Point X="27.032470703125" Y="2.256307617188" />
                  <Point X="27.04073828125" Y="2.234216064453" />
                  <Point X="27.0453203125" Y="2.223889160156" />
                  <Point X="27.055681640625" Y="2.203845214844" />
                  <Point X="27.0614609375" Y="2.194128173828" />
                  <Point X="27.08516015625" Y="2.159201904297" />
                  <Point X="27.104419921875" Y="2.130819580078" />
                  <Point X="27.109810546875" Y="2.123631347656" />
                  <Point X="27.13046484375" Y="2.100123046875" />
                  <Point X="27.15759765625" Y="2.075387695312" />
                  <Point X="27.1682578125" Y="2.066981933594" />
                  <Point X="27.20318359375" Y="2.043283081055" />
                  <Point X="27.23156640625" Y="2.024024414062" />
                  <Point X="27.241279296875" Y="2.01824597168" />
                  <Point X="27.26132421875" Y="2.00788293457" />
                  <Point X="27.27165625" Y="2.003298461914" />
                  <Point X="27.293744140625" Y="1.995032470703" />
                  <Point X="27.304546875" Y="1.991707763672" />
                  <Point X="27.326470703125" Y="1.986364746094" />
                  <Point X="27.337591796875" Y="1.984346557617" />
                  <Point X="27.375892578125" Y="1.979728149414" />
                  <Point X="27.407015625" Y="1.975974975586" />
                  <Point X="27.416044921875" Y="1.975320800781" />
                  <Point X="27.44757421875" Y="1.975497436523" />
                  <Point X="27.4843125" Y="1.979822875977" />
                  <Point X="27.49774609375" Y="1.982395996094" />
                  <Point X="27.5420390625" Y="1.994240234375" />
                  <Point X="27.578033203125" Y="2.003865600586" />
                  <Point X="27.5839921875" Y="2.005670288086" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572753906" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.30509765625" Y="2.414156494141" />
                  <Point X="28.940404296875" Y="2.780950683594" />
                  <Point X="28.943759765625" Y="2.776287109375" />
                  <Point X="29.04396484375" Y="2.637026123047" />
                  <Point X="29.13688671875" Y="2.483471679688" />
                  <Point X="28.7987890625" Y="2.224039306641" />
                  <Point X="28.172953125" Y="1.743819824219" />
                  <Point X="28.168140625" Y="1.739869506836" />
                  <Point X="28.152125" Y="1.725222045898" />
                  <Point X="28.134669921875" Y="1.70660559082" />
                  <Point X="28.12857421875" Y="1.699421142578" />
                  <Point X="28.096697265625" Y="1.657834716797" />
                  <Point X="28.07079296875" Y="1.624039794922" />
                  <Point X="28.06563671875" Y="1.616602416992" />
                  <Point X="28.04973828125" Y="1.589370605469" />
                  <Point X="28.034763671875" Y="1.555548095703" />
                  <Point X="28.030140625" Y="1.54267578125" />
                  <Point X="28.018265625" Y="1.500215820312" />
                  <Point X="28.008615234375" Y="1.465711303711" />
                  <Point X="28.006224609375" Y="1.454661987305" />
                  <Point X="28.002771484375" Y="1.432362915039" />
                  <Point X="28.001708984375" Y="1.42111328125" />
                  <Point X="28.000892578125" Y="1.397541381836" />
                  <Point X="28.001173828125" Y="1.386236450195" />
                  <Point X="28.003078125" Y="1.363750244141" />
                  <Point X="28.004701171875" Y="1.352568847656" />
                  <Point X="28.01444921875" Y="1.305326416016" />
                  <Point X="28.02237109375" Y="1.266935791016" />
                  <Point X="28.02459765625" Y="1.258239135742" />
                  <Point X="28.03468359375" Y="1.228611694336" />
                  <Point X="28.05028515625" Y="1.195374511719" />
                  <Point X="28.056916015625" Y="1.183528076172" />
                  <Point X="28.083427734375" Y="1.14323034668" />
                  <Point X="28.10497265625" Y="1.110482788086" />
                  <Point X="28.111740234375" Y="1.101422851562" />
                  <Point X="28.126296875" Y="1.08417578125" />
                  <Point X="28.1340859375" Y="1.075988891602" />
                  <Point X="28.151330078125" Y="1.059899414062" />
                  <Point X="28.16003515625" Y="1.052695800781" />
                  <Point X="28.178244140625" Y="1.039370239258" />
                  <Point X="28.187748046875" Y="1.033248291016" />
                  <Point X="28.226169921875" Y="1.011621154785" />
                  <Point X="28.257390625" Y="0.994045898438" />
                  <Point X="28.265482421875" Y="0.98998626709" />
                  <Point X="28.294677734375" Y="0.978084533691" />
                  <Point X="28.330275390625" Y="0.96802130127" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.395619140625" Y="0.958392028809" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="29.136169921875" Y="1.036471069336" />
                  <Point X="29.704703125" Y="1.111319946289" />
                  <Point X="29.70965234375" Y="1.090992431641" />
                  <Point X="29.7526875" Y="0.914206726074" />
                  <Point X="29.783873046875" Y="0.713921203613" />
                  <Point X="29.409212890625" Y="0.613531188965" />
                  <Point X="28.6919921875" Y="0.42135269165" />
                  <Point X="28.68603125" Y="0.419543792725" />
                  <Point X="28.665625" Y="0.412137023926" />
                  <Point X="28.642380859375" Y="0.401618682861" />
                  <Point X="28.634005859375" Y="0.397316040039" />
                  <Point X="28.582970703125" Y="0.367816345215" />
                  <Point X="28.54149609375" Y="0.343843597412" />
                  <Point X="28.533884765625" Y="0.338946777344" />
                  <Point X="28.508779296875" Y="0.319873413086" />
                  <Point X="28.48199609375" Y="0.294351898193" />
                  <Point X="28.472796875" Y="0.284225769043" />
                  <Point X="28.44217578125" Y="0.245206451416" />
                  <Point X="28.417291015625" Y="0.213497909546" />
                  <Point X="28.41085546875" Y="0.204207794189" />
                  <Point X="28.399130859375" Y="0.18492678833" />
                  <Point X="28.393841796875" Y="0.174936065674" />
                  <Point X="28.384068359375" Y="0.153472091675" />
                  <Point X="28.380005859375" Y="0.142926544189" />
                  <Point X="28.37316015625" Y="0.121426612854" />
                  <Point X="28.370376953125" Y="0.110472366333" />
                  <Point X="28.360169921875" Y="0.057174259186" />
                  <Point X="28.351875" Y="0.013861829758" />
                  <Point X="28.350603515625" Y="0.004966303349" />
                  <Point X="28.3485859375" Y="-0.02626159668" />
                  <Point X="28.35028125" Y="-0.062939647675" />
                  <Point X="28.351875" Y="-0.076421829224" />
                  <Point X="28.36208203125" Y="-0.129720077515" />
                  <Point X="28.370376953125" Y="-0.173032501221" />
                  <Point X="28.37316015625" Y="-0.183986755371" />
                  <Point X="28.380005859375" Y="-0.205486694336" />
                  <Point X="28.384068359375" Y="-0.216032241821" />
                  <Point X="28.393841796875" Y="-0.237496063232" />
                  <Point X="28.399130859375" Y="-0.247487228394" />
                  <Point X="28.41085546875" Y="-0.266768066406" />
                  <Point X="28.417291015625" Y="-0.276057891846" />
                  <Point X="28.447912109375" Y="-0.315077209473" />
                  <Point X="28.472796875" Y="-0.346785919189" />
                  <Point X="28.478720703125" Y="-0.353635192871" />
                  <Point X="28.501142578125" Y="-0.375807647705" />
                  <Point X="28.530177734375" Y="-0.398725463867" />
                  <Point X="28.54149609375" Y="-0.406404052734" />
                  <Point X="28.59253125" Y="-0.435903625488" />
                  <Point X="28.634005859375" Y="-0.459876342773" />
                  <Point X="28.63949609375" Y="-0.462814483643" />
                  <Point X="28.659158203125" Y="-0.472016021729" />
                  <Point X="28.68302734375" Y="-0.481027160645" />
                  <Point X="28.6919921875" Y="-0.483912963867" />
                  <Point X="29.2748359375" Y="-0.640085021973" />
                  <Point X="29.78487890625" Y="-0.776750732422" />
                  <Point X="29.761619140625" Y="-0.931038513184" />
                  <Point X="29.727802734375" Y="-1.079219482422" />
                  <Point X="29.268556640625" Y="-1.018758666992" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042480469" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.25431640625" Y="-0.934447998047" />
                  <Point X="28.17291796875" Y="-0.952140075684" />
                  <Point X="28.157876953125" Y="-0.95674230957" />
                  <Point X="28.128755859375" Y="-0.968366699219" />
                  <Point X="28.114677734375" Y="-0.975388977051" />
                  <Point X="28.0868515625" Y="-0.992280883789" />
                  <Point X="28.074125" Y="-1.001530883789" />
                  <Point X="28.050375" Y="-1.022002197266" />
                  <Point X="28.039349609375" Y="-1.033223510742" />
                  <Point X="27.978806640625" Y="-1.106038452148" />
                  <Point X="27.92960546875" Y="-1.165211181641" />
                  <Point X="27.92132421875" Y="-1.176851318359" />
                  <Point X="27.90660546875" Y="-1.201233276367" />
                  <Point X="27.90016796875" Y="-1.213974975586" />
                  <Point X="27.88882421875" Y="-1.241361328125" />
                  <Point X="27.884365234375" Y="-1.254927612305" />
                  <Point X="27.877533203125" Y="-1.282577026367" />
                  <Point X="27.87516015625" Y="-1.29666027832" />
                  <Point X="27.866482421875" Y="-1.390959594727" />
                  <Point X="27.859431640625" Y="-1.467590576172" />
                  <Point X="27.859291015625" Y="-1.483319702148" />
                  <Point X="27.861609375" Y="-1.514591064453" />
                  <Point X="27.864068359375" Y="-1.530133300781" />
                  <Point X="27.87180078125" Y="-1.561754394531" />
                  <Point X="27.876791015625" Y="-1.576674194336" />
                  <Point X="27.889162109375" Y="-1.605482177734" />
                  <Point X="27.89654296875" Y="-1.619370117188" />
                  <Point X="27.951974609375" Y="-1.705592773438" />
                  <Point X="27.9970234375" Y="-1.775660766602" />
                  <Point X="28.001744140625" Y="-1.782352172852" />
                  <Point X="28.019794921875" Y="-1.804448974609" />
                  <Point X="28.043490234375" Y="-1.828119628906" />
                  <Point X="28.052798828125" Y="-1.836277587891" />
                  <Point X="28.5936796875" Y="-2.251310791016" />
                  <Point X="29.087171875" Y="-2.629980712891" />
                  <Point X="29.0454765625" Y="-2.697447998047" />
                  <Point X="29.001275390625" Y="-2.760251953125" />
                  <Point X="28.58926171875" Y="-2.522375732422" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.6518984375" Y="-2.044807739258" />
                  <Point X="27.555021484375" Y="-2.027311889648" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.035136474609" />
                  <Point X="27.415068359375" Y="-2.044960083008" />
                  <Point X="27.40058984375" Y="-2.051108642578" />
                  <Point X="27.301552734375" Y="-2.103230957031" />
                  <Point X="27.221072265625" Y="-2.145587646484" />
                  <Point X="27.20896875" Y="-2.153170410156" />
                  <Point X="27.186037109375" Y="-2.170064208984" />
                  <Point X="27.175208984375" Y="-2.179375244141" />
                  <Point X="27.15425" Y="-2.200334716797" />
                  <Point X="27.14494140625" Y="-2.211161132813" />
                  <Point X="27.128048828125" Y="-2.234090576172" />
                  <Point X="27.12046484375" Y="-2.246193603516" />
                  <Point X="27.068341796875" Y="-2.345230224609" />
                  <Point X="27.025984375" Y="-2.425711669922" />
                  <Point X="27.0198359375" Y="-2.440193115234" />
                  <Point X="27.01001171875" Y="-2.469971679688" />
                  <Point X="27.0063359375" Y="-2.485268798828" />
                  <Point X="27.00137890625" Y="-2.517441894531" />
                  <Point X="27.000279296875" Y="-2.533133300781" />
                  <Point X="27.00068359375" Y="-2.564484619141" />
                  <Point X="27.0021875" Y="-2.58014453125" />
                  <Point X="27.02371875" Y="-2.699357421875" />
                  <Point X="27.04121484375" Y="-2.796234863281" />
                  <Point X="27.04301953125" Y="-2.804231689453" />
                  <Point X="27.05123828125" Y="-2.831539794922" />
                  <Point X="27.064072265625" Y="-2.862478759766" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.41712109375" Y="-3.475588623047" />
                  <Point X="27.735896484375" Y="-4.027722900391" />
                  <Point X="27.723755859375" Y="-4.036083496094" />
                  <Point X="27.399220703125" Y="-3.613140625" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.655724609375" Y="-2.745056396484" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.2798046875" Y="-2.658349365234" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.944568359375" Y="-2.80497265625" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883087158203" />
                  <Point X="25.83218359375" Y="-2.906836425781" />
                  <Point X="25.822935546875" Y="-2.919562255859" />
                  <Point X="25.80604296875" Y="-2.947388671875" />
                  <Point X="25.79901953125" Y="-2.961466796875" />
                  <Point X="25.78739453125" Y="-2.990588378906" />
                  <Point X="25.78279296875" Y="-3.005631835938" />
                  <Point X="25.75310546875" Y="-3.142221435547" />
                  <Point X="25.728978515625" Y="-3.253219726562" />
                  <Point X="25.7275859375" Y="-3.261286865234" />
                  <Point X="25.724724609375" Y="-3.289677001953" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520263672" />
                  <Point X="25.8240546875" Y="-4.083693359375" />
                  <Point X="25.83308984375" Y="-4.152326660156" />
                  <Point X="25.655068359375" Y="-3.487936767578" />
                  <Point X="25.652607421875" Y="-3.480121582031" />
                  <Point X="25.642146484375" Y="-3.453578613281" />
                  <Point X="25.6267890625" Y="-3.423815429688" />
                  <Point X="25.62041015625" Y="-3.413209716797" />
                  <Point X="25.530083984375" Y="-3.283067871094" />
                  <Point X="25.456681640625" Y="-3.177309326172" />
                  <Point X="25.446671875" Y="-3.165172119141" />
                  <Point X="25.424787109375" Y="-3.142716308594" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104937255859" />
                  <Point X="25.3452421875" Y="-3.090829589844" />
                  <Point X="25.33065625" Y="-3.084938476562" />
                  <Point X="25.1908828125" Y="-3.041557861328" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.063376953125" Y="-3.003109130859" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.7952421875" Y="-3.049685546875" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.6670703125" Y="-3.090829589844" />
                  <Point X="24.6390703125" Y="-3.104937255859" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.587525390625" Y="-3.142717285156" />
                  <Point X="24.565640625" Y="-3.165173828125" />
                  <Point X="24.555630859375" Y="-3.177310791016" />
                  <Point X="24.465306640625" Y="-3.307452392578" />
                  <Point X="24.391904296875" Y="-3.413211181641" />
                  <Point X="24.38753125" Y="-3.420132080078" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.177658203125" Y="-4.158168457031" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.871151091829" Y="-4.637169478091" />
                  <Point X="24.038151739148" Y="-4.678807415961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.880537550575" Y="-4.541601490316" />
                  <Point X="24.062743570683" Y="-4.587030553416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.32432053855" Y="-4.055684715929" />
                  <Point X="22.537884474877" Y="-4.108932185653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.865162831602" Y="-4.439859847552" />
                  <Point X="24.087335402218" Y="-4.49525369087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.115437949973" Y="-3.905696142502" />
                  <Point X="22.602739947866" Y="-4.027194176418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.844670917715" Y="-4.3368423448" />
                  <Point X="24.111927233753" Y="-4.403476828325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.043246522515" Y="-3.789788503283" />
                  <Point X="22.698868663774" Y="-3.953253462383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.824178560306" Y="-4.233824731465" />
                  <Point X="24.136519065288" Y="-4.31169996578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.092660804476" Y="-3.704200572728" />
                  <Point X="22.805579289299" Y="-3.881951114739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.789618849403" Y="-4.127299732979" />
                  <Point X="24.161110896823" Y="-4.219923103234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.142075086436" Y="-3.618612642173" />
                  <Point X="22.913369335962" Y="-3.810917897012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.670468746095" Y="-3.999683980894" />
                  <Point X="24.185702628783" Y="-4.128146215862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.191489368397" Y="-3.533024711618" />
                  <Point X="23.205436779587" Y="-3.785830194638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.514478289916" Y="-3.862882897204" />
                  <Point X="24.210294155919" Y="-4.036369277421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.240903650358" Y="-3.447436781063" />
                  <Point X="24.234885683056" Y="-3.944592338981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.290317945691" Y="-3.361848853842" />
                  <Point X="24.259477210192" Y="-3.85281540054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.18446713594" Y="-2.988220985216" />
                  <Point X="21.235812816605" Y="-3.001022901231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.339732243201" Y="-3.276260927164" />
                  <Point X="24.284068737328" Y="-3.7610384621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.831632989067" Y="-4.146889566257" />
                  <Point X="25.832399233505" Y="-4.147080612453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.0924887665" Y="-2.867379907271" />
                  <Point X="21.354248568106" Y="-2.93264395583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.389146540711" Y="-3.190673000486" />
                  <Point X="24.308660264464" Y="-3.669261523659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.803520628962" Y="-4.041972072869" />
                  <Point X="25.819072399592" Y="-4.045849564781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.000511719568" Y="-2.746539159064" />
                  <Point X="21.472684319608" Y="-2.864265010429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.438560838222" Y="-3.105085073808" />
                  <Point X="24.333251791601" Y="-3.577484585219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.775408268858" Y="-3.937054579481" />
                  <Point X="25.80574490288" Y="-3.944618351855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.930772654618" Y="-2.631242962491" />
                  <Point X="21.591120071109" Y="-2.795886065028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.487975135732" Y="-3.01949714713" />
                  <Point X="24.357989757524" Y="-3.485744158069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.747295908753" Y="-3.832137086093" />
                  <Point X="25.792417406168" Y="-3.843387138928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.86218591349" Y="-2.516234072516" />
                  <Point X="21.70955582261" Y="-2.727507119626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.537389433242" Y="-2.933909220452" />
                  <Point X="24.401915005867" Y="-3.398787657724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.719183548649" Y="-3.727219592704" />
                  <Point X="25.779089909455" Y="-3.742155926002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.854027813115" Y="-2.416291734854" />
                  <Point X="21.827991634912" Y="-2.659128189384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.586803730752" Y="-2.848321293773" />
                  <Point X="24.459844197235" Y="-3.315322732526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.691071188545" Y="-3.622302099316" />
                  <Point X="25.765762412743" Y="-3.640924713075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.950332297855" Y="-2.342394844911" />
                  <Point X="21.946427464063" Y="-2.590749263344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.636218028262" Y="-2.762733367095" />
                  <Point X="24.517772833414" Y="-3.231857668904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.66295882844" Y="-3.517384605928" />
                  <Point X="25.752434916031" Y="-3.539693500149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.719121180228" Y="-4.03004345862" />
                  <Point X="27.728963339472" Y="-4.032497384528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.046636782595" Y="-2.268497954968" />
                  <Point X="22.064863293215" Y="-2.522370337303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.677747718866" Y="-2.675179587124" />
                  <Point X="24.58076438647" Y="-3.149654932235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.616765936937" Y="-3.407959129755" />
                  <Point X="25.739107419318" Y="-3.438462287222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.626220128346" Y="-3.908972330104" />
                  <Point X="27.674249522212" Y="-3.920947402955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.142941267335" Y="-2.194601065025" />
                  <Point X="22.183299122367" Y="-2.453991411262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.688267832784" Y="-2.579894251329" />
                  <Point X="24.696015360626" Y="-3.080481932659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.534591627776" Y="-3.289562478579" />
                  <Point X="25.725779922606" Y="-3.337231074296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.533319076465" Y="-3.787901201589" />
                  <Point X="27.608216626543" Y="-3.806575258167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.239245735968" Y="-2.120704171066" />
                  <Point X="22.301734951518" Y="-2.385612485221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.64890035265" Y="-2.472170541342" />
                  <Point X="24.870948164313" Y="-3.026189284446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.451407306518" Y="-3.170914003104" />
                  <Point X="25.731679476797" Y="-3.240793703572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.440418024584" Y="-3.666830073073" />
                  <Point X="27.542183730874" Y="-3.69220311338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.335549959711" Y="-2.046807216049" />
                  <Point X="25.751867083001" Y="-3.147918744321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.347516987732" Y="-3.545758948304" />
                  <Point X="27.476150835204" Y="-3.577830968592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.431854183454" Y="-1.972910261032" />
                  <Point X="25.772053439574" Y="-3.055043473501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.254615962854" Y="-3.424687826521" />
                  <Point X="27.410117960539" Y="-3.463458829042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.528158407197" Y="-1.899013306015" />
                  <Point X="25.798151184368" Y="-2.963642077301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.161714937977" Y="-3.303616704738" />
                  <Point X="27.344085262912" Y="-3.349086733632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.62446263094" Y="-1.825116350997" />
                  <Point X="25.855710723681" Y="-2.880084987495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.068813913099" Y="-3.182545582955" />
                  <Point X="27.278052565286" Y="-3.234714638222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.720766854682" Y="-1.75121939598" />
                  <Point X="25.945161685319" Y="-2.804479322324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.975912888222" Y="-3.061474461172" />
                  <Point X="27.21201986766" Y="-3.120342542813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.817071078425" Y="-1.677322440963" />
                  <Point X="26.035750408414" Y="-2.729157332945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.883011863344" Y="-2.940403339389" />
                  <Point X="27.145987170034" Y="-3.005970447403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.318406097857" Y="-1.205754999639" />
                  <Point X="20.538000903853" Y="-1.260506134053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.913375302168" Y="-1.603425485946" />
                  <Point X="26.180934868133" Y="-2.667447589542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.759311291852" Y="-2.81165302816" />
                  <Point X="27.079954472407" Y="-2.891598351993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.291695763051" Y="-1.101187070418" />
                  <Point X="20.794991087937" Y="-1.226672688612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.995917969525" Y="-1.526097389559" />
                  <Point X="27.038906933353" Y="-2.783455756271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.264985525546" Y="-0.996619165457" />
                  <Point X="21.051981287304" Y="-1.192839246982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.039312084726" Y="-1.439008462849" />
                  <Point X="27.020390693402" Y="-2.680930844355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.247841333564" Y="-0.894436343522" />
                  <Point X="21.308971622113" Y="-1.159005839122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.050790733603" Y="-1.343962116661" />
                  <Point X="27.002024116512" Y="-2.578443247632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.233320918075" Y="-0.792907702539" />
                  <Point X="21.565961956922" Y="-1.125172431262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.022502993761" Y="-1.239000896193" />
                  <Point X="27.007165420774" Y="-2.481816823968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.218800502586" Y="-0.691379061557" />
                  <Point X="27.043250279995" Y="-2.392905495062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.33436072371" Y="-0.622283165909" />
                  <Point X="27.088802096468" Y="-2.306354543701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.523637028895" Y="-0.571566754279" />
                  <Point X="27.137919406603" Y="-2.220692569753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.772716064265" Y="-2.628293155463" />
                  <Point X="29.046091317569" Y="-2.696453261396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.712913297159" Y="-0.520850333442" />
                  <Point X="27.22374241324" Y="-2.144182353808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.474235528096" Y="-2.455965304704" />
                  <Point X="29.046258264033" Y="-2.598586591036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.902189426603" Y="-0.470133877994" />
                  <Point X="27.349974212018" Y="-2.077747181304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.175755094125" Y="-2.283637479426" />
                  <Point X="28.857245777147" Y="-2.45355219038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.091465556048" Y="-0.419417422546" />
                  <Point X="27.534070604432" Y="-2.025739272367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.877274660154" Y="-2.111309654148" />
                  <Point X="28.668233290261" Y="-2.308517789724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.280741685493" Y="-0.368700967098" />
                  <Point X="28.479221076998" Y="-2.16348345729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.470017814937" Y="-0.31798451165" />
                  <Point X="28.29020904196" Y="-2.018449169293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.600154259483" Y="-0.252522876678" />
                  <Point X="28.101197006922" Y="-1.873414881295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.669072075234" Y="-0.171797723251" />
                  <Point X="27.977018421041" Y="-1.744545387693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.700011721557" Y="-0.081603548689" />
                  <Point X="27.90205703378" Y="-1.627947119929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.700628559129" Y="0.016150951219" />
                  <Point X="27.862492721028" Y="-1.520174334058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.660897102479" Y="0.123965410744" />
                  <Point X="27.86357705513" Y="-1.422536394126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.716012266125" Y="0.457459954697" />
                  <Point X="27.87238438968" Y="-1.326824014472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.227194167107" Y="0.677244289867" />
                  <Point X="27.891957890985" Y="-1.233795941672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.241166413326" Y="0.77166891241" />
                  <Point X="27.943336170575" Y="-1.148697690724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.255138770401" Y="0.866093507314" />
                  <Point X="28.010765611559" Y="-1.067601443789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.269456197503" Y="0.960432066597" />
                  <Point X="28.091075599276" Y="-0.989716677847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.29430827182" Y="1.052144043329" />
                  <Point X="28.258471479562" Y="-0.933544863574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.319160346138" Y="1.143856020061" />
                  <Point X="28.684574201824" Y="-0.941875909534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.344012420456" Y="1.235567996793" />
                  <Point X="29.516593255479" Y="-1.051413263721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.448451676555" Y="1.30743666044" />
                  <Point X="29.743576882256" Y="-1.010098343275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.254355288309" Y="1.204410617225" />
                  <Point X="29.763704119507" Y="-0.917208332353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.435793548251" Y="1.257081273023" />
                  <Point X="29.777929651158" Y="-0.822846860961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.516551278443" Y="1.334854404228" />
                  <Point X="28.572701180321" Y="-0.424441358569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.557716871708" Y="1.422498963862" />
                  <Point X="28.428732784522" Y="-0.290637711184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.578510632824" Y="1.51522279172" />
                  <Point X="28.371773168919" Y="-0.178527789195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.558725225624" Y="1.61806414257" />
                  <Point X="28.351782003886" Y="-0.075635137154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.493160610171" Y="1.732319531987" />
                  <Point X="28.35340823224" Y="0.021867693367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.321312698525" Y="1.873074323379" />
                  <Point X="28.371589053247" Y="0.115243000363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.132300324517" Y="2.018108695891" />
                  <Point X="28.410408374109" Y="0.203472551409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.94328857043" Y="2.163142913839" />
                  <Point X="28.473981891973" Y="0.285530187955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.777166568777" Y="2.302470075528" />
                  <Point X="28.569008063824" Y="0.359745797097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.827054207491" Y="2.387939984989" />
                  <Point X="28.70289159995" Y="0.42427317721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.876941992393" Y="2.473409858002" />
                  <Point X="28.892167971179" Y="0.474989572374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.926829940079" Y="2.558879690427" />
                  <Point X="29.081444342407" Y="0.525705967539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.976717887765" Y="2.644349522853" />
                  <Point X="29.270720713636" Y="0.576422362703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.034660800342" Y="2.72781102697" />
                  <Point X="29.459997022366" Y="0.62713877345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.098457777917" Y="2.809812948752" />
                  <Point X="28.16735526095" Y="1.047338857004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.530667370828" Y="0.956754974239" />
                  <Point X="29.64927316066" Y="0.677855226692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.162254755493" Y="2.891814870533" />
                  <Point X="28.065372450022" Y="1.170674322365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.787657483696" Y="0.990588437436" />
                  <Point X="29.779291393609" Y="0.743346335125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.226051728844" Y="2.973816793369" />
                  <Point X="21.496411484302" Y="2.906408535491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.079292704503" Y="2.761079924964" />
                  <Point X="28.019680483133" Y="1.279974904004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.044647596563" Y="1.024421900632" />
                  <Point X="29.76343080324" Y="0.845209119234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.165532589991" Y="2.837486201338" />
                  <Point X="28.001497306328" Y="1.38241677395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.301637786325" Y="1.058255344657" />
                  <Point X="29.744500794563" Y="0.94783719528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.227672172853" Y="2.919901358033" />
                  <Point X="28.011972045329" Y="1.477713422983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.558628018619" Y="1.092088778077" />
                  <Point X="29.719126806718" Y="1.052071935782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.250987292615" Y="3.011996540575" />
                  <Point X="28.040501756429" Y="1.568508461881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.245351407644" Y="3.111310019308" />
                  <Point X="28.093203506304" Y="1.653276734626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.218911930913" Y="3.215810416026" />
                  <Point X="28.161809847326" Y="1.734079547425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.154279320192" Y="3.329833430563" />
                  <Point X="27.147061278913" Y="2.084993076164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.527087135423" Y="1.990241988331" />
                  <Point X="28.256944120059" Y="1.808268203991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.088246092584" Y="3.444205658112" />
                  <Point X="27.054589963659" Y="2.205957059305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.676091732071" Y="2.050999264623" />
                  <Point X="28.353248667005" Y="1.882165078425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.0222133328" Y="3.55857776902" />
                  <Point X="27.020305221615" Y="2.312413500355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.794527411177" Y="2.119378228074" />
                  <Point X="28.449553213951" Y="1.956061952858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.956181563621" Y="3.672949632942" />
                  <Point X="27.013449207797" Y="2.412031191376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.912963090283" Y="2.187757191526" />
                  <Point X="28.545857760897" Y="2.029958827291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.014257549377" Y="3.756377958188" />
                  <Point X="27.031873385077" Y="2.505345822839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.031398769389" Y="2.256136154977" />
                  <Point X="28.642162307843" Y="2.103855701725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.110621585209" Y="3.830260000376" />
                  <Point X="27.063575022751" Y="2.595350011619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.149834448495" Y="2.324515118429" />
                  <Point X="28.73846685479" Y="2.177752576158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.206986263369" Y="3.904141882414" />
                  <Point X="27.112867170249" Y="2.680968393715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.268270127601" Y="2.39289408188" />
                  <Point X="28.83477123097" Y="2.251649493168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.303350941529" Y="3.978023764452" />
                  <Point X="27.162281390619" Y="2.766556339627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.386705925625" Y="2.461273015682" />
                  <Point X="28.931075320873" Y="2.325546481555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.412326935421" Y="4.048761292326" />
                  <Point X="27.211695610988" Y="2.852144285538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.505141777314" Y="2.529651936103" />
                  <Point X="29.027379410776" Y="2.399443469943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.533966456143" Y="4.116341448345" />
                  <Point X="27.261109831357" Y="2.93773223145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.623577629004" Y="2.598030856525" />
                  <Point X="29.123683500679" Y="2.47334045833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.655605976866" Y="4.183921604365" />
                  <Point X="22.95330246428" Y="4.109697533705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.235562018281" Y="4.039322322822" />
                  <Point X="27.310524051727" Y="3.023320177361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.742013480693" Y="2.666409776946" />
                  <Point X="29.076677090464" Y="2.582968767498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.777245481159" Y="4.251501764481" />
                  <Point X="22.830561993871" Y="4.238208464848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.382558337295" Y="4.100580318965" />
                  <Point X="27.359938272096" Y="3.108908123273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.860449332382" Y="2.734788697368" />
                  <Point X="28.99836192398" Y="2.700403226338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.465694401286" Y="4.177760464954" />
                  <Point X="27.409352492465" Y="3.194496069184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.504178810318" Y="4.266073518898" />
                  <Point X="27.458766712835" Y="3.280084015096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.531450360997" Y="4.357182252421" />
                  <Point X="27.508180933204" Y="3.365671961007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.535014084749" Y="4.454202011084" />
                  <Point X="24.888696791954" Y="4.116691005213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.015348125149" Y="4.08511328125" />
                  <Point X="27.557595129359" Y="3.451259912956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.521687182772" Y="4.555433075726" />
                  <Point X="24.782270773187" Y="4.241134286711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.16539665138" Y="4.145610276664" />
                  <Point X="27.607009279098" Y="3.536847876478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.692387950208" Y="4.610780889085" />
                  <Point X="24.751564379129" Y="4.346698545404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.224451007596" Y="4.228794666757" />
                  <Point X="27.656423428838" Y="3.622435839999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.877228168743" Y="4.662603341341" />
                  <Point X="24.723451805695" Y="4.451616091981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.25345352684" Y="4.319471821345" />
                  <Point X="27.705837578577" Y="3.708023803521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.062068600916" Y="4.714425740331" />
                  <Point X="24.695339232261" Y="4.556533638559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.278045472784" Y="4.411248655365" />
                  <Point X="27.755251728316" Y="3.793611767042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.326299358566" Y="4.746453908025" />
                  <Point X="24.667226671664" Y="4.661451181935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.302637418728" Y="4.503025489385" />
                  <Point X="27.804665878056" Y="3.879199730564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.593543178162" Y="4.777730835001" />
                  <Point X="24.639114825814" Y="4.766368547106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.327229278525" Y="4.594802344884" />
                  <Point X="27.662044880447" Y="4.01266743385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.351820413446" Y="4.686579381115" />
                  <Point X="27.380845067762" Y="4.180686716334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.376411548366" Y="4.778356417345" />
                  <Point X="27.00106442878" Y="4.373284959359" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.692533203125" Y="-4.361870117188" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.373994140625" Y="-3.39140234375" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.1345625" Y="-3.223019287109" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.8515625" Y="-3.231146972656" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.621396484375" Y="-3.415785400391" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.361185546875" Y="-4.207344238281" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.032658203125" Y="-4.963862304688" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.738083984375" Y="-4.89646875" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.664248046875" Y="-4.753103515625" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.65692578125" Y="-4.366885253906" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.48503125" Y="-4.089774414062" />
                  <Point X="23.38045703125" Y="-3.998064208984" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.17996484375" Y="-3.974568359375" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.867806640625" Y="-4.068883300781" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.63944140625" Y="-4.288691894531" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.3389765625" Y="-4.288231933594" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.920298828125" Y="-3.995240234375" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.083275390625" Y="-3.340456787109" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.59759375" />
                  <Point X="22.486021484375" Y="-2.568764892578" />
                  <Point X="22.468673828125" Y="-2.551416259766" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.830255859375" Y="-2.877214111328" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.992205078125" Y="-3.049334716797" />
                  <Point X="20.838302734375" Y="-2.847136962891" />
                  <Point X="20.677802734375" Y="-2.578002441406" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.117638671875" Y="-1.97452722168" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396017700195" />
                  <Point X="21.861884765625" Y="-1.366263061523" />
                  <Point X="21.859673828125" Y="-1.33459375" />
                  <Point X="21.838841796875" Y="-1.310638916016" />
                  <Point X="21.812359375" Y="-1.295052368164" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.050724609375" Y="-1.384644287109" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.132802734375" Y="-1.246853515625" />
                  <Point X="20.072607421875" Y="-1.011188354492" />
                  <Point X="20.03014453125" Y="-0.714293762207" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="20.62397265625" Y="-0.347979400635" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.4734140625" Y="-0.110799880981" />
                  <Point X="21.497677734375" Y="-0.090644966125" />
                  <Point X="21.51020703125" Y="-0.059460945129" />
                  <Point X="21.516599609375" Y="-0.031280040741" />
                  <Point X="21.50925" Y="-1.8321589E-05" />
                  <Point X="21.497677734375" Y="0.028084888458" />
                  <Point X="21.470546875" Y="0.050230701447" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.777337890625" Y="0.244325271606" />
                  <Point X="20.001814453125" Y="0.452126068115" />
                  <Point X="20.043560546875" Y="0.73424822998" />
                  <Point X="20.08235546875" Y="0.996414733887" />
                  <Point X="20.16783203125" Y="1.311854980469" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.659966796875" Y="1.471229736328" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.30218359375" Y="1.406551269531" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056640625" />
                  <Point X="21.3608828125" Y="1.443787353516" />
                  <Point X="21.37448046875" Y="1.476614868164" />
                  <Point X="21.385529296875" Y="1.503291748047" />
                  <Point X="21.38928515625" Y="1.524606933594" />
                  <Point X="21.38368359375" Y="1.545513183594" />
                  <Point X="21.36727734375" Y="1.577030639648" />
                  <Point X="21.353943359375" Y="1.602642822266" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.958470703125" Y="1.912003540039" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.689244140625" Y="2.528749511719" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="21.066412109375" Y="3.078044677734" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.480466796875" Y="3.135006835938" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.908681640625" Y="2.916305664062" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775146484" />
                  <Point X="21.986748046875" Y="2.927403808594" />
                  <Point X="22.020248046875" Y="2.960903808594" />
                  <Point X="22.04747265625" Y="2.988127197266" />
                  <Point X="22.0591015625" Y="3.006381835938" />
                  <Point X="22.061927734375" Y="3.027841064453" />
                  <Point X="22.057798828125" Y="3.075037109375" />
                  <Point X="22.054443359375" Y="3.113390625" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.8788515625" Y="3.426889892578" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.984580078125" Y="3.973041992188" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.60373828125" Y="4.372458007812" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.9218515625" Y="4.431348144531" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.101283203125" Y="4.246315429688" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.218491699219" />
                  <Point X="23.18619140625" Y="4.22225" />
                  <Point X="23.24090234375" Y="4.244913085938" />
                  <Point X="23.285365234375" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275743652344" />
                  <Point X="23.31391796875" Y="4.294487304688" />
                  <Point X="23.331724609375" Y="4.350966796875" />
                  <Point X="23.346197265625" Y="4.396863769531" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.328861328125" Y="4.564437011719" />
                  <Point X="23.31086328125" Y="4.701141113281" />
                  <Point X="23.69202734375" Y="4.808006347656" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.46423046875" Y="4.953893554688" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.85125" Y="4.708774902344" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.14108984375" Y="4.63423046875" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.563443359375" Y="4.956645019531" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.21787890625" Y="4.839212402344" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.741576171875" Y="4.684504394531" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.156150390625" Y="4.510506347656" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.556171875" Y="4.298434570312" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.93762890625" Y="4.049832519531" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.70519921875" Y="3.326917480469" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491514404297" />
                  <Point X="27.2130078125" Y="2.447221923828" />
                  <Point X="27.2033828125" Y="2.411228027344" />
                  <Point X="27.202044921875" Y="2.392327636719" />
                  <Point X="27.2066640625" Y="2.35402734375" />
                  <Point X="27.210416015625" Y="2.322902832031" />
                  <Point X="27.21868359375" Y="2.300811279297" />
                  <Point X="27.2423828125" Y="2.265885009766" />
                  <Point X="27.261642578125" Y="2.237502685547" />
                  <Point X="27.27494140625" Y="2.224203613281" />
                  <Point X="27.3098671875" Y="2.200504638672" />
                  <Point X="27.33825" Y="2.18124609375" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.398638671875" Y="2.168361816406" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.49295703125" Y="2.177790771484" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.21009765625" Y="2.578701416016" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.097986328125" Y="2.887258544922" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.316935546875" Y="2.552927734375" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.914453125" Y="2.073302490234" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583832763672" />
                  <Point X="28.247494140625" Y="1.542246337891" />
                  <Point X="28.22158984375" Y="1.508451416016" />
                  <Point X="28.21312109375" Y="1.491501098633" />
                  <Point X="28.20124609375" Y="1.449041259766" />
                  <Point X="28.191595703125" Y="1.414536621094" />
                  <Point X="28.190779296875" Y="1.39096472168" />
                  <Point X="28.20052734375" Y="1.34372253418" />
                  <Point X="28.20844921875" Y="1.30533190918" />
                  <Point X="28.215646484375" Y="1.287955200195" />
                  <Point X="28.242158203125" Y="1.247657348633" />
                  <Point X="28.263703125" Y="1.214909790039" />
                  <Point X="28.280947265625" Y="1.1988203125" />
                  <Point X="28.319369140625" Y="1.177193115234" />
                  <Point X="28.35058984375" Y="1.159617919922" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.420513671875" Y="1.14675402832" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.111369140625" Y="1.224845581055" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.89426171875" Y="1.135934570312" />
                  <Point X="29.93919140625" Y="0.951367614746" />
                  <Point X="29.97522265625" Y="0.719953430176" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="29.458388671875" Y="0.430005371094" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819503784" />
                  <Point X="28.678052734375" Y="0.203319747925" />
                  <Point X="28.636578125" Y="0.179347137451" />
                  <Point X="28.622265625" Y="0.166927001953" />
                  <Point X="28.59164453125" Y="0.127907806396" />
                  <Point X="28.566759765625" Y="0.096199172974" />
                  <Point X="28.556986328125" Y="0.07473526001" />
                  <Point X="28.546779296875" Y="0.021437068939" />
                  <Point X="28.538484375" Y="-0.021875299454" />
                  <Point X="28.538484375" Y="-0.040684780121" />
                  <Point X="28.54869140625" Y="-0.093982971191" />
                  <Point X="28.556986328125" Y="-0.137295333862" />
                  <Point X="28.566759765625" Y="-0.158759246826" />
                  <Point X="28.597380859375" Y="-0.197778442383" />
                  <Point X="28.622265625" Y="-0.229487075806" />
                  <Point X="28.636578125" Y="-0.241907211304" />
                  <Point X="28.68761328125" Y="-0.271406829834" />
                  <Point X="28.729087890625" Y="-0.295379577637" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.32401171875" Y="-0.456559082031" />
                  <Point X="29.998068359375" Y="-0.637172424316" />
                  <Point X="29.973517578125" Y="-0.800012207031" />
                  <Point X="29.948431640625" Y="-0.966412902832" />
                  <Point X="29.90226953125" Y="-1.168696044922" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.243755859375" Y="-1.207133178711" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.294671875" Y="-1.120112792969" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.185447265625" Y="-1.154697021484" />
                  <Point X="28.124904296875" Y="-1.227512084961" />
                  <Point X="28.075703125" Y="-1.286684936523" />
                  <Point X="28.064359375" Y="-1.314071166992" />
                  <Point X="28.055681640625" Y="-1.408370361328" />
                  <Point X="28.048630859375" Y="-1.485001464844" />
                  <Point X="28.05636328125" Y="-1.516622436523" />
                  <Point X="28.111794921875" Y="-1.602844970703" />
                  <Point X="28.15684375" Y="-1.672913085938" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.70934375" Y="-2.100573730469" />
                  <Point X="29.339076171875" Y="-2.583784179688" />
                  <Point X="29.27484765625" Y="-2.687715332031" />
                  <Point X="29.204130859375" Y="-2.802142578125" />
                  <Point X="29.108662109375" Y="-2.937790771484" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.49426171875" Y="-2.686920654297" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.61812890625" Y="-2.231782958984" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.390041015625" Y="-2.2713671875" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334683349609" />
                  <Point X="27.236478515625" Y="-2.433719970703" />
                  <Point X="27.19412109375" Y="-2.514201416016" />
                  <Point X="27.1891640625" Y="-2.546374511719" />
                  <Point X="27.2106953125" Y="-2.665587402344" />
                  <Point X="27.22819140625" Y="-2.76246484375" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.5816640625" Y="-3.380588623047" />
                  <Point X="27.98667578125" Y="-4.082088867188" />
                  <Point X="27.919208984375" Y="-4.130278320312" />
                  <Point X="27.835314453125" Y="-4.190202148438" />
                  <Point X="27.728556640625" Y="-4.259304199219" />
                  <Point X="27.679775390625" Y="-4.29087890625" />
                  <Point X="27.248482421875" Y="-3.728805175781" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.552974609375" Y="-2.904876708984" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.29721484375" Y="-2.847550048828" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.066041015625" Y="-2.951068359375" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045985839844" />
                  <Point X="25.93876953125" Y="-3.182575439453" />
                  <Point X="25.914642578125" Y="-3.293573730469" />
                  <Point X="25.9139296875" Y="-3.310719970703" />
                  <Point X="26.0124296875" Y="-4.058893066406" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.07373046875" Y="-4.945846191406" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.89573828125" Y="-4.981160644531" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#172" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.106609374343" Y="4.752996095487" Z="1.4" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.4" />
                  <Point X="-0.547739060359" Y="5.034834628843" Z="1.4" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.4" />
                  <Point X="-1.327626913789" Y="4.887429855699" Z="1.4" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.4" />
                  <Point X="-1.72686849711" Y="4.589190881874" Z="1.4" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.4" />
                  <Point X="-1.722117291954" Y="4.397283259761" Z="1.4" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.4" />
                  <Point X="-1.784386680358" Y="4.322387374726" Z="1.4" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.4" />
                  <Point X="-1.88178625045" Y="4.321946287265" Z="1.4" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.4" />
                  <Point X="-2.044637379235" Y="4.493066048266" Z="1.4" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.4" />
                  <Point X="-2.426701805349" Y="4.447445585722" Z="1.4" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.4" />
                  <Point X="-3.051997752229" Y="4.044001900013" Z="1.4" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.4" />
                  <Point X="-3.170605723859" Y="3.433169839907" Z="1.4" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.4" />
                  <Point X="-2.998169165875" Y="3.101959518461" Z="1.4" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.4" />
                  <Point X="-3.02126352379" Y="3.027540044096" Z="1.4" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.4" />
                  <Point X="-3.093116947145" Y="2.997395533298" Z="1.4" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.4" />
                  <Point X="-3.50068950839" Y="3.209588224887" Z="1.4" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.4" />
                  <Point X="-3.97920829997" Y="3.140027088315" Z="1.4" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.4" />
                  <Point X="-4.360094327911" Y="2.585234704526" Z="1.4" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.4" />
                  <Point X="-4.078122848383" Y="1.903615802694" Z="1.4" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.4" />
                  <Point X="-3.683229350657" Y="1.585221779797" Z="1.4" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.4" />
                  <Point X="-3.67787236177" Y="1.527027492502" Z="1.4" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.4" />
                  <Point X="-3.719008416809" Y="1.485517260433" Z="1.4" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.4" />
                  <Point X="-4.339664003161" Y="1.552082021973" Z="1.4" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.4" />
                  <Point X="-4.886583773817" Y="1.356212398948" Z="1.4" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.4" />
                  <Point X="-5.012057007773" Y="0.772847385504" Z="1.4" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.4" />
                  <Point X="-4.241761203174" Y="0.227308962878" Z="1.4" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.4" />
                  <Point X="-3.564118597031" Y="0.040433477307" Z="1.4" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.4" />
                  <Point X="-3.544660408605" Y="0.016443931866" Z="1.4" />
                  <Point X="-3.539556741714" Y="0" Z="1.4" />
                  <Point X="-3.54370419253" Y="-0.013363019195" Z="1.4" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.4" />
                  <Point X="-3.561249998092" Y="-0.038442505827" Z="1.4" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.4" />
                  <Point X="-4.395126585598" Y="-0.268403093878" Z="1.4" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.4" />
                  <Point X="-5.02550821904" Y="-0.69009263967" Z="1.4" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.4" />
                  <Point X="-4.921798668998" Y="-1.227947404549" Z="1.4" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.4" />
                  <Point X="-3.948907465691" Y="-1.402936512406" Z="1.4" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.4" />
                  <Point X="-3.207286246072" Y="-1.313851028259" Z="1.4" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.4" />
                  <Point X="-3.196130377033" Y="-1.335786109845" Z="1.4" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.4" />
                  <Point X="-3.9189563711" Y="-1.903579573936" Z="1.4" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.4" />
                  <Point X="-4.371298507939" Y="-2.572332342331" Z="1.4" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.4" />
                  <Point X="-4.053736220787" Y="-3.048337967092" Z="1.4" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.4" />
                  <Point X="-3.150901598947" Y="-2.889235275267" Z="1.4" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.4" />
                  <Point X="-2.565061825756" Y="-2.563268643375" Z="1.4" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.4" />
                  <Point X="-2.966181737902" Y="-3.284176783545" Z="1.4" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.4" />
                  <Point X="-3.116361635248" Y="-4.003577363632" Z="1.4" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.4" />
                  <Point X="-2.693503129646" Y="-4.299462563005" Z="1.4" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.4" />
                  <Point X="-2.327047167291" Y="-4.287849688674" Z="1.4" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.4" />
                  <Point X="-2.110571120553" Y="-4.079176433275" Z="1.4" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.4" />
                  <Point X="-1.829461333909" Y="-3.993181460465" Z="1.4" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.4" />
                  <Point X="-1.554091594976" Y="-4.096086096045" Z="1.4" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.4" />
                  <Point X="-1.398270382738" Y="-4.345360234246" Z="1.4" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.4" />
                  <Point X="-1.39148088268" Y="-4.715297033687" Z="1.4" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.4" />
                  <Point X="-1.280532404753" Y="-4.913611495702" Z="1.4" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.4" />
                  <Point X="-0.983025532825" Y="-4.981666486745" Z="1.4" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.4" />
                  <Point X="-0.596674943702" Y="-4.189005277928" Z="1.4" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.4" />
                  <Point X="-0.343684559131" Y="-3.413014472663" Z="1.4" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.4" />
                  <Point X="-0.139773418293" Y="-3.247619879356" Z="1.4" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.4" />
                  <Point X="0.113585661068" Y="-3.239492165932" Z="1.4" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.4" />
                  <Point X="0.326761300301" Y="-3.388631283105" Z="1.4" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.4" />
                  <Point X="0.6380799167" Y="-4.343530751887" Z="1.4" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.4" />
                  <Point X="0.898518840537" Y="-4.999075318521" Z="1.4" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.4" />
                  <Point X="1.078278435028" Y="-4.963406617075" Z="1.4" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.4" />
                  <Point X="1.05584466756" Y="-4.021086907199" Z="1.4" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.4" />
                  <Point X="0.981471720589" Y="-3.161915460299" Z="1.4" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.4" />
                  <Point X="1.091850124276" Y="-2.958234834558" Z="1.4" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.4" />
                  <Point X="1.295640923948" Y="-2.866059514941" Z="1.4" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.4" />
                  <Point X="1.519777720602" Y="-2.915654651899" Z="1.4" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.4" />
                  <Point X="2.20265754462" Y="-3.72796317627" Z="1.4" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.4" />
                  <Point X="2.74957058136" Y="-4.269998078885" Z="1.4" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.4" />
                  <Point X="2.942113008207" Y="-4.139685175149" Z="1.4" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.4" />
                  <Point X="2.618807640535" Y="-3.324308558053" Z="1.4" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.4" />
                  <Point X="2.253740900582" Y="-2.625421195733" Z="1.4" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.4" />
                  <Point X="2.274567870989" Y="-2.425726877999" Z="1.4" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.4" />
                  <Point X="2.407171500373" Y="-2.284333438978" Z="1.4" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.4" />
                  <Point X="2.60308560778" Y="-2.249707108909" Z="1.4" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.4" />
                  <Point X="3.463105039054" Y="-2.69894185974" Z="1.4" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.4" />
                  <Point X="4.143394715437" Y="-2.935287874953" Z="1.4" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.4" />
                  <Point X="4.311222969993" Y="-2.682720779103" Z="1.4" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.4" />
                  <Point X="3.733624196702" Y="-2.029625958744" Z="1.4" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.4" />
                  <Point X="3.147695599955" Y="-1.544525031676" Z="1.4" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.4" />
                  <Point X="3.099314092104" Y="-1.381671230682" Z="1.4" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.4" />
                  <Point X="3.157191797441" Y="-1.228199496147" Z="1.4" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.4" />
                  <Point X="3.299134248192" Y="-1.137691834563" Z="1.4" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.4" />
                  <Point X="4.231073266393" Y="-1.225425462574" Z="1.4" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.4" />
                  <Point X="4.944859043342" Y="-1.148539829739" Z="1.4" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.4" />
                  <Point X="5.016802805053" Y="-0.776185969034" Z="1.4" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.4" />
                  <Point X="4.33079523899" Y="-0.376982817434" Z="1.4" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.4" />
                  <Point X="3.706478725945" Y="-0.196837936667" Z="1.4" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.4" />
                  <Point X="3.630558330461" Y="-0.135629654144" Z="1.4" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.4" />
                  <Point X="3.591641928966" Y="-0.053298205175" Z="1.4" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.4" />
                  <Point X="3.589729521458" Y="0.043312326031" Z="1.4" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.4" />
                  <Point X="3.624821107939" Y="0.128319084431" Z="1.4" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.4" />
                  <Point X="3.696916688408" Y="0.191310907575" Z="1.4" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.4" />
                  <Point X="4.465172578316" Y="0.412989115334" Z="1.4" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.4" />
                  <Point X="5.018469901685" Y="0.758925395024" Z="1.4" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.4" />
                  <Point X="4.936685888448" Y="1.179041022674" Z="1.4" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.4" />
                  <Point X="4.098686987039" Y="1.30569790394" Z="1.4" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.4" />
                  <Point X="3.420907351054" Y="1.227603185832" Z="1.4" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.4" />
                  <Point X="3.337738764863" Y="1.252043778901" Z="1.4" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.4" />
                  <Point X="3.277773441818" Y="1.306418651478" Z="1.4" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.4" />
                  <Point X="3.24333966623" Y="1.385107206223" Z="1.4" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.4" />
                  <Point X="3.243241627859" Y="1.466853843086" Z="1.4" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.4" />
                  <Point X="3.281020825565" Y="1.543108607303" Z="1.4" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.4" />
                  <Point X="3.938732779548" Y="2.064914878594" Z="1.4" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.4" />
                  <Point X="4.353555889004" Y="2.610094092344" Z="1.4" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.4" />
                  <Point X="4.132415321535" Y="2.947741829066" Z="1.4" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.4" />
                  <Point X="3.178941517548" Y="2.653282793881" Z="1.4" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.4" />
                  <Point X="2.473884071255" Y="2.257373543742" Z="1.4" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.4" />
                  <Point X="2.398467176181" Y="2.249282210551" Z="1.4" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.4" />
                  <Point X="2.331784294938" Y="2.273159206516" Z="1.4" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.4" />
                  <Point X="2.277599393719" Y="2.325240565444" Z="1.4" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.4" />
                  <Point X="2.250147492208" Y="2.391291267651" Z="1.4" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.4" />
                  <Point X="2.255154398915" Y="2.465585504627" Z="1.4" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.4" />
                  <Point X="2.742342558979" Y="3.333197564407" Z="1.4" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.4" />
                  <Point X="2.960449282218" Y="4.121859568368" Z="1.4" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.4" />
                  <Point X="2.575185672225" Y="4.372917082729" Z="1.4" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.4" />
                  <Point X="2.171175719584" Y="4.587078569254" Z="1.4" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.4" />
                  <Point X="1.752467420256" Y="4.762788058308" Z="1.4" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.4" />
                  <Point X="1.223456814604" Y="4.919096030794" Z="1.4" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.4" />
                  <Point X="0.562492508346" Y="5.037652910583" Z="1.4" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.4" />
                  <Point X="0.08663518721" Y="4.678451410464" Z="1.4" />
                  <Point X="0" Y="4.355124473572" Z="1.4" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>