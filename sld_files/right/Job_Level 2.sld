<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#119" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="337" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.571390625" Y="-3.542700439453" />
                  <Point X="25.5633046875" Y="-3.512524169922" />
                  <Point X="25.555904296875" Y="-3.493433105469" />
                  <Point X="25.54537890625" Y="-3.473103759766" />
                  <Point X="25.539060546875" Y="-3.462615234375" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.357109375" Y="-3.207521972656" />
                  <Point X="25.32834765625" Y="-3.187322753906" />
                  <Point X="25.30849609375" Y="-3.178310058594" />
                  <Point X="25.2973828125" Y="-3.174081787109" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0203984375" Y="-3.091593017578" />
                  <Point X="24.988212890625" Y="-3.092229736328" />
                  <Point X="24.968279296875" Y="-3.096063964844" />
                  <Point X="24.9580625" Y="-3.098623046875" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.680494140625" Y="-3.188985107422" />
                  <Point X="24.6524140625" Y="-3.210509765625" />
                  <Point X="24.637591796875" Y="-3.22708984375" />
                  <Point X="24.63037109375" Y="-3.236238769531" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481570800781" />
                  <Point X="24.449009765625" Y="-3.512523925781" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.920662109375" Y="-4.845350585938" />
                  <Point X="23.9189140625" Y="-4.844900878906" />
                  <Point X="23.753583984375" Y="-4.802362304688" />
                  <Point X="23.784064453125" Y="-4.570831542969" />
                  <Point X="23.785560546875" Y="-4.543641601562" />
                  <Point X="23.783806640625" Y="-4.521209960938" />
                  <Point X="23.78226953125" Y="-4.510081542969" />
                  <Point X="23.722962890625" Y="-4.211932128906" />
                  <Point X="23.71296875" Y="-4.181770996094" />
                  <Point X="23.69468359375" Y="-4.151690917969" />
                  <Point X="23.679638671875" Y="-4.134965820312" />
                  <Point X="23.6716484375" Y="-4.127074707031" />
                  <Point X="23.443095703125" Y="-3.926639160156" />
                  <Point X="23.416794921875" Y="-3.908789550781" />
                  <Point X="23.3839765625" Y="-3.896060058594" />
                  <Point X="23.361861328125" Y="-3.891951416016" />
                  <Point X="23.35072265625" Y="-3.890556640625" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.015646484375" Y="-3.872525634766" />
                  <Point X="22.9818671875" Y="-3.882403320312" />
                  <Point X="22.961822265625" Y="-3.89260546875" />
                  <Point X="22.952134765625" Y="-3.898280761719" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.687212890625" Y="-4.076823242188" />
                  <Point X="22.664896484375" Y="-4.099463867187" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.19828125" Y="-4.089380126953" />
                  <Point X="22.19587109375" Y="-4.087524658203" />
                  <Point X="21.89527734375" Y="-3.856077880859" />
                  <Point X="22.5612109375" Y="-2.702647216797" />
                  <Point X="22.57623828125" Y="-2.676620849609" />
                  <Point X="22.58713671875" Y="-2.647681396484" />
                  <Point X="22.593412109375" Y="-2.616180664062" />
                  <Point X="22.59440234375" Y="-2.585017089844" />
                  <Point X="22.58525" Y="-2.555210693359" />
                  <Point X="22.5706796875" Y="-2.526004882812" />
                  <Point X="22.55284765625" Y="-2.501239746094" />
                  <Point X="22.535849609375" Y="-2.484241455078" />
                  <Point X="22.510693359375" Y="-2.466214111328" />
                  <Point X="22.481865234375" Y="-2.451996826172" />
                  <Point X="22.452248046875" Y="-2.44301171875" />
                  <Point X="22.421314453125" Y="-2.444023681641" />
                  <Point X="22.389787109375" Y="-2.450294189453" />
                  <Point X="22.360818359375" Y="-2.461196777344" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="20.91714453125" Y="-2.7938671875" />
                  <Point X="20.9154140625" Y="-2.790965087891" />
                  <Point X="20.693857421875" Y="-2.419449707031" />
                  <Point X="21.86775390625" Y="-1.518687133789" />
                  <Point X="21.894044921875" Y="-1.498512939453" />
                  <Point X="21.915740234375" Y="-1.475110595703" />
                  <Point X="21.933861328125" Y="-1.44737878418" />
                  <Point X="21.946298828125" Y="-1.419231201172" />
                  <Point X="21.95384765625" Y="-1.390084228516" />
                  <Point X="21.95665234375" Y="-1.359646728516" />
                  <Point X="21.954439453125" Y="-1.327963623047" />
                  <Point X="21.94731640625" Y="-1.297937011719" />
                  <Point X="21.930947265625" Y="-1.27177722168" />
                  <Point X="21.909560546875" Y="-1.247505615234" />
                  <Point X="21.886470703125" Y="-1.228439331055" />
                  <Point X="21.860546875" Y="-1.213181396484" />
                  <Point X="21.831283203125" Y="-1.20195703125" />
                  <Point X="21.79939453125" Y="-1.195474975586" />
                  <Point X="21.7680703125" Y="-1.194383911133" />
                  <Point X="20.267900390625" Y="-1.391885131836" />
                  <Point X="20.165923828125" Y="-0.992649414063" />
                  <Point X="20.165466796875" Y="-0.98946081543" />
                  <Point X="20.107578125" Y="-0.584698364258" />
                  <Point X="21.43717578125" Y="-0.228433273315" />
                  <Point X="21.467125" Y="-0.220408279419" />
                  <Point X="21.48307421875" Y="-0.214562667847" />
                  <Point X="21.512830078125" Y="-0.199081420898" />
                  <Point X="21.5400234375" Y="-0.180208267212" />
                  <Point X="21.562875" Y="-0.157781234741" />
                  <Point X="21.58233203125" Y="-0.130838150024" />
                  <Point X="21.596044921875" Y="-0.103378517151" />
                  <Point X="21.60508203125" Y="-0.074259506226" />
                  <Point X="21.609349609375" Y="-0.045518306732" />
                  <Point X="21.6091640625" Y="-0.015274533272" />
                  <Point X="21.604896484375" Y="0.012302587509" />
                  <Point X="21.59583203125" Y="0.041507965088" />
                  <Point X="21.581345703125" Y="0.070021026611" />
                  <Point X="21.561541015625" Y="0.09666493988" />
                  <Point X="21.539462890625" Y="0.118036712646" />
                  <Point X="21.51226953125" Y="0.136910171509" />
                  <Point X="21.498076171875" Y="0.14504675293" />
                  <Point X="21.467125" Y="0.157848144531" />
                  <Point X="20.108185546875" Y="0.521975280762" />
                  <Point X="20.175513671875" Y="0.976974914551" />
                  <Point X="20.176435546875" Y="0.980378540039" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="21.2128671875" Y="1.302619506836" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.25503515625" Y="1.299343505859" />
                  <Point X="21.276615234375" Y="1.301236083984" />
                  <Point X="21.296916015625" Y="1.305280273438" />
                  <Point X="21.2981015625" Y="1.305654296875" />
                  <Point X="21.3582890625" Y="1.324630859375" />
                  <Point X="21.3772421875" Y="1.332973388672" />
                  <Point X="21.396005859375" Y="1.343812744141" />
                  <Point X="21.412705078125" Y="1.356066162109" />
                  <Point X="21.426349609375" Y="1.371646972656" />
                  <Point X="21.438763671875" Y="1.389406982422" />
                  <Point X="21.44869921875" Y="1.407553588867" />
                  <Point X="21.44914453125" Y="1.408631103516" />
                  <Point X="21.473294921875" Y="1.466934082031" />
                  <Point X="21.47908203125" Y="1.486778686523" />
                  <Point X="21.482841796875" Y="1.50808215332" />
                  <Point X="21.484197265625" Y="1.528713500977" />
                  <Point X="21.48105859375" Y="1.549149658203" />
                  <Point X="21.475466796875" Y="1.57004699707" />
                  <Point X="21.467974609375" Y="1.589332519531" />
                  <Point X="21.467349609375" Y="1.590533081055" />
                  <Point X="21.438208984375" Y="1.646509643555" />
                  <Point X="21.426716796875" Y="1.663707519531" />
                  <Point X="21.4128046875" Y="1.680287109375" />
                  <Point X="21.39786328125" Y="1.694590332031" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.9188515625" Y="2.733666503906" />
                  <Point X="20.92129296875" Y="2.736804199219" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.780158203125" Y="2.852283935547" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.812275390625" Y="2.836340820312" />
                  <Point X="21.83291796875" Y="2.82983203125" />
                  <Point X="21.853203125" Y="2.825796630859" />
                  <Point X="21.8549296875" Y="2.825645507812" />
                  <Point X="21.938751953125" Y="2.818312011719" />
                  <Point X="21.95942578125" Y="2.81876171875" />
                  <Point X="21.980875" Y="2.821583496094" />
                  <Point X="22.000958984375" Y="2.826494628906" />
                  <Point X="22.01950390625" Y="2.835634033203" />
                  <Point X="22.03775390625" Y="2.847252685547" />
                  <Point X="22.053890625" Y="2.860195556641" />
                  <Point X="22.055150390625" Y="2.861454833984" />
                  <Point X="22.1146484375" Y="2.920952636719" />
                  <Point X="22.127591796875" Y="2.937079101562" />
                  <Point X="22.139216796875" Y="2.955323974609" />
                  <Point X="22.148361328125" Y="2.973864746094" />
                  <Point X="22.15328125" Y="2.9939453125" />
                  <Point X="22.156111328125" Y="3.015393554688" />
                  <Point X="22.156578125" Y="3.035966308594" />
                  <Point X="22.156416015625" Y="3.037848876953" />
                  <Point X="22.14908203125" Y="3.121671386719" />
                  <Point X="22.145044921875" Y="3.141960205078" />
                  <Point X="22.138537109375" Y="3.162598632812" />
                  <Point X="22.13020703125" Y="3.181529785156" />
                  <Point X="21.816666015625" Y="3.724595703125" />
                  <Point X="22.299376953125" Y="4.094684570313" />
                  <Point X="22.303212890625" Y="4.096815917969" />
                  <Point X="22.83296484375" Y="4.391134277344" />
                  <Point X="22.95276953125" Y="4.235" />
                  <Point X="22.956806640625" Y="4.229739257812" />
                  <Point X="22.971111328125" Y="4.214796875" />
                  <Point X="22.987689453125" Y="4.200886230469" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.00680859375" Y="4.18839453125" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.11938671875" Y="4.132330078125" />
                  <Point X="23.140296875" Y="4.126728027344" />
                  <Point X="23.160744140625" Y="4.12358203125" />
                  <Point X="23.181388671875" Y="4.124937011719" />
                  <Point X="23.20270703125" Y="4.128698730469" />
                  <Point X="23.222548828125" Y="4.134482421875" />
                  <Point X="23.224544921875" Y="4.135309082031" />
                  <Point X="23.321716796875" Y="4.175559082031" />
                  <Point X="23.3398671875" Y="4.185517578125" />
                  <Point X="23.35760546875" Y="4.19794140625" />
                  <Point X="23.3731640625" Y="4.211591308594" />
                  <Point X="23.385396484375" Y="4.228287109375" />
                  <Point X="23.396216796875" Y="4.247045898438" />
                  <Point X="23.40454296875" Y="4.265991210938" />
                  <Point X="23.4051796875" Y="4.268011230469" />
                  <Point X="23.43680078125" Y="4.368296875" />
                  <Point X="23.4408359375" Y="4.388583007812" />
                  <Point X="23.44272265625" Y="4.410143554688" />
                  <Point X="23.442271484375" Y="4.430825195312" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="24.05036328125" Y="4.809807617188" />
                  <Point X="24.0550078125" Y="4.810351074219" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.8621953125" Y="4.300873046875" />
                  <Point X="24.866095703125" Y="4.28631640625" />
                  <Point X="24.87887109375" Y="4.258124023438" />
                  <Point X="24.89673046875" Y="4.231396484375" />
                  <Point X="24.91788671875" Y="4.208808105469" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.307419921875" Y="4.8879375" />
                  <Point X="25.84404296875" Y="4.83173828125" />
                  <Point X="25.84789453125" Y="4.83080859375" />
                  <Point X="26.48103125" Y="4.677949707031" />
                  <Point X="26.482080078125" Y="4.677569824219" />
                  <Point X="26.894638671875" Y="4.527931640625" />
                  <Point X="26.8970703125" Y="4.526793945312" />
                  <Point X="27.294568359375" Y="4.3408984375" />
                  <Point X="27.296955078125" Y="4.339508300781" />
                  <Point X="27.680974609375" Y="4.115778808594" />
                  <Point X="27.68319140625" Y="4.114201660156" />
                  <Point X="27.943259765625" Y="3.929254638672" />
                  <Point X="27.16497265625" Y="2.581220214844" />
                  <Point X="27.14758203125" Y="2.551096435547" />
                  <Point X="27.14154296875" Y="2.538616210938" />
                  <Point X="27.13264453125" Y="2.514437255859" />
                  <Point X="27.111607421875" Y="2.435771484375" />
                  <Point X="27.108537109375" Y="2.416657470703" />
                  <Point X="27.107373046875" Y="2.3963203125" />
                  <Point X="27.10790234375" Y="2.379520263672" />
                  <Point X="27.116099609375" Y="2.311530517578" />
                  <Point X="27.121443359375" Y="2.289602294922" />
                  <Point X="27.1297109375" Y="2.267512207031" />
                  <Point X="27.140076171875" Y="2.247465576172" />
                  <Point X="27.140943359375" Y="2.246187744141" />
                  <Point X="27.18303515625" Y="2.184156982422" />
                  <Point X="27.195431640625" Y="2.169378173828" />
                  <Point X="27.2100078125" Y="2.155211425781" />
                  <Point X="27.22287890625" Y="2.144724853516" />
                  <Point X="27.28491015625" Y="2.102634521484" />
                  <Point X="27.304943359375" Y="2.092276123047" />
                  <Point X="27.32701953125" Y="2.08401171875" />
                  <Point X="27.348927734375" Y="2.07866796875" />
                  <Point X="27.350365234375" Y="2.078494140625" />
                  <Point X="27.418388671875" Y="2.070291748047" />
                  <Point X="27.4379296875" Y="2.069960449219" />
                  <Point X="27.458453125" Y="2.071731689453" />
                  <Point X="27.474826171875" Y="2.074604492188" />
                  <Point X="27.5534921875" Y="2.095640625" />
                  <Point X="27.565283203125" Y="2.099638183594" />
                  <Point X="27.58853515625" Y="2.110145263672" />
                  <Point X="28.967326171875" Y="2.906190429688" />
                  <Point X="29.12328125" Y="2.689447265625" />
                  <Point X="29.12451171875" Y="2.687415771484" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.25368359375" Y="1.686021850586" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.2203125" Y="1.659108764648" />
                  <Point X="28.202806640625" Y="1.64010559082" />
                  <Point X="28.14619140625" Y="1.566245849609" />
                  <Point X="28.135984375" Y="1.549643432617" />
                  <Point X="28.127080078125" Y="1.531138427734" />
                  <Point X="28.1211953125" Y="1.515533081055" />
                  <Point X="28.10010546875" Y="1.440122192383" />
                  <Point X="28.09665234375" Y="1.417805664062" />
                  <Point X="28.09583984375" Y="1.394214355469" />
                  <Point X="28.097751953125" Y="1.371707397461" />
                  <Point X="28.098109375" Y="1.369978881836" />
                  <Point X="28.11541015625" Y="1.286134521484" />
                  <Point X="28.121279296875" Y="1.267563476563" />
                  <Point X="28.129447265625" Y="1.248712158203" />
                  <Point X="28.137251953125" Y="1.234267822266" />
                  <Point X="28.184337890625" Y="1.162697021484" />
                  <Point X="28.1988984375" Y="1.145446044922" />
                  <Point X="28.216150390625" Y="1.129351196289" />
                  <Point X="28.234375" Y="1.116018798828" />
                  <Point X="28.235759765625" Y="1.115239257812" />
                  <Point X="28.30398828125" Y="1.076832519531" />
                  <Point X="28.32209765625" Y="1.068990844727" />
                  <Point X="28.341974609375" Y="1.062741699219" />
                  <Point X="28.35801953125" Y="1.05918737793" />
                  <Point X="28.45028125" Y="1.046993896484" />
                  <Point X="28.462705078125" Y="1.046174926758" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.77683984375" Y="1.21663671875" />
                  <Point X="29.84594140625" Y="0.932778930664" />
                  <Point X="29.846326171875" Y="0.930309753418" />
                  <Point X="29.890865234375" Y="0.644238830566" />
                  <Point X="28.742822265625" Y="0.336621368408" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.70316796875" Y="0.324896148682" />
                  <Point X="28.6796796875" Y="0.313988647461" />
                  <Point X="28.589037109375" Y="0.261595489502" />
                  <Point X="28.5731328125" Y="0.250055465698" />
                  <Point X="28.55769921875" Y="0.236207336426" />
                  <Point X="28.54641015625" Y="0.224148193359" />
                  <Point X="28.492025390625" Y="0.15484815979" />
                  <Point X="28.480296875" Y="0.135558792114" />
                  <Point X="28.470521484375" Y="0.114085159302" />
                  <Point X="28.46367578125" Y="0.092575401306" />
                  <Point X="28.4633046875" Y="0.0906354599" />
                  <Point X="28.4451796875" Y="-0.004006072044" />
                  <Point X="28.4435" Y="-0.023582019806" />
                  <Point X="28.443873046875" Y="-0.044341777802" />
                  <Point X="28.445552734375" Y="-0.060504112244" />
                  <Point X="28.463681640625" Y="-0.155164672852" />
                  <Point X="28.47053515625" Y="-0.176678527832" />
                  <Point X="28.480318359375" Y="-0.198154281616" />
                  <Point X="28.492052734375" Y="-0.217441833496" />
                  <Point X="28.493146484375" Y="-0.218835876465" />
                  <Point X="28.54753125" Y="-0.288135925293" />
                  <Point X="28.561404296875" Y="-0.302430175781" />
                  <Point X="28.577583984375" Y="-0.315930084229" />
                  <Point X="28.590904296875" Y="-0.325234832764" />
                  <Point X="28.681546875" Y="-0.37762802124" />
                  <Point X="28.692708984375" Y="-0.3831378479" />
                  <Point X="28.716580078125" Y="-0.392149963379" />
                  <Point X="29.891474609375" Y="-0.706961853027" />
                  <Point X="29.855025390625" Y="-0.948725769043" />
                  <Point X="29.854525390625" Y="-0.950911987305" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="28.45481640625" Y="-1.007447509766" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.405109375" Y="-1.002877258301" />
                  <Point X="28.384298828125" Y="-1.004386962891" />
                  <Point X="28.370994140625" Y="-1.006305419922" />
                  <Point X="28.193095703125" Y="-1.04497265625" />
                  <Point X="28.1614453125" Y="-1.058188842773" />
                  <Point X="28.131404296875" Y="-1.077745117188" />
                  <Point X="28.110185546875" Y="-1.096624023438" />
                  <Point X="28.00265625" Y="-1.225947875977" />
                  <Point X="27.986845703125" Y="-1.253079467773" />
                  <Point X="27.97518359375" Y="-1.283915527344" />
                  <Point X="27.96944140625" Y="-1.308816894531" />
                  <Point X="27.95403125" Y="-1.47629675293" />
                  <Point X="27.955109375" Y="-1.508471679688" />
                  <Point X="27.963740234375" Y="-1.541582275391" />
                  <Point X="27.972279296875" Y="-1.559905517578" />
                  <Point X="27.978478515625" Y="-1.571152099609" />
                  <Point X="28.076931640625" Y="-1.724287841797" />
                  <Point X="28.08693359375" Y="-1.737238647461" />
                  <Point X="28.11062890625" Y="-1.760908569336" />
                  <Point X="29.213125" Y="-2.606883056641" />
                  <Point X="29.12480859375" Y="-2.749791015625" />
                  <Point X="29.123779296875" Y="-2.751253173828" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="27.828060546875" Y="-2.192592041016" />
                  <Point X="27.800955078125" Y="-2.176942871094" />
                  <Point X="27.7829609375" Y="-2.168913818359" />
                  <Point X="27.762486328125" Y="-2.162223632813" />
                  <Point X="27.749865234375" Y="-2.159037597656" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.505974609375" Y="-2.119082275391" />
                  <Point X="27.47203515625" Y="-2.124885009766" />
                  <Point X="27.452740234375" Y="-2.131962402344" />
                  <Point X="27.4412109375" Y="-2.137083740234" />
                  <Point X="27.26531640625" Y="-2.229655761719" />
                  <Point X="27.240001953125" Y="-2.249020507812" />
                  <Point X="27.217134765625" Y="-2.273603759766" />
                  <Point X="27.202625" Y="-2.294062744141" />
                  <Point X="27.110052734375" Y="-2.469957275391" />
                  <Point X="27.09873046875" Y="-2.50012109375" />
                  <Point X="27.09405859375" Y="-2.534453857422" />
                  <Point X="27.095064453125" Y="-2.555323974609" />
                  <Point X="27.096466796875" Y="-2.567635009766" />
                  <Point X="27.134703125" Y="-2.779349121094" />
                  <Point X="27.13898828125" Y="-2.795140380859" />
                  <Point X="27.151822265625" Y="-2.826078125" />
                  <Point X="27.86128515625" Y="-4.054904541016" />
                  <Point X="27.78182421875" Y="-4.111661132813" />
                  <Point X="27.78069921875" Y="-4.112389160156" />
                  <Point X="27.701765625" Y="-4.163481445312" />
                  <Point X="26.77923046875" Y="-2.961211669922" />
                  <Point X="26.758548828125" Y="-2.934256591797" />
                  <Point X="26.7446953125" Y="-2.9196953125" />
                  <Point X="26.727763671875" Y="-2.905307861328" />
                  <Point X="26.717623046875" Y="-2.897791259766" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479744140625" Y="-2.749644287109" />
                  <Point X="26.44579296875" Y="-2.742002929688" />
                  <Point X="26.424630859375" Y="-2.741215820312" />
                  <Point X="26.41239453125" Y="-2.741549804688" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.152798828125" Y="-2.770960693359" />
                  <Point X="26.12178125" Y="-2.785324462891" />
                  <Point X="26.10096484375" Y="-2.798481689453" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.902615234375" Y="-2.968638427734" />
                  <Point X="25.885037109375" Y="-2.998916748047" />
                  <Point X="25.87783203125" Y="-3.019240722656" />
                  <Point X="25.8745390625" Y="-3.030805908203" />
                  <Point X="25.821810546875" Y="-3.273396240234" />
                  <Point X="25.819724609375" Y="-3.289627197266" />
                  <Point X="25.8197421875" Y="-3.323118652344" />
                  <Point X="26.02206640625" Y="-4.859915527344" />
                  <Point X="25.975689453125" Y="-4.870081542969" />
                  <Point X="25.9746015625" Y="-4.870279296875" />
                  <Point X="25.92931640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941578125" Y="-4.752637695312" />
                  <Point X="23.858755859375" Y="-4.731327636719" />
                  <Point X="23.878251953125" Y="-4.583230957031" />
                  <Point X="23.878921875" Y="-4.57605078125" />
                  <Point X="23.88041796875" Y="-4.548860839844" />
                  <Point X="23.880271484375" Y="-4.536236328125" />
                  <Point X="23.878517578125" Y="-4.5138046875" />
                  <Point X="23.875443359375" Y="-4.491547851562" />
                  <Point X="23.81613671875" Y="-4.1933984375" />
                  <Point X="23.813140625" Y="-4.18205078125" />
                  <Point X="23.803146484375" Y="-4.151889648438" />
                  <Point X="23.794146484375" Y="-4.132424316406" />
                  <Point X="23.775861328125" Y="-4.102344238281" />
                  <Point X="23.7653125" Y="-4.088157226562" />
                  <Point X="23.750267578125" Y="-4.071432128906" />
                  <Point X="23.734287109375" Y="-4.055649902344" />
                  <Point X="23.505734375" Y="-3.855214355469" />
                  <Point X="23.496443359375" Y="-3.848032714844" />
                  <Point X="23.470142578125" Y="-3.830183105469" />
                  <Point X="23.451150390625" Y="-3.82021875" />
                  <Point X="23.41833203125" Y="-3.807489257812" />
                  <Point X="23.401328125" Y="-3.802658203125" />
                  <Point X="23.379212890625" Y="-3.798549560547" />
                  <Point X="23.356935546875" Y="-3.795760009766" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0418515625" Y="-3.7758359375" />
                  <Point X="23.010115234375" Y="-3.777686767578" />
                  <Point X="22.988982421875" Y="-3.781343994141" />
                  <Point X="22.955203125" Y="-3.791221679688" />
                  <Point X="22.938775390625" Y="-3.797738525391" />
                  <Point X="22.91873046875" Y="-3.807940673828" />
                  <Point X="22.89935546875" Y="-3.819291259766" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.6403203125" Y="-3.992755859375" />
                  <Point X="22.6195546875" Y="-4.010134277344" />
                  <Point X="22.59723828125" Y="-4.032774902344" />
                  <Point X="22.58952734375" Y="-4.041631347656" />
                  <Point X="22.496796875" Y="-4.162478027344" />
                  <Point X="22.252396484375" Y="-4.011150878906" />
                  <Point X="22.019134765625" Y="-3.831547363281" />
                  <Point X="22.643482421875" Y="-2.750147216797" />
                  <Point X="22.658509765625" Y="-2.724120849609" />
                  <Point X="22.665142578125" Y="-2.710101806641" />
                  <Point X="22.676041015625" Y="-2.681162353516" />
                  <Point X="22.680306640625" Y="-2.666241943359" />
                  <Point X="22.68658203125" Y="-2.634741210938" />
                  <Point X="22.68836328125" Y="-2.619197753906" />
                  <Point X="22.689353515625" Y="-2.588034179688" />
                  <Point X="22.685216796875" Y="-2.557131347656" />
                  <Point X="22.676064453125" Y="-2.527324951172" />
                  <Point X="22.6702578125" Y="-2.512801269531" />
                  <Point X="22.6556875" Y="-2.483595458984" />
                  <Point X="22.6477734375" Y="-2.470493652344" />
                  <Point X="22.62994140625" Y="-2.445728515625" />
                  <Point X="22.6200234375" Y="-2.434065185547" />
                  <Point X="22.603025390625" Y="-2.417066894531" />
                  <Point X="22.591185546875" Y="-2.407021972656" />
                  <Point X="22.566029296875" Y="-2.388994628906" />
                  <Point X="22.552712890625" Y="-2.381012207031" />
                  <Point X="22.523884765625" Y="-2.366794921875" />
                  <Point X="22.5094453125" Y="-2.361088134766" />
                  <Point X="22.479828125" Y="-2.352103027344" />
                  <Point X="22.449142578125" Y="-2.3480625" />
                  <Point X="22.418208984375" Y="-2.349074462891" />
                  <Point X="22.402783203125" Y="-2.350848632812" />
                  <Point X="22.371255859375" Y="-2.357119140625" />
                  <Point X="22.35632421875" Y="-2.361382568359" />
                  <Point X="22.32735546875" Y="-2.37228515625" />
                  <Point X="22.313318359375" Y="-2.378924316406" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="20.99598828125" Y="-2.740599365234" />
                  <Point X="20.818734375" Y="-2.443372558594" />
                  <Point X="21.9255859375" Y="-1.594055664062" />
                  <Point X="21.951876953125" Y="-1.573881469727" />
                  <Point X="21.963712890625" Y="-1.563099121094" />
                  <Point X="21.985408203125" Y="-1.539696655273" />
                  <Point X="21.995267578125" Y="-1.527076660156" />
                  <Point X="22.013388671875" Y="-1.499344848633" />
                  <Point X="22.020755859375" Y="-1.485774902344" />
                  <Point X="22.033193359375" Y="-1.457627319336" />
                  <Point X="22.038263671875" Y="-1.443049438477" />
                  <Point X="22.0458125" Y="-1.413902587891" />
                  <Point X="22.048447265625" Y="-1.398801147461" />
                  <Point X="22.051251953125" Y="-1.368363647461" />
                  <Point X="22.051421875" Y="-1.353027587891" />
                  <Point X="22.049208984375" Y="-1.321344482422" />
                  <Point X="22.046875" Y="-1.306035888672" />
                  <Point X="22.039751953125" Y="-1.276009277344" />
                  <Point X="22.027849609375" Y="-1.247544433594" />
                  <Point X="22.01148046875" Y="-1.221384521484" />
                  <Point X="22.002224609375" Y="-1.208971679688" />
                  <Point X="21.980837890625" Y="-1.184700195312" />
                  <Point X="21.970048828125" Y="-1.174251831055" />
                  <Point X="21.946958984375" Y="-1.155185546875" />
                  <Point X="21.934658203125" Y="-1.146567504883" />
                  <Point X="21.908734375" Y="-1.131309570312" />
                  <Point X="21.894568359375" Y="-1.124482177734" />
                  <Point X="21.8653046875" Y="-1.1132578125" />
                  <Point X="21.85020703125" Y="-1.108860839844" />
                  <Point X="21.818318359375" Y="-1.10237878418" />
                  <Point X="21.802701171875" Y="-1.100532592773" />
                  <Point X="21.771376953125" Y="-1.09944152832" />
                  <Point X="21.755669921875" Y="-1.100196533203" />
                  <Point X="20.33908203125" Y="-1.286694091797" />
                  <Point X="20.259236328125" Y="-0.974102600098" />
                  <Point X="20.213548828125" Y="-0.654654602051" />
                  <Point X="21.461763671875" Y="-0.320196258545" />
                  <Point X="21.491712890625" Y="-0.312171234131" />
                  <Point X="21.49981640625" Y="-0.309605987549" />
                  <Point X="21.526921875" Y="-0.2988387146" />
                  <Point X="21.556677734375" Y="-0.283357543945" />
                  <Point X="21.56699609375" Y="-0.277126556396" />
                  <Point X="21.594189453125" Y="-0.258253356934" />
                  <Point X="21.60656640625" Y="-0.248010284424" />
                  <Point X="21.62941796875" Y="-0.225583236694" />
                  <Point X="21.639892578125" Y="-0.213399291992" />
                  <Point X="21.659349609375" Y="-0.186456192017" />
                  <Point X="21.66732421875" Y="-0.173281509399" />
                  <Point X="21.681037109375" Y="-0.14582194519" />
                  <Point X="21.686775390625" Y="-0.131536911011" />
                  <Point X="21.6958125" Y="-0.102417976379" />
                  <Point X="21.699052734375" Y="-0.08821245575" />
                  <Point X="21.7033203125" Y="-0.059471172333" />
                  <Point X="21.70434765625" Y="-0.044935554504" />
                  <Point X="21.704162109375" Y="-0.014691693306" />
                  <Point X="21.703046875" Y="-0.000746112287" />
                  <Point X="21.698779296875" Y="0.026831010818" />
                  <Point X="21.695626953125" Y="0.040462551117" />
                  <Point X="21.6865625" Y="0.069667984009" />
                  <Point X="21.68052734375" Y="0.084538444519" />
                  <Point X="21.666041015625" Y="0.113051445007" />
                  <Point X="21.65758984375" Y="0.126693984985" />
                  <Point X="21.63778515625" Y="0.153337905884" />
                  <Point X="21.627615234375" Y="0.164923065186" />
                  <Point X="21.605537109375" Y="0.186294876099" />
                  <Point X="21.59362890625" Y="0.19608140564" />
                  <Point X="21.566435546875" Y="0.214954864502" />
                  <Point X="21.559517578125" Y="0.219327941895" />
                  <Point X="21.534384765625" Y="0.232834350586" />
                  <Point X="21.50343359375" Y="0.245635681152" />
                  <Point X="21.491712890625" Y="0.249611053467" />
                  <Point X="20.214556640625" Y="0.591824707031" />
                  <Point X="20.268669921875" Y="0.957531066895" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="21.200466796875" Y="1.208432128906" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.2322734375" Y="1.204815307617" />
                  <Point X="21.25297265625" Y="1.204365844727" />
                  <Point X="21.263333984375" Y="1.204706787109" />
                  <Point X="21.2849140625" Y="1.206599365234" />
                  <Point X="21.29517578125" Y="1.208066894531" />
                  <Point X="21.3154765625" Y="1.212111083984" />
                  <Point X="21.32666796875" Y="1.21505090332" />
                  <Point X="21.38685546875" Y="1.234027587891" />
                  <Point X="21.396560546875" Y="1.237681274414" />
                  <Point X="21.415513671875" Y="1.246023803711" />
                  <Point X="21.42476171875" Y="1.250712768555" />
                  <Point X="21.443525390625" Y="1.261551879883" />
                  <Point X="21.45220703125" Y="1.267220458984" />
                  <Point X="21.46890625" Y="1.279473754883" />
                  <Point X="21.484173828125" Y="1.293478759766" />
                  <Point X="21.497818359375" Y="1.309059570312" />
                  <Point X="21.504212890625" Y="1.317220825195" />
                  <Point X="21.516626953125" Y="1.334980834961" />
                  <Point X="21.522091796875" Y="1.343783569336" />
                  <Point X="21.53202734375" Y="1.361930297852" />
                  <Point X="21.5369296875" Y="1.37231640625" />
                  <Point X="21.5610625" Y="1.430578491211" />
                  <Point X="21.56449609375" Y="1.440337768555" />
                  <Point X="21.570283203125" Y="1.460182495117" />
                  <Point X="21.57263671875" Y="1.470267700195" />
                  <Point X="21.576396484375" Y="1.491571044922" />
                  <Point X="21.57763671875" Y="1.501854125977" />
                  <Point X="21.5789921875" Y="1.522485595703" />
                  <Point X="21.578095703125" Y="1.543134887695" />
                  <Point X="21.57495703125" Y="1.563571044922" />
                  <Point X="21.572830078125" Y="1.573706176758" />
                  <Point X="21.56723828125" Y="1.594603393555" />
                  <Point X="21.56401953125" Y="1.604448486328" />
                  <Point X="21.55652734375" Y="1.623734008789" />
                  <Point X="21.551615234375" Y="1.634400756836" />
                  <Point X="21.522474609375" Y="1.690377319336" />
                  <Point X="21.517197265625" Y="1.699291748047" />
                  <Point X="21.505705078125" Y="1.716489624023" />
                  <Point X="21.499490234375" Y="1.724772827148" />
                  <Point X="21.485578125" Y="1.741352539062" />
                  <Point X="21.478498046875" Y="1.748911865234" />
                  <Point X="21.463556640625" Y="1.763215087891" />
                  <Point X="21.4556953125" Y="1.769958862305" />
                  <Point X="20.77238671875" Y="2.294280761719" />
                  <Point X="20.99771484375" Y="2.680323486328" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.732658203125" Y="2.770011474609" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774013671875" Y="2.749386474609" />
                  <Point X="21.78370703125" Y="2.745738037109" />
                  <Point X="21.804349609375" Y="2.739229248047" />
                  <Point X="21.8143828125" Y="2.736657714844" />
                  <Point X="21.83466796875" Y="2.732622314453" />
                  <Point X="21.846646484375" Y="2.731007324219" />
                  <Point X="21.93046875" Y="2.723673828125" />
                  <Point X="21.940818359375" Y="2.723334472656" />
                  <Point X="21.9614921875" Y="2.723784179688" />
                  <Point X="21.97181640625" Y="2.724573242188" />
                  <Point X="21.993265625" Y="2.727395019531" />
                  <Point X="22.00344140625" Y="2.729302490234" />
                  <Point X="22.023525390625" Y="2.734213623047" />
                  <Point X="22.042955078125" Y="2.741281005859" />
                  <Point X="22.0615" Y="2.750420410156" />
                  <Point X="22.0705234375" Y="2.75549609375" />
                  <Point X="22.0887734375" Y="2.767114746094" />
                  <Point X="22.097193359375" Y="2.773145263672" />
                  <Point X="22.113330078125" Y="2.786088134766" />
                  <Point X="22.122326171875" Y="2.794279541016" />
                  <Point X="22.18182421875" Y="2.85377734375" />
                  <Point X="22.188736328125" Y="2.861488525391" />
                  <Point X="22.2016796875" Y="2.877614990234" />
                  <Point X="22.2077109375" Y="2.886030273438" />
                  <Point X="22.2193359375" Y="2.904275146484" />
                  <Point X="22.22441796875" Y="2.913302001953" />
                  <Point X="22.2335625" Y="2.931842773438" />
                  <Point X="22.2406328125" Y="2.951257568359" />
                  <Point X="22.245552734375" Y="2.971338134766" />
                  <Point X="22.24746484375" Y="2.981517822266" />
                  <Point X="22.250294921875" Y="3.002966064453" />
                  <Point X="22.2510859375" Y="3.013238525391" />
                  <Point X="22.251552734375" Y="3.033811279297" />
                  <Point X="22.2510546875" Y="3.046129150391" />
                  <Point X="22.243720703125" Y="3.129951660156" />
                  <Point X="22.242255859375" Y="3.140211181641" />
                  <Point X="22.23821875" Y="3.1605" />
                  <Point X="22.235646484375" Y="3.170529296875" />
                  <Point X="22.229138671875" Y="3.191167724609" />
                  <Point X="22.2254921875" Y="3.200860107422" />
                  <Point X="22.217162109375" Y="3.219791259766" />
                  <Point X="22.212478515625" Y="3.229030029297" />
                  <Point X="21.940611328125" Y="3.699914794922" />
                  <Point X="22.351630859375" Y="4.015039306641" />
                  <Point X="22.8074765625" Y="4.268295898437" />
                  <Point X="22.877400390625" Y="4.17716796875" />
                  <Point X="22.88818359375" Y="4.164044433594" />
                  <Point X="22.90248828125" Y="4.149102050781" />
                  <Point X="22.910046875" Y="4.142022460938" />
                  <Point X="22.926625" Y="4.128111816406" />
                  <Point X="22.934908203125" Y="4.121897460938" />
                  <Point X="22.95210546875" Y="4.11040625" />
                  <Point X="22.96294140625" Y="4.10412890625" />
                  <Point X="23.056236328125" Y="4.0555625" />
                  <Point X="23.06567578125" Y="4.051286132812" />
                  <Point X="23.084958984375" Y="4.043788085938" />
                  <Point X="23.094802734375" Y="4.040566162109" />
                  <Point X="23.115712890625" Y="4.034964111328" />
                  <Point X="23.125849609375" Y="4.032833007812" />
                  <Point X="23.146296875" Y="4.029687011719" />
                  <Point X="23.166966796875" Y="4.028785888672" />
                  <Point X="23.187611328125" Y="4.030140869141" />
                  <Point X="23.197896484375" Y="4.031382324219" />
                  <Point X="23.21921484375" Y="4.035144042969" />
                  <Point X="23.22929296875" Y="4.037494384766" />
                  <Point X="23.249134765625" Y="4.043278076172" />
                  <Point X="23.26089453125" Y="4.047538330078" />
                  <Point X="23.35806640625" Y="4.087788330078" />
                  <Point X="23.3674140625" Y="4.092271728516" />
                  <Point X="23.385564453125" Y="4.102229980469" />
                  <Point X="23.3943671875" Y="4.107705078125" />
                  <Point X="23.41210546875" Y="4.12012890625" />
                  <Point X="23.4202578125" Y="4.126528808594" />
                  <Point X="23.43581640625" Y="4.140178710937" />
                  <Point X="23.449796875" Y="4.1554453125" />
                  <Point X="23.462029296875" Y="4.172141113281" />
                  <Point X="23.4676875" Y="4.1808203125" />
                  <Point X="23.4785078125" Y="4.199579101562" />
                  <Point X="23.4831875" Y="4.208823242188" />
                  <Point X="23.491513671875" Y="4.227768554687" />
                  <Point X="23.49578515625" Y="4.239452148438" />
                  <Point X="23.52740625" Y="4.339737792969" />
                  <Point X="23.529974609375" Y="4.349763183594" />
                  <Point X="23.534009765625" Y="4.370049316406" />
                  <Point X="23.535474609375" Y="4.380301269531" />
                  <Point X="23.537361328125" Y="4.401861816406" />
                  <Point X="23.53769921875" Y="4.412215332031" />
                  <Point X="23.537248046875" Y="4.432896972656" />
                  <Point X="23.536458984375" Y="4.443225585938" />
                  <Point X="23.520734375" Y="4.562654785156" />
                  <Point X="24.068822265625" Y="4.716319335938" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.770431640625" Y="4.27628515625" />
                  <Point X="24.77956640625" Y="4.247104980469" />
                  <Point X="24.792341796875" Y="4.218912597656" />
                  <Point X="24.7998828125" Y="4.20534375" />
                  <Point X="24.8177421875" Y="4.178616210938" />
                  <Point X="24.827392578125" Y="4.166455566406" />
                  <Point X="24.848548828125" Y="4.1438671875" />
                  <Point X="24.873103515625" Y="4.125025390625" />
                  <Point X="24.9003984375" Y="4.110436035156" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.37819140625" Y="4.785006347656" />
                  <Point X="25.82787890625" Y="4.737911621094" />
                  <Point X="26.453603515625" Y="4.586841796875" />
                  <Point X="26.858244140625" Y="4.440075683594" />
                  <Point X="27.25044921875" Y="4.25665625" />
                  <Point X="27.629421875" Y="4.035866699219" />
                  <Point X="27.81778125" Y="3.901916259766" />
                  <Point X="27.082701171875" Y="2.628720214844" />
                  <Point X="27.065310546875" Y="2.598596435547" />
                  <Point X="27.062068359375" Y="2.592476074219" />
                  <Point X="27.052388671875" Y="2.571427001953" />
                  <Point X="27.043490234375" Y="2.547248046875" />
                  <Point X="27.040869140625" Y="2.538979980469" />
                  <Point X="27.01983203125" Y="2.460314208984" />
                  <Point X="27.017810546875" Y="2.450838378906" />
                  <Point X="27.01369140625" Y="2.422086181641" />
                  <Point X="27.01252734375" Y="2.401749023438" />
                  <Point X="27.012419921875" Y="2.393328857422" />
                  <Point X="27.0135859375" Y="2.368148925781" />
                  <Point X="27.021783203125" Y="2.300159179688" />
                  <Point X="27.02380078125" Y="2.289037841797" />
                  <Point X="27.02914453125" Y="2.267109619141" />
                  <Point X="27.032470703125" Y="2.256302734375" />
                  <Point X="27.04073828125" Y="2.234212646484" />
                  <Point X="27.04532421875" Y="2.223879394531" />
                  <Point X="27.055689453125" Y="2.203832763672" />
                  <Point X="27.0623359375" Y="2.192841552734" />
                  <Point X="27.104427734375" Y="2.130810791016" />
                  <Point X="27.11025" Y="2.123104980469" />
                  <Point X="27.129220703125" Y="2.101253173828" />
                  <Point X="27.143796875" Y="2.087086425781" />
                  <Point X="27.150001953125" Y="2.081561279297" />
                  <Point X="27.1695390625" Y="2.06611328125" />
                  <Point X="27.2315703125" Y="2.024022949219" />
                  <Point X="27.24127734375" Y="2.018247802734" />
                  <Point X="27.261310546875" Y="2.007889282227" />
                  <Point X="27.27163671875" Y="2.003306152344" />
                  <Point X="27.293712890625" Y="1.995041748047" />
                  <Point X="27.3045078125" Y="1.991717529297" />
                  <Point X="27.326416015625" Y="1.986373779297" />
                  <Point X="27.3389921875" Y="1.984177490234" />
                  <Point X="27.407015625" Y="1.975974975586" />
                  <Point X="27.41677734375" Y="1.975305419922" />
                  <Point X="27.44609765625" Y="1.975312255859" />
                  <Point X="27.46662109375" Y="1.977083496094" />
                  <Point X="27.47487109375" Y="1.978161132812" />
                  <Point X="27.4993671875" Y="1.982829223633" />
                  <Point X="27.578033203125" Y="2.003865356445" />
                  <Point X="27.583994140625" Y="2.005670776367" />
                  <Point X="27.60440234375" Y="2.013066650391" />
                  <Point X="27.627654296875" Y="2.023573852539" />
                  <Point X="27.63603515625" Y="2.027872802734" />
                  <Point X="28.940404296875" Y="2.780950195312" />
                  <Point X="29.043970703125" Y="2.637014648438" />
                  <Point X="29.136884765625" Y="2.483471679688" />
                  <Point X="28.1958515625" Y="1.761390380859" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.16754296875" Y="1.739342163086" />
                  <Point X="28.15044140625" Y="1.723474731445" />
                  <Point X="28.132935546875" Y="1.704471679688" />
                  <Point X="28.127408203125" Y="1.697899780273" />
                  <Point X="28.07079296875" Y="1.624040039062" />
                  <Point X="28.06526171875" Y="1.616000366211" />
                  <Point X="28.05037890625" Y="1.590835327148" />
                  <Point X="28.041474609375" Y="1.572330200195" />
                  <Point X="28.038189453125" Y="1.564658569336" />
                  <Point X="28.029705078125" Y="1.541119628906" />
                  <Point X="28.008615234375" Y="1.465708618164" />
                  <Point X="28.00622265625" Y="1.454649047852" />
                  <Point X="28.00276953125" Y="1.432332519531" />
                  <Point X="28.001708984375" Y="1.421075561523" />
                  <Point X="28.000896484375" Y="1.39748425293" />
                  <Point X="28.001181640625" Y="1.386172485352" />
                  <Point X="28.00309375" Y="1.363665527344" />
                  <Point X="28.005078125" Y="1.350741943359" />
                  <Point X="28.02237109375" Y="1.266936279297" />
                  <Point X="28.024826171875" Y="1.257506591797" />
                  <Point X="28.034109375" Y="1.229794433594" />
                  <Point X="28.04227734375" Y="1.210943115234" />
                  <Point X="28.0458671875" Y="1.203551757813" />
                  <Point X="28.05788671875" Y="1.182054321289" />
                  <Point X="28.10497265625" Y="1.110483520508" />
                  <Point X="28.111740234375" Y="1.101421875" />
                  <Point X="28.12630078125" Y="1.084170776367" />
                  <Point X="28.13409375" Y="1.075981811523" />
                  <Point X="28.151345703125" Y="1.05988684082" />
                  <Point X="28.16005859375" Y="1.052677978516" />
                  <Point X="28.178283203125" Y="1.039345581055" />
                  <Point X="28.189158203125" Y="1.032455322266" />
                  <Point X="28.25738671875" Y="0.994048461914" />
                  <Point X="28.26623828125" Y="0.989654724121" />
                  <Point X="28.29360546875" Y="0.978364135742" />
                  <Point X="28.313482421875" Y="0.972114990234" />
                  <Point X="28.321427734375" Y="0.96999029541" />
                  <Point X="28.345572265625" Y="0.965006347656" />
                  <Point X="28.437833984375" Y="0.952812866211" />
                  <Point X="28.444033203125" Y="0.952199645996" />
                  <Point X="28.46571875" Y="0.95122277832" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797302246" />
                  <Point X="29.704703125" Y="1.111319824219" />
                  <Point X="29.752689453125" Y="0.914196228027" />
                  <Point X="29.78387109375" Y="0.713921325684" />
                  <Point X="28.718234375" Y="0.428384307861" />
                  <Point X="28.6919921875" Y="0.421352813721" />
                  <Point X="28.685201171875" Y="0.419257507324" />
                  <Point X="28.66315625" Y="0.411058898926" />
                  <Point X="28.63966796875" Y="0.400151306152" />
                  <Point X="28.632138671875" Y="0.396237182617" />
                  <Point X="28.54149609375" Y="0.343844055176" />
                  <Point X="28.53324609375" Y="0.338486785889" />
                  <Point X="28.5096875" Y="0.320764251709" />
                  <Point X="28.49425390625" Y="0.306916168213" />
                  <Point X="28.488345703125" Y="0.301131622314" />
                  <Point X="28.47167578125" Y="0.282797790527" />
                  <Point X="28.417291015625" Y="0.213497619629" />
                  <Point X="28.410853515625" Y="0.204203643799" />
                  <Point X="28.399125" Y="0.184914321899" />
                  <Point X="28.393833984375" Y="0.174918991089" />
                  <Point X="28.38405859375" Y="0.153445358276" />
                  <Point X="28.37999609375" Y="0.142895965576" />
                  <Point X="28.373150390625" Y="0.121386222839" />
                  <Point X="28.36999609375" Y="0.108484405518" />
                  <Point X="28.351875" Y="0.013862772942" />
                  <Point X="28.35052734375" Y="0.004115490437" />
                  <Point X="28.348515625" Y="-0.025288801193" />
                  <Point X="28.348888671875" Y="-0.046048591614" />
                  <Point X="28.3493828125" Y="-0.054161914825" />
                  <Point X="28.352248046875" Y="-0.078373321533" />
                  <Point X="28.370376953125" Y="-0.173033752441" />
                  <Point X="28.3731640625" Y="-0.184000335693" />
                  <Point X="28.380017578125" Y="-0.205514251709" />
                  <Point X="28.38408203125" Y="-0.216061569214" />
                  <Point X="28.393865234375" Y="-0.237537277222" />
                  <Point X="28.399158203125" Y="-0.247531265259" />
                  <Point X="28.410892578125" Y="-0.266818817139" />
                  <Point X="28.418412109375" Y="-0.277485321045" />
                  <Point X="28.472796875" Y="-0.346785491943" />
                  <Point X="28.479359375" Y="-0.354299102783" />
                  <Point X="28.50054296875" Y="-0.375373840332" />
                  <Point X="28.51672265625" Y="-0.388873687744" />
                  <Point X="28.523181640625" Y="-0.393810516357" />
                  <Point X="28.54336328125" Y="-0.40748336792" />
                  <Point X="28.634005859375" Y="-0.459876647949" />
                  <Point X="28.63949609375" Y="-0.462814910889" />
                  <Point X="28.659154296875" Y="-0.472014831543" />
                  <Point X="28.683025390625" Y="-0.481027008057" />
                  <Point X="28.6919921875" Y="-0.483912963867" />
                  <Point X="29.784880859375" Y="-0.776751220703" />
                  <Point X="29.76162109375" Y="-0.931024780273" />
                  <Point X="29.727802734375" Y="-1.079219726563" />
                  <Point X="28.467216796875" Y="-0.913260253906" />
                  <Point X="28.436783203125" Y="-0.909253662109" />
                  <Point X="28.42716015625" Y="-0.908481384277" />
                  <Point X="28.398236328125" Y="-0.908126342773" />
                  <Point X="28.37742578125" Y="-0.909636047363" />
                  <Point X="28.370740234375" Y="-0.910359436035" />
                  <Point X="28.35081640625" Y="-0.913472900391" />
                  <Point X="28.17291796875" Y="-0.952140258789" />
                  <Point X="28.156490234375" Y="-0.95730847168" />
                  <Point X="28.12483984375" Y="-0.970524597168" />
                  <Point X="28.1096171875" Y="-0.978572570801" />
                  <Point X="28.079576171875" Y="-0.99812878418" />
                  <Point X="28.068255859375" Y="-1.006770751953" />
                  <Point X="28.047037109375" Y="-1.025649536133" />
                  <Point X="28.037138671875" Y="-1.03588671875" />
                  <Point X="27.929609375" Y="-1.165210571289" />
                  <Point X="27.920576171875" Y="-1.178116699219" />
                  <Point X="27.904765625" Y="-1.205248291016" />
                  <Point X="27.89798828125" Y="-1.219473754883" />
                  <Point X="27.886326171875" Y="-1.250309814453" />
                  <Point X="27.88261328125" Y="-1.262568969727" />
                  <Point X="27.87687109375" Y="-1.287470336914" />
                  <Point X="27.874841796875" Y="-1.300112426758" />
                  <Point X="27.859431640625" Y="-1.467592529297" />
                  <Point X="27.859083984375" Y="-1.479478271484" />
                  <Point X="27.860162109375" Y="-1.511653198242" />
                  <Point X="27.863181640625" Y="-1.532434204102" />
                  <Point X="27.8718125" Y="-1.565544921875" />
                  <Point X="27.877630859375" Y="-1.5817109375" />
                  <Point X="27.886169921875" Y="-1.600034179688" />
                  <Point X="27.898568359375" Y="-1.62252734375" />
                  <Point X="27.997021484375" Y="-1.775662963867" />
                  <Point X="28.001744140625" Y="-1.78235534668" />
                  <Point X="28.019794921875" Y="-1.804449829102" />
                  <Point X="28.043490234375" Y="-1.828119750977" />
                  <Point X="28.052796875" Y="-1.836277099609" />
                  <Point X="29.087173828125" Y="-2.629981933594" />
                  <Point X="29.0454921875" Y="-2.697428466797" />
                  <Point X="29.001275390625" Y="-2.760252441406" />
                  <Point X="27.875560546875" Y="-2.110319580078" />
                  <Point X="27.848455078125" Y="-2.094670410156" />
                  <Point X="27.839666015625" Y="-2.0901875" />
                  <Point X="27.812466796875" Y="-2.078612304688" />
                  <Point X="27.7919921875" Y="-2.071922119141" />
                  <Point X="27.76675" Y="-2.065550048828" />
                  <Point X="27.555021484375" Y="-2.027311889648" />
                  <Point X="27.543201171875" Y="-2.025934692383" />
                  <Point X="27.5110390625" Y="-2.024217407227" />
                  <Point X="27.48996484375" Y="-2.025441040039" />
                  <Point X="27.456025390625" Y="-2.031243774414" />
                  <Point X="27.4393203125" Y="-2.035695678711" />
                  <Point X="27.420025390625" Y="-2.042773071289" />
                  <Point X="27.396966796875" Y="-2.053015625" />
                  <Point X="27.221072265625" Y="-2.145587646484" />
                  <Point X="27.207595703125" Y="-2.154201416016" />
                  <Point X="27.18228125" Y="-2.173566162109" />
                  <Point X="27.170443359375" Y="-2.184317138672" />
                  <Point X="27.147576171875" Y="-2.208900390625" />
                  <Point X="27.13964453125" Y="-2.218646728516" />
                  <Point X="27.125134765625" Y="-2.239105712891" />
                  <Point X="27.118556640625" Y="-2.249818359375" />
                  <Point X="27.025984375" Y="-2.425712890625" />
                  <Point X="27.021111328125" Y="-2.436572509766" />
                  <Point X="27.0097890625" Y="-2.466736328125" />
                  <Point X="27.00459765625" Y="-2.487312011719" />
                  <Point X="26.99992578125" Y="-2.521644775391" />
                  <Point X="26.99916796875" Y="-2.539027099609" />
                  <Point X="27.000173828125" Y="-2.559897216797" />
                  <Point X="27.002978515625" Y="-2.584519287109" />
                  <Point X="27.04121484375" Y="-2.796233398438" />
                  <Point X="27.04301953125" Y="-2.804228759766" />
                  <Point X="27.05123828125" Y="-2.831541748047" />
                  <Point X="27.064072265625" Y="-2.862479492188" />
                  <Point X="27.06955078125" Y="-2.873578125" />
                  <Point X="27.735896484375" Y="-4.027721679687" />
                  <Point X="27.723755859375" Y="-4.036083496094" />
                  <Point X="26.854599609375" Y="-2.903379394531" />
                  <Point X="26.83391796875" Y="-2.876424316406" />
                  <Point X="26.827375" Y="-2.868774902344" />
                  <Point X="26.813521484375" Y="-2.854213623047" />
                  <Point X="26.8062109375" Y="-2.847301757812" />
                  <Point X="26.789279296875" Y="-2.832914306641" />
                  <Point X="26.768998046875" Y="-2.817881103516" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.549783203125" Y="-2.677832519531" />
                  <Point X="26.5207265625" Y="-2.663938476562" />
                  <Point X="26.500603515625" Y="-2.656962646484" />
                  <Point X="26.46665234375" Y="-2.649321289062" />
                  <Point X="26.44932421875" Y="-2.647068603516" />
                  <Point X="26.428162109375" Y="-2.646281494141" />
                  <Point X="26.403689453125" Y="-2.646949462891" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.15933984375" Y="-2.670825439453" />
                  <Point X="26.128125" Y="-2.679220458984" />
                  <Point X="26.11287890625" Y="-2.684755371094" />
                  <Point X="26.081861328125" Y="-2.699119140625" />
                  <Point X="26.0710234375" Y="-2.705020507813" />
                  <Point X="26.05020703125" Y="-2.718177734375" />
                  <Point X="26.040228515625" Y="-2.72543359375" />
                  <Point X="25.863876953125" Y="-2.872063476562" />
                  <Point X="25.855220703125" Y="-2.880228759766" />
                  <Point X="25.83322265625" Y="-2.903755615234" />
                  <Point X="25.82045703125" Y="-2.92094140625" />
                  <Point X="25.80287890625" Y="-2.951219726562" />
                  <Point X="25.795498046875" Y="-2.967173828125" />
                  <Point X="25.78829296875" Y="-2.987497802734" />
                  <Point X="25.78170703125" Y="-3.010628173828" />
                  <Point X="25.728978515625" Y="-3.253218505859" />
                  <Point X="25.7275859375" Y="-3.261286865234" />
                  <Point X="25.724724609375" Y="-3.289677001953" />
                  <Point X="25.7247421875" Y="-3.323168457031" />
                  <Point X="25.7255546875" Y="-3.335518798828" />
                  <Point X="25.83308984375" Y="-4.152325195312" />
                  <Point X="25.663154296875" Y="-3.518112792969" />
                  <Point X="25.655068359375" Y="-3.487936523438" />
                  <Point X="25.6518828125" Y="-3.478188232422" />
                  <Point X="25.644482421875" Y="-3.459097167969" />
                  <Point X="25.640267578125" Y="-3.449754394531" />
                  <Point X="25.6297421875" Y="-3.429425048828" />
                  <Point X="25.61710546875" Y="-3.408447998047" />
                  <Point X="25.456681640625" Y="-3.177309326172" />
                  <Point X="25.449296875" Y="-3.167976806641" />
                  <Point X="25.42776953125" Y="-3.144022216797" />
                  <Point X="25.41170703125" Y="-3.129778808594" />
                  <Point X="25.3829453125" Y="-3.109579589844" />
                  <Point X="25.36762109375" Y="-3.1008203125" />
                  <Point X="25.34776953125" Y="-3.091807617188" />
                  <Point X="25.32554296875" Y="-3.083351074219" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.066814453125" Y="-3.0036953125" />
                  <Point X="25.038076171875" Y="-2.998252441406" />
                  <Point X="25.01851953125" Y="-2.996611572266" />
                  <Point X="24.986333984375" Y="-2.997248291016" />
                  <Point X="24.97026953125" Y="-2.998939941406" />
                  <Point X="24.9503359375" Y="-3.002774169922" />
                  <Point X="24.92990234375" Y="-3.007892333984" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.67053515625" Y="-3.089170654297" />
                  <Point X="24.641212890625" Y="-3.102486572266" />
                  <Point X="24.62269921875" Y="-3.113588134766" />
                  <Point X="24.594619140625" Y="-3.135112792969" />
                  <Point X="24.58158984375" Y="-3.147194091797" />
                  <Point X="24.566767578125" Y="-3.163774169922" />
                  <Point X="24.552326171875" Y="-3.182072021484" />
                  <Point X="24.391904296875" Y="-3.4132109375" />
                  <Point X="24.38753125" Y="-3.420129150391" />
                  <Point X="24.374025390625" Y="-3.445260498047" />
                  <Point X="24.36122265625" Y="-3.476213623047" />
                  <Point X="24.35724609375" Y="-3.487936035156" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.091411330485" Y="-3.887197836988" />
                  <Point X="22.55653266584" Y="-4.084630130328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.870467627533" Y="-4.642362431704" />
                  <Point X="24.029795363959" Y="-4.709993043341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.05541230509" Y="-3.768712921446" />
                  <Point X="22.621106285573" Y="-4.008835769843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.880354080601" Y="-4.5433547462" />
                  <Point X="24.054624916662" Y="-4.617328327307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.103269082782" Y="-3.685822682508" />
                  <Point X="22.712243168411" Y="-3.944316845582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.863823891553" Y="-4.433133861388" />
                  <Point X="24.079454469365" Y="-4.524663611274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.151125860475" Y="-3.60293244357" />
                  <Point X="22.806695492765" Y="-3.881205242749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.841401724047" Y="-4.320411980104" />
                  <Point X="24.104284022068" Y="-4.431998895241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.198982638167" Y="-3.520042204632" />
                  <Point X="22.901293907118" Y="-3.818155651442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.818979556541" Y="-4.207690098819" />
                  <Point X="24.129113574771" Y="-4.339334179208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.18219722266" Y="-2.985238166406" />
                  <Point X="21.228851307229" Y="-3.005041650379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.24683941586" Y="-3.437151965694" />
                  <Point X="23.044753438328" Y="-3.775846373732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.7559241785" Y="-4.07772044296" />
                  <Point X="24.153943127474" Y="-4.246669463174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.06614766108" Y="-2.83277381423" />
                  <Point X="21.331867530675" Y="-2.94556520704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.294696193553" Y="-3.354261726756" />
                  <Point X="23.330759059732" Y="-3.794044321459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.534690199331" Y="-3.88060795446" />
                  <Point X="24.178772680178" Y="-4.154004747141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.963401452848" Y="-2.685956400521" />
                  <Point X="21.434883754122" Y="-2.8860887637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.342552971245" Y="-3.271371487818" />
                  <Point X="24.203602232881" Y="-4.061340031108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.880994127248" Y="-2.547772330279" />
                  <Point X="21.537899977569" Y="-2.826612320361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.390409748938" Y="-3.18848124888" />
                  <Point X="24.228431785584" Y="-3.968675315075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.839905818469" Y="-2.427127142109" />
                  <Point X="21.640916201015" Y="-2.767135877022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.43826652663" Y="-3.105591009942" />
                  <Point X="24.253261338287" Y="-3.876010599041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.926500963448" Y="-2.360680364504" />
                  <Point X="21.743932424462" Y="-2.707659433682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.486123304323" Y="-3.022700771004" />
                  <Point X="24.27809089099" Y="-3.783345883008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.013096108426" Y="-2.2942335869" />
                  <Point X="21.846948647909" Y="-2.648182990343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.533980082015" Y="-2.939810532066" />
                  <Point X="24.302920443694" Y="-3.690681166975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.099691253404" Y="-2.227786809296" />
                  <Point X="21.949964871355" Y="-2.588706547004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.581836859708" Y="-2.856920293128" />
                  <Point X="24.327749996397" Y="-3.598016450941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.186286398382" Y="-2.161340031692" />
                  <Point X="22.052981094802" Y="-2.529230103664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.6296936374" Y="-2.774030054189" />
                  <Point X="24.3525795491" Y="-3.505351734908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.827486349468" Y="-4.131412527921" />
                  <Point X="25.83050533608" Y="-4.132694011708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.27288154336" Y="-2.094893254087" />
                  <Point X="22.155997318249" Y="-2.469753660325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.673009453647" Y="-2.689212291476" />
                  <Point X="24.389098932579" Y="-3.417649057645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.796284210731" Y="-4.014963769962" />
                  <Point X="25.816113929072" Y="-4.02338098601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.359476688338" Y="-2.028446476483" />
                  <Point X="22.259013541695" Y="-2.410277216986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.689199527232" Y="-2.592880334132" />
                  <Point X="24.44421525101" Y="-3.337840310928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.765082071994" Y="-3.898515012002" />
                  <Point X="25.801722522064" Y="-3.914067960312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.446071833317" Y="-1.961999698879" />
                  <Point X="22.375109330178" Y="-2.356352719611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.649071625088" Y="-2.472642814391" />
                  <Point X="24.499543920648" Y="-3.25812170195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.733879933257" Y="-3.782066254043" />
                  <Point X="25.787331115056" Y="-3.804754934613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.532666978295" Y="-1.895552921275" />
                  <Point X="24.555134197311" Y="-3.178514138566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.70267779452" Y="-3.665617496084" />
                  <Point X="25.772939708048" Y="-3.695441908915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.33879778371" Y="-1.285581278472" />
                  <Point X="20.340866077988" Y="-1.286459217305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.619262123273" Y="-1.82910614367" />
                  <Point X="24.632071771069" Y="-3.107967965193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.671475655783" Y="-3.549168738125" />
                  <Point X="25.75854830104" Y="-3.586128883217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.30923034812" Y="-1.16982641083" />
                  <Point X="20.526442660637" Y="-1.262027567264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.705857268251" Y="-1.762659366066" />
                  <Point X="24.762119540744" Y="-3.059965732471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.628790895288" Y="-3.427845896405" />
                  <Point X="25.744156894032" Y="-3.476815857519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.279662912529" Y="-1.054071543188" />
                  <Point X="20.712019243285" Y="-1.237595917223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.792452413229" Y="-1.696212588462" />
                  <Point X="24.902564657432" Y="-3.016376911711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.529764825092" Y="-3.282607587605" />
                  <Point X="25.729765487024" Y="-3.36750283182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.254378218502" Y="-0.940134591484" />
                  <Point X="20.897595825933" Y="-1.213164267182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.879047558207" Y="-1.629765810858" />
                  <Point X="25.243473676535" Y="-3.057879969086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.409950439192" Y="-3.128545162317" />
                  <Point X="25.727384268416" Y="-3.263287828636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.238663944146" Y="-0.830260041912" />
                  <Point X="21.083172408582" Y="-1.188732617141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.964111546581" Y="-1.562669095835" />
                  <Point X="25.747377360142" Y="-3.168570156718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.691220428366" Y="-3.993682585843" />
                  <Point X="27.724367325385" Y="-4.007752608863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.222949669791" Y="-0.72038549234" />
                  <Point X="21.26874899123" Y="-1.1643009671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.021598976123" Y="-1.483866826071" />
                  <Point X="25.76791462764" Y="-3.074083473711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.573775772217" Y="-3.840626051156" />
                  <Point X="27.645439322793" Y="-3.871045423616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.273430861029" Y="-0.638609250859" />
                  <Point X="21.454325573878" Y="-1.139869317059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.04904513994" Y="-1.392312795559" />
                  <Point X="25.790748387825" Y="-2.980571594016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.456331116068" Y="-3.687569516469" />
                  <Point X="27.566511320201" Y="-3.734338238368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.422478572514" Y="-0.598672014945" />
                  <Point X="21.639902156527" Y="-1.115437667018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.042166803756" Y="-1.286188879218" />
                  <Point X="25.838817806755" Y="-2.897771615928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.338886459918" Y="-3.534512981781" />
                  <Point X="27.487583317609" Y="-3.59763105312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.571526283999" Y="-0.55873477903" />
                  <Point X="25.917111352025" Y="-2.827801018313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.221441803769" Y="-3.381456447094" />
                  <Point X="27.408655315017" Y="-3.460923867873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.720573995484" Y="-0.518797543116" />
                  <Point X="25.999284366936" Y="-2.759477157862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.10399714762" Y="-3.228399912407" />
                  <Point X="27.329727312425" Y="-3.324216682625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.869621706969" Y="-0.478860307202" />
                  <Point X="26.090643136005" Y="-2.695052418718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.986552491471" Y="-3.07534337772" />
                  <Point X="27.250799309832" Y="-3.187509497378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.018669418455" Y="-0.438923071287" />
                  <Point X="26.253099703924" Y="-2.660806904674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.869107835322" Y="-2.922286843033" />
                  <Point X="27.17187130724" Y="-3.05080231213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.16771712994" Y="-0.398985835373" />
                  <Point X="26.47202281337" Y="-2.650530015466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.579955793202" Y="-2.696344847244" />
                  <Point X="27.092943304648" Y="-2.914095126883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.316764841425" Y="-0.359048599458" />
                  <Point X="27.039786987179" Y="-2.788327372941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.465812523425" Y="-0.319111351028" />
                  <Point X="27.019600393838" Y="-2.676554436589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.583329906587" Y="-0.265790284794" />
                  <Point X="27.000797078209" Y="-2.56536866679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.654707669935" Y="-0.192884111919" />
                  <Point X="27.010033519142" Y="-2.466085067504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.694520932235" Y="-0.106579603263" />
                  <Point X="27.050101409653" Y="-2.379888642111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.219152096832" Y="0.622881547839" />
                  <Point X="20.42543333735" Y="0.535320356183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.703564112459" Y="-0.007213969673" />
                  <Point X="27.094498866356" Y="-2.295530008532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.233520667621" Y="0.719986687248" />
                  <Point X="21.084779571565" Y="0.35864872045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.666775290118" Y="0.111606194782" />
                  <Point X="27.14397399412" Y="-2.213326718443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.24788923841" Y="0.817091826657" />
                  <Point X="27.223952979361" Y="-2.14407154765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.262257809199" Y="0.914196966066" />
                  <Point X="27.332501310713" Y="-2.086943344797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.282558468578" Y="1.00878408326" />
                  <Point X="27.448893154956" Y="-2.033144515637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.555420106027" Y="-2.502837339824" />
                  <Point X="29.038213051505" Y="-2.707770786622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.307639800452" Y="1.101341925376" />
                  <Point X="27.770746472877" Y="-2.066558907754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.880337362376" Y="-2.113077480432" />
                  <Point X="29.073661377821" Y="-2.619613472567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.332721132326" Y="1.193899767492" />
                  <Point X="28.772644219218" Y="-2.38863503364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.3578024642" Y="1.286457609608" />
                  <Point X="28.471627060615" Y="-2.157656594712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.597849032374" Y="1.287768122554" />
                  <Point X="28.170609902012" Y="-1.926678155785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.950295370404" Y="1.241367763849" />
                  <Point X="27.974144129323" Y="-1.740079147178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.276978156063" Y="1.205903384301" />
                  <Point X="27.88587822816" Y="-1.59940825915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.420107330115" Y="1.248352890304" />
                  <Point X="27.859265917202" Y="-1.484907767494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.503410666796" Y="1.31619695763" />
                  <Point X="27.867031162317" Y="-1.384999682633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.548504277026" Y="1.400260091569" />
                  <Point X="27.877185749208" Y="-1.286105813184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.576402650583" Y="1.491622170435" />
                  <Point X="27.90973626652" Y="-1.196718452184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.565676871764" Y="1.599379229281" />
                  <Point X="27.968487380371" Y="-1.118452584585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.491793877926" Y="1.733944935365" />
                  <Point X="28.031913385947" Y="-1.042171090791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.215028999394" Y="1.954628892167" />
                  <Point X="28.116565042778" Y="-0.974899351413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.914013275282" Y="2.185606722189" />
                  <Point X="28.260985901021" Y="-0.932998132819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.797949962549" Y="2.338076911383" />
                  <Point X="28.453305855439" Y="-0.91142887427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.846227636997" Y="2.420788490249" />
                  <Point X="28.805752539231" Y="-0.957829379743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.894505311444" Y="2.503500069114" />
                  <Point X="29.158199334339" Y="-1.004229932467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.942782985892" Y="2.586211647979" />
                  <Point X="29.510646129447" Y="-1.05063048519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.991060660339" Y="2.668923226845" />
                  <Point X="29.736044795989" Y="-1.043102306891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.049754840131" Y="2.747213261519" />
                  <Point X="29.757516332077" Y="-0.949012197372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.110114265565" Y="2.824796441355" />
                  <Point X="28.418318170374" Y="-0.277352067962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.269488476572" Y="-0.638652427248" />
                  <Point X="29.773449510874" Y="-0.85257119466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.170473690999" Y="2.902379621191" />
                  <Point X="28.36636703401" Y="-0.152095883048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.230833116432" Y="2.979962801027" />
                  <Point X="21.773379440194" Y="2.749665549962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.77421870548" Y="2.749309302984" />
                  <Point X="28.348805809312" Y="-0.041437349569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.039352907616" Y="2.739970747115" />
                  <Point X="28.360128712456" Y="0.056960599054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.131926496215" Y="2.803879825962" />
                  <Point X="28.382920602141" Y="0.150490251722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.201596233584" Y="2.877511012851" />
                  <Point X="28.432365491792" Y="0.232706377129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.243492946273" Y="2.962931149286" />
                  <Point X="28.496315101177" Y="0.308765614292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.249528409374" Y="3.063573483049" />
                  <Point X="28.590321935178" Y="0.37206631656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.234867066225" Y="3.173001089841" />
                  <Point X="28.70753371056" Y="0.425517105601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.169131778187" Y="3.304108300003" />
                  <Point X="28.856581402786" Y="0.46545434969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.090203201835" Y="3.440815728797" />
                  <Point X="29.005629041068" Y="0.505391616678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.011274625483" Y="3.577523157592" />
                  <Point X="29.154676679349" Y="0.545328883665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.949684287381" Y="3.706870940891" />
                  <Point X="28.125140708669" Y="1.085545211454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.437839895828" Y="0.952812281456" />
                  <Point X="29.303724317631" Y="0.585266150653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.036325737462" Y="3.773298063145" />
                  <Point X="28.035385988093" Y="1.226848065828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.638302002402" Y="0.970925401465" />
                  <Point X="29.452771955912" Y="0.62520341764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.122967187543" Y="3.8397251854" />
                  <Point X="28.006847721194" Y="1.342166077279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.823878621865" Y="0.995357035879" />
                  <Point X="29.601819594194" Y="0.665140684628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.209608637623" Y="3.906152307654" />
                  <Point X="28.004913933291" Y="1.446191157397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.009455241328" Y="1.019788670293" />
                  <Point X="29.750867232475" Y="0.705077951615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.296250087704" Y="3.972579429908" />
                  <Point X="28.02914323998" Y="1.539110662747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.195031860791" Y="1.044220304707" />
                  <Point X="29.77047553353" Y="0.799958957482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.389625465384" Y="4.036148169483" />
                  <Point X="28.071137431332" Y="1.624489421944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.380608480254" Y="1.068651939121" />
                  <Point X="29.753270159267" Y="0.910466441414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.494930529999" Y="4.094653057388" />
                  <Point X="27.097540009377" Y="2.140961244544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.479151418773" Y="1.978976811677" />
                  <Point X="28.131070129765" Y="1.702253736645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.566185099717" Y="1.093083573535" />
                  <Point X="29.72561665428" Y="1.025408893714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.600235594614" Y="4.153157945293" />
                  <Point X="27.027489752007" Y="2.27390005052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.622600963451" Y="2.021290328418" />
                  <Point X="28.209754451554" Y="1.772058459469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.705540659229" Y="4.211662833198" />
                  <Point X="22.980905190033" Y="4.094777524595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.127398894432" Y="4.032594636344" />
                  <Point X="27.012884146431" Y="2.383303998116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.72678884157" Y="2.080269433856" />
                  <Point X="28.296349497745" Y="1.838505279006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.298566414577" Y="4.063142570543" />
                  <Point X="27.025414554448" Y="2.48118939133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.829805102009" Y="2.139745861493" />
                  <Point X="28.382944543937" Y="1.904952098542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.410348317355" Y="4.118898203759" />
                  <Point X="27.052972269686" Y="2.572696071073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.932821362448" Y="2.19922228913" />
                  <Point X="28.469539590128" Y="1.971398918079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.475538286024" Y="4.194430939643" />
                  <Point X="27.098727376881" Y="2.656478416209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.035837622887" Y="2.258698716767" />
                  <Point X="28.55613463632" Y="2.037845737616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.509575304518" Y="4.283187318327" />
                  <Point X="27.14658409353" Y="2.739368681058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.138853883326" Y="2.318175144404" />
                  <Point X="28.642729682511" Y="2.104292557153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.534814064506" Y="4.375678336173" />
                  <Point X="27.19444081018" Y="2.822258945907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.241870143764" Y="2.377651572042" />
                  <Point X="28.729324728703" Y="2.170739376689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.531583677074" Y="4.480253790138" />
                  <Point X="27.242297526829" Y="2.905149210757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.344886404203" Y="2.437127999679" />
                  <Point X="28.815919774894" Y="2.237186196226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.556782996309" Y="4.57276154959" />
                  <Point X="27.290154243479" Y="2.988039475606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.447902664642" Y="2.496604427316" />
                  <Point X="28.902514821086" Y="2.303633015763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.703205294038" Y="4.613813207527" />
                  <Point X="27.338010960128" Y="3.070929740455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.550918925081" Y="2.556080854953" />
                  <Point X="28.989109867278" Y="2.370079835299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.849627591767" Y="4.654864865463" />
                  <Point X="24.774864388923" Y="4.26212514604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.12090235425" Y="4.115240744306" />
                  <Point X="27.385867676778" Y="3.153820005305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.65393518552" Y="2.61555728259" />
                  <Point X="29.075704913469" Y="2.436526654836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.996049889496" Y="4.695916523399" />
                  <Point X="24.742941642703" Y="4.378879783728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.199027166306" Y="4.18528296492" />
                  <Point X="27.433724393428" Y="3.236710270154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.756951445959" Y="2.675033710227" />
                  <Point X="29.112219761653" Y="2.524231257218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.164686187905" Y="4.727538897479" />
                  <Point X="24.711739456519" Y="4.495328561827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.240440428915" Y="4.270908313739" />
                  <Point X="27.481581110077" Y="3.319600535003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.859967706398" Y="2.734510137864" />
                  <Point X="29.023883115926" Y="2.664932174531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.355271805169" Y="4.749844338472" />
                  <Point X="24.680537270335" Y="4.611777339926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.265269686406" Y="4.363573155083" />
                  <Point X="27.529437826727" Y="3.402490799852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.545857422434" Y="4.772149779465" />
                  <Point X="24.649335084151" Y="4.728226118025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.290098943897" Y="4.456237996426" />
                  <Point X="27.577294543376" Y="3.485381064702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.314928201388" Y="4.54890283777" />
                  <Point X="27.625151260026" Y="3.568271329551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.339757458878" Y="4.641567679113" />
                  <Point X="27.673007976676" Y="3.6511615944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.364586716369" Y="4.734232520457" />
                  <Point X="27.720864693325" Y="3.734051859249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.524105331576" Y="4.769725121438" />
                  <Point X="27.768721409975" Y="3.816942124099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.861059654332" Y="4.729900733069" />
                  <Point X="27.816578126624" Y="3.899832388948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.424884720824" Y="4.593775427449" />
                  <Point X="27.334636586689" Y="4.207608671389" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.479626953125" Y="-3.567288085938" />
                  <Point X="25.471541015625" Y="-3.537111816406" />
                  <Point X="25.461015625" Y="-3.516782470703" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.28907421875" Y="-3.273825195312" />
                  <Point X="25.26922265625" Y="-3.2648125" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="25.00615625" Y="-3.18551953125" />
                  <Point X="24.98622265625" Y="-3.189353759766" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.72323828125" Y="-3.273825439453" />
                  <Point X="24.708416015625" Y="-3.290405517578" />
                  <Point X="24.547994140625" Y="-3.521544433594" />
                  <Point X="24.5407734375" Y="-3.537111816406" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.89524609375" Y="-4.936905273438" />
                  <Point X="23.648412109375" Y="-4.873396484375" />
                  <Point X="23.689876953125" Y="-4.558431640625" />
                  <Point X="23.690849609375" Y="-4.551046875" />
                  <Point X="23.689095703125" Y="-4.528615234375" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.6240546875" Y="-4.215224609375" />
                  <Point X="23.609009765625" Y="-4.198499511719" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.366625" Y="-3.989461914063" />
                  <Point X="23.344509765625" Y="-3.985353271484" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.024958984375" Y="-3.967068115234" />
                  <Point X="23.0049140625" Y="-3.977270263672" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740265625" Y="-4.157296386719" />
                  <Point X="22.542904296875" Y="-4.4145" />
                  <Point X="22.144166015625" Y="-4.167609863281" />
                  <Point X="22.137919921875" Y="-4.162801269531" />
                  <Point X="21.771419921875" Y="-3.880608398438" />
                  <Point X="22.478939453125" Y="-2.655147216797" />
                  <Point X="22.493966796875" Y="-2.629120849609" />
                  <Point X="22.5002421875" Y="-2.597620117188" />
                  <Point X="22.485671875" Y="-2.568414306641" />
                  <Point X="22.468673828125" Y="-2.551416015625" />
                  <Point X="22.439845703125" Y="-2.537198730469" />
                  <Point X="22.408318359375" Y="-2.543469238281" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.83830078125" Y="-2.847135253906" />
                  <Point X="20.8338203125" Y="-2.839621826172" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.809921875" Y="-1.443318603516" />
                  <Point X="21.836212890625" Y="-1.42314453125" />
                  <Point X="21.854333984375" Y="-1.395412719727" />
                  <Point X="21.8618828125" Y="-1.366265869141" />
                  <Point X="21.859669921875" Y="-1.334582763672" />
                  <Point X="21.838283203125" Y="-1.310311157227" />
                  <Point X="21.812359375" Y="-1.295053100586" />
                  <Point X="21.780470703125" Y="-1.288571044922" />
                  <Point X="20.19671875" Y="-1.497076049805" />
                  <Point X="20.072607421875" Y="-1.011186523437" />
                  <Point X="20.071423828125" Y="-1.002910766602" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="21.412587890625" Y="-0.136670379639" />
                  <Point X="21.442537109375" Y="-0.128645339966" />
                  <Point X="21.4586640625" Y="-0.121036483765" />
                  <Point X="21.485857421875" Y="-0.102163269043" />
                  <Point X="21.505314453125" Y="-0.075220100403" />
                  <Point X="21.5143515625" Y="-0.04610112381" />
                  <Point X="21.514166015625" Y="-0.015857367516" />
                  <Point X="21.5051015625" Y="0.013348042488" />
                  <Point X="21.485296875" Y="0.039991966248" />
                  <Point X="21.458103515625" Y="0.058865337372" />
                  <Point X="21.442537109375" Y="0.066085227966" />
                  <Point X="20.001814453125" Y="0.452125915527" />
                  <Point X="20.08235546875" Y="0.996413513184" />
                  <Point X="20.0847421875" Y="1.005222412109" />
                  <Point X="20.226484375" Y="1.528298706055" />
                  <Point X="21.225267578125" Y="1.396806640625" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.26831640625" Y="1.395872802734" />
                  <Point X="21.26953515625" Y="1.396257568359" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348486328125" Y="1.426073486328" />
                  <Point X="21.360900390625" Y="1.443833374023" />
                  <Point X="21.361376953125" Y="1.444986694336" />
                  <Point X="21.38552734375" Y="1.503289672852" />
                  <Point X="21.389287109375" Y="1.524593139648" />
                  <Point X="21.3836953125" Y="1.545490478516" />
                  <Point X="21.383083984375" Y="1.546665649414" />
                  <Point X="21.353943359375" Y="1.602642211914" />
                  <Point X="21.34003125" Y="1.619221801758" />
                  <Point X="20.52389453125" Y="2.245465820312" />
                  <Point X="20.839986328125" Y="2.787005859375" />
                  <Point X="20.846306640625" Y="2.795131347656" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.827658203125" Y="2.934556396484" />
                  <Point X="21.84084375" Y="2.926943603516" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.863212890625" Y="2.920283691406" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968484375" Y="2.915771972656" />
                  <Point X="21.986734375" Y="2.927390625" />
                  <Point X="21.987974609375" Y="2.928630126953" />
                  <Point X="22.04747265625" Y="2.988127929688" />
                  <Point X="22.05909765625" Y="3.006372802734" />
                  <Point X="22.061927734375" Y="3.027821044922" />
                  <Point X="22.06177734375" Y="3.029568603516" />
                  <Point X="22.054443359375" Y="3.113391113281" />
                  <Point X="22.047935546875" Y="3.134029541016" />
                  <Point X="21.692720703125" Y="3.749276611328" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.25707421875" Y="4.179859375" />
                  <Point X="22.858453125" Y="4.513972167969" />
                  <Point X="23.028138671875" Y="4.29283203125" />
                  <Point X="23.03217578125" Y="4.287571289062" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.05067578125" Y="4.27266015625" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164880859375" Y="4.218491699219" />
                  <Point X="23.18619921875" Y="4.222253417969" />
                  <Point X="23.1881953125" Y="4.223080078125" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30310546875" Y="4.27575390625" />
                  <Point X="23.31392578125" Y="4.294512695312" />
                  <Point X="23.314576171875" Y="4.296579101562" />
                  <Point X="23.346197265625" Y="4.396864746094" />
                  <Point X="23.348083984375" Y="4.418425292969" />
                  <Point X="23.31086328125" Y="4.701141113281" />
                  <Point X="24.03190625" Y="4.903296386719" />
                  <Point X="24.043966796875" Y="4.904707519531" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.953958984375" Y="4.3254609375" />
                  <Point X="24.957859375" Y="4.310904296875" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.2366484375" Y="4.990868652344" />
                  <Point X="25.860205078125" Y="4.925565429688" />
                  <Point X="25.870185546875" Y="4.92315625" />
                  <Point X="26.5084609375" Y="4.769057128906" />
                  <Point X="26.514470703125" Y="4.766877441406" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="26.93731640625" Y="4.61284765625" />
                  <Point X="27.33869921875" Y="4.425135253906" />
                  <Point X="27.34477734375" Y="4.421593261719" />
                  <Point X="27.73252734375" Y="4.195689941406" />
                  <Point X="27.73825" Y="4.191620117188" />
                  <Point X="28.068740234375" Y="3.956593261719" />
                  <Point X="27.247244140625" Y="2.533720214844" />
                  <Point X="27.229853515625" Y="2.503596435547" />
                  <Point X="27.224419921875" Y="2.48989453125" />
                  <Point X="27.2033828125" Y="2.411228759766" />
                  <Point X="27.20221875" Y="2.390891601562" />
                  <Point X="27.210416015625" Y="2.322901855469" />
                  <Point X="27.21868359375" Y="2.300811767578" />
                  <Point X="27.21955078125" Y="2.299533935547" />
                  <Point X="27.261642578125" Y="2.237503173828" />
                  <Point X="27.27621875" Y="2.223336425781" />
                  <Point X="27.33825" Y="2.18124609375" />
                  <Point X="27.360326171875" Y="2.172981689453" />
                  <Point X="27.36173828125" Y="2.172811035156" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.45028515625" Y="2.166379882812" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417724609" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.202595703125" Y="2.741875244141" />
                  <Point X="29.20578515625" Y="2.73660546875" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.311515625" Y="1.610653320312" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.278205078125" Y="1.582311401367" />
                  <Point X="28.22158984375" Y="1.508451660156" />
                  <Point X="28.212685546875" Y="1.489946655273" />
                  <Point X="28.191595703125" Y="1.414535766602" />
                  <Point X="28.190783203125" Y="1.390944458008" />
                  <Point X="28.191140625" Y="1.389215942383" />
                  <Point X="28.20844921875" Y="1.305332519531" />
                  <Point X="28.2166171875" Y="1.286481201172" />
                  <Point X="28.263703125" Y="1.21491027832" />
                  <Point X="28.280955078125" Y="1.198815429688" />
                  <Point X="28.282361328125" Y="1.198024291992" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.370466796875" Y="1.153368408203" />
                  <Point X="28.462728515625" Y="1.141174926758" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.8489765625" Y="1.321953491211" />
                  <Point X="29.93919140625" Y="0.951367980957" />
                  <Point X="29.9401953125" Y="0.944924621582" />
                  <Point X="29.997859375" Y="0.57455645752" />
                  <Point X="28.76741015625" Y="0.244858352661" />
                  <Point X="28.74116796875" Y="0.237826919556" />
                  <Point X="28.727220703125" Y="0.231739990234" />
                  <Point X="28.636578125" Y="0.179346893311" />
                  <Point X="28.62114453125" Y="0.165498687744" />
                  <Point X="28.566759765625" Y="0.096198623657" />
                  <Point X="28.556984375" Y="0.074724975586" />
                  <Point X="28.55661328125" Y="0.072785186768" />
                  <Point X="28.538484375" Y="-0.021875238419" />
                  <Point X="28.538857421875" Y="-0.042635002136" />
                  <Point X="28.556986328125" Y="-0.137295425415" />
                  <Point X="28.56676953125" Y="-0.158771057129" />
                  <Point X="28.567880859375" Y="-0.160186340332" />
                  <Point X="28.622265625" Y="-0.229486404419" />
                  <Point X="28.6384453125" Y="-0.24298626709" />
                  <Point X="28.729087890625" Y="-0.295379364014" />
                  <Point X="28.74116796875" Y="-0.300387023926" />
                  <Point X="29.998068359375" Y="-0.637172424316" />
                  <Point X="29.94843359375" Y="-0.966394897461" />
                  <Point X="29.94714453125" Y="-0.972047119141" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.442416015625" Y="-1.101634765625" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.391171875" Y="-1.099137939453" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.183232421875" Y="-1.157361328125" />
                  <Point X="28.075703125" Y="-1.286685180664" />
                  <Point X="28.064041015625" Y="-1.317521240234" />
                  <Point X="28.048630859375" Y="-1.485001220703" />
                  <Point X="28.049849609375" Y="-1.501453613281" />
                  <Point X="28.058388671875" Y="-1.519776855469" />
                  <Point X="28.156841796875" Y="-1.672912597656" />
                  <Point X="28.1684609375" Y="-1.685540039062" />
                  <Point X="29.339076171875" Y="-2.583784179688" />
                  <Point X="29.204130859375" Y="-2.802144042969" />
                  <Point X="29.201466796875" Y="-2.805930175781" />
                  <Point X="29.056689453125" Y="-3.011638183594" />
                  <Point X="27.780560546875" Y="-2.274864501953" />
                  <Point X="27.753455078125" Y="-2.259215332031" />
                  <Point X="27.73298046875" Y="-2.252525146484" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.50475" Y="-2.214074462891" />
                  <Point X="27.485455078125" Y="-2.221151855469" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.286693359375" Y="-2.338307128906" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.18894921875" Y="-2.529880615234" />
                  <Point X="27.189955078125" Y="-2.550750732422" />
                  <Point X="27.22819140625" Y="-2.76246484375" />
                  <Point X="27.23409375" Y="-2.778578125" />
                  <Point X="27.98667578125" Y="-4.082087158203" />
                  <Point X="27.8352890625" Y="-4.190218261719" />
                  <Point X="27.8323203125" Y="-4.192140625" />
                  <Point X="27.679775390625" Y="-4.290879394531" />
                  <Point X="26.703861328125" Y="-3.019043945312" />
                  <Point X="26.6831796875" Y="-2.992088867188" />
                  <Point X="26.666248046875" Y="-2.977701416016" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.44226171875" Y="-2.836937255859" />
                  <Point X="26.421099609375" Y="-2.836150146484" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.161701171875" Y="-2.871529785156" />
                  <Point X="25.985349609375" Y="-3.018159667969" />
                  <Point X="25.974576171875" Y="-3.030659667969" />
                  <Point X="25.96737109375" Y="-3.050983642578" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310718505859" />
                  <Point X="26.12764453125" Y="-4.934028808594" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.99158984375" Y="-4.963748046875" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#118" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.00390064914" Y="4.369681895088" Z="0.05" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.05" />
                  <Point X="-0.968003380622" Y="4.985648828303" Z="0.05" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.05" />
                  <Point X="-1.735049949642" Y="4.773202724265" Z="0.05" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.05" />
                  <Point X="-1.749657495632" Y="4.76229068577" Z="0.05" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.05" />
                  <Point X="-1.739273588101" Y="4.342870572963" Z="0.05" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.05" />
                  <Point X="-1.837097220393" Y="4.300553770622" Z="0.05" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.05" />
                  <Point X="-1.932393262754" Y="4.348290817886" Z="0.05" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.05" />
                  <Point X="-1.93835169858" Y="4.354551788394" Z="0.05" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.05" />
                  <Point X="-2.773365456428" Y="4.254846840066" Z="0.05" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.05" />
                  <Point X="-3.366716927053" Y="3.802710162212" Z="0.05" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.05" />
                  <Point X="-3.371056583724" Y="3.780360893541" Z="0.05" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.05" />
                  <Point X="-2.99419109081" Y="3.056490372642" Z="0.05" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.05" />
                  <Point X="-3.053537848399" Y="2.995265719487" Z="0.05" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.05" />
                  <Point X="-3.138586092965" Y="3.001373608363" Z="0.05" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.05" />
                  <Point X="-3.153498454756" Y="3.009137365022" Z="0.05" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.05" />
                  <Point X="-4.19931624633" Y="2.857109324516" Z="0.05" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.05" />
                  <Point X="-4.540791668474" Y="2.275656880946" Z="0.05" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.05" />
                  <Point X="-4.530474828928" Y="2.250717646477" Z="0.05" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.05" />
                  <Point X="-3.667422733659" Y="1.554857547071" Z="0.05" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.05" />
                  <Point X="-3.690972397514" Y="1.495401206497" Z="0.05" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.05" />
                  <Point X="-3.751656180949" Y="1.475223470073" Z="0.05" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.05" />
                  <Point X="-3.774364875135" Y="1.477658957397" Z="0.05" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.05" />
                  <Point X="-4.969675146706" Y="1.04957976763" Z="0.05" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.05" />
                  <Point X="-5.058560937174" Y="0.458578267408" Z="0.05" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.05" />
                  <Point X="-5.030377171099" Y="0.438617977872" Z="0.05" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.05" />
                  <Point X="-3.549368052329" Y="0.03019575693" Z="0.05" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.05" />
                  <Point X="-3.539743475892" Y="0.00060165449" Z="0.05" />
                  <Point X="-3.539556741714" Y="0" Z="0.05" />
                  <Point X="-3.548621125243" Y="-0.029205296571" Z="0.05" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.05" />
                  <Point X="-3.576000542794" Y="-0.048680226204" Z="0.05" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.05" />
                  <Point X="-3.606510617673" Y="-0.057094078884" Z="0.05" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.05" />
                  <Point X="-4.984229401837" Y="-0.978709775635" Z="0.05" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.05" />
                  <Point X="-4.849643612815" Y="-1.510431759269" Z="0.05" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.05" />
                  <Point X="-4.814047238116" Y="-1.516834302359" Z="0.05" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.05" />
                  <Point X="-3.193210857741" Y="-1.322135189324" Z="0.05" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.05" />
                  <Point X="-3.200225286589" Y="-1.351596718477" Z="0.05" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.05" />
                  <Point X="-3.226672215948" Y="-1.372371280762" Z="0.05" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.05" />
                  <Point X="-4.215280109534" Y="-2.833951380975" Z="0.05" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.05" />
                  <Point X="-3.86924543407" Y="-3.290720878224" Z="0.05" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.05" />
                  <Point X="-3.836212306232" Y="-3.284899591101" Z="0.05" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.05" />
                  <Point X="-2.555841151336" Y="-2.572489317796" Z="0.05" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.05" />
                  <Point X="-2.570517422067" Y="-2.59886607626" Z="0.05" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.05" />
                  <Point X="-2.89874028676" Y="-4.171138553667" Z="0.05" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.05" />
                  <Point X="-2.459986207747" Y="-4.444050442405" Z="0.05" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.05" />
                  <Point X="-2.446578229853" Y="-4.443625547797" Z="0.05" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.05" />
                  <Point X="-1.973463056298" Y="-3.987563618858" Z="0.05" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.05" />
                  <Point X="-1.66491591836" Y="-4.003966348519" Z="0.05" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.05" />
                  <Point X="-1.430114359069" Y="-4.204811361994" Z="0.05" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.05" />
                  <Point X="-1.366100284126" Y="-4.507090090598" Z="0.05" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.05" />
                  <Point X="-1.365851868284" Y="-4.520625426187" Z="0.05" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.05" />
                  <Point X="-1.12337049826" Y="-4.954047888065" Z="0.05" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.05" />
                  <Point X="-0.823718218124" Y="-5.012588597834" Z="0.05" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.05" />
                  <Point X="-0.809582330895" Y="-4.983586521031" Z="0.05" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.05" />
                  <Point X="-0.256663929494" Y="-3.287634335822" Z="0.05" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.05" />
                  <Point X="-0.005114063067" Y="-3.205826584888" Z="0.05" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.05" />
                  <Point X="0.248245016294" Y="-3.2812854604" Z="0.05" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.05" />
                  <Point X="0.413781929938" Y="-3.514011419946" Z="0.05" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.05" />
                  <Point X="0.425172529506" Y="-3.548949508784" Z="0.05" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.05" />
                  <Point X="0.994369946537" Y="-4.981662670772" Z="0.05" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.05" />
                  <Point X="1.173438988355" Y="-4.942547478528" Z="0.05" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.05" />
                  <Point X="1.172618176336" Y="-4.9080696609" Z="0.05" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.05" />
                  <Point X="1.010073773645" Y="-3.030323580723" Z="0.05" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.05" />
                  <Point X="1.187510772313" Y="-2.878696142601" Z="0.05" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.05" />
                  <Point X="1.419525591962" Y="-2.854659535104" Z="0.05" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.05" />
                  <Point X="1.63305199199" Y="-2.988479542273" Z="0.05" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.05" />
                  <Point X="1.658037361379" Y="-3.01820047973" Z="0.05" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.05" />
                  <Point X="2.853332915767" Y="-4.202834719354" Z="0.05" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.05" />
                  <Point X="3.042692806531" Y="-4.067843367192" Z="0.05" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.05" />
                  <Point X="3.030863632857" Y="-4.03801017363" Z="0.05" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.05" />
                  <Point X="2.232998849965" Y="-2.510569975499" Z="0.05" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.05" />
                  <Point X="2.324783132933" Y="-2.330313774627" Z="0.05" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.05" />
                  <Point X="2.502584603746" Y="-2.234118177034" Z="0.05" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.05" />
                  <Point X="2.717936828013" Y="-2.270449159526" Z="0.05" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.05" />
                  <Point X="2.749403423477" Y="-2.286885867417" Z="0.05" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.05" />
                  <Point X="4.236197797082" Y="-2.803427478795" Z="0.05" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.05" />
                  <Point X="4.395989308518" Y="-2.545556071662" Z="0.05" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.05" />
                  <Point X="4.374855987248" Y="-2.521660483069" Z="0.05" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.05" />
                  <Point X="3.094290705691" Y="-1.461457228435" Z="0.05" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.05" />
                  <Point X="3.107673967496" Y="-1.29082246569" Z="0.05" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.05" />
                  <Point X="3.2155203405" Y="-1.158048366925" Z="0.05" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.05" />
                  <Point X="3.395634976776" Y="-1.116717001991" Z="0.05" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.05" />
                  <Point X="3.429732983189" Y="-1.119927020836" Z="0.05" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.05" />
                  <Point X="4.989734132655" Y="-0.951891064569" Z="0.05" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.05" />
                  <Point X="5.04687311413" Y="-0.576735963862" Z="0.05" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.05" />
                  <Point X="5.02177330617" Y="-0.562129824021" Z="0.05" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.05" />
                  <Point X="3.657310032407" Y="-0.168417550475" Z="0.05" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.05" />
                  <Point X="3.601057114339" Y="-0.09803817216" Z="0.05" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.05" />
                  <Point X="3.581808190258" Y="-0.001950087443" Z="0.05" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.05" />
                  <Point X="3.599563260166" Y="0.094660443805" Z="0.05" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.05" />
                  <Point X="3.654322324062" Y="0.165910566415" Z="0.05" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.05" />
                  <Point X="3.746085381945" Y="0.219731293767" Z="0.05" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.05" />
                  <Point X="3.774194511136" Y="0.227842108748" Z="0.05" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.05" />
                  <Point X="4.983443123711" Y="0.983896672156" Z="0.05" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.05" />
                  <Point X="4.882830763218" Y="1.400261684516" Z="0.05" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.05" />
                  <Point X="4.852169860137" Y="1.404895836607" Z="0.05" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.05" />
                  <Point X="3.370861258359" Y="1.234217386644" Z="0.05" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.05" />
                  <Point X="3.300724279901" Y="1.272879727956" Z="0.05" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.05" />
                  <Point X="3.252231039542" Y="1.345241980211" Z="0.05" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.05" />
                  <Point X="3.233948682095" Y="1.430620703531" Z="0.05" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.05" />
                  <Point X="3.254681524036" Y="1.507760171147" Z="0.05" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.05" />
                  <Point X="3.31173182357" Y="1.583173524272" Z="0.05" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.05" />
                  <Point X="3.335796344946" Y="1.602265496211" Z="0.05" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.05" />
                  <Point X="4.242405230237" Y="2.793771726203" Z="0.05" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.05" />
                  <Point X="4.007023273151" Y="3.122008330542" Z="0.05" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.05" />
                  <Point X="3.972137346875" Y="3.11123459334" Z="0.05" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.05" />
                  <Point X="2.431212237163" Y="2.245962539392" Z="0.05" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.05" />
                  <Point X="2.361568131808" Y="2.253731551859" Z="0.05" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.05" />
                  <Point X="2.298135961221" Y="2.295990972545" Z="0.05" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.05" />
                  <Point X="2.25476762769" Y="2.358888899161" Z="0.05" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.05" />
                  <Point X="2.2456981509" Y="2.428190312024" Z="0.05" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.05" />
                  <Point X="2.266565403266" Y="2.508257338719" Z="0.05" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.05" />
                  <Point X="2.284390759519" Y="2.54000173508" Z="0.05" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.05" />
                  <Point X="2.761069827592" Y="4.263647221515" Z="0.05" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.05" />
                  <Point X="2.363791019259" Y="4.496076097035" Z="0.05" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.05" />
                  <Point X="1.952342054843" Y="4.689419755257" Z="0.05" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.05" />
                  <Point X="1.52536251974" Y="4.84516068365" Z="0.05" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.05" />
                  <Point X="0.875763290288" Y="5.003039966007" Z="0.05" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.05" />
                  <Point X="0.206754693237" Y="5.074908160767" Z="0.05" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.05" />
                  <Point X="0.189343912413" Y="5.061765610863" Z="0.05" />
                  <Point X="0" Y="4.355124473572" Z="0.05" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>