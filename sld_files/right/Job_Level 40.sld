<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#195" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2883" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.871037109375" Y="-4.661000488281" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.4165859375" Y="-3.286154296875" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.107861328125" Y="-3.11526171875" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.768541015625" Y="-3.157443115234" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.18977734375" />
                  <Point X="24.655560546875" Y="-3.209021484375" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.5078984375" Y="-3.412699707031" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481571777344" />
                  <Point X="24.449009765625" Y="-3.512524658203" />
                  <Point X="24.356162109375" Y="-3.859038574219" />
                  <Point X="24.0834140625" Y="-4.876941894531" />
                  <Point X="23.985857421875" Y="-4.858005371094" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.75358203125" Y="-4.802362304688" />
                  <Point X="23.7850390625" Y="-4.563436523438" />
                  <Point X="23.78580078125" Y="-4.547928710938" />
                  <Point X="23.7834921875" Y="-4.516223632812" />
                  <Point X="23.7369921875" Y="-4.282461914062" />
                  <Point X="23.722962890625" Y="-4.211932128906" />
                  <Point X="23.712060546875" Y="-4.182966308594" />
                  <Point X="23.69598828125" Y="-4.155128417969" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.497162109375" Y="-3.974053955078" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.119140625" Y="-3.875377929688" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.75916796875" Y="-4.027217529297" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.664900390625" Y="-4.0994609375" />
                  <Point X="22.612771484375" Y="-4.167394042969" />
                  <Point X="22.519853515625" Y="-4.288489257813" />
                  <Point X="22.2938515625" Y="-4.148555175781" />
                  <Point X="22.198287109375" Y="-4.089384033203" />
                  <Point X="21.89527734375" Y="-3.856077636719" />
                  <Point X="22.0043515625" Y="-3.667157470703" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647654296875" />
                  <Point X="22.593412109375" Y="-2.616127441406" />
                  <Point X="22.59442578125" Y="-2.585193603516" />
                  <Point X="22.58544140625" Y="-2.555575683594" />
                  <Point X="22.571224609375" Y="-2.526746582031" />
                  <Point X="22.5531953125" Y="-2.501588134766" />
                  <Point X="22.535853515625" Y="-2.484246582031" />
                  <Point X="22.5107109375" Y="-2.466225585938" />
                  <Point X="22.481880859375" Y="-2.452002685547" />
                  <Point X="22.452259765625" Y="-2.443013183594" />
                  <Point X="22.4213203125" Y="-2.444023681641" />
                  <Point X="22.3897890625" Y="-2.450294189453" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="22.061955078125" Y="-2.633745117188" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="20.99263671875" Y="-2.893048095703" />
                  <Point X="20.917142578125" Y="-2.793862060547" />
                  <Point X="20.695830078125" Y="-2.422758544922" />
                  <Point X="20.693857421875" Y="-2.419450195312" />
                  <Point X="20.893427734375" Y="-2.266314208984" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.915419921875" Y="-1.475596191406" />
                  <Point X="21.933384765625" Y="-1.448466918945" />
                  <Point X="21.946142578125" Y="-1.419839111328" />
                  <Point X="21.9520625" Y="-1.396986694336" />
                  <Point X="21.953849609375" Y="-1.390085449219" />
                  <Point X="21.956654296875" Y="-1.359645874023" />
                  <Point X="21.954443359375" Y="-1.32797668457" />
                  <Point X="21.947439453125" Y="-1.298231933594" />
                  <Point X="21.93135546875" Y="-1.272249267578" />
                  <Point X="21.910521484375" Y="-1.248294677734" />
                  <Point X="21.88702734375" Y="-1.228766845703" />
                  <Point X="21.86668359375" Y="-1.21679296875" />
                  <Point X="21.860544921875" Y="-1.213180419922" />
                  <Point X="21.831279296875" Y="-1.201955322266" />
                  <Point X="21.79939453125" Y="-1.195474609375" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="21.390787109375" Y="-1.244054321289" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.195451171875" Y="-1.108256347656" />
                  <Point X="20.16592578125" Y="-0.992652893066" />
                  <Point X="20.107576171875" Y="-0.584698303223" />
                  <Point X="20.327271484375" Y="-0.525831115723" />
                  <Point X="21.467125" Y="-0.220408279419" />
                  <Point X="21.482505859375" Y="-0.214828155518" />
                  <Point X="21.51226953125" Y="-0.199471221924" />
                  <Point X="21.53358984375" Y="-0.184673629761" />
                  <Point X="21.5400234375" Y="-0.180209030151" />
                  <Point X="21.562474609375" Y="-0.158329818726" />
                  <Point X="21.581720703125" Y="-0.132074783325" />
                  <Point X="21.595833984375" Y="-0.104064453125" />
                  <Point X="21.602939453125" Y="-0.081166275024" />
                  <Point X="21.605083984375" Y="-0.074257530212" />
                  <Point X="21.6093515625" Y="-0.04610269928" />
                  <Point X="21.6093515625" Y="-0.016457435608" />
                  <Point X="21.605083984375" Y="0.011697399139" />
                  <Point X="21.597978515625" Y="0.034595581055" />
                  <Point X="21.595833984375" Y="0.041504470825" />
                  <Point X="21.581720703125" Y="0.069514648438" />
                  <Point X="21.562474609375" Y="0.095769828796" />
                  <Point X="21.540021484375" Y="0.11764919281" />
                  <Point X="21.518701171875" Y="0.132446624756" />
                  <Point X="21.509095703125" Y="0.138303466797" />
                  <Point X="21.48709765625" Y="0.149986618042" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="21.123212890625" Y="0.249999313354" />
                  <Point X="20.108185546875" Y="0.521975402832" />
                  <Point X="20.156484375" Y="0.848377990723" />
                  <Point X="20.17551171875" Y="0.976968811035" />
                  <Point X="20.29337890625" Y="1.411932739258" />
                  <Point X="20.296451171875" Y="1.423267822266" />
                  <Point X="20.417259765625" Y="1.407362792969" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341674805" />
                  <Point X="21.276576171875" Y="1.301227783203" />
                  <Point X="21.296865234375" Y="1.305263427734" />
                  <Point X="21.344052734375" Y="1.320141967773" />
                  <Point X="21.358291015625" Y="1.324630859375" />
                  <Point X="21.37722265625" Y="1.332961791992" />
                  <Point X="21.395966796875" Y="1.343783935547" />
                  <Point X="21.4126484375" Y="1.356015625" />
                  <Point X="21.426287109375" Y="1.371567993164" />
                  <Point X="21.438701171875" Y="1.389297851562" />
                  <Point X="21.448650390625" Y="1.407432617188" />
                  <Point X="21.467583984375" Y="1.453144775391" />
                  <Point X="21.473296875" Y="1.466936889648" />
                  <Point X="21.479083984375" Y="1.486789428711" />
                  <Point X="21.48284375" Y="1.508106567383" />
                  <Point X="21.484197265625" Y="1.528751708984" />
                  <Point X="21.481048828125" Y="1.549200195312" />
                  <Point X="21.4754453125" Y="1.570108154297" />
                  <Point X="21.467951171875" Y="1.589379760742" />
                  <Point X="21.44510546875" Y="1.633267822266" />
                  <Point X="21.4382109375" Y="1.646509643555" />
                  <Point X="21.4267109375" Y="1.663716674805" />
                  <Point X="21.412798828125" Y="1.680293334961" />
                  <Point X="21.39786328125" Y="1.69458996582" />
                  <Point X="21.200595703125" Y="1.845959960938" />
                  <Point X="20.648140625" Y="2.269873535156" />
                  <Point X="20.844908203125" Y="2.606983886719" />
                  <Point X="20.9188515625" Y="2.733664794922" />
                  <Point X="21.23106640625" Y="3.134973144531" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.29151953125" Y="3.134399902344" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.918927734375" Y="2.820046630859" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.00098828125" Y="2.826505126953" />
                  <Point X="22.019541015625" Y="2.835655029297" />
                  <Point X="22.037794921875" Y="2.847284912109" />
                  <Point X="22.053921875" Y="2.860229003906" />
                  <Point X="22.100572265625" Y="2.906877685547" />
                  <Point X="22.114646484375" Y="2.920952392578" />
                  <Point X="22.127595703125" Y="2.937084960938" />
                  <Point X="22.139224609375" Y="2.955338623047" />
                  <Point X="22.14837109375" Y="2.973887939453" />
                  <Point X="22.1532890625" Y="2.993977050781" />
                  <Point X="22.156115234375" Y="3.015434814453" />
                  <Point X="22.15656640625" Y="3.036120117188" />
                  <Point X="22.15081640625" Y="3.101840576172" />
                  <Point X="22.14908203125" Y="3.121669677734" />
                  <Point X="22.145044921875" Y="3.141960449219" />
                  <Point X="22.13853515625" Y="3.162603759766" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="22.0427890625" Y="3.332941650391" />
                  <Point X="21.81666796875" Y="3.724595703125" />
                  <Point X="22.170603515625" Y="3.995954345703" />
                  <Point X="22.29937890625" Y="4.094686279297" />
                  <Point X="22.791107421875" Y="4.367880371094" />
                  <Point X="22.83296484375" Y="4.391134765625" />
                  <Point X="22.9568046875" Y="4.229741699219" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.078033203125" Y="4.151316894531" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.202685546875" Y="4.128692871094" />
                  <Point X="23.222546875" Y="4.134481933594" />
                  <Point X="23.298732421875" Y="4.166040039062" />
                  <Point X="23.321720703125" Y="4.175561523437" />
                  <Point X="23.3398515625" Y="4.185508300781" />
                  <Point X="23.35758203125" Y="4.197922363281" />
                  <Point X="23.373134765625" Y="4.211560546875" />
                  <Point X="23.3853671875" Y="4.228241699219" />
                  <Point X="23.396189453125" Y="4.246985351563" />
                  <Point X="23.404521484375" Y="4.265920898438" />
                  <Point X="23.429318359375" Y="4.344568359375" />
                  <Point X="23.43680078125" Y="4.368297363281" />
                  <Point X="23.4408359375" Y="4.388583007812" />
                  <Point X="23.44272265625" Y="4.410145507812" />
                  <Point X="23.442271484375" Y="4.430827636719" />
                  <Point X="23.43233203125" Y="4.506315917969" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.883662109375" Y="4.763070800781" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.6464921875" Y="4.879576171875" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.717642578125" Y="4.840352539062" />
                  <Point X="24.866095703125" Y="4.286315917969" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.1910078125" Y="4.453477539062" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.698478515625" Y="4.846983398438" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.33723828125" Y="4.712666503906" />
                  <Point X="26.48102734375" Y="4.677950195312" />
                  <Point X="26.80170703125" Y="4.561638183594" />
                  <Point X="26.894646484375" Y="4.527928222656" />
                  <Point X="27.205060546875" Y="4.3827578125" />
                  <Point X="27.294576171875" Y="4.340893554688" />
                  <Point X="27.594474609375" Y="4.166173339844" />
                  <Point X="27.680978515625" Y="4.115775878906" />
                  <Point X="27.94326171875" Y="3.929253417969" />
                  <Point X="27.8095" Y="3.697571289062" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.539935546875" />
                  <Point X="27.133078125" Y="2.516058349609" />
                  <Point X="27.116583984375" Y="2.454381103516" />
                  <Point X="27.114677734375" Y="2.445608886719" />
                  <Point X="27.10836328125" Y="2.408099365234" />
                  <Point X="27.107728515625" Y="2.380954589844" />
                  <Point X="27.11416015625" Y="2.327621337891" />
                  <Point X="27.116099609375" Y="2.311529785156" />
                  <Point X="27.121439453125" Y="2.289613525391" />
                  <Point X="27.129705078125" Y="2.267521728516" />
                  <Point X="27.1400703125" Y="2.247469726562" />
                  <Point X="27.173072265625" Y="2.198834960938" />
                  <Point X="27.178634765625" Y="2.191441894531" />
                  <Point X="27.201892578125" Y="2.163468505859" />
                  <Point X="27.221599609375" Y="2.145592529297" />
                  <Point X="27.270234375" Y="2.112591796875" />
                  <Point X="27.284908203125" Y="2.102635009766" />
                  <Point X="27.304953125" Y="2.092272705078" />
                  <Point X="27.327041015625" Y="2.084006591797" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.402296875" Y="2.072232421875" />
                  <Point X="27.412033203125" Y="2.071563232422" />
                  <Point X="27.44702734375" Y="2.070960449219" />
                  <Point X="27.473205078125" Y="2.074171142578" />
                  <Point X="27.5348828125" Y="2.090664550781" />
                  <Point X="27.5397109375" Y="2.092093505859" />
                  <Point X="27.570404296875" Y="2.102071289063" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="27.9344453125" Y="2.309855957031" />
                  <Point X="28.967326171875" Y="2.906190673828" />
                  <Point X="29.071958984375" Y="2.760775390625" />
                  <Point X="29.12328125" Y="2.689449951172" />
                  <Point X="29.26219921875" Y="2.459883300781" />
                  <Point X="29.10226171875" Y="2.337157714844" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.221427734375" Y="1.660242797852" />
                  <Point X="28.20397265625" Y="1.641626831055" />
                  <Point X="28.159583984375" Y="1.583717529297" />
                  <Point X="28.14619140625" Y="1.566245483398" />
                  <Point X="28.136609375" Y="1.550915893555" />
                  <Point X="28.121630859375" Y="1.51708972168" />
                  <Point X="28.105095703125" Y="1.457964111328" />
                  <Point X="28.10010546875" Y="1.44012512207" />
                  <Point X="28.09665234375" Y="1.417826904297" />
                  <Point X="28.0958359375" Y="1.394252563477" />
                  <Point X="28.097740234375" Y="1.371766357422" />
                  <Point X="28.111314453125" Y="1.305981811523" />
                  <Point X="28.11541015625" Y="1.286133544922" />
                  <Point X="28.1206796875" Y="1.268977905273" />
                  <Point X="28.13628125" Y="1.235741577148" />
                  <Point X="28.17319921875" Y="1.179626708984" />
                  <Point X="28.184337890625" Y="1.162696166992" />
                  <Point X="28.19889453125" Y="1.145448974609" />
                  <Point X="28.216138671875" Y="1.129359863281" />
                  <Point X="28.23434765625" Y="1.11603515625" />
                  <Point X="28.287849609375" Y="1.085919189453" />
                  <Point X="28.296033203125" Y="1.081818847656" />
                  <Point X="28.330150390625" Y="1.066733886719" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.428455078125" Y="1.049878540039" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.816794921875" Y="1.090244384766" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.823896484375" Y="1.023336547852" />
                  <Point X="29.84594140625" Y="0.932784545898" />
                  <Point X="29.890865234375" Y="0.644238647461" />
                  <Point X="29.715310546875" Y="0.597198486328" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.704787109375" Y="0.325584533691" />
                  <Point X="28.681546875" Y="0.315067749023" />
                  <Point X="28.610478515625" Y="0.273989318848" />
                  <Point X="28.589037109375" Y="0.261595336914" />
                  <Point X="28.574314453125" Y="0.251097351074" />
                  <Point X="28.54753125" Y="0.225576538086" />
                  <Point X="28.504890625" Y="0.171242416382" />
                  <Point X="28.492025390625" Y="0.154848907471" />
                  <Point X="28.480302734375" Y="0.135569717407" />
                  <Point X="28.470529296875" Y="0.114107467651" />
                  <Point X="28.463681640625" Y="0.092604545593" />
                  <Point X="28.44946875" Y="0.018386648178" />
                  <Point X="28.4451796875" Y="-0.004006072044" />
                  <Point X="28.443484375" Y="-0.021874074936" />
                  <Point X="28.4451796875" Y="-0.05855406189" />
                  <Point X="28.459392578125" Y="-0.13277180481" />
                  <Point X="28.463681640625" Y="-0.155164520264" />
                  <Point X="28.470529296875" Y="-0.176667449951" />
                  <Point X="28.480302734375" Y="-0.198129699707" />
                  <Point X="28.492025390625" Y="-0.217408737183" />
                  <Point X="28.534666015625" Y="-0.271743011475" />
                  <Point X="28.54753125" Y="-0.288136535645" />
                  <Point X="28.560001953125" Y="-0.301237243652" />
                  <Point X="28.589037109375" Y="-0.324155456543" />
                  <Point X="28.66010546875" Y="-0.365233886719" />
                  <Point X="28.681546875" Y="-0.377627868652" />
                  <Point X="28.69270703125" Y="-0.383136657715" />
                  <Point X="28.716580078125" Y="-0.392149963379" />
                  <Point X="29.0179140625" Y="-0.472891815186" />
                  <Point X="29.891474609375" Y="-0.706961853027" />
                  <Point X="29.867330078125" Y="-0.867107177734" />
                  <Point X="29.855025390625" Y="-0.948725036621" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="29.58262890625" Y="-1.155926879883" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.2351796875" Y="-1.035825683594" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.1639765625" Y="-1.056596557617" />
                  <Point X="28.136150390625" Y="-1.07348840332" />
                  <Point X="28.1123984375" Y="-1.093959838867" />
                  <Point X="28.028091796875" Y="-1.195355224609" />
                  <Point X="28.002654296875" Y="-1.225947875977" />
                  <Point X="27.98793359375" Y="-1.250330932617" />
                  <Point X="27.97658984375" Y="-1.277718139648" />
                  <Point X="27.969759765625" Y="-1.305366455078" />
                  <Point X="27.95767578125" Y="-1.436677978516" />
                  <Point X="27.95403125" Y="-1.47629675293" />
                  <Point X="27.956349609375" Y="-1.507568603516" />
                  <Point X="27.96408203125" Y="-1.539189208984" />
                  <Point X="27.976453125" Y="-1.567997192383" />
                  <Point X="28.053642578125" Y="-1.688062133789" />
                  <Point X="28.07693359375" Y="-1.724287841797" />
                  <Point X="28.086935546875" Y="-1.737240234375" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.39026953125" Y="-1.975483520508" />
                  <Point X="29.213123046875" Y="-2.606882324219" />
                  <Point X="29.159498046875" Y="-2.693656005859" />
                  <Point X="29.124798828125" Y="-2.749801269531" />
                  <Point X="29.028982421875" Y="-2.885944824219" />
                  <Point X="28.832529296875" Y="-2.772522949219" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.58822265625" Y="-2.129844970703" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.30692578125" Y="-2.207757324219" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.24238671875" Y="-2.246547851562" />
                  <Point X="27.221427734375" Y="-2.267506347656" />
                  <Point X="27.204533203125" Y="-2.290437988281" />
                  <Point X="27.131953125" Y="-2.428346679688" />
                  <Point X="27.110052734375" Y="-2.469956054688" />
                  <Point X="27.100228515625" Y="-2.499735107422" />
                  <Point X="27.095271484375" Y="-2.531910400391" />
                  <Point X="27.09567578125" Y="-2.563259765625" />
                  <Point X="27.12565625" Y="-2.729263916016" />
                  <Point X="27.134703125" Y="-2.779350097656" />
                  <Point X="27.13898828125" Y="-2.795143066406" />
                  <Point X="27.1518203125" Y="-2.826078369141" />
                  <Point X="27.331515625" Y="-3.137321044922" />
                  <Point X="27.86128515625" Y="-4.054907226563" />
                  <Point X="27.823015625" Y="-4.082241699219" />
                  <Point X="27.781865234375" Y="-4.111634277344" />
                  <Point X="27.701765625" Y="-4.163481445312" />
                  <Point X="27.545734375" Y="-3.960135742188" />
                  <Point X="26.758548828125" Y="-2.934255371094" />
                  <Point X="26.74750390625" Y="-2.922177490234" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.55819921875" Y="-2.795297119141" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479990234375" Y="-2.751167236328" />
                  <Point X="26.448369140625" Y="-2.743435546875" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.2380390625" Y="-2.757593994141" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.156365234375" Y="-2.769396972656" />
                  <Point X="26.12898046875" Y="-2.780739746094" />
                  <Point X="26.10459765625" Y="-2.7954609375" />
                  <Point X="25.96633203125" Y="-2.910424804688" />
                  <Point X="25.92461328125" Y="-2.945111328125" />
                  <Point X="25.904142578125" Y="-2.968859863281" />
                  <Point X="25.88725" Y="-2.996685791016" />
                  <Point X="25.875625" Y="-3.025808105469" />
                  <Point X="25.83428515625" Y="-3.216009277344" />
                  <Point X="25.821810546875" Y="-3.273395996094" />
                  <Point X="25.819724609375" Y="-3.289626708984" />
                  <Point X="25.8197421875" Y="-3.323120605469" />
                  <Point X="25.87066796875" Y="-3.709930419922" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="26.014619140625" Y="-4.861547851562" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="24.003958984375" Y="-4.76474609375" />
                  <Point X="23.94157421875" Y="-4.75263671875" />
                  <Point X="23.85875390625" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575837402344" />
                  <Point X="23.879923828125" Y="-4.568097167969" />
                  <Point X="23.88055078125" Y="-4.541029785156" />
                  <Point X="23.8782421875" Y="-4.509324707031" />
                  <Point X="23.876666015625" Y="-4.497689453125" />
                  <Point X="23.830166015625" Y="-4.263927734375" />
                  <Point X="23.81613671875" Y="-4.193397949219" />
                  <Point X="23.811873046875" Y="-4.178467285156" />
                  <Point X="23.800970703125" Y="-4.149501464844" />
                  <Point X="23.79433203125" Y="-4.135466308594" />
                  <Point X="23.778259765625" Y="-4.107628417969" />
                  <Point X="23.76942578125" Y="-4.094863525391" />
                  <Point X="23.74979296875" Y="-4.070939208984" />
                  <Point X="23.738994140625" Y="-4.059779785156" />
                  <Point X="23.55980078125" Y="-3.902629638672" />
                  <Point X="23.505734375" Y="-3.855215087891" />
                  <Point X="23.49326171875" Y="-3.845965332031" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.125353515625" Y="-3.780581298828" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812255859" />
                  <Point X="22.706388671875" Y="-3.948228027344" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.61955859375" Y="-4.010133056641" />
                  <Point X="22.597244140625" Y="-4.032770996094" />
                  <Point X="22.589533203125" Y="-4.041627197266" />
                  <Point X="22.537404296875" Y="-4.109560546875" />
                  <Point X="22.496798828125" Y="-4.162478515625" />
                  <Point X="22.34386328125" Y="-4.067784423828" />
                  <Point X="22.252408203125" Y="-4.011157714844" />
                  <Point X="22.01913671875" Y="-3.831547119141" />
                  <Point X="22.086623046875" Y="-3.714657714844" />
                  <Point X="22.658509765625" Y="-2.724119873047" />
                  <Point X="22.6651484375" Y="-2.710084960938" />
                  <Point X="22.67605078125" Y="-2.681119628906" />
                  <Point X="22.680314453125" Y="-2.666188964844" />
                  <Point X="22.6865859375" Y="-2.634662109375" />
                  <Point X="22.688361328125" Y="-2.619238769531" />
                  <Point X="22.689375" Y="-2.588304931641" />
                  <Point X="22.6853359375" Y="-2.557616943359" />
                  <Point X="22.6763515625" Y="-2.527999023438" />
                  <Point X="22.67064453125" Y="-2.51355859375" />
                  <Point X="22.656427734375" Y="-2.484729492188" />
                  <Point X="22.648443359375" Y="-2.471409179688" />
                  <Point X="22.6304140625" Y="-2.446250732422" />
                  <Point X="22.620369140625" Y="-2.434412597656" />
                  <Point X="22.60302734375" Y="-2.417071044922" />
                  <Point X="22.591197265625" Y="-2.407031982422" />
                  <Point X="22.5660546875" Y="-2.389010986328" />
                  <Point X="22.5527421875" Y="-2.381029052734" />
                  <Point X="22.523912109375" Y="-2.366806152344" />
                  <Point X="22.50946875" Y="-2.361096679688" />
                  <Point X="22.47984765625" Y="-2.352107177734" />
                  <Point X="22.449158203125" Y="-2.348063720703" />
                  <Point X="22.41821875" Y="-2.34907421875" />
                  <Point X="22.402791015625" Y="-2.350848144531" />
                  <Point X="22.371259765625" Y="-2.357118652344" />
                  <Point X="22.356328125" Y="-2.361382324219" />
                  <Point X="22.327357421875" Y="-2.372285400391" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="22.014455078125" Y="-2.55147265625" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="21.06823046875" Y="-2.835509765625" />
                  <Point X="20.99598046875" Y="-2.740586669922" />
                  <Point X="20.818734375" Y="-2.443373291016" />
                  <Point X="20.951259765625" Y="-2.341682617188" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.963517578125" Y="-1.563310302734" />
                  <Point X="21.984892578125" Y="-1.540393066406" />
                  <Point X="21.994626953125" Y="-1.528047241211" />
                  <Point X="22.012591796875" Y="-1.50091796875" />
                  <Point X="22.020158203125" Y="-1.487137084961" />
                  <Point X="22.032916015625" Y="-1.458509155273" />
                  <Point X="22.038107421875" Y="-1.443662475586" />
                  <Point X="22.04402734375" Y="-1.420810058594" />
                  <Point X="22.04844921875" Y="-1.398801757812" />
                  <Point X="22.05125390625" Y="-1.368362182617" />
                  <Point X="22.051423828125" Y="-1.353029663086" />
                  <Point X="22.049212890625" Y="-1.321360473633" />
                  <Point X="22.0469140625" Y="-1.306202758789" />
                  <Point X="22.03991015625" Y="-1.276458007812" />
                  <Point X="22.02821484375" Y="-1.248229370117" />
                  <Point X="22.012130859375" Y="-1.222246704102" />
                  <Point X="22.003037109375" Y="-1.209905639648" />
                  <Point X="21.982203125" Y="-1.185951049805" />
                  <Point X="21.97124609375" Y="-1.175236206055" />
                  <Point X="21.947751953125" Y="-1.155708496094" />
                  <Point X="21.93521484375" Y="-1.146895385742" />
                  <Point X="21.91487109375" Y="-1.134921508789" />
                  <Point X="21.89456640625" Y="-1.124481201172" />
                  <Point X="21.86530078125" Y="-1.113256103516" />
                  <Point X="21.850201171875" Y="-1.108858886719" />
                  <Point X="21.81831640625" Y="-1.102378173828" />
                  <Point X="21.802701171875" Y="-1.100532104492" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="21.37838671875" Y="-1.14986706543" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.28749609375" Y="-1.084744628906" />
                  <Point X="20.2592421875" Y="-0.974116943359" />
                  <Point X="20.213548828125" Y="-0.654654296875" />
                  <Point X="20.351859375" Y="-0.617594055176" />
                  <Point X="21.491712890625" Y="-0.312171173096" />
                  <Point X="21.4995234375" Y="-0.309712677002" />
                  <Point X="21.52606640625" Y="-0.29925289917" />
                  <Point X="21.555830078125" Y="-0.283895965576" />
                  <Point X="21.5664375" Y="-0.277515441895" />
                  <Point X="21.5877578125" Y="-0.262717803955" />
                  <Point X="21.606326171875" Y="-0.24824520874" />
                  <Point X="21.62877734375" Y="-0.226365982056" />
                  <Point X="21.63909375" Y="-0.214494873047" />
                  <Point X="21.65833984375" Y="-0.188239898682" />
                  <Point X="21.666560546875" Y="-0.174821777344" />
                  <Point X="21.680673828125" Y="-0.146811569214" />
                  <Point X="21.68656640625" Y="-0.132219329834" />
                  <Point X="21.693671875" Y="-0.109321144104" />
                  <Point X="21.69901171875" Y="-0.088494613647" />
                  <Point X="21.703279296875" Y="-0.06033978653" />
                  <Point X="21.7043515625" Y="-0.0461027565" />
                  <Point X="21.7043515625" Y="-0.016457389832" />
                  <Point X="21.703279296875" Y="-0.002220356941" />
                  <Point X="21.69901171875" Y="0.025934467316" />
                  <Point X="21.69581640625" Y="0.03985226059" />
                  <Point X="21.6887109375" Y="0.062750442505" />
                  <Point X="21.680673828125" Y="0.084251716614" />
                  <Point X="21.666560546875" Y="0.112261932373" />
                  <Point X="21.65833984375" Y="0.125679603577" />
                  <Point X="21.63909375" Y="0.151934738159" />
                  <Point X="21.628775390625" Y="0.163808670044" />
                  <Point X="21.606322265625" Y="0.185688034058" />
                  <Point X="21.594189453125" Y="0.195693634033" />
                  <Point X="21.572869140625" Y="0.210491119385" />
                  <Point X="21.55365625" Y="0.222204681396" />
                  <Point X="21.531658203125" Y="0.233887786865" />
                  <Point X="21.521892578125" Y="0.238384963989" />
                  <Point X="21.501919921875" Y="0.246246673584" />
                  <Point X="21.491712890625" Y="0.249611190796" />
                  <Point X="21.14780078125" Y="0.341762298584" />
                  <Point X="20.214556640625" Y="0.591824829102" />
                  <Point X="20.2504609375" Y="0.834472045898" />
                  <Point X="20.26866796875" Y="0.957522583008" />
                  <Point X="20.366416015625" Y="1.318236816406" />
                  <Point X="20.404859375" Y="1.313175537109" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364257812" />
                  <Point X="21.263294921875" Y="1.204703125" />
                  <Point X="21.28485546875" Y="1.206589355469" />
                  <Point X="21.295109375" Y="1.208053100586" />
                  <Point X="21.3153984375" Y="1.212088745117" />
                  <Point X="21.32543359375" Y="1.214660522461" />
                  <Point X="21.37262109375" Y="1.2295390625" />
                  <Point X="21.3965546875" Y="1.237677612305" />
                  <Point X="21.415486328125" Y="1.246008544922" />
                  <Point X="21.42472265625" Y="1.250689819336" />
                  <Point X="21.443466796875" Y="1.261511962891" />
                  <Point X="21.452142578125" Y="1.267172119141" />
                  <Point X="21.46882421875" Y="1.279403808594" />
                  <Point X="21.48407421875" Y="1.29337878418" />
                  <Point X="21.497712890625" Y="1.308931152344" />
                  <Point X="21.504107421875" Y="1.317079711914" />
                  <Point X="21.516521484375" Y="1.334809570312" />
                  <Point X="21.521990234375" Y="1.343603393555" />
                  <Point X="21.531939453125" Y="1.36173828125" />
                  <Point X="21.536419921875" Y="1.371079345703" />
                  <Point X="21.555353515625" Y="1.416791625977" />
                  <Point X="21.5645" Y="1.440350585938" />
                  <Point X="21.570287109375" Y="1.46020300293" />
                  <Point X="21.572640625" Y="1.470288696289" />
                  <Point X="21.576400390625" Y="1.491605834961" />
                  <Point X="21.577640625" Y="1.501891601563" />
                  <Point X="21.578994140625" Y="1.522536743164" />
                  <Point X="21.578091796875" Y="1.543208374023" />
                  <Point X="21.574943359375" Y="1.563656860352" />
                  <Point X="21.572810546875" Y="1.57379309082" />
                  <Point X="21.56720703125" Y="1.594701049805" />
                  <Point X="21.563986328125" Y="1.6045390625" />
                  <Point X="21.5564921875" Y="1.623810668945" />
                  <Point X="21.55221875" Y="1.633244384766" />
                  <Point X="21.529373046875" Y="1.677132324219" />
                  <Point X="21.5171953125" Y="1.699297119141" />
                  <Point X="21.5056953125" Y="1.716504272461" />
                  <Point X="21.499478515625" Y="1.724788330078" />
                  <Point X="21.48556640625" Y="1.741364990234" />
                  <Point X="21.478490234375" Y="1.748920410156" />
                  <Point X="21.4635546875" Y="1.763217041016" />
                  <Point X="21.4556953125" Y="1.769958251953" />
                  <Point X="21.258427734375" Y="1.92132824707" />
                  <Point X="20.77238671875" Y="2.294281005859" />
                  <Point X="20.926955078125" Y="2.559094482422" />
                  <Point X="20.99771484375" Y="2.680321044922" />
                  <Point X="21.273662109375" Y="3.035012939453" />
                  <Point X="21.74584375" Y="2.762398681641" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774015625" Y="2.749385742188" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.9106484375" Y="2.725408203125" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.023568359375" Y="2.734227783203" />
                  <Point X="22.0430078125" Y="2.741303466797" />
                  <Point X="22.061560546875" Y="2.750453369141" />
                  <Point X="22.070587890625" Y="2.755534667969" />
                  <Point X="22.088841796875" Y="2.767164550781" />
                  <Point X="22.097259765625" Y="2.773197753906" />
                  <Point X="22.11338671875" Y="2.786141845703" />
                  <Point X="22.121095703125" Y="2.793052734375" />
                  <Point X="22.16774609375" Y="2.839701416016" />
                  <Point X="22.188732421875" Y="2.861485595703" />
                  <Point X="22.201681640625" Y="2.877618164062" />
                  <Point X="22.20771875" Y="2.886041259766" />
                  <Point X="22.21934765625" Y="2.904294921875" />
                  <Point X="22.2244296875" Y="2.913324951172" />
                  <Point X="22.233576171875" Y="2.931874267578" />
                  <Point X="22.240646484375" Y="2.951298339844" />
                  <Point X="22.245564453125" Y="2.971387451172" />
                  <Point X="22.2474765625" Y="2.981571777344" />
                  <Point X="22.250302734375" Y="3.003029541016" />
                  <Point X="22.251091796875" Y="3.01336328125" />
                  <Point X="22.25154296875" Y="3.034048583984" />
                  <Point X="22.251205078125" Y="3.044400146484" />
                  <Point X="22.245455078125" Y="3.110120605469" />
                  <Point X="22.242255859375" Y="3.140207763672" />
                  <Point X="22.23821875" Y="3.160498535156" />
                  <Point X="22.235646484375" Y="3.17053125" />
                  <Point X="22.22913671875" Y="3.191174560547" />
                  <Point X="22.22548828125" Y="3.200868652344" />
                  <Point X="22.217158203125" Y="3.219797851562" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="22.125060546875" Y="3.380441650391" />
                  <Point X="21.94061328125" Y="3.699914550781" />
                  <Point X="22.22840625" Y="3.9205625" />
                  <Point X="22.351634765625" Y="4.015041748047" />
                  <Point X="22.807474609375" Y="4.268297363281" />
                  <Point X="22.881435546875" Y="4.171909667969" />
                  <Point X="22.888177734375" Y="4.164050292969" />
                  <Point X="22.90248046875" Y="4.149108398438" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.934908203125" Y="4.121897460938" />
                  <Point X="22.952107421875" Y="4.110405273438" />
                  <Point X="22.96101953125" Y="4.105129394531" />
                  <Point X="23.034166015625" Y="4.067051025391" />
                  <Point X="23.056236328125" Y="4.055562255859" />
                  <Point X="23.065673828125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.0437890625" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031377929688" />
                  <Point X="23.2191796875" Y="4.035135742188" />
                  <Point X="23.22926953125" Y="4.037488037109" />
                  <Point X="23.249130859375" Y="4.043277099609" />
                  <Point X="23.25890234375" Y="4.046713867188" />
                  <Point X="23.335087890625" Y="4.078271972656" />
                  <Point X="23.358076171875" Y="4.087793457031" />
                  <Point X="23.3674140625" Y="4.092272216797" />
                  <Point X="23.385544921875" Y="4.10221875" />
                  <Point X="23.394337890625" Y="4.107687011719" />
                  <Point X="23.412068359375" Y="4.120101074219" />
                  <Point X="23.420216796875" Y="4.126494628906" />
                  <Point X="23.43576953125" Y="4.1401328125" />
                  <Point X="23.449744140625" Y="4.155382324219" />
                  <Point X="23.4619765625" Y="4.172063476562" />
                  <Point X="23.467638671875" Y="4.180739746094" />
                  <Point X="23.4784609375" Y="4.199483398437" />
                  <Point X="23.48314453125" Y="4.208723632812" />
                  <Point X="23.4914765625" Y="4.227659179688" />
                  <Point X="23.495125" Y="4.237354492188" />
                  <Point X="23.519921875" Y="4.316001953125" />
                  <Point X="23.527404296875" Y="4.339730957031" />
                  <Point X="23.529974609375" Y="4.349763183594" />
                  <Point X="23.534009765625" Y="4.370048828125" />
                  <Point X="23.535474609375" Y="4.380302246094" />
                  <Point X="23.537361328125" Y="4.401864746094" />
                  <Point X="23.53769921875" Y="4.412217285156" />
                  <Point X="23.537248046875" Y="4.432899414062" />
                  <Point X="23.536458984375" Y="4.443229003906" />
                  <Point X="23.52651953125" Y="4.518717285156" />
                  <Point X="23.520734375" Y="4.562655273438" />
                  <Point X="23.90930859375" Y="4.671598144531" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.77433203125" Y="4.261728515625" />
                  <Point X="24.779564453125" Y="4.247106933594" />
                  <Point X="24.79233984375" Y="4.218912597656" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.282771484375" Y="4.428889648438" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.688583984375" Y="4.7525" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.314943359375" Y="4.620319824219" />
                  <Point X="26.453595703125" Y="4.586843261719" />
                  <Point X="26.769314453125" Y="4.472331054688" />
                  <Point X="26.85826171875" Y="4.440069335938" />
                  <Point X="27.16481640625" Y="4.296703613281" />
                  <Point X="27.25044921875" Y="4.256655273438" />
                  <Point X="27.54665234375" Y="4.084088134766" />
                  <Point X="27.6294296875" Y="4.035861572266" />
                  <Point X="27.817783203125" Y="3.901915039062" />
                  <Point X="27.727228515625" Y="3.745071289062" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.06237890625" Y="2.593110595703" />
                  <Point X="27.0531875" Y="2.573448974609" />
                  <Point X="27.044185546875" Y="2.549571777344" />
                  <Point X="27.041302734375" Y="2.540601318359" />
                  <Point X="27.02480859375" Y="2.478924072266" />
                  <Point X="27.02099609375" Y="2.461379638672" />
                  <Point X="27.014681640625" Y="2.423870117188" />
                  <Point X="27.013388671875" Y="2.4103203125" />
                  <Point X="27.01275390625" Y="2.383175537109" />
                  <Point X="27.013412109375" Y="2.369580566406" />
                  <Point X="27.01984375" Y="2.316247314453" />
                  <Point X="27.023798828125" Y="2.289041259766" />
                  <Point X="27.029138671875" Y="2.267125" />
                  <Point X="27.032462890625" Y="2.256323242188" />
                  <Point X="27.040728515625" Y="2.234231445312" />
                  <Point X="27.0453125" Y="2.223898193359" />
                  <Point X="27.055677734375" Y="2.203846191406" />
                  <Point X="27.061458984375" Y="2.194127441406" />
                  <Point X="27.0944609375" Y="2.145492675781" />
                  <Point X="27.1055859375" Y="2.130706542969" />
                  <Point X="27.12884375" Y="2.102733154297" />
                  <Point X="27.13806640625" Y="2.093104003906" />
                  <Point X="27.1577734375" Y="2.075228027344" />
                  <Point X="27.1682578125" Y="2.066981201172" />
                  <Point X="27.216892578125" Y="2.03398046875" />
                  <Point X="27.24128125" Y="2.018244384766" />
                  <Point X="27.261326171875" Y="2.007882080078" />
                  <Point X="27.27165625" Y="2.003299194336" />
                  <Point X="27.293744140625" Y="1.995033081055" />
                  <Point X="27.304546875" Y="1.991708007812" />
                  <Point X="27.326470703125" Y="1.986365112305" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.390923828125" Y="1.977915649414" />
                  <Point X="27.410396484375" Y="1.976577148438" />
                  <Point X="27.445390625" Y="1.975974487305" />
                  <Point X="27.458591796875" Y="1.976666992188" />
                  <Point X="27.48476953125" Y="1.979877685547" />
                  <Point X="27.49774609375" Y="1.982395996094" />
                  <Point X="27.559423828125" Y="1.998889160156" />
                  <Point X="27.569080078125" Y="2.001747436523" />
                  <Point X="27.5997734375" Y="2.011725219727" />
                  <Point X="27.609048828125" Y="2.015286987305" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="27.9819453125" Y="2.227583496094" />
                  <Point X="28.940404296875" Y="2.780950683594" />
                  <Point X="28.994845703125" Y="2.705289550781" />
                  <Point X="29.043966796875" Y="2.637024169922" />
                  <Point X="29.13688671875" Y="2.483470947266" />
                  <Point X="29.0444296875" Y="2.412526123047" />
                  <Point X="28.172953125" Y="1.743819824219" />
                  <Point X="28.16813671875" Y="1.7398671875" />
                  <Point X="28.152126953125" Y="1.725222412109" />
                  <Point X="28.134671875" Y="1.706606323242" />
                  <Point X="28.12857421875" Y="1.699420898438" />
                  <Point X="28.084185546875" Y="1.64151159668" />
                  <Point X="28.07079296875" Y="1.624039550781" />
                  <Point X="28.065634765625" Y="1.616599365234" />
                  <Point X="28.049744140625" Y="1.589380371094" />
                  <Point X="28.034765625" Y="1.555554199219" />
                  <Point X="28.030140625" Y="1.54267590332" />
                  <Point X="28.01360546875" Y="1.483550292969" />
                  <Point X="28.006224609375" Y="1.454663452148" />
                  <Point X="28.002771484375" Y="1.432365356445" />
                  <Point X="28.001708984375" Y="1.421114868164" />
                  <Point X="28.000892578125" Y="1.397540649414" />
                  <Point X="28.001173828125" Y="1.386235839844" />
                  <Point X="28.003078125" Y="1.363749633789" />
                  <Point X="28.004701171875" Y="1.352567993164" />
                  <Point X="28.018275390625" Y="1.286783569336" />
                  <Point X="28.02237109375" Y="1.266935302734" />
                  <Point X="28.02459765625" Y="1.258239501953" />
                  <Point X="28.03468359375" Y="1.228609985352" />
                  <Point X="28.05028515625" Y="1.195373657227" />
                  <Point X="28.056916015625" Y="1.183527832031" />
                  <Point X="28.093833984375" Y="1.127412841797" />
                  <Point X="28.10497265625" Y="1.110482299805" />
                  <Point X="28.11173828125" Y="1.101422851562" />
                  <Point X="28.126294921875" Y="1.084175537109" />
                  <Point X="28.1340859375" Y="1.075987792969" />
                  <Point X="28.151330078125" Y="1.059898681641" />
                  <Point X="28.160037109375" Y="1.052694091797" />
                  <Point X="28.17824609375" Y="1.039369384766" />
                  <Point X="28.187748046875" Y="1.033249389648" />
                  <Point X="28.24125" Y="1.003133483887" />
                  <Point X="28.2576171875" Y="0.994933044434" />
                  <Point X="28.291734375" Y="0.979847961426" />
                  <Point X="28.30445703125" Y="0.975274230957" />
                  <Point X="28.33042578125" Y="0.967979064941" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.4160078125" Y="0.955697509766" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="28.8291953125" Y="0.996057067871" />
                  <Point X="29.704703125" Y="1.111319702148" />
                  <Point X="29.731591796875" Y="1.000866210938" />
                  <Point X="29.752689453125" Y="0.914203369141" />
                  <Point X="29.78387109375" Y="0.713920776367" />
                  <Point X="29.69072265625" Y="0.688961303711" />
                  <Point X="28.6919921875" Y="0.42135269165" />
                  <Point X="28.686029296875" Y="0.419543334961" />
                  <Point X="28.66562109375" Y="0.412135101318" />
                  <Point X="28.642380859375" Y="0.401618377686" />
                  <Point X="28.634005859375" Y="0.397316497803" />
                  <Point X="28.5629375" Y="0.35623815918" />
                  <Point X="28.54149609375" Y="0.343844207764" />
                  <Point X="28.5338828125" Y="0.338945159912" />
                  <Point X="28.508779296875" Y="0.319873718262" />
                  <Point X="28.48199609375" Y="0.294352935791" />
                  <Point X="28.472796875" Y="0.284226654053" />
                  <Point X="28.43015625" Y="0.229892501831" />
                  <Point X="28.417291015625" Y="0.213499099731" />
                  <Point X="28.410853515625" Y="0.204205413818" />
                  <Point X="28.399130859375" Y="0.184926193237" />
                  <Point X="28.393845703125" Y="0.174940673828" />
                  <Point X="28.384072265625" Y="0.15347833252" />
                  <Point X="28.3800078125" Y="0.142933990479" />
                  <Point X="28.37316015625" Y="0.12143107605" />
                  <Point X="28.370376953125" Y="0.110472511292" />
                  <Point X="28.3561640625" Y="0.036254692078" />
                  <Point X="28.351875" Y="0.013861978531" />
                  <Point X="28.350603515625" Y="0.004967195034" />
                  <Point X="28.3485859375" Y="-0.026260259628" />
                  <Point X="28.35028125" Y="-0.062940242767" />
                  <Point X="28.351875" Y="-0.07642212677" />
                  <Point X="28.366087890625" Y="-0.150639801025" />
                  <Point X="28.370376953125" Y="-0.173032501221" />
                  <Point X="28.37316015625" Y="-0.183991073608" />
                  <Point X="28.3800078125" Y="-0.205493972778" />
                  <Point X="28.384072265625" Y="-0.216038330078" />
                  <Point X="28.393845703125" Y="-0.237500671387" />
                  <Point X="28.399130859375" Y="-0.247486495972" />
                  <Point X="28.410853515625" Y="-0.266765563965" />
                  <Point X="28.417291015625" Y="-0.276058807373" />
                  <Point X="28.459931640625" Y="-0.330393096924" />
                  <Point X="28.472796875" Y="-0.346786499023" />
                  <Point X="28.47872265625" Y="-0.35363684082" />
                  <Point X="28.501142578125" Y="-0.37580645752" />
                  <Point X="28.530177734375" Y="-0.398724700928" />
                  <Point X="28.54149609375" Y="-0.406404205322" />
                  <Point X="28.612564453125" Y="-0.447482666016" />
                  <Point X="28.634005859375" Y="-0.459876647949" />
                  <Point X="28.639498046875" Y="-0.462815063477" />
                  <Point X="28.65915234375" Y="-0.472013183594" />
                  <Point X="28.683025390625" Y="-0.481026428223" />
                  <Point X="28.6919921875" Y="-0.483912963867" />
                  <Point X="28.993326171875" Y="-0.564654724121" />
                  <Point X="29.784880859375" Y="-0.776751159668" />
                  <Point X="29.773392578125" Y="-0.852944519043" />
                  <Point X="29.761619140625" Y="-0.93103729248" />
                  <Point X="29.7278046875" Y="-1.079219726563" />
                  <Point X="29.595029296875" Y="-1.061739624023" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042480469" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.215001953125" Y="-0.942993225098" />
                  <Point X="28.17291796875" Y="-0.952140075684" />
                  <Point X="28.157875" Y="-0.956742614746" />
                  <Point X="28.128755859375" Y="-0.968366577148" />
                  <Point X="28.1146796875" Y="-0.975388427734" />
                  <Point X="28.086853515625" Y="-0.992280273438" />
                  <Point X="28.07412890625" Y="-1.001527893066" />
                  <Point X="28.050376953125" Y="-1.021999389648" />
                  <Point X="28.039349609375" Y="-1.033222900391" />
                  <Point X="27.95504296875" Y="-1.134618164062" />
                  <Point X="27.92960546875" Y="-1.1652109375" />
                  <Point X="27.921326171875" Y="-1.176848022461" />
                  <Point X="27.90660546875" Y="-1.201231201172" />
                  <Point X="27.9001640625" Y="-1.213977050781" />
                  <Point X="27.8888203125" Y="-1.241364257812" />
                  <Point X="27.88436328125" Y="-1.254934814453" />
                  <Point X="27.877533203125" Y="-1.282583129883" />
                  <Point X="27.87516015625" Y="-1.296660888672" />
                  <Point X="27.863076171875" Y="-1.42797253418" />
                  <Point X="27.859431640625" Y="-1.467591186523" />
                  <Point X="27.859291015625" Y="-1.48332019043" />
                  <Point X="27.861609375" Y="-1.514592163086" />
                  <Point X="27.864068359375" Y="-1.530134765625" />
                  <Point X="27.87180078125" Y="-1.561755371094" />
                  <Point X="27.876791015625" Y="-1.576675048828" />
                  <Point X="27.889162109375" Y="-1.605483032227" />
                  <Point X="27.89654296875" Y="-1.619371459961" />
                  <Point X="27.973732421875" Y="-1.739436401367" />
                  <Point X="27.9970234375" Y="-1.775662109375" />
                  <Point X="28.0017421875" Y="-1.782350952148" />
                  <Point X="28.019796875" Y="-1.804452758789" />
                  <Point X="28.0434921875" Y="-1.828121704102" />
                  <Point X="28.052798828125" Y="-1.836277587891" />
                  <Point X="28.3324375" Y="-2.050852050781" />
                  <Point X="29.087171875" Y="-2.62998046875" />
                  <Point X="29.07868359375" Y="-2.643714355469" />
                  <Point X="29.045462890625" Y="-2.697467041016" />
                  <Point X="29.001275390625" Y="-2.760251953125" />
                  <Point X="28.880029296875" Y="-2.690250488281" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.605107421875" Y="-2.036357299805" />
                  <Point X="27.555021484375" Y="-2.027311889648" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.03513659668" />
                  <Point X="27.415068359375" Y="-2.044960571289" />
                  <Point X="27.40058984375" Y="-2.051109130859" />
                  <Point X="27.262681640625" Y="-2.123689453125" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.208970703125" Y="-2.153169677734" />
                  <Point X="27.186041015625" Y="-2.170061523438" />
                  <Point X="27.175212890625" Y="-2.179371826172" />
                  <Point X="27.15425390625" Y="-2.200330322266" />
                  <Point X="27.144943359375" Y="-2.211157714844" />
                  <Point X="27.128048828125" Y="-2.234089355469" />
                  <Point X="27.12046484375" Y="-2.246193603516" />
                  <Point X="27.047884765625" Y="-2.384102294922" />
                  <Point X="27.025984375" Y="-2.425711669922" />
                  <Point X="27.0198359375" Y="-2.440193115234" />
                  <Point X="27.01001171875" Y="-2.469972167969" />
                  <Point X="27.0063359375" Y="-2.485269775391" />
                  <Point X="27.00137890625" Y="-2.517445068359" />
                  <Point X="27.000279296875" Y="-2.533135498047" />
                  <Point X="27.00068359375" Y="-2.564484863281" />
                  <Point X="27.0021875" Y="-2.580143798828" />
                  <Point X="27.03216796875" Y="-2.746147949219" />
                  <Point X="27.04121484375" Y="-2.796234130859" />
                  <Point X="27.043017578125" Y="-2.804227294922" />
                  <Point X="27.05123828125" Y="-2.831541992188" />
                  <Point X="27.0640703125" Y="-2.862477294922" />
                  <Point X="27.069546875" Y="-2.873578125" />
                  <Point X="27.2492421875" Y="-3.184820800781" />
                  <Point X="27.73589453125" Y="-4.027724853516" />
                  <Point X="27.723755859375" Y="-4.036083496094" />
                  <Point X="27.621103515625" Y="-3.902303710938" />
                  <Point X="26.83391796875" Y="-2.876423339844" />
                  <Point X="26.828654296875" Y="-2.870145019531" />
                  <Point X="26.808830078125" Y="-2.849624023438" />
                  <Point X="26.783251953125" Y="-2.828003662109" />
                  <Point X="26.77330078125" Y="-2.820646728516" />
                  <Point X="26.60957421875" Y="-2.71538671875" />
                  <Point X="26.56017578125" Y="-2.683627929688" />
                  <Point X="26.546283203125" Y="-2.676245605469" />
                  <Point X="26.51747265625" Y="-2.663874511719" />
                  <Point X="26.5025546875" Y="-2.658885742188" />
                  <Point X="26.47093359375" Y="-2.651154052734" />
                  <Point X="26.45539453125" Y="-2.648695800781" />
                  <Point X="26.424125" Y="-2.646376953125" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.229333984375" Y="-2.662993652344" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670339111328" />
                  <Point X="26.133578125" Y="-2.677170410156" />
                  <Point X="26.12001171875" Y="-2.681627929688" />
                  <Point X="26.092626953125" Y="-2.692970703125" />
                  <Point X="26.07987890625" Y="-2.699412841797" />
                  <Point X="26.05549609375" Y="-2.714134033203" />
                  <Point X="26.043861328125" Y="-2.722413085938" />
                  <Point X="25.905595703125" Y="-2.837376953125" />
                  <Point X="25.863876953125" Y="-2.872063476562" />
                  <Point X="25.85265625" Y="-2.8830859375" />
                  <Point X="25.832185546875" Y="-2.906834472656" />
                  <Point X="25.822935546875" Y="-2.919560546875" />
                  <Point X="25.80604296875" Y="-2.947386474609" />
                  <Point X="25.79901953125" Y="-2.961466064453" />
                  <Point X="25.78739453125" Y="-2.990588378906" />
                  <Point X="25.78279296875" Y="-3.005631103516" />
                  <Point X="25.741453125" Y="-3.195832275391" />
                  <Point X="25.728978515625" Y="-3.253218994141" />
                  <Point X="25.7275859375" Y="-3.261286376953" />
                  <Point X="25.724724609375" Y="-3.289676513672" />
                  <Point X="25.7247421875" Y="-3.323170410156" />
                  <Point X="25.7255546875" Y="-3.335520996094" />
                  <Point X="25.77648046875" Y="-3.722330810547" />
                  <Point X="25.83308984375" Y="-4.152321289062" />
                  <Point X="25.655068359375" Y="-3.487937255859" />
                  <Point X="25.652607421875" Y="-3.480122314453" />
                  <Point X="25.642146484375" Y="-3.453578613281" />
                  <Point X="25.6267890625" Y="-3.423815429688" />
                  <Point X="25.62041015625" Y="-3.413209716797" />
                  <Point X="25.494630859375" Y="-3.231987060547" />
                  <Point X="25.456681640625" Y="-3.177309326172" />
                  <Point X="25.446671875" Y="-3.165172119141" />
                  <Point X="25.424787109375" Y="-3.142716308594" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104937255859" />
                  <Point X="25.3452421875" Y="-3.090829589844" />
                  <Point X="25.33065625" Y="-3.084938476562" />
                  <Point X="25.136021484375" Y="-3.024531005859" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.063376953125" Y="-3.003109130859" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.740380859375" Y="-3.066712402344" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.667068359375" Y="-3.090830078125" />
                  <Point X="24.639068359375" Y="-3.104938232422" />
                  <Point X="24.62565625" Y="-3.113154785156" />
                  <Point X="24.599400390625" Y="-3.132398925781" />
                  <Point X="24.587525390625" Y="-3.142717041016" />
                  <Point X="24.565640625" Y="-3.165172851562" />
                  <Point X="24.555630859375" Y="-3.177310546875" />
                  <Point X="24.429853515625" Y="-3.358532958984" />
                  <Point X="24.391904296875" Y="-3.4132109375" />
                  <Point X="24.38753125" Y="-3.420131591797" />
                  <Point X="24.374025390625" Y="-3.445261230469" />
                  <Point X="24.36122265625" Y="-3.476214111328" />
                  <Point X="24.35724609375" Y="-3.487937011719" />
                  <Point X="24.2643984375" Y="-3.834450927734" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.865579935057" Y="-4.679484136649" />
                  <Point X="24.04042566412" Y="-4.670320860272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.878192285904" Y="-4.58369277848" />
                  <Point X="24.066279062615" Y="-4.5738355682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.874884937736" Y="-4.488735736383" />
                  <Point X="24.092132461109" Y="-4.477350276128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.856156796117" Y="-4.394586863826" />
                  <Point X="24.117985859604" Y="-4.380864984056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.837428654498" Y="-4.300437991268" />
                  <Point X="24.143839258099" Y="-4.284379691985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.818700936576" Y="-4.206289096506" />
                  <Point X="24.169692656594" Y="-4.187894399913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.819454621734" Y="-4.101434038974" />
                  <Point X="25.826342877365" Y="-4.101073040793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.698343751353" Y="-4.002965632172" />
                  <Point X="27.720916737727" Y="-4.001782632084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.781425854995" Y="-4.113112227884" />
                  <Point X="24.195546055089" Y="-4.091409107841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.794317461714" Y="-4.007621048838" />
                  <Point X="25.813904533972" Y="-4.006594533879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.628169950296" Y="-3.911512912379" />
                  <Point X="27.667606085987" Y="-3.909446152084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.381888135685" Y="-4.091328518906" />
                  <Point X="22.558497093429" Y="-4.082072835629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.696413524824" Y="-4.02243716245" />
                  <Point X="24.221399453584" Y="-3.994923815769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.769180301693" Y="-3.913808058703" />
                  <Point X="25.80146619058" Y="-3.912116026964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.557995867622" Y="-3.820060207346" />
                  <Point X="27.614295434247" Y="-3.817109672084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.242468884443" Y="-4.003504799383" />
                  <Point X="22.656105572693" Y="-3.981827019122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.594056128149" Y="-3.932671113433" />
                  <Point X="24.247252852078" Y="-3.898438523697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.744043141673" Y="-3.819995068567" />
                  <Point X="25.789027847187" Y="-3.817637520049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.487821753415" Y="-3.728607503965" />
                  <Point X="27.560984782507" Y="-3.724773192084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.126790793574" Y="-3.914436858368" />
                  <Point X="22.810593787714" Y="-3.878600261978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.488601455786" Y="-3.843067385757" />
                  <Point X="24.273106123276" Y="-3.801953238297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.718905981653" Y="-3.726182078432" />
                  <Point X="25.776589503794" Y="-3.723159013135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.417647639208" Y="-3.637154800583" />
                  <Point X="27.507674130767" Y="-3.632436712084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.023065376574" Y="-3.824742504258" />
                  <Point X="24.298959143829" Y="-3.705467966032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.693768821632" Y="-3.632369088296" />
                  <Point X="25.764150858877" Y="-3.628680522022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.347473525" Y="-3.545702097202" />
                  <Point X="27.454363479026" Y="-3.540100232084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.079702805281" Y="-3.726643889525" />
                  <Point X="24.324812164381" Y="-3.608982693767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.668631661612" Y="-3.53855609816" />
                  <Point X="25.751712211292" Y="-3.53420203105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.277299410793" Y="-3.454249393821" />
                  <Point X="27.401052827286" Y="-3.447763752084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.136340031567" Y="-3.628545285401" />
                  <Point X="24.350665184933" Y="-3.512497421503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.637743007162" Y="-3.445044531075" />
                  <Point X="25.739273563708" Y="-3.439723540077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.207125296586" Y="-3.36279669044" />
                  <Point X="27.347742175546" Y="-3.355427272084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.192977229678" Y="-3.530446682754" />
                  <Point X="24.390600604769" Y="-3.415274121964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.578629478025" Y="-3.353012166993" />
                  <Point X="25.726834916124" Y="-3.345245049105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.136951182378" Y="-3.271343987059" />
                  <Point X="27.294431523806" Y="-3.263090792084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.249614427789" Y="-3.432348080106" />
                  <Point X="24.458984915081" Y="-3.316559879253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.514920694464" Y="-3.26122062999" />
                  <Point X="25.729685804166" Y="-3.249965267524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.066777068171" Y="-3.179891283678" />
                  <Point X="27.241120927777" Y="-3.170754309165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.306251625899" Y="-3.334249477458" />
                  <Point X="24.527502338146" Y="-3.217838660399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.450225317485" Y="-3.169480798158" />
                  <Point X="25.750602075097" Y="-3.153738719343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.996602953963" Y="-3.088438580297" />
                  <Point X="27.187810641741" Y="-3.078417809999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.36288882401" Y="-3.236150874811" />
                  <Point X="24.619176863018" Y="-3.117903829263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.31874321919" Y="-3.081241110075" />
                  <Point X="25.771516703905" Y="-3.057512257223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.926428839756" Y="-2.996985876916" />
                  <Point X="27.134500355704" Y="-2.986081310833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.419526022121" Y="-3.138052272163" />
                  <Point X="24.935490400144" Y="-3.006196166356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.045656627782" Y="-3.000422599013" />
                  <Point X="25.799288729436" Y="-2.960926414169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.856254725549" Y="-2.905533173535" />
                  <Point X="27.081190069667" Y="-2.893744811668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.476163220232" Y="-3.039953669515" />
                  <Point X="25.876267190979" Y="-2.861761771077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.764820385199" Y="-2.815194671393" />
                  <Point X="27.042212392256" Y="-2.800657172313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.902404209541" Y="-2.703168650128" />
                  <Point X="29.046609883962" Y="-2.695611150972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.202252223698" Y="-3.011586142978" />
                  <Point X="21.219039755107" Y="-3.010706345737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.532800418343" Y="-2.941855066868" />
                  <Point X="25.998377121469" Y="-2.760231887922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.628002983912" Y="-2.727234594692" />
                  <Point X="27.024994723151" Y="-2.706429139245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.751345639487" Y="-2.615954921457" />
                  <Point X="29.048591791277" Y="-2.600376910741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.132620719676" Y="-2.920105002602" />
                  <Point X="21.400260126417" Y="-2.906078615646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.589437616454" Y="-2.84375646422" />
                  <Point X="27.007975197321" Y="-2.612190721928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.600287075676" Y="-2.528741192458" />
                  <Point X="28.932541672615" Y="-2.511328466876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.062989311376" Y="-2.82862385721" />
                  <Point X="21.581480497727" Y="-2.801450885555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.646074814564" Y="-2.745657861573" />
                  <Point X="27.001384967095" Y="-2.51740572839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.449228511866" Y="-2.441527463459" />
                  <Point X="28.816491553954" Y="-2.42228002301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.993909394533" Y="-2.737113809375" />
                  <Point X="21.762700869037" Y="-2.696823155464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.683823452631" Y="-2.648549166411" />
                  <Point X="27.028542122517" Y="-2.420852109313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.298169948055" Y="-2.35431373446" />
                  <Point X="28.700441435293" Y="-2.333231579145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.938896826707" Y="-2.644866523018" />
                  <Point X="21.943921240347" Y="-2.592195425373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.684058719915" Y="-2.553406463705" />
                  <Point X="27.080030043648" Y="-2.323023368836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.147111384245" Y="-2.267100005461" />
                  <Point X="28.584391316632" Y="-2.24418313528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.883884258881" Y="-2.55261923666" />
                  <Point X="22.125142394173" Y="-2.487567654273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.640661657334" Y="-2.460550434513" />
                  <Point X="27.134725734615" Y="-2.225026516267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.996052820434" Y="-2.179886276463" />
                  <Point X="28.468341197971" Y="-2.155134691415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.828871691055" Y="-2.460371950303" />
                  <Point X="22.306364046649" Y="-2.382939857038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.532583758124" Y="-2.37108418433" />
                  <Point X="27.263735045661" Y="-2.123135051898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.844655794745" Y="-2.092690285499" />
                  <Point X="28.352291079309" Y="-2.066086247549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.927278304801" Y="-2.36008430534" />
                  <Point X="28.2362409726" Y="-1.977037803058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.060342707357" Y="-2.25798032263" />
                  <Point X="28.120190868357" Y="-1.887989358437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.1934071994" Y="-2.155876335231" />
                  <Point X="28.01483642365" Y="-1.798380378052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.326471691443" Y="-2.053772347832" />
                  <Point X="27.952566333151" Y="-1.706513442341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.459536183485" Y="-1.951668360433" />
                  <Point X="27.893930716045" Y="-1.61445603195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.592600675528" Y="-1.849564373034" />
                  <Point X="27.862617885277" Y="-1.520966695004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.725665167571" Y="-1.747460385634" />
                  <Point X="27.863275930927" Y="-1.425801835423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.858729659614" Y="-1.645356398235" />
                  <Point X="27.872072751005" Y="-1.330210440748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.981733901293" Y="-1.543779646217" />
                  <Point X="27.891852582421" Y="-1.23404345084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.037383495438" Y="-1.4457328017" />
                  <Point X="27.954188162835" Y="-1.13564620863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.529364647323" Y="-1.053094707099" />
                  <Point X="29.736240343398" Y="-1.04225281128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.051203808997" Y="-1.349878136887" />
                  <Point X="28.036889519024" Y="-1.036181641339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.012521142257" Y="-0.985050954567" />
                  <Point X="29.758211350034" Y="-0.945970986744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.031346614841" Y="-1.255788435466" />
                  <Point X="28.283365132682" Y="-0.92813402891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.49567763719" Y="-0.917007202034" />
                  <Point X="29.773833243968" Y="-0.850021905105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.329667790873" Y="-1.249839270813" />
                  <Point X="20.810385973333" Y="-1.22464589841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.958311905008" Y="-1.164485649549" />
                  <Point X="29.714838342013" Y="-0.757983324035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.305688741464" Y="-1.155965586672" />
                  <Point X="29.417887182307" Y="-0.678415502001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.281710657369" Y="-1.062091851941" />
                  <Point X="29.120936022601" Y="-0.598847679967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.25839351261" Y="-0.968183478848" />
                  <Point X="28.823984097899" Y="-0.519279898025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.244888068253" Y="-0.873760896325" />
                  <Point X="28.593128069803" Y="-0.436248176922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.231382623896" Y="-0.779338313802" />
                  <Point X="28.473324497704" Y="-0.347396443216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.217877179538" Y="-0.684915731279" />
                  <Point X="28.404238616533" Y="-0.255886707958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.513454300357" Y="-0.5742948179" />
                  <Point X="28.368385492977" Y="-0.162635317674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.954809634018" Y="-0.456033992119" />
                  <Point X="28.350929011449" Y="-0.068419800235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.396164967678" Y="-0.337773166337" />
                  <Point X="28.354370544439" Y="0.026890935736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.624346113566" Y="-0.230684326337" />
                  <Point X="28.373670216967" Y="0.123032761584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.686535961847" Y="-0.132294721625" />
                  <Point X="28.422979277242" Y="0.220747312801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.7043515625" Y="-0.036230672688" />
                  <Point X="28.509486992231" Y="0.320411362904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.690137014422" Y="0.058154747283" />
                  <Point X="28.709407603028" Y="0.426019131019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.640028871874" Y="0.150659063678" />
                  <Point X="29.150763873668" Y="0.544280005906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.519172564005" Y="0.23945562584" />
                  <Point X="29.592120144309" Y="0.662540880792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.230955011719" Y="0.319481156844" />
                  <Point X="29.775562900063" Y="0.767285081117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.934004328384" Y="0.399049003844" />
                  <Point X="29.760872087881" Y="0.861645541144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.637053686254" Y="0.478616853003" />
                  <Point X="29.742559015702" Y="0.955816166569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.340103044123" Y="0.558184702162" />
                  <Point X="28.303717591852" Y="0.975540055675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.917808460913" Y="1.0077231944" />
                  <Point X="29.719692037807" Y="1.049748131909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.22274522001" Y="0.647164612089" />
                  <Point X="28.148503429106" Y="1.062535998962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.236931589764" Y="0.743038461093" />
                  <Point X="28.076415743223" Y="1.153888416301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.251117935904" Y="0.83891230886" />
                  <Point X="28.02858960623" Y="1.246512327539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.26530379579" Y="0.934786131144" />
                  <Point X="28.00718698861" Y="1.340521036748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.288616698459" Y="1.031138281471" />
                  <Point X="28.003248403315" Y="1.435444997109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.314766954558" Y="1.127639131191" />
                  <Point X="28.027106242759" Y="1.531825706363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.340917210656" Y="1.224139980911" />
                  <Point X="20.870384357498" Y="1.25188817828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.473277164141" Y="1.283484451422" />
                  <Point X="28.074950497596" Y="1.62946349038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.541011338354" Y="1.382164621944" />
                  <Point X="28.156088416139" Y="1.728846121377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.574182961879" Y="1.479033445938" />
                  <Point X="28.286309523003" Y="1.830801093273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.572731574352" Y="1.57408775481" />
                  <Point X="28.419374284657" Y="1.932905094802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.53453478647" Y="1.667216318851" />
                  <Point X="27.430789644834" Y="1.976225942065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.473056062079" Y="1.978441031131" />
                  <Point X="28.552439046311" Y="2.035009096331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.468101165546" Y="1.758865053179" />
                  <Point X="27.181095351395" Y="2.058270391516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.739361394165" Y="2.087527875067" />
                  <Point X="28.685503807964" Y="2.13711309786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.35397172431" Y="1.848014155483" />
                  <Point X="27.092254156543" Y="2.148744794655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.920582339404" Y="2.192155635236" />
                  <Point X="28.818568569618" Y="2.239217099388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.237922300182" Y="1.937062635747" />
                  <Point X="27.038180617368" Y="2.241041293419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.101802879721" Y="2.296783374184" />
                  <Point X="28.951633331272" Y="2.341321100917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.121872221218" Y="2.026111081692" />
                  <Point X="27.017571255639" Y="2.335091575408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.283023212733" Y="2.401411102268" />
                  <Point X="29.084697986411" Y="2.443425096864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.005822142254" Y="2.115159527638" />
                  <Point X="27.015734722098" Y="2.430125699633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.464243545745" Y="2.506038830352" />
                  <Point X="29.102973738273" Y="2.539513261304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.88977206329" Y="2.204207973584" />
                  <Point X="27.037504109498" Y="2.526396957753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.645463878758" Y="2.610666558436" />
                  <Point X="29.047176855007" Y="2.631719443431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.773721984326" Y="2.29325641953" />
                  <Point X="27.079831525215" Y="2.623745616483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.82668421177" Y="2.71529428652" />
                  <Point X="28.981797601532" Y="2.723423434815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.829006238256" Y="2.391284117377" />
                  <Point X="27.136468719512" Y="2.721844218931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.886284801638" Y="2.489416332554" />
                  <Point X="27.193105913809" Y="2.819942821379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.943563683805" Y="2.587548564438" />
                  <Point X="27.249743108106" Y="2.918041423826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.001928980757" Y="2.685737732909" />
                  <Point X="21.853795305381" Y="2.730382155228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.039781960843" Y="2.740129302817" />
                  <Point X="27.306380302403" Y="3.016140026274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.079085485756" Y="2.784911706863" />
                  <Point X="21.65460818164" Y="2.815073573281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.170043732701" Y="2.842086405876" />
                  <Point X="27.3630174967" Y="3.114238628722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.156241990756" Y="2.884085680817" />
                  <Point X="21.503549606449" Y="2.902287301683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.236794183688" Y="2.940715021648" />
                  <Point X="27.419654690997" Y="3.212337231169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.233398495756" Y="2.983259654771" />
                  <Point X="21.352491031257" Y="2.989501030086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.2514592311" Y="3.036613957085" />
                  <Point X="27.476291885294" Y="3.310435833617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.243201806086" Y="3.131311576648" />
                  <Point X="27.532929079591" Y="3.408534436065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.214551223938" Y="3.224940436132" />
                  <Point X="27.589566273888" Y="3.506633038513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.161520328054" Y="3.317291577515" />
                  <Point X="27.646203468185" Y="3.60473164096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.108209808491" Y="3.409628064442" />
                  <Point X="27.702840662482" Y="3.702830243408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.054899452309" Y="3.501964559932" />
                  <Point X="27.759478224952" Y="3.800928865151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.001589096126" Y="3.594301055421" />
                  <Point X="27.816116065844" Y="3.899027501485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.948278739944" Y="3.686637550911" />
                  <Point X="27.696861256769" Y="3.987907994643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.054646162943" Y="3.787342404208" />
                  <Point X="27.560702251937" Y="4.07590257644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.187829363565" Y="3.88945261286" />
                  <Point X="27.410892026424" Y="4.163181728077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.321011306535" Y="3.991562755602" />
                  <Point X="23.122385217892" Y="4.033560982671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.236273927121" Y="4.039529637007" />
                  <Point X="24.872754202151" Y="4.125293934062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.159117878804" Y="4.140301618423" />
                  <Point X="27.261081612163" Y="4.250460869822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.497223588326" Y="4.095928022842" />
                  <Point X="22.939114985683" Y="4.119086569662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.440565654497" Y="4.145366485635" />
                  <Point X="24.79379930466" Y="4.216286456091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.229120640825" Y="4.239100680594" />
                  <Point X="27.080515980044" Y="4.336128198897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.686284751325" Y="4.200966671414" />
                  <Point X="22.852456871779" Y="4.209675383225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.497049078007" Y="4.243457029297" />
                  <Point X="24.761472118886" Y="4.309722632944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.257810637743" Y="4.33573463249" />
                  <Point X="26.897599502663" Y="4.421672325393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.527520455976" Y="4.340184339418" />
                  <Point X="24.736335143285" Y="4.403535632744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.283663832797" Y="4.4322199139" />
                  <Point X="26.678378512764" Y="4.50531381301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.537025493682" Y="4.435812850206" />
                  <Point X="24.711198167683" Y="4.497348632545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.30951717588" Y="4.528705203067" />
                  <Point X="26.447400178793" Y="4.588339124334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.524992815759" Y="4.530312617147" />
                  <Point X="24.686061192082" Y="4.591161632346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.335370518962" Y="4.625190492235" />
                  <Point X="26.123654880567" Y="4.66650272507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.795190946883" Y="4.639603474035" />
                  <Point X="24.660924216481" Y="4.684974632146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.361223862045" Y="4.721675781403" />
                  <Point X="25.775566840792" Y="4.743390576781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.575639606856" Y="4.775635428019" />
                  <Point X="24.635787240879" Y="4.778787631947" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.7792734375" Y="-4.685588378906" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.338541015625" Y="-3.340321533203" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.079701171875" Y="-3.205992431641" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.796701171875" Y="-3.248173828125" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285644042969" />
                  <Point X="24.585943359375" Y="-3.466866455078" />
                  <Point X="24.547994140625" Y="-3.521544433594" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.44792578125" Y="-3.883626220703" />
                  <Point X="24.152255859375" Y="-4.987077148438" />
                  <Point X="23.967755859375" Y="-4.951264648438" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.6740546875" Y="-4.879994628906" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.653806640625" Y="-4.8324140625" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.5347578125" />
                  <Point X="23.643818359375" Y="-4.30099609375" />
                  <Point X="23.6297890625" Y="-4.230466308594" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.4345234375" Y="-4.045478515625" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.112927734375" Y="-3.970174560547" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.811947265625" Y="-4.10620703125" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.688138671875" Y="-4.225227539062" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.24383984375" Y="-4.229325683594" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.831638671875" Y="-3.926974609375" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="21.922080078125" Y="-3.619657470703" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597592773438" />
                  <Point X="22.486021484375" Y="-2.568763671875" />
                  <Point X="22.4686796875" Y="-2.551422119141" />
                  <Point X="22.439849609375" Y="-2.53719921875" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="22.109455078125" Y="-2.716017578125" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.91704296875" Y="-2.950586181641" />
                  <Point X="20.838302734375" Y="-2.847135986328" />
                  <Point X="20.61423828125" Y="-2.471417236328" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="20.835595703125" Y="-2.190945556641" />
                  <Point X="21.836212890625" Y="-1.423144897461" />
                  <Point X="21.854177734375" Y="-1.39601574707" />
                  <Point X="21.86009765625" Y="-1.373163330078" />
                  <Point X="21.861884765625" Y="-1.366262084961" />
                  <Point X="21.859673828125" Y="-1.334592895508" />
                  <Point X="21.83883984375" Y="-1.310638305664" />
                  <Point X="21.81849609375" Y="-1.298664428711" />
                  <Point X="21.812357421875" Y="-1.295051757812" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.4031875" Y="-1.338241577148" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.10340625" Y="-1.131767700195" />
                  <Point X="20.072609375" Y="-1.011189697266" />
                  <Point X="20.013328125" Y="-0.596708679199" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="20.30268359375" Y="-0.434068237305" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.4581015625" Y="-0.121427024841" />
                  <Point X="21.479421875" Y="-0.106629463196" />
                  <Point X="21.48585546875" Y="-0.102164848328" />
                  <Point X="21.5051015625" Y="-0.075909736633" />
                  <Point X="21.51220703125" Y="-0.053011520386" />
                  <Point X="21.5143515625" Y="-0.046102703094" />
                  <Point X="21.5143515625" Y="-0.016457374573" />
                  <Point X="21.50724609375" Y="0.006440839291" />
                  <Point X="21.5051015625" Y="0.013349655151" />
                  <Point X="21.48585546875" Y="0.03960477066" />
                  <Point X="21.46453515625" Y="0.054402183533" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="21.098625" Y="0.158236282349" />
                  <Point X="20.001814453125" Y="0.45212600708" />
                  <Point X="20.0625078125" Y="0.862284179688" />
                  <Point X="20.08235546875" Y="0.996415283203" />
                  <Point X="20.201685546875" Y="1.436779907227" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.42966015625" Y="1.501550170898" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866333008" />
                  <Point X="21.315484375" Y="1.410744873047" />
                  <Point X="21.32972265625" Y="1.415233764648" />
                  <Point X="21.348466796875" Y="1.426055908203" />
                  <Point X="21.360880859375" Y="1.443785888672" />
                  <Point X="21.379814453125" Y="1.489498046875" />
                  <Point X="21.38552734375" Y="1.503290161133" />
                  <Point X="21.389287109375" Y="1.524607299805" />
                  <Point X="21.38368359375" Y="1.545515258789" />
                  <Point X="21.360837890625" Y="1.589403320312" />
                  <Point X="21.353943359375" Y="1.602645019531" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.142763671875" Y="1.770591674805" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.762861328125" Y="2.654873535156" />
                  <Point X="20.83998828125" Y="2.787007568359" />
                  <Point X="21.1560859375" Y="3.193307373047" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.33901953125" Y="3.216672119141" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.92720703125" Y="2.914685058594" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927405273438" />
                  <Point X="22.0333984375" Y="2.974053955078" />
                  <Point X="22.04747265625" Y="2.988128662109" />
                  <Point X="22.0591015625" Y="3.006382324219" />
                  <Point X="22.061927734375" Y="3.027840087891" />
                  <Point X="22.056177734375" Y="3.093560546875" />
                  <Point X="22.054443359375" Y="3.113389648438" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.960517578125" Y="3.285441650391" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="22.11280078125" Y="4.071346191406" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.744970703125" Y="4.450924316406" />
                  <Point X="22.858453125" Y="4.513972167969" />
                  <Point X="22.87855078125" Y="4.487779785156" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.121900390625" Y="4.235582519531" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.18619140625" Y="4.22225" />
                  <Point X="23.262376953125" Y="4.253808105469" />
                  <Point X="23.285365234375" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275743652344" />
                  <Point X="23.31391796875" Y="4.294487304688" />
                  <Point X="23.33871484375" Y="4.373134765625" />
                  <Point X="23.346197265625" Y="4.396863769531" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.33814453125" Y="4.493914550781" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.858015625" Y="4.854543457031" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.63544921875" Y="4.973932128906" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.80940625" Y="4.864939941406" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.099244140625" Y="4.478065429688" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.708373046875" Y="4.941466796875" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.359533203125" Y="4.805013183594" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.834099609375" Y="4.6509453125" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.2453046875" Y="4.468812011719" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.642296875" Y="4.248258300781" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="28.018857421875" Y="3.992066894531" />
                  <Point X="28.0687421875" Y="3.956592529297" />
                  <Point X="27.891771484375" Y="3.650071289062" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491515380859" />
                  <Point X="27.208359375" Y="2.429838134766" />
                  <Point X="27.202044921875" Y="2.392328613281" />
                  <Point X="27.2084765625" Y="2.338995361328" />
                  <Point X="27.210416015625" Y="2.322903808594" />
                  <Point X="27.218681640625" Y="2.300812011719" />
                  <Point X="27.25168359375" Y="2.252177246094" />
                  <Point X="27.27494140625" Y="2.224203857422" />
                  <Point X="27.323576171875" Y="2.191203125" />
                  <Point X="27.33825" Y="2.181246337891" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.413669921875" Y="2.166549072266" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.510341796875" Y="2.182439697266" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="27.8869453125" Y="2.392128417969" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.149072265625" Y="2.816261230469" />
                  <Point X="29.20259765625" Y="2.741875" />
                  <Point X="29.36221875" Y="2.478095947266" />
                  <Point X="29.387513671875" Y="2.436295654297" />
                  <Point X="29.16009375" Y="2.261789306641" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583832641602" />
                  <Point X="28.234982421875" Y="1.525923461914" />
                  <Point X="28.22158984375" Y="1.508451293945" />
                  <Point X="28.21312109375" Y="1.491503417969" />
                  <Point X="28.1965859375" Y="1.432377929688" />
                  <Point X="28.191595703125" Y="1.41453894043" />
                  <Point X="28.190779296875" Y="1.390964599609" />
                  <Point X="28.204353515625" Y="1.325180053711" />
                  <Point X="28.20844921875" Y="1.305331787109" />
                  <Point X="28.215646484375" Y="1.287955444336" />
                  <Point X="28.252564453125" Y="1.231840576172" />
                  <Point X="28.263703125" Y="1.21491003418" />
                  <Point X="28.280947265625" Y="1.198820800781" />
                  <Point X="28.33444921875" Y="1.168704711914" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.44090234375" Y="1.144059448242" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.80439453125" Y="1.184431640625" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.916201171875" Y="1.045807006836" />
                  <Point X="29.939193359375" Y="0.951366699219" />
                  <Point X="29.9894921875" Y="0.628298217773" />
                  <Point X="29.997859375" Y="0.574556335449" />
                  <Point X="29.7398984375" Y="0.505435577393" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232818893433" />
                  <Point X="28.65801953125" Y="0.191740509033" />
                  <Point X="28.636578125" Y="0.1793465271" />
                  <Point X="28.622265625" Y="0.166926391602" />
                  <Point X="28.579625" Y="0.112592285156" />
                  <Point X="28.566759765625" Y="0.09619871521" />
                  <Point X="28.556986328125" Y="0.074736480713" />
                  <Point X="28.5427734375" Y="0.000518648803" />
                  <Point X="28.538484375" Y="-0.021874082565" />
                  <Point X="28.538484375" Y="-0.040685997009" />
                  <Point X="28.552697265625" Y="-0.114903823853" />
                  <Point X="28.556986328125" Y="-0.137296554565" />
                  <Point X="28.566759765625" Y="-0.158758789063" />
                  <Point X="28.609400390625" Y="-0.213093048096" />
                  <Point X="28.622265625" Y="-0.229486465454" />
                  <Point X="28.636578125" Y="-0.241906600952" />
                  <Point X="28.707646484375" Y="-0.282984985352" />
                  <Point X="28.729087890625" Y="-0.295378967285" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.042501953125" Y="-0.381128875732" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.961267578125" Y="-0.88126953125" />
                  <Point X="29.948431640625" Y="-0.96641217041" />
                  <Point X="29.88398828125" Y="-1.248811767578" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.570228515625" Y="-1.250114135742" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.255357421875" Y="-1.128657958984" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.185447265625" Y="-1.154696777344" />
                  <Point X="28.101140625" Y="-1.256092163086" />
                  <Point X="28.075703125" Y="-1.286684814453" />
                  <Point X="28.064359375" Y="-1.314072021484" />
                  <Point X="28.052275390625" Y="-1.445383544922" />
                  <Point X="28.048630859375" Y="-1.485002441406" />
                  <Point X="28.05636328125" Y="-1.516622924805" />
                  <Point X="28.133552734375" Y="-1.636687866211" />
                  <Point X="28.15684375" Y="-1.672913452148" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.4481015625" Y="-1.900114990234" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.2403125" Y="-2.743597412109" />
                  <Point X="29.204130859375" Y="-2.802142578125" />
                  <Point X="29.070853515625" Y="-2.99151171875" />
                  <Point X="29.056689453125" Y="-3.011638183594" />
                  <Point X="28.785029296875" Y="-2.854795410156" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.571337890625" Y="-2.223332519531" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.351169921875" Y="-2.291825195312" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334682373047" />
                  <Point X="27.216021484375" Y="-2.472591064453" />
                  <Point X="27.19412109375" Y="-2.514200439453" />
                  <Point X="27.1891640625" Y="-2.546375732422" />
                  <Point X="27.21914453125" Y="-2.712379882812" />
                  <Point X="27.22819140625" Y="-2.762466064453" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.4137890625" Y="-3.089821289062" />
                  <Point X="27.98667578125" Y="-4.082088867188" />
                  <Point X="27.878232421875" Y="-4.159546875" />
                  <Point X="27.835314453125" Y="-4.190202148438" />
                  <Point X="27.686283203125" Y="-4.286666992188" />
                  <Point X="27.679775390625" Y="-4.29087890625" />
                  <Point X="27.470365234375" Y="-4.017967773438" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467529297" />
                  <Point X="26.50682421875" Y="-2.875207519531" />
                  <Point X="26.45742578125" Y="-2.843448730469" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.246744140625" Y="-2.852194335938" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868508789062" />
                  <Point X="26.027068359375" Y="-2.98347265625" />
                  <Point X="25.985349609375" Y="-3.018159179688" />
                  <Point X="25.96845703125" Y="-3.045985107422" />
                  <Point X="25.9271171875" Y="-3.236186279297" />
                  <Point X="25.914642578125" Y="-3.293572998047" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.96485546875" Y="-3.697530029297" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.0349609375" Y="-4.954344726562" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#194" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.148453669796" Y="4.909161140093" Z="1.95" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.95" />
                  <Point X="-0.376520263215" Y="5.054873288322" Z="1.95" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.95" />
                  <Point X="-1.161639751034" Y="4.933966835172" Z="1.95" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.95" />
                  <Point X="-1.717584090305" Y="4.518668739546" Z="1.95" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.95" />
                  <Point X="-1.71512768982" Y="4.419451391419" Z="1.95" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.95" />
                  <Point X="-1.762912015899" Y="4.331282546769" Z="1.95" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.95" />
                  <Point X="-1.861168578771" Y="4.311213330345" Z="1.95" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.95" />
                  <Point X="-2.087938952835" Y="4.549497783769" Z="1.95" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.95" />
                  <Point X="-2.285468466021" Y="4.525911741359" Z="1.95" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.95" />
                  <Point X="-2.923778829153" Y="4.142305941339" Z="1.95" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.95" />
                  <Point X="-3.088940558729" Y="3.291721632871" Z="1.95" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.95" />
                  <Point X="-2.999789863124" Y="3.120483985277" Z="1.95" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.95" />
                  <Point X="-3.008114724876" Y="3.040688843011" Z="1.95" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.95" />
                  <Point X="-3.07459248033" Y="2.995774836049" Z="1.95" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.95" />
                  <Point X="-3.642137715426" Y="3.291253390017" Z="1.95" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.95" />
                  <Point X="-3.889534692194" Y="3.255289880974" Z="1.95" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.95" />
                  <Point X="-4.286476892867" Y="2.711359003022" Z="1.95" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.95" />
                  <Point X="-3.893831300754" Y="1.762203940412" Z="1.95" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.95" />
                  <Point X="-3.689669083508" Y="1.59759239313" Z="1.95" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.95" />
                  <Point X="-3.672535310171" Y="1.53991227569" Z="1.95" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.95" />
                  <Point X="-3.705707475863" Y="1.489711026875" Z="1.95" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.95" />
                  <Point X="-4.56997105532" Y="1.582402529763" Z="1.95" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.95" />
                  <Point X="-4.85273173301" Y="1.481136804301" Z="1.95" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.95" />
                  <Point X="-4.993110962462" Y="0.900882952136" Z="1.95" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.95" />
                  <Point X="-3.920473216242" Y="0.141220104918" Z="1.95" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.95" />
                  <Point X="-3.570128078206" Y="0.044604400423" Z="1.95" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.95" />
                  <Point X="-3.546663603414" Y="0.02289819302" Z="1.95" />
                  <Point X="-3.539556741714" Y="0" Z="1.95" />
                  <Point X="-3.541700997721" Y="-0.006908758041" Z="1.95" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.95" />
                  <Point X="-3.555240516917" Y="-0.034271582711" Z="1.95" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.95" />
                  <Point X="-4.716414572531" Y="-0.354491951838" Z="1.95" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.95" />
                  <Point X="-5.042325514938" Y="-0.572507880574" Z="1.95" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.95" />
                  <Point X="-4.951195173368" Y="-1.11286118596" Z="1.95" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.95" />
                  <Point X="-3.596443113962" Y="-1.356533709091" Z="1.95" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.95" />
                  <Point X="-3.21302066354" Y="-1.310475999677" Z="1.95" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.95" />
                  <Point X="-3.194462080547" Y="-1.329344750772" Z="1.95" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.95" />
                  <Point X="-4.200998063939" Y="-2.119997767452" Z="1.95" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.95" />
                  <Point X="-4.434861559141" Y="-2.465746808069" Z="1.95" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.95" />
                  <Point X="-4.128899133894" Y="-2.949589373668" Z="1.95" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.95" />
                  <Point X="-2.871700940424" Y="-2.728038702149" Z="1.95" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.95" />
                  <Point X="-2.568818396816" Y="-2.559512072315" Z="1.95" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.95" />
                  <Point X="-3.127378311019" Y="-3.563377442068" Z="1.95" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.95" />
                  <Point X="-3.205022184632" Y="-3.935311693617" Z="1.95" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.95" />
                  <Point X="-2.788639653383" Y="-4.240556389916" Z="1.95" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.95" />
                  <Point X="-2.278349326987" Y="-4.224385449772" Z="1.95" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.95" />
                  <Point X="-2.166429961546" Y="-4.116500172482" Z="1.95" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.95" />
                  <Point X="-1.896498355058" Y="-3.988787617184" Z="1.95" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.95" />
                  <Point X="-1.604600839234" Y="-4.051790617324" Z="1.95" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.95" />
                  <Point X="-1.411376719209" Y="-4.279470292769" Z="1.95" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.95" />
                  <Point X="-1.40192233299" Y="-4.794607688595" Z="1.95" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.95" />
                  <Point X="-1.34456132962" Y="-4.897137409925" Z="1.95" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.95" />
                  <Point X="-1.047928512888" Y="-4.969068589634" Z="1.95" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.95" />
                  <Point X="-0.509934897067" Y="-3.865286993701" Z="1.95" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.95" />
                  <Point X="-0.379137408243" Y="-3.464095269154" Z="1.95" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.95" />
                  <Point X="-0.194634637089" Y="-3.264646777102" Z="1.95" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.95" />
                  <Point X="0.058724442272" Y="-3.222465268186" Z="1.95" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.95" />
                  <Point X="0.291308451189" Y="-3.337550486614" Z="1.95" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.95" />
                  <Point X="0.724819963334" Y="-4.667249036115" Z="1.95" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.95" />
                  <Point X="0.859468389945" Y="-5.006169360197" Z="1.95" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.95" />
                  <Point X="1.03950932071" Y="-4.971904784631" Z="1.95" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.95" />
                  <Point X="1.008270275095" Y="-3.659723563098" Z="1.95" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.95" />
                  <Point X="0.969819032307" Y="-3.215526966793" Z="1.95" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.95" />
                  <Point X="1.052877267668" Y="-2.990639486837" Z="1.95" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.95" />
                  <Point X="1.245169392534" Y="-2.870703951172" Z="1.95" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.95" />
                  <Point X="1.473628943371" Y="-2.885985252117" Z="1.95" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.95" />
                  <Point X="2.424539841496" Y="-4.017125756342" Z="1.95" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.95" />
                  <Point X="2.707297037713" Y="-4.297360929065" Z="1.95" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.95" />
                  <Point X="2.901136053335" Y="-4.168954059871" Z="1.95" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.95" />
                  <Point X="2.450932976996" Y="-3.033541233188" Z="1.95" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.95" />
                  <Point X="2.262191365649" Y="-2.672212433606" Z="1.95" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.95" />
                  <Point X="2.254109801309" Y="-2.464598883077" Z="1.95" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.95" />
                  <Point X="2.368299495295" Y="-2.304791508658" Z="1.95" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.95" />
                  <Point X="2.556294369907" Y="-2.241256643842" Z="1.95" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.95" />
                  <Point X="3.753872363919" Y="-2.866816523279" Z="1.95" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.95" />
                  <Point X="4.105586052545" Y="-2.989008777091" Z="1.95" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.95" />
                  <Point X="4.276688535779" Y="-2.73860269695" Z="1.95" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.95" />
                  <Point X="3.472381615369" Y="-1.829167448834" Z="1.95" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.95" />
                  <Point X="3.16945314947" Y="-1.578367470033" Z="1.95" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.95" />
                  <Point X="3.095908216945" Y="-1.418683690494" Z="1.95" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.95" />
                  <Point X="3.133428316935" Y="-1.25677958583" Z="1.95" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.95" />
                  <Point X="3.259819136546" Y="-1.146237136722" Z="1.95" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.95" />
                  <Point X="4.557545233624" Y="-1.268406309207" Z="1.95" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.95" />
                  <Point X="4.926576599548" Y="-1.228655993326" Z="1.95" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.95" />
                  <Point X="5.004551938392" Y="-0.857443378549" Z="1.95" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.95" />
                  <Point X="4.049285656065" Y="-0.301552555492" Z="1.95" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.95" />
                  <Point X="3.726510415904" Y="-0.208416612523" Z="1.95" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.95" />
                  <Point X="3.642577344437" Y="-0.15094470236" Z="1.95" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.95" />
                  <Point X="3.595648266958" Y="-0.074217808696" Z="1.95" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.95" />
                  <Point X="3.585723183466" Y="0.022392722494" Z="1.95" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.95" />
                  <Point X="3.612802093963" Y="0.113004036215" Z="1.95" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.95" />
                  <Point X="3.676884998448" Y="0.179732231719" Z="1.95" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.95" />
                  <Point X="4.746682161241" Y="0.488419377277" Z="1.95" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.95" />
                  <Point X="5.032740070489" Y="0.667270430267" Z="1.95" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.95" />
                  <Point X="4.958626865393" Y="1.088914086368" Z="1.95" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.95" />
                  <Point X="3.791712483185" Y="1.265283931372" Z="1.95" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.95" />
                  <Point X="3.44129649993" Y="1.224908511428" Z="1.95" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.95" />
                  <Point X="3.352818740217" Y="1.243555058916" Z="1.95" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.95" />
                  <Point X="3.288179605708" Y="1.290601739772" Z="1.95" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.95" />
                  <Point X="3.24716562273" Y="1.366564670283" Z="1.95" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.95" />
                  <Point X="3.238580929416" Y="1.450188302024" Z="1.95" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.95" />
                  <Point X="3.268508937489" Y="1.526785863353" Z="1.95" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.95" />
                  <Point X="4.184373549201" Y="2.25340166401" Z="1.95" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.95" />
                  <Point X="4.398839490724" Y="2.535262463735" Z="1.95" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.95" />
                  <Point X="4.183500970876" Y="2.876744365502" Z="1.95" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.95" />
                  <Point X="2.855787661156" Y="2.466709838545" Z="1.95" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.95" />
                  <Point X="2.491268892552" Y="2.262022471441" Z="1.95" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.95" />
                  <Point X="2.413500120185" Y="2.247469515944" Z="1.95" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.95" />
                  <Point X="2.345492875341" Y="2.263857375912" Z="1.95" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.95" />
                  <Point X="2.286901224323" Y="2.311531985041" Z="1.95" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.95" />
                  <Point X="2.251960186815" Y="2.376258323647" Z="1.95" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.95" />
                  <Point X="2.250505471217" Y="2.44820068333" Z="1.95" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.95" />
                  <Point X="2.928915514314" Y="3.656351420799" Z="1.95" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.95" />
                  <Point X="3.041677948918" Y="4.064094228197" Z="1.95" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.95" />
                  <Point X="2.661309419729" Y="4.322741188011" Z="1.95" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.95" />
                  <Point X="2.26033017559" Y="4.545384011994" Z="1.95" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.95" />
                  <Point X="1.844991638984" Y="4.729228840577" Z="1.95" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.95" />
                  <Point X="1.365109731919" Y="4.884896649781" Z="1.95" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.95" />
                  <Point X="0.707422729317" Y="5.022474845694" Z="1.95" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.95" />
                  <Point X="0.044790891757" Y="4.522286365857" Z="1.95" />
                  <Point X="0" Y="4.355124473572" Z="1.95" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>