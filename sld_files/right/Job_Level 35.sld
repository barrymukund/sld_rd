<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#185" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2548" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.831609375" Y="-4.51385546875" />
                  <Point X="25.5633046875" Y="-3.512524414063" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.432701171875" Y="-3.309372802734" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.132798828125" Y="-3.123001220703" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.793478515625" Y="-3.149703613281" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231477050781" />
                  <Point X="24.524013671875" Y="-3.389480957031" />
                  <Point X="24.46994921875" Y="-3.467377441406" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.316734375" Y="-4.006182861328" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="24.015359375" Y="-4.863731445312" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.75358203125" Y="-4.802362792969" />
                  <Point X="23.7850390625" Y="-4.5634375" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.742951171875" Y="-4.312411132812" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182963867188" />
                  <Point X="23.69598828125" Y="-4.155126953125" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.52012109375" Y="-3.994188476562" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.14961328125" Y="-3.877375244141" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.78455859375" Y="-4.010252197266" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.664900390625" Y="-4.099461425781" />
                  <Point X="22.59063671875" Y="-4.196241699219" />
                  <Point X="22.519853515625" Y="-4.288489257813" />
                  <Point X="22.337095703125" Y="-4.175330566406" />
                  <Point X="22.198287109375" Y="-4.089384033203" />
                  <Point X="21.929896484375" Y="-3.882731933594" />
                  <Point X="21.895279296875" Y="-3.856077880859" />
                  <Point X="22.077623046875" Y="-3.540247802734" />
                  <Point X="22.576240234375" Y="-2.676619384766" />
                  <Point X="22.587140625" Y="-2.647655029297" />
                  <Point X="22.593412109375" Y="-2.616129394531" />
                  <Point X="22.59442578125" Y="-2.585194824219" />
                  <Point X="22.58544140625" Y="-2.555576660156" />
                  <Point X="22.571224609375" Y="-2.526747802734" />
                  <Point X="22.55319921875" Y="-2.501591796875" />
                  <Point X="22.5358515625" Y="-2.484243164062" />
                  <Point X="22.5106953125" Y="-2.466215087891" />
                  <Point X="22.4818671875" Y="-2.451997314453" />
                  <Point X="22.452248046875" Y="-2.44301171875" />
                  <Point X="22.4213125" Y="-2.444023925781" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.935046875" Y="-2.707016113281" />
                  <Point X="21.181978515625" Y="-3.141800537109" />
                  <Point X="21.026802734375" Y="-2.93793359375" />
                  <Point X="20.9171328125" Y="-2.793849365234" />
                  <Point X="20.72472265625" Y="-2.471206787109" />
                  <Point X="20.693857421875" Y="-2.419450439453" />
                  <Point X="21.02162890625" Y="-2.167942382812" />
                  <Point X="21.894044921875" Y="-1.498513305664" />
                  <Point X="21.915419921875" Y="-1.475598388672" />
                  <Point X="21.933384765625" Y="-1.448471069336" />
                  <Point X="21.946140625" Y="-1.41984375" />
                  <Point X="21.951302734375" Y="-1.399919067383" />
                  <Point X="21.953853515625" Y="-1.390066162109" />
                  <Point X="21.956654296875" Y="-1.359644287109" />
                  <Point X="21.954443359375" Y="-1.327977294922" />
                  <Point X="21.94744140625" Y="-1.298235961914" />
                  <Point X="21.931359375" Y="-1.272255615234" />
                  <Point X="21.910529296875" Y="-1.248301879883" />
                  <Point X="21.887029296875" Y="-1.228767211914" />
                  <Point X="21.869291015625" Y="-1.218327514648" />
                  <Point X="21.860546875" Y="-1.213180786133" />
                  <Point X="21.831287109375" Y="-1.201957763672" />
                  <Point X="21.7993984375" Y="-1.195475219727" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="21.230576171875" Y="-1.265146362305" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.208814453125" Y="-1.160567871094" />
                  <Point X="20.16592578125" Y="-0.992651855469" />
                  <Point X="20.115015625" Y="-0.636705749512" />
                  <Point X="20.107578125" Y="-0.584698303223" />
                  <Point X="20.4733125" Y="-0.486699859619" />
                  <Point X="21.467125" Y="-0.220408432007" />
                  <Point X="21.4825078125" Y="-0.214827087402" />
                  <Point X="21.51226953125" Y="-0.199470611572" />
                  <Point X="21.530859375" Y="-0.186569046021" />
                  <Point X="21.5400234375" Y="-0.180208724976" />
                  <Point X="21.56248046875" Y="-0.158323898315" />
                  <Point X="21.581724609375" Y="-0.132068557739" />
                  <Point X="21.5958359375" Y="-0.104063087463" />
                  <Point X="21.60203125" Y="-0.084098556519" />
                  <Point X="21.6050859375" Y="-0.074256164551" />
                  <Point X="21.609353515625" Y="-0.046100574493" />
                  <Point X="21.609353515625" Y="-0.016459560394" />
                  <Point X="21.6050859375" Y="0.011696336746" />
                  <Point X="21.598890625" Y="0.031660564423" />
                  <Point X="21.5958359375" Y="0.041503105164" />
                  <Point X="21.581724609375" Y="0.069508430481" />
                  <Point X="21.56248046875" Y="0.095763755798" />
                  <Point X="21.5400234375" Y="0.117648590088" />
                  <Point X="21.52143359375" Y="0.13055015564" />
                  <Point X="21.51299609375" Y="0.135775695801" />
                  <Point X="21.488263671875" Y="0.149356689453" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="20.977173828125" Y="0.289130554199" />
                  <Point X="20.10818359375" Y="0.521975524902" />
                  <Point X="20.14787109375" Y="0.790179931641" />
                  <Point X="20.17551171875" Y="0.976968139648" />
                  <Point X="20.277990234375" Y="1.355148925781" />
                  <Point X="20.29644921875" Y="1.423268066406" />
                  <Point X="20.5219453125" Y="1.393580932617" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255017578125" Y="1.299341796875" />
                  <Point X="21.276578125" Y="1.301228271484" />
                  <Point X="21.29686328125" Y="1.305263671875" />
                  <Point X="21.338005859375" Y="1.318235961914" />
                  <Point X="21.3582890625" Y="1.324631225586" />
                  <Point X="21.37722265625" Y="1.332962036133" />
                  <Point X="21.395966796875" Y="1.343784057617" />
                  <Point X="21.4126484375" Y="1.356015625" />
                  <Point X="21.426287109375" Y="1.371567993164" />
                  <Point X="21.438701171875" Y="1.389297851562" />
                  <Point X="21.448650390625" Y="1.407432861328" />
                  <Point X="21.465158203125" Y="1.447288208008" />
                  <Point X="21.473296875" Y="1.466937011719" />
                  <Point X="21.479083984375" Y="1.486788085938" />
                  <Point X="21.48284375" Y="1.508103271484" />
                  <Point X="21.484197265625" Y="1.528745849609" />
                  <Point X="21.48105078125" Y="1.549192260742" />
                  <Point X="21.47544921875" Y="1.570098510742" />
                  <Point X="21.467951171875" Y="1.589378662109" />
                  <Point X="21.44803125" Y="1.627643554688" />
                  <Point X="21.4382109375" Y="1.646508300781" />
                  <Point X="21.426716796875" Y="1.663710083008" />
                  <Point X="21.412802734375" Y="1.680290405273" />
                  <Point X="21.39786328125" Y="1.694590332031" />
                  <Point X="21.116826171875" Y="1.91023828125" />
                  <Point X="20.648140625" Y="2.269873535156" />
                  <Point X="20.8114453125" Y="2.549655029297" />
                  <Point X="20.9188515625" Y="2.7336640625" />
                  <Point X="21.1903046875" Y="3.082581054688" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.355814453125" Y="3.097279052734" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.9105078125" Y="2.820783203125" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.00098828125" Y="2.826504882812" />
                  <Point X="22.0195390625" Y="2.835654541016" />
                  <Point X="22.03779296875" Y="2.847284179688" />
                  <Point X="22.053921875" Y="2.860228515625" />
                  <Point X="22.094595703125" Y="2.900900634766" />
                  <Point X="22.114646484375" Y="2.920951904297" />
                  <Point X="22.12759375" Y="2.937084228516" />
                  <Point X="22.13922265625" Y="2.955337646484" />
                  <Point X="22.14837109375" Y="2.973887207031" />
                  <Point X="22.1532890625" Y="2.993976806641" />
                  <Point X="22.156115234375" Y="3.015434814453" />
                  <Point X="22.15656640625" Y="3.036120605469" />
                  <Point X="22.151552734375" Y="3.093420898438" />
                  <Point X="22.14908203125" Y="3.121670166016" />
                  <Point X="22.145044921875" Y="3.141959960938" />
                  <Point X="22.13853515625" Y="3.162603515625" />
                  <Point X="22.130205078125" Y="3.181533203125" />
                  <Point X="22.00566796875" Y="3.397236572266" />
                  <Point X="21.81666796875" Y="3.724595703125" />
                  <Point X="22.112322265625" Y="3.951270751953" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.726912109375" Y="4.332213867188" />
                  <Point X="22.83296484375" Y="4.391133789062" />
                  <Point X="22.9568046875" Y="4.229741699219" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.0686640625" Y="4.1561953125" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.1193828125" Y="4.132331054688" />
                  <Point X="23.1402890625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.12358203125" />
                  <Point X="23.18137109375" Y="4.124934570312" />
                  <Point X="23.2026875" Y="4.128692871094" />
                  <Point X="23.222548828125" Y="4.134481933594" />
                  <Point X="23.288974609375" Y="4.161997070312" />
                  <Point X="23.32172265625" Y="4.175561523437" />
                  <Point X="23.339857421875" Y="4.185511230469" />
                  <Point X="23.3575859375" Y="4.197925292969" />
                  <Point X="23.37313671875" Y="4.211562011719" />
                  <Point X="23.3853671875" Y="4.228240722656" />
                  <Point X="23.396189453125" Y="4.246983886719" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.426140625" Y="4.3344921875" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583007812" />
                  <Point X="23.44272265625" Y="4.410146484375" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.42811328125" Y="4.538370605469" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.808212890625" Y="4.741916992188" />
                  <Point X="24.050369140625" Y="4.80980859375" />
                  <Point X="24.568666015625" Y="4.870467773438" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.736662109375" Y="4.769368652344" />
                  <Point X="24.866095703125" Y="4.286315917969" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.210029296875" Y="4.5244609375" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.6326015625" Y="4.853882324219" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.272849609375" Y="4.728211425781" />
                  <Point X="26.48102734375" Y="4.677950195312" />
                  <Point X="26.759650390625" Y="4.576892578125" />
                  <Point X="26.894646484375" Y="4.527928222656" />
                  <Point X="27.164537109375" Y="4.401709960938" />
                  <Point X="27.294576171875" Y="4.340894042969" />
                  <Point X="27.555326171875" Y="4.188980957031" />
                  <Point X="27.680978515625" Y="4.115775878906" />
                  <Point X="27.92687890625" Y="3.940904541016" />
                  <Point X="27.94326171875" Y="3.929253662109" />
                  <Point X="27.724693359375" Y="3.55068359375" />
                  <Point X="27.147583984375" Y="2.551098144531" />
                  <Point X="27.142080078125" Y="2.539935546875" />
                  <Point X="27.133078125" Y="2.516058349609" />
                  <Point X="27.118697265625" Y="2.462283203125" />
                  <Point X="27.111607421875" Y="2.435771972656" />
                  <Point X="27.108619140625" Y="2.417936523438" />
                  <Point X="27.107728515625" Y="2.380953613281" />
                  <Point X="27.1133359375" Y="2.334453613281" />
                  <Point X="27.116099609375" Y="2.311528808594" />
                  <Point X="27.121443359375" Y="2.289603515625" />
                  <Point X="27.1297109375" Y="2.267512939453" />
                  <Point X="27.140072265625" Y="2.247469726562" />
                  <Point X="27.168845703125" Y="2.205066162109" />
                  <Point X="27.18303125" Y="2.184161132813" />
                  <Point X="27.19446875" Y="2.170326904297" />
                  <Point X="27.2216015625" Y="2.145592529297" />
                  <Point X="27.26400390625" Y="2.116819824219" />
                  <Point X="27.28491015625" Y="2.102635009766" />
                  <Point X="27.304955078125" Y="2.092271484375" />
                  <Point X="27.327041015625" Y="2.084006347656" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.39546484375" Y="2.073056396484" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845458984" />
                  <Point X="27.47320703125" Y="2.074170898438" />
                  <Point X="27.526982421875" Y="2.088551269531" />
                  <Point X="27.553494140625" Y="2.095640625" />
                  <Point X="27.565283203125" Y="2.099637939453" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.08133203125" Y="2.394661865234" />
                  <Point X="28.967326171875" Y="2.906190673828" />
                  <Point X="29.04873828125" Y="2.793046875" />
                  <Point X="29.12327734375" Y="2.689453613281" />
                  <Point X="29.260357421875" Y="2.462926513672" />
                  <Point X="29.26219921875" Y="2.459883056641" />
                  <Point X="28.99060546875" Y="2.251482177734" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.2214296875" Y="1.660244873047" />
                  <Point X="28.20397265625" Y="1.641626953125" />
                  <Point X="28.165271484375" Y="1.591137207031" />
                  <Point X="28.14619140625" Y="1.566245605469" />
                  <Point X="28.13660546875" Y="1.550909057617" />
                  <Point X="28.1216328125" Y="1.517088623047" />
                  <Point X="28.10721484375" Y="1.465538330078" />
                  <Point X="28.100107421875" Y="1.440124023438" />
                  <Point X="28.09665234375" Y="1.41782434082" />
                  <Point X="28.0958359375" Y="1.394254882812" />
                  <Point X="28.097740234375" Y="1.371766601562" />
                  <Point X="28.109576171875" Y="1.314410400391" />
                  <Point X="28.11541015625" Y="1.286133911133" />
                  <Point X="28.1206796875" Y="1.268979003906" />
                  <Point X="28.136283203125" Y="1.235741577148" />
                  <Point X="28.16847265625" Y="1.18681640625" />
                  <Point X="28.18433984375" Y="1.162696166992" />
                  <Point X="28.198888671875" Y="1.145455810547" />
                  <Point X="28.2161328125" Y="1.129364257812" />
                  <Point X="28.23434765625" Y="1.116034912109" />
                  <Point X="28.280994140625" Y="1.08977734375" />
                  <Point X="28.303990234375" Y="1.076832397461" />
                  <Point X="28.320521484375" Y="1.069501342773" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.4191875" Y="1.051103271484" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.956328125" Y="1.108614379883" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.813923828125" Y="1.064303466797" />
                  <Point X="29.845939453125" Y="0.932788818359" />
                  <Point X="29.88913671875" Y="0.655344543457" />
                  <Point X="29.890865234375" Y="0.644238647461" />
                  <Point X="29.5873515625" Y="0.562912109375" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.704787109375" Y="0.325584991455" />
                  <Point X="28.681546875" Y="0.315067901611" />
                  <Point X="28.619583984375" Y="0.279252502441" />
                  <Point X="28.589037109375" Y="0.261595489502" />
                  <Point X="28.5743125" Y="0.251096755981" />
                  <Point X="28.54753125" Y="0.225576538086" />
                  <Point X="28.510353515625" Y="0.178203826904" />
                  <Point X="28.492025390625" Y="0.154848907471" />
                  <Point X="28.48030078125" Y="0.135567443848" />
                  <Point X="28.47052734375" Y="0.11410382843" />
                  <Point X="28.463681640625" Y="0.092603935242" />
                  <Point X="28.4512890625" Y="0.02789509964" />
                  <Point X="28.4451796875" Y="-0.004006679058" />
                  <Point X="28.443484375" Y="-0.021875591278" />
                  <Point X="28.4451796875" Y="-0.058553455353" />
                  <Point X="28.457572265625" Y="-0.123262290955" />
                  <Point X="28.463681640625" Y="-0.155164077759" />
                  <Point X="28.47052734375" Y="-0.176663955688" />
                  <Point X="28.48030078125" Y="-0.198127578735" />
                  <Point X="28.492025390625" Y="-0.217409042358" />
                  <Point X="28.529203125" Y="-0.264781768799" />
                  <Point X="28.54753125" Y="-0.28813684082" />
                  <Point X="28.56" Y="-0.301236328125" />
                  <Point X="28.589037109375" Y="-0.324155609131" />
                  <Point X="28.651" Y="-0.359970855713" />
                  <Point X="28.681546875" Y="-0.377627868652" />
                  <Point X="28.69270703125" Y="-0.383137237549" />
                  <Point X="28.716580078125" Y="-0.392149963379" />
                  <Point X="29.145873046875" Y="-0.507178344727" />
                  <Point X="29.891474609375" Y="-0.706961853027" />
                  <Point X="29.8728984375" Y="-0.830171875" />
                  <Point X="29.855025390625" Y="-0.94872479248" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="29.434232421875" Y="-1.136390014648" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.253048828125" Y="-1.031941162109" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.163978515625" Y="-1.056595825195" />
                  <Point X="28.136150390625" Y="-1.073488037109" />
                  <Point X="28.112396484375" Y="-1.093960083008" />
                  <Point X="28.038890625" Y="-1.182364624023" />
                  <Point X="28.00265234375" Y="-1.225948242188" />
                  <Point X="27.987931640625" Y="-1.250335205078" />
                  <Point X="27.97658984375" Y="-1.277720825195" />
                  <Point X="27.969759765625" Y="-1.305365966797" />
                  <Point X="27.959224609375" Y="-1.419853759766" />
                  <Point X="27.95403125" Y="-1.476296142578" />
                  <Point X="27.956349609375" Y="-1.507562133789" />
                  <Point X="27.964080078125" Y="-1.539182617188" />
                  <Point X="27.976451171875" Y="-1.567996582031" />
                  <Point X="28.043751953125" Y="-1.672678710938" />
                  <Point X="28.076931640625" Y="-1.724287231445" />
                  <Point X="28.0869375" Y="-1.73724230957" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.509015625" Y="-2.066601074219" />
                  <Point X="29.213123046875" Y="-2.606882324219" />
                  <Point X="29.1751953125" Y="-2.668255126953" />
                  <Point X="29.124810546875" Y="-2.749782714844" />
                  <Point X="29.028982421875" Y="-2.885944824219" />
                  <Point X="28.70036328125" Y="-2.696216064453" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.6094921875" Y="-2.133686035156" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395507812" />
                  <Point X="27.474609375" Y="-2.125353515625" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.32459375" Y="-2.198458251953" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.24238671875" Y="-2.246548583984" />
                  <Point X="27.221427734375" Y="-2.267507324219" />
                  <Point X="27.204533203125" Y="-2.290438476562" />
                  <Point X="27.141251953125" Y="-2.410677978516" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.100228515625" Y="-2.499734863281" />
                  <Point X="27.095271484375" Y="-2.531908447266" />
                  <Point X="27.09567578125" Y="-2.563259277344" />
                  <Point X="27.12181640625" Y="-2.707994628906" />
                  <Point X="27.134703125" Y="-2.779349609375" />
                  <Point X="27.13898828125" Y="-2.795139648438" />
                  <Point X="27.1518203125" Y="-2.826078613281" />
                  <Point X="27.407822265625" Y="-3.26948828125" />
                  <Point X="27.86128515625" Y="-4.054905517578" />
                  <Point X="27.841638671875" Y="-4.068937744141" />
                  <Point X="27.781814453125" Y="-4.111666992188" />
                  <Point X="27.701767578125" Y="-4.163481445312" />
                  <Point X="27.44487890625" Y="-3.828697998047" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922177490234" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.57917578125" Y="-2.808783203125" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479990234375" Y="-2.751167236328" />
                  <Point X="26.448369140625" Y="-2.743435546875" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.26098046875" Y="-2.755482910156" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.156365234375" Y="-2.769396972656" />
                  <Point X="26.12898046875" Y="-2.780739746094" />
                  <Point X="26.10459765625" Y="-2.7954609375" />
                  <Point X="25.984046875" Y="-2.895695556641" />
                  <Point X="25.92461328125" Y="-2.945111328125" />
                  <Point X="25.904142578125" Y="-2.968860351563" />
                  <Point X="25.88725" Y="-2.996686523438" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.83958203125" Y="-3.191640869141" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289626953125" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.89229296875" Y="-3.874186035156" />
                  <Point X="26.02206640625" Y="-4.859916015625" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94158203125" Y="-4.752638183594" />
                  <Point X="23.85875390625" Y="-4.731327636719" />
                  <Point X="23.8792265625" Y="-4.575838378906" />
                  <Point X="23.879923828125" Y="-4.568098144531" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497689453125" />
                  <Point X="23.836125" Y="-4.293877441406" />
                  <Point X="23.81613671875" Y="-4.193397949219" />
                  <Point X="23.811875" Y="-4.17846875" />
                  <Point X="23.80097265625" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135462402344" />
                  <Point X="23.778259765625" Y="-4.107625488281" />
                  <Point X="23.769423828125" Y="-4.094859863281" />
                  <Point X="23.749791015625" Y="-4.070937011719" />
                  <Point X="23.738994140625" Y="-4.059779785156" />
                  <Point X="23.582759765625" Y="-3.922764160156" />
                  <Point X="23.505734375" Y="-3.855215087891" />
                  <Point X="23.49326171875" Y="-3.845965332031" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.155826171875" Y="-3.782578613281" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812255859" />
                  <Point X="22.731779296875" Y="-3.931262695313" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.619556640625" Y="-4.010133789063" />
                  <Point X="22.5972421875" Y="-4.032772216797" />
                  <Point X="22.589533203125" Y="-4.041628417969" />
                  <Point X="22.51526953125" Y="-4.138408691406" />
                  <Point X="22.49680078125" Y="-4.162478515625" />
                  <Point X="22.387107421875" Y="-4.094559814453" />
                  <Point X="22.25240625" Y="-4.011156494141" />
                  <Point X="22.019138671875" Y="-3.831548095703" />
                  <Point X="22.159896484375" Y="-3.587747558594" />
                  <Point X="22.658513671875" Y="-2.724119140625" />
                  <Point X="22.66515234375" Y="-2.710080322266" />
                  <Point X="22.676052734375" Y="-2.681115966797" />
                  <Point X="22.680314453125" Y="-2.666190429688" />
                  <Point X="22.6865859375" Y="-2.634664794922" />
                  <Point X="22.688361328125" Y="-2.619240722656" />
                  <Point X="22.689375" Y="-2.588306152344" />
                  <Point X="22.6853359375" Y="-2.557618408203" />
                  <Point X="22.6763515625" Y="-2.528000244141" />
                  <Point X="22.67064453125" Y="-2.513559326172" />
                  <Point X="22.656427734375" Y="-2.48473046875" />
                  <Point X="22.648447265625" Y="-2.471414794922" />
                  <Point X="22.630421875" Y="-2.446258789062" />
                  <Point X="22.620376953125" Y="-2.434418457031" />
                  <Point X="22.603029296875" Y="-2.417069824219" />
                  <Point X="22.591189453125" Y="-2.407024658203" />
                  <Point X="22.566033203125" Y="-2.388996582031" />
                  <Point X="22.552716796875" Y="-2.381013671875" />
                  <Point X="22.523888671875" Y="-2.366795898438" />
                  <Point X="22.5094453125" Y="-2.361088623047" />
                  <Point X="22.479826171875" Y="-2.352103027344" />
                  <Point X="22.449140625" Y="-2.3480625" />
                  <Point X="22.418205078125" Y="-2.349074707031" />
                  <Point X="22.402779296875" Y="-2.350849121094" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.887546875" Y="-2.624743652344" />
                  <Point X="21.2069140625" Y="-3.017707519531" />
                  <Point X="21.102396484375" Y="-2.880395019531" />
                  <Point X="20.995970703125" Y="-2.740573242188" />
                  <Point X="20.818734375" Y="-2.443374023438" />
                  <Point X="21.0794609375" Y="-2.243311035156" />
                  <Point X="21.951876953125" Y="-1.573881835938" />
                  <Point X="21.963513671875" Y="-1.563313720703" />
                  <Point X="21.984888671875" Y="-1.540398803711" />
                  <Point X="21.994626953125" Y="-1.528052001953" />
                  <Point X="22.012591796875" Y="-1.500924682617" />
                  <Point X="22.02016015625" Y="-1.48713671875" />
                  <Point X="22.032916015625" Y="-1.458509399414" />
                  <Point X="22.038103515625" Y="-1.443669799805" />
                  <Point X="22.043265625" Y="-1.423745117188" />
                  <Point X="22.043271484375" Y="-1.423728271484" />
                  <Point X="22.048453125" Y="-1.398775512695" />
                  <Point X="22.05125390625" Y="-1.368353637695" />
                  <Point X="22.051423828125" Y="-1.353027832031" />
                  <Point X="22.049212890625" Y="-1.321360717773" />
                  <Point X="22.046916015625" Y="-1.306206787109" />
                  <Point X="22.0399140625" Y="-1.276465454102" />
                  <Point X="22.02821875" Y="-1.248234741211" />
                  <Point X="22.01213671875" Y="-1.222254272461" />
                  <Point X="22.003044921875" Y="-1.209917358398" />
                  <Point X="21.98221484375" Y="-1.185963623047" />
                  <Point X="21.9712578125" Y="-1.175246459961" />
                  <Point X="21.9477578125" Y="-1.155711791992" />
                  <Point X="21.93521484375" Y="-1.14689440918" />
                  <Point X="21.9174765625" Y="-1.136454711914" />
                  <Point X="21.894568359375" Y="-1.124481811523" />
                  <Point X="21.86530859375" Y="-1.113258789062" />
                  <Point X="21.850212890625" Y="-1.108861938477" />
                  <Point X="21.81832421875" Y="-1.102379516602" />
                  <Point X="21.802705078125" Y="-1.100532836914" />
                  <Point X="21.77137890625" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="21.21817578125" Y="-1.170959106445" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.300859375" Y="-1.137056640625" />
                  <Point X="20.259240234375" Y="-0.974114624023" />
                  <Point X="20.213548828125" Y="-0.654654541016" />
                  <Point X="20.497900390625" Y="-0.578462890625" />
                  <Point X="21.491712890625" Y="-0.312171417236" />
                  <Point X="21.49952734375" Y="-0.309711853027" />
                  <Point X="21.526068359375" Y="-0.299251190186" />
                  <Point X="21.555830078125" Y="-0.283894744873" />
                  <Point X="21.56643359375" Y="-0.277516571045" />
                  <Point X="21.5850234375" Y="-0.264615081787" />
                  <Point X="21.606326171875" Y="-0.248245010376" />
                  <Point X="21.628783203125" Y="-0.226360290527" />
                  <Point X="21.639103515625" Y="-0.21448487854" />
                  <Point X="21.65834765625" Y="-0.188229614258" />
                  <Point X="21.6665625" Y="-0.174816833496" />
                  <Point X="21.680673828125" Y="-0.146811386108" />
                  <Point X="21.686568359375" Y="-0.132218704224" />
                  <Point X="21.692763671875" Y="-0.112254043579" />
                  <Point X="21.699013671875" Y="-0.08849281311" />
                  <Point X="21.70328125" Y="-0.060337249756" />
                  <Point X="21.704353515625" Y="-0.046100517273" />
                  <Point X="21.704353515625" Y="-0.016459615707" />
                  <Point X="21.70328125" Y="-0.002223030806" />
                  <Point X="21.699013671875" Y="0.025932828903" />
                  <Point X="21.695818359375" Y="0.03985225296" />
                  <Point X="21.689623046875" Y="0.059816467285" />
                  <Point X="21.680673828125" Y="0.084251548767" />
                  <Point X="21.6665625" Y="0.112256858826" />
                  <Point X="21.658345703125" Y="0.12566947937" />
                  <Point X="21.6391015625" Y="0.151924743652" />
                  <Point X="21.628783203125" Y="0.163800018311" />
                  <Point X="21.606326171875" Y="0.185684875488" />
                  <Point X="21.5941875" Y="0.195694625854" />
                  <Point X="21.57559765625" Y="0.208596130371" />
                  <Point X="21.55872265625" Y="0.219047302246" />
                  <Point X="21.533990234375" Y="0.232628311157" />
                  <Point X="21.52367578125" Y="0.237509979248" />
                  <Point X="21.502537109375" Y="0.246001541138" />
                  <Point X="21.491712890625" Y="0.249611297607" />
                  <Point X="21.00176171875" Y="0.380893432617" />
                  <Point X="20.2145546875" Y="0.591825012207" />
                  <Point X="20.24184765625" Y="0.776273681641" />
                  <Point X="20.26866796875" Y="0.957520996094" />
                  <Point X="20.3664140625" Y="1.318237182617" />
                  <Point X="20.509544921875" Y="1.299393676758" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815551758" />
                  <Point X="21.2529453125" Y="1.204364379883" />
                  <Point X="21.263298828125" Y="1.204703369141" />
                  <Point X="21.284859375" Y="1.20658984375" />
                  <Point X="21.29511328125" Y="1.208054077148" />
                  <Point X="21.3153984375" Y="1.212089477539" />
                  <Point X="21.3254296875" Y="1.214660644531" />
                  <Point X="21.366572265625" Y="1.2276328125" />
                  <Point X="21.38685546875" Y="1.234028320312" />
                  <Point X="21.396548828125" Y="1.237676391602" />
                  <Point X="21.415482421875" Y="1.246007324219" />
                  <Point X="21.42472265625" Y="1.250689819336" />
                  <Point X="21.443466796875" Y="1.26151184082" />
                  <Point X="21.452140625" Y="1.26717199707" />
                  <Point X="21.468822265625" Y="1.279403564453" />
                  <Point X="21.48407421875" Y="1.29337878418" />
                  <Point X="21.497712890625" Y="1.308931274414" />
                  <Point X="21.504107421875" Y="1.317079711914" />
                  <Point X="21.516521484375" Y="1.334809570312" />
                  <Point X="21.521990234375" Y="1.343603881836" />
                  <Point X="21.531939453125" Y="1.361738891602" />
                  <Point X="21.536419921875" Y="1.371079467773" />
                  <Point X="21.552927734375" Y="1.410934814453" />
                  <Point X="21.56106640625" Y="1.430583618164" />
                  <Point X="21.5645" Y="1.440348754883" />
                  <Point X="21.570287109375" Y="1.460199829102" />
                  <Point X="21.572640625" Y="1.470285888672" />
                  <Point X="21.576400390625" Y="1.491601074219" />
                  <Point X="21.577640625" Y="1.501887573242" />
                  <Point X="21.578994140625" Y="1.522530273438" />
                  <Point X="21.578091796875" Y="1.54319519043" />
                  <Point X="21.5749453125" Y="1.563641479492" />
                  <Point X="21.572814453125" Y="1.573779052734" />
                  <Point X="21.567212890625" Y="1.594685302734" />
                  <Point X="21.563990234375" Y="1.604531738281" />
                  <Point X="21.5564921875" Y="1.623812011719" />
                  <Point X="21.552216796875" Y="1.633245605469" />
                  <Point X="21.532296875" Y="1.671510498047" />
                  <Point X="21.5224765625" Y="1.690375244141" />
                  <Point X="21.51719921875" Y="1.699288330078" />
                  <Point X="21.505705078125" Y="1.716490112305" />
                  <Point X="21.49948828125" Y="1.724778808594" />
                  <Point X="21.48557421875" Y="1.741359130859" />
                  <Point X="21.4784921875" Y="1.748918579102" />
                  <Point X="21.463552734375" Y="1.763218383789" />
                  <Point X="21.4556953125" Y="1.769958862305" />
                  <Point X="21.174658203125" Y="1.985606811523" />
                  <Point X="20.77238671875" Y="2.294281005859" />
                  <Point X="20.8934921875" Y="2.501765625" />
                  <Point X="20.99771484375" Y="2.680321044922" />
                  <Point X="21.26528515625" Y="3.024246826172" />
                  <Point X="21.273662109375" Y="3.035013427734" />
                  <Point X="21.308314453125" Y="3.015006591797" />
                  <Point X="21.74584375" Y="2.7623984375" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.902228515625" Y="2.726144775391" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310546875" />
                  <Point X="22.023568359375" Y="2.734227294922" />
                  <Point X="22.04301171875" Y="2.7413046875" />
                  <Point X="22.0615625" Y="2.750454345703" />
                  <Point X="22.070583984375" Y="2.755533691406" />
                  <Point X="22.088837890625" Y="2.767163330078" />
                  <Point X="22.09725390625" Y="2.773194091797" />
                  <Point X="22.1133828125" Y="2.786138427734" />
                  <Point X="22.121095703125" Y="2.793052001953" />
                  <Point X="22.16176953125" Y="2.833724121094" />
                  <Point X="22.1818203125" Y="2.853775390625" />
                  <Point X="22.188736328125" Y="2.861489990234" />
                  <Point X="22.20168359375" Y="2.877622314453" />
                  <Point X="22.20771484375" Y="2.886040039062" />
                  <Point X="22.21934375" Y="2.904293457031" />
                  <Point X="22.224423828125" Y="2.913317138672" />
                  <Point X="22.233572265625" Y="2.931866699219" />
                  <Point X="22.240646484375" Y="2.951298095703" />
                  <Point X="22.245564453125" Y="2.971387695312" />
                  <Point X="22.2474765625" Y="2.981571777344" />
                  <Point X="22.250302734375" Y="3.003029785156" />
                  <Point X="22.251091796875" Y="3.01336328125" />
                  <Point X="22.25154296875" Y="3.034049072266" />
                  <Point X="22.251205078125" Y="3.044401367188" />
                  <Point X="22.24619140625" Y="3.101701660156" />
                  <Point X="22.243720703125" Y="3.129950927734" />
                  <Point X="22.242255859375" Y="3.140209228516" />
                  <Point X="22.23821875" Y="3.160499023438" />
                  <Point X="22.235646484375" Y="3.170530517578" />
                  <Point X="22.22913671875" Y="3.191174072266" />
                  <Point X="22.22548828125" Y="3.200867675781" />
                  <Point X="22.217158203125" Y="3.219797363281" />
                  <Point X="22.2124765625" Y="3.229033447266" />
                  <Point X="22.087939453125" Y="3.444736816406" />
                  <Point X="21.94061328125" Y="3.699914550781" />
                  <Point X="22.170125" Y="3.87587890625" />
                  <Point X="22.351634765625" Y="4.015041748047" />
                  <Point X="22.773048828125" Y="4.249169921875" />
                  <Point X="22.8074765625" Y="4.268296386719" />
                  <Point X="22.881435546875" Y="4.171909667969" />
                  <Point X="22.888177734375" Y="4.164050292969" />
                  <Point X="22.90248046875" Y="4.149108398438" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93491015625" Y="4.121895996094" />
                  <Point X="22.952111328125" Y="4.110403320312" />
                  <Point X="22.9610234375" Y="4.105128417969" />
                  <Point X="23.024798828125" Y="4.071929199219" />
                  <Point X="23.056240234375" Y="4.055561523438" />
                  <Point X="23.065673828125" Y="4.051286865234" />
                  <Point X="23.084951171875" Y="4.043790283203" />
                  <Point X="23.094794921875" Y="4.040568359375" />
                  <Point X="23.115701171875" Y="4.034966308594" />
                  <Point X="23.1258359375" Y="4.032834960938" />
                  <Point X="23.146279296875" Y="4.029687988281" />
                  <Point X="23.1669453125" Y="4.028785400391" />
                  <Point X="23.187583984375" Y="4.030137939453" />
                  <Point X="23.197865234375" Y="4.031377685547" />
                  <Point X="23.219181640625" Y="4.035135986328" />
                  <Point X="23.229271484375" Y="4.037488037109" />
                  <Point X="23.2491328125" Y="4.043277099609" />
                  <Point X="23.258904296875" Y="4.046713623047" />
                  <Point X="23.325330078125" Y="4.074228759766" />
                  <Point X="23.358078125" Y="4.087793212891" />
                  <Point X="23.36741796875" Y="4.092273681641" />
                  <Point X="23.385552734375" Y="4.102223632812" />
                  <Point X="23.39434765625" Y="4.107692871094" />
                  <Point X="23.412076171875" Y="4.120106933594" />
                  <Point X="23.420220703125" Y="4.126498535156" />
                  <Point X="23.435771484375" Y="4.140135253906" />
                  <Point X="23.44974609375" Y="4.155384277344" />
                  <Point X="23.4619765625" Y="4.172062988281" />
                  <Point X="23.467638671875" Y="4.180737792969" />
                  <Point X="23.4784609375" Y="4.199480957031" />
                  <Point X="23.48314453125" Y="4.208725585937" />
                  <Point X="23.4914765625" Y="4.227663085938" />
                  <Point X="23.495125" Y="4.237355957031" />
                  <Point X="23.516744140625" Y="4.305926757812" />
                  <Point X="23.527404296875" Y="4.339732421875" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370048828125" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401866210938" />
                  <Point X="23.53769921875" Y="4.41221875" />
                  <Point X="23.537248046875" Y="4.432898925781" />
                  <Point X="23.536458984375" Y="4.4432265625" />
                  <Point X="23.52230078125" Y="4.550770507812" />
                  <Point X="23.520736328125" Y="4.562655273438" />
                  <Point X="23.833859375" Y="4.650443847656" />
                  <Point X="24.068830078125" Y="4.716320800781" />
                  <Point X="24.579708984375" Y="4.776111816406" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.6448984375" Y="4.74478125" />
                  <Point X="24.77433203125" Y="4.261728515625" />
                  <Point X="24.779564453125" Y="4.247106933594" />
                  <Point X="24.79233984375" Y="4.218912597656" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.247104980469" />
                  <Point X="25.23798046875" Y="4.2617265625" />
                  <Point X="25.30179296875" Y="4.499872558594" />
                  <Point X="25.378193359375" Y="4.785005859375" />
                  <Point X="25.62270703125" Y="4.759398925781" />
                  <Point X="25.827876953125" Y="4.737912109375" />
                  <Point X="26.2505546875" Y="4.635864746094" />
                  <Point X="26.453595703125" Y="4.58684375" />
                  <Point X="26.7272578125" Y="4.487585449219" />
                  <Point X="26.8582578125" Y="4.440070800781" />
                  <Point X="27.12429296875" Y="4.315655273438" />
                  <Point X="27.25045703125" Y="4.256651367187" />
                  <Point X="27.50750390625" Y="4.106895996094" />
                  <Point X="27.6294296875" Y="4.035861572266" />
                  <Point X="27.81778125" Y="3.901915527344" />
                  <Point X="27.642421875" Y="3.598183837891" />
                  <Point X="27.0653125" Y="2.598598388672" />
                  <Point X="27.06237890625" Y="2.593110107422" />
                  <Point X="27.0531875" Y="2.573448974609" />
                  <Point X="27.044185546875" Y="2.549571777344" />
                  <Point X="27.041302734375" Y="2.540601318359" />
                  <Point X="27.026921875" Y="2.486826171875" />
                  <Point X="27.01983203125" Y="2.460314941406" />
                  <Point X="27.0179140625" Y="2.451470214844" />
                  <Point X="27.013646484375" Y="2.420223632812" />
                  <Point X="27.012755859375" Y="2.383240722656" />
                  <Point X="27.013412109375" Y="2.369580078125" />
                  <Point X="27.01901953125" Y="2.323080078125" />
                  <Point X="27.021783203125" Y="2.300155273438" />
                  <Point X="27.02380078125" Y="2.289033447266" />
                  <Point X="27.02914453125" Y="2.267108154297" />
                  <Point X="27.032470703125" Y="2.2563046875" />
                  <Point X="27.04073828125" Y="2.234214111328" />
                  <Point X="27.0453203125" Y="2.223887207031" />
                  <Point X="27.055681640625" Y="2.203843994141" />
                  <Point X="27.0614609375" Y="2.194127685547" />
                  <Point X="27.090234375" Y="2.151724121094" />
                  <Point X="27.104419921875" Y="2.130819091797" />
                  <Point X="27.109814453125" Y="2.123628417969" />
                  <Point X="27.13046875" Y="2.100120361328" />
                  <Point X="27.1576015625" Y="2.075385986328" />
                  <Point X="27.168259765625" Y="2.066982177734" />
                  <Point X="27.210662109375" Y="2.038209350586" />
                  <Point X="27.231568359375" Y="2.024024536133" />
                  <Point X="27.241279296875" Y="2.018246704102" />
                  <Point X="27.26132421875" Y="2.007883056641" />
                  <Point X="27.271658203125" Y="2.003297607422" />
                  <Point X="27.293744140625" Y="1.995032470703" />
                  <Point X="27.304548828125" Y="1.991707519531" />
                  <Point X="27.32647265625" Y="1.986364868164" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.384091796875" Y="1.978739624023" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.41604296875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975497070312" />
                  <Point X="27.484314453125" Y="1.979822387695" />
                  <Point X="27.49775" Y="1.982395751953" />
                  <Point X="27.551525390625" Y="1.996776123047" />
                  <Point X="27.578037109375" Y="2.003865478516" />
                  <Point X="27.584" Y="2.005671875" />
                  <Point X="27.60440234375" Y="2.01306640625" />
                  <Point X="27.627654296875" Y="2.023573486328" />
                  <Point X="27.63603515625" Y="2.027872680664" />
                  <Point X="28.12883203125" Y="2.312389404297" />
                  <Point X="28.940404296875" Y="2.780950683594" />
                  <Point X="28.971625" Y="2.737561035156" />
                  <Point X="29.043958984375" Y="2.637031494141" />
                  <Point X="29.136884765625" Y="2.483470703125" />
                  <Point X="28.9327734375" Y="2.326850830078" />
                  <Point X="28.172953125" Y="1.743820068359" />
                  <Point X="28.168138671875" Y="1.739868530273" />
                  <Point X="28.15212890625" Y="1.725224731445" />
                  <Point X="28.134671875" Y="1.706606811523" />
                  <Point X="28.12857421875" Y="1.699420776367" />
                  <Point X="28.089873046875" Y="1.648931030273" />
                  <Point X="28.07079296875" Y="1.624039550781" />
                  <Point X="28.0656328125" Y="1.61659777832" />
                  <Point X="28.04973828125" Y="1.589366333008" />
                  <Point X="28.034765625" Y="1.555545898438" />
                  <Point X="28.03014453125" Y="1.542677001953" />
                  <Point X="28.0157265625" Y="1.491126708984" />
                  <Point X="28.008619140625" Y="1.465712402344" />
                  <Point X="28.006228515625" Y="1.454669555664" />
                  <Point X="28.0027734375" Y="1.432369995117" />
                  <Point X="28.001708984375" Y="1.421113037109" />
                  <Point X="28.000892578125" Y="1.397543579102" />
                  <Point X="28.001173828125" Y="1.386239013672" />
                  <Point X="28.003078125" Y="1.363750732422" />
                  <Point X="28.004701171875" Y="1.352567138672" />
                  <Point X="28.016537109375" Y="1.2952109375" />
                  <Point X="28.02237109375" Y="1.266934448242" />
                  <Point X="28.02459765625" Y="1.258238769531" />
                  <Point X="28.03468359375" Y="1.228608032227" />
                  <Point X="28.050287109375" Y="1.195370605469" />
                  <Point X="28.056919921875" Y="1.183525878906" />
                  <Point X="28.089109375" Y="1.134600708008" />
                  <Point X="28.1049765625" Y="1.11048046875" />
                  <Point X="28.111736328125" Y="1.101427856445" />
                  <Point X="28.12628515625" Y="1.08418762207" />
                  <Point X="28.13407421875" Y="1.075999633789" />
                  <Point X="28.151318359375" Y="1.059908081055" />
                  <Point X="28.16003125" Y="1.05269934082" />
                  <Point X="28.17824609375" Y="1.039369873047" />
                  <Point X="28.187748046875" Y="1.033249633789" />
                  <Point X="28.23439453125" Y="1.006991943359" />
                  <Point X="28.257390625" Y="0.994047058105" />
                  <Point X="28.265478515625" Y="0.989988769531" />
                  <Point X="28.2946796875" Y="0.978083618164" />
                  <Point X="28.33027734375" Y="0.968020935059" />
                  <Point X="28.343671875" Y="0.965257629395" />
                  <Point X="28.406740234375" Y="0.956922241211" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199768066" />
                  <Point X="28.46571875" Y="0.951222839355" />
                  <Point X="28.49121875" Y="0.952032409668" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="28.968728515625" Y="1.014427124023" />
                  <Point X="29.704703125" Y="1.111319702148" />
                  <Point X="29.721619140625" Y="1.041832885742" />
                  <Point X="29.7526875" Y="0.914210144043" />
                  <Point X="29.78387109375" Y="0.713921081543" />
                  <Point X="29.562763671875" Y="0.654675048828" />
                  <Point X="28.6919921875" Y="0.421352752686" />
                  <Point X="28.68603125" Y="0.419544311523" />
                  <Point X="28.665619140625" Y="0.412135162354" />
                  <Point X="28.64237890625" Y="0.401618011475" />
                  <Point X="28.634005859375" Y="0.397316711426" />
                  <Point X="28.57204296875" Y="0.361501281738" />
                  <Point X="28.54149609375" Y="0.343844299316" />
                  <Point X="28.533884765625" Y="0.338947021484" />
                  <Point X="28.508775390625" Y="0.319871551514" />
                  <Point X="28.481994140625" Y="0.294351379395" />
                  <Point X="28.472796875" Y="0.284226898193" />
                  <Point X="28.435619140625" Y="0.236854248047" />
                  <Point X="28.417291015625" Y="0.213499359131" />
                  <Point X="28.410853515625" Y="0.20420715332" />
                  <Point X="28.39912890625" Y="0.184925720215" />
                  <Point X="28.393841796875" Y="0.174936325073" />
                  <Point X="28.384068359375" Y="0.15347265625" />
                  <Point X="28.380005859375" Y="0.14292666626" />
                  <Point X="28.37316015625" Y="0.121426742554" />
                  <Point X="28.370376953125" Y="0.110472938538" />
                  <Point X="28.357984375" Y="0.045764026642" />
                  <Point X="28.351875" Y="0.01386227417" />
                  <Point X="28.350603515625" Y="0.004966154575" />
                  <Point X="28.3485859375" Y="-0.026262037277" />
                  <Point X="28.35028125" Y="-0.062939785004" />
                  <Point X="28.351875" Y="-0.076422409058" />
                  <Point X="28.364267578125" Y="-0.141131317139" />
                  <Point X="28.370376953125" Y="-0.173033065796" />
                  <Point X="28.37316015625" Y="-0.183986877441" />
                  <Point X="28.380005859375" Y="-0.205486801147" />
                  <Point X="28.384068359375" Y="-0.216032791138" />
                  <Point X="28.393841796875" Y="-0.237496459961" />
                  <Point X="28.39912890625" Y="-0.247485839844" />
                  <Point X="28.410853515625" Y="-0.266767272949" />
                  <Point X="28.417291015625" Y="-0.27605947876" />
                  <Point X="28.45446875" Y="-0.323432128906" />
                  <Point X="28.472796875" Y="-0.346787322998" />
                  <Point X="28.478720703125" Y="-0.353634979248" />
                  <Point X="28.501140625" Y="-0.375806091309" />
                  <Point X="28.530177734375" Y="-0.398725372314" />
                  <Point X="28.54149609375" Y="-0.406404418945" />
                  <Point X="28.603458984375" Y="-0.442219696045" />
                  <Point X="28.634005859375" Y="-0.459876708984" />
                  <Point X="28.639494140625" Y="-0.462813201904" />
                  <Point X="28.659154296875" Y="-0.472014434814" />
                  <Point X="28.68302734375" Y="-0.48102722168" />
                  <Point X="28.6919921875" Y="-0.483912872314" />
                  <Point X="29.12128515625" Y="-0.598941345215" />
                  <Point X="29.78487890625" Y="-0.776751098633" />
                  <Point X="29.7789609375" Y="-0.816008911133" />
                  <Point X="29.761619140625" Y="-0.931037414551" />
                  <Point X="29.7278046875" Y="-1.079219604492" />
                  <Point X="29.4466328125" Y="-1.042202758789" />
                  <Point X="28.436783203125" Y="-0.909253540039" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042419434" />
                  <Point X="28.36672265625" Y="-0.910840820312" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.23287109375" Y="-0.939108642578" />
                  <Point X="28.17291796875" Y="-0.952140014648" />
                  <Point X="28.157875" Y="-0.956742553711" />
                  <Point X="28.1287578125" Y="-0.968365905762" />
                  <Point X="28.11468359375" Y="-0.97538659668" />
                  <Point X="28.08685546875" Y="-0.992278747559" />
                  <Point X="28.074130859375" Y="-1.001525939941" />
                  <Point X="28.050376953125" Y="-1.02199798584" />
                  <Point X="28.03934765625" Y="-1.033222900391" />
                  <Point X="27.965841796875" Y="-1.121627441406" />
                  <Point X="27.929603515625" Y="-1.165211181641" />
                  <Point X="27.9213203125" Y="-1.176854248047" />
                  <Point X="27.906599609375" Y="-1.201241210938" />
                  <Point X="27.900162109375" Y="-1.213984985352" />
                  <Point X="27.8888203125" Y="-1.241370483398" />
                  <Point X="27.88436328125" Y="-1.254935058594" />
                  <Point X="27.877533203125" Y="-1.282580200195" />
                  <Point X="27.87516015625" Y="-1.296660888672" />
                  <Point X="27.864625" Y="-1.411148681641" />
                  <Point X="27.859431640625" Y="-1.467591064453" />
                  <Point X="27.859291015625" Y="-1.483321044922" />
                  <Point X="27.861609375" Y="-1.514587036133" />
                  <Point X="27.864068359375" Y="-1.530122924805" />
                  <Point X="27.871798828125" Y="-1.561743286133" />
                  <Point X="27.87678515625" Y="-1.576661743164" />
                  <Point X="27.88915625" Y="-1.605475708008" />
                  <Point X="27.896541015625" Y="-1.619371459961" />
                  <Point X="27.963841796875" Y="-1.724053466797" />
                  <Point X="27.997021484375" Y="-1.775661987305" />
                  <Point X="28.00174609375" Y="-1.782357055664" />
                  <Point X="28.01980078125" Y="-1.804454833984" />
                  <Point X="28.043494140625" Y="-1.828121948242" />
                  <Point X="28.052798828125" Y="-1.836277709961" />
                  <Point X="28.45118359375" Y="-2.141969482422" />
                  <Point X="29.087171875" Y="-2.629981201172" />
                  <Point X="29.045501953125" Y="-2.697408203125" />
                  <Point X="29.0012734375" Y="-2.760250732422" />
                  <Point X="28.74786328125" Y="-2.613943603516" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.626376953125" Y="-2.040198608398" />
                  <Point X="27.555021484375" Y="-2.027312011719" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403442383" />
                  <Point X="27.492314453125" Y="-2.02650390625" />
                  <Point X="27.460140625" Y="-2.031461791992" />
                  <Point X="27.444845703125" Y="-2.03513659668" />
                  <Point X="27.4150703125" Y="-2.044960083008" />
                  <Point X="27.40058984375" Y="-2.051108886719" />
                  <Point X="27.280349609375" Y="-2.114390136719" />
                  <Point X="27.221072265625" Y="-2.145587890625" />
                  <Point X="27.20896875" Y="-2.153170898438" />
                  <Point X="27.1860390625" Y="-2.170063476562" />
                  <Point X="27.175212890625" Y="-2.179373046875" />
                  <Point X="27.15425390625" Y="-2.200331787109" />
                  <Point X="27.144943359375" Y="-2.211157958984" />
                  <Point X="27.128048828125" Y="-2.234089111328" />
                  <Point X="27.12046484375" Y="-2.246194091797" />
                  <Point X="27.05718359375" Y="-2.36643359375" />
                  <Point X="27.025984375" Y="-2.425712158203" />
                  <Point X="27.0198359375" Y="-2.440192871094" />
                  <Point X="27.01001171875" Y="-2.469971191406" />
                  <Point X="27.0063359375" Y="-2.485268798828" />
                  <Point X="27.00137890625" Y="-2.517442382812" />
                  <Point X="27.000279296875" Y="-2.533133544922" />
                  <Point X="27.00068359375" Y="-2.564484375" />
                  <Point X="27.0021875" Y="-2.580144042969" />
                  <Point X="27.028328125" Y="-2.724879394531" />
                  <Point X="27.04121484375" Y="-2.796234375" />
                  <Point X="27.04301953125" Y="-2.804230957031" />
                  <Point X="27.051236328125" Y="-2.831534912109" />
                  <Point X="27.064068359375" Y="-2.862473876953" />
                  <Point X="27.069546875" Y="-2.873578613281" />
                  <Point X="27.325548828125" Y="-3.31698828125" />
                  <Point X="27.735892578125" Y="-4.02772265625" />
                  <Point X="27.723755859375" Y="-4.036082275391" />
                  <Point X="27.520248046875" Y="-3.770865722656" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.828654296875" Y="-2.870144042969" />
                  <Point X="26.808830078125" Y="-2.849624023438" />
                  <Point X="26.783251953125" Y="-2.828003662109" />
                  <Point X="26.77330078125" Y="-2.820646728516" />
                  <Point X="26.63055078125" Y="-2.728872802734" />
                  <Point X="26.56017578125" Y="-2.683627929688" />
                  <Point X="26.546283203125" Y="-2.676245605469" />
                  <Point X="26.51747265625" Y="-2.663874511719" />
                  <Point X="26.5025546875" Y="-2.658885742188" />
                  <Point X="26.47093359375" Y="-2.651154052734" />
                  <Point X="26.45539453125" Y="-2.648695800781" />
                  <Point X="26.424125" Y="-2.646376953125" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.252275390625" Y="-2.660882568359" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670339111328" />
                  <Point X="26.133578125" Y="-2.677170410156" />
                  <Point X="26.12001171875" Y="-2.681627929688" />
                  <Point X="26.092626953125" Y="-2.692970703125" />
                  <Point X="26.07987890625" Y="-2.699412841797" />
                  <Point X="26.05549609375" Y="-2.714134033203" />
                  <Point X="26.043861328125" Y="-2.722413085938" />
                  <Point X="25.923310546875" Y="-2.822647705078" />
                  <Point X="25.863876953125" Y="-2.872063476562" />
                  <Point X="25.85265625" Y="-2.883086669922" />
                  <Point X="25.832185546875" Y="-2.906835693359" />
                  <Point X="25.822935546875" Y="-2.919561523438" />
                  <Point X="25.80604296875" Y="-2.947387695312" />
                  <Point X="25.79901953125" Y="-2.961466552734" />
                  <Point X="25.78739453125" Y="-2.990588623047" />
                  <Point X="25.78279296875" Y="-3.005631835938" />
                  <Point X="25.74675" Y="-3.171464111328" />
                  <Point X="25.728978515625" Y="-3.253219726562" />
                  <Point X="25.7275859375" Y="-3.261286621094" />
                  <Point X="25.724724609375" Y="-3.289676757812" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520263672" />
                  <Point X="25.79810546875" Y="-3.886586181641" />
                  <Point X="25.833091796875" Y="-4.152329589844" />
                  <Point X="25.655068359375" Y="-3.487936767578" />
                  <Point X="25.652607421875" Y="-3.480121582031" />
                  <Point X="25.642146484375" Y="-3.453578613281" />
                  <Point X="25.6267890625" Y="-3.423815429688" />
                  <Point X="25.62041015625" Y="-3.413209716797" />
                  <Point X="25.51074609375" Y="-3.255205566406" />
                  <Point X="25.456681640625" Y="-3.177309326172" />
                  <Point X="25.446671875" Y="-3.165172119141" />
                  <Point X="25.424787109375" Y="-3.142716308594" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104937255859" />
                  <Point X="25.3452421875" Y="-3.090829589844" />
                  <Point X="25.33065625" Y="-3.084938476562" />
                  <Point X="25.160958984375" Y="-3.032270507812" />
                  <Point X="25.077296875" Y="-3.006305175781" />
                  <Point X="25.063376953125" Y="-3.003109130859" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.765318359375" Y="-3.058972900391" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.6670703125" Y="-3.090829589844" />
                  <Point X="24.6390703125" Y="-3.104937255859" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.587525390625" Y="-3.142717041016" />
                  <Point X="24.565640625" Y="-3.165173339844" />
                  <Point X="24.555630859375" Y="-3.177310302734" />
                  <Point X="24.44596875" Y="-3.335314208984" />
                  <Point X="24.391904296875" Y="-3.413210693359" />
                  <Point X="24.387529296875" Y="-3.420132568359" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.224970703125" Y="-3.981594970703" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.665119360535" Y="-3.959665700048" />
                  <Point X="27.689205675514" Y="-3.9468587792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.606482861695" Y="-3.883249124705" />
                  <Point X="27.641676627576" Y="-3.864536267451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.547846362855" Y="-3.806832549363" />
                  <Point X="27.594147579638" Y="-3.782213755702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.489209865801" Y="-3.73041597307" />
                  <Point X="27.5466185317" Y="-3.699891243953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.430573370334" Y="-3.653999395934" />
                  <Point X="27.499089483762" Y="-3.617568732204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.371936874868" Y="-3.577582818798" />
                  <Point X="27.451560435824" Y="-3.535246220455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.957090596825" Y="-2.7347416342" />
                  <Point X="29.054408076363" Y="-2.682997012464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.313300379401" Y="-3.501166241662" />
                  <Point X="27.404031387886" Y="-3.452923708706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.860076987838" Y="-2.678730530282" />
                  <Point X="29.031746870643" Y="-2.587452034463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.254663883935" Y="-3.424749664525" />
                  <Point X="27.356502339948" Y="-3.370601196957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.763063378852" Y="-2.622719426364" />
                  <Point X="28.948920818987" Y="-2.5238972725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.814490666191" Y="-4.08290919281" />
                  <Point X="25.823333285221" Y="-4.078207488872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.196027388468" Y="-3.348333087389" />
                  <Point X="27.308973361558" Y="-3.288278648229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.666049513295" Y="-2.566708458867" />
                  <Point X="28.866094767331" Y="-2.460342510538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.789256106779" Y="-3.988732491238" />
                  <Point X="25.810094749011" Y="-3.97765238862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.137390893002" Y="-3.271916510253" />
                  <Point X="27.261444513043" Y="-3.205956030445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.56903560007" Y="-2.510697516716" />
                  <Point X="28.783268715675" Y="-2.396787748575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.764021547367" Y="-3.894555789666" />
                  <Point X="25.796856205604" Y="-3.877097292196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.078754397535" Y="-3.195499933116" />
                  <Point X="27.213915664528" Y="-3.123633412661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.472021686845" Y="-2.454686574565" />
                  <Point X="28.700442664019" Y="-2.333232986612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.946172347905" Y="-4.753529199542" />
                  <Point X="24.030085083133" Y="-4.708912006785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.738786987955" Y="-3.800379088093" />
                  <Point X="25.783617593123" Y="-3.776542232499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.020117902068" Y="-3.11908335598" />
                  <Point X="27.166386816012" Y="-3.041310794877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.37500777362" Y="-2.398675632413" />
                  <Point X="28.617616612363" Y="-2.26967822465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.864262887383" Y="-4.689487077429" />
                  <Point X="24.063704993367" Y="-4.583441828607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.713552428543" Y="-3.706202386521" />
                  <Point X="25.770378980641" Y="-3.675987172802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.961481406602" Y="-3.042666778844" />
                  <Point X="27.118857967497" Y="-2.958988177093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.277993860395" Y="-2.342664690262" />
                  <Point X="28.534790560707" Y="-2.206123462687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.879406500047" Y="-4.573840920931" />
                  <Point X="24.097324903601" Y="-4.457971650428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.688317869131" Y="-3.612025684949" />
                  <Point X="25.75714036816" Y="-3.575432113105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.902844911135" Y="-2.966250201707" />
                  <Point X="27.071329118982" Y="-2.876665559309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.18097994717" Y="-2.286653748111" />
                  <Point X="28.451964509051" Y="-2.142568700724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.871271974805" Y="-4.470571969908" />
                  <Point X="24.130944813835" Y="-4.33250147225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.663083309719" Y="-3.517848983376" />
                  <Point X="25.743901755679" Y="-3.474877053408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.844208415669" Y="-2.889833624571" />
                  <Point X="27.039377426307" Y="-2.786060420847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.083966033945" Y="-2.23064280596" />
                  <Point X="28.369138445121" Y="-2.079013945287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.851917073918" Y="-4.373268998444" />
                  <Point X="24.164564724069" Y="-4.207031294071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.629112136109" Y="-3.428317621975" />
                  <Point X="25.730663143197" Y="-3.374321993711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.772700580919" Y="-2.82026085993" />
                  <Point X="27.021648045351" Y="-2.687893145103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.98695212072" Y="-2.174631863808" />
                  <Point X="28.286312381075" Y="-2.015459189913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.832561926922" Y="-4.275966157837" />
                  <Point X="24.198184634304" Y="-4.081561115893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.57586828752" Y="-3.349033723632" />
                  <Point X="25.726832245508" Y="-3.268764763328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.68110061288" Y="-2.76137127206" />
                  <Point X="27.003918123801" Y="-2.589726156798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.889938207495" Y="-2.118620921657" />
                  <Point X="28.203486317029" Y="-1.951904434538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.812098725222" Y="-4.179252480368" />
                  <Point X="24.231804479306" Y="-3.956090972399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.521321490958" Y="-3.270442615015" />
                  <Point X="25.751882138667" Y="-3.147851344058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.589501046986" Y="-2.702481470367" />
                  <Point X="27.007554214826" Y="-2.480198658091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.78119484459" Y="-2.068846638528" />
                  <Point X="28.120660252982" Y="-1.888349679163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.659359680642" Y="-1.070208680985" />
                  <Point X="29.739596329713" Y="-1.027546097909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.769131926807" Y="-4.094504177518" />
                  <Point X="24.265424068622" Y="-3.830620964855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.466774732654" Y="-3.191851486056" />
                  <Point X="25.778322914403" Y="-3.026198379403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.479663975066" Y="-2.653288722638" />
                  <Point X="27.071958454271" Y="-2.338360161723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.631110862183" Y="-2.0410535527" />
                  <Point X="28.039354282414" Y="-1.823986675749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.497164312913" Y="-1.048855332963" />
                  <Point X="29.765355256598" Y="-0.906255678719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.698641096622" Y="-4.024390661957" />
                  <Point X="24.299043657938" Y="-3.705150957312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.397491905593" Y="-3.121095663842" />
                  <Point X="25.86154527798" Y="-2.874354108949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.265276988295" Y="-2.659686150714" />
                  <Point X="27.185902577314" Y="-2.170180842003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.431771419298" Y="-2.039450059769" />
                  <Point X="27.979408476257" Y="-1.748266271457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.33496902459" Y="-1.02750194272" />
                  <Point X="29.782989426792" Y="-0.789285269292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.622262610082" Y="-3.957407668811" />
                  <Point X="24.332663247254" Y="-3.579680949768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.288023617228" Y="-3.071706830417" />
                  <Point X="27.927857516088" Y="-1.668082248374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.1727737722" Y="-1.00614853337" />
                  <Point X="29.664746923633" Y="-0.74456176863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.545883699469" Y="-3.890424901148" />
                  <Point X="24.37146264044" Y="-3.451456791653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.160250766252" Y="-3.032050705576" />
                  <Point X="27.880623883129" Y="-1.585602661694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.010578519811" Y="-0.984795124021" />
                  <Point X="29.530196902131" Y="-0.708509129277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.462352615523" Y="-3.827245011504" />
                  <Point X="24.480141714554" Y="-3.286076948107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.022201049262" Y="-2.997858887322" />
                  <Point X="27.859721202748" Y="-1.489122659184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.848383267421" Y="-0.963441714672" />
                  <Point X="29.39564688063" Y="-0.672456489924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.323351913477" Y="-3.793558840974" />
                  <Point X="27.867743142288" Y="-1.377263163456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.686188015032" Y="-0.942088305323" />
                  <Point X="29.261096859128" Y="-0.636403850571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.464626397359" Y="-4.142557142287" />
                  <Point X="22.544795541218" Y="-4.099930452369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.143203507977" Y="-3.781751292462" />
                  <Point X="27.882686057602" Y="-1.261723719631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.523992762642" Y="-0.920734895973" />
                  <Point X="29.126546837627" Y="-0.600351211218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.371137712831" Y="-4.084671802789" />
                  <Point X="27.983297771747" Y="-1.100633367469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.32456357238" Y="-0.919179122569" />
                  <Point X="28.991996645165" Y="-0.564298662767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.277649309783" Y="-4.026786313625" />
                  <Point X="28.857446445745" Y="-0.528246118014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.192066871762" Y="-3.964697148289" />
                  <Point X="28.722896246326" Y="-0.492193573262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.109408746791" Y="-3.901053098125" />
                  <Point X="28.60876260829" Y="-0.44528535026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.026750621819" Y="-3.83740904796" />
                  <Point X="28.515605762412" Y="-0.387223569022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.100519989736" Y="-3.690591024455" />
                  <Point X="28.448214030365" Y="-0.315462233752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.190156080732" Y="-3.53533651464" />
                  <Point X="28.393578528609" Y="-0.236918290523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.27979265889" Y="-3.380081745796" />
                  <Point X="28.364924176919" Y="-0.14455992476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.369429237048" Y="-3.224826976952" />
                  <Point X="28.349460722422" Y="-0.045187834547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.770295747628" Y="0.71028354919" />
                  <Point X="29.783356239833" Y="0.717227936078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.459065815207" Y="-3.069572208108" />
                  <Point X="28.362499291328" Y="0.069339050332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.362370152827" Y="0.600979817834" />
                  <Point X="29.767885324232" Y="0.816596059152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.548702393365" Y="-2.914317439264" />
                  <Point X="28.409448477165" Y="0.201896530066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.954446456215" Y="0.491677095763" />
                  <Point X="29.752278137396" Y="0.915891725525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.638338971523" Y="-2.75906267042" />
                  <Point X="29.729087311818" Y="1.011155099652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.687670985269" Y="-2.625238218613" />
                  <Point X="29.705896312724" Y="1.10641838152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.674935030925" Y="-2.524415890844" />
                  <Point X="29.449593698511" Y="1.077734018999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.627049080383" Y="-2.442283147575" />
                  <Point X="29.180646881999" Y="1.04232661486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.546080540016" Y="-2.377740729341" />
                  <Point X="28.911699908243" Y="1.006919127112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.18767059846" Y="-2.992425952513" />
                  <Point X="21.985016959558" Y="-2.568469372017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.389233968201" Y="-2.353543376083" />
                  <Point X="28.642752350167" Y="0.971511328677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.129369266699" Y="-2.915831165674" />
                  <Point X="28.411721095164" Y="0.956263986199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.071068306471" Y="-2.839236181287" />
                  <Point X="28.269618575146" Y="0.988300890857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.012767666119" Y="-2.762641026818" />
                  <Point X="28.171960730365" Y="1.043969448527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.96128964762" Y="-2.682418219961" />
                  <Point X="28.102312118085" Y="1.114530779291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.912572814012" Y="-2.600727265056" />
                  <Point X="28.050678852348" Y="1.194671039727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.863855980404" Y="-2.519036310151" />
                  <Point X="28.018600713015" Y="1.285208945309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.852434009077" Y="-2.417515325218" />
                  <Point X="28.001392770326" Y="1.383653474697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.309078806816" Y="-2.067118824526" />
                  <Point X="28.018270277809" Y="1.500221559424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.765724371957" Y="-1.716721915799" />
                  <Point X="28.08593519449" Y="1.643793788631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.027923403013" Y="-1.469714063198" />
                  <Point X="28.401442424502" Y="1.919146113402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.051194719894" Y="-1.34974632971" />
                  <Point X="28.858089536943" Y="2.269543844843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.030301198778" Y="-1.253261457132" />
                  <Point X="29.117699263099" Y="2.515174939606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.972459790522" Y="-1.176422124627" />
                  <Point X="29.068439491445" Y="2.596577209232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.879012114834" Y="-1.118514980342" />
                  <Point X="29.015791835019" Y="2.676178108571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.696443722365" Y="-1.107994161725" />
                  <Point X="27.51962204641" Y="1.988244675416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.123967287753" Y="2.309580740218" />
                  <Point X="28.959796985886" Y="2.753999273978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.427496931168" Y="-1.143401552404" />
                  <Point X="27.317735744799" Y="1.988493979542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.158550032855" Y="-1.178809000038" />
                  <Point X="27.209879873198" Y="2.038740150167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.889602758501" Y="-1.214216647616" />
                  <Point X="27.128079172923" Y="2.102840101129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.620655484147" Y="-1.249624295194" />
                  <Point X="27.070992619078" Y="2.180080796845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.351708209793" Y="-1.285031942773" />
                  <Point X="27.029590125694" Y="2.265660855433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.316021055802" Y="-1.196412984324" />
                  <Point X="27.013970462682" Y="2.364949888106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.291824484324" Y="-1.101684374777" />
                  <Point X="27.024616193239" Y="2.478204478266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.267628533307" Y="-1.006955435325" />
                  <Point X="27.072676088726" Y="2.611352532796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.249896955473" Y="-0.908789327682" />
                  <Point X="21.250329324044" Y="-0.376850001574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.679833862939" Y="-0.148478387302" />
                  <Point X="27.162312036579" Y="2.766606966501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.235595684328" Y="-0.808799293619" />
                  <Point X="20.842404899839" Y="-0.486153110513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.704353515625" Y="-0.027846901892" />
                  <Point X="27.251947984433" Y="2.921861400206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.221294413182" Y="-0.708809259557" />
                  <Point X="20.434480630477" Y="-0.595456137121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.685914280833" Y="0.069942937872" />
                  <Point X="27.341583932287" Y="3.077115833912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.638672710526" Y="0.152418304188" />
                  <Point X="27.431219880141" Y="3.232370267617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.560064958572" Y="0.218215975889" />
                  <Point X="27.520855827995" Y="3.387624701322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.441872394843" Y="0.262966029817" />
                  <Point X="27.610491775848" Y="3.542879135027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.307322325327" Y="0.29901864364" />
                  <Point X="27.700127853941" Y="3.698133637982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.172772255811" Y="0.335071257463" />
                  <Point X="27.789764004099" Y="3.853388179254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.038222186295" Y="0.371123871286" />
                  <Point X="27.758270180563" Y="3.944236771056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.903672102419" Y="0.407176477474" />
                  <Point X="27.67170026796" Y="4.005800886843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.769122013206" Y="0.443229080824" />
                  <Point X="27.580020058885" Y="4.064647809796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.634571923992" Y="0.479281684174" />
                  <Point X="27.48346349307" Y="4.120901927879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.500021834778" Y="0.515334287523" />
                  <Point X="27.386906644764" Y="4.177155895759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.365471745565" Y="0.551386890873" />
                  <Point X="27.290349796458" Y="4.233409863638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.230921656351" Y="0.587439494223" />
                  <Point X="27.187276912079" Y="4.286199193681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.229733005051" Y="0.694401631931" />
                  <Point X="21.197274243067" Y="1.208852433705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.546034126261" Y="1.394291352984" />
                  <Point X="27.079616134904" Y="4.336549097853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.247013560145" Y="0.811184020874" />
                  <Point X="21.035078977639" Y="1.230205836122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.578782202518" Y="1.519297968815" />
                  <Point X="26.971954709828" Y="4.386898657529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.264294630028" Y="0.927966683536" />
                  <Point X="20.872883712211" Y="1.251559238538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.559318957396" Y="1.616543332628" />
                  <Point X="26.864293284751" Y="4.437248217205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.294111027255" Y="1.051414497975" />
                  <Point X="20.710688446783" Y="1.272912640955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.515989476428" Y="1.701098793744" />
                  <Point X="26.744706201781" Y="4.4812567921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.328174587037" Y="1.177120568802" />
                  <Point X="20.548493181355" Y="1.294266043372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.450556811592" Y="1.773901783527" />
                  <Point X="26.624410475153" Y="4.524888574479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.362238146819" Y="1.30282663963" />
                  <Point X="20.386297888685" Y="1.315619431303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.367730819081" Y="1.837456576938" />
                  <Point X="26.5041146824" Y="4.568520321698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.28490482657" Y="1.901011370348" />
                  <Point X="26.372874184234" Y="4.606332665823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.202078834059" Y="1.964566163759" />
                  <Point X="26.233709558604" Y="4.639931676637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.119252797287" Y="2.028120933636" />
                  <Point X="24.988553106162" Y="4.085464401795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.211560947924" Y="4.204039774595" />
                  <Point X="26.094544410739" Y="4.673530409774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.03642673861" Y="2.091675691865" />
                  <Point X="24.867986456745" Y="4.128952131972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.25796477324" Y="4.336307280996" />
                  <Point X="25.955379262874" Y="4.707129142912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.953600679933" Y="2.155230450095" />
                  <Point X="24.802350459963" Y="4.201647008242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.291585264886" Y="4.461777768317" />
                  <Point X="25.813708988788" Y="4.739395876809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.870774621256" Y="2.218785208325" />
                  <Point X="21.836634061043" Y="2.732341782118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.238777885637" Y="2.94616544654" />
                  <Point X="24.766682683349" Y="4.290276269825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.325204823748" Y="4.587247759667" />
                  <Point X="25.644651780557" Y="4.757100719518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.787948562579" Y="2.282339966554" />
                  <Point X="21.719457970446" Y="2.777632304398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.249870195267" Y="3.059657487004" />
                  <Point X="24.741448113052" Y="4.38445296561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.358823975927" Y="4.712717534781" />
                  <Point X="25.475594352028" Y="4.774805445093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.846339595736" Y="2.420981184423" />
                  <Point X="21.622444216777" Y="2.833643331387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.238092962928" Y="3.160989576306" />
                  <Point X="24.716213542755" Y="4.478629661394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.937403414944" Y="2.576994830795" />
                  <Point X="21.525430463109" Y="2.889654358376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.201022206609" Y="3.248872860348" />
                  <Point X="24.690978972457" Y="4.572806357179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.04592791312" Y="2.742292484857" />
                  <Point X="21.42841670944" Y="2.945665385365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.153493045599" Y="3.331195311975" />
                  <Point X="24.66574440216" Y="4.666983052963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.18869075749" Y="2.925794990515" />
                  <Point X="21.331402955771" Y="3.001676412354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.105963884589" Y="3.413517763603" />
                  <Point X="24.640510132416" Y="4.761159908555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.058434969208" Y="3.495840345834" />
                  <Point X="23.087444246171" Y="4.042974283662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.50360438266" Y="4.264250553315" />
                  <Point X="24.434260447627" Y="4.759089160691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.010906203882" Y="3.57816300785" />
                  <Point X="22.981720625262" Y="4.094354192091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.536253482297" Y="4.389204542343" />
                  <Point X="24.174793501021" Y="4.728722292792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.963377438557" Y="3.660485669867" />
                  <Point X="22.895688266557" Y="4.156204130355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.529854143716" Y="4.493396108478" />
                  <Point X="23.815578883629" Y="4.645318647549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.179191668426" Y="3.882830286191" />
                  <Point X="22.835488746195" Y="4.231789632412" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.739845703125" Y="-4.538443359375" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.35465625" Y="-3.363540039062" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.104638671875" Y="-3.213731933594" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.821638671875" Y="-3.240434326172" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.60205859375" Y="-3.443647705078" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.408498046875" Y="-4.030770751953" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.9972578125" Y="-4.956990722656" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.703158203125" Y="-4.887482910156" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.658552734375" Y="-4.796363769531" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534756835938" />
                  <Point X="23.64977734375" Y="-4.330944824219" />
                  <Point X="23.6297890625" Y="-4.230465332031" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.457482421875" Y="-4.065613037109" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.143400390625" Y="-3.972171875" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.837337890625" Y="-4.089241699219" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.66600390625" Y="-4.254074707031" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.287083984375" Y="-4.256101074219" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.871939453125" Y="-3.958004394531" />
                  <Point X="21.771419921875" Y="-3.880607666016" />
                  <Point X="21.995349609375" Y="-3.492748046875" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597593994141" />
                  <Point X="22.486021484375" Y="-2.568765136719" />
                  <Point X="22.468673828125" Y="-2.551416503906" />
                  <Point X="22.439845703125" Y="-2.537198730469" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.982546875" Y="-2.789288574219" />
                  <Point X="21.15704296875" Y="-3.265893554688" />
                  <Point X="20.951208984375" Y="-2.995472167969" />
                  <Point X="20.838294921875" Y="-2.847125244141" />
                  <Point X="20.643130859375" Y="-2.519865234375" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="20.963796875" Y="-2.092573730469" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396017456055" />
                  <Point X="21.85933984375" Y="-1.376092895508" />
                  <Point X="21.861884765625" Y="-1.366260864258" />
                  <Point X="21.859673828125" Y="-1.33459387207" />
                  <Point X="21.83884375" Y="-1.310640136719" />
                  <Point X="21.82110546875" Y="-1.300200439453" />
                  <Point X="21.812361328125" Y="-1.295053588867" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.2429765625" Y="-1.359333618164" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.11676953125" Y="-1.184078979492" />
                  <Point X="20.072609375" Y="-1.011188415527" />
                  <Point X="20.02097265625" Y="-0.65015625" />
                  <Point X="20.00160546875" Y="-0.5147421875" />
                  <Point X="20.448724609375" Y="-0.394936920166" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.45810546875" Y="-0.121424743652" />
                  <Point X="21.4766953125" Y="-0.108523155212" />
                  <Point X="21.485859375" Y="-0.102162719727" />
                  <Point X="21.505103515625" Y="-0.075907455444" />
                  <Point X="21.511298828125" Y="-0.055942951202" />
                  <Point X="21.514353515625" Y="-0.046100574493" />
                  <Point X="21.514353515625" Y="-0.016459503174" />
                  <Point X="21.508158203125" Y="0.003504843235" />
                  <Point X="21.505103515625" Y="0.013347373962" />
                  <Point X="21.485859375" Y="0.039602642059" />
                  <Point X="21.46726953125" Y="0.052504230499" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.9525859375" Y="0.197367584229" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.05389453125" Y="0.804085754395" />
                  <Point X="20.08235546875" Y="0.996414733887" />
                  <Point X="20.186296875" Y="1.379995605469" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.534345703125" Y="1.487768188477" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866699219" />
                  <Point X="21.309439453125" Y="1.408838989258" />
                  <Point X="21.32972265625" Y="1.41523425293" />
                  <Point X="21.348466796875" Y="1.426056274414" />
                  <Point X="21.360880859375" Y="1.443786132813" />
                  <Point X="21.377388671875" Y="1.483641601562" />
                  <Point X="21.38552734375" Y="1.503290405273" />
                  <Point X="21.389287109375" Y="1.52460546875" />
                  <Point X="21.383685546875" Y="1.54551171875" />
                  <Point X="21.363765625" Y="1.583776611328" />
                  <Point X="21.3539453125" Y="1.602641357422" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.058994140625" Y="1.834869750977" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.7293984375" Y="2.597544433594" />
                  <Point X="20.83998828125" Y="2.787007568359" />
                  <Point X="21.11532421875" Y="3.140915283203" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.403314453125" Y="3.179551513672" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.918787109375" Y="2.915421630859" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927405029297" />
                  <Point X="22.027421875" Y="2.968077148438" />
                  <Point X="22.04747265625" Y="2.988128417969" />
                  <Point X="22.0591015625" Y="3.006381835938" />
                  <Point X="22.061927734375" Y="3.02783984375" />
                  <Point X="22.0569140625" Y="3.085140136719" />
                  <Point X="22.054443359375" Y="3.113389404297" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.923396484375" Y="3.349736328125" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="22.05451953125" Y="4.026662597656" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.680775390625" Y="4.4152578125" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.898232421875" Y="4.46212890625" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.112529296875" Y="4.240461425781" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.218491699219" />
                  <Point X="23.186193359375" Y="4.22225" />
                  <Point X="23.252619140625" Y="4.249765136719" />
                  <Point X="23.2853671875" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275743652344" />
                  <Point X="23.31391796875" Y="4.294486816406" />
                  <Point X="23.335537109375" Y="4.363057617187" />
                  <Point X="23.346197265625" Y="4.39686328125" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.33392578125" Y="4.525970703125" />
                  <Point X="23.31086328125" Y="4.701141113281" />
                  <Point X="23.78256640625" Y="4.833390136719" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.557623046875" Y="4.964823730469" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.82842578125" Y="4.793956054688" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.118265625" Y="4.549049316406" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.64249609375" Y="4.948365722656" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.29514453125" Y="4.820558105469" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.79204296875" Y="4.666199707031" />
                  <Point X="26.93103515625" Y="4.615786132813" />
                  <Point X="27.20478125" Y="4.487764648438" />
                  <Point X="27.3386953125" Y="4.42513671875" />
                  <Point X="27.6031484375" Y="4.271065917969" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.981935546875" Y="4.018323974609" />
                  <Point X="28.0687421875" Y="3.956592529297" />
                  <Point X="27.80696484375" Y="3.503183349609" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491515380859" />
                  <Point X="27.21047265625" Y="2.437740234375" />
                  <Point X="27.2033828125" Y="2.411229003906" />
                  <Point X="27.202044921875" Y="2.392327148438" />
                  <Point X="27.20765234375" Y="2.345827148438" />
                  <Point X="27.210416015625" Y="2.32290234375" />
                  <Point X="27.21868359375" Y="2.300811767578" />
                  <Point X="27.24745703125" Y="2.258408203125" />
                  <Point X="27.261642578125" Y="2.237503173828" />
                  <Point X="27.274943359375" Y="2.224202880859" />
                  <Point X="27.317345703125" Y="2.195430175781" />
                  <Point X="27.338251953125" Y="2.181245361328" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.406837890625" Y="2.167373046875" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946044922" />
                  <Point X="27.502439453125" Y="2.180326416016" />
                  <Point X="27.528951171875" Y="2.187415771484" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.03383203125" Y="2.476934326172" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.1258515625" Y="2.848532714844" />
                  <Point X="29.202595703125" Y="2.741875" />
                  <Point X="29.341634765625" Y="2.512110351562" />
                  <Point X="29.387513671875" Y="2.436295654297" />
                  <Point X="29.0484375" Y="2.176113525391" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583833129883" />
                  <Point X="28.240669921875" Y="1.533343383789" />
                  <Point X="28.22158984375" Y="1.508451782227" />
                  <Point X="28.21312109375" Y="1.491500244141" />
                  <Point X="28.198703125" Y="1.439949951172" />
                  <Point X="28.191595703125" Y="1.414535644531" />
                  <Point X="28.190779296875" Y="1.390966064453" />
                  <Point X="28.202615234375" Y="1.333609863281" />
                  <Point X="28.20844921875" Y="1.305333374023" />
                  <Point X="28.215646484375" Y="1.287957275391" />
                  <Point X="28.2478359375" Y="1.239032104492" />
                  <Point X="28.263703125" Y="1.214911865234" />
                  <Point X="28.280947265625" Y="1.19882019043" />
                  <Point X="28.32759375" Y="1.17256262207" />
                  <Point X="28.35058984375" Y="1.159617675781" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.431634765625" Y="1.145284301758" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.943927734375" Y="1.202801635742" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.906228515625" Y="1.086774047852" />
                  <Point X="29.93919140625" Y="0.951367614746" />
                  <Point X="29.983005859375" Y="0.669959655762" />
                  <Point X="29.997859375" Y="0.574556335449" />
                  <Point X="29.611939453125" Y="0.471149169922" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819046021" />
                  <Point X="28.667125" Y="0.197003723145" />
                  <Point X="28.636578125" Y="0.179346679688" />
                  <Point X="28.622265625" Y="0.166926239014" />
                  <Point X="28.585087890625" Y="0.11955342865" />
                  <Point X="28.566759765625" Y="0.096198562622" />
                  <Point X="28.556986328125" Y="0.074734954834" />
                  <Point X="28.54459375" Y="0.010026067734" />
                  <Point X="28.538484375" Y="-0.021875602722" />
                  <Point X="28.538484375" Y="-0.040684474945" />
                  <Point X="28.550876953125" Y="-0.105393363953" />
                  <Point X="28.556986328125" Y="-0.137295028687" />
                  <Point X="28.566759765625" Y="-0.158758636475" />
                  <Point X="28.6039375" Y="-0.20613130188" />
                  <Point X="28.622265625" Y="-0.229486312866" />
                  <Point X="28.636578125" Y="-0.24190675354" />
                  <Point X="28.698541015625" Y="-0.277722076416" />
                  <Point X="28.729087890625" Y="-0.295379119873" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.1704609375" Y="-0.415415435791" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.9668359375" Y="-0.844334289551" />
                  <Point X="29.948431640625" Y="-0.966414001465" />
                  <Point X="29.892296875" Y="-1.212395996094" />
                  <Point X="29.874546875" Y="-1.290178588867" />
                  <Point X="29.42183203125" Y="-1.230577392578" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.2732265625" Y="-1.124773681641" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.1854453125" Y="-1.154697265625" />
                  <Point X="28.111939453125" Y="-1.243101806641" />
                  <Point X="28.075701171875" Y="-1.286685424805" />
                  <Point X="28.064359375" Y="-1.314071044922" />
                  <Point X="28.05382421875" Y="-1.42855871582" />
                  <Point X="28.048630859375" Y="-1.485001342773" />
                  <Point X="28.056361328125" Y="-1.516621826172" />
                  <Point X="28.123662109375" Y="-1.621304077148" />
                  <Point X="28.156841796875" Y="-1.672912475586" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.56684765625" Y="-1.991232543945" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.256009765625" Y="-2.718196533203" />
                  <Point X="29.2041328125" Y="-2.802139160156" />
                  <Point X="29.088041015625" Y="-2.967092529297" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.65286328125" Y="-2.778488525391" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.592607421875" Y="-2.227173583984" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219245117188" />
                  <Point X="27.368837890625" Y="-2.282526367188" />
                  <Point X="27.309560546875" Y="-2.313724121094" />
                  <Point X="27.2886015625" Y="-2.334682861328" />
                  <Point X="27.2253203125" Y="-2.454922363281" />
                  <Point X="27.19412109375" Y="-2.514200927734" />
                  <Point X="27.1891640625" Y="-2.546374511719" />
                  <Point X="27.2153046875" Y="-2.691109863281" />
                  <Point X="27.22819140625" Y="-2.76246484375" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.490095703125" Y="-3.22198828125" />
                  <Point X="27.98667578125" Y="-4.082087890625" />
                  <Point X="27.89685546875" Y="-4.146243164062" />
                  <Point X="27.835279296875" Y="-4.190224609375" />
                  <Point X="27.705501953125" Y="-4.274228515625" />
                  <Point X="27.67977734375" Y="-4.290879394531" />
                  <Point X="27.369509765625" Y="-3.886530273438" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467529297" />
                  <Point X="26.52780078125" Y="-2.888693603516" />
                  <Point X="26.45742578125" Y="-2.843448730469" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.269685546875" Y="-2.850083251953" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868508789062" />
                  <Point X="26.044783203125" Y="-2.968743408203" />
                  <Point X="25.985349609375" Y="-3.018159179688" />
                  <Point X="25.96845703125" Y="-3.045985351562" />
                  <Point X="25.9324140625" Y="-3.211817626953" />
                  <Point X="25.914642578125" Y="-3.293573242188" />
                  <Point X="25.9139296875" Y="-3.310719970703" />
                  <Point X="25.98648046875" Y="-3.861785888672" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.052583984375" Y="-4.950481933594" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.8744375" Y="-4.985029785156" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#184" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.129433535499" Y="4.838177028909" Z="1.7" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.7" />
                  <Point X="-0.45434698919" Y="5.045764806741" Z="1.7" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.7" />
                  <Point X="-1.237088461377" Y="4.912813662684" Z="1.7" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.7" />
                  <Point X="-1.721804275216" Y="4.550724258786" Z="1.7" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.7" />
                  <Point X="-1.718304781699" Y="4.409374967938" Z="1.7" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.7" />
                  <Point X="-1.772673227017" Y="4.327239286749" Z="1.7" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.7" />
                  <Point X="-1.870540247716" Y="4.316091947127" Z="1.7" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.7" />
                  <Point X="-2.06825641938" Y="4.523846994904" Z="1.7" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.7" />
                  <Point X="-2.349665438443" Y="4.490245306979" Z="1.7" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.7" />
                  <Point X="-2.982060157824" Y="4.097622286191" Z="1.7" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.7" />
                  <Point X="-3.126061088333" Y="3.356016272433" Z="1.7" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.7" />
                  <Point X="-2.999053182556" Y="3.112063773088" Z="1.7" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.7" />
                  <Point X="-3.014091451655" Y="3.034712116231" Z="1.7" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.7" />
                  <Point X="-3.083012692519" Y="2.996511516617" Z="1.7" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.7" />
                  <Point X="-3.577843075864" Y="3.254132860412" Z="1.7" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.7" />
                  <Point X="-3.930295423001" Y="3.202897702492" Z="1.7" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.7" />
                  <Point X="-4.319939363342" Y="2.654029776433" Z="1.7" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.7" />
                  <Point X="-3.97760018604" Y="1.826482059631" Z="1.7" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.7" />
                  <Point X="-3.686741932212" Y="1.59196938707" Z="1.7" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.7" />
                  <Point X="-3.674961242716" Y="1.534055556059" Z="1.7" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.7" />
                  <Point X="-3.711753358111" Y="1.487804769401" Z="1.7" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.7" />
                  <Point X="-4.465286031611" Y="1.568620480768" Z="1.7" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.7" />
                  <Point X="-4.868119024286" Y="1.424352983686" Z="1.7" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.7" />
                  <Point X="-5.00172280124" Y="0.842684967303" Z="1.7" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.7" />
                  <Point X="-4.066513210302" Y="0.180351403991" Z="1.7" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.7" />
                  <Point X="-3.567396495854" Y="0.04270852628" Z="1.7" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.7" />
                  <Point X="-3.545753060319" Y="0.01996443795" Z="1.7" />
                  <Point X="-3.539556741714" Y="0" Z="1.7" />
                  <Point X="-3.542611540816" Y="-0.009842513111" Z="1.7" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.7" />
                  <Point X="-3.557972099269" Y="-0.036167456854" Z="1.7" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.7" />
                  <Point X="-4.57037457847" Y="-0.315360652765" Z="1.7" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.7" />
                  <Point X="-5.03468128953" Y="-0.625955498345" Z="1.7" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.7" />
                  <Point X="-4.937833125927" Y="-1.165173103501" Z="1.7" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.7" />
                  <Point X="-3.75665418293" Y="-1.377625892416" Z="1.7" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.7" />
                  <Point X="-3.210414110146" Y="-1.312010103578" Z="1.7" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.7" />
                  <Point X="-3.195220397131" Y="-1.33227264126" Z="1.7" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.7" />
                  <Point X="-4.072797294467" Y="-2.021625861309" Z="1.7" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.7" />
                  <Point X="-4.40596926314" Y="-2.514194778188" Z="1.7" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.7" />
                  <Point X="-4.094734173391" Y="-2.994475097951" Z="1.7" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.7" />
                  <Point X="-2.998610330662" Y="-2.801309871748" Z="1.7" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.7" />
                  <Point X="-2.567110864516" Y="-2.561219604615" Z="1.7" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.7" />
                  <Point X="-3.054107141421" Y="-3.43646805183" Z="1.7" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.7" />
                  <Point X="-3.164721934912" Y="-3.966341543624" Z="1.7" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.7" />
                  <Point X="-2.745395778957" Y="-4.267331923138" Z="1.7" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.7" />
                  <Point X="-2.300484708943" Y="-4.253232831091" Z="1.7" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.7" />
                  <Point X="-2.141039579276" Y="-4.099534836479" Z="1.7" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.7" />
                  <Point X="-1.866026981808" Y="-3.990784818675" Z="1.7" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.7" />
                  <Point X="-1.581642091844" Y="-4.071924925834" Z="1.7" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.7" />
                  <Point X="-1.40541929354" Y="-4.309420266168" Z="1.7" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.7" />
                  <Point X="-1.397176219213" Y="-4.75855739091" Z="1.7" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.7" />
                  <Point X="-1.315457272862" Y="-4.904625630733" Z="1.7" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.7" />
                  <Point X="-1.018427158314" Y="-4.974794906503" Z="1.7" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.7" />
                  <Point X="-0.549362190992" Y="-4.01243166835" Z="1.7" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.7" />
                  <Point X="-0.363022476829" Y="-3.440876725295" Z="1.7" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.7" />
                  <Point X="-0.169697719454" Y="-3.256907278127" Z="1.7" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.7" />
                  <Point X="0.083661359907" Y="-3.230204767161" Z="1.7" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.7" />
                  <Point X="0.307423382604" Y="-3.360769030473" Z="1.7" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.7" />
                  <Point X="0.685392669409" Y="-4.520104361466" Z="1.7" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.7" />
                  <Point X="0.87721859476" Y="-5.002944795799" Z="1.7" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.7" />
                  <Point X="1.0571316454" Y="-4.968041981196" Z="1.7" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.7" />
                  <Point X="1.029894998943" Y="-3.823979628599" Z="1.7" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.7" />
                  <Point X="0.975115708799" Y="-3.191158100205" Z="1.7" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.7" />
                  <Point X="1.07059220249" Y="-2.975910099437" Z="1.7" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.7" />
                  <Point X="1.268110997722" Y="-2.868592843794" Z="1.7" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.7" />
                  <Point X="1.494605660294" Y="-2.899471342927" Z="1.7" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.7" />
                  <Point X="2.323684252007" Y="-3.885688219946" Z="1.7" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.7" />
                  <Point X="2.726512284826" Y="-4.284923269893" Z="1.7" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.7" />
                  <Point X="2.919761941913" Y="-4.155650021361" Z="1.7" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.7" />
                  <Point X="2.527239642241" Y="-3.165708199036" Z="1.7" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.7" />
                  <Point X="2.258350245164" Y="-2.650943689118" Z="1.7" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.7" />
                  <Point X="2.263408923891" Y="-2.44692978986" Z="1.7" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.7" />
                  <Point X="2.385968588512" Y="-2.295492386076" Z="1.7" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.7" />
                  <Point X="2.577563114394" Y="-2.245097764327" Z="1.7" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.7" />
                  <Point X="3.621705398071" Y="-2.790509858034" Z="1.7" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.7" />
                  <Point X="4.122771808405" Y="-2.96459018521" Z="1.7" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.7" />
                  <Point X="4.292386005877" Y="-2.713201825202" Z="1.7" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.7" />
                  <Point X="3.591128243248" Y="-1.920284953338" Z="1.7" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.7" />
                  <Point X="3.159563354236" Y="-1.562984543507" Z="1.7" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.7" />
                  <Point X="3.097456342017" Y="-1.401859845125" Z="1.7" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.7" />
                  <Point X="3.144229898983" Y="-1.243788635974" Z="1.7" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.7" />
                  <Point X="3.27768964184" Y="-1.142352908468" Z="1.7" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.7" />
                  <Point X="4.409148884883" Y="-1.248869560737" Z="1.7" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.7" />
                  <Point X="4.934886801273" Y="-1.192239555332" Z="1.7" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.7" />
                  <Point X="5.010120514147" Y="-0.820508192406" Z="1.7" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.7" />
                  <Point X="4.177244557395" Y="-0.335839038193" Z="1.7" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.7" />
                  <Point X="3.717405102286" Y="-0.203153578043" Z="1.7" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.7" />
                  <Point X="3.637114156266" Y="-0.143983316807" Z="1.7" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.7" />
                  <Point X="3.593827204234" Y="-0.064708898005" Z="1.7" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.7" />
                  <Point X="3.58754424619" Y="0.031901633193" Z="1.7" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.7" />
                  <Point X="3.618265282134" Y="0.119965421768" Z="1.7" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.7" />
                  <Point X="3.685990312066" Y="0.184995266199" Z="1.7" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.7" />
                  <Point X="4.618723259912" Y="0.454132894576" Z="1.7" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.7" />
                  <Point X="5.026253630123" Y="0.708931777884" Z="1.7" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.7" />
                  <Point X="4.948653694054" Y="1.129880875598" Z="1.7" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.7" />
                  <Point X="3.931246348573" Y="1.283653918903" Z="1.7" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.7" />
                  <Point X="3.432028704986" Y="1.22613336343" Z="1.7" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.7" />
                  <Point X="3.345964205965" Y="1.247413568" Z="1.7" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.7" />
                  <Point X="3.283449531212" Y="1.297791245093" Z="1.7" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.7" />
                  <Point X="3.245426551594" Y="1.374993095711" Z="1.7" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.7" />
                  <Point X="3.240699428708" Y="1.457763547961" Z="1.7" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.7" />
                  <Point X="3.274196159341" Y="1.534205292421" Z="1.7" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.7" />
                  <Point X="4.072718653904" Y="2.167725852457" Z="1.7" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.7" />
                  <Point X="4.378256035397" Y="2.569276840375" Z="1.7" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.7" />
                  <Point X="4.160280221175" Y="2.909015939849" Z="1.7" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.7" />
                  <Point X="3.002675777698" Y="2.551515727334" Z="1.7" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.7" />
                  <Point X="2.483366701054" Y="2.259909322487" Z="1.7" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.7" />
                  <Point X="2.40666696382" Y="2.248293468038" Z="1.7" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.7" />
                  <Point X="2.33926170243" Y="2.268085480732" Z="1.7" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.7" />
                  <Point X="2.282673119503" Y="2.317763157952" Z="1.7" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.7" />
                  <Point X="2.251136234721" Y="2.383091480013" Z="1.7" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.7" />
                  <Point X="2.25261862017" Y="2.456102874828" Z="1.7" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.7" />
                  <Point X="2.844109625525" Y="3.509463304257" Z="1.7" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.7" />
                  <Point X="3.004755827691" Y="4.090351201002" Z="1.7" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.7" />
                  <Point X="2.622162261773" Y="4.345548412883" Z="1.7" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.7" />
                  <Point X="2.21980542286" Y="4.564336083476" Z="1.7" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.7" />
                  <Point X="1.802935175926" Y="4.744483030455" Z="1.7" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.7" />
                  <Point X="1.30072204223" Y="4.900441822969" Z="1.7" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.7" />
                  <Point X="0.641545356148" Y="5.029373966098" Z="1.7" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.7" />
                  <Point X="0.063811026054" Y="4.593270477042" Z="1.7" />
                  <Point X="0" Y="4.355124473572" Z="1.7" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>