<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#129" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="672" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999526367188" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.61081640625" Y="-3.689845703125" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140869141" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.5229453125" Y="-3.439396728516" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.2724453125" Y="-3.166342529297" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.933125" Y="-3.106362548828" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.633673828125" Y="-3.231479980469" />
                  <Point X="24.614255859375" Y="-3.259459960938" />
                  <Point X="24.469947265625" Y="-3.467380371094" />
                  <Point X="24.4618125" Y="-3.481571533203" />
                  <Point X="24.449009765625" Y="-3.512524169922" />
                  <Point X="24.09594140625" Y="-4.830192871094" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.9206640625" Y="-4.845351074219" />
                  <Point X="23.889810546875" Y="-4.837413085938" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.779318359375" Y="-4.606883300781" />
                  <Point X="23.7850390625" Y="-4.563439941406" />
                  <Point X="23.78580078125" Y="-4.547930664062" />
                  <Point X="23.7834921875" Y="-4.51622265625" />
                  <Point X="23.7763125" Y="-4.480130371094" />
                  <Point X="23.722962890625" Y="-4.211931152344" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131205566406" />
                  <Point X="23.648689453125" Y="-4.106941894531" />
                  <Point X="23.443095703125" Y="-3.926640869141" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.320251953125" Y="-3.888559570312" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801513672" />
                  <Point X="22.926744140625" Y="-3.91524609375" />
                  <Point X="22.699376953125" Y="-4.067169433594" />
                  <Point X="22.68721484375" Y="-4.076821533203" />
                  <Point X="22.6648984375" Y="-4.099461425781" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.19829296875" Y="-4.089387207031" />
                  <Point X="22.155580078125" Y="-4.056499511719" />
                  <Point X="21.895279296875" Y="-3.856076660156" />
                  <Point X="22.48794140625" Y="-2.829555175781" />
                  <Point X="22.57623828125" Y="-2.676619384766" />
                  <Point X="22.587142578125" Y="-2.647646484375" />
                  <Point X="22.593412109375" Y="-2.616115966797" />
                  <Point X="22.594248046875" Y="-2.583920410156" />
                  <Point X="22.584279296875" Y="-2.553295654297" />
                  <Point X="22.568" Y="-2.522407226562" />
                  <Point X="22.551134765625" Y="-2.499528564453" />
                  <Point X="22.535853515625" Y="-2.484245849609" />
                  <Point X="22.510697265625" Y="-2.466216308594" />
                  <Point X="22.4818671875" Y="-2.451997558594" />
                  <Point X="22.452248046875" Y="-2.443011962891" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.224353515625" Y="-3.117334716797" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="20.917146484375" Y="-2.793868652344" />
                  <Point X="20.886521484375" Y="-2.742515625" />
                  <Point X="20.693857421875" Y="-2.419449462891" />
                  <Point X="21.739552734375" Y="-1.617059448242" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.916935546875" Y="-1.475880615234" />
                  <Point X="21.935359375" Y="-1.447130737305" />
                  <Point X="21.942986328125" Y="-1.428534667969" />
                  <Point X="21.947056640625" Y="-1.416305786133" />
                  <Point X="21.95384765625" Y="-1.390086547852" />
                  <Point X="21.95665234375" Y="-1.359662475586" />
                  <Point X="21.9544453125" Y="-1.327991455078" />
                  <Point X="21.9468359375" Y="-1.296805786133" />
                  <Point X="21.929275390625" Y="-1.269934814453" />
                  <Point X="21.90530078125" Y="-1.244129394531" />
                  <Point X="21.88388671875" Y="-1.226917724609" />
                  <Point X="21.860544921875" Y="-1.213179931641" />
                  <Point X="21.831283203125" Y="-1.201956176758" />
                  <Point X="21.799396484375" Y="-1.195474731445" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="20.33339453125" Y="-1.383262695312" />
                  <Point X="20.2678984375" Y="-1.391885375977" />
                  <Point X="20.165923828125" Y="-0.992655029297" />
                  <Point X="20.157822265625" Y="-0.936012573242" />
                  <Point X="20.107578125" Y="-0.584698425293" />
                  <Point X="21.291134765625" Y="-0.267564727783" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.485291015625" Y="-0.213481506348" />
                  <Point X="21.50415234375" Y="-0.203976394653" />
                  <Point X="21.51556640625" Y="-0.197184387207" />
                  <Point X="21.54002734375" Y="-0.180207092285" />
                  <Point X="21.563978515625" Y="-0.158682907104" />
                  <Point X="21.58376953125" Y="-0.130805847168" />
                  <Point X="21.592294921875" Y="-0.112528610229" />
                  <Point X="21.59693359375" Y="-0.100525177002" />
                  <Point X="21.6050859375" Y="-0.07425390625" />
                  <Point X="21.60930078125" Y="-0.042951843262" />
                  <Point X="21.608201171875" Y="-0.009772438049" />
                  <Point X="21.603986328125" Y="0.015236849785" />
                  <Point X="21.5958359375" Y="0.041501293182" />
                  <Point X="21.582515625" Y="0.0708334198" />
                  <Point X="21.5617421875" Y="0.098234008789" />
                  <Point X="21.5466328125" Y="0.112033729553" />
                  <Point X="21.536734375" Y="0.119931533813" />
                  <Point X="21.5122734375" Y="0.136908981323" />
                  <Point X="21.498076171875" Y="0.145046920776" />
                  <Point X="21.467125" Y="0.157848327637" />
                  <Point X="20.159349609375" Y="0.508265838623" />
                  <Point X="20.10818359375" Y="0.521975585938" />
                  <Point X="20.175513671875" Y="0.976972167969" />
                  <Point X="20.191822265625" Y="1.037159667969" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="21.108181640625" Y="1.316401489258" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228027344" />
                  <Point X="21.2968671875" Y="1.305264282227" />
                  <Point X="21.30415234375" Y="1.307561523438" />
                  <Point X="21.35829296875" Y="1.324631835938" />
                  <Point X="21.377224609375" Y="1.332962646484" />
                  <Point X="21.39596875" Y="1.343784912109" />
                  <Point X="21.412646484375" Y="1.356014282227" />
                  <Point X="21.426283203125" Y="1.371563232422" />
                  <Point X="21.43869921875" Y="1.389293579102" />
                  <Point X="21.448650390625" Y="1.407428588867" />
                  <Point X="21.45157421875" Y="1.414486450195" />
                  <Point X="21.473296875" Y="1.466932739258" />
                  <Point X="21.4790859375" Y="1.486797119141" />
                  <Point X="21.48284375" Y="1.508112060547" />
                  <Point X="21.484197265625" Y="1.528754882812" />
                  <Point X="21.481048828125" Y="1.549201171875" />
                  <Point X="21.4754453125" Y="1.570107177734" />
                  <Point X="21.467951171875" Y="1.589376831055" />
                  <Point X="21.464423828125" Y="1.596152954102" />
                  <Point X="21.4382109375" Y="1.646506591797" />
                  <Point X="21.426716796875" Y="1.663706665039" />
                  <Point X="21.4128046875" Y="1.680286499023" />
                  <Point X="21.39786328125" Y="1.694590087891" />
                  <Point X="20.648140625" Y="2.269874023438" />
                  <Point X="20.918849609375" Y="2.733661621094" />
                  <Point X="20.962044921875" Y="2.789185546875" />
                  <Point X="21.24949609375" Y="3.158662353516" />
                  <Point X="21.71586328125" Y="2.889404052734" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.863353515625" Y="2.824908691406" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.019537109375" Y="2.835653564453" />
                  <Point X="22.037791015625" Y="2.847282714844" />
                  <Point X="22.053919921875" Y="2.860226074219" />
                  <Point X="22.061123046875" Y="2.867428466797" />
                  <Point X="22.11464453125" Y="2.920949462891" />
                  <Point X="22.12759765625" Y="2.937089599609" />
                  <Point X="22.1392265625" Y="2.955345947266" />
                  <Point X="22.148375" Y="2.973897705078" />
                  <Point X="22.1532890625" Y="2.993989501953" />
                  <Point X="22.15611328125" Y="3.015450927734" />
                  <Point X="22.156564453125" Y="3.036120117188" />
                  <Point X="22.155677734375" Y="3.046261962891" />
                  <Point X="22.14908203125" Y="3.121664306641" />
                  <Point X="22.145044921875" Y="3.141961425781" />
                  <Point X="22.13853515625" Y="3.162604248047" />
                  <Point X="22.130205078125" Y="3.181532714844" />
                  <Point X="21.816666015625" Y="3.724596679688" />
                  <Point X="22.299373046875" Y="4.094683349609" />
                  <Point X="22.367408203125" Y="4.132481933594" />
                  <Point X="22.83296484375" Y="4.391134277344" />
                  <Point X="22.9330859375" Y="4.26065234375" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.0161796875" Y="4.183516113281" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222552734375" Y="4.134483886719" />
                  <Point X="23.234314453125" Y="4.139356445312" />
                  <Point X="23.3217265625" Y="4.175563476562" />
                  <Point X="23.339853515625" Y="4.185508789062" />
                  <Point X="23.357583984375" Y="4.197922851562" />
                  <Point X="23.373140625" Y="4.211563964844" />
                  <Point X="23.385373046875" Y="4.228249511719" />
                  <Point X="23.396193359375" Y="4.246993652344" />
                  <Point X="23.404521484375" Y="4.265925292969" />
                  <Point X="23.408349609375" Y="4.278068359375" />
                  <Point X="23.43680078125" Y="4.368301757813" />
                  <Point X="23.4408359375" Y="4.388583007812" />
                  <Point X="23.44272265625" Y="4.410143066406" />
                  <Point X="23.442271484375" Y="4.430825683594" />
                  <Point X="23.415798828125" Y="4.631896972656" />
                  <Point X="24.050369140625" Y="4.80980859375" />
                  <Point X="24.1328359375" Y="4.819459960938" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.84317578125" Y="4.371856933594" />
                  <Point X="24.866095703125" Y="4.286315917969" />
                  <Point X="24.87887109375" Y="4.258122558594" />
                  <Point X="24.89673046875" Y="4.231395507813" />
                  <Point X="24.91788671875" Y="4.208808105469" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183885742188" />
                  <Point X="25.067130859375" Y="4.194217773438" />
                  <Point X="25.094423828125" Y="4.208806640625" />
                  <Point X="25.11558203125" Y="4.23139453125" />
                  <Point X="25.13344140625" Y="4.25812109375" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="25.912279296875" Y="4.815264648438" />
                  <Point X="26.4810234375" Y="4.677951660156" />
                  <Point X="26.524134765625" Y="4.662315429688" />
                  <Point X="26.894640625" Y="4.5279296875" />
                  <Point X="26.937595703125" Y="4.507841308594" />
                  <Point X="27.294576171875" Y="4.340893066406" />
                  <Point X="27.336103515625" Y="4.316700195313" />
                  <Point X="27.68098046875" Y="4.1157734375" />
                  <Point X="27.720115234375" Y="4.087943603516" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.24978125" Y="2.728109863281" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.140171875" Y="2.534932861328" />
                  <Point X="27.132623046875" Y="2.513327392578" />
                  <Point X="27.13053125" Y="2.50653515625" />
                  <Point X="27.111607421875" Y="2.435771484375" />
                  <Point X="27.108390625" Y="2.412432128906" />
                  <Point X="27.108046875" Y="2.385295898438" />
                  <Point X="27.10872265625" Y="2.372721923828" />
                  <Point X="27.116099609375" Y="2.311531494141" />
                  <Point X="27.12144140625" Y="2.289609619141" />
                  <Point X="27.12970703125" Y="2.26751953125" />
                  <Point X="27.140072265625" Y="2.247467285156" />
                  <Point X="27.14516796875" Y="2.239958251953" />
                  <Point X="27.18303125" Y="2.184158691406" />
                  <Point X="27.198720703125" Y="2.166327880859" />
                  <Point X="27.219529296875" Y="2.147932617188" />
                  <Point X="27.229109375" Y="2.140496337891" />
                  <Point X="27.28491015625" Y="2.102634033203" />
                  <Point X="27.304955078125" Y="2.092271728516" />
                  <Point X="27.32704296875" Y="2.084006103516" />
                  <Point X="27.348962890625" Y="2.078663818359" />
                  <Point X="27.357197265625" Y="2.077670654297" />
                  <Point X="27.41838671875" Y="2.070292236328" />
                  <Point X="27.442623046875" Y="2.070483398438" />
                  <Point X="27.471048828125" Y="2.074367675781" />
                  <Point X="27.482728515625" Y="2.076717773438" />
                  <Point X="27.5534921875" Y="2.095640869141" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.90390625" Y="2.869574951172" />
                  <Point X="28.967328125" Y="2.906191162109" />
                  <Point X="29.1232734375" Y="2.689463867188" />
                  <Point X="29.145091796875" Y="2.653407714844" />
                  <Point X="29.26219921875" Y="2.459884033203" />
                  <Point X="28.365337890625" Y="1.771697387695" />
                  <Point X="28.23078515625" Y="1.668451171875" />
                  <Point X="28.217478515625" Y="1.656044189453" />
                  <Point X="28.20137890625" Y="1.637853271484" />
                  <Point X="28.197119140625" Y="1.632685791016" />
                  <Point X="28.14619140625" Y="1.566245483398" />
                  <Point X="28.134083984375" Y="1.545433105469" />
                  <Point X="28.1230625" Y="1.519353881836" />
                  <Point X="28.119080078125" Y="1.507960571289" />
                  <Point X="28.100107421875" Y="1.440124755859" />
                  <Point X="28.09665234375" Y="1.417824951172" />
                  <Point X="28.0958359375" Y="1.394253173828" />
                  <Point X="28.09773828125" Y="1.371767456055" />
                  <Point X="28.099833984375" Y="1.361610595703" />
                  <Point X="28.115408203125" Y="1.286134765625" />
                  <Point X="28.1233203125" Y="1.263165893555" />
                  <Point X="28.13621875" Y="1.23712512207" />
                  <Point X="28.141984375" Y="1.227076538086" />
                  <Point X="28.184341796875" Y="1.162695068359" />
                  <Point X="28.198892578125" Y="1.145452880859" />
                  <Point X="28.216134765625" Y="1.12936340332" />
                  <Point X="28.2343515625" Y="1.116031982422" />
                  <Point X="28.24261328125" Y="1.111382080078" />
                  <Point X="28.303994140625" Y="1.076829467773" />
                  <Point X="28.326990234375" Y="1.067595458984" />
                  <Point X="28.356134765625" Y="1.060121337891" />
                  <Point X="28.367287109375" Y="1.057962646484" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.73771875" Y="1.211486328125" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.845939453125" Y="0.9327890625" />
                  <Point X="29.8528125" Y="0.888646911621" />
                  <Point X="29.890865234375" Y="0.644238769531" />
                  <Point X="28.87078125" Y="0.370907836914" />
                  <Point X="28.716580078125" Y="0.329589874268" />
                  <Point X="28.699205078125" Y="0.323056671143" />
                  <Point X="28.67615234375" Y="0.311706695557" />
                  <Point X="28.67057421875" Y="0.308725372314" />
                  <Point X="28.589037109375" Y="0.261595367432" />
                  <Point X="28.56954296875" Y="0.246661941528" />
                  <Point X="28.548646484375" Y="0.225852386475" />
                  <Point X="28.540947265625" Y="0.217186981201" />
                  <Point X="28.492025390625" Y="0.154848480225" />
                  <Point X="28.48030078125" Y="0.135566864014" />
                  <Point X="28.47052734375" Y="0.114102783203" />
                  <Point X="28.46368359375" Y="0.092609725952" />
                  <Point X="28.46148828125" Y="0.081150917053" />
                  <Point X="28.445181640625" Y="-0.004000760555" />
                  <Point X="28.443732421875" Y="-0.028748209" />
                  <Point X="28.445927734375" Y="-0.059015666962" />
                  <Point X="28.447375" Y="-0.07001121521" />
                  <Point X="28.463681640625" Y="-0.155162734985" />
                  <Point X="28.47052734375" Y="-0.17666293335" />
                  <Point X="28.48030078125" Y="-0.198126998901" />
                  <Point X="28.49202734375" Y="-0.217410903931" />
                  <Point X="28.498611328125" Y="-0.225800064087" />
                  <Point X="28.547533203125" Y="-0.288138549805" />
                  <Point X="28.565654296875" Y="-0.30577645874" />
                  <Point X="28.590939453125" Y="-0.324539398193" />
                  <Point X="28.600009765625" Y="-0.330497924805" />
                  <Point X="28.681546875" Y="-0.377627929688" />
                  <Point X="28.692708984375" Y="-0.383138244629" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="29.86244140625" Y="-0.699182495117" />
                  <Point X="29.891474609375" Y="-0.706961853027" />
                  <Point X="29.85502734375" Y="-0.948713195801" />
                  <Point X="29.84621484375" Y="-0.987324523926" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="28.603212890625" Y="-1.026984130859" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.353125" Y="-1.01018951416" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.1639765625" Y="-1.056595947266" />
                  <Point X="28.1361484375" Y="-1.07348840332" />
                  <Point X="28.112400390625" Y="-1.093957763672" />
                  <Point X="28.0993828125" Y="-1.109612792969" />
                  <Point X="28.00265625" Y="-1.225945922852" />
                  <Point X="27.987931640625" Y="-1.250334594727" />
                  <Point X="27.97658984375" Y="-1.277719726562" />
                  <Point X="27.969759765625" Y="-1.30536730957" />
                  <Point X="27.96789453125" Y="-1.325641479492" />
                  <Point X="27.95403125" Y="-1.476297485352" />
                  <Point X="27.95634765625" Y="-1.507561401367" />
                  <Point X="27.964078125" Y="-1.539182373047" />
                  <Point X="27.976451171875" Y="-1.56799621582" />
                  <Point X="27.988369140625" Y="-1.586534057617" />
                  <Point X="28.076931640625" Y="-1.724286865234" />
                  <Point X="28.0869375" Y="-1.73724206543" />
                  <Point X="28.110630859375" Y="-1.760909423828" />
                  <Point X="29.17399609375" Y="-2.576859130859" />
                  <Point X="29.213125" Y="-2.606883300781" />
                  <Point X="29.124814453125" Y="-2.749780761719" />
                  <Point X="29.106591796875" Y="-2.775674316406" />
                  <Point X="29.028982421875" Y="-2.885945556641" />
                  <Point X="27.9602265625" Y="-2.268898925781" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159824951172" />
                  <Point X="27.72859375" Y="-2.155196289062" />
                  <Point X="27.538134765625" Y="-2.120799316406" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135176269531" />
                  <Point X="27.423541015625" Y="-2.146382324219" />
                  <Point X="27.26531640625" Y="-2.229655273438" />
                  <Point X="27.242384765625" Y="-2.246549804688" />
                  <Point X="27.22142578125" Y="-2.267509521484" />
                  <Point X="27.20453515625" Y="-2.290436767578" />
                  <Point X="27.193328125" Y="-2.311729492188" />
                  <Point X="27.1100546875" Y="-2.469954833984" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531908447266" />
                  <Point X="27.09567578125" Y="-2.563258789062" />
                  <Point X="27.1003046875" Y="-2.588889160156" />
                  <Point X="27.134703125" Y="-2.779349121094" />
                  <Point X="27.13898828125" Y="-2.795140136719" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.835142578125" Y="-4.009623291016" />
                  <Point X="27.861287109375" Y="-4.054905517578" />
                  <Point X="27.781869140625" Y="-4.111630859375" />
                  <Point X="27.76148046875" Y="-4.124829101562" />
                  <Point X="27.701767578125" Y="-4.16348046875" />
                  <Point X="26.880087890625" Y="-3.092647705078" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900556640625" />
                  <Point X="26.696646484375" Y="-2.8843046875" />
                  <Point X="26.50880078125" Y="-2.763537841797" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.389453125" Y="-2.743660888672" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.08325" Y="-2.813211181641" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968861083984" />
                  <Point X="25.88725" Y="-2.996687744141" />
                  <Point X="25.875625" Y="-3.025808837891" />
                  <Point X="25.8692421875" Y="-3.055175292969" />
                  <Point X="25.821810546875" Y="-3.273396728516" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="26.013390625" Y="-4.79401953125" />
                  <Point X="26.02206640625" Y="-4.859915527344" />
                  <Point X="25.975677734375" Y="-4.870083496094" />
                  <Point X="25.956859375" Y="-4.873502441406" />
                  <Point X="25.92931640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.9415703125" Y="-4.752636230469" />
                  <Point X="23.91348046875" Y="-4.745409179688" />
                  <Point X="23.858755859375" Y="-4.731328613281" />
                  <Point X="23.873505859375" Y="-4.619282714844" />
                  <Point X="23.8792265625" Y="-4.575839355469" />
                  <Point X="23.879923828125" Y="-4.568100097656" />
                  <Point X="23.88055078125" Y="-4.541032226562" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497687988281" />
                  <Point X="23.869486328125" Y="-4.461595703125" />
                  <Point X="23.81613671875" Y="-4.193396484375" />
                  <Point X="23.811873046875" Y="-4.178467285156" />
                  <Point X="23.800970703125" Y="-4.149500488281" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.769423828125" Y="-4.094858886719" />
                  <Point X="23.749791015625" Y="-4.070937011719" />
                  <Point X="23.738994140625" Y="-4.059782470703" />
                  <Point X="23.711328125" Y="-4.035518798828" />
                  <Point X="23.505734375" Y="-3.855217773438" />
                  <Point X="23.493265625" Y="-3.84596875" />
                  <Point X="23.466982421875" Y="-3.829623291016" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.32646484375" Y="-3.793762939453" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.79549609375" />
                  <Point X="22.918134765625" Y="-3.808269042969" />
                  <Point X="22.9045625" Y="-3.815811767578" />
                  <Point X="22.87396484375" Y="-3.836256347656" />
                  <Point X="22.64659765625" Y="-3.9881796875" />
                  <Point X="22.6403203125" Y="-3.992755859375" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032771484375" />
                  <Point X="22.589529296875" Y="-4.04162890625" />
                  <Point X="22.496796875" Y="-4.162478515625" />
                  <Point X="22.252412109375" Y="-4.01116015625" />
                  <Point X="22.213537109375" Y="-3.981227294922" />
                  <Point X="22.01913671875" Y="-3.831546142578" />
                  <Point X="22.570212890625" Y="-2.877055175781" />
                  <Point X="22.658509765625" Y="-2.724119384766" />
                  <Point X="22.665150390625" Y="-2.710082275391" />
                  <Point X="22.6760546875" Y="-2.681109375" />
                  <Point X="22.680318359375" Y="-2.666173583984" />
                  <Point X="22.686587890625" Y="-2.634643066406" />
                  <Point X="22.688380859375" Y="-2.618581787109" />
                  <Point X="22.689216796875" Y="-2.586386230469" />
                  <Point X="22.68458203125" Y="-2.554515380859" />
                  <Point X="22.67461328125" Y="-2.523890625" />
                  <Point X="22.668322265625" Y="-2.509002441406" />
                  <Point X="22.65204296875" Y="-2.478114013672" />
                  <Point X="22.64446875" Y="-2.466037597656" />
                  <Point X="22.627603515625" Y="-2.443158935547" />
                  <Point X="22.6183125" Y="-2.432356689453" />
                  <Point X="22.60303125" Y="-2.417073974609" />
                  <Point X="22.5911953125" Y="-2.407029541016" />
                  <Point X="22.5660390625" Y="-2.389" />
                  <Point X="22.55271875" Y="-2.381014892578" />
                  <Point X="22.523888671875" Y="-2.366796142578" />
                  <Point X="22.5094453125" Y="-2.361088867188" />
                  <Point X="22.479826171875" Y="-2.352103271484" />
                  <Point X="22.449140625" Y="-2.348062744141" />
                  <Point X="22.418205078125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="20.995986328125" Y="-2.740593994141" />
                  <Point X="20.96811328125" Y="-2.693856933594" />
                  <Point X="20.818734375" Y="-2.443372802734" />
                  <Point X="21.797384765625" Y="-1.692427978516" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.960837890625" Y="-1.566067871094" />
                  <Point X="21.983728515625" Y="-1.543435058594" />
                  <Point X="21.996921875" Y="-1.527137939453" />
                  <Point X="22.015345703125" Y="-1.498388061523" />
                  <Point X="22.02325390625" Y="-1.4831796875" />
                  <Point X="22.030880859375" Y="-1.464583618164" />
                  <Point X="22.039021484375" Y="-1.440125610352" />
                  <Point X="22.0458125" Y="-1.41390637207" />
                  <Point X="22.048447265625" Y="-1.398807250977" />
                  <Point X="22.051251953125" Y="-1.368383178711" />
                  <Point X="22.051421875" Y="-1.353058349609" />
                  <Point X="22.04921484375" Y="-1.321387451172" />
                  <Point X="22.04673828125" Y="-1.305471923828" />
                  <Point X="22.03912890625" Y="-1.274286254883" />
                  <Point X="22.026359375" Y="-1.244835571289" />
                  <Point X="22.008798828125" Y="-1.217964599609" />
                  <Point X="21.998875" Y="-1.205273803711" />
                  <Point X="21.974900390625" Y="-1.179468505859" />
                  <Point X="21.96481640625" Y="-1.170082641602" />
                  <Point X="21.94340234375" Y="-1.15287097168" />
                  <Point X="21.932072265625" Y="-1.145045166016" />
                  <Point X="21.90873046875" Y="-1.131307373047" />
                  <Point X="21.89456640625" Y="-1.124480957031" />
                  <Point X="21.8653046875" Y="-1.113257324219" />
                  <Point X="21.85020703125" Y="-1.108859863281" />
                  <Point X="21.8183203125" Y="-1.102378417969" />
                  <Point X="21.802703125" Y="-1.100532348633" />
                  <Point X="21.77137890625" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="20.339080078125" Y="-1.286694458008" />
                  <Point X="20.259240234375" Y="-0.974118286133" />
                  <Point X="20.251865234375" Y="-0.922561645508" />
                  <Point X="20.213548828125" Y="-0.654654541016" />
                  <Point X="21.31572265625" Y="-0.359327636719" />
                  <Point X="21.491712890625" Y="-0.312171234131" />
                  <Point X="21.50097265625" Y="-0.309174255371" />
                  <Point X="21.528044921875" Y="-0.298317779541" />
                  <Point X="21.54690625" Y="-0.28881262207" />
                  <Point X="21.569734375" Y="-0.275228637695" />
                  <Point X="21.5941953125" Y="-0.258251434326" />
                  <Point X="21.60352734375" Y="-0.25086680603" />
                  <Point X="21.627478515625" Y="-0.229342498779" />
                  <Point X="21.64144140625" Y="-0.213677352905" />
                  <Point X="21.661232421875" Y="-0.185800308228" />
                  <Point X="21.66986328125" Y="-0.170964477539" />
                  <Point X="21.678388671875" Y="-0.152687286377" />
                  <Point X="21.687666015625" Y="-0.128680526733" />
                  <Point X="21.695818359375" Y="-0.102409210205" />
                  <Point X="21.699236328125" Y="-0.08693132782" />
                  <Point X="21.703451171875" Y="-0.055629268646" />
                  <Point X="21.704248046875" Y="-0.039805095673" />
                  <Point X="21.7031484375" Y="-0.006625781536" />
                  <Point X="21.701880859375" Y="0.006015336514" />
                  <Point X="21.697666015625" Y="0.031024702072" />
                  <Point X="21.69471875" Y="0.043392799377" />
                  <Point X="21.686568359375" Y="0.06965713501" />
                  <Point X="21.682333984375" Y="0.080782150269" />
                  <Point X="21.669013671875" Y="0.110114212036" />
                  <Point X="21.65821875" Y="0.128227035522" />
                  <Point X="21.6374453125" Y="0.155627593994" />
                  <Point X="21.62580859375" Y="0.168380477905" />
                  <Point X="21.61069921875" Y="0.182180114746" />
                  <Point X="21.59090234375" Y="0.197975601196" />
                  <Point X="21.56644140625" Y="0.214953079224" />
                  <Point X="21.559517578125" Y="0.219328994751" />
                  <Point X="21.534384765625" Y="0.232834503174" />
                  <Point X="21.50343359375" Y="0.245635986328" />
                  <Point X="21.491712890625" Y="0.249611343384" />
                  <Point X="20.2145546875" Y="0.591825073242" />
                  <Point X="20.268671875" Y="0.957528991699" />
                  <Point X="20.283515625" Y="1.012314025879" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="21.09578125" Y="1.222214233398" />
                  <Point X="21.221935546875" Y="1.20560546875" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.20470324707" />
                  <Point X="21.28485546875" Y="1.206589355469" />
                  <Point X="21.295109375" Y="1.208053588867" />
                  <Point X="21.315400390625" Y="1.21208984375" />
                  <Point X="21.33272265625" Y="1.216959228516" />
                  <Point X="21.38686328125" Y="1.234029541016" />
                  <Point X="21.396556640625" Y="1.237678466797" />
                  <Point X="21.41548828125" Y="1.246009277344" />
                  <Point X="21.4247265625" Y="1.250690917969" />
                  <Point X="21.443470703125" Y="1.261513183594" />
                  <Point X="21.45214453125" Y="1.267174316406" />
                  <Point X="21.468822265625" Y="1.279403564453" />
                  <Point X="21.4840703125" Y="1.293374755859" />
                  <Point X="21.49770703125" Y="1.308923706055" />
                  <Point X="21.504099609375" Y="1.31707019043" />
                  <Point X="21.516515625" Y="1.334800537109" />
                  <Point X="21.521984375" Y="1.343592651367" />
                  <Point X="21.531935546875" Y="1.361727661133" />
                  <Point X="21.539341796875" Y="1.378127685547" />
                  <Point X="21.561064453125" Y="1.43057409668" />
                  <Point X="21.564501953125" Y="1.440352661133" />
                  <Point X="21.570291015625" Y="1.460217041016" />
                  <Point X="21.572642578125" Y="1.470303100586" />
                  <Point X="21.576400390625" Y="1.491618041992" />
                  <Point X="21.577640625" Y="1.501896362305" />
                  <Point X="21.578994140625" Y="1.52253918457" />
                  <Point X="21.57808984375" Y="1.543213134766" />
                  <Point X="21.57494140625" Y="1.563659423828" />
                  <Point X="21.572810546875" Y="1.573796264648" />
                  <Point X="21.56720703125" Y="1.594702270508" />
                  <Point X="21.563984375" Y="1.604541137695" />
                  <Point X="21.556490234375" Y="1.623810791016" />
                  <Point X="21.548689453125" Y="1.640018310547" />
                  <Point X="21.5224765625" Y="1.690372070312" />
                  <Point X="21.517197265625" Y="1.699290283203" />
                  <Point X="21.505703125" Y="1.716490356445" />
                  <Point X="21.499490234375" Y="1.724771484375" />
                  <Point X="21.485578125" Y="1.741351318359" />
                  <Point X="21.4785" Y="1.748910400391" />
                  <Point X="21.46355859375" Y="1.763213989258" />
                  <Point X="21.4556953125" Y="1.769958618164" />
                  <Point X="20.77238671875" Y="2.29428125" />
                  <Point X="20.997712890625" Y="2.68031640625" />
                  <Point X="21.03702734375" Y="2.730852783203" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.66836328125" Y="2.807131835938" />
                  <Point X="21.74584375" Y="2.762398681641" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.85507421875" Y="2.730270263672" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.02356640625" Y="2.734227294922" />
                  <Point X="22.043005859375" Y="2.741302978516" />
                  <Point X="22.061556640625" Y="2.750451904297" />
                  <Point X="22.070580078125" Y="2.755531738281" />
                  <Point X="22.088833984375" Y="2.767160888672" />
                  <Point X="22.09725" Y="2.773190429688" />
                  <Point X="22.11337890625" Y="2.786133789062" />
                  <Point X="22.128294921875" Y="2.80025" />
                  <Point X="22.18181640625" Y="2.853770996094" />
                  <Point X="22.188734375" Y="2.861488769531" />
                  <Point X="22.2016875" Y="2.87762890625" />
                  <Point X="22.20772265625" Y="2.886051269531" />
                  <Point X="22.2193515625" Y="2.904307617188" />
                  <Point X="22.2244296875" Y="2.913329589844" />
                  <Point X="22.233578125" Y="2.931881347656" />
                  <Point X="22.240654296875" Y="2.951327880859" />
                  <Point X="22.245568359375" Y="2.971419677734" />
                  <Point X="22.2474765625" Y="2.981594726562" />
                  <Point X="22.25030078125" Y="3.003056152344" />
                  <Point X="22.25108984375" Y="3.013377685547" />
                  <Point X="22.251541015625" Y="3.034046875" />
                  <Point X="22.25031640625" Y="3.054536376953" />
                  <Point X="22.243720703125" Y="3.129938720703" />
                  <Point X="22.2422578125" Y="3.140196777344" />
                  <Point X="22.238220703125" Y="3.160493896484" />
                  <Point X="22.235646484375" Y="3.170532958984" />
                  <Point X="22.22913671875" Y="3.19117578125" />
                  <Point X="22.22548828125" Y="3.200870361328" />
                  <Point X="22.217158203125" Y="3.219798828125" />
                  <Point X="22.2124765625" Y="3.229032714844" />
                  <Point X="21.940611328125" Y="3.699915527344" />
                  <Point X="22.35162890625" Y="4.015038574219" />
                  <Point X="22.413544921875" Y="4.049437744141" />
                  <Point X="22.8074765625" Y="4.268296386719" />
                  <Point X="22.857716796875" Y="4.2028203125" />
                  <Point X="22.881435546875" Y="4.171909179688" />
                  <Point X="22.8881796875" Y="4.164048828125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.934908203125" Y="4.121897460938" />
                  <Point X="22.952107421875" Y="4.110405273438" />
                  <Point X="22.9723125" Y="4.099250488281" />
                  <Point X="23.056236328125" Y="4.0555625" />
                  <Point X="23.065673828125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.0437890625" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.2292734375" Y="4.037489013672" />
                  <Point X="23.249138671875" Y="4.043279541016" />
                  <Point X="23.270673828125" Y="4.05158984375" />
                  <Point X="23.3580859375" Y="4.087796875" />
                  <Point X="23.367421875" Y="4.092275390625" />
                  <Point X="23.385548828125" Y="4.102220703125" />
                  <Point X="23.39433984375" Y="4.1076875" />
                  <Point X="23.4120703125" Y="4.1201015625" />
                  <Point X="23.420216796875" Y="4.126494140625" />
                  <Point X="23.4357734375" Y="4.140135253906" />
                  <Point X="23.4497578125" Y="4.155395507812" />
                  <Point X="23.461990234375" Y="4.172081054688" />
                  <Point X="23.4676484375" Y="4.180754882812" />
                  <Point X="23.47846875" Y="4.199499023438" />
                  <Point X="23.483150390625" Y="4.208740234375" />
                  <Point X="23.491478515625" Y="4.227671875" />
                  <Point X="23.498953125" Y="4.249505371094" />
                  <Point X="23.527404296875" Y="4.339738769531" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370044921875" />
                  <Point X="23.535474609375" Y="4.380301269531" />
                  <Point X="23.537361328125" Y="4.401861328125" />
                  <Point X="23.53769921875" Y="4.41221484375" />
                  <Point X="23.537248046875" Y="4.432897460938" />
                  <Point X="23.536458984375" Y="4.443226074219" />
                  <Point X="23.520734375" Y="4.562654296875" />
                  <Point X="24.068826171875" Y="4.7163203125" />
                  <Point X="24.14387890625" Y="4.725104003906" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.751412109375" Y="4.347269042969" />
                  <Point X="24.77433203125" Y="4.261728027344" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912597656" />
                  <Point X="24.7998828125" Y="4.205341308594" />
                  <Point X="24.8177421875" Y="4.178614257812" />
                  <Point X="24.82739453125" Y="4.166452636719" />
                  <Point X="24.84855078125" Y="4.143865234375" />
                  <Point X="24.873103515625" Y="4.125025390625" />
                  <Point X="24.9003984375" Y="4.110436035156" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.021630859375" Y="4.085113037109" />
                  <Point X="25.05216796875" Y="4.090154541016" />
                  <Point X="25.06723046875" Y="4.093927246094" />
                  <Point X="25.09766796875" Y="4.104259277344" />
                  <Point X="25.1119140625" Y="4.110436035156" />
                  <Point X="25.13920703125" Y="4.125024902344" />
                  <Point X="25.1637578125" Y="4.143861328125" />
                  <Point X="25.184916015625" Y="4.16644921875" />
                  <Point X="25.1945703125" Y="4.178612792969" />
                  <Point X="25.2124296875" Y="4.205339355469" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.24710546875" />
                  <Point X="25.23798046875" Y="4.261727050781" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="25.889984375" Y="4.72291796875" />
                  <Point X="26.45359765625" Y="4.58684375" />
                  <Point X="26.491744140625" Y="4.573008300781" />
                  <Point X="26.85824609375" Y="4.440075195312" />
                  <Point X="26.8973515625" Y="4.421786621094" />
                  <Point X="27.25044921875" Y="4.256654296875" />
                  <Point X="27.28828125" Y="4.234614257812" />
                  <Point X="27.6294453125" Y="4.035850830078" />
                  <Point X="27.665060546875" Y="4.0105234375" />
                  <Point X="27.817783203125" Y="3.901915771484" />
                  <Point X="27.167509765625" Y="2.775609863281" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.061228515625" Y="2.590693847656" />
                  <Point X="27.05048828125" Y="2.566267822266" />
                  <Point X="27.042939453125" Y="2.544662353516" />
                  <Point X="27.038755859375" Y="2.531077880859" />
                  <Point X="27.01983203125" Y="2.460314208984" />
                  <Point X="27.017498046875" Y="2.448742431641" />
                  <Point X="27.01428125" Y="2.425403076172" />
                  <Point X="27.0133984375" Y="2.413635498047" />
                  <Point X="27.0130546875" Y="2.386499267578" />
                  <Point X="27.01440625" Y="2.361351318359" />
                  <Point X="27.021783203125" Y="2.300160888672" />
                  <Point X="27.02380078125" Y="2.289040527344" />
                  <Point X="27.029142578125" Y="2.267118652344" />
                  <Point X="27.032466796875" Y="2.256317138672" />
                  <Point X="27.040732421875" Y="2.234227050781" />
                  <Point X="27.045314453125" Y="2.223896240234" />
                  <Point X="27.0556796875" Y="2.203843994141" />
                  <Point X="27.06655859375" Y="2.186613525391" />
                  <Point X="27.104421875" Y="2.130813964844" />
                  <Point X="27.1117109375" Y="2.121402832031" />
                  <Point X="27.127400390625" Y="2.103572021484" />
                  <Point X="27.13580078125" Y="2.09515234375" />
                  <Point X="27.156609375" Y="2.076757080078" />
                  <Point X="27.17576953125" Y="2.061884521484" />
                  <Point X="27.2315703125" Y="2.024022216797" />
                  <Point X="27.241283203125" Y="2.018243408203" />
                  <Point X="27.261328125" Y="2.007881103516" />
                  <Point X="27.27166015625" Y="2.003297485352" />
                  <Point X="27.293748046875" Y="1.995031860352" />
                  <Point X="27.304548828125" Y="1.991707763672" />
                  <Point X="27.32646875" Y="1.986365478516" />
                  <Point X="27.345822265625" Y="1.983354248047" />
                  <Point X="27.40701171875" Y="1.975975830078" />
                  <Point X="27.41913671875" Y="1.975295166016" />
                  <Point X="27.443373046875" Y="1.975486328125" />
                  <Point X="27.455484375" Y="1.976358154297" />
                  <Point X="27.48391015625" Y="1.980242553711" />
                  <Point X="27.50726953125" Y="1.984942504883" />
                  <Point X="27.578033203125" Y="2.003865600586" />
                  <Point X="27.5839921875" Y="2.005670288086" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572875977" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.94040625" Y="2.780951416016" />
                  <Point X="29.043958984375" Y="2.637037353516" />
                  <Point X="29.063814453125" Y="2.604224853516" />
                  <Point X="29.13688671875" Y="2.483472167969" />
                  <Point X="28.307505859375" Y="1.847065795898" />
                  <Point X="28.172953125" Y="1.743819824219" />
                  <Point X="28.166" Y="1.737933959961" />
                  <Point X="28.146337890625" Y="1.719005493164" />
                  <Point X="28.13023828125" Y="1.700814575195" />
                  <Point X="28.121720703125" Y="1.690479736328" />
                  <Point X="28.07079296875" Y="1.624039428711" />
                  <Point X="28.064076171875" Y="1.614015625" />
                  <Point X="28.05196875" Y="1.59320324707" />
                  <Point X="28.046578125" Y="1.582414794922" />
                  <Point X="28.035556640625" Y="1.556335449219" />
                  <Point X="28.027591796875" Y="1.533548706055" />
                  <Point X="28.008619140625" Y="1.465712890625" />
                  <Point X="28.006228515625" Y="1.454670288086" />
                  <Point X="28.0027734375" Y="1.432370483398" />
                  <Point X="28.001708984375" Y="1.42111328125" />
                  <Point X="28.000892578125" Y="1.397541503906" />
                  <Point X="28.001173828125" Y="1.386244384766" />
                  <Point X="28.003076171875" Y="1.363758789062" />
                  <Point X="28.00679296875" Y="1.342413330078" />
                  <Point X="28.0223671875" Y="1.26693737793" />
                  <Point X="28.025587890625" Y="1.255194335938" />
                  <Point X="28.0335" Y="1.232225463867" />
                  <Point X="28.03819140625" Y="1.22099987793" />
                  <Point X="28.05108984375" Y="1.194959106445" />
                  <Point X="28.06262109375" Y="1.174861938477" />
                  <Point X="28.104978515625" Y="1.11048046875" />
                  <Point X="28.111740234375" Y="1.10142578125" />
                  <Point X="28.126291015625" Y="1.08418359375" />
                  <Point X="28.134080078125" Y="1.07599609375" />
                  <Point X="28.151322265625" Y="1.059906616211" />
                  <Point X="28.16003125" Y="1.052699584961" />
                  <Point X="28.178248046875" Y="1.039368286133" />
                  <Point X="28.196017578125" Y="1.02859387207" />
                  <Point X="28.2573984375" Y="0.994041320801" />
                  <Point X="28.26859375" Y="0.988671264648" />
                  <Point X="28.29158984375" Y="0.979437194824" />
                  <Point X="28.303390625" Y="0.975573303223" />
                  <Point X="28.33253515625" Y="0.968099182129" />
                  <Point X="28.35483984375" Y="0.963781555176" />
                  <Point X="28.437833984375" Y="0.952813049316" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797302246" />
                  <Point X="29.704703125" Y="1.111319946289" />
                  <Point X="29.7526875" Y="0.914211486816" />
                  <Point X="29.758943359375" Y="0.874031311035" />
                  <Point X="29.78387109375" Y="0.713921081543" />
                  <Point X="28.846193359375" Y="0.462670715332" />
                  <Point X="28.6919921875" Y="0.421352813721" />
                  <Point X="28.68314453125" Y="0.418511566162" />
                  <Point X="28.6572421875" Y="0.408286468506" />
                  <Point X="28.634189453125" Y="0.396936584473" />
                  <Point X="28.623033203125" Y="0.390973999023" />
                  <Point X="28.54149609375" Y="0.343843902588" />
                  <Point X="28.531265625" Y="0.337010498047" />
                  <Point X="28.511771484375" Y="0.322077178955" />
                  <Point X="28.5025078125" Y="0.313976928711" />
                  <Point X="28.481611328125" Y="0.293167358398" />
                  <Point X="28.466212890625" Y="0.275836730957" />
                  <Point X="28.417291015625" Y="0.213498214722" />
                  <Point X="28.410853515625" Y="0.204206466675" />
                  <Point X="28.39912890625" Y="0.184924880981" />
                  <Point X="28.393841796875" Y="0.174935043335" />
                  <Point X="28.384068359375" Y="0.153470932007" />
                  <Point X="28.380005859375" Y="0.142926422119" />
                  <Point X="28.373162109375" Y="0.121433334351" />
                  <Point X="28.368185546875" Y="0.09902620697" />
                  <Point X="28.35187890625" Y="0.01387451458" />
                  <Point X="28.35034375" Y="0.001552934885" />
                  <Point X="28.34889453125" Y="-0.02319455719" />
                  <Point X="28.34898046875" Y="-0.035620616913" />
                  <Point X="28.35117578125" Y="-0.065887962341" />
                  <Point X="28.3540703125" Y="-0.087879241943" />
                  <Point X="28.370376953125" Y="-0.173030639648" />
                  <Point X="28.37316015625" Y="-0.183985183716" />
                  <Point X="28.380005859375" Y="-0.205485412598" />
                  <Point X="28.384068359375" Y="-0.216031097412" />
                  <Point X="28.393841796875" Y="-0.23749520874" />
                  <Point X="28.399130859375" Y="-0.247486679077" />
                  <Point X="28.410857421875" Y="-0.2667706604" />
                  <Point X="28.42387890625" Y="-0.284451873779" />
                  <Point X="28.47280078125" Y="-0.346790252686" />
                  <Point X="28.481271484375" Y="-0.356215148926" />
                  <Point X="28.499392578125" Y="-0.373853118896" />
                  <Point X="28.50904296875" Y="-0.382066467285" />
                  <Point X="28.534328125" Y="-0.400829376221" />
                  <Point X="28.55246875" Y="-0.412746551514" />
                  <Point X="28.634005859375" Y="-0.459876495361" />
                  <Point X="28.639494140625" Y="-0.462813293457" />
                  <Point X="28.65915625" Y="-0.472015869141" />
                  <Point X="28.68302734375" Y="-0.48102746582" />
                  <Point X="28.6919921875" Y="-0.483912841797" />
                  <Point X="29.784880859375" Y="-0.776751281738" />
                  <Point X="29.761623046875" Y="-0.931014465332" />
                  <Point X="29.753595703125" Y="-0.96618572998" />
                  <Point X="29.727802734375" Y="-1.079219482422" />
                  <Point X="28.61561328125" Y="-0.932796875" />
                  <Point X="28.436783203125" Y="-0.909253662109" />
                  <Point X="28.428625" Y="-0.908535644531" />
                  <Point X="28.40009765625" Y="-0.908042358398" />
                  <Point X="28.36672265625" Y="-0.910840942383" />
                  <Point X="28.354482421875" Y="-0.912676269531" />
                  <Point X="28.332947265625" Y="-0.917356994629" />
                  <Point X="28.17291796875" Y="-0.952140136719" />
                  <Point X="28.157876953125" Y="-0.956741943359" />
                  <Point X="28.1287578125" Y="-0.96836541748" />
                  <Point X="28.1146796875" Y="-0.975386962891" />
                  <Point X="28.0868515625" Y="-0.992279418945" />
                  <Point X="28.074125" Y="-1.001529907227" />
                  <Point X="28.050376953125" Y="-1.021999267578" />
                  <Point X="28.03935546875" Y="-1.033218261719" />
                  <Point X="28.026337890625" Y="-1.048873046875" />
                  <Point X="27.929611328125" Y="-1.165206298828" />
                  <Point X="27.921330078125" Y="-1.176844848633" />
                  <Point X="27.90660546875" Y="-1.201233520508" />
                  <Point X="27.900162109375" Y="-1.213983764648" />
                  <Point X="27.8888203125" Y="-1.241368896484" />
                  <Point X="27.88436328125" Y="-1.254935791016" />
                  <Point X="27.877533203125" Y="-1.282583251953" />
                  <Point X="27.87516015625" Y="-1.29666394043" />
                  <Point X="27.873294921875" Y="-1.316938232422" />
                  <Point X="27.859431640625" Y="-1.467594238281" />
                  <Point X="27.859291015625" Y="-1.483317016602" />
                  <Point X="27.861607421875" Y="-1.514580932617" />
                  <Point X="27.864064453125" Y="-1.530121948242" />
                  <Point X="27.871794921875" Y="-1.561743041992" />
                  <Point X="27.87678515625" Y="-1.576666748047" />
                  <Point X="27.889158203125" Y="-1.60548059082" />
                  <Point X="27.896541015625" Y="-1.619370605469" />
                  <Point X="27.908458984375" Y="-1.637908447266" />
                  <Point X="27.997021484375" Y="-1.775661254883" />
                  <Point X="28.00174609375" Y="-1.782356201172" />
                  <Point X="28.019798828125" Y="-1.804454101562" />
                  <Point X="28.0434921875" Y="-1.828121337891" />
                  <Point X="28.052798828125" Y="-1.836277954102" />
                  <Point X="29.087173828125" Y="-2.629982177734" />
                  <Point X="29.045494140625" Y="-2.697422851562" />
                  <Point X="29.02890234375" Y="-2.721" />
                  <Point X="29.001275390625" Y="-2.760252685547" />
                  <Point X="28.0077265625" Y="-2.186626464844" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513671875" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337158203" />
                  <Point X="27.7454765625" Y="-2.061708496094" />
                  <Point X="27.555017578125" Y="-2.027311523438" />
                  <Point X="27.539359375" Y="-2.025807250977" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503295898" />
                  <Point X="27.460140625" Y="-2.03146105957" />
                  <Point X="27.444845703125" Y="-2.035135986328" />
                  <Point X="27.4150703125" Y="-2.044959228516" />
                  <Point X="27.40058984375" Y="-2.051107666016" />
                  <Point X="27.379296875" Y="-2.062313720703" />
                  <Point X="27.221072265625" Y="-2.145586669922" />
                  <Point X="27.20896875" Y="-2.153171142578" />
                  <Point X="27.186037109375" Y="-2.170065673828" />
                  <Point X="27.175208984375" Y="-2.179375732422" />
                  <Point X="27.15425" Y="-2.200335449219" />
                  <Point X="27.14494140625" Y="-2.211162353516" />
                  <Point X="27.12805078125" Y="-2.234089599609" />
                  <Point X="27.12046875" Y="-2.246189941406" />
                  <Point X="27.10926171875" Y="-2.267482666016" />
                  <Point X="27.02598828125" Y="-2.425708007812" />
                  <Point X="27.019837890625" Y="-2.440187744141" />
                  <Point X="27.01001171875" Y="-2.469968261719" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517442138672" />
                  <Point X="27.000279296875" Y="-2.533133544922" />
                  <Point X="27.00068359375" Y="-2.564483886719" />
                  <Point X="27.0021875" Y="-2.580142822266" />
                  <Point X="27.00681640625" Y="-2.605773193359" />
                  <Point X="27.04121484375" Y="-2.796233154297" />
                  <Point X="27.04301953125" Y="-2.804229248047" />
                  <Point X="27.05123828125" Y="-2.831540771484" />
                  <Point X="27.064072265625" Y="-2.862479248047" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.735896484375" Y="-4.02772265625" />
                  <Point X="27.723755859375" Y="-4.036081298828" />
                  <Point X="26.95545703125" Y="-3.034815429688" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849626464844" />
                  <Point X="26.78325390625" Y="-2.828004882812" />
                  <Point X="26.77330078125" Y="-2.820645996094" />
                  <Point X="26.748021484375" Y="-2.804394042969" />
                  <Point X="26.56017578125" Y="-2.683627197266" />
                  <Point X="26.54628125" Y="-2.676244140625" />
                  <Point X="26.51746875" Y="-2.663873046875" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.380748046875" Y="-2.649060546875" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="26.022513671875" Y="-2.740163330078" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883087646484" />
                  <Point X="25.83218359375" Y="-2.906837158203" />
                  <Point X="25.822935546875" Y="-2.919562744141" />
                  <Point X="25.80604296875" Y="-2.947389404297" />
                  <Point X="25.79901953125" Y="-2.961466796875" />
                  <Point X="25.78739453125" Y="-2.990587890625" />
                  <Point X="25.78279296875" Y="-3.005631591797" />
                  <Point X="25.77641015625" Y="-3.034998046875" />
                  <Point X="25.728978515625" Y="-3.253219482422" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520263672" />
                  <Point X="25.83308984375" Y="-4.152328613281" />
                  <Point X="25.702580078125" Y="-3.6652578125" />
                  <Point X="25.655068359375" Y="-3.487936767578" />
                  <Point X="25.652607421875" Y="-3.48012109375" />
                  <Point X="25.642146484375" Y="-3.453580078125" />
                  <Point X="25.6267890625" Y="-3.423816162109" />
                  <Point X="25.62041015625" Y="-3.413209716797" />
                  <Point X="25.600990234375" Y="-3.385229492188" />
                  <Point X="25.456681640625" Y="-3.177309326172" />
                  <Point X="25.446671875" Y="-3.165172119141" />
                  <Point X="25.424787109375" Y="-3.142716308594" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104936767578" />
                  <Point X="25.3452421875" Y="-3.090829345703" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.30060546875" Y="-3.075612060547" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.90496484375" Y="-3.015631835938" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.6670703125" Y="-3.090829589844" />
                  <Point X="24.6390703125" Y="-3.104937255859" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.5875234375" Y="-3.142718261719" />
                  <Point X="24.56563671875" Y="-3.165177490234" />
                  <Point X="24.555626953125" Y="-3.177316162109" />
                  <Point X="24.536208984375" Y="-3.205296142578" />
                  <Point X="24.391900390625" Y="-3.413216552734" />
                  <Point X="24.38752734375" Y="-3.420135498047" />
                  <Point X="24.374025390625" Y="-3.445260742188" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936279297" />
                  <Point X="24.01457421875" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.82093337283" Y="-2.447060164451" />
                  <Point X="21.291022559864" Y="-2.969147098657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.038043769547" Y="-3.798798202973" />
                  <Point X="22.17691290957" Y="-3.953028007807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.893672969824" Y="-2.385870398959" />
                  <Point X="21.375133010353" Y="-2.92058594536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.08798825586" Y="-3.712291902283" />
                  <Point X="22.48898021344" Y="-4.157638588717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.96927459305" Y="-2.327859235618" />
                  <Point X="21.459243460841" Y="-2.872024792064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.137932742174" Y="-3.625785601592" />
                  <Point X="22.554023139906" Y="-4.087900804614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.044876216276" Y="-2.269848072277" />
                  <Point X="21.54335391133" Y="-2.823463638768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.187877228487" Y="-3.539279300902" />
                  <Point X="22.615521887884" Y="-4.014226811527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.120477839502" Y="-2.211836908936" />
                  <Point X="21.627464361819" Y="-2.774902485471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.237821714801" Y="-3.452773000211" />
                  <Point X="22.692367346813" Y="-3.957597067684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.308891043685" Y="-1.168503184395" />
                  <Point X="20.40723216292" Y="-1.277722062139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.196079462728" Y="-2.153825745595" />
                  <Point X="21.711574812308" Y="-2.726341332175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.287766201115" Y="-3.366266699521" />
                  <Point X="22.772182656074" Y="-3.904265676788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.258775416497" Y="-0.970868869414" />
                  <Point X="20.521519585194" Y="-1.262675831368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.271681085954" Y="-2.095814582254" />
                  <Point X="21.795685262797" Y="-2.677780178879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.337710687428" Y="-3.27976039883" />
                  <Point X="22.851997965335" Y="-3.850934285891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.234634074792" Y="-0.802081920955" />
                  <Point X="20.635807007468" Y="-1.247629600596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.34728270918" Y="-2.037803418913" />
                  <Point X="21.879795713285" Y="-2.629219025582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.387655173742" Y="-3.193254098139" />
                  <Point X="22.934671059478" Y="-3.800776786649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.226580836254" Y="-0.651162621212" />
                  <Point X="20.750094429742" Y="-1.232583369824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.422884332406" Y="-1.979792255573" />
                  <Point X="21.963906163774" Y="-2.580657872286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.437599660055" Y="-3.106747797449" />
                  <Point X="23.040283572582" Y="-3.776096093187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.864065064544" Y="-4.690998127645" />
                  <Point X="23.912929306632" Y="-4.745267366436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.329568785111" Y="-0.623567053852" />
                  <Point X="20.864381852017" Y="-1.217537139052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.498485955632" Y="-1.921781092232" />
                  <Point X="22.048016614263" Y="-2.53209671899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.487544146369" Y="-3.020241496758" />
                  <Point X="23.175092673804" Y="-3.78384149588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.87995689405" Y="-4.56667252014" />
                  <Point X="24.025028463742" Y="-4.727790820986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.432556733969" Y="-0.595971486493" />
                  <Point X="20.978669274291" Y="-1.20249090828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.574087578858" Y="-1.863769928891" />
                  <Point X="22.132127064752" Y="-2.483535565693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.537488632682" Y="-2.933735196068" />
                  <Point X="23.310945190571" Y="-3.792745728936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.857096158764" Y="-4.399307829196" />
                  <Point X="24.054346038245" Y="-4.618376013897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.535544682826" Y="-0.568375919133" />
                  <Point X="21.092956696565" Y="-1.187444677508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.649689202084" Y="-1.80575876555" />
                  <Point X="22.216237515241" Y="-2.434974412397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.587433031765" Y="-2.847228798498" />
                  <Point X="23.478350872961" Y="-3.836693302614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.820846431084" Y="-4.217073155739" />
                  <Point X="24.083663612748" Y="-4.508961206807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.638532631683" Y="-0.540780351774" />
                  <Point X="21.207244118839" Y="-1.172398446736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.72529082531" Y="-1.747747602209" />
                  <Point X="22.300347965729" Y="-2.3864132591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.637377265081" Y="-2.760722216824" />
                  <Point X="24.112981187251" Y="-4.399546399718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.741520580541" Y="-0.513184784414" />
                  <Point X="21.321531541113" Y="-1.157352215964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.800892448528" Y="-1.689736438859" />
                  <Point X="22.397166409359" Y="-2.351965762024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.680261077726" Y="-2.666374243595" />
                  <Point X="24.142298761755" Y="-4.290131592628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.844508529398" Y="-0.485589217055" />
                  <Point X="21.435818963387" Y="-1.142305985192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.87649407157" Y="-1.631725275313" />
                  <Point X="22.549910336542" Y="-2.379629806881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.670023852691" Y="-2.513029381116" />
                  <Point X="24.171616336258" Y="-4.180716785539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.947496478256" Y="-0.457993649695" />
                  <Point X="21.550106385661" Y="-1.12725975442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.952084144301" Y="-1.573701283848" />
                  <Point X="24.200933910761" Y="-4.071301978449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.050484427113" Y="-0.430398082336" />
                  <Point X="21.664393807935" Y="-1.112213523648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.013999048508" Y="-1.500489479078" />
                  <Point X="24.230251485264" Y="-3.96188717136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.22067373482" Y="0.633175329701" />
                  <Point X="20.271690385217" Y="0.576515599306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.15347237597" Y="-0.402802514976" />
                  <Point X="21.781031545986" Y="-1.099777582992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.0486173936" Y="-1.396961774143" />
                  <Point X="24.259569059767" Y="-3.852472364271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.23871782481" Y="0.755110609776" />
                  <Point X="20.440174402002" Y="0.531370413952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.256460324828" Y="-0.375206947617" />
                  <Point X="24.28888663427" Y="-3.743057557181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.256761914801" Y="0.877045889851" />
                  <Point X="20.608658418788" Y="0.486225228599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.359448289766" Y="-0.347611398117" />
                  <Point X="24.318204208773" Y="-3.633642750092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.278724126889" Y="0.99462965449" />
                  <Point X="20.777142435573" Y="0.441080043245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.4624362765" Y="-0.320015872823" />
                  <Point X="24.347521783277" Y="-3.524227943002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.308296023859" Y="1.103762007865" />
                  <Point X="20.945626452358" Y="0.395934857892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.556848758757" Y="-0.282896284938" />
                  <Point X="24.385323773343" Y="-3.424236034018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.337868365848" Y="1.212893866996" />
                  <Point X="21.114110469144" Y="0.350789672539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.631948428717" Y="-0.224327646017" />
                  <Point X="24.440364337247" Y="-3.343389500876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.371448864104" Y="1.317574217616" />
                  <Point X="21.282594485929" Y="0.305644487185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.683460692986" Y="-0.139562539144" />
                  <Point X="24.49601000198" Y="-3.263215000287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.516475456612" Y="1.29848114123" />
                  <Point X="21.451078502714" Y="0.260499301832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.703589785257" Y="-0.019942888696" />
                  <Point X="24.551654889812" Y="-3.183039636863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.661502049121" Y="1.279388064844" />
                  <Point X="24.620179103059" Y="-3.117168213426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.80652864163" Y="1.260294988458" />
                  <Point X="24.71083920017" Y="-3.075881179636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.951555234138" Y="1.241201912072" />
                  <Point X="24.810753173735" Y="-3.044871616847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.096581828387" Y="1.222108833754" />
                  <Point X="24.910667144509" Y="-3.013862050958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.789597767592" Y="-3.990013400621" />
                  <Point X="25.815509234962" Y="-4.018791000559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.240142724882" Y="1.204643577704" />
                  <Point X="25.024230725985" Y="-2.998011913537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.735438737048" Y="-3.787888431269" />
                  <Point X="25.793616770576" Y="-3.852501683394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.351545178328" Y="1.22289389096" />
                  <Point X="25.191427435429" Y="-3.041727399246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.681280436518" Y="-3.585764272682" />
                  <Point X="25.77172430619" Y="-3.686212366229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.444186607993" Y="1.26198043202" />
                  <Point X="25.379785596855" Y="-3.108945058359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.508280560794" Y="-3.251653173402" />
                  <Point X="25.749831841804" Y="-3.519923049065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.51211391096" Y="1.328514791483" />
                  <Point X="25.727939377418" Y="-3.3536337319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.556965385971" Y="1.420677454265" />
                  <Point X="25.736053341602" Y="-3.220669929831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.821870516291" Y="2.379058288356" />
                  <Point X="21.179439302766" Y="1.981937919185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.578274729097" Y="1.538986303344" />
                  <Point X="25.760911701899" Y="-3.106302663638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.872147881622" Y="2.465194889443" />
                  <Point X="25.786674952553" Y="-2.992940379999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.922425246953" Y="2.551331490531" />
                  <Point X="25.83453061823" Y="-2.904114208969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.972702612284" Y="2.637468091619" />
                  <Point X="25.903696424417" Y="-2.838955346681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.027491129727" Y="2.718594550715" />
                  <Point X="25.976801383605" Y="-2.778171357014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.086746652897" Y="2.794759897347" />
                  <Point X="26.050303940952" Y="-2.717828944838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.146002502885" Y="2.87092488101" />
                  <Point X="26.14008252726" Y="-2.675562894118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.205258352874" Y="2.947089864674" />
                  <Point X="26.254514089675" Y="-2.660676747191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.264514202862" Y="3.023254848337" />
                  <Point X="26.372567716809" Y="-2.64981331067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.498799844162" Y="2.905029555302" />
                  <Point X="26.511164203682" Y="-2.661765031265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.580479225148" Y="-3.8493596764" />
                  <Point X="27.726636297606" Y="-4.011683550203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.762027487894" Y="2.754660912161" />
                  <Point X="26.805978971051" Y="-2.847214729224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.843373695976" Y="-2.888745778715" />
                  <Point X="27.498173601613" Y="-3.615974748625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.916676845822" Y="2.724880672073" />
                  <Point X="27.269710905621" Y="-3.220265947047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.033003394184" Y="2.737662223892" />
                  <Point X="27.054400817252" Y="-2.839164596098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.115439029729" Y="2.788083447625" />
                  <Point X="27.015529911112" Y="-2.654018809039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.182970008655" Y="2.855057969528" />
                  <Point X="27.004156811968" Y="-2.499412430561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.235896442618" Y="2.938252481841" />
                  <Point X="27.040611061811" Y="-2.397923704417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.249378901742" Y="3.065253966244" />
                  <Point X="27.087768236992" Y="-2.3083217811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.181611223592" Y="3.282492869936" />
                  <Point X="27.137371982639" Y="-2.221437049562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.953146697161" Y="3.678203704421" />
                  <Point X="27.205825377104" Y="-2.1554869739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.012089094484" Y="3.754716812538" />
                  <Point X="27.291413152278" Y="-2.108566555887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.087716370756" Y="3.812699485285" />
                  <Point X="27.378146947896" Y="-2.062918922522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.163343647028" Y="3.870682158032" />
                  <Point X="27.475523023657" Y="-2.02909073867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.2389709233" Y="3.928664830779" />
                  <Point X="27.610832346842" Y="-2.037391694135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.314598199572" Y="3.986647503526" />
                  <Point X="27.763491731571" Y="-2.064961845084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.395115793847" Y="4.039198927897" />
                  <Point X="27.993455157467" Y="-2.1783868316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.480325414878" Y="4.086539328634" />
                  <Point X="28.259694598519" Y="-2.332100414536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.565535065978" Y="4.133879695975" />
                  <Point X="28.525933990836" Y="-2.485813943347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.650744717078" Y="4.181220063316" />
                  <Point X="27.953769106349" Y="-1.708385189852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.104968620287" Y="-1.876309262268" />
                  <Point X="28.792173383154" Y="-2.639527472158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.735954368179" Y="4.228560430657" />
                  <Point X="27.859928948161" Y="-1.462189863538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.518546849626" Y="-2.193659147396" />
                  <Point X="29.013311771016" Y="-2.74315126099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.987197652904" Y="4.091501766611" />
                  <Point X="27.871782068654" Y="-1.33337881526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.932125078965" Y="-2.511009032524" />
                  <Point X="29.067667738469" Y="-2.661544406462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.171248244711" Y="4.029068148226" />
                  <Point X="27.897626324911" Y="-1.220106497459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.276596009382" Y="4.05404287461" />
                  <Point X="27.95189628912" Y="-1.138404126652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.36915181045" Y="4.093224515861" />
                  <Point X="28.013269378471" Y="-1.064590575522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.445344359827" Y="4.150579389223" />
                  <Point X="28.080289750968" Y="-0.997048967728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.494863151974" Y="4.237558471182" />
                  <Point X="28.168818174741" Y="-0.953394470852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.528133584391" Y="4.342583184803" />
                  <Point X="28.27545637465" Y="-0.929852917992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.5315107687" Y="4.480807713882" />
                  <Point X="28.384807588123" Y="-0.90932447195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.572566314547" Y="4.577186183099" />
                  <Point X="28.522771997099" Y="-0.920574198923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.674634983876" Y="4.605802713806" />
                  <Point X="28.667798518113" Y="-0.939667195905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.776703653206" Y="4.634419244514" />
                  <Point X="28.812825159884" Y="-0.958760327003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.878772322535" Y="4.663035775222" />
                  <Point X="28.957851801655" Y="-0.9778534581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.980840991865" Y="4.69165230593" />
                  <Point X="28.374405007203" Y="-0.187894874208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.603336478044" Y="-0.442149030762" />
                  <Point X="29.102878443426" Y="-0.996946589198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.084783395584" Y="4.718187843776" />
                  <Point X="28.349202839049" Y="-0.017929758618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.793191471153" Y="-0.511029089875" />
                  <Point X="29.247905085197" Y="-1.016039720295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.200431715468" Y="4.73172264463" />
                  <Point X="28.368886913889" Y="0.102184133759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.961675417324" Y="-0.556174196802" />
                  <Point X="29.392931726968" Y="-1.035132851393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.316079982504" Y="4.745257504179" />
                  <Point X="24.796108814133" Y="4.212131476293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.875871775052" Y="4.123545733677" />
                  <Point X="28.408437789295" Y="0.200233708798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.130159363494" Y="-0.60131930373" />
                  <Point X="29.537958368739" Y="-1.05422598249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.43172824954" Y="4.758792363728" />
                  <Point X="24.729867302692" Y="4.427675400138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.036153087221" Y="4.087510574725" />
                  <Point X="28.467199656541" Y="0.276947315876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.298643309665" Y="-0.646464410658" />
                  <Point X="29.68298501051" Y="-1.073319113588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.547376516576" Y="4.772327223277" />
                  <Point X="24.675708041323" Y="4.629800625846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.133133353809" Y="4.121778349199" />
                  <Point X="28.537315255276" Y="0.341051326674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.467127255835" Y="-0.691609517585" />
                  <Point X="29.745662051678" Y="-1.000953747664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.201055713624" Y="4.188318198588" />
                  <Point X="28.62116966838" Y="0.389896838294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.635611202006" Y="-0.736754624513" />
                  <Point X="29.768636735125" Y="-0.884494446387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.243671019139" Y="4.282964379197" />
                  <Point X="27.02066827756" Y="2.309408985178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.307340321217" Y="1.991027425841" />
                  <Point X="28.71510536933" Y="0.427545945467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.272988721193" Y="4.392379044627" />
                  <Point X="27.018513563127" Y="2.45377731023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.448815918036" Y="1.975878129708" />
                  <Point X="28.003820724199" Y="1.359482846192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.360823847907" Y="0.96299070917" />
                  <Point X="28.818093425855" Y="0.45514139325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.302306423247" Y="4.501793710057" />
                  <Point X="27.048971240095" Y="2.561925905253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.556609404085" Y="1.998136607321" />
                  <Point X="28.015109885371" Y="1.488920234751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.49802640087" Y="0.952587109019" />
                  <Point X="28.921081398022" Y="0.482736934722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.3316241253" Y="4.611208375488" />
                  <Point X="27.095971824558" Y="2.651701740182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.650270119495" Y="2.036091116876" />
                  <Point X="28.050926426724" Y="1.591117207922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.612417936959" Y="0.967517709686" />
                  <Point X="29.024069338535" Y="0.510332511348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.360941827354" Y="4.720623040918" />
                  <Point X="27.145916089994" Y="2.738208286183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.734380553037" Y="2.084652288994" />
                  <Point X="28.106810102263" Y="1.671027370732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.72670535044" Y="0.982563950223" />
                  <Point X="29.127057279049" Y="0.537928087974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.43628366872" Y="4.778922721244" />
                  <Point X="27.195860474514" Y="2.824714699926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.818490986579" Y="2.133213461112" />
                  <Point X="28.170768050699" Y="1.741970145013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.840992763922" Y="0.99761019076" />
                  <Point X="29.230045219563" Y="0.5655236646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.577428238778" Y="4.764141067574" />
                  <Point X="27.245804949736" Y="2.911221012935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.902601420121" Y="2.18177463323" />
                  <Point X="28.246277535659" Y="1.800083638265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.955280177403" Y="1.012656431297" />
                  <Point X="29.333033160077" Y="0.593119241226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.718572808837" Y="4.749359413904" />
                  <Point X="27.295749424959" Y="2.997727325943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.986711853663" Y="2.230335805347" />
                  <Point X="28.321879150433" Y="1.858094810993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.069567590885" Y="1.027702671834" />
                  <Point X="29.436021100591" Y="0.620714817852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.864724869845" Y="4.729016378118" />
                  <Point X="27.345693900182" Y="3.084233638951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.070822287205" Y="2.278896977465" />
                  <Point X="28.397480769643" Y="1.916105978794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.183855004367" Y="1.042748912371" />
                  <Point X="29.539009041105" Y="0.648310394478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.02806862975" Y="4.689580026384" />
                  <Point X="27.395638375404" Y="3.170739951959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.154932720747" Y="2.327458149583" />
                  <Point X="28.473082388853" Y="1.974117146595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.298142417848" Y="1.057795152908" />
                  <Point X="29.641996981619" Y="0.675905971104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.191412436434" Y="4.650143622698" />
                  <Point X="27.445582850627" Y="3.257246264968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.239043154289" Y="2.376019321701" />
                  <Point X="28.548684008064" Y="2.032128314396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.41242983133" Y="1.072841393445" />
                  <Point X="29.744984922132" Y="0.703501547729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.354756243118" Y="4.610707219012" />
                  <Point X="27.495527325849" Y="3.343752577976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.323153587831" Y="2.424580493818" />
                  <Point X="28.624285627274" Y="2.090139482197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.526717244812" Y="1.087887633982" />
                  <Point X="29.76723663781" Y="0.820763786059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.528558610973" Y="4.559655406403" />
                  <Point X="27.545471801072" Y="3.430258890984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.407264021373" Y="2.473141665936" />
                  <Point X="28.699887246484" Y="2.148150649998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.641004658293" Y="1.102933874519" />
                  <Point X="29.731104994329" Y="1.002867313727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.71838929848" Y="4.490802341396" />
                  <Point X="27.595416276295" Y="3.516765203993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.491374454915" Y="2.521702838054" />
                  <Point X="28.775488865694" Y="2.206161817799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.916378483247" Y="4.412888347231" />
                  <Point X="27.645360751517" Y="3.603271517001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.575484888456" Y="2.570264010172" />
                  <Point X="28.851090484904" Y="2.2641729856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.137198748219" Y="4.309617869662" />
                  <Point X="27.69530522674" Y="3.689777830009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.659595321998" Y="2.618825182289" />
                  <Point X="28.926692104114" Y="2.322184153401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.381432653364" Y="4.180343910299" />
                  <Point X="27.745249701963" Y="3.776284143018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.74370575554" Y="2.667386354407" />
                  <Point X="29.002293723324" Y="2.380195321202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.657037623503" Y="4.016228853551" />
                  <Point X="27.795194177185" Y="3.862790456026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.827816189082" Y="2.715947526525" />
                  <Point X="29.077895342534" Y="2.438206489003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.911926622624" Y="2.764508698643" />
                  <Point X="29.079324162788" Y="2.578594895585" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.519052734375" Y="-3.71443359375" />
                  <Point X="25.471541015625" Y="-3.537112548828" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.444900390625" Y="-3.493563964844" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.24428515625" Y="-3.257072998047" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.96128515625" Y="-3.197093261719" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.692302734375" Y="-3.313623779297" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112060547" />
                  <Point X="24.187705078125" Y="-4.854780761719" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.866140625" Y="-4.929416992188" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.685130859375" Y="-4.594483398438" />
                  <Point X="23.6908515625" Y="-4.551040039062" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.683138671875" Y="-4.498665039062" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.58605078125" Y="-4.178365234375" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.3140390625" Y="-3.983356201172" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.9795234375" Y="-3.994235839844" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157293945313" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.52925" Y="-4.406044433594" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="22.09762109375" Y="-4.131771484375" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.405669921875" Y="-2.782055175781" />
                  <Point X="22.493966796875" Y="-2.629119384766" />
                  <Point X="22.500236328125" Y="-2.597588867188" />
                  <Point X="22.48395703125" Y="-2.566700439453" />
                  <Point X="22.46867578125" Y="-2.551417724609" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.271853515625" Y="-3.199607177734" />
                  <Point X="21.157041015625" Y="-3.26589453125" />
                  <Point X="21.14253125" Y="-3.24683203125" />
                  <Point X="20.83830078125" Y="-2.847135742188" />
                  <Point X="20.804927734375" Y="-2.791173828125" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="21.681720703125" Y="-1.541690917969" />
                  <Point X="21.836212890625" Y="-1.423144897461" />
                  <Point X="21.84746484375" Y="-1.411081787109" />
                  <Point X="21.855091796875" Y="-1.392485717773" />
                  <Point X="21.8618828125" Y="-1.366266601562" />
                  <Point X="21.85967578125" Y="-1.334595703125" />
                  <Point X="21.835701171875" Y="-1.308790283203" />
                  <Point X="21.812359375" Y="-1.295052490234" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.345794921875" Y="-1.477449951172" />
                  <Point X="20.196716796875" Y="-1.497076416016" />
                  <Point X="20.191595703125" Y="-1.477025878906" />
                  <Point X="20.072607421875" Y="-1.011187683105" />
                  <Point X="20.063779296875" Y="-0.94946307373" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="21.266546875" Y="-0.175801742554" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.4613984375" Y="-0.119140266418" />
                  <Point X="21.485859375" Y="-0.102162872314" />
                  <Point X="21.49767578125" Y="-0.090647277832" />
                  <Point X="21.506201171875" Y="-0.072369987488" />
                  <Point X="21.514353515625" Y="-0.04609859848" />
                  <Point X="21.51325390625" Y="-0.012919065475" />
                  <Point X="21.505103515625" Y="0.013345396042" />
                  <Point X="21.49767578125" Y="0.028087657928" />
                  <Point X="21.48256640625" Y="0.041887428284" />
                  <Point X="21.45810546875" Y="0.058864818573" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.13476171875" Y="0.416502929688" />
                  <Point X="20.0018125" Y="0.452126220703" />
                  <Point X="20.00566796875" Y="0.478177093506" />
                  <Point X="20.08235546875" Y="0.996414978027" />
                  <Point X="20.10012890625" Y="1.062006103516" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="21.12058203125" Y="1.410588745117" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.27558203125" Y="1.398163818359" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056518555" />
                  <Point X="21.3608828125" Y="1.443786743164" />
                  <Point X="21.363806640625" Y="1.450844726562" />
                  <Point X="21.385529296875" Y="1.503291137695" />
                  <Point X="21.389287109375" Y="1.524606079102" />
                  <Point X="21.38368359375" Y="1.545512084961" />
                  <Point X="21.38015625" Y="1.552288330078" />
                  <Point X="21.353943359375" Y="1.602641845703" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.589888671875" Y="2.194827148438" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.542009765625" Y="2.276500976562" />
                  <Point X="20.83998828125" Y="2.787007568359" />
                  <Point X="20.887064453125" Y="2.847519042969" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.76336328125" Y="2.971676513672" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.8716328125" Y="2.919547119141" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404541016" />
                  <Point X="21.993951171875" Y="2.934606933594" />
                  <Point X="22.04747265625" Y="2.988127929688" />
                  <Point X="22.0591015625" Y="3.006384277344" />
                  <Point X="22.06192578125" Y="3.027845703125" />
                  <Point X="22.0610390625" Y="3.037987548828" />
                  <Point X="22.054443359375" Y="3.113389892578" />
                  <Point X="22.04793359375" Y="3.134032714844" />
                  <Point X="21.715521484375" Y="3.709785888672" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.728142578125" Y="3.776433837891" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.321271484375" Y="4.215525878906" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="23.008455078125" Y="4.318484375" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.060046875" Y="4.267781738281" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.197955078125" Y="4.227123046875" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.29448828125" />
                  <Point X="23.31774609375" Y="4.306631347656" />
                  <Point X="23.346197265625" Y="4.396864746094" />
                  <Point X="23.348083984375" Y="4.418424804688" />
                  <Point X="23.310861328125" Y="4.701140136719" />
                  <Point X="23.3600546875" Y="4.714932128906" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.12179296875" Y="4.913815917969" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.934939453125" Y="4.396444824219" />
                  <Point X="24.957859375" Y="4.310903808594" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176269531" />
                  <Point X="25.054453125" Y="4.310902832031" />
                  <Point X="25.22477734375" Y="4.946560058594" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.27358203125" Y="4.987000976562" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="25.93457421875" Y="4.907611328125" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.55652734375" Y="4.751622558594" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="26.977841796875" Y="4.593895507812" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.38392578125" Y="4.398786132812" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.775171875" Y="4.16536328125" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.332052734375" Y="2.680609863281" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.222306640625" Y="2.481992431641" />
                  <Point X="27.2033828125" Y="2.411228759766" />
                  <Point X="27.2030390625" Y="2.384092529297" />
                  <Point X="27.210416015625" Y="2.322902099609" />
                  <Point X="27.218681640625" Y="2.300812011719" />
                  <Point X="27.22377734375" Y="2.293302978516" />
                  <Point X="27.261640625" Y="2.237503417969" />
                  <Point X="27.28244921875" Y="2.219108154297" />
                  <Point X="27.33825" Y="2.181245849609" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.368572265625" Y="2.171987060547" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4581875" Y="2.168492919922" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.85640625" Y="2.951847412109" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="28.995814453125" Y="3.029253662109" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.226369140625" Y="2.702591064453" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.423169921875" Y="1.696328857422" />
                  <Point X="28.2886171875" Y="1.593082641602" />
                  <Point X="28.272517578125" Y="1.574891845703" />
                  <Point X="28.22158984375" Y="1.508451538086" />
                  <Point X="28.210568359375" Y="1.482372314453" />
                  <Point X="28.191595703125" Y="1.414536621094" />
                  <Point X="28.190779296875" Y="1.39096472168" />
                  <Point X="28.192875" Y="1.380807739258" />
                  <Point X="28.20844921875" Y="1.30533190918" />
                  <Point X="28.22134765625" Y="1.279291137695" />
                  <Point X="28.263705078125" Y="1.214909667969" />
                  <Point X="28.280947265625" Y="1.19882019043" />
                  <Point X="28.289208984375" Y="1.194170288086" />
                  <Point X="28.35058984375" Y="1.159617675781" />
                  <Point X="28.379734375" Y="1.152143554688" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.725318359375" Y="1.305673583984" />
                  <Point X="29.848974609375" Y="1.32195324707" />
                  <Point X="29.85037890625" Y="1.316188232422" />
                  <Point X="29.93919140625" Y="0.951367492676" />
                  <Point X="29.946681640625" Y="0.903263122559" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="28.895369140625" Y="0.279144836426" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.718115234375" Y="0.226476867676" />
                  <Point X="28.636578125" Y="0.179346832275" />
                  <Point X="28.615681640625" Y="0.158537322998" />
                  <Point X="28.566759765625" Y="0.09619871521" />
                  <Point X="28.556986328125" Y="0.074734649658" />
                  <Point X="28.554791015625" Y="0.063275733948" />
                  <Point X="28.538484375" Y="-0.021875907898" />
                  <Point X="28.5406796875" Y="-0.052143241882" />
                  <Point X="28.556986328125" Y="-0.137294723511" />
                  <Point X="28.566759765625" Y="-0.158758789063" />
                  <Point X="28.57334375" Y="-0.167147857666" />
                  <Point X="28.622265625" Y="-0.229486465454" />
                  <Point X="28.64755078125" Y="-0.248249404907" />
                  <Point X="28.729087890625" Y="-0.295379425049" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.887029296875" Y="-0.607419616699" />
                  <Point X="29.998068359375" Y="-0.637172363281" />
                  <Point X="29.99801953125" Y="-0.637497253418" />
                  <Point X="29.948431640625" Y="-0.966412841797" />
                  <Point X="29.938833984375" Y="-1.008463684082" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.5908125" Y="-1.121171508789" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.373302734375" Y="-1.103021972656" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.1854453125" Y="-1.154697387695" />
                  <Point X="28.172427734375" Y="-1.170352416992" />
                  <Point X="28.075701171875" Y="-1.286685424805" />
                  <Point X="28.064359375" Y="-1.314070556641" />
                  <Point X="28.062494140625" Y="-1.334344726563" />
                  <Point X="28.048630859375" Y="-1.485000854492" />
                  <Point X="28.056361328125" Y="-1.516621826172" />
                  <Point X="28.068279296875" Y="-1.535159667969" />
                  <Point X="28.156841796875" Y="-1.672912475586" />
                  <Point X="28.168462890625" Y="-1.685540771484" />
                  <Point X="29.231828125" Y="-2.501490722656" />
                  <Point X="29.339076171875" Y="-2.583784423828" />
                  <Point X="29.2041328125" Y="-2.802140869141" />
                  <Point X="29.18428125" Y="-2.830348876953" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="27.9127265625" Y="-2.351171386719" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.7117109375" Y="-2.248684082031" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.46778515625" Y="-2.230450927734" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.33468359375" />
                  <Point X="27.27739453125" Y="-2.355976318359" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.1891640625" Y="-2.546374755859" />
                  <Point X="27.19379296875" Y="-2.572005126953" />
                  <Point X="27.22819140625" Y="-2.762465087891" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.9174140625" Y="-3.962123291016" />
                  <Point X="27.98667578125" Y="-4.082088623047" />
                  <Point X="27.8353125" Y="-4.190202636719" />
                  <Point X="27.81310546875" Y="-4.204578125" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="26.80471875" Y="-3.150479980469" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.645271484375" Y="-2.964215332031" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.398158203125" Y="-2.838261230469" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.143986328125" Y="-2.886259033203" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045986083984" />
                  <Point X="25.96207421875" Y="-3.075352539062" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310719970703" />
                  <Point X="26.107578125" Y="-4.781619628906" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="25.994349609375" Y="-4.963246582031" />
                  <Point X="25.97383984375" Y="-4.96697265625" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#128" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.022920783437" Y="4.440666006273" Z="0.3" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.3" />
                  <Point X="-0.890176654647" Y="4.994757309884" Z="0.3" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.3" />
                  <Point X="-1.659601239299" Y="4.794355896753" Z="0.3" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.3" />
                  <Point X="-1.74543731072" Y="4.73023516653" Z="0.3" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.3" />
                  <Point X="-1.736096496222" Y="4.352946996444" Z="0.3" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.3" />
                  <Point X="-1.827336009275" Y="4.304597030641" Z="0.3" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.3" />
                  <Point X="-1.923021593809" Y="4.343412201105" Z="0.3" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.3" />
                  <Point X="-1.958034232034" Y="4.380202577259" Z="0.3" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.3" />
                  <Point X="-2.709168484006" Y="4.290513274447" Z="0.3" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.3" />
                  <Point X="-3.308435598382" Y="3.84739381736" Z="0.3" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.3" />
                  <Point X="-3.333936054119" Y="3.716066253979" Z="0.3" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.3" />
                  <Point X="-2.994927771378" Y="3.064910584831" Z="0.3" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.3" />
                  <Point X="-3.04756112162" Y="3.001242446267" Z="0.3" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.3" />
                  <Point X="-3.130165880776" Y="3.000636927795" Z="0.3" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.3" />
                  <Point X="-3.217793094318" Y="3.046257894626" Z="0.3" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.3" />
                  <Point X="-4.158555515522" Y="2.909501502997" Z="0.3" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.3" />
                  <Point X="-4.507329198" Y="2.332986107535" Z="0.3" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.3" />
                  <Point X="-4.446705943642" Y="2.186439527258" Z="0.3" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.3" />
                  <Point X="-3.670349884955" Y="1.560480553132" Z="0.3" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.3" />
                  <Point X="-3.688546464969" Y="1.501257926128" Z="0.3" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.3" />
                  <Point X="-3.745610298701" Y="1.477129727547" Z="0.3" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.3" />
                  <Point X="-3.879049898843" Y="1.491441006393" Z="0.3" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.3" />
                  <Point X="-4.95428785543" Y="1.106363588244" Z="0.3" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.3" />
                  <Point X="-5.049949098396" Y="0.516776252241" Z="0.3" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.3" />
                  <Point X="-4.884337177039" Y="0.399486678799" Z="0.3" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.3" />
                  <Point X="-3.552099634682" Y="0.032091631074" Z="0.3" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.3" />
                  <Point X="-3.540654018987" Y="0.003535409559" Z="0.3" />
                  <Point X="-3.539556741714" Y="0" Z="0.3" />
                  <Point X="-3.547710582148" Y="-0.026271541502" Z="0.3" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.3" />
                  <Point X="-3.573268960442" Y="-0.04678435206" Z="0.3" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.3" />
                  <Point X="-3.752550611733" Y="-0.096225377957" Z="0.3" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.3" />
                  <Point X="-4.991873627245" Y="-0.925262157864" Z="0.3" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.3" />
                  <Point X="-4.863005660257" Y="-1.458119841728" Z="0.3" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.3" />
                  <Point X="-4.653836169148" Y="-1.495742119034" Z="0.3" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.3" />
                  <Point X="-3.195817411136" Y="-1.320601085424" Z="0.3" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.3" />
                  <Point X="-3.199466970005" Y="-1.348668827989" Z="0.3" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.3" />
                  <Point X="-3.354872985421" Y="-1.470743186905" Z="0.3" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.3" />
                  <Point X="-4.244172405535" Y="-2.785503410855" Z="0.3" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.3" />
                  <Point X="-3.903410394573" Y="-3.24583515394" Z="0.3" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.3" />
                  <Point X="-3.709302915994" Y="-3.211628421502" Z="0.3" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.3" />
                  <Point X="-2.557548683636" Y="-2.570781785496" Z="0.3" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.3" />
                  <Point X="-2.643788591666" Y="-2.725775466498" Z="0.3" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.3" />
                  <Point X="-2.93904053648" Y="-4.14010870366" Z="0.3" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.3" />
                  <Point X="-2.503230082172" Y="-4.417274909183" Z="0.3" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.3" />
                  <Point X="-2.424442847897" Y="-4.414778166478" Z="0.3" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.3" />
                  <Point X="-1.998853438567" Y="-4.004528954861" Z="0.3" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.3" />
                  <Point X="-1.69538729161" Y="-4.001969147028" Z="0.3" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.3" />
                  <Point X="-1.45307310646" Y="-4.184677053485" Z="0.3" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.3" />
                  <Point X="-1.372057709795" Y="-4.477140117199" Z="0.3" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.3" />
                  <Point X="-1.370597982061" Y="-4.556675723872" Z="0.3" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.3" />
                  <Point X="-1.152474555018" Y="-4.946559667257" Z="0.3" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.3" />
                  <Point X="-0.853219572698" Y="-5.006862280966" Z="0.3" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.3" />
                  <Point X="-0.77015503697" Y="-4.836441846383" Z="0.3" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.3" />
                  <Point X="-0.272778860908" Y="-3.310852879682" Z="0.3" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.3" />
                  <Point X="-0.030050980702" Y="-3.213566083864" Z="0.3" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.3" />
                  <Point X="0.22330809866" Y="-3.273545961424" Z="0.3" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.3" />
                  <Point X="0.397666998524" Y="-3.490792876086" Z="0.3" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.3" />
                  <Point X="0.464599823431" Y="-3.696094183433" Z="0.3" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.3" />
                  <Point X="0.976619741722" Y="-4.98488723517" Z="0.3" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.3" />
                  <Point X="1.155816663665" Y="-4.946410281962" Z="0.3" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.3" />
                  <Point X="1.150993452488" Y="-4.743813595399" Z="0.3" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.3" />
                  <Point X="1.004777097153" Y="-3.054692447311" Z="0.3" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.3" />
                  <Point X="1.169795837491" Y="-2.893425530001" Z="0.3" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.3" />
                  <Point X="1.396583986774" Y="-2.856770642481" Z="0.3" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.3" />
                  <Point X="1.612075275066" Y="-2.974993451463" Z="0.3" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.3" />
                  <Point X="1.758892950868" Y="-3.149638016126" Z="0.3" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.3" />
                  <Point X="2.834117668655" Y="-4.215272378526" Z="0.3" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.3" />
                  <Point X="3.024066917953" Y="-4.081147405703" Z="0.3" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.3" />
                  <Point X="2.954556967612" Y="-3.905843207782" Z="0.3" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.3" />
                  <Point X="2.23683997045" Y="-2.531838719987" Z="0.3" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.3" />
                  <Point X="2.31548401035" Y="-2.347982867844" Z="0.3" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.3" />
                  <Point X="2.484915510528" Y="-2.243417299617" Z="0.3" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.3" />
                  <Point X="2.696668083526" Y="-2.266608039041" Z="0.3" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.3" />
                  <Point X="2.881570389325" Y="-2.363192532662" Z="0.3" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.3" />
                  <Point X="4.219012041221" Y="-2.827846070676" Z="0.3" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.3" />
                  <Point X="4.380291838421" Y="-2.57095694341" Z="0.3" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.3" />
                  <Point X="4.25610935937" Y="-2.430542978564" Z="0.3" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.3" />
                  <Point X="3.104180500925" Y="-1.476840154961" Z="0.3" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.3" />
                  <Point X="3.106125842423" Y="-1.307646311059" Z="0.3" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.3" />
                  <Point X="3.204718758452" Y="-1.171039316781" Z="0.3" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.3" />
                  <Point X="3.377764471483" Y="-1.120601230245" Z="0.3" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.3" />
                  <Point X="3.578129331931" Y="-1.139463769306" Z="0.3" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.3" />
                  <Point X="4.981423930931" Y="-0.988307502563" Z="0.3" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.3" />
                  <Point X="5.041304538375" Y="-0.613671150005" Z="0.3" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.3" />
                  <Point X="4.893814404841" Y="-0.52784334132" Z="0.3" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.3" />
                  <Point X="3.666415346025" Y="-0.173680584955" Z="0.3" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.3" />
                  <Point X="3.60652030251" Y="-0.104999557712" Z="0.3" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.3" />
                  <Point X="3.583629252982" Y="-0.011458998134" Z="0.3" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.3" />
                  <Point X="3.597742197442" Y="0.085151533107" Z="0.3" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.3" />
                  <Point X="3.648859135891" Y="0.158949180863" Z="0.3" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.3" />
                  <Point X="3.736980068327" Y="0.214468259287" Z="0.3" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.3" />
                  <Point X="3.902153412466" Y="0.262128591449" Z="0.3" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.3" />
                  <Point X="4.989929564077" Y="0.942235324539" Z="0.3" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.3" />
                  <Point X="4.892803934557" Y="1.359294895286" Z="0.3" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.3" />
                  <Point X="4.712635994749" Y="1.386525849076" Z="0.3" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.3" />
                  <Point X="3.380129053303" Y="1.232992534642" Z="0.3" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.3" />
                  <Point X="3.307578814153" Y="1.269021218871" Z="0.3" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.3" />
                  <Point X="3.256961114038" Y="1.33805247489" Z="0.3" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.3" />
                  <Point X="3.235687753231" Y="1.422192278104" Z="0.3" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.3" />
                  <Point X="3.252563024744" Y="1.50018492521" Z="0.3" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.3" />
                  <Point X="3.306044601717" Y="1.575754095203" Z="0.3" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.3" />
                  <Point X="3.447451240243" Y="1.687941307763" Z="0.3" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.3" />
                  <Point X="4.262988685564" Y="2.759757349562" Z="0.3" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.3" />
                  <Point X="4.030244022852" Y="3.089736756195" Z="0.3" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.3" />
                  <Point X="3.825249230333" Y="3.026428704552" Z="0.3" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.3" />
                  <Point X="2.439114428662" Y="2.248075688346" Z="0.3" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.3" />
                  <Point X="2.368401288174" Y="2.252907599765" Z="0.3" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.3" />
                  <Point X="2.304367134131" Y="2.291762867725" Z="0.3" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.3" />
                  <Point X="2.25899573251" Y="2.352657726251" Z="0.3" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.3" />
                  <Point X="2.246522102994" Y="2.421357155659" Z="0.3" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.3" />
                  <Point X="2.264452254312" Y="2.50035514722" Z="0.3" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.3" />
                  <Point X="2.369196648308" Y="2.686889851622" Z="0.3" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.3" />
                  <Point X="2.797991948819" Y="4.23739024871" Z="0.3" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.3" />
                  <Point X="2.402938177215" Y="4.473268872164" Z="0.3" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.3" />
                  <Point X="1.992866807573" Y="4.670467683775" Z="0.3" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.3" />
                  <Point X="1.567418982799" Y="4.829906493772" Z="0.3" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.3" />
                  <Point X="0.940150979976" Y="4.98749479282" Z="0.3" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.3" />
                  <Point X="0.272632066406" Y="5.068009040363" Z="0.3" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.3" />
                  <Point X="0.170323778116" Y="4.990781499678" Z="0.3" />
                  <Point X="0" Y="4.355124473572" Z="0.3" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>