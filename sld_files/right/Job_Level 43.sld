<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#201" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3084" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.894693359375" Y="-4.749287109375" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497141357422" />
                  <Point X="25.542365234375" Y="-3.467376708984" />
                  <Point X="25.40691796875" Y="-3.272222900391" />
                  <Point X="25.37863671875" Y="-3.231476318359" />
                  <Point X="25.356751953125" Y="-3.209020019531" />
                  <Point X="25.33049609375" Y="-3.189776367188" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.092900390625" Y="-3.110617919922" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766357422" />
                  <Point X="24.9913359375" Y="-3.092766357422" />
                  <Point X="24.96317578125" Y="-3.097035644531" />
                  <Point X="24.753580078125" Y="-3.162086669922" />
                  <Point X="24.70981640625" Y="-3.175668945312" />
                  <Point X="24.681814453125" Y="-3.189778320312" />
                  <Point X="24.65555859375" Y="-3.2090234375" />
                  <Point X="24.63367578125" Y="-3.231477783203" />
                  <Point X="24.498228515625" Y="-3.426631347656" />
                  <Point X="24.46994921875" Y="-3.467378173828" />
                  <Point X="24.461814453125" Y="-3.481571044922" />
                  <Point X="24.449009765625" Y="-3.512524658203" />
                  <Point X="24.379818359375" Y="-3.770751708984" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.96815625" Y="-4.854569335938" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.753583984375" Y="-4.802362304688" />
                  <Point X="23.7850390625" Y="-4.563438964844" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.73341796875" Y="-4.264491210938" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.18296484375" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.48338671875" Y="-3.961973388672" />
                  <Point X="23.443095703125" Y="-3.926639404297" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.100857421875" Y="-3.8741796875" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.016580078125" Y="-3.873708984375" />
                  <Point X="22.98553125" Y="-3.882029052734" />
                  <Point X="22.957341796875" Y="-3.894802001953" />
                  <Point X="22.74393359375" Y="-4.037397216797" />
                  <Point X="22.699376953125" Y="-4.067170166016" />
                  <Point X="22.68721484375" Y="-4.076822021484" />
                  <Point X="22.664900390625" Y="-4.099459960938" />
                  <Point X="22.626052734375" Y="-4.150084472656" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.267904296875" Y="-4.132489746094" />
                  <Point X="22.19828515625" Y="-4.0893828125" />
                  <Point X="21.895279296875" Y="-3.856077148438" />
                  <Point X="21.960388671875" Y="-3.743303466797" />
                  <Point X="22.57623828125" Y="-2.676619873047" />
                  <Point X="22.587140625" Y="-2.647655761719" />
                  <Point X="22.593412109375" Y="-2.616131347656" />
                  <Point X="22.59442578125" Y="-2.585198730469" />
                  <Point X="22.585443359375" Y="-2.555581542969" />
                  <Point X="22.571228515625" Y="-2.526753417969" />
                  <Point X="22.55319921875" Y="-2.501593017578" />
                  <Point X="22.53884765625" Y="-2.487240966797" />
                  <Point X="22.5358515625" Y="-2.484244384766" />
                  <Point X="22.510697265625" Y="-2.466218017578" />
                  <Point X="22.4818671875" Y="-2.451998535156" />
                  <Point X="22.45225" Y="-2.443012207031" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="22.1381015625" Y="-2.589782226562" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="20.972138671875" Y="-2.866116455078" />
                  <Point X="20.917142578125" Y="-2.793862304688" />
                  <Point X="20.693857421875" Y="-2.41944921875" />
                  <Point X="20.8165078125" Y="-2.325337402344" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.915419921875" Y="-1.475596069336" />
                  <Point X="21.933384765625" Y="-1.448467041016" />
                  <Point X="21.946142578125" Y="-1.419839111328" />
                  <Point X="21.952517578125" Y="-1.395229980469" />
                  <Point X="21.95385546875" Y="-1.390062133789" />
                  <Point X="21.956654296875" Y="-1.359643188477" />
                  <Point X="21.954443359375" Y="-1.327976196289" />
                  <Point X="21.94744140625" Y="-1.298232543945" />
                  <Point X="21.931357421875" Y="-1.272250732422" />
                  <Point X="21.910525390625" Y="-1.248297241211" />
                  <Point X="21.887029296875" Y="-1.228767333984" />
                  <Point X="21.86512109375" Y="-1.215873046875" />
                  <Point X="21.860546875" Y="-1.213180786133" />
                  <Point X="21.831283203125" Y="-1.20195703125" />
                  <Point X="21.799396484375" Y="-1.195475097656" />
                  <Point X="21.768072265625" Y="-1.194383789062" />
                  <Point X="21.4869140625" Y="-1.231398925781" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.187435546875" Y="-1.076868896484" />
                  <Point X="20.165923828125" Y="-0.992649719238" />
                  <Point X="20.107576171875" Y="-0.584698608398" />
                  <Point X="20.2396484375" Y="-0.549309997559" />
                  <Point X="21.467125" Y="-0.220408309937" />
                  <Point X="21.4825078125" Y="-0.214827438354" />
                  <Point X="21.5122734375" Y="-0.199468978882" />
                  <Point X="21.535232421875" Y="-0.183534179688" />
                  <Point X="21.540021484375" Y="-0.180210571289" />
                  <Point X="21.56247265625" Y="-0.1583309021" />
                  <Point X="21.58172265625" Y="-0.132070251465" />
                  <Point X="21.595833984375" Y="-0.1040625" />
                  <Point X="21.603486328125" Y="-0.079403999329" />
                  <Point X="21.605083984375" Y="-0.074255729675" />
                  <Point X="21.609353515625" Y="-0.046098762512" />
                  <Point X="21.609353515625" Y="-0.016461385727" />
                  <Point X="21.605083984375" Y="0.011695732117" />
                  <Point X="21.597431640625" Y="0.0363540802" />
                  <Point X="21.595833984375" Y="0.041502658844" />
                  <Point X="21.5817265625" Y="0.069503883362" />
                  <Point X="21.562482421875" Y="0.095761192322" />
                  <Point X="21.54002734375" Y="0.117646942139" />
                  <Point X="21.517068359375" Y="0.133581893921" />
                  <Point X="21.50659375" Y="0.139893341064" />
                  <Point X="21.48623046875" Y="0.150440795898" />
                  <Point X="21.467125" Y="0.157848175049" />
                  <Point X="21.210837890625" Y="0.226520568848" />
                  <Point X="20.108185546875" Y="0.521975463867" />
                  <Point X="20.161650390625" Y="0.883296630859" />
                  <Point X="20.17551171875" Y="0.976969055176" />
                  <Point X="20.296451171875" Y="1.423267822266" />
                  <Point X="20.35444921875" Y="1.415632202148" />
                  <Point X="21.2343359375" Y="1.29979284668" />
                  <Point X="21.255015625" Y="1.299341674805" />
                  <Point X="21.27657421875" Y="1.301227416992" />
                  <Point X="21.29686328125" Y="1.305263183594" />
                  <Point X="21.347677734375" Y="1.321285400391" />
                  <Point X="21.3582890625" Y="1.324630615234" />
                  <Point X="21.377220703125" Y="1.3329609375" />
                  <Point X="21.39596484375" Y="1.343782714844" />
                  <Point X="21.41264453125" Y="1.356012329102" />
                  <Point X="21.42628125" Y="1.371561889648" />
                  <Point X="21.438697265625" Y="1.389291992188" />
                  <Point X="21.4486484375" Y="1.407429931641" />
                  <Point X="21.4690390625" Y="1.45665612793" />
                  <Point X="21.473298828125" Y="1.466940185547" />
                  <Point X="21.47908984375" Y="1.486807983398" />
                  <Point X="21.482845703125" Y="1.508119873047" />
                  <Point X="21.4841953125" Y="1.528757324219" />
                  <Point X="21.481048828125" Y="1.549198120117" />
                  <Point X="21.475447265625" Y="1.570101196289" />
                  <Point X="21.467953125" Y="1.589375" />
                  <Point X="21.4433515625" Y="1.63663671875" />
                  <Point X="21.438201171875" Y="1.646526123047" />
                  <Point X="21.426708984375" Y="1.663720092773" />
                  <Point X="21.412798828125" Y="1.680294067383" />
                  <Point X="21.39786328125" Y="1.69458984375" />
                  <Point X="21.250857421875" Y="1.807393066406" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.864986328125" Y="2.641381591797" />
                  <Point X="20.9188515625" Y="2.733664550781" />
                  <Point X="21.24949609375" Y="3.158662597656" />
                  <Point X="21.252943359375" Y="3.156671875" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.92398046875" Y="2.819604492188" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.00098828125" Y="2.826504882812" />
                  <Point X="22.0195390625" Y="2.835654541016" />
                  <Point X="22.03779296875" Y="2.847284179688" />
                  <Point X="22.053921875" Y="2.860229003906" />
                  <Point X="22.104158203125" Y="2.910463867188" />
                  <Point X="22.114646484375" Y="2.920952392578" />
                  <Point X="22.127595703125" Y="2.937086181641" />
                  <Point X="22.139224609375" Y="2.955340576172" />
                  <Point X="22.14837109375" Y="2.973889404297" />
                  <Point X="22.1532890625" Y="2.993978271484" />
                  <Point X="22.156115234375" Y="3.015437255859" />
                  <Point X="22.15656640625" Y="3.03612109375" />
                  <Point X="22.150375" Y="3.106893798828" />
                  <Point X="22.14908203125" Y="3.121670654297" />
                  <Point X="22.145044921875" Y="3.141964355469" />
                  <Point X="22.13853515625" Y="3.162605957031" />
                  <Point X="22.130205078125" Y="3.181533447266" />
                  <Point X="22.065060546875" Y="3.294365234375" />
                  <Point X="21.81666796875" Y="3.724595947266" />
                  <Point X="22.205572265625" Y="4.022764404297" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.829626953125" Y="4.389280273438" />
                  <Point X="22.83296484375" Y="4.391134277344" />
                  <Point X="22.956802734375" Y="4.229745117188" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.083658203125" Y="4.148389648438" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.1193828125" Y="4.132331054688" />
                  <Point X="23.1402890625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.12358203125" />
                  <Point X="23.18137109375" Y="4.124934570312" />
                  <Point X="23.2026875" Y="4.128692871094" />
                  <Point X="23.222548828125" Y="4.134481445312" />
                  <Point X="23.304591796875" Y="4.168465332031" />
                  <Point X="23.32172265625" Y="4.175561035156" />
                  <Point X="23.339857421875" Y="4.185511230469" />
                  <Point X="23.3575859375" Y="4.197925292969" />
                  <Point X="23.37313671875" Y="4.211562011719" />
                  <Point X="23.3853671875" Y="4.228240722656" />
                  <Point X="23.396189453125" Y="4.246983886719" />
                  <Point X="23.404521484375" Y="4.265920410156" />
                  <Point X="23.431224609375" Y="4.350613769531" />
                  <Point X="23.43680078125" Y="4.368296875" />
                  <Point X="23.4408359375" Y="4.388583007812" />
                  <Point X="23.44272265625" Y="4.410146484375" />
                  <Point X="23.442271484375" Y="4.430827148438" />
                  <Point X="23.434865234375" Y="4.48708203125" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.928931640625" Y="4.775762695312" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.6931875" Y="4.885041015625" />
                  <Point X="24.70623046875" Y="4.882942871094" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.179595703125" Y="4.410887207031" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.73800390625" Y="4.84284375" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.375869140625" Y="4.703339355469" />
                  <Point X="26.48102734375" Y="4.677950683594" />
                  <Point X="26.82694140625" Y="4.552485839844" />
                  <Point X="26.89464453125" Y="4.527928710938" />
                  <Point X="27.229376953125" Y="4.37138671875" />
                  <Point X="27.294576171875" Y="4.340894042969" />
                  <Point X="27.617962890625" Y="4.152489257813" />
                  <Point X="27.680978515625" Y="4.115775878906" />
                  <Point X="27.94326171875" Y="3.929254150391" />
                  <Point X="27.8603828125" Y="3.785704589844" />
                  <Point X="27.147583984375" Y="2.551098144531" />
                  <Point X="27.142080078125" Y="2.539937988281" />
                  <Point X="27.133078125" Y="2.516059082031" />
                  <Point X="27.11531640625" Y="2.449640625" />
                  <Point X="27.11319921875" Y="2.43955859375" />
                  <Point X="27.10815234375" Y="2.406789306641" />
                  <Point X="27.107728515625" Y="2.380955078125" />
                  <Point X="27.114654296875" Y="2.323521972656" />
                  <Point X="27.116099609375" Y="2.311530273438" />
                  <Point X="27.121439453125" Y="2.289612792969" />
                  <Point X="27.129705078125" Y="2.267521484375" />
                  <Point X="27.140072265625" Y="2.247469970703" />
                  <Point X="27.175611328125" Y="2.195096435547" />
                  <Point X="27.182013671875" Y="2.186704589844" />
                  <Point X="27.202734375" Y="2.16246875" />
                  <Point X="27.221599609375" Y="2.145592041016" />
                  <Point X="27.27397265625" Y="2.110054443359" />
                  <Point X="27.284908203125" Y="2.102634521484" />
                  <Point X="27.304955078125" Y="2.092270996094" />
                  <Point X="27.32704296875" Y="2.084005615234" />
                  <Point X="27.34896484375" Y="2.078663330078" />
                  <Point X="27.4063984375" Y="2.071737792969" />
                  <Point X="27.417439453125" Y="2.071055175781" />
                  <Point X="27.448333984375" Y="2.070947021484" />
                  <Point X="27.47320703125" Y="2.074171142578" />
                  <Point X="27.539626953125" Y="2.091932373047" />
                  <Point X="27.545314453125" Y="2.093645507812" />
                  <Point X="27.571263671875" Y="2.102355224609" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="27.8463125" Y="2.258972412109" />
                  <Point X="28.967326171875" Y="2.906190429688" />
                  <Point X="29.085890625" Y="2.741412109375" />
                  <Point X="29.12328125" Y="2.689450683594" />
                  <Point X="29.26219921875" Y="2.459883056641" />
                  <Point X="29.16925390625" Y="2.388563476562" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.221427734375" Y="1.660243896484" />
                  <Point X="28.20397265625" Y="1.641626953125" />
                  <Point X="28.156171875" Y="1.579266235352" />
                  <Point X="28.150671875" Y="1.571274780273" />
                  <Point X="28.13222265625" Y="1.541305786133" />
                  <Point X="28.121630859375" Y="1.51708996582" />
                  <Point X="28.10382421875" Y="1.453419311523" />
                  <Point X="28.10010546875" Y="1.440125488281" />
                  <Point X="28.09665234375" Y="1.417826538086" />
                  <Point X="28.0958359375" Y="1.394250488281" />
                  <Point X="28.09773828125" Y="1.371765258789" />
                  <Point X="28.11235546875" Y="1.300923583984" />
                  <Point X="28.114880859375" Y="1.29127722168" />
                  <Point X="28.125130859375" Y="1.259111572266" />
                  <Point X="28.13628125" Y="1.235741577148" />
                  <Point X="28.176037109375" Y="1.175313232422" />
                  <Point X="28.184337890625" Y="1.162696166992" />
                  <Point X="28.198892578125" Y="1.145451049805" />
                  <Point X="28.21613671875" Y="1.129360839844" />
                  <Point X="28.23434765625" Y="1.116034667969" />
                  <Point X="28.2919609375" Y="1.083603637695" />
                  <Point X="28.301361328125" Y="1.078974975586" />
                  <Point X="28.3313671875" Y="1.066205688477" />
                  <Point X="28.356119140625" Y="1.059438476562" />
                  <Point X="28.434015625" Y="1.049143432617" />
                  <Point X="28.43951171875" Y="1.048579101563" />
                  <Point X="28.468853515625" Y="1.046426391602" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="28.73307421875" Y="1.079222290039" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.829880859375" Y="0.998756591797" />
                  <Point X="29.84594140625" Y="0.932786132812" />
                  <Point X="29.8908671875" Y="0.644238708496" />
                  <Point X="29.7920859375" Y="0.617770324707" />
                  <Point X="28.716580078125" Y="0.32958972168" />
                  <Point X="28.704787109375" Y="0.325584594727" />
                  <Point X="28.681546875" Y="0.315067657471" />
                  <Point X="28.605015625" Y="0.270831512451" />
                  <Point X="28.5973046875" Y="0.265862976074" />
                  <Point X="28.567015625" Y="0.244207641602" />
                  <Point X="28.547533203125" Y="0.225577194214" />
                  <Point X="28.501615234375" Y="0.167066253662" />
                  <Point X="28.49202734375" Y="0.154849395752" />
                  <Point X="28.4803046875" Y="0.135571411133" />
                  <Point X="28.470529296875" Y="0.114105369568" />
                  <Point X="28.463681640625" Y="0.092603042603" />
                  <Point X="28.448375" Y="0.012679788589" />
                  <Point X="28.4470625" Y="0.00332843852" />
                  <Point X="28.4438671875" Y="-0.032165313721" />
                  <Point X="28.4451796875" Y="-0.058552555084" />
                  <Point X="28.460486328125" Y="-0.138475662231" />
                  <Point X="28.463681640625" Y="-0.155163192749" />
                  <Point X="28.470529296875" Y="-0.176665512085" />
                  <Point X="28.4803046875" Y="-0.198131561279" />
                  <Point X="28.49202734375" Y="-0.217409393311" />
                  <Point X="28.5379453125" Y="-0.275920501709" />
                  <Point X="28.5444921875" Y="-0.283418029785" />
                  <Point X="28.568390625" Y="-0.308053741455" />
                  <Point X="28.589037109375" Y="-0.32415536499" />
                  <Point X="28.665568359375" Y="-0.36839151001" />
                  <Point X="28.67010546875" Y="-0.370852325439" />
                  <Point X="28.6981640625" Y="-0.385096496582" />
                  <Point X="28.716580078125" Y="-0.392149871826" />
                  <Point X="28.941138671875" Y="-0.452319885254" />
                  <Point X="29.891474609375" Y="-0.706961914062" />
                  <Point X="29.86398828125" Y="-0.889268554688" />
                  <Point X="29.855025390625" Y="-0.948725158691" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="29.671666015625" Y="-1.167648803711" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710144043" />
                  <Point X="28.37466015625" Y="-1.005508728027" />
                  <Point X="28.22445703125" Y="-1.03815625" />
                  <Point X="28.193095703125" Y="-1.04497265625" />
                  <Point X="28.1639765625" Y="-1.056596801758" />
                  <Point X="28.1361484375" Y="-1.073489746094" />
                  <Point X="28.1123984375" Y="-1.093960571289" />
                  <Point X="28.021609375" Y="-1.203150634766" />
                  <Point X="28.002654296875" Y="-1.225948608398" />
                  <Point X="27.987931640625" Y="-1.250334594727" />
                  <Point X="27.97658984375" Y="-1.277719238281" />
                  <Point X="27.969759765625" Y="-1.305365722656" />
                  <Point X="27.956748046875" Y="-1.446771606445" />
                  <Point X="27.95403125" Y="-1.476296142578" />
                  <Point X="27.95634765625" Y="-1.507561767578" />
                  <Point X="27.964078125" Y="-1.539182373047" />
                  <Point X="27.976451171875" Y="-1.567996582031" />
                  <Point X="28.059576171875" Y="-1.697291503906" />
                  <Point X="28.076931640625" Y="-1.724287109375" />
                  <Point X="28.086935546875" Y="-1.737240966797" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.319021484375" Y="-1.920813110352" />
                  <Point X="29.213123046875" Y="-2.606882324219" />
                  <Point X="29.150080078125" Y="-2.708895996094" />
                  <Point X="29.124796875" Y="-2.749804443359" />
                  <Point X="29.028982421875" Y="-2.885945068359" />
                  <Point X="28.911830078125" Y="-2.818306884766" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.575458984375" Y="-2.127540283203" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.29632421875" Y="-2.213336914062" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.24238671875" Y="-2.246547851562" />
                  <Point X="27.221427734375" Y="-2.267506347656" />
                  <Point X="27.204533203125" Y="-2.290437744141" />
                  <Point X="27.126373046875" Y="-2.438947998047" />
                  <Point X="27.110052734375" Y="-2.469955810547" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531911132812" />
                  <Point X="27.09567578125" Y="-2.563260253906" />
                  <Point X="27.1279609375" Y="-2.742025634766" />
                  <Point X="27.134703125" Y="-2.779350585938" />
                  <Point X="27.13898828125" Y="-2.795144287109" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.285734375" Y="-3.058020996094" />
                  <Point X="27.861287109375" Y="-4.054906005859" />
                  <Point X="27.81183984375" Y="-4.090224121094" />
                  <Point X="27.78184375" Y="-4.111648925781" />
                  <Point X="27.701767578125" Y="-4.163481933594" />
                  <Point X="27.606248046875" Y="-4.038998535156" />
                  <Point X="26.758548828125" Y="-2.934255371094" />
                  <Point X="26.74750390625" Y="-2.922177490234" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.54561328125" Y="-2.787205322266" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479990234375" Y="-2.751167236328" />
                  <Point X="26.448369140625" Y="-2.743435546875" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.2242734375" Y="-2.758860839844" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397949219" />
                  <Point X="26.128978515625" Y="-2.780741455078" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="25.955701171875" Y="-2.919262695313" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.90414453125" Y="-2.968858886719" />
                  <Point X="25.887251953125" Y="-2.996683349609" />
                  <Point X="25.875625" Y="-3.025807373047" />
                  <Point X="25.831107421875" Y="-3.230629638672" />
                  <Point X="25.821810546875" Y="-3.273395263672" />
                  <Point X="25.819724609375" Y="-3.289626220703" />
                  <Point X="25.8197421875" Y="-3.323120605469" />
                  <Point X="25.857693359375" Y="-3.611376708984" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="26.004046875" Y="-4.863865234375" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.9862578125" Y="-4.761310058594" />
                  <Point X="23.94157421875" Y="-4.75263671875" />
                  <Point X="23.858755859375" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575838867188" />
                  <Point X="23.879923828125" Y="-4.568099121094" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688964844" />
                  <Point X="23.826591796875" Y="-4.24595703125" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.811873046875" Y="-4.178467773438" />
                  <Point X="23.800970703125" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135463867188" />
                  <Point X="23.778259765625" Y="-4.107626464844" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.74979296875" Y="-4.070937744141" />
                  <Point X="23.738994140625" Y="-4.059779541016" />
                  <Point X="23.546025390625" Y="-3.890548828125" />
                  <Point X="23.505734375" Y="-3.85521484375" />
                  <Point X="23.49326171875" Y="-3.845965332031" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.1070703125" Y="-3.779383056641" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.007267578125" Y="-3.779166503906" />
                  <Point X="22.991990234375" Y="-3.781946533203" />
                  <Point X="22.96094140625" Y="-3.790266601562" />
                  <Point X="22.946322265625" Y="-3.795497558594" />
                  <Point X="22.9181328125" Y="-3.808270507812" />
                  <Point X="22.9045625" Y="-3.8158125" />
                  <Point X="22.691154296875" Y="-3.958407714844" />
                  <Point X="22.64659765625" Y="-3.988180664062" />
                  <Point X="22.640322265625" Y="-3.992755859375" />
                  <Point X="22.61955859375" Y="-4.010132080078" />
                  <Point X="22.597244140625" Y="-4.032770019531" />
                  <Point X="22.589533203125" Y="-4.041625732422" />
                  <Point X="22.550685546875" Y="-4.092250244141" />
                  <Point X="22.496796875" Y="-4.162478515625" />
                  <Point X="22.317916015625" Y="-4.051719238281" />
                  <Point X="22.25240625" Y="-4.01115625" />
                  <Point X="22.019138671875" Y="-3.831547607422" />
                  <Point X="22.042662109375" Y="-3.790803222656" />
                  <Point X="22.65851171875" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.710086425781" />
                  <Point X="22.67605078125" Y="-2.681122314453" />
                  <Point X="22.680314453125" Y="-2.666191894531" />
                  <Point X="22.6865859375" Y="-2.634667480469" />
                  <Point X="22.688361328125" Y="-2.619242919922" />
                  <Point X="22.689375" Y="-2.588310302734" />
                  <Point X="22.6853359375" Y="-2.557626953125" />
                  <Point X="22.676353515625" Y="-2.528009765625" />
                  <Point X="22.6706484375" Y="-2.513567871094" />
                  <Point X="22.65643359375" Y="-2.484739746094" />
                  <Point X="22.64844921875" Y="-2.471418945312" />
                  <Point X="22.630419921875" Y="-2.446258544922" />
                  <Point X="22.620375" Y="-2.434418945313" />
                  <Point X="22.6060234375" Y="-2.420066894531" />
                  <Point X="22.591189453125" Y="-2.407025390625" />
                  <Point X="22.56603515625" Y="-2.388999023438" />
                  <Point X="22.55271875" Y="-2.381017578125" />
                  <Point X="22.523888671875" Y="-2.366798095703" />
                  <Point X="22.50944921875" Y="-2.361091064453" />
                  <Point X="22.47983203125" Y="-2.352104736328" />
                  <Point X="22.44914453125" Y="-2.348062988281" />
                  <Point X="22.41820703125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="22.0906015625" Y="-2.507509765625" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="21.047732421875" Y="-2.808578369141" />
                  <Point X="20.99598046875" Y="-2.740587402344" />
                  <Point X="20.818734375" Y="-2.443373291016" />
                  <Point X="20.87433984375" Y="-2.400706298828" />
                  <Point X="21.951876953125" Y="-1.573882324219" />
                  <Point X="21.963517578125" Y="-1.563310180664" />
                  <Point X="21.984892578125" Y="-1.540392578125" />
                  <Point X="21.994626953125" Y="-1.528047363281" />
                  <Point X="22.012591796875" Y="-1.500918334961" />
                  <Point X="22.020158203125" Y="-1.487137084961" />
                  <Point X="22.032916015625" Y="-1.458509033203" />
                  <Point X="22.038107421875" Y="-1.443662475586" />
                  <Point X="22.044482421875" Y="-1.419053344727" />
                  <Point X="22.044486328125" Y="-1.419039306641" />
                  <Point X="22.048455078125" Y="-1.398766357422" />
                  <Point X="22.05125390625" Y="-1.368347290039" />
                  <Point X="22.051423828125" Y="-1.353026611328" />
                  <Point X="22.049212890625" Y="-1.321359619141" />
                  <Point X="22.046916015625" Y="-1.306207275391" />
                  <Point X="22.0399140625" Y="-1.276463623047" />
                  <Point X="22.028216796875" Y="-1.248228881836" />
                  <Point X="22.0121328125" Y="-1.222247070313" />
                  <Point X="22.003041015625" Y="-1.209908813477" />
                  <Point X="21.982208984375" Y="-1.185955444336" />
                  <Point X="21.97125" Y="-1.175239501953" />
                  <Point X="21.94775390625" Y="-1.155709594727" />
                  <Point X="21.935216796875" Y="-1.146895263672" />
                  <Point X="21.91330859375" Y="-1.134000854492" />
                  <Point X="21.89456640625" Y="-1.124480957031" />
                  <Point X="21.865302734375" Y="-1.113257202148" />
                  <Point X="21.85020703125" Y="-1.108861083984" />
                  <Point X="21.8183203125" Y="-1.102379150391" />
                  <Point X="21.802705078125" Y="-1.100532714844" />
                  <Point X="21.771380859375" Y="-1.09944140625" />
                  <Point X="21.755671875" Y="-1.100196533203" />
                  <Point X="21.474513671875" Y="-1.137211669922" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.27948046875" Y="-1.053357910156" />
                  <Point X="20.259240234375" Y="-0.974113586426" />
                  <Point X="20.213548828125" Y="-0.654654663086" />
                  <Point X="20.264236328125" Y="-0.641072937012" />
                  <Point X="21.491712890625" Y="-0.312171325684" />
                  <Point X="21.499525390625" Y="-0.309712677002" />
                  <Point X="21.526068359375" Y="-0.299251556396" />
                  <Point X="21.555833984375" Y="-0.283893005371" />
                  <Point X="21.56644140625" Y="-0.277513366699" />
                  <Point X="21.589400390625" Y="-0.261578613281" />
                  <Point X="21.60632421875" Y="-0.24824609375" />
                  <Point X="21.628775390625" Y="-0.226366424561" />
                  <Point X="21.639091796875" Y="-0.214495620728" />
                  <Point X="21.658341796875" Y="-0.18823500061" />
                  <Point X="21.6665625" Y="-0.174815689087" />
                  <Point X="21.680673828125" Y="-0.146807998657" />
                  <Point X="21.686564453125" Y="-0.132219467163" />
                  <Point X="21.694216796875" Y="-0.107560997009" />
                  <Point X="21.699009765625" Y="-0.088498031616" />
                  <Point X="21.703279296875" Y="-0.060341125488" />
                  <Point X="21.704353515625" Y="-0.046098743439" />
                  <Point X="21.704353515625" Y="-0.016461402893" />
                  <Point X="21.703279296875" Y="-0.002219167948" />
                  <Point X="21.699009765625" Y="0.025938035965" />
                  <Point X="21.695814453125" Y="0.039852855682" />
                  <Point X="21.688162109375" Y="0.064511177063" />
                  <Point X="21.680673828125" Y="0.084246665955" />
                  <Point X="21.66656640625" Y="0.112247810364" />
                  <Point X="21.6583515625" Y="0.125662071228" />
                  <Point X="21.639107421875" Y="0.151919433594" />
                  <Point X="21.6287890625" Y="0.163793212891" />
                  <Point X="21.606333984375" Y="0.185678970337" />
                  <Point X="21.5941953125" Y="0.195691101074" />
                  <Point X="21.571236328125" Y="0.21162600708" />
                  <Point X="21.550287109375" Y="0.224249145508" />
                  <Point X="21.529923828125" Y="0.234796615601" />
                  <Point X="21.520572265625" Y="0.239016464233" />
                  <Point X="21.501466796875" Y="0.246423828125" />
                  <Point X="21.491712890625" Y="0.249611038208" />
                  <Point X="21.23542578125" Y="0.318283447266" />
                  <Point X="20.2145546875" Y="0.591824890137" />
                  <Point X="20.255626953125" Y="0.869390808105" />
                  <Point X="20.26866796875" Y="0.957521606445" />
                  <Point X="20.366416015625" Y="1.318236938477" />
                  <Point X="21.221935546875" Y="1.20560559082" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364257812" />
                  <Point X="21.26329296875" Y="1.20470300293" />
                  <Point X="21.2848515625" Y="1.206588745117" />
                  <Point X="21.295107421875" Y="1.208052734375" />
                  <Point X="21.315396484375" Y="1.212088623047" />
                  <Point X="21.325431640625" Y="1.21466027832" />
                  <Point X="21.37624609375" Y="1.230682495117" />
                  <Point X="21.39655078125" Y="1.237676391602" />
                  <Point X="21.415482421875" Y="1.246006713867" />
                  <Point X="21.424720703125" Y="1.250688232422" />
                  <Point X="21.44346484375" Y="1.261510009766" />
                  <Point X="21.452138671875" Y="1.267169677734" />
                  <Point X="21.468818359375" Y="1.279399169922" />
                  <Point X="21.484068359375" Y="1.293374023438" />
                  <Point X="21.497705078125" Y="1.308923706055" />
                  <Point X="21.50409765625" Y="1.317068481445" />
                  <Point X="21.516513671875" Y="1.334798339844" />
                  <Point X="21.521986328125" Y="1.343596923828" />
                  <Point X="21.5319375" Y="1.361734863281" />
                  <Point X="21.536416015625" Y="1.37107421875" />
                  <Point X="21.556806640625" Y="1.420300537109" />
                  <Point X="21.56450390625" Y="1.440356079102" />
                  <Point X="21.570294921875" Y="1.460223876953" />
                  <Point X="21.5726484375" Y="1.470319946289" />
                  <Point X="21.576404296875" Y="1.491631835938" />
                  <Point X="21.577642578125" Y="1.501920532227" />
                  <Point X="21.5789921875" Y="1.522557983398" />
                  <Point X="21.57808984375" Y="1.543210449219" />
                  <Point X="21.574943359375" Y="1.563651367188" />
                  <Point X="21.572810546875" Y="1.573788330078" />
                  <Point X="21.567208984375" Y="1.59469140625" />
                  <Point X="21.563990234375" Y="1.604528808594" />
                  <Point X="21.55649609375" Y="1.623802490234" />
                  <Point X="21.552220703125" Y="1.633239257813" />
                  <Point X="21.527619140625" Y="1.680500854492" />
                  <Point X="21.527609375" Y="1.680518188477" />
                  <Point X="21.51718359375" Y="1.69931652832" />
                  <Point X="21.50569140625" Y="1.716510498047" />
                  <Point X="21.4994765625" Y="1.724792480469" />
                  <Point X="21.48556640625" Y="1.741366455078" />
                  <Point X="21.47848828125" Y="1.748922973633" />
                  <Point X="21.463552734375" Y="1.76321875" />
                  <Point X="21.4556953125" Y="1.769958129883" />
                  <Point X="21.308689453125" Y="1.882761230469" />
                  <Point X="20.77238671875" Y="2.294280517578" />
                  <Point X="20.947033203125" Y="2.593491943359" />
                  <Point X="20.997716796875" Y="2.680322265625" />
                  <Point X="21.273662109375" Y="3.035013427734" />
                  <Point X="21.7458359375" Y="2.762403076172" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.915701171875" Y="2.724966064453" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310546875" />
                  <Point X="22.023568359375" Y="2.734227294922" />
                  <Point X="22.04301171875" Y="2.7413046875" />
                  <Point X="22.0615625" Y="2.750454345703" />
                  <Point X="22.070583984375" Y="2.755533691406" />
                  <Point X="22.088837890625" Y="2.767163330078" />
                  <Point X="22.097255859375" Y="2.773195068359" />
                  <Point X="22.113384765625" Y="2.786139892578" />
                  <Point X="22.121095703125" Y="2.793052978516" />
                  <Point X="22.17133203125" Y="2.843287841797" />
                  <Point X="22.188734375" Y="2.86148828125" />
                  <Point X="22.20168359375" Y="2.877622070312" />
                  <Point X="22.20771875" Y="2.886043945312" />
                  <Point X="22.21934765625" Y="2.904298339844" />
                  <Point X="22.2244296875" Y="2.913326171875" />
                  <Point X="22.233576171875" Y="2.931875" />
                  <Point X="22.240646484375" Y="2.951299560547" />
                  <Point X="22.245564453125" Y="2.971388427734" />
                  <Point X="22.2474765625" Y="2.981573730469" />
                  <Point X="22.250302734375" Y="3.003032714844" />
                  <Point X="22.251091796875" Y="3.013365478516" />
                  <Point X="22.25154296875" Y="3.034049316406" />
                  <Point X="22.251205078125" Y="3.044400390625" />
                  <Point X="22.245013671875" Y="3.115173095703" />
                  <Point X="22.242255859375" Y="3.140206298828" />
                  <Point X="22.23821875" Y="3.1605" />
                  <Point X="22.235646484375" Y="3.170537353516" />
                  <Point X="22.22913671875" Y="3.191178955078" />
                  <Point X="22.225486328125" Y="3.200873779297" />
                  <Point X="22.21715625" Y="3.219801269531" />
                  <Point X="22.2124765625" Y="3.229033935547" />
                  <Point X="22.14733203125" Y="3.341865722656" />
                  <Point X="21.94061328125" Y="3.699914794922" />
                  <Point X="22.263375" Y="3.947372558594" />
                  <Point X="22.351634765625" Y="4.015041992188" />
                  <Point X="22.8074765625" Y="4.268296875" />
                  <Point X="22.88143359375" Y="4.171913085938" />
                  <Point X="22.888171875" Y="4.164057128906" />
                  <Point X="22.9024765625" Y="4.149111816406" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93491015625" Y="4.121895996094" />
                  <Point X="22.952111328125" Y="4.110403320312" />
                  <Point X="22.9610234375" Y="4.105128417969" />
                  <Point X="23.03979296875" Y="4.064123535156" />
                  <Point X="23.065673828125" Y="4.051286865234" />
                  <Point X="23.084951171875" Y="4.043790283203" />
                  <Point X="23.094794921875" Y="4.040568359375" />
                  <Point X="23.115701171875" Y="4.034966308594" />
                  <Point X="23.1258359375" Y="4.032834960938" />
                  <Point X="23.146279296875" Y="4.029687988281" />
                  <Point X="23.1669453125" Y="4.028785400391" />
                  <Point X="23.187583984375" Y="4.030137939453" />
                  <Point X="23.197865234375" Y="4.031377685547" />
                  <Point X="23.219181640625" Y="4.035135986328" />
                  <Point X="23.22926953125" Y="4.037487548828" />
                  <Point X="23.249130859375" Y="4.043276123047" />
                  <Point X="23.258904296875" Y="4.046713134766" />
                  <Point X="23.340947265625" Y="4.080697021484" />
                  <Point X="23.367419921875" Y="4.092274169922" />
                  <Point X="23.3855546875" Y="4.102224121094" />
                  <Point X="23.39434765625" Y="4.107692871094" />
                  <Point X="23.412076171875" Y="4.120106933594" />
                  <Point X="23.420220703125" Y="4.126498535156" />
                  <Point X="23.435771484375" Y="4.140135253906" />
                  <Point X="23.44974609375" Y="4.155384277344" />
                  <Point X="23.4619765625" Y="4.172062988281" />
                  <Point X="23.467638671875" Y="4.180737792969" />
                  <Point X="23.4784609375" Y="4.199480957031" />
                  <Point X="23.48314453125" Y="4.208723632812" />
                  <Point X="23.4914765625" Y="4.22766015625" />
                  <Point X="23.495125" Y="4.237354003906" />
                  <Point X="23.521828125" Y="4.322047363281" />
                  <Point X="23.529974609375" Y="4.349763183594" />
                  <Point X="23.534009765625" Y="4.370049316406" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401866210938" />
                  <Point X="23.53769921875" Y="4.41221875" />
                  <Point X="23.537248046875" Y="4.432899414062" />
                  <Point X="23.536458984375" Y="4.443227539062" />
                  <Point X="23.529052734375" Y="4.499482421875" />
                  <Point X="23.520736328125" Y="4.562655273438" />
                  <Point X="23.954578125" Y="4.684290039062" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.271359375" Y="4.386299316406" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.728109375" Y="4.748360351562" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.35357421875" Y="4.610992675781" />
                  <Point X="26.453595703125" Y="4.586844238281" />
                  <Point X="26.794548828125" Y="4.463178710938" />
                  <Point X="26.858255859375" Y="4.440070800781" />
                  <Point X="27.1891328125" Y="4.28533203125" />
                  <Point X="27.250453125" Y="4.256653320312" />
                  <Point X="27.570140625" Y="4.070404052734" />
                  <Point X="27.6294296875" Y="4.035861572266" />
                  <Point X="27.81778125" Y="3.901915771484" />
                  <Point X="27.778111328125" Y="3.833204833984" />
                  <Point X="27.0653125" Y="2.598598388672" />
                  <Point X="27.0623828125" Y="2.593117675781" />
                  <Point X="27.0531875" Y="2.57344921875" />
                  <Point X="27.044185546875" Y="2.5495703125" />
                  <Point X="27.041302734375" Y="2.540601806641" />
                  <Point X="27.023541015625" Y="2.474183349609" />
                  <Point X="27.019306640625" Y="2.454019287109" />
                  <Point X="27.014259765625" Y="2.42125" />
                  <Point X="27.013166015625" Y="2.40834765625" />
                  <Point X="27.0127421875" Y="2.382513427734" />
                  <Point X="27.013412109375" Y="2.369581542969" />
                  <Point X="27.020337890625" Y="2.3121484375" />
                  <Point X="27.023798828125" Y="2.289042724609" />
                  <Point X="27.029138671875" Y="2.267125244141" />
                  <Point X="27.032462890625" Y="2.256321777344" />
                  <Point X="27.040728515625" Y="2.23423046875" />
                  <Point X="27.04531640625" Y="2.223890625" />
                  <Point X="27.05568359375" Y="2.203839111328" />
                  <Point X="27.061462890625" Y="2.194127441406" />
                  <Point X="27.097001953125" Y="2.14175390625" />
                  <Point X="27.109806640625" Y="2.124970214844" />
                  <Point X="27.13052734375" Y="2.100734375" />
                  <Point X="27.13939453125" Y="2.091665771484" />
                  <Point X="27.158259765625" Y="2.0747890625" />
                  <Point X="27.1682578125" Y="2.066980957031" />
                  <Point X="27.220630859375" Y="2.031443481445" />
                  <Point X="27.24128125" Y="2.018244262695" />
                  <Point X="27.261328125" Y="2.007880737305" />
                  <Point X="27.27166015625" Y="2.003296508789" />
                  <Point X="27.293748046875" Y="1.99503112793" />
                  <Point X="27.30455078125" Y="1.991706665039" />
                  <Point X="27.32647265625" Y="1.986364501953" />
                  <Point X="27.337591796875" Y="1.984346557617" />
                  <Point X="27.395025390625" Y="1.977421020508" />
                  <Point X="27.417107421875" Y="1.976055786133" />
                  <Point X="27.448001953125" Y="1.975947631836" />
                  <Point X="27.460546875" Y="1.976735229492" />
                  <Point X="27.485419921875" Y="1.979959472656" />
                  <Point X="27.497748046875" Y="1.982395751953" />
                  <Point X="27.56416796875" Y="2.000156982422" />
                  <Point X="27.57554296875" Y="2.003583374023" />
                  <Point X="27.6014921875" Y="2.01229296875" />
                  <Point X="27.610322265625" Y="2.015755737305" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="27.8938125" Y="2.176699951172" />
                  <Point X="28.940404296875" Y="2.780950195312" />
                  <Point X="29.00877734375" Y="2.685926513672" />
                  <Point X="29.04396484375" Y="2.637025878906" />
                  <Point X="29.13688671875" Y="2.483471191406" />
                  <Point X="29.111421875" Y="2.463931884766" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.168142578125" Y="1.739871337891" />
                  <Point X="28.152125" Y="1.725221679688" />
                  <Point X="28.134669921875" Y="1.706604736328" />
                  <Point X="28.12857421875" Y="1.699421020508" />
                  <Point X="28.0807734375" Y="1.637060424805" />
                  <Point X="28.0697734375" Y="1.621077270508" />
                  <Point X="28.05132421875" Y="1.591108276367" />
                  <Point X="28.04518359375" Y="1.579375610352" />
                  <Point X="28.034591796875" Y="1.555159790039" />
                  <Point X="28.030140625" Y="1.542676635742" />
                  <Point X="28.012333984375" Y="1.479005981445" />
                  <Point X="28.006224609375" Y="1.454663452148" />
                  <Point X="28.002771484375" Y="1.432364624023" />
                  <Point X="28.001708984375" Y="1.421114257812" />
                  <Point X="28.000892578125" Y="1.397538208008" />
                  <Point X="28.001173828125" Y="1.386241699219" />
                  <Point X="28.003076171875" Y="1.363756469727" />
                  <Point X="28.004697265625" Y="1.352567749023" />
                  <Point X="28.019314453125" Y="1.281726074219" />
                  <Point X="28.024365234375" Y="1.262433349609" />
                  <Point X="28.034615234375" Y="1.230267700195" />
                  <Point X="28.039390625" Y="1.218202636719" />
                  <Point X="28.050541015625" Y="1.194832641602" />
                  <Point X="28.056916015625" Y="1.183527832031" />
                  <Point X="28.096671875" Y="1.123099365234" />
                  <Point X="28.11173828125" Y="1.101423339844" />
                  <Point X="28.12629296875" Y="1.084178222656" />
                  <Point X="28.13408203125" Y="1.07599206543" />
                  <Point X="28.151326171875" Y="1.059901855469" />
                  <Point X="28.16003515625" Y="1.05269519043" />
                  <Point X="28.17824609375" Y="1.039369018555" />
                  <Point X="28.187748046875" Y="1.033249511719" />
                  <Point X="28.245361328125" Y="1.000818359375" />
                  <Point X="28.264162109375" Y="0.991561096191" />
                  <Point X="28.29416796875" Y="0.978791870117" />
                  <Point X="28.306314453125" Y="0.974568908691" />
                  <Point X="28.33106640625" Y="0.967801635742" />
                  <Point X="28.343671875" Y="0.965257507324" />
                  <Point X="28.421568359375" Y="0.954962402344" />
                  <Point X="28.432560546875" Y="0.953833740234" />
                  <Point X="28.46190234375" Y="0.951680969238" />
                  <Point X="28.471591796875" Y="0.951465881348" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="28.745474609375" Y="0.98503503418" />
                  <Point X="29.704703125" Y="1.111319824219" />
                  <Point X="29.737576171875" Y="0.976285949707" />
                  <Point X="29.752689453125" Y="0.914204956055" />
                  <Point X="29.783873046875" Y="0.713920959473" />
                  <Point X="29.767498046875" Y="0.709533203125" />
                  <Point X="28.6919921875" Y="0.42135269165" />
                  <Point X="28.686029296875" Y="0.419543487549" />
                  <Point X="28.66562109375" Y="0.41213494873" />
                  <Point X="28.642380859375" Y="0.401617950439" />
                  <Point X="28.634005859375" Y="0.397316345215" />
                  <Point X="28.557474609375" Y="0.353080230713" />
                  <Point X="28.542052734375" Y="0.343143157959" />
                  <Point X="28.511763671875" Y="0.321487762451" />
                  <Point X="28.501359375" Y="0.312867340088" />
                  <Point X="28.481876953125" Y="0.294236846924" />
                  <Point X="28.472798828125" Y="0.284226806641" />
                  <Point X="28.426880859375" Y="0.225715896606" />
                  <Point X="28.410857421875" Y="0.204208236694" />
                  <Point X="28.399134765625" Y="0.184930206299" />
                  <Point X="28.39384765625" Y="0.17494303894" />
                  <Point X="28.384072265625" Y="0.153476989746" />
                  <Point X="28.3800078125" Y="0.142932647705" />
                  <Point X="28.37316015625" Y="0.121430328369" />
                  <Point X="28.370376953125" Y="0.110472366333" />
                  <Point X="28.3550703125" Y="0.030549060822" />
                  <Point X="28.3524453125" Y="0.011846354485" />
                  <Point X="28.34925" Y="-0.023647468567" />
                  <Point X="28.348984375" Y="-0.036884716034" />
                  <Point X="28.350296875" Y="-0.063271968842" />
                  <Point X="28.351875" Y="-0.076421974182" />
                  <Point X="28.367181640625" Y="-0.156344985962" />
                  <Point X="28.37316015625" Y="-0.183990478516" />
                  <Point X="28.3800078125" Y="-0.205492797852" />
                  <Point X="28.384072265625" Y="-0.216037139893" />
                  <Point X="28.39384765625" Y="-0.237503189087" />
                  <Point X="28.399134765625" Y="-0.247490646362" />
                  <Point X="28.410857421875" Y="-0.26676852417" />
                  <Point X="28.41729296875" Y="-0.276058929443" />
                  <Point X="28.4632109375" Y="-0.334570007324" />
                  <Point X="28.4763046875" Y="-0.349565155029" />
                  <Point X="28.500203125" Y="-0.374200744629" />
                  <Point X="28.50996875" Y="-0.382966064453" />
                  <Point X="28.530615234375" Y="-0.399067718506" />
                  <Point X="28.54149609375" Y="-0.406404052734" />
                  <Point X="28.61802734375" Y="-0.450640319824" />
                  <Point X="28.6271015625" Y="-0.455561828613" />
                  <Point X="28.65516015625" Y="-0.469805999756" />
                  <Point X="28.664185546875" Y="-0.473812255859" />
                  <Point X="28.6919921875" Y="-0.483912841797" />
                  <Point X="28.91655078125" Y="-0.544082946777" />
                  <Point X="29.78487890625" Y="-0.776751220703" />
                  <Point X="29.77005078125" Y="-0.875105407715" />
                  <Point X="29.761619140625" Y="-0.931038635254" />
                  <Point X="29.7278046875" Y="-1.079219726563" />
                  <Point X="29.68406640625" Y="-1.073461547852" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042297363" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.204279296875" Y="-0.945323791504" />
                  <Point X="28.17291796875" Y="-0.952140197754" />
                  <Point X="28.157875" Y="-0.956742736816" />
                  <Point X="28.128755859375" Y="-0.968367004395" />
                  <Point X="28.1146796875" Y="-0.975388549805" />
                  <Point X="28.0868515625" Y="-0.992281494141" />
                  <Point X="28.074125" Y="-1.001530883789" />
                  <Point X="28.050375" Y="-1.022001586914" />
                  <Point X="28.0393515625" Y="-1.033223266602" />
                  <Point X="27.9485625" Y="-1.142413208008" />
                  <Point X="27.929607421875" Y="-1.165211181641" />
                  <Point X="27.921326171875" Y="-1.176848388672" />
                  <Point X="27.906603515625" Y="-1.201234375" />
                  <Point X="27.900162109375" Y="-1.213983154297" />
                  <Point X="27.8888203125" Y="-1.241367797852" />
                  <Point X="27.88436328125" Y="-1.254934448242" />
                  <Point X="27.877533203125" Y="-1.282580932617" />
                  <Point X="27.87516015625" Y="-1.296660888672" />
                  <Point X="27.8621484375" Y="-1.438066772461" />
                  <Point X="27.859431640625" Y="-1.467591308594" />
                  <Point X="27.859291015625" Y="-1.483315307617" />
                  <Point X="27.861607421875" Y="-1.514580932617" />
                  <Point X="27.864064453125" Y="-1.530122436523" />
                  <Point X="27.871794921875" Y="-1.561743164063" />
                  <Point X="27.87678515625" Y="-1.576666381836" />
                  <Point X="27.889158203125" Y="-1.60548059082" />
                  <Point X="27.896541015625" Y="-1.619371582031" />
                  <Point X="27.979666015625" Y="-1.748666503906" />
                  <Point X="27.997021484375" Y="-1.775662109375" />
                  <Point X="28.0017421875" Y="-1.782353027344" />
                  <Point X="28.019798828125" Y="-1.804454711914" />
                  <Point X="28.043494140625" Y="-1.828122802734" />
                  <Point X="28.052798828125" Y="-1.836277587891" />
                  <Point X="28.261189453125" Y="-1.996181640625" />
                  <Point X="29.087171875" Y="-2.62998046875" />
                  <Point X="29.069265625" Y="-2.658954345703" />
                  <Point X="29.0454609375" Y="-2.697470947266" />
                  <Point X="29.001275390625" Y="-2.760252197266" />
                  <Point X="28.959330078125" Y="-2.736034667969" />
                  <Point X="27.848455078125" Y="-2.094670898438" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.592341796875" Y="-2.034052612305" />
                  <Point X="27.555017578125" Y="-2.027311889648" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.03513659668" />
                  <Point X="27.415068359375" Y="-2.044960571289" />
                  <Point X="27.40058984375" Y="-2.051109130859" />
                  <Point X="27.252080078125" Y="-2.129269042969" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.208970703125" Y="-2.153169677734" />
                  <Point X="27.186041015625" Y="-2.170061523438" />
                  <Point X="27.175212890625" Y="-2.179371826172" />
                  <Point X="27.15425390625" Y="-2.200330322266" />
                  <Point X="27.144943359375" Y="-2.211157470703" />
                  <Point X="27.128048828125" Y="-2.234088867188" />
                  <Point X="27.12046484375" Y="-2.246193115234" />
                  <Point X="27.0423046875" Y="-2.394703369141" />
                  <Point X="27.025984375" Y="-2.425711181641" />
                  <Point X="27.0198359375" Y="-2.440193115234" />
                  <Point X="27.01001171875" Y="-2.46997265625" />
                  <Point X="27.0063359375" Y="-2.485270263672" />
                  <Point X="27.00137890625" Y="-2.517446044922" />
                  <Point X="27.000279296875" Y="-2.533136230469" />
                  <Point X="27.00068359375" Y="-2.564485351562" />
                  <Point X="27.0021875" Y="-2.580144287109" />
                  <Point X="27.03447265625" Y="-2.758909667969" />
                  <Point X="27.04121484375" Y="-2.796234619141" />
                  <Point X="27.043017578125" Y="-2.8042265625" />
                  <Point X="27.051240234375" Y="-2.831549072266" />
                  <Point X="27.06407421875" Y="-2.862483398438" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.203462890625" Y="-3.105520996094" />
                  <Point X="27.735896484375" Y="-4.027722167969" />
                  <Point X="27.723755859375" Y="-4.036082763672" />
                  <Point X="27.6816171875" Y="-3.981166259766" />
                  <Point X="26.83391796875" Y="-2.876423095703" />
                  <Point X="26.828654296875" Y="-2.870145019531" />
                  <Point X="26.808830078125" Y="-2.849624023438" />
                  <Point X="26.783251953125" Y="-2.828003662109" />
                  <Point X="26.77330078125" Y="-2.820646728516" />
                  <Point X="26.59698828125" Y="-2.707294921875" />
                  <Point X="26.56017578125" Y="-2.683627929688" />
                  <Point X="26.546283203125" Y="-2.676245605469" />
                  <Point X="26.51747265625" Y="-2.663874511719" />
                  <Point X="26.5025546875" Y="-2.658885742188" />
                  <Point X="26.47093359375" Y="-2.651154052734" />
                  <Point X="26.45539453125" Y="-2.648695800781" />
                  <Point X="26.424125" Y="-2.646376953125" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.215568359375" Y="-2.664260498047" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.161224609375" Y="-2.670339355469" />
                  <Point X="26.13357421875" Y="-2.677171630859" />
                  <Point X="26.1200078125" Y="-2.681629882812" />
                  <Point X="26.092623046875" Y="-2.692973388672" />
                  <Point X="26.079876953125" Y="-2.6994140625" />
                  <Point X="26.05549609375" Y="-2.714133789062" />
                  <Point X="26.043861328125" Y="-2.722412841797" />
                  <Point X="25.89496484375" Y="-2.846214355469" />
                  <Point X="25.863876953125" Y="-2.872063232422" />
                  <Point X="25.852654296875" Y="-2.883087646484" />
                  <Point X="25.832185546875" Y="-2.906834960938" />
                  <Point X="25.822939453125" Y="-2.919557861328" />
                  <Point X="25.806046875" Y="-2.947382324219" />
                  <Point X="25.7990234375" Y="-2.961460449219" />
                  <Point X="25.787396484375" Y="-2.990584472656" />
                  <Point X="25.78279296875" Y="-3.005630371094" />
                  <Point X="25.738275390625" Y="-3.210452636719" />
                  <Point X="25.728978515625" Y="-3.253218261719" />
                  <Point X="25.7275859375" Y="-3.261285888672" />
                  <Point X="25.724724609375" Y="-3.289676025391" />
                  <Point X="25.7247421875" Y="-3.323170410156" />
                  <Point X="25.7255546875" Y="-3.335520996094" />
                  <Point X="25.763505859375" Y="-3.623777099609" />
                  <Point X="25.83308984375" Y="-4.152321289062" />
                  <Point X="25.655068359375" Y="-3.4879375" />
                  <Point X="25.652607421875" Y="-3.480120117188" />
                  <Point X="25.642146484375" Y="-3.453581542969" />
                  <Point X="25.6267890625" Y="-3.423816894531" />
                  <Point X="25.62041015625" Y="-3.413209716797" />
                  <Point X="25.484962890625" Y="-3.218055908203" />
                  <Point X="25.456681640625" Y="-3.177309326172" />
                  <Point X="25.446671875" Y="-3.165172607422" />
                  <Point X="25.424787109375" Y="-3.142716308594" />
                  <Point X="25.412912109375" Y="-3.132396728516" />
                  <Point X="25.38665625" Y="-3.113153076172" />
                  <Point X="25.373240234375" Y="-3.104936035156" />
                  <Point X="25.345240234375" Y="-3.090828857422" />
                  <Point X="25.33065625" Y="-3.084938720703" />
                  <Point X="25.121060546875" Y="-3.019887451172" />
                  <Point X="25.077296875" Y="-3.006305419922" />
                  <Point X="25.063376953125" Y="-3.003109375" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766357422" />
                  <Point X="24.9913359375" Y="-2.997766357422" />
                  <Point X="24.977095703125" Y="-2.998839599609" />
                  <Point X="24.948935546875" Y="-3.003108886719" />
                  <Point X="24.935015625" Y="-3.006304931641" />
                  <Point X="24.725419921875" Y="-3.071355957031" />
                  <Point X="24.68165625" Y="-3.084938232422" />
                  <Point X="24.667068359375" Y="-3.090830078125" />
                  <Point X="24.63906640625" Y="-3.104939453125" />
                  <Point X="24.62565234375" Y="-3.113156982422" />
                  <Point X="24.599396484375" Y="-3.132402099609" />
                  <Point X="24.5875234375" Y="-3.142719726562" />
                  <Point X="24.565640625" Y="-3.165174072266" />
                  <Point X="24.555630859375" Y="-3.177310791016" />
                  <Point X="24.42018359375" Y="-3.372464355469" />
                  <Point X="24.391904296875" Y="-3.413211181641" />
                  <Point X="24.38752734375" Y="-3.420137451172" />
                  <Point X="24.374029296875" Y="-3.445256591797" />
                  <Point X="24.361224609375" Y="-3.476210205078" />
                  <Point X="24.35724609375" Y="-3.487937011719" />
                  <Point X="24.2880546875" Y="-3.7461640625" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.010751535904" Y="4.700038963015" />
                  <Point X="23.530782604573" Y="4.48634302673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.866951705001" Y="4.190786468046" />
                  <Point X="22.184622582999" Y="3.886993970109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.356508139404" Y="4.74998927457" />
                  <Point X="23.535851422344" Y="4.384609363344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.939259974774" Y="4.118989737476" />
                  <Point X="21.952348621515" Y="3.679588493149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.637809052646" Y="4.771242063891" />
                  <Point X="23.504343019868" Y="4.266590472275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.044429582367" Y="4.061823817141" />
                  <Point X="22.000110084992" Y="3.596862820283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.662703184782" Y="4.678335199156" />
                  <Point X="23.416238362601" Y="4.123373305093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.217314853412" Y="4.03480685262" />
                  <Point X="22.047871548468" Y="3.514137147417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.687597316918" Y="4.585428334421" />
                  <Point X="22.095633011944" Y="3.43141147455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.712491449055" Y="4.492521469685" />
                  <Point X="22.143394475421" Y="3.348685801684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.34559525679" Y="2.993482704433" />
                  <Point X="21.186108225979" Y="2.922474503381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.560181768875" Y="4.76594706987" />
                  <Point X="25.347742504422" Y="4.671363015449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.737385581191" Y="4.39961460495" />
                  <Point X="22.191156675747" Y="3.265960456885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.447289412358" Y="2.934769413162" />
                  <Point X="21.062330544081" Y="2.763374682341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.749270369153" Y="4.74614429232" />
                  <Point X="25.316103650955" Y="4.553286043857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.762279713327" Y="4.306707740215" />
                  <Point X="22.232537939518" Y="3.180394136092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.548983567926" Y="2.876056121891" />
                  <Point X="20.95851516052" Y="2.613162649146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.916364426238" Y="4.716548913221" />
                  <Point X="25.284464797487" Y="4.435209072266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.793592289199" Y="4.216658550746" />
                  <Point X="22.247810470598" Y="3.083203458568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.650677723494" Y="2.81734283062" />
                  <Point X="20.8765040186" Y="2.47265848979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.067808242646" Y="4.679985598041" />
                  <Point X="25.252826093759" Y="4.317132167342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.854099229807" Y="4.139607529907" />
                  <Point X="22.246962503333" Y="2.978835472759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.752856154956" Y="2.758845152869" />
                  <Point X="20.794493387994" Y="2.332154558086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.219252059055" Y="4.64342228286" />
                  <Point X="25.202742599746" Y="4.190843112688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.972143530473" Y="4.088173792242" />
                  <Point X="22.167518239331" Y="2.839474161084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.911211148379" Y="2.725358891945" />
                  <Point X="20.835030408434" Y="2.246212355944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.370695851632" Y="4.606858957069" />
                  <Point X="20.920791823499" Y="2.180405351566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.511850873596" Y="4.565714775465" />
                  <Point X="21.006553238564" Y="2.114598347188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.64056242689" Y="4.519030404664" />
                  <Point X="21.09231465363" Y="2.048791342809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.769273980183" Y="4.472346033863" />
                  <Point X="21.178076068695" Y="1.982984338431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.893416594843" Y="4.423627440521" />
                  <Point X="21.26383748376" Y="1.917177334053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.007329883775" Y="4.370354457933" />
                  <Point X="21.349598528734" Y="1.851370164899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.121243172707" Y="4.317081475345" />
                  <Point X="21.43535916795" Y="1.78556281509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.381320777462" Y="1.316274688229" />
                  <Point X="20.363766337669" Y="1.308458948078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.235155232215" Y="4.263807945383" />
                  <Point X="21.507558229177" Y="1.713717461743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.561583951098" Y="1.292542577578" />
                  <Point X="20.331720246443" Y="1.190200662554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.338040624285" Y="4.205625026773" />
                  <Point X="21.553588199972" Y="1.630220878666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.741847124734" Y="1.268810466927" />
                  <Point X="20.299674155217" Y="1.071942377031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.439215713054" Y="4.146680632074" />
                  <Point X="21.578350043117" Y="1.537255115078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.92211029837" Y="1.245078356277" />
                  <Point X="20.268133411876" Y="0.953909086881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.540390801824" Y="4.087736237375" />
                  <Point X="21.558367706843" Y="1.424367959311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.102373472006" Y="1.221346245626" />
                  <Point X="20.251660320345" Y="0.842584347537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.640216066687" Y="4.028190862352" />
                  <Point X="21.471170885254" Y="1.281554986614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.314945296063" Y="1.211998872927" />
                  <Point X="20.235187265483" Y="0.73125962452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.730143912048" Y="3.964238872257" />
                  <Point X="20.218714210622" Y="0.619934901503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.815722957054" Y="3.898350671496" />
                  <Point X="20.323549079669" Y="0.562619945966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.734911710316" Y="3.758380739895" />
                  <Point X="20.469361884616" Y="0.523549542955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.654100117824" Y="3.618410654354" />
                  <Point X="20.615174689564" Y="0.484479139945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.573288525333" Y="3.478440568814" />
                  <Point X="20.760987494511" Y="0.445408736935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.492476932841" Y="3.338470483273" />
                  <Point X="20.906800299458" Y="0.406338333925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.41166534035" Y="3.198500397732" />
                  <Point X="21.052613104405" Y="0.367267930914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.330853747858" Y="3.058530312191" />
                  <Point X="21.198425909353" Y="0.328197527904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.250042155367" Y="2.918560226651" />
                  <Point X="21.344238404261" Y="0.289126986856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.169230562875" Y="2.77859014111" />
                  <Point X="21.490050793747" Y="0.25005639887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.088418970383" Y="2.638620055569" />
                  <Point X="21.596759812784" Y="0.193575868668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.033121749518" Y="2.510009700164" />
                  <Point X="21.662515939997" Y="0.11886193628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.012980700734" Y="2.397051881035" />
                  <Point X="21.697930273155" Y="0.030638966815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.02255816112" Y="2.297325594673" />
                  <Point X="21.701550926999" Y="-0.071739460692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.662479212098" Y="-0.534363994259" />
                  <Point X="20.224250343909" Y="-0.729476057107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.053899741077" Y="2.207289318655" />
                  <Point X="21.651015193613" Y="-0.198229865287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.249070294407" Y="-0.377187264326" />
                  <Point X="20.238233395989" Y="-0.82724084767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.107972893828" Y="2.127373790906" />
                  <Point X="20.252216448069" Y="-0.925005638234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.18316920958" Y="2.056862901251" />
                  <Point X="20.271109583173" Y="-1.020584318989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.965312192136" Y="2.746333631948" />
                  <Point X="28.594463768601" Y="2.581221275888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.285115006184" Y="1.998261647788" />
                  <Point X="20.294959218182" Y="-1.113956223807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.02198286157" Y="2.667574593137" />
                  <Point X="27.807382202279" Y="2.126799538427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.474351432356" Y="1.978524686566" />
                  <Point X="20.318809337054" Y="-1.207327913195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.074309236088" Y="2.586881349613" />
                  <Point X="20.388824860017" Y="-1.280145440413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.123881785432" Y="2.504962024126" />
                  <Point X="20.720452353633" Y="-1.236485813876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.898729288611" Y="2.300727227515" />
                  <Point X="21.05207984725" Y="-1.192826187339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.575875662523" Y="2.052993085567" />
                  <Point X="21.383707340867" Y="-1.149166560802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.253022036434" Y="1.805258943618" />
                  <Point X="21.715334627494" Y="-1.105507026423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.06858930015" Y="1.619153752456" />
                  <Point X="21.900040137618" Y="-1.127261281439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.015883275487" Y="1.49169707193" />
                  <Point X="21.987671063265" Y="-1.192235926079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.001588353202" Y="1.381342116016" />
                  <Point X="22.038690799232" Y="-1.273510922568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.018649682204" Y="1.284947862639" />
                  <Point X="22.050912303045" Y="-1.37206000495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.050413626456" Y="1.195099635321" />
                  <Point X="22.018264525691" Y="-1.490586178398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.10272023692" Y="1.114397592272" />
                  <Point X="21.795862919949" Y="-1.693596199391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.174283119755" Y="1.042268994056" />
                  <Point X="21.473010409275" Y="-1.941329844725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.279396435076" Y="0.985078010787" />
                  <Point X="21.150157898601" Y="-2.189063490059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.441340227321" Y="0.953189586044" />
                  <Point X="20.827303761691" Y="-2.43679785944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.746834863997" Y="0.985214115042" />
                  <Point X="20.86643789408" Y="-2.523364667584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.078462190211" Y="1.028873667046" />
                  <Point X="20.915442072823" Y="-2.605537047965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.410089516425" Y="1.072533219051" />
                  <Point X="20.964446251567" Y="-2.687709428347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.707252368599" Y="1.100848198588" />
                  <Point X="21.017056213465" Y="-2.768276410635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.730092517939" Y="1.007026841793" />
                  <Point X="21.076174612856" Y="-2.845945649855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.752850746462" Y="0.913169011501" />
                  <Point X="22.426327427391" Y="-2.348809333732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.160430029299" Y="-2.467194482711" />
                  <Point X="21.135292802572" Y="-2.923614982428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.767992123583" Y="0.815919940472" />
                  <Point X="29.168632789425" Y="0.549067972097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.4191920067" Y="0.215395437688" />
                  <Point X="22.567411283958" Y="-2.389985200212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.373347943387" Y="-2.92161645151" />
                  <Point X="21.194410992287" Y="-3.001284315001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.783133500704" Y="0.718670869443" />
                  <Point X="29.75522190036" Y="0.706243824317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.366021813627" Y="0.087732096071" />
                  <Point X="22.641102399305" Y="-2.461166248265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.349248417174" Y="-0.023726347638" />
                  <Point X="22.682085595477" Y="-2.546909800172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.360720622667" Y="-0.122609039127" />
                  <Point X="22.683480098815" Y="-2.650279373742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.384134936814" Y="-0.21617476128" />
                  <Point X="22.626442958842" Y="-2.779664391044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.434358217718" Y="-0.29780436241" />
                  <Point X="22.545631211637" Y="-2.919634545468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.499036532134" Y="-0.372998167972" />
                  <Point X="22.464819464432" Y="-3.059604699892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.592002449444" Y="-0.435597521288" />
                  <Point X="22.384007717227" Y="-3.199574854315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.707636025925" Y="-0.488104582512" />
                  <Point X="22.303195970022" Y="-3.339545008739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.853448974338" Y="-0.527174921647" />
                  <Point X="22.222384222817" Y="-3.479515163163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.999261764562" Y="-0.566245331212" />
                  <Point X="22.141572475611" Y="-3.619485317586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.145074434102" Y="-0.60531579451" />
                  <Point X="28.456568502752" Y="-0.911858385152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.954339760975" Y="-1.135465027578" />
                  <Point X="22.060760728406" Y="-3.75945547201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.290887103641" Y="-0.644386257808" />
                  <Point X="28.636831700524" Y="-0.935590485057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.880011734712" Y="-1.27254844345" />
                  <Point X="22.060637823707" Y="-3.863500639166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.43669977318" Y="-0.683456721106" />
                  <Point X="28.817094898296" Y="-0.959322584961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.867288797177" Y="-1.382203506661" />
                  <Point X="22.146212793839" Y="-3.929390654177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.58251244272" Y="-0.722527184403" />
                  <Point X="28.997358096069" Y="-0.983054684865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.85975286933" Y="-1.489549164366" />
                  <Point X="22.231787763972" Y="-3.995280669187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.728325112259" Y="-0.761597647701" />
                  <Point X="29.177621293841" Y="-1.00678678477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.880134077952" Y="-1.584465312105" />
                  <Point X="22.326564120072" Y="-4.057073963221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.774591190302" Y="-0.844989109058" />
                  <Point X="29.357884491613" Y="-1.030518884674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.927420550865" Y="-1.667402464395" />
                  <Point X="24.886092726819" Y="-3.021488853087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.567380631783" Y="-3.163388620152" />
                  <Point X="23.173983949924" Y="-3.783768792929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.586785532959" Y="-4.04520637213" />
                  <Point X="22.42426206492" Y="-4.117566482136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.755595079284" Y="-0.95743716905" />
                  <Point X="29.538147689385" Y="-1.054250984579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.97939871702" Y="-1.748250740271" />
                  <Point X="25.122354144436" Y="-3.020288939191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.459908130799" Y="-3.31522890693" />
                  <Point X="23.375571396254" Y="-3.798006725683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.729181140529" Y="-1.073187858734" />
                  <Point X="29.718410989492" Y="-1.077983038921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.040434242553" Y="-1.825066419939" />
                  <Point X="27.577189735634" Y="-2.031316162731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.131121183269" Y="-2.229918677858" />
                  <Point X="25.259981527231" Y="-3.063003726944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.367843884107" Y="-3.460208996906" />
                  <Point X="23.496322635773" Y="-3.848235256521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.124774167401" Y="-1.891506312538" />
                  <Point X="27.743354204327" Y="-2.061325421247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.056911436823" Y="-2.366949432163" />
                  <Point X="26.428567081177" Y="-2.646706363548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.826386704271" Y="-2.914814341077" />
                  <Point X="25.384237698344" Y="-3.111671761696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.332608179365" Y="-3.579887389863" />
                  <Point X="23.57593566986" Y="-3.916779696479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.210535443695" Y="-1.957313378701" />
                  <Point X="27.871779487182" Y="-2.10813724786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.004981707923" Y="-2.494060483547" />
                  <Point X="26.567962417168" Y="-2.688634007825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.774952271196" Y="-3.041704872552" />
                  <Point X="25.459915356053" Y="-3.181968344105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.300969748509" Y="-3.697964173296" />
                  <Point X="23.654584410257" Y="-3.985753467649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.296296754107" Y="-2.023120429674" />
                  <Point X="27.973473801634" Y="-2.166850468391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.005388724902" Y="-2.597869714371" />
                  <Point X="26.663530223603" Y="-2.750074925466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.749928710592" Y="-3.156836526" />
                  <Point X="25.515052938302" Y="-3.261409957307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.269331047386" Y="-3.81604107706" />
                  <Point X="23.733233150655" Y="-4.054727238818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.382058113744" Y="-2.088927458731" />
                  <Point X="28.075168116087" Y="-2.225563688922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.022771730743" Y="-2.694120747992" />
                  <Point X="26.759098125297" Y="-2.811515800695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.726584677335" Y="-3.271220405695" />
                  <Point X="25.570189894668" Y="-3.340851849171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.237692159837" Y="-3.934118063826" />
                  <Point X="23.792518198221" Y="-4.13232228149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.467819473382" Y="-2.154734487787" />
                  <Point X="28.17686243054" Y="-2.284276909453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.040155730919" Y="-2.790371338906" />
                  <Point X="26.837218719351" Y="-2.880724717767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.730547854754" Y="-3.373446331881" />
                  <Point X="25.624808980906" Y="-3.42052431167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.206053272288" Y="-4.052195050592" />
                  <Point X="23.82205725941" Y="-4.22316109057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.553580833019" Y="-2.220541516844" />
                  <Point X="28.278556744993" Y="-2.342990129984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.073085310292" Y="-2.879700592032" />
                  <Point X="26.896694370777" Y="-2.958234898133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.743480903455" Y="-3.471678614069" />
                  <Point X="25.660599461489" Y="-3.508579809512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.174414384739" Y="-4.170272037359" />
                  <Point X="23.841059950743" Y="-4.318690993748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.639342192656" Y="-2.2863485459" />
                  <Point X="28.380251059445" Y="-2.401703350515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.120846985412" Y="-2.962426170669" />
                  <Point X="26.956170022204" Y="-3.035745078499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.756413952155" Y="-3.569910896257" />
                  <Point X="25.685493808963" Y="-3.601486578372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.142775497189" Y="-4.288349024125" />
                  <Point X="23.860062645857" Y="-4.414220895243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.725103552293" Y="-2.352155574957" />
                  <Point X="28.481945373898" Y="-2.460416571046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.168608660532" Y="-3.045151749305" />
                  <Point X="27.01564567363" Y="-3.113255258864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.769346763417" Y="-3.66814328416" />
                  <Point X="25.710388156437" Y="-3.694393347233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.11113660964" Y="-4.406426010891" />
                  <Point X="23.878298119576" Y="-4.510092385712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.810864911931" Y="-2.417962604013" />
                  <Point X="28.583639688351" Y="-2.519129791577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.216370336669" Y="-3.127877327489" />
                  <Point X="27.075121325056" Y="-3.19076543923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.782279286397" Y="-3.766375800414" />
                  <Point X="25.735282503911" Y="-3.787300116094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.079497722091" Y="-4.524502997657" />
                  <Point X="23.873935916626" Y="-4.616025010054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.896626271568" Y="-2.48376963307" />
                  <Point X="28.685334002804" Y="-2.577843012107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.264132015552" Y="-3.210602904449" />
                  <Point X="27.134596976482" Y="-3.268275619596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.795211809377" Y="-3.864608316668" />
                  <Point X="25.760176851385" Y="-3.880206884954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.047858834542" Y="-4.642579984424" />
                  <Point X="23.859392750746" Y="-4.726490491137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.982387631205" Y="-2.549576662126" />
                  <Point X="28.787028317256" Y="-2.636556232638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.311893694434" Y="-3.29332848141" />
                  <Point X="27.194072627908" Y="-3.345785799962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.808144332357" Y="-3.962840832922" />
                  <Point X="25.785071198859" Y="-3.973113653815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.016219946993" Y="-4.76065697119" />
                  <Point X="24.006101647982" Y="-4.765161928156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.068148990842" Y="-2.615383691183" />
                  <Point X="28.888722631709" Y="-2.695269453169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.359655373317" Y="-3.376054058371" />
                  <Point X="27.253548279334" Y="-3.423295980327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.821076855338" Y="-4.061073349175" />
                  <Point X="25.809965546333" Y="-4.066020422675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.012656813709" Y="-2.744080846711" />
                  <Point X="28.99041665641" Y="-2.753982802706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.4074170522" Y="-3.458779635332" />
                  <Point X="27.31302393076" Y="-3.500806160693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.455178731083" Y="-3.541505212293" />
                  <Point X="27.372499582187" Y="-3.578316341059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.502940409965" Y="-3.624230789254" />
                  <Point X="27.431975233613" Y="-3.655826521425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.550702088848" Y="-3.706956366215" />
                  <Point X="27.491450885039" Y="-3.73333670179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.598463767731" Y="-3.789681943176" />
                  <Point X="27.550926536465" Y="-3.810846882156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.646225446614" Y="-3.872407520137" />
                  <Point X="27.610402187891" Y="-3.888357062522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.693987125496" Y="-3.955133097098" />
                  <Point X="27.669877839317" Y="-3.965867242887" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.9998359375" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.8029296875" Y="-4.773875" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521543701172" />
                  <Point X="25.328873046875" Y="-3.326389892578" />
                  <Point X="25.300591796875" Y="-3.285643310547" />
                  <Point X="25.2743359375" Y="-3.266399658203" />
                  <Point X="25.064740234375" Y="-3.201348388672" />
                  <Point X="25.0209765625" Y="-3.187766357422" />
                  <Point X="24.9913359375" Y="-3.187766357422" />
                  <Point X="24.781740234375" Y="-3.252817382812" />
                  <Point X="24.7379765625" Y="-3.266399658203" />
                  <Point X="24.711720703125" Y="-3.285644775391" />
                  <Point X="24.5762734375" Y="-3.480798339844" />
                  <Point X="24.547994140625" Y="-3.521545166016" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.47158203125" Y="-3.795339355469" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="23.9500546875" Y="-4.947828613281" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.656591796875" Y="-4.875501464844" />
                  <Point X="23.650958984375" Y="-4.854044433594" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.640244140625" Y="-4.283025390625" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.420748046875" Y="-4.033397949219" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.09464453125" Y="-3.968976318359" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791503906" />
                  <Point X="22.796712890625" Y="-4.11638671875" />
                  <Point X="22.75215625" Y="-4.146159667969" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.701419921875" Y="-4.207918945312" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.217892578125" Y="-4.213260253906" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.807458984375" Y="-3.908356445312" />
                  <Point X="21.771419921875" Y="-3.880607666016" />
                  <Point X="21.8781171875" Y="-3.695803222656" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597595214844" />
                  <Point X="22.4860234375" Y="-2.568767089844" />
                  <Point X="22.471671875" Y="-2.554415039062" />
                  <Point X="22.46867578125" Y="-2.551418457031" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="22.1856015625" Y="-2.6720546875" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.896544921875" Y="-2.923654541016" />
                  <Point X="20.838302734375" Y="-2.84713671875" />
                  <Point X="20.596904296875" Y="-2.442347900391" />
                  <Point X="20.56898046875" Y="-2.395526367188" />
                  <Point X="20.75867578125" Y="-2.24996875" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.39601574707" />
                  <Point X="21.860552734375" Y="-1.371406616211" />
                  <Point X="21.861884765625" Y="-1.366259765625" />
                  <Point X="21.859673828125" Y="-1.334592895508" />
                  <Point X="21.838841796875" Y="-1.310639526367" />
                  <Point X="21.81693359375" Y="-1.297745239258" />
                  <Point X="21.812359375" Y="-1.295052978516" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.499314453125" Y="-1.325586181641" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.095390625" Y="-1.100379882812" />
                  <Point X="20.072607421875" Y="-1.011186950684" />
                  <Point X="20.008740234375" Y="-0.56464074707" />
                  <Point X="20.001603515625" Y="-0.514742370605" />
                  <Point X="20.215060546875" Y="-0.457547088623" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.45810546875" Y="-0.12142489624" />
                  <Point X="21.481064453125" Y="-0.105489959717" />
                  <Point X="21.485853515625" Y="-0.102166366577" />
                  <Point X="21.505103515625" Y="-0.07590562439" />
                  <Point X="21.512755859375" Y="-0.051247123718" />
                  <Point X="21.514353515625" Y="-0.046098747253" />
                  <Point X="21.514353515625" Y="-0.016461328506" />
                  <Point X="21.506701171875" Y="0.008197021484" />
                  <Point X="21.505103515625" Y="0.01334554863" />
                  <Point X="21.485859375" Y="0.039602794647" />
                  <Point X="21.462900390625" Y="0.055537731171" />
                  <Point X="21.46289453125" Y="0.055541229248" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="21.18625" Y="0.134757598877" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.067673828125" Y="0.897202636719" />
                  <Point X="20.08235546875" Y="0.996416320801" />
                  <Point X="20.21091796875" Y="1.470850463867" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.366849609375" Y="1.509819458008" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268294921875" Y="1.395866088867" />
                  <Point X="21.319109375" Y="1.411888305664" />
                  <Point X="21.329720703125" Y="1.415233642578" />
                  <Point X="21.34846484375" Y="1.426055419922" />
                  <Point X="21.360880859375" Y="1.443785522461" />
                  <Point X="21.381271484375" Y="1.49301171875" />
                  <Point X="21.38553125" Y="1.503295776367" />
                  <Point X="21.389287109375" Y="1.524607788086" />
                  <Point X="21.383685546875" Y="1.545510742188" />
                  <Point X="21.359083984375" Y="1.592772460938" />
                  <Point X="21.35394140625" Y="1.602647705078" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.193025390625" Y="1.732024780273" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.782939453125" Y="2.689271240234" />
                  <Point X="20.83998828125" Y="2.787007080078" />
                  <Point X="21.180541015625" Y="3.224742431641" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.300443359375" Y="3.238944335938" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.932259765625" Y="2.914242919922" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927405029297" />
                  <Point X="22.036984375" Y="2.977639892578" />
                  <Point X="22.04747265625" Y="2.988128417969" />
                  <Point X="22.0591015625" Y="3.0063828125" />
                  <Point X="22.061927734375" Y="3.027841796875" />
                  <Point X="22.055736328125" Y="3.098614501953" />
                  <Point X="22.054443359375" Y="3.113391357422" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.9827890625" Y="3.246864746094" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="22.14776953125" Y="4.09815625" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.783490234375" Y="4.47232421875" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.866740234375" Y="4.503170410156" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.1275234375" Y="4.232655761719" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.218491699219" />
                  <Point X="23.186193359375" Y="4.22225" />
                  <Point X="23.268236328125" Y="4.256233886719" />
                  <Point X="23.2853671875" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275743652344" />
                  <Point X="23.31391796875" Y="4.294486816406" />
                  <Point X="23.34062109375" Y="4.379180175781" />
                  <Point X="23.346197265625" Y="4.39686328125" />
                  <Point X="23.348083984375" Y="4.418426757812" />
                  <Point X="23.340677734375" Y="4.474681640625" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.90328515625" Y="4.867235351562" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.68214453125" Y="4.979396972656" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.797994140625" Y="4.907530761719" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.08783203125" Y="4.435475097656" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.7478984375" Y="4.937327148438" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.3981640625" Y="4.795686035156" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.859333984375" Y="4.64179296875" />
                  <Point X="26.93103515625" Y="4.615786132813" />
                  <Point X="27.26962109375" Y="4.45744140625" />
                  <Point X="27.3386953125" Y="4.42513671875" />
                  <Point X="27.66578515625" Y="4.23457421875" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="28.04101171875" Y="3.976312988281" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.942654296875" Y="3.738204345703" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491516357422" />
                  <Point X="27.207091796875" Y="2.425097900391" />
                  <Point X="27.202044921875" Y="2.392328613281" />
                  <Point X="27.208970703125" Y="2.334895507812" />
                  <Point X="27.210416015625" Y="2.322903808594" />
                  <Point X="27.218681640625" Y="2.3008125" />
                  <Point X="27.254220703125" Y="2.248438964844" />
                  <Point X="27.27494140625" Y="2.224203125" />
                  <Point X="27.327314453125" Y="2.188665527344" />
                  <Point X="27.33825" Y="2.181245605469" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.417771484375" Y="2.1660546875" />
                  <Point X="27.448666015625" Y="2.165946533203" />
                  <Point X="27.5150859375" Y="2.183707763672" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="27.7988125" Y="2.341244873047" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.16300390625" Y="2.796897705078" />
                  <Point X="29.20259765625" Y="2.741873535156" />
                  <Point X="29.374568359375" Y="2.457687011719" />
                  <Point X="29.387513671875" Y="2.436295410156" />
                  <Point X="29.2270859375" Y="2.313194824219" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583832885742" />
                  <Point X="28.2315703125" Y="1.521472167969" />
                  <Point X="28.21312109375" Y="1.491503295898" />
                  <Point X="28.195314453125" Y="1.427832641602" />
                  <Point X="28.191595703125" Y="1.414538818359" />
                  <Point X="28.190779296875" Y="1.390962768555" />
                  <Point X="28.205396484375" Y="1.32012109375" />
                  <Point X="28.215646484375" Y="1.287955444336" />
                  <Point X="28.25540234375" Y="1.227527099609" />
                  <Point X="28.263703125" Y="1.21491003418" />
                  <Point X="28.280947265625" Y="1.198819824219" />
                  <Point X="28.338560546875" Y="1.166388793945" />
                  <Point X="28.36856640625" Y="1.153619506836" />
                  <Point X="28.446462890625" Y="1.143324462891" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="28.720673828125" Y="1.173409667969" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.922185546875" Y="1.021227294922" />
                  <Point X="29.939193359375" Y="0.951366821289" />
                  <Point X="29.993384765625" Y="0.603301879883" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="29.816673828125" Y="0.526007446289" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232818893433" />
                  <Point X="28.652556640625" Y="0.188582717896" />
                  <Point X="28.622267578125" Y="0.166927444458" />
                  <Point X="28.576349609375" Y="0.108416542053" />
                  <Point X="28.56676171875" Y="0.096199775696" />
                  <Point X="28.556986328125" Y="0.07473374176" />
                  <Point X="28.5416796875" Y="-0.00518951416" />
                  <Point X="28.538484375" Y="-0.040683258057" />
                  <Point X="28.553791015625" Y="-0.120606361389" />
                  <Point X="28.556986328125" Y="-0.137293823242" />
                  <Point X="28.56676171875" Y="-0.158759857178" />
                  <Point X="28.6126796875" Y="-0.2172709198" />
                  <Point X="28.636578125" Y="-0.241906600952" />
                  <Point X="28.713109375" Y="-0.28614276123" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="28.9657265625" Y="-0.360556976318" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.95792578125" Y="-0.903431152344" />
                  <Point X="29.948431640625" Y="-0.966411682129" />
                  <Point X="29.879001953125" Y="-1.270661743164" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.659265625" Y="-1.26183605957" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.244634765625" Y="-1.130988647461" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.1854453125" Y="-1.154697998047" />
                  <Point X="28.09465625" Y="-1.263888061523" />
                  <Point X="28.075701171875" Y="-1.286686035156" />
                  <Point X="28.064359375" Y="-1.314070678711" />
                  <Point X="28.05134765625" Y="-1.45547644043" />
                  <Point X="28.048630859375" Y="-1.485000976562" />
                  <Point X="28.056361328125" Y="-1.516621582031" />
                  <Point X="28.139486328125" Y="-1.645916381836" />
                  <Point X="28.156841796875" Y="-1.672912109375" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.376853515625" Y="-1.845444580078" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.23089453125" Y="-2.758837402344" />
                  <Point X="29.204130859375" Y="-2.802142089844" />
                  <Point X="29.06054296875" Y="-3.006162353516" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.864330078125" Y="-2.900579345703" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.558576171875" Y="-2.221027832031" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.340568359375" Y="-2.297404785156" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334682373047" />
                  <Point X="27.21044140625" Y="-2.483192626953" />
                  <Point X="27.19412109375" Y="-2.514200439453" />
                  <Point X="27.1891640625" Y="-2.546376220703" />
                  <Point X="27.22144921875" Y="-2.725141601562" />
                  <Point X="27.22819140625" Y="-2.762466552734" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.368005859375" Y="-3.010520996094" />
                  <Point X="27.98667578125" Y="-4.082088867188" />
                  <Point X="27.867056640625" Y="-4.167529296875" />
                  <Point X="27.83530078125" Y="-4.1902109375" />
                  <Point X="27.67977734375" Y="-4.290879882812" />
                  <Point X="27.53087890625" Y="-4.096830566406" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467529297" />
                  <Point X="26.49423828125" Y="-2.867115722656" />
                  <Point X="26.45742578125" Y="-2.843448730469" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.232978515625" Y="-2.853461181641" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509521484" />
                  <Point X="26.0164375" Y="-2.992311035156" />
                  <Point X="25.985349609375" Y="-3.018159912109" />
                  <Point X="25.96845703125" Y="-3.045984375" />
                  <Point X="25.923939453125" Y="-3.250806640625" />
                  <Point X="25.914642578125" Y="-3.293572265625" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="25.951880859375" Y="-3.598976318359" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.024388671875" Y="-4.956662109375" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#200" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.159865750374" Y="4.951751606804" Z="2.1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.1" />
                  <Point X="-0.329824227631" Y="5.060338377271" Z="2.1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.1" />
                  <Point X="-1.116370524828" Y="4.946658738665" Z="2.1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.1" />
                  <Point X="-1.715051979358" Y="4.499435428002" Z="2.1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.1" />
                  <Point X="-1.713221434693" Y="4.425497245507" Z="2.1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.1" />
                  <Point X="-1.757055289229" Y="4.33370850278" Z="2.1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.1" />
                  <Point X="-1.855545577404" Y="4.308286160276" Z="2.1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.1" />
                  <Point X="-2.099748472908" Y="4.564888257088" Z="2.1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.1" />
                  <Point X="-2.246950282568" Y="4.547311601988" Z="2.1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.1" />
                  <Point X="-2.88881003195" Y="4.169116134428" Z="2.1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.1" />
                  <Point X="-3.066668240966" Y="3.253144849134" Z="2.1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.1" />
                  <Point X="-3.000231871464" Y="3.12553611259" Z="2.1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.1" />
                  <Point X="-3.004528688808" Y="3.044274879078" Z="2.1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.1" />
                  <Point X="-3.069540353017" Y="2.995332827709" Z="2.1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.1" />
                  <Point X="-3.680714499163" Y="3.31352570778" Z="2.1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.1" />
                  <Point X="-3.865078253709" Y="3.286725188062" Z="2.1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.1" />
                  <Point X="-4.266399410583" Y="2.745756538975" Z="2.1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.1" />
                  <Point X="-3.843569969582" Y="1.723637068881" Z="2.1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.1" />
                  <Point X="-3.691425374286" Y="1.600966196766" Z="2.1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.1" />
                  <Point X="-3.671079750644" Y="1.543426307468" Z="2.1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.1" />
                  <Point X="-3.702079946514" Y="1.49085478136" Z="2.1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.1" />
                  <Point X="-4.632782069545" Y="1.590671759161" Z="2.1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.1" />
                  <Point X="-4.843499358245" Y="1.515207096669" Z="2.1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.1" />
                  <Point X="-4.987943859195" Y="0.935801743036" Z="2.1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.1" />
                  <Point X="-3.832849219806" Y="0.117741325474" Z="2.1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.1" />
                  <Point X="-3.571767027618" Y="0.04574192491" Z="2.1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.1" />
                  <Point X="-3.547209929271" Y="0.024658446061" Z="2.1" />
                  <Point X="-3.539556741714" Y="0" Z="2.1" />
                  <Point X="-3.541154671864" Y="-0.005148505" Z="2.1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.1" />
                  <Point X="-3.553601567506" Y="-0.033134058224" Z="2.1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.1" />
                  <Point X="-4.804038568967" Y="-0.377970731282" Z="2.1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.1" />
                  <Point X="-5.046912050183" Y="-0.540439309911" Z="2.1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.1" />
                  <Point X="-4.959212401833" Y="-1.081474035436" Z="2.1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.1" />
                  <Point X="-3.500316472582" Y="-1.343878399096" Z="2.1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.1" />
                  <Point X="-3.214584595577" Y="-1.309555537337" Z="2.1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.1" />
                  <Point X="-3.194007090596" Y="-1.32758801648" Z="2.1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.1" />
                  <Point X="-4.277918525623" Y="-2.179020911138" Z="2.1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.1" />
                  <Point X="-4.452196936742" Y="-2.436678025997" Z="2.1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.1" />
                  <Point X="-4.149398110196" Y="-2.922657939097" Z="2.1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.1" />
                  <Point X="-2.795555306281" Y="-2.684076000389" Z="2.1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.1" />
                  <Point X="-2.569842916196" Y="-2.558487552935" Z="2.1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.1" />
                  <Point X="-3.171341012779" Y="-3.639523076211" Z="2.1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.1" />
                  <Point X="-3.229202334464" Y="-3.916693783613" Z="2.1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.1" />
                  <Point X="-2.814585978039" Y="-4.224491069983" Z="2.1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.1" />
                  <Point X="-2.265068097814" Y="-4.20707702098" Z="2.1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.1" />
                  <Point X="-2.181664190908" Y="-4.126679374084" Z="2.1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.1" />
                  <Point X="-1.914781179008" Y="-3.987589296289" Z="2.1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.1" />
                  <Point X="-1.618376087668" Y="-4.039710032219" Z="2.1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.1" />
                  <Point X="-1.41495117461" Y="-4.26150030873" Z="2.1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.1" />
                  <Point X="-1.404770001256" Y="-4.816237867206" Z="2.1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.1" />
                  <Point X="-1.362023763675" Y="-4.89264447744" Z="2.1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.1" />
                  <Point X="-1.065629325632" Y="-4.965632799513" Z="2.1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.1" />
                  <Point X="-0.486278520712" Y="-3.777000188912" Z="2.1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.1" />
                  <Point X="-0.388806367091" Y="-3.47802639547" Z="2.1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.1" />
                  <Point X="-0.209596787669" Y="-3.269290476488" Z="2.1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.1" />
                  <Point X="0.043762291691" Y="-3.217821568801" Z="2.1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.1" />
                  <Point X="0.281639492341" Y="-3.323619360298" Z="2.1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.1" />
                  <Point X="0.748476339689" Y="-4.755535840904" Z="2.1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.1" />
                  <Point X="0.848818267056" Y="-5.008104098836" Z="2.1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.1" />
                  <Point X="1.028935925896" Y="-4.974222466691" Z="2.1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.1" />
                  <Point X="0.995295440787" Y="-3.561169923798" Z="2.1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.1" />
                  <Point X="0.966641026412" Y="-3.230148286745" Z="2.1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.1" />
                  <Point X="1.042248306775" Y="-2.999477119276" Z="2.1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.1" />
                  <Point X="1.231404429422" Y="-2.871970615598" Z="2.1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.1" />
                  <Point X="1.461042913217" Y="-2.877893597631" Z="2.1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.1" />
                  <Point X="2.485053195189" Y="-4.09598827818" Z="2.1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.1" />
                  <Point X="2.695767889446" Y="-4.304823524569" Z="2.1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.1" />
                  <Point X="2.889960520187" Y="-4.176936482978" Z="2.1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.1" />
                  <Point X="2.405148977849" Y="-2.95424105368" Z="2.1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.1" />
                  <Point X="2.26449603794" Y="-2.684973680299" Z="2.1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.1" />
                  <Point X="2.24853032776" Y="-2.475200339007" Z="2.1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.1" />
                  <Point X="2.357698039365" Y="-2.310370982207" Z="2.1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.1" />
                  <Point X="2.543533123214" Y="-2.238951971551" Z="2.1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.1" />
                  <Point X="3.833172543427" Y="-2.912600522426" Z="2.1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.1" />
                  <Point X="4.095274599029" Y="-3.00365993222" Z="2.1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.1" />
                  <Point X="4.267270053721" Y="-2.753843219999" Z="2.1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.1" />
                  <Point X="3.401133638641" Y="-1.774496946131" Z="2.1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.1" />
                  <Point X="3.175387026611" Y="-1.587597225949" Z="2.1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.1" />
                  <Point X="3.094979341901" Y="-1.428777997715" Z="2.1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.1" />
                  <Point X="3.126947367706" Y="-1.264574155744" Z="2.1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.1" />
                  <Point X="3.24909683337" Y="-1.148567673675" Z="2.1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.1" />
                  <Point X="4.646583042869" Y="-1.280128358289" Z="2.1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.1" />
                  <Point X="4.921590478513" Y="-1.250505856123" Z="2.1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.1" />
                  <Point X="5.001210792939" Y="-0.879604490234" Z="2.1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.1" />
                  <Point X="3.972510315267" Y="-0.280980665871" Z="2.1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.1" />
                  <Point X="3.731973604075" Y="-0.211574433211" Z="2.1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.1" />
                  <Point X="3.64585525734" Y="-0.155121533692" Z="2.1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.1" />
                  <Point X="3.596740904592" Y="-0.079923155111" Z="2.1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.1" />
                  <Point X="3.584630545832" Y="0.016687376075" Z="2.1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.1" />
                  <Point X="3.609524181061" Y="0.108827204883" Z="2.1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.1" />
                  <Point X="3.671421810277" Y="0.176574411031" Z="2.1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.1" />
                  <Point X="4.823457502039" Y="0.508991266898" Z="2.1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.1" />
                  <Point X="5.036631934708" Y="0.642273621697" Z="2.1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.1" />
                  <Point X="4.964610768197" Y="1.064334012829" Z="2.1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.1" />
                  <Point X="3.707992163952" Y="1.254261938854" Z="2.1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.1" />
                  <Point X="3.446857176896" Y="1.224173600226" Z="2.1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.1" />
                  <Point X="3.356931460768" Y="1.241239953466" Z="2.1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.1" />
                  <Point X="3.291017650405" Y="1.286288036579" Z="2.1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.1" />
                  <Point X="3.248209065412" Y="1.361507615027" Z="2.1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.1" />
                  <Point X="3.237309829841" Y="1.445643154462" Z="2.1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.1" />
                  <Point X="3.265096604377" Y="1.522334205912" Z="2.1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.1" />
                  <Point X="4.251366486379" Y="2.304807150941" Z="2.1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.1" />
                  <Point X="4.411189563921" Y="2.514853837751" Z="2.1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.1" />
                  <Point X="4.197433420696" Y="2.857381420894" Z="2.1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.1" />
                  <Point X="2.767654791231" Y="2.415826305272" Z="2.1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.1" />
                  <Point X="2.496010207451" Y="2.263290360813" Z="2.1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.1" />
                  <Point X="2.417600014005" Y="2.246975144688" Z="2.1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.1" />
                  <Point X="2.349231579087" Y="2.26132051302" Z="2.1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.1" />
                  <Point X="2.289438087215" Y="2.307793281295" Z="2.1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.1" />
                  <Point X="2.252454558071" Y="2.372158429828" Z="2.1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.1" />
                  <Point X="2.249237581844" Y="2.443459368431" Z="2.1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.1" />
                  <Point X="2.979799047587" Y="3.744484290724" Z="2.1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.1" />
                  <Point X="3.063831221654" Y="4.048340044514" Z="2.1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.1" />
                  <Point X="2.684797714503" Y="4.309056853089" Z="2.1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.1" />
                  <Point X="2.284645027228" Y="4.534012769105" Z="2.1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.1" />
                  <Point X="1.870225516819" Y="4.72007632665" Z="2.1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.1" />
                  <Point X="1.403742345732" Y="4.875569545869" Z="2.1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.1" />
                  <Point X="0.746949153218" Y="5.018335373451" Z="2.1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.1" />
                  <Point X="0.033378811179" Y="4.479695899146" Z="2.1" />
                  <Point X="0" Y="4.355124473572" Z="2.1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>