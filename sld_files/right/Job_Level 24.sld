<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#163" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1811" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999524414062" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.744869140625" Y="-4.190137207031" />
                  <Point X="25.5633046875" Y="-3.512524414063" />
                  <Point X="25.55772265625" Y="-3.497140136719" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.468154296875" Y="-3.360453613281" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209020751953" />
                  <Point X="25.33049609375" Y="-3.189776855469" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.18766015625" Y="-3.140028320312" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.84833984375" Y="-3.132676757812" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.189776855469" />
                  <Point X="24.655560546875" Y="-3.209020751953" />
                  <Point X="24.63367578125" Y="-3.231477539062" />
                  <Point X="24.559466796875" Y="-3.338400634766" />
                  <Point X="24.46994921875" Y="-3.467377929688" />
                  <Point X="24.4618125" Y="-3.481572021484" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.229994140625" Y="-4.329901367188" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="24.08026171875" Y="-4.876329589844" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.790859375" Y="-4.811953613281" />
                  <Point X="23.753583984375" Y="-4.802362792969" />
                  <Point X="23.763181640625" Y="-4.729453125" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.756056640625" Y="-4.378301269531" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.57062890625" Y="-4.038484130859" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295898438" />
                  <Point X="23.387115234375" Y="-3.897994628906" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.2166484375" Y="-3.881769042969" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894801513672" />
                  <Point X="22.84041796875" Y="-3.972928222656" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.5419375" Y="-4.259706542969" />
                  <Point X="22.5198515625" Y="-4.288488769531" />
                  <Point X="22.432232421875" Y="-4.234236816406" />
                  <Point X="22.198287109375" Y="-4.089384277344" />
                  <Point X="22.018556640625" Y="-3.950997558594" />
                  <Point X="21.89527734375" Y="-3.856077148438" />
                  <Point X="22.238818359375" Y="-3.261047363281" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647655273438" />
                  <Point X="22.593412109375" Y="-2.616129882812" />
                  <Point X="22.59442578125" Y="-2.585197753906" />
                  <Point X="22.585443359375" Y="-2.555581298828" />
                  <Point X="22.571228515625" Y="-2.526752685547" />
                  <Point X="22.553201171875" Y="-2.501592285156" />
                  <Point X="22.535853515625" Y="-2.484243652344" />
                  <Point X="22.5106953125" Y="-2.46621484375" />
                  <Point X="22.481865234375" Y="-2.451996826172" />
                  <Point X="22.452248046875" Y="-2.443011962891" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.655845703125" Y="-2.868212646484" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="21.10196484375" Y="-3.036682373047" />
                  <Point X="20.917138671875" Y="-2.793860351562" />
                  <Point X="20.78828515625" Y="-2.577792236328" />
                  <Point X="20.693857421875" Y="-2.419450439453" />
                  <Point X="21.303669921875" Y="-1.951523925781" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.915421875" Y="-1.475594726562" />
                  <Point X="21.93338671875" Y="-1.448464477539" />
                  <Point X="21.946142578125" Y="-1.419835449219" />
                  <Point X="21.95384765625" Y="-1.390088256836" />
                  <Point X="21.95665234375" Y="-1.359663330078" />
                  <Point X="21.9544453125" Y="-1.327992431641" />
                  <Point X="21.9474453125" Y="-1.298243286133" />
                  <Point X="21.931361328125" Y="-1.272256469727" />
                  <Point X="21.91052734375" Y="-1.248299438477" />
                  <Point X="21.887029296875" Y="-1.228767700195" />
                  <Point X="21.860546875" Y="-1.213181152344" />
                  <Point X="21.83128515625" Y="-1.201957397461" />
                  <Point X="21.7993984375" Y="-1.195475219727" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="20.87811328125" Y="-1.311549194336" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.2382109375" Y="-1.275654052734" />
                  <Point X="20.165921875" Y="-0.992650390625" />
                  <Point X="20.13183203125" Y="-0.75429095459" />
                  <Point X="20.107576171875" Y="-0.584698364258" />
                  <Point X="20.794599609375" Y="-0.400611053467" />
                  <Point X="21.467125" Y="-0.220408279419" />
                  <Point X="21.489373046875" Y="-0.211297363281" />
                  <Point X="21.517521484375" Y="-0.195346466064" />
                  <Point X="21.530787109375" Y="-0.186265762329" />
                  <Point X="21.557779296875" Y="-0.164215179443" />
                  <Point X="21.57398828125" Y="-0.147226165771" />
                  <Point X="21.585537109375" Y="-0.126780334473" />
                  <Point X="21.59715625" Y="-0.098530632019" />
                  <Point X="21.601783203125" Y="-0.084101593018" />
                  <Point X="21.6090859375" Y="-0.052987453461" />
                  <Point X="21.611599609375" Y="-0.031613124847" />
                  <Point X="21.609236328125" Y="-0.010221735001" />
                  <Point X="21.602796875" Y="0.018105836868" />
                  <Point X="21.598298828125" Y="0.032495716095" />
                  <Point X="21.58581640625" Y="0.063532138824" />
                  <Point X="21.574427734375" Y="0.084069480896" />
                  <Point X="21.558349609375" Y="0.10118585968" />
                  <Point X="21.533951171875" Y="0.12143560791" />
                  <Point X="21.52078515625" Y="0.130602981567" />
                  <Point X="21.49004296875" Y="0.148354721069" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="20.655884765625" Y="0.375219512939" />
                  <Point X="20.10818359375" Y="0.521975463867" />
                  <Point X="20.12892578125" Y="0.66214440918" />
                  <Point X="20.17551171875" Y="0.97696862793" />
                  <Point X="20.244138671875" Y="1.230224731445" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.752251953125" Y="1.363260253906" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255017578125" Y="1.299341796875" />
                  <Point X="21.276578125" Y="1.301228271484" />
                  <Point X="21.29686328125" Y="1.305263427734" />
                  <Point X="21.324705078125" Y="1.314041992188" />
                  <Point X="21.3582890625" Y="1.324631225586" />
                  <Point X="21.377224609375" Y="1.332962890625" />
                  <Point X="21.39596875" Y="1.343785400391" />
                  <Point X="21.4126484375" Y="1.356015014648" />
                  <Point X="21.42628515625" Y="1.371564331055" />
                  <Point X="21.438701171875" Y="1.389294921875" />
                  <Point X="21.448650390625" Y="1.407431762695" />
                  <Point X="21.459822265625" Y="1.43440246582" />
                  <Point X="21.473296875" Y="1.466936035156" />
                  <Point X="21.479087890625" Y="1.486806274414" />
                  <Point X="21.48284375" Y="1.508121704102" />
                  <Point X="21.484193359375" Y="1.528755981445" />
                  <Point X="21.481048828125" Y="1.549193725586" />
                  <Point X="21.475447265625" Y="1.570100219727" />
                  <Point X="21.467951171875" Y="1.589375976562" />
                  <Point X="21.45447265625" Y="1.615270751953" />
                  <Point X="21.4382109375" Y="1.646505981445" />
                  <Point X="21.42671484375" Y="1.663711303711" />
                  <Point X="21.412802734375" Y="1.680289550781" />
                  <Point X="21.39786328125" Y="1.694590332031" />
                  <Point X="20.93253515625" Y="2.051650146484" />
                  <Point X="20.648140625" Y="2.269873779297" />
                  <Point X="20.737828125" Y="2.423531005859" />
                  <Point X="20.9188515625" Y="2.733665283203" />
                  <Point X="21.100630859375" Y="2.967318603516" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.49726171875" Y="3.015614013672" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.891982421875" Y="2.822404052734" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.95943359375" Y="2.818762939453" />
                  <Point X="21.980892578125" Y="2.821587890625" />
                  <Point X="22.000984375" Y="2.826504150391" />
                  <Point X="22.019537109375" Y="2.835652832031" />
                  <Point X="22.037791015625" Y="2.847281738281" />
                  <Point X="22.053923828125" Y="2.860228515625" />
                  <Point X="22.081447265625" Y="2.887751708984" />
                  <Point X="22.1146484375" Y="2.920951904297" />
                  <Point X="22.127595703125" Y="2.937085205078" />
                  <Point X="22.139224609375" Y="2.955339599609" />
                  <Point X="22.14837109375" Y="2.973888427734" />
                  <Point X="22.1532890625" Y="2.993977294922" />
                  <Point X="22.156115234375" Y="3.015436279297" />
                  <Point X="22.15656640625" Y="3.036120849609" />
                  <Point X="22.153173828125" Y="3.074896728516" />
                  <Point X="22.14908203125" Y="3.121670410156" />
                  <Point X="22.145044921875" Y="3.141962158203" />
                  <Point X="22.13853515625" Y="3.162604736328" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.92400390625" Y="3.538684326172" />
                  <Point X="21.81666796875" Y="3.724595947266" />
                  <Point X="21.9841015625" Y="3.852966552734" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.585677734375" Y="4.253747558594" />
                  <Point X="22.832962890625" Y="4.391133789062" />
                  <Point X="22.8661640625" Y="4.347864746094" />
                  <Point X="22.9568046875" Y="4.229740722656" />
                  <Point X="22.971109375" Y="4.214797851563" />
                  <Point X="22.987689453125" Y="4.200885742188" />
                  <Point X="23.00488671875" Y="4.189395019531" />
                  <Point X="23.04804296875" Y="4.166928710938" />
                  <Point X="23.100103515625" Y="4.139828125" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.14029296875" Y="4.126728515625" />
                  <Point X="23.160736328125" Y="4.12358203125" />
                  <Point X="23.181376953125" Y="4.124935546875" />
                  <Point X="23.20269140625" Y="4.128694335938" />
                  <Point X="23.222548828125" Y="4.134481933594" />
                  <Point X="23.2675" Y="4.1531015625" />
                  <Point X="23.32172265625" Y="4.175561523437" />
                  <Point X="23.33985546875" Y="4.185509765625" />
                  <Point X="23.3575859375" Y="4.197924316406" />
                  <Point X="23.373140625" Y="4.211565429688" />
                  <Point X="23.385373046875" Y="4.22825" />
                  <Point X="23.396193359375" Y="4.246994140625" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.41915234375" Y="4.31232421875" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583984375" />
                  <Point X="23.44272265625" Y="4.410145019531" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.418828125" Y="4.608892578125" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.6422265625" Y="4.695380371094" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.397447265625" Y="4.850429199219" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.7785078125" Y="4.613203125" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.251873046875" Y="4.680626464844" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.487669921875" Y="4.869060546875" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.131197265625" Y="4.762411132812" />
                  <Point X="26.48102734375" Y="4.677950195312" />
                  <Point X="26.667126953125" Y="4.610451660156" />
                  <Point X="26.894640625" Y="4.527930175781" />
                  <Point X="27.0753828125" Y="4.443403808594" />
                  <Point X="27.294576171875" Y="4.340894042969" />
                  <Point X="27.469203125" Y="4.23915625" />
                  <Point X="27.680982421875" Y="4.1157734375" />
                  <Point X="27.845650390625" Y="3.998669921875" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.53812109375" Y="3.227529296875" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142078125" Y="2.53993359375" />
                  <Point X="27.133078125" Y="2.516059326172" />
                  <Point X="27.123345703125" Y="2.479669189453" />
                  <Point X="27.111607421875" Y="2.435772949219" />
                  <Point X="27.108619140625" Y="2.417935791016" />
                  <Point X="27.107728515625" Y="2.380952392578" />
                  <Point X="27.1115234375" Y="2.349485351562" />
                  <Point X="27.116099609375" Y="2.311527587891" />
                  <Point X="27.121443359375" Y="2.289603515625" />
                  <Point X="27.1297109375" Y="2.267512695312" />
                  <Point X="27.140072265625" Y="2.247470458984" />
                  <Point X="27.15954296875" Y="2.218775390625" />
                  <Point X="27.18303125" Y="2.184161865234" />
                  <Point X="27.194466796875" Y="2.170327636719" />
                  <Point X="27.221599609375" Y="2.145592529297" />
                  <Point X="27.250294921875" Y="2.126121582031" />
                  <Point X="27.284908203125" Y="2.102635009766" />
                  <Point X="27.304953125" Y="2.092272705078" />
                  <Point X="27.327041015625" Y="2.084006591797" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.380431640625" Y="2.074869140625" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.47320703125" Y="2.074171142578" />
                  <Point X="27.50959765625" Y="2.083902587891" />
                  <Point X="27.553494140625" Y="2.095640869141" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.404486328125" Y="2.581234863281" />
                  <Point X="28.967326171875" Y="2.906190673828" />
                  <Point X="28.99765234375" Y="2.864044433594" />
                  <Point X="29.12328125" Y="2.689449462891" />
                  <Point X="29.21507421875" Y="2.537758789062" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.74496484375" Y="2.062995361328" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.221427734375" Y="1.660243896484" />
                  <Point X="28.20397265625" Y="1.641626708984" />
                  <Point X="28.177783203125" Y="1.607459838867" />
                  <Point X="28.14619140625" Y="1.566245605469" />
                  <Point X="28.136607421875" Y="1.550911621094" />
                  <Point X="28.121630859375" Y="1.517087646484" />
                  <Point X="28.111875" Y="1.482203125" />
                  <Point X="28.10010546875" Y="1.440123168945" />
                  <Point X="28.09665234375" Y="1.417825561523" />
                  <Point X="28.0958359375" Y="1.394254272461" />
                  <Point X="28.097740234375" Y="1.371765625" />
                  <Point X="28.10575" Y="1.332951904297" />
                  <Point X="28.11541015625" Y="1.28613269043" />
                  <Point X="28.1206796875" Y="1.268979003906" />
                  <Point X="28.136283203125" Y="1.235740356445" />
                  <Point X="28.15806640625" Y="1.202632080078" />
                  <Point X="28.18433984375" Y="1.162694946289" />
                  <Point X="28.198890625" Y="1.145453491211" />
                  <Point X="28.216134765625" Y="1.129362426758" />
                  <Point X="28.23434765625" Y="1.116034545898" />
                  <Point X="28.2659140625" Y="1.09826574707" />
                  <Point X="28.303990234375" Y="1.07683215332" />
                  <Point X="28.3205234375" Y="1.069500976563" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.398798828125" Y="1.053797851562" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.263302734375" Y="1.149028198242" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.791982421875" Y="1.154431030273" />
                  <Point X="29.84594140625" Y="0.932786193848" />
                  <Point X="29.8748671875" Y="0.746999938965" />
                  <Point X="29.890865234375" Y="0.644238769531" />
                  <Point X="29.305841796875" Y="0.487481842041" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.704791015625" Y="0.32558605957" />
                  <Point X="28.681544921875" Y="0.315067596436" />
                  <Point X="28.639615234375" Y="0.2908309021" />
                  <Point X="28.58903515625" Y="0.261595336914" />
                  <Point X="28.5743125" Y="0.251097213745" />
                  <Point X="28.54753125" Y="0.225576385498" />
                  <Point X="28.522373046875" Y="0.193518722534" />
                  <Point X="28.492025390625" Y="0.154848754883" />
                  <Point X="28.48030078125" Y="0.135566680908" />
                  <Point X="28.47052734375" Y="0.114102310181" />
                  <Point X="28.463681640625" Y="0.092604698181" />
                  <Point X="28.455294921875" Y="0.048815151215" />
                  <Point X="28.4451796875" Y="-0.00400592041" />
                  <Point X="28.443484375" Y="-0.021875896454" />
                  <Point X="28.4451796875" Y="-0.058554214478" />
                  <Point X="28.45356640625" Y="-0.102343452454" />
                  <Point X="28.463681640625" Y="-0.155164825439" />
                  <Point X="28.47052734375" Y="-0.176662445068" />
                  <Point X="28.48030078125" Y="-0.198126815796" />
                  <Point X="28.492025390625" Y="-0.217408889771" />
                  <Point X="28.51718359375" Y="-0.249466567993" />
                  <Point X="28.54753125" Y="-0.288136535645" />
                  <Point X="28.560001953125" Y="-0.301238006592" />
                  <Point X="28.589037109375" Y="-0.324155914307" />
                  <Point X="28.63096875" Y="-0.348392578125" />
                  <Point X="28.681546875" Y="-0.377628173828" />
                  <Point X="28.69270703125" Y="-0.383137237549" />
                  <Point X="28.716580078125" Y="-0.392149810791" />
                  <Point X="29.427380859375" Y="-0.582608581543" />
                  <Point X="29.89147265625" Y="-0.706961791992" />
                  <Point X="29.8851484375" Y="-0.748914245605" />
                  <Point X="29.855025390625" Y="-0.948724731445" />
                  <Point X="29.8179609375" Y="-1.111143554688" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="29.107759765625" Y="-1.093409057617" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.292365234375" Y="-1.023395996094" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.163974609375" Y="-1.056597412109" />
                  <Point X="28.1361484375" Y="-1.073489868164" />
                  <Point X="28.1123984375" Y="-1.093960327148" />
                  <Point X="28.06265625" Y="-1.153784667969" />
                  <Point X="28.002654296875" Y="-1.225948364258" />
                  <Point X="27.987935546875" Y="-1.250329345703" />
                  <Point X="27.976591796875" Y="-1.277714599609" />
                  <Point X="27.969759765625" Y="-1.305365966797" />
                  <Point X="27.962630859375" Y="-1.382841186523" />
                  <Point X="27.95403125" Y="-1.476296142578" />
                  <Point X="27.95634765625" Y="-1.507561401367" />
                  <Point X="27.964078125" Y="-1.539182495117" />
                  <Point X="27.976451171875" Y="-1.567996582031" />
                  <Point X="28.021994140625" Y="-1.638836181641" />
                  <Point X="28.076931640625" Y="-1.724287231445" />
                  <Point X="28.0869375" Y="-1.737242919922" />
                  <Point X="28.110630859375" Y="-1.760909301758" />
                  <Point X="28.7702578125" Y="-2.267059570312" />
                  <Point X="29.213125" Y="-2.606883056641" />
                  <Point X="29.209732421875" Y="-2.612372314453" />
                  <Point X="29.124822265625" Y="-2.749765625" />
                  <Point X="29.04816015625" Y="-2.858696044922" />
                  <Point X="29.028982421875" Y="-2.885945068359" />
                  <Point X="28.409595703125" Y="-2.528341552734" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.65628125" Y="-2.14213671875" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.363466796875" Y="-2.178000244141" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246549804688" />
                  <Point X="27.22142578125" Y="-2.267509521484" />
                  <Point X="27.204533203125" Y="-2.290438476562" />
                  <Point X="27.161708984375" Y="-2.371806152344" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531908447266" />
                  <Point X="27.09567578125" Y="-2.563259277344" />
                  <Point X="27.113365234375" Y="-2.661203369141" />
                  <Point X="27.134703125" Y="-2.779349609375" />
                  <Point X="27.13898828125" Y="-2.795140136719" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.57569921875" Y="-3.560255615234" />
                  <Point X="27.861287109375" Y="-4.054906494141" />
                  <Point X="27.781865234375" Y="-4.111634277344" />
                  <Point X="27.701767578125" Y="-4.163480957031" />
                  <Point X="27.22299609375" Y="-3.539535400391" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.625326171875" Y="-2.838452636719" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.311453125" Y="-2.750838623047" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.02301953125" Y="-2.863291015625" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968860839844" />
                  <Point X="25.88725" Y="-2.996687255859" />
                  <Point X="25.875625" Y="-3.025809082031" />
                  <Point X="25.851234375" Y="-3.138029785156" />
                  <Point X="25.821810546875" Y="-3.273396972656" />
                  <Point X="25.819724609375" Y="-3.289627197266" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.9398671875" Y="-4.235549316406" />
                  <Point X="26.02206640625" Y="-4.859915527344" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.941572265625" Y="-4.752636230469" />
                  <Point X="23.85875390625" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575836914062" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030761719" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688964844" />
                  <Point X="23.84923046875" Y="-4.359767089844" />
                  <Point X="23.81613671875" Y="-4.193397460938" />
                  <Point X="23.811875" Y="-4.178468261719" />
                  <Point X="23.80097265625" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.74979296875" Y="-4.070937744141" />
                  <Point X="23.738994140625" Y="-4.059779296875" />
                  <Point X="23.633267578125" Y="-3.967059326172" />
                  <Point X="23.505734375" Y="-3.85521484375" />
                  <Point X="23.49326171875" Y="-3.845965087891" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822527587891" />
                  <Point X="23.423470703125" Y="-3.810226318359" />
                  <Point X="23.4086875" Y="-3.805476318359" />
                  <Point X="23.378544921875" Y="-3.798447998047" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.222861328125" Y="-3.786972412109" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.946326171875" Y="-3.79549609375" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812011719" />
                  <Point X="22.787638671875" Y="-3.893938720703" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629394531" />
                  <Point X="22.496798828125" Y="-4.162478027344" />
                  <Point X="22.482244140625" Y="-4.153466308594" />
                  <Point X="22.252408203125" Y="-4.011157470703" />
                  <Point X="22.076513671875" Y="-3.875725097656" />
                  <Point X="22.01913671875" Y="-3.831546386719" />
                  <Point X="22.32108984375" Y="-3.308547363281" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.7100859375" />
                  <Point X="22.67605078125" Y="-2.681121582031" />
                  <Point X="22.680314453125" Y="-2.666190917969" />
                  <Point X="22.6865859375" Y="-2.634665527344" />
                  <Point X="22.688361328125" Y="-2.619241455078" />
                  <Point X="22.689375" Y="-2.588309326172" />
                  <Point X="22.6853359375" Y="-2.557625244141" />
                  <Point X="22.676353515625" Y="-2.528008789062" />
                  <Point X="22.6706484375" Y="-2.513568359375" />
                  <Point X="22.65643359375" Y="-2.484739746094" />
                  <Point X="22.648453125" Y="-2.471422119141" />
                  <Point X="22.63042578125" Y="-2.44626171875" />
                  <Point X="22.62037890625" Y="-2.434418945313" />
                  <Point X="22.60303125" Y="-2.4170703125" />
                  <Point X="22.591189453125" Y="-2.407024169922" />
                  <Point X="22.56603125" Y="-2.388995361328" />
                  <Point X="22.55271484375" Y="-2.381012695312" />
                  <Point X="22.523884765625" Y="-2.366794677734" />
                  <Point X="22.509443359375" Y="-2.361087890625" />
                  <Point X="22.479826171875" Y="-2.352103027344" />
                  <Point X="22.449140625" Y="-2.348062744141" />
                  <Point X="22.418205078125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.608345703125" Y="-2.785940185547" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="21.17755859375" Y="-2.979144042969" />
                  <Point X="20.9959765625" Y="-2.740584228516" />
                  <Point X="20.869876953125" Y="-2.529134033203" />
                  <Point X="20.818734375" Y="-2.443374023438" />
                  <Point X="21.361501953125" Y="-2.026892456055" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.963515625" Y="-1.563311279297" />
                  <Point X="21.984892578125" Y="-1.540392578125" />
                  <Point X="21.994630859375" Y="-1.528044433594" />
                  <Point X="22.012595703125" Y="-1.50091418457" />
                  <Point X="22.020162109375" Y="-1.487128051758" />
                  <Point X="22.03291796875" Y="-1.458499145508" />
                  <Point X="22.038107421875" Y="-1.443656005859" />
                  <Point X="22.0458125" Y="-1.413908935547" />
                  <Point X="22.048447265625" Y="-1.39880871582" />
                  <Point X="22.051251953125" Y="-1.368383789063" />
                  <Point X="22.051421875" Y="-1.353059082031" />
                  <Point X="22.04921484375" Y="-1.321388183594" />
                  <Point X="22.046919921875" Y="-1.306233032227" />
                  <Point X="22.039919921875" Y="-1.276484008789" />
                  <Point X="22.028224609375" Y="-1.248246582031" />
                  <Point X="22.012140625" Y="-1.222259765625" />
                  <Point X="22.003046875" Y="-1.209916503906" />
                  <Point X="21.982212890625" Y="-1.185959350586" />
                  <Point X="21.97125390625" Y="-1.17524206543" />
                  <Point X="21.947755859375" Y="-1.155710327148" />
                  <Point X="21.935216796875" Y="-1.146895629883" />
                  <Point X="21.908734375" Y="-1.131309082031" />
                  <Point X="21.894568359375" Y="-1.124482177734" />
                  <Point X="21.865306640625" Y="-1.113258422852" />
                  <Point X="21.8502109375" Y="-1.108861572266" />
                  <Point X="21.81832421875" Y="-1.102379394531" />
                  <Point X="21.802705078125" Y="-1.100532836914" />
                  <Point X="21.77137890625" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="20.865712890625" Y="-1.217361938477" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.330255859375" Y="-1.252142700195" />
                  <Point X="20.259236328125" Y="-0.974112854004" />
                  <Point X="20.225875" Y="-0.740841125488" />
                  <Point X="20.213548828125" Y="-0.654654541016" />
                  <Point X="20.8191875" Y="-0.492374084473" />
                  <Point X="21.491712890625" Y="-0.312171173096" />
                  <Point X="21.503126953125" Y="-0.308322143555" />
                  <Point X="21.525375" Y="-0.299211273193" />
                  <Point X="21.536208984375" Y="-0.293949279785" />
                  <Point X="21.564357421875" Y="-0.277998474121" />
                  <Point X="21.57118359375" Y="-0.273738952637" />
                  <Point X="21.590888671875" Y="-0.25983706665" />
                  <Point X="21.617880859375" Y="-0.237786468506" />
                  <Point X="21.626513671875" Y="-0.229793823242" />
                  <Point X="21.64272265625" Y="-0.212804885864" />
                  <Point X="21.656705078125" Y="-0.193948501587" />
                  <Point X="21.66825390625" Y="-0.173502746582" />
                  <Point X="21.673396484375" Y="-0.162916793823" />
                  <Point X="21.685015625" Y="-0.134666992188" />
                  <Point X="21.687619140625" Y="-0.12753918457" />
                  <Point X="21.69426953125" Y="-0.105808883667" />
                  <Point X="21.701572265625" Y="-0.074694831848" />
                  <Point X="21.703435546875" Y="-0.064083152771" />
                  <Point X="21.70594921875" Y="-0.042708950043" />
                  <Point X="21.706025390625" Y="-0.021181221008" />
                  <Point X="21.703662109375" Y="0.000210221902" />
                  <Point X="21.701873046875" Y="0.010836462021" />
                  <Point X="21.69543359375" Y="0.039164134979" />
                  <Point X="21.693470703125" Y="0.046448886871" />
                  <Point X="21.6864375" Y="0.067943916321" />
                  <Point X="21.673955078125" Y="0.098980247498" />
                  <Point X="21.668896484375" Y="0.109603363037" />
                  <Point X="21.6575078125" Y="0.130140823364" />
                  <Point X="21.643669921875" Y="0.149111785889" />
                  <Point X="21.627591796875" Y="0.166228103638" />
                  <Point X="21.619021484375" Y="0.174287918091" />
                  <Point X="21.594623046875" Y="0.194537780762" />
                  <Point X="21.588236328125" Y="0.199398208618" />
                  <Point X="21.568291015625" Y="0.212872207642" />
                  <Point X="21.537548828125" Y="0.230624023438" />
                  <Point X="21.526400390625" Y="0.236122467041" />
                  <Point X="21.503482421875" Y="0.245615905762" />
                  <Point X="21.491712890625" Y="0.249611190796" />
                  <Point X="20.68047265625" Y="0.466982452393" />
                  <Point X="20.2145546875" Y="0.591824707031" />
                  <Point X="20.22290234375" Y="0.648237731934" />
                  <Point X="20.26866796875" Y="0.9575234375" />
                  <Point X="20.33583203125" Y="1.205377929688" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="20.7398515625" Y="1.269072998047" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.2529453125" Y="1.204364379883" />
                  <Point X="21.263298828125" Y="1.204703369141" />
                  <Point X="21.284859375" Y="1.20658984375" />
                  <Point X="21.29511328125" Y="1.208053833008" />
                  <Point X="21.3153984375" Y="1.212088989258" />
                  <Point X="21.3254296875" Y="1.214660400391" />
                  <Point X="21.353271484375" Y="1.223438964844" />
                  <Point X="21.38685546875" Y="1.234028198242" />
                  <Point X="21.396548828125" Y="1.237676391602" />
                  <Point X="21.415484375" Y="1.246008056641" />
                  <Point X="21.4247265625" Y="1.25069152832" />
                  <Point X="21.443470703125" Y="1.261514038086" />
                  <Point X="21.452142578125" Y="1.267172241211" />
                  <Point X="21.468822265625" Y="1.279401855469" />
                  <Point X="21.484072265625" Y="1.293376342773" />
                  <Point X="21.497708984375" Y="1.30892565918" />
                  <Point X="21.504103515625" Y="1.317071899414" />
                  <Point X="21.51651953125" Y="1.334802612305" />
                  <Point X="21.5219921875" Y="1.343604492188" />
                  <Point X="21.53194140625" Y="1.361741333008" />
                  <Point X="21.53641796875" Y="1.371076171875" />
                  <Point X="21.54758984375" Y="1.39804675293" />
                  <Point X="21.561064453125" Y="1.430580444336" />
                  <Point X="21.564501953125" Y="1.440354980469" />
                  <Point X="21.57029296875" Y="1.460225219727" />
                  <Point X="21.572646484375" Y="1.470320922852" />
                  <Point X="21.57640234375" Y="1.491636230469" />
                  <Point X="21.577640625" Y="1.501921386719" />
                  <Point X="21.578990234375" Y="1.522555664062" />
                  <Point X="21.578087890625" Y="1.543202636719" />
                  <Point X="21.574943359375" Y="1.563640380859" />
                  <Point X="21.5728125" Y="1.573780273438" />
                  <Point X="21.5672109375" Y="1.594686767578" />
                  <Point X="21.56398828125" Y="1.604532470703" />
                  <Point X="21.5564921875" Y="1.623808227539" />
                  <Point X="21.55221875" Y="1.63323840332" />
                  <Point X="21.538740234375" Y="1.659133300781" />
                  <Point X="21.522478515625" Y="1.690368408203" />
                  <Point X="21.517201171875" Y="1.699284667969" />
                  <Point X="21.505705078125" Y="1.716489990234" />
                  <Point X="21.499486328125" Y="1.724779541016" />
                  <Point X="21.48557421875" Y="1.741357788086" />
                  <Point X="21.478494140625" Y="1.748915649414" />
                  <Point X="21.4635546875" Y="1.763216186523" />
                  <Point X="21.4556953125" Y="1.769958862305" />
                  <Point X="20.9903671875" Y="2.127018554688" />
                  <Point X="20.77238671875" Y="2.29428125" />
                  <Point X="20.819875" Y="2.375641601562" />
                  <Point X="20.99771484375" Y="2.680322753906" />
                  <Point X="21.175611328125" Y="2.908984375" />
                  <Point X="21.273662109375" Y="3.035013427734" />
                  <Point X="21.44976171875" Y="2.933341796875" />
                  <Point X="21.74584375" Y="2.762398681641" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.883703125" Y="2.727765625" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.961505859375" Y="2.723785644531" />
                  <Point X="21.97183203125" Y="2.724575683594" />
                  <Point X="21.993291015625" Y="2.727400634766" />
                  <Point X="22.00347265625" Y="2.729310302734" />
                  <Point X="22.023564453125" Y="2.7342265625" />
                  <Point X="22.043" Y="2.741300292969" />
                  <Point X="22.061552734375" Y="2.750448974609" />
                  <Point X="22.070580078125" Y="2.755530517578" />
                  <Point X="22.088833984375" Y="2.767159423828" />
                  <Point X="22.09725" Y="2.773190185547" />
                  <Point X="22.1133828125" Y="2.786136962891" />
                  <Point X="22.121099609375" Y="2.793052978516" />
                  <Point X="22.148623046875" Y="2.820576171875" />
                  <Point X="22.18182421875" Y="2.853776367188" />
                  <Point X="22.188740234375" Y="2.8614921875" />
                  <Point X="22.2016875" Y="2.877625488281" />
                  <Point X="22.20771875" Y="2.88604296875" />
                  <Point X="22.21934765625" Y="2.904297363281" />
                  <Point X="22.2244296875" Y="2.913325195312" />
                  <Point X="22.233576171875" Y="2.931874023438" />
                  <Point X="22.240646484375" Y="2.951298583984" />
                  <Point X="22.245564453125" Y="2.971387451172" />
                  <Point X="22.2474765625" Y="2.981572753906" />
                  <Point X="22.250302734375" Y="3.003031738281" />
                  <Point X="22.251091796875" Y="3.013364746094" />
                  <Point X="22.25154296875" Y="3.034049316406" />
                  <Point X="22.251205078125" Y="3.044400878906" />
                  <Point X="22.2478125" Y="3.083176757812" />
                  <Point X="22.243720703125" Y="3.129950439453" />
                  <Point X="22.242255859375" Y="3.140207763672" />
                  <Point X="22.23821875" Y="3.160499511719" />
                  <Point X="22.235646484375" Y="3.170533935547" />
                  <Point X="22.22913671875" Y="3.191176513672" />
                  <Point X="22.225486328125" Y="3.200871337891" />
                  <Point X="22.21715625" Y="3.219799560547" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="22.006275390625" Y="3.586184326172" />
                  <Point X="21.94061328125" Y="3.699915283203" />
                  <Point X="22.041904296875" Y="3.777574951172" />
                  <Point X="22.3516328125" Y="4.015041015625" />
                  <Point X="22.631814453125" Y="4.170703613281" />
                  <Point X="22.807474609375" Y="4.268296386719" />
                  <Point X="22.881435546875" Y="4.171908691406" />
                  <Point X="22.8881796875" Y="4.164046875" />
                  <Point X="22.902484375" Y="4.149104003906" />
                  <Point X="22.910044921875" Y="4.142022949219" />
                  <Point X="22.926625" Y="4.128110839844" />
                  <Point X="22.93491015625" Y="4.121895996094" />
                  <Point X="22.952107421875" Y="4.110405273438" />
                  <Point X="22.96101953125" Y="4.105129394531" />
                  <Point X="23.00417578125" Y="4.082663085938" />
                  <Point X="23.056236328125" Y="4.0555625" />
                  <Point X="23.065673828125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.0437890625" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.11570703125" Y="4.034965332031" />
                  <Point X="23.125841796875" Y="4.032834228516" />
                  <Point X="23.14628515625" Y="4.029687744141" />
                  <Point X="23.166953125" Y="4.028785644531" />
                  <Point X="23.18759375" Y="4.030139160156" />
                  <Point X="23.197875" Y="4.031379150391" />
                  <Point X="23.219189453125" Y="4.035137939453" />
                  <Point X="23.2292734375" Y="4.037489257812" />
                  <Point X="23.249130859375" Y="4.043276855469" />
                  <Point X="23.258904296875" Y="4.046713623047" />
                  <Point X="23.30385546875" Y="4.065333251953" />
                  <Point X="23.358078125" Y="4.087793212891" />
                  <Point X="23.36741796875" Y="4.092272949219" />
                  <Point X="23.38555078125" Y="4.102221191406" />
                  <Point X="23.39434375" Y="4.107689453125" />
                  <Point X="23.41207421875" Y="4.120104003906" />
                  <Point X="23.420224609375" Y="4.126499511719" />
                  <Point X="23.435779296875" Y="4.140140625" />
                  <Point X="23.449755859375" Y="4.15539453125" />
                  <Point X="23.46198828125" Y="4.172079101562" />
                  <Point X="23.4676484375" Y="4.180755371094" />
                  <Point X="23.47846875" Y="4.199499511719" />
                  <Point X="23.4831484375" Y="4.208733398438" />
                  <Point X="23.4914765625" Y="4.227660644531" />
                  <Point X="23.495125" Y="4.237354003906" />
                  <Point X="23.509755859375" Y="4.283756835938" />
                  <Point X="23.527404296875" Y="4.33973046875" />
                  <Point X="23.529974609375" Y="4.349764160156" />
                  <Point X="23.534009765625" Y="4.370050292969" />
                  <Point X="23.535474609375" Y="4.380302734375" />
                  <Point X="23.537361328125" Y="4.401863769531" />
                  <Point X="23.53769921875" Y="4.412216796875" />
                  <Point X="23.537248046875" Y="4.4328984375" />
                  <Point X="23.536458984375" Y="4.443227050781" />
                  <Point X="23.520736328125" Y="4.562655273438" />
                  <Point X="23.667873046875" Y="4.603907226562" />
                  <Point X="24.068826171875" Y="4.7163203125" />
                  <Point X="24.408490234375" Y="4.756073242188" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.686744140625" Y="4.588615234375" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.34363671875" Y="4.656038574219" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.477775390625" Y="4.774577148438" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.10890234375" Y="4.670064453125" />
                  <Point X="26.45359765625" Y="4.586843261719" />
                  <Point X="26.634734375" Y="4.52114453125" />
                  <Point X="26.858251953125" Y="4.440072753906" />
                  <Point X="27.035138671875" Y="4.357349121094" />
                  <Point X="27.25045703125" Y="4.256651367187" />
                  <Point X="27.421380859375" Y="4.157071289063" />
                  <Point X="27.629435546875" Y="4.035858398438" />
                  <Point X="27.79059375" Y="3.921250488281" />
                  <Point X="27.817783203125" Y="3.901915527344" />
                  <Point X="27.455849609375" Y="3.275029296875" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.062380859375" Y="2.593116699219" />
                  <Point X="27.053185546875" Y="2.573444091797" />
                  <Point X="27.044185546875" Y="2.549569824219" />
                  <Point X="27.041302734375" Y="2.540604003906" />
                  <Point X="27.0315703125" Y="2.504213867188" />
                  <Point X="27.01983203125" Y="2.460317626953" />
                  <Point X="27.0179140625" Y="2.451469726562" />
                  <Point X="27.013646484375" Y="2.420222900391" />
                  <Point X="27.012755859375" Y="2.383239501953" />
                  <Point X="27.013412109375" Y="2.369577880859" />
                  <Point X="27.01720703125" Y="2.338110839844" />
                  <Point X="27.021783203125" Y="2.300153076172" />
                  <Point X="27.02380078125" Y="2.289031005859" />
                  <Point X="27.02914453125" Y="2.267106933594" />
                  <Point X="27.032470703125" Y="2.256304931641" />
                  <Point X="27.04073828125" Y="2.234214111328" />
                  <Point X="27.0453203125" Y="2.223885253906" />
                  <Point X="27.055681640625" Y="2.203843017578" />
                  <Point X="27.0614609375" Y="2.194129638672" />
                  <Point X="27.080931640625" Y="2.165434570313" />
                  <Point X="27.104419921875" Y="2.130821044922" />
                  <Point X="27.10980859375" Y="2.123635253906" />
                  <Point X="27.13046484375" Y="2.100122070312" />
                  <Point X="27.15759765625" Y="2.075386962891" />
                  <Point X="27.1682578125" Y="2.066981201172" />
                  <Point X="27.196953125" Y="2.047510253906" />
                  <Point X="27.23156640625" Y="2.024023681641" />
                  <Point X="27.24128125" Y="2.018244384766" />
                  <Point X="27.261326171875" Y="2.007882080078" />
                  <Point X="27.27165625" Y="2.003299194336" />
                  <Point X="27.293744140625" Y="1.995033081055" />
                  <Point X="27.304546875" Y="1.991708007812" />
                  <Point X="27.326470703125" Y="1.986365112305" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.36905859375" Y="1.980552368164" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.416044921875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975497314453" />
                  <Point X="27.484314453125" Y="1.979822875977" />
                  <Point X="27.49775" Y="1.982395996094" />
                  <Point X="27.534140625" Y="1.992127441406" />
                  <Point X="27.578037109375" Y="2.003865722656" />
                  <Point X="27.584" Y="2.005671875" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572753906" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.451986328125" Y="2.498962402344" />
                  <Point X="28.940404296875" Y="2.780950683594" />
                  <Point X="29.04396484375" Y="2.637025390625" />
                  <Point X="29.133796875" Y="2.488575195312" />
                  <Point X="29.136884765625" Y="2.483471435547" />
                  <Point X="28.6871328125" Y="2.138363769531" />
                  <Point X="28.172953125" Y="1.743819946289" />
                  <Point X="28.168142578125" Y="1.739871337891" />
                  <Point X="28.152125" Y="1.725221191406" />
                  <Point X="28.134669921875" Y="1.706604003906" />
                  <Point X="28.12857421875" Y="1.699420410156" />
                  <Point X="28.102384765625" Y="1.665253662109" />
                  <Point X="28.07079296875" Y="1.624039428711" />
                  <Point X="28.0656328125" Y="1.616596435547" />
                  <Point X="28.0497421875" Y="1.589374023438" />
                  <Point X="28.034765625" Y="1.555550048828" />
                  <Point X="28.030140625" Y="1.542673828125" />
                  <Point X="28.020384765625" Y="1.507789306641" />
                  <Point X="28.008615234375" Y="1.465709350586" />
                  <Point X="28.006224609375" Y="1.454662231445" />
                  <Point X="28.002771484375" Y="1.432364624023" />
                  <Point X="28.001708984375" Y="1.421114013672" />
                  <Point X="28.000892578125" Y="1.397542724609" />
                  <Point X="28.001173828125" Y="1.386238525391" />
                  <Point X="28.003078125" Y="1.36374987793" />
                  <Point X="28.004701171875" Y="1.352565551758" />
                  <Point X="28.0127109375" Y="1.313751708984" />
                  <Point X="28.02237109375" Y="1.266932617188" />
                  <Point X="28.02459765625" Y="1.258235717773" />
                  <Point X="28.03468359375" Y="1.22860925293" />
                  <Point X="28.050287109375" Y="1.195370605469" />
                  <Point X="28.056919921875" Y="1.183524414062" />
                  <Point X="28.078703125" Y="1.150416137695" />
                  <Point X="28.1049765625" Y="1.110479003906" />
                  <Point X="28.11173828125" Y="1.101424072266" />
                  <Point X="28.1262890625" Y="1.084182617188" />
                  <Point X="28.134078125" Y="1.075996337891" />
                  <Point X="28.151322265625" Y="1.059905151367" />
                  <Point X="28.160033203125" Y="1.052697265625" />
                  <Point X="28.17824609375" Y="1.039369384766" />
                  <Point X="28.187748046875" Y="1.033249267578" />
                  <Point X="28.219314453125" Y="1.015480285645" />
                  <Point X="28.257390625" Y="0.994046630859" />
                  <Point X="28.26548046875" Y="0.989987182617" />
                  <Point X="28.294681640625" Y="0.978083374023" />
                  <Point X="28.33027734375" Y="0.968020996094" />
                  <Point X="28.343671875" Y="0.9652578125" />
                  <Point X="28.3863515625" Y="0.959616821289" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="29.275703125" Y="1.054841064453" />
                  <Point X="29.704703125" Y="1.111319702148" />
                  <Point X="29.752689453125" Y="0.914207946777" />
                  <Point X="29.780998046875" Y="0.732385070801" />
                  <Point X="29.783873046875" Y="0.713921386719" />
                  <Point X="29.28125390625" Y="0.579244750977" />
                  <Point X="28.6919921875" Y="0.42135269165" />
                  <Point X="28.68603125" Y="0.419543792725" />
                  <Point X="28.665626953125" Y="0.412138061523" />
                  <Point X="28.642380859375" Y="0.401619567871" />
                  <Point X="28.634001953125" Y="0.397315765381" />
                  <Point X="28.592072265625" Y="0.373079071045" />
                  <Point X="28.5414921875" Y="0.343843475342" />
                  <Point X="28.533880859375" Y="0.338944854736" />
                  <Point X="28.508775390625" Y="0.319871185303" />
                  <Point X="28.481994140625" Y="0.294350402832" />
                  <Point X="28.472796875" Y="0.284226196289" />
                  <Point X="28.447638671875" Y="0.252168548584" />
                  <Point X="28.417291015625" Y="0.213498657227" />
                  <Point X="28.410853515625" Y="0.204205856323" />
                  <Point X="28.39912890625" Y="0.184923812866" />
                  <Point X="28.393841796875" Y="0.174934432983" />
                  <Point X="28.384068359375" Y="0.153470016479" />
                  <Point X="28.380005859375" Y="0.142927886963" />
                  <Point X="28.37316015625" Y="0.121430328369" />
                  <Point X="28.370376953125" Y="0.110474594116" />
                  <Point X="28.361990234375" Y="0.066685081482" />
                  <Point X="28.351875" Y="0.013864059448" />
                  <Point X="28.350603515625" Y="0.004966452122" />
                  <Point X="28.3485859375" Y="-0.026262191772" />
                  <Point X="28.35028125" Y="-0.062940540314" />
                  <Point X="28.351875" Y="-0.076424201965" />
                  <Point X="28.36026171875" Y="-0.120213562012" />
                  <Point X="28.370376953125" Y="-0.173034881592" />
                  <Point X="28.37316015625" Y="-0.183990478516" />
                  <Point X="28.380005859375" Y="-0.205488037109" />
                  <Point X="28.384068359375" Y="-0.216030151367" />
                  <Point X="28.393841796875" Y="-0.237494567871" />
                  <Point X="28.39912890625" Y="-0.247483963013" />
                  <Point X="28.410853515625" Y="-0.266765991211" />
                  <Point X="28.417291015625" Y="-0.276058654785" />
                  <Point X="28.44244921875" Y="-0.308116455078" />
                  <Point X="28.472796875" Y="-0.346786346436" />
                  <Point X="28.478720703125" Y="-0.353634735107" />
                  <Point X="28.501142578125" Y="-0.375807647705" />
                  <Point X="28.530177734375" Y="-0.398725463867" />
                  <Point X="28.54149609375" Y="-0.406405090332" />
                  <Point X="28.583427734375" Y="-0.43064163208" />
                  <Point X="28.634005859375" Y="-0.459877227783" />
                  <Point X="28.63949609375" Y="-0.462814483643" />
                  <Point X="28.659154296875" Y="-0.472014678955" />
                  <Point X="28.68302734375" Y="-0.481027160645" />
                  <Point X="28.6919921875" Y="-0.483912689209" />
                  <Point X="29.40279296875" Y="-0.674371582031" />
                  <Point X="29.784876953125" Y="-0.776750854492" />
                  <Point X="29.761615234375" Y="-0.931058044434" />
                  <Point X="29.727802734375" Y="-1.079219726563" />
                  <Point X="29.12016015625" Y="-0.999221862793" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042480469" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.2721875" Y="-0.930563598633" />
                  <Point X="28.17291796875" Y="-0.952140075684" />
                  <Point X="28.157875" Y="-0.956742614746" />
                  <Point X="28.12875390625" Y="-0.968367614746" />
                  <Point X="28.11467578125" Y="-0.975390014648" />
                  <Point X="28.086849609375" Y="-0.992282531738" />
                  <Point X="28.074125" Y="-1.001530395508" />
                  <Point X="28.050375" Y="-1.022000854492" />
                  <Point X="28.039349609375" Y="-1.033223266602" />
                  <Point X="27.989607421875" Y="-1.093047485352" />
                  <Point X="27.92960546875" Y="-1.165211181641" />
                  <Point X="27.921326171875" Y="-1.176850341797" />
                  <Point X="27.906607421875" Y="-1.201231323242" />
                  <Point X="27.90016796875" Y="-1.213973388672" />
                  <Point X="27.88882421875" Y="-1.241358642578" />
                  <Point X="27.884365234375" Y="-1.254927368164" />
                  <Point X="27.877533203125" Y="-1.282578857422" />
                  <Point X="27.87516015625" Y="-1.296661376953" />
                  <Point X="27.86803125" Y="-1.374136474609" />
                  <Point X="27.859431640625" Y="-1.467591430664" />
                  <Point X="27.859291015625" Y="-1.483315307617" />
                  <Point X="27.861607421875" Y="-1.514580566406" />
                  <Point X="27.864064453125" Y="-1.530121948242" />
                  <Point X="27.871794921875" Y="-1.561742919922" />
                  <Point X="27.87678515625" Y="-1.576666625977" />
                  <Point X="27.889158203125" Y="-1.605480712891" />
                  <Point X="27.896541015625" Y="-1.61937109375" />
                  <Point X="27.942083984375" Y="-1.690210693359" />
                  <Point X="27.997021484375" Y="-1.775661743164" />
                  <Point X="28.001744140625" Y="-1.78235534668" />
                  <Point X="28.01980078125" Y="-1.804456298828" />
                  <Point X="28.043494140625" Y="-1.828122680664" />
                  <Point X="28.052798828125" Y="-1.836277832031" />
                  <Point X="28.71242578125" Y="-2.342427978516" />
                  <Point X="29.087171875" Y="-2.629980224609" />
                  <Point X="29.04549609375" Y="-2.697416748047" />
                  <Point X="29.001275390625" Y="-2.760251220703" />
                  <Point X="28.457095703125" Y="-2.446069091797" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.6731640625" Y="-2.048649169922" />
                  <Point X="27.555017578125" Y="-2.027311889648" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.03513659668" />
                  <Point X="27.415068359375" Y="-2.044960571289" />
                  <Point X="27.40058984375" Y="-2.051109130859" />
                  <Point X="27.31922265625" Y="-2.093932373047" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.20896875" Y="-2.153170654297" />
                  <Point X="27.186037109375" Y="-2.170064453125" />
                  <Point X="27.175208984375" Y="-2.179375732422" />
                  <Point X="27.15425" Y="-2.200335449219" />
                  <Point X="27.14494140625" Y="-2.211160888672" />
                  <Point X="27.128048828125" Y="-2.23408984375" />
                  <Point X="27.12046484375" Y="-2.246193359375" />
                  <Point X="27.077640625" Y="-2.327561035156" />
                  <Point X="27.025984375" Y="-2.425711425781" />
                  <Point X="27.0198359375" Y="-2.440193359375" />
                  <Point X="27.01001171875" Y="-2.469972167969" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517442138672" />
                  <Point X="27.000279296875" Y="-2.533133544922" />
                  <Point X="27.00068359375" Y="-2.564484375" />
                  <Point X="27.0021875" Y="-2.580143798828" />
                  <Point X="27.019876953125" Y="-2.678087890625" />
                  <Point X="27.04121484375" Y="-2.796234130859" />
                  <Point X="27.04301953125" Y="-2.80423046875" />
                  <Point X="27.05123828125" Y="-2.831540771484" />
                  <Point X="27.064072265625" Y="-2.862479248047" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.493427734375" Y="-3.607755615234" />
                  <Point X="27.735896484375" Y="-4.027722900391" />
                  <Point X="27.7237578125" Y="-4.036082763672" />
                  <Point X="27.298365234375" Y="-3.481702880859" />
                  <Point X="26.83391796875" Y="-2.876422607422" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.676701171875" Y="-2.758542480469" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.302748046875" Y="-2.65623828125" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.962283203125" Y="-2.790243164062" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883087158203" />
                  <Point X="25.83218359375" Y="-2.906836425781" />
                  <Point X="25.822935546875" Y="-2.919562255859" />
                  <Point X="25.80604296875" Y="-2.947388671875" />
                  <Point X="25.79901953125" Y="-2.961467041016" />
                  <Point X="25.78739453125" Y="-2.990588867188" />
                  <Point X="25.78279296875" Y="-3.005632324219" />
                  <Point X="25.75840234375" Y="-3.117853027344" />
                  <Point X="25.728978515625" Y="-3.253220214844" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677001953" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520263672" />
                  <Point X="25.833091796875" Y="-4.152331542969" />
                  <Point X="25.655068359375" Y="-3.487936767578" />
                  <Point X="25.652607421875" Y="-3.480121582031" />
                  <Point X="25.642146484375" Y="-3.453578613281" />
                  <Point X="25.6267890625" Y="-3.423815429688" />
                  <Point X="25.62041015625" Y="-3.413209716797" />
                  <Point X="25.54619921875" Y="-3.306286376953" />
                  <Point X="25.456681640625" Y="-3.177309326172" />
                  <Point X="25.446671875" Y="-3.165172119141" />
                  <Point X="25.424787109375" Y="-3.142716308594" />
                  <Point X="25.412912109375" Y="-3.132397705078" />
                  <Point X="25.38665625" Y="-3.113153808594" />
                  <Point X="25.3732421875" Y="-3.104936767578" />
                  <Point X="25.3452421875" Y="-3.090829345703" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.2158203125" Y="-3.049297851562" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.8201796875" Y="-3.041946044922" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.6670703125" Y="-3.090829589844" />
                  <Point X="24.6390703125" Y="-3.104937255859" />
                  <Point X="24.62565625" Y="-3.113153808594" />
                  <Point X="24.599400390625" Y="-3.132397705078" />
                  <Point X="24.587525390625" Y="-3.142717773438" />
                  <Point X="24.565640625" Y="-3.165174560547" />
                  <Point X="24.555630859375" Y="-3.177311279297" />
                  <Point X="24.481421875" Y="-3.284234375" />
                  <Point X="24.391904296875" Y="-3.413211669922" />
                  <Point X="24.38753125" Y="-3.420131835938" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213378906" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.13823046875" Y="-4.305313476563" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.862944332899" Y="-4.69950155076" />
                  <Point X="24.041929086476" Y="-4.664710439029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.665633250235" Y="-3.960333703454" />
                  <Point X="27.693825349236" Y="-3.954853714535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.876021237847" Y="-4.600181571913" />
                  <Point X="24.069285907326" Y="-4.562614725714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.60101112055" Y="-3.876116886978" />
                  <Point X="27.643588269105" Y="-3.86784072768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.877403730821" Y="-4.503134756481" />
                  <Point X="24.096642728177" Y="-4.4605190124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.82561835814" Y="-4.124440194956" />
                  <Point X="25.829324912786" Y="-4.123719713718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.536388990865" Y="-3.791900070503" />
                  <Point X="27.593351188975" Y="-3.780827740825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.859201908513" Y="-4.409894746307" />
                  <Point X="24.123999549028" Y="-4.358423299085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.800970587682" Y="-4.032453150176" />
                  <Point X="25.816901542815" Y="-4.029356486193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.47176686118" Y="-3.707683254028" />
                  <Point X="27.543114108845" Y="-3.69381475397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.840667545887" Y="-4.316719375423" />
                  <Point X="24.151356197469" Y="-4.256327619283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.776322817225" Y="-3.940466105397" />
                  <Point X="25.804478172844" Y="-3.934993258667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.407144731495" Y="-3.623466437552" />
                  <Point X="27.49287703018" Y="-3.606801766831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.822133371927" Y="-4.223543967866" />
                  <Point X="24.178712658987" Y="-4.154231975816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.751675046767" Y="-3.848479060617" />
                  <Point X="25.792054802873" Y="-3.840630031142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.34252260181" Y="-3.539249621077" />
                  <Point X="27.44264008369" Y="-3.519788753999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.792622606502" Y="-4.132502193552" />
                  <Point X="24.206069120505" Y="-4.052136332348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.727027276309" Y="-3.756492015837" />
                  <Point X="25.779631432901" Y="-3.746266803617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.27790057693" Y="-3.455032784229" />
                  <Point X="27.392403137199" Y="-3.432775741167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.726271170865" Y="-4.048621520102" />
                  <Point X="24.233425582023" Y="-3.95004068888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.702379505851" Y="-3.664504971058" />
                  <Point X="25.76720806293" Y="-3.651903576092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.213278778194" Y="-3.370815903424" />
                  <Point X="27.342166190709" Y="-3.345762728335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.635939114278" Y="-3.969402207166" />
                  <Point X="24.260782043541" Y="-3.847945045413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.677731735394" Y="-3.572517926278" />
                  <Point X="25.754784692959" Y="-3.557540348567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.148656979457" Y="-3.286599022619" />
                  <Point X="27.291929244219" Y="-3.258749715503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.412689496525" Y="-4.110399759996" />
                  <Point X="22.558509442154" Y="-4.082055233886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.545607408454" Y="-3.890182826048" />
                  <Point X="24.288138505059" Y="-3.745849401945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.65275635924" Y="-3.480594561595" />
                  <Point X="25.742361322988" Y="-3.463177121041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.08403518072" Y="-3.202382141813" />
                  <Point X="27.241692297729" Y="-3.171736702671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.293732621339" Y="-4.036744548148" />
                  <Point X="22.688863284601" Y="-3.959938927674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.434840013996" Y="-3.814935740404" />
                  <Point X="24.315494966577" Y="-3.643753758478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.606279767045" Y="-3.392850609933" />
                  <Point X="25.729937953017" Y="-3.368813893516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.019413381983" Y="-3.118165261008" />
                  <Point X="27.191455351239" Y="-3.08472368984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.96587324279" Y="-2.73981179154" />
                  <Point X="29.023550016361" Y="-2.728600562464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.186914596747" Y="-3.960729782769" />
                  <Point X="22.893120752597" Y="-3.82345721188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.116636777579" Y="-3.780010097847" />
                  <Point X="24.342851428095" Y="-3.54165811501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.547094911924" Y="-3.307576894347" />
                  <Point X="25.726433963374" Y="-3.272716914085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.954791583246" Y="-3.033948380203" />
                  <Point X="27.141218404749" Y="-2.997710677008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.840469214951" Y="-2.667409779218" />
                  <Point X="29.076228211921" Y="-2.621582872505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.08655818574" Y="-3.883459006944" />
                  <Point X="24.377890738853" Y="-3.438069076933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.487910000066" Y="-3.222303189789" />
                  <Point X="25.746629937657" Y="-3.17201312834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.890169784509" Y="-2.949731499398" />
                  <Point X="27.090981458259" Y="-2.910697664176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.715065187112" Y="-2.595007766896" />
                  <Point X="28.975596519995" Y="-2.54436560587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.039791339333" Y="-3.795771474985" />
                  <Point X="24.451791342038" Y="-3.326926168819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.420158794872" Y="-3.138694603979" />
                  <Point X="25.768593044816" Y="-3.07096584676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.824397845546" Y="-2.865738183205" />
                  <Point X="27.048424792794" Y="-2.822191755944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.589661159273" Y="-2.522605754573" />
                  <Point X="28.874964828069" Y="-2.467148339235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.102729338663" Y="-3.686759481198" />
                  <Point X="24.529434658356" Y="-3.21505575097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.279888291103" Y="-3.069182341843" />
                  <Point X="25.796073955414" Y="-2.968846012842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.72343181813" Y="-2.788585904806" />
                  <Point X="27.029102590254" Y="-2.729169525626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.464257131434" Y="-2.450203742251" />
                  <Point X="28.774333136143" Y="-2.3899310726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.165667337992" Y="-3.577747487411" />
                  <Point X="24.664826393749" Y="-3.091960177569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.08815071323" Y="-3.009674265483" />
                  <Point X="25.884557922259" Y="-2.854868385992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.607846016685" Y="-2.714275422602" />
                  <Point X="27.012216632197" Y="-2.635673737353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.338853050213" Y="-2.377801740305" />
                  <Point X="28.673701538417" Y="-2.312713787654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.228605337322" Y="-3.468735493624" />
                  <Point X="26.03646595146" Y="-2.728562370295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.449568250758" Y="-2.648263417652" />
                  <Point X="27.00038326715" Y="-2.541195824488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.21344896576" Y="-2.305399738988" />
                  <Point X="28.573070091285" Y="-2.235496473436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.291543336651" Y="-3.359723499837" />
                  <Point X="27.019679717211" Y="-2.440666888539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.088044881306" Y="-2.23299773767" />
                  <Point X="28.472438644154" Y="-2.158279159217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.354481444447" Y="-3.250711484967" />
                  <Point X="27.074673312729" Y="-2.333199130421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.962640796852" Y="-2.160595736353" />
                  <Point X="28.371807197023" Y="-2.081061844999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.417419648219" Y="-3.14169945144" />
                  <Point X="27.134965624573" Y="-2.224701406185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.835691136253" Y="-2.088494164604" />
                  <Point X="28.271175749892" Y="-2.00384453078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.48035785199" Y="-3.032687417914" />
                  <Point X="27.324721011918" Y="-2.091038609312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.605584476399" Y="-2.03644428226" />
                  <Point X="28.170544302761" Y="-1.926627216562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.543296055762" Y="-2.923675384387" />
                  <Point X="28.069912855629" Y="-1.849409902344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.606234259534" Y="-2.814663350861" />
                  <Point X="27.991957326802" Y="-1.767784836115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.188464320844" Y="-2.993471823809" />
                  <Point X="21.279560798514" Y="-2.975764462318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.666631760463" Y="-2.706145179939" />
                  <Point X="27.93664941584" Y="-1.681757518925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.12429491843" Y="-2.909167006067" />
                  <Point X="21.532264467903" Y="-2.829865758921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.688826298589" Y="-2.605052912736" />
                  <Point X="27.88469161036" Y="-1.595079007196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.060125518512" Y="-2.824862187839" />
                  <Point X="21.784968955964" Y="-2.683966896391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.669856465178" Y="-2.511962188798" />
                  <Point X="27.860746130741" Y="-1.502955450906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.995960085219" Y="-2.740556598578" />
                  <Point X="22.037673796674" Y="-2.538067965312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.612327093945" Y="-2.426366679742" />
                  <Point X="27.86516186965" Y="-1.405319032191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.617502898613" Y="-1.064698441266" />
                  <Point X="29.736390570912" Y="-1.041589018772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.944241139265" Y="-2.65383165726" />
                  <Point X="22.290378637383" Y="-2.392169034234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.486337788507" Y="-2.35407843386" />
                  <Point X="27.874229231057" Y="-1.306778429657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.320667776586" Y="-1.025619258028" />
                  <Point X="29.759501861049" Y="-0.94031855303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.892522193311" Y="-2.567106715942" />
                  <Point X="27.905220187945" Y="-1.203976311856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.023832547905" Y="-0.986540095521" />
                  <Point X="29.775270522698" Y="-0.840475349683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.840803718589" Y="-2.480381683026" />
                  <Point X="27.991841344359" Y="-1.090360778673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.72699709722" Y="-0.947460976168" />
                  <Point X="29.709344553491" Y="-0.756511973937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.915567711584" Y="-2.369070948934" />
                  <Point X="28.13557972048" Y="-0.965642782667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.429138354224" Y="-0.90858076469" />
                  <Point X="29.500017680559" Y="-0.700422910188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.084479876448" Y="-2.23945966409" />
                  <Point X="29.290690830644" Y="-0.644333841964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.253392041311" Y="-2.109848379246" />
                  <Point X="29.081364000691" Y="-0.588244769861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.4223045024" Y="-1.980237036822" />
                  <Point X="28.872037170738" Y="-0.532155697757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.591217490194" Y="-1.850625592016" />
                  <Point X="28.667448043515" Y="-0.475145709532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.760130477988" Y="-1.721014147211" />
                  <Point X="28.537423548171" Y="-0.403641825112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.929043465781" Y="-1.591402702405" />
                  <Point X="28.45416670735" Y="-0.323047229548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.025145105449" Y="-1.475944349957" />
                  <Point X="28.394082011042" Y="-0.23794842537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.050716184094" Y="-1.374195749765" />
                  <Point X="28.365344632594" Y="-0.146756319855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.040602206179" Y="-1.279383621898" />
                  <Point X="28.349821584624" Y="-0.052995608698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.988160961337" Y="-1.192799081261" />
                  <Point X="28.357905288599" Y="0.045353790201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.876800876348" Y="-1.117667202986" />
                  <Point X="28.381463785368" Y="0.146711184106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.86671362176" Y="-1.217230189768" />
                  <Point X="28.451513070925" Y="0.257105471908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.323584570737" Y="-1.226025696587" />
                  <Point X="28.613254056883" Y="0.38532282078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.300033102702" Y="-1.133825552203" />
                  <Point X="29.72576192047" Y="0.698350529243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.276481634667" Y="-1.04162540782" />
                  <Point X="29.769893709495" Y="0.803706966057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.255630566183" Y="-0.948900358937" />
                  <Point X="29.755268624353" Y="0.897642223507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.242164214708" Y="-0.854739866479" />
                  <Point X="29.734160818741" Y="0.990317367747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.228697863234" Y="-0.760579374021" />
                  <Point X="29.711664986764" Y="1.082722706994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.215231337102" Y="-0.666418915512" />
                  <Point X="28.639341728787" Y="0.971062266634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.364665627671" Y="-0.346213436757" />
                  <Point X="28.258037087782" Y="0.99372223866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.657517180874" Y="-0.192510775293" />
                  <Point X="28.14258457938" Y="1.068058630407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.698504451231" Y="-0.08776557099" />
                  <Point X="28.077534539778" Y="1.15219226962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.702063651721" Y="0.009704353522" />
                  <Point X="28.030841782005" Y="1.239894202951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.673066634848" Y="0.100845990439" />
                  <Point X="28.008862614278" Y="1.332399971554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.606495592694" Y="0.184683976706" />
                  <Point X="28.002351012795" Y="1.427912330466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.471079614256" Y="0.255139862976" />
                  <Point X="28.026419749561" Y="1.52936890498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.261752584638" Y="0.311228896269" />
                  <Point X="28.080460399832" Y="1.636651429306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.05242555502" Y="0.367317929562" />
                  <Point X="28.186197528218" Y="1.75398273103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.843098525403" Y="0.423406962854" />
                  <Point X="28.355110503069" Y="1.883594173319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.633771464582" Y="0.479495990082" />
                  <Point X="28.52402347792" Y="2.013205615609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.424444295106" Y="0.535584996189" />
                  <Point X="28.692936431011" Y="2.142817053669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.215117125629" Y="0.591674002296" />
                  <Point X="27.363638991956" Y="1.98120589255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.652437957808" Y="2.037342724811" />
                  <Point X="28.861848772555" Y="2.272428372856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.229259723908" Y="0.691201130942" />
                  <Point X="27.199160337721" Y="2.046012566914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.905141902039" Y="2.183241481632" />
                  <Point X="29.0307611141" Y="2.402039692044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.244004265989" Y="0.790845265611" />
                  <Point X="27.108638881673" Y="2.125195064325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.157845846271" Y="2.329140238453" />
                  <Point X="29.117406965256" Y="2.515660025398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.25874880807" Y="0.890489400279" />
                  <Point X="27.0520004047" Y="2.210963745683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.410549790502" Y="2.475038995274" />
                  <Point X="29.065006994634" Y="2.60225258893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.277727753424" Y="0.990956619565" />
                  <Point X="27.02158116289" Y="2.301828930076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.663253839995" Y="2.620937772555" />
                  <Point X="29.007405736173" Y="2.687834124525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.305411031759" Y="1.093115789786" />
                  <Point X="21.020530936074" Y="1.232121017858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.511436185912" Y="1.327543332079" />
                  <Point X="27.013086170814" Y="2.396955756911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.915957910132" Y="2.766836553849" />
                  <Point X="28.946314238774" Y="2.772737226395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.333094310093" Y="1.195274960008" />
                  <Point X="20.723695032448" Y="1.271200049169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.562343266567" Y="1.434216752174" />
                  <Point X="27.029627665558" Y="2.496949183794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.360776949598" Y="1.297434006053" />
                  <Point X="20.426859811869" Y="1.31027921325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.578484293973" Y="1.534132336092" />
                  <Point X="27.066655956983" Y="2.600924840549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.555314647842" Y="1.626406699135" />
                  <Point X="27.12959393993" Y="2.709936831151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.5074486158" Y="1.71388057105" />
                  <Point X="27.192531922878" Y="2.818948821754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.42383539772" Y="1.794405893893" />
                  <Point X="27.255469905826" Y="2.927960812357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.323204052293" Y="1.871623227881" />
                  <Point X="27.318407888773" Y="3.036972802959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.222572706867" Y="1.948840561868" />
                  <Point X="27.381345871721" Y="3.145984793562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.121941361441" Y="2.026057895856" />
                  <Point X="27.444283854668" Y="3.254996784164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.021310016014" Y="2.103275229844" />
                  <Point X="27.507222052681" Y="3.364008816571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.920678668641" Y="2.180492563453" />
                  <Point X="27.570160299112" Y="3.47302085839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.820047320403" Y="2.257709896895" />
                  <Point X="27.633098545543" Y="3.582032900208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.805925939752" Y="2.351743064579" />
                  <Point X="27.696036791974" Y="3.691044942027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.869643456425" Y="2.460906581188" />
                  <Point X="27.758975038405" Y="3.800056983846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.933361329737" Y="2.570070167121" />
                  <Point X="21.804067793745" Y="2.739318358763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.130893572384" Y="2.802846854649" />
                  <Point X="27.810769740554" Y="3.906902940082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.997079203048" Y="2.679233753054" />
                  <Point X="21.665012256659" Y="2.809066786498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.226873699781" Y="2.918281587504" />
                  <Point X="27.703894734958" Y="3.982906629476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.085537144944" Y="2.793206321166" />
                  <Point X="21.539608317692" Y="2.881468816095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.251232041706" Y="3.019794455559" />
                  <Point X="27.591657401136" Y="4.057867987851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.174244377232" Y="2.907227346421" />
                  <Point X="21.414204491099" Y="2.953870867535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.244997035093" Y="3.115360579066" />
                  <Point X="27.467100900314" Y="4.130434742737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.262953102002" Y="3.021248661786" />
                  <Point X="21.288800948454" Y="3.02627297417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.22245597688" Y="3.207757127223" />
                  <Point X="27.342544201912" Y="4.203001459217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.174290734513" Y="3.295172838543" />
                  <Point X="27.212350746233" Y="4.274472501076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.124053810448" Y="3.382185855734" />
                  <Point X="27.066171205079" Y="4.342836162697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.073816886383" Y="3.469198872925" />
                  <Point X="26.919990849364" Y="4.411199665984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.023579962318" Y="3.556211890116" />
                  <Point X="24.893576698855" Y="4.114082742988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.188917463686" Y="4.171491172157" />
                  <Point X="26.757901437798" Y="4.476470762077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.973343220674" Y="3.643224942766" />
                  <Point X="24.807388522977" Y="4.194107544738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.242525479362" Y="4.278689600837" />
                  <Point X="26.584179829952" Y="4.539480788261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.999541498823" Y="3.745095458192" />
                  <Point X="24.768542495218" Y="4.283334727874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.269882176774" Y="4.380785290158" />
                  <Point X="26.398452597828" Y="4.600157157486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.168641554027" Y="3.874743265218" />
                  <Point X="23.069408093082" Y="4.049834543541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.404834395822" Y="4.11503481196" />
                  <Point X="24.743895020366" Y="4.375321830113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.297238874185" Y="4.482880979478" />
                  <Point X="26.176389969404" Y="4.653770641145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.337742163308" Y="4.004391179946" />
                  <Point X="22.936564685367" Y="4.120790486903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.491880134446" Y="4.228732875564" />
                  <Point X="24.719247545513" Y="4.467308932353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.324595571597" Y="4.584976668799" />
                  <Point X="25.954326109941" Y="4.707383885515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.597560347063" Y="4.151672804845" />
                  <Point X="22.858102378052" Y="4.202317045372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.524947531386" Y="4.331938612424" />
                  <Point X="24.69460007066" Y="4.559296034592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.351952148892" Y="4.687072334771" />
                  <Point X="25.688560626314" Y="4.752502394671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.537286947535" Y="4.431115237971" />
                  <Point X="24.66995223146" Y="4.651283066009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.525611494104" Y="4.525623845744" />
                  <Point X="24.645304221799" Y="4.743270064293" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.65310546875" Y="-4.214725097656" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544189453" />
                  <Point X="25.390109375" Y="-3.414620849609" />
                  <Point X="25.300591796875" Y="-3.285643798828" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.1595" Y="-3.230758789062" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.8765" Y="-3.223407470703" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285643798828" />
                  <Point X="24.63751171875" Y="-3.392566894531" />
                  <Point X="24.547994140625" Y="-3.521544189453" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.3217578125" Y="-4.354489257813" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.06216015625" Y="-4.969588867188" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.7671875" Y="-4.90395703125" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.668994140625" Y="-4.717053710938" />
                  <Point X="23.6908515625" Y="-4.5510390625" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.6628828125" Y="-4.396835449219" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.507990234375" Y="-4.109908691406" />
                  <Point X="23.38045703125" Y="-3.998064208984" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.210435546875" Y="-3.976565673828" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.893197265625" Y="-4.051917724609" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.617306640625" Y="-4.3175390625" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.382220703125" Y="-4.315007324219" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.960599609375" Y="-4.026270019531" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.156546875" Y="-3.213547363281" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597594238281" />
                  <Point X="22.4860234375" Y="-2.568765625" />
                  <Point X="22.46867578125" Y="-2.551416992188" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.703345703125" Y="-2.950485107422" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.02637109375" Y="-3.094220458984" />
                  <Point X="20.83830078125" Y="-2.847135742188" />
                  <Point X="20.706693359375" Y="-2.626450683594" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="21.245837890625" Y="-1.876155395508" />
                  <Point X="21.836212890625" Y="-1.423144897461" />
                  <Point X="21.854177734375" Y="-1.396014770508" />
                  <Point X="21.8618828125" Y="-1.366267578125" />
                  <Point X="21.85967578125" Y="-1.334596679688" />
                  <Point X="21.838841796875" Y="-1.310639770508" />
                  <Point X="21.812359375" Y="-1.295053222656" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.890513671875" Y="-1.405736450195" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.146166015625" Y="-1.299165161133" />
                  <Point X="20.072607421875" Y="-1.011187744141" />
                  <Point X="20.0377890625" Y="-0.767741210938" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="20.77001171875" Y="-0.308848114014" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.470685546875" Y="-0.112694488525" />
                  <Point X="21.497677734375" Y="-0.090643913269" />
                  <Point X="21.509296875" Y="-0.062394203186" />
                  <Point X="21.516599609375" Y="-0.031280038834" />
                  <Point X="21.51016015625" Y="-0.002952492237" />
                  <Point X="21.497677734375" Y="0.028083814621" />
                  <Point X="21.473279296875" Y="0.048333656311" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.631296875" Y="0.283456573486" />
                  <Point X="20.001814453125" Y="0.452126037598" />
                  <Point X="20.03494921875" Y="0.676050415039" />
                  <Point X="20.08235546875" Y="0.996414916992" />
                  <Point X="20.1524453125" Y="1.255071411133" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.76465234375" Y="1.457447631836" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866699219" />
                  <Point X="21.296138671875" Y="1.404645141602" />
                  <Point X="21.32972265625" Y="1.41523425293" />
                  <Point X="21.348466796875" Y="1.426056762695" />
                  <Point X="21.3608828125" Y="1.443787353516" />
                  <Point X="21.3720546875" Y="1.470758056641" />
                  <Point X="21.385529296875" Y="1.503291625977" />
                  <Point X="21.38928515625" Y="1.524607055664" />
                  <Point X="21.38368359375" Y="1.545513427734" />
                  <Point X="21.370205078125" Y="1.571407958984" />
                  <Point X="21.353943359375" Y="1.602643310547" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.874703125" Y="1.976281616211" />
                  <Point X="20.52389453125" Y="2.245466308594" />
                  <Point X="20.65578125" Y="2.471420410156" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="21.025650390625" Y="3.025652587891" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.54476171875" Y="3.097886474609" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.90026171875" Y="2.917042480469" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775146484" />
                  <Point X="21.986748046875" Y="2.927404052734" />
                  <Point X="22.014271484375" Y="2.954927246094" />
                  <Point X="22.04747265625" Y="2.988127441406" />
                  <Point X="22.0591015625" Y="3.006381835938" />
                  <Point X="22.061927734375" Y="3.027840820312" />
                  <Point X="22.05853515625" Y="3.066616699219" />
                  <Point X="22.054443359375" Y="3.113390380859" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.841732421875" Y="3.491184326172" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.926298828125" Y="3.928358154297" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.539541015625" Y="4.336791503906" />
                  <Point X="22.858451171875" Y="4.513971679688" />
                  <Point X="22.941533203125" Y="4.405696777344" />
                  <Point X="23.032173828125" Y="4.287572753906" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.09191015625" Y="4.251194335938" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.16487890625" Y="4.218491699219" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.23114453125" Y="4.240870117188" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744628906" />
                  <Point X="23.31391796875" Y="4.294488769531" />
                  <Point X="23.328548828125" Y="4.340891601563" />
                  <Point X="23.346197265625" Y="4.396865234375" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.324640625" Y="4.5964921875" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.616580078125" Y="4.786853027344" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.386404296875" Y="4.94478515625" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.870271484375" Y="4.637791015625" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.160109375" Y="4.705214355469" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.497564453125" Y="4.963543945312" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.1534921875" Y="4.8547578125" />
                  <Point X="26.508458984375" Y="4.769057128906" />
                  <Point X="26.69951953125" Y="4.699758789062" />
                  <Point X="26.931029296875" Y="4.615788085938" />
                  <Point X="27.115626953125" Y="4.529458496094" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.517025390625" Y="4.321241699219" />
                  <Point X="27.73252734375" Y="4.195689453125" />
                  <Point X="27.90070703125" Y="4.076089355469" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.620392578125" Y="3.180029296875" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491514648438" />
                  <Point X="27.21512109375" Y="2.455124511719" />
                  <Point X="27.2033828125" Y="2.411228271484" />
                  <Point X="27.202044921875" Y="2.392326904297" />
                  <Point X="27.20583984375" Y="2.360859863281" />
                  <Point X="27.210416015625" Y="2.322902099609" />
                  <Point X="27.21868359375" Y="2.300811279297" />
                  <Point X="27.238154296875" Y="2.272116210938" />
                  <Point X="27.261642578125" Y="2.237502685547" />
                  <Point X="27.27494140625" Y="2.224203857422" />
                  <Point X="27.30363671875" Y="2.204732910156" />
                  <Point X="27.33825" Y="2.181246337891" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.3918046875" Y="2.169185791016" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.4850546875" Y="2.175677734375" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.356986328125" Y="2.663507324219" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.074765625" Y="2.919530273438" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.2963515625" Y="2.586942138672" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.802796875" Y="1.987626831055" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583832885742" />
                  <Point X="28.253181640625" Y="1.549666015625" />
                  <Point X="28.22158984375" Y="1.508451782227" />
                  <Point X="28.21312109375" Y="1.491501586914" />
                  <Point X="28.203365234375" Y="1.456616943359" />
                  <Point X="28.191595703125" Y="1.414537109375" />
                  <Point X="28.190779296875" Y="1.390965698242" />
                  <Point X="28.1987890625" Y="1.352151977539" />
                  <Point X="28.20844921875" Y="1.305332763672" />
                  <Point X="28.215646484375" Y="1.287956298828" />
                  <Point X="28.2374296875" Y="1.254848022461" />
                  <Point X="28.263703125" Y="1.214910888672" />
                  <Point X="28.280947265625" Y="1.198819946289" />
                  <Point X="28.312513671875" Y="1.181051147461" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.41124609375" Y="1.147978881836" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.25090234375" Y="1.243215576172" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.884287109375" Y="1.176901000977" />
                  <Point X="29.939193359375" Y="0.951366577148" />
                  <Point X="29.968736328125" Y="0.761614746094" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="29.3304296875" Y="0.395718841553" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819503784" />
                  <Point X="28.687158203125" Y="0.208582809448" />
                  <Point X="28.636578125" Y="0.179347137451" />
                  <Point X="28.622265625" Y="0.166926696777" />
                  <Point X="28.597107421875" Y="0.134868942261" />
                  <Point X="28.566759765625" Y="0.096199020386" />
                  <Point X="28.556986328125" Y="0.074734649658" />
                  <Point X="28.548599609375" Y="0.03094524765" />
                  <Point X="28.538484375" Y="-0.021875907898" />
                  <Point X="28.538484375" Y="-0.040684169769" />
                  <Point X="28.54687109375" Y="-0.084473419189" />
                  <Point X="28.556986328125" Y="-0.137294723511" />
                  <Point X="28.566759765625" Y="-0.158759094238" />
                  <Point X="28.59191796875" Y="-0.190816833496" />
                  <Point X="28.622265625" Y="-0.22948677063" />
                  <Point X="28.636578125" Y="-0.24190675354" />
                  <Point X="28.678509765625" Y="-0.266143432617" />
                  <Point X="28.729087890625" Y="-0.295379119873" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.45196875" Y="-0.49084564209" />
                  <Point X="29.998068359375" Y="-0.637172424316" />
                  <Point X="29.9790859375" Y="-0.763077148438" />
                  <Point X="29.948431640625" Y="-0.966412475586" />
                  <Point X="29.910580078125" Y="-1.132279541016" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.095359375" Y="-1.187596435547" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.31254296875" Y="-1.116228393555" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.185447265625" Y="-1.154697265625" />
                  <Point X="28.135705078125" Y="-1.214521728516" />
                  <Point X="28.075703125" Y="-1.286685424805" />
                  <Point X="28.064359375" Y="-1.314070678711" />
                  <Point X="28.05723046875" Y="-1.391545898438" />
                  <Point X="28.048630859375" Y="-1.485000976562" />
                  <Point X="28.056361328125" Y="-1.516622070312" />
                  <Point X="28.101904296875" Y="-1.587461669922" />
                  <Point X="28.156841796875" Y="-1.672912719727" />
                  <Point X="28.168462890625" Y="-1.685540771484" />
                  <Point X="28.82808984375" Y="-2.191691162109" />
                  <Point X="29.339076171875" Y="-2.583784179688" />
                  <Point X="29.290544921875" Y="-2.662314453125" />
                  <Point X="29.2041328125" Y="-2.802139160156" />
                  <Point X="29.125849609375" Y="-2.913371826172" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.362095703125" Y="-2.610614013672" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.6393984375" Y="-2.235624267578" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.4077109375" Y="-2.262068115234" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.33468359375" />
                  <Point X="27.24577734375" Y="-2.416051269531" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.1891640625" Y="-2.546374755859" />
                  <Point X="27.206853515625" Y="-2.644318847656" />
                  <Point X="27.22819140625" Y="-2.762465087891" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.657970703125" Y="-3.512755615234" />
                  <Point X="27.98667578125" Y="-4.082088867188" />
                  <Point X="27.9378359375" Y="-4.116974121094" />
                  <Point X="27.835314453125" Y="-4.190202148438" />
                  <Point X="27.7477734375" Y="-4.246866210938" />
                  <Point X="27.67977734375" Y="-4.29087890625" />
                  <Point X="27.147626953125" Y="-3.597367675781" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.573951171875" Y="-2.918362792969" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.320158203125" Y="-2.845438964844" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.083755859375" Y="-2.936338867188" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045985839844" />
                  <Point X="25.94406640625" Y="-3.158206542969" />
                  <Point X="25.914642578125" Y="-3.293573730469" />
                  <Point X="25.9139296875" Y="-3.310719970703" />
                  <Point X="26.0340546875" Y="-4.223149414062" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.091353515625" Y="-4.941983398438" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.91348828125" Y="-4.977936035156" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#162" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.087589240046" Y="4.682011984302" Z="1.15" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.15" />
                  <Point X="-0.625565786334" Y="5.025726147262" Z="1.15" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.15" />
                  <Point X="-1.403075624132" Y="4.866276683211" Z="1.15" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.15" />
                  <Point X="-1.731088682022" Y="4.621246401114" Z="1.15" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.15" />
                  <Point X="-1.725294383833" Y="4.38720683628" Z="1.15" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.15" />
                  <Point X="-1.794147891476" Y="4.318344114707" Z="1.15" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.15" />
                  <Point X="-1.891157919395" Y="4.326824904047" Z="1.15" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.15" />
                  <Point X="-2.02495484578" Y="4.467415259401" Z="1.15" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.15" />
                  <Point X="-2.490898777771" Y="4.411779151341" Z="1.15" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.15" />
                  <Point X="-3.1102790809" Y="3.999318244864" Z="1.15" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.15" />
                  <Point X="-3.207726253463" Y="3.497464479469" Z="1.15" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.15" />
                  <Point X="-2.997432485308" Y="3.093539306272" Z="1.15" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.15" />
                  <Point X="-3.02724025057" Y="3.021563317317" Z="1.15" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.15" />
                  <Point X="-3.101537159334" Y="2.998132213865" Z="1.15" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.15" />
                  <Point X="-3.436394868828" Y="3.172467695282" Z="1.15" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.15" />
                  <Point X="-4.019969030777" Y="3.087634909834" Z="1.15" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.15" />
                  <Point X="-4.393556798386" Y="2.527905477937" Z="1.15" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.15" />
                  <Point X="-4.161891733669" Y="1.967893921913" Z="1.15" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.15" />
                  <Point X="-3.680302199361" Y="1.579598773737" Z="1.15" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.15" />
                  <Point X="-3.680298294316" Y="1.521170772872" Z="1.15" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.15" />
                  <Point X="-3.725054299057" Y="1.483611002958" Z="1.15" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.15" />
                  <Point X="-4.234978979452" Y="1.538299972978" Z="1.15" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.15" />
                  <Point X="-4.901971065093" Y="1.299428578334" Z="1.15" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.15" />
                  <Point X="-5.020668846551" Y="0.714649400672" Z="1.15" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.15" />
                  <Point X="-4.387801197235" Y="0.266440261951" Z="1.15" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.15" />
                  <Point X="-3.561387014679" Y="0.038537603163" Z="1.15" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.15" />
                  <Point X="-3.54374986551" Y="0.013510176796" Z="1.15" />
                  <Point X="-3.539556741714" Y="0" Z="1.15" />
                  <Point X="-3.544614735625" Y="-0.016296774265" Z="1.15" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.15" />
                  <Point X="-3.563981580444" Y="-0.040338379971" Z="1.15" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.15" />
                  <Point X="-4.249086591538" Y="-0.229271794805" Z="1.15" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.15" />
                  <Point X="-5.017863993632" Y="-0.743540257442" Z="1.15" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.15" />
                  <Point X="-4.908436621557" Y="-1.28025932209" Z="1.15" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.15" />
                  <Point X="-4.109118534658" Y="-1.42402869573" Z="1.15" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.15" />
                  <Point X="-3.204679692677" Y="-1.31538513216" Z="1.15" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.15" />
                  <Point X="-3.196888693617" Y="-1.338714000332" Z="1.15" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.15" />
                  <Point X="-3.790755601627" Y="-1.805207667793" Z="1.15" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.15" />
                  <Point X="-4.342406211938" Y="-2.62078031245" Z="1.15" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.15" />
                  <Point X="-4.019571260284" Y="-3.093223691376" Z="1.15" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.15" />
                  <Point X="-3.277810989185" Y="-2.962506444866" Z="1.15" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.15" />
                  <Point X="-2.563354293456" Y="-2.564976175676" Z="1.15" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.15" />
                  <Point X="-2.892910568303" Y="-3.157267393307" Z="1.15" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.15" />
                  <Point X="-3.076061385528" Y="-4.034607213638" Z="1.15" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.15" />
                  <Point X="-2.65025925522" Y="-4.326238096227" Z="1.15" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.15" />
                  <Point X="-2.349182549246" Y="-4.316697069993" Z="1.15" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.15" />
                  <Point X="-2.085180738284" Y="-4.062211097272" Z="1.15" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.15" />
                  <Point X="-1.798989960659" Y="-3.995178661957" Z="1.15" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.15" />
                  <Point X="-1.531132847586" Y="-4.116220404554" Z="1.15" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.15" />
                  <Point X="-1.392312957069" Y="-4.375310207644" Z="1.15" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.15" />
                  <Point X="-1.386734768903" Y="-4.679246736002" Z="1.15" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.15" />
                  <Point X="-1.251428347995" Y="-4.92109971651" Z="1.15" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.15" />
                  <Point X="-0.95352417825" Y="-4.987392803613" Z="1.15" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.15" />
                  <Point X="-0.636102237626" Y="-4.336149952577" Z="1.15" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.15" />
                  <Point X="-0.327569627717" Y="-3.389795928804" Z="1.15" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.15" />
                  <Point X="-0.114836500658" Y="-3.23988038038" Z="1.15" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.15" />
                  <Point X="0.138522578703" Y="-3.247231664908" Z="1.15" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.15" />
                  <Point X="0.342876231715" Y="-3.411849826964" Z="1.15" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.15" />
                  <Point X="0.598652622775" Y="-4.196386077239" Z="1.15" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.15" />
                  <Point X="0.916269045352" Y="-4.995850754123" Z="1.15" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.15" />
                  <Point X="1.095900759719" Y="-4.95954381364" Z="1.15" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.15" />
                  <Point X="1.077469391407" Y="-4.185342972699" Z="1.15" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.15" />
                  <Point X="0.986768397081" Y="-3.137546593711" Z="1.15" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.15" />
                  <Point X="1.109565059097" Y="-2.943505447159" Z="1.15" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.15" />
                  <Point X="1.318582529136" Y="-2.863948407564" Z="1.15" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.15" />
                  <Point X="1.540754437526" Y="-2.929140742709" Z="1.15" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.15" />
                  <Point X="2.101801955131" Y="-3.596525639874" Z="1.15" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.15" />
                  <Point X="2.768785828473" Y="-4.257560419713" Z="1.15" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.15" />
                  <Point X="2.960738896786" Y="-4.126381136638" Z="1.15" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.15" />
                  <Point X="2.69511430578" Y="-3.4564755239" Z="1.15" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.15" />
                  <Point X="2.249899780098" Y="-2.604152451245" Z="1.15" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.15" />
                  <Point X="2.283866993571" Y="-2.408057784782" Z="1.15" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.15" />
                  <Point X="2.42484059359" Y="-2.275034316396" Z="1.15" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.15" />
                  <Point X="2.624354352267" Y="-2.253548229393" Z="1.15" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.15" />
                  <Point X="3.330938073207" Y="-2.622635194495" Z="1.15" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.15" />
                  <Point X="4.160580471297" Y="-2.910869283072" Z="1.15" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.15" />
                  <Point X="4.326920440091" Y="-2.657319907355" Z="1.15" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.15" />
                  <Point X="3.852370824581" Y="-2.120743463248" Z="1.15" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.15" />
                  <Point X="3.137805804721" Y="-1.52914210515" Z="1.15" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.15" />
                  <Point X="3.100862217177" Y="-1.364847385313" Z="1.15" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.15" />
                  <Point X="3.167993379489" Y="-1.215208546291" Z="1.15" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.15" />
                  <Point X="3.317004753485" Y="-1.133807606309" Z="1.15" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.15" />
                  <Point X="4.082676917651" Y="-1.205888714104" Z="1.15" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.15" />
                  <Point X="4.953169245067" Y="-1.112123391744" Z="1.15" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.15" />
                  <Point X="5.022371380808" Y="-0.739250782891" Z="1.15" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.15" />
                  <Point X="4.45875414032" Y="-0.411269300136" Z="1.15" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.15" />
                  <Point X="3.697373412327" Y="-0.191574902187" Z="1.15" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.15" />
                  <Point X="3.62509514229" Y="-0.128668268591" Z="1.15" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.15" />
                  <Point X="3.589820866242" Y="-0.043789294484" Z="1.15" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.15" />
                  <Point X="3.591550584182" Y="0.05282123673" Z="1.15" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.15" />
                  <Point X="3.63028429611" Y="0.135280469984" Z="1.15" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.15" />
                  <Point X="3.706022002026" Y="0.196573942055" Z="1.15" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.15" />
                  <Point X="4.337213676987" Y="0.378702632633" Z="1.15" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.15" />
                  <Point X="5.011983461319" Y="0.800586742641" Z="1.15" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.15" />
                  <Point X="4.926712717109" Y="1.220007811904" Z="1.15" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.15" />
                  <Point X="4.238220852428" Y="1.324067891471" Z="1.15" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.15" />
                  <Point X="3.41163955611" Y="1.228828037834" Z="1.15" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.15" />
                  <Point X="3.33088423061" Y="1.255902287985" Z="1.15" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.15" />
                  <Point X="3.273043367322" Y="1.313608156799" Z="1.15" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.15" />
                  <Point X="3.241600595094" Y="1.393535631651" Z="1.15" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.15" />
                  <Point X="3.245360127151" Y="1.474429089023" Z="1.15" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.15" />
                  <Point X="3.286708047418" Y="1.550528036371" Z="1.15" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.15" />
                  <Point X="3.827077884251" Y="1.979239067042" Z="1.15" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.15" />
                  <Point X="4.332972433677" Y="2.644108468985" Z="1.15" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.15" />
                  <Point X="4.109194571834" Y="2.980013403414" Z="1.15" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.15" />
                  <Point X="3.32582963409" Y="2.73808868267" Z="1.15" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.15" />
                  <Point X="2.465981879757" Y="2.255260394789" Z="1.15" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.15" />
                  <Point X="2.391634019816" Y="2.250106162645" Z="1.15" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.15" />
                  <Point X="2.325553122027" Y="2.277387311336" Z="1.15" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.15" />
                  <Point X="2.273371288899" Y="2.331471738355" Z="1.15" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.15" />
                  <Point X="2.249323540114" Y="2.398124424016" Z="1.15" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.15" />
                  <Point X="2.257267547869" Y="2.473487696125" Z="1.15" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.15" />
                  <Point X="2.65753667019" Y="3.186309447865" Z="1.15" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.15" />
                  <Point X="2.923527160991" Y="4.148116541173" Z="1.15" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.15" />
                  <Point X="2.536038514268" Y="4.3957243076" Z="1.15" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.15" />
                  <Point X="2.130650966854" Y="4.606030640736" Z="1.15" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.15" />
                  <Point X="1.710410957197" Y="4.778042248186" Z="1.15" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.15" />
                  <Point X="1.159069124916" Y="4.934641203982" Z="1.15" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.15" />
                  <Point X="0.496615135178" Y="5.044552030988" Z="1.15" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.15" />
                  <Point X="0.105655321507" Y="4.749435521649" Z="1.15" />
                  <Point X="0" Y="4.355124473572" Z="1.15" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>