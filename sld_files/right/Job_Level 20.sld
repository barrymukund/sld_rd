<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#155" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1543" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004715820312" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.713328125" Y="-4.072421630859" />
                  <Point X="25.5633046875" Y="-3.512524658203" />
                  <Point X="25.55772265625" Y="-3.497140625" />
                  <Point X="25.542365234375" Y="-3.467377197266" />
                  <Point X="25.481046875" Y="-3.379028564453" />
                  <Point X="25.37863671875" Y="-3.231476806641" />
                  <Point X="25.356751953125" Y="-3.209020263672" />
                  <Point X="25.33049609375" Y="-3.189776611328" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.207609375" Y="-3.146219970703" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097035888672" />
                  <Point X="24.8682890625" Y="-3.126485107422" />
                  <Point X="24.70981640625" Y="-3.175669189453" />
                  <Point X="24.68181640625" Y="-3.18977734375" />
                  <Point X="24.655560546875" Y="-3.209021484375" />
                  <Point X="24.63367578125" Y="-3.231477050781" />
                  <Point X="24.572357421875" Y="-3.319825439453" />
                  <Point X="24.46994921875" Y="-3.467377441406" />
                  <Point X="24.4618125" Y="-3.481571289062" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.198453125" Y="-4.447616699219" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.814142578125" Y="-4.817943847656" />
                  <Point X="23.753583984375" Y="-4.802362304688" />
                  <Point X="23.766978515625" Y="-4.700612792969" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.516223144531" />
                  <Point X="23.760822265625" Y="-4.402261230469" />
                  <Point X="23.722962890625" Y="-4.211931640625" />
                  <Point X="23.712060546875" Y="-4.182965332031" />
                  <Point X="23.69598828125" Y="-4.155127929687" />
                  <Point X="23.67635546875" Y="-4.131204589844" />
                  <Point X="23.58899609375" Y="-4.054591552734" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295898438" />
                  <Point X="23.387115234375" Y="-3.897994628906" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.24102734375" Y="-3.883366699219" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.86073046875" Y="-3.959356201172" />
                  <Point X="22.699376953125" Y="-4.067169921875" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.524228515625" Y="-4.282784667969" />
                  <Point X="22.5198515625" Y="-4.288488769531" />
                  <Point X="22.466826171875" Y="-4.255656738281" />
                  <Point X="22.19828515625" Y="-4.089382568359" />
                  <Point X="22.050796875" Y="-3.975821533203" />
                  <Point X="21.89527734375" Y="-3.856077148438" />
                  <Point X="22.297435546875" Y="-3.159520019531" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647653808594" />
                  <Point X="22.593412109375" Y="-2.616125976562" />
                  <Point X="22.594423828125" Y="-2.585190185547" />
                  <Point X="22.585439453125" Y="-2.555570556641" />
                  <Point X="22.571220703125" Y="-2.526740966797" />
                  <Point X="22.5531953125" Y="-2.501587158203" />
                  <Point X="22.5358515625" Y="-2.484243408203" />
                  <Point X="22.510697265625" Y="-2.466218017578" />
                  <Point X="22.4818671875" Y="-2.451998535156" />
                  <Point X="22.45225" Y="-2.443012207031" />
                  <Point X="22.4213125" Y="-2.444024169922" />
                  <Point X="22.38978515625" Y="-2.450294921875" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.554318359375" Y="-2.926829589844" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="21.129296875" Y="-3.072591064453" />
                  <Point X="20.917140625" Y="-2.793861572266" />
                  <Point X="20.811400390625" Y="-2.616550537109" />
                  <Point X="20.693857421875" Y="-2.419449951172" />
                  <Point X="21.406232421875" Y="-1.872826293945" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.91541796875" Y="-1.475600097656" />
                  <Point X="21.9333828125" Y="-1.448473876953" />
                  <Point X="21.946142578125" Y="-1.41983984375" />
                  <Point X="21.953849609375" Y="-1.390085693359" />
                  <Point X="21.956654296875" Y="-1.3596484375" />
                  <Point X="21.954443359375" Y="-1.327978759766" />
                  <Point X="21.94744140625" Y="-1.298236816406" />
                  <Point X="21.931359375" Y="-1.272255981445" />
                  <Point X="21.91052734375" Y="-1.248300415039" />
                  <Point X="21.887029296875" Y="-1.228767211914" />
                  <Point X="21.860546875" Y="-1.213180664062" />
                  <Point X="21.831283203125" Y="-1.201956665039" />
                  <Point X="21.799396484375" Y="-1.195474975586" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="20.749943359375" Y="-1.328422973633" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.248900390625" Y="-1.317503540039" />
                  <Point X="20.165921875" Y="-0.992650085449" />
                  <Point X="20.137947265625" Y="-0.797049072266" />
                  <Point X="20.107576171875" Y="-0.584698364258" />
                  <Point X="20.911431640625" Y="-0.369305969238" />
                  <Point X="21.467125" Y="-0.220408279419" />
                  <Point X="21.488701171875" Y="-0.211675018311" />
                  <Point X="21.514662109375" Y="-0.197241119385" />
                  <Point X="21.5226640625" Y="-0.192256011963" />
                  <Point X="21.5400234375" Y="-0.180207809448" />
                  <Point X="21.56398046875" Y="-0.158681350708" />
                  <Point X="21.585251953125" Y="-0.127464607239" />
                  <Point X="21.596142578125" Y="-0.101562187195" />
                  <Point X="21.6009453125" Y="-0.086913360596" />
                  <Point X="21.6089765625" Y="-0.053451629639" />
                  <Point X="21.611595703125" Y="-0.032191421509" />
                  <Point X="21.609384765625" Y="-0.010884907722" />
                  <Point X="21.603673828125" Y="0.015096289635" />
                  <Point X="21.59922265625" Y="0.029658296585" />
                  <Point X="21.58601171875" Y="0.063041549683" />
                  <Point X="21.57480078125" Y="0.083555023193" />
                  <Point X="21.558919921875" Y="0.100709541321" />
                  <Point X="21.536705078125" Y="0.119442443848" />
                  <Point X="21.52962890625" Y="0.124861816406" />
                  <Point X="21.51226953125" Y="0.136910018921" />
                  <Point X="21.498076171875" Y="0.145046447754" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="20.539052734375" Y="0.406524627686" />
                  <Point X="20.10818359375" Y="0.521975646973" />
                  <Point X="20.12203515625" Y="0.615586547852" />
                  <Point X="20.17551171875" Y="0.976966552734" />
                  <Point X="20.231830078125" Y="1.184797241211" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.836" Y="1.352234863281" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228149414" />
                  <Point X="21.296865234375" Y="1.305263793945" />
                  <Point X="21.319869140625" Y="1.312517211914" />
                  <Point X="21.358291015625" Y="1.324631347656" />
                  <Point X="21.37722265625" Y="1.332962158203" />
                  <Point X="21.395966796875" Y="1.343784179688" />
                  <Point X="21.4126484375" Y="1.356016113281" />
                  <Point X="21.426287109375" Y="1.371568847656" />
                  <Point X="21.438701171875" Y="1.389299072266" />
                  <Point X="21.448650390625" Y="1.407432861328" />
                  <Point X="21.457880859375" Y="1.429718017578" />
                  <Point X="21.473296875" Y="1.466937011719" />
                  <Point X="21.479083984375" Y="1.4867890625" />
                  <Point X="21.48284375" Y="1.508104858398" />
                  <Point X="21.484197265625" Y="1.528750976562" />
                  <Point X="21.481048828125" Y="1.549200561523" />
                  <Point X="21.4754453125" Y="1.570107666016" />
                  <Point X="21.467951171875" Y="1.589375366211" />
                  <Point X="21.456814453125" Y="1.610771362305" />
                  <Point X="21.4382109375" Y="1.646505004883" />
                  <Point X="21.42671484375" Y="1.66371081543" />
                  <Point X="21.412802734375" Y="1.680289306641" />
                  <Point X="21.39786328125" Y="1.694590332031" />
                  <Point X="20.86551953125" Y="2.103072509766" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.71105859375" Y="2.377667236328" />
                  <Point X="20.9188515625" Y="2.733665039062" />
                  <Point X="21.0680234375" Y="2.925404541016" />
                  <Point X="21.24949609375" Y="3.158662109375" />
                  <Point X="21.548697265625" Y="2.985917724609" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.853205078125" Y="2.825796386719" />
                  <Point X="21.885244140625" Y="2.822993164062" />
                  <Point X="21.93875390625" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.98089453125" Y="2.821588134766" />
                  <Point X="22.000986328125" Y="2.826504638672" />
                  <Point X="22.019537109375" Y="2.835653076172" />
                  <Point X="22.037791015625" Y="2.847281982422" />
                  <Point X="22.053921875" Y="2.860228759766" />
                  <Point X="22.0766640625" Y="2.882970703125" />
                  <Point X="22.114646484375" Y="2.920952148438" />
                  <Point X="22.127595703125" Y="2.937085449219" />
                  <Point X="22.139224609375" Y="2.95533984375" />
                  <Point X="22.14837109375" Y="2.973888916016" />
                  <Point X="22.1532890625" Y="2.993977783203" />
                  <Point X="22.156115234375" Y="3.015437011719" />
                  <Point X="22.15656640625" Y="3.036119873047" />
                  <Point X="22.153763671875" Y="3.068159667969" />
                  <Point X="22.14908203125" Y="3.121669433594" />
                  <Point X="22.145044921875" Y="3.141963378906" />
                  <Point X="22.13853515625" Y="3.16260546875" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.894306640625" Y="3.590120117188" />
                  <Point X="21.81666796875" Y="3.724595703125" />
                  <Point X="21.9374765625" Y="3.817219726562" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.5343203125" Y="4.225214355469" />
                  <Point X="22.832962890625" Y="4.391133789062" />
                  <Point X="22.88191015625" Y="4.32734375" />
                  <Point X="22.9568046875" Y="4.229740234375" />
                  <Point X="22.971109375" Y="4.214797851563" />
                  <Point X="22.987689453125" Y="4.200885742188" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.040548828125" Y="4.170831054688" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.1193828125" Y="4.132331054688" />
                  <Point X="23.1402890625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.12358203125" />
                  <Point X="23.181373046875" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222548828125" Y="4.134482421875" />
                  <Point X="23.259689453125" Y="4.149867675781" />
                  <Point X="23.32172265625" Y="4.175562011719" />
                  <Point X="23.339853515625" Y="4.185509277344" />
                  <Point X="23.357583984375" Y="4.197923828125" />
                  <Point X="23.373134765625" Y="4.211561523438" />
                  <Point X="23.3853671875" Y="4.2282421875" />
                  <Point X="23.396189453125" Y="4.246985839844" />
                  <Point X="23.404521484375" Y="4.265923339844" />
                  <Point X="23.416609375" Y="4.304265136719" />
                  <Point X="23.43680078125" Y="4.368299804688" />
                  <Point X="23.4408359375" Y="4.388583007812" />
                  <Point X="23.44272265625" Y="4.410145019531" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.5818671875" Y="4.678458007812" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.335185546875" Y="4.843142089844" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.79372265625" Y="4.556416015625" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.26708984375" Y="4.7374140625" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.43496875" Y="4.874580078125" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.0796875" Y="4.774847167969" />
                  <Point X="26.4810234375" Y="4.677952148438" />
                  <Point X="26.63348046875" Y="4.622654296875" />
                  <Point X="26.894650390625" Y="4.527926269531" />
                  <Point X="27.042962890625" Y="4.458565917969" />
                  <Point X="27.294576171875" Y="4.340893554688" />
                  <Point X="27.437884765625" Y="4.25740234375" />
                  <Point X="27.68097265625" Y="4.115777832031" />
                  <Point X="27.81611328125" Y="4.019675292969" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.47027734375" Y="3.110018798828" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142080078125" Y="2.539935546875" />
                  <Point X="27.133078125" Y="2.516058105469" />
                  <Point X="27.125037109375" Y="2.485989501953" />
                  <Point X="27.111607421875" Y="2.435771728516" />
                  <Point X="27.108619140625" Y="2.417936767578" />
                  <Point X="27.107728515625" Y="2.380949707031" />
                  <Point X="27.110865234375" Y="2.354948974609" />
                  <Point X="27.116099609375" Y="2.311524902344" />
                  <Point X="27.12144140625" Y="2.289611083984" />
                  <Point X="27.12970703125" Y="2.267520507813" />
                  <Point X="27.140072265625" Y="2.247467773438" />
                  <Point X="27.156162109375" Y="2.2237578125" />
                  <Point X="27.18303125" Y="2.184159179688" />
                  <Point X="27.194466796875" Y="2.170328125" />
                  <Point X="27.22159765625" Y="2.145593505859" />
                  <Point X="27.245306640625" Y="2.129505126953" />
                  <Point X="27.28490625" Y="2.102635986328" />
                  <Point X="27.304955078125" Y="2.092271728516" />
                  <Point X="27.32704296875" Y="2.084006103516" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.37496484375" Y="2.075528320312" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845703125" />
                  <Point X="27.47320703125" Y="2.074171142578" />
                  <Point X="27.503275390625" Y="2.082211914062" />
                  <Point X="27.553494140625" Y="2.095640869141" />
                  <Point X="27.56528125" Y="2.099637451172" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.52199609375" Y="2.649079589844" />
                  <Point X="28.967328125" Y="2.90619140625" />
                  <Point X="28.979078125" Y="2.889861083984" />
                  <Point X="29.123275390625" Y="2.6894609375" />
                  <Point X="29.198607421875" Y="2.564970703125" />
                  <Point X="29.26219921875" Y="2.459883789063" />
                  <Point X="28.655640625" Y="1.994454345703" />
                  <Point X="28.23078515625" Y="1.668451171875" />
                  <Point X="28.221427734375" Y="1.660243164062" />
                  <Point X="28.203974609375" Y="1.641627685547" />
                  <Point X="28.182333984375" Y="1.613396240234" />
                  <Point X="28.146193359375" Y="1.566246337891" />
                  <Point X="28.13660546875" Y="1.550909057617" />
                  <Point X="28.1216328125" Y="1.517090209961" />
                  <Point X="28.1135703125" Y="1.48826574707" />
                  <Point X="28.100107421875" Y="1.440125610352" />
                  <Point X="28.09665234375" Y="1.41782409668" />
                  <Point X="28.0958359375" Y="1.394254882812" />
                  <Point X="28.097740234375" Y="1.371763549805" />
                  <Point X="28.104359375" Y="1.339692749023" />
                  <Point X="28.11541015625" Y="1.286130859375" />
                  <Point X="28.1206796875" Y="1.268982421875" />
                  <Point X="28.13628125" Y="1.235741577148" />
                  <Point X="28.154279296875" Y="1.208384765625" />
                  <Point X="28.184337890625" Y="1.162696044922" />
                  <Point X="28.198892578125" Y="1.145451049805" />
                  <Point X="28.21613671875" Y="1.129360717773" />
                  <Point X="28.234345703125" Y="1.116034912109" />
                  <Point X="28.260427734375" Y="1.101352905273" />
                  <Point X="28.30398828125" Y="1.076832641602" />
                  <Point X="28.3205234375" Y="1.069500976563" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.391384765625" Y="1.054777832031" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.3749296875" Y="1.163724487305" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.78400390625" Y="1.187204589844" />
                  <Point X="29.84594140625" Y="0.932787536621" />
                  <Point X="29.869677734375" Y="0.780329345703" />
                  <Point X="29.890865234375" Y="0.644238708496" />
                  <Point X="29.203474609375" Y="0.460052581787" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.7047890625" Y="0.325585754395" />
                  <Point X="28.681546875" Y="0.315068023682" />
                  <Point X="28.646900390625" Y="0.295041717529" />
                  <Point X="28.589037109375" Y="0.261595489502" />
                  <Point X="28.574314453125" Y="0.251098419189" />
                  <Point X="28.54753125" Y="0.225575775146" />
                  <Point X="28.526744140625" Y="0.199087158203" />
                  <Point X="28.492025390625" Y="0.15484815979" />
                  <Point X="28.48030078125" Y="0.135566833496" />
                  <Point X="28.47052734375" Y="0.114102310181" />
                  <Point X="28.463681640625" Y="0.092604545593" />
                  <Point X="28.456751953125" Y="0.056422428131" />
                  <Point X="28.4451796875" Y="-0.00400592041" />
                  <Point X="28.443484375" Y="-0.021875743866" />
                  <Point X="28.4451796875" Y="-0.05855406189" />
                  <Point X="28.452109375" Y="-0.094736328125" />
                  <Point X="28.463681640625" Y="-0.155164672852" />
                  <Point X="28.47052734375" Y="-0.17666229248" />
                  <Point X="28.48030078125" Y="-0.198126815796" />
                  <Point X="28.492025390625" Y="-0.21740814209" />
                  <Point X="28.5128125" Y="-0.243896774292" />
                  <Point X="28.54753125" Y="-0.288135772705" />
                  <Point X="28.560001953125" Y="-0.301238616943" />
                  <Point X="28.589037109375" Y="-0.324155609131" />
                  <Point X="28.62368359375" Y="-0.344181762695" />
                  <Point X="28.681546875" Y="-0.37762802124" />
                  <Point X="28.692708984375" Y="-0.383138305664" />
                  <Point X="28.716580078125" Y="-0.392149810791" />
                  <Point X="29.529748046875" Y="-0.610037719727" />
                  <Point X="29.891474609375" Y="-0.706961791992" />
                  <Point X="29.889603515625" Y="-0.719366088867" />
                  <Point X="29.855025390625" Y="-0.94872467041" />
                  <Point X="29.824609375" Y="-1.082010253906" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="28.98904296875" Y="-1.077779663086" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.306662109375" Y="-1.020288513184" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.1639765625" Y="-1.056596069336" />
                  <Point X="28.1361484375" Y="-1.07348840332" />
                  <Point X="28.1123984375" Y="-1.093959716797" />
                  <Point X="28.071296875" Y="-1.143391357422" />
                  <Point X="28.002654296875" Y="-1.225947753906" />
                  <Point X="27.987931640625" Y="-1.250334838867" />
                  <Point X="27.97658984375" Y="-1.277720214844" />
                  <Point X="27.969759765625" Y="-1.305365844727" />
                  <Point X="27.963869140625" Y="-1.369381958008" />
                  <Point X="27.95403125" Y="-1.476296142578" />
                  <Point X="27.95634765625" Y="-1.507561767578" />
                  <Point X="27.964078125" Y="-1.539182739258" />
                  <Point X="27.976451171875" Y="-1.567996337891" />
                  <Point X="28.01408203125" Y="-1.626529663086" />
                  <Point X="28.076931640625" Y="-1.724286987305" />
                  <Point X="28.0869375" Y="-1.737243286133" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.865255859375" Y="-2.339953857422" />
                  <Point X="29.213123046875" Y="-2.606882568359" />
                  <Point X="29.124802734375" Y="-2.749798095703" />
                  <Point X="29.06191015625" Y="-2.839160644531" />
                  <Point X="29.028982421875" Y="-2.885945556641" />
                  <Point X="28.303861328125" Y="-2.467296142578" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.673294921875" Y="-2.145209472656" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135176757812" />
                  <Point X="27.3776015625" Y="-2.170560546875" />
                  <Point X="27.26531640625" Y="-2.229655761719" />
                  <Point X="27.242384765625" Y="-2.246549804688" />
                  <Point X="27.22142578125" Y="-2.267509521484" />
                  <Point X="27.204533203125" Y="-2.290438476562" />
                  <Point X="27.1691484375" Y="-2.357670898438" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531908203125" />
                  <Point X="27.09567578125" Y="-2.563260009766" />
                  <Point X="27.11029296875" Y="-2.644189208984" />
                  <Point X="27.134703125" Y="-2.779350341797" />
                  <Point X="27.13898828125" Y="-2.795139648438" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.636744140625" Y="-3.665989257812" />
                  <Point X="27.861287109375" Y="-4.054906494141" />
                  <Point X="27.781873046875" Y="-4.111629394531" />
                  <Point X="27.7115234375" Y="-4.157164550781" />
                  <Point X="27.701765625" Y="-4.163480957031" />
                  <Point X="27.142310546875" Y="-3.434385498047" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.642107421875" Y="-2.849241455078" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.3298046875" Y="-2.749149658203" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.03719140625" Y="-2.851507568359" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968861083984" />
                  <Point X="25.88725" Y="-2.996687744141" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.855470703125" Y="-3.118534179688" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627441406" />
                  <Point X="25.8197421875" Y="-3.323120361328" />
                  <Point X="25.957166015625" Y="-4.366954101563" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="25.975689453125" Y="-4.870081054688" />
                  <Point X="25.92931640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94157421875" Y="-4.75263671875" />
                  <Point X="23.85875390625" Y="-4.731328125" />
                  <Point X="23.861166015625" Y="-4.71301171875" />
                  <Point X="23.8792265625" Y="-4.575837402344" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030761719" />
                  <Point X="23.8782421875" Y="-4.50932421875" />
                  <Point X="23.876666015625" Y="-4.497688476562" />
                  <Point X="23.85399609375" Y="-4.3837265625" />
                  <Point X="23.81613671875" Y="-4.193396972656" />
                  <Point X="23.811873046875" Y="-4.178467285156" />
                  <Point X="23.800970703125" Y="-4.149500976562" />
                  <Point X="23.79433203125" Y="-4.135464355469" />
                  <Point X="23.778259765625" Y="-4.107626953125" />
                  <Point X="23.76942578125" Y="-4.094861572266" />
                  <Point X="23.74979296875" Y="-4.070938232422" />
                  <Point X="23.738994140625" Y="-4.059780273438" />
                  <Point X="23.651634765625" Y="-3.983167236328" />
                  <Point X="23.505734375" Y="-3.855215332031" />
                  <Point X="23.49326171875" Y="-3.845965087891" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822527587891" />
                  <Point X="23.423470703125" Y="-3.810226318359" />
                  <Point X="23.4086875" Y="-3.805476318359" />
                  <Point X="23.378544921875" Y="-3.798447998047" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.247240234375" Y="-3.788570068359" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.808270019531" />
                  <Point X="22.9045625" Y="-3.8158125" />
                  <Point X="22.807951171875" Y="-3.880366943359" />
                  <Point X="22.64659765625" Y="-3.988180664062" />
                  <Point X="22.64031640625" Y="-3.992759277344" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629150391" />
                  <Point X="22.496798828125" Y="-4.162478027344" />
                  <Point X="22.25240625" Y="-4.011156494141" />
                  <Point X="22.10875390625" Y="-3.900549072266" />
                  <Point X="22.01913671875" Y="-3.831546386719" />
                  <Point X="22.37970703125" Y="-3.207020019531" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.710084472656" />
                  <Point X="22.67605078125" Y="-2.681118652344" />
                  <Point X="22.680314453125" Y="-2.666187988281" />
                  <Point X="22.6865859375" Y="-2.63466015625" />
                  <Point X="22.688361328125" Y="-2.619231201172" />
                  <Point X="22.689373046875" Y="-2.588295410156" />
                  <Point X="22.685333984375" Y="-2.557614990234" />
                  <Point X="22.676349609375" Y="-2.527995361328" />
                  <Point X="22.670640625" Y="-2.513549316406" />
                  <Point X="22.656421875" Y="-2.484719726562" />
                  <Point X="22.64844140625" Y="-2.471404785156" />
                  <Point X="22.630416015625" Y="-2.446250976562" />
                  <Point X="22.62037109375" Y="-2.434412109375" />
                  <Point X="22.60302734375" Y="-2.417068359375" />
                  <Point X="22.5911875" Y="-2.407023193359" />
                  <Point X="22.566033203125" Y="-2.388997802734" />
                  <Point X="22.55271875" Y="-2.381017578125" />
                  <Point X="22.523888671875" Y="-2.366798095703" />
                  <Point X="22.50944921875" Y="-2.361091064453" />
                  <Point X="22.47983203125" Y="-2.352104736328" />
                  <Point X="22.44914453125" Y="-2.348062988281" />
                  <Point X="22.41820703125" Y="-2.349074951172" />
                  <Point X="22.402779296875" Y="-2.350849365234" />
                  <Point X="22.371251953125" Y="-2.357120117188" />
                  <Point X="22.356322265625" Y="-2.361383789062" />
                  <Point X="22.32735546875" Y="-2.372286132812" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.506818359375" Y="-2.844557128906" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="21.204890625" Y="-3.015052734375" />
                  <Point X="20.995978515625" Y="-2.740585693359" />
                  <Point X="20.8929921875" Y="-2.567892333984" />
                  <Point X="20.818734375" Y="-2.443373535156" />
                  <Point X="21.464064453125" Y="-1.948194824219" />
                  <Point X="21.951876953125" Y="-1.573882080078" />
                  <Point X="21.963513671875" Y="-1.563313110352" />
                  <Point X="21.98488671875" Y="-1.540399658203" />
                  <Point X="21.994623046875" Y="-1.528055297852" />
                  <Point X="22.012587890625" Y="-1.500928833008" />
                  <Point X="22.02015625" Y="-1.487141845703" />
                  <Point X="22.032916015625" Y="-1.45850793457" />
                  <Point X="22.038107421875" Y="-1.443661010742" />
                  <Point X="22.045814453125" Y="-1.413906860352" />
                  <Point X="22.04844921875" Y="-1.398802734375" />
                  <Point X="22.05125390625" Y="-1.368365600586" />
                  <Point X="22.051423828125" Y="-1.353032470703" />
                  <Point X="22.049212890625" Y="-1.321362670898" />
                  <Point X="22.046916015625" Y="-1.306208740234" />
                  <Point X="22.0399140625" Y="-1.276466796875" />
                  <Point X="22.02821875" Y="-1.248236206055" />
                  <Point X="22.01213671875" Y="-1.222255371094" />
                  <Point X="22.003044921875" Y="-1.209917114258" />
                  <Point X="21.982212890625" Y="-1.185961547852" />
                  <Point X="21.971255859375" Y="-1.175245239258" />
                  <Point X="21.9477578125" Y="-1.155712036133" />
                  <Point X="21.935216796875" Y="-1.146895141602" />
                  <Point X="21.908734375" Y="-1.13130859375" />
                  <Point X="21.89456640625" Y="-1.124481079102" />
                  <Point X="21.865302734375" Y="-1.113257080078" />
                  <Point X="21.85020703125" Y="-1.108860595703" />
                  <Point X="21.8183203125" Y="-1.10237890625" />
                  <Point X="21.802703125" Y="-1.100532592773" />
                  <Point X="21.77137890625" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196777344" />
                  <Point X="20.73754296875" Y="-1.234235717773" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.259236328125" Y="-0.974111572266" />
                  <Point X="20.231990234375" Y="-0.783599121094" />
                  <Point X="20.213548828125" Y="-0.654654541016" />
                  <Point X="20.93601953125" Y="-0.461068939209" />
                  <Point X="21.491712890625" Y="-0.312171203613" />
                  <Point X="21.502767578125" Y="-0.308468109131" />
                  <Point X="21.52434375" Y="-0.299734863281" />
                  <Point X="21.534865234375" Y="-0.294704742432" />
                  <Point X="21.560826171875" Y="-0.280270935059" />
                  <Point X="21.576830078125" Y="-0.270300689697" />
                  <Point X="21.594189453125" Y="-0.258252593994" />
                  <Point X="21.603517578125" Y="-0.250871810913" />
                  <Point X="21.627474609375" Y="-0.229345428467" />
                  <Point X="21.642486328125" Y="-0.212176651001" />
                  <Point X="21.6637578125" Y="-0.180959884644" />
                  <Point X="21.672826171875" Y="-0.164284988403" />
                  <Point X="21.683716796875" Y="-0.13838269043" />
                  <Point X="21.6864140625" Y="-0.131158569336" />
                  <Point X="21.693322265625" Y="-0.109084945679" />
                  <Point X="21.701353515625" Y="-0.07562323761" />
                  <Point X="21.703263671875" Y="-0.06506729126" />
                  <Point X="21.7058828125" Y="-0.04380708313" />
                  <Point X="21.706087890625" Y="-0.022386058807" />
                  <Point X="21.703876953125" Y="-0.001079624295" />
                  <Point X="21.702169921875" Y="0.009510205269" />
                  <Point X="21.696458984375" Y="0.035491424561" />
                  <Point X="21.6945234375" Y="0.042866542816" />
                  <Point X="21.687556640625" Y="0.06461542511" />
                  <Point X="21.674345703125" Y="0.097998664856" />
                  <Point X="21.669375" Y="0.10860067749" />
                  <Point X="21.6581640625" Y="0.129114212036" />
                  <Point X="21.644513671875" Y="0.148092315674" />
                  <Point X="21.6286328125" Y="0.165246826172" />
                  <Point X="21.620162109375" Y="0.173334732056" />
                  <Point X="21.597947265625" Y="0.192067611694" />
                  <Point X="21.583794921875" Y="0.202906539917" />
                  <Point X="21.566435546875" Y="0.214954803467" />
                  <Point X="21.559515625" Y="0.219328186035" />
                  <Point X="21.53438671875" Y="0.23283354187" />
                  <Point X="21.503435546875" Y="0.245635482788" />
                  <Point X="21.491712890625" Y="0.249611297607" />
                  <Point X="20.563640625" Y="0.498287628174" />
                  <Point X="20.214552734375" Y="0.591825256348" />
                  <Point X="20.21601171875" Y="0.601680908203" />
                  <Point X="20.26866796875" Y="0.957517883301" />
                  <Point X="20.3235234375" Y="1.159950073242" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="20.823599609375" Y="1.258047607422" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815673828" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.204703369141" />
                  <Point X="21.28485546875" Y="1.20658972168" />
                  <Point X="21.295109375" Y="1.208053466797" />
                  <Point X="21.3153984375" Y="1.212089111328" />
                  <Point X="21.32543359375" Y="1.214661010742" />
                  <Point X="21.3484375" Y="1.221914428711" />
                  <Point X="21.386859375" Y="1.234028442383" />
                  <Point X="21.3965546875" Y="1.237677978516" />
                  <Point X="21.415486328125" Y="1.246008789062" />
                  <Point X="21.42472265625" Y="1.250689819336" />
                  <Point X="21.443466796875" Y="1.261511962891" />
                  <Point X="21.452142578125" Y="1.267173095703" />
                  <Point X="21.46882421875" Y="1.279404907227" />
                  <Point X="21.48407421875" Y="1.293380126953" />
                  <Point X="21.497712890625" Y="1.308932861328" />
                  <Point X="21.504107421875" Y="1.317081420898" />
                  <Point X="21.516521484375" Y="1.334811645508" />
                  <Point X="21.52198828125" Y="1.343602783203" />
                  <Point X="21.5319375" Y="1.361736572266" />
                  <Point X="21.536419921875" Y="1.371079101563" />
                  <Point X="21.545650390625" Y="1.393364257812" />
                  <Point X="21.56106640625" Y="1.430583251953" />
                  <Point X="21.5645" Y="1.440349975586" />
                  <Point X="21.570287109375" Y="1.460202026367" />
                  <Point X="21.572640625" Y="1.470287353516" />
                  <Point X="21.576400390625" Y="1.491603149414" />
                  <Point X="21.577640625" Y="1.501890258789" />
                  <Point X="21.578994140625" Y="1.522536376953" />
                  <Point X="21.578091796875" Y="1.54320690918" />
                  <Point X="21.574943359375" Y="1.563656494141" />
                  <Point X="21.572810546875" Y="1.573794433594" />
                  <Point X="21.56720703125" Y="1.594701538086" />
                  <Point X="21.563984375" Y="1.604544677734" />
                  <Point X="21.556490234375" Y="1.62381237793" />
                  <Point X="21.55221875" Y="1.633237304688" />
                  <Point X="21.54108203125" Y="1.654633300781" />
                  <Point X="21.522478515625" Y="1.690366943359" />
                  <Point X="21.517201171875" Y="1.699282714844" />
                  <Point X="21.505705078125" Y="1.716488525391" />
                  <Point X="21.499486328125" Y="1.724778442383" />
                  <Point X="21.48557421875" Y="1.741356933594" />
                  <Point X="21.47849609375" Y="1.748914794922" />
                  <Point X="21.463556640625" Y="1.763215698242" />
                  <Point X="21.4556953125" Y="1.769958862305" />
                  <Point X="20.9233515625" Y="2.178441162109" />
                  <Point X="20.77238671875" Y="2.294280761719" />
                  <Point X="20.79310546875" Y="2.329777832031" />
                  <Point X="20.99771484375" Y="2.680321533203" />
                  <Point X="21.14300390625" Y="2.867070068359" />
                  <Point X="21.273662109375" Y="3.035013183594" />
                  <Point X="21.501197265625" Y="2.903645263672" />
                  <Point X="21.74584375" Y="2.762398681641" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774015625" Y="2.749385742188" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.814384765625" Y="2.736657226562" />
                  <Point X="21.834669921875" Y="2.732622070312" />
                  <Point X="21.844923828125" Y="2.731157958984" />
                  <Point X="21.876962890625" Y="2.728354736328" />
                  <Point X="21.93047265625" Y="2.723673339844" />
                  <Point X="21.940826171875" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.993294921875" Y="2.727400878906" />
                  <Point X="22.003474609375" Y="2.729310791016" />
                  <Point X="22.02356640625" Y="2.734227294922" />
                  <Point X="22.04300390625" Y="2.741302001953" />
                  <Point X="22.0615546875" Y="2.750450439453" />
                  <Point X="22.070580078125" Y="2.755530761719" />
                  <Point X="22.088833984375" Y="2.767159667969" />
                  <Point X="22.09725390625" Y="2.773193847656" />
                  <Point X="22.113384765625" Y="2.786140625" />
                  <Point X="22.121095703125" Y="2.793053222656" />
                  <Point X="22.143837890625" Y="2.815795166016" />
                  <Point X="22.1818203125" Y="2.853776611328" />
                  <Point X="22.188734375" Y="2.861487060547" />
                  <Point X="22.20168359375" Y="2.877620361328" />
                  <Point X="22.20771875" Y="2.886043212891" />
                  <Point X="22.21934765625" Y="2.904297607422" />
                  <Point X="22.2244296875" Y="2.913325683594" />
                  <Point X="22.233576171875" Y="2.931874755859" />
                  <Point X="22.240646484375" Y="2.951299072266" />
                  <Point X="22.245564453125" Y="2.971387939453" />
                  <Point X="22.2474765625" Y="2.981573486328" />
                  <Point X="22.250302734375" Y="3.003032714844" />
                  <Point X="22.251091796875" Y="3.013365234375" />
                  <Point X="22.25154296875" Y="3.034048095703" />
                  <Point X="22.251205078125" Y="3.0443984375" />
                  <Point X="22.24840234375" Y="3.076438232422" />
                  <Point X="22.243720703125" Y="3.129947998047" />
                  <Point X="22.242255859375" Y="3.140204833984" />
                  <Point X="22.23821875" Y="3.160498779297" />
                  <Point X="22.235646484375" Y="3.170535888672" />
                  <Point X="22.22913671875" Y="3.191177978516" />
                  <Point X="22.225486328125" Y="3.200873291016" />
                  <Point X="22.21715625" Y="3.21980078125" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="21.976578125" Y="3.637620117188" />
                  <Point X="21.94061328125" Y="3.699915283203" />
                  <Point X="21.995279296875" Y="3.741828369141" />
                  <Point X="22.3516328125" Y="4.015040283203" />
                  <Point X="22.58045703125" Y="4.142170410156" />
                  <Point X="22.807474609375" Y="4.268296386719" />
                  <Point X="22.881435546875" Y="4.171908203125" />
                  <Point X="22.888181640625" Y="4.164045410156" />
                  <Point X="22.902486328125" Y="4.149103027344" />
                  <Point X="22.910044921875" Y="4.142022949219" />
                  <Point X="22.926625" Y="4.128110839844" />
                  <Point X="22.9349140625" Y="4.121894042969" />
                  <Point X="22.95211328125" Y="4.110402832031" />
                  <Point X="22.9610234375" Y="4.105128417969" />
                  <Point X="22.99668359375" Y="4.086564941406" />
                  <Point X="23.056240234375" Y="4.055561523438" />
                  <Point X="23.065673828125" Y="4.051286865234" />
                  <Point X="23.084951171875" Y="4.043790283203" />
                  <Point X="23.094794921875" Y="4.040568359375" />
                  <Point X="23.115701171875" Y="4.034966308594" />
                  <Point X="23.1258359375" Y="4.032834960938" />
                  <Point X="23.146279296875" Y="4.029687988281" />
                  <Point X="23.166947265625" Y="4.028785400391" />
                  <Point X="23.187587890625" Y="4.030138427734" />
                  <Point X="23.197869140625" Y="4.031378417969" />
                  <Point X="23.21918359375" Y="4.03513671875" />
                  <Point X="23.229271484375" Y="4.037488525391" />
                  <Point X="23.2491328125" Y="4.043277587891" />
                  <Point X="23.25890625" Y="4.04671484375" />
                  <Point X="23.296046875" Y="4.062100097656" />
                  <Point X="23.358080078125" Y="4.087794433594" />
                  <Point X="23.36741796875" Y="4.092273681641" />
                  <Point X="23.385548828125" Y="4.102220703125" />
                  <Point X="23.394341796875" Y="4.107688964844" />
                  <Point X="23.412072265625" Y="4.120103515625" />
                  <Point X="23.42022265625" Y="4.126499023438" />
                  <Point X="23.4357734375" Y="4.14013671875" />
                  <Point X="23.449744140625" Y="4.155382324219" />
                  <Point X="23.4619765625" Y="4.172062988281" />
                  <Point X="23.467638671875" Y="4.180740234375" />
                  <Point X="23.4784609375" Y="4.199483886719" />
                  <Point X="23.48314453125" Y="4.208727539062" />
                  <Point X="23.4914765625" Y="4.227665039062" />
                  <Point X="23.495125" Y="4.237358886719" />
                  <Point X="23.507212890625" Y="4.275700683594" />
                  <Point X="23.527404296875" Y="4.339735351562" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370046875" />
                  <Point X="23.535474609375" Y="4.380301757812" />
                  <Point X="23.537361328125" Y="4.401863769531" />
                  <Point X="23.53769921875" Y="4.412216796875" />
                  <Point X="23.537248046875" Y="4.4328984375" />
                  <Point X="23.536458984375" Y="4.443227050781" />
                  <Point X="23.520734375" Y="4.562655761719" />
                  <Point X="23.607513671875" Y="4.586985351562" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.346228515625" Y="4.748786132813" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.701958984375" Y="4.531828613281" />
                  <Point X="24.77433203125" Y="4.261728027344" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.358853515625" Y="4.712826171875" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.42507421875" Y="4.780096679688" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.057392578125" Y="4.682500488281" />
                  <Point X="26.453591796875" Y="4.586845703125" />
                  <Point X="26.601087890625" Y="4.533347167969" />
                  <Point X="26.85826171875" Y="4.440068847656" />
                  <Point X="27.00271875" Y="4.372511230469" />
                  <Point X="27.250453125" Y="4.256652832031" />
                  <Point X="27.3900625" Y="4.175316894531" />
                  <Point X="27.629419921875" Y="4.035865966797" />
                  <Point X="27.761056640625" Y="3.942255126953" />
                  <Point X="27.81778125" Y="3.901916015625" />
                  <Point X="27.38800390625" Y="3.157518798828" />
                  <Point X="27.065310546875" Y="2.598597900391" />
                  <Point X="27.06237890625" Y="2.593110595703" />
                  <Point X="27.0531875" Y="2.573448486328" />
                  <Point X="27.044185546875" Y="2.549571044922" />
                  <Point X="27.041302734375" Y="2.540600830078" />
                  <Point X="27.03326171875" Y="2.510532226562" />
                  <Point X="27.01983203125" Y="2.460314453125" />
                  <Point X="27.0179140625" Y="2.451470214844" />
                  <Point X="27.013646484375" Y="2.420223632812" />
                  <Point X="27.012755859375" Y="2.383236572266" />
                  <Point X="27.013412109375" Y="2.369571533203" />
                  <Point X="27.016548828125" Y="2.343570800781" />
                  <Point X="27.021783203125" Y="2.300146728516" />
                  <Point X="27.023802734375" Y="2.289026123047" />
                  <Point X="27.02914453125" Y="2.267112304688" />
                  <Point X="27.032466796875" Y="2.256319091797" />
                  <Point X="27.040732421875" Y="2.234228515625" />
                  <Point X="27.045314453125" Y="2.223898193359" />
                  <Point X="27.0556796875" Y="2.203845458984" />
                  <Point X="27.061462890625" Y="2.194123046875" />
                  <Point X="27.077552734375" Y="2.170413085938" />
                  <Point X="27.104421875" Y="2.130814453125" />
                  <Point X="27.10981640625" Y="2.123624267578" />
                  <Point X="27.130462890625" Y="2.100124267578" />
                  <Point X="27.15759375" Y="2.075389648438" />
                  <Point X="27.16825390625" Y="2.066983642578" />
                  <Point X="27.191962890625" Y="2.050895263672" />
                  <Point X="27.2315625" Y="2.024026000977" />
                  <Point X="27.24128125" Y="2.018245361328" />
                  <Point X="27.261330078125" Y="2.007881103516" />
                  <Point X="27.27166015625" Y="2.003297485352" />
                  <Point X="27.293748046875" Y="1.995031860352" />
                  <Point X="27.304548828125" Y="1.991707397461" />
                  <Point X="27.326470703125" Y="1.986365112305" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.363591796875" Y="1.981211547852" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.416044921875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975497314453" />
                  <Point X="27.484314453125" Y="1.979822875977" />
                  <Point X="27.49775" Y="1.982395874023" />
                  <Point X="27.527818359375" Y="1.990436767578" />
                  <Point X="27.578037109375" Y="2.003865722656" />
                  <Point X="27.584" Y="2.005671875" />
                  <Point X="27.604400390625" Y="2.013065307617" />
                  <Point X="27.627654296875" Y="2.023572753906" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.56949609375" Y="2.566807128906" />
                  <Point X="28.94040625" Y="2.780951660156" />
                  <Point X="29.04395703125" Y="2.637041748047" />
                  <Point X="29.117330078125" Y="2.515787841797" />
                  <Point X="29.136884765625" Y="2.483471679688" />
                  <Point X="28.59780859375" Y="2.069822753906" />
                  <Point X="28.172953125" Y="1.743819702148" />
                  <Point X="28.168140625" Y="1.739869262695" />
                  <Point X="28.152123046875" Y="1.725219726562" />
                  <Point X="28.134669921875" Y="1.706604248047" />
                  <Point X="28.128578125" Y="1.699422973633" />
                  <Point X="28.1069375" Y="1.67119140625" />
                  <Point X="28.070796875" Y="1.624041503906" />
                  <Point X="28.065638671875" Y="1.616604125977" />
                  <Point X="28.04973828125" Y="1.589367919922" />
                  <Point X="28.034765625" Y="1.555549072266" />
                  <Point X="28.03014453125" Y="1.542680419922" />
                  <Point X="28.02208203125" Y="1.513855957031" />
                  <Point X="28.008619140625" Y="1.465715942383" />
                  <Point X="28.0062265625" Y="1.454670043945" />
                  <Point X="28.002771484375" Y="1.432368530273" />
                  <Point X="28.001708984375" Y="1.421112792969" />
                  <Point X="28.000892578125" Y="1.397543579102" />
                  <Point X="28.001173828125" Y="1.386240112305" />
                  <Point X="28.003078125" Y="1.363748779297" />
                  <Point X="28.004701171875" Y="1.352561035156" />
                  <Point X="28.0113203125" Y="1.320490234375" />
                  <Point X="28.02237109375" Y="1.266928466797" />
                  <Point X="28.0246015625" Y="1.258226196289" />
                  <Point X="28.034681640625" Y="1.228619018555" />
                  <Point X="28.050283203125" Y="1.195378173828" />
                  <Point X="28.056916015625" Y="1.183527709961" />
                  <Point X="28.0749140625" Y="1.156170898438" />
                  <Point X="28.10497265625" Y="1.110482177734" />
                  <Point X="28.11173828125" Y="1.101422973633" />
                  <Point X="28.12629296875" Y="1.084177978516" />
                  <Point X="28.13408203125" Y="1.0759921875" />
                  <Point X="28.151326171875" Y="1.059901977539" />
                  <Point X="28.160033203125" Y="1.052697021484" />
                  <Point X="28.1782421875" Y="1.039371459961" />
                  <Point X="28.187744140625" Y="1.03325012207" />
                  <Point X="28.213826171875" Y="1.018568054199" />
                  <Point X="28.25738671875" Y="0.994047790527" />
                  <Point X="28.26548046875" Y="0.989986816406" />
                  <Point X="28.294681640625" Y="0.978083312988" />
                  <Point X="28.33027734375" Y="0.968021118164" />
                  <Point X="28.343671875" Y="0.965257751465" />
                  <Point X="28.3789375" Y="0.960596801758" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199768066" />
                  <Point X="28.46571875" Y="0.951222839355" />
                  <Point X="28.49121875" Y="0.952032409668" />
                  <Point X="28.50060546875" Y="0.952797180176" />
                  <Point X="29.387330078125" Y="1.069537231445" />
                  <Point X="29.704703125" Y="1.111319824219" />
                  <Point X="29.752689453125" Y="0.91421295166" />
                  <Point X="29.77580859375" Y="0.76571484375" />
                  <Point X="29.783873046875" Y="0.713921203613" />
                  <Point X="29.17888671875" Y="0.551815490723" />
                  <Point X="28.6919921875" Y="0.421352813721" />
                  <Point X="28.686033203125" Y="0.419544525146" />
                  <Point X="28.665623046875" Y="0.412136260986" />
                  <Point X="28.642380859375" Y="0.401618530273" />
                  <Point X="28.634005859375" Y="0.397316619873" />
                  <Point X="28.599359375" Y="0.377290283203" />
                  <Point X="28.54149609375" Y="0.343844024658" />
                  <Point X="28.53388671875" Y="0.338947631836" />
                  <Point X="28.50877734375" Y="0.319872497559" />
                  <Point X="28.481994140625" Y="0.294349914551" />
                  <Point X="28.472796875" Y="0.284224517822" />
                  <Point X="28.452009765625" Y="0.257735900879" />
                  <Point X="28.417291015625" Y="0.213496963501" />
                  <Point X="28.410853515625" Y="0.204206695557" />
                  <Point X="28.39912890625" Y="0.184925384521" />
                  <Point X="28.393841796875" Y="0.174934356689" />
                  <Point X="28.384068359375" Y="0.153469787598" />
                  <Point X="28.380005859375" Y="0.14292767334" />
                  <Point X="28.37316015625" Y="0.121429962158" />
                  <Point X="28.370376953125" Y="0.110474372864" />
                  <Point X="28.363447265625" Y="0.074292266846" />
                  <Point X="28.351875" Y="0.013863965034" />
                  <Point X="28.350603515625" Y="0.004966505051" />
                  <Point X="28.3485859375" Y="-0.026261997223" />
                  <Point X="28.35028125" Y="-0.062940353394" />
                  <Point X="28.351875" Y="-0.076423873901" />
                  <Point X="28.3588046875" Y="-0.112606124878" />
                  <Point X="28.370376953125" Y="-0.173034423828" />
                  <Point X="28.37316015625" Y="-0.183990310669" />
                  <Point X="28.380005859375" Y="-0.205487869263" />
                  <Point X="28.384068359375" Y="-0.216029693604" />
                  <Point X="28.393841796875" Y="-0.237494262695" />
                  <Point X="28.39912890625" Y="-0.247485290527" />
                  <Point X="28.410853515625" Y="-0.26676675415" />
                  <Point X="28.417291015625" Y="-0.276056854248" />
                  <Point X="28.438078125" Y="-0.302545471191" />
                  <Point X="28.472796875" Y="-0.346784576416" />
                  <Point X="28.478716796875" Y="-0.353630462646" />
                  <Point X="28.50114453125" Y="-0.375809295654" />
                  <Point X="28.5301796875" Y="-0.398726379395" />
                  <Point X="28.54149609375" Y="-0.406404388428" />
                  <Point X="28.576142578125" Y="-0.42643057251" />
                  <Point X="28.634005859375" Y="-0.459876831055" />
                  <Point X="28.639494140625" Y="-0.462813476562" />
                  <Point X="28.65915625" Y="-0.472016052246" />
                  <Point X="28.68302734375" Y="-0.481027648926" />
                  <Point X="28.6919921875" Y="-0.483912719727" />
                  <Point X="29.50516015625" Y="-0.701800598145" />
                  <Point X="29.78487890625" Y="-0.776750793457" />
                  <Point X="29.7616171875" Y="-0.931050476074" />
                  <Point X="29.731990234375" Y="-1.060874389648" />
                  <Point X="29.7278046875" Y="-1.079219726563" />
                  <Point X="29.001443359375" Y="-0.983592346191" />
                  <Point X="28.436783203125" Y="-0.909253540039" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042419434" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.286484375" Y="-0.927456176758" />
                  <Point X="28.17291796875" Y="-0.952140197754" />
                  <Point X="28.157876953125" Y="-0.956742004395" />
                  <Point X="28.1287578125" Y="-0.968365661621" />
                  <Point X="28.114681640625" Y="-0.975386901855" />
                  <Point X="28.086853515625" Y="-0.99227923584" />
                  <Point X="28.074125" Y="-1.001530273438" />
                  <Point X="28.050375" Y="-1.022001586914" />
                  <Point X="28.0393515625" Y="-1.033222045898" />
                  <Point X="27.99825" Y="-1.082653686523" />
                  <Point X="27.929607421875" Y="-1.165210083008" />
                  <Point X="27.921326171875" Y="-1.176849121094" />
                  <Point X="27.906603515625" Y="-1.201236206055" />
                  <Point X="27.900162109375" Y="-1.21398425293" />
                  <Point X="27.8888203125" Y="-1.241369628906" />
                  <Point X="27.88436328125" Y="-1.254934692383" />
                  <Point X="27.877533203125" Y="-1.282580444336" />
                  <Point X="27.87516015625" Y="-1.296660888672" />
                  <Point X="27.86926953125" Y="-1.360677001953" />
                  <Point X="27.859431640625" Y="-1.467591064453" />
                  <Point X="27.859291015625" Y="-1.483315307617" />
                  <Point X="27.861607421875" Y="-1.514580932617" />
                  <Point X="27.864064453125" Y="-1.530122314453" />
                  <Point X="27.871794921875" Y="-1.561743286133" />
                  <Point X="27.87678515625" Y="-1.576667480469" />
                  <Point X="27.889158203125" Y="-1.605480957031" />
                  <Point X="27.896541015625" Y="-1.619370483398" />
                  <Point X="27.934171875" Y="-1.677903808594" />
                  <Point X="27.997021484375" Y="-1.775661132812" />
                  <Point X="28.001744140625" Y="-1.782353271484" />
                  <Point X="28.01980078125" Y="-1.804457275391" />
                  <Point X="28.043494140625" Y="-1.828123291016" />
                  <Point X="28.052798828125" Y="-1.836277587891" />
                  <Point X="28.807423828125" Y="-2.415322509766" />
                  <Point X="29.087171875" Y="-2.629981689453" />
                  <Point X="29.045482421875" Y="-2.697440429688" />
                  <Point X="29.00127734375" Y="-2.760252685547" />
                  <Point X="28.351361328125" Y="-2.385023681641" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.690177734375" Y="-2.051721923828" />
                  <Point X="27.555017578125" Y="-2.027311889648" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.035136352539" />
                  <Point X="27.415068359375" Y="-2.044960205078" />
                  <Point X="27.40058984375" Y="-2.051108642578" />
                  <Point X="27.333357421875" Y="-2.086492431641" />
                  <Point X="27.221072265625" Y="-2.145587646484" />
                  <Point X="27.20896875" Y="-2.153170654297" />
                  <Point X="27.186037109375" Y="-2.170064697266" />
                  <Point X="27.175208984375" Y="-2.179375732422" />
                  <Point X="27.15425" Y="-2.200335449219" />
                  <Point X="27.14494140625" Y="-2.211160888672" />
                  <Point X="27.128048828125" Y="-2.23408984375" />
                  <Point X="27.12046484375" Y="-2.246193359375" />
                  <Point X="27.085080078125" Y="-2.31342578125" />
                  <Point X="27.025984375" Y="-2.425711425781" />
                  <Point X="27.0198359375" Y="-2.440193359375" />
                  <Point X="27.01001171875" Y="-2.469972167969" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517441894531" />
                  <Point X="27.000279296875" Y="-2.533133056641" />
                  <Point X="27.00068359375" Y="-2.564484863281" />
                  <Point X="27.0021875" Y="-2.580145507812" />
                  <Point X="27.0168046875" Y="-2.661074707031" />
                  <Point X="27.04121484375" Y="-2.796235839844" />
                  <Point X="27.04301953125" Y="-2.804232910156" />
                  <Point X="27.05123828125" Y="-2.831539794922" />
                  <Point X="27.064072265625" Y="-2.862478759766" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.55447265625" Y="-3.713489257813" />
                  <Point X="27.735896484375" Y="-4.027723388672" />
                  <Point X="27.723755859375" Y="-4.036083007813" />
                  <Point X="27.2176796875" Y="-3.376553222656" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.693482421875" Y="-2.769331298828" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.321099609375" Y="-2.654549316406" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.976455078125" Y="-2.778459716797" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883087646484" />
                  <Point X="25.83218359375" Y="-2.906837158203" />
                  <Point X="25.822935546875" Y="-2.919562744141" />
                  <Point X="25.80604296875" Y="-2.947389404297" />
                  <Point X="25.79901953125" Y="-2.961466552734" />
                  <Point X="25.78739453125" Y="-2.990587402344" />
                  <Point X="25.78279296875" Y="-3.005631103516" />
                  <Point X="25.762638671875" Y="-3.098356689453" />
                  <Point X="25.728978515625" Y="-3.253218994141" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677246094" />
                  <Point X="25.7247421875" Y="-3.323170166016" />
                  <Point X="25.7255546875" Y="-3.335520507812" />
                  <Point X="25.83308984375" Y="-4.152321289062" />
                  <Point X="25.805091796875" Y="-4.047833984375" />
                  <Point X="25.655068359375" Y="-3.487937011719" />
                  <Point X="25.652607421875" Y="-3.480121337891" />
                  <Point X="25.642146484375" Y="-3.453579345703" />
                  <Point X="25.6267890625" Y="-3.423815917969" />
                  <Point X="25.62041015625" Y="-3.413210449219" />
                  <Point X="25.559091796875" Y="-3.324861816406" />
                  <Point X="25.456681640625" Y="-3.177310058594" />
                  <Point X="25.446671875" Y="-3.165173339844" />
                  <Point X="25.424787109375" Y="-3.142716796875" />
                  <Point X="25.412912109375" Y="-3.132396972656" />
                  <Point X="25.38665625" Y="-3.113153320312" />
                  <Point X="25.373240234375" Y="-3.104936279297" />
                  <Point X="25.345240234375" Y="-3.090829101562" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.23576953125" Y="-3.055489501953" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.99883984375" />
                  <Point X="24.948935546875" Y="-3.003109130859" />
                  <Point X="24.935015625" Y="-3.006305175781" />
                  <Point X="24.84012890625" Y="-3.035754394531" />
                  <Point X="24.68165625" Y="-3.084938476562" />
                  <Point X="24.667068359375" Y="-3.090830078125" />
                  <Point X="24.639068359375" Y="-3.104938232422" />
                  <Point X="24.62565625" Y="-3.113154785156" />
                  <Point X="24.599400390625" Y="-3.132398925781" />
                  <Point X="24.587525390625" Y="-3.142716552734" />
                  <Point X="24.565640625" Y="-3.165172119141" />
                  <Point X="24.555630859375" Y="-3.177310058594" />
                  <Point X="24.4943125" Y="-3.265658447266" />
                  <Point X="24.391904296875" Y="-3.413210449219" />
                  <Point X="24.38753125" Y="-3.420130615234" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476214111328" />
                  <Point X="24.35724609375" Y="-3.487936767578" />
                  <Point X="24.106689453125" Y="-4.423028808594" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.706234362282" Y="0.693117966038" />
                  <Point X="29.612487662754" Y="1.099179533339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.614415478989" Y="0.668515152981" />
                  <Point X="29.517864751773" Y="1.086722298563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.522596595696" Y="0.643912339923" />
                  <Point X="29.423241840791" Y="1.074265063787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.103767623748" Y="2.458059927366" />
                  <Point X="29.073852161638" Y="2.587638029764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.772318532139" Y="-0.860066293905" />
                  <Point X="29.750987057551" Y="-0.767669526365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.430777712402" Y="0.619309526866" />
                  <Point X="29.328618952832" Y="1.061807729293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.020941534333" Y="2.394505044585" />
                  <Point X="28.93274454338" Y="2.776528183082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.725340294723" Y="-1.078895282768" />
                  <Point X="29.647059104576" Y="-0.739822196238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.338958829109" Y="0.594706713809" />
                  <Point X="29.233996078954" Y="1.049350333805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.938115444917" Y="2.330950161804" />
                  <Point X="28.846712933082" Y="2.726857936664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.624785088838" Y="-1.065656925288" />
                  <Point X="29.543131151602" Y="-0.711974866111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.247139945816" Y="0.570103900752" />
                  <Point X="29.139373205076" Y="1.036892938317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.855289355501" Y="2.267395279023" />
                  <Point X="28.760681322785" Y="2.677187690247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.524229882952" Y="-1.052418567808" />
                  <Point X="29.439203182435" Y="-0.684127465848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.155321058189" Y="0.545501106467" />
                  <Point X="29.044750331198" Y="1.024435542829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.772463266086" Y="2.203840396243" />
                  <Point X="28.674649712488" Y="2.627517443829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.423674677067" Y="-1.039180210328" />
                  <Point X="29.335275203947" Y="-0.656280025206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.063502158008" Y="0.520898366555" />
                  <Point X="28.95012745732" Y="1.011978147341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.68963717667" Y="2.140285513462" />
                  <Point X="28.588618102191" Y="2.577847197411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.323119471182" Y="-1.025941852847" />
                  <Point X="29.231347225458" Y="-0.628432584565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.971683257828" Y="0.496295626643" />
                  <Point X="28.855504583442" Y="0.999520751854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.606811087255" Y="2.076730630681" />
                  <Point X="28.502586521451" Y="2.528176822968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.222564265297" Y="-1.012703495367" />
                  <Point X="29.127419246969" Y="-0.600585143924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.879864357648" Y="0.471692886732" />
                  <Point X="28.760881709564" Y="0.987063356366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.523984964264" Y="2.013175893328" />
                  <Point X="28.416554949158" Y="2.478506411937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.122009059411" Y="-0.999465137887" />
                  <Point X="29.02349126848" Y="-0.572737703282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.788045457468" Y="0.44709014682" />
                  <Point X="28.666258835686" Y="0.974605960878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.44115883718" Y="1.949621173709" />
                  <Point X="28.330523376864" Y="2.428836000906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.021453853526" Y="-0.986226780407" />
                  <Point X="28.919563289992" Y="-0.544890262641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.696226557288" Y="0.422487406908" />
                  <Point X="28.571635961808" Y="0.96214856539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.358332710095" Y="1.886066454091" />
                  <Point X="28.244491804571" Y="2.379165589875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.920898654074" Y="-0.97298845079" />
                  <Point X="28.815635311503" Y="-0.517042822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.60800810454" Y="0.382289415804" />
                  <Point X="28.476579866858" Y="0.951567656523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.275506583011" Y="1.822511734472" />
                  <Point X="28.158460232278" Y="2.329495178844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.801820570463" Y="3.874271269809" />
                  <Point X="27.791049467474" Y="3.920926042546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.820343456219" Y="-0.959750128096" />
                  <Point X="28.711707333014" Y="-0.489195381358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.522510659182" Y="0.330305446843" />
                  <Point X="28.376935342061" Y="0.960861420844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.192680455926" Y="1.758957014854" />
                  <Point X="28.072428659985" Y="2.279824767813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.732172336446" Y="3.753636824296" />
                  <Point X="27.67439917931" Y="4.00387986061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.719788258365" Y="-0.946511805402" />
                  <Point X="28.603348605736" Y="-0.442156259228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.444094296282" Y="0.247649940048" />
                  <Point X="28.273463624107" Y="0.986732579984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.113474903473" Y="1.679719863568" />
                  <Point X="27.986397087692" Y="2.230154356781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.662524102428" Y="3.633002378783" />
                  <Point X="27.560206106476" Y="4.076190309756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.031206325709" Y="-2.717725741763" />
                  <Point X="28.994538877581" Y="-2.558901574829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.619233060511" Y="-0.933273482708" />
                  <Point X="28.487392681849" Y="-0.362210063279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.374653157486" Y="0.126118466579" />
                  <Point X="28.160878448692" Y="1.052078460253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.041257711828" Y="1.57021279604" />
                  <Point X="27.900365515399" Y="2.18048394575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.592875868411" Y="3.51236793327" />
                  <Point X="27.44755508417" Y="4.141821404241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.934643860656" Y="-2.721781844874" />
                  <Point X="28.876049300357" Y="-2.467980920573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.518677862656" Y="-0.920035160014" />
                  <Point X="27.814333943106" Y="2.130813534719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.523227634394" Y="3.391733487757" />
                  <Point X="27.334904192439" Y="4.207451933144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.822150539001" Y="-2.656833826951" />
                  <Point X="28.757559756105" Y="-2.377060409137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.41848361054" Y="-0.908360265076" />
                  <Point X="27.728302370812" Y="2.081143123688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.453579400376" Y="3.271099042244" />
                  <Point X="27.223092205956" Y="4.269448764208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.709657217346" Y="-2.591885809029" />
                  <Point X="28.639070257231" Y="-2.286140094254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.323534155906" Y="-0.919403083896" />
                  <Point X="27.642270798519" Y="2.031472712657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.38393116394" Y="3.150464607209" />
                  <Point X="27.1137921249" Y="4.320565337514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.597163895691" Y="-2.526937791107" />
                  <Point X="28.520580758357" Y="-2.195219779371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.230694024978" Y="-0.939582387464" />
                  <Point X="27.55270913869" Y="1.997092790623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.314282888552" Y="3.02983034089" />
                  <Point X="27.004492043843" Y="4.371681910821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.484670574035" Y="-2.461989773184" />
                  <Point X="28.402091259483" Y="-2.104299464488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.138905130743" Y="-0.964315097405" />
                  <Point X="27.45986196434" Y="1.976943995468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.244634613165" Y="2.909196074571" />
                  <Point X="26.895192130922" Y="4.422797755855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.37217725238" Y="-2.397041755262" />
                  <Point X="28.283601760609" Y="-2.013379149605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.054002379004" Y="-1.018874967432" />
                  <Point X="27.361314430975" Y="1.981486167864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.174986337777" Y="2.788561808252" />
                  <Point X="26.787806147999" Y="4.465623459275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.259683925389" Y="-2.33209371423" />
                  <Point X="28.165112261736" Y="-1.922458834723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.977100455831" Y="-1.108090233366" />
                  <Point X="27.257232788415" Y="2.009999200724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.10533806239" Y="2.667927541933" />
                  <Point X="26.681396848946" Y="4.504218680078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.147190597187" Y="-2.26714566795" />
                  <Point X="28.04642785868" Y="-1.830694297077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.90283010333" Y="-1.208704084178" />
                  <Point X="27.141183290504" Y="2.090350710303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.039119589347" Y="2.532437169502" />
                  <Point X="26.574987509114" Y="4.542814077514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.034697268985" Y="-2.20219762167" />
                  <Point X="27.902158182466" Y="-1.628107766013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.861047022789" Y="-1.450035769707" />
                  <Point X="26.468578043808" Y="4.581410018437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.922203940783" Y="-2.13724957539" />
                  <Point X="26.364879515655" Y="4.608263600484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.811085019897" Y="-2.078254741243" />
                  <Point X="26.261625368857" Y="4.633192355416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.708212492436" Y="-2.054978961266" />
                  <Point X="26.158371222058" Y="4.658121110348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.606471526947" Y="-2.036604514667" />
                  <Point X="26.055117075504" Y="4.68304986422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.506412431178" Y="-2.025515046188" />
                  <Point X="25.951862939807" Y="4.707978571068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.413551520956" Y="-2.045604344744" />
                  <Point X="25.84860880411" Y="4.732907277915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.326344494908" Y="-2.090183306193" />
                  <Point X="25.748023705817" Y="4.746275113635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.658440281957" Y="-3.950962286594" />
                  <Point X="27.636359883587" Y="-3.855321573758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.239408732501" Y="-2.135937239559" />
                  <Point X="25.648109063554" Y="4.756738885241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.518982310743" Y="-3.769217539646" />
                  <Point X="27.473895578235" Y="-3.573925445538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.15630328555" Y="-2.198282091916" />
                  <Point X="25.54819442129" Y="4.767202656847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.379524339528" Y="-3.587472792699" />
                  <Point X="27.311430986248" Y="-3.292528075768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.085293702459" Y="-2.313019886761" />
                  <Point X="25.448279779027" Y="4.777666428453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.240066368313" Y="-3.405728045751" />
                  <Point X="27.148966394261" Y="-3.011130705997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.01825880073" Y="-2.444973918032" />
                  <Point X="25.362558261024" Y="4.726653024747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.100608042828" Y="-3.223981764292" />
                  <Point X="25.310184671207" Y="4.531193874641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.961149649599" Y="-3.042235189399" />
                  <Point X="25.257810990845" Y="4.335735116733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.822433872947" Y="-2.863705240293" />
                  <Point X="25.196068811583" Y="4.180855785783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.704831552937" Y="-2.776627719255" />
                  <Point X="25.114507456988" Y="4.111822734639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.590339025091" Y="-2.703020187948" />
                  <Point X="25.023118304866" Y="4.085358551384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.481460207178" Y="-2.653728305784" />
                  <Point X="24.921817674133" Y="4.101825698609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.382839222417" Y="-2.64886798044" />
                  <Point X="24.800704574614" Y="4.204110076387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.287368587751" Y="-2.657653320526" />
                  <Point X="24.571374006839" Y="4.775135807095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.191897971494" Y="-2.666438740354" />
                  <Point X="24.476440174139" Y="4.764025322241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.099834976417" Y="-2.689984189109" />
                  <Point X="24.38150634144" Y="4.752914837387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.015304686465" Y="-2.746157368381" />
                  <Point X="24.28657252563" Y="4.741804279373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.93350748312" Y="-2.814168846352" />
                  <Point X="24.191638719809" Y="4.730693678095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.852074932372" Y="-2.883759808254" />
                  <Point X="24.096704913988" Y="4.719583076817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.782753825062" Y="-3.005811195207" />
                  <Point X="24.004145735945" Y="4.69818683261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.735474107944" Y="-3.223334332011" />
                  <Point X="23.912574026483" Y="4.672513392067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.807401160884" Y="-3.957198717376" />
                  <Point X="23.82100231702" Y="4.646839951525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.566320266312" Y="-3.335276729633" />
                  <Point X="23.729430607558" Y="4.621166510982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.424259571006" Y="-3.142258346076" />
                  <Point X="23.637858898096" Y="4.595493070439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.312205394679" Y="-3.07921247555" />
                  <Point X="23.546287153573" Y="4.569819781761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.207181194743" Y="-3.046616778156" />
                  <Point X="23.512684703333" Y="4.293053893448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.102157047415" Y="-3.014021308635" />
                  <Point X="23.44752810002" Y="4.152964057903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.000905462356" Y="-2.997766601562" />
                  <Point X="23.36437741088" Y="4.090815170993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.907359493304" Y="-3.014888584322" />
                  <Point X="23.275475293134" Y="4.053578458339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.816379566207" Y="-3.043125315903" />
                  <Point X="23.183450573664" Y="4.029867219717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.725399658604" Y="-3.071362131923" />
                  <Point X="23.082518923366" Y="4.044736137088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.636075689926" Y="-3.106771607445" />
                  <Point X="22.972450734211" Y="4.099179752091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.555053827267" Y="-3.178141454891" />
                  <Point X="22.848144826805" Y="4.215293700207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.481891536078" Y="-3.283554846547" />
                  <Point X="22.746260896936" Y="4.234287393565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.408729323566" Y="-3.388968578984" />
                  <Point X="22.659846034326" Y="4.186277195296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.344801484515" Y="-3.534380777286" />
                  <Point X="22.573431174358" Y="4.138266985582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.292428161464" Y="-3.729841082883" />
                  <Point X="22.487016344247" Y="4.090256646542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.240054838414" Y="-3.92530138848" />
                  <Point X="22.400601514136" Y="4.042246307503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.187681515364" Y="-4.120761694077" />
                  <Point X="22.315737277092" Y="3.987519612001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.135308192313" Y="-4.316221999674" />
                  <Point X="22.232900767142" Y="3.924009865513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.082934568994" Y="-4.511681004663" />
                  <Point X="22.150064257193" Y="3.860500119024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.030560583921" Y="-4.707138442724" />
                  <Point X="22.25035855666" Y="3.003763689707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.188938960131" Y="3.269801190283" />
                  <Point X="22.067227747243" Y="3.796990372536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.943659234461" Y="-4.753041434944" />
                  <Point X="23.826604875475" Y="-4.246023303014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.809636402131" Y="-4.172524770102" />
                  <Point X="22.18632669639" Y="2.858802056809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.026474782196" Y="3.551196766599" />
                  <Point X="21.984391288684" Y="3.733480403448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.672682351786" Y="-4.001625695991" />
                  <Point X="22.106819536431" Y="2.780871311158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.550431702651" Y="-3.894414049495" />
                  <Point X="22.020275199042" Y="2.733421929767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.434556706001" Y="-3.814818387916" />
                  <Point X="21.924914688786" Y="2.724159588457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.332284825772" Y="-3.794144296934" />
                  <Point X="21.82501885872" Y="2.734541875481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.233287902127" Y="-3.787655601381" />
                  <Point X="21.717281624047" Y="2.778889017387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.134291029197" Y="-3.781167125502" />
                  <Point X="21.604788310085" Y="2.843837001982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.035684025571" Y="-3.776367359106" />
                  <Point X="21.492294990303" Y="2.908785011792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.942954147598" Y="-3.797024220686" />
                  <Point X="22.681039052209" Y="-2.662545303897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.631430362923" Y="-2.447666463101" />
                  <Point X="21.379801602779" Y="2.973733315024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.857112638417" Y="-3.847517885501" />
                  <Point X="22.615110150919" Y="-2.799289949389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.514395286007" Y="-2.363045941841" />
                  <Point X="21.269415359848" Y="3.029554572285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.772644099652" Y="-3.903958538552" />
                  <Point X="22.545461885045" Y="-2.919924256913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.41378827067" Y="-2.34958317297" />
                  <Point X="21.548225459013" Y="1.399581263399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.464477301042" Y="1.762334389167" />
                  <Point X="21.194228201991" Y="2.932911841755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.688175523269" Y="-3.960399028659" />
                  <Point X="22.47581361917" Y="-3.040558564437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.32210414375" Y="-2.374769680005" />
                  <Point X="21.476786823126" Y="1.286701900391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.345739268728" Y="1.854331220649" />
                  <Point X="21.119041028463" Y="2.836269179099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.605460207261" Y="-4.024433723784" />
                  <Point X="22.406165353295" Y="-3.161192871962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.235885668519" Y="-2.423630525473" />
                  <Point X="22.01982115133" Y="-1.487752281979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.942273449889" Y="-1.151856284083" />
                  <Point X="21.697506199369" Y="-0.091652843638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.641373579623" Y="0.15148424455" />
                  <Point X="21.39108162802" Y="1.235617794449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.227249768997" Y="1.945251539246" />
                  <Point X="21.043853821439" Y="2.739626661536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.529878539632" Y="-4.119367644754" />
                  <Point X="22.336517095506" Y="-3.281827214506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.149854069569" Y="-2.473300821041" />
                  <Point X="21.943662158219" Y="-1.580185531555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.834091956431" Y="-1.105584845968" />
                  <Point X="21.6308934338" Y="-0.225435347501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.52411264149" Y="0.237083078224" />
                  <Point X="21.299734090863" Y="1.208973356973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.108760269265" Y="2.036171857842" />
                  <Point X="20.970722795821" Y="2.634077843815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.433248032766" Y="-4.123129026389" />
                  <Point X="22.266868842669" Y="-3.402461578504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.063822470619" Y="-2.522971116609" />
                  <Point X="21.860836066616" Y="-1.643740404861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.735948607676" Y="-1.10279338946" />
                  <Point X="21.547734733821" Y="-0.287549535653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.419238017645" Y="0.26903089039" />
                  <Point X="21.202419504943" Y="1.208175047255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.990270769534" Y="2.127092176439" />
                  <Point X="20.900857721699" Y="2.514382635982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.31948732403" Y="-4.052691351905" />
                  <Point X="22.197220589833" Y="-3.523095942502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.977790871669" Y="-2.572641412176" />
                  <Point X="21.778009975013" Y="-1.707295278167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.641325715161" Y="-1.115250704222" />
                  <Point X="21.458005383843" Y="-0.321203111855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.315310041541" Y="0.296878320701" />
                  <Point X="21.101864303862" Y="1.221413383928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.87178126615" Y="2.218012510859" />
                  <Point X="20.830992647578" Y="2.394687428149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.203750230107" Y="-3.973693012665" />
                  <Point X="22.127572336996" Y="-3.6437303065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.891759272719" Y="-2.622311707744" />
                  <Point X="21.69518388341" Y="-1.770850151472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.546702822646" Y="-1.127708018983" />
                  <Point X="21.366186502103" Y="-0.345805931639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.211382065437" Y="0.324725751012" />
                  <Point X="21.00130910278" Y="1.234651720601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.085172930665" Y="-3.882392391739" />
                  <Point X="22.05792408416" Y="-3.764364670498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.805727673769" Y="-2.671982003312" />
                  <Point X="21.612357791807" Y="-1.834405024778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.452079930131" Y="-1.140165333745" />
                  <Point X="21.274367620363" Y="-0.370408751424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.107454089333" Y="0.352573181323" />
                  <Point X="20.900753901698" Y="1.247890057275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.719696074819" Y="-2.721652298879" />
                  <Point X="21.529531700204" Y="-1.897959898083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.357457037616" Y="-1.152622648507" />
                  <Point X="21.182548738623" Y="-0.395011571208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.00352611323" Y="0.380420611634" />
                  <Point X="20.800198702269" Y="1.261128386793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.633664475869" Y="-2.771322594447" />
                  <Point X="21.446705598786" Y="-1.961514728879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.262834145101" Y="-1.165079963268" />
                  <Point X="21.090729856883" Y="-0.419614390992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.899598137126" Y="0.408268041945" />
                  <Point X="20.699643508285" Y="1.274366692719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.547632876919" Y="-2.820992890015" />
                  <Point X="21.363879460355" Y="-2.02506939935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.168211252585" Y="-1.17753727803" />
                  <Point X="20.998910975143" Y="-0.444217210776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.795670161022" Y="0.436115472257" />
                  <Point X="20.599088314302" Y="1.287604998646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.461601290785" Y="-2.870663241091" />
                  <Point X="21.281053321924" Y="-2.088624069821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.07358836007" Y="-1.189994592792" />
                  <Point X="20.907092093115" Y="-0.468820029317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.691742184918" Y="0.463962902568" />
                  <Point X="20.498533120319" Y="1.300843304572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.375569716217" Y="-2.920333642271" />
                  <Point X="21.198227183493" Y="-2.152178740292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.978965467555" Y="-1.202451907553" />
                  <Point X="20.815273210463" Y="-0.493422845152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.587814208814" Y="0.491810332879" />
                  <Point X="20.397977926336" Y="1.314081610499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.28953814165" Y="-2.97000404345" />
                  <Point X="21.115401045062" Y="-2.215733410763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.88434257504" Y="-1.214909222315" />
                  <Point X="20.723454327811" Y="-0.518025660987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.483886245868" Y="0.5196577062" />
                  <Point X="20.330294263768" Y="1.184937671149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.201372455473" Y="-3.010430591682" />
                  <Point X="21.032574906631" Y="-2.279288081234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.789719682525" Y="-1.227366537077" />
                  <Point X="20.631635445159" Y="-0.542628476823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.379958286909" Y="0.547505062247" />
                  <Point X="20.277648357028" Y="0.990658055229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.061426027331" Y="-2.826570105337" />
                  <Point X="20.9497487682" Y="-2.342842751705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.695096798301" Y="-1.239823887751" />
                  <Point X="20.539816562508" Y="-0.567231292658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.276030327951" Y="0.575352418294" />
                  <Point X="20.237080906982" Y="0.744060895538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.911290578439" Y="-2.598576121434" />
                  <Point X="20.866922629768" Y="-2.406397422176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.600473924269" Y="-1.25228128257" />
                  <Point X="20.447997679856" Y="-0.591834108493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.505851050236" Y="-1.264738677389" />
                  <Point X="20.356178797204" Y="-0.616436924328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.411228176204" Y="-1.277196072208" />
                  <Point X="20.264359914552" Y="-0.641039740164" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.621564453125" Y="-4.097009277344" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521543945312" />
                  <Point X="25.403001953125" Y="-3.4331953125" />
                  <Point X="25.300591796875" Y="-3.285643554688" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.17944921875" Y="-3.236950439453" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.89644921875" Y="-3.217215820312" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285644042969" />
                  <Point X="24.65040234375" Y="-3.373992431641" />
                  <Point X="24.547994140625" Y="-3.521544433594" />
                  <Point X="24.5407734375" Y="-3.537112060547" />
                  <Point X="24.290216796875" Y="-4.472204589844" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.08576171875" Y="-4.974169921875" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.790470703125" Y="-4.909947265625" />
                  <Point X="23.648412109375" Y="-4.873396484375" />
                  <Point X="23.672791015625" Y="-4.688213378906" />
                  <Point X="23.6908515625" Y="-4.5510390625" />
                  <Point X="23.690318359375" Y="-4.5347578125" />
                  <Point X="23.6676484375" Y="-4.420795898438" />
                  <Point X="23.6297890625" Y="-4.230466308594" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.526357421875" Y="-4.126016113281" />
                  <Point X="23.38045703125" Y="-3.998064208984" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.234814453125" Y="-3.978163330078" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.913509765625" Y="-4.038345458984" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.59959765625" Y="-4.3406171875" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.416814453125" Y="-4.336427734375" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.99283984375" Y="-4.051093994141" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.2151640625" Y="-3.112020019531" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597591796875" />
                  <Point X="22.48601953125" Y="-2.568762207031" />
                  <Point X="22.46867578125" Y="-2.551418457031" />
                  <Point X="22.439845703125" Y="-2.537198974609" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.601818359375" Y="-3.009102050781" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.053703125" Y="-3.130129150391" />
                  <Point X="20.838302734375" Y="-2.847136962891" />
                  <Point X="20.72980859375" Y="-2.665208984375" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.348400390625" Y="-1.797457763672" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396018676758" />
                  <Point X="21.861884765625" Y="-1.366264526367" />
                  <Point X="21.859673828125" Y="-1.334594848633" />
                  <Point X="21.838841796875" Y="-1.310639282227" />
                  <Point X="21.812359375" Y="-1.295052734375" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.76234375" Y="-1.422610229492" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.15685546875" Y="-1.341014770508" />
                  <Point X="20.072607421875" Y="-1.011187561035" />
                  <Point X="20.043904296875" Y="-0.810499389648" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="20.88684375" Y="-0.277543029785" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.468498046875" Y="-0.114211395264" />
                  <Point X="21.485857421875" Y="-0.10216317749" />
                  <Point X="21.497677734375" Y="-0.090644317627" />
                  <Point X="21.508568359375" Y="-0.06474181366" />
                  <Point X="21.516599609375" Y="-0.031280038834" />
                  <Point X="21.510888671875" Y="-0.005298885345" />
                  <Point X="21.497677734375" Y="0.02808423996" />
                  <Point X="21.475462890625" Y="0.046817058563" />
                  <Point X="21.458103515625" Y="0.058865276337" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.51446484375" Y="0.314761627197" />
                  <Point X="20.001814453125" Y="0.452126098633" />
                  <Point X="20.02805859375" Y="0.629491882324" />
                  <Point X="20.08235546875" Y="0.996414916992" />
                  <Point X="20.14013671875" Y="1.209644287109" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.848400390625" Y="1.446422119141" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.29130078125" Y="1.403119995117" />
                  <Point X="21.32972265625" Y="1.415234130859" />
                  <Point X="21.348466796875" Y="1.426056274414" />
                  <Point X="21.360880859375" Y="1.443786499023" />
                  <Point X="21.370111328125" Y="1.466071777344" />
                  <Point X="21.38552734375" Y="1.503290771484" />
                  <Point X="21.389287109375" Y="1.524606567383" />
                  <Point X="21.38368359375" Y="1.545513427734" />
                  <Point X="21.372546875" Y="1.566909423828" />
                  <Point X="21.353943359375" Y="1.602643188477" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.8076875" Y="2.027704101563" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.62901171875" Y="2.425556884766" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="20.99304296875" Y="2.983738769531" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.596197265625" Y="3.068189941406" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.893525390625" Y="2.917631591797" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775390625" />
                  <Point X="21.986748046875" Y="2.927404296875" />
                  <Point X="22.009490234375" Y="2.950146240234" />
                  <Point X="22.04747265625" Y="2.988127685547" />
                  <Point X="22.0591015625" Y="3.006382080078" />
                  <Point X="22.061927734375" Y="3.027841308594" />
                  <Point X="22.059125" Y="3.059881103516" />
                  <Point X="22.054443359375" Y="3.113390869141" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.81203515625" Y="3.542620117188" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.879673828125" Y="3.892611328125" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.48818359375" Y="4.308258300781" />
                  <Point X="22.858451171875" Y="4.513971679688" />
                  <Point X="22.957279296875" Y="4.385176269531" />
                  <Point X="23.032173828125" Y="4.287572753906" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.0844140625" Y="4.255097167969" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.218491699219" />
                  <Point X="23.18619140625" Y="4.22225" />
                  <Point X="23.22333203125" Y="4.237635253906" />
                  <Point X="23.285365234375" Y="4.263329589844" />
                  <Point X="23.303095703125" Y="4.275744140625" />
                  <Point X="23.31391796875" Y="4.294487792969" />
                  <Point X="23.326005859375" Y="4.332829589844" />
                  <Point X="23.346197265625" Y="4.396864257812" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.321263671875" Y="4.62213671875" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.556220703125" Y="4.769930664062" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.324142578125" Y="4.937498046875" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.885486328125" Y="4.58100390625" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.175326171875" Y="4.762001953125" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.44486328125" Y="4.969063476562" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.101982421875" Y="4.867193847656" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.665873046875" Y="4.711961425781" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="27.08320703125" Y="4.544620117188" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.48570703125" Y="4.339487304688" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="27.871169921875" Y="4.097095214844" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.552548828125" Y="3.062518798828" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491515380859" />
                  <Point X="27.2168125" Y="2.461446777344" />
                  <Point X="27.2033828125" Y="2.411229003906" />
                  <Point X="27.202044921875" Y="2.392327880859" />
                  <Point X="27.205181640625" Y="2.366327148438" />
                  <Point X="27.210416015625" Y="2.322903076172" />
                  <Point X="27.218681640625" Y="2.3008125" />
                  <Point X="27.234771484375" Y="2.277102539062" />
                  <Point X="27.261640625" Y="2.23750390625" />
                  <Point X="27.27494140625" Y="2.224203369141" />
                  <Point X="27.298650390625" Y="2.208114990234" />
                  <Point X="27.33825" Y="2.181245849609" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.386337890625" Y="2.169844970703" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.4486640625" Y="2.165946289062" />
                  <Point X="27.478732421875" Y="2.173987060547" />
                  <Point X="27.528951171875" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.47449609375" Y="2.731352050781" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.056189453125" Y="2.945347412109" />
                  <Point X="29.20259765625" Y="2.741874267578" />
                  <Point X="29.279884765625" Y="2.614153808594" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.71347265625" Y="1.9190859375" />
                  <Point X="28.2886171875" Y="1.593082641602" />
                  <Point X="28.27937109375" Y="1.583832519531" />
                  <Point X="28.25773046875" Y="1.555601074219" />
                  <Point X="28.22158984375" Y="1.508451171875" />
                  <Point X="28.21312109375" Y="1.4915" />
                  <Point X="28.20505859375" Y="1.462675537109" />
                  <Point X="28.191595703125" Y="1.414535400391" />
                  <Point X="28.190779296875" Y="1.390966064453" />
                  <Point X="28.1973984375" Y="1.358895141602" />
                  <Point X="28.20844921875" Y="1.305333374023" />
                  <Point X="28.215646484375" Y="1.287955444336" />
                  <Point X="28.23364453125" Y="1.260598754883" />
                  <Point X="28.263703125" Y="1.21491003418" />
                  <Point X="28.280947265625" Y="1.198819824219" />
                  <Point X="28.307029296875" Y="1.184137817383" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.40383203125" Y="1.148958862305" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.362529296875" Y="1.257911621094" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.87630859375" Y="1.209674194336" />
                  <Point X="29.939193359375" Y="0.951366882324" />
                  <Point X="29.963546875" Y="0.794943847656" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="29.2280625" Y="0.368289672852" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819351196" />
                  <Point X="28.69444140625" Y="0.21279309082" />
                  <Point X="28.636578125" Y="0.179346832275" />
                  <Point X="28.622265625" Y="0.166927001953" />
                  <Point X="28.601478515625" Y="0.14043838501" />
                  <Point X="28.566759765625" Y="0.096199325562" />
                  <Point X="28.556986328125" Y="0.074734802246" />
                  <Point X="28.550056640625" Y="0.038552581787" />
                  <Point X="28.538484375" Y="-0.02187575531" />
                  <Point X="28.538484375" Y="-0.040684322357" />
                  <Point X="28.5454140625" Y="-0.076866546631" />
                  <Point X="28.556986328125" Y="-0.137294876099" />
                  <Point X="28.566759765625" Y="-0.158759399414" />
                  <Point X="28.587546875" Y="-0.185248016357" />
                  <Point X="28.622265625" Y="-0.229487075806" />
                  <Point X="28.636578125" Y="-0.241906906128" />
                  <Point X="28.671224609375" Y="-0.261933166504" />
                  <Point X="28.729087890625" Y="-0.295379425049" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.5543359375" Y="-0.518274780273" />
                  <Point X="29.998068359375" Y="-0.637172424316" />
                  <Point X="29.983541015625" Y="-0.733528869629" />
                  <Point X="29.948431640625" Y="-0.966412780762" />
                  <Point X="29.917228515625" Y="-1.103146240234" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="28.976642578125" Y="-1.171967041016" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.32683984375" Y="-1.11312097168" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.1854453125" Y="-1.154697387695" />
                  <Point X="28.14434375" Y="-1.20412902832" />
                  <Point X="28.075701171875" Y="-1.286685424805" />
                  <Point X="28.064359375" Y="-1.314070800781" />
                  <Point X="28.05846875" Y="-1.378086914062" />
                  <Point X="28.048630859375" Y="-1.485001220703" />
                  <Point X="28.056361328125" Y="-1.516622192383" />
                  <Point X="28.0939921875" Y="-1.575155395508" />
                  <Point X="28.156841796875" Y="-1.672912719727" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.923087890625" Y="-2.264585205078" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.3031015625" Y="-2.641994140625" />
                  <Point X="29.204134765625" Y="-2.802138183594" />
                  <Point X="29.13959765625" Y="-2.893837158203" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.256361328125" Y="-2.549568603516" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.656412109375" Y="-2.238697021484" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.421845703125" Y="-2.254628662109" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.33468359375" />
                  <Point X="27.253216796875" Y="-2.401916015625" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.1891640625" Y="-2.546374511719" />
                  <Point X="27.20378125" Y="-2.627303710938" />
                  <Point X="27.22819140625" Y="-2.76246484375" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.719015625" Y="-3.618489257812" />
                  <Point X="27.98667578125" Y="-4.082088867188" />
                  <Point X="27.952736328125" Y="-4.106331054688" />
                  <Point X="27.835314453125" Y="-4.190202148438" />
                  <Point X="27.76314453125" Y="-4.236916015625" />
                  <Point X="27.679775390625" Y="-4.29087890625" />
                  <Point X="27.06694140625" Y="-3.492217773438" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.590732421875" Y="-2.929151611328" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.338509765625" Y="-2.84375" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.097927734375" Y="-2.924555419922" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045986083984" />
                  <Point X="25.948302734375" Y="-3.138711669922" />
                  <Point X="25.914642578125" Y="-3.293573974609" />
                  <Point X="25.9139296875" Y="-3.310720214844" />
                  <Point X="26.051353515625" Y="-4.354554199219" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.105451171875" Y="-4.938893066406" />
                  <Point X="25.994349609375" Y="-4.96324609375" />
                  <Point X="25.927689453125" Y="-4.975356445312" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#154" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.072373132609" Y="4.625224695354" Z="0.95" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.95" />
                  <Point X="-0.687827167114" Y="5.018439361996" Z="0.95" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.95" />
                  <Point X="-1.463434592406" Y="4.849354145221" Z="0.95" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.95" />
                  <Point X="-1.734464829951" Y="4.646890816506" Z="0.95" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.95" />
                  <Point X="-1.727836057336" Y="4.379145697495" Z="0.95" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.95" />
                  <Point X="-1.80195686037" Y="4.315109506691" Z="0.95" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.95" />
                  <Point X="-1.898655254551" Y="4.330727797472" Z="0.95" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.95" />
                  <Point X="-2.009208819016" Y="4.446894628308" Z="0.95" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.95" />
                  <Point X="-2.542256355709" Y="4.383246003837" Z="0.95" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.95" />
                  <Point X="-3.156904143837" Y="3.963571320746" Z="0.95" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.95" />
                  <Point X="-3.237422677147" Y="3.548900191118" Z="0.95" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.95" />
                  <Point X="-2.996843140853" Y="3.086803136521" Z="0.95" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.95" />
                  <Point X="-3.032021631993" Y="3.016781935893" Z="0.95" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.95" />
                  <Point X="-3.108273329085" Y="2.998721558319" Z="0.95" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.95" />
                  <Point X="-3.384959157179" Y="3.142771271599" Z="0.95" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.95" />
                  <Point X="-4.052577615423" Y="3.045721167049" Z="0.95" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.95" />
                  <Point X="-4.420326774766" Y="2.482042096666" Z="0.95" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.95" />
                  <Point X="-4.228906841898" Y="2.019316417288" Z="0.95" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.95" />
                  <Point X="-3.677960478325" Y="1.575100368889" Z="0.95" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.95" />
                  <Point X="-3.682239040352" Y="1.516485397167" Z="0.95" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.95" />
                  <Point X="-3.729891004856" Y="1.482085996979" Z="0.95" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.95" />
                  <Point X="-4.151230960485" Y="1.527274333781" Z="0.95" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.95" />
                  <Point X="-4.914280898113" Y="1.254001521842" Z="0.95" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.95" />
                  <Point X="-5.027558317573" Y="0.668091012806" Z="0.95" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.95" />
                  <Point X="-4.504633192483" Y="0.29774530121" Z="0.95" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.95" />
                  <Point X="-3.559201748797" Y="0.037020903848" Z="0.95" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.95" />
                  <Point X="-3.543021431034" Y="0.011163172741" Z="0.95" />
                  <Point X="-3.539556741714" Y="0" Z="0.95" />
                  <Point X="-3.545343170101" Y="-0.01864377832" Z="0.95" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.95" />
                  <Point X="-3.566166846326" Y="-0.041855079286" Z="0.95" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.95" />
                  <Point X="-4.13225459629" Y="-0.197966755546" Z="0.95" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.95" />
                  <Point X="-5.011748613306" Y="-0.786298351659" Z="0.95" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.95" />
                  <Point X="-4.897746983604" Y="-1.322108856122" Z="0.95" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.95" />
                  <Point X="-4.237287389832" Y="-1.44090244239" Z="0.95" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.95" />
                  <Point X="-3.202594449962" Y="-1.316612415281" Z="0.95" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.95" />
                  <Point X="-3.197495346885" Y="-1.341056312722" Z="0.95" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.95" />
                  <Point X="-3.688194986049" Y="-1.726510142878" Z="0.95" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.95" />
                  <Point X="-4.319292375137" Y="-2.659538688545" Z="0.95" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.95" />
                  <Point X="-3.992239291881" Y="-3.129132270803" Z="0.95" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.95" />
                  <Point X="-3.379338501375" Y="-3.021123380545" Z="0.95" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.95" />
                  <Point X="-2.561988267616" Y="-2.566342201516" Z="0.95" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.95" />
                  <Point X="-2.834293632624" Y="-3.055739881117" Z="0.95" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.95" />
                  <Point X="-3.043821185752" Y="-4.059431093643" Z="0.95" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.95" />
                  <Point X="-2.61566415568" Y="-4.347658522805" Z="0.95" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.95" />
                  <Point X="-2.366890854811" Y="-4.339774975048" Z="0.95" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.95" />
                  <Point X="-2.064868432468" Y="-4.04863882847" Z="0.95" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.95" />
                  <Point X="-1.774612862059" Y="-3.99677642315" Z="0.95" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.95" />
                  <Point X="-1.512765849674" Y="-4.132327851361" Z="0.95" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.95" />
                  <Point X="-1.387547016534" Y="-4.399270186363" Z="0.95" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.95" />
                  <Point X="-1.382937877881" Y="-4.650406497854" Z="0.95" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.95" />
                  <Point X="-1.228145102589" Y="-4.927090293157" Z="0.95" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.95" />
                  <Point X="-0.929923094591" Y="-4.991973857108" Z="0.95" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.95" />
                  <Point X="-0.667644072766" Y="-4.453865692296" Z="0.95" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.95" />
                  <Point X="-0.314677682586" Y="-3.371221093716" Z="0.95" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.95" />
                  <Point X="-0.094886966551" Y="-3.2336887812" Z="0.95" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.95" />
                  <Point X="0.15847211281" Y="-3.253423264088" Z="0.95" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.95" />
                  <Point X="0.355768176847" Y="-3.430424662052" Z="0.95" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.95" />
                  <Point X="0.567110787635" Y="-4.07867033752" Z="0.95" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.95" />
                  <Point X="0.930469209204" Y="-4.993271102605" Z="0.95" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.95" />
                  <Point X="1.109998619471" Y="-4.956453570892" Z="0.95" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.95" />
                  <Point X="1.094769170485" Y="-4.316747825099" Z="0.95" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.95" />
                  <Point X="0.991005738275" Y="-3.11805150044" Z="0.95" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.95" />
                  <Point X="1.123737006955" Y="-2.931721937239" Z="0.95" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.95" />
                  <Point X="1.336935813286" Y="-2.862259521662" Z="0.95" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.95" />
                  <Point X="1.557535811065" Y="-2.939929615357" Z="0.95" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.95" />
                  <Point X="2.02111748354" Y="-3.491375610757" Z="0.95" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.95" />
                  <Point X="2.784158026163" Y="-4.247610292375" Z="0.95" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.95" />
                  <Point X="2.975639607649" Y="-4.11573790583" Z="0.95" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.95" />
                  <Point X="2.756159637975" Y="-3.562209096578" Z="0.95" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.95" />
                  <Point X="2.24682688371" Y="-2.587137455655" Z="0.95" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.95" />
                  <Point X="2.291306291637" Y="-2.393922510208" Z="0.95" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.95" />
                  <Point X="2.438975868164" Y="-2.26759501833" Z="0.95" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.95" />
                  <Point X="2.641369347858" Y="-2.256621125781" Z="0.95" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.95" />
                  <Point X="3.225204500529" Y="-2.561589862299" Z="0.95" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.95" />
                  <Point X="4.174329075985" Y="-2.891334409567" Z="0.95" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.95" />
                  <Point X="4.339478416168" Y="-2.636999209956" Z="0.95" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.95" />
                  <Point X="3.947368126884" Y="-2.193637466852" Z="0.95" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.95" />
                  <Point X="3.129893968534" Y="-1.516835763929" Z="0.95" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.95" />
                  <Point X="3.102100717235" Y="-1.351388309018" Z="0.95" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.95" />
                  <Point X="3.176634645127" Y="-1.204815786407" Z="0.95" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.95" />
                  <Point X="3.33130115772" Y="-1.130700223706" Z="0.95" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.95" />
                  <Point X="3.963959838658" Y="-1.190259315328" Z="0.95" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.95" />
                  <Point X="4.959817406447" Y="-1.082990241349" Z="0.95" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.95" />
                  <Point X="5.026826241412" Y="-0.709702633977" Z="0.95" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.95" />
                  <Point X="4.561121261384" Y="-0.438698486297" Z="0.95" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.95" />
                  <Point X="3.690089161432" Y="-0.187364474603" Z="0.95" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.95" />
                  <Point X="3.620724591754" Y="-0.123099160149" Z="0.95" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.95" />
                  <Point X="3.588364016063" Y="-0.036182165931" Z="0.95" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.95" />
                  <Point X="3.593007434361" Y="0.060428365289" Z="0.95" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.95" />
                  <Point X="3.634654846647" Y="0.140849578426" Z="0.95" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.95" />
                  <Point X="3.71330625292" Y="0.200784369639" Z="0.95" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.95" />
                  <Point X="4.234846555923" Y="0.351273446472" Z="0.95" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.95" />
                  <Point X="5.006794309027" Y="0.833915820735" Z="0.95" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.95" />
                  <Point X="4.918734180038" Y="1.252781243288" Z="0.95" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.95" />
                  <Point X="4.349847944739" Y="1.338763881496" Z="0.95" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.95" />
                  <Point X="3.404225320156" Y="1.229807919436" Z="0.95" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.95" />
                  <Point X="3.325400603209" Y="1.258989095253" Z="0.95" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.95" />
                  <Point X="3.269259307726" Y="1.319359761055" Z="0.95" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.95" />
                  <Point X="3.240209338185" Y="1.400278371993" Z="0.95" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.95" />
                  <Point X="3.247054926584" Y="1.480489285773" Z="0.95" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.95" />
                  <Point X="3.2912578249" Y="1.556463579626" Z="0.95" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.95" />
                  <Point X="3.737753968014" Y="1.9106984178" Z="0.95" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.95" />
                  <Point X="4.316505669415" Y="2.671319970297" Z="0.95" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.95" />
                  <Point X="4.090617972073" Y="3.005830662892" Z="0.95" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.95" />
                  <Point X="3.443340127324" Y="2.805933393701" Z="0.95" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.95" />
                  <Point X="2.459660126558" Y="2.253569875626" Z="0.95" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.95" />
                  <Point X="2.386167494724" Y="2.25076532432" Z="0.95" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.95" />
                  <Point X="2.320568183699" Y="2.280769795192" Z="0.95" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.95" />
                  <Point X="2.269988805043" Y="2.336456676683" Z="0.95" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.95" />
                  <Point X="2.248664378438" Y="2.403590949109" Z="0.95" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.95" />
                  <Point X="2.258958067032" Y="2.479809449324" Z="0.95" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.95" />
                  <Point X="2.589691959159" Y="3.068798954631" Z="0.95" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.95" />
                  <Point X="2.89398946401" Y="4.169122119417" Z="0.95" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.95" />
                  <Point X="2.504720787903" Y="4.413970087498" Z="0.95" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.95" />
                  <Point X="2.09823116467" Y="4.621192297922" Z="0.95" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.95" />
                  <Point X="1.676765786751" Y="4.790245600089" Z="0.95" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.95" />
                  <Point X="1.107558973165" Y="4.947077342532" Z="0.95" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.95" />
                  <Point X="0.443913236643" Y="5.050071327311" Z="0.95" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.95" />
                  <Point X="0.120871428944" Y="4.806222810597" Z="0.95" />
                  <Point X="0" Y="4.355124473572" Z="0.95" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>