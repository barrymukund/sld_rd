<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#179" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2347" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004716064453" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.807953125" Y="-4.425568847656" />
                  <Point X="25.5633046875" Y="-3.512524414063" />
                  <Point X="25.55772265625" Y="-3.497140625" />
                  <Point X="25.542365234375" Y="-3.467376953125" />
                  <Point X="25.44237109375" Y="-3.323303955078" />
                  <Point X="25.37863671875" Y="-3.2314765625" />
                  <Point X="25.356751953125" Y="-3.209020507813" />
                  <Point X="25.33049609375" Y="-3.189776611328" />
                  <Point X="25.30249609375" Y="-3.175669189453" />
                  <Point X="25.14776171875" Y="-3.127644775391" />
                  <Point X="25.04913671875" Y="-3.097035888672" />
                  <Point X="25.0209765625" Y="-3.092766357422" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097036132812" />
                  <Point X="24.80844140625" Y="-3.145060302734" />
                  <Point X="24.70981640625" Y="-3.175669433594" />
                  <Point X="24.68181640625" Y="-3.18977734375" />
                  <Point X="24.655560546875" Y="-3.209021484375" />
                  <Point X="24.63367578125" Y="-3.231477050781" />
                  <Point X="24.533681640625" Y="-3.375549804688" />
                  <Point X="24.46994921875" Y="-3.467377441406" />
                  <Point X="24.4618125" Y="-3.481571777344" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.293078125" Y="-4.094469726562" />
                  <Point X="24.0834140625" Y="-4.876941894531" />
                  <Point X="24.03305859375" Y="-4.867167480469" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.75358203125" Y="-4.802362304688" />
                  <Point X="23.755587890625" Y="-4.787132324219" />
                  <Point X="23.7850390625" Y="-4.5634375" />
                  <Point X="23.78580078125" Y="-4.547929199219" />
                  <Point X="23.7834921875" Y="-4.516223632812" />
                  <Point X="23.746525390625" Y="-4.330381835937" />
                  <Point X="23.722962890625" Y="-4.211932128906" />
                  <Point X="23.712060546875" Y="-4.18296484375" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204101562" />
                  <Point X="23.533896484375" Y="-4.006269287109" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295410156" />
                  <Point X="23.387115234375" Y="-3.897994384766" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.16789453125" Y="-3.878573486328" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708740234" />
                  <Point X="22.985533203125" Y="-3.882028564453" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.79979296875" Y="-4.000072998047" />
                  <Point X="22.699376953125" Y="-4.067169677734" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.664900390625" Y="-4.099461425781" />
                  <Point X="22.57735546875" Y="-4.213550292969" />
                  <Point X="22.519853515625" Y="-4.288489746094" />
                  <Point X="22.363041015625" Y="-4.191395996094" />
                  <Point X="22.19828515625" Y="-4.0893828125" />
                  <Point X="21.954076171875" Y="-3.901349853516" />
                  <Point X="21.895279296875" Y="-3.856078125" />
                  <Point X="22.1215859375" Y="-3.464102294922" />
                  <Point X="22.576240234375" Y="-2.676619384766" />
                  <Point X="22.587140625" Y="-2.647654541016" />
                  <Point X="22.593412109375" Y="-2.616127929688" />
                  <Point X="22.59442578125" Y="-2.585193847656" />
                  <Point X="22.58544140625" Y="-2.555575927734" />
                  <Point X="22.571224609375" Y="-2.526746826172" />
                  <Point X="22.55319921875" Y="-2.501591064453" />
                  <Point X="22.53585546875" Y="-2.48424609375" />
                  <Point X="22.510701171875" Y="-2.46621875" />
                  <Point X="22.48187109375" Y="-2.451999023438" />
                  <Point X="22.452251953125" Y="-2.443012451172" />
                  <Point X="22.42131640625" Y="-2.444023925781" />
                  <Point X="22.389787109375" Y="-2.450294433594" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.858900390625" Y="-2.750979003906" />
                  <Point X="21.1819765625" Y="-3.141801269531" />
                  <Point X="21.04730078125" Y="-2.964865234375" />
                  <Point X="20.917140625" Y="-2.793861816406" />
                  <Point X="20.74205859375" Y="-2.500275390625" />
                  <Point X="20.693857421875" Y="-2.419449951172" />
                  <Point X="21.098548828125" Y="-2.108919189453" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.915419921875" Y="-1.475598388672" />
                  <Point X="21.933384765625" Y="-1.448470947266" />
                  <Point X="21.946142578125" Y="-1.419837646484" />
                  <Point X="21.953849609375" Y="-1.390082275391" />
                  <Point X="21.956654296875" Y="-1.359645751953" />
                  <Point X="21.954443359375" Y="-1.327977416992" />
                  <Point X="21.94744140625" Y="-1.298234619141" />
                  <Point X="21.931357421875" Y="-1.272253173828" />
                  <Point X="21.910525390625" Y="-1.248298828125" />
                  <Point X="21.887029296875" Y="-1.228767211914" />
                  <Point X="21.860546875" Y="-1.213180664062" />
                  <Point X="21.831283203125" Y="-1.201956665039" />
                  <Point X="21.799396484375" Y="-1.195474975586" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="21.134451171875" Y="-1.277801757812" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.21683203125" Y="-1.191955200195" />
                  <Point X="20.165921875" Y="-0.99265045166" />
                  <Point X="20.1196015625" Y="-0.668774841309" />
                  <Point X="20.107576171875" Y="-0.584698425293" />
                  <Point X="20.560935546875" Y="-0.463221252441" />
                  <Point X="21.467125" Y="-0.220408279419" />
                  <Point X="21.4825078125" Y="-0.214827850342" />
                  <Point X="21.51226953125" Y="-0.199470458984" />
                  <Point X="21.52921875" Y="-0.18770640564" />
                  <Point X="21.5400234375" Y="-0.1802084198" />
                  <Point X="21.562474609375" Y="-0.15832875061" />
                  <Point X="21.581720703125" Y="-0.132072967529" />
                  <Point X="21.595833984375" Y="-0.104065216064" />
                  <Point X="21.6050859375" Y="-0.074251617432" />
                  <Point X="21.609353515625" Y="-0.046089038849" />
                  <Point X="21.6093515625" Y="-0.016451818466" />
                  <Point X="21.60508203125" Y="0.011698917389" />
                  <Point X="21.595833984375" Y="0.041499160767" />
                  <Point X="21.58173046875" Y="0.069496589661" />
                  <Point X="21.562486328125" Y="0.095756622314" />
                  <Point X="21.5400234375" Y="0.117648887634" />
                  <Point X="21.523072265625" Y="0.129412948608" />
                  <Point X="21.515208984375" Y="0.134320022583" />
                  <Point X="21.488837890625" Y="0.149038833618" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="20.889548828125" Y="0.312609344482" />
                  <Point X="20.108185546875" Y="0.521975402832" />
                  <Point X="20.142705078125" Y="0.755261169434" />
                  <Point X="20.17551171875" Y="0.976968811035" />
                  <Point X="20.2687578125" Y="1.321078735352" />
                  <Point X="20.29644921875" Y="1.423267944336" />
                  <Point X="20.584755859375" Y="1.385311523438" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228149414" />
                  <Point X="21.296865234375" Y="1.305263671875" />
                  <Point X="21.33437890625" Y="1.317092041016" />
                  <Point X="21.358291015625" Y="1.324631225586" />
                  <Point X="21.377224609375" Y="1.332962402344" />
                  <Point X="21.39596875" Y="1.343784545898" />
                  <Point X="21.412646484375" Y="1.356014160156" />
                  <Point X="21.426283203125" Y="1.371563598633" />
                  <Point X="21.43869921875" Y="1.389294067383" />
                  <Point X="21.44865234375" Y="1.407432617188" />
                  <Point X="21.463705078125" Y="1.443774291992" />
                  <Point X="21.473298828125" Y="1.466937011719" />
                  <Point X="21.479087890625" Y="1.486806152344" />
                  <Point X="21.48284375" Y="1.508122192383" />
                  <Point X="21.484193359375" Y="1.528755981445" />
                  <Point X="21.481048828125" Y="1.549193115234" />
                  <Point X="21.475447265625" Y="1.570100219727" />
                  <Point X="21.467951171875" Y="1.589378051758" />
                  <Point X="21.4497890625" Y="1.624269287109" />
                  <Point X="21.4382109375" Y="1.6465078125" />
                  <Point X="21.42671484375" Y="1.663712768555" />
                  <Point X="21.412802734375" Y="1.680290649414" />
                  <Point X="21.39786328125" Y="1.694590332031" />
                  <Point X="21.066564453125" Y="1.948805175781" />
                  <Point X="20.648140625" Y="2.269873291016" />
                  <Point X="20.791369140625" Y="2.515257568359" />
                  <Point X="20.918853515625" Y="2.733665771484" />
                  <Point X="21.16584765625" Y="3.051145996094" />
                  <Point X="21.24949609375" Y="3.158662353516" />
                  <Point X="21.394390625" Y="3.075006835938" />
                  <Point X="21.79334375" Y="2.844670898438" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.905455078125" Y="2.821225341797" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.959435546875" Y="2.818762939453" />
                  <Point X="21.980892578125" Y="2.821587890625" />
                  <Point X="22.000984375" Y="2.826504150391" />
                  <Point X="22.01953515625" Y="2.83565234375" />
                  <Point X="22.0377890625" Y="2.847281005859" />
                  <Point X="22.053921875" Y="2.860228759766" />
                  <Point X="22.0910078125" Y="2.897314697266" />
                  <Point X="22.114646484375" Y="2.920952148438" />
                  <Point X="22.12758984375" Y="2.937077392578" />
                  <Point X="22.139220703125" Y="2.955331054688" />
                  <Point X="22.14837109375" Y="2.973883056641" />
                  <Point X="22.1532890625" Y="2.993976074219" />
                  <Point X="22.156115234375" Y="3.015434326172" />
                  <Point X="22.15656640625" Y="3.036120849609" />
                  <Point X="22.151994140625" Y="3.088369140625" />
                  <Point X="22.14908203125" Y="3.121670410156" />
                  <Point X="22.145044921875" Y="3.141958251953" />
                  <Point X="22.13853515625" Y="3.162602539062" />
                  <Point X="22.130205078125" Y="3.181532958984" />
                  <Point X="21.983396484375" Y="3.435812988281" />
                  <Point X="21.81666796875" Y="3.724595703125" />
                  <Point X="22.077353515625" Y="3.924460449219" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.688392578125" Y="4.310813964844" />
                  <Point X="22.83296484375" Y="4.391134277344" />
                  <Point X="22.834673828125" Y="4.38890625" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971107421875" Y="4.214799804688" />
                  <Point X="22.9876875" Y="4.200887207031" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.063041015625" Y="4.159122558594" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.119384765625" Y="4.132330566406" />
                  <Point X="23.140291015625" Y="4.126729003906" />
                  <Point X="23.160732421875" Y="4.123583007812" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222548828125" Y="4.134482421875" />
                  <Point X="23.2831171875" Y="4.159571289062" />
                  <Point X="23.32172265625" Y="4.175562011719" />
                  <Point X="23.339857421875" Y="4.185510253906" />
                  <Point X="23.3575859375" Y="4.197923828125" />
                  <Point X="23.37313671875" Y="4.211561523438" />
                  <Point X="23.3853671875" Y="4.228241699219" />
                  <Point X="23.396189453125" Y="4.246985351563" />
                  <Point X="23.404521484375" Y="4.265921875" />
                  <Point X="23.424234375" Y="4.328447265625" />
                  <Point X="23.43680078125" Y="4.368298339844" />
                  <Point X="23.4408359375" Y="4.388583007812" />
                  <Point X="23.44272265625" Y="4.410145507812" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.425580078125" Y="4.557604003906" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.762943359375" Y="4.729225585938" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.52196875" Y="4.865002441406" />
                  <Point X="24.7052890625" Y="4.88645703125" />
                  <Point X="24.74807421875" Y="4.726778320312" />
                  <Point X="24.866095703125" Y="4.286315917969" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286314941406" />
                  <Point X="25.22144140625" Y="4.567051757812" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.59307421875" Y="4.858021972656" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.234216796875" Y="4.737538574219" />
                  <Point X="26.4810234375" Y="4.677952148438" />
                  <Point X="26.734416015625" Y="4.586044433594" />
                  <Point X="26.894650390625" Y="4.527926757812" />
                  <Point X="27.14022265625" Y="4.413081054688" />
                  <Point X="27.294576171875" Y="4.340894042969" />
                  <Point X="27.531837890625" Y="4.202665039063" />
                  <Point X="27.68097265625" Y="4.115777832031" />
                  <Point X="27.9047265625" Y="3.956658691406" />
                  <Point X="27.94326171875" Y="3.929254150391" />
                  <Point X="27.673810546875" Y="3.462550292969" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142078125" Y="2.53993359375" />
                  <Point X="27.133078125" Y="2.516058349609" />
                  <Point X="27.11996484375" Y="2.467024658203" />
                  <Point X="27.111607421875" Y="2.435771972656" />
                  <Point X="27.108619140625" Y="2.417936035156" />
                  <Point X="27.107728515625" Y="2.380953125" />
                  <Point X="27.112841796875" Y="2.338552978516" />
                  <Point X="27.116099609375" Y="2.311528320312" />
                  <Point X="27.121443359375" Y="2.289604492188" />
                  <Point X="27.1297109375" Y="2.267513427734" />
                  <Point X="27.140072265625" Y="2.247469482422" />
                  <Point X="27.16630859375" Y="2.2088046875" />
                  <Point X="27.18303125" Y="2.184160888672" />
                  <Point X="27.19446484375" Y="2.170329589844" />
                  <Point X="27.221599609375" Y="2.145592773438" />
                  <Point X="27.260263671875" Y="2.119356933594" />
                  <Point X="27.284908203125" Y="2.102635253906" />
                  <Point X="27.304955078125" Y="2.092271728516" />
                  <Point X="27.32704296875" Y="2.084006103516" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.391365234375" Y="2.07355078125" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845458984" />
                  <Point X="27.47320703125" Y="2.074170898438" />
                  <Point X="27.5222421875" Y="2.087283203125" />
                  <Point X="27.553494140625" Y="2.095640625" />
                  <Point X="27.5652890625" Y="2.099639648438" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.16946484375" Y="2.445545410156" />
                  <Point X="28.967326171875" Y="2.906190185547" />
                  <Point X="29.0348046875" Y="2.812409912109" />
                  <Point X="29.12328125" Y="2.689450927734" />
                  <Point X="29.2480078125" Y="2.483335693359" />
                  <Point X="29.26219921875" Y="2.459883544922" />
                  <Point X="28.92361328125" Y="2.200076416016" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.221427734375" Y="1.660243896484" />
                  <Point X="28.20397265625" Y="1.641626831055" />
                  <Point X="28.16868359375" Y="1.595588745117" />
                  <Point X="28.14619140625" Y="1.566245483398" />
                  <Point X="28.13660546875" Y="1.550909912109" />
                  <Point X="28.1216328125" Y="1.517088500977" />
                  <Point X="28.108486328125" Y="1.470083496094" />
                  <Point X="28.100107421875" Y="1.440123901367" />
                  <Point X="28.09665234375" Y="1.41782434082" />
                  <Point X="28.0958359375" Y="1.394252197266" />
                  <Point X="28.09773828125" Y="1.371766845703" />
                  <Point X="28.108529296875" Y="1.319467529297" />
                  <Point X="28.115408203125" Y="1.286133911133" />
                  <Point X="28.1206796875" Y="1.26897668457" />
                  <Point X="28.13628125" Y="1.235741577148" />
                  <Point X="28.165630859375" Y="1.191130249023" />
                  <Point X="28.184337890625" Y="1.162696166992" />
                  <Point X="28.198892578125" Y="1.145450195312" />
                  <Point X="28.21613671875" Y="1.129360107422" />
                  <Point X="28.234345703125" Y="1.116034545898" />
                  <Point X="28.27687890625" Y="1.092092163086" />
                  <Point X="28.30398828125" Y="1.07683215332" />
                  <Point X="28.3205234375" Y="1.069500366211" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.413626953125" Y="1.051838134766" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.040048828125" Y="1.119636474609" />
                  <Point X="29.77683984375" Y="1.216636474609" />
                  <Point X="29.807939453125" Y="1.088883789062" />
                  <Point X="29.84594140625" Y="0.932785400391" />
                  <Point X="29.88524609375" Y="0.680341796875" />
                  <Point X="29.8908671875" Y="0.644239013672" />
                  <Point X="29.510576171875" Y="0.542340270996" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.704791015625" Y="0.325586517334" />
                  <Point X="28.681546875" Y="0.315067901611" />
                  <Point X="28.625048828125" Y="0.282410339355" />
                  <Point X="28.589037109375" Y="0.261595336914" />
                  <Point X="28.574310546875" Y="0.251094924927" />
                  <Point X="28.547533203125" Y="0.225576690674" />
                  <Point X="28.5136328125" Y="0.18238079834" />
                  <Point X="28.49202734375" Y="0.154849060059" />
                  <Point X="28.48030078125" Y="0.135566070557" />
                  <Point X="28.47052734375" Y="0.114101852417" />
                  <Point X="28.463681640625" Y="0.09260408783" />
                  <Point X="28.452380859375" Y="0.033600597382" />
                  <Point X="28.4451796875" Y="-0.00400637579" />
                  <Point X="28.443484375" Y="-0.021876653671" />
                  <Point X="28.4451796875" Y="-0.058553150177" />
                  <Point X="28.456478515625" Y="-0.117556793213" />
                  <Point X="28.463681640625" Y="-0.155163619995" />
                  <Point X="28.470529296875" Y="-0.176668060303" />
                  <Point X="28.480302734375" Y="-0.198129852295" />
                  <Point X="28.49202734375" Y="-0.217409347534" />
                  <Point X="28.525927734375" Y="-0.260605102539" />
                  <Point X="28.547533203125" Y="-0.288136993408" />
                  <Point X="28.56" Y="-0.301234832764" />
                  <Point X="28.589037109375" Y="-0.324155609131" />
                  <Point X="28.645537109375" Y="-0.356813171387" />
                  <Point X="28.681546875" Y="-0.37762802124" />
                  <Point X="28.69270703125" Y="-0.383137237549" />
                  <Point X="28.716580078125" Y="-0.392149963379" />
                  <Point X="29.2226484375" Y="-0.527750366211" />
                  <Point X="29.891474609375" Y="-0.706961975098" />
                  <Point X="29.876240234375" Y="-0.808010925293" />
                  <Point X="29.855025390625" Y="-0.948723937988" />
                  <Point X="29.8046640625" Y="-1.169409790039" />
                  <Point X="29.80117578125" Y="-1.18469921875" />
                  <Point X="29.345193359375" Y="-1.12466809082" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.263771484375" Y="-1.029610839844" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.1639765625" Y="-1.056596313477" />
                  <Point X="28.1361484375" Y="-1.073488891602" />
                  <Point X="28.1123984375" Y="-1.093960083008" />
                  <Point X="28.045373046875" Y="-1.174569702148" />
                  <Point X="28.002654296875" Y="-1.225947998047" />
                  <Point X="27.987931640625" Y="-1.250334106445" />
                  <Point X="27.97658984375" Y="-1.27771875" />
                  <Point X="27.969759765625" Y="-1.305365844727" />
                  <Point X="27.960154296875" Y="-1.409759399414" />
                  <Point X="27.95403125" Y="-1.476296142578" />
                  <Point X="27.95634765625" Y="-1.507560913086" />
                  <Point X="27.964078125" Y="-1.539182128906" />
                  <Point X="27.976451171875" Y="-1.567996826172" />
                  <Point X="28.037818359375" Y="-1.66344909668" />
                  <Point X="28.076931640625" Y="-1.724287353516" />
                  <Point X="28.0869375" Y="-1.737241943359" />
                  <Point X="28.110630859375" Y="-1.760909179688" />
                  <Point X="28.580263671875" Y="-2.121271484375" />
                  <Point X="29.213123046875" Y="-2.606882080078" />
                  <Point X="29.18461328125" Y="-2.653014892578" />
                  <Point X="29.124802734375" Y="-2.749797607422" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="28.6210625" Y="-2.650432128906" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.754224609375" Y="-2.159825195312" />
                  <Point X="27.62225" Y="-2.135990722656" />
                  <Point X="27.538134765625" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395507812" />
                  <Point X="27.474609375" Y="-2.125353515625" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.3351953125" Y="-2.192878662109" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246549804688" />
                  <Point X="27.22142578125" Y="-2.267509277344" />
                  <Point X="27.204533203125" Y="-2.290438476562" />
                  <Point X="27.146830078125" Y="-2.400076660156" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.100228515625" Y="-2.499735351563" />
                  <Point X="27.095271484375" Y="-2.531908203125" />
                  <Point X="27.09567578125" Y="-2.563259277344" />
                  <Point X="27.11951171875" Y="-2.695233398438" />
                  <Point X="27.134703125" Y="-2.779349609375" />
                  <Point X="27.13898828125" Y="-2.795139648438" />
                  <Point X="27.1518203125" Y="-2.826078369141" />
                  <Point X="27.45360546875" Y="-3.348788085938" />
                  <Point X="27.86128515625" Y="-4.054905517578" />
                  <Point X="27.852814453125" Y="-4.060955322266" />
                  <Point X="27.781833984375" Y="-4.111653320313" />
                  <Point X="27.701765625" Y="-4.163480957031" />
                  <Point X="27.384365234375" Y="-3.749835449219" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922177490234" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.59176171875" Y="-2.816874755859" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.479990234375" Y="-2.751167236328" />
                  <Point X="26.448369140625" Y="-2.743435546875" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.27474609375" Y="-2.754216308594" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.156365234375" Y="-2.769396972656" />
                  <Point X="26.12898046875" Y="-2.780739746094" />
                  <Point X="26.10459765625" Y="-2.7954609375" />
                  <Point X="25.99467578125" Y="-2.886857910156" />
                  <Point X="25.92461328125" Y="-2.945111328125" />
                  <Point X="25.904142578125" Y="-2.968860595703" />
                  <Point X="25.88725" Y="-2.996687011719" />
                  <Point X="25.875625" Y="-3.02580859375" />
                  <Point X="25.842759765625" Y="-3.17701953125" />
                  <Point X="25.821810546875" Y="-3.273396484375" />
                  <Point X="25.819724609375" Y="-3.289627197266" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.905267578125" Y="-3.972739501953" />
                  <Point X="26.022064453125" Y="-4.859915527344" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94157421875" Y="-4.75263671875" />
                  <Point X="23.858755859375" Y="-4.731328125" />
                  <Point X="23.8792265625" Y="-4.575842285156" />
                  <Point X="23.879923828125" Y="-4.568098144531" />
                  <Point X="23.88055078125" Y="-4.541030273438" />
                  <Point X="23.8782421875" Y="-4.509324707031" />
                  <Point X="23.876666015625" Y="-4.497689941406" />
                  <Point X="23.83969921875" Y="-4.311848144531" />
                  <Point X="23.81613671875" Y="-4.1933984375" />
                  <Point X="23.811875" Y="-4.17846875" />
                  <Point X="23.80097265625" Y="-4.149501464844" />
                  <Point X="23.79433203125" Y="-4.135463867188" />
                  <Point X="23.778259765625" Y="-4.107626464844" />
                  <Point X="23.76942578125" Y="-4.094861083984" />
                  <Point X="23.74979296875" Y="-4.070937744141" />
                  <Point X="23.738994140625" Y="-4.059779785156" />
                  <Point X="23.59653515625" Y="-3.934844970703" />
                  <Point X="23.505734375" Y="-3.855215332031" />
                  <Point X="23.493263671875" Y="-3.845965820312" />
                  <Point X="23.46698046875" Y="-3.829621582031" />
                  <Point X="23.45316796875" Y="-3.822526855469" />
                  <Point X="23.423470703125" Y="-3.810225830078" />
                  <Point X="23.4086875" Y="-3.805475830078" />
                  <Point X="23.378544921875" Y="-3.798447753906" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.174107421875" Y="-3.783776855469" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166259766" />
                  <Point X="22.991994140625" Y="-3.781946044922" />
                  <Point X="22.9609453125" Y="-3.790265869141" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.80826953125" />
                  <Point X="22.9045625" Y="-3.815812255859" />
                  <Point X="22.747013671875" Y="-3.921083496094" />
                  <Point X="22.64659765625" Y="-3.988180175781" />
                  <Point X="22.64031640625" Y="-3.992759765625" />
                  <Point X="22.619556640625" Y="-4.010133789063" />
                  <Point X="22.5972421875" Y="-4.032772216797" />
                  <Point X="22.589533203125" Y="-4.041628417969" />
                  <Point X="22.50198828125" Y="-4.155717285156" />
                  <Point X="22.496798828125" Y="-4.162479003906" />
                  <Point X="22.413052734375" Y="-4.110625488281" />
                  <Point X="22.252404296875" Y="-4.011155517578" />
                  <Point X="22.019138671875" Y="-3.831548583984" />
                  <Point X="22.203859375" Y="-3.511602294922" />
                  <Point X="22.658513671875" Y="-2.724119384766" />
                  <Point X="22.66515234375" Y="-2.710079833984" />
                  <Point X="22.676052734375" Y="-2.681114990234" />
                  <Point X="22.680314453125" Y="-2.666189453125" />
                  <Point X="22.6865859375" Y="-2.634662841797" />
                  <Point X="22.688361328125" Y="-2.619239257812" />
                  <Point X="22.689375" Y="-2.588305175781" />
                  <Point X="22.6853359375" Y="-2.5576171875" />
                  <Point X="22.6763515625" Y="-2.527999267578" />
                  <Point X="22.67064453125" Y="-2.513558837891" />
                  <Point X="22.656427734375" Y="-2.484729736328" />
                  <Point X="22.648447265625" Y="-2.471413330078" />
                  <Point X="22.630421875" Y="-2.446257568359" />
                  <Point X="22.620376953125" Y="-2.434418212891" />
                  <Point X="22.603033203125" Y="-2.417073242188" />
                  <Point X="22.5911953125" Y="-2.407028564453" />
                  <Point X="22.566041015625" Y="-2.389001220703" />
                  <Point X="22.552724609375" Y="-2.381018554688" />
                  <Point X="22.52389453125" Y="-2.366798828125" />
                  <Point X="22.509453125" Y="-2.361091064453" />
                  <Point X="22.479833984375" Y="-2.352104492188" />
                  <Point X="22.4491484375" Y="-2.348063232422" />
                  <Point X="22.418212890625" Y="-2.349074707031" />
                  <Point X="22.40278515625" Y="-2.350848632812" />
                  <Point X="22.371255859375" Y="-2.357119140625" />
                  <Point X="22.35632421875" Y="-2.361383056641" />
                  <Point X="22.32735546875" Y="-2.372285888672" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.811400390625" Y="-2.668706542969" />
                  <Point X="21.206912109375" Y="-3.017708251953" />
                  <Point X="21.12289453125" Y="-2.907326904297" />
                  <Point X="20.995978515625" Y="-2.7405859375" />
                  <Point X="20.823650390625" Y="-2.4516171875" />
                  <Point X="20.818734375" Y="-2.443373535156" />
                  <Point X="21.156380859375" Y="-2.184287841797" />
                  <Point X="21.951876953125" Y="-1.573881958008" />
                  <Point X="21.963513671875" Y="-1.563313598633" />
                  <Point X="21.984888671875" Y="-1.54039855957" />
                  <Point X="21.994626953125" Y="-1.528051879883" />
                  <Point X="22.012591796875" Y="-1.500924438477" />
                  <Point X="22.02016015625" Y="-1.487134765625" />
                  <Point X="22.03291796875" Y="-1.458501464844" />
                  <Point X="22.038107421875" Y="-1.443657836914" />
                  <Point X="22.045814453125" Y="-1.41390246582" />
                  <Point X="22.04844921875" Y="-1.398799438477" />
                  <Point X="22.05125390625" Y="-1.368362915039" />
                  <Point X="22.051423828125" Y="-1.353029418945" />
                  <Point X="22.049212890625" Y="-1.321361083984" />
                  <Point X="22.046916015625" Y="-1.306207763672" />
                  <Point X="22.0399140625" Y="-1.276465087891" />
                  <Point X="22.028216796875" Y="-1.24823046875" />
                  <Point X="22.0121328125" Y="-1.222248901367" />
                  <Point X="22.003041015625" Y="-1.209912475586" />
                  <Point X="21.982208984375" Y="-1.185958129883" />
                  <Point X="21.97125390625" Y="-1.175243896484" />
                  <Point X="21.9477578125" Y="-1.155712158203" />
                  <Point X="21.935216796875" Y="-1.146895263672" />
                  <Point X="21.908734375" Y="-1.13130859375" />
                  <Point X="21.89456640625" Y="-1.124481079102" />
                  <Point X="21.865302734375" Y="-1.113257080078" />
                  <Point X="21.85020703125" Y="-1.108860473633" />
                  <Point X="21.8183203125" Y="-1.10237890625" />
                  <Point X="21.802703125" Y="-1.100532592773" />
                  <Point X="21.77137890625" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196655273" />
                  <Point X="21.12205078125" Y="-1.183614379883" />
                  <Point X="20.33908203125" Y="-1.286694213867" />
                  <Point X="20.308876953125" Y="-1.168444091797" />
                  <Point X="20.259236328125" Y="-0.974113342285" />
                  <Point X="20.21364453125" Y="-0.655324829102" />
                  <Point X="20.213548828125" Y="-0.654654418945" />
                  <Point X="20.5855234375" Y="-0.554984130859" />
                  <Point X="21.491712890625" Y="-0.312171173096" />
                  <Point X="21.499521484375" Y="-0.309713409424" />
                  <Point X="21.5260703125" Y="-0.299250793457" />
                  <Point X="21.55583203125" Y="-0.283893463135" />
                  <Point X="21.5664375" Y="-0.277514099121" />
                  <Point X="21.58338671875" Y="-0.26575" />
                  <Point X="21.606326171875" Y="-0.248243865967" />
                  <Point X="21.62877734375" Y="-0.226364196777" />
                  <Point X="21.63909375" Y="-0.214492797852" />
                  <Point X="21.65833984375" Y="-0.188237075806" />
                  <Point X="21.66655859375" Y="-0.174823120117" />
                  <Point X="21.680671875" Y="-0.146815429688" />
                  <Point X="21.68656640625" Y="-0.132221694946" />
                  <Point X="21.695818359375" Y="-0.102408096313" />
                  <Point X="21.699013671875" Y="-0.088484802246" />
                  <Point X="21.70328125" Y="-0.060322250366" />
                  <Point X="21.704353515625" Y="-0.046082839966" />
                  <Point X="21.7043515625" Y="-0.016445501328" />
                  <Point X="21.70327734375" Y="-0.002206386566" />
                  <Point X="21.6990078125" Y="0.02594427681" />
                  <Point X="21.6958125" Y="0.039855976105" />
                  <Point X="21.686564453125" Y="0.069656204224" />
                  <Point X="21.680677734375" Y="0.084238342285" />
                  <Point X="21.66657421875" Y="0.112235771179" />
                  <Point X="21.658357421875" Y="0.125651069641" />
                  <Point X="21.63911328125" Y="0.151911102295" />
                  <Point X="21.628791015625" Y="0.163790390015" />
                  <Point X="21.606328125" Y="0.185682540894" />
                  <Point X="21.5941875" Y="0.195695404053" />
                  <Point X="21.577236328125" Y="0.207459503174" />
                  <Point X="21.561509765625" Y="0.217273666382" />
                  <Point X="21.535138671875" Y="0.231992538452" />
                  <Point X="21.5245546875" Y="0.237069198608" />
                  <Point X="21.502841796875" Y="0.245878677368" />
                  <Point X="21.491712890625" Y="0.249611190796" />
                  <Point X="20.91413671875" Y="0.4043722229" />
                  <Point X="20.2145546875" Y="0.591824829102" />
                  <Point X="20.236681640625" Y="0.74135534668" />
                  <Point X="20.26866796875" Y="0.957523010254" />
                  <Point X="20.360451171875" Y="1.296231933594" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="20.57235546875" Y="1.291124267578" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815429688" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.204703369141" />
                  <Point X="21.28485546875" Y="1.20658972168" />
                  <Point X="21.295109375" Y="1.208053344727" />
                  <Point X="21.3153984375" Y="1.212088989258" />
                  <Point X="21.32543359375" Y="1.214660766602" />
                  <Point X="21.362947265625" Y="1.226489135742" />
                  <Point X="21.386859375" Y="1.234028320312" />
                  <Point X="21.396552734375" Y="1.237677001953" />
                  <Point X="21.415486328125" Y="1.246008300781" />
                  <Point X="21.424724609375" Y="1.250690429688" />
                  <Point X="21.44346875" Y="1.261512573242" />
                  <Point X="21.452146484375" Y="1.267174438477" />
                  <Point X="21.46882421875" Y="1.279404174805" />
                  <Point X="21.4840703125" Y="1.293375732422" />
                  <Point X="21.49770703125" Y="1.308925170898" />
                  <Point X="21.504099609375" Y="1.317070922852" />
                  <Point X="21.516515625" Y="1.334801391602" />
                  <Point X="21.521984375" Y="1.343593139648" />
                  <Point X="21.5319375" Y="1.361731689453" />
                  <Point X="21.536421875" Y="1.371078735352" />
                  <Point X="21.551474609375" Y="1.407420410156" />
                  <Point X="21.561068359375" Y="1.430583129883" />
                  <Point X="21.564505859375" Y="1.440362915039" />
                  <Point X="21.570294921875" Y="1.460231933594" />
                  <Point X="21.572646484375" Y="1.470321289062" />
                  <Point X="21.57640234375" Y="1.491637207031" />
                  <Point X="21.577640625" Y="1.50192175293" />
                  <Point X="21.578990234375" Y="1.522555541992" />
                  <Point X="21.578087890625" Y="1.543203125" />
                  <Point X="21.574943359375" Y="1.563640258789" />
                  <Point X="21.5728125" Y="1.573778930664" />
                  <Point X="21.5672109375" Y="1.594686035156" />
                  <Point X="21.56398828125" Y="1.604529296875" />
                  <Point X="21.5564921875" Y="1.623807128906" />
                  <Point X="21.55221875" Y="1.63324206543" />
                  <Point X="21.534056640625" Y="1.668133300781" />
                  <Point X="21.522478515625" Y="1.690371826172" />
                  <Point X="21.517201171875" Y="1.699287231445" />
                  <Point X="21.505705078125" Y="1.71649230957" />
                  <Point X="21.499486328125" Y="1.724781738281" />
                  <Point X="21.48557421875" Y="1.741359619141" />
                  <Point X="21.4784921875" Y="1.748919189453" />
                  <Point X="21.463552734375" Y="1.76321887207" />
                  <Point X="21.4556953125" Y="1.769958862305" />
                  <Point X="21.124396484375" Y="2.024173706055" />
                  <Point X="20.77238671875" Y="2.294280761719" />
                  <Point X="20.873416015625" Y="2.467367919922" />
                  <Point X="20.99771875" Y="2.680324951172" />
                  <Point X="21.240828125" Y="2.992812011719" />
                  <Point X="21.273662109375" Y="3.035013916016" />
                  <Point X="21.346890625" Y="2.992734619141" />
                  <Point X="21.74584375" Y="2.762398681641" />
                  <Point X="21.755083984375" Y="2.757716064453" />
                  <Point X="21.774017578125" Y="2.749385253906" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.89717578125" Y="2.726586914062" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.9615078125" Y="2.723785644531" />
                  <Point X="21.9718359375" Y="2.724575683594" />
                  <Point X="21.99329296875" Y="2.727400634766" />
                  <Point X="22.00347265625" Y="2.729310302734" />
                  <Point X="22.023564453125" Y="2.7342265625" />
                  <Point X="22.043001953125" Y="2.741301269531" />
                  <Point X="22.061552734375" Y="2.750449462891" />
                  <Point X="22.070578125" Y="2.755529541016" />
                  <Point X="22.08883203125" Y="2.767158203125" />
                  <Point X="22.097251953125" Y="2.773191650391" />
                  <Point X="22.113384765625" Y="2.786139404297" />
                  <Point X="22.12109765625" Y="2.793053710938" />
                  <Point X="22.15818359375" Y="2.830139648438" />
                  <Point X="22.181822265625" Y="2.853777099609" />
                  <Point X="22.188732421875" Y="2.861485351562" />
                  <Point X="22.20167578125" Y="2.877610595703" />
                  <Point X="22.207708984375" Y="2.886027587891" />
                  <Point X="22.21933984375" Y="2.90428125" />
                  <Point X="22.224419921875" Y="2.913307861328" />
                  <Point X="22.2335703125" Y="2.931859863281" />
                  <Point X="22.240646484375" Y="2.951297607422" />
                  <Point X="22.245564453125" Y="2.971390625" />
                  <Point X="22.2474765625" Y="2.981571289062" />
                  <Point X="22.250302734375" Y="3.003029541016" />
                  <Point X="22.251091796875" Y="3.013362792969" />
                  <Point X="22.25154296875" Y="3.034049316406" />
                  <Point X="22.251205078125" Y="3.044402587891" />
                  <Point X="22.2466328125" Y="3.096650878906" />
                  <Point X="22.243720703125" Y="3.129952148438" />
                  <Point X="22.242255859375" Y="3.140211181641" />
                  <Point X="22.23821875" Y="3.160499023438" />
                  <Point X="22.235646484375" Y="3.170527832031" />
                  <Point X="22.22913671875" Y="3.191172119141" />
                  <Point X="22.22548828125" Y="3.200865478516" />
                  <Point X="22.217158203125" Y="3.219795898438" />
                  <Point X="22.2124765625" Y="3.229032958984" />
                  <Point X="22.06566796875" Y="3.483312988281" />
                  <Point X="21.94061328125" Y="3.699914550781" />
                  <Point X="22.13515625" Y="3.849068603516" />
                  <Point X="22.351634765625" Y="4.015041992188" />
                  <Point X="22.734529296875" Y="4.227770019531" />
                  <Point X="22.807474609375" Y="4.268296875" />
                  <Point X="22.881435546875" Y="4.171909179688" />
                  <Point X="22.8881796875" Y="4.164048828125" />
                  <Point X="22.902482421875" Y="4.149107421875" />
                  <Point X="22.910041015625" Y="4.142026367188" />
                  <Point X="22.92662109375" Y="4.128113769531" />
                  <Point X="22.93491015625" Y="4.121895996094" />
                  <Point X="22.952111328125" Y="4.110403320312" />
                  <Point X="22.9610234375" Y="4.105128417969" />
                  <Point X="23.01917578125" Y="4.074856445312" />
                  <Point X="23.056240234375" Y="4.055561523438" />
                  <Point X="23.06567578125" Y="4.051286621094" />
                  <Point X="23.084955078125" Y="4.043789550781" />
                  <Point X="23.094798828125" Y="4.040567382812" />
                  <Point X="23.115705078125" Y="4.034965820313" />
                  <Point X="23.12583984375" Y="4.032834472656" />
                  <Point X="23.14628125" Y="4.029688476562" />
                  <Point X="23.166943359375" Y="4.028786132812" />
                  <Point X="23.18758203125" Y="4.030138183594" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.229271484375" Y="4.037488525391" />
                  <Point X="23.2491328125" Y="4.043277587891" />
                  <Point X="23.258904296875" Y="4.046714111328" />
                  <Point X="23.31947265625" Y="4.071802978516" />
                  <Point X="23.358078125" Y="4.087793701172" />
                  <Point X="23.3674140625" Y="4.092271240234" />
                  <Point X="23.385548828125" Y="4.102219726562" />
                  <Point X="23.39434765625" Y="4.107690429687" />
                  <Point X="23.412076171875" Y="4.120104003906" />
                  <Point X="23.420224609375" Y="4.126499023438" />
                  <Point X="23.435775390625" Y="4.14013671875" />
                  <Point X="23.449748046875" Y="4.15538671875" />
                  <Point X="23.461978515625" Y="4.172066894531" />
                  <Point X="23.467638671875" Y="4.180739746094" />
                  <Point X="23.4784609375" Y="4.199483398437" />
                  <Point X="23.48314453125" Y="4.208725097656" />
                  <Point X="23.4914765625" Y="4.227661621094" />
                  <Point X="23.495125" Y="4.237356445312" />
                  <Point X="23.514837890625" Y="4.299881835937" />
                  <Point X="23.527404296875" Y="4.339732910156" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370048339844" />
                  <Point X="23.535474609375" Y="4.380302246094" />
                  <Point X="23.537361328125" Y="4.401864746094" />
                  <Point X="23.53769921875" Y="4.412217285156" />
                  <Point X="23.537248046875" Y="4.4328984375" />
                  <Point X="23.536458984375" Y="4.443227050781" />
                  <Point X="23.520734375" Y="4.562655273438" />
                  <Point X="23.78858984375" Y="4.637752929688" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.53301171875" Y="4.770646484375" />
                  <Point X="24.63477734375" Y="4.782556152344" />
                  <Point X="24.656310546875" Y="4.702190917969" />
                  <Point X="24.77433203125" Y="4.261728515625" />
                  <Point X="24.779564453125" Y="4.247106933594" />
                  <Point X="24.79233984375" Y="4.218912597656" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218911621094" />
                  <Point X="25.232748046875" Y="4.247104980469" />
                  <Point X="25.23798046875" Y="4.2617265625" />
                  <Point X="25.313205078125" Y="4.542463378906" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.5831796875" Y="4.763538574219" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.211921875" Y="4.645191894531" />
                  <Point X="26.45359375" Y="4.586845214844" />
                  <Point X="26.7020234375" Y="4.496737304688" />
                  <Point X="26.858265625" Y="4.440067382812" />
                  <Point X="27.099978515625" Y="4.327026855469" />
                  <Point X="27.250453125" Y="4.256653808594" />
                  <Point X="27.484015625" Y="4.120580078125" />
                  <Point X="27.629419921875" Y="4.035865478516" />
                  <Point X="27.81778125" Y="3.901915283203" />
                  <Point X="27.5915390625" Y="3.510050292969" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.062380859375" Y="2.593116699219" />
                  <Point X="27.05318359375" Y="2.573442871094" />
                  <Point X="27.04418359375" Y="2.549567626953" />
                  <Point X="27.041302734375" Y="2.540602050781" />
                  <Point X="27.028189453125" Y="2.491568359375" />
                  <Point X="27.01983203125" Y="2.460315673828" />
                  <Point X="27.0179140625" Y="2.451469726562" />
                  <Point X="27.013646484375" Y="2.420223144531" />
                  <Point X="27.012755859375" Y="2.383240234375" />
                  <Point X="27.013412109375" Y="2.369578857422" />
                  <Point X="27.018525390625" Y="2.327178710938" />
                  <Point X="27.021783203125" Y="2.300154052734" />
                  <Point X="27.023802734375" Y="2.289031494141" />
                  <Point X="27.029146484375" Y="2.267107666016" />
                  <Point X="27.032470703125" Y="2.256306396484" />
                  <Point X="27.04073828125" Y="2.234215332031" />
                  <Point X="27.0453203125" Y="2.223888916016" />
                  <Point X="27.055681640625" Y="2.203844970703" />
                  <Point X="27.0614609375" Y="2.194127441406" />
                  <Point X="27.087697265625" Y="2.155462646484" />
                  <Point X="27.104419921875" Y="2.130818847656" />
                  <Point X="27.109810546875" Y="2.1236328125" />
                  <Point X="27.130462890625" Y="2.100124023438" />
                  <Point X="27.15759765625" Y="2.075387207031" />
                  <Point X="27.1682578125" Y="2.066982177734" />
                  <Point X="27.206921875" Y="2.04074621582" />
                  <Point X="27.23156640625" Y="2.024024414062" />
                  <Point X="27.24128125" Y="2.018244995117" />
                  <Point X="27.261328125" Y="2.007881469727" />
                  <Point X="27.27166015625" Y="2.003297485352" />
                  <Point X="27.293748046875" Y="1.995031860352" />
                  <Point X="27.304548828125" Y="1.991707397461" />
                  <Point X="27.326470703125" Y="1.986365112305" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.3799921875" Y="1.979234008789" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.41604296875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975496948242" />
                  <Point X="27.484314453125" Y="1.979822509766" />
                  <Point X="27.497748046875" Y="1.982395507812" />
                  <Point X="27.546783203125" Y="1.99550769043" />
                  <Point X="27.57803515625" Y="2.003865234375" />
                  <Point X="27.583998046875" Y="2.005671142578" />
                  <Point X="27.604412109375" Y="2.013069458008" />
                  <Point X="27.627658203125" Y="2.023574829102" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.21696484375" Y="2.363272949219" />
                  <Point X="28.940404296875" Y="2.780949951172" />
                  <Point X="28.95769140625" Y="2.756924316406" />
                  <Point X="29.04396484375" Y="2.637025878906" />
                  <Point X="29.13688671875" Y="2.483471679688" />
                  <Point X="28.86578125" Y="2.275444824219" />
                  <Point X="28.172953125" Y="1.743819824219" />
                  <Point X="28.168142578125" Y="1.739871337891" />
                  <Point X="28.152125" Y="1.725221435547" />
                  <Point X="28.134669921875" Y="1.706604492188" />
                  <Point X="28.12857421875" Y="1.699420776367" />
                  <Point X="28.09328515625" Y="1.65338269043" />
                  <Point X="28.07079296875" Y="1.624039428711" />
                  <Point X="28.065634765625" Y="1.616600097656" />
                  <Point X="28.049736328125" Y="1.589366333008" />
                  <Point X="28.034763671875" Y="1.555544921875" />
                  <Point X="28.03014453125" Y="1.542676391602" />
                  <Point X="28.016998046875" Y="1.495671386719" />
                  <Point X="28.008619140625" Y="1.465711791992" />
                  <Point X="28.006228515625" Y="1.454669555664" />
                  <Point X="28.0027734375" Y="1.432369873047" />
                  <Point X="28.001708984375" Y="1.421112670898" />
                  <Point X="28.000892578125" Y="1.397540649414" />
                  <Point X="28.001173828125" Y="1.386243530273" />
                  <Point X="28.003076171875" Y="1.363758056641" />
                  <Point X="28.004697265625" Y="1.352569702148" />
                  <Point X="28.01548828125" Y="1.300270263672" />
                  <Point X="28.0223671875" Y="1.266936767578" />
                  <Point X="28.02459765625" Y="1.258232788086" />
                  <Point X="28.03468359375" Y="1.228607421875" />
                  <Point X="28.05028515625" Y="1.195372436523" />
                  <Point X="28.056916015625" Y="1.183527954102" />
                  <Point X="28.086265625" Y="1.138916625977" />
                  <Point X="28.10497265625" Y="1.110482543945" />
                  <Point X="28.111736328125" Y="1.101425048828" />
                  <Point X="28.126291015625" Y="1.084178955078" />
                  <Point X="28.13408203125" Y="1.075990966797" />
                  <Point X="28.151326171875" Y="1.059900878906" />
                  <Point X="28.160033203125" Y="1.052696044922" />
                  <Point X="28.1782421875" Y="1.039370605469" />
                  <Point X="28.187744140625" Y="1.033249389648" />
                  <Point X="28.23027734375" Y="1.009306945801" />
                  <Point X="28.25738671875" Y="0.994046936035" />
                  <Point X="28.26548046875" Y="0.989986572266" />
                  <Point X="28.29468359375" Y="0.978082336426" />
                  <Point X="28.330279296875" Y="0.968020690918" />
                  <Point X="28.343671875" Y="0.9652578125" />
                  <Point X="28.4011796875" Y="0.957657226563" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199829102" />
                  <Point X="28.46571875" Y="0.951222900391" />
                  <Point X="28.49121875" Y="0.952032287598" />
                  <Point X="28.50060546875" Y="0.952797241211" />
                  <Point X="29.05244921875" Y="1.02544921875" />
                  <Point X="29.704703125" Y="1.111319702148" />
                  <Point X="29.715634765625" Y="1.066413574219" />
                  <Point X="29.752689453125" Y="0.914205444336" />
                  <Point X="29.783873046875" Y="0.713921264648" />
                  <Point X="29.48598828125" Y="0.634103271484" />
                  <Point X="28.6919921875" Y="0.42135269165" />
                  <Point X="28.686033203125" Y="0.419544830322" />
                  <Point X="28.665625" Y="0.412137023926" />
                  <Point X="28.642380859375" Y="0.401618377686" />
                  <Point X="28.634005859375" Y="0.397316040039" />
                  <Point X="28.5775078125" Y="0.364658538818" />
                  <Point X="28.54149609375" Y="0.343843597412" />
                  <Point X="28.533884765625" Y="0.338946044922" />
                  <Point X="28.508771484375" Y="0.319867462158" />
                  <Point X="28.481994140625" Y="0.294349212646" />
                  <Point X="28.47280078125" Y="0.28422769165" />
                  <Point X="28.438900390625" Y="0.241031936646" />
                  <Point X="28.417294921875" Y="0.213500137329" />
                  <Point X="28.410857421875" Y="0.204210464478" />
                  <Point X="28.399130859375" Y="0.184927536011" />
                  <Point X="28.393841796875" Y="0.17493397522" />
                  <Point X="28.384068359375" Y="0.153469863892" />
                  <Point X="28.380005859375" Y="0.14292729187" />
                  <Point X="28.37316015625" Y="0.12142943573" />
                  <Point X="28.370376953125" Y="0.110474441528" />
                  <Point X="28.359076171875" Y="0.051470855713" />
                  <Point X="28.351875" Y="0.013863910675" />
                  <Point X="28.350603515625" Y="0.004965708733" />
                  <Point X="28.3485859375" Y="-0.026263233185" />
                  <Point X="28.35028125" Y="-0.062939647675" />
                  <Point X="28.351875" Y="-0.076420341492" />
                  <Point X="28.363173828125" Y="-0.135424072266" />
                  <Point X="28.370376953125" Y="-0.17303086853" />
                  <Point X="28.37316015625" Y="-0.183988388062" />
                  <Point X="28.3800078125" Y="-0.205492797852" />
                  <Point X="28.384072265625" Y="-0.216039672852" />
                  <Point X="28.393845703125" Y="-0.237501403809" />
                  <Point X="28.399134765625" Y="-0.247491836548" />
                  <Point X="28.410859375" Y="-0.266771362305" />
                  <Point X="28.417294921875" Y="-0.276060577393" />
                  <Point X="28.4511953125" Y="-0.319256347656" />
                  <Point X="28.47280078125" Y="-0.346788146973" />
                  <Point X="28.478720703125" Y="-0.353634002686" />
                  <Point X="28.501138671875" Y="-0.375802734375" />
                  <Point X="28.53017578125" Y="-0.398723510742" />
                  <Point X="28.54149609375" Y="-0.406404510498" />
                  <Point X="28.59799609375" Y="-0.439062011719" />
                  <Point X="28.634005859375" Y="-0.459876922607" />
                  <Point X="28.639494140625" Y="-0.462813873291" />
                  <Point X="28.659154296875" Y="-0.472014373779" />
                  <Point X="28.68302734375" Y="-0.481027160645" />
                  <Point X="28.6919921875" Y="-0.483912963867" />
                  <Point X="29.198060546875" Y="-0.619513244629" />
                  <Point X="29.784880859375" Y="-0.776751403809" />
                  <Point X="29.782302734375" Y="-0.793848510742" />
                  <Point X="29.761619140625" Y="-0.931034790039" />
                  <Point X="29.727802734375" Y="-1.079219848633" />
                  <Point X="29.35759375" Y="-1.030480834961" />
                  <Point X="28.436783203125" Y="-0.909253601074" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042480469" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.24359375" Y="-0.936778381348" />
                  <Point X="28.17291796875" Y="-0.952140075684" />
                  <Point X="28.157875" Y="-0.95674230957" />
                  <Point X="28.128755859375" Y="-0.968366088867" />
                  <Point X="28.1146796875" Y="-0.975387512207" />
                  <Point X="28.0868515625" Y="-0.992280151367" />
                  <Point X="28.074125" Y="-1.001530578613" />
                  <Point X="28.050375" Y="-1.02200177002" />
                  <Point X="28.0393515625" Y="-1.03322253418" />
                  <Point X="27.972326171875" Y="-1.11383203125" />
                  <Point X="27.929607421875" Y="-1.165210327148" />
                  <Point X="27.921326171875" Y="-1.176847900391" />
                  <Point X="27.906603515625" Y="-1.201234008789" />
                  <Point X="27.900162109375" Y="-1.213982666016" />
                  <Point X="27.8888203125" Y="-1.24136730957" />
                  <Point X="27.88436328125" Y="-1.254934448242" />
                  <Point X="27.877533203125" Y="-1.282581542969" />
                  <Point X="27.87516015625" Y="-1.296661499023" />
                  <Point X="27.8655546875" Y="-1.401055053711" />
                  <Point X="27.859431640625" Y="-1.467591796875" />
                  <Point X="27.859291015625" Y="-1.483315429688" />
                  <Point X="27.861607421875" Y="-1.514580200195" />
                  <Point X="27.864064453125" Y="-1.53012109375" />
                  <Point X="27.871794921875" Y="-1.561742431641" />
                  <Point X="27.87678515625" Y="-1.576665649414" />
                  <Point X="27.889158203125" Y="-1.60548034668" />
                  <Point X="27.896541015625" Y="-1.619371826172" />
                  <Point X="27.957908203125" Y="-1.71482409668" />
                  <Point X="27.997021484375" Y="-1.775662353516" />
                  <Point X="28.00174609375" Y="-1.782358642578" />
                  <Point X="28.019798828125" Y="-1.804454101562" />
                  <Point X="28.0434921875" Y="-1.828121337891" />
                  <Point X="28.052798828125" Y="-1.836277587891" />
                  <Point X="28.522431640625" Y="-2.196640136719" />
                  <Point X="29.087171875" Y="-2.629980957031" />
                  <Point X="29.045482421875" Y="-2.697439453125" />
                  <Point X="29.001275390625" Y="-2.760252197266" />
                  <Point X="28.6685625" Y="-2.568159667969" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.81502734375" Y="-2.079513427734" />
                  <Point X="27.783119140625" Y="-2.069325927734" />
                  <Point X="27.771107421875" Y="-2.066337646484" />
                  <Point X="27.6391328125" Y="-2.042503051758" />
                  <Point X="27.555017578125" Y="-2.027311889648" />
                  <Point X="27.539359375" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403442383" />
                  <Point X="27.492314453125" Y="-2.02650378418" />
                  <Point X="27.460140625" Y="-2.031461914062" />
                  <Point X="27.444845703125" Y="-2.03513659668" />
                  <Point X="27.4150703125" Y="-2.044960083008" />
                  <Point X="27.40058984375" Y="-2.051108886719" />
                  <Point X="27.290951171875" Y="-2.108810546875" />
                  <Point X="27.221072265625" Y="-2.145587890625" />
                  <Point X="27.20896875" Y="-2.153170654297" />
                  <Point X="27.186037109375" Y="-2.170064453125" />
                  <Point X="27.175208984375" Y="-2.179375488281" />
                  <Point X="27.15425" Y="-2.200334960938" />
                  <Point X="27.14494140625" Y="-2.211161132813" />
                  <Point X="27.128048828125" Y="-2.234090332031" />
                  <Point X="27.12046484375" Y="-2.246193359375" />
                  <Point X="27.06276171875" Y="-2.355831542969" />
                  <Point X="27.025984375" Y="-2.425711425781" />
                  <Point X="27.0198359375" Y="-2.440193359375" />
                  <Point X="27.01001171875" Y="-2.469972167969" />
                  <Point X="27.0063359375" Y="-2.485269042969" />
                  <Point X="27.00137890625" Y="-2.517441894531" />
                  <Point X="27.000279296875" Y="-2.533133300781" />
                  <Point X="27.00068359375" Y="-2.564484375" />
                  <Point X="27.0021875" Y="-2.580144042969" />
                  <Point X="27.0260234375" Y="-2.712118164062" />
                  <Point X="27.04121484375" Y="-2.796234375" />
                  <Point X="27.04301953125" Y="-2.804230957031" />
                  <Point X="27.051236328125" Y="-2.83153515625" />
                  <Point X="27.064068359375" Y="-2.862473876953" />
                  <Point X="27.069546875" Y="-2.873578125" />
                  <Point X="27.37133203125" Y="-3.396287841797" />
                  <Point X="27.735892578125" Y="-4.027722900391" />
                  <Point X="27.723755859375" Y="-4.036082763672" />
                  <Point X="27.459734375" Y="-3.692003173828" />
                  <Point X="26.83391796875" Y="-2.876422851562" />
                  <Point X="26.828654296875" Y="-2.870144042969" />
                  <Point X="26.808830078125" Y="-2.849624023438" />
                  <Point X="26.783251953125" Y="-2.828003662109" />
                  <Point X="26.77330078125" Y="-2.820646728516" />
                  <Point X="26.64313671875" Y="-2.736964355469" />
                  <Point X="26.56017578125" Y="-2.683627929688" />
                  <Point X="26.546283203125" Y="-2.676245605469" />
                  <Point X="26.51747265625" Y="-2.663874511719" />
                  <Point X="26.5025546875" Y="-2.658885742188" />
                  <Point X="26.47093359375" Y="-2.651154052734" />
                  <Point X="26.45539453125" Y="-2.648695800781" />
                  <Point X="26.424125" Y="-2.646376953125" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.266041015625" Y="-2.659615966797" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670339111328" />
                  <Point X="26.133578125" Y="-2.677170410156" />
                  <Point X="26.12001171875" Y="-2.681627929688" />
                  <Point X="26.092626953125" Y="-2.692970703125" />
                  <Point X="26.07987890625" Y="-2.699412841797" />
                  <Point X="26.05549609375" Y="-2.714134033203" />
                  <Point X="26.043861328125" Y="-2.722413085938" />
                  <Point X="25.933939453125" Y="-2.813810058594" />
                  <Point X="25.863876953125" Y="-2.872063476562" />
                  <Point X="25.852654296875" Y="-2.883086914062" />
                  <Point X="25.83218359375" Y="-2.906836181641" />
                  <Point X="25.822935546875" Y="-2.919562011719" />
                  <Point X="25.80604296875" Y="-2.947388427734" />
                  <Point X="25.79901953125" Y="-2.961466552734" />
                  <Point X="25.78739453125" Y="-2.990588134766" />
                  <Point X="25.78279296875" Y="-3.005631591797" />
                  <Point X="25.749927734375" Y="-3.156842529297" />
                  <Point X="25.728978515625" Y="-3.253219482422" />
                  <Point X="25.7275859375" Y="-3.261286865234" />
                  <Point X="25.724724609375" Y="-3.289677001953" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520263672" />
                  <Point X="25.811080078125" Y="-3.985139648438" />
                  <Point X="25.83308984375" Y="-4.152326660156" />
                  <Point X="25.655068359375" Y="-3.487936767578" />
                  <Point X="25.652607421875" Y="-3.480120849609" />
                  <Point X="25.642146484375" Y="-3.453579589844" />
                  <Point X="25.6267890625" Y="-3.423815917969" />
                  <Point X="25.62041015625" Y="-3.413209960938" />
                  <Point X="25.520416015625" Y="-3.269136962891" />
                  <Point X="25.456681640625" Y="-3.177309570312" />
                  <Point X="25.446671875" Y="-3.165172363281" />
                  <Point X="25.424787109375" Y="-3.142716308594" />
                  <Point X="25.412912109375" Y="-3.132397460938" />
                  <Point X="25.38665625" Y="-3.113153564453" />
                  <Point X="25.3732421875" Y="-3.104936523438" />
                  <Point X="25.3452421875" Y="-3.090829101562" />
                  <Point X="25.33065625" Y="-3.084938720703" />
                  <Point X="25.175921875" Y="-3.036914306641" />
                  <Point X="25.077296875" Y="-3.006305419922" />
                  <Point X="25.063376953125" Y="-3.003109375" />
                  <Point X="25.035216796875" Y="-2.99883984375" />
                  <Point X="25.0209765625" Y="-2.997766357422" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.998840087891" />
                  <Point X="24.948935546875" Y="-3.003109619141" />
                  <Point X="24.935015625" Y="-3.006305664062" />
                  <Point X="24.78028125" Y="-3.054329833984" />
                  <Point X="24.68165625" Y="-3.084938964844" />
                  <Point X="24.6670703125" Y="-3.090830078125" />
                  <Point X="24.6390703125" Y="-3.104937988281" />
                  <Point X="24.62565625" Y="-3.113154785156" />
                  <Point X="24.599400390625" Y="-3.132398925781" />
                  <Point X="24.587525390625" Y="-3.142716552734" />
                  <Point X="24.565640625" Y="-3.165172119141" />
                  <Point X="24.555630859375" Y="-3.177310058594" />
                  <Point X="24.45563671875" Y="-3.3213828125" />
                  <Point X="24.391904296875" Y="-3.413210449219" />
                  <Point X="24.38753125" Y="-3.420131835938" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213623047" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.201314453125" Y="-4.069881835938" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.394145906082" Y="4.783335672048" />
                  <Point X="25.377187795037" Y="4.78125347783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.658762021689" Y="4.693041884708" />
                  <Point X="23.521945296067" Y="4.553458342225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.814842851198" Y="4.739277328309" />
                  <Point X="25.350669373006" Y="4.682283991606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.683591457104" Y="4.600377122614" />
                  <Point X="23.534346987122" Y="4.459267644997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.082528555482" Y="4.67643156655" />
                  <Point X="25.324150950976" Y="4.583314505383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.708420892519" Y="4.50771236052" />
                  <Point X="23.532677125454" Y="4.363349178347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.345322220885" Y="4.612985137946" />
                  <Point X="25.297632002134" Y="4.484344954475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.733250327933" Y="4.415047598426" />
                  <Point X="23.503543605954" Y="4.264058598529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.870380809586" Y="4.186315982597" />
                  <Point X="22.600205592989" Y="4.15314263726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.569515642557" Y="4.544799195365" />
                  <Point X="25.271112682999" Y="4.385375358101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.758079763348" Y="4.322382836333" />
                  <Point X="23.454867300179" Y="4.162368466279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.966302624748" Y="4.102380267134" />
                  <Point X="22.379053235563" Y="4.030275108742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.766662500388" Y="4.473292352319" />
                  <Point X="25.244593363864" Y="4.286405761727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.787204692321" Y="4.230245494529" />
                  <Point X="23.237966892508" Y="4.040023011545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.151826537971" Y="4.029446305937" />
                  <Point X="22.221420002688" Y="3.915206748041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.94504875626" Y="4.399481996999" />
                  <Point X="25.198892158642" Y="4.185080925892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.85057347885" Y="4.14231276974" />
                  <Point X="22.072775698098" Y="3.801242088953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.10715143247" Y="4.3236722695" />
                  <Point X="21.946335286192" Y="3.690003725078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.266188828684" Y="4.247486172942" />
                  <Point X="21.997937295106" Y="3.600626221666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.401878368962" Y="4.168433320146" />
                  <Point X="22.049539304019" Y="3.511248718253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.537566906491" Y="4.089380344227" />
                  <Point X="22.101141376678" Y="3.421871222668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.666498393365" Y="4.009497706811" />
                  <Point X="22.15274347832" Y="3.332493730641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.78127387274" Y="3.92787693023" />
                  <Point X="22.204345579962" Y="3.243116238614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.777223088215" Y="3.831666123012" />
                  <Point X="22.239954744955" Y="3.151775060883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.27635393792" Y="3.033459758906" />
                  <Point X="21.272040923915" Y="3.032930187375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.717746975526" Y="3.728649941213" />
                  <Point X="22.250076039146" Y="3.057304366128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.413059932887" Y="2.954531711052" />
                  <Point X="21.189711555936" Y="2.92710797866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.658270862837" Y="3.625633759413" />
                  <Point X="22.242951775802" Y="2.960716183163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.549766958312" Y="2.875603789723" />
                  <Point X="21.107383925786" Y="2.821285983323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.598794750147" Y="3.522617577614" />
                  <Point X="22.185569467241" Y="2.857957088184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.686473983738" Y="2.796675868393" />
                  <Point X="21.025056295635" Y="2.715463987986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.539318423858" Y="3.419601369588" />
                  <Point X="22.052282942915" Y="2.745878127402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.896003734252" Y="2.726689453388" />
                  <Point X="20.957521383669" Y="2.611458310056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.47984206789" Y="3.316585157918" />
                  <Point X="20.8973404631" Y="2.508355588731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.420365711921" Y="3.213568946247" />
                  <Point X="20.83716010148" Y="2.405252936036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.360889355953" Y="3.110552734577" />
                  <Point X="20.776980108692" Y="2.302150328628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.301412999985" Y="3.007536522907" />
                  <Point X="20.871708802032" Y="2.218068116226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.241936644017" Y="2.904520311237" />
                  <Point X="20.979238349612" Y="2.135557651091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.182460288049" Y="2.801504099566" />
                  <Point X="21.086767897192" Y="2.053047185956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.122983932081" Y="2.698487887896" />
                  <Point X="21.194297474026" Y="1.970536724413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.063649765314" Y="2.595489134865" />
                  <Point X="21.301827066607" Y="1.888026264804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.973948593358" Y="2.734330904241" />
                  <Point X="28.828785853161" Y="2.716507160926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.029254961727" Y="2.49555255059" />
                  <Point X="21.409356659189" Y="1.805515805194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.037228791311" Y="2.646387302142" />
                  <Point X="28.618225066475" Y="2.594940113771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.01310784932" Y="2.397856501065" />
                  <Point X="21.502174161283" Y="1.721198928014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.092139944048" Y="2.557416110501" />
                  <Point X="28.407664279788" Y="2.473373066616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.021420388954" Y="2.303163719175" />
                  <Point X="21.552910362456" Y="1.631715116779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.111636619279" Y="2.464096567789" />
                  <Point X="28.197103587185" Y="2.351806031012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.051884361411" Y="2.211190791238" />
                  <Point X="21.578266549092" Y="1.539115031602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.963138618043" Y="2.350149872494" />
                  <Point X="27.986543797922" Y="2.230239106325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.110637616215" Y="2.122691350412" />
                  <Point X="21.564913513576" Y="1.441762051581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.459294385202" Y="1.306009092378" />
                  <Point X="20.359789856755" Y="1.293791472545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.81464044302" Y="2.23620315586" />
                  <Point X="27.775984008659" Y="2.108672181638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.209350513292" Y="2.039098336717" />
                  <Point X="21.520088184735" Y="1.340544759844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.83547189114" Y="1.256484448847" />
                  <Point X="20.332960811464" Y="1.194783846581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.666141937157" Y="2.122256398605" />
                  <Point X="21.358000922564" Y="1.224929513111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.211649791566" Y="1.206959853754" />
                  <Point X="20.306131766173" Y="1.095776220617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.517643431295" Y="2.008309641349" />
                  <Point X="20.279302720882" Y="0.996768594652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.369144925432" Y="1.894362884093" />
                  <Point X="20.259961012922" Y="0.898680298115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.220646419569" Y="1.780416126838" />
                  <Point X="20.245536220807" Y="0.80119572293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.106554584858" Y="1.670693977591" />
                  <Point X="20.231111191985" Y="0.703711118682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.039735740953" Y="1.566776221764" />
                  <Point X="20.216685786884" Y="0.606226468232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.009062361371" Y="1.467296570902" />
                  <Point X="20.423324496071" Y="0.535885077986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.002482496189" Y="1.370775231626" />
                  <Point X="20.668282538513" Y="0.470248710249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.020240493518" Y="1.277242206112" />
                  <Point X="20.913240580956" Y="0.404612342511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.055604365858" Y="1.185870910231" />
                  <Point X="21.15819881122" Y="0.338975997835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.115082224829" Y="1.097460439609" />
                  <Point X="21.403157042174" Y="0.273339653244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.220675073084" Y="1.014712177698" />
                  <Point X="21.587592845274" Y="0.200272088924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.707091678425" Y="1.101507754485" />
                  <Point X="28.565174100903" Y="0.961297906142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.4891862878" Y="0.951967775876" />
                  <Point X="21.665423189077" Y="0.114115020094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.729716091036" Y="1.008572249635" />
                  <Point X="21.699516884949" Y="0.022587766152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.752340992022" Y="0.915636804749" />
                  <Point X="21.7013756846" Y="-0.072897435367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.767086963452" Y="0.821733948958" />
                  <Point X="21.667599170976" Y="-0.17275810318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.781709688057" Y="0.727815960359" />
                  <Point X="21.559380253891" Y="-0.281759148814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.222075660424" Y="0.563388108591" />
                  <Point X="20.984634895085" Y="-0.448042438745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.629948132125" Y="0.394970556612" />
                  <Point X="20.325291508795" Y="-0.624713060333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.46909590619" Y="0.279506953261" />
                  <Point X="20.224721127728" Y="-0.732774983831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.39365828131" Y="0.174530944196" />
                  <Point X="20.238173437419" Y="-0.826836681311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.36360709478" Y="0.075127689035" />
                  <Point X="20.25162574711" Y="-0.920898378791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.34883556052" Y="-0.022399460732" />
                  <Point X="20.269530725376" Y="-1.014413357315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.359605620475" Y="-0.116790497068" />
                  <Point X="20.293236622654" Y="-1.107216072546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.381665859233" Y="-0.209795273757" />
                  <Point X="20.316942207526" Y="-1.200018826135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.435243214628" Y="-0.298930235119" />
                  <Point X="21.845945571623" Y="-1.107994252874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.51292405338" Y="-0.385105660861" />
                  <Point X="21.983009283817" Y="-1.186878378575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.643665651877" Y="-0.464766044516" />
                  <Point X="22.039574901271" Y="-1.275646427492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.87276202879" Y="-0.532349979891" />
                  <Point X="22.051108234692" Y="-1.369943745631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.117720625534" Y="-0.597986279569" />
                  <Point X="22.02849241944" Y="-1.468434051995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.362678856096" Y="-0.663622624209" />
                  <Point X="21.952293649916" Y="-1.573503517871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.607636907947" Y="-0.729258990791" />
                  <Point X="21.803886527011" Y="-1.68743905471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.7808161136" Y="-0.803708691486" />
                  <Point X="28.670560170381" Y="-0.940030979964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.058250999176" Y="-1.015213092687" />
                  <Point X="21.655388335973" Y="-1.80138577331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.766113202504" Y="-0.901227415388" />
                  <Point X="29.046737059975" Y="-0.989555699172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.965247227241" Y="-1.122345953405" />
                  <Point X="21.506890144934" Y="-1.91533249191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.746016000004" Y="-0.99940847499" />
                  <Point X="29.422913959588" Y="-1.03908041715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.894896086265" Y="-1.226697420777" />
                  <Point X="21.358391953895" Y="-2.02927921051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.87253831377" Y="-1.325156043475" />
                  <Point X="21.209893762857" Y="-2.143225929111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.863630613917" Y="-1.421963204908" />
                  <Point X="21.061395712445" Y="-2.257172630444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.862126176614" Y="-1.517861360001" />
                  <Point X="20.912897741261" Y="-2.371119322049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.891540649061" Y="-1.609963150336" />
                  <Point X="20.838194308119" Y="-2.476005183704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.947601750011" Y="-1.69879314609" />
                  <Point X="22.519921382576" Y="-2.365228496727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.287642442853" Y="-2.393748764348" />
                  <Point X="20.891379178282" Y="-2.565188336193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.005829964118" Y="-1.787357053808" />
                  <Point X="22.631157279382" Y="-2.4472838794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.077081592196" Y="-2.515315819358" />
                  <Point X="20.944564048446" Y="-2.654371488682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.098889441661" Y="-1.871644220138" />
                  <Point X="22.679114889015" Y="-2.537108858778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.866520741539" Y="-2.636882874368" />
                  <Point X="20.998196348485" Y="-2.74349970369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.20641898915" Y="-1.954154685284" />
                  <Point X="27.577513268093" Y="-2.031374598094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.39566009661" Y="-2.053703359903" />
                  <Point X="22.68690794483" Y="-2.63186542526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.655960526489" Y="-2.758449851335" />
                  <Point X="21.064822508618" Y="-2.831032473292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.313948536638" Y="-2.03666515043" />
                  <Point X="27.847135123151" Y="-2.093982630429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.179130659289" Y="-2.176003265206" />
                  <Point X="22.654197502405" Y="-2.731595195988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.44540053683" Y="-2.880016800627" />
                  <Point X="21.131448675095" Y="-2.918565242115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.421478084127" Y="-2.119175615577" />
                  <Point X="27.98394809019" Y="-2.172897543764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.102050712205" Y="-2.281180926081" />
                  <Point X="22.594720942243" Y="-2.834611432731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.234840547171" Y="-3.00158374992" />
                  <Point X="21.19807488464" Y="-3.00609800565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.52900763613" Y="-2.201686080168" />
                  <Point X="28.120655288808" Y="-2.251825443828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.048196383847" Y="-2.38350683956" />
                  <Point X="22.53524438208" Y="-2.937627669473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.636537257439" Y="-2.28419653625" />
                  <Point X="28.257362487426" Y="-2.330753343892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.006560995854" Y="-2.484332455811" />
                  <Point X="22.475767821918" Y="-3.040643906215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.744066878749" Y="-2.366706992332" />
                  <Point X="28.394069686044" Y="-2.409681243956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.002265040373" Y="-2.580573366237" />
                  <Point X="26.450442908493" Y="-2.648328604397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.095055868541" Y="-2.691964646048" />
                  <Point X="22.416291261756" Y="-3.143660142957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.851596500058" Y="-2.449217448414" />
                  <Point X="28.530776884662" Y="-2.48860914402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.019176869746" Y="-2.674210288112" />
                  <Point X="26.621480380944" Y="-2.723041276862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.942898790523" Y="-2.806360619479" />
                  <Point X="22.356814701593" Y="-3.246676379699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.959126121368" Y="-2.531727904497" />
                  <Point X="28.66748408328" Y="-2.567537044084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.036088128148" Y="-2.767847280094" />
                  <Point X="26.746483940953" Y="-2.803406203054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.825136446547" Y="-2.916533450594" />
                  <Point X="25.08203248052" Y="-3.007775144767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.83094903421" Y="-3.038604315473" />
                  <Point X="22.297338141431" Y="-3.349692616441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.066655742677" Y="-2.614238360579" />
                  <Point X="28.804190848441" Y="-2.64646499737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.063141424308" Y="-2.860238986422" />
                  <Point X="26.842305869266" Y="-2.88735418308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.780154735122" Y="-3.017769943698" />
                  <Point X="25.303004708896" Y="-3.076356600153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.564061099874" Y="-3.167087466719" />
                  <Point X="22.237861581269" Y="-3.452708853183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.033840646686" Y="-2.713980981149" />
                  <Point X="28.940897610155" Y="-2.725392951079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.113533226772" Y="-2.949765084502" />
                  <Point X="26.909425583664" Y="-2.974826351838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.758781324397" Y="-3.116107701968" />
                  <Point X="25.437321589797" Y="-3.155577994328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.489982797071" Y="-3.27189657202" />
                  <Point X="22.178385126324" Y="-3.555725077006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.165135114862" Y="-3.03914260275" />
                  <Point X="26.976545298062" Y="-3.062298520596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.737406695952" Y="-3.214445609754" />
                  <Point X="25.502477391383" Y="-3.243291301259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.417364646501" Y="-3.376526393169" />
                  <Point X="22.118908811821" Y="-3.658741283585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.216737002952" Y="-3.128520120998" />
                  <Point X="27.04366501246" Y="-3.149770689355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.72473617549" Y="-3.311714787464" />
                  <Point X="25.563691297357" Y="-3.331488612112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.360189257294" Y="-3.479260081646" />
                  <Point X="22.059432497317" Y="-3.761757490165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.268338891041" Y="-3.217897639245" />
                  <Point X="27.110784726858" Y="-3.237242858113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.734858079847" Y="-3.406185407301" />
                  <Point X="25.62434636442" Y="-3.419754539754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.333031119864" Y="-3.578308115044" />
                  <Point X="22.053716919368" Y="-3.858172708312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.319940779131" Y="-3.307275157493" />
                  <Point X="27.177904441256" Y="-3.324715026871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.747258736637" Y="-3.500376231521" />
                  <Point X="25.661231750298" Y="-3.510939017264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.306512337202" Y="-3.677277645548" />
                  <Point X="23.346905083197" Y="-3.79510260087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.842950174144" Y="-3.856980483093" />
                  <Point X="22.160928591236" Y="-3.940722203676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.371542668885" Y="-3.396652675536" />
                  <Point X="27.245024155654" Y="-3.41218719563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.759659393427" Y="-3.59456705574" />
                  <Point X="25.686061029164" Y="-3.603603798579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.279993554541" Y="-3.776247176051" />
                  <Point X="23.521837230924" Y="-3.869337067342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.667457850567" Y="-3.974241664404" />
                  <Point X="22.271338292732" Y="-4.022879030377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.423144964698" Y="-3.486030143722" />
                  <Point X="27.312143870053" Y="-3.499659364388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.772060050217" Y="-3.68875787996" />
                  <Point X="25.710890308031" Y="-3.696268579895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.253474771879" Y="-3.875216706554" />
                  <Point X="23.617573720126" Y="-3.953295537972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.55743051873" Y="-4.083464755449" />
                  <Point X="22.400338655432" Y="-4.102753210906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.474747260511" Y="-3.575407611908" />
                  <Point X="27.379263584451" Y="-3.587131533146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.784460707007" Y="-3.78294870418" />
                  <Point X="25.735719586898" Y="-3.78893336121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.226955989217" Y="-3.974186237058" />
                  <Point X="23.71330893106" Y="-4.037254165553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.526349556323" Y="-3.664785080093" />
                  <Point X="27.446383298849" Y="-3.674603701904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.796861363797" Y="-3.877139528399" />
                  <Point X="25.760548865765" Y="-3.881598142526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.200437196634" Y="-4.073155768779" />
                  <Point X="23.787622597288" Y="-4.123843028094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.577951852136" Y="-3.754162548279" />
                  <Point X="27.513503030347" Y="-3.762075868563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.809262020587" Y="-3.971330352619" />
                  <Point X="25.785378144631" Y="-3.974262923841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.173918114053" Y="-4.172125336108" />
                  <Point X="23.82053626974" Y="-4.215515170693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.629554147949" Y="-3.843540016465" />
                  <Point X="27.58062276609" Y="-3.849548034701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.821662120871" Y="-4.065521245169" />
                  <Point X="25.810207423498" Y="-4.066927705156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.147399031473" Y="-4.271094903437" />
                  <Point X="23.839122023784" Y="-4.308946560462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.681156443762" Y="-3.93291748465" />
                  <Point X="27.647742501834" Y="-3.937020200838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.120879948893" Y="-4.370064470766" />
                  <Point X="23.857707010735" Y="-4.402378044419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.732758739574" Y="-4.022294952836" />
                  <Point X="27.714862237577" Y="-4.024492366975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.094360866313" Y="-4.469034038094" />
                  <Point X="23.8762919731" Y="-4.495809531394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.067841783733" Y="-4.568003605423" />
                  <Point X="23.877176398006" Y="-4.591414371089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.041322701153" Y="-4.666973172752" />
                  <Point X="23.864368062152" Y="-4.688700470401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.014803618572" Y="-4.765942740081" />
                  <Point X="24.011937277085" Y="-4.766294682562" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.716189453125" Y="-4.450156738281" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521543945312" />
                  <Point X="25.364326171875" Y="-3.377470947266" />
                  <Point X="25.300591796875" Y="-3.285643554688" />
                  <Point X="25.2743359375" Y="-3.266399658203" />
                  <Point X="25.1196015625" Y="-3.218375244141" />
                  <Point X="25.0209765625" Y="-3.187766357422" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.8366015625" Y="-3.235790771484" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285644042969" />
                  <Point X="24.6117265625" Y="-3.429716796875" />
                  <Point X="24.547994140625" Y="-3.521544433594" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.384841796875" Y="-4.119057617188" />
                  <Point X="24.152255859375" Y="-4.987077148438" />
                  <Point X="24.01495703125" Y="-4.960426757812" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.72062109375" Y="-4.891975585938" />
                  <Point X="23.648412109375" Y="-4.873396484375" />
                  <Point X="23.661400390625" Y="-4.774733398438" />
                  <Point X="23.6908515625" Y="-4.551038574219" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.6533515625" Y="-4.348915527344" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.202628417969" />
                  <Point X="23.4712578125" Y="-4.077693603516" />
                  <Point X="23.38045703125" Y="-3.998063964844" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.161681640625" Y="-3.973370117188" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791259766" />
                  <Point X="22.852572265625" Y="-4.0790625" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.65272265625" Y="-4.271383300781" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.313029296875" Y="-4.272166503906" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.896119140625" Y="-3.976622314453" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.0393125" Y="-3.416602539062" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597593017578" />
                  <Point X="22.486021484375" Y="-2.568763916016" />
                  <Point X="22.468677734375" Y="-2.551418945312" />
                  <Point X="22.43984765625" Y="-2.53719921875" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.906400390625" Y="-2.833251464844" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="20.97170703125" Y="-3.022403320313" />
                  <Point X="20.838302734375" Y="-2.847136962891" />
                  <Point X="20.660466796875" Y="-2.548933837891" />
                  <Point X="20.56898046875" Y="-2.395526611328" />
                  <Point X="21.040716796875" Y="-2.033550537109" />
                  <Point X="21.836212890625" Y="-1.423144897461" />
                  <Point X="21.854177734375" Y="-1.396017456055" />
                  <Point X="21.861884765625" Y="-1.366262084961" />
                  <Point X="21.859673828125" Y="-1.33459375" />
                  <Point X="21.838841796875" Y="-1.310639282227" />
                  <Point X="21.812359375" Y="-1.295052734375" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="21.1468515625" Y="-1.371989013672" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.124787109375" Y="-1.215466186523" />
                  <Point X="20.072607421875" Y="-1.011187805176" />
                  <Point X="20.02555859375" Y="-0.682225097656" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="20.53634765625" Y="-0.371458251953" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.4581015625" Y="-0.121426872253" />
                  <Point X="21.47505078125" Y="-0.109662811279" />
                  <Point X="21.48585546875" Y="-0.102164848328" />
                  <Point X="21.5051015625" Y="-0.075908973694" />
                  <Point X="21.514353515625" Y="-0.046095249176" />
                  <Point X="21.5143515625" Y="-0.016458133698" />
                  <Point X="21.505103515625" Y="0.013342202187" />
                  <Point X="21.485859375" Y="0.039602336884" />
                  <Point X="21.468908203125" Y="0.051366401672" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.8649609375" Y="0.220846435547" />
                  <Point X="20.001814453125" Y="0.45212600708" />
                  <Point X="20.048728515625" Y="0.769167297363" />
                  <Point X="20.08235546875" Y="0.996414489746" />
                  <Point X="20.177064453125" Y="1.345925292969" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.59715625" Y="1.479498779297" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.305810546875" Y="1.407694946289" />
                  <Point X="21.32972265625" Y="1.415234008789" />
                  <Point X="21.348466796875" Y="1.426056274414" />
                  <Point X="21.3608828125" Y="1.443786743164" />
                  <Point X="21.375935546875" Y="1.480128295898" />
                  <Point X="21.385529296875" Y="1.503291137695" />
                  <Point X="21.38928515625" Y="1.524607177734" />
                  <Point X="21.38368359375" Y="1.545514038086" />
                  <Point X="21.365521484375" Y="1.580405273438" />
                  <Point X="21.353943359375" Y="1.602643798828" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="21.008732421875" Y="1.873436645508" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.709322265625" Y="2.563147216797" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="21.0908671875" Y="3.109479980469" />
                  <Point X="21.225330078125" Y="3.282311035156" />
                  <Point X="21.441890625" Y="3.157279296875" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.913734375" Y="2.915863769531" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.9684921875" Y="2.915775146484" />
                  <Point X="21.98674609375" Y="2.927403808594" />
                  <Point X="22.02383203125" Y="2.964489746094" />
                  <Point X="22.047470703125" Y="2.988127197266" />
                  <Point X="22.0591015625" Y="3.006380859375" />
                  <Point X="22.061927734375" Y="3.027839111328" />
                  <Point X="22.05735546875" Y="3.080087402344" />
                  <Point X="22.054443359375" Y="3.113388671875" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.901125" Y="3.388312988281" />
                  <Point X="21.69272265625" Y="3.749276855469" />
                  <Point X="22.01955078125" Y="3.999852294922" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.642255859375" Y="4.393857910156" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.91004296875" Y="4.44673828125" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273660644531" />
                  <Point X="23.10690625" Y="4.243388671875" />
                  <Point X="23.143970703125" Y="4.22409375" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.24676171875" Y="4.247339355469" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.303095703125" Y="4.275743652344" />
                  <Point X="23.31391796875" Y="4.294487304688" />
                  <Point X="23.333630859375" Y="4.357012695312" />
                  <Point X="23.346197265625" Y="4.396863769531" />
                  <Point X="23.348083984375" Y="4.418426269531" />
                  <Point X="23.331392578125" Y="4.545203613281" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.737296875" Y="4.820698242188" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.51092578125" Y="4.959358398438" />
                  <Point X="24.77580078125" Y="4.990357910156" />
                  <Point X="24.839837890625" Y="4.751365722656" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.129677734375" Y="4.591640136719" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.60296875" Y="4.952505371094" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.25651171875" Y="4.829885253906" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.76680859375" Y="4.6753515625" />
                  <Point X="26.9310390625" Y="4.615784179688" />
                  <Point X="27.180466796875" Y="4.499135742188" />
                  <Point X="27.3386953125" Y="4.42513671875" />
                  <Point X="27.57966015625" Y="4.28475" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="27.959783203125" Y="4.034078369141" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.75608203125" Y="3.415050292969" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491514648438" />
                  <Point X="27.211740234375" Y="2.442480957031" />
                  <Point X="27.2033828125" Y="2.411228271484" />
                  <Point X="27.202044921875" Y="2.392327392578" />
                  <Point X="27.207158203125" Y="2.349927246094" />
                  <Point X="27.210416015625" Y="2.322902587891" />
                  <Point X="27.21868359375" Y="2.300811523438" />
                  <Point X="27.244919921875" Y="2.262146728516" />
                  <Point X="27.261642578125" Y="2.237502929688" />
                  <Point X="27.27494140625" Y="2.224203369141" />
                  <Point X="27.31360546875" Y="2.197967529297" />
                  <Point X="27.33825" Y="2.181245849609" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.40273828125" Y="2.167867431641" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.448666015625" Y="2.165946289062" />
                  <Point X="27.497701171875" Y="2.17905859375" />
                  <Point X="27.528953125" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.12196484375" Y="2.527817871094" />
                  <Point X="28.994248046875" Y="3.031430419922" />
                  <Point X="29.11191796875" Y="2.867895507812" />
                  <Point X="29.20259765625" Y="2.741873291016" />
                  <Point X="29.32928515625" Y="2.532519287109" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.9814453125" Y="2.124708007812" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583832885742" />
                  <Point X="28.24408203125" Y="1.537794799805" />
                  <Point X="28.22158984375" Y="1.508451538086" />
                  <Point X="28.21312109375" Y="1.491500610352" />
                  <Point X="28.199974609375" Y="1.444495605469" />
                  <Point X="28.191595703125" Y="1.414536010742" />
                  <Point X="28.190779296875" Y="1.390963867188" />
                  <Point X="28.2015703125" Y="1.338664672852" />
                  <Point X="28.20844921875" Y="1.305331054688" />
                  <Point X="28.215646484375" Y="1.287955078125" />
                  <Point X="28.24499609375" Y="1.24334362793" />
                  <Point X="28.263703125" Y="1.214909667969" />
                  <Point X="28.280947265625" Y="1.198819702148" />
                  <Point X="28.32348046875" Y="1.174877319336" />
                  <Point X="28.35058984375" Y="1.15961730957" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.42607421875" Y="1.146019165039" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.0276484375" Y="1.213823730469" />
                  <Point X="29.8489765625" Y="1.32195324707" />
                  <Point X="29.900244140625" Y="1.111354003906" />
                  <Point X="29.939193359375" Y="0.951366210938" />
                  <Point X="29.979115234375" Y="0.694956787109" />
                  <Point X="29.997859375" Y="0.57455645752" />
                  <Point X="29.5351640625" Y="0.450577270508" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819656372" />
                  <Point X="28.67258984375" Y="0.200162124634" />
                  <Point X="28.636578125" Y="0.179347137451" />
                  <Point X="28.622265625" Y="0.16692578125" />
                  <Point X="28.588365234375" Y="0.123729782104" />
                  <Point X="28.566759765625" Y="0.096198104858" />
                  <Point X="28.556986328125" Y="0.074733894348" />
                  <Point X="28.545685546875" Y="0.015730275154" />
                  <Point X="28.538484375" Y="-0.02187666893" />
                  <Point X="28.538484375" Y="-0.040685844421" />
                  <Point X="28.549783203125" Y="-0.099689460754" />
                  <Point X="28.556986328125" Y="-0.137296401978" />
                  <Point X="28.566759765625" Y="-0.158758178711" />
                  <Point X="28.60066015625" Y="-0.201954025269" />
                  <Point X="28.622265625" Y="-0.229485855103" />
                  <Point X="28.636578125" Y="-0.24190675354" />
                  <Point X="28.693078125" Y="-0.274564300537" />
                  <Point X="28.729087890625" Y="-0.295379119873" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.247236328125" Y="-0.435987335205" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.970177734375" Y="-0.822173095703" />
                  <Point X="29.948431640625" Y="-0.966414001465" />
                  <Point X="29.897283203125" Y="-1.190546020508" />
                  <Point X="29.874546875" Y="-1.290178588867" />
                  <Point X="29.33279296875" Y="-1.21885534668" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.28394921875" Y="-1.122443359375" />
                  <Point X="28.2132734375" Y="-1.137805053711" />
                  <Point X="28.1854453125" Y="-1.154697631836" />
                  <Point X="28.118419921875" Y="-1.235307373047" />
                  <Point X="28.075701171875" Y="-1.286685546875" />
                  <Point X="28.064359375" Y="-1.31407019043" />
                  <Point X="28.05475390625" Y="-1.418463745117" />
                  <Point X="28.048630859375" Y="-1.485000610352" />
                  <Point X="28.056361328125" Y="-1.516621826172" />
                  <Point X="28.117728515625" Y="-1.61207409668" />
                  <Point X="28.156841796875" Y="-1.672912353516" />
                  <Point X="28.168462890625" Y="-1.685540649414" />
                  <Point X="28.638095703125" Y="-2.045903076172" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.265427734375" Y="-2.702956298828" />
                  <Point X="29.204134765625" Y="-2.802138183594" />
                  <Point X="29.0983515625" Y="-2.952441650391" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.5735625" Y="-2.732704589844" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.6053671875" Y="-2.229478271484" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219245117188" />
                  <Point X="27.379439453125" Y="-2.276946777344" />
                  <Point X="27.309560546875" Y="-2.313724121094" />
                  <Point X="27.2886015625" Y="-2.33468359375" />
                  <Point X="27.2308984375" Y="-2.444321777344" />
                  <Point X="27.19412109375" Y="-2.514201660156" />
                  <Point X="27.1891640625" Y="-2.546374511719" />
                  <Point X="27.213" Y="-2.678348632812" />
                  <Point X="27.22819140625" Y="-2.76246484375" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.53587890625" Y="-3.301288330078" />
                  <Point X="27.98667578125" Y="-4.082087890625" />
                  <Point X="27.90803125" Y="-4.138260742187" />
                  <Point X="27.835279296875" Y="-4.190224609375" />
                  <Point X="27.71703125" Y="-4.266766113281" />
                  <Point X="27.67977734375" Y="-4.290879882812" />
                  <Point X="27.30899609375" Y="-3.807667724609" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467529297" />
                  <Point X="26.54038671875" Y="-2.89678515625" />
                  <Point X="26.45742578125" Y="-2.843448730469" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.283451171875" Y="-2.848816650391" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868508789062" />
                  <Point X="26.055412109375" Y="-2.959905761719" />
                  <Point X="25.985349609375" Y="-3.018159179688" />
                  <Point X="25.96845703125" Y="-3.045985595703" />
                  <Point X="25.935591796875" Y="-3.197196533203" />
                  <Point X="25.914642578125" Y="-3.293573486328" />
                  <Point X="25.9139296875" Y="-3.310719970703" />
                  <Point X="25.999455078125" Y="-3.960339355469" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.06315625" Y="-4.9481640625" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.885087890625" Y="-4.983095214844" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#178" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.118021454921" Y="4.795586562198" Z="1.55" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.55" />
                  <Point X="-0.501043024775" Y="5.040299717792" Z="1.55" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.55" />
                  <Point X="-1.282357687583" Y="4.900121759192" Z="1.55" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.55" />
                  <Point X="-1.724336386163" Y="4.56995757033" Z="1.55" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.55" />
                  <Point X="-1.720211036827" Y="4.403329113849" Z="1.55" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.55" />
                  <Point X="-1.778529953687" Y="4.324813330738" Z="1.55" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.55" />
                  <Point X="-1.876163249083" Y="4.319019117196" Z="1.55" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.55" />
                  <Point X="-2.056446899308" Y="4.508456521585" Z="1.55" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.55" />
                  <Point X="-2.388183621896" Y="4.46884544635" Z="1.55" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.55" />
                  <Point X="-3.017028955026" Y="4.070812093102" Z="1.55" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.55" />
                  <Point X="-3.148333406096" Y="3.39459305617" Z="1.55" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.55" />
                  <Point X="-2.998611174216" Y="3.107011645774" Z="1.55" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.55" />
                  <Point X="-3.017677487723" Y="3.031126080164" Z="1.55" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.55" />
                  <Point X="-3.088064819832" Y="2.996953524957" Z="1.55" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.55" />
                  <Point X="-3.539266292127" Y="3.23186054265" Z="1.55" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.55" />
                  <Point X="-3.954751861485" Y="3.171462395404" Z="1.55" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.55" />
                  <Point X="-4.340016845627" Y="2.619632240479" Z="1.55" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.55" />
                  <Point X="-4.027861517212" Y="1.865048931163" Z="1.55" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.55" />
                  <Point X="-3.684985641435" Y="1.588595583434" Z="1.55" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.55" />
                  <Point X="-3.676416802243" Y="1.530541524281" Z="1.55" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.55" />
                  <Point X="-3.71538088746" Y="1.486661014917" Z="1.55" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.55" />
                  <Point X="-4.402475017386" Y="1.56035125137" Z="1.55" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.55" />
                  <Point X="-4.877351399051" Y="1.390282691317" Z="1.55" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.55" />
                  <Point X="-5.006889904506" Y="0.807766176404" Z="1.55" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.55" />
                  <Point X="-4.154137206738" Y="0.203830183435" Z="1.55" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.55" />
                  <Point X="-3.565757546443" Y="0.041571001793" Z="1.55" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.55" />
                  <Point X="-3.545206734462" Y="0.018204184908" Z="1.55" />
                  <Point X="-3.539556741714" Y="0" Z="1.55" />
                  <Point X="-3.543157866673" Y="-0.011602766153" Z="1.55" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.55" />
                  <Point X="-3.559611048681" Y="-0.037304981341" Z="1.55" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.55" />
                  <Point X="-4.482750582034" Y="-0.291881873321" Z="1.55" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.55" />
                  <Point X="-5.030094754285" Y="-0.658024069008" Z="1.55" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.55" />
                  <Point X="-4.929815897462" Y="-1.196560254025" Z="1.55" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.55" />
                  <Point X="-3.85278082431" Y="-1.390281202411" Z="1.55" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.55" />
                  <Point X="-3.208850178109" Y="-1.312930565919" Z="1.55" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.55" />
                  <Point X="-3.195675387082" Y="-1.334029375552" Z="1.55" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.55" />
                  <Point X="-3.995876832783" Y="-1.962602717623" Z="1.55" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.55" />
                  <Point X="-4.38863388554" Y="-2.543263560259" Z="1.55" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.55" />
                  <Point X="-4.074235197089" Y="-3.021406532522" Z="1.55" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.55" />
                  <Point X="-3.074755964804" Y="-2.845272573507" Z="1.55" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.55" />
                  <Point X="-2.566086345136" Y="-2.562244123995" Z="1.55" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.55" />
                  <Point X="-3.010144439661" Y="-3.360322417688" Z="1.55" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.55" />
                  <Point X="-3.14054178508" Y="-3.984959453628" Z="1.55" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.55" />
                  <Point X="-2.719449454302" Y="-4.283397243072" Z="1.55" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.55" />
                  <Point X="-2.313765938117" Y="-4.270541259882" Z="1.55" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.55" />
                  <Point X="-2.125805349915" Y="-4.089355634877" Z="1.55" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.55" />
                  <Point X="-1.847744157858" Y="-3.99198313957" Z="1.55" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.55" />
                  <Point X="-1.56786684341" Y="-4.084005510939" Z="1.55" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.55" />
                  <Point X="-1.401844838139" Y="-4.327390250207" Z="1.55" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.55" />
                  <Point X="-1.394328550946" Y="-4.736927212299" Z="1.55" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.55" />
                  <Point X="-1.297994838808" Y="-4.909118563218" Z="1.55" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.55" />
                  <Point X="-1.000726345569" Y="-4.978230696624" Z="1.55" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.55" />
                  <Point X="-0.573018567347" Y="-4.100718473139" Z="1.55" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.55" />
                  <Point X="-0.35335351798" Y="-3.426945598979" Z="1.55" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.55" />
                  <Point X="-0.154735568874" Y="-3.252263578741" Z="1.55" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.55" />
                  <Point X="0.098623510487" Y="-3.234848466547" Z="1.55" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.55" />
                  <Point X="0.317092341452" Y="-3.374700156789" Z="1.55" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.55" />
                  <Point X="0.661736293054" Y="-4.431817556677" Z="1.55" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.55" />
                  <Point X="0.887868717649" Y="-5.00101005716" Z="1.55" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.55" />
                  <Point X="1.067705040214" Y="-4.965724299135" Z="1.55" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.55" />
                  <Point X="1.042869833251" Y="-3.922533267899" Z="1.55" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.55" />
                  <Point X="0.978293714694" Y="-3.176536780252" Z="1.55" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.55" />
                  <Point X="1.081221163383" Y="-2.967072466998" Z="1.55" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.55" />
                  <Point X="1.281875960835" Y="-2.867326179368" Z="1.55" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.55" />
                  <Point X="1.507191690448" Y="-2.907562997413" Z="1.55" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.55" />
                  <Point X="2.263170898313" Y="-3.806825698108" Z="1.55" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.55" />
                  <Point X="2.738041433093" Y="-4.277460674389" Z="1.55" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.55" />
                  <Point X="2.93093747506" Y="-4.147667598255" Z="1.55" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.55" />
                  <Point X="2.573023641388" Y="-3.245008378544" Z="1.55" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.55" />
                  <Point X="2.256045572873" Y="-2.638182442426" Z="1.55" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.55" />
                  <Point X="2.26898839744" Y="-2.43632833393" Z="1.55" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.55" />
                  <Point X="2.396570044443" Y="-2.289912912527" Z="1.55" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.55" />
                  <Point X="2.590324361087" Y="-2.247402436618" Z="1.55" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.55" />
                  <Point X="3.542405218563" Y="-2.744725858887" Z="1.55" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.55" />
                  <Point X="4.133083261921" Y="-2.949939030082" Z="1.55" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.55" />
                  <Point X="4.301804487935" Y="-2.697961302153" Z="1.55" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.55" />
                  <Point X="3.662376219975" Y="-1.974955456041" Z="1.55" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.55" />
                  <Point X="3.153629477095" Y="-1.553754787591" Z="1.55" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.55" />
                  <Point X="3.098385217061" Y="-1.391765537903" Z="1.55" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.55" />
                  <Point X="3.150710848212" Y="-1.235994066061" Z="1.55" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.55" />
                  <Point X="3.288411945016" Y="-1.140022371516" Z="1.55" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.55" />
                  <Point X="4.320111075638" Y="-1.237147511655" Z="1.55" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.55" />
                  <Point X="4.939872922308" Y="-1.170389692535" Z="1.55" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.55" />
                  <Point X="5.0134616596" Y="-0.79834708072" Z="1.55" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.55" />
                  <Point X="4.254019898193" Y="-0.356410927814" Z="1.55" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.55" />
                  <Point X="3.711941914116" Y="-0.199995757355" Z="1.55" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.55" />
                  <Point X="3.633836243364" Y="-0.139806485476" Z="1.55" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.55" />
                  <Point X="3.5927345666" Y="-0.05900355159" Z="1.55" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.55" />
                  <Point X="3.588636883824" Y="0.037606979612" Z="1.55" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.55" />
                  <Point X="3.621543195037" Y="0.124142253099" Z="1.55" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.55" />
                  <Point X="3.691453500237" Y="0.188153086887" Z="1.55" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.55" />
                  <Point X="4.541947919114" Y="0.433561004955" Z="1.55" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.55" />
                  <Point X="5.022361765904" Y="0.733928586454" Z="1.55" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.55" />
                  <Point X="4.942669791251" Y="1.154460949136" Z="1.55" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.55" />
                  <Point X="4.014966667806" Y="1.294675911422" Z="1.55" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.55" />
                  <Point X="3.42646802802" Y="1.226868274631" Z="1.55" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.55" />
                  <Point X="3.341851485414" Y="1.249728673451" Z="1.55" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.55" />
                  <Point X="3.280611486515" Y="1.302104948285" Z="1.55" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.55" />
                  <Point X="3.244383108912" Y="1.380050150967" Z="1.55" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.55" />
                  <Point X="3.241970528283" Y="1.462308695524" Z="1.55" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.55" />
                  <Point X="3.277608492453" Y="1.538656949862" Z="1.55" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.55" />
                  <Point X="4.005725716726" Y="2.116320365526" Z="1.55" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.55" />
                  <Point X="4.365905962201" Y="2.58968546636" Z="1.55" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.55" />
                  <Point X="4.146347771355" Y="2.928378884458" Z="1.55" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.55" />
                  <Point X="3.090808647623" Y="2.602399260608" Z="1.55" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.55" />
                  <Point X="2.478625386154" Y="2.258641433115" Z="1.55" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.55" />
                  <Point X="2.402567070001" Y="2.248787839295" Z="1.55" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.55" />
                  <Point X="2.335522998684" Y="2.270622343624" Z="1.55" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.55" />
                  <Point X="2.280136256611" Y="2.321501861698" Z="1.55" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.55" />
                  <Point X="2.250641863464" Y="2.387191373832" Z="1.55" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.55" />
                  <Point X="2.253886509543" Y="2.460844189727" Z="1.55" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.55" />
                  <Point X="2.793226092252" Y="3.421330434332" Z="1.55" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.55" />
                  <Point X="2.982602554955" Y="4.106105384685" Z="1.55" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.55" />
                  <Point X="2.598673966999" Y="4.359232747806" Z="1.55" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.55" />
                  <Point X="2.195490571222" Y="4.575707326365" Z="1.55" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.55" />
                  <Point X="1.777701298091" Y="4.753635544382" Z="1.55" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.55" />
                  <Point X="1.262089428417" Y="4.909768926881" Z="1.55" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.55" />
                  <Point X="0.602018932247" Y="5.033513438341" Z="1.55" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.55" />
                  <Point X="0.075223106632" Y="4.635860943753" Z="1.55" />
                  <Point X="0" Y="4.355124473572" Z="1.55" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>