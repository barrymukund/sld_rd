<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#157" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1610" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999525390625" Y="0.004715820312" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.721212890625" Y="-4.101850585938" />
                  <Point X="25.5633046875" Y="-3.512524414063" />
                  <Point X="25.55772265625" Y="-3.497139648438" />
                  <Point X="25.54236328125" Y="-3.467376708984" />
                  <Point X="25.4778203125" Y="-3.374384521484" />
                  <Point X="25.378634765625" Y="-3.231476318359" />
                  <Point X="25.356751953125" Y="-3.209021484375" />
                  <Point X="25.33049609375" Y="-3.18977734375" />
                  <Point X="25.30249609375" Y="-3.175669433594" />
                  <Point X="25.202623046875" Y="-3.144672119141" />
                  <Point X="25.04913671875" Y="-3.097036132812" />
                  <Point X="25.0209765625" Y="-3.092766601562" />
                  <Point X="24.9913359375" Y="-3.092766601562" />
                  <Point X="24.96317578125" Y="-3.097036132812" />
                  <Point X="24.863302734375" Y="-3.128033203125" />
                  <Point X="24.70981640625" Y="-3.175669433594" />
                  <Point X="24.68181640625" Y="-3.18977734375" />
                  <Point X="24.655560546875" Y="-3.209021484375" />
                  <Point X="24.63367578125" Y="-3.231477294922" />
                  <Point X="24.569134765625" Y="-3.324469238281" />
                  <Point X="24.46994921875" Y="-3.467377685547" />
                  <Point X="24.4618125" Y="-3.481571777344" />
                  <Point X="24.449009765625" Y="-3.512524414063" />
                  <Point X="24.206337890625" Y="-4.418187988281" />
                  <Point X="24.0834140625" Y="-4.87694140625" />
                  <Point X="23.920666015625" Y="-4.845351074219" />
                  <Point X="23.808322265625" Y="-4.816446289062" />
                  <Point X="23.753583984375" Y="-4.802362304688" />
                  <Point X="23.766029296875" Y="-4.707822753906" />
                  <Point X="23.7850390625" Y="-4.563438476562" />
                  <Point X="23.78580078125" Y="-4.5479296875" />
                  <Point X="23.7834921875" Y="-4.51622265625" />
                  <Point X="23.759630859375" Y="-4.396270507812" />
                  <Point X="23.722962890625" Y="-4.211931152344" />
                  <Point X="23.712060546875" Y="-4.182964355469" />
                  <Point X="23.69598828125" Y="-4.155127441406" />
                  <Point X="23.67635546875" Y="-4.131204589844" />
                  <Point X="23.584404296875" Y="-4.050564697266" />
                  <Point X="23.443095703125" Y="-3.926639648438" />
                  <Point X="23.4168125" Y="-3.910295898438" />
                  <Point X="23.387115234375" Y="-3.897994628906" />
                  <Point X="23.35697265625" Y="-3.890966308594" />
                  <Point X="23.234931640625" Y="-3.882967285156" />
                  <Point X="23.0473828125" Y="-3.870674804688" />
                  <Point X="23.01658203125" Y="-3.873708496094" />
                  <Point X="22.985533203125" Y="-3.882028076172" />
                  <Point X="22.957341796875" Y="-3.894801757812" />
                  <Point X="22.85565234375" Y="-3.962749267578" />
                  <Point X="22.699376953125" Y="-4.067169921875" />
                  <Point X="22.68721484375" Y="-4.076822998047" />
                  <Point X="22.6648984375" Y="-4.099461914062" />
                  <Point X="22.52865625" Y="-4.277015136719" />
                  <Point X="22.5198515625" Y="-4.288489257813" />
                  <Point X="22.458177734375" Y="-4.250302246094" />
                  <Point X="22.19828515625" Y="-4.089382568359" />
                  <Point X="22.042736328125" Y="-3.969615478516" />
                  <Point X="21.89527734375" Y="-3.856077392578" />
                  <Point X="22.28278125" Y="-3.184901855469" />
                  <Point X="22.57623828125" Y="-2.676619628906" />
                  <Point X="22.587140625" Y="-2.647653564453" />
                  <Point X="22.593412109375" Y="-2.616125488281" />
                  <Point X="22.594423828125" Y="-2.585189941406" />
                  <Point X="22.585439453125" Y="-2.555570800781" />
                  <Point X="22.571220703125" Y="-2.526740966797" />
                  <Point X="22.553197265625" Y="-2.501588378906" />
                  <Point X="22.53585546875" Y="-2.484245605469" />
                  <Point X="22.510701171875" Y="-2.46621875" />
                  <Point X="22.48187109375" Y="-2.451999023438" />
                  <Point X="22.452251953125" Y="-2.443012451172" />
                  <Point X="22.42131640625" Y="-2.444023925781" />
                  <Point X="22.389787109375" Y="-2.450294433594" />
                  <Point X="22.360818359375" Y="-2.461197265625" />
                  <Point X="21.579701171875" Y="-2.912175537109" />
                  <Point X="21.1819765625" Y="-3.141801513672" />
                  <Point X="21.122462890625" Y="-3.063613525391" />
                  <Point X="20.917142578125" Y="-2.793861816406" />
                  <Point X="20.80562109375" Y="-2.606860595703" />
                  <Point X="20.693857421875" Y="-2.419450195312" />
                  <Point X="21.380591796875" Y="-1.892500976562" />
                  <Point X="21.894044921875" Y="-1.498513427734" />
                  <Point X="21.915421875" Y="-1.475594726562" />
                  <Point X="21.93338671875" Y="-1.44846484375" />
                  <Point X="21.946142578125" Y="-1.419835449219" />
                  <Point X="21.95384765625" Y="-1.390088256836" />
                  <Point X="21.95665234375" Y="-1.359663574219" />
                  <Point X="21.9544453125" Y="-1.32799230957" />
                  <Point X="21.9474453125" Y="-1.298243164062" />
                  <Point X="21.931361328125" Y="-1.272256469727" />
                  <Point X="21.91052734375" Y="-1.248299438477" />
                  <Point X="21.887029296875" Y="-1.228767211914" />
                  <Point X="21.860546875" Y="-1.213180786133" />
                  <Point X="21.831283203125" Y="-1.201956787109" />
                  <Point X="21.799396484375" Y="-1.195474975586" />
                  <Point X="21.768072265625" Y="-1.194383911133" />
                  <Point X="20.781986328125" Y="-1.324204589844" />
                  <Point X="20.267900390625" Y="-1.391885253906" />
                  <Point X="20.246228515625" Y="-1.307041503906" />
                  <Point X="20.165923828125" Y="-0.992650634766" />
                  <Point X="20.13641796875" Y="-0.786359375" />
                  <Point X="20.107576171875" Y="-0.584698425293" />
                  <Point X="20.88222265625" Y="-0.377132415771" />
                  <Point X="21.467125" Y="-0.220408279419" />
                  <Point X="21.48887890625" Y="-0.211575897217" />
                  <Point X="21.515388671875" Y="-0.196762069702" />
                  <Point X="21.52321484375" Y="-0.191875930786" />
                  <Point X="21.54002734375" Y="-0.180207199097" />
                  <Point X="21.563984375" Y="-0.15867741394" />
                  <Point X="21.585328125" Y="-0.12728276062" />
                  <Point X="21.596400390625" Y="-0.100794281006" />
                  <Point X="21.60115234375" Y="-0.086218460083" />
                  <Point X="21.609001953125" Y="-0.05334249115" />
                  <Point X="21.61159765625" Y="-0.032045944214" />
                  <Point X="21.609345703125" Y="-0.010710348129" />
                  <Point X="21.603453125" Y="0.015858580589" />
                  <Point X="21.598994140625" Y="0.030364578247" />
                  <Point X="21.58596484375" Y="0.06316009903" />
                  <Point X="21.574705078125" Y="0.083687049866" />
                  <Point X="21.558767578125" Y="0.100837043762" />
                  <Point X="21.5360078125" Y="0.119948516846" />
                  <Point X="21.5290859375" Y="0.125239776611" />
                  <Point X="21.5122734375" Y="0.136908813477" />
                  <Point X="21.498076171875" Y="0.145046905518" />
                  <Point X="21.467125" Y="0.157848297119" />
                  <Point X="20.56826171875" Y="0.398698150635" />
                  <Point X="20.10818359375" Y="0.521975585938" />
                  <Point X="20.1237578125" Y="0.627225830078" />
                  <Point X="20.17551171875" Y="0.976968200684" />
                  <Point X="20.23490625" Y="1.196154296875" />
                  <Point X="20.29644921875" Y="1.423268066406" />
                  <Point X="20.8150625" Y="1.354991210938" />
                  <Point X="21.2343359375" Y="1.29979296875" />
                  <Point X="21.255015625" Y="1.299341796875" />
                  <Point X="21.276576171875" Y="1.301228149414" />
                  <Point X="21.296865234375" Y="1.305263793945" />
                  <Point X="21.321078125" Y="1.3128984375" />
                  <Point X="21.358291015625" Y="1.324631225586" />
                  <Point X="21.37722265625" Y="1.332961547852" />
                  <Point X="21.395966796875" Y="1.343783447266" />
                  <Point X="21.4126484375" Y="1.356014892578" />
                  <Point X="21.426287109375" Y="1.371567138672" />
                  <Point X="21.438701171875" Y="1.38929675293" />
                  <Point X="21.448650390625" Y="1.407434692383" />
                  <Point X="21.458365234375" Y="1.430891357422" />
                  <Point X="21.473296875" Y="1.466939086914" />
                  <Point X="21.479083984375" Y="1.486788208008" />
                  <Point X="21.48284375" Y="1.508104125977" />
                  <Point X="21.484197265625" Y="1.528750244141" />
                  <Point X="21.481048828125" Y="1.549199829102" />
                  <Point X="21.4754453125" Y="1.570106811523" />
                  <Point X="21.467951171875" Y="1.589376220703" />
                  <Point X="21.456228515625" Y="1.611896972656" />
                  <Point X="21.4382109375" Y="1.646506103516" />
                  <Point X="21.426716796875" Y="1.663709228516" />
                  <Point X="21.4128046875" Y="1.680288330078" />
                  <Point X="21.39786328125" Y="1.694590332031" />
                  <Point X="20.8822734375" Y="2.090216796875" />
                  <Point X="20.648140625" Y="2.269873046875" />
                  <Point X="20.717751953125" Y="2.3891328125" />
                  <Point X="20.918853515625" Y="2.733666015625" />
                  <Point X="21.07617578125" Y="2.935883056641" />
                  <Point X="21.24949609375" Y="3.158661865234" />
                  <Point X="21.535837890625" Y="2.993341796875" />
                  <Point X="21.79334375" Y="2.844671142578" />
                  <Point X="21.81227734375" Y="2.836340087891" />
                  <Point X="21.832919921875" Y="2.829831542969" />
                  <Point X="21.85320703125" Y="2.825796386719" />
                  <Point X="21.886931640625" Y="2.822845947266" />
                  <Point X="21.938755859375" Y="2.818311767578" />
                  <Point X="21.95943359375" Y="2.818762939453" />
                  <Point X="21.980892578125" Y="2.821587890625" />
                  <Point X="22.000984375" Y="2.826504394531" />
                  <Point X="22.019537109375" Y="2.835653320313" />
                  <Point X="22.037791015625" Y="2.847282470703" />
                  <Point X="22.053921875" Y="2.860228759766" />
                  <Point X="22.077859375" Y="2.884166015625" />
                  <Point X="22.114646484375" Y="2.920952148438" />
                  <Point X="22.127595703125" Y="2.937084472656" />
                  <Point X="22.139224609375" Y="2.955338378906" />
                  <Point X="22.14837109375" Y="2.973887695312" />
                  <Point X="22.1532890625" Y="2.993977050781" />
                  <Point X="22.156115234375" Y="3.015435791016" />
                  <Point X="22.15656640625" Y="3.036122070312" />
                  <Point X="22.153615234375" Y="3.069845703125" />
                  <Point X="22.14908203125" Y="3.121671630859" />
                  <Point X="22.145044921875" Y="3.141961181641" />
                  <Point X="22.13853515625" Y="3.162604248047" />
                  <Point X="22.130205078125" Y="3.181533203125" />
                  <Point X="21.90173046875" Y="3.577261474609" />
                  <Point X="21.81666796875" Y="3.724595947266" />
                  <Point X="21.9491328125" Y="3.826156494141" />
                  <Point X="22.29937890625" Y="4.094686035156" />
                  <Point X="22.54716015625" Y="4.232347167969" />
                  <Point X="22.83296484375" Y="4.391133789062" />
                  <Point X="22.877974609375" Y="4.332474609375" />
                  <Point X="22.9568046875" Y="4.229741210938" />
                  <Point X="22.971109375" Y="4.214798339844" />
                  <Point X="22.987689453125" Y="4.200886230469" />
                  <Point X="23.004888671875" Y="4.18939453125" />
                  <Point X="23.042423828125" Y="4.16985546875" />
                  <Point X="23.10010546875" Y="4.139827636719" />
                  <Point X="23.1193828125" Y="4.132331542969" />
                  <Point X="23.1402890625" Y="4.126729492187" />
                  <Point X="23.160732421875" Y="4.123582519531" />
                  <Point X="23.18137109375" Y="4.124935058594" />
                  <Point X="23.2026875" Y="4.128693359375" />
                  <Point X="23.222548828125" Y="4.134482421875" />
                  <Point X="23.261642578125" Y="4.150676269531" />
                  <Point X="23.32172265625" Y="4.175562011719" />
                  <Point X="23.33985546875" Y="4.185509765625" />
                  <Point X="23.3575859375" Y="4.197924316406" />
                  <Point X="23.373140625" Y="4.211565429688" />
                  <Point X="23.385373046875" Y="4.22825" />
                  <Point X="23.396193359375" Y="4.246994140625" />
                  <Point X="23.404521484375" Y="4.265921386719" />
                  <Point X="23.41724609375" Y="4.306278320312" />
                  <Point X="23.43680078125" Y="4.368297851562" />
                  <Point X="23.4408359375" Y="4.388583496094" />
                  <Point X="23.44272265625" Y="4.410144042969" />
                  <Point X="23.442271484375" Y="4.430826660156" />
                  <Point X="23.416294921875" Y="4.628125976562" />
                  <Point X="23.415798828125" Y="4.631897949219" />
                  <Point X="23.59695703125" Y="4.682688476562" />
                  <Point X="24.050365234375" Y="4.809808105469" />
                  <Point X="24.35075" Y="4.844963867188" />
                  <Point X="24.7052890625" Y="4.886457519531" />
                  <Point X="24.789919921875" Y="4.570612792969" />
                  <Point X="24.866095703125" Y="4.286315429688" />
                  <Point X="24.87887109375" Y="4.258121582031" />
                  <Point X="24.89673046875" Y="4.231395019531" />
                  <Point X="24.91788671875" Y="4.208807617188" />
                  <Point X="24.945181640625" Y="4.19421875" />
                  <Point X="24.975619140625" Y="4.183886230469" />
                  <Point X="25.00615625" Y="4.178844238281" />
                  <Point X="25.036693359375" Y="4.183886230469" />
                  <Point X="25.067130859375" Y="4.19421875" />
                  <Point X="25.09442578125" Y="4.208807617188" />
                  <Point X="25.11558203125" Y="4.231395019531" />
                  <Point X="25.13344140625" Y="4.258121582031" />
                  <Point X="25.146216796875" Y="4.286315429688" />
                  <Point X="25.26328515625" Y="4.723217285156" />
                  <Point X="25.307421875" Y="4.8879375" />
                  <Point X="25.44814453125" Y="4.873200195312" />
                  <Point X="25.84404296875" Y="4.831738769531" />
                  <Point X="26.092564453125" Y="4.77173828125" />
                  <Point X="26.4810234375" Y="4.677952148438" />
                  <Point X="26.641892578125" Y="4.619603515625" />
                  <Point X="26.894650390625" Y="4.527926269531" />
                  <Point X="27.05106640625" Y="4.454775878906" />
                  <Point X="27.294576171875" Y="4.340893554688" />
                  <Point X="27.44571484375" Y="4.252840820312" />
                  <Point X="27.68097265625" Y="4.115777832031" />
                  <Point X="27.823498046875" Y="4.014423828125" />
                  <Point X="27.94326171875" Y="3.92925390625" />
                  <Point X="27.48723828125" Y="3.139396484375" />
                  <Point X="27.147583984375" Y="2.551097900391" />
                  <Point X="27.142078125" Y="2.539933105469" />
                  <Point X="27.133080078125" Y="2.516060302734" />
                  <Point X="27.124615234375" Y="2.484411376953" />
                  <Point X="27.111609375" Y="2.435773925781" />
                  <Point X="27.108619140625" Y="2.417935791016" />
                  <Point X="27.107728515625" Y="2.380951660156" />
                  <Point X="27.111029296875" Y="2.353584472656" />
                  <Point X="27.116099609375" Y="2.311526855469" />
                  <Point X="27.12144140625" Y="2.289609130859" />
                  <Point X="27.12970703125" Y="2.26751953125" />
                  <Point X="27.140072265625" Y="2.247468017578" />
                  <Point X="27.1570078125" Y="2.22251171875" />
                  <Point X="27.18303125" Y="2.184159423828" />
                  <Point X="27.19446484375" Y="2.170329101562" />
                  <Point X="27.22159765625" Y="2.145593505859" />
                  <Point X="27.246552734375" Y="2.128659423828" />
                  <Point X="27.28490625" Y="2.102635986328" />
                  <Point X="27.304955078125" Y="2.092271728516" />
                  <Point X="27.32704296875" Y="2.084006103516" />
                  <Point X="27.34896484375" Y="2.078663574219" />
                  <Point X="27.37633203125" Y="2.075363525391" />
                  <Point X="27.418388671875" Y="2.070291992188" />
                  <Point X="27.436466796875" Y="2.069845458984" />
                  <Point X="27.47320703125" Y="2.074170898438" />
                  <Point X="27.504857421875" Y="2.082634277344" />
                  <Point X="27.553494140625" Y="2.095640625" />
                  <Point X="27.5652890625" Y="2.099639648438" />
                  <Point X="27.58853515625" Y="2.110145019531" />
                  <Point X="28.492619140625" Y="2.632118408203" />
                  <Point X="28.967328125" Y="2.906191650391" />
                  <Point X="28.98372265625" Y="2.883406738281" />
                  <Point X="29.12326953125" Y="2.689467285156" />
                  <Point X="29.202724609375" Y="2.558166992188" />
                  <Point X="29.26219921875" Y="2.459883544922" />
                  <Point X="28.67797265625" Y="2.01158984375" />
                  <Point X="28.23078515625" Y="1.668451416016" />
                  <Point X="28.221427734375" Y="1.660243896484" />
                  <Point X="28.20397265625" Y="1.641626708984" />
                  <Point X="28.1811953125" Y="1.611911254883" />
                  <Point X="28.14619140625" Y="1.566245361328" />
                  <Point X="28.136607421875" Y="1.550912353516" />
                  <Point X="28.121630859375" Y="1.517086669922" />
                  <Point X="28.113146484375" Y="1.486747070312" />
                  <Point X="28.10010546875" Y="1.440122436523" />
                  <Point X="28.09665234375" Y="1.417825805664" />
                  <Point X="28.0958359375" Y="1.394254272461" />
                  <Point X="28.097740234375" Y="1.371764160156" />
                  <Point X="28.10470703125" Y="1.338007568359" />
                  <Point X="28.11541015625" Y="1.286131347656" />
                  <Point X="28.120681640625" Y="1.268973999023" />
                  <Point X="28.136283203125" Y="1.2357421875" />
                  <Point X="28.1552265625" Y="1.206947631836" />
                  <Point X="28.18433984375" Y="1.162696655273" />
                  <Point X="28.198890625" Y="1.145452880859" />
                  <Point X="28.2161328125" Y="1.12936340332" />
                  <Point X="28.23434765625" Y="1.116034667969" />
                  <Point X="28.26180078125" Y="1.100581054688" />
                  <Point X="28.303990234375" Y="1.076832397461" />
                  <Point X="28.3205234375" Y="1.069500976563" />
                  <Point X="28.356119140625" Y="1.059438720703" />
                  <Point X="28.39323828125" Y="1.054532958984" />
                  <Point X="28.45028125" Y="1.046994018555" />
                  <Point X="28.462705078125" Y="1.046175048828" />
                  <Point X="28.488205078125" Y="1.04698449707" />
                  <Point X="29.3470234375" Y="1.160050415039" />
                  <Point X="29.77683984375" Y="1.216636352539" />
                  <Point X="29.785998046875" Y="1.179011352539" />
                  <Point X="29.84594140625" Y="0.932787475586" />
                  <Point X="29.870974609375" Y="0.771997070312" />
                  <Point X="29.890865234375" Y="0.644238647461" />
                  <Point X="29.22906640625" Y="0.466909851074" />
                  <Point X="28.716580078125" Y="0.329589813232" />
                  <Point X="28.704791015625" Y="0.32558605957" />
                  <Point X="28.681546875" Y="0.315067749023" />
                  <Point X="28.645080078125" Y="0.293988769531" />
                  <Point X="28.589037109375" Y="0.261595184326" />
                  <Point X="28.5743125" Y="0.251096755981" />
                  <Point X="28.54753125" Y="0.22557699585" />
                  <Point X="28.525650390625" Y="0.197696136475" />
                  <Point X="28.492025390625" Y="0.154849365234" />
                  <Point X="28.48030078125" Y="0.135568344116" />
                  <Point X="28.47052734375" Y="0.114105194092" />
                  <Point X="28.463681640625" Y="0.092603179932" />
                  <Point X="28.456388671875" Y="0.054519130707" />
                  <Point X="28.4451796875" Y="-0.004007438183" />
                  <Point X="28.443484375" Y="-0.021874984741" />
                  <Point X="28.4451796875" Y="-0.058552696228" />
                  <Point X="28.45247265625" Y="-0.09663659668" />
                  <Point X="28.463681640625" Y="-0.155163162231" />
                  <Point X="28.47052734375" Y="-0.176665176392" />
                  <Point X="28.48030078125" Y="-0.198128326416" />
                  <Point X="28.492025390625" Y="-0.217409347534" />
                  <Point X="28.51390625" Y="-0.245290359497" />
                  <Point X="28.54753125" Y="-0.288137145996" />
                  <Point X="28.56" Y="-0.301236175537" />
                  <Point X="28.589037109375" Y="-0.324155303955" />
                  <Point X="28.62550390625" Y="-0.345234130859" />
                  <Point X="28.681546875" Y="-0.377627716064" />
                  <Point X="28.6927109375" Y="-0.383138763428" />
                  <Point X="28.716580078125" Y="-0.392149810791" />
                  <Point X="29.50415625" Y="-0.603180419922" />
                  <Point X="29.891474609375" Y="-0.706961914062" />
                  <Point X="29.888490234375" Y="-0.726753295898" />
                  <Point X="29.855025390625" Y="-0.948723815918" />
                  <Point X="29.822947265625" Y="-1.089293457031" />
                  <Point X="29.80117578125" Y="-1.18469909668" />
                  <Point X="29.01872265625" Y="-1.081687133789" />
                  <Point X="28.4243828125" Y="-1.003440856934" />
                  <Point X="28.40803515625" Y="-1.002710266113" />
                  <Point X="28.37466015625" Y="-1.005508666992" />
                  <Point X="28.303087890625" Y="-1.021065368652" />
                  <Point X="28.193095703125" Y="-1.04497253418" />
                  <Point X="28.1639765625" Y="-1.056596069336" />
                  <Point X="28.1361484375" Y="-1.07348840332" />
                  <Point X="28.1123984375" Y="-1.093959838867" />
                  <Point X="28.06913671875" Y="-1.145989624023" />
                  <Point X="28.002654296875" Y="-1.225947875977" />
                  <Point X="27.9879296875" Y="-1.250335327148" />
                  <Point X="27.976587890625" Y="-1.277721069336" />
                  <Point X="27.969759765625" Y="-1.305364868164" />
                  <Point X="27.96355859375" Y="-1.372745849609" />
                  <Point X="27.95403125" Y="-1.476295288086" />
                  <Point X="27.956349609375" Y="-1.50756237793" />
                  <Point X="27.964080078125" Y="-1.539182739258" />
                  <Point X="27.976451171875" Y="-1.567996337891" />
                  <Point X="28.016060546875" Y="-1.629606445312" />
                  <Point X="28.076931640625" Y="-1.724286987305" />
                  <Point X="28.0869375" Y="-1.73724206543" />
                  <Point X="28.110630859375" Y="-1.760909301758" />
                  <Point X="28.841505859375" Y="-2.321730224609" />
                  <Point X="29.213123046875" Y="-2.6068828125" />
                  <Point X="29.124802734375" Y="-2.749796630859" />
                  <Point X="29.05847265625" Y="-2.844044189453" />
                  <Point X="29.028982421875" Y="-2.8859453125" />
                  <Point X="28.330294921875" Y="-2.482557617188" />
                  <Point X="27.800955078125" Y="-2.176943115234" />
                  <Point X="27.7861328125" Y="-2.170012695312" />
                  <Point X="27.7542265625" Y="-2.159825195312" />
                  <Point X="27.669044921875" Y="-2.14444140625" />
                  <Point X="27.53813671875" Y="-2.120799560547" />
                  <Point X="27.506783203125" Y="-2.120395263672" />
                  <Point X="27.474609375" Y="-2.125353027344" />
                  <Point X="27.444833984375" Y="-2.135177001953" />
                  <Point X="27.374068359375" Y="-2.172420654297" />
                  <Point X="27.26531640625" Y="-2.229656005859" />
                  <Point X="27.242384765625" Y="-2.246549560547" />
                  <Point X="27.22142578125" Y="-2.267509033203" />
                  <Point X="27.204533203125" Y="-2.290438476562" />
                  <Point X="27.1672890625" Y="-2.361204589844" />
                  <Point X="27.110052734375" Y="-2.469956542969" />
                  <Point X="27.100228515625" Y="-2.499735107422" />
                  <Point X="27.095271484375" Y="-2.531908447266" />
                  <Point X="27.09567578125" Y="-2.563259521484" />
                  <Point X="27.111060546875" Y="-2.648442382812" />
                  <Point X="27.134703125" Y="-2.779349853516" />
                  <Point X="27.13898828125" Y="-2.795140136719" />
                  <Point X="27.151822265625" Y="-2.826078613281" />
                  <Point X="27.621482421875" Y="-3.639555664062" />
                  <Point X="27.861287109375" Y="-4.054906494141" />
                  <Point X="27.78186328125" Y="-4.111635742188" />
                  <Point X="27.707681640625" Y="-4.159652832031" />
                  <Point X="27.701767578125" Y="-4.163481445312" />
                  <Point X="27.162482421875" Y="-3.460672851563" />
                  <Point X="26.758548828125" Y="-2.934255126953" />
                  <Point X="26.74750390625" Y="-2.922178222656" />
                  <Point X="26.72192578125" Y="-2.900557128906" />
                  <Point X="26.637912109375" Y="-2.846544189453" />
                  <Point X="26.50880078125" Y="-2.763538330078" />
                  <Point X="26.47998828125" Y="-2.751166748047" />
                  <Point X="26.4483671875" Y="-2.743435302734" />
                  <Point X="26.417099609375" Y="-2.741116699219" />
                  <Point X="26.325216796875" Y="-2.749571777344" />
                  <Point X="26.184013671875" Y="-2.762565673828" />
                  <Point X="26.15636328125" Y="-2.769397216797" />
                  <Point X="26.128978515625" Y="-2.780740234375" />
                  <Point X="26.10459765625" Y="-2.795461181641" />
                  <Point X="26.0336484375" Y="-2.854453369141" />
                  <Point X="25.92461328125" Y="-2.945111572266" />
                  <Point X="25.904142578125" Y="-2.968860839844" />
                  <Point X="25.88725" Y="-2.996687255859" />
                  <Point X="25.875625" Y="-3.025809082031" />
                  <Point X="25.854412109375" Y="-3.123408447266" />
                  <Point X="25.821810546875" Y="-3.273396972656" />
                  <Point X="25.819724609375" Y="-3.289627197266" />
                  <Point X="25.8197421875" Y="-3.323120117188" />
                  <Point X="25.952841796875" Y="-4.334103027344" />
                  <Point X="26.02206640625" Y="-4.859915527344" />
                  <Point X="25.975681640625" Y="-4.870083007812" />
                  <Point X="25.92931640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="23.94157421875" Y="-4.75263671875" />
                  <Point X="23.85875390625" Y="-4.731328125" />
                  <Point X="23.860216796875" Y="-4.720221679688" />
                  <Point X="23.8792265625" Y="-4.575837402344" />
                  <Point X="23.879923828125" Y="-4.568098632812" />
                  <Point X="23.88055078125" Y="-4.541030761719" />
                  <Point X="23.8782421875" Y="-4.509323730469" />
                  <Point X="23.876666015625" Y="-4.497687988281" />
                  <Point X="23.8528046875" Y="-4.377735839844" />
                  <Point X="23.81613671875" Y="-4.193396484375" />
                  <Point X="23.811873046875" Y="-4.178467285156" />
                  <Point X="23.800970703125" Y="-4.149500488281" />
                  <Point X="23.79433203125" Y="-4.135462890625" />
                  <Point X="23.778259765625" Y="-4.107625976562" />
                  <Point X="23.769423828125" Y="-4.094860351562" />
                  <Point X="23.749791015625" Y="-4.0709375" />
                  <Point X="23.738994140625" Y="-4.059780029297" />
                  <Point X="23.64704296875" Y="-3.979140136719" />
                  <Point X="23.505734375" Y="-3.855215087891" />
                  <Point X="23.49326171875" Y="-3.845965087891" />
                  <Point X="23.466978515625" Y="-3.829621337891" />
                  <Point X="23.45316796875" Y="-3.822527587891" />
                  <Point X="23.423470703125" Y="-3.810226318359" />
                  <Point X="23.4086875" Y="-3.805476318359" />
                  <Point X="23.378544921875" Y="-3.798447998047" />
                  <Point X="23.363185546875" Y="-3.796169677734" />
                  <Point X="23.24114453125" Y="-3.788170654297" />
                  <Point X="23.053595703125" Y="-3.775878173828" />
                  <Point X="23.0380703125" Y="-3.776132324219" />
                  <Point X="23.00726953125" Y="-3.779166015625" />
                  <Point X="22.991994140625" Y="-3.781945556641" />
                  <Point X="22.9609453125" Y="-3.790265136719" />
                  <Point X="22.946326171875" Y="-3.795496337891" />
                  <Point X="22.918134765625" Y="-3.808270019531" />
                  <Point X="22.9045625" Y="-3.8158125" />
                  <Point X="22.802873046875" Y="-3.883760009766" />
                  <Point X="22.64659765625" Y="-3.988180664062" />
                  <Point X="22.64031640625" Y="-3.992759277344" />
                  <Point X="22.61955859375" Y="-4.010131591797" />
                  <Point X="22.5972421875" Y="-4.032770507812" />
                  <Point X="22.589529296875" Y="-4.041629394531" />
                  <Point X="22.496798828125" Y="-4.162478515625" />
                  <Point X="22.25240625" Y="-4.011156494141" />
                  <Point X="22.100693359375" Y="-3.894343017578" />
                  <Point X="22.01913671875" Y="-3.831546630859" />
                  <Point X="22.365052734375" Y="-3.232401855469" />
                  <Point X="22.658509765625" Y="-2.724119628906" />
                  <Point X="22.6651484375" Y="-2.710084228516" />
                  <Point X="22.67605078125" Y="-2.681118164062" />
                  <Point X="22.680314453125" Y="-2.6661875" />
                  <Point X="22.6865859375" Y="-2.634659423828" />
                  <Point X="22.688361328125" Y="-2.619230712891" />
                  <Point X="22.689373046875" Y="-2.588295166016" />
                  <Point X="22.685333984375" Y="-2.557614257812" />
                  <Point X="22.676349609375" Y="-2.527995117188" />
                  <Point X="22.670640625" Y="-2.513550048828" />
                  <Point X="22.656421875" Y="-2.484720214844" />
                  <Point X="22.64844140625" Y="-2.471406982422" />
                  <Point X="22.63041796875" Y="-2.446254394531" />
                  <Point X="22.620375" Y="-2.434415039062" />
                  <Point X="22.603033203125" Y="-2.417072265625" />
                  <Point X="22.591193359375" Y="-2.40702734375" />
                  <Point X="22.5660390625" Y="-2.389000488281" />
                  <Point X="22.552724609375" Y="-2.381018554688" />
                  <Point X="22.52389453125" Y="-2.366798828125" />
                  <Point X="22.509453125" Y="-2.361091064453" />
                  <Point X="22.479833984375" Y="-2.352104492188" />
                  <Point X="22.4491484375" Y="-2.348063232422" />
                  <Point X="22.418212890625" Y="-2.349074707031" />
                  <Point X="22.40278515625" Y="-2.350848632812" />
                  <Point X="22.371255859375" Y="-2.357119140625" />
                  <Point X="22.35632421875" Y="-2.361383056641" />
                  <Point X="22.32735546875" Y="-2.372285888672" />
                  <Point X="22.313318359375" Y="-2.378924804688" />
                  <Point X="21.532201171875" Y="-2.829903076172" />
                  <Point X="21.206912109375" Y="-3.017708740234" />
                  <Point X="21.198056640625" Y="-3.006074951172" />
                  <Point X="20.995982421875" Y="-2.740587158203" />
                  <Point X="20.887212890625" Y="-2.558201660156" />
                  <Point X="20.818734375" Y="-2.443373535156" />
                  <Point X="21.438423828125" Y="-1.967869628906" />
                  <Point X="21.951876953125" Y="-1.573882080078" />
                  <Point X="21.963515625" Y="-1.563311279297" />
                  <Point X="21.984892578125" Y="-1.540392578125" />
                  <Point X="21.994630859375" Y="-1.528045043945" />
                  <Point X="22.012595703125" Y="-1.500915039063" />
                  <Point X="22.0201640625" Y="-1.487128173828" />
                  <Point X="22.032919921875" Y="-1.458498779297" />
                  <Point X="22.038107421875" Y="-1.44365612793" />
                  <Point X="22.0458125" Y="-1.413908935547" />
                  <Point X="22.048447265625" Y="-1.398808837891" />
                  <Point X="22.051251953125" Y="-1.368384155273" />
                  <Point X="22.051421875" Y="-1.353059448242" />
                  <Point X="22.04921484375" Y="-1.321388183594" />
                  <Point X="22.046919921875" Y="-1.306233032227" />
                  <Point X="22.039919921875" Y="-1.276483886719" />
                  <Point X="22.028224609375" Y="-1.24824621582" />
                  <Point X="22.012140625" Y="-1.222259521484" />
                  <Point X="22.003046875" Y="-1.209916503906" />
                  <Point X="21.982212890625" Y="-1.185959472656" />
                  <Point X="21.97125390625" Y="-1.175242797852" />
                  <Point X="21.947755859375" Y="-1.155710571289" />
                  <Point X="21.93521484375" Y="-1.146895141602" />
                  <Point X="21.908732421875" Y="-1.13130859375" />
                  <Point X="21.89456640625" Y="-1.124481323242" />
                  <Point X="21.865302734375" Y="-1.113257202148" />
                  <Point X="21.85020703125" Y="-1.108860717773" />
                  <Point X="21.8183203125" Y="-1.10237890625" />
                  <Point X="21.802703125" Y="-1.100532592773" />
                  <Point X="21.77137890625" Y="-1.09944152832" />
                  <Point X="21.755671875" Y="-1.100196777344" />
                  <Point X="20.7695859375" Y="-1.230017333984" />
                  <Point X="20.33908203125" Y="-1.286694335938" />
                  <Point X="20.3382734375" Y="-1.283530273438" />
                  <Point X="20.259240234375" Y="-0.974114135742" />
                  <Point X="20.2304609375" Y="-0.77290838623" />
                  <Point X="20.213548828125" Y="-0.654654541016" />
                  <Point X="20.906810546875" Y="-0.46889541626" />
                  <Point X="21.491712890625" Y="-0.312171203613" />
                  <Point X="21.50286328125" Y="-0.308429901123" />
                  <Point X="21.5246171875" Y="-0.299597412109" />
                  <Point X="21.535220703125" Y="-0.294506164551" />
                  <Point X="21.56173046875" Y="-0.279692321777" />
                  <Point X="21.577380859375" Y="-0.269920532227" />
                  <Point X="21.594193359375" Y="-0.258251678467" />
                  <Point X="21.60352734375" Y="-0.250866317749" />
                  <Point X="21.627484375" Y="-0.229336654663" />
                  <Point X="21.642546875" Y="-0.21208895874" />
                  <Point X="21.663890625" Y="-0.18069430542" />
                  <Point X="21.672978515625" Y="-0.163921020508" />
                  <Point X="21.68405078125" Y="-0.137432540894" />
                  <Point X="21.686720703125" Y="-0.130240524292" />
                  <Point X="21.6935546875" Y="-0.108280899048" />
                  <Point X="21.701404296875" Y="-0.075404914856" />
                  <Point X="21.7033046875" Y="-0.064836334229" />
                  <Point X="21.705900390625" Y="-0.043539855957" />
                  <Point X="21.706072265625" Y="-0.022074098587" />
                  <Point X="21.7038203125" Y="-0.000738534689" />
                  <Point X="21.702091796875" Y="0.009859320641" />
                  <Point X="21.69619921875" Y="0.036428344727" />
                  <Point X="21.694259765625" Y="0.043771507263" />
                  <Point X="21.68728125" Y="0.065440284729" />
                  <Point X="21.674251953125" Y="0.098235717773" />
                  <Point X="21.669255859375" Y="0.108848731995" />
                  <Point X="21.65799609375" Y="0.129375640869" />
                  <Point X="21.644294921875" Y="0.148357162476" />
                  <Point X="21.628357421875" Y="0.165507217407" />
                  <Point X="21.619857421875" Y="0.173589630127" />
                  <Point X="21.59709765625" Y="0.192701049805" />
                  <Point X="21.58325390625" Y="0.203283599854" />
                  <Point X="21.56644140625" Y="0.214952728271" />
                  <Point X="21.559517578125" Y="0.219328323364" />
                  <Point X="21.534384765625" Y="0.232834442139" />
                  <Point X="21.50343359375" Y="0.245635925293" />
                  <Point X="21.491712890625" Y="0.249611297607" />
                  <Point X="20.592849609375" Y="0.490461151123" />
                  <Point X="20.2145546875" Y="0.591825073242" />
                  <Point X="20.217734375" Y="0.613319702148" />
                  <Point X="20.26866796875" Y="0.957521179199" />
                  <Point X="20.326599609375" Y="1.171307495117" />
                  <Point X="20.3664140625" Y="1.318237060547" />
                  <Point X="20.802662109375" Y="1.260803955078" />
                  <Point X="21.221935546875" Y="1.205605712891" />
                  <Point X="21.232263671875" Y="1.204815673828" />
                  <Point X="21.252943359375" Y="1.204364379883" />
                  <Point X="21.263294921875" Y="1.204703369141" />
                  <Point X="21.28485546875" Y="1.20658972168" />
                  <Point X="21.295109375" Y="1.208053466797" />
                  <Point X="21.3153984375" Y="1.212089111328" />
                  <Point X="21.32543359375" Y="1.214661010742" />
                  <Point X="21.349646484375" Y="1.222295654297" />
                  <Point X="21.386859375" Y="1.234028442383" />
                  <Point X="21.396552734375" Y="1.237677001953" />
                  <Point X="21.415484375" Y="1.246007324219" />
                  <Point X="21.42472265625" Y="1.250689086914" />
                  <Point X="21.443466796875" Y="1.261510986328" />
                  <Point X="21.452140625" Y="1.267171142578" />
                  <Point X="21.468822265625" Y="1.279402587891" />
                  <Point X="21.48407421875" Y="1.293377807617" />
                  <Point X="21.497712890625" Y="1.308930053711" />
                  <Point X="21.504107421875" Y="1.317078369141" />
                  <Point X="21.516521484375" Y="1.334807983398" />
                  <Point X="21.521994140625" Y="1.343608276367" />
                  <Point X="21.531943359375" Y="1.36174621582" />
                  <Point X="21.536419921875" Y="1.371083618164" />
                  <Point X="21.546134765625" Y="1.394540283203" />
                  <Point X="21.56106640625" Y="1.430588012695" />
                  <Point X="21.5645" Y="1.440348510742" />
                  <Point X="21.570287109375" Y="1.460197631836" />
                  <Point X="21.572640625" Y="1.470286499023" />
                  <Point X="21.576400390625" Y="1.491602416992" />
                  <Point X="21.577640625" Y="1.501889526367" />
                  <Point X="21.578994140625" Y="1.522535644531" />
                  <Point X="21.578091796875" Y="1.543206054688" />
                  <Point X="21.574943359375" Y="1.563655761719" />
                  <Point X="21.572810546875" Y="1.573793945312" />
                  <Point X="21.56720703125" Y="1.594700805664" />
                  <Point X="21.563984375" Y="1.604541137695" />
                  <Point X="21.556490234375" Y="1.623810546875" />
                  <Point X="21.55221875" Y="1.633239624023" />
                  <Point X="21.54049609375" Y="1.655760375977" />
                  <Point X="21.522478515625" Y="1.690369506836" />
                  <Point X="21.517201171875" Y="1.699283325195" />
                  <Point X="21.50570703125" Y="1.716486450195" />
                  <Point X="21.499490234375" Y="1.724775634766" />
                  <Point X="21.485578125" Y="1.741354858398" />
                  <Point X="21.47849609375" Y="1.748915893555" />
                  <Point X="21.4635546875" Y="1.763217773438" />
                  <Point X="21.4556953125" Y="1.769958862305" />
                  <Point X="20.94010546875" Y="2.165585449219" />
                  <Point X="20.77238671875" Y="2.294280273438" />
                  <Point X="20.799798828125" Y="2.341242919922" />
                  <Point X="20.99771875" Y="2.680324951172" />
                  <Point X="21.15115625" Y="2.877548828125" />
                  <Point X="21.273662109375" Y="3.035012939453" />
                  <Point X="21.488337890625" Y="2.911069335938" />
                  <Point X="21.74584375" Y="2.762398681641" />
                  <Point X="21.75508203125" Y="2.757716796875" />
                  <Point X="21.774015625" Y="2.749385742188" />
                  <Point X="21.7837109375" Y="2.745736816406" />
                  <Point X="21.804353515625" Y="2.739228271484" />
                  <Point X="21.81438671875" Y="2.736656738281" />
                  <Point X="21.834673828125" Y="2.732621582031" />
                  <Point X="21.844927734375" Y="2.731157958984" />
                  <Point X="21.87865234375" Y="2.728207519531" />
                  <Point X="21.9304765625" Y="2.723673339844" />
                  <Point X="21.940828125" Y="2.723334472656" />
                  <Point X="21.961505859375" Y="2.723785644531" />
                  <Point X="21.97183203125" Y="2.724575683594" />
                  <Point X="21.993291015625" Y="2.727400634766" />
                  <Point X="22.00347265625" Y="2.729310546875" />
                  <Point X="22.023564453125" Y="2.734227050781" />
                  <Point X="22.043" Y="2.741301025391" />
                  <Point X="22.061552734375" Y="2.750449951172" />
                  <Point X="22.070580078125" Y="2.755531494141" />
                  <Point X="22.088833984375" Y="2.767160644531" />
                  <Point X="22.09725390625" Y="2.773193115234" />
                  <Point X="22.113384765625" Y="2.786139404297" />
                  <Point X="22.121095703125" Y="2.793053222656" />
                  <Point X="22.145033203125" Y="2.816990478516" />
                  <Point X="22.1818203125" Y="2.853776611328" />
                  <Point X="22.188732421875" Y="2.861484863281" />
                  <Point X="22.201681640625" Y="2.8776171875" />
                  <Point X="22.20771875" Y="2.886041259766" />
                  <Point X="22.21934765625" Y="2.904295166016" />
                  <Point X="22.2244296875" Y="2.913324707031" />
                  <Point X="22.233576171875" Y="2.931874023438" />
                  <Point X="22.240646484375" Y="2.951298339844" />
                  <Point X="22.245564453125" Y="2.971387695312" />
                  <Point X="22.2474765625" Y="2.981572509766" />
                  <Point X="22.250302734375" Y="3.00303125" />
                  <Point X="22.251091796875" Y="3.013364257812" />
                  <Point X="22.25154296875" Y="3.034050537109" />
                  <Point X="22.251205078125" Y="3.044403808594" />
                  <Point X="22.24825390625" Y="3.078127441406" />
                  <Point X="22.243720703125" Y="3.129953369141" />
                  <Point X="22.242255859375" Y="3.140210693359" />
                  <Point X="22.23821875" Y="3.160500244141" />
                  <Point X="22.235646484375" Y="3.170532470703" />
                  <Point X="22.22913671875" Y="3.191175537109" />
                  <Point X="22.22548828125" Y="3.200869628906" />
                  <Point X="22.217158203125" Y="3.219798583984" />
                  <Point X="22.2124765625" Y="3.229033447266" />
                  <Point X="21.984001953125" Y="3.62476171875" />
                  <Point X="21.94061328125" Y="3.699915527344" />
                  <Point X="22.006935546875" Y="3.750765136719" />
                  <Point X="22.3516328125" Y="4.015040039062" />
                  <Point X="22.593296875" Y="4.149302734375" />
                  <Point X="22.8074765625" Y="4.268295898437" />
                  <Point X="22.881435546875" Y="4.171909667969" />
                  <Point X="22.8881796875" Y="4.164047363281" />
                  <Point X="22.902484375" Y="4.149104492188" />
                  <Point X="22.910044921875" Y="4.1420234375" />
                  <Point X="22.926625" Y="4.128111328125" />
                  <Point X="22.934912109375" Y="4.121895507812" />
                  <Point X="22.952111328125" Y="4.110403808594" />
                  <Point X="22.9610234375" Y="4.105127929688" />
                  <Point X="22.99855859375" Y="4.085588867188" />
                  <Point X="23.056240234375" Y="4.055561035156" />
                  <Point X="23.06567578125" Y="4.051286132812" />
                  <Point X="23.084953125" Y="4.043790039063" />
                  <Point X="23.094794921875" Y="4.040568847656" />
                  <Point X="23.115701171875" Y="4.034966796875" />
                  <Point X="23.1258359375" Y="4.032835449219" />
                  <Point X="23.146279296875" Y="4.029688476562" />
                  <Point X="23.1669453125" Y="4.028785888672" />
                  <Point X="23.187583984375" Y="4.030138427734" />
                  <Point X="23.197865234375" Y="4.031378173828" />
                  <Point X="23.219181640625" Y="4.035136474609" />
                  <Point X="23.229271484375" Y="4.037488525391" />
                  <Point X="23.2491328125" Y="4.043277587891" />
                  <Point X="23.258904296875" Y="4.046714355469" />
                  <Point X="23.297998046875" Y="4.062908203125" />
                  <Point X="23.358078125" Y="4.087793945312" />
                  <Point X="23.367416015625" Y="4.092272460938" />
                  <Point X="23.385548828125" Y="4.102220214844" />
                  <Point X="23.39434375" Y="4.107689453125" />
                  <Point X="23.41207421875" Y="4.120104003906" />
                  <Point X="23.420224609375" Y="4.126499511719" />
                  <Point X="23.435779296875" Y="4.140140625" />
                  <Point X="23.449755859375" Y="4.15539453125" />
                  <Point X="23.46198828125" Y="4.172079101562" />
                  <Point X="23.4676484375" Y="4.180755371094" />
                  <Point X="23.47846875" Y="4.199499511719" />
                  <Point X="23.4831484375" Y="4.208733398438" />
                  <Point X="23.4914765625" Y="4.227660644531" />
                  <Point X="23.495125" Y="4.237354003906" />
                  <Point X="23.507849609375" Y="4.2777109375" />
                  <Point X="23.527404296875" Y="4.33973046875" />
                  <Point X="23.529974609375" Y="4.349763671875" />
                  <Point X="23.534009765625" Y="4.370049316406" />
                  <Point X="23.535474609375" Y="4.380301757812" />
                  <Point X="23.537361328125" Y="4.401862304688" />
                  <Point X="23.53769921875" Y="4.412215820312" />
                  <Point X="23.537248046875" Y="4.4328984375" />
                  <Point X="23.536458984375" Y="4.443227539062" />
                  <Point X="23.520734375" Y="4.562655273438" />
                  <Point X="23.622603515625" Y="4.591215820312" />
                  <Point X="24.068822265625" Y="4.716319824219" />
                  <Point X="24.36179296875" Y="4.750607910156" />
                  <Point X="24.63477734375" Y="4.782556640625" />
                  <Point X="24.69815625" Y="4.546024902344" />
                  <Point X="24.77433203125" Y="4.261727539062" />
                  <Point X="24.779564453125" Y="4.247105957031" />
                  <Point X="24.79233984375" Y="4.218912109375" />
                  <Point X="24.7998828125" Y="4.20533984375" />
                  <Point X="24.8177421875" Y="4.17861328125" />
                  <Point X="24.82739453125" Y="4.166452148438" />
                  <Point X="24.84855078125" Y="4.143864746094" />
                  <Point X="24.87310546875" Y="4.125024414062" />
                  <Point X="24.900400390625" Y="4.110435546875" />
                  <Point X="24.91464453125" Y="4.104260742188" />
                  <Point X="24.94508203125" Y="4.093928222656" />
                  <Point X="24.960142578125" Y="4.090155273438" />
                  <Point X="24.9906796875" Y="4.08511328125" />
                  <Point X="25.0216328125" Y="4.08511328125" />
                  <Point X="25.052169921875" Y="4.090155273438" />
                  <Point X="25.06723046875" Y="4.093928222656" />
                  <Point X="25.09766796875" Y="4.104260742188" />
                  <Point X="25.111912109375" Y="4.110435546875" />
                  <Point X="25.13920703125" Y="4.125024414062" />
                  <Point X="25.16376171875" Y="4.143864746094" />
                  <Point X="25.18491796875" Y="4.166452148438" />
                  <Point X="25.1945703125" Y="4.17861328125" />
                  <Point X="25.2124296875" Y="4.20533984375" />
                  <Point X="25.21997265625" Y="4.218912109375" />
                  <Point X="25.232748046875" Y="4.247105957031" />
                  <Point X="25.23798046875" Y="4.261727539062" />
                  <Point X="25.355048828125" Y="4.698629394531" />
                  <Point X="25.378193359375" Y="4.785006347656" />
                  <Point X="25.43825" Y="4.778716796875" />
                  <Point X="25.82787890625" Y="4.737912109375" />
                  <Point X="26.07026953125" Y="4.679391601562" />
                  <Point X="26.453591796875" Y="4.586845703125" />
                  <Point X="26.6095" Y="4.530296386719" />
                  <Point X="26.858265625" Y="4.440067382812" />
                  <Point X="27.010822265625" Y="4.368721679688" />
                  <Point X="27.25044921875" Y="4.256654785156" />
                  <Point X="27.397892578125" Y="4.170755371094" />
                  <Point X="27.629421875" Y="4.035865234375" />
                  <Point X="27.76844140625" Y="3.937003662109" />
                  <Point X="27.81778125" Y="3.901915039062" />
                  <Point X="27.404966796875" Y="3.186896484375" />
                  <Point X="27.0653125" Y="2.598597900391" />
                  <Point X="27.062380859375" Y="2.593115234375" />
                  <Point X="27.05318359375" Y="2.573439208984" />
                  <Point X="27.044185546875" Y="2.54956640625" />
                  <Point X="27.041306640625" Y="2.540606201172" />
                  <Point X="27.032841796875" Y="2.508957275391" />
                  <Point X="27.0198359375" Y="2.460319824219" />
                  <Point X="27.017916015625" Y="2.451479736328" />
                  <Point X="27.013646484375" Y="2.420222900391" />
                  <Point X="27.012755859375" Y="2.383238769531" />
                  <Point X="27.013412109375" Y="2.369576171875" />
                  <Point X="27.016712890625" Y="2.342208984375" />
                  <Point X="27.021783203125" Y="2.300151367188" />
                  <Point X="27.02380078125" Y="2.289031982422" />
                  <Point X="27.029142578125" Y="2.267114257812" />
                  <Point X="27.032466796875" Y="2.256315917969" />
                  <Point X="27.040732421875" Y="2.234226318359" />
                  <Point X="27.04531640625" Y="2.223895019531" />
                  <Point X="27.055681640625" Y="2.203843505859" />
                  <Point X="27.061462890625" Y="2.194123291016" />
                  <Point X="27.0783984375" Y="2.169166992187" />
                  <Point X="27.104421875" Y="2.130814697266" />
                  <Point X="27.1098125" Y="2.12362890625" />
                  <Point X="27.130462890625" Y="2.100124267578" />
                  <Point X="27.157595703125" Y="2.075388671875" />
                  <Point X="27.16825390625" Y="2.066983642578" />
                  <Point X="27.193208984375" Y="2.050049560547" />
                  <Point X="27.2315625" Y="2.024026245117" />
                  <Point X="27.24128125" Y="2.018245361328" />
                  <Point X="27.261330078125" Y="2.007881103516" />
                  <Point X="27.27166015625" Y="2.003297485352" />
                  <Point X="27.293748046875" Y="1.995031860352" />
                  <Point X="27.304548828125" Y="1.991707397461" />
                  <Point X="27.326470703125" Y="1.986365112305" />
                  <Point X="27.337591796875" Y="1.984346801758" />
                  <Point X="27.364958984375" Y="1.98104675293" />
                  <Point X="27.407015625" Y="1.975975219727" />
                  <Point X="27.41604296875" Y="1.975320922852" />
                  <Point X="27.44757421875" Y="1.975496948242" />
                  <Point X="27.484314453125" Y="1.979822509766" />
                  <Point X="27.497748046875" Y="1.982395385742" />
                  <Point X="27.5293984375" Y="1.990858764648" />
                  <Point X="27.57803515625" Y="2.003865112305" />
                  <Point X="27.583998046875" Y="2.005671142578" />
                  <Point X="27.604412109375" Y="2.013069458008" />
                  <Point X="27.627658203125" Y="2.023574829102" />
                  <Point X="27.63603515625" Y="2.027872558594" />
                  <Point X="28.540119140625" Y="2.549845947266" />
                  <Point X="28.94040625" Y="2.780951660156" />
                  <Point X="29.04394921875" Y="2.637048095703" />
                  <Point X="29.121447265625" Y="2.508983154297" />
                  <Point X="29.136884765625" Y="2.483471191406" />
                  <Point X="28.620140625" Y="2.086958251953" />
                  <Point X="28.172953125" Y="1.743820068359" />
                  <Point X="28.168142578125" Y="1.739871337891" />
                  <Point X="28.152125" Y="1.725221191406" />
                  <Point X="28.134669921875" Y="1.706603881836" />
                  <Point X="28.12857421875" Y="1.699420532227" />
                  <Point X="28.105796875" Y="1.669705078125" />
                  <Point X="28.07079296875" Y="1.62403918457" />
                  <Point X="28.0656328125" Y="1.616598510742" />
                  <Point X="28.049740234375" Y="1.589373046875" />
                  <Point X="28.034763671875" Y="1.555547363281" />
                  <Point X="28.030140625" Y="1.542671508789" />
                  <Point X="28.02165625" Y="1.51233190918" />
                  <Point X="28.008615234375" Y="1.465707275391" />
                  <Point X="28.006224609375" Y="1.454661865234" />
                  <Point X="28.002771484375" Y="1.432365356445" />
                  <Point X="28.001708984375" Y="1.421114135742" />
                  <Point X="28.000892578125" Y="1.397542602539" />
                  <Point X="28.001173828125" Y="1.386239013672" />
                  <Point X="28.003078125" Y="1.363749023438" />
                  <Point X="28.004701171875" Y="1.35256237793" />
                  <Point X="28.01166796875" Y="1.318805786133" />
                  <Point X="28.02237109375" Y="1.26692956543" />
                  <Point X="28.024599609375" Y="1.25823034668" />
                  <Point X="28.0346875" Y="1.2286015625" />
                  <Point X="28.0502890625" Y="1.195369750977" />
                  <Point X="28.05691796875" Y="1.183529418945" />
                  <Point X="28.075861328125" Y="1.154734863281" />
                  <Point X="28.104974609375" Y="1.110483886719" />
                  <Point X="28.111734375" Y="1.101430664062" />
                  <Point X="28.12628515625" Y="1.084186889648" />
                  <Point X="28.134078125" Y="1.07599609375" />
                  <Point X="28.1513203125" Y="1.059906616211" />
                  <Point X="28.16003125" Y="1.052697021484" />
                  <Point X="28.17824609375" Y="1.039368408203" />
                  <Point X="28.187748046875" Y="1.033249511719" />
                  <Point X="28.215201171875" Y="1.017795837402" />
                  <Point X="28.257390625" Y="0.994047180176" />
                  <Point X="28.26548046875" Y="0.989987854004" />
                  <Point X="28.294681640625" Y="0.978083312988" />
                  <Point X="28.33027734375" Y="0.968021118164" />
                  <Point X="28.343671875" Y="0.965257751465" />
                  <Point X="28.380791015625" Y="0.960351867676" />
                  <Point X="28.437833984375" Y="0.952812988281" />
                  <Point X="28.444033203125" Y="0.952199768066" />
                  <Point X="28.46571875" Y="0.951222839355" />
                  <Point X="28.49121875" Y="0.952032409668" />
                  <Point X="28.50060546875" Y="0.952797180176" />
                  <Point X="29.359423828125" Y="1.06586315918" />
                  <Point X="29.704703125" Y="1.111319702148" />
                  <Point X="29.752689453125" Y="0.914213500977" />
                  <Point X="29.77710546875" Y="0.757382751465" />
                  <Point X="29.78387109375" Y="0.713921020508" />
                  <Point X="29.204478515625" Y="0.558672851562" />
                  <Point X="28.6919921875" Y="0.421352813721" />
                  <Point X="28.68603125" Y="0.419543792725" />
                  <Point X="28.665625" Y="0.412136993408" />
                  <Point X="28.642380859375" Y="0.401618652344" />
                  <Point X="28.63400390625" Y="0.397315887451" />
                  <Point X="28.597537109375" Y="0.376236846924" />
                  <Point X="28.541494140625" Y="0.343843292236" />
                  <Point X="28.53388671875" Y="0.338947357178" />
                  <Point X="28.50877734375" Y="0.319872192383" />
                  <Point X="28.48199609375" Y="0.29435244751" />
                  <Point X="28.472796875" Y="0.284227661133" />
                  <Point X="28.450916015625" Y="0.256346893311" />
                  <Point X="28.417291015625" Y="0.213500076294" />
                  <Point X="28.41085546875" Y="0.204208480835" />
                  <Point X="28.399130859375" Y="0.184927474976" />
                  <Point X="28.393841796875" Y="0.174937927246" />
                  <Point X="28.384068359375" Y="0.153474700928" />
                  <Point X="28.38000390625" Y="0.142925445557" />
                  <Point X="28.373158203125" Y="0.121423423767" />
                  <Point X="28.370376953125" Y="0.110470657349" />
                  <Point X="28.363083984375" Y="0.072386619568" />
                  <Point X="28.351875" Y="0.0138601017" />
                  <Point X="28.350603515625" Y="0.004966059208" />
                  <Point X="28.3485859375" Y="-0.02626140213" />
                  <Point X="28.35028125" Y="-0.062939163208" />
                  <Point X="28.351875" Y="-0.076420303345" />
                  <Point X="28.35916796875" Y="-0.114504188538" />
                  <Point X="28.370376953125" Y="-0.173030700684" />
                  <Point X="28.373158203125" Y="-0.183983322144" />
                  <Point X="28.38000390625" Y="-0.205485351562" />
                  <Point X="28.384068359375" Y="-0.216034744263" />
                  <Point X="28.393841796875" Y="-0.237497833252" />
                  <Point X="28.399130859375" Y="-0.247487518311" />
                  <Point X="28.41085546875" Y="-0.26676852417" />
                  <Point X="28.417291015625" Y="-0.276059844971" />
                  <Point X="28.439171875" Y="-0.303940917969" />
                  <Point X="28.472796875" Y="-0.346787719727" />
                  <Point X="28.478720703125" Y="-0.353636413574" />
                  <Point X="28.501140625" Y="-0.375806182861" />
                  <Point X="28.530177734375" Y="-0.398725341797" />
                  <Point X="28.54149609375" Y="-0.406403625488" />
                  <Point X="28.577962890625" Y="-0.427482391357" />
                  <Point X="28.634005859375" Y="-0.459875946045" />
                  <Point X="28.63949609375" Y="-0.462813903809" />
                  <Point X="28.659158203125" Y="-0.472016204834" />
                  <Point X="28.68302734375" Y="-0.48102734375" />
                  <Point X="28.6919921875" Y="-0.483912719727" />
                  <Point X="29.479568359375" Y="-0.69494329834" />
                  <Point X="29.78487890625" Y="-0.776751098633" />
                  <Point X="29.761619140625" Y="-0.931040771484" />
                  <Point X="29.730328125" Y="-1.068157836914" />
                  <Point X="29.7278046875" Y="-1.079219726563" />
                  <Point X="29.031123046875" Y="-0.987499816895" />
                  <Point X="28.436783203125" Y="-0.909253540039" />
                  <Point X="28.428625" Y="-0.908535583496" />
                  <Point X="28.40009765625" Y="-0.908042419434" />
                  <Point X="28.36672265625" Y="-0.910840881348" />
                  <Point X="28.354482421875" Y="-0.912676208496" />
                  <Point X="28.28291015625" Y="-0.928233032227" />
                  <Point X="28.17291796875" Y="-0.952140197754" />
                  <Point X="28.157876953125" Y="-0.956742004395" />
                  <Point X="28.1287578125" Y="-0.968365661621" />
                  <Point X="28.114681640625" Y="-0.975386901855" />
                  <Point X="28.086853515625" Y="-0.99227923584" />
                  <Point X="28.074123046875" Y="-1.001530395508" />
                  <Point X="28.050373046875" Y="-1.02200189209" />
                  <Point X="28.0393515625" Y="-1.033222290039" />
                  <Point X="27.99608984375" Y="-1.085252075195" />
                  <Point X="27.929607421875" Y="-1.165210327148" />
                  <Point X="27.921328125" Y="-1.176845092773" />
                  <Point X="27.906603515625" Y="-1.201232543945" />
                  <Point X="27.900158203125" Y="-1.213985229492" />
                  <Point X="27.88881640625" Y="-1.24137097168" />
                  <Point X="27.884359375" Y="-1.254940185547" />
                  <Point X="27.87753125" Y="-1.282584106445" />
                  <Point X="27.87516015625" Y="-1.296658691406" />
                  <Point X="27.868958984375" Y="-1.364039672852" />
                  <Point X="27.859431640625" Y="-1.467588989258" />
                  <Point X="27.859291015625" Y="-1.483319946289" />
                  <Point X="27.861609375" Y="-1.514587036133" />
                  <Point X="27.864068359375" Y="-1.530123291016" />
                  <Point X="27.871798828125" Y="-1.561743652344" />
                  <Point X="27.87678515625" Y="-1.576662475586" />
                  <Point X="27.88915625" Y="-1.605476074219" />
                  <Point X="27.896541015625" Y="-1.61937097168" />
                  <Point X="27.936150390625" Y="-1.680981079102" />
                  <Point X="27.997021484375" Y="-1.775661621094" />
                  <Point X="28.00174609375" Y="-1.782356811523" />
                  <Point X="28.019798828125" Y="-1.804454223633" />
                  <Point X="28.0434921875" Y="-1.828121459961" />
                  <Point X="28.052798828125" Y="-1.836277832031" />
                  <Point X="28.783673828125" Y="-2.397098632812" />
                  <Point X="29.087171875" Y="-2.629981201172" />
                  <Point X="29.0454921875" Y="-2.697424316406" />
                  <Point X="29.001275390625" Y="-2.760251708984" />
                  <Point X="28.377794921875" Y="-2.40028515625" />
                  <Point X="27.848455078125" Y="-2.094670654297" />
                  <Point X="27.841193359375" Y="-2.090885498047" />
                  <Point X="27.815029296875" Y="-2.079513916016" />
                  <Point X="27.783123046875" Y="-2.069326416016" />
                  <Point X="27.771111328125" Y="-2.066337646484" />
                  <Point X="27.6859296875" Y="-2.050953857422" />
                  <Point X="27.555021484375" Y="-2.027311889648" />
                  <Point X="27.539361328125" Y="-2.025807495117" />
                  <Point X="27.5080078125" Y="-2.025403198242" />
                  <Point X="27.492314453125" Y="-2.026503417969" />
                  <Point X="27.460140625" Y="-2.031461181641" />
                  <Point X="27.44484375" Y="-2.03513659668" />
                  <Point X="27.415068359375" Y="-2.044960571289" />
                  <Point X="27.40058984375" Y="-2.051109130859" />
                  <Point X="27.32982421875" Y="-2.088352783203" />
                  <Point X="27.221072265625" Y="-2.145588134766" />
                  <Point X="27.208970703125" Y="-2.153170166016" />
                  <Point X="27.1860390625" Y="-2.170063720703" />
                  <Point X="27.175208984375" Y="-2.179375244141" />
                  <Point X="27.15425" Y="-2.200334716797" />
                  <Point X="27.14494140625" Y="-2.211161132813" />
                  <Point X="27.128048828125" Y="-2.234090576172" />
                  <Point X="27.12046484375" Y="-2.246193603516" />
                  <Point X="27.083220703125" Y="-2.316959716797" />
                  <Point X="27.025984375" Y="-2.425711669922" />
                  <Point X="27.0198359375" Y="-2.440193115234" />
                  <Point X="27.01001171875" Y="-2.469971679688" />
                  <Point X="27.0063359375" Y="-2.485268798828" />
                  <Point X="27.00137890625" Y="-2.517442138672" />
                  <Point X="27.000279296875" Y="-2.533133544922" />
                  <Point X="27.00068359375" Y="-2.564484619141" />
                  <Point X="27.0021875" Y="-2.580144287109" />
                  <Point X="27.017572265625" Y="-2.665327148438" />
                  <Point X="27.04121484375" Y="-2.796234619141" />
                  <Point X="27.04301953125" Y="-2.804230957031" />
                  <Point X="27.05123828125" Y="-2.831540771484" />
                  <Point X="27.064072265625" Y="-2.862479248047" />
                  <Point X="27.06955078125" Y="-2.873578613281" />
                  <Point X="27.5392109375" Y="-3.687055664062" />
                  <Point X="27.735896484375" Y="-4.027722900391" />
                  <Point X="27.723755859375" Y="-4.036083007813" />
                  <Point X="27.2378515625" Y="-3.402840332031" />
                  <Point X="26.83391796875" Y="-2.876422607422" />
                  <Point X="26.82865234375" Y="-2.870142089844" />
                  <Point X="26.80883203125" Y="-2.849625732422" />
                  <Point X="26.78325390625" Y="-2.828004638672" />
                  <Point X="26.77330078125" Y="-2.820646972656" />
                  <Point X="26.689287109375" Y="-2.766634033203" />
                  <Point X="26.56017578125" Y="-2.683628173828" />
                  <Point X="26.546283203125" Y="-2.676245117188" />
                  <Point X="26.517470703125" Y="-2.663873535156" />
                  <Point X="26.50255078125" Y="-2.658885009766" />
                  <Point X="26.4709296875" Y="-2.651153564453" />
                  <Point X="26.455392578125" Y="-2.6486953125" />
                  <Point X="26.424125" Y="-2.646376708984" />
                  <Point X="26.40839453125" Y="-2.646516357422" />
                  <Point X="26.31651171875" Y="-2.654971435547" />
                  <Point X="26.17530859375" Y="-2.667965332031" />
                  <Point X="26.1612265625" Y="-2.670338867188" />
                  <Point X="26.133576171875" Y="-2.677170410156" />
                  <Point X="26.1200078125" Y="-2.681628417969" />
                  <Point X="26.092623046875" Y="-2.692971435547" />
                  <Point X="26.079875" Y="-2.699414794922" />
                  <Point X="26.055494140625" Y="-2.714135742188" />
                  <Point X="26.043861328125" Y="-2.722413330078" />
                  <Point X="25.972912109375" Y="-2.781405517578" />
                  <Point X="25.863876953125" Y="-2.872063720703" />
                  <Point X="25.852654296875" Y="-2.883087158203" />
                  <Point X="25.83218359375" Y="-2.906836425781" />
                  <Point X="25.822935546875" Y="-2.919562255859" />
                  <Point X="25.80604296875" Y="-2.947388671875" />
                  <Point X="25.79901953125" Y="-2.961467041016" />
                  <Point X="25.78739453125" Y="-2.990588867188" />
                  <Point X="25.78279296875" Y="-3.005632324219" />
                  <Point X="25.761580078125" Y="-3.103231689453" />
                  <Point X="25.728978515625" Y="-3.253220214844" />
                  <Point X="25.7275859375" Y="-3.261287109375" />
                  <Point X="25.724724609375" Y="-3.289677001953" />
                  <Point X="25.7247421875" Y="-3.323169921875" />
                  <Point X="25.7255546875" Y="-3.335520263672" />
                  <Point X="25.83308984375" Y="-4.152330566406" />
                  <Point X="25.8129765625" Y="-4.077262939453" />
                  <Point X="25.655068359375" Y="-3.487936767578" />
                  <Point X="25.652607421875" Y="-3.480122558594" />
                  <Point X="25.64214453125" Y="-3.453573486328" />
                  <Point X="25.62678515625" Y="-3.423810546875" />
                  <Point X="25.62040625" Y="-3.413208984375" />
                  <Point X="25.55586328125" Y="-3.320216796875" />
                  <Point X="25.456677734375" Y="-3.17730859375" />
                  <Point X="25.446671875" Y="-3.165173339844" />
                  <Point X="25.4247890625" Y="-3.142718505859" />
                  <Point X="25.412912109375" Y="-3.132398925781" />
                  <Point X="25.38665625" Y="-3.113154785156" />
                  <Point X="25.3732421875" Y="-3.104937988281" />
                  <Point X="25.3452421875" Y="-3.090830078125" />
                  <Point X="25.33065625" Y="-3.084938964844" />
                  <Point X="25.230783203125" Y="-3.053941650391" />
                  <Point X="25.077296875" Y="-3.006305664062" />
                  <Point X="25.063376953125" Y="-3.003109619141" />
                  <Point X="25.035216796875" Y="-2.998840087891" />
                  <Point X="25.0209765625" Y="-2.997766601562" />
                  <Point X="24.9913359375" Y="-2.997766601562" />
                  <Point X="24.977095703125" Y="-2.998840087891" />
                  <Point X="24.948935546875" Y="-3.003109619141" />
                  <Point X="24.935015625" Y="-3.006305664062" />
                  <Point X="24.835142578125" Y="-3.037302734375" />
                  <Point X="24.68165625" Y="-3.084938964844" />
                  <Point X="24.6670703125" Y="-3.090830078125" />
                  <Point X="24.6390703125" Y="-3.104937988281" />
                  <Point X="24.62565625" Y="-3.113154785156" />
                  <Point X="24.599400390625" Y="-3.132398925781" />
                  <Point X="24.587525390625" Y="-3.142717041016" />
                  <Point X="24.565640625" Y="-3.165172851562" />
                  <Point X="24.555630859375" Y="-3.177310546875" />
                  <Point X="24.49108984375" Y="-3.270302490234" />
                  <Point X="24.391904296875" Y="-3.4132109375" />
                  <Point X="24.38753125" Y="-3.420131591797" />
                  <Point X="24.374025390625" Y="-3.445260986328" />
                  <Point X="24.36122265625" Y="-3.476213623047" />
                  <Point X="24.35724609375" Y="-3.487936523438" />
                  <Point X="24.11457421875" Y="-4.393600097656" />
                  <Point X="24.014572265625" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.752598547492" Y="4.478393558904" />
                  <Point X="25.361749760397" Y="4.723637726154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.121774638831" Y="4.316832325239" />
                  <Point X="25.337067932177" Y="4.631524270274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.642426919735" Y="4.754008222674" />
                  <Point X="24.542060523929" Y="4.771705526211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.395617260313" Y="4.172080954475" />
                  <Point X="25.312386028313" Y="4.539410827731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.669556753374" Y="4.652758972892" />
                  <Point X="24.213233340956" Y="4.73322110243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.632181503482" Y="4.033902767604" />
                  <Point X="25.287704124449" Y="4.447297385188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.696686587012" Y="4.55150972311" />
                  <Point X="23.950360120266" Y="4.683107215614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.812553549358" Y="3.905632781221" />
                  <Point X="25.263022220584" Y="4.355183942646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.723815724138" Y="4.450260596142" />
                  <Point X="23.739133326091" Y="4.623886670347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.768698085897" Y="3.816900154552" />
                  <Point X="25.23834031672" Y="4.263070500103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.750944821372" Y="4.349011476209" />
                  <Point X="23.527906596505" Y="4.56466611369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.718149830843" Y="3.729347647616" />
                  <Point X="25.191583018853" Y="4.174849545133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.77941412315" Y="4.247526042054" />
                  <Point X="23.533295997511" Y="4.467250288754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.66760157579" Y="3.641795140681" />
                  <Point X="25.079604608909" Y="4.098128831934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.85700303762" Y="4.1373794949" />
                  <Point X="23.534094721704" Y="4.370643924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.617053320736" Y="3.554242633745" />
                  <Point X="23.508176731719" Y="4.278748436791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.566505065683" Y="3.46669012681" />
                  <Point X="23.472192919911" Y="4.188627825552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.515956810629" Y="3.379137619875" />
                  <Point X="23.392088339373" Y="4.10628689625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.85976651358" Y="4.200149596557" />
                  <Point X="22.726965691627" Y="4.223565964528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.465408555576" Y="3.291585112939" />
                  <Point X="23.231498600238" Y="4.038137671955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.01708453658" Y="4.075944656422" />
                  <Point X="22.595164773205" Y="4.150340494399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.414860300522" Y="3.204032606004" />
                  <Point X="22.463363754247" Y="4.077115041997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.364311898667" Y="3.116480124953" />
                  <Point X="22.336055794191" Y="4.003097342084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.972417807906" Y="2.736462137189" />
                  <Point X="28.888865564787" Y="2.751194651949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.313763461086" Y="3.028927650202" />
                  <Point X="22.233760883284" Y="3.924669166737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.05049618398" Y="2.626229284748" />
                  <Point X="28.760872344227" Y="2.677297781952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.263215023506" Y="2.941375175451" />
                  <Point X="22.131465972377" Y="3.84624099139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.11584490451" Y="2.518241014034" />
                  <Point X="28.632879123667" Y="2.603400911956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.212666585925" Y="2.8538227007" />
                  <Point X="22.029171061471" Y="3.767812816043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.067573937759" Y="2.430286959728" />
                  <Point X="28.50488593478" Y="2.529504036374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.162118148345" Y="2.766270225949" />
                  <Point X="21.948939083028" Y="3.685494350429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.965348661252" Y="2.351846505958" />
                  <Point X="28.376892829285" Y="2.455607146089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.111569710764" Y="2.678717751198" />
                  <Point X="22.010944773018" Y="3.578095546197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.863123384746" Y="2.273406052187" />
                  <Point X="28.248899723789" Y="2.381710255803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.061435259205" Y="2.591092279542" />
                  <Point X="22.072951856546" Y="3.470696496247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.760898108239" Y="2.194965598416" />
                  <Point X="28.120906618293" Y="2.307813365518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.030469793954" Y="2.500086798407" />
                  <Point X="22.134958940075" Y="3.363297446297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.658672831733" Y="2.116525144646" />
                  <Point X="27.992913512797" Y="2.233916475232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.013319519093" Y="2.406645326462" />
                  <Point X="22.196966023604" Y="3.255898396346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.556447267719" Y="2.03808474157" />
                  <Point X="27.864920407301" Y="2.160019584946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.020731777002" Y="2.308872817276" />
                  <Point X="22.23993881525" Y="3.151855605613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.454221529773" Y="1.959644369164" />
                  <Point X="27.736927301805" Y="2.086122694661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.054315914057" Y="2.20648549966" />
                  <Point X="22.25040516488" Y="3.053544577655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.351995791826" Y="1.881203996757" />
                  <Point X="27.60432619089" Y="2.013038320058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.135264954793" Y="2.095746471586" />
                  <Point X="22.242406880641" Y="2.958489362837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.24977005388" Y="1.802763624351" />
                  <Point X="22.195766340061" Y="2.870247820407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.279081375563" Y="3.031884112457" />
                  <Point X="21.272175290088" Y="3.033101841658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.15075270747" Y="1.723757525952" />
                  <Point X="22.115406275033" Y="2.787951939913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.519628772867" Y="2.893003588044" />
                  <Point X="21.206179259324" Y="2.948273194372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.082509914002" Y="1.63932504355" />
                  <Point X="21.93340968093" Y="2.723577321722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.765304356938" Y="2.753218825942" />
                  <Point X="21.140183300829" Y="2.863444534343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.03332219358" Y="1.551532637651" />
                  <Point X="21.074187704715" Y="2.778615810417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.007305218112" Y="1.459654604253" />
                  <Point X="21.008192108601" Y="2.693787086491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.716767483613" Y="1.061764756213" />
                  <Point X="29.55070641915" Y="1.091045802323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.003062188399" Y="1.363937236743" />
                  <Point X="20.954768615856" Y="2.606741561536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.741305788507" Y="0.960972462871" />
                  <Point X="29.237485356492" Y="1.049809598467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.023139093008" Y="1.263931608642" />
                  <Point X="20.903716644016" Y="2.519277873461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.760967707492" Y="0.861040007932" />
                  <Point X="28.924265190441" Y="1.008573236515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.073305068518" Y="1.158620465517" />
                  <Point X="20.852664672176" Y="2.431814185385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.776409735961" Y="0.761851633547" />
                  <Point X="28.611045024389" Y="0.967336874563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.170535508022" Y="1.045010587557" />
                  <Point X="20.801612700336" Y="2.34435049731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.671665037709" Y="0.683855421806" />
                  <Point X="20.84217030434" Y="2.240733569322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.454535725509" Y="0.62567564972" />
                  <Point X="21.005394716311" Y="2.115487173452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.237406413309" Y="0.567495877634" />
                  <Point X="21.168618644772" Y="1.990240862838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.020276607792" Y="0.509316192534" />
                  <Point X="21.331842573233" Y="1.864994552224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.80314671409" Y="0.451136522983" />
                  <Point X="21.485503582966" Y="1.741434442196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.617008785864" Y="0.387492133733" />
                  <Point X="21.552235683475" Y="1.633202244268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.500132936132" Y="0.311634971305" />
                  <Point X="21.578576966941" Y="1.532092037157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.428511777549" Y="0.227798185824" />
                  <Point X="21.563758075099" Y="1.438239479484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.379087630648" Y="0.140047468292" />
                  <Point X="21.524759891658" Y="1.348650383294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.358270321831" Y="0.047252593373" />
                  <Point X="21.449508706935" Y="1.265453669362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.349575938329" Y="-0.047679880365" />
                  <Point X="21.248306167168" Y="1.204465577581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.36537850273" Y="-0.146931826962" />
                  <Point X="20.352258358993" Y="1.265997454037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.400398625673" Y="-0.249572347634" />
                  <Point X="20.327310492474" Y="1.173930907887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.486365798226" Y="-0.361196207739" />
                  <Point X="20.302362393523" Y="1.08186440272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.801206661296" Y="-0.513176674657" />
                  <Point X="20.277414287755" Y="0.989797898755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.783948013216" Y="-0.782926018188" />
                  <Point X="20.259633498453" Y="0.896467603518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.769782021359" Y="-0.876893699744" />
                  <Point X="20.245721889577" Y="0.802455067379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.752651120615" Y="-0.970338587868" />
                  <Point X="20.231810280702" Y="0.70844253124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.731488610739" Y="-1.063072594527" />
                  <Point X="20.217898671826" Y="0.6144299951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.344173246034" Y="-0.914916993107" />
                  <Point X="21.014260243825" Y="0.377544435428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.1213025003" Y="-0.972084395552" />
                  <Point X="21.618792933058" Y="0.174483483467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.024212009867" Y="-1.051430250648" />
                  <Point X="21.687067071823" Y="0.065979382589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.954259548602" Y="-1.135561272489" />
                  <Point X="21.705978211392" Y="-0.033820689682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.896876717101" Y="-1.221908659195" />
                  <Point X="21.687708990984" Y="-0.127064861337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.873540341056" Y="-1.314259354596" />
                  <Point X="21.639914723263" Y="-0.215102970543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.864805294042" Y="-1.409184658258" />
                  <Point X="21.537122460317" Y="-0.293443449307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.860895619332" Y="-1.50496080525" />
                  <Point X="21.334759134488" Y="-0.354226863187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.889703605995" Y="-1.606505958688" />
                  <Point X="21.117629573866" Y="-0.412406591469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.958034393858" Y="-1.715020048331" />
                  <Point X="20.900500015009" Y="-0.470586320062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.041586735234" Y="-1.826218108546" />
                  <Point X="20.683370515148" Y="-0.528766059058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.202346471404" Y="-1.951029915573" />
                  <Point X="20.466241015288" Y="-0.586945798054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.365570453121" Y="-2.076276235578" />
                  <Point X="20.249111515427" Y="-0.64512553705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.528794434839" Y="-2.201522555582" />
                  <Point X="27.788118262477" Y="-2.070921362427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.531704892086" Y="-2.025708767013" />
                  <Point X="20.225383726903" Y="-0.73740721587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.692018416556" Y="-2.326768875587" />
                  <Point X="28.056311446867" Y="-2.214676585006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.332351175835" Y="-2.087022856262" />
                  <Point X="21.906716661795" Y="-1.130337103974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.744284568201" Y="-1.10169594334" />
                  <Point X="20.239537903064" Y="-0.836368507146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="28.855242382187" Y="-2.452015192755" />
                  <Point X="28.296859890299" Y="-2.35355729388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.199592023227" Y="-2.160079363851" />
                  <Point X="22.028211788274" Y="-1.248225500926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.431063808371" Y="-1.142932200594" />
                  <Point X="20.253692766304" Y="-0.935329919572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.018466327217" Y="-2.577261506291" />
                  <Point X="28.537408374921" Y="-2.492438010017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.122496770865" Y="-2.242950918904" />
                  <Point X="22.051120202695" Y="-1.348730400603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.117843048541" Y="-1.184168457847" />
                  <Point X="20.274930093359" Y="-1.035540161459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="29.056042400371" Y="-2.680352709946" />
                  <Point X="28.77795688043" Y="-2.631318729836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.075740707405" Y="-2.331172091534" />
                  <Point X="22.038294366071" Y="-1.442934387685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.804622288712" Y="-1.2254047151" />
                  <Point X="20.300732069894" Y="-1.136555274207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.029282152945" Y="-2.419445723027" />
                  <Point X="21.992099593777" Y="-1.531254531091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="20.491401822213" Y="-1.266641024075" />
                  <Point X="20.326534046429" Y="-1.237570386955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.002346538344" Y="-2.51116177556" />
                  <Point X="21.902340219405" Y="-1.611893059747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.007309214122" Y="-2.608502357325" />
                  <Point X="21.800114922268" Y="-1.69033350988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.02530466699" Y="-2.708140969325" />
                  <Point X="21.697889625131" Y="-1.768773960013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.044131593628" Y="-2.807926192584" />
                  <Point X="26.643685605667" Y="-2.73731676059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.224632574564" Y="-2.663426404859" />
                  <Point X="21.595664327994" Y="-1.847214410146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.09223804012" Y="-2.912874185176" />
                  <Point X="26.824291357618" Y="-2.865627955659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="26.03830093079" Y="-2.727036636831" />
                  <Point X="21.493439030858" Y="-1.925654860279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.15424473379" Y="-3.020273166383" />
                  <Point X="26.91144765093" Y="-2.977461489838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.942581413288" Y="-2.806624231444" />
                  <Point X="21.391213582452" Y="-2.004095283739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.216251427459" Y="-3.127672147591" />
                  <Point X="26.997050107232" Y="-3.089021040628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.849552113278" Y="-2.886686183985" />
                  <Point X="21.288987957769" Y="-2.082535676117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.278258121129" Y="-3.235071128799" />
                  <Point X="27.082652563535" Y="-3.200580591418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.794255501602" Y="-2.973401427534" />
                  <Point X="22.593555911493" Y="-2.409031732655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.345796972034" Y="-2.365345146917" />
                  <Point X="21.186762333087" Y="-2.160976068495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.340264814799" Y="-3.342470110006" />
                  <Point X="27.168255019838" Y="-3.312140142208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.769770167396" Y="-3.065549530611" />
                  <Point X="22.672993872551" Y="-2.519504316611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.210941635501" Y="-2.438032040723" />
                  <Point X="21.084536708404" Y="-2.239416460873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.402271508468" Y="-3.449869091214" />
                  <Point X="27.253857522787" Y="-3.423699701224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.749576842244" Y="-3.158454430686" />
                  <Point X="25.328033623463" Y="-3.08412498768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.917510402377" Y="-3.011738667595" />
                  <Point X="22.688379250321" Y="-2.61868270195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.08294849327" Y="-2.511928924531" />
                  <Point X="20.982311083721" Y="-2.31785685325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.464278202138" Y="-3.557268072421" />
                  <Point X="27.339460228565" Y="-3.535259296003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.729383017249" Y="-3.251359242624" />
                  <Point X="25.477211983956" Y="-3.206894685702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.719302967527" Y="-3.073254877183" />
                  <Point X="22.66472592008" Y="-2.710977509774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.954955351039" Y="-2.585825808339" />
                  <Point X="20.880085459038" Y="-2.396297245628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.526284895807" Y="-3.664667053629" />
                  <Point X="27.425062934343" Y="-3.646818890783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.727122123026" Y="-3.347426114101" />
                  <Point X="25.553500085282" Y="-3.316811864402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.584392191513" Y="-3.145931995513" />
                  <Point X="22.615422190337" Y="-2.7987494601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.826962208808" Y="-2.659722692147" />
                  <Point X="20.84443968871" Y="-2.4864774627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.588292050934" Y="-3.772066116204" />
                  <Point X="27.510665640121" Y="-3.758378485562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.7401238768" Y="-3.446184202217" />
                  <Point X="25.628141419625" Y="-3.426438673651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.518518832317" Y="-3.230782273106" />
                  <Point X="22.56487372255" Y="-2.886301929524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.698969066577" Y="-2.733619575955" />
                  <Point X="20.908728273999" Y="-2.594278802968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.650299327592" Y="-3.879465200208" />
                  <Point X="27.596268345899" Y="-3.869938080342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.753125630574" Y="-3.544942290334" />
                  <Point X="25.666237687256" Y="-3.529621601628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.458867130293" Y="-3.316729596723" />
                  <Point X="22.514325254763" Y="-2.973854398949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.570975924346" Y="-2.807516459764" />
                  <Point X="20.973018057784" Y="-2.702080354562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="27.71230660425" Y="-3.986864284212" />
                  <Point X="27.681871051677" Y="-3.981497675121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.766127384349" Y="-3.64370037845" />
                  <Point X="25.693367104317" Y="-3.630870777956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.399215427132" Y="-3.402676920139" />
                  <Point X="22.463776786976" Y="-3.061406868374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.442982750886" Y="-2.881413338065" />
                  <Point X="21.050495571868" Y="-2.812207258822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.779129138123" Y="-3.742458466566" />
                  <Point X="25.720496521378" Y="-3.732119954284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.356272390599" Y="-3.491570432294" />
                  <Point X="22.413228319189" Y="-3.148959337799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="21.314989563854" Y="-2.955310213974" />
                  <Point X="21.135301452907" Y="-2.923626351901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.792130891897" Y="-3.841216554682" />
                  <Point X="25.747625938439" Y="-3.833369130612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.331590654859" Y="-3.583683904482" />
                  <Point X="22.362679854887" Y="-3.236511807838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.805132645672" Y="-3.939974642798" />
                  <Point X="25.7747553555" Y="-3.93461830694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.306908919119" Y="-3.675797376669" />
                  <Point X="22.312131461322" Y="-3.32406429035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.818134399446" Y="-4.038732730914" />
                  <Point X="25.801884772561" Y="-4.035867483268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.282227183378" Y="-3.767910848856" />
                  <Point X="22.261583067757" Y="-3.411616772863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="25.831136153221" Y="-4.13749081903" />
                  <Point X="25.829013463921" Y="-4.137116531635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.257545447638" Y="-3.860024321044" />
                  <Point X="22.211034674192" Y="-3.499169255375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.232863711898" Y="-3.952137793231" />
                  <Point X="23.417909036745" Y="-3.808439295947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="23.339532237685" Y="-3.794619351611" />
                  <Point X="22.160486280628" Y="-3.586721737887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.208181976157" Y="-4.044251265419" />
                  <Point X="23.598755534203" Y="-3.936792940945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.906508421996" Y="-3.814731097745" />
                  <Point X="22.109937887063" Y="-3.674274220399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.183500240417" Y="-4.136364737606" />
                  <Point X="23.736434107764" Y="-4.057534916258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.792023090172" Y="-3.891009772979" />
                  <Point X="22.059389493498" Y="-3.761826702912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.158818504677" Y="-4.228478209794" />
                  <Point X="23.807372162686" Y="-4.166508737429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.67779654351" Y="-3.967334079018" />
                  <Point X="22.052233815778" Y="-3.857030491993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.134136768936" Y="-4.320591681981" />
                  <Point X="23.830798482053" Y="-4.267104957721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.585064233361" Y="-4.047448398884" />
                  <Point X="22.214732268672" Y="-3.982148881691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.109454951521" Y="-4.412705139767" />
                  <Point X="23.850684462485" Y="-4.367076920739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="22.519865302331" Y="-4.13241759636" />
                  <Point X="22.419733826888" Y="-4.114761715621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.084772821988" Y="-4.504818542518" />
                  <Point X="23.870571197299" Y="-4.467049016774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.060090692456" Y="-4.596931945269" />
                  <Point X="23.879991532082" Y="-4.565175604092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="24.035408562923" Y="-4.68904534802" />
                  <Point X="23.868203234871" Y="-4.659562537367" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="24.999836914062" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="25.62944921875" Y="-4.126438476562" />
                  <Point X="25.471541015625" Y="-3.537112304688" />
                  <Point X="25.4643203125" Y="-3.521544433594" />
                  <Point X="25.39977734375" Y="-3.428552246094" />
                  <Point X="25.300591796875" Y="-3.285644042969" />
                  <Point X="25.2743359375" Y="-3.266399902344" />
                  <Point X="25.174462890625" Y="-3.235402587891" />
                  <Point X="25.0209765625" Y="-3.187766601562" />
                  <Point X="24.9913359375" Y="-3.187766601562" />
                  <Point X="24.891462890625" Y="-3.218763671875" />
                  <Point X="24.7379765625" Y="-3.266399902344" />
                  <Point X="24.711720703125" Y="-3.285644042969" />
                  <Point X="24.6471796875" Y="-3.378635986328" />
                  <Point X="24.547994140625" Y="-3.521544433594" />
                  <Point X="24.5407734375" Y="-3.537112304688" />
                  <Point X="24.2981015625" Y="-4.442775878906" />
                  <Point X="24.152255859375" Y="-4.987076660156" />
                  <Point X="24.079861328125" Y="-4.973024414062" />
                  <Point X="23.8997578125" Y="-4.938065917969" />
                  <Point X="23.784650390625" Y="-4.908449707031" />
                  <Point X="23.648412109375" Y="-4.873396972656" />
                  <Point X="23.671841796875" Y="-4.695423339844" />
                  <Point X="23.6908515625" Y="-4.5510390625" />
                  <Point X="23.690318359375" Y="-4.534757324219" />
                  <Point X="23.66645703125" Y="-4.414805175781" />
                  <Point X="23.6297890625" Y="-4.230465820313" />
                  <Point X="23.613716796875" Y="-4.20262890625" />
                  <Point X="23.521765625" Y="-4.121989257813" />
                  <Point X="23.38045703125" Y="-3.998064208984" />
                  <Point X="23.350759765625" Y="-3.985762939453" />
                  <Point X="23.22871875" Y="-3.977763916016" />
                  <Point X="23.041169921875" Y="-3.965471435547" />
                  <Point X="23.01012109375" Y="-3.973791015625" />
                  <Point X="22.908431640625" Y="-4.041738525391" />
                  <Point X="22.75215625" Y="-4.146159179688" />
                  <Point X="22.740267578125" Y="-4.157294433594" />
                  <Point X="22.604025390625" Y="-4.33484765625" />
                  <Point X="22.54290625" Y="-4.4145" />
                  <Point X="22.408166015625" Y="-4.331072753906" />
                  <Point X="22.144166015625" Y="-4.167610351562" />
                  <Point X="21.984779296875" Y="-4.044887939453" />
                  <Point X="21.771419921875" Y="-3.880607910156" />
                  <Point X="22.200509765625" Y="-3.137401855469" />
                  <Point X="22.493966796875" Y="-2.629119628906" />
                  <Point X="22.50023828125" Y="-2.597591552734" />
                  <Point X="22.48601953125" Y="-2.56876171875" />
                  <Point X="22.468677734375" Y="-2.551418945312" />
                  <Point X="22.43984765625" Y="-2.53719921875" />
                  <Point X="22.408318359375" Y="-2.543469726562" />
                  <Point X="21.627201171875" Y="-2.994447998047" />
                  <Point X="21.157041015625" Y="-3.265894287109" />
                  <Point X="21.046869140625" Y="-3.121151855469" />
                  <Point X="20.838302734375" Y="-2.847135742188" />
                  <Point X="20.724029296875" Y="-2.65551953125" />
                  <Point X="20.56898046875" Y="-2.395526855469" />
                  <Point X="21.322759765625" Y="-1.817132324219" />
                  <Point X="21.836212890625" Y="-1.423144775391" />
                  <Point X="21.854177734375" Y="-1.396014770508" />
                  <Point X="21.8618828125" Y="-1.366267578125" />
                  <Point X="21.85967578125" Y="-1.334596435547" />
                  <Point X="21.838841796875" Y="-1.310639404297" />
                  <Point X="21.812359375" Y="-1.295052856445" />
                  <Point X="21.78047265625" Y="-1.288571044922" />
                  <Point X="20.79438671875" Y="-1.418391723633" />
                  <Point X="20.19671875" Y="-1.497076293945" />
                  <Point X="20.15418359375" Y="-1.330552368164" />
                  <Point X="20.072607421875" Y="-1.011187438965" />
                  <Point X="20.042375" Y="-0.799809875488" />
                  <Point X="20.00160546875" Y="-0.514742248535" />
                  <Point X="20.857634765625" Y="-0.285369415283" />
                  <Point X="21.442537109375" Y="-0.128645401001" />
                  <Point X="21.469046875" Y="-0.113831710815" />
                  <Point X="21.485859375" Y="-0.102162872314" />
                  <Point X="21.497677734375" Y="-0.090644470215" />
                  <Point X="21.50875" Y="-0.064156013489" />
                  <Point X="21.516599609375" Y="-0.031280040741" />
                  <Point X="21.51070703125" Y="-0.004711108208" />
                  <Point X="21.497677734375" Y="0.028084388733" />
                  <Point X="21.47491796875" Y="0.047195827484" />
                  <Point X="21.45810546875" Y="0.058864818573" />
                  <Point X="21.442537109375" Y="0.066085319519" />
                  <Point X="20.543673828125" Y="0.306935272217" />
                  <Point X="20.001814453125" Y="0.452126068115" />
                  <Point X="20.02978125" Y="0.641131347656" />
                  <Point X="20.08235546875" Y="0.996414733887" />
                  <Point X="20.143212890625" Y="1.221000976562" />
                  <Point X="20.226484375" Y="1.528298828125" />
                  <Point X="20.827462890625" Y="1.449178466797" />
                  <Point X="21.246736328125" Y="1.393980224609" />
                  <Point X="21.268296875" Y="1.395866577148" />
                  <Point X="21.292509765625" Y="1.403501220703" />
                  <Point X="21.32972265625" Y="1.415234008789" />
                  <Point X="21.348466796875" Y="1.426055908203" />
                  <Point X="21.360880859375" Y="1.443785522461" />
                  <Point X="21.370595703125" Y="1.46724230957" />
                  <Point X="21.38552734375" Y="1.503289916992" />
                  <Point X="21.389287109375" Y="1.524605834961" />
                  <Point X="21.38368359375" Y="1.545512817383" />
                  <Point X="21.3719609375" Y="1.568033569336" />
                  <Point X="21.353943359375" Y="1.602642700195" />
                  <Point X="21.34003125" Y="1.619221679688" />
                  <Point X="20.82444140625" Y="2.014848388672" />
                  <Point X="20.52389453125" Y="2.245466064453" />
                  <Point X="20.635705078125" Y="2.437022705078" />
                  <Point X="20.83998828125" Y="2.787007324219" />
                  <Point X="21.0011953125" Y="2.994217285156" />
                  <Point X="21.225330078125" Y="3.282310791016" />
                  <Point X="21.583337890625" Y="3.075614013672" />
                  <Point X="21.84084375" Y="2.926943359375" />
                  <Point X="21.861486328125" Y="2.920434814453" />
                  <Point X="21.8952109375" Y="2.917484375" />
                  <Point X="21.94703515625" Y="2.912950195312" />
                  <Point X="21.968494140625" Y="2.915775146484" />
                  <Point X="21.986748046875" Y="2.927404296875" />
                  <Point X="22.010685546875" Y="2.951341552734" />
                  <Point X="22.04747265625" Y="2.988127685547" />
                  <Point X="22.0591015625" Y="3.006381591797" />
                  <Point X="22.061927734375" Y="3.027840332031" />
                  <Point X="22.0589765625" Y="3.061563964844" />
                  <Point X="22.054443359375" Y="3.113389892578" />
                  <Point X="22.04793359375" Y="3.134032958984" />
                  <Point X="21.819458984375" Y="3.529761230469" />
                  <Point X="21.69272265625" Y="3.749276611328" />
                  <Point X="21.891330078125" Y="3.901548095703" />
                  <Point X="22.247123046875" Y="4.174330566406" />
                  <Point X="22.5010234375" Y="4.315391601562" />
                  <Point X="22.858453125" Y="4.513971679688" />
                  <Point X="22.95334375" Y="4.390306640625" />
                  <Point X="23.032173828125" Y="4.287573242188" />
                  <Point X="23.04875390625" Y="4.273661132813" />
                  <Point X="23.0862890625" Y="4.254122070313" />
                  <Point X="23.143970703125" Y="4.224094238281" />
                  <Point X="23.164876953125" Y="4.2184921875" />
                  <Point X="23.186193359375" Y="4.222250488281" />
                  <Point X="23.225287109375" Y="4.238444335938" />
                  <Point X="23.2853671875" Y="4.263330078125" />
                  <Point X="23.30309765625" Y="4.275744628906" />
                  <Point X="23.31391796875" Y="4.294488769531" />
                  <Point X="23.326642578125" Y="4.334845703125" />
                  <Point X="23.346197265625" Y="4.396865234375" />
                  <Point X="23.348083984375" Y="4.41842578125" />
                  <Point X="23.322107421875" Y="4.615725097656" />
                  <Point X="23.31086328125" Y="4.701140625" />
                  <Point X="23.571310546875" Y="4.774161132812" />
                  <Point X="24.031908203125" Y="4.903296386719" />
                  <Point X="24.33970703125" Y="4.939319824219" />
                  <Point X="24.77580078125" Y="4.990358398438" />
                  <Point X="24.88168359375" Y="4.595200683594" />
                  <Point X="24.957859375" Y="4.310903320312" />
                  <Point X="24.97571875" Y="4.284176757813" />
                  <Point X="25.00615625" Y="4.273844238281" />
                  <Point X="25.03659375" Y="4.284176757813" />
                  <Point X="25.054453125" Y="4.310903320312" />
                  <Point X="25.171521484375" Y="4.747805175781" />
                  <Point X="25.236650390625" Y="4.990868652344" />
                  <Point X="25.4580390625" Y="4.96768359375" />
                  <Point X="25.86020703125" Y="4.925565429688" />
                  <Point X="26.114859375" Y="4.864084960938" />
                  <Point X="26.508455078125" Y="4.76905859375" />
                  <Point X="26.67428515625" Y="4.708910644531" />
                  <Point X="26.93103515625" Y="4.615785644531" />
                  <Point X="27.091310546875" Y="4.540830078125" />
                  <Point X="27.33869921875" Y="4.425134277344" />
                  <Point X="27.493537109375" Y="4.33492578125" />
                  <Point X="27.73252734375" Y="4.195688964844" />
                  <Point X="27.8785546875" Y="4.09184375" />
                  <Point X="28.0687421875" Y="3.956592773438" />
                  <Point X="27.569509765625" Y="3.091896484375" />
                  <Point X="27.22985546875" Y="2.503597900391" />
                  <Point X="27.224853515625" Y="2.491514404297" />
                  <Point X="27.216388671875" Y="2.459865478516" />
                  <Point X="27.2033828125" Y="2.411228027344" />
                  <Point X="27.202044921875" Y="2.392327148438" />
                  <Point X="27.205345703125" Y="2.364959960938" />
                  <Point X="27.210416015625" Y="2.32290234375" />
                  <Point X="27.218681640625" Y="2.300812744141" />
                  <Point X="27.2356171875" Y="2.275856445312" />
                  <Point X="27.261640625" Y="2.237504150391" />
                  <Point X="27.27494140625" Y="2.224203369141" />
                  <Point X="27.299896484375" Y="2.207269287109" />
                  <Point X="27.33825" Y="2.181245849609" />
                  <Point X="27.360337890625" Y="2.172980224609" />
                  <Point X="27.387705078125" Y="2.169680175781" />
                  <Point X="27.42976171875" Y="2.164608642578" />
                  <Point X="27.448666015625" Y="2.165946289062" />
                  <Point X="27.48031640625" Y="2.174409667969" />
                  <Point X="27.528953125" Y="2.187416015625" />
                  <Point X="27.54103515625" Y="2.192417480469" />
                  <Point X="28.445119140625" Y="2.714390869141" />
                  <Point X="28.994248046875" Y="3.031430664062" />
                  <Point X="29.060833984375" Y="2.938893310547" />
                  <Point X="29.202595703125" Y="2.741876220703" />
                  <Point X="29.284001953125" Y="2.607350830078" />
                  <Point X="29.387513671875" Y="2.436295898438" />
                  <Point X="28.7358046875" Y="1.936221313477" />
                  <Point X="28.2886171875" Y="1.593082885742" />
                  <Point X="28.27937109375" Y="1.583832885742" />
                  <Point X="28.25659375" Y="1.554117431641" />
                  <Point X="28.22158984375" Y="1.508451538086" />
                  <Point X="28.21312109375" Y="1.491501831055" />
                  <Point X="28.20463671875" Y="1.461162231445" />
                  <Point X="28.191595703125" Y="1.414537475586" />
                  <Point X="28.190779296875" Y="1.390965942383" />
                  <Point X="28.19774609375" Y="1.357209350586" />
                  <Point X="28.20844921875" Y="1.305333129883" />
                  <Point X="28.2156484375" Y="1.287954956055" />
                  <Point X="28.234591796875" Y="1.259160400391" />
                  <Point X="28.263705078125" Y="1.214909423828" />
                  <Point X="28.280947265625" Y="1.198819824219" />
                  <Point X="28.308400390625" Y="1.183366210938" />
                  <Point X="28.35058984375" Y="1.159617553711" />
                  <Point X="28.36856640625" Y="1.153619750977" />
                  <Point X="28.405685546875" Y="1.148713989258" />
                  <Point X="28.462728515625" Y="1.141175048828" />
                  <Point X="28.4758046875" Y="1.14117175293" />
                  <Point X="29.334623046875" Y="1.254237670898" />
                  <Point X="29.8489765625" Y="1.321953125" />
                  <Point X="29.878302734375" Y="1.201480834961" />
                  <Point X="29.939193359375" Y="0.951366455078" />
                  <Point X="29.96484375" Y="0.78661138916" />
                  <Point X="29.997859375" Y="0.574556396484" />
                  <Point X="29.253654296875" Y="0.375146942139" />
                  <Point X="28.74116796875" Y="0.237826858521" />
                  <Point X="28.729087890625" Y="0.232819503784" />
                  <Point X="28.69262109375" Y="0.211740600586" />
                  <Point X="28.636578125" Y="0.179346984863" />
                  <Point X="28.622265625" Y="0.166926391602" />
                  <Point X="28.600384765625" Y="0.139045455933" />
                  <Point X="28.566759765625" Y="0.09619871521" />
                  <Point X="28.556986328125" Y="0.074735565186" />
                  <Point X="28.549693359375" Y="0.036651584625" />
                  <Point X="28.538484375" Y="-0.021874994278" />
                  <Point X="28.538484375" Y="-0.040685081482" />
                  <Point X="28.54577734375" Y="-0.078769065857" />
                  <Point X="28.556986328125" Y="-0.137295639038" />
                  <Point X="28.566759765625" Y="-0.158758789063" />
                  <Point X="28.588640625" Y="-0.186639724731" />
                  <Point X="28.622265625" Y="-0.229486465454" />
                  <Point X="28.636578125" Y="-0.241907058716" />
                  <Point X="28.673044921875" Y="-0.262985961914" />
                  <Point X="28.729087890625" Y="-0.295379577637" />
                  <Point X="28.74116796875" Y="-0.300386932373" />
                  <Point X="29.528744140625" Y="-0.511417541504" />
                  <Point X="29.998068359375" Y="-0.637172485352" />
                  <Point X="29.982427734375" Y="-0.740915771484" />
                  <Point X="29.948431640625" Y="-0.966412963867" />
                  <Point X="29.91556640625" Y="-1.11042956543" />
                  <Point X="29.874548828125" Y="-1.290178588867" />
                  <Point X="29.006322265625" Y="-1.175874389648" />
                  <Point X="28.411982421875" Y="-1.097628173828" />
                  <Point X="28.394837890625" Y="-1.098341186523" />
                  <Point X="28.323265625" Y="-1.113897827148" />
                  <Point X="28.2132734375" Y="-1.137804931641" />
                  <Point X="28.1854453125" Y="-1.154697387695" />
                  <Point X="28.14218359375" Y="-1.206727172852" />
                  <Point X="28.075701171875" Y="-1.286685424805" />
                  <Point X="28.064359375" Y="-1.314071166992" />
                  <Point X="28.058158203125" Y="-1.381452026367" />
                  <Point X="28.048630859375" Y="-1.485001464844" />
                  <Point X="28.056361328125" Y="-1.516621826172" />
                  <Point X="28.095970703125" Y="-1.578231811523" />
                  <Point X="28.156841796875" Y="-1.672912475586" />
                  <Point X="28.168462890625" Y="-1.685540771484" />
                  <Point X="28.899337890625" Y="-2.246361816406" />
                  <Point X="29.33907421875" Y="-2.583783935547" />
                  <Point X="29.299962890625" Y="-2.647073974609" />
                  <Point X="29.2041328125" Y="-2.802140380859" />
                  <Point X="29.13616015625" Y="-2.898720703125" />
                  <Point X="29.056689453125" Y="-3.011638427734" />
                  <Point X="28.282794921875" Y="-2.564830078125" />
                  <Point X="27.753455078125" Y="-2.259215576172" />
                  <Point X="27.737341796875" Y="-2.253312744141" />
                  <Point X="27.65216015625" Y="-2.237928955078" />
                  <Point X="27.521251953125" Y="-2.214287109375" />
                  <Point X="27.489078125" Y="-2.219244873047" />
                  <Point X="27.4183125" Y="-2.256488525391" />
                  <Point X="27.309560546875" Y="-2.313723876953" />
                  <Point X="27.2886015625" Y="-2.334683349609" />
                  <Point X="27.251357421875" Y="-2.405449462891" />
                  <Point X="27.19412109375" Y="-2.514201416016" />
                  <Point X="27.1891640625" Y="-2.546374755859" />
                  <Point X="27.204548828125" Y="-2.631557617188" />
                  <Point X="27.22819140625" Y="-2.762465087891" />
                  <Point X="27.23409375" Y="-2.778578613281" />
                  <Point X="27.70375390625" Y="-3.592055664062" />
                  <Point X="27.98667578125" Y="-4.082089111328" />
                  <Point X="27.94901171875" Y="-4.108991699219" />
                  <Point X="27.83530078125" Y="-4.1902109375" />
                  <Point X="27.7593046875" Y="-4.239403320313" />
                  <Point X="27.67977734375" Y="-4.290879394531" />
                  <Point X="27.08711328125" Y="-3.518505126953" />
                  <Point X="26.6831796875" Y="-2.992087402344" />
                  <Point X="26.67055078125" Y="-2.980467285156" />
                  <Point X="26.586537109375" Y="-2.926454345703" />
                  <Point X="26.45742578125" Y="-2.843448486328" />
                  <Point X="26.4258046875" Y="-2.835717041016" />
                  <Point X="26.333921875" Y="-2.844172119141" />
                  <Point X="26.19271875" Y="-2.857166015625" />
                  <Point X="26.165333984375" Y="-2.868509033203" />
                  <Point X="26.094384765625" Y="-2.927501220703" />
                  <Point X="25.985349609375" Y="-3.018159423828" />
                  <Point X="25.96845703125" Y="-3.045985839844" />
                  <Point X="25.947244140625" Y="-3.143585205078" />
                  <Point X="25.914642578125" Y="-3.293573730469" />
                  <Point X="25.9139296875" Y="-3.310719970703" />
                  <Point X="26.047029296875" Y="-4.321703125" />
                  <Point X="26.127642578125" Y="-4.934028808594" />
                  <Point X="26.10192578125" Y="-4.939666015625" />
                  <Point X="25.9943515625" Y="-4.96324609375" />
                  <Point X="25.924138671875" Y="-4.976001464844" />
                  <Point X="25.860201171875" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#156" />
            <Label Value="Polygon" />
            <Translation X="25.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.076177159468" Y="4.639421517591" Z="1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1" />
                  <Point X="-0.672261821919" Y="5.020261058313" Z="1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1" />
                  <Point X="-1.448344850338" Y="4.853584779719" Z="1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1" />
                  <Point X="-1.733620792968" Y="4.640479712658" Z="1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1" />
                  <Point X="-1.72720063896" Y="4.381160982191" Z="1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1" />
                  <Point X="-1.800004618146" Y="4.315918158695" Z="1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1" />
                  <Point X="-1.896780920762" Y="4.329752074116" Z="1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1" />
                  <Point X="-2.013145325707" Y="4.452024786082" Z="1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1" />
                  <Point X="-2.529416961224" Y="4.390379290713" Z="1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1" />
                  <Point X="-3.145247878103" Y="3.972508051775" Z="1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1" />
                  <Point X="-3.229998571226" Y="3.536041263206" Z="1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1" />
                  <Point X="-2.996990476967" Y="3.088487178959" Z="1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1" />
                  <Point X="-3.030826286637" Y="3.017977281249" Z="1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1" />
                  <Point X="-3.106589286648" Y="2.998574222206" Z="1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1" />
                  <Point X="-3.397818085091" Y="3.150195377519" Z="1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1" />
                  <Point X="-4.044425469262" Y="3.056199602745" Z="1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1" />
                  <Point X="-4.413634280671" Y="2.493507941984" Z="1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1" />
                  <Point X="-4.212153064841" Y="2.006460793445" Z="1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1" />
                  <Point X="-3.678545908584" Y="1.576224970101" Z="1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1" />
                  <Point X="-3.681753853843" Y="1.517656741093" Z="1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1" />
                  <Point X="-3.728681828406" Y="1.482467248474" Z="1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1" />
                  <Point X="-4.172167965227" Y="1.53003074358" Z="1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1" />
                  <Point X="-4.911203439858" Y="1.265358285965" Z="1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1" />
                  <Point X="-5.025835949818" Y="0.679730609772" Z="1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1" />
                  <Point X="-4.475425193671" Y="0.289919041395" Z="1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1" />
                  <Point X="-3.559748065268" Y="0.037400078677" Z="1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1" />
                  <Point X="-3.543203539653" Y="0.011749923755" Z="1" />
                  <Point X="-3.539556741714" Y="0" Z="1" />
                  <Point X="-3.545161061482" Y="-0.018057027306" Z="1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1" />
                  <Point X="-3.565620529855" Y="-0.041475904457" Z="1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1" />
                  <Point X="-4.161462595102" Y="-0.205793015361" Z="1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1" />
                  <Point X="-5.013277458387" Y="-0.775608828104" Z="1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1" />
                  <Point X="-4.900419393092" Y="-1.311646472614" Z="1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1" />
                  <Point X="-4.205245176039" Y="-1.436684005725" Z="1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1" />
                  <Point X="-3.203115760641" Y="-1.316305594501" Z="1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1" />
                  <Point X="-3.197343683568" Y="-1.340470734625" Z="1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1" />
                  <Point X="-3.713835139944" Y="-1.746184524107" Z="1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1" />
                  <Point X="-4.325070834337" Y="-2.649849094522" Z="1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1" />
                  <Point X="-3.999072283982" Y="-3.120155125946" Z="1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1" />
                  <Point X="-3.353956623328" Y="-3.006469146625" Z="1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1" />
                  <Point X="-2.562329774076" Y="-2.566000695056" Z="1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1" />
                  <Point X="-2.848947866543" Y="-3.081121759164" Z="1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1" />
                  <Point X="-3.051881235696" Y="-4.053225123642" Z="1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1" />
                  <Point X="-2.624312930565" Y="-4.34230341616" Z="1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1" />
                  <Point X="-2.36246377842" Y="-4.334005498785" Z="1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1" />
                  <Point X="-2.069946508922" Y="-4.05203189567" Z="1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1" />
                  <Point X="-1.780707136709" Y="-3.996376982851" Z="1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1" />
                  <Point X="-1.517357599152" Y="-4.128300989659" Z="1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1" />
                  <Point X="-1.388738501668" Y="-4.393280191684" Z="1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1" />
                  <Point X="-1.383887100637" Y="-4.657616557391" Z="1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1" />
                  <Point X="-1.23396591394" Y="-4.925592648995" Z="1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1" />
                  <Point X="-0.935823365506" Y="-4.990828593734" Z="1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1" />
                  <Point X="-0.659758613981" Y="-4.424436757366" Z="1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1" />
                  <Point X="-0.317900668868" Y="-3.375864802488" Z="1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1" />
                  <Point X="-0.099874350078" Y="-3.235236680995" Z="1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1" />
                  <Point X="0.153484729283" Y="-3.251875364293" Z="1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1" />
                  <Point X="0.352545190564" Y="-3.42578095328" Z="1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1" />
                  <Point X="0.57499624642" Y="-4.108099272449" Z="1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1" />
                  <Point X="0.926919168241" Y="-4.993916015485" Z="1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1" />
                  <Point X="1.106474154533" Y="-4.957226131579" Z="1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1" />
                  <Point X="1.090444225716" Y="-4.283896611999" Z="1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1" />
                  <Point X="0.989946402976" Y="-3.122925273758" Z="1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1" />
                  <Point X="1.12019401999" Y="-2.934667814719" Z="1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1" />
                  <Point X="1.332347492248" Y="-2.862681743138" Z="1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1" />
                  <Point X="1.55334046768" Y="-2.937232397195" Z="1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1" />
                  <Point X="2.041288601438" Y="-3.517663118036" Z="1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1" />
                  <Point X="2.78031497674" Y="-4.250097824209" Z="1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1" />
                  <Point X="2.971914429933" Y="-4.118398713532" Z="1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1" />
                  <Point X="2.740898304926" Y="-3.535775703409" Z="1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1" />
                  <Point X="2.247595107807" Y="-2.591391204553" Z="1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1" />
                  <Point X="2.289446467121" Y="-2.397456328852" Z="1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1" />
                  <Point X="2.43544204952" Y="-2.269454842846" Z="1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1" />
                  <Point X="2.63711559896" Y="-2.255852901684" Z="1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1" />
                  <Point X="3.251637893698" Y="-2.576851195348" Z="1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1" />
                  <Point X="4.170891924813" Y="-2.896218127943" Z="1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1" />
                  <Point X="4.336338922149" Y="-2.642079384306" Z="1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1" />
                  <Point X="3.923618801309" Y="-2.175413965951" Z="1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1" />
                  <Point X="3.13187192758" Y="-1.519912349234" Z="1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1" />
                  <Point X="3.10179109222" Y="-1.354753078092" Z="1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1" />
                  <Point X="3.174474328718" Y="-1.207413976378" Z="1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1" />
                  <Point X="3.327727056661" Y="-1.131477069357" Z="1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1" />
                  <Point X="3.993639108407" Y="-1.194166665022" Z="1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1" />
                  <Point X="4.958155366102" Y="-1.090273528948" Z="1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1" />
                  <Point X="5.025712526261" Y="-0.717089671205" Z="1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1" />
                  <Point X="4.535529481118" Y="-0.431841189756" Z="1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1" />
                  <Point X="3.691910224156" Y="-0.188417081499" Z="1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1" />
                  <Point X="3.621817229388" Y="-0.12449143726" Z="1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1" />
                  <Point X="3.588728228608" Y="-0.038083948069" Z="1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1" />
                  <Point X="3.592643221816" Y="0.05852658315" Z="1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1" />
                  <Point X="3.633562209012" Y="0.139457301315" Z="1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1" />
                  <Point X="3.711485190197" Y="0.199731762743" Z="1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1" />
                  <Point X="4.260438336189" Y="0.358130743012" Z="1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1" />
                  <Point X="5.0080915971" Y="0.825583551211" Z="1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1" />
                  <Point X="4.920728814306" Y="1.244587885442" Z="1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1" />
                  <Point X="4.321941171661" Y="1.33508988399" Z="1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1" />
                  <Point X="3.406078879144" Y="1.229562949036" Z="1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1" />
                  <Point X="3.326771510059" Y="1.258217393436" Z="1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1" />
                  <Point X="3.270205322625" Y="1.317921859991" Z="1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1" />
                  <Point X="3.240557152412" Y="1.398592686907" Z="1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1" />
                  <Point X="3.246631226726" Y="1.478974236586" Z="1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1" />
                  <Point X="3.290120380529" Y="1.554979693812" Z="1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1" />
                  <Point X="3.760084947073" Y="1.92783358011" Z="1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1" />
                  <Point X="4.32062236048" Y="2.664517094969" Z="1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1" />
                  <Point X="4.095262122014" Y="2.999376348022" Z="1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1" />
                  <Point X="3.413962504015" Y="2.788972215943" Z="1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1" />
                  <Point X="2.461240564858" Y="2.253992505416" Z="1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1" />
                  <Point X="2.387534125997" Y="2.250600533901" Z="1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1" />
                  <Point X="2.321814418281" Y="2.279924174228" Z="1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1" />
                  <Point X="2.270834426007" Y="2.335210442101" Z="1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1" />
                  <Point X="2.248829168857" Y="2.402224317836" Z="1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1" />
                  <Point X="2.258535437241" Y="2.478229011024" Z="1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1" />
                  <Point X="2.606653136917" Y="3.09817657794" Z="1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1" />
                  <Point X="2.901373888255" Y="4.163870724856" Z="1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1" />
                  <Point X="2.512550219494" Y="4.409408642523" Z="1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1" />
                  <Point X="2.106336115216" Y="4.617401883626" Z="1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1" />
                  <Point X="1.685177079362" Y="4.787194762113" Z="1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1" />
                  <Point X="1.120436511103" Y="4.943968307894" Z="1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1" />
                  <Point X="0.457088711277" Y="5.04869150323" Z="1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1" />
                  <Point X="0.117067402085" Y="4.79202598836" Z="1" />
                  <Point X="0" Y="4.355124473572" Z="1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>